define(['angular'], function(angular) {
	angular.module('xtaas.config', [])
		.constant("XTAAS_CONFIG", {
			pusherToken: '@@pusherToken',
			pusherCluster: '@@pusherCluster',
			reportingUrl: '@@reportingUrl'
		});
});