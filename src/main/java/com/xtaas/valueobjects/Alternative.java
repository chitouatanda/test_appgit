package com.xtaas.valueobjects;

public class Alternative {
	
	private String confidence;
	private String content;
	
	public String getConfidence() {
		return confidence;
	}
	public String getContent() {
		return content;
	}

}
