package com.xtaas.valueobjects;

import java.util.ArrayList;
import java.util.List;

public class Item {

    private String start_time;
    private String end_time;
    private List<Alternative> alternatives = new ArrayList<Alternative>();
    private String type;
}