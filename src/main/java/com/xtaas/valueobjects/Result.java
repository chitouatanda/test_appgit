package com.xtaas.valueobjects;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private List<Transcript> transcripts = new ArrayList<Transcript>();
    private List<Item>       items       = new ArrayList<Item>();
	
    public List<Transcript> getTranscripts() {
		return transcripts;
	}
	
    public List<Item> getItems() {
		return items;
	}
	
}
