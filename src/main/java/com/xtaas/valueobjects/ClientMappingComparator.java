package com.xtaas.valueobjects;

import java.util.Comparator;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.ClientMapping;

public class ClientMappingComparator implements Comparator<ClientMapping>{



	@Override
	public int compare(ClientMapping o1, ClientMapping o2) {
		// TODO Auto-generated method stub
		return o1.getSequence() - o2.getSequence();
	}

}
