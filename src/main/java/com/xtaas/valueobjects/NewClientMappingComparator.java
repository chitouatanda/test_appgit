package com.xtaas.valueobjects;

import java.util.Comparator;
import com.xtaas.db.entity.NewClientMapping;

public class NewClientMappingComparator implements Comparator<NewClientMapping> {

	@Override
	public int compare(NewClientMapping o1, NewClientMapping o2) {
		return o1.getSequence() - o2.getSequence();
	}

}
