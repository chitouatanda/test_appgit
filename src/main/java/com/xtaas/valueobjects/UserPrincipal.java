/**
 * 
 */
package com.xtaas.valueobjects;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;

/**
 * @author djain
 *
 */
@SuppressWarnings("serial")
public class UserPrincipal extends User {
	
	private Login login;
	
	private Organization organization;

	public UserPrincipal(Login login, Organization org, List<GrantedAuthority> authorities) {
		super(login.getUsername(), login.getPassword(), authorities);
		
		this.login = login;
		this.organization = org;
	}

	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((login == null) ? 0 : login.getUsername().hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserPrincipal other = (UserPrincipal) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.getUsername().equals(other.login.getUsername()))
			return false;
		return true;
	}
}
