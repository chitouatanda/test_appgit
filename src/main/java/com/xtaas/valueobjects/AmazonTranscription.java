package com.xtaas.valueobjects;

public class AmazonTranscription {

    private String jobName;
    private String accountId;
    private Result results;
    private String status;
    
	public String getJobName() {
		return jobName;
	}
	public String getAccountId() {
		return accountId;
	}
	public Result getResults() {
		return results;
	}
	public String getStatus() {
		return status;
	}
    
	
}