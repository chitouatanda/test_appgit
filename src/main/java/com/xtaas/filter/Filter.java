package com.xtaas.filter;

public interface Filter {
	public boolean execute(String request);
}