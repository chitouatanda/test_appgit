package com.xtaas.filter;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xtaas.service.ProspectCallLogServiceImpl;

@Component
public class StateCodeFilter implements Filter {

	private final ProspectCallLogServiceImpl prospectCallLogServiceImpl;
	
	public StateCodeFilter(@Autowired ProspectCallLogServiceImpl prospectCallLogServiceImpl) {
		this.prospectCallLogServiceImpl = prospectCallLogServiceImpl;
	}

	public boolean execute(String stateCode) {
		List<String> excludedStateCodes = prospectCallLogServiceImpl.getStateCodesWithBusinessClosed();
		if (excludedStateCodes != null && excludedStateCodes.size() > 0) {
			return excludedStateCodes.contains(stateCode) ? false : true;
		}
		return false;
	}
}