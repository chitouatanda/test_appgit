package com.xtaas.filter;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xtaas.service.ProspectCallLogServiceImpl;

@Component
public class TimeZoneFilter implements Filter {

	private final ProspectCallLogServiceImpl prospectCallLogServiceImpl;

	public TimeZoneFilter(@Autowired ProspectCallLogServiceImpl prospectCallLogServiceImpl) {
		super();
		this.prospectCallLogServiceImpl = prospectCallLogServiceImpl;
	}

	public boolean execute(String timezone) {
		List<String> openTimeZones = prospectCallLogServiceImpl.getOpenTimeZones();
		if (openTimeZones != null && openTimeZones.size() > 0) {
			return openTimeZones.contains(timezone) ? true : false;
		}
		return false;
	}
}