package com.xtaas.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.xtaas.datagenarator.specification.AgentSpecification;
import com.xtaas.db.entity.MasterData;
import com.xtaas.db.entity.Vertical;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.FakeAddressRepository;
import com.xtaas.db.repository.MasterDataRepository;
import com.xtaas.db.repository.VerticalRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.FakeAddress;
import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.CompletedCampaign;
import com.xtaas.domain.valueobject.DayOfWeek;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.WorkSchedule;

@Component
public class AgentGenerator {
	private Random random = new Random();

	@Autowired
	private FakeAddressRepository fakeAddressRepository;

	@Autowired
	private MasterDataRepository masterDataRepository;

	@Autowired
	private VerticalRepository verticalRepository;

	@Autowired
	private AgentRepository agentRepository;

	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		AgentGenerator agentGenerator = context.getBean(AgentGenerator.class);
		List<AgentSpecification> agentSpecifications = new ArrayList<AgentSpecification>();
		agentSpecifications.add(new AgentSpecification(12000, 16, 32, 5, 0, 0, 0, 5, 20, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(4000, 8, 32, 5, 1, 0, 0, 5, 20, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(1000, 8, 32, 5, 2, 1, 1, 5, 20, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(1000, 8, 32, 5, 2, 1, 1, 8, 30, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(500, 4, 24, 5, 5, 2, 4, 8, 30, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(500, 4, 24, 5, 10, 5, 8, 8, 30, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(400, 4, 20, 5, 20, 14, 18, 10, 35, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(300, 2, 16, 5, 50, 20, 35, 10, 35, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(200, 0, 14, 5, 100, 30, 60, 15, 35, 20, 80, 20, 80, 100));
		agentSpecifications.add(new AgentSpecification(100, 0, 10, 5, 200, 40, 100, 15, 35, 20, 80, 20, 80, 100));
		agentGenerator.generateFreelancers(agentSpecifications);
	}

	public void generateFreelancers(List<AgentSpecification> agentSpecifications) {
		int requiredAgents = getRequiredAgents(agentSpecifications);

		MasterData languageData = masterDataRepository.findOneByType("Language");
		List<Vertical> verticals = verticalRepository.findAll();

		List<FakeAddress> fakeAddresses = fakeAddressRepository.findAll(PageRequest.of(0, requiredAgents))
				.getContent();

		int globalIndex = 0;
		for (AgentSpecification agentSpecification : agentSpecifications) {
			int localIndex = 0;

			while (localIndex < agentSpecification.getCount()) {
				double averageConversion = generateRandom(agentSpecification.getMinAverageConversion(),
						agentSpecification.getMaxAverageConversion()) + random.nextDouble();
				double averageQuality = generateRandom(agentSpecification.getMinAverageQuality(),
						agentSpecification.getMaxAverageQuality())
						+ random.nextDouble();
				int perHourPrice = generateRandom(agentSpecification.getMinPerHourPrice(),
						agentSpecification.getMaxPerHourPrice());
				int totalReviews = generateRandom(agentSpecification.getMinTotalReviews(),
						agentSpecification.getMaxTotalReviews());
				int averageAvailableHours = generateRandom(agentSpecification.getMinAverageAvailableHours(),
						agentSpecification.getMaxAverageAvailableHours())
						* agentSpecification.getStepAverageAvailableHours();

				FakeAddress fakeAddress = fakeAddresses.get(globalIndex);

				Address address = new Address(fakeAddress.getLine1(), "", fakeAddress.getCity(),
						fakeAddress.getState(), fakeAddress.getCountryCode(), fakeAddress.getPostalCode(), fakeAddress.getTimeZone());

				Agent agent = new Agent(null, fakeAddress.getGivenName(), fakeAddress.getSurName(), address);
				agent.setPerHourPrice(perHourPrice);
				List<WorkSchedule> workSchedules = generateSchedule();
				for (WorkSchedule workSchedule : workSchedules) {
					agent.addWorkSchedule(workSchedule);
				}
				agent.setAverageAvailableHours(averageAvailableHours);

				List<String> languages = getLanauges(languageData, generateRandom(1, 3));

				for (String language : languages) {
					agent.addLanguage(language);
				}

				List<String> domains = getDomains(verticals, generateRandom(1, 3));

				for (String domain : domains) {
					agent.addDomain(domain);
				}

				int totalReview = 0;
				for (int index = 0; index < agentSpecification.getTotalCompletedCampaigns(); index++) {
					if (index < totalReviews) {
						int currentReview = generateRandom(1, 5);
						totalReview += currentReview;
						agent.addCompletedCampaign(new CompletedCampaign((new ObjectId()).toString(), currentReview));
					} else {
						agent.addCompletedCampaign(new CompletedCampaign((new ObjectId()).toString(), 0));
					}
				}
				if (totalReviews != 0) {
					agent.setAverageRating(((totalReview * 2) / totalReviews) / 2.0);
					agent.setAverageConversion(averageConversion);
					agent.setAverageQuality(averageQuality);
					agent.setTotalVolume(agentSpecification.getTotalCompletedCampaigns()
							* agentSpecification.getVolumePerCampaign());
					agent.setOverview(String
							.format("A freelancer agent from city: %s, state: %s, country: %s and timeZone: %s. I charge $%d per hour. My average rating is %f, conversion is %f and average quality is %f. I am avialble %d hours per month. I know the languages: %s and i have skills in domains: %s",
									address.getCity(), fakeAddress.getState(), fakeAddress.getCountry(),
									fakeAddress.getTimeZone(), perHourPrice, ((totalReview * 2) / totalReviews) / 2.0,
									averageConversion, averageQuality, averageAvailableHours, join(languages),
									join(domains)));
				} else {
					agent.setOverview(String
							.format("A freelancer agent from city: %s, state: %s, country: %s and timeZone: %s. I charge $%d per hour. I am avialble %d hours per month. I know the languages: %s and i have skills in domains: %s",
									address.getCity(), fakeAddress.getState(), fakeAddress.getCountry(),
									fakeAddress.getTimeZone(), perHourPrice, averageAvailableHours, join(languages),
									join(domains)));

				}

				agentRepository.save(agent);
				globalIndex++;
				localIndex++;
			}
		}

	}

	private List<WorkSchedule> generateSchedule() {
		List<WorkSchedule> workSchedules = new ArrayList<WorkSchedule>();
		Date startDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.YEAR, 1);
		Date endDate = calendar.getTime();
		int morningStartHour = generateRandom(8, 11);
		int morningEndHour = generateRandom(morningStartHour + 1, 12);
		int afternoonStartHour = generateRandom(13, 17);
		int afternoonEndHour = generateRandom(afternoonStartHour + 1, 20);
		int exceptionDay = generateRandom(0, 4);
		int exceptionAfternoonStartHour = generateRandom(13, 17);
		int exceptionAfternoonEndHour = generateRandom(exceptionAfternoonStartHour + 1, 20);

		List<DayOfWeek> mainRecurrence = new ArrayList<DayOfWeek>();
		List<DayOfWeek> exceptionRecurrence = new ArrayList<DayOfWeek>();
		DayOfWeek[] dayOfWeeks = DayOfWeek.values();
		for (int index = 0; index < 5; index++) {
			if (index == exceptionDay) {
				exceptionRecurrence.add(dayOfWeeks[index]);
			} else {
				mainRecurrence.add(dayOfWeeks[index]);
			}
		}
		workSchedules.add(new WorkSchedule(startDate, morningStartHour, morningEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Daily, null)));
		workSchedules
				.add(new WorkSchedule(startDate, afternoonStartHour, afternoonEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Weekly, mainRecurrence)));
		workSchedules.add(new WorkSchedule(startDate, exceptionAfternoonStartHour, exceptionAfternoonEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Weekly,
				exceptionRecurrence)));
		return workSchedules;
	}

	private int getRequiredAgents(List<AgentSpecification> agentSpecifications) {
		int requiredAgents = 0;
		for (AgentSpecification agentSpecification : agentSpecifications) {
			requiredAgents += agentSpecification.getCount();
		}
		return requiredAgents;
	}

	private List<String> getLanauges(MasterData languageData, int count) {
		Set<String> languages = new HashSet<String>();
		for (int index = 0; index < count; index++) {
			int randomIndex = generateRandom(0, languageData.getData().size() - 1);
			languages.add(languageData.getData().get(randomIndex).getValue());
		}
		List<String> languageList = new ArrayList<String>();
		languageList.addAll(languages);
		return languageList;

	}

	private String join(List<String> items) {
		boolean first = true;
		StringBuilder result = new StringBuilder();

		for (String item : items) {
			if (!first) {
				result.append(", ");
			} else {
				first = false;
			}
			result.append(item);
		}
		return result.toString();
	}

	private List<String> getDomains(List<Vertical> verticals, int count) {
		Set<String> domains = new HashSet<String>();
		for (int index = 0; index < count; index++) {
			int verticalIndex = generateRandom(0, verticals.size() - 1);
			Vertical vertical = verticals.get(verticalIndex);
			int domainIndex = generateRandom(0, vertical.getDomains().size() - 1);
			domains.add(String.format("%s|%s", vertical.getName(), vertical.getDomains().get(domainIndex).getValue()));
		}
		List<String> domainList = new ArrayList<String>();
		domainList.addAll(domains);
		return domainList;
	}

	private int generateRandom(int minimumValue, int maximumValue) {
		return random.nextInt(maximumValue - minimumValue + 1) + minimumValue;
	}

}
