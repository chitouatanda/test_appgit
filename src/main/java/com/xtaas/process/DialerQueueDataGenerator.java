package com.xtaas.process;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.DialerPreProcessEntity;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.DialerPreProcessQueueRepository;
import com.xtaas.db.repository.FakeAddressRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.FakeAddress;


@Component
public class DialerQueueDataGenerator {
	@Autowired
	private FakeAddressRepository fakeAddressRepository;
	
	@Autowired
	private ProspectCallLogRepository prospectLogRepository;
	
	@Autowired
	private DialerPreProcessQueueRepository dialerPreProcessQueueRepository;
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		DialerQueueDataGenerator dialerQueueDataGenerator = context.getBean(DialerQueueDataGenerator.class);
		String campaignId = "54868cdd1d3e9b17c0711750";//"548ffff1e4b035e668b901af"; //54868cdd1d3e9b17c0711750
		dialerQueueDataGenerator.generate(campaignId);
		System.out.println("Data generated for cmapaign " + campaignId);
	}
	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d= new Date();
		//System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid+Long.toString(d.getTime());
		return pcidv;
	}

	private void generate(String campaignId) {
		int count = 50;
		List<FakeAddress> fakeAddresses = fakeAddressRepository.findAll(PageRequest.of(0, count)).getContent();
		
		int i = 1;
		for (FakeAddress fakeAddress : fakeAddresses) {
			Prospect p = new Prospect();
			p.setSource("FakeAddress");
			p.setSourceId(fakeAddress.getId());
			p.setPrefix(fakeAddress.getTitle());
			p.setFirstName(fakeAddress.getGivenName());
			p.setLastName(fakeAddress.getSurName());
			p.setPhone("917744003867");
			/*if (i==1) {
				p.setPrefix("Mr");
				p.setFirstName("Manish");
				p.setLastName("Panjiar");
				p.setPhone("4152389923");
			} else if (i==2) {
				p.setPrefix("Ms");
				p.setFirstName("Gina");
				p.setLastName("McMurry");
				p.setPhone("5036803067");
			} else if (i==3) {
				p.setPrefix("Mr");
				p.setFirstName("Raghava");
				p.setLastName("Reddy");
				p.setPhone("919860000228");
			} else if (i==4) {
				p.setPrefix("Mr");
				p.setFirstName("Dharmveer");
				p.setLastName("Jain");
				p.setPhone("917744003867");
			}*/
			i++;
			String coName = RandomStringUtils.randomAlphabetic(7);
			p.setCompany(coName + " Corporation");
			p.setTitle("Vice President - " + RandomStringUtils.randomAlphabetic(10));
			p.setEmail(RandomStringUtils.randomAlphabetic(7)+"@"+coName+".com");
			p.setIndustry("Communications");
			p.setAddressLine1(fakeAddress.getLine1());
			p.setCity(fakeAddress.getCity());
			p.setStateCode("CA");
			p.setZipCode(fakeAddress.getPostalCode());
			p.setCountry("USA");
			
			ProspectCall pc = new ProspectCall(getUniqueProspect(), campaignId, p);
			/*DialerPreProcessEntity entity = new DialerPreProcessEntity();
			entity.setProspectCall(pc);
			entity = dialerPreProcessQueueRepository.save(entity);*/
			
			//create a log for this prospect in ProspectLog
			ProspectCallLog prospectLog = new ProspectCallLog();
			//prospectLog.setId(entity.getId()); //keep the Id in prospectlog and preprocess queue same
			prospectLog.setProspectCall(pc);
			prospectLog.setStatus(ProspectCallLog.ProspectCallStatus.QUEUED);
			prospectLogRepository.save(prospectLog);
		}
	}
}
