package com.xtaas.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.xtaas.datagenarator.specification.TeamSpecification;
import com.xtaas.db.entity.MasterData;
import com.xtaas.db.entity.Vertical;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.FakeAddressRepository;
import com.xtaas.db.repository.MasterDataRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.db.repository.VerticalRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.FakeAddress;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.CompletedCampaign;
import com.xtaas.domain.valueobject.DayOfWeek;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.WorkDays;
import com.xtaas.domain.valueobject.WorkHours;
import com.xtaas.domain.valueobject.WorkSchedule;

@Component
public class TeamGenerator {
	private static ClassPathXmlApplicationContext context;

	private Random random = new Random();

	@Autowired
	private FakeAddressRepository fakeAddressRepository;

	@Autowired
	private MasterDataRepository masterDataRepository;

	@Autowired
	private VerticalRepository verticalRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private TeamRepository teamRepository;
	
	private List<Vertical> verticals;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		TeamGenerator teamGenerator = context.getBean(TeamGenerator.class);
		List<TeamSpecification> teamSpecifications = new ArrayList<TeamSpecification>();

		teamSpecifications.add(new TeamSpecification(400, 16, 32, 5, 0, 0, 0, 5, 20, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(100, 8, 32, 5, 1, 0, 0, 5, 20, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(25, 8, 32, 5, 2, 1, 1, 5, 20, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(25, 8, 32, 5, 2, 1, 1, 8, 30, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(13, 4, 24, 5, 5, 2, 4, 8, 30, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(12, 4, 24, 5, 10, 5, 8, 8, 30, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(10, 4, 20, 5, 20, 14, 18, 10, 35, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(8, 2, 16, 5, 50, 20, 35, 10, 35, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(5, 0, 14, 5, 100, 30, 60, 15, 35, 20, 80, 20, 80, 100));
		teamSpecifications.add(new TeamSpecification(2, 0, 10, 5, 200, 40, 100, 15, 35, 20, 80, 20, 80, 100));

		teamGenerator.generateTeams(teamSpecifications);
	}

	private void generateTeams(List<TeamSpecification> teamSpecifications) {
		verticals = verticalRepository.findAll();
		List<FakeAddress> fakeAddresses = fakeAddressRepository.findAll(PageRequest.of(0, 70000)).getContent();
		fakeAddresses = generateColocatedGroups(fakeAddresses, teamSpecifications);
		fakeAddresses = generateCoStateGroups(fakeAddresses, teamSpecifications);
		fakeAddresses = generateNationalGroups(fakeAddresses, teamSpecifications);
		fakeAddresses = generateInternationalGroups(fakeAddresses, teamSpecifications);
	}

	private List<FakeAddress> generateColocatedGroups(List<FakeAddress> fakeAddresses,
			List<TeamSpecification> teamSpecifications) {
		List<List<FakeAddress>> fakeAddressGroups = groupData(fakeAddresses, new String[] { "countryCode", "stateCode",
				"city" });
		generateTeams("Colocated Team", fakeAddressGroups, teamSpecifications);
		return ungroupData(fakeAddressGroups);
	}

	private List<FakeAddress> generateCoStateGroups(List<FakeAddress> fakeAddresses,
			List<TeamSpecification> teamSpecifications) {
		List<List<FakeAddress>> fakeAddressGroups = groupData(fakeAddresses,
				new String[] { "countryCode", "stateCode" });
		generateTeams("State Team", fakeAddressGroups, teamSpecifications);
		return ungroupData(fakeAddressGroups);
	}

	private List<FakeAddress> generateNationalGroups(List<FakeAddress> fakeAddresses,
			List<TeamSpecification> teamSpecifications) {
		List<List<FakeAddress>> fakeAddressGroups = groupData(fakeAddresses, new String[] { "countryCode" });
		generateTeams("National Team", fakeAddressGroups, teamSpecifications);
		return ungroupData(fakeAddressGroups);
	}

	private List<FakeAddress> generateInternationalGroups(List<FakeAddress> fakeAddresses,
			List<TeamSpecification> teamSpecifications) {
		List<List<FakeAddress>> fakeAddressGroups = groupData(fakeAddresses, new String[] {});
		generateTeams("International Team", fakeAddressGroups, teamSpecifications);
		return ungroupData(fakeAddressGroups);
	}

	private void generateTeams(String teamName, List<List<FakeAddress>> fakeAddressGroups, List<TeamSpecification> teamSpecifications) {
		int counter = 0;
		int globalIndex = 0;
		for (TeamSpecification teamSpecification : teamSpecifications) {
			int localIndex = 0;

			while (localIndex < teamSpecification.getCount()) {
				int agentCount = generateRandom(2, 10);
				int startingIndex = globalIndex;
				boolean found = true;
				while (fakeAddressGroups.get(globalIndex).size() < agentCount) {
					globalIndex++;
					if (globalIndex == fakeAddressGroups.size()) {
						globalIndex = 0;
					}
					if (globalIndex == startingIndex) {
						found = false;
						break;
					}
				}
				if (found) {
					Team team = createTeam(String.format("%s %d", teamName, counter), fakeAddressGroups.get(globalIndex));
					counter++;
					Set<String> languageSet = new HashSet<String>();
					Set<String> domainSet = new HashSet<String>();
					double averageConversion = generateRandom(teamSpecification.getMinAverageConversion(),
							teamSpecification.getMaxAverageConversion()) + random.nextDouble();
					double averageQuality = generateRandom(teamSpecification.getMinAverageQuality(),
							teamSpecification.getMaxAverageQuality())
							+ random.nextDouble();
					int perLeadPrice = generateRandom(teamSpecification.getMinPerLeadPrice(),
							teamSpecification.getMaxPerLeadPrice());
					int totalReviews = generateRandom(teamSpecification.getMinTotalReviews(),
							teamSpecification.getMaxTotalReviews());
					team.setPerLeadPrice(perLeadPrice);
					int totalAvailableHours = 0;

					for (int agentIndex = 0; agentIndex < agentCount; agentIndex++) {
						int averageAvailableHours = generateRandom(teamSpecification.getMinAverageAvailableHours(),
								teamSpecification.getMaxAverageAvailableHours())
								* teamSpecification.getStepAverageAvailableHours();
						FakeAddress fakeAddress = popAddress(fakeAddressGroups.get(globalIndex));
						
						Agent agent = createAgent(team.getPartnerId(), fakeAddress);
						TeamResource agentResource = new TeamResource(agent.getId());
						List<WorkSchedule> workSchedules = generateSchedule();
						for (WorkSchedule workSchedule : workSchedules) {
							agent.addWorkSchedule(workSchedule);
							agentResource.addWorkSchedule(workSchedule);
						}
						agentResource.setAddress(agent.getAddress());
						agent.setAverageAvailableHours(averageAvailableHours);
						agentResource.setAverageAvailableHours(averageAvailableHours);
						
						List<String> languages = getLanguages(splitLanguages(fakeAddress.getLanguages()), generateRandom(1, 3));
						
						for (String language : languages) {
							agent.addLanguage(language);
							agentResource.addLanguage(language);
							languageSet.add(language);
						}

						List<String> domains = getDomains(verticals, generateRandom(1, 3));

						for (String domain : domains) {
							agent.addDomain(domain);
							agentResource.addDomain(domain);
							domainSet.add(domain);
						}
						team.addAgent(agentResource);
						totalAvailableHours += averageAvailableHours;
						agentRepository.save(agent);
					}
					
					int totalReview = 0;
					for (int index = 0; index < teamSpecification.getTotalCampaigns(); index++) {
						if (index < totalReviews) {
							int currentReview = generateRandom(1, 5);
							totalReview += currentReview;
							team.addCompletedCampaign(new CompletedCampaign((new ObjectId()).toString(), currentReview));
						} else {
							team.addCompletedCampaign(new CompletedCampaign((new ObjectId()).toString(), 0));
						}
					}
					if (totalReviews != 0) {
						team.setAverageRating(((totalReview * 2) / totalReviews) / 2.0);
						team.setAverageConversion(averageConversion);
						team.setAverageQuality(averageQuality);
						team.setTotalVolume(teamSpecification.getTotalCampaigns() * teamSpecification.getVolumePerCampaignPerResource() * agentCount);
						team.setOverview(String
								.format("A %d member team with per lead charge $%d. Our average rating is %f, conversion is %f and average quality is %f. We are avialble %d hours per month. We know the languages: %s and have skills in domains: %s",
										agentCount, perLeadPrice, ((totalReview * 2) / totalReviews) / 2.0, averageConversion, averageQuality, totalAvailableHours, join(new ArrayList<String>(languageSet)), join(new ArrayList<String>(domainSet))));
					} else {
						team.setOverview(String
								.format("A %d member team with per lead charge $%d. We are avialble %d hours per month. We know the languages: %s and have skills in domains: %s",
										agentCount, perLeadPrice, totalAvailableHours, join(new ArrayList<String>(languageSet)), join(new ArrayList<String>(domainSet))));
					
					}

					globalIndex++;
					if (globalIndex == fakeAddressGroups.size()) {
						globalIndex = 0;
					}
					team.setAverageAvailableHours(totalAvailableHours);
					teamRepository.save(team);
				}
				localIndex++;
			}
		}
	}
	
	private List<WorkSchedule> generateSchedule() {
		List<WorkSchedule> workSchedules = new ArrayList<WorkSchedule>();
		Date startDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.YEAR, 1);
		Date endDate = calendar.getTime();
		int morningStartHour = generateRandom(8, 11);
		int morningEndHour = generateRandom(morningStartHour + 1, 12);
		int afternoonStartHour = generateRandom(13, 17);
		int afternoonEndHour = generateRandom(afternoonStartHour + 1, 20);
		int exceptionDay = generateRandom(0, 4);
		int exceptionAfternoonStartHour = generateRandom(13, 17);
		int exceptionAfternoonEndHour = generateRandom(exceptionAfternoonStartHour + 1, 20);

		List<DayOfWeek> mainRecurrence = new ArrayList<DayOfWeek>();
		List<DayOfWeek> exceptionRecurrence = new ArrayList<DayOfWeek>();
		DayOfWeek[] dayOfWeeks = DayOfWeek.values();
		for (int index = 0; index < 5; index++) {
			if (index == exceptionDay) {
				exceptionRecurrence.add(dayOfWeeks[index]);
			} else {
				mainRecurrence.add(dayOfWeeks[index]);
			}
		}
		workSchedules.add(new WorkSchedule(startDate, morningStartHour, morningEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Daily, null)));
		workSchedules
				.add(new WorkSchedule(startDate, afternoonStartHour, afternoonEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Weekly, mainRecurrence)));
		workSchedules.add(new WorkSchedule(startDate, exceptionAfternoonStartHour, exceptionAfternoonEndHour, new ScheduleRecurrence(startDate, endDate, RecurrenceTypes.Weekly,
				exceptionRecurrence)));
		return workSchedules;
	}


	private Team createTeam(String teamName, List<FakeAddress> fakeAddresses) {
		String partnerId = (new ObjectId()).toString();
		FakeAddress firstAddress = fakeAddresses.get(fakeAddresses.size() - 1);
		Agent supervisor = createAgent(partnerId, firstAddress);
		TeamResource supervisorResource = new TeamResource(supervisor.getId());
		return new Team(partnerId, supervisorResource, teamName, supervisor.getAddress());
	}

	private Agent createAgent(String partnerId, FakeAddress fakeAddress) {
		Address address = new Address(fakeAddress.getLine1(), "", fakeAddress.getCity(),
								fakeAddress.getStateCode(), fakeAddress.getCountryCode(), fakeAddress.getPostalCode(), fakeAddress.getTimeZone());
		Agent agent = new Agent(partnerId, fakeAddress.getGivenName(), fakeAddress.getSurName(), address);
		agent = agentRepository.save(agent);
		return agent;
	}

	private List<List<FakeAddress>> groupData(List<FakeAddress> fakeAddresses, String[] attributes) {
		Map<String, List<FakeAddress>> fakeAddressGroups = new HashMap<String, List<FakeAddress>>();
		List<List<FakeAddress>> fakeAddressGroupList = new ArrayList<List<FakeAddress>>();
		for (FakeAddress fakeAddress : fakeAddresses) {
			String key = generateKey(fakeAddress, attributes);
			if (!fakeAddressGroups.containsKey(key)) {
				fakeAddressGroups.put(key, new ArrayList<FakeAddress>());
			}
			fakeAddressGroups.get(key).add(fakeAddress);
		}
		fakeAddressGroupList.addAll(fakeAddressGroups.values());
		return fakeAddressGroupList;
	}

	private List<FakeAddress> ungroupData(List<List<FakeAddress>> fakeAddressGroups) {
		List<FakeAddress> allFakeAddresses = new ArrayList<FakeAddress>();
		for (List<FakeAddress> fakeAddresses : fakeAddressGroups) {
			allFakeAddresses.addAll(fakeAddresses);
		}
		return allFakeAddresses;
	}

	private String generateKey(FakeAddress fakeAddress, String[] attributes) {
		StringBuilder key = new StringBuilder();
		boolean first = true;
		for (String attribute : attributes) {
			if (!first) {
				key.append("|");
			} else {
				first = false;
			}
			key.append(fakeAddress.getByKey(attribute));
		}
		return key.toString();
	}

	private int generateRandom(int minimumValue, int maximumValue) {
		return random.nextInt(maximumValue - minimumValue + 1) + minimumValue;
	}

	private FakeAddress popAddress(List<FakeAddress> fakeAddresses) {
		FakeAddress fakeAddress = fakeAddresses.get(fakeAddresses.size() - 1);
		fakeAddresses.remove(fakeAddresses.size() - 1);
		return fakeAddress;
	}

	private List<String> splitLanguages(String languages) {
		List<String> languageList = new ArrayList<String>();
		for (String language : languages.split("\\|")) {
			languageList.add(language.trim());
		}
		return languageList;
	}

	private List<String> getLanguages(List<String> inputList, int count) {
		Set<String> languages = new HashSet<String>();
		for (int index = 0; index < count; index++) {
			int randomIndex = generateRandom(0, inputList.size() - 1);
			languages.add(inputList.get(randomIndex));
		}
		List<String> languageList = new ArrayList<String>();
		languageList.addAll(languages);
		return languageList;
	}
	
	private String join(List<String> items) {
		boolean first = true;
		StringBuilder result = new StringBuilder();
		
		for (String item : items) {
			if (!first) {
				result.append(", ");
			} else {
				first = false;
			}
			result.append(item);
		}
		return result.toString();
	}

	private List<String> getDomains(List<Vertical> verticals, int count) {
		Set<String> domains = new HashSet<String>();
		for (int index = 0; index < count; index++) {
			int verticalIndex = generateRandom(0, verticals.size() - 1);
			Vertical vertical = verticals.get(verticalIndex);
			int domainIndex = generateRandom(0, vertical.getDomains().size() - 1);
			domains.add(String.format("%s|%s", vertical.getName(), vertical.getDomains().get(domainIndex).getValue()));
		}
		List<String> domainList = new ArrayList<String>();
		domainList.addAll(domains);
		return domainList;
	}
}
