package com.xtaas.process;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xtaas.batch.processor.TeamAvailableHrsProcessor;
import com.xtaas.batch.reader.CustomBillingQueryParams;

public class BatchLauncher {
	
	private static final Logger logger = LoggerFactory.getLogger(TeamAvailableHrsProcessor.class);
	
	public enum JOBS {
		IndependentAgentAvailableHrsCalculatorJob, 
		TeamAvailableHrsCalculatorJob,
		CallBillingJob
	};

	public static void main(String[] args) {

		String[] springConfig = { "batchjob.xml" };

		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		
		String inputtedJobName = args[0] ;
		String jobBeanName = null;
		JobParameters param = new JobParameters();
		switch(Enum.valueOf(JOBS.class, inputtedJobName)) {
			case IndependentAgentAvailableHrsCalculatorJob : 
				jobBeanName = "independentAgentAvailableHrsCalculatorJob";
				break;
			case TeamAvailableHrsCalculatorJob :
				jobBeanName = "teamAvailableHrsCalculatorJob";
				param = new JobParametersBuilder()
					.addString("collection", "team")
					.addString("rundate", new Date().toString()).toJobParameters();
				break;
			case CallBillingJob :
				jobBeanName = "callBillingJob";
				param = new JobParametersBuilder()
					.addString("collection", "callbillinglog")
					.addString("rundate", new Date().toString()).toJobParameters();
				CustomBillingQueryParams customBillingQueryParams = (CustomBillingQueryParams) context.getBean("customBillingQueryParams");
				customBillingQueryParams.setQueryParams("callBillingReader");//set query parameter for starting batch job from unprocessed Items
				break;
			default :
				usage();
				break;
		}

		Job job = (Job) context.getBean(jobBeanName);
		try {
			JobExecution execution = jobLauncher.run(job, param);
			logger.debug("Job [{}] {}", inputtedJobName, execution.getStatus());
		} catch (Exception e) {
			logger.error("Job [{}] Failed. Error: ",inputtedJobName , e);
		}
	}
	
	private static void usage() {
        System.out.println("Usage: BatchLauncher [jobname]");
        System.exit(1);
	}
}