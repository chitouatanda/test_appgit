/**
 * 
 */
package com.xtaas.process;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.dialer.DialerPreProcessThread;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.job.MasterJob;
import com.xtaas.dialer.job.PreviewCallMasterJob;

/**
 * @author djain
 *
 */
@Component
public class DialerLauncher {

	private final Logger logger = LoggerFactory.getLogger(DialerLauncher.class);

	@Autowired
	private DialerThreadPool dialerThreadPool;

	@Autowired
	private MasterJob masterJob;

	@Autowired
	private PreviewCallMasterJob previewCallMasterJob;

	@Autowired
	private DialerPreProcessThread dialerPreProcessThread;

	// @Autowired
	// private CampaignSpeedJob campaignSpeedJob;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		DialerLauncher dialerLauncher = context.getBean(DialerLauncher.class);
		dialerLauncher.launch();
	}

	private void launch() {
		// Set up the GLOBAL security context
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL);
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("dialer", "secret", authorities));

		dialerThreadPool.scheduleJob(masterJob, 10);

		// start PreviewCallMasterJob Thread
		startPreviewCallMasterJob();

		// start dialer preprocess thread - Responsible for setting CallTimeRestriction
		// and MaxRetryLimitReached
		startDialerPreProcessThread();

		logger.debug("launch(): DIALER STARTED !!! ");
	}

	// Creates a ThreadPool for PreviewCallMonitorJob
	private void startPreviewCallMasterJob() {
		logger.debug("startPreviewCallMasterJob(): Starting PREVIEW CALL MASTER Thread.");
		// PreviewCallMasterJob previewCallMasterJob =
		// BeanLocator.getBean("PreviewCallMasterJobThread",
		// PreviewCallMasterJob.class);
		Thread previewCallMasterJobThread = new Thread(previewCallMasterJob, "PREVIEW-CALL-MASTER-JOB-THREAD");
		previewCallMasterJobThread.start();
		logger.debug("startPreviewCallMasterJob(): Started PREVIEW-CALL-MASTER-JOB Thread.");
	}

	// Creates a single thread for Dialer Preprocessing. Runs every hour
	private void startDialerPreProcessThread() {
		logger.debug("startDialerPreProcessThread(): Starting DIALER PREPROCESS Thread.");
		Thread dialerPreProcess = new Thread(dialerPreProcessThread, "DIALER-PREPROCESSOR-THREAD");
		dialerPreProcess.start();
		logger.debug("startDialerPreProcessThread(): Started DIALER PREPROCESS Thread.");
	}
}
