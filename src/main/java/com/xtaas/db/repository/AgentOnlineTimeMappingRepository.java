package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AgentOnlineTimeMapping;;

public interface AgentOnlineTimeMappingRepository extends MongoRepository<AgentOnlineTimeMapping, String> {

	@Query("{ 'status' : 'ACTIVE' }")
	public List<AgentOnlineTimeMapping> findMapping();

	@Query("{'status' : 'ACTIVE', 'supervisorId' : ?0}")
	public List<AgentOnlineTimeMapping> findBySupervisorId(String supervisorId);

}
