package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.domain.valueobject.CampaignCriteria;
import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.Expression;


@Document(collection="databuyqueue")
public class DataBuyQueue {
	
	public static enum DataBuyQueueStatus {
		QUEUED,
		INPROCESS,
		COMPLETED,
		ERROR,
		KILL,
		PAUSED,
		PAUSEDZOOM,
		READY_FOR_CLEANUP,
		CLEANUP_INPROCESS
	}
	
	private String id;
	private Date jobRequestTime;
	private Date jobStartTime;
	private Date jobEndTime;
	private String campaignId;
	private String campaignName;
	private Long requestCount;
	private Long purchasedCount;
	private String status;
	private String searchResponseSample;
	private String errorMsg;
	private int jobretry;
	private boolean isBuyProspect;
	private boolean isSearchCompany;
	private int searchPage;
	private String searchCompany;
	private String searchState;
	private String searchMinEmp;
	private String searchMaxEmp;
	private String abmId;
	private String sicCode;
	private boolean interupted;
	private Long totalBought;
	
	private String searchManagementlevel;
	private String searchEmployeeMin;
	private String searchEmployeeMax;
	private String searchRevenueMin;
	private String searchRevenueMax;
	private String organizationId;
	private List<Expression> campaignCriteriaDTO;
	private String requestorId;
	private Long salutaryRequestCount;
	private Long salutaryPurchasedCount;
	private Long zoomInfoRequestCount;
	private Long zoomInfoPurchasedCount;
	private Long insideViewRequestCount;
	private Long insideViewPurchasedCount;
	private String insideViewJobId;
	private String insideViewJobStatus;
	private boolean isDailyBuyProspect;
	private Long leadIqRequestCount;
	private Long leadIqPurchasedCount;
	private String searchDepartment;
	private String searchIndustry;

	private boolean isDataActivate;
	private CampaignCriteria campaignCriteria;
	private boolean restFullAPI;

	public boolean isDataActivate() {
		return isDataActivate;
	}

	public void setDataActivate(boolean dataActivate) {
		isDataActivate = dataActivate;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getSearchEmployeeMin() {
		return searchEmployeeMin;
	}

	public void setSearchEmployeeMin(String searchEmployeeMin) {
		this.searchEmployeeMin = searchEmployeeMin;
	}

	public String getSearchEmployeeMax() {
		return searchEmployeeMax;
	}

	public void setSearchEmployeeMax(String searchEmployeeMax) {
		this.searchEmployeeMax = searchEmployeeMax;
	}

	public String getSearchRevenueMin() {
		return searchRevenueMin;
	}

	public void setSearchRevenueMin(String searchRevenueMin) {
		this.searchRevenueMin = searchRevenueMin;
	}

	public String getSearchRevenueMax() {
		return searchRevenueMax;
	}

	public void setSearchRevenueMax(String searchRevenueMax) {
		this.searchRevenueMax = searchRevenueMax;
	}

	public String getSearchManagementlevel() {
		return searchManagementlevel;
	}

	public void setSearchManagementlevel(String searchManagementlevel) {
		this.searchManagementlevel = searchManagementlevel;
	}

	public Long getTotalBought() {
		return totalBought;
	}

	public void setTotalBought(Long totalBought) {
		this.totalBought = totalBought;
	}

	public boolean isInterupted() {
		return interupted;
	}

	public void setInterupted(boolean interupted) {
		this.interupted = interupted;
	}

	public String getSicCode() {
		return sicCode;
	}

	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	public String getAbmId() {
		return abmId;
	}

	public void setAbmId(String abmId) {
		this.abmId = abmId;
	}

	public int getSearchPage() {
		return searchPage;
	}

	public void setSearchPage(int searchPage) {
		this.searchPage = searchPage;
	}

	public String getSearchCompany() {
		return searchCompany;
	}

	public void setSearchCompany(String searchCompany) {
		this.searchCompany = searchCompany;
	}

	public String getSearchState() {
		return searchState;
	}

	public void setSearchState(String searchState) {
		this.searchState = searchState;
	}

	public String getSearchMinEmp() {
		return searchMinEmp;
	}

	public void setSearchMinEmp(String searchMinEmp) {
		this.searchMinEmp = searchMinEmp;
	}

	public String getSearchMaxEmp() {
		return searchMaxEmp;
	}

	public void setSearchMaxEmp(String searchMaxEmp) {
		this.searchMaxEmp = searchMaxEmp;
	}

	public boolean isBuyProspect() {
		return isBuyProspect;
	}

	public void setBuyProspect(boolean isBuyProspect) {
		this.isBuyProspect = isBuyProspect;
	}

	public boolean isSearchCompany() {
		return isSearchCompany;
	}

	public void setSearchCompany(boolean isSearchCompany) {
		this.isSearchCompany = isSearchCompany;
	}

	public int getJobretry() {
		return jobretry;
	}

	public void setJobretry(int jobretry) {
		this.jobretry = jobretry;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getSearchResponseSample() {
		return searchResponseSample;
	}
	public void setSearchResponseSample(String searchResponseSample) {
		this.searchResponseSample = searchResponseSample;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public Date getJobRequestTime() {
		return jobRequestTime;
	}
	public void setJobRequestTime(Date jobRequestTime) {
		this.jobRequestTime = jobRequestTime;
	}
	public Date getJobStartTime() {
		return jobStartTime;
	}
	public void setJobStartTime(Date jobStartTime) {
		this.jobStartTime = jobStartTime;
	}
	public Date getJobEndTime() {
		return jobEndTime;
	}
	public void setJobEndTime(Date jobEndTime) {
		this.jobEndTime = jobEndTime;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public Long getRequestCount() {
		return requestCount;
	}
	public void setRequestCount(Long requestCount) {
		this.requestCount = requestCount;
	}
	public Long getPurchasedCount() {
		return purchasedCount;
	}
	public void setPurchasedCount(Long purchasedCount) {
		this.purchasedCount = purchasedCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public List<Expression> getCampaignCriteriaDTO() {
		return campaignCriteriaDTO;
	}

	public void setCampaignCriteriaDTO(List<Expression> campaignCriteriaDTO) {
		this.campaignCriteriaDTO = campaignCriteriaDTO;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public Long getSalutaryRequestCount() {
		return salutaryRequestCount;
	}

	public void setSalutaryRequestCount(Long salutaryRequestCount) {
		this.salutaryRequestCount = salutaryRequestCount;
	}

	public Long getSalutaryPurchasedCount() {
		return salutaryPurchasedCount;
	}

	public void setSalutaryPurchasedCount(Long salutaryPurchasedCount) {
		this.salutaryPurchasedCount = salutaryPurchasedCount;
	}

	public Long getZoomInfoRequestCount() {
		return zoomInfoRequestCount;
	}

	public void setZoomInfoRequestCount(Long zoomInfoRequestCount) {
		this.zoomInfoRequestCount = zoomInfoRequestCount;
	}

	public Long getZoomInfoPurchasedCount() {
		return zoomInfoPurchasedCount;
	}

	public void setZoomInfoPurchasedCount(Long zoomInfoPurchasedCount) {
		this.zoomInfoPurchasedCount = zoomInfoPurchasedCount;
	}

	public Long getInsideViewRequestCount() {
		return insideViewRequestCount;
	}

	public void setInsideViewRequestCount(Long insideViewRequestCount) {
		this.insideViewRequestCount = insideViewRequestCount;
	}

	public Long getInsideViewPurchasedCount() {
		return insideViewPurchasedCount;
	}

	public void setInsideViewPurchasedCount(Long insideViewPurchasedCount) {
		this.insideViewPurchasedCount = insideViewPurchasedCount;
	}

	public String getInsideViewJobId() {
		return insideViewJobId;
	}

	public void setInsideViewJobId(String insideViewJobId) {
		this.insideViewJobId = insideViewJobId;
	}

	public boolean isDailyBuyProspect() {
		return isDailyBuyProspect;
	}

	public void setDailyBuyProspect(boolean dailyBuyProspect) {
		isDailyBuyProspect = dailyBuyProspect;
	}

	public String getInsideViewJobStatus() {
		return insideViewJobStatus;
	}

	public void setInsideViewJobStatus(String insideViewJobStatus) {
		this.insideViewJobStatus = insideViewJobStatus;
	}

	public Long getLeadIqRequestCount() {
		return leadIqRequestCount;
	}

	public void setLeadIqRequestCount(Long leadIqRequestCount) {
		this.leadIqRequestCount = leadIqRequestCount;
	}

	public Long getLeadIqPurchasedCount() {
		return leadIqPurchasedCount;
	}

	public void setLeadIqPurchasedCount(Long leadIqPurchasedCount) {
		this.leadIqPurchasedCount = leadIqPurchasedCount;
	}

	public String getSearchDepartment() {
		return searchDepartment;
	}

	public CampaignCriteria getCampaignCriteria() {
		return campaignCriteria;
	}

	public void setCampaignCriteria(CampaignCriteria campaignCriteria) {
		this.campaignCriteria = campaignCriteria;
	}

	public boolean isRestFullAPI() {
		return restFullAPI;
	}

	public void setRestFullAPI(boolean restFullAPI) {
		this.restFullAPI = restFullAPI;
	}

	public void setSearchDepartment(String searchDepartment) {
		this.searchDepartment = searchDepartment;
	}

	public String getSearchIndustry() {
		return searchIndustry;
	}

	public void setSearchIndustry(String searchIndustry) {
		this.searchIndustry = searchIndustry;
	}
}
