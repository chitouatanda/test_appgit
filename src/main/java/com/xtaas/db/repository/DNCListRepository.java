/**
 * 
 */
package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.DNCList;

/**
 * @author djain
 *
 */
public interface DNCListRepository extends MongoRepository<DNCList, String> {

	@Query("{ 'phoneNumber' : ?0, 'partnerId' : ?1 }")
	public DNCList findPhoneNumber(String phoneNumber, String partnerId);

//	@Query("{ 'phoneNumber' : ?0, 'firstName' : {'$regex' : ?1, $options: 'i'}, 'lastName' : {'$regex' : ?2, $options: 'i'}}")	
	@Query("{ 'phoneNumber' : ?0, 'firstName' : ?1, 'lastName' : ?2 }")
	public List<DNCList> findPhoneNumber(String phoneNumber, String firstName, String lastName);

	@Query(value = "{ 'phoneNumber' : ?0, 'firstName' : ?1, 'lastName' : ?2 }", count = true)
	public long countByPhoneNumberFirstNameLastName(String phoneNumber, String firstName, String lastName);

	@Query("{ 'domain' : ?0, 'firstName' : ?1, 'lastName' : ?2 }")
	public List<DNCList> findDNCByProspectDomain(String domain, String firstName, String lastName);

	@Query("{ 'phoneNumber' : {'$regex' : ?0 }, 'partnerId' : ?1 }")
	public List<DNCList> findMatchingDNCList(String phoneNumber, String partnerId, Pageable pageRequest);

	@Query(value = "{ 'phoneNumber' : {'$regex' : ?0 },  'partnerId' : ?1 }", count = true)
	public long countMatchingDNCList(String phoneNumber, String partnerId);

	@Query("{ 'phoneNumber' : ?0 }")
	public List<DNCList> findPhoneNumber(String phoneNumber);

	@Query("{ 'domain' : ?0 }")
	public List<DNCList> findDomain(String phoneNumber);

	@Query("{ 'partnerId' : 'DEMANDSHORE','dncLevel' : 'COMPANY' }")
	public List<DNCList> findByPidAndLevel();

	@Query("{ 'partnerId' : ?0, 'phoneNumber' : ?1 }")
	public List<DNCList> findByPartnerIdAndPhoneNumber(String partnerId, String phoneNumber);

	@Query("{ 'partnerId' : ?0, 'domain' : ?1 }")
	public List<DNCList> findByPartnerIdAndDomain(String partnerId, String domain);
	
	@Query("{ 'partnerId' : ?0, 'phoneNumber' : ?1, 'firstName' : ?2, 'lastName' : ?3 }")
	public List<DNCList> findPhoneNumberByPartner(String partnerId, String phoneNumber, String firstName, String lastName);
	
	@Query("{ 'partnerId' : ?0, 'domain' : ?1, 'firstName' : ?2, 'lastName' : ?3 }")
	public List<DNCList> findPartnerDNCByProspectDomain(String partnerId, String domain, String firstName, String lastName);
}
