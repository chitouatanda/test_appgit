package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.BasicDBObject;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.TeamSortClauses;
import com.xtaas.domain.valueobject.TeamStatus;
import com.xtaas.web.dto.TeamSearchResultDTO;

public class TeamRepositoryImpl implements TeamRepositoryCustom {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	TeamRepository teamRepository;

	@Override
	public List<TeamSearchResultDTO> searchTeams(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerLeadPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimumAverageConversion, double minimumAverageQuality, int minimumAvailableHours,
			Pageable pageRequest) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatch(countries, states, cities, timeZones, languages, ratings, domain,
				maximumPerLeadPrice, minimumCompletedCampaigns, minimumTotalVolume, minimumAverageConversion,
				minimumAverageQuality));

		if (minimumAvailableHours > 0
				|| pageRequest.getSort().getOrderFor(TeamSortClauses.AverageAvailableHours.getFieldName()) != null) {

			aggregationOperations.add(Aggregation.project("_id", "name", "overview", "teamSize", "perLeadPrice",
					"totalCompletedCampaigns", "totalRatings", "averageRating", "totalVolume", "averageConversion",
					"averageQuality", "agents"));

			aggregationOperations.add(Aggregation.unwind("agents"));
			aggregationOperations.add(buildUnwindMatch(countries, states, cities, timeZones, languages, domain));

			aggregationOperations.add(Aggregation
					.group("_id", "name", "overview", "teamSize", "perLeadPrice", "totalCompletedCampaigns",
							"totalRatings", "averageRating", "totalVolume", "averageConversion", "averageQuality")
					.sum("agents.averageAvailableHours").as("averageAvailableHours"));

			if (minimumAvailableHours > 0) {
				aggregationOperations.add(Aggregation.match(Criteria.where("averageAvailableHours").gte(
						minimumAvailableHours)));
			}

			aggregationOperations.add(Aggregation.project("averageAvailableHours").and("_id.id").as("teamId")
					.and("_id.name").as("name").and("_id.overview").as("overview").and("_id.teamSize").as("teamSize")
					.and("_id.perLeadPrice").as("perLeadPrice").and("_id.totalCompletedCampaigns")
					.as("totalCompletedCampaigns").and("_id.averageRating").as("averageRating").and("_id.totalVolume")
					.as("totalVolume").and("_id.averageConversion").as("averageConversion").and("_id.averageQuality")
					.as("averageQuality"));
			aggregationOperations.add(buildSortClause(pageRequest));
			
		} else {
			aggregationOperations.add(Aggregation.project("_id", "name", "overview", "teamSize", "perLeadPrice",
					"totalCompletedCampaigns", "totalRatings", "averageRating", "totalVolume", "averageConversion",
					"averageQuality", "averageAvailableHours"));
			aggregationOperations.add(Aggregation.sort(pageRequest.getSort()));
		}

		aggregationOperations.add(Aggregation.skip(pageRequest.getOffset()));
		aggregationOperations.add(Aggregation.limit(pageRequest.getPageSize()));

		AggregationResults<TeamSearchResultDTO> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "team", TeamSearchResultDTO.class);
		return results.getMappedResults();
	}

	@Override
	public List<Object> countTeams(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerLeadPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimumAverageConversion, double minimumAverageQuality, int minimumAvailableHours) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatch(countries, states, cities, timeZones, languages, ratings, domain,
				maximumPerLeadPrice, minimumCompletedCampaigns, minimumTotalVolume, minimumAverageConversion,
				minimumAverageQuality));
		aggregationOperations.add(Aggregation.project("_id", "agents"));
		aggregationOperations.add(Aggregation.unwind("agents"));
		aggregationOperations.add(buildUnwindMatch(countries, states, cities, timeZones, languages, domain));
		if (minimumAvailableHours > 0) {
			aggregationOperations.add(Aggregation.group("_id").push("agents").as("agents")
					.sum("agents.averageAvailableHours").as("averageAvailableHours"));
			aggregationOperations.add(Aggregation.match(Criteria.where("averageAvailableHours").gte(
					minimumAvailableHours)));
			aggregationOperations.add(Aggregation.unwind("agents"));
			aggregationOperations.add(Aggregation.project("_id", "agents"));
		}
		if (languages != null && languages.size() != 0) {
			aggregationOperations.add(Aggregation.unwind("agents.languages"));
		}
		aggregationOperations.add(buildExistsGroup(countries, states, cities, timeZones, languages));
		aggregationOperations.add(buildCounterGroup(countries, states, cities, timeZones, languages));
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"team", Object.class);
		return results.getMappedResults();
	}

	@Override
	public List<Object> countTeams(List<Double> ratings, double maximumPerLeadPrice,
			int minimumCompletedCampaigns, int minimumTotalVolume, double minimumAverageConversion,
			double minimumAverageQuality, int minimumAvailableHours) {

		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("status").is(TeamStatus.ACTIVE.name());

		if (ratings != null && ratings.size() != 0) {
			criterias.add(Criteria.where("averageRating").in(ratings));
		}
		if (maximumPerLeadPrice > 0) {
			criterias.add(Criteria.where("perLeadPrice").lte(maximumPerLeadPrice));
		}
		if (minimumCompletedCampaigns > 0) {
			criterias.add(Criteria.where("totalCampaignsCompleted").gte(minimumCompletedCampaigns));
		}
		if (minimumTotalVolume > 0) {
			criterias.add(Criteria.where("totalVolume").gte(minimumTotalVolume));
		}
		if (minimumAverageConversion > 0) {
			criterias.add(Criteria.where("averageConversion").gte(minimumAverageConversion));
		}
		if (minimumAverageQuality > 0) {
			criterias.add(Criteria.where("averageQuality").gte(minimumAverageQuality));
		}
		if (minimumAvailableHours > 0) {
			criterias.add(Criteria.where("averageAvailableHours").gte(minimumAvailableHours));
		}
		List<Object> teamCountList = new ArrayList<Object>();
		if (criterias.size() == 0) {
			int teamCount = mongoTemplate.find(Query.query(criteria), Team.class, "team").size();
			teamCountList.add(new BasicDBObject("_id",teamCount));
		} else {
			int teamCount = mongoTemplate.find(
					Query.query(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]))), Team.class, "team").size();
			teamCountList.add(new BasicDBObject("_id",teamCount));
		}
		return teamCountList;
	}
	
	@Override
	public List<TeamSearchResultDTO> searchTeams(List<Double> ratings, double maximumPerLeadPrice,
			int minimumCompletedCampaigns, int minimumTotalVolume, double minimumAverageConversion,
			double minimumAverageQuality, int minimumAvailableHours, Pageable pageRequest) {

		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("status").is(AgentStatus.ACTIVE.name());

		if (ratings != null && ratings.size() != 0) {
			criterias.add(Criteria.where("averageRating").in(ratings));
		}
		if (maximumPerLeadPrice > 0) {
			criterias.add(Criteria.where("perLeadPrice").lte(maximumPerLeadPrice));
		}
		if (minimumCompletedCampaigns > 0) {
			criterias.add(Criteria.where("totalCampaignsCompleted").gte(minimumCompletedCampaigns));
		}
		if (minimumTotalVolume > 0) {
			criterias.add(Criteria.where("totalVolume").gte(minimumTotalVolume));
		}
		if (minimumAverageConversion > 0) {
			criterias.add(Criteria.where("averageConversion").gte(minimumAverageConversion));
		}
		if (minimumAverageQuality > 0) {
			criterias.add(Criteria.where("averageQuality").gte(minimumAverageQuality));
		}
		if (minimumAvailableHours > 0) {
			criterias.add(Criteria.where("averageAvailableHours").gte(minimumAvailableHours));
		}
		if (criterias.size() == 0) {
			return mongoTemplate.find(Query.query(criteria).with(pageRequest), TeamSearchResultDTO.class, "team");
		} else {
			return mongoTemplate.find(
					Query.query(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]))).with(
							pageRequest), TeamSearchResultDTO.class, "team");
		}
	}

	private AggregationOperation buildBasicMatch(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerLeadPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimumAverageConversion, double minimumAverageQuality) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("status").is(AgentStatus.ACTIVE.name());
		if (countries != null && countries.size() != 0) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("address.country").in(countries)));
		}
		if (states != null && states.size() != 0) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("address.state").in(states)));
		}
		if (cities != null && cities.size() != 0) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("address.city").in(cities)));
		}
		if (timeZones != null && timeZones.size() != 0) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("address.timeZone").in(timeZones)));
		}
		if (languages != null && languages.size() != 0) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("languages").in(languages)));
		}
		/*if (domain != null && !domain.isEmpty()) {
			criterias.add(Criteria.where("agents").elemMatch(Criteria.where("domains").in(domain)));
		}*/
		if (ratings != null && ratings.size() != 0) {
			criterias.add(Criteria.where("averageRating").in(ratings));
		}
		if (maximumPerLeadPrice > 0) {
			criterias.add(Criteria.where("perLeadPrice").lte(maximumPerLeadPrice));
		}
		if (minimumCompletedCampaigns > 0) {
			criterias.add(Criteria.where("totalCampaignsCompleted").gte(minimumCompletedCampaigns));
		}
		if (minimumTotalVolume > 0) {
			criterias.add(Criteria.where("totalVolume").gte(minimumTotalVolume));
		}
		if (minimumAverageConversion > 0) {
			criterias.add(Criteria.where("averageConversion").gte(minimumAverageConversion));
		}
		if (minimumAverageQuality > 0) {
			criterias.add(Criteria.where("averageQuality").gte(minimumAverageQuality));
		}
		if (criterias.size() == 0) {
			return Aggregation.match(criteria);
		} else {
			return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
		}
	}
	
	private AggregationOperation buildSortClause(Pageable pageable) {
		BasicDBObject sortGroup = new BasicDBObject();
		Iterator<Order> orders = pageable.getSort().iterator();
		while (orders.hasNext()) {
			Order order = orders.next();
			sortGroup = sortGroup.append(order.getProperty(), order.getDirection() == Direction.ASC ? 1 : -1);
		}
		return new CustomAggregationOperation(new Document("$sort", sortGroup));
	}

	private AggregationOperation buildExistsGroup(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages) {
		BasicDBObject counterGroup = new BasicDBObject("_id", "$_id");

		if (countries != null) {
			for (String country : countries) {
				counterGroup = counterGroup
						.append(String.format("countries_%s", country), new BasicDBObject("$max", new BasicDBObject(
								"$cond", new Object[] {
										new BasicDBObject("$eq", new Object[] { "$agents.address.country", country }),
										1, 0 })));
			}
		}
		if (states != null) {
			for (String state : states) {
				counterGroup = counterGroup.append(String.format("states_%s", state), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$agents.address.state", state }), 1, 0 })));
			}
		}
		if (cities != null) {
			for (String city : cities) {
				counterGroup = counterGroup.append(String.format("cities_%s", city), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$agents.address.city", city }), 1, 0 })));
			}
		}
		if (timeZones != null) {
			for (String timeZone : timeZones) {
				counterGroup = counterGroup.append(String.format("timeZones_%s", timeZone),
						new BasicDBObject("$max",
								new BasicDBObject("$cond",
										new Object[] {
												new BasicDBObject("$eq", new Object[] { "$agents.address.timeZone",
														timeZone }), 1, 0 })));
			}
		}
		if (languages != null) {
			for (String language : languages) {
				counterGroup = counterGroup.append(String.format("languages_%s", language), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$agents.languages", language }), 1, 0 })));
			}
		}
		return new CustomAggregationOperation(new Document("$group", counterGroup));
	}

	private AggregationOperation buildCounterGroup(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages) {
		BasicDBObject counterGroup = new BasicDBObject("_id", null);

		if (countries != null) {
			for (String country : countries) {
				counterGroup = counterGroup.append(String.format("countries_%s", country), new BasicDBObject("$sum",
						String.format("$countries_%s", country)));
			}
		}
		if (states != null) {
			for (String state : states) {
				counterGroup = counterGroup.append(String.format("states_%s", state),
						new BasicDBObject("$sum", String.format("$states_%s", state)));
			}
		}
		if (cities != null) {
			for (String city : cities) {
				counterGroup = counterGroup.append(String.format("cities_%s", city),
						new BasicDBObject("$sum", String.format("$cities_%s", city)));
			}
		}
		if (timeZones != null) {
			for (String timeZone : timeZones) {
				counterGroup = counterGroup.append(String.format("timeZones_%s", timeZone), new BasicDBObject("$sum",
						String.format("$timeZones_%s", timeZone)));
			}
		}
		if (languages != null) {
			for (String language : languages) {
				counterGroup = counterGroup.append(String.format("languages_%s", language), new BasicDBObject("$sum",
						String.format("$languages_%s", language)));
			}
		}
		return new CustomAggregationOperation(new Document("$group", counterGroup));
	}

	private AggregationOperation buildUnwindMatch(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, String domain) {

		List<Criteria> criterias = new ArrayList<Criteria>();
		if (countries != null && countries.size() != 0) {
			criterias.add(Criteria.where("agents.address.country").in(countries));
		}
		if (states != null && states.size() != 0) {
			criterias.add(Criteria.where("agents.address.state").in(states));
		}
		if (cities != null && cities.size() != 0) {
			criterias.add(Criteria.where("agents.address.city").in(cities));
		}
		if (timeZones != null && timeZones.size() != 0) {
			criterias.add(Criteria.where("agents.address.timeZone").in(timeZones));
		}
		if (languages != null && languages.size() != 0) {
			criterias.add(Criteria.where("agents.languages").in(languages));
		}
		/*if (domain != null && !domain.isEmpty()) {
			criterias.add(Criteria.where("agents.domains").in(domain));
		}*/
		if (criterias.size() == 0) {
			return Aggregation.match(new Criteria());
		} else {
			return Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()])));
		}
	}

	@Override
	public int getSupervisorOverrideAgentsCount(String campaignId) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(Aggregation.match(buildSupervisorOverrideBasicMatch(campaignId)));
		aggregationOperations.add(Aggregation.unwind("$agents"));
		aggregationOperations.add(Aggregation.match(Criteria.where("agents.supervisorOverrideCampaignId").is(campaignId)));
		aggregationOperations.add(buildSupervisorOverrideGroup());
		
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"team", Object.class);
		List<Object> supervisorOverrideAgentList = results.getMappedResults();
		int supervisorOverrideCampaignAgentsCount = 0;
		if (!supervisorOverrideAgentList.isEmpty()) {
			LinkedHashMap<?, ?> supervisorOverrideAgentResult = (LinkedHashMap<?, ?>) supervisorOverrideAgentList.get(0);
			supervisorOverrideCampaignAgentsCount = (Integer) supervisorOverrideAgentResult.get("Count");
		}
		return supervisorOverrideCampaignAgentsCount;
		
	}
	
	private Criteria buildSupervisorOverrideBasicMatch(String campaignId) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("agents.supervisorOverrideCampaignId").is(campaignId);
		criterias.add(Criteria.where("status").is("ACTIVE"));
		if (criterias.size() == 0) {
			return criteria;
		} else {
			return criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]));
		}
	}
	
	private AggregationOperation buildSupervisorOverrideGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$agents.supervisorOverrideCampaignId");
		group = group.append("Count", new BasicDBObject("$sum", 1));
		return new CustomAggregationOperation(new Document("$group", group));
	}
}
