package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.db.entity.HealthChecks;

public interface HealthChecksRepository extends MongoRepository<HealthChecks, String> {

}
