package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;

public interface PlivoOutboundNumberRepositoryCustom {

	public List<CountryDetails> getDistinctCountryDetails();

	public PlivoOutboundNumber findOneByIsdCode(int isd);

}
