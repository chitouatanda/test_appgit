package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CampaignMetaData;


public interface CampaignMetaDataRepository extends MongoRepository<CampaignMetaData, String> {

	@Query("{ '_id' : ?0 }")
    public CampaignMetaData findOneById(String id);
}
