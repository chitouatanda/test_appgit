package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.RealTimeDelivery;

public interface RealTimeDeliveryRepository extends MongoRepository<RealTimeDelivery, String> {

	@Query("{ 'xtaasCampaignId' : ?0 }")
	public RealTimeDelivery findByXtaasCampaignId(String xtaasCampaignId);

}
