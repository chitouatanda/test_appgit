package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.db.entity.SuppressionListNew;

public interface SuppressionListNewRepositoryCustom {

	void bulkinsert(List<SuppressionListNew> suppressionListNew);
}
