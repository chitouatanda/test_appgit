package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ProspectCallSpeedLog;



public interface ProspectCallSpeedRepository extends MongoRepository<ProspectCallSpeedLog, String> {
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public ProspectCallSpeedLog findByCampaignId(String campaignId);

}
