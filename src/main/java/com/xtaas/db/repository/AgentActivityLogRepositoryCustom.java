package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

public interface AgentActivityLogRepositoryCustom {

	public int getOnlineAgentsCount(String campaignId, Date fromDate, Date toDate);
	
	public double calculateOnlineTime(List<String> campaignIds, Date startDate, Date endDate);
}
