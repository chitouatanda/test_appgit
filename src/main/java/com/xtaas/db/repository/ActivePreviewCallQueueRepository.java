/**
 * 
 */
package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ActivePreviewCall;

/**
 * @author djain
 *
 */
public interface ActivePreviewCallQueueRepository extends MongoRepository<ActivePreviewCall, String> {
	@Query("{ '_id' : ?0 }")
	public ActivePreviewCall findOneById(String id);

	@Query(value = "{'processing' : false}")
	public List<ActivePreviewCall> findOldest(Pageable pageable);
	
	@Query(value = "{'parentCallSid' : ?0}")
	public ActivePreviewCall findByParentCallSid(String parentCallSid);

}
