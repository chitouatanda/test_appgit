package com.xtaas.db.repository;

import com.xtaas.db.entity.DataProvider;
import com.xtaas.db.entity.EnrichSourcePriority;
import com.xtaas.db.entity.EverStringCompany;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface EnrichSourcePriorityRepository extends MongoRepository<EnrichSourcePriority, String> {

    @Query(value = "{source : ?0}")
    public EnrichSourcePriority findBySource(String source);
}
