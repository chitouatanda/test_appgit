package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.domain.entity.PlivoOutboundNumber;

public interface PlivoOutboundNumberRepository
		extends MongoRepository<PlivoOutboundNumber, String>, PlivoOutboundNumberRepositoryCustom {

	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2}")
	public List<PlivoOutboundNumber> findByPartnerBucketWithAreaCode(String partnerId, String bucketName, int areaCode);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1}")
	public List<PlivoOutboundNumber> findByPartnerBucket(String partnerId, String bucketName);

	@Query("{'partnerId' : ?0}")
	public List<PlivoOutboundNumber> findOutboundNumbers(String partnerId);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2, 'pool': ?3}")
	public List<PlivoOutboundNumber> findByPartnerBucketAreaCodePool(String partnerId, String bucketName, int areaCode,
			int pool);

	@Query("{'bucketName' : ?0, 'areaCode' : ?1}")
	public List<PlivoOutboundNumber> findByBucketWithAreaCode(String bucketName, int areaCode);

	@Query("{'areaCode' : ?0}")
	public List<PlivoOutboundNumber> findTimeZone(int areaCode);

	@Query("{'bucketName' : ?0}")
	public List<PlivoOutboundNumber> findOutboundNumbersByBucketNumber(String bucketName);

	@Query("{'displayPhoneNumber' : ?0}")
	public List<PlivoOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber);
	
	@Query("{'isdCode' : ?0}")
	public List<PlivoOutboundNumber> findByIsdCode(int isd);
	
	@Query(value = "{'phoneNumber' : ?0}", delete = true)
	public List<PlivoOutboundNumber> deleteManyByPlivoPhoneNumber(String phoneNumber);

}
