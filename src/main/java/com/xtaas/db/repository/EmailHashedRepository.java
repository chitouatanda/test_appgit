package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.EmailHashed;



public interface EmailHashedRepository extends MongoRepository<EmailHashed, String> {
	
	

	@Query("{'emailHash' : {$exists : false}}")
	public List<EmailHashed> findByStatus(boolean status,Pageable pageable);
	
	
}
