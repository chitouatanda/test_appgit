package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ProspectMovement;

public interface ProspectMovementRepository
		extends MongoRepository<ProspectMovement, String> {
	
	@Query("{ '_id' : ?0 }")
    public ProspectMovement findOneById(String id);

}
