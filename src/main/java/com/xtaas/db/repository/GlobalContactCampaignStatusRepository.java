package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.GlobalContactCampaignStatus;

public interface GlobalContactCampaignStatusRepository
		extends MongoRepository<GlobalContactCampaignStatus, String>, GlobalContactCampaignStatusRepositoryCustom {

	@Query(value = "{ 'campaignId' : ?0 }")
	public GlobalContactCampaignStatus findByCampaignId(String campaignId);

	@Query(value = "{ 'status' : { $in : ?0 } }")
	public List<GlobalContactCampaignStatus> findByStatuses(List<String> statuses, Sort sort);

	@Query(value = "{ 'campaignId' : ?0, 'status' : ?1 }")
	public GlobalContactCampaignStatus findByCampaignIdAndStatus(String campaignId, String staus);

}
