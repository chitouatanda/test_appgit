package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Country;

public interface CountryRepository extends MongoRepository<Country, String> {

	@Query("{ 'country' : ?0 }")
	public Country findByCountry(String country);

}
