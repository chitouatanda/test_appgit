package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.client.result.DeleteResult;
import com.xtaas.db.entity.ProspectFeedback;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

public class ProspectFeedbackRepositoryImpl implements ProspectFeedbackRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(ProspectFeedbackRepositoryImpl.class);

	private MongoTemplate mongoTemplate;
	private PropertyService propertyService;

	public ProspectFeedbackRepositoryImpl(MongoTemplate mongoTemplate, PropertyService propertyService) {
		this.mongoTemplate = mongoTemplate;
		this.propertyService = propertyService;
	}

	@Override
	public void deleteAllBySourceAndCreatedDate(String source, Date createdDate) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		criteria.and("source").in(source);
		criteria.and("createdDate").gte(createdDate);
		query.addCriteria(criteria);
		logger.info("Deleting records from ProspectFeedback collection - [{}]", query.toString());
		DeleteResult deleteResult = mongoTemplate.remove(query, ProspectFeedback.class);
		logger.info("Deleted [{}] records from ProspectFeedback collection.", deleteResult.getDeletedCount());
	}

	@Override
	public void bulkInsert(List<ProspectFeedback> prospectFeedbackList) {
		int listSize = prospectFeedbackList.size();
		int batchSize = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.BULKINSERT_BATCHSIZE.name(), 2000);
		if (listSize <= batchSize) {
			mongoTemplate.insert(prospectFeedbackList, ProspectFeedback.class);
		} else {
			int batches = listSize / batchSize;
			int modSize = listSize % batchSize;
			for (int i = 0; i <= batches; i++) {
				int fromIndex = i * batchSize;
				int toIndex = fromIndex + batchSize;
				if (i == batches) {
					toIndex = fromIndex + modSize;
				}
				List<ProspectFeedback> tempProspectFeedbackList = prospectFeedbackList.subList(fromIndex, toIndex);
				mongoTemplate.insert(tempProspectFeedbackList, ProspectFeedback.class);
			}
		}
	}

}
