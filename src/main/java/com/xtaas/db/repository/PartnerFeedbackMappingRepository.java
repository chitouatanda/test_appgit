package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.PartnerFeedbackMapping;

/**
 * Repository to perform DB operations on PartnerFeedbackMapping collection
 * 
 * @author pranay
 *
 */
public interface PartnerFeedbackMappingRepository extends MongoRepository<PartnerFeedbackMapping, String> {

	@Query("{ 'enableDataFeedback' : ?0 }")
	public List<PartnerFeedbackMapping> findByEnableDataFeedback(boolean enableDataFeedback);

	@Query("{ 'name' : ?0, 'enableDataFeedback': ?1 }")
	public PartnerFeedbackMapping findByNameAndEnableDataFeedback(String name, boolean enableDataFeedback);

}
