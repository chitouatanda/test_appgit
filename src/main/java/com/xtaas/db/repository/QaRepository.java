package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.Qa;

public interface QaRepository extends MongoRepository<Qa, String> {
	@Query("{ '_id' : ?0 }")
	public Qa findOneById(String id);
	
	@Query("{ '_id' : { '$in' : ?0 }}")
	public List<Qa> findQaByIds(List<String> ids);
	
	@Query("{ '_id' : ?0 }")
	public Qa findQaById(String id);

}
