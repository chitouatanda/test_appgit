package com.xtaas.db.repository;

public interface DataBuyQueueRepositoryCustom {

	public DataBuyQueue findByCampaignAndSortByJobRequestTime(String campaignId);

}
