package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.DialerPreProcessEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.Campaign;


public interface DataBuyQueueRepository extends MongoRepository<DataBuyQueue, String>, DataBuyQueueRepositoryCustom {
	
	@Query("{ '_id' : ?0 }")
	public DataBuyQueue findOneById(String id);

	@Query("{'status' : {'$in': ?0}}")
	public List<DataBuyQueue> findByStatus(List<String> dataBuyStatusList,Pageable pageable);
	
	
	@Query("{'campaignId' : ?0,'status' : ?1 , 'jobStartTime' : {$gte : ?2}}")
	public DataBuyQueue findByCampaignIdAndStatusAndReqDate(String campaignId, String status,Date jobStartTime);
	
	@Query("{'campaignId' : ?0,'status' : ?1 }")
	public DataBuyQueue findByCampaignIdAndStatus(String campaignId, String status);
	
	@Query("{'insideViewJobId' : ?0}")
	public DataBuyQueue findByInsideViewJobId(String insideViewJobId);
	
	@Query("{'campaignId' : ?0,'status' : {'$in': ?1}}")
	public List<DataBuyQueue> findByStatusAndCampaignId(String campaignId,List<String> dataBuyStatusList);

	@Query("{'status' : {'$nin': ?0}, 'jobRequestTime' : {$gte : ?1, $lte : ?2}, 'isDailyBuyProspect' : {'$nin': ?3}  }")
	public List<DataBuyQueue> findByStatusAndJobStartTime(List<String> dataBuyStatusList, Date startDate, Date endDate, List<Boolean> dailyDataBuyVal );

	@Query("{'campaignId' : ?0,'_id' : ?1 }")
	public DataBuyQueue findByCampaignIdAndId(String campaignId, String id);

	@Query("{ 'campaignId' : ?0 }")
	public List<DataBuyQueue> findDbqBasedOnCampaign(String campaignId);

	@Query("{'campaignId' : ?0,'isDataActivate' : ?1 }")
	public List<DataBuyQueue> findByCampaignIdAndActivateStatus(String campaignId,boolean isDataActivate);
}
