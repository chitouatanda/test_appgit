package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.FailedRecordingsToAWS;

public interface FailedRecordingToAWSRepository extends MongoRepository<FailedRecordingsToAWS, String> {

	
	
	
}
