package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import com.mongodb.BasicDBObject;
import com.xtaas.domain.valueobject.CountryDetails;

public class OutboundNumberRepositoryImpl implements OutboundNumberRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(OutboundNumberRepositoryImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<CountryDetails> getDistinctCountryDetails() {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(createGroupCriterias());
		AggregationResults<CountryDetails> results = mongoTemplate
				.aggregate(Aggregation.newAggregation(aggregationOperations), "outboundnumber", CountryDetails.class);
		List<CountryDetails> countryDetails = results.getMappedResults();
		return countryDetails;
	}

	private AggregationOperation createGroupCriterias() {
		BasicDBObject group = new BasicDBObject("_id", new BasicDBObject("countryName", "$countryName")
				.append("countryCode", "$countryCode").append("isdCode", "$isdCode"));
		return new CustomAggregationOperation(new Document("$group", group));
	}

}