/**
 * 
 */
package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.StateCallConfig;

/**
 * @author djain
 *
 */
public interface StateCallConfigRepository extends MongoRepository<StateCallConfig, String> {

	@Query("{ 'stateCode' : ?0 }")
	public StateCallConfig findByStateCode(String stateCode);

	@Query("{ 'countryName' : ?0 }")
	public List<StateCallConfig> findByCountryName(String countryName);
	
	@Query("{ 'stateName' : ?0 }")
	public List<StateCallConfig> findByStateName(String stateName);

}
