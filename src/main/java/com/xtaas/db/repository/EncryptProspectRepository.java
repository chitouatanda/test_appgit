package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.domain.entity.PcLogNew;

public interface EncryptProspectRepository extends MongoRepository<PcLogNew, String> {

}
