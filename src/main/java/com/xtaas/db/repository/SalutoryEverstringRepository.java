package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SalutoryEverstring;

public interface SalutoryEverstringRepository extends MongoRepository<SalutoryEverstring, String>{

	@Query(value = "{'ES_ECID' :  ?0 }")
	public List<SalutoryEverstring> findByEcId(int ecid);
	
	@Query(value="{'status' : 'FOUND'}")
	public List<SalutoryEverstring> findSalutoryEverstringDomainList(Pageable pageable);
	
	@Query(value="{'ES_PrimaryWebsite' : ?0 ,'status' : 'FOUND'}")
	public List<SalutoryEverstring> findDomainList(String domain);
	
	@Query(value="{'status' : 'FOUND', 'pointer' : {$exists : false}}")
	public List<SalutoryEverstring> findSalutoryEverstringDomainListByPointer(Pageable pageable);
}
