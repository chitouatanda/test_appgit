package com.xtaas.db.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.client.result.DeleteResult;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

public class AbmListNewRepositoryImpl implements AbmListNewRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	PropertyService propertyService;

	@Override
	public void bulkinsert(List<AbmListNew> abmListNew) {
		int listSize = abmListNew.size();
		int batchSize = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.BULKINSERT_BATCHSIZE.name(), 2000);
		if (listSize <= batchSize) {
			mongoTemplate.insert(abmListNew, AbmListNew.class);
		} else {
			int batches = listSize / batchSize;
			int modSize = listSize % batchSize;
			for (int i = 0; i <= batches; i++) {
				int fromIndex = i * batchSize;
				int toIndex = fromIndex + batchSize;
				if (i == batches) {
					toIndex = fromIndex + modSize;
				}
				List<AbmListNew> tempABMList = abmListNew.subList(fromIndex, toIndex);
				mongoTemplate.insert(tempABMList, AbmListNew.class);
			}
		}
	}

	@Override
	public long deleteAllByCampaignIdAndStatus(String campaignId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("campaignId").is(campaignId);
		criteria = criteria.and("status").is("ACTIVE");
		query.addCriteria(criteria);
		DeleteResult deleteResult = mongoTemplate.remove(query, AbmListNew.class);
		long deletedCount = 0;
		if (deleteResult != null) {
			deletedCount = deleteResult.getDeletedCount();
		}
		return deletedCount;
	}

	@Override
	public AbmListNew findOneByCampaignIdCompanyName(String campaignId, String company) {
		Query query = new Query();
		Criteria criteria = Criteria.where("campaignId").is(campaignId);
		criteria = criteria.and("companyName").is(company);
		criteria = criteria.and("status").is("ACTIVE");
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, AbmListNew.class);
	}

	@Override
	public AbmListNew findOneByCampaignIdCompanyId(String campaignId, String companyId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("campaignId").is(campaignId);
		criteria = criteria.and("companyId").is(companyId);
		criteria = criteria.and("status").is("ACTIVE");
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, AbmListNew.class);
	}

}
