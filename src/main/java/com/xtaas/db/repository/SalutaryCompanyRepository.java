package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SalutaryCompany;


public interface SalutaryCompanyRepository extends MongoRepository<SalutaryCompany, String> {

	@Query(value = "{'companyDomain' : ?0 }")
	public List<SalutaryCompany> findCompanyDomain(String companyDomain);
	
}
