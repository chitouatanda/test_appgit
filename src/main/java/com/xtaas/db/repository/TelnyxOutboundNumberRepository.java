package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.TelnyxOutboundNumber;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface TelnyxOutboundNumberRepository
		extends MongoRepository<TelnyxOutboundNumber, String>, TelnyxOutboundNumberRepositoryCustom {

	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2}")
	public List<TelnyxOutboundNumber> findByPartnerBucketWithAreaCode(String partnerId, String bucketName, int areaCode);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1}")
	public List<TelnyxOutboundNumber> findByPartnerBucket(String partnerId, String bucketName);

	@Query("{'partnerId' : ?0}")
	public List<TelnyxOutboundNumber> findOutboundNumbers(String partnerId);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2, 'pool': ?3}")
	public List<TelnyxOutboundNumber> findByPartnerBucketAreaCodePool(String partnerId, String bucketName, int areaCode,
			int pool);

	@Query("{'bucketName' : ?0, 'areaCode' : ?1}")
	public List<TelnyxOutboundNumber> findByBucketWithAreaCode(String bucketName, int areaCode);

	@Query("{'areaCode' : ?0}")
	public List<TelnyxOutboundNumber> findTimeZone(int areaCode);

	@Query("{'bucketName' : ?0}")
	public List<TelnyxOutboundNumber> findOutboundNumbersByBucketNumber(String bucketName);

	@Query("{'displayPhoneNumber' : ?0}")
	public List<TelnyxOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber);
	
	@Query(value = "{'phoneNumber' : ?0}", delete = true)
	public List<TelnyxOutboundNumber> deleteManyByTelnyxPhoneNumber(String phoneNumber);

}
