package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.AgentSearchResult;

public interface AgentRepository extends MongoRepository<Agent, String>, AgentRepositoryCustom {
	@Query("{ '_id' : ?0 }")
	public Agent findOneById(String id);

	@Query("{ '_id' : { '$in' : ?0 }}")
	public List<AgentSearchResult> findByIds(List<String> ids);

	@Query("{ '_id' : { '$in' : ?0 }}")
	public List<Agent> findAgentByIds(List<String> ids);

	// Date:11/10/2017 Reason:TO show only Active agents to supervisor
	@Query("{ '_id' : { '$in' : ?0 } , 'status' : 'ACTIVE'}")
	public List<AgentSearchResult> findByIdsAndByStatus(List<String> ids);

	// DATE:28/12/2017 Reason:To fetch agents using partnerId and status
	@Query("{ 'partnerId' :  ?0 ,'status' : 'ACTIVE'}")
	public List<AgentSearchResult> findByPartnerIdAndStatus(String organization);

	@Query("{ '_id' : ?0 ,'partnerId' :  ?1}")
	public Agent findByIdAndPartnerId(String id, String organization);

	@Query("{ 'status' : 'INACTIVE' }")
	public List<Agent> findInactive();

}
