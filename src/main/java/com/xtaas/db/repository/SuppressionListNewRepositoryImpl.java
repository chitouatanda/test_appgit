package com.xtaas.db.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

public class SuppressionListNewRepositoryImpl implements SuppressionListNewRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	PropertyService propertyService;

	@Override
	public void bulkinsert(List<SuppressionListNew> suppressionListNew) {
		int listSize = suppressionListNew.size();
		int batchSize = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.BULKINSERT_BATCHSIZE.name(), 2000);
		if (listSize <= batchSize) {
			mongoTemplate.insert(suppressionListNew, SuppressionListNew.class);
		} else {
			int batches = listSize/batchSize;
			int modSize = listSize%batchSize;
			for (int i = 0; i <= batches; i++) {
				int fromIndex = i*batchSize;
				int toIndex = fromIndex + batchSize;
				if (i == batches) {
					toIndex = fromIndex + modSize;
				}
				List<SuppressionListNew> tempSuppressionListNew = suppressionListNew.subList(fromIndex, toIndex);
				mongoTemplate.insert(tempSuppressionListNew, SuppressionListNew.class);
			}
		}
	}
}
