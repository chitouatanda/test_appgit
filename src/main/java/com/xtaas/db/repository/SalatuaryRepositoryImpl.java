package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.DataBuyMapping;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectSalutary;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.Salatuary;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.valueobjects.KeyValuePair;

public class SalatuaryRepositoryImpl {

  @Autowired
  private SalatuaryRepository salatuaryRepository;

  @Autowired
  private CampaignRepository campaignRepository;

  @Autowired
  private StateCallConfigRepository stateCallConfigRepository;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private MongoTemplate mongoTemplate;
  
  @Autowired
  DataBuyMappingRepository buyMappingRepository;

  private Criteria buildBasicMatch(List<String> states, List<String> industries, List<String> functions,
      List<String> mgmtLevels, List<String> titles, Long minimumRevenue, Long maximumRevenue,
      Integer minimumEmployeeCount, Integer maximumEmployeeCount, List<String> city, List<String> zip,
      List<String> naics, List<String> sic, List<String> orgName, List<String> domain, List<String> email) {

    List<Criteria> criterias = new ArrayList<Criteria>();

    Criteria criteria = new Criteria();

    if (industries != null && industries.size() != 0) {
      criterias.add(Criteria.where("sicDescription").in(industries));
    }
    if (states != null && states.size() != 0) {
      criterias.add(Criteria.where("address1_state").in(states));
    }
    if (functions != null && functions.size() != 0) {
      criterias.add(Criteria.where("jobFunction").in(functions));
    }
    if (mgmtLevels != null && mgmtLevels.size() != 0) {
      criterias.add(Criteria.where("jobLevel").in(mgmtLevels));
    }
    /*
     * if (titles != null && titles.size() != 0) {
     * criterias.add(Criteria.where("prospectCall.prospect.title").in(titles)); }
     */
    if (titles != null && titles.size() != 0) {
      criterias.add(Criteria.where("jobTitle").regex(titles.get(0), "i"));
    }
    // if (minimumRevenue > 0) {
    // criterias.add(Criteria.where("revenueRange").gte(minimumRevenue));
    // }
    if (maximumRevenue > 0) {
      criterias.add(Criteria.where("revenueRange").lte(maximumRevenue));
    }
    // if (minimumEmployeeCount > 0) {
    // criterias.add(Criteria.where("employeeCountRange")
    // .gte(minimumEmployeeCount));
    // }
    if (maximumEmployeeCount > 0) {
      criterias.add(Criteria.where("employeeCountRange").lte(maximumEmployeeCount));
    }

    if (city != null && city.size() != 0) {
      criterias.add(Criteria.where("address2_city").lte(maximumEmployeeCount));
    }
    if (zip != null && zip.size() != 0) {
      criterias.add(Criteria.where("address2_postal").lte(maximumEmployeeCount));
    }
    if (naics != null && naics.size() != 0) {
      criterias.add(Criteria.where("NAICS").lte(maximumEmployeeCount));
    }
    if (sic != null && sic.size() != 0) {
      criterias.add(Criteria.where("sicCode").lte(maximumEmployeeCount));
    }
    if (maximumEmployeeCount > 0) {
      criterias.add(Criteria.where("orgName").lte(maximumEmployeeCount));
    }
    if (orgName != null && orgName.size() != 0) { //// emailDomain
      criterias.add(Criteria.where("orgDomain").lte(maximumEmployeeCount));
    }
    if (email != null && email.size() != 0) {
      criterias.add(Criteria.where("emailAddres").lte(maximumEmployeeCount));
    }

    return criteria.andOperator(criterias.toArray(new Criteria[0]));
  }
  
  private List<String> getSicIndex() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.companySIC");
 
	    return attList;
  }
  
  private List<String> getNaicsIndex() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.companyNAICS");
	    return attList;
  }
  
  private List<String> getAbmIndex() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.customAttributes.domain");//
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.customAttributes.maxRevenue");
	    attList.add("prospectCall.prospect.customAttributes.minRevenue");
	    attList.add("prospectCall.prospect.directPhone");
	    
	    return attList;
  }
  
  private List<String> getZipIndex() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.stateCode");
	    attList.add("prospectCall.prospect.zipCode");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.industry");
	   // attList.add("prospectCall.prospect.title");
	   // attList.add("NEGATIVE_TITLE");
	    attList.add("prospectCall.prospect.customAttributes.maxRevenue");
	    attList.add("prospectCall.prospect.customAttributes.minRevenue");
	    attList.add("prospectCall.prospect.directPhone");	    
	    
	    return attList;
  }
  
  private List<String> getDefaultIndex() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.industry");
	   // attList.add("prospectCall.prospect.title");
	   // attList.add("NEGATIVE_TITLE");
	    attList.add("prospectCall.prospect.customAttributes.maxRevenue");
	    attList.add("prospectCall.prospect.customAttributes.minRevenue");
	    attList.add("prospectCall.prospect.directPhone");

	    
	    return attList;
  }
  
  private List<String> getSearchAttributes() {
	  List<String> attList= new ArrayList<String>();
	    attList.add("prospectCall.prospect.department");
	    attList.add("prospectCall.prospect.customAttributes.maxEmployeeCount");
	    attList.add("prospectCall.prospect.customAttributes.minEmployeeCount");
	    attList.add("prospectCall.prospect.country");
	    attList.add("prospectCall.prospect.managementLevel");
	    attList.add("prospectCall.prospect.industry");
	   // attList.add("prospectCall.prospect.title");
	   // attList.add("NEGATIVE_TITLE");
	    attList.add("prospectCall.prospect.customAttributes.maxRevenue");
	    attList.add("prospectCall.prospect.customAttributes.minRevenue");
	    attList.add("prospectCall.prospect.customAttributes.domain");//
	    attList.add("prospectCall.prospect.companyNAICS");
	    attList.add("prospectCall.prospect.companySIC");
	    attList.add("prospectCall.prospect.stateCode");
	    attList.add("prospectCall.prospect.zipCode");
	    attList.add("prospectCall.prospect.directPhone");
	    
	    return attList;
  }
  
  
  private Criteria buildBasicMatchUsingMap(HashMap<String, Map<String, Object>> finalCriteriaMap,  List<String> abmDomains,
		  boolean maximumRev, boolean maximumEmp, boolean isDirectPurchase) {
	    List<String> attList= new ArrayList<String>();
	    if(finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.domain")) {
	    	attList = getAbmIndex();
	    }else if(finalCriteriaMap.containsKey("prospectCall.prospect.companyNAICS") ) {
	    	attList = getSicIndex();
	    }else if(finalCriteriaMap.containsKey("prospectCall.prospect.companySIC")) {
	    	attList = getNaicsIndex();
	    }else if(finalCriteriaMap.containsKey("prospectCall.prospect.stateCode") || finalCriteriaMap.containsKey("prospectCall.prospect.zipCode")) {
	    	attList = getZipIndex();
	    }else {
	    	attList = getDefaultIndex();
	    }
	    String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570,1802,263946,132874,198410,67338";
	    String[] notInValsArr = removeIndusCodes.split(",");
		List<String> notInValsList = Arrays.asList(notInValsArr);
		List<String> notInValsSaltList = new ArrayList<String>();

		 Map<String, List<String>> industryMap = getSalutaryIndustry();
	        if (industryMap != null) {
	          for (String nvalue : notInValsList) {
	            if (industryMap.containsKey(nvalue)) {
	            	notInValsSaltList.addAll(industryMap.get(nvalue));
	            }
	          }
	        }

  		String removeIndustries = "Public Safety|Cities,Towns & Municipalities|"
				+ "Cities,Towns & Municipalities General|Organizations|Membership Organizations|"
				+ "Charitable Organizations & Foundations|Organizations General|"
				+ "Religious Organizations|Government|Education|Education General|K-12 Schools|Training|Colleges & Universities";
    List<Criteria> criterias = new ArrayList<Criteria>();
    Criteria criteria = new Criteria();
    for(String searchField : attList) {
    	if(finalCriteriaMap.containsKey(searchField)) {
    	      for (Map.Entry<String, Object> operatorCriteriaEntry : finalCriteriaMap.get(searchField).entrySet()) {
    	          if (operatorCriteriaEntry.getKey().equalsIgnoreCase("IN")){
    	          	if (operatorCriteriaEntry.getValue() instanceof ArrayList<?>) {
      		            if (searchField.contains("prospectCall.prospect.industry")) {
      		            	List<String>  indList = (List<String>)operatorCriteriaEntry.getValue();
      	        			if(indList.size()==1 && indList.contains("All")) {
      	      	              criterias.add(Criteria.where(searchField).nin((List<String>)notInValsSaltList));
      	        			}else {
          		            	criterias.add(Criteria.where(searchField).in((List<String>)operatorCriteriaEntry.getValue()));	
      	        			} 
      		            }else {
      		            	criterias.add(Criteria.where(searchField).in((List<String>)operatorCriteriaEntry.getValue()));	
      		            }
    	          	}else {
    		            if (searchField.contains("min")) {
    			              criterias.add(Criteria.where(searchField).gte((Number)operatorCriteriaEntry.getValue()));  
    			        } else if (searchField.contains("max") ) {
    			        	List<Integer> zeroInt = new ArrayList<Integer>(1);
    			        	zeroInt.add(0);
    			        	/*if(searchField.contains("maxRevenue") && maximumRev) {
        			            criterias.add(Criteria.where(searchField).in(zeroInt));
    			        	}else if(searchField.contains("maxRevenue") && !maximumRev) {
        			            criterias.add(Criteria.where(searchField).nin(zeroInt));
    			        	}*/

                            if(searchField.contains("maxRevenue")) {
        			            criterias.add(Criteria.where(searchField).lte((Number)operatorCriteriaEntry.getValue()).nin(zeroInt));
                                //criterias.add(Criteria.where(searchField).nin(zeroInt));
                            }

                            if(searchField.contains("maxEmployeeCount")){
        			            criterias.add(Criteria.where(searchField).lte((Number)operatorCriteriaEntry.getValue()).nin(zeroInt));
                                //criterias.add(Criteria.where(searchField).nin(zeroInt));
                            }
    			        	
    			        /*	if(searchField.contains("maxEmployeeCount") && maximumEmp){
        			            criterias.add(Criteria.where(searchField).in(zeroInt));
    			        	}else if(searchField.contains("maxEmployeeCount") && !maximumEmp){
        			            criterias.add(Criteria.where(searchField).nin(zeroInt));
    			        	}*/
    			        	//.orOperator(criteria)
    			           // criterias.add(Criteria.where(searchField).lte((Number)operatorCriteriaEntry.getValue()));
    			        }
    		        }
    	          }else if (operatorCriteriaEntry.getKey().equalsIgnoreCase("NOT IN")) {
  		            if (searchField.contains("prospectCall.prospect.industry")) {
  		            	List<String>  ninIndustryList = (List<String>)operatorCriteriaEntry.getValue();
  		            	ninIndustryList.addAll(notInValsSaltList);
      	              criterias.add(Criteria.where(searchField).nin((List<String>)ninIndustryList));

  		            }else {
    	              criterias.add(Criteria.where(searchField).nin((List<String>)operatorCriteriaEntry.getValue()));
  		            }
    	          }
    	      }
    	}else {
    	    if(searchField.equalsIgnoreCase("prospectCall.prospect.stateCode")) {
    	    	List<String> statceinvalid = new ArrayList<String>(1);
    	    	statceinvalid.add("N/A");
    			criterias.add(Criteria.where(searchField).nin((List<String>)statceinvalid));	
    	    }else if(searchField.equalsIgnoreCase("prospectCall.prospect.directPhone")){
	            criterias.add(Criteria.where(searchField).in(isDirectPurchase));
    	    }else if (searchField.contains("prospectCall.prospect.industry")) {
  	              criterias.add(Criteria.where(searchField).nin((List<String>)notInValsSaltList));
	       }else {
    	    	List<String> arr = new ArrayList<String>(0);
    			criterias.add(Criteria.where(searchField).nin(arr));	
    	    }

    	}
    }
    List<String> searchAttribs = getSearchAttributes();
    for(String otherField :  searchAttribs) {
    	if(attList.contains(otherField)) {
    		//do nothing
    	}else {
    		if(finalCriteriaMap.containsKey(otherField)) {
      	      for (Map.Entry<String, Object> operatorCriteriaEntry : finalCriteriaMap.get(otherField).entrySet()) {
      	          if (operatorCriteriaEntry.getKey().equalsIgnoreCase("IN")){
      	          	if (operatorCriteriaEntry.getValue() instanceof ArrayList<?>) {
              			criterias.add(Criteria.where(otherField).in((List<String>)operatorCriteriaEntry.getValue()));	
      	          	}else {
      		            if (otherField.contains("min")) {
      			              criterias.add(Criteria.where(otherField).gte((Number)operatorCriteriaEntry.getValue()));  
      			            } else if (otherField.contains("max")) {
      			              criterias.add(Criteria.where(otherField).lte((Number)operatorCriteriaEntry.getValue()));
      			            }
      		        }

      	          }else if (operatorCriteriaEntry.getKey().equalsIgnoreCase("NOT IN")) {
      	              criterias.add(Criteria.where(otherField).nin((List<String>)operatorCriteriaEntry.getValue()));
      	          }
      	      }
      	}
    	}
    }
    /*if(finalCriteriaMap.containsKey("prospectCall.prospect.stateCode")) {
    	// Do Nothing
    }else {
    	Map<String, Object> stateninmap = new HashMap<String, Object>();
    	List<String> statceinvalid = new ArrayList<String>(1);
    	statceinvalid.add("N/A");
    	stateninmap.put("NOT IN", statceinvalid);
    	finalCriteriaMap.put("prospectCall.prospect.stateCode", stateninmap);
    }
    for (Map.Entry<String, Map<String, Object>> finalEntry : finalCriteriaMap.entrySet()) {
      for (Map.Entry<String, Object> operatorCriteriaEntry : finalEntry.getValue().entrySet()) {
        if (operatorCriteriaEntry.getKey().equalsIgnoreCase("IN")){
        	if (operatorCriteriaEntry.getValue() instanceof ArrayList<?>) {
        		if(finalEntry.getKey().equalsIgnoreCase("prospectCall.prospect.title")) {       		    	  
        		    	    List<Criteria> criteriasOR = new ArrayList<Criteria>();
        		    	    Criteria orcriteria = new Criteria();
        		    	    List<String> titArray = (List<String>)operatorCriteriaEntry.getValue();
        		    	    for(String title : titArray) {
            		    	    criteriasOR.add(orcriteria.where("prospectCall.prospect.title").regex(title, "i"));
        		    	    }
        		    	    orcriteria.orOperator(criteriasOR.toArray(new Criteria[criteriasOR.size()]));
        		    	    criterias.add(orcriteria);

        		}else {
        			criterias.add(Criteria.where(finalEntry.getKey()).in((List<String>)operatorCriteriaEntry.getValue()));	
        		}
        	} else {
	            if (finalEntry.getKey().contains("min")) {
	              criterias.add(Criteria.where(finalEntry.getKey()).gte((Number)operatorCriteriaEntry.getValue()));  
	            } else if (finalEntry.getKey().contains("max")) {
	              criterias.add(Criteria.where(finalEntry.getKey()).lte((Number)operatorCriteriaEntry.getValue()));
	            }
        	}
        } else if (operatorCriteriaEntry.getKey().equalsIgnoreCase("NOT IN")) {
          criterias.add(Criteria.where(finalEntry.getKey()).nin((List<String>)operatorCriteriaEntry.getValue()));
        }
      }
    }*/
    //Handle abm campaigns
//    if(abmDomains!=null && abmDomains.size()>0) {  	  
//	    List<Criteria> criteriasOR = new ArrayList<Criteria>();
//	    Criteria orcriteria = new Criteria();
//	    for(String domain : abmDomains) {
//    	    criteriasOR.add(orcriteria.where("prospectCall.prospect.customAttributes.domain").regex(domain, "i"));
//	    }
//	    orcriteria.orOperator(criteriasOR.toArray(new Criteria[criteriasOR.size()]));
//	    criterias.add(orcriteria);
//    }
    
    Criteria finalCriteria = criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]));
    return finalCriteria;
  }
  
  // to do
  public Query getSearchProspectSalutaryContactsQuery(HashMap<String, Map<String, Object>> finalCriteriaMap , 
		  List<String> abmDomains,boolean maximumRev, boolean maximumEmp,boolean isDirectPurchase) {
    Query searchQuery = Query.query(buildBasicMatchUsingMap(finalCriteriaMap,abmDomains, maximumRev,  maximumEmp,isDirectPurchase));
    return searchQuery;
  }
  
  public List<ProspectSalutary> searchProspectSalutaryContacts(Query searchQuery, Pageable pageable) {
    List<ProspectSalutary> prospectSalutaryList = mongoTemplate.find(searchQuery.with(pageable), ProspectSalutary.class, "prospectsalutary_new");
    return prospectSalutaryList;
  }
  
  public List<Salatuary> searchContacts(List<String> states, List<String> industries, List<String> functions,
      List<String> mgmtLevels, List<String> titles, Long minimumRevenue, Long maximumRevenue,
      Integer minimumEmployeeCount, Integer maximumEmployeeCount, List<String> city, List<String> zip,
      List<String> naics, List<String> sic, List<String> orgName, List<String> domain, List<String> email) {
    return mongoTemplate
        .find(Query.query(buildBasicMatch(states, industries, functions, mgmtLevels, titles, minimumRevenue,
            maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, city, zip, naics, sic, orgName, domain, email))
        // .withHint("idx_camp_mgmtLvl_revAttrib_EmpCnt_title_industry")
            , Salatuary.class, "salatuary");
  }

  private List<ProspectCallLog> getProspects(List<String> states, List<String> industries, List<String> functions,
      List<String> mgmtLevels, List<String> titles, Long minimumRevenue, Long maximumRevenue,
      Integer minimumEmployeeCount, Integer maximumEmployeeCount, List<String> city, List<String> zip,
      List<String> naics, List<String> sic, List<String> orgName, List<String> domain, List<String> email) {

    List<Salatuary> sList = searchContacts(states, industries, functions, mgmtLevels, titles, minimumRevenue,
        maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, city, zip, naics, sic, orgName, domain, email);

    List<ProspectCallLog> queuedList = new ArrayList<ProspectCallLog>();

    for (Salatuary salt : sList) {
      queuedList.add(convertSCLToPCL(salt));
    }

    return queuedList;
  }

  public String getTimeZone(String state) {
    StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(state);
    return stateCallConfig.getTimezone();

  }

  private String getUSPhone(String phone) {
    phone = phone.replaceAll("[^A-Za-z0-9]", "");
    if (phone.length() >= 10) {
      phone = phone.substring(0, 10);
    }
    return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
  }

  private ProspectCallLog convertSCLToPCL(Salatuary s) {

    ProspectCallLog ps = new ProspectCallLog();
    try {
      Prospect p = new Prospect();
      p.setFirstName(s.getFirstName());
      p.setLastName(s.getLastName());
      if (s.getAddress1_state() != null && !s.getAddress1_state().isEmpty()) {
        p.setStateCode(s.getAddress1_state());
        p.setCity(s.getAddress1_city());
        p.setAddressLine1(s.getAddress1_line1());
        p.setZipCode(s.getAddress2_postal());
        p.setTimeZone(getTimeZone(s.getAddress1_state()));
      } else if (s.getAddress2_state() != null && !s.getAddress2_state().isEmpty()) {
        p.setStateCode(s.getAddress2_state());
        p.setCity(s.getAddress2_city());
        p.setAddressLine1(s.getAddress2_line1());
        p.setZipCode(s.getAddress2_postal());
        p.setTimeZone(getTimeZone(s.getAddress2_state()));

      } else {
        p.setStateCode("CA");
        p.setCity(s.getAddress1_city());
        p.setAddressLine1(s.getAddress1_line1());
        p.setZipCode(s.getAddress2_postal());
        p.setTimeZone("US/Pacific");
      }
      p.setSource("SALUTORY");
      p.setDataSource("Xtaas Data Source");
      p.setSourceId(UUID.randomUUID().toString());
      p.setCompany(s.getOrgName());
      p.setTitle(s.getJobTitle());
      p.setDepartment(s.getJobFunction());
      p.setPhone(getUSPhone(s.getPhone1()));
      Map<String, Object> customAttributes = new HashMap<String, Object>();
      if (s.getRevenueRange() != null && !s.getRevenueRange().isEmpty()) {
        Map<String, String[]> revMap = getRevenueMap();
        String[] revArr = revMap.get(s.getRevenueRange());
        customAttributes.put("minRevenue", new Double(Integer.parseInt(revArr[0])));
        customAttributes.put("maxRevenue", new Double(revArr[1]));
      }
      if (s.getEmployeeCountRange() != null && !s.getEmployeeCountRange().isEmpty()) {
        Map<String, String[]> empMap = getEmployeeMap();
        String[] empArr = empMap.get(s.getEmployeeCountRange());

        customAttributes.put("maxEmployeeCount", new Double(Integer.parseInt(empArr[1])));
        customAttributes.put("minEmployeeCount", new Double(Integer.parseInt(empArr[0])));
      }

      if (s.getOrgDomain() != null && !s.getOrgDomain().isEmpty())
        customAttributes.put("domain", s.getOrgDomain());
      else
        customAttributes.put("domain", s.getEmailDomain());

      p.setCustomAttributes(customAttributes);
      p.setSourceCompanyId(s.getId());
      p.setManagementLevel(s.getJobLevel());

      p.setZoomPersonUrl(s.getLinkedIn_url());
      p.setSourceType("INTERNAL");
      List<String> sicList = new ArrayList<String>(1);
      sicList.add(s.getSicCode());
      p.setCompanySIC(sicList);
      List<String> naicsList = new ArrayList<String>(1);
      naicsList.add(s.getNAICS());
      p.setCompanyNAICS(naicsList);

      if (s.getPhone2() != null && !s.getPhone2().isEmpty()) {
        p.setCompanyPhone(getUSPhone(s.getPhone2()));
      } else {
        p.setCompanyPhone(getUSPhone(s.getPhone1()));

      }
      if (s.getSicDescription() != null && !s.getSicDescription().isEmpty())
        p.setIndustry(s.getSicDescription());
      else if (s.getNAICS_description() != null && !s.getNAICS_description().isEmpty())
        p.setIndustry(s.getNAICS_description());
      p.setEmail(s.getEmailAddres());
      p.setCountry("United States");

      ProspectCall pc = new ProspectCall(p.getSourceId(), "SALUTARY_DATA", p);
      ps.setDataSlice("slice10");
      pc.setCampaignId("SALUTARY_DATA");
      pc.setDataSlice("slice10");
      pc.setCallRetryCount(786);
      pc.setCallDuration(0);
      pc.setOrganizationId("XTAAS CALL CENTER");

      ps.setStatus(com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
      ps.setProspectCall(pc);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return ps;
  }
  
  public Map<Integer, String> getEmpSortedMap(){
	  Map<Integer, String> empSortedMap = new HashMap<Integer, String>();

	    empSortedMap.put(1, "1 to 19");  
	    empSortedMap.put(2, "20 to 99");	   
	    empSortedMap.put(3, "100 to 499");	  
	    empSortedMap.put(4, "500 to 999");
	    empSortedMap.put(5, "1000 To 4999");
	    empSortedMap.put(6, "5000+");

	  return empSortedMap;
  }

  public Map<String, String[]> getEmployeeMap() {
    Map<String, String[]> empMap = new HashMap<String, String[]>();
    /////
    
    String[] empArray5 = new String[2];
    empArray5[0] = "1";
    empArray5[1] = "20";
    empMap.put("1 to 19", empArray5);

    String[] empArray3 = new String[2];
    empArray3[0] = "20";
    empArray3[1] = "100";
    empMap.put("20 to 99", empArray3);
    
    String[] empArray1 = new String[2];
    empArray1[0] = "100";
    empArray1[1] = "500";
    empMap.put("100 to 499", empArray1);

    String[] empArray6 = new String[2];
    empArray6[0] = "400";
    empArray6[1] = "1000";
    empMap.put("500 to 999", empArray6);

    String[] empArray7 = new String[2];
    empArray7[0] = "1000";
    empArray7[1] = "5000";
    empMap.put("1000 To 4999", empArray7);
    
    String[] empArray2 = new String[2];
    empArray2[0] = "5000";
    empArray2[1] = "10000";
    empMap.put("5000+", empArray2);

    return empMap;
  }

  public Map<String, String[]> getRevenueMap() {
    Map<String, String[]> empMap = new HashMap<String, String[]>();
    /////
    String[] empArray2 = new String[2];
    empArray2[0] = "1000000000";
    empArray2[1] = "5000000000";
    empMap.put("$1B+", empArray2);

    String[] empArray3 = new String[2];
    empArray3[0] = "100000000";
    empArray3[1] = "1000000000";
    empMap.put("$100M to <$1B", empArray3);

    String[] empArray4 = new String[2];
    empArray4[0] = "50000000";
    empArray4[1] = "100000000";
    empMap.put("$50M to <$100M", empArray4);
    
    String[] empArray1 = new String[2];
    empArray1[0] = "10000000";
    empArray1[1] = "50000000";
    empMap.put("$10M to <$50M", empArray1);

    String[] empArray6 = new String[2];
    empArray6[0] = "0";
    empArray6[1] = "10000000";
    empMap.put("$1M to <$10M", empArray6);
    
    String[] empArray5 = new String[2];
    empArray5[0] = "0";
    empArray5[1] = "1000000";
    empMap.put("<$1M", empArray5);

    return empMap;
  }

  //////////////////////////////////////////////
  
  public Map<Integer, String> getMgmtWeighMap(){
	  Map<Integer, String> mgtweightMap = new HashMap<Integer, String>();
	  mgtweightMap.put(1, "Non Management");
	  mgtweightMap.put(2, "MANAGER");
	  mgtweightMap.put(3, "DIRECTOR");
	  mgtweightMap.put(4, "VP_EXECUTIVES");
	  mgtweightMap.put(5, "C_EXECUTIVES");

	  return mgtweightMap;
  }

  public Map<String, List<String>> getSalutaryMgmtLevel() {
    Map<String, List<String>> mgmtMap = new HashMap<String, List<String>>();
    List<String> mgr = new ArrayList<String>(1);
    mgr.add("Manager");
    mgmtMap.put("MANAGER", mgr);
    List<String> nm = new ArrayList<String>(1);
    nm.add("Staff");
    nm.add("");
    nm.add("Consultant");
    mgmtMap.put("Non Management", nm);
    List<String> dir = new ArrayList<String>(1);
    dir.add("Director");
    mgmtMap.put("DIRECTOR", dir);
    List<String> cteam = new ArrayList<String>(1);
    cteam.add("C-Team");
    mgmtMap.put("C_EXECUTIVES", cteam);
    List<String> vp = new ArrayList<String>(1);
    vp.add("VP");
    mgmtMap.put("VP_EXECUTIVES", vp);

    return mgmtMap;
  }

  public Map<String, List<String>> getSalutaryRevRange() {
    Map<String, List<String>> revMap = new HashMap<String, List<String>>();

    List<String> bplus = new ArrayList<String>(1);
    bplus.add("$1B+");
    // bplus.add("");
    revMap.put("$1B-$5B", bplus);

    // List<String> fiveplus = new ArrayList<String>(1);
    // bplus.add("");
    // revMap.put("More than $5Billion", fiveplus);

    List<String> hm = new ArrayList<String>(1);
    hm.add("$100M to <$1B");

    revMap.put("$100M-$250M", hm);
    revMap.put("$250M-$500M", hm);
    revMap.put("$500M-$1B", hm);

    List<String> fivem = new ArrayList<String>(1);
    fivem.add("$50M to <$100M");
    revMap.put("$50M-$100M", fivem);

    List<String> tenm = new ArrayList<String>(1);
    tenm.add("$10M to <$50M");
    revMap.put("$10M-$25M", tenm);
    revMap.put("$25M-$50M", tenm);

    List<String> onem = new ArrayList<String>(1);
    onem.add("$1M to <$10M");

    revMap.put("$5M-$10M", onem);
    revMap.put("0-$10M", onem);

    return revMap;
  }

  public Map<String, List<String>> getSalutaryEmpRange() {
    Map<String, List<String>> empMap = new HashMap<String, List<String>>();

    List<String> fivet = new ArrayList<String>(1);
    fivet.add("5000+");
    fivet.add("");

    empMap.put("5000-10000", fivet);
    empMap.put("Over 10000", fivet);

    List<String> onet = new ArrayList<String>(1);
    onet.add("1000 to 4999");
    onet.add("1000 To 4999");

    empMap.put("1000-5000", onet);

    List<String> oneh = new ArrayList<String>(1);
    oneh.add("100 to 499");

    empMap.put("100-250", oneh);
    empMap.put("250-500", oneh);

    List<String> fiveh = new ArrayList<String>(1);
    fiveh.add("500 to 999");

    empMap.put("500-1000", fiveh);

    List<String> twty = new ArrayList<String>(1);
    twty.add("20 to 99");

    empMap.put("20-50", twty);
    empMap.put("50-100", twty);

    List<String> zero = new ArrayList<String>(1);
    zero.add("1 to 19");
    
    empMap.put("10-20", zero);
    empMap.put("1-5", zero);
    empMap.put("5-10", zero);

    return empMap;
  }

  public Map<String, String> getSalutaryDepartment() {
    Map<String, String> dept = new HashMap<String, String>();

    dept.put("4980839", "Consultants");//not in salutary so just keeping the name as it is as zoom.
    dept.put("4980839", "Biometrics");//not in salutary so just keeping the name as it is as zoom.
    dept.put("2228226", "Biotechnical Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("3211266", "Chemical Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("3801090", "Civil Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("4653058", "Database Engineering &  Design");//not in salutary so just keeping the name as it is as zoom.
    dept.put("4849666", "Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("1376258", "Hardware Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("5570562", "Industrial Engineering");//not in salutary so just keeping the name as it is as zoom.
    dept.put("4849767", "Scientists");//not in salutary so just keeping the name as it is as zoom.
    dept.put("4259942", "General Management");//not in salutary so just keeping the name as it is as zoom.




    
    dept.put("4784231", "Engineering");
    dept.put("5505026", "Information Technology");
    dept.put("5308418", "Information Technology");
    dept.put("6029314", "Information Technology");
    dept.put("1114114", "Information Technology");
    dept.put("4718594", "Information Technology");
    dept.put("4522087", "Sales and Support");
     dept.put("393218", "Sales and Support");
    dept.put("196610", "Real Estate");
    dept.put("4063234", "Risk, Safety, Compliance");
    dept.put("6094850", "Quality");
    dept.put("5832807", "Legal");
    dept.put("1572866", "Legal");
    dept.put("1507330", "Legal");
    dept.put("4325378", "Program and Project Management");
    dept.put("4653159", "Operations");
    dept.put("3014658", "Operations");
    dept.put("4390914", "Operations");
    dept.put("2752514", "Operations");
    dept.put("5373954", "Operations");
    dept.put("5701634", "Operations");
    dept.put("2686978", "Operations");
    dept.put("4456551", "Marketing and Product");
    dept.put("2555906", "Marketing and Product");
    dept.put("3932162", "Marketing and Product");
    dept.put("393216", "Marketing and Product");
    dept.put("3276802", "Marketing and Product");
    dept.put("4587623", "Finance and Administration");
    dept.put("262146", "Finance and Administration");
    dept.put("327682", "Finance and Administration");
    dept.put("2621442", "Finance and Administration");
    dept.put("4718695", "Human Resources");
    dept.put("2162690", "Human Resources");
    dept.put("4915202", "Human Resources");
    dept.put("3473410", "Human Resources");
    dept.put("5177346", "Human Resources");
    dept.put("5242882", "Human Resources");
    dept.put("393218", "Business Management");//
    dept.put("5701735", "Healthcare");
    dept.put("4784130", "Healthcare");
    dept.put("3670018", "Healthcare");
    dept.put("589826", "Healthcare");
    dept.put("3145730", "Healthcare");
    dept.put("1769474", "Banking and Wealth Management");//
    dept.put("1441794", "Banking and Wealth Management");//
    dept.put("786434", "Publishing, Editorial and Reporting");//
    dept.put("2293762", "Engineering");//

    return dept;
  }

  public Map<String, List<String>> getSalutaryIndustry() {
    // Map<String, String> industry = new HashMap<String, String>();

    // // industry.put("","Real Estate");
    // industry.put("266", "Agriculture");
    // industry.put("65802", "Animals & Livestock");
    // industry.put("131338", "Crops");
    // industry.put("196874", "Agriculture General");
    // industry.put("522", "Business Services");
    // industry.put("66058", "Accounting & Accounting Services");
    // industry.put("197130", "Auctions");
    // industry.put("262666", "Call Centers & Business Centers");
    // industry.put("328202", "Debt Collection");
    // industry.put("786954", "Management Consulting");
    // industry.put("655882", "Information Technology");
    // industry.put("852490", "Multimedia & Graphic Design");
    // industry.put("393738", "Food Service");
    // industry.put("983562", "Business Services General");
    // industry.put("590346", "Human Resources & Staffing");
    // industry.put("131594", "Advertising & Marketing");
    // industry.put("524810", "Commercial Printing");
    // industry.put("1802", "Educational Services");
    
    DataBuyMapping industrydbMappingObj = buyMappingRepository.findBySourceAndAttribute("SALUTARY", "INDUSTRY");
    Map<String, List<String>> industryMainMapping = new HashMap<>();
    if (industrydbMappingObj != null && industrydbMappingObj.getMapping() != null && industrydbMappingObj.getMapping().size() > 0) {
      for (KeyValuePair<String, String> element : industrydbMappingObj.getMapping()) {
        if (element.getKey() != null && element.getValue() != null) {
          List<String> salutaryIndustryValues = Arrays.stream(element.getValue().toString().split(";")).collect(Collectors.toList());
          industryMainMapping.put(element.getKey().toString(), salutaryIndustryValues);
        }
      }
    }
    return industryMainMapping;
  }
  
  
  public List<String> getSalutaryZoomMap(List<String> zoomList) {
	   // Map<String, List<String>> empMap = new HashMap<String, List<String>>();

	    List<String> fivet = new ArrayList<String>(1);
	  /*  fivet.add("5000+");
	    fivet.add("");

	    empMap.put("5000-10000", fivet);
	    empMap.put("Over 10000", fivet);

	    List<String> onet = new ArrayList<String>(1);
	    onet.add("1000 to 4999");
	    onet.add("1000 To 4999");

	    empMap.put("1000-5000", onet);

	    List<String> oneh = new ArrayList<String>(1);
	    oneh.add("100 to 499");

	    empMap.put("100-250", oneh);
	    empMap.put("250-500", oneh);

	    List<String> fiveh = new ArrayList<String>(1);
	    fiveh.add("500 to 999");

	    empMap.put("500-1000", fiveh);

	    List<String> twty = new ArrayList<String>(1);
	    twty.add("20 to 99");

	    empMap.put("20-50", twty);
	    empMap.put("50-100", twty);

	    List<String> zero = new ArrayList<String>(1);
	    zero.add("1 to 19");
	    
	    empMap.put("10-20", zero);
	    empMap.put("1-5", zero);
	    empMap.put("5-10", zero);*/
	    
	    if(zoomList.contains("1-5") && zoomList.contains("5-10") && zoomList.contains("10-20")) {
	    	 fivet.add("1 to 19");
	    }
	    if(zoomList.contains("20-50") && zoomList.contains("50-100") ) {
	    	 fivet.add("20 to 99");
	 	   // fivet.add("");
	    }
	    if(zoomList.contains("100-250") && zoomList.contains("250-500") ) {
	    	 fivet.add("100 to 499");
	 	    //fivet.add("");
	    }
	    if(zoomList.contains("500-1000")) {
	    	 fivet.add("500 to 999");
	 	   // fivet.add("");
	    }
	    if(zoomList.contains("1000-5000")) {
	    	fivet.add("1000 to 4999");
	    	fivet.add("1000 To 4999");
	    }
	    if(zoomList.contains("5000-10000") && zoomList.contains("Over 10000") ) {
	    	 fivet.add("5000+");
	 	    fivet.add("");
	    }

	    return fivet;
	  }
}
