package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AgentOnlineTime;;

public interface AgentOnlineTimeRepository extends MongoRepository<AgentOnlineTime, String> {

	@Query("{ 'agentId' : ?0 }")
	public List<AgentOnlineTime> findLatestAgentStatusEntry(String agentId, Pageable pageable);

	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE'}")
	public List<AgentOnlineTime> getLastStatus(String agentId, Pageable pageable);

}
