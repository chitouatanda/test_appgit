package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.AgentConference;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AgentConferenceRepository extends MongoRepository<AgentConference,String>, AgentConferenceRepositoryCustom {

  public List<AgentConference> findByConferenceId(String conferenceId);

  public List<AgentConference> findByAgentId(String agentId);

}