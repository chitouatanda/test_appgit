package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.SignalwireOutboundNumber;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface SignalwireOutboundNumberRepository extends MongoRepository<SignalwireOutboundNumber, String> {
    
    @Query("{'partnerId' : ?0}")
	public List<SignalwireOutboundNumber> findOutboundNumbers(String partnerId);

    @Query(value = "{'phoneNumber' : ?0}", delete = true)
	public List<SignalwireOutboundNumber> deleteManyBySignalwirePhoneNumber(String phoneNumber);
}
