package com.xtaas.db.repository;

import com.xtaas.db.entity.ProspectSalutaryNew;
import com.xtaas.db.entity.ProspectSalutaryQ3;

import java.util.List;

public interface ProspectSalutaryQ3RepositoryCustom {

	void bulkInsert(List<ProspectSalutaryQ3> updateProspectSalutaryNewQ3);


}
