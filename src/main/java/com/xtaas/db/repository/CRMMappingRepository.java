package com.xtaas.db.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.CRMMapping;

public interface CRMMappingRepository extends MongoRepository<CRMMapping, String>{
    @Query("{ '_id' : ?0 }")
	public CRMMapping findOneById(String id);
}
