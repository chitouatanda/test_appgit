package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.ProspectCallInteractionTemp;


import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

 public interface ProspectCallInteractionTempRepository extends MongoRepository<ProspectCallInteractionTemp, String> {
	
	 @Query("{'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?0,$lte: ?1}}")
		public List<ProspectCallInteractionTemp> findByDate(Date startDate, Date endDate, Pageable pageable);
		
		@Query(value = "{'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?0,$lte: ?1}}", count = true)
		public int findByDateCount(Date startDate, Date endDate);
		
		@Query(value = "{'prospectCall.campaignId' : ?0 ,'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?1,$lte: ?2}}", count = true)
		public int findByCampaignDateCount(String campaignId, Date startDate, Date endDate);
		
		@Query("{'prospectCall.campaignId' : ?0 ,'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?1,$lte: ?2}}")
		public List<ProspectCallInteractionTemp> findByCampaignDate(String campaignId,Date startDate, Date endDate, Pageable pageable);
}
