package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import com.mongodb.BasicDBObject;


public class ProspectCallInteractionRepositoryImpl implements ProspectCallInteractionRepositoryCustom{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public int getHandleTime(List<String> campaignIds, Date startDate, Date endDate) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatch(campaignIds, startDate, endDate));
		aggregationOperations.add(buildGroup());
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcallinteraction", Object.class);
	  List<Object> handleTimeList =  results.getMappedResults();
	  int count = 0;
		if (!handleTimeList.isEmpty()) {
			LinkedHashMap<?, ?> handleTimeMap = (LinkedHashMap<?, ?>) handleTimeList.get(0);
			for (Entry<?, ?> object : handleTimeMap.entrySet()) {
				 if (object.getKey().toString().equalsIgnoreCase("count")) {
					 count = Integer.parseInt(object.getValue().toString());	
				}
			}
//			count = (Integer) handleTimeMap.get("count");
		}
		return count;
	}

	private AggregationOperation buildGroup() {
		BasicDBObject group = new BasicDBObject("_id", "prospectHandleDuration");
		group = group.append("count", new BasicDBObject("$sum", "$prospectCall.prospectHandleDuration"));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	private AggregationOperation buildBasicMatch(List<String> campaignIds, Date startDate, Date endDate) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignIds);
		criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
		criteria = criteria.and("status").in("WRAPUP_COMPLETE");
		return Aggregation.match(criteria);
	}

}
