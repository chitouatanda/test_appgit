package com.xtaas.db.repository;

import com.xtaas.db.entity.TitleCategoryMapping;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface TitleCategoryMappingRepository extends MongoRepository<TitleCategoryMapping, String> {

}
