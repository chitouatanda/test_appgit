package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.utils.DataBuyUtils;


public class SuccessCallLogRepositoryImpl implements SuccessCallLogRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Long findBySuccessCriteriaCount(CampaignCriteria cc){
		Criteria criteria = getInternalCriteria(cc);
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.count(query, SuccessCallLog.class);
	}
	
	@Override
	public List<SuccessCallLog> findByCriteria(CampaignCriteria cc){
		List<SuccessCallLog> sclogList = new ArrayList<SuccessCallLog>();
		int size  = 100;
		Long successLeads = findBySuccessCriteriaCount(cc);
		long batchsize = 0;
		if(successLeads!=null && successLeads>0){
			batchsize = successLeads /100;
			long modSize = successLeads % 100;
			if(modSize > 0){
				batchsize = batchsize +1;
			}
			for(int i = 0; i< batchsize ; i++){
				Pageable pageable = PageRequest.of(i, size);
				Criteria criteria = getInternalCriteria(cc);
				Query query = new Query();
				
				query.addCriteria(criteria);
				//sclogList = mongoTemplate.find(query.with(pageable), SuccessCallLog.class);
				sclogList.addAll(mongoTemplate.find(query.with(pageable ), SuccessCallLog.class));
			}
		}
		
		if(cc.getTitleKeyWord()!=null && !cc.getTitleKeyWord().isEmpty()){
			sclogList = findByTitle(sclogList,cc);
		}
		return sclogList;

	}
	
	
	private Criteria getInternalCriteria(CampaignCriteria cc){
		
		
		//List<GlobalContact> globalContacts = new ArrayList<GlobalContact>();
		Criteria criteria = new Criteria();
		List<String> arr = new ArrayList<String>();
		List<String> countries = new ArrayList<String>();
		List<String> managementLevel = new ArrayList<String>();
		List<String> managementLevelDB = new ArrayList<String>();
		List<String> departments = new ArrayList<String>();
		List<String> departmentsZoom = new ArrayList<String>();
		//List<String> titles = new ArrayList<String>();
		List<String> industry = new ArrayList<String>();
		List<String> industriesZoom = new ArrayList<String>();
		List<String> directPhone = new ArrayList<String>();
		List<String> minEmployee = new ArrayList<String>();
		List<String> maxEmployee = new ArrayList<String>();
		List<String> minRevenue = new ArrayList<String>();
		List<String> maxRevenue = new ArrayList<String>();
		List<String> source = new ArrayList<String>();
		
		if (cc.getCountry() != null && !cc.getCountry().isEmpty()) {
			arr = Arrays.asList(cc.getCountry().split(","));
			for(String country : arr){
				if(country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA")){
					countries.add("United States");
				}else{
					countries.add(country);
				}
			}
			criteria = criteria.and("prospectCall.prospect.country").in(countries);
		}else{
			criteria = criteria.and("prospectCall.prospect.country").nin(countries);
		}
		
		if (cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
			departments = Arrays.asList(cc.getDepartment().split(","));
			Map<String,String> titleMap = DataBuyUtils.getTitlesMap();
			Map<String,String> titleGrpMap = DataBuyUtils.getTitlesGroupMap();
			Map<String,String> titleMapZoom = new HashMap<String, String>();
			for(Map.Entry<String, String> entry : titleMap.entrySet()){
				titleMapZoom.put(entry.getValue(), entry.getKey());
			}
			for(String dzoom : departments){
				departmentsZoom.add(titleMapZoom.get(dzoom));
			}
			// Getting the root departments
			for(String dzoom : departments){
				String parentDepartment = titleGrpMap.get(dzoom);
				if(parentDepartment!=null && !departments.contains(parentDepartment))
					departmentsZoom.add(titleMapZoom.get(parentDepartment));
			}
			criteria = criteria.and("prospectCall.prospect.department").in(departmentsZoom);
		}else{
			criteria = criteria.and("prospectCall.prospect.department").nin(departments);
		}
		
		//Titles are managed at code
		//criteria = criteria.and("department").nin(departments);
		
		if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()) {
			industry = Arrays.asList(cc.getIndustry().split(","));
			Map<String,String> industryMap = DataBuyUtils.getIndustryMap();
			Map<String, String> zoomIndustryGrpMap = DataBuyUtils.getIndustryGrpMap();
			Map<String,String> industryMapZoom = new HashMap<String, String>();
			for(Map.Entry<String, String> entry : industryMap.entrySet()){
				industryMapZoom.put(entry.getValue(), entry.getKey());
			}
			for(String dindustry : industry){
				industriesZoom.add(industryMapZoom.get(dindustry));
			}
			
			for(String dindustry : industry){
				String parentIndustry = zoomIndustryGrpMap.get(dindustry);
				if(parentIndustry!=null && industry.contains(parentIndustry))
					industriesZoom.add(industryMapZoom.get(parentIndustry));
			}
			criteria = criteria.and("prospectCall.prospect.industry").in(industriesZoom);
		}else{
			criteria = criteria.and("prospectCall.prospect.industry").nin(industry);
		}
		
		/*if (isDirect) {
			criteria = criteria.and("directPhone").in(true);
		}else{*/
			criteria = criteria.and("prospectCall.prospect.directPhone").nin(directPhone);
		//}
		
		if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()) {
			Long minEmpCount = new Long(cc.getMinEmployeeCount());
			if(minEmpCount>0)
				criteria = criteria.and("prospectCall.prospect.customAttributes.minEmployeeCount").gte(minEmpCount);
		}else{
			criteria = criteria.and("prospectCall.prospect.customAttributes.minEmployeeCount").nin(minEmployee);
		}
		
		if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()) {
			Long maxEmpCount = new Long(cc.getMaxEmployeeCount());
			if(maxEmpCount>0){
				maxEmpCount =maxEmpCount+1;
				criteria = criteria.and("prospectCall.prospect.customAttributes.maxEmployeeCount").lte(maxEmpCount);
			}
		}else{
			criteria = criteria.and("prospectCall.prospect.customAttributes.maxEmployeeCount").nin(maxEmployee);
		}
		
		if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty()) {
			Double minRevenueTemp = new Double(cc.getMinRevenue());
			if(minRevenueTemp>0){
				Double minRev = minRevenueTemp*1000;
				criteria = criteria.and("prospectCall.prospect.customAttributes.minRevenue").gte(minRev);
			}
		}else{
			criteria = criteria.and("prospectCall.prospect.customAttributes.minRevenue").nin(minRevenue);
		}
		
		if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty()) {
			Double maxRevenueTemp = new Double(cc.getMaxRevenue());
			if(maxRevenueTemp>0){
				Double maxRev = maxRevenueTemp*1000;
				criteria = criteria.and("prospectCall.prospect.customAttributes.maxRevenue").lte(maxRev);
			}
		}else{
			criteria = criteria.and("prospectCall.prospect.customAttributes.maxRevenue").nin(maxRevenue);
		}
	
		if (cc.getSource() != null && !cc.getSource().isEmpty()) {
			source = Arrays.asList(cc.getSource().split(","));
			criteria = criteria.and("prospectCall.prospect.source").lte(source);
		}else{
			criteria = criteria.and("prospectCall.prospect.source").nin(source);
		}
		
	
		/*Query query = new Query();
		query.addCriteria(criteria);*/
		return criteria;
		//return globalContacts;
	
	}
	
	@Override
	public List<SuccessCallLog> findByCompanies(CampaignCriteria cc,long size,int start){
		
		List<String> sourceCompanyIds = cc.getAbmCompanyIds();
		List<String> cids = new ArrayList<String>();
		//int count = (int)(size - 99);
				
		for(int i=start;i<=size;i++){
			if(sourceCompanyIds.size()>size){
				String cid = sourceCompanyIds.get(i);
				cids.add(cid);
			}
		}

		Criteria criteria = new Criteria();
		List<String> arr = new ArrayList<String>();
		List<String> countries = new ArrayList<String>();
		List<String> managementLevel = new ArrayList<String>();
		List<String> managementLevelDB = new ArrayList<String>();
		List<String> departments = new ArrayList<String>();
		List<String> departmentsZoom = new ArrayList<String>();
		List<String> abmList = new ArrayList<String>();
		
		
		if (cc.getCountry() != null && !cc.getCountry().isEmpty()) {
			arr = Arrays.asList(cc.getCountry().split(","));
			for(String country : arr){
				if(country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA")){
					countries.add("United States");
				}else{
					countries.add(country);
				}
			}
			criteria = criteria.and("prospectCall.prospect.country").in(countries);
		}else{
			criteria = criteria.and("prospectCall.prospect.country").nin(countries);
		}
		
		
		if(cc.getAbmCompanyIds()!=null && cc.getAbmCompanyIds().size()>0){
			criteria = criteria.and("prospectCall.prospect.sourceCompanyId").in(cids);
		}else{
			criteria = criteria.and("prospectCall.prospect.sourceCompanyId").nin(abmList);
		}
		
		if (cc.getManagementLevel() != null && !cc.getManagementLevel().isEmpty()) {
			managementLevel = Arrays.asList(cc.getManagementLevel().split(","));
			
			for(String mgmt : managementLevel){
				if(mgmt.equalsIgnoreCase("VP_EXECUTIVES")){
					managementLevelDB.add("VP-Level");
					managementLevelDB.add("VP_EXECUTIVES");
				}else if(mgmt.equalsIgnoreCase("C_EXECUTIVES")){
					managementLevelDB.add("C-Level");
					managementLevelDB.add("C_EXECUTIVES");
				}else if(mgmt.equalsIgnoreCase("MANAGER")){
					managementLevelDB.add("MANAGER");
					managementLevelDB.add("Manager");
				}else if(mgmt.equalsIgnoreCase("DIRECTOR")){
					managementLevelDB.add("Director");
					managementLevelDB.add("DIRECTOR");
				}
				else{
					managementLevelDB.add(mgmt);
				}
			}
			criteria = criteria.and("prospectCall.prospect.managementLevel").in(managementLevelDB);
		}else{
			criteria = criteria.and("prospectCall.prospect.managementLevel").nin(managementLevel);
		}
		
		if (cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
			departments = Arrays.asList(cc.getDepartment().split(","));
			Map<String,String> titleMap = DataBuyUtils.getTitlesMap();
			Map<String,String> titleGrpMap = DataBuyUtils.getTitlesGroupMap();
			Map<String,String> titleMapZoom = new HashMap<String, String>();
			for(Map.Entry<String, String> entry : titleMap.entrySet()){
				titleMapZoom.put(entry.getValue(), entry.getKey());
			}
			for(String dzoom : departments){
				departmentsZoom.add(titleMapZoom.get(dzoom));
			}
			// Getting the root departments
			for(String dzoom : departments){
				String parentDepartment = titleGrpMap.get(dzoom);
				if(parentDepartment!=null && !departments.contains(parentDepartment))
					departmentsZoom.add(titleMapZoom.get(parentDepartment));
			}
			criteria = criteria.and("prospectCall.prospect.department").in(departmentsZoom);
		}else{
			criteria = criteria.and("prospectCall.prospect.department").nin(departments);
		}
		
		Query query = new Query();
		query.addCriteria(criteria);
		List<SuccessCallLog> scList =  mongoTemplate.find(query, SuccessCallLog.class);
		if(cc.getTitleKeyWord()!=null && !cc.getTitleKeyWord().isEmpty()){
			scList = findByTitle(scList,cc);
		}
		return scList;
	}
	
	
	private List<SuccessCallLog> findByTitle(List<SuccessCallLog> scList ,CampaignCriteria cc){
		List<SuccessCallLog> titleSCList = new ArrayList<SuccessCallLog>();
		List<String> titleList = Arrays.asList(cc.getTitleKeyWord().split(","));;
		for(SuccessCallLog sc : scList){
			if(sc.getProspectCall().getProspect().getTitle()!=null && !sc.getProspectCall().getProspect().getTitle().isEmpty()){
				for(String title : titleList){
	
					 String apurchaseTitle = title.toLowerCase();
					 String bpurchaseTitle = sc.getProspectCall().getProspect().getTitle().toLowerCase();
					 List<String> spacePersonTitleList = Arrays.asList(apurchaseTitle.replaceAll("[^a-zA-Z]+"," ").split(" "));
					 List<String> spaceZoomTitleList = Arrays.asList(bpurchaseTitle.replaceAll("[^a-zA-Z]+"," ").split(" "));
	
					 boolean flag = false;
	
					 if(spaceZoomTitleList.containsAll(spacePersonTitleList)){
						 flag= true;
					 }
					 //spacePersonTitleList.parallelStream().allMatch(bpurchaseTitle::contains);//.anyMatch(purchaseTitle::contains).
					 if(flag){
						 //System.out.println("------------>FLAG IS TURE: SOURCE-->"+apurchaseTitle+"----------->PRSPECT---->"+bpurchaseTitle);
						 titleSCList.add(sc);
					 }
				 
				}
			}
		}
		return titleSCList;
	}
}
