package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AgentStatusLog;

public interface AgentStatusLogRepository extends MongoRepository<AgentStatusLog, String> {

	@Query("{ 'agentId' : ?0 }")
	public List<AgentStatusLog> findLatestAgentStatusEntry(String agentId, Pageable pageable);

	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE'}")
	public List<AgentStatusLog> getLastStatus(String agentId, Pageable pageable);
	
	@Query("{'agentId' : ?0,'createdDate' :  {$gte : ?1, $lte : ?2} , 'activityType' : 'AGENT_STATUS_CHANGE', 'status' : 'ONLINE'}")
	public List<AgentStatusLog> getOnlineByDate(String agentId, Date fromDate, Date toDat,Pageable pageable);

}
