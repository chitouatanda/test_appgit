package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.QAProspectReport;

public interface QAProspectReportRepository extends MongoRepository<QAProspectReport, String> {


}

