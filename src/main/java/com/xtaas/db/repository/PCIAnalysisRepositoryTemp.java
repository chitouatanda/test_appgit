package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.PCIAnalysisTemp;


public interface PCIAnalysisRepositoryTemp extends MongoRepository<PCIAnalysisTemp, String> {
	
	@Query("{ 'campaignid' : ?0, 'email' : ?1 }")
	public PCIAnalysisTemp getProspectByCIDAndEmail(String campaignId,String email);
	
	@Query(value= "{ 'prospectcallid' : ?0 }")
	public List<PCIAnalysisTemp> findByProspectCallIdWithSort(String prospectCallId, Pageable pageable);
	
}