package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.EmailBounces;

public interface EmailBouncesRepository extends MongoRepository<EmailBounces, String>, EmailBouncesRepositoryCustom {

	@Query(value = "{ 'prospectCallId' : ?0 }")
	public List<EmailBounces> findByProspectCallId(String prospectCallId);

}
