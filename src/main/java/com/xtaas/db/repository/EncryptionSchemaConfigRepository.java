package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.db.entity.EncryptionSchemaConfig;

public interface EncryptionSchemaConfigRepository extends MongoRepository<EncryptionSchemaConfig, String> {

}