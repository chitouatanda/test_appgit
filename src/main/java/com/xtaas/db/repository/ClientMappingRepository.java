package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ClientMapping;

public interface ClientMappingRepository extends MongoRepository<ClientMapping, String> {

	@Query("{ 'clientName' : ?0, 'key' : {'$regex' : ?1, $options: 'i'} }")
	public ClientMapping findByClientNameAndByKey(String clientName, String key);
	
	@Query("{ 'clientName' : ?0 }")
	public List<ClientMapping> findByClientName(String clientName);

	@Query("{ 'clientName' : ?0, 'value' : {'$regex' : ?1, $options: 'i'} }")
	public ClientMapping findByClientNameAndByValue(String clientName, String value);
	
	@Query("{ 'campaignId' : ?0, 'key' : {'$regex' : ?1, $options: 'i'} }")
	public ClientMapping findByCampaignIdAndByKey(String campaignId, String key);
	
	@Query("{ 'campaignId' : ?0, 'value' : {'$regex' : ?1, $options: 'i'} }")
	public ClientMapping findByCampaignIdAndByValue(String campaignId, String value);
	
	@Query("{ 'campaignId' : ?0, 'clientName' : ?1 }")
	public List<ClientMapping> findByCampaignIdAndClientName(String campaignId, String clientName);
	
	@Query("{ 'campaignId' : ?0, 'clientName' : ?1 }")
	public List<ClientMapping> findAllByCampaignIdAndClientNameAndBySequenceSort(String campaignId, String clientName, Pageable pageRequest);
	
	@Query("{ 'campaignId' : ?0}")
	public List<ClientMapping> findClientMappingByCampaignId(String campaignId);
	
}
