package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.valueobject.ContactStatus;
import com.xtaas.domain.valueobject.CountResult;

public class ContactRepositoryImpl implements ContactRepositoryCustom {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Contact> searchContacts(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount, int offset, int size) {
		return mongoTemplate.find(
				Query.query(
						buildBasicMatch(industries, functions, mgmtLevels, titles, domains, countries, states, cities,
								minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount))
						.skip(offset).limit(size), Contact.class, "contact");
	}

	@Override
	public List<CountResult> countContacts(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(Aggregation.match(buildBasicMatch(industries, functions, mgmtLevels, titles, domains, countries,
						states, cities, minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount)));
		aggregationOperations.add(buildCounterGroup());
		AggregationResults<CountResult> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "contact", CountResult.class);
		return results.getMappedResults();
	}

	private AggregationOperation buildCounterGroup() {
		BasicDBObject counterGroup = new BasicDBObject("_id", null);
		counterGroup = counterGroup.append("count", new BasicDBObject("$sum", 1));
		return new CustomAggregationOperation(new Document("$group", counterGroup));
	}
	
	public String getQueryString(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount) {
		Criteria criteria = buildBasicMatch(industries, functions, mgmtLevels, titles, domains, countries,
				states, cities, minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount);
		return Query.query(criteria).getQueryObject().toString();
	}

	private Criteria buildBasicMatch(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("status").is(ContactStatus.ACTIVE.name());
		if (industries != null && industries.size() != 0) {
			criterias.add(Criteria.where("industry").in(industries));
		}
		if (functions != null && functions.size() != 0) {
			criterias.add(Criteria.where("function").in(functions));
		}
		if (mgmtLevels != null && mgmtLevels.size() != 0) {
			criterias.add(Criteria.where("mgmtLevel").in(mgmtLevels));
		}
		if (titles != null && titles.size() != 0) {
			criterias.add(Criteria.where("title").in(titles));
		}
		if (domains != null && domains.size() != 0) {
			criterias.add(Criteria.where("domain").in(domains));
		}
		if (countries != null && countries.size() != 0) {
			criterias.add(Criteria.where("country").in(countries));
		}
		if (states != null && states.size() != 0) {
			criterias.add(Criteria.where("state").in(states));
		}
		if (cities != null && cities.size() != 0) {
			criterias.add(Criteria.where("city").in(cities));
		}
		if (minimumRevenue != null && minimumRevenue > 0) {
			criterias.add(Criteria.where("revenue").gte(minimumRevenue));
		}
		if (maximumRevenue != null && maximumRevenue < 0) {
			criterias.add(Criteria.where("revenue").lte(maximumRevenue));
		}
		if (minimumEmployeeCount != null && minimumEmployeeCount > 0) {
			criterias.add(Criteria.where("employeeCount").gte(minimumEmployeeCount));
		}
		if (maximumEmployeeCount != null && maximumEmployeeCount < 0) {
			criterias.add(Criteria.where("employeeCount").gte(maximumEmployeeCount));
		}
		if (criterias.size() == 0) {
			return criteria;
		} else {
			return criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]));
		}
	}
}
