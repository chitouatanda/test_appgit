package com.xtaas.db.repository;

import com.xtaas.db.entity.ProspectSalutary;
import com.xtaas.db.entity.ProspectSalutaryTemp;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ProspectSalutaryRepositoryTemp extends MongoRepository<ProspectSalutaryTemp, String> {

    @Query(value = "{'dataMoveStatus' : {$exists : false}}")
    public List<ProspectSalutaryTemp> findSalutaryProspectsByPointer(Pageable pageable);

    @Query(value = "{}")
    public List<ProspectSalutaryTemp> findSalutaryProspectsPage( Pageable pageable);
}
