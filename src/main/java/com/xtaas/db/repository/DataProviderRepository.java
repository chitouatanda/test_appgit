package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.DataProvider;

public interface DataProviderRepository extends MongoRepository<DataProvider, String> {

	
}
