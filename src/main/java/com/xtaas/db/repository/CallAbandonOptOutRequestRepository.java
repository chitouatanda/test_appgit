/**
 * 
 */
package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.CallAbandonOptOutRequest;

/**
 * @author djain
 *
 */
public interface CallAbandonOptOutRequestRepository extends MongoRepository<CallAbandonOptOutRequest, String> {

}
