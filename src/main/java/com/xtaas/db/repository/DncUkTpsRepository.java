package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.DncUkTps;

public interface DncUkTpsRepository extends MongoRepository<DncUkTps, String> {

	public DncUkTps findByTelNo(String telNo);

}
