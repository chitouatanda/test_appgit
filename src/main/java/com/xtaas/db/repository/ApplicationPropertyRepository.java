/**
 * 
 */
package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.ApplicationProperty;

/**
 * @author djain
 *
 */
public interface ApplicationPropertyRepository extends MongoRepository<ApplicationProperty, String> {
	
	@Query("{ '_id' : ?0 }")
    public ApplicationProperty findOneById(String id);
}
