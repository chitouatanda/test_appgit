package com.xtaas.db.repository;

import com.xtaas.db.entity.DialerPreProcessEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author djain
 *
 */
public interface DialerPreProcessQueueRepository extends MongoRepository<DialerPreProcessEntity, String> {
	
	@Query("{ 'prospectCall.campaignId' : ?0 }")
	public DialerPreProcessEntity findNext(String campaignId, Sort sortOrder); //by default returned in the descending order of CRM's created date, i.e. latest first (LIFO)
	
}
