package com.xtaas.db.repository;

import com.xtaas.domain.entity.AgentConference;

public interface AgentConferenceRepositoryCustom {

  public String findAgentIdByConferenceId(String conferenceId);

  public AgentConference findConferenceIdByAgentId(String agentId);

}