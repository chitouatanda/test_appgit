package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SupervisorPreference;
import com.xtaas.domain.valueobject.PreferenceId;

public interface SupervisorPreferenceRepository extends MongoRepository<SupervisorPreference, String> {
	
	@Query("{ 'supervisorId' : ?0, 'preferenceId': ?1}")
	public SupervisorPreference findBySupervisorAndPreferenceId(String supervisorId, PreferenceId preferenceId);

}
