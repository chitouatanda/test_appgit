package com.xtaas.db.repository;

import com.xtaas.db.entity.DataBuyMapping;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface DataBuyMappingRepository extends MongoRepository<DataBuyMapping, String> {
  
  @Query("{ 'source' : ?0, 'attribute' : ?1}")
	public DataBuyMapping findBySourceAndAttribute(String source, String attribute);
  
  @Query("{ 'source' : ?0, 'inputType' : ?1}")
	public List<DataBuyMapping> findBySourceAndInputType(String source, String inputType);

  
}