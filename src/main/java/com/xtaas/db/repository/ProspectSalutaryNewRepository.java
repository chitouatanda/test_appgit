package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.ProspectSalutaryNew;

public interface ProspectSalutaryNewRepository extends MongoRepository<ProspectSalutaryNew, String>,ProspectSalutaryNewRepositoryCustom {


}

