package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CampaignDNorm;

public interface CampaignDNormRepository extends MongoRepository<CampaignDNorm, String> {

	@Query("{ 'campaignId' :  ?0 }")
	public CampaignDNorm findByCampaignId(String campaignId);

}
