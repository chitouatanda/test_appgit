package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CallStats;

public interface CallStatsRepository extends MongoRepository<CallStats, String>{

	@Query("{'type' : ?0, 'updatedDate' : {$gte: ?1, $lt: ?2}}")
	public List<CallStats> findCallstatsRecords(String type, Date startDate, Date endDate);
	
	@Query("{'type' : ?0}")
	public List<CallStats> findLatestCallstatsRecord(String type,Pageable pageable);

}
