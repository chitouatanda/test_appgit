package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.DncUsDncScrub;

public interface DncUsDncScrubRepository extends MongoRepository<DncUsDncScrub, String> {

	public DncUsDncScrub findByPhone(String phone);

}
