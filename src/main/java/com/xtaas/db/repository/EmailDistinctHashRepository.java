package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.EmailDistinctHash;



public interface EmailDistinctHashRepository extends MongoRepository<EmailDistinctHash, String> {
	
	

	@Query("{'emailHash' : {$exists : false}}")
	public List<EmailDistinctHash> findByStatus(boolean status,Pageable pageable);
	
	
}
