package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.EverStringCompany;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.web.dto.IdNameDTO;
import com.xtaas.web.dto.StatsDTO;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/10/14
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CampaignRepository extends MongoRepository<Campaign, String> , CampaignRepositoryCustom{
	@Query("{ '_id' : ?0 }")
	public Campaign findOneById(String id);
		
	@Query("{ 'team.teamId' : {'$in' : ?0 }, 'status': {'$nin': ['STOPPED','DELISTED']}}")
	public List<Campaign> findByTeams(List<String> teamIds);
	
	@Query("{ 'team.teamId' : {'$in' : ?0 }, 'organizationId' : ?1, 'status': {'$nin': ['STOPPED','DELISTED']}}")
	public List<Campaign> findByTeamsAndClient(List<String> teamIds, String clientId);
	
	@Query("{ 'campaignManagerId' : ?0 }")
	public List<Campaign> findByCampaignManagerId(String campaignManagerId);
	
	@Query(value = "{ 'campaignManagerId' : ?0, 'status': ?1 }" , count = true)
	public int findByCampaignManagerId(String campaignManagerId, CampaignStatus status);
	
	@Query("{ 'campaignManagerId' : ?0 }")
	public List<Campaign> findByCampaignManagerId(String campaignManagerId, Pageable pageable);
	
	@Query("{ $or : [{'team.supervisorId' : ?0}, {'partnerSupervisors': ?0} ], 'status': {'$in': ?1} }")
	public List<Campaign> findBySupervisorId(String supervisorId, List<String> statuses, Pageable pageable);
	
	@Query(value = "{ $or : [{'team.supervisorId' : ?0}, {'partnerSupervisors': ?0} ], 'status': {'$in': ?1} }", count = true)
	public long countBySupervisorId(String supervisorId, List<String> statuses);
	
	@Query(value="{ 'campaignManagerId' : ?0,  'agentInvitations.0': {'$exists': true}, '_id': {'$ne': ?1}}")
	public List<Campaign> findBySelfManagedTeam(String campaignManagerId, String campaignId, Pageable pageable);
	
	@Query(value="{ 'campaignManagerId' : ?0,  'teamInvitations.0': {'$exists': true}, '_id': {'$ne': ?1}}")
	public List<Campaign> findBySupervisorManagedTeam(String campaignManagerId, String campaignId, Pageable pageable);
	
	@Query("{ 'status' : ?0 }")
	public List<Campaign> findByStatus(CampaignStatus status);
	
	@Query("{ 'name' : {'$regex' : ?0 } }")
	public Campaign findByName(String campaignName);
	
	@Query("{ 'name' : {'$regex' : ?0, $options: 'i'}, 'organizationId': ?1, 'status' : { $in : ['PLANNING','RUNNING']}}")
	public List<Campaign> findCampaignsByName(String campaignName, String organizationId);
	
	@Query("{ 'name' : {'$regex' : ?0, $options: 'i'}, 'organizationId': ?1, 'status' : { $in : ['PLANNING','RUNNING']}, $or : [{'team.supervisorId' : ?2}, {'partnerSupervisors': ?2} ]}")
	public List<Campaign> findCampaignsByNameAndSupervisorId(String campaignName, String organizationId,String supervisorId);
	
	@Query("{ 'name' :  ?0  }")
	public Campaign findByExactName(String campaignName);
	
	@Query("{ 'team.agents' : { $elemMatch : { 'resourceId' : ?0 } }, 'status': {'$nin': ['DELISTED', 'STOPPED', 'COMPLETED']}, $or : [{'startDate' : {$lte : ?1}, 'endDate' : {$gte : ?1}}, {'startDate' : {$gte : ?1, $lte : ?2}}]}")
	public List<Campaign> findByAllocatedAgent(String agentId, Date startDate, Date endDate);
	
	@Query("{ 'team.agents.workSchedules.scheduleId' : ?0 }")
	public Campaign findByWorkSchedule(String scheduleId);
	
	@Query("{ '_id' : {'$in' : ?0 } }")
	public List<Campaign> findCampaignByIds(List<String> campaignIds);
	
	@Query("{ 'status' : {'$in' : ['RUNNING'] } }")
	 public List<Campaign> getActiveCampaigns();
	
	@Query("{ 'status' : {'$in' : ['RUNNING'] }, 'organizationId' : ?0 }")
	 public List<Campaign> getActiveCampaignsByOrg(String organizationId);
	//public List<Campaign> findActiveCampaigns();
	
	@Query("{ 'status' : {'$in' : ['RUNNING'] }, 'organizationId' :  {'$in' : ['XTAAS CALL CENTER'] } }")
	 public List<Campaign> getActiveXtaasCampaigns();
	
	@Query("{ 'status' : {'$in' : ['RUNNING'] }, 'organizationId' :  {'$nin' : ['XTAAS CALL CENTER'] } }")
	 public List<Campaign> getActiveOtherCampaigns();
	
	@Query("{ 'name' : {'$regex' : ?0, $options: 'i' }, 'organizationId' : ?1}")
	public List<Campaign> searchCampaignByName(String name, String organizationId, Pageable pageRequest);
	
	@Query(value = "{ 'name' : {'$regex' : ?0, $options: 'i' }, 'organizationId' : ?1}", count = true)
	public long countMatchingCampaignList(String name, String campaignManagerId);

	@Query(value = "{'status':{$in:['RUNNING','PAUSED']}, $or : [{'partnerSupervisors': ?0 },{'team.supervisorId': ?0 }]}", fields="{ _id : 1, 'name' : 1}")
	public List<StatsDTO> getActiveCampaignsForSupervisorStats(String id);
	
	@Query(value = "{}", fields = "{_id : 1, name : 1}")
	public List<IdNameDTO> findAllIdsAndNames(Sort sort);
	
	@Query("{ 'sfdcCampaignId' :  ?0  }")
	public Campaign findCdpCampaign(String sfdcCampaignId);

	//TODO campaignManagerIds are hardcoded here,need a better way to do this.
	@Query(value = "{'status' : 'RUNNING', 'organizationId' : {$nin : ['XTAAS CALL CENTER']}, 'campaignManagerId' : {$in : ['demandshorecm','coledacm']} }", fields="{ _id : 1, 'name' : 1}")
	public List<Campaign> getConnectCampaigns();
	
	@Query(value = "{'status' : 'RUNNING', 'organizationId' : 'XTAAS CALL CENTER'}", fields="{ _id : 1, 'name' : 1, 'costPerLead' : 1}")
	public List<Campaign> getCampaignsForServiceModel();

	@Query(value = "{'status' : 'RUNNING','organizationId' : {$nin : ['XTAAS CALL CENTER']},'campaignManagerId' : {$in : ['lgsparmar','lgjjummancm']}}", fields="{ _id : 1, 'name' : 1})")
	public List<Campaign> getCampaignsForPlatFormModel();
	
	@Query(value = "{ 'name' : ?0, 'organizationId' : ?1}")
	public Campaign findByNameAndOrganizationId(String campaignName, String organizationId);
	
	@Query(value = "{ 'status' : 'RUNNING', 'organizationId' : ?0}")
	public List<Campaign> findByOrganizationId(String organizationId);

	@Query("{ 'status' : {'$in' : ?0 }, 'organizationId' :  {'$in' : ?1 } }")
	public List<Campaign> getActiveCampaignsByPartner(List<String>  status,List<String> partnerId);
	
	@Query("{'status': {'$in': ['RUNNING']},'organizationId' : ?0, 'startDate' : {$lte : ?2}, 'endDate' : {$gte : ?2}}}")
	public List<Campaign> getCampaignPerformanceList(String organizationId, Date fromDate, Date toDate);
}
