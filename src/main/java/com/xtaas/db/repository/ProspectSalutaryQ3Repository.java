package com.xtaas.db.repository;

import com.xtaas.db.entity.ProspectSalutaryQ3;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ProspectSalutaryQ3Repository extends MongoRepository<ProspectSalutaryQ3, String>, ProspectSalutaryQ3RepositoryCustom {

    @Query(value = "{'enrichedStatus' : {$exists : false}}")
    public List<ProspectSalutaryQ3> findSalutaryProspectsByPointer(Pageable pageable);

    @Query(value = "{}")
    public List<ProspectSalutaryQ3> findSalutaryProspectsPage( Pageable pageable);

    @Query(value = "{'companyMaster' : {$exists : false}}")
    public List<ProspectSalutaryQ3> findSalutaryProspectsDomainInMasterCompany(Pageable pageable);
}
