package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.ListProviderUsage;

public interface ListProviderUsageRepository extends MongoRepository<ListProviderUsage, String> {

	@Query("{ 'listPurchaseTransactionId' : ?0, 'purchaseStatus':'PURCHASE_SUMMARY'}")
	public ListProviderUsage findPurchaseSummaryByTransactionId(String listPurchaseTransactionId);
	
	@Query("{ 'campaignId' : ?0, 'purchaseStatus':'PURCHASE_SUMMARY'}")//by sort
	public List<ListProviderUsage> findPurchaseHitoryByCampaignId(String campaignId, Sort sort);
}
