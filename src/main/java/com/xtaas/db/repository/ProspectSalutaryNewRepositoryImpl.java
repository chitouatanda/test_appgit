package com.xtaas.db.repository;

import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectSalutaryNew;

public class ProspectSalutaryNewRepositoryImpl implements ProspectSalutaryNewRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public void bulkInsert(List<ProspectSalutaryNew> updateProspectSalutaryNew) {
		BulkOperations action = mongoTemplate.bulkOps(BulkMode.UNORDERED, ProspectSalutaryNew.class); 
		FindAndReplaceOptions options = FindAndReplaceOptions.options();
		for(ProspectSalutaryNew pNew : updateProspectSalutaryNew) {
			Prospect p = pNew.getProspectCall().getProspect();
			Criteria criteria = Criteria.where("prospectCall.prospect.firstName").is(p.getFirstName());
			criteria.and("prospectCall.prospect.lastName").is(p.getLastName());
			criteria.and("prospectCall.prospect.company").is(p.getCompany());
			criteria.and("prospectCall.prospect.title").is(p.getTitle());
			Query query = new Query();
			query.addCriteria(criteria);
			Document doc = new Document();
			mongoTemplate.getConverter().write(pNew,doc);
			action.replaceOne(query, doc, options.upsert());
		}
		action.execute();
	}

}
