package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.DataSufficiencyResult;

public interface DataSufficiencyResultRepository extends MongoRepository<DataSufficiencyResult, String> {

	
	
	
}
