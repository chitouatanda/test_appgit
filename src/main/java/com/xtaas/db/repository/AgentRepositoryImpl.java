package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.BasicDBObject;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;

public class AgentRepositoryImpl implements AgentRepositoryCustom {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<AgentSearchResult> searchAgents(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerHourPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimulAverageConversion, double minimumAverageQuality, int minimumAvailableHours,
			Pageable pageRequest) {
		return mongoTemplate.find(
				Query.query(
						buildBasicMatch(countries, states, cities, timeZones, languages, ratings, domain,
								maximumPerHourPrice, minimumCompletedCampaigns, minimumTotalVolume,
								minimulAverageConversion, minimumAverageQuality, minimumAvailableHours)).with(
						pageRequest), AgentSearchResult.class, "agent");
	}

	@Override
	public List<Object> countAgents(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerHourPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimulAverageConversion, double minimumAverageQuality, int minimumAvailableHours) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(Aggregation.match(buildBasicMatch(countries, states, cities, timeZones, languages,
				ratings, domain, maximumPerHourPrice, minimumCompletedCampaigns, minimumTotalVolume,
				minimulAverageConversion, minimumAverageQuality, minimumAvailableHours)));
		if (languages != null && languages.size() != 0) {
			aggregationOperations.add(Aggregation.unwind("agents.languages"));
		}
		aggregationOperations.add(buildExistsGroup(countries, states, cities, timeZones, languages));
		aggregationOperations.add(buildCounterGroup(countries, states, cities, timeZones, languages));
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"agent", Object.class);
		return results.getMappedResults();

	}

	private Criteria buildBasicMatch(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerHourPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimulAverageConversion, double minimumAverageQuality, int minimumAvailableHours) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("status").is(AgentStatus.ACTIVE.name());
		criterias.add(Criteria.where("partnerId").exists(false));

		if (countries != null && countries.size() != 0) {
			criterias.add(Criteria.where("address.country").in(countries));
		}
		if (states != null && states.size() != 0) {
			criterias.add(Criteria.where("address.state").in(states));
		}
		if (cities != null && cities.size() != 0) {
			criterias.add(Criteria.where("address.city").in(cities));
		}
		if (timeZones != null && timeZones.size() != 0) {
			criterias.add(Criteria.where("address.timeZone").in(timeZones));
		}
		if (languages != null && languages.size() != 0) {
			criterias.add(Criteria.where("languages").in(languages));
		}
		if (ratings != null && ratings.size() != 0) {
			criterias.add(Criteria.where("averageRating").in(ratings));
		}
		if (domain != null && !domain.isEmpty()) {
			criterias.add(Criteria.where("domains").in(domain));
		}
		if (maximumPerHourPrice > 0) {
			criterias.add(Criteria.where("perHourPrice").lte(maximumPerHourPrice));
		}
		if (minimumCompletedCampaigns > 0) {
			criterias.add(Criteria.where("totalCompletedCampaigns").gte(minimumCompletedCampaigns));
		}
		if (minimumTotalVolume > 0) {
			criterias.add(Criteria.where("totalVolume").gte(minimumTotalVolume));
		}
		if (minimulAverageConversion > 0) {
			criterias.add(Criteria.where("averageConversion").gte(minimulAverageConversion));
		}
		if (minimumAverageQuality > 0) {
			criterias.add(Criteria.where("averageQuality").gte(minimumAverageQuality));
		}
		if (minimumAvailableHours > 0) {
			criterias.add(Criteria.where("averageAvailableHours").gte(minimumAvailableHours));
		}
		if (criterias.size() == 0) {
			return criteria;
		} else {
			return criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]));
		}
	}

	private AggregationOperation buildExistsGroup(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages) {
		BasicDBObject counterGroup = new BasicDBObject("_id", "$_id");

		if (countries != null) {
			for (String country : countries) {
				counterGroup = counterGroup.append(String.format("countries_%s", country), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$address.country", country }), 1, 0 })));
			}
		}
		if (states != null) {
			for (String state : states) {
				counterGroup = counterGroup.append(String.format("states_%s", state), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$address.state", state }), 1, 0 })));
			}
		}
		if (cities != null) {
			for (String city : cities) {
				counterGroup = counterGroup.append(String.format("cities_%s", city), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$address.city", city }), 1, 0 })));
			}
		}
		if (timeZones != null) {
			for (String timeZone : timeZones) {
				counterGroup = counterGroup.append(String.format("timeZones_%s", timeZone), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "$address.timeZone", timeZone }), 1, 0 })));
			}
		}
		if (languages != null) {
			for (String language : languages) {
				counterGroup = counterGroup.append(String.format("languages_%s", language), new BasicDBObject("$max",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$eq", new Object[] { "languages", language }), 1, 0 })));
			}
		}
		return new CustomAggregationOperation(new Document("$group", counterGroup));
	}

	private AggregationOperation buildCounterGroup(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages) {
		BasicDBObject counterGroup = new BasicDBObject("_id", null);

		if (countries != null) {
			for (String country : countries) {
				counterGroup = counterGroup.append(String.format("countries_%s", country), new BasicDBObject("$sum",
						String.format("$countries_%s", country)));
			}
		}
		if (states != null) {
			for (String state : states) {
				counterGroup = counterGroup.append(String.format("states_%s", state),
						new BasicDBObject("$sum", String.format("$states_%s", state)));
			}
		}
		if (cities != null) {
			for (String city : cities) {
				counterGroup = counterGroup.append(String.format("cities_%s", city),
						new BasicDBObject("$sum", String.format("$cities_%s", city)));
			}
		}
		if (timeZones != null) {
			for (String timeZone : timeZones) {
				counterGroup = counterGroup.append(String.format("timeZones_%s", timeZone), new BasicDBObject("$sum",
						String.format("$timeZones_%s", timeZone)));
			}
		}
		if (languages != null) {
			for (String language : languages) {
				counterGroup = counterGroup.append(String.format("languages_%s", language), new BasicDBObject("$sum",
						String.format("$languages_%s", language)));
			}
		}
		return new CustomAggregationOperation(new Document("$group", counterGroup));
	}
}
