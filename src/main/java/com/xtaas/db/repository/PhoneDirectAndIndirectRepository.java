package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.PhoneDirectAndIndirect;

public interface PhoneDirectAndIndirectRepository extends MongoRepository<PhoneDirectAndIndirect,String> {
	@Query("{ $or : [{'number' : ?0}, {'local_format': ?0} ]}")
	public List<PhoneDirectAndIndirect> findByPhoneNumber(String phoneNumber);
}
