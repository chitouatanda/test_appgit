package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.xtaas.domain.entity.CampaignContact;

public class CampaignContactRepositoryImpl implements CampaignContactRepositoryCustom {

private static final Logger logger = LoggerFactory.getLogger(CampaignContactRepositoryImpl.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	/**
	 * Find out the organizationd which already exists for the campaign from the list specified as input.
	 * Example: db.campaigncontact.distinct("organizationName", {"organizationName" : {$in: ["The Bon-Ton Stores, Inc.", "Cabelas Incorporated", "A123"]} }) 
	 * @param organizationName
	 * @return
	 */
	public HashSet<String> findExistingOrganizations(String campaignId, List<String> organizationNames) {
		BasicDBObject match = new BasicDBObject("organizationName", new BasicDBObject("$in", organizationNames)).append("campaignId", campaignId);
		@SuppressWarnings("unchecked")
		DistinctIterable<String> iterable = mongoTemplate.getCollection("campaigncontact").distinct("organizationName", match, String.class); //returns json as {  "0" : "Cabelas Incorporated", "1" : "The Bon-Ton Stores, Inc."}
		MongoCursor<String> cursor = iterable.iterator();
		List<String> list = new ArrayList<>();
		while (cursor.hasNext()) {
			list.add(cursor.next());
		}
		System.out.println(list.size());
		return new HashSet<String>(list); 
	}
	
	//TEST METHOD -  to update title from database (implemented to fix a PROD issue on 25 Nov 2015)
	public boolean updateCampaignContactTitle(String email, String title) {
		Query query = new Query();
		query.addCriteria(Criteria.where("email").is(email));
		Update update = new Update();
		update.set("title", title);
		//set the audit trails fields
		update.set("updatedBy", "SYSTEM");
		update.set("updatedDate", new Date());
				
		UpdateResult wr = mongoTemplate.updateFirst(query, update, CampaignContact.class);
		
		if (wr.getModifiedCount() == 0) {
			logger.error("removeNonAsciiCharacters() : Error occurred while updating non-ascii company. email: " + email + " Error Msg: Modified Records: " + wr.getModifiedCount());
		}
		
		return (wr.getModifiedCount() != 0);  //returns TRUE if no error else False
	}

}
