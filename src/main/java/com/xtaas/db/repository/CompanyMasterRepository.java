package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CompanyMaster;


public interface CompanyMasterRepository extends MongoRepository<CompanyMaster, String> {

	@Query("{ 'companyId' : ?0 }")
	public List<CompanyMaster> findCompanyMasterListByCompanyId(String companyId);
	
	@Query(value="{'status' : 'FOUND'}")
	public List<CompanyMaster> findCompanyMasterListByPage(Pageable pageable);
	
	@Query("{ 'companyWebsite' : ?0 }")
	public List<CompanyMaster> findCompanyMasterListByCompanyWebsite(String companyWebsite);

}
