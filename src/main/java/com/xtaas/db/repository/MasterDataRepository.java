package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.db.entity.MasterData;

import org.springframework.data.repository.Repository;

public interface MasterDataRepository extends Repository<MasterData, String> {
	MasterData findOneByType(String type);
	List<MasterData> findAll();
}
