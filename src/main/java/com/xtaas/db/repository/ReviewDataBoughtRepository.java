package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ReviewDataBought;

public interface ReviewDataBoughtRepository extends MongoRepository<ReviewDataBought, String> {
	
	@Query("{ '_id' : ?0 }")
	public ReviewDataBought findOneById(String id);
}
