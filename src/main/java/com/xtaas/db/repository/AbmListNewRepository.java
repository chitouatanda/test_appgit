package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AbmListNew;

public interface AbmListNewRepository extends MongoRepository<AbmListNew, String>, AbmListNewRepositoryCustom {

	@Query("{ 'campaignId' : ?0 }")
	public List<AbmListNew> findABMListNewByCampaignId(String campaignId);

	@Query("{ 'campaignId' : ?0, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findABMListNewByCampaignIdAndStatus(String campaignId);

	@Query("{ 'campaignId' : ?0, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findABMListNewByCampaignIdAndStatusPage(String campaignId, Pageable pageable);

	@Query(value = "{ 'campaignId' : ?0, 'companyName' :  {$exists : true} , 'status' : 'ACTIVE' }", count = true)
	public int countABMListNewByCampaignCompanyName(String campaignId);

	@Query("{ 'campaignId' : ?0, 'companyName' :  {$exists : true} , 'status' : 'ACTIVE' }")
	public List<AbmListNew> pageABMListNewByCampaignCompanyName(String campaignId, Pageable pageable);

	@Query("{ 'campaignId' : ?0, 'companyName'  : ?1, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findAbmListNewByCampaignCompanyName(String campaignId, String company);

	@Query(value = "{ 'campaignId' : ?0, 'companyName'  : ?1, 'status' : 'ACTIVE' }", count = true)
	public long countByCampaignCompanyName(String campaignId, String company);

	@Query("{ 'campaignId' : ?0, 'companyId'  : ?1, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findAbmListNewByCampaignCompanyId(String campaignId, String companyId);

	@Query(value = "{ 'campaignId' : ?0 }", count = true)
	public int countABMListNewByCampaignIdAndStatus(String campaignId);

	@Query("{ 'campaignId' : ?0, 'companyName'  : {'$regex' : ?1, $options: 'i' }, 'status' : 'ACTIVE' }")
	public List<AbmListNew> searchAbmListNewByCampaignCompanyName(String campaignId, String companyName);

	@Query("{ 'campaignId' : ?0, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findABMListByPagination(String campaignId, Pageable pageable);

	@Query("{ 'campaignId' : ?0, , 'companyId'  : { $in: ['GET_COMPANY_ID','NOT_FOUND'] }'status' : 'ACTIVE' }")
	public List<AbmListNew> findABMListByPaginationPending(String campaignId, Pageable pageable);

	@Query("{ 'campaignId' : ?0, 'domain'  : ?1, 'status' : 'ACTIVE' }")
	public List<AbmListNew> findAbmListNewByCampaigndomain(String campaignId, String domain);

	@Query("{ 'campaignId' : ?0 }")
	public List<AbmListNew> findABMListNewByCampaignIdAndPage(String campaignId, Pageable pageable);

	@Query(value = "{ 'campaignId' : ?0 }", count = true)
	public int countABMListNewByCampaignIdAndStatusLeadIq(String campaignId);

}
