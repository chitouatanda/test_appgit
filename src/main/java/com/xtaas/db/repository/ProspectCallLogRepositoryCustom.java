package com.xtaas.db.repository;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;

import com.xtaas.application.service.QaMetricsServiceImpl.QaMetrics;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.DistinctCampaignIdsDTO;
import com.xtaas.domain.valueobject.Pivot;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.GlobalContactCriteriaDTO;
import com.xtaas.web.dto.HealthChecksDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.RecycleStatusDTO;
import com.xtaas.web.dto.TranscribeCriteriaDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;

public interface ProspectCallLogRepositoryCustom {
	public List<AgentSearchResult> getAgentCallStats(List<String> agentIds, Date fromDate, Date toDate);

	public double getAvgFeedbackScore(String campaignId, Date fromDate, Date toDate);

	/*
	 * public List<ProspectCallLog> searchProspectCallsByQa(List<String>
	 * campaignIds, ProspectCallSearchDTO prospectCallSearchDTO, Pageable
	 * pageRequest) throws ParseException;
	 */
	public List<ProspectCallLog> searchProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException;

	public List<ProspectCallLog> searchProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO,
			Pageable pageRequest) throws ParseException;

	public long countProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public int countProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException;

	boolean updateCallStatus(String prospectCallId, ProspectCallStatus status, String callSid, Date callbackDate,
			Integer callRetryCount, String allocatedAgentId);

	boolean updateProspectInteractionSessionId(String prospectCallId, String prospectInteractionSessionId);

	public int countProspectsContacted(String campaignId, Date fromDate, Date toDate);

	public int countProspectsDelivered(String campaignId, Date fromDate, Date toDate);

	public int countProspectsDialed(String campaignId, Date fromDate, Date toDate);

	public HashMap<QaMetrics, Integer> getQaMetricsByDispositionStatus(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public HashMap<QaMetrics, Integer> getQaMetricsByProspectCallStatus(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate(List<String> campaignIds);

	public boolean removeNonAsciiCharacters(String prospectCallId, String nonAsciiCompanyName);

	public HashSet<String> findCallableOrganizations(String campaignId, List<String> organizationNames);

	public HashMap<String, HashMap<String, Integer>> countRecycleRecords(Map<String, List<String>> stringClauses,
			String campaignId);

	public void saveRecycleRecords(Map<String, List<String>> stringClauses, String campaignId,
			List<RecycleStatusDTO> listDTOs);

	public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> agentIds,
			List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public int countProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public List<ProspectCallLog> searchProspectCallsBySecQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException;

	public long countProspectCallsBySecQa(List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException;

	public List<ProspectCallLog> getRingingProspects(List<String> campaignIds);
	
	public Criteria getCallableProspectsCriteria(Campaign campaign);

	public int findAllCallableRecordCount(Campaign campaign);
	
	public List<ProspectCallLog> findAllCallableRecords(Campaign campaign);
	
	public List<ProspectCallLog> findAllCallableRecordsWithLimit(Campaign campaign);

	public List<Pivot> generatePivotAggregationQuery(Campaign campaign, String fieldName, boolean isLimit);

	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuery(List<String> campaignIds,
			int duplicateCompanyDuration);
			
	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQueryQA(List<String> campaignIds,
			int duplicateCompanyDuration);
	
	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuerySecondaryQA(List<String> campaignIds,
			int duplicateCompanyDuration);
	
	public List<ProspectCallLog> getDistinctCampaignIds(List<String> agentIds, ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	
	public List<String> getDistinctPartnerIds(List<String> campaignIds);
	
	public int nukeCallableProspectByPivots(Campaign campaign, List<String> list, String checkPoint,String action);

	public List<ProspectCallLog> searchFailureProspectCallsByQa(List<String> agentIds, List<String> activeCampaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException;
	
	public List<ProspectCallLog> searchFailutreProspectCallsBySecQa(List<String> activeCampaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException;

	public List<String> getDistinctTeamIds(List<String> campaignIds);

	public List<ProspectCallLog> findByCampaignIdAndStatus(GlobalContactCriteriaDTO criteriaDTO);

	public int getbackCallableProspects(CallableGetbackCriteriaDTO criteriaDTO);

	public long getDepartmentHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getTitleHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getIndustryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getCountryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getSicCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getNaicsCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getUSStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getZipHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getEmployeeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public long getRevenueHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO);
	
	public int getNewlyBuyProspectsOfCampaign(String campaignId);
	
	public boolean updateInteractionData(List<String> campaignIds, List<String> emails, List<String> phoneNumbers);

	public List<DistinctCampaignIdsDTO> generateDistinctCampaignIdsAggregationQuery(String startDate, String endDate,
			List<String> agentIds) throws ParseException;

	public List<String> getDistinctCampaignIdsForFilter(List<String> agentIds,ProspectCallSearchDTO prospectCallSearchDTO);	
	
	public void bulkinsert(List<ProspectCallLog> prospectCallLogList);

	public void bulkUpdate(List<ProspectCallLog> prospectCallLogList);

	public List<ProspectCallLog> getProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria) throws ParseException;
	
	public long getCountOfProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria) throws ParseException;

}