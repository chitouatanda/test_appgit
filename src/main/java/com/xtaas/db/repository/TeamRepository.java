package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.Team;
import com.xtaas.web.dto.StatsDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/23/14
 * Time: 11:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TeamRepository extends MongoRepository<Team, String>, TeamRepositoryCustom {
	@Query("{ '_id' : ?0 }")
	public Team findOneById(String id);
	
	@Query("{ '_id' : { '$in' : ?0 }}")
	public List<TeamSearchResultDTO> findByIds(List<String> ids);
	
	@Query("{ 'supervisor.resourceId' : ?0 }")
	public List<Team> findBySupervisorId(String id);
	
	@Query("{ 'qas.resourceId' : ?0 }")
	public List<TeamSearchResultDTO> findByQaId(String id);
	
	@Query(value = "{ 'qas.resourceId' : ?0 }")
	public List<Team> findTeamsByQaId(String id);
		
	@Query("{'agents' : { $elemMatch : {'resourceId' : ?0, 'supervisorOverrideCampaignId' : {$exists : true}}}, 'status' : 'ACTIVE'}")
	public List<Team> findByAgentIdWithCampaignOverriden(String agentId); //It will always return 1 record as 1 Agent will be member of 1 team only. Returning List for future support
	
	@Query("{'agents.resourceId' : ?0 }")
	public Team findByAgent(String agentId);
	
	@Query("{'agents.resourceId' : ?0 , '_id' :{ $ne: ?1 } }")
	public Team findOtherTeamByAgent(String resourceId, String teamId);
	
	@Query("{'agents' : { $elemMatch : {'supervisorOverrideCampaignId' : ?0}}, 'supervisor.resourceId' : ?1, 'status' : 'ACTIVE'}")
	public List<Team> findTeamsTemporarilyOnCampaign(String campaignId, String supervisorId); //Return List<Team> whose agents are temporarily working on the given campaign
	
	@Query("{ $or: [{'supervisor.resourceId' : ?0}, {'qas.resourceId' : ?0}, {'agents.resourceId' : ?0}] }")
	public List<Team> findTeamsByUserId(String userId);
	
	@Query("{ 'supervisor.resourceId' : ?0 }")
    public Team findTeamBySupervisorId(String id);
	
	@Query("{ 'partnerId' : ?0 }")
    public Team findTeamByPartnerId(String id);
	
	@Query("{ 'partnerId' : ?0 }")
    public List<Team> findAllByPartnerId(String id);

	@Query("{'agents' : { $elemMatch : {'supervisorOverrideCampaignId' : ?0}}, 'status' : 'ACTIVE'}")
	public List<Team> findBySupervisorOverrideCampaignId(String campaignId);
	
	@Query("{'agents' : { $elemMatch : {'supervisorOverrideCampaignId' : { '$in' : ?0 }}}, 'status' : 'ACTIVE'}")
	public List<Team> findListBySupervisorOverrideCampaignId(List<String> campaignIds);

	@Query(value = "{'supervisor.resourceId': ?0}")
	public Team getAgents(String id);
	
	@Query("{ 'partnerId' : { '$in' : ?0 }}")
	public List<Team> findInternalAndPartnerTeams(List<String> teams);

	@Query("{ $or : [{'adminSupervisorIds' : ?0}, {'supervisor.resourceId': ?0} ]}")
	public List<Team> findByAdminSupervisorAndSupervisorId(String id);

	@Query("{ $or : [{'adminSupervisorIds' : {'$in': ?0}}, {'supervisor.resourceId': {'$in': ?0}} ]}")
	public List<Team> findByAdminSupervisorAndSupervisorId(List<String> supervisorIds);

	@Query("{'agents.resourceId' : ?0 }")
	public List<Team> findAllTeamsByAgentId(String agentId);
}
