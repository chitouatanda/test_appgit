package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Geography;
import com.xtaas.domain.valueobject.GeographyType;

public interface GeographyRepository extends RetrieveRepository<Geography, String> {
	@Query(value = "{}", fields = "{ 'states' : 0 }")
	public List<Geography> find();

	@Query(value = "{'countryCode' : { '$nin' : ?0 }}", fields = "{ 'states' : 0 }")
	public List<Geography> getByNegativeCountries(List<String> negativeCodes);

	@Query(value = "{'countryCode' : { '$in' : ?0 }}", fields = "{ 'states.cities' : 0 }")
	public List<Geography> getByCountryCodes(List<String> countryCodes);
	
	@Query(value = "{'countryName' : {'$regex' : ?0,$options:'i'}}", fields = "{ 'states.cities' : 0 }")
	public List<Geography> getByCountryNames(String countryName);

	@Query(value = "{'countryCode' : { '$in' : ?0 }, 'states': { '$elemMatch': { 'stateCode': { '$nin' : ?1 }}}}", fields = "{ 'states.cities' : 0 }")
	public List<Geography> getByNegativeStates(List<String> countryCodes, List<String> negativeCodes);

	@Query(value = "{'countryCode' : { '$in' : ?0 }, 'states': { '$elemMatch': { 'stateCode': { '$in' : ?1 }}}}")
	public List<Geography> getByStateCodes(List<String> countryCodes, List<String> stateCodes);
	
	@Query(value = "{'countryCode' : { '$in' : ?0 }, 'states': { '$elemMatch': { 'stateCode': { '$in' : ?1 }}, 'cities': {'$elemMatch': {'$nin' : ?2}}}}")
	public List<Geography> getByNegativeCities(List<String> countryCodes, List<String> stateCodes, List<String> negativeCodes);
	
	@Query(value = "{'locationType' : ?0 }")
	public List<Geography> findByType(GeographyType type);
}
