package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.QaFeedbackForm;

public interface QaFeedbackFormRepository  extends MongoRepository<QaFeedbackForm, String>{
	@Query("{ '_id' : ?0 }")
	public QaFeedbackForm findOneById(String id);

	@Query("{ 'campaignId' : ?0 }")
	public QaFeedbackForm  findOneByCampaignId(String campaignId);
}
