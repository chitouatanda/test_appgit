package com.xtaas.db.repository;

import com.xtaas.db.entity.ClassifierStatisticsLog;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * <p>
 */
public interface ClassifierStatisticsLogRepository extends MongoRepository<ClassifierStatisticsLog, String> {
}
