package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ProspectSalutary;
import com.xtaas.domain.entity.Campaign;

public interface ProspectSalutaryRepository extends MongoRepository<ProspectSalutary, String> {

	@Query(value = "{'prospectCall.campaignId' : ?0}")
	public List<ProspectSalutary> findSalutaryProspects(String campaignId, Pageable pageable);
	
	@Query(value = "{}")
	public List<ProspectSalutary> findSalutaryProspectsPage( Pageable pageable);
	
	@Query(value = "{'prospectCall.prospect.stateCode' : ?0}")
	public List<ProspectSalutary> findMissingStates( String stateCode, Pageable pageable);
	
	@Query(value = "{'prospectCall.prospect.stateCode' : ?0 }" , count = true)
	public int countByMissingState(String campaignId);
	
	@Query(value = "{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}", fields = "{'prospectCall.prospectCallId' : 1, 'prospectCall.prospect.phone' : 1, 'prospectCall.prospect.companyPhone' : 1}")
	public List<ProspectSalutary> findContactsTenByRetryCount(int retryCount,String campaignId);
	
	@Query(value = "{ 'prospectCall.callRetryCount' : ?0,  'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}", fields = "{'prospectCall.prospectCallId' : 1, 'prospectCall.prospect.phone' : 1, 'prospectCall.prospect.companyPhone' : 1}")
	public List<ProspectSalutary> findContactsByMaxRetryCount(int retryCount,String campaignId);
	
	@Query(value = "{'prospectCall.prospect.phone' : ?0 }")
	public List<ProspectSalutary> findByPhoneNumber(String phoneNumber);
	
	@Query(value = "{'tagDirectIndirect' : {$exists : false}}")
	public List<ProspectSalutary> findProspectByNotTagged(boolean notTagged, Pageable pageable);
	
	@Query(value = "{'prospectCall.prospect.customAttributes.domain' : ?0 }")
	public List<ProspectSalutary> findProspectByDomain(String domain);
	
	@Query(value = "{'enrichedStatus' : {$exists : false}}")
	public List<ProspectSalutary> findSalutaryProspectsByPointer( Pageable pageable);
}

