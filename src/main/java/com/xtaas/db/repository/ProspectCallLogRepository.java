package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author djain
 *
 */
public interface ProspectCallLogRepository extends MongoRepository<ProspectCallLog, String>, ProspectCallLogRepositoryCustom {
	@Query("{ '_id' : ?0 }")
	public ProspectCallLog findOneById(String id);

	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.agentId' : ?1, 'prospectCall.dispositionStatus' :  {'$in': ['CALLBACK', 'CALLBACK_USER_GENERATED']}, 'callbackDate' : {'$eq': ?2}}" , count = true)
	public int getCountAlreadyScheduledCallbackOfAgent(String campaignId, String agentId, Date callbackDate);
	
	@Query("{ 'prospectCall.campaignId' : ?0 , 'prospectCall.prospect.firstName' : ?1,'prospectCall.prospect.lastName' : ?2}")
	public List<ProspectCallLog> getEncryptProspectByName(String campaignId, String fname, String lastname);

	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public ProspectCallLog findByProspectCallId(String prospectCallId);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public List<ProspectCallLog> findAllByProspectCallId(String prospectCallId, Pageable pageable);

	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public List<ProspectCallLog> findByProspectCallIds(List<String> prospectCallId);

	@Query(value = "{'prospectCall.campaignId': ?0,'prospectCall.dispositionStatus' : 'SUCCESS'}")
	public List<ProspectCallLog> getLeadCount(String campaignId);


	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.email' : ?1 }")
	public ProspectCallLog getProspectByCIDAndEmail(String campaignId,String email);

	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospectCallId' : ?1 }")
	public ProspectCallLog getProspectByCIDAndPCID(String campaignId,String prospectCallId);

	@Query("{ 'prospectCall.twilioCallSid' : ?0 }")
	public ProspectCallLog findByTwilioCallId(String twilioCallSid);

	@Query("{ 'prospectCall.twilioCallSid' : ?0 }")
	public List<ProspectCallLog> findMultipleByTwilioCallId(String twilioCallSid);

	@Query(value = "{'prospectCall.campaignId' : ?0 }" , count = true)
	public int countByCampaignId(String campaignId);

	@Query("{'prospectCall.campaignId' : ?0 }")
	public List<ProspectCallLog> getByCampaignId(String campaignId);

	@Query("{ 'status' : 'QUEUED', 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1} }")
	public List<ProspectCallLog> findQueuedByCampaignId(String campaignId, List<String> excludeStateCodes, Pageable pageable);

	@Query("{ 'callbackDate' : {$lte : ?1}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }")
	public List<ProspectCallLog> findByExpiredCallback(String campaignId, Date currentDateTime,  List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date diffDate, Pageable pageable);
	
	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, 'prospectCall.dispositionStatus' :  {'$in': ['CALLBACK', 'CALLBACK_USER_GENERATED']}, 'callbackDate' : {$gte : ?2, $lte : ?3}} ")
    public List<ProspectCallLog> getTodayCallbackProspects(String campaignId, List<String> excludeStateCodes,Date fromDate, Date toDate, Sort sort);
	
	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, 'prospectCall.dispositionStatus' :  {'$in': ['CALLBACK', 'CALLBACK_USER_GENERATED']}, 'callbackDate' : {$lte : ?2}} ")
    public List<ProspectCallLog> getMissedCallbackProspects(String campaignId, List<String> excludeStateCodes, Date toDate, Sort sort);

	@Query("{ 'callbackDate' : {$lte : ?1}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }")
	public List<ProspectCallLog> findByExpiredCallbackWithTimeZone(String campaignId, Date currentDateTime, List<String> openTimeZones, Long maxDailyCallRetryCount, Date diffDate, Pageable pageable);

	@Query(value = "{ 'callbackDate' : {$lte : ?1}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }", count = true)
	public int countByExpiredCallback(String campaignId, Date currentDateTime, List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date diffDate);

	@Query("{ 'prospectCall.dispositionStatus' : 'CALLBACK', 'callbackDate' : {$lte : ?0}, 'prospectCall.campaignId' : ?1, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }")
	public List<ProspectCallLog> findByExpiredCallbackOld(Date currentDateTime, String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date diffDate, Pageable pageable);

	@Query(value = "{ 'prospectCall.dispositionStatus' : 'CALLBACK', 'callbackDate' : {$lte : ?0}, 'prospectCall.campaignId' : ?1, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }", count = true)
	public int countByExpiredCallbackOld(Date currentDateTime, String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date diffDate);
	
	@Query(value = "{ 'callbackDate' : {$lte : ?1}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$in': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 },'updatedDate' : {$lte: ?4}}] }", count = true)
	public int countByExpiredCallbackWithTimeZone(String campaignId, Date currentDateTime, List<String> openTimeZones, Long maxDailyCallRetryCount, Date diffDate);

	@Query("{ 'status' : 'CALL_TIME_RESTRICTION', 'callbackDate' : {$lte : ?0}, 'prospectCall.campaignId' : ?1 }")
	public List<ProspectCallLog> findByCallTimeRestrictionAndExpiredCallback(Date currentDateTime, String campaignId, Pageable pageable);

	@Query("{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?4}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN','AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}}")
	public List<ProspectCallLog> findProspectsForDialer(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date diffDate, Pageable pageable);
	
	@Query("{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?4}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN','AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$in': ?1}}")
	public List<ProspectCallLog> findProspectsForDialerWithTimeZone(String campaignId, List<String> openTimeZones, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date diffDate, Pageable pageable);

	@Query("{ 'status' : 'QUEUED', 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}}")
	public List<ProspectCallLog> findQueuedProspectsForDialer(String campaignId, List<String> excludeStateCodes, Pageable pageable);

	@Query(" { $and :  [{$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 }}]}, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN','AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}}")
	public List<ProspectCallLog> findRetryProspectsForDialer(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, Pageable pageable);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : 'DIALERCODE'}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 }, 'updatedDate' : { $lte: ?4 } }] },  {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] } ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}}", count = true)
	public int countProspectsForDialer(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date diffDate);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : 'DIALERCODE'}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 }, 'updatedDate' : { $lte: ?4 } }] },  {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] } ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$in': ?1}}", count = true)
	public int countProspectsForDialerWithTimeZone(String campaignId, List<String> openTimeZones, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date diffDate);

	@Query("{ 'status' : { $in : ['WRAPUP_COMPLETE', 'CALL_COMPLETE']}, 'prospectCall.prospect.phone' : {'$regex' : ?0, $options : 's' } }")
	public List<ProspectCallLog> findByPhoneNumber(String phoneNumberRegex, Pageable pageable); //PhoneNumber regex: /503.*680.*3067$/

	//TEST METHOD -  to remove ascii chars from database (implemented to fix a PROD issue on 25 Nov 2015)
	@Query(value = "{'prospectCall.campaignId' : ?0, 'createdDate' : {$gte : ?1}}")
	public List<ProspectCallLog> findRecentlyUploadedList(String campaignId, Date date);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : 'DIALERCODE'}, {'status' : 'QUEUED'}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?0 }}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?1 }}] }  ], 'prospectCall.prospect.phone' : ?2,  'prospectCall.prospect.company' : ?3 }")
	public List<ProspectCallLog> findIndirectProspects(Long maxDailyCallRetryCount, int maxCallRetryLimit, String phone, String company);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : 'DIALERCODE'}, {'status' : 'QUEUED'}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?0 }}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?1 }}] }  ], 'prospectCall.prospect.phone' : ?2,  'prospectCall.prospect.sourceCompanyId' : ?3, 'prospectCall.campaignId' : ?4, 'prospectCall.prospect.stateCode' : {'$nin': ?5} }")
	public List<ProspectCallLog> findIndirectProspects(Long maxDailyCallRetryCount, int maxCallRetryLimit, String phone, String sourceCompanyId, String campaignId, List<String> excludeStateCodes);

	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.dispositionStatus' : 'SUCCESS'}}" , count = true)
	public int getSuccessCount(String campaignId);

	@Query(value = "{ 'prospectCall.campaignId' : ?0, 'prospectCall.callRetryCount' : {$gte : ?1}, 'prospectCall.dispositionStatus' : {$in : ['DIALERCODE', 'CALLBACK']}, 'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED']} }")
	public List<ProspectCallLog> findByMaxCallRetryLimit(String campaignId, int maxCallRetryLimit, Pageable pageable);

	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$in': ?1}, $or : [{'prospectCall.dispositionStatus' : 'DIALERCODE'}, {'status' : 'QUEUED'}]  }")
	public List<ProspectCallLog> findCallableProspectsByStateCodes(String campaignId, List<String> stateCodes, Pageable pageable);

	//Returns the list of Organizations for whose new Contacts must not be purchased. Don't purchase the contacts of Orgs who has status as QUEUED OR dispositionStatus as SUCCESS,CALLBACK OR who are being retried (basically the ones NOT with DIALERMISDETECT and ANSWERMACHINE)
	@Query(value ="{ $and : [ {'prospectCall.campaignId' : ?0}, {$or : [  {'status' : {$in : ['QUEUED']} },{'prospectCall.dispositionStatus' : {$in : ['SUCCESS','CALLBACK']}} ,  {$and :[{'prospectCall.dispositionStatus' : 'DIALERCODE'},  {'prospectCall.subStatus': {$nin : ['DIALERMISDETECT', 'ANSWERMACHINE']}}]} ]}]  }")
	public List<ProspectCallLog> findOrganizationsExcludedFromDataPurchase(String campaignId);

	@Query("{  'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : '580568a9e4b0de4709a31375'}")
	public List<ProspectCallLog> findContactsByCustomQuery(Date startDate, Date endDate);

	@Query("{ 'prospectCall.campaignId' : ?0 }")
    public List<ProspectCallLog> findAllRecordByCampaignId(String campaignId);

	@Query("{'prospectCall.prospect.source' : 'ZOOMINFO', 'prospectCall.campaignId' : '5880b9aee4b0bd66e369c2ba'}")
    public List<ProspectCallLog> findAllRecordByZoom();
	@Query("{'prospectCall.prospect.company' : ?0, 'prospectCall.prospect.source' : 'ZOOMINFO', 'prospectCall.campaignId' : '5880b9aee4b0bd66e369c2ba'}")
    public List<ProspectCallLog> findAllRecordByCompanyZoom(String companyName);

	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.title' : ?3, 'prospectCall.prospect.company' : ?4 }")
    public List<ProspectCallLog> findUinqueRecord(String campaignId,String firstName, String lastName,  String title, String company);


	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.title' : ?3, 'prospectCall.prospect.company' : ?4 }" , count = true)
	public int findUniqueProspectRecord(String campaignId,String firstName, String lastName,  String title, String company);


	@Query(value = "{'prospectCall.campaignId' : {'$in': ?0}, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.title' : ?3, 'prospectCall.prospect.company' : ?4 }" , count = true)
	public int findUniqueProspectRecordGroup(List<String> campaignIds,String firstName, String lastName,  String title, String company);


	@Query("{'prospectCall.prospect.company' : ?0}")
    public List<ProspectCallLog> findTopLevelIndustries(String companyName);

	@Query("{'status' : ?0 }")
	public List<ProspectCallLog> findByStatus(String status);

	@Query(value = "{'qaFeedback.qaId': ?0, 'status': ?1}", count = true)
	public int findByQaIdAndStatus(String qaId, String status);

	@Query("{'prospectCall.campaignId' : ?0, 'status' : {'$in': ?1}, 'updatedDate' : {$gte : ?2} }")
	public List<ProspectCallLog> findByCampaignIdAndStatus(String campaignId, List<String> status, Date date);

	@Query(value = "{'prospectCall.campaignId' : {$in : ?0}, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.title' : ?3, 'prospectCall.prospect.company' : ?4 }" , count = true)
	public int findProspectCountbyRunningCampaigns(List<String> campaignIds,String firstName, String lastName,  String title, String company);

	@Query(value = "{'status' : 'QUEUED', 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}}" ,  count = true)
	public int getCountOfQueuedCallables(String campaignId,List<String> excludeStateCodes);

	@Query(value = "{ 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.campaignId' : ?1, 'status':{$in:['SECONDARY_QA_INITIATED','QA_INITIATED','QA_WRAPUP_COMPLETE','WRAPUP_COMPLETE','CALL_MACHINE_ANSWERED','CALL_ABANDONED','CALL_DIALED','CALL_COMPLETE','CALL_NO_ANSWER','CALL_INPROGRESS','MAX_RETRY_LIMIT_REACHED','LOCAL_DNC_ERROR','CALL_FAILED','CALL_TIME_RESTRICTION','CALL_BUSY','CALL_CANCELED','QA_COMPLETE']}, 'updatedDate' : {$gte : ?1, $lte : ?2}  }", count = true)
	public int getCountOfRecordsAttempted(String campaignId, Date startDate, Date endDate);

	@Query(value = "{ 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.campaignId' : ?1, 'status':{$in:['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE','SECONDARY_QA_INITIATED','QA_INITIATED','QA_COMPLETE']},'prospectCall.subStatus':{$in:['NOTINTERESTED','DEADAIR','OTHER','GATEKEEPER_ANSWERMACHINE','SUBMITLEAD','PROSPECT_UNREACHABLE','NOANSWER','DNCL','NO_CONSENT','FAILED_QUALIFICATION','ABANDONED','OUT_OF_COUNTRY','BUSY','DNRM','CALLBACK_SYSTEM_GENERATED','CALLBACK_GATEKEEPER','CALLBACK_USER_GENERATED']}, 'updatedDate' : {$gte : ?1, $lte : ?2}  }", count = true)
	public int getCountOfRecordsContacted(String campaignId, Date startDate, Date endDate);

	@Query(value = "{ 'prospectCall.dispositionStatus' : {$in :['SUCCESS','FAILURE','CALLBACK','DIALERCODE']}, 'prospectCall.campaignId' : ?1, 'prospectCall.campaignId' : ?1,'status':{$in:['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE','SECONDARY_QA_INITIATED','QA_INITIATED','QA_COMPLETE']},'prospectCall.subStatus':{$in:['ABANDONED']}, 'updatedDate' : {$gte : ?1, $lte : ?2}  }", count = true)
	public int getCountOfRecordsAbandoned(String campaignId, Date startDate, Date endDate);

	@Query("{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : {$in :['CALLBACK','DIALERCODE']}, 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}")
	public List<ProspectCallLog> findContactsTenByRetryCount(int retryCount,String campaignId);

	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ['58b895c1e4b0eeaa7f1fa0dc','58b9a097e4b03fbadf1b6187','5880e27ce4b03eb7948227d0','58e3fda9e4b0e5dd7d009778','58e3ef23e4b0e5dd7d008268','58e3fc41e4b0e5dd7d00964b','5880b82de4b0bd66e369c2b8','58b40f9ce4b06793d841c5da', '58c0811ce4b04357e217df0f','58c93ba5e4b0d62ae5ce2d7b','590c9aaae4b017560da2fdf3','590c9b36e4b017560da2fe2b','5938e874e4b059d446a171ab','5938ee72e4b059d446a171b4','5938e100e4b059d446a17198','5938f0d1e4b059d446a171b9', '594a5c53e4b0c806cd1825d4','594be4fde4b0942c43051a79','594a5bc4e4b0c806cd1825d1','594bea99e4b0942c43052563']}}")
	@Query("{ 'status' : 'QUEUED', 'prospectCall.campaignId' : ?0}")
	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ['594a5c53e4b0c806cd1825d4','594be4fde4b0942c43051a79','594a5bc4e4b0c806cd1825d1','594bea99e4b0942c43052563','596dc3a1e4b0a4813ada2069','596dc4c6e4b0a4813ada206a','596dc5f6e4b0a4813ada206b','596dc65fe4b0a4813ada206c']}}")
	public List<ProspectCallLog> findContactsTenByQueued(String campaignId);

	@Query("{ ''prospectCall.callRetryCount' : 222222, 'prospectCall.campaignId' : ?0, 'prospectCall.dailyCallRetryCount' : {$exists : false} }")
	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ['594a5c53e4b0c806cd1825d4','594be4fde4b0942c43051a79','594a5bc4e4b0c806cd1825d1','594bea99e4b0942c43052563','596dc3a1e4b0a4813ada2069','596dc4c6e4b0a4813ada206a','596dc5f6e4b0a4813ada206b','596dc65fe4b0a4813ada206c']}}")
	public List<ProspectCallLog> findContactsTenBy222222(String campaignId);

	@Query("{ ''prospectCall.callRetryCount' : 222333, 'prospectCall.campaignId' : ?0, 'prospectCall.dailyCallRetryCount' : {$exists : false} }")
	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ['594a5c53e4b0c806cd1825d4','594be4fde4b0942c43051a79','594a5bc4e4b0c806cd1825d1','594bea99e4b0942c43052563','596dc3a1e4b0a4813ada2069','596dc4c6e4b0a4813ada206a','596dc5f6e4b0a4813ada206b','596dc65fe4b0a4813ada206c']}}")
	public List<ProspectCallLog> findContactsTenBy222333(String campaignId);

	@Query("{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}")
	public List<ProspectCallLog> findContactsByRetryCount(int retryCount,String campaignId );

	@Query("{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}")
	public List<ProspectCallLog> findContactsByRetryCountByCampaigs(int retryCount,List<String> campaignIds );

	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ['58b895c1e4b0eeaa7f1fa0dc','58b9a097e4b03fbadf1b6187','5880e27ce4b03eb7948227d0','58e3fda9e4b0e5dd7d009778','58e3ef23e4b0e5dd7d008268','58e3fc41e4b0e5dd7d00964b','5880b82de4b0bd66e369c2b8','58b40f9ce4b06793d841c5da', '58c0811ce4b04357e217df0f','58c93ba5e4b0d62ae5ce2d7b','590c9aaae4b017560da2fdf3','590c9b36e4b017560da2fe2b','5938e874e4b059d446a171ab','5938ee72e4b059d446a171b4','5938e100e4b059d446a17198','5938f0d1e4b059d446a171b9', '594a5c53e4b0c806cd1825d4','594be4fde4b0942c43051a79','594a5bc4e4b0c806cd1825d1','594bea99e4b0942c43052563']}}")
	//@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : [?0]}}")
	@Query("{ 'prospectCall.callRetryCount' : { $in: [0, 786, 886] }, 'status' : {$in : ['QUEUED', 'MAX_RETRY_LIMIT_REACHED']} , 'prospectCall.campaignId' : ?0}")
	public List<ProspectCallLog> findContactsByQueued(String campaignId);

	@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in :  ?0}}")
	public List<ProspectCallLog> findContactsByQueuedByCampaigns(List<String> campaignIds);
	
	@Query("{ 'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.directPhone' : ?1}")
	public List<ProspectCallLog> findContactsByQueuedDirect(String campaignId,boolean direct);
	
	@Query("{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1,'prospectCall.prospect.directPhone' : ?2}")
	public List<ProspectCallLog> findContactsByRetryCountDirect(int retryCount,String campaignId, boolean direct );
	

	@Query("{ 'prospectCall.dispositionStatus' : 'SUCCESS', 'prospectCall.leadStatus' : {$nin : ['REJECTED', 'QA_REJECTED']}, 'prospectCall.campaignId' : ?0, 'prospectCall.callStartTime' : {$gte : ?1}}")
	public List<ProspectCallLog> findContactsBySuccess(String campaignId, Date startDate);

	@Query("{ 'prospectCall.dispositionStatus' : 'SUCCESS', 'prospectCall.leadStatus' : {$nin : ['REJECTED', 'QA_REJECTED']}, 'prospectCall.campaignId' : {$in :  ?0}, 'prospectCall.callStartTime' : {$gte : ?1}}")
	public List<ProspectCallLog> findContactsBySuccessByCampaigns(List<String> campaignIds, Date startDate);

	@Query("{ 'prospectCall.twilioCallSid' : ?0 }")
	public List<ProspectCallLog> findAllByTwilioCallId(String twilioCallSid);

//	@Query(value = "{ 'prospectCall.campaignId' : {$in : ?0}, 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, $or : [{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','CALLBACK']}, 'prospectCall.callRetryCount' : {$in : [1,2,3,4,5,6]}}, {'status' : 'QUEUED'}]}", count = true)
//	public int findAllCallableRecordCount(List<String> campaignIds);

	@Query("{ 'prospectCall.campaignId': ?0, 'prospectCall.callStartTime' : {$gte: ?1, $lte: ?2} }")
	public List<ProspectCallLog> findAllByCallStartTime(String campaignId, Date startDate, Date endDate);

	@Query("{ 'prospectCall.dispositionStatus' : 'SUCCESS', 'prospectCall.callStartTime' : {$gte: ?0} }")
	public List<ProspectCallLog> findSuccessByDate( Date startDate);

	@Query(value = "{'prospectCall.dispositionStatus':{$in : ?0}, 'prospectCall.agentId': {$in : ?1}, 'status': {$in : ?2} }", count = true)
	public int findByAgentIdAndStatus(List<String> dispositionStatuses, List<String> agentIds, List<String> statuses);

	@Query(value = "{'prospectCall.dispositionStatus':{$in : ?0}, 'prospectCall.agentId': {$in : ?1}, 'prospectCall.leadStatus': {$in : ?2} }", count = true)
	public int findByAgentIdsAndLeadStatuses(List<String> dispositionStatuses, List<String> agentIds, List<String> leadStatuses);

	@Query("{ 'prospectCall.campaignId' : ?0,'prospectCall.dispositionStatus' : {$nin :['SUCCESS']},'prospectCall.leadStatus':{$nin:[{'$regex' : 'ACCEPTED', $options : 'i' },{'$regex' : 'REJECTED', $options : 'i' },{'$regex' : 'SCORED', $options : 'i' }]},'status' : {$nin : [{'$regex' : 'DNC', $options : 'i' }]},'prospectCall.subStatus' : {$nin : ['DNCL','DNRM','NOTINTERESTED','NO_CONSENT','PROSPECT_UNREACHABLE','FAILED_QUALIFICATION','SUBMITLEAD','OUT_OF_COUNTRY']},'prospectCall.callRetryCount' : {$in : [222222]}}")
	public List<ProspectCallLog> findReplinishData(String campaignId );

	@Query("{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 }}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, $or : [{'updatedDate' : {$lte: ?4}}, {'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?5 }}] }")
	public List<ProspectCallLog> findProspectsForDialerAfterFirstRetry(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date lastUpdatedDate, Long prospectDailyCallRetryCount, Pageable pageable);

	@Query("{ 'callbackDate' : {$lte : ?0}, 'prospectCall.campaignId' : ?1, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 }}], $or : [{'updatedDate' : {$lte: ?4}}, {'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?5 }}] }")
	public List<ProspectCallLog> findByExpiredCallbackAfterFirstRetry(Date currentDateTime, String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date lastUpdatedDate, Long prospectDailyCallRetryCount, Pageable pageable);

	@Query("{ 'prospectCall.campaignId': ?0, 'updatedDate' : {$gte: ?1, $lte: ?2} }")
	public List<ProspectCallLog> findAllByUpdatedDate(String campaignId, Date startDate, Date endDate);

	@Query("{ 'prospectCall.campaignId': ?0, 'updatedDate' : {$gte: ?1, $lte: ?2}, 'status': 'CALL_DIALED'}")
	public List<ProspectCallLog> findAllCallDialedProspectsOfCampaign(String campaignId, Date startDate, Date endDate);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 }}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, $or : [{'updatedDate' : {$lte: ?4}}, {'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?5 }}] }", count = true)
	public int countProspectsForDialerAfterFirstRetry(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, Date lastUpdatedDate, Long prospectDailyCallRetryCount);

	@Query(value = "{ 'callbackDate' : {$lte : ?0}, 'prospectCall.campaignId' : ?1, 'prospectCall.prospect.stateCode' : {'$nin': ?2}, $or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?3 }}], $or : [{'updatedDate' : {$lte: ?4}}, {'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?5 }}] }", count = true)
	public int countByExpiredCallbackAfterFirstRetry(Date currentDateTime, String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, Date lastUpdatedDate, Long prospectDailyCallRetryCount);

	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.twilioCallSid' : ?1 }")
	public List<ProspectCallLog> findByCampaignIdAndTwilioCallId(String campaignId, String twilioCallSid);

	@Query(value = "{ 'prospectCall.campaignId' : ?0,  'prospectCall.prospect.firstName' : ?1 , 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.phone' : ?3}", count=true)
	public int findManualProspectRecordCount(String campaignId,String firstName, String lastName, String phoneNumber);

	@Query("{ 'prospectCall.campaignId' : ?0,  'prospectCall.prospect.firstName' : ?1 , 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.phone' : ?3}")
	public ProspectCallLog findManualProspectRecord(String campaignId,String firstName, String lastName, String phoneNumber);

	@Query("{'prospectCall.campaignId' : ?0,'prospectCall.dispositionStatus' : {$nin :['SUCCESS']},'prospectCall.leadStatus':{$nin:[{'$regex' : 'ACCEPTED', $options : 'i' },{'$regex' : 'REJECTED', $options : 'i' },{'$regex' : 'SCORED', $options : 'i' }]},'status' : {$in : ['MAX_RETRY_LIMIT_REACHED']},'prospectCall.subStatus' : {$nin : ['DNCL','DNRM','NOTINTERESTED','NO_CONSENT','PROSPECT_UNREACHABLE','FAILED_QUALIFICATION','SUBMITLEAD','OUT_OF_COUNTRY']},'prospectCall.callRetryCount' : {$gte : ?1,$lte : ?2}}")
	public List<ProspectCallLog> getBackProspectsFromMaxRetries(String campaignId,int callRetryCount,int newRetryCount);


	@Query("{'prospectCall.leadStatus': 'QA_REJECTED','prospectCall.dispositionStatus': 'SUCCESS','prospectCall.callStartTime':{$gte: ?0}}")
	public List<ProspectCallLog> findByNotInABM(Date date);

	@Query("{'prospectCall.campaignId' : ?0, 'prospectCall.prospect.sourceId' : ?1}")
	public List<ProspectCallLog> getProspectByPersonId(String campaignId,String personId);

	@Query(value = "{'prospectCall.campaignId' : {'$in': ?0}, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.title' : ?3, 'prospectCall.prospect.company' : ?4 }" , count = true)
	public int findUniqueProspectRecord(List<String> campaignId,String firstName, String lastName, String title, String company);

	@Query("{ 'prospectCall.campaignId' : ?0 }")
    public List<ProspectCallLog> findByCampaignIdPageable(String campaignId, Pageable pageable);

	@Query("{'prospectCall.partnerId':?0, 'prospectCall.dispositionStatus' : ?1, 'prospectCall.subStatus' : ?2, 'updatedDate' : {$gte : ?3, $lte: ?4}}")
	public List<ProspectCallLog> getProspectsByPartnerIdAndDispositionStatus(String partnerId, String dispositionStatus, String subStatus, Date startDate, Date endDate);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?5}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] } ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, 'dataSlice' : {'$in': ?4}}", count = true)
    public long countProspectsForDialerBySlice(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, List<String> slices, Date diffDate);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?5}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$in': ?1}, 'dataSlice' : ?4}", count = true)
	public long countProspectsForDialerBySliceWithTimeZone(String campaignId, List<String> openTimeZones, Long maxDailyCallRetryCount, int maxCallRetryLimit, String slice, Date diffDate);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?5}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.stateCode' : {'$nin': ?1}, 'dataSlice' : {'$in': ?4}}")
    public List<ProspectCallLog> findProspectsForDialerBySlice(String campaignId, List<String> excludeStateCodes, Long maxDailyCallRetryCount, int maxCallRetryLimit, List<String> slices, Date diffDate, Pageable pageable);

	@Query(value = "{ $and : [{$or : [{'prospectCall.dispositionStatus' : { $in: ['DIALERCODE']}}, {'status' : { $in: ['QUEUED', 'CALL_ABANDONED']}}]}, {$or : [{'prospectCall.dailyCallRetryCount' : {$exists : false}}, {'prospectCall.dailyCallRetryCount' : {'$lt' : ?2 },'updatedDate' : {$lte: ?5}}] }, {$or : [{'prospectCall.callRetryCount' : {$exists : false}}, {'prospectCall.callRetryCount' : {'$lte' : ?3 }}] }  ], 'prospectCall.subStatus' : {'$nin' : ['GATEKEEPER_ANSWERMACHINE', 'UNKNOWN', 'AUTO']}, 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.timeZone' : {'$in': ?1}, 'dataSlice' : ?4}")
	public List<ProspectCallLog> findProspectsForDialerBySliceWithTimeZone(String campaignId, List<String> openTimeZones, Long maxDailyCallRetryCount, int maxCallRetryLimit, String slice, Date diffDate, Pageable pageable);

	@Query("{ 'prospectCall.contactId' : ?0 }")
	public ProspectCallLog findByProspectContactId(String contactId);

	@Query("{'prospectCall.campaignId' : ?0, 'prospectCall.prospect.firstName' : ?1, 'prospectCall.prospect.lastName' : ?2, 'prospectCall.prospect.phone' : ?3}")
	public ProspectCallLog findProspectForLeadTag(String campaignId, String firstName, String lastName, String phone);

	@Query(value = "{ 'prospectCall.dispositionStatus':'SUCCESS', 'prospectCall.leadStatus' : 'NOT_SCORED', 'prospectCall.partnerId': ?0,'prospectCall.callStartTime' : {$gte : ?1, $lt : ?2} }", count = true)
	public int getPartnerNotScoredLeadsCount(String partnerId, Date startDate, Date endDate);

	@Query(value = "{ 'prospectCall.dispositionStatus':'SUCCESS', 'prospectCall.leadStatus' : 'QA_RETURNED', 'prospectCall.partnerId': ?0,'prospectCall.callStartTime' : {$gte : ?1, $lt : ?2} }", count = true)
	public int getPartnerQaReturnedLeadsCount(String partnerId, Date startDate, Date endDate);

	//@Query("{  'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ?0}}}")
	//public List<ProspectCallLog> findContactsByQueuedList(List<String> campaignId);

	@Query("{ 'prospectCall.callRetryCount' : ?0,  'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?1}")
	public List<ProspectCallLog> findContactsByMaxRetryCount(int retryCount,String campaignId);

	@Query("{'prospectCall.campaignId' : ?0, 'prospectCall.dispositionStatus' : {$nin :['SUCCESS']},'prospectCall.leadStatus':{$nin:[{'$regex' : 'REJECTED', $options : 'i' },{'$regex' : 'ACCEPTED', $options : 'i' },{'$regex' : 'SCORED', $options : 'i' }]},'status': {$nin : [{'$regex' : 'DNC', $options : 'i' }]},'prospectCall.callRetryCount' : {$nin : [0,1,2,3,4,5,6,7,8,9,10,786,886,444444]}}")
	public List<ProspectCallLog> findNonCallableToMove(String campaignId);

	/*@Query("{'prospectCall.campaignId' : ?0, 'status' : {'$in': ?1}}")
	public List<ProspectCallLog> findByCampaignIdAndStatusForColdEmail(String campaignId, List<String> status);
	*/
	@Query("{'prospectCall.campaignId' : ?0, 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, $or : [{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','CALLBACK']},'prospectCall.callRetryCount' : {$in : [1,2,3,4,5,6,7,8,9]}},{'status' : 'QUEUED'}]}")
	public List<ProspectCallLog> findProspectsForColdEmail(String campaignId);


	@Query("{'prospectCall.campaignId' : ?0, 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']},'status' : 'QUEUED'}")
	public List<ProspectCallLog> findQueuedProspectsForColdEmail(String campaignId);

	@Query("{ 'prospectCall.campaignId' : {$in : ?0},'prospectCall.prospect.directPhone' : true, 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']},$or : [{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','CALLBACK']},'prospectCall.callRetryCount' : {$in : [1,2,3,4,5,6,7,8,9,10,786,886]}}, {'status' : 'QUEUED'}] }")
	public List<ProspectCallLog> findByDirectNumbers(List<String> campaignIds);

	@Query("{'prospectCall.prospect.phone' : ?0 }")
	public List<ProspectCallLog> findByPhoneNumber(String phoneNumber);

	@Query("{  'prospectCall.callRetryCount' : 0, 'status' : 'QUEUED', 'prospectCall.campaignId' : {$in : ?0}}}")
	public List<ProspectCallLog> findContactsByQueuedList(List<String> campaignId);

	@Query("{  'prospectCall.callRetryCount' : ?0, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId': {$in : ?1}}")
	public List<ProspectCallLog> findContactsByRetryCountList(int retryCount, List<String> campaignId);

	@Query(value = "{'prospectCall.dispositionStatus' : {$in : ?0}, 'prospectCall.recordingUrlAws' : {$exists : false} , 'prospectCall.callStartTime' : {$gte : ?1}}")
	public List<ProspectCallLog> findSuccessWithMissingRecordings(List<String> status, Date fromDate);
	
	@Query("{  'prospectCall.callRetryCount' : 786122, 'status' : 'MAX_RETRY_LIMIT_REACHED', 'prospectCall.campaignId' : {$in : ?0}}}")
	public List<ProspectCallLog> findContactsToEmailSend(List<String> campaignId);
	
	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.sourceId' : ?1, 'prospectCall.prospect.source' : ?2}")
	public ProspectCallLog findByCampaignIdSourceIdAndSource(String campaignId, String peopleId, String source);
	
	@Query(value = "{'prospectCall.campaignId' : {$in : ?0}, 'prospectCall.dispositionStatus' : {$in : ?1}, 'prospectCall.recordingUrlAws' : {$exists : false}}")
	public List<ProspectCallLog> findTelnyxMissingRecordings(List<String> campaignIds, List<String> status, Pageable pageable);
	
	@Query("{'prospectCall.campaignId' : {$in : ?0}}")
	public List<ProspectCallLog> findProspectCallLogByCampaignId(List<String> campaignIds,Pageable pageable);
	
	@Query(value = "{ 'prospectCall.callRetryCount' : { $in: [786] }, 'status' : {$in : ['MAX_RETRY_LIMIT_REACHED']} , 'prospectCall.campaignId' : ?0}", count = true)
	public int getCallableCount(String campaignId);
	
	@Query("{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dailyCallRetryCount' : {'$lt' : ?1 }, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?2}")
	public List<ProspectCallLog> findContactsByCallRetryCountAndDailyCallRetryCountInCampaign(int retryCount,int dailyRetryCount,String campaignId );

	@Query(value = "{ 'prospectCall.callRetryCount' : ?0, 'prospectCall.dailyCallRetryCount' : {'$lt' : ?1 }, 'prospectCall.dispositionStatus' : 'DIALERCODE', 'prospectCall.subStatus' : {$nin : ['GATEKEEPER_ANSWERMACHINE']}, 'prospectCall.campaignId' : ?2}", count = true)
	public int countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(int retryCount,int dailyRetryCount,String campaignId );
	
	@Query("{ 'prospectCall.callRetryCount' : { $in: [0] }, 'status' : {$in : ['QUEUED']} , 'prospectCall.campaignId' : ?0}")
	public List<ProspectCallLog> findContactsByQueuedInCampaign(String campaignId);

	@Query(value = "{ 'prospectCall.callRetryCount' : { $in: [0] }, 'status' : {$in : ['QUEUED']} , 'prospectCall.campaignId' : ?0}", count = true)
	public int countContactsByQueuedInCampaign(String campaignId);

	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.prospect.sourceId' : ?1}" , count = true)
	public int findUniqueProspectRecordBySourceId(String campaignId,String sourceId);
	@Query("{'prospectCall.campaignId' : ?0 , $or : [{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','CALLBACK']}, 'prospectCall.callRetryCount' : {$in : [1,2,3,4,5,6,7,8,9,786,886]}},{'status' : 'QUEUED'}]}")
	public List<ProspectCallLog> findCallableContacts(String campaignId);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.dispositionStatus' : 'SUCCESS', 'status' : 'WRAPUP_COMPLETE', 'prospectCall.callStartTime' : {$gte: ?1 ,$lt: ?2}}" , count = true)
	public int getSuccessCount(String campaignId,Date startDate,Date endtDate);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.leadStatus' : {$in : ['QA_REJECTED','REJECTED']}, 'prospectCall.subStatus' : 'SUBMITLEAD', 'prospectCall.callStartTime' : {$gte: ?1 ,$lt: ?2}}" , count = true)
	public int getRejectedCount(String campaignId,Date startDate,Date endtDate);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'status' : {$in : ['SECONDARY_QA_INITIATED','PRIMARY_REVIEW','SECONDARY_REVIEW','QA_INITIATED']}, 'prospectCall.callStartTime' : {$gte: ?1 ,$lt: ?2}}" , count = true)
	public int getInQACount(String campaignId,Date startDate,Date endtDate);

	@Query(value = "{'prospectCall.prospect.source' : {$in : ?0}, 'status' : {$in : ?1}, 'prospectCall.callStartTime' : {$gte: ?2}}", count = true)
	public int countBySourceStauseAndCallStartTime(List<String> sourceList, List<String> statuses, Date callStartTime);

	@Query(value = "{'prospectCall.prospect.source' : {$in : ?0}, 'status' : {$in : ?1}, 'prospectCall.callStartTime' : {$gte: ?2}}")
	public List<ProspectCallLog> findBySourceStauseAndCallStartTime(List<String> sourceList, List<String> statuses, Date callStartTime, Pageable pageable);

	@Query("{'prospectCall.campaignId' : ?0, 'insideViewBuy' : ?1, 'prospectCall.prospect.source' : ?2}")
	public List<ProspectCallLog> findProspectsByCampaignIdAndInsideViewBuysAndSource(String campaignId,String insideViewBuy, String source);


	@Query("{'prospectCall.campaignId' : ?0, 'leadIQBuy' : ?1, 'prospectCall.prospect.source' : ?2}")
	public List<ProspectCallLog> findProspectsByCampaignIdAndLeadIQBuysAndSource(String campaignId,String leadIQBuy, String source);
}
