package com.xtaas.db.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface RetrieveRepository<T, ID extends Serializable> extends Repository<T, ID> {
	@Query("{ '_id' : ?0 }")
	public T findOneById(ID id);
	List<T> findAll();
}
