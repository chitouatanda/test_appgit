package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SuppressionListNew;

/**
 * SupressionListNewRepository
 */
public interface SuppressionListNewRepository
		extends MongoRepository<SuppressionListNew, String>, SuppressionListNewRepositoryCustom {

	@Query("{ 'campaignId' : ?0, 'emailId' :  {$exists : true} , 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> pageByCampaignEmail(String campaignId, Pageable pageable);

	@Query("{ 'campaignId' : ?0, 'emailId' :  {$exists : true} , 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> findByCampaignEmail(String campaignId);

	@Query(value = "{ 'campaignId' : ?0, 'emailId' :  {$exists : true} , 'status' : 'ACTIVE' }", count = true)
	public int countByCampaignEmail(String campaignId);

	@Query(value = "{ $or : [ {'campaignId' : ?0, 'emailId' :  {$exists : true}}, {'campaignId' : {$exists : false}, 'organizationId' : ?1, 'emailId' :  {$exists : true}} ], 'status' : 'ACTIVE' }", count = true)
	public int countByCampaignOrganizationEmail(String campaignId, String organizationId);

	@Query("{ 'campaignId' : ?0, 'emailId'  : ?1, 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> findByCampaignEmail(String campaignId, String email);

	@Query(value = "{ $or : [ {'campaignId' : ?0, 'emailId' :  ?1}, {'campaignId' : {$exists : false},'organizationId' : ?2, 'emailId' : ?1} ], 'status' : 'ACTIVE' }", count = true)
	public int countByCampaignEmailOrganization(String campaignId, String email, String organizationId);

	@Query(value = "{ 'campaignId' : {$exists : false},'organizationId' : ?0, 'emailId' : ?1}, 'status' : 'ACTIVE' }", count = true)
	public int countByOrganizationEmail(String organizationId, String email);

	@Query("{ 'campaignId' : ?0, 'domain' : ?1, 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> findSupressionListByCampaignDomain(String campaignId, String domain);

	@Query(value = "{ 'campaignId' : ?0, 'domain' : ?1, 'status' : 'ACTIVE' }", count = true)
	public long countSupressionListByCampaignDomain(String campaignId, String domain);

	@Query(value = "{ 'campaignId' : ?0, 'company'  : ?1, 'status' : 'ACTIVE' }", count = true)
	public long countSupressionListByCampaignCompany(String campaignId, String company);

	@Query("{ 'campaignId' : ?0}")
	public List<SuppressionListNew> findSupressionListByCampaignToClone(String campaignId, Pageable pageable);

	@Query(value = "{ 'campaignId' : ?0, 'domain' :  {$exists : true} , 'status' : 'ACTIVE' }", count = true)
	public int countSupressionListByCampaignDomain(String campaignId);

	@Query(value = "{ 'campaignId' : ?0, 'company' :  {$exists : true} , 'status' : 'ACTIVE' }", count = true)
	public int countSupressionListByCampaignCompany(String campaignId);

	@Query("{ 'campaignId' : ?0, 'domain' :  {$exists : true} , 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> pageSupressionListByCampaignDomain(String campaignId, Pageable pageable);

	@Query(value = "{ 'campaignId' : {'$in': ?0} }", count = true)
	public int countSupressionListByCampaignIds(List<String> campaignId);

	@Query("{ 'campaignId' : ?0, 'domain' :  {$exists : true} , 'status' : 'ACTIVE' }")
	public List<SuppressionListNew> findSupressionListByCampaignDomain(String campaignId);

	@Query("{ 'campaignId' : ?0, 'status' : 'INACTIVE' }")
	public List<SuppressionListNew> removeSupressionListByCampaignId(String campaignId);

}
