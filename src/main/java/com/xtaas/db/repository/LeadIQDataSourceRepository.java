package com.xtaas.db.repository;

import com.xtaas.db.entity.InsideviewDataSource;
import com.xtaas.db.entity.LeadIQDataSource;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface LeadIQDataSourceRepository extends MongoRepository<LeadIQDataSource, String> {

    @Query("{'peopleId' : ?0}")
    public LeadIQDataSource findByPeopleId(String peopleId);

}
