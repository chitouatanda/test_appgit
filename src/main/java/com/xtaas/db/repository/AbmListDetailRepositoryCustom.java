package com.xtaas.db.repository;

import com.xtaas.db.entity.AbmListDetail;

public interface AbmListDetailRepositoryCustom {

	public AbmListDetail findOneByCompanyId(String companyId);

	public AbmListDetail findOneByDomain(String domain);

	public AbmListDetail findOneByDomainRegex(String domain);

}
