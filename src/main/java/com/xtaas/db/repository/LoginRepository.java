package com.xtaas.db.repository;

import com.xtaas.db.entity.Login;
import com.xtaas.web.dto.LoginDTO;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created with IntelliJ IDEA. User: hashimca Date: 3/10/14 Time: 1:14 AM To
 * change this template use File | Settings | File Templates.
 */
public interface LoginRepository extends MongoRepository<Login, String> {

	@Query("{ 'externalSecretKey' : ?0 }")
	public Login findByExternalSecretKey(String secretKey);

	@Query("{'roles' : {$elemMatch : {$eq : 'SUPERVISOR'}}, 'organization' :  ?0 , 'password' :{$ne : 'TERMINATED'} }")
	public List<LoginDTO> findSupervisorByOrganization(String organization);

	@Query("{ '_id' : ?0 }")
	public Login findBySupervisorId(String supervisorId);

	@Query("{ '_id' : ?0 }")
	public Login findOneById(String id);

	@Query("{'_id' : ?0 }")
	public Login findUser(String userName);

	@Query("{'organization' : ?0 ,'adminRole': 'ADMIN_SUPERVISOR'}")
	public List<Login> findUserByOrganizationIdAndAdminRole(String organizationId);

	@Query("{'organization' : ?0 ,'roles':{$in : ['CAMPAIGN_MANAGER']}}")
	public List<Login> findCampaignManagersByOrganization(String organization);

	@Query(value = "{ '_id' : {'$nin' : ?0}, 'roles' : {$nin : ['SUPPORT_ADMIN']} }")
	public List<LoginDTO> findByIdsAndRoles(List<String> ids, Pageable pageable);

	@Query(value = "{ '_id' : {'$nin' : ?0}, 'roles' : {$nin : ['SUPPORT_ADMIN']} }", count = true)
	public long countByIdsAndRoles(List<String> ids);

	@Query(value = "{ '$and' : [ {'_id' : {'$regex' : ?0, $options : 'i'}}, {'_id' : {$nin : ?1}} ], 'roles' : {$nin : ['SUPPORT_ADMIN']} }")
	public List<LoginDTO> findByUsernameAndRoles(String id, List<String> ids, Pageable pageable);

	@Query(value = "{ '$and' : [ {'_id' : {'$regex' : ?0, $options : 'i'}}, {'_id' : {$nin : ?1}} ], 'roles':{$nin : ['SUPPORT_ADMIN']}}", count = true)
	public long countByUsernameAndRoles(String id, List<String> ids);

	@Query(value = "{ 'organization' : {'$regex' : ?0, $options : 'i'}, '_id' : {$nin : ?1}, 'roles' : {$nin : ['SUPPORT_ADMIN']} }")
	public List<LoginDTO> findByOrganizationAndIdsAndRoles(String organization, List<String> ids, Pageable pageable);

	@Query(value = "{ 'organization' : {'$regex' : ?0, $options : 'i'}, '_id' : {$nin : ?1}, 'roles' : {$nin : ['SUPPORT_ADMIN']} }", count = true)
	public long countByOrganizationAndIdsAndRoles(String organization, List<String> ids);

}
