/**
 * 
 */
package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.AgentCallLog;

/**
 * @author djain
 *
 */
public interface AgentCallLogRepository extends MongoRepository<AgentCallLog, String> {

}
