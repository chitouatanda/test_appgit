package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.AgentConference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class AgentConferenceRepositoryImpl implements AgentConferenceRepositoryCustom {

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public String findAgentIdByConferenceId(String conferenceId) {
    List<AgentConference> agents = mongoTemplate.find(Query.query(Criteria.where("conferenceId").is(conferenceId)), AgentConference.class, "agentConference");
    if (agents.size() > 0) {
      String agentId = agents.get(0).getAgentId();
      return agentId;
    }
    return null;
  }

  @Override
  public AgentConference findConferenceIdByAgentId(String agentId) {
    List<AgentConference> agents = mongoTemplate.find(Query.query(Criteria.where("agentId").is(agentId)), AgentConference.class, "agentConference");
    if (agents.size() > 0) {
      AgentConference agentConference = agents.get(0);
      return agentConference;
    }
    return null;
  }

}