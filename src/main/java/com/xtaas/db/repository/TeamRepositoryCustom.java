package com.xtaas.db.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import com.xtaas.domain.valueobject.TeamSearchResult;
import com.xtaas.web.dto.TeamSearchResultDTO;

public interface TeamRepositoryCustom {
	public List<TeamSearchResultDTO> searchTeams(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerLeadPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimumAverageConversion, double minimumAverageQuality, int minimumAvailableHours,
			Pageable pageRequest);

	public List<Object> countTeams(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerLeadPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimumAverageConversion, double minimumAverageQuality, int minimumAvailableHours);
	
	public List<Object> countTeams(List<Double> ratings, double maximumPerLeadPrice,
			int minimumCompletedCampaigns, int minimumTotalVolume, double minimumAverageConversion,
			double minimumAverageQuality, int minimumAvailableHours);

	public List<TeamSearchResultDTO> searchTeams(List<Double> ratings, double maximumPerLeadPrice,
			int minimumCompletedCampaigns, int minimumTotalVolume, double minimumAverageConversion,
			double minimumAverageQuality, int minimumAvailableHours, Pageable pageRequest);
	
	public int getSupervisorOverrideAgentsCount(String campaignId);
}
