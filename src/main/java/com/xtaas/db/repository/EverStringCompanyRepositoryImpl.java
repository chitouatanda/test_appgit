package com.xtaas.db.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.xtaas.db.entity.EverStringCompany;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

public class EverStringCompanyRepositoryImpl implements EverStringCompanyRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	PropertyService propertyService;

	@Override
	public void bulkinsert(List<EverStringCompany> lstEverStringCompany) {
		int listSize = lstEverStringCompany.size();
		int batchSize = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.BULKINSERT_BATCHSIZE.name(), 2000);
		if (listSize <= batchSize) {
			mongoTemplate.insert(lstEverStringCompany, EverStringCompany.class);
		} else {
			int batches = listSize/batchSize;
			int modSize = listSize%batchSize;
			for (int i = 0; i <= batches; i++) {
				int fromIndex = i*batchSize;
				int toIndex = fromIndex + batchSize;
				if (i == batches) {
					toIndex = fromIndex + modSize -1;
				}
				List<EverStringCompany> templstEverStringCompany = lstEverStringCompany.subList(fromIndex, toIndex);
				mongoTemplate.insert(templstEverStringCompany, EverStringCompany.class);
			}
		}
	}
}
