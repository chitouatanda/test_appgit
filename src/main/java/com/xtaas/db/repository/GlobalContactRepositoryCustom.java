package com.xtaas.db.repository;

import com.xtaas.db.entity.GlobalContact;

public interface GlobalContactRepositoryCustom {

	public long countUniqueRecordByFLP(String firstName, String lastName, String phone);
	
	public GlobalContact findUniqueRecordByFLP(String firstName, String lastName, String phone);
	
	public GlobalContact findUniqueRecordByFLTC(String firstName, String lastName, String title, String company);

}
