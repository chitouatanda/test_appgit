package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.GlobalContactProspectStatus;

public interface GlobalContactProspectStatusRepository
		extends MongoRepository<GlobalContactProspectStatus, String>, GlobalContactProspectStatusRepositoryCustom {

	@Query(value = "{ 'prospectCallId' : ?0, 'status' : ?1 }")
	public GlobalContactProspectStatus findByProspectCallIdAndStatus(String prospectCallId, String status);

	@Query(value = "{ 'campaignId' : ?0, 'prospectCallId' : ?1, 'status' : ?2 }")
	public GlobalContactProspectStatus findByCampaignIdAndProspectCallIdAndStatus(String campaignId,
			String prospectCallId, String status);

}
