package com.xtaas.db.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

import com.xtaas.db.entity.GlobalContact;

public interface GlobalContactRepository extends MongoRepository<GlobalContact, String>, GlobalContactRepositoryCustom {
	@Query("{ '_id' : ?0 }")
	public GlobalContact findOneById(String id);

	public List<GlobalContact> findByPhone(String phone);

//	@Query("{ 'firstName' : ?0, 'lastName' : ?1, 'phone' : ?2 }")
//	public GlobalContact findUniqueRecordByFLP(String firstName, String lastName, String phone);
//
//	@Query("{ 'firstName' : ?0, 'lastName' : ?1, 'title' : ?2, 'organizationName' : ?3 }")
//	public GlobalContact findUniqueRecordByFLTC(String firstName, String lastName, String title, String company);

	@Query("{ 'sourceId' : ?0}")
	public List<GlobalContact> findUniqueRecordBySourceId(String sourceId, Pageable pageable);

}
