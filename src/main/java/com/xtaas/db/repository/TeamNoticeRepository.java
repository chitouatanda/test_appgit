package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.TeamNotice;

public interface TeamNoticeRepository extends MongoRepository<TeamNotice, String>{

	@Query(value = "{ 'startDate' : {'$lte' : ?0}, 'endDate' : {'$gte' : ?0}, $or: [{'forResources': ?1}, {'forTeams':{'$in': ?2}}]}")
	public List<TeamNotice> getNotices(Date todayDate, String userId, List<String> teamIds);
	
	@Query(value = "{ 'startDate' : {'$lte' : ?0}, 'endDate' : {'$gte' : ?0}, 'createdBy' : ?1}")
	public List<TeamNotice> getNoticesBySupervisorId(Date todayDate, String supervisorId);
	
	@Query(value = "{'startDate' : {'$lte' : ?0}, 'endDate' : {'$gte' : ?0}, 'requestorId': ?1}")
	public List<TeamNotice> getNoticesByRequestorId(Date todayDate, String userId);
	
}
