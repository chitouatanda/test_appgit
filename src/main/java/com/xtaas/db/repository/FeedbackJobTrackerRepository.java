package com.xtaas.db.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.FeedbackJobTracker;

public interface FeedbackJobTrackerRepository extends MongoRepository<FeedbackJobTracker, String> {

	public Optional<FeedbackJobTracker> findByJobId(String jobId);

}
