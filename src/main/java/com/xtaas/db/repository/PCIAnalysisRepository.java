package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.PCIAnalysis;


public interface PCIAnalysisRepository extends MongoRepository<PCIAnalysis, String> {
	
	@Query("{ 'campaignid' : ?0, 'email' : ?1 }")
	public PCIAnalysis getProspectByCIDAndEmail(String campaignId,String email);
	
	@Query(value= "{ 'prospectcallid' : ?0 }")
	public List<PCIAnalysis> findByProspectCallIdWithSort(String prospectCallId, Pageable pageable);
	
}