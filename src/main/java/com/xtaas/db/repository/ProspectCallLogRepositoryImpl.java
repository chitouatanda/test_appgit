package com.xtaas.db.repository;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.RegEx;

import com.xtaas.db.entity.*;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.domain.valueobject.Expression;

import com.xtaas.service.*;
import jdk.nashorn.internal.ir.debug.ObjectSizeCalculator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.QaMetricsServiceImpl.QaMetrics;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CallbackDispositionStatus;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.DialerCodeDispositionStatus;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.DistinctCampaignIdsDTO;
import com.xtaas.domain.valueobject.Pivot;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.CampaignCriteriaDTO;
import com.xtaas.web.dto.GlobalContactCriteriaDTO;
import com.xtaas.web.dto.HealthChecksCriteriaDTO;
import com.xtaas.web.dto.HealthChecksDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.RecycleStatusDTO;
import com.xtaas.web.dto.TranscribeCriteriaDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;
import com.xtaas.worker.ColdEmailByCampaignThread;

public class ProspectCallLogRepositoryImpl implements ProspectCallLogRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(ProspectCallLogRepositoryImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private CampaignContactRepository campaignContactRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private PCIAnalysisRepository pCIAnalysisRepository;
	
	@Autowired
	private DomoUtilsService domoUtilsService;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;

	@Autowired
	private DataProviderService dataProviderService;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;
	
	@Autowired
	private UserService userService;

	@Autowired
	private InsideViewServiceImpl insideViewServiceImpl;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private LeadIQSearchServiceImpl leadIQSearchServiceImpl;

	@Autowired
	private DataBuyMappingRepository dataBuyMappingRepository;
	
	@Value("${encrypted_mongoconnection}") 
	String encryptedMongoConnection;
	
	final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd";
	final private String insideView = "INSIDEVIEW";

	private Thread coldEmailByCampaignThread = null;

	/**
	 * This methods is to display agent wise stats (AvgTalkTime, ContactRate,
	 * LeadDelivered) on supervisor screen
	 */
	public List<AgentSearchResult> getAgentCallStats(List<String> agentIds, Date fromDate, Date toDate) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatch(agentIds, fromDate, toDate));
		aggregationOperations.add(buildGroup());
		AggregationResults<AgentSearchResult> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "prospectcalllog", AgentSearchResult.class);
		return results.getMappedResults();
	}

	/**
	 * This methods is to calculate average score of QA on supervisor screen
	 */
	public double getAvgFeedbackScore(String campaignId, Date fromDate, Date toDate) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatchOfProspectFeedback(campaignId, fromDate, toDate));
		aggregationOperations.add(buildGroupofProspectFeedback());
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", Object.class);
		List<Object> qaScoreList = results.getMappedResults();
		double qaScore = 0;
		if (!qaScoreList.isEmpty()) {
			LinkedHashMap<?, ?> qaScoreMap = (LinkedHashMap<?, ?>) qaScoreList.get(0);
			qaScore = (Double) qaScoreMap.get("score");
		}
		return qaScore;
	}

	private AggregationOperation buildBasicMatch(List<String> agentIds, Date fromDate, Date toDate) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		ArrayList<String> callStatusList = new ArrayList<String>();
		callStatusList.add(ProspectCallStatus.CALL_COMPLETE.name());
		callStatusList.add(ProspectCallStatus.WRAPUP_COMPLETE.name());
		callStatusList.add(ProspectCallStatus.QA_INITIATED.name());
		callStatusList.add(ProspectCallStatus.QA_COMPLETE.name());

		Criteria criteria = Criteria.where("status").in(callStatusList);
		criterias.add(Criteria.where("prospectCall.agentId").in(agentIds));
		criterias.add(Criteria.where("prospectCall.callStartTime").gte(fromDate));
		criterias.add(Criteria.where("prospectCall.callStartTime").lte(toDate));

		if (criterias.size() == 0) {
			return Aggregation.match(criteria);
		} else {
			return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
		}
	}

	private AggregationOperation buildBasicMatchOfProspectFeedback(String campaignId, Date fromDate, Date toDate) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		ArrayList<String> callStatusList = new ArrayList<String>();
		callStatusList.add(ProspectCallStatus.QA_COMPLETE.name());

		Criteria criteria = Criteria.where("status").in(callStatusList);
		criterias.add(Criteria.where("prospectCall.campaignId").in(campaignId));

		if (fromDate != null && toDate == null) {
			criterias.add(Criteria.where("qaFeedback.feedbackTime").gte(fromDate));
		}
		if (fromDate == null && toDate != null) {
			criterias.add(Criteria.where("qaFeedback.feedbackTime").lte(toDate));
		}
		if (fromDate != null && toDate != null) {
			criterias.add(Criteria.where("qaFeedback.feedbackTime").gte(fromDate).lte(toDate));
		}
		if (criterias.size() == 0) {
			return Aggregation.match(criteria);
		} else {
			return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
		}
	}

	private AggregationOperation buildGroupofProspectFeedback() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.campaignId");
		group = group.append("score", new BasicDBObject("$avg", "$qaFeedback.overallScore"));

		return new CustomAggregationOperation(new Document("$group", group));
	}

	private AggregationOperation buildGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.agentId");
		group = group.append("contactsMade", new BasicDBObject("$sum", 1));
		group = group.append("leadsDelivered", new BasicDBObject("$sum", new BasicDBObject("$cond", new Object[] {
				new BasicDBObject("$eq", new Object[] { "$prospectCall.dispositionStatus", "SUCCESS" }), 1, 0 })));
		group = group.append("averageTalkTime", new BasicDBObject("$avg", "$prospectCall.callDuration"));

		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public List<ProspectCallLog> getRingingProspects(List<String> campaignIds) {
		return mongoTemplate.find(Query.query(buildRingingProspectsCriteria(campaignIds)), ProspectCallLog.class);
	}

	private Criteria buildRingingProspectsCriteria(List<String> campaignIds) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignIds);
		criteria.and("status").in(ProspectCallStatus.CALL_DIALED.name());
		Date todaysDate = null;
		try {
			todaysDate = XtaasDateUtils.getBizTime();
			criteria.and("updatedDate").gte(todaysDate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return criteria;
	}
	
	@Override
	public List<ProspectCallLog> getProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria)
			throws ParseException {
		Query query = new Query();
		query.addCriteria(buildTranscriptionCriteria(transcribeCriteria));
		query.fields().include("prospectCall.campaignId").include("prospectCall.prospectCallId")
				.include("prospectCall.recordingUrlAws").include("prospectCall.callDuration")
				.include("prospectCall.prospect.phone").exclude("_id");
		return mongoTemplate.find(query, ProspectCallLog.class);
	}
	
	@Override
	public long getCountOfProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria)
			throws ParseException {
		Query query = new Query();
		query.addCriteria(buildTranscriptionCriteria(transcribeCriteria));
		query.fields().include("prospectCall.campaignId").include("prospectCall.prospectCallId")
				.include("prospectCall.recordingUrlAws").include("prospectCall.callDuration")
				.include("prospectCall.prospect.phone").exclude("_id");
		return mongoTemplate.count(query, ProspectCallLog.class);
	}
	
	
	@SuppressWarnings("unused")
	private Criteria buildTranscriptionCriteria(TranscribeCriteriaDTO transcribeCriteria) throws ParseException {
		Criteria criteria = new Criteria();

		Date minDate = null, maxDate = null;
		if (transcribeCriteria.getMinDate() != null && !transcribeCriteria.getMinDate().trim().isEmpty()) {
			minDate = XtaasDateUtils.convertStringToDate(transcribeCriteria.getMinDate(), DATE_FORMAT_DEFAULT);
		}
		if (transcribeCriteria.getMaxDate() != null && !transcribeCriteria.getMaxDate().trim().isEmpty()) {
			minDate = XtaasDateUtils.convertStringToDate(transcribeCriteria.getMaxDate(), DATE_FORMAT_DEFAULT);
		}
		if (minDate != null && maxDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(minDate).lte(maxDate);
		}

		if (transcribeCriteria.getCampaignIds() != null && !transcribeCriteria.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(transcribeCriteria.getCampaignIds());
		} 
		if (transcribeCriteria.getDispositionStatus() != null
				&& !transcribeCriteria.getDispositionStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in(transcribeCriteria.getDispositionStatus());
		} 
		if (transcribeCriteria.getSubStatus() != null && transcribeCriteria.getSubStatus().size() > 0) {
			criteria = criteria.and("prospectCall.subStatus").in(transcribeCriteria.getSubStatus());
		} 
		if (transcribeCriteria.getDirectPhone() != null && !transcribeCriteria.getDirectPhone().isEmpty()) {
			if (transcribeCriteria.getDirectPhone().equalsIgnoreCase("YES")) {
				criteria = criteria.and("prospectCall.directPhone").in(true);
			}
			if (transcribeCriteria.getDirectPhone().equalsIgnoreCase("NO")) {
				criteria = criteria.and("prospectCall.directPhone").in(false);
			}
		} 
		if (transcribeCriteria.getEmailRevealed() != null && !transcribeCriteria.getEmailRevealed().isEmpty()) {
			if (transcribeCriteria.getEmailRevealed().equalsIgnoreCase("YES")) {
				criteria = criteria.and("prospectCall.emailReveal").in(true);
			}
			if (transcribeCriteria.getEmailRevealed().equalsIgnoreCase("NO")) {
				criteria = criteria.and("prospectCall.emailReveal").in(false);
			}
		}
		return criteria;
	}

	// DATE:03/01/2018 Fetch prospect by agentIds and campaign status RUNNING and
	// qaRequired for primaryqa
	@Override
	public List<ProspectCallLog> searchProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException {
		Query query = new Query();
		query.addCriteria(buildProspectCallCriteriaByQa(agentIds, campaignIds, prospectCallSearchDTO));
		return mongoTemplate.find(query.with(pageRequest), ProspectCallLog.class);
	}

	// DATE:28/12/2017 Fetch prospect by agentIds for primaryqa
	@Override
	public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> agentIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		return mongoTemplate.find(Query.query(buildProspectCallCriteriaByQa(agentIds, prospectCallSearchDTO)),
				ProspectCallLog.class);
	}

	public List<ProspectCallLog> searchProspectCallByReport(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		return mongoTemplate.find(Query.query(buildProspectCallCriteriaByReport(prospectCallSearchDTO)),
				ProspectCallLog.class);
	}

	// DATE:03/01/2018 Fetch prospect by agentIds and campaign status RUNNING and
	// qaRequired for primaryqa
	@Override
	public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> agentIds,
			List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		return mongoTemplate.find(
				Query.query(buildProspectCallCriteriaByQa(agentIds, campaignIds, prospectCallSearchDTO)),
				ProspectCallLog.class);
	}

	// DATE:16/01/2018 REASON:Added to fetch prospect with SECONDARY_QA_INITIATED
	// and RUNNIGN and qaRequired campaign
	@Override
	public List<ProspectCallLog> searchProspectCallsBySecQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException {
		Query query = new Query();
		query.addCriteria(buildProspectCallCriteriaBySecQa(campaignIds, prospectCallSearchDTO)).with(pageRequest);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	// DATE:16/01/2018 REASON:Added to fetch prospect with SECONDARY_QA_INITIATED
	// without pagination
	@Override
	public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		return mongoTemplate.find(Query.query(buildProspectCallCriteriaBySecQa(campaignIds, prospectCallSearchDTO)),
				ProspectCallLog.class);
	}

	@Override
	public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		return mongoTemplate.find(Query.query(buildProspectCallCriteriaBySecQa(prospectCallSearchDTO)),
				ProspectCallLog.class);
	}

	@Override
	public List<ProspectCallLog> searchProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO,
			Pageable pageRequest) throws ParseException {
		return mongoTemplate.find(
				Query.query(buildProspectCallCriteriaByAgent(agentId, prospectCallSearchDTO)).with(pageRequest),
				ProspectCallLog.class);
	}

	/*
	 * @Override public int countProspectCallsByQa(List<String> campaignIds,
	 * ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException { return
	 * mongoTemplate.find( Query.query( buildProspectCallCriteriaByQa(campaignIds,
	 * prospectCallSearchDTO)), ProspectCallLog.class).size(); }
	 */

	// DATE:03/01/2018 Fetch prospect by agentIds and campaign status RUNNING and
	// qaRequired for primaryqa
	@Override
	public long countProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Query query = new Query();
		query.addCriteria(buildProspectCallCriteriaByQa(agentIds, campaignIds, prospectCallSearchDTO));
		return mongoTemplate.count(query, ProspectCallLog.class);
	}

	@Override
	public int countProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		return mongoTemplate
				.find(Query.query(buildProspectCallCriteriaBySecQa(prospectCallSearchDTO)), ProspectCallLog.class)
				.size();
	}

	// DATE:04/01/2018 Fetch prospect by agentIds and campaign status RUNNING and
	// qaRequired for primaryqa
	@Override
	public long countProspectCallsBySecQa(List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		Query query = new Query();
		query.addCriteria(buildProspectCallCriteriaBySecQa(campaignIds, prospectCallSearchDTO));
		return mongoTemplate.count(query, ProspectCallLog.class);
	}

	@Override
	public int countProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		return mongoTemplate.find(Query.query(buildProspectCallCriteriaByAgent(agentId, prospectCallSearchDTO)),
				ProspectCallLog.class).size();
	}

	@Override
	public int countProspectsContacted(String campaignId, Date fromDate, Date toDate) {
		return mongoTemplate
				.find(Query.query(buildProspectsContactedCriteria(campaignId, fromDate, toDate)), ProspectCallLog.class)
				.size();
	}

	private Criteria buildProspectsContactedCriteria(String campaignId, Date fromDate, Date toDate) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		// criteria = criteria.and("prospectCall.recordingUrl").exists(true);
		ArrayList<String> dispoStatusList = new ArrayList<String>();
		dispoStatusList.add(DispositionType.SUCCESS.name());
		dispoStatusList.add(DispositionType.FAILURE.name());

		List<Criteria> orCriterias = new ArrayList<Criteria>();
		orCriterias.add(
				Criteria.where("prospectCall.subStatus").in(CallbackDispositionStatus.CALLBACK_USER_GENERATED.name()));
		orCriterias.add(Criteria.where("prospectCall.dispositionStatus").in(dispoStatusList));
		criteria = criteria.orOperator(orCriterias.toArray(new Criteria[0]));

		if (fromDate != null && toDate == null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate);
		}
		if (fromDate == null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").lte(toDate);
		}
		if (fromDate != null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate).lte(toDate);
		}
		return criteria;
	}

	@Override
	public int countProspectsDelivered(String campaignId, Date fromDate, Date toDate) {
		return mongoTemplate
				.find(Query.query(buildProspectsDeliveredCriteria(campaignId, fromDate, toDate)), ProspectCallLog.class)
				.size();
	}

	private Criteria buildProspectsDeliveredCriteria(String campaignId, Date fromDate, Date toDate) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		criteria = criteria.and("prospectCall.dispositionStatus").is("SUCCESS");
		// criteria = criteria.and("prospectCall.recordingUrl").exists(true);
		if (fromDate != null && toDate == null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate);
		}
		if (fromDate == null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").lte(toDate);
		}
		if (fromDate != null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate).lte(toDate);
		}
		return criteria;
	}

	@Override
	public int countProspectsDialed(String campaignId, Date fromDate, Date toDate) {
		return mongoTemplate
				.find(Query.query(buildProspectsDialedCriteria(campaignId, fromDate, toDate)), ProspectCallLog.class)
				.size();
	}

	private Criteria buildProspectsDialedCriteria(String campaignId, Date fromDate, Date toDate) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		criteria = criteria.and("prospectCall.twilioCallSid").exists(true);
		if (fromDate != null && toDate == null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate);
		}
		if (fromDate == null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").lte(toDate);
		}
		if (fromDate != null && toDate != null) {
			criteria = criteria.and("prospectCall.callStartTime").gte(fromDate).lte(toDate);
		}
		return criteria;
	}

	private Criteria buildProspectCallCriteriaByReport(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {

		Criteria criteria = new Criteria();

		if (prospectCallSearchDTO.getLeadStatus() != null && prospectCallSearchDTO.getLeadStatus().size() > 0) {
			if (prospectCallSearchDTO.getLeadStatus().contains("CALLBACK")) {
				criteria = Criteria.where("prospectCall.dispositionStatus").in("CALLBACK");
//				criteria = criteria.and("prospectCall.subStatus").in("CALLBACK");
			} else {
				criteria = Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
				criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
			}
		} else if (prospectCallSearchDTO.getDisposition() != null && prospectCallSearchDTO.getDisposition().size() > 0
				&& prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 0) {
			criteria = Criteria.where("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
			criteria = criteria.and("prospectCall.dispositionStatus").in("FAILURE");
		} else {
			List<String> leadStatus = new ArrayList<String>();
			// // leadStatus.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
			// leadStatus.add(XtaasConstants.LEAD_STATUS.ACCEPTED.name());
			// leadStatus.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
			// leadStatus.add(XtaasConstants.LEAD_STATUS.REJECTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.ACCEPTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.REJECTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.name());
			leadStatus.add(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.name());
			criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
			criteria = Criteria.where("prospectCall.leadStatus").in(leadStatus);
		}
		if (prospectCallSearchDTO.getAgentIds() != null && prospectCallSearchDTO.getAgentIds().size() > 0) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}

		// if (prospectCallSearchDTO.getQaIds() != null &&
		// prospectCallSearchDTO.getQaIds().size()>0) {
		// criteria =
		// criteria.and("qaFeedback.qaId").in(prospectCallSearchDTO.getQaIds());
		// }

		if (prospectCallSearchDTO.getPartnerIds() != null && prospectCallSearchDTO.getPartnerIds().size() > 0) {
			criteria = criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getPartnerIds());
		}

		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		}
		Date fromDate = null, toDate = null;

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		/*
		 * if (prospectCallSearchDTO.getDisposition() != null &&
		 * !prospectCallSearchDTO.getDisposition().isEmpty()) { criteria =
		 * criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.
		 * getDisposition()); }
		 */
		/*
		 * if (prospectCallSearchDTO.getSubStatus() != null &&
		 * !prospectCallSearchDTO.getSubStatus().isEmpty()) { criteria =
		 * criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus(
		 * )); }
		 */
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		if (prospectCallSearchDTO.getClientId() != null) {
			criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getClientId());
		}
		/*
		 * if (prospectCallSearchDTO.getClientId() != null ) {
		 * criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getClientId()
		 * ); }
		 */
		return criteria;
	}

	private Criteria buildProspectCallCriteriaByQa(List<String> agentIds, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		ArrayList<String> callStatusList = new ArrayList<String>();
		callStatusList.add(ProspectCallStatus.WRAPUP_COMPLETE.name());
		callStatusList.add(ProspectCallStatus.QA_INITIATED.name());
		callStatusList.add(ProspectCallStatus.PRIMARY_REVIEW.name());

		Criteria criteria = new Criteria();
		if (prospectCallSearchDTO.getAgentIds() == null || prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = Criteria.where("prospectCall.agentId").in(agentIds);
		} else {
			criteria = Criteria.where("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}
		// criteria = criteria.and("prospectCall.recordingUrl").exists(true);

		if (prospectCallSearchDTO.getProspectCallStatus() == null
				|| prospectCallSearchDTO.getProspectCallStatus().isEmpty()) {
			criteria.and("status").in(callStatusList);
		}
		/*
		 * DATE:12/12/2017 REASON:Added to show QA_COMPLETE leads READY_FOR_DELIVERY AND
		 * REJECTED_LEADS on QA page
		 */
		else if (prospectCallSearchDTO.getProspectCallStatus().equals("READY_FOR_DELIVERY")) {
			criteria.and("status").in(ProspectCallStatus.QA_COMPLETE.name());

			Criteria searchCriteria = Criteria.where("attribute").in("LEAD_VALIDATION_VALID");
			searchCriteria.and("feedback").in("YES");

			criteria.and("qaFeedback.feedbackResponseList.responseAttributes").elemMatch(searchCriteria);
		} else if (prospectCallSearchDTO.getProspectCallStatus().equals("REJECTED_LEADS")) {
			criteria.and("status").in(ProspectCallStatus.QA_COMPLETE.name());

			Criteria searchCriteria = Criteria.where("attribute").in("LEAD_VALIDATION_VALID");
			searchCriteria.and("feedback").in("NO");

			criteria.and("qaFeedback.feedbackResponseList.responseAttributes").elemMatch(searchCriteria);
		} else {
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress")) {
				criteria.and("status").in(ProspectCallStatus.QA_INITIATED.name());
			}
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")) {
				criteria.and("status").in(ProspectCallStatus.QA_COMPLETE.name());
			}
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Not Scored")) {
				criteria.and("status").in(ProspectCallStatus.WRAPUP_COMPLETE.name());
			}
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		}
		if (prospectCallSearchDTO.getLeadStatus() != null) {
			criteria.and("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
		}
		Date fromDate = null, toDate = null;

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (prospectCallSearchDTO.getDisposition() != null && !prospectCallSearchDTO.getDisposition().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.getDisposition());
		}
		if (prospectCallSearchDTO.getSubStatus() != null && !prospectCallSearchDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
		}
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		if (prospectCallSearchDTO.getClientId() != null) {
			criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getClientId());
		}
		return criteria;
	}

	// To fetch prospect using agent and campaign RUNNING and qaRequired
	private Criteria buildProspectCallCriteriaByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Criteria criteria = new Criteria();
		/**
		 * DATE :- 02/01/2020 Added ANSWERING M/C and GK ANSWERING M/C options in
		 * leadstatus list to fetch answering machine records.
		 */
		if (prospectCallSearchDTO.getLeadStatus() != null && !prospectCallSearchDTO.getLeadStatus().isEmpty()) {
			if (prospectCallSearchDTO.getLeadStatus().contains("GK_ANSWERMACHINE")
					&& prospectCallSearchDTO.getLeadStatus().contains("ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("GATEKEEPER_ANSWERMACHINE", "ANSWERMACHINE");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("GK_ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("GATEKEEPER_ANSWERMACHINE");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("ANSWERMACHINE");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("NOT_DISPOSED")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("NOT_DISPOSED");
				criteria = criteria.and("prospectCall.subStatus").in("NOT_DISPOSED");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("CALLBACK")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("CALLBACK");
				criteria = criteria.and("prospectCall.subStatus").in("CALLBACK");
			} else {
				criteria = Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
				criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
			}
		} else if (prospectCallSearchDTO.getDisposition() != null && !prospectCallSearchDTO.getDisposition().isEmpty()
				&& prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 0) {
			criteria = Criteria.where("prospectCall.dispositionStatus").in("FAILURE","AUTO");
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());

		} else {
			ArrayList<String> leadStatuses = new ArrayList<String>();
			/*
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.ACCEPTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString());
			 */
			leadStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
			// leadStatuses.add(XtaasConstants.LEAD_STATUS.REJECTED.toString());
			leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
			criteria = Criteria.where("prospectCall.leadStatus").in(leadStatuses);
			criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		} else if (campaignIds != null && !campaignIds.isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(campaignIds);
		}
		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		} else if (agentIds != null && !agentIds.isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(agentIds);
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}

		Date fromDate = null, toDate = null;
		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		/*
		 * START LAST 72 HOURS SUCCESS DATE : 15/06/2017 REASON : added to fetch last 72
		 * hours SUCCESS records
		 */
		boolean campaignId = false;
		boolean teamIds = false;
		boolean disposition = false;
		boolean subStatus = false;
		boolean prospectCallIds = false;

		if (prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			campaignId = true;
		}
		if (prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) {
			teamIds = true;
		}
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			disposition = true;
		}
		if (prospectCallSearchDTO.getSubStatus() == null || prospectCallSearchDTO.getSubStatus().isEmpty()) {
			subStatus = true;
		}
		if (prospectCallSearchDTO.getProspectCallIds() == null
				|| prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			prospectCallIds = true;
		}
		// if (campaignId && teamIds && prospectCallIds && disposition && subStatus
		// && prospectCallSearchDTO.getCallDurationRangeEnd() == null
		// && prospectCallSearchDTO.getCallDurationRangeStart() == null
		// && prospectCallSearchDTO.getCampaignId() == null &&
		// prospectCallSearchDTO.getClientId() == null
		// && prospectCallSearchDTO.getEndHour() == null &&
		// prospectCallSearchDTO.getEndMinute() == 0
		// && prospectCallSearchDTO.getFromDate() == null &&
		// prospectCallSearchDTO.getProspectCallStatus() == null
		// && prospectCallSearchDTO.getStartHour() == null &&
		// prospectCallSearchDTO.getStartMinute() == 0
		// && prospectCallSearchDTO.getToDate() == null) {
		// criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
		// }

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate == null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			// DateTime endDate = getDateTime(toDate, 23, 59,
			// prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate);
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		/*
		 * if (prospectCallSearchDTO.getDisposition() != null &&
		 * !prospectCallSearchDTO.getDisposition().isEmpty()) { criteria =
		 * criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.
		 * getDisposition()); }
		 */
		/*
		 * if ((prospectCallSearchDTO.getLeadStatus() == null ||
		 * prospectCallSearchDTO.getLeadStatus().isEmpty()) &&
		 * prospectCallSearchDTO.getSubStatus() != null &&
		 * !prospectCallSearchDTO.getSubStatus().isEmpty()) { criteria =
		 * criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus(
		 * )); }
		 */
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		// Below criterias for searching leads on qa screen by
		// firstName/lastName/companyName/firstname and lastName both.
		if (prospectCallSearchDTO.getQaSearchString() != null && !prospectCallSearchDTO.getQaSearchString().isEmpty()
				&& prospectCallSearchDTO.getQaSearchString() != "") {
			List<Criteria> orCriteriaList = new ArrayList<>();
			List<Criteria> andCriteriaList = new ArrayList<>();
			String[] names = prospectCallSearchDTO.getQaSearchString().split("\\s+");
			if (encryptedMongoConnection.equalsIgnoreCase("false")) {
				orCriteriaList.add(Criteria.where("prospectCall.prospect.firstName")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.lastName")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.company")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				if (names != null && names.length > 1) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").regex(names[0], "i"));
					andCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").regex(names[1], "i"));
					orCriteriaList.addAll(andCriteriaList);
				}
				criteria = criteria.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
			} else {
				orCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").is(prospectCallSearchDTO.getQaSearchString()));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").is(prospectCallSearchDTO.getQaSearchString()));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.company").is(prospectCallSearchDTO.getQaSearchString()));
				if (names != null && names.length > 1) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").is(names[0]));
					andCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").is(names[1]));
					orCriteriaList.addAll(andCriteriaList);
				}
				criteria = criteria.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
			}
			
		}

		return criteria;
	}

	private Criteria buildProspectCallCriteriaBySecQa(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		ArrayList<String> callStatusList = new ArrayList<String>();

		Criteria criteria = new Criteria();

		// DATE:12/12/2017 REASON:Added to show QA_COMPLETE leads READY_FOR_DELIVERY AND
		// REJECTED_LEADS on QA page.
		if (prospectCallSearchDTO.getProspectCallStatus() != null
				&& prospectCallSearchDTO.getProspectCallStatus().equals("READY_FOR_DELIVERY")) {
			criteria = Criteria.where("status").in(ProspectCallStatus.QA_COMPLETE.name());

			Criteria searchCriteria = Criteria.where("attribute").in("LEAD_VALIDATION_VALID");
			searchCriteria.and("feedback").in("YES");

			criteria.and("qaFeedback.feedbackResponseList.responseAttributes").elemMatch(searchCriteria);
		} else if (prospectCallSearchDTO.getProspectCallStatus() != null
				&& prospectCallSearchDTO.getProspectCallStatus().equals("REJECTED_LEADS")) {
			criteria = Criteria.where("status").in(ProspectCallStatus.QA_COMPLETE.name());

			Criteria searchCriteria = Criteria.where("attribute").in("LEAD_VALIDATION_VALID");
			searchCriteria.and("feedback").in("NO");

			criteria.and("qaFeedback.feedbackResponseList.responseAttributes").elemMatch(searchCriteria);
		} else {
			criteria = Criteria.where("status").in("SECONDARY_QA_INITIATED", "SECONDARY_REVIEW");
		}

		if (prospectCallSearchDTO.getCampaignIds() != null) {
			criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		}

		if (prospectCallSearchDTO.getClientId() != null) {
			criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getClientId());
		}

		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}

		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}

		if (prospectCallSearchDTO.getLeadStatus() != null) {
			criteria.and("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
		}

		// if (prospectCallSearchDTO.getQaIds() != null &&
		// !prospectCallSearchDTO.getQaIds().isEmpty()) {
		// criteria =
		// criteria.and("qaFeedback.qaId").in(prospectCallSearchDTO.getQaIds());
		// }

		if (prospectCallSearchDTO.getPartnerIds() != null && !prospectCallSearchDTO.getPartnerIds().isEmpty()) {
			criteria = criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getPartnerIds());
		}

		Date fromDate = null, toDate = null;

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
		}
		if (prospectCallSearchDTO.getDisposition() != null && !prospectCallSearchDTO.getDisposition().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.getDisposition());
		}
		if (prospectCallSearchDTO.getSubStatus() != null && !prospectCallSearchDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
		}
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		return criteria;
	}

	private Criteria buildProspectCallCriteriaBySecQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Criteria criteria = new Criteria();

		/**
		 * DATE :- 02/01/2020 Added ANSWERING M/C and GK ANSWERING M/C options in
		 * leadstatus list to fetch answering machine records.
		 */
		if (prospectCallSearchDTO.getLeadStatus() != null && !prospectCallSearchDTO.getLeadStatus().isEmpty()) {
			if (prospectCallSearchDTO.getLeadStatus().contains("GK_ANSWERMACHINE")
					&& prospectCallSearchDTO.getLeadStatus().contains("ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("GATEKEEPER_ANSWERMACHINE", "ANSWERMACHINE");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("GK_ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("GATEKEEPER_ANSWERMACHINE");
			} else if (prospectCallSearchDTO.getLeadStatus().contains("ANSWERMACHINE")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("DIALERCODE");
				criteria = criteria.and("prospectCall.subStatus").in("ANSWERMACHINE");
			}  else if (prospectCallSearchDTO.getLeadStatus().contains("NOT_DISPOSED")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("NOT_DISPOSED");
				criteria = criteria.and("prospectCall.subStatus").in("NOT_DISPOSED");
			}  else if (prospectCallSearchDTO.getLeadStatus().contains("CALLBACK")) {
				criteria = criteria.and("prospectCall.dispositionStatus").in("CALLBACK");
//				criteria = criteria.and("prospectCall.subStatus").in("CALLBACK");
			} else {
				criteria = Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
				criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
			}
		} else if (prospectCallSearchDTO.getDisposition() != null && !prospectCallSearchDTO.getDisposition().isEmpty()
				&& prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 0) {
			criteria = Criteria.where("prospectCall.dispositionStatus").in("FAILURE","AUTO");
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
			// criteria = andCriteria2;
		} else {
			ArrayList<String> leadStatuses = new ArrayList<String>();
			leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString());
			leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString());
			// leadStatuses.add(XtaasConstants.LEAD_STATUS.ACCEPTED.toString());
			/*
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.REJECTED.toString());
			 * leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
			 */
			criteria = Criteria.where("prospectCall.leadStatus").in(leadStatuses);
			criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		} else if (campaignIds != null && !campaignIds.isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(campaignIds);
		}
		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}
		// if (prospectCallSearchDTO.getQaIds() != null &&
		// !prospectCallSearchDTO.getQaIds().isEmpty()) {
		// criteria =
		// criteria.and("qaFeedback.qaId").in(prospectCallSearchDTO.getQaIds());
		// }
		if (prospectCallSearchDTO.getPartnerIds() != null && !prospectCallSearchDTO.getPartnerIds().isEmpty()) {
			criteria = criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getPartnerIds());
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}
		if (prospectCallSearchDTO.getSupervisorIds() != null && !prospectCallSearchDTO.getSupervisorIds().isEmpty()) {
			criteria = criteria.and("prospectCall.supervisorId").in(prospectCallSearchDTO.getSupervisorIds());
		}

		Date fromDate = null, toDate = null;
		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		/*
		 * START LAST 72 HOURS SUCCESS DATE : 15/06/2017 REASON : added to fetch last 72
		 * hours SUCCESS records
		 */
		boolean agentIds = false;
		boolean teamIds = false;
		boolean disposition = false;
		boolean subStatus = false;
		boolean prospectCallIds = false;

		if (prospectCallSearchDTO.getAgentIds() == null || prospectCallSearchDTO.getAgentIds().isEmpty()) {
			agentIds = true;
		}
		if (prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) {
			teamIds = true;
		}
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			disposition = true;
		}
		if (prospectCallSearchDTO.getSubStatus() == null || prospectCallSearchDTO.getSubStatus().isEmpty()) {
			subStatus = true;
		}
		if (prospectCallSearchDTO.getProspectCallIds() == null
				|| prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			prospectCallIds = true;
		}

		// if (agentIds && teamIds && prospectCallIds && disposition && subStatus
		// && prospectCallSearchDTO.getCallDurationRangeEnd() == null
		// && prospectCallSearchDTO.getCallDurationRangeStart() == null
		// && prospectCallSearchDTO.getCampaignId() == null &&
		// prospectCallSearchDTO.getClientId() == null
		// && prospectCallSearchDTO.getEndHour() == null &&
		// prospectCallSearchDTO.getEndMinute() == 0
		// && prospectCallSearchDTO.getFromDate() == null &&
		// prospectCallSearchDTO.getProspectCallStatus() == null
		// && prospectCallSearchDTO.getStartHour() == null &&
		// prospectCallSearchDTO.getStartMinute() == 0
		// && prospectCallSearchDTO.getToDate() == null) {
		// criteria = criteria.and("prospectCall.dispositionStatus").in("SUCCESS");
		// }

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate == null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			// DateTime endDate = getDateTime(toDate, 23, 59,
			// prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate);
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		/*
		 * if (prospectCallSearchDTO.getDisposition() != null &&
		 * !prospectCallSearchDTO.getDisposition().isEmpty()) { criteria =
		 * criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.
		 * getDisposition()); }
		 */
		/*
		 * if ((prospectCallSearchDTO.getLeadStatus() == null ||
		 * prospectCallSearchDTO.getLeadStatus().isEmpty()) &&
		 * prospectCallSearchDTO.getSubStatus() != null &&
		 * !prospectCallSearchDTO.getSubStatus().isEmpty()) { criteria =
		 * criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus(
		 * )); }
		 */
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		// Below criterias for searching leads on qa screen by
		// firstName/lastName/companyName/firstname and lastName both.
		if (prospectCallSearchDTO.getQaSearchString() != null && !prospectCallSearchDTO.getQaSearchString().isEmpty()
				&& prospectCallSearchDTO.getQaSearchString() != "") {
			List<Criteria> orCriteriaList = new ArrayList<>();
			List<Criteria> andCriteriaList = new ArrayList<>();
			String[] names = prospectCallSearchDTO.getQaSearchString().split("\\s+");
			if (encryptedMongoConnection.equalsIgnoreCase("false")) {
				orCriteriaList.add(Criteria.where("prospectCall.prospect.firstName")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.lastName")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.company")
						.regex(prospectCallSearchDTO.getQaSearchString(), "i"));
				if (names != null && names.length > 1) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").regex(names[0], "i"));
					andCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").regex(names[1], "i"));
					orCriteriaList.addAll(andCriteriaList);
				}
				criteria = criteria.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
			} else {
				orCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").is(prospectCallSearchDTO.getQaSearchString()));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").is(prospectCallSearchDTO.getQaSearchString()));
				orCriteriaList.add(Criteria.where("prospectCall.prospect.company").is(prospectCallSearchDTO.getQaSearchString()));
				if (names != null && names.length > 1) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.firstName").is(names[0]));
					andCriteriaList.add(Criteria.where("prospectCall.prospect.lastName").is(names[1]));
					orCriteriaList.addAll(andCriteriaList);
				}
				criteria = criteria.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
			}
			
		}
		return criteria;
	}

	private Criteria buildProspectCallCriteriaByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		ArrayList<String> callStatusList = new ArrayList<String>();
		callStatusList.add(ProspectCallStatus.QA_COMPLETE.name());

		Criteria criteria = Criteria.where("prospectCall.agentId").is(agentId);
		// criteria = criteria.and("prospectCall.recordingUrl").exists(true);
		criteria.and("status").in(ProspectCallStatus.QA_COMPLETE.name());

		Date fromDate = null, toDate = null;

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			criteria = criteria.and("qaFeedback.feedbackTime").lte(endDate);
		}
		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()
				&& prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
		}
		return criteria;
	}

	private DateTime getDateTime(Date date, int hour, int minute, String timeZone) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		DateTime dateInClientTZ = new DateTime(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
				cal.get(Calendar.DAY_OF_MONTH), hour, minute, DateTimeZone.forID(timeZone));
		DateTime dateInUTC = XtaasDateUtils.changeTimeZone(dateInClientTZ, DateTimeZone.UTC.getID());
		return dateInUTC;
	}

	@Override
	public boolean updateCallStatus(String prospectCallId, ProspectCallStatus status, String callSid, Date callbackDate,
			Integer callRetryCount, String allocatedAgentId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("prospectCall.prospectCallId").is(prospectCallId));
		Update update = new Update();
		update.set("status", status.name());
		if (callSid != null) {
			update.set("prospectCall.twilioCallSid", callSid);
		}
		if (callbackDate != null) {
			update.set("callbackDate", callbackDate);
		}
		if (callRetryCount != null && callRetryCount > 0) {
			update.set("prospectCall.callRetryCount", callRetryCount);
		}
		if (allocatedAgentId != null) {
			update.set("prospectCall.agentId", allocatedAgentId);
		}

		// set the audit trails fields
		update.set("updatedBy", XtaasUserUtils.getCurrentLoggedInUsername());
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateFirst(query, update, ProspectCallLog.class);

		if (wr.getModifiedCount() == 0) {
			logger.error("updateCallStatus() : Error occurred while updating prospect call record. ProspectCallId: "
					+ prospectCallId + " Error Msg: Modified Records: " + wr.getModifiedCount());
		}

		return (wr.getModifiedCount() != 0); // returns TRUE if no error else False
	}

	@Override
	public boolean updateProspectInteractionSessionId(String prospectCallId, String prospectInteractionSessionId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("prospectCall.prospectCallId").is(prospectCallId));
		Update update = new Update();
		update.set("prospectInteractionSessionId", prospectInteractionSessionId);

		// set the audit trails fields
		update.set("updatedBy", XtaasUserUtils.getCurrentLoggedInUsername());
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateFirst(query, update, ProspectCallLog.class);

		if (wr.getModifiedCount() == 0) {
			logger.error(
					"updateProspectInteractionSessionId() : Error occurred while updating prospect interaction session id. ProspectCallId: "
							+ prospectCallId + " Error Msg: Modified Records: " + wr.getModifiedCount());
		}

		return (wr.getModifiedCount() != 0); 
	}

	// TEST METHOD - to remove ascii chars from database (implemented to fix a PROD
	// issue on 25 Nov 2015)
	public boolean removeNonAsciiCharacters(String prospectCallId, String nonAsciiCompanyName) {
		Query query = new Query();
		query.addCriteria(Criteria.where("prospectCall.prospectCallId").is(prospectCallId));
		Update update = new Update();
		update.set("prospectCall.prospect.company", nonAsciiCompanyName);
		// set the audit trails fields
		update.set("updatedBy", "SYSTEM");
		update.set("updatedDate", new Date());

		UpdateResult wr = mongoTemplate.updateFirst(query, update, ProspectCallLog.class);

		if (wr.getModifiedCount() == 0) {
			logger.error(
					"removeNonAsciiCharacters() : Error occurred while updating non-ascii company. ProspectCallId: "
							+ prospectCallId + " Error Msg: Modified Records: " + wr.getModifiedCount());
		}

		return (wr.getModifiedCount() != 0); 
	}

	// TEST METHOD - to update title from database (implemented to fix a PROD issue
	// on 25 Nov 2015)
	public boolean updateProspectTitle(String sourceId, String title) {
		Query query = new Query();
		query.addCriteria(Criteria.where("prospectCall.prospect.sourceId").is(sourceId));
		Update update = new Update();
		update.set("prospectCall.prospect.title", title);
		// set the audit trails fields
		update.set("updatedBy", "SYSTEM");
		update.set("updatedDate", new Date());

		UpdateResult wr = mongoTemplate.updateFirst(query, update, ProspectCallLog.class);

		if (wr.getModifiedCount() == 0) {
			logger.error(
					"removeNonAsciiCharacters() : Error occurred while updating non-ascii company. ProspectCallId: "
							+ sourceId + " Error Msg: Modified Records: " + wr.getModifiedCount());
		}

		return (wr.getModifiedCount() != 0); 
	}

	private Criteria buildProspectCallMetricsCriteriaByQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		ArrayList<String> callStatusList = new ArrayList<String>();
		callStatusList.add(ProspectCallStatus.WRAPUP_COMPLETE.name());
		callStatusList.add(ProspectCallStatus.QA_INITIATED.name());
		callStatusList.add(ProspectCallStatus.QA_COMPLETE.name());
		callStatusList.add(ProspectCallStatus.SECONDARY_QA_INITIATED.name());

		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignIds);
		// criteria = criteria.and("prospectCall.recordingUrl").exists(true);

		if (prospectCallSearchDTO.getProspectCallStatus() == null
				|| prospectCallSearchDTO.getProspectCallStatus().isEmpty()) {
			criteria.and("status").in(callStatusList);
		} else {
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress")) {
				criteria.and("status").in(ProspectCallStatus.QA_INITIATED.name());
			}
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")) {
				criteria.and("status").in(ProspectCallStatus.QA_COMPLETE.name());
			}
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Not Scored")) {
				criteria.and("status").in(ProspectCallStatus.WRAPUP_COMPLETE.name());
			}
			/*
			 * START SECONDARY_QA_INITIATED DATE : 05/06/2017 REASON : Added to fetch
			 * prospectCallLog records for QA review with status as SECONDARY_QA_INITIATED
			 */
			if (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("SECONDARY_QA_INITIATED")) {
				criteria.and("status").in(ProspectCallStatus.SECONDARY_QA_INITIATED.name());
			}
			/* END SECONDARY_QA_INITIATED */
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}
		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}
		Date fromDate = null, toDate = null;

		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (prospectCallSearchDTO.getDisposition() != null && !prospectCallSearchDTO.getDisposition().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in(prospectCallSearchDTO.getDisposition());
		}
		if (prospectCallSearchDTO.getSubStatus() != null && !prospectCallSearchDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
		}
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		return criteria;
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByDispositionStatus(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(Aggregation.match(buildProspectCallMetricsCriteriaByQa(campaignIds, prospectCallSearchDTO)));
		aggregationOperations.add(buildDispositionStatusGroup());

		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", Object.class);
		List<Object> dispositionStatusMetrics = results.getMappedResults();
		if (!dispositionStatusMetrics.isEmpty()) {
			// construct QA metrics of failure, callback , success, dialer-code disposition
			// status
			HashMap<QaMetrics, Integer> metrics = new HashMap<QaMetrics, Integer>();
			for (Object dispositionStatusMetric : dispositionStatusMetrics) {
				LinkedHashMap<?, ?> dispositionStatusMetricsMap = (LinkedHashMap<?, ?>) dispositionStatusMetric;
				String dispositionStatus = (String) dispositionStatusMetricsMap.get("_id");
				Integer dispositionStatusCount = (Integer) dispositionStatusMetricsMap.get("count");
				if (dispositionStatus != null) {
					if (dispositionStatus.equals(DispositionType.FAILURE.name())) {
						metrics.put(QaMetrics.COUNT_FAILURE_PROSPECTCALL, dispositionStatusCount);
					}
					if (dispositionStatus.equals(DispositionType.CALLBACK.name())) {
						metrics.put(QaMetrics.COUNT_CALLBACK_PROSPECTCALL, dispositionStatusCount);
					}
					if (dispositionStatus.equals(DispositionType.DIALERCODE.name())) {
						metrics.put(QaMetrics.COUNT_DIALERCODE_PROSPECTCALL, dispositionStatusCount);
					}
					if (dispositionStatus.equals(DispositionType.SUCCESS.name())) {
						metrics.put(QaMetrics.COUNT_SUCCESS_PROSPECTCALL, dispositionStatusCount);
					}
				}
			}
			return metrics;
		}
		return null;
	}

	private AggregationOperation buildDispositionStatusGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.dispositionStatus");
		group = group.append("count", new BasicDBObject("$sum", 1));

		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByProspectCallStatus(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(Aggregation.match(buildProspectCallMetricsCriteriaByQa(campaignIds, prospectCallSearchDTO)));
		aggregationOperations.add(buildProspectCallStatusGroup());

		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", Object.class);
		List<Object> prospectCallStatusMetrics = results.getMappedResults();
		if (!prospectCallStatusMetrics.isEmpty()) {
			// construct QA metrics of In progress, not scored , completeD prospect call
			// status
			HashMap<QaMetrics, Integer> metrics = new HashMap<QaMetrics, Integer>();
			int qacount = 0;
			int secqacount = 0;
			for (Object prospectCallStatusMetric : prospectCallStatusMetrics) {
				LinkedHashMap<?, ?> prospectCallStatusMetricsMap = (LinkedHashMap<?, ?>) prospectCallStatusMetric;
				String status = (String) prospectCallStatusMetricsMap.get("_id");
				Integer statusCount = (Integer) prospectCallStatusMetricsMap.get("count");
				if (status.equals(ProspectCallStatus.QA_COMPLETE.name())) {
					metrics.put(QaMetrics.COUNT_COMPLETE_PROSPECTCALL, statusCount);
				}
				if (status.equals(ProspectCallStatus.WRAPUP_COMPLETE.name())) {
					metrics.put(QaMetrics.COUNT_NOT_SCORED_PROSPECTCALL, statusCount);
				}
				if (status.equals(ProspectCallStatus.QA_INITIATED.name())) {
					qacount = statusCount;
				}
				if (status.equals(ProspectCallStatus.SECONDARY_QA_INITIATED.name())) {
					secqacount = statusCount;
				}
				/*
				 * if (status.equals(ProspectCallStatus.QA_INITIATED.name()) ||
				 * status.equals(ProspectCallStatus.SECONDARY_QA_INITIATED.name())) {
				 * //metrics.put(QaMetrics.COUNT_IN_PROGRESS_PROSPECTCALL,statusCount); }
				 */
			}
			metrics.put(QaMetrics.COUNT_IN_PROGRESS_PROSPECTCALL, (qacount + secqacount));
			return metrics;
		}
		return null;
	}

	private AggregationOperation buildProspectCallStatusGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$status");
		group = group.append("count", new BasicDBObject("$sum", 1));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate(List<String> campaignIds) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(Aggregation.match(Criteria.where("prospectCall.campaignId").in(campaignIds)));
		aggregationOperations.add(buildScoredRateGroup());
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", Object.class);
		List<Object> scoreRateMetrics = results.getMappedResults();
		if (!scoreRateMetrics.isEmpty()) {
			// construct QA metrics of previous week peding prospect calls, last 24 hours
			// completed prospect calls, last 48 hours prospect calls,
			// last 24 hours reviewed prospect calls and total prospect calls
			HashMap<QaMetrics, Integer> metrics = new HashMap<QaMetrics, Integer>();
			LinkedHashMap<?, ?> scoreRateMetric = (LinkedHashMap<?, ?>) scoreRateMetrics.get(0);

			metrics.put(QaMetrics.QA_PENDING_PROSPECTCALL_COUNT_LAST_WEEK,
					(Integer) scoreRateMetric.get(QaMetrics.QA_PENDING_PROSPECTCALL_COUNT_LAST_WEEK.name()));
			metrics.put(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS,
					(Integer) scoreRateMetric.get(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS.name()));
			metrics.put(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS,
					(Integer) scoreRateMetric.get(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS.name()));
			metrics.put(QaMetrics.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS,
					(Integer) scoreRateMetric.get(QaMetrics.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS.name()));
			metrics.put(QaMetrics.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS,
					(Integer) scoreRateMetric.get(QaMetrics.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS.name()));
			return metrics;
		}
		return null;
	}

	private AggregationOperation buildScoredRateGroup() {
		Calendar cal = Calendar.getInstance();// Get calendar of current date and time

		int week = cal.get(Calendar.WEEK_OF_YEAR);// get current week number in year
		int hour = cal.get(Calendar.HOUR_OF_DAY);// get current hour (24 hours format) in day
		int minute = cal.get(Calendar.MINUTE);// get current minute in day

		Interval previousWeekInterval = XtaasDateUtils.getWeekIntervalInTZ(week - 1, null);// get Interval of last week
																							// from SUNDAY to SATURADY
		Date yesterdaysDate = XtaasDateUtils.getYesterdaysDate(hour, minute);// get date before 24 hours
		Date dayBeforeYesterdayDate = XtaasDateUtils.getDayBeforeYesterdayDate(hour, minute);// get date before 48hours

		BasicDBObject group = new BasicDBObject("_id", null);
		group = group
				.append(QaMetrics.QA_PENDING_PROSPECTCALL_COUNT_LAST_WEEK.name(),
						new BasicDBObject("$sum",
								new BasicDBObject("$cond",
										new Object[] {
												new BasicDBObject("$and",
														new Object[] {
																new BasicDBObject("$gte",
																		new Object[] { "$prospectCall.callStartTime",
																				new Date(previousWeekInterval
																						.getStartMillis()) }),
																new BasicDBObject(
																		"$lte",
																		new Object[] {
																				"$prospectCall.callStartTime",
																				new Date(previousWeekInterval
																						.getEndMillis()) }),
																new BasicDBObject("$eq", new Object[] { "$status",
																		"WRAPUP_COMPLETE" }) }),
												1, 0 })));

		group = group.append(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS.name(),
				new BasicDBObject("$sum",
						new BasicDBObject("$cond",
								new Object[] {
										new BasicDBObject("$and",
												new Object[] {
														new BasicDBObject("$gte",
																new Object[] { "$qaFeedback.feedbackTime",
																		yesterdaysDate }),
														new BasicDBObject(
																"$lte",
																new Object[] { "$qaFeedback.feedbackTime",
																		new Date() }),
														new BasicDBObject("$eq",
																new Object[] { "$status", "QA_COMPLETE" }) }),
										1, 0 })));

		group = group.append(QaMetrics.QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS.name(),
				new BasicDBObject("$sum",
						new BasicDBObject("$cond", new Object[] {
								new BasicDBObject("$and",
										new Object[] {
												new BasicDBObject("$gte",
														new Object[] { "$qaFeedback.feedbackTime",
																dayBeforeYesterdayDate }),
												new BasicDBObject("$lte",
														new Object[] { "$qaFeedback.feedbackTime", new Date() }),
												new BasicDBObject("$eq", new Object[] { "$status", "QA_COMPLETE" }) }),
								1, 0 })));

		group = group.append(QaMetrics.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS.name(), new BasicDBObject("$sum",
				new BasicDBObject("$cond", new Object[] { new BasicDBObject("$and", new Object[] {
						new BasicDBObject("$gte", new Object[] { "$qaFeedback.feedbackTime", yesterdaysDate }),
						new BasicDBObject("$lte", new Object[] { "$qaFeedback.feedbackTime", new Date() }),
						new BasicDBObject("$eq", new Object[] { "$status", "QA_COMPLETE" }), new BasicDBObject("$eq",
								new Object[] { "$qaFeedback.qaId", XtaasUserUtils.getCurrentLoggedInUsername() }) // QAId
				}), 1, 0 })));
		group = group
				.append(QaMetrics.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS.name(),
						new BasicDBObject("$sum",
								new BasicDBObject("$cond",
										new Object[] {
												new BasicDBObject("$and",
														new Object[] {
																new BasicDBObject("$gte",
																		new Object[] { "$prospectCall.callStartTime",
																				yesterdaysDate }),
																new BasicDBObject("$lte",
																		new Object[] { "$prospectCall.callStartTime",
																				new Date() }),
																new BasicDBObject("$or",
																		new Object[] {
																				new BasicDBObject("$eq",
																						new Object[] { "$status",
																								"QA_COMPLETE" }),
																				new BasicDBObject("$eq",
																						new Object[] { "$status",
																								"WRAPUP_COMPLETE" }),
																				new BasicDBObject("$eq",
																						new Object[] { "$status",
																								"QA_INITIATED" }) }) }),
												1, 0 })));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	public HashSet<String> findCallableOrganizations(String campaignId, List<String> organizationNames) {
		List<String> callStatus = new ArrayList<String>();
		callStatus.add("CALLBACK");
		callStatus.add("DIALERCODE");
		BasicDBObject elemMatch = new BasicDBObject();
		elemMatch.put("prospectCall.prospect.company", new BasicDBObject("$in", organizationNames));
		elemMatch.put("prospectCall.dispositionStatus", new BasicDBObject("$in", callStatus));
		// List<String> callableList = mongoTemplate.getCollection("prospectcalllog")
		// 		.distinct("prospectCall.prospect.company", elemMatch);
		DistinctIterable<String> iterable = mongoTemplate.getCollection("prospectcalllog")
				.distinct("prospectCall.prospect.company", elemMatch, String.class);
		MongoCursor<String> cursor = iterable.iterator();
		List<String> callableList = new ArrayList<>();
		while (cursor.hasNext()) {
			callableList.add(cursor.next());
		}
		return new HashSet<String>(callableList);
	}

	@Override
	public HashMap<String, HashMap<String, Integer>> countRecycleRecords(Map<String, List<String>> stringClauses,
			String campaignId) {
		List<String> uniqueDBRecords = validateRecord(campaignId);
		boolean isWithInCampaign = true;
		boolean accrossCampaign = false;
		HashMap<String, HashMap<String, Integer>> map = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, Integer> recycledRecordInsideCampaignMap = getRecycleRecords(stringClauses, campaignId,
				isWithInCampaign, uniqueDBRecords);
		HashMap<String, Integer> recycledRecordAccrossPlateformMap = getRecycleRecords(stringClauses, campaignId,
				accrossCampaign, uniqueDBRecords);
		map.put("withInCampaign", recycledRecordInsideCampaignMap);
		map.put("acrossCampaign", recycledRecordAccrossPlateformMap);
		return map;
	}
	
	public List<ProspectCallLog> getProspectByName(String campaignId, String fname, String lname){
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = new Criteria();
		criterias.add(Criteria.where("prospectCall.campaignId").in(campaignId));
		//criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		if(fname!=null && !fname.isEmpty()) {
			criterias.add(Criteria.where("prospectCall.prospect.firstName").regex(fname, "i"));
			//criteria.and("prospectCall.campaignId").in(campaignId);
		}
		if(lname!=null && !lname.isEmpty()) {
			criterias.add(Criteria.where("prospectCall.prospect.lastName").regex(lname, "i"));
		}
		Query searchQuery = Query
				.query(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()])));
		return mongoTemplate.find(searchQuery, ProspectCallLog.class);

	}
	
	public List<ProspectCallLog> getProspectByFirstNameOrLastName(String campaignId, String fname, String lname){
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = new Criteria();
		criterias.add(Criteria.where("prospectCall.campaignId").in(campaignId));
		//criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		if(fname!=null && !fname.isEmpty()) {
			criterias.add(Criteria.where("prospectCall.prospect.firstName").in(fname));
			//criteria.and("prospectCall.campaignId").in(campaignId);
		}
		if(lname!=null && !lname.isEmpty()) {
			criterias.add(Criteria.where("prospectCall.prospect.lastName").in(lname));
		}
		Query searchQuery = Query
				.query(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()])));
		return mongoTemplate.find(searchQuery, ProspectCallLog.class);

	}

	private Criteria buildBasicMatch(List<String> states, List<String> industries, List<String> functions,
			List<String> mgmtLevels, List<String> titles, Integer minimumRevenue, Integer maximumRevenue,
			Integer minimumEmployeeCount, Integer maximumEmployeeCount, String campaignId, String status,
			String subStatus, String dispositionStatus, boolean isWithInCampaign) {

		Campaign campaign = campaignRepository.findOneById(campaignId);
		int maxCallRetryLimit = 5;
		if (campaign != null && campaign.getCallMaxRetries() > 0) {
			maxCallRetryLimit = campaign.getCallMaxRetries(); // get call max retry count from campaign
		} else {
			maxCallRetryLimit = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
		}
		List<Criteria> criterias = new ArrayList<Criteria>();

		Criteria criteria = new Criteria();
		if (isWithInCampaign) {
			criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		}
		if (!isWithInCampaign) {
			criteria = Criteria.where("prospectCall.campaignId").nin(campaignId);
		}
		if (industries != null && industries.size() != 0) {
			criterias.add(Criteria.where("prospectCall.prospect.industry").in(industries));
		}
		if (states != null && states.size() != 0) {
			criterias.add(Criteria.where("prospectCall.prospect.stateCode").in(states));
		}
		if (functions != null && functions.size() != 0) {
			criterias.add(Criteria.where("prospectCall.prospect.department").in(functions));
		}
		if (mgmtLevels != null && mgmtLevels.size() != 0) {
			criterias.add(Criteria.where("prospectCall.prospect.managementLevel").in(mgmtLevels));
		}
		/*
		 * if (titles != null && titles.size() != 0) {
		 * criterias.add(Criteria.where("prospectCall.prospect.title").in(titles)); }
		 */
		if (titles != null && titles.size() != 0) {
			criterias.add(Criteria.where("prospectCall.prospect.title").regex(titles.get(0), "i"));
		}
		if (minimumRevenue > 0) {
			criterias.add(Criteria.where("prospectCall.prospect.customAttributes.minRevenue").gte(minimumRevenue));
		}
		if (maximumRevenue > 0) {
			criterias.add(Criteria.where("prospectCall.prospect.customAttributes.maxRevenue").lte(maximumRevenue));
		}
		if (minimumEmployeeCount > 0) {
			criterias.add(Criteria.where("prospectCall.prospect.customAttributes.minEmployeeCount")
					.gte(minimumEmployeeCount));
		}
		if (maximumEmployeeCount > 0) {
			criterias.add(Criteria.where("prospectCall.prospect.customAttributes.maxEmployeeCount")
					.lte(maximumEmployeeCount));
		}
		if (status != null) {
			criterias.add(Criteria.where("status").is(status));
		}
		if (subStatus != null) {
			criterias.add(Criteria.where("prospectCall.subStatus").is(subStatus));
		}
		if (dispositionStatus != null) {
			criterias.add(Criteria.where("prospectCall.dispositionStatus").is(dispositionStatus));
		}
		if (isWithInCampaign) {
			criteria = Criteria.where("prospectCall.callRetryCount").gte(maxCallRetryLimit);
		}
		return criteria.andOperator(criterias.toArray(new Criteria[0]));
	}

	public HashMap<String, Integer> getRecycleRecords(Map<String, List<String>> stringClauses, String campaignId,
			boolean isWithInCampaign, List<String> uniqueDBRecords) {
		HashMap<String, Integer> recycledRecordMap = new HashMap<String, Integer>();
		List<String> industries = null;
		List<String> states = null;
		List<String> functions = null;
		List<String> mgmtLevels = null;
		List<String> titles = null;
		Integer minimumRevenue = 0;
		Integer maximumRevenue = 0;
		Integer minimumEmployeeCount = 0;
		Integer maximumEmployeeCount = 0;
		if (stringClauses.containsKey("industry")) {
			industries = stringClauses.get("industry");
		}
		if (stringClauses.containsKey("department")) {
			functions = stringClauses.get("department");
		}
		if (stringClauses.containsKey("mgmtLevel")) {
			mgmtLevels = stringClauses.get("mgmtLevel");
		}
		if (stringClauses.containsKey("title")) {
			titles = stringClauses.get("title");
		}
		if (stringClauses.containsKey("revMin")) {
			minimumRevenue = Integer.valueOf(stringClauses.get("revMin").get(0));
		}
		if (stringClauses.containsKey("revMax")) {
			maximumRevenue = Integer.valueOf(stringClauses.get("revMax").get(0));
		}
		if (stringClauses.containsKey("empMin")) {
			minimumEmployeeCount = Integer.valueOf(stringClauses.get("empMin").get(0));
		}
		if (stringClauses.containsKey("empMax")) {
			maximumEmployeeCount = Integer.valueOf(stringClauses.get("empMax").get(0));
		}
		if (stringClauses.containsKey("state")) {
			states = stringClauses.get("state");
		}
		Map<String, List<ProspectCallLog>> recycledList = dataList(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
				isWithInCampaign, uniqueDBRecords);
		List<ProspectCallLog> queuedList = recycledList.get("QUEUED");
		List<ProspectCallLog> unknownErrorList = recycledList.get("UNKNOWN_ERROR");
		List<ProspectCallLog> maxRetryLimitList = recycledList.get("MAX_RETRY_LIMIT_REACHED");
		List<ProspectCallLog> callAbandonedList = recycledList.get("CALL_ABANDONED");
		List<ProspectCallLog> successList = recycledList.get("SUCCESS");
		List<ProspectCallLog> failureList = recycledList.get("FAILURE");
		List<ProspectCallLog> callbackList = recycledList.get("CALLBACK");
		List<ProspectCallLog> noAnswerList = recycledList.get("NOANSWER");
		List<ProspectCallLog> answermachineList = recycledList.get("ANSWERMACHINE");
		List<ProspectCallLog> dialermisdetectList = recycledList.get("DIALERMISDETECT");
		List<ProspectCallLog> gatekeeperAMList = recycledList.get("GATEKEEPER_ANSWERMACHINE");
		List<ProspectCallLog> busyList = recycledList.get("BUSY");
		List<ProspectCallLog> deadAirList = recycledList.get("DEADAIR");

		recycledRecordMap.put("QUEUED", queuedList.size());
		recycledRecordMap.put("UNKNOWN_ERROR", unknownErrorList.size());
		recycledRecordMap.put("MAX_RETRY_LIMIT_REACHED", maxRetryLimitList.size());
		recycledRecordMap.put("CALL_ABANDONED", callAbandonedList.size());
		recycledRecordMap.put("SUCCESS", successList.size());
		recycledRecordMap.put("FAILURE", failureList.size());
		recycledRecordMap.put("CALLBACK", callbackList.size());
		recycledRecordMap.put("NOANSWER", noAnswerList.size());
		recycledRecordMap.put("ANSWERMACHINE", answermachineList.size());
		recycledRecordMap.put("DIALERMISDETECT", dialermisdetectList.size());
		recycledRecordMap.put("GATEKEEPER_ANSWERMACHINE", gatekeeperAMList.size());
		recycledRecordMap.put("BUSY", busyList.size());
		recycledRecordMap.put("DEADAIR", deadAirList.size());
		return recycledRecordMap;
	}

	private List<ProspectCallLog> getDistinctResult(List<ProspectCallLog> recordList, List<String> uniqueDBRecords,
			boolean isWithInCampaign) {
		List<ProspectCallLog> uniqueRecordList = new ArrayList<ProspectCallLog>();
		ArrayList<String> combinedList = new ArrayList<String>();
		if (recordList != null && recordList.size() > 0) {
			for (ProspectCallLog prospectCallLog : recordList) {
				Prospect prospect = prospectCallLog.getProspectCall().getProspect();
				String combineRecord = prospect.getFirstName() + "@" + prospect.getLastName() + "@"
						+ prospect.getTitle() + "@" + prospect.getCompany() + "@" + prospect.getPhone();
				if (isWithInCampaign) {
					if (!uniqueRecordList.contains(combineRecord)) {
						uniqueRecordList.add(prospectCallLog);
						combinedList.add(combineRecord);
					}
				} else {
					if (!uniqueRecordList.contains(combineRecord) && !uniqueDBRecords.contains(combineRecord)) {
						uniqueRecordList.add(prospectCallLog);
						combinedList.add(combineRecord);
					}
				}
			}

		}
		return uniqueRecordList;
	}

	private Map<String, List<ProspectCallLog>> dataList(List<String> states, List<String> industries,
			List<String> functions, List<String> mgmtLevels, List<String> titles, Integer minimumRevenue,
			Integer maximumRevenue, Integer minimumEmployeeCount, Integer maximumEmployeeCount, String campaignId,
			boolean isWithInCampaign, List<String> uniqueDBRecords) {
		Map<String, List<ProspectCallLog>> mapList = new HashMap<String, List<ProspectCallLog>>();

		// Statue wise
		List<ProspectCallLog> queuedList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
				ProspectCallStatus.QUEUED.name(), null, null, isWithInCampaign);
		mapList.put("QUEUED", getDistinctResult(queuedList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> unknownErrorList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
				ProspectCallStatus.UNKNOWN_ERROR.name(), null, null, isWithInCampaign);
		mapList.put("UNKNOWN_ERROR", getDistinctResult(unknownErrorList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> maxRetryLimitList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
				ProspectCallStatus.MAX_RETRY_LIMIT_REACHED.name(), null, null, isWithInCampaign);
		mapList.put("MAX_RETRY_LIMIT_REACHED", getDistinctResult(maxRetryLimitList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> callAbandonedList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
				ProspectCallStatus.CALL_ABANDONED.name(), null, null, isWithInCampaign);
		mapList.put("CALL_ABANDONED", getDistinctResult(callAbandonedList, uniqueDBRecords, isWithInCampaign));

		// Disposition status
		List<ProspectCallLog> successList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
				DispositionType.SUCCESS.name(), isWithInCampaign);
		mapList.put("SUCCESS", getDistinctResult(successList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> failureList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
				DispositionType.FAILURE.name(), isWithInCampaign);
		mapList.put("FAILURE", getDistinctResult(failureList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> callbackList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
				DispositionType.CALLBACK.name(), isWithInCampaign);
		mapList.put("CALLBACK", getDistinctResult(callbackList, uniqueDBRecords, isWithInCampaign));

		// SubStatus wise
		List<ProspectCallLog> noAnswerList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.NOANSWER.name(), null, isWithInCampaign);
		mapList.put("NOANSWER", getDistinctResult(noAnswerList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> answermachineList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.ANSWERMACHINE.name(), null, isWithInCampaign);
		mapList.put("ANSWERMACHINE", getDistinctResult(answermachineList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> dialermisdetectList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.DIALERMISDETECT.name(), null, isWithInCampaign);
		mapList.put("DIALERMISDETECT", getDistinctResult(dialermisdetectList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> gatekeeperAMList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.GATEKEEPER_ANSWERMACHINE.name(), null, isWithInCampaign);
		mapList.put("GATEKEEPER_ANSWERMACHINE", getDistinctResult(gatekeeperAMList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> busyList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.BUSY.name(), null, isWithInCampaign);
		mapList.put("BUSY", getDistinctResult(busyList, uniqueDBRecords, isWithInCampaign));

		List<ProspectCallLog> deadAirList = searchContacts(states, industries, functions, mgmtLevels, titles,
				minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
				DialerCodeDispositionStatus.DEADAIR.name(), null, isWithInCampaign);
		mapList.put("DEADAIR", getDistinctResult(deadAirList, uniqueDBRecords, isWithInCampaign));

		return mapList;
	}

	public List<ProspectCallLog> searchContacts(List<String> states, List<String> industries, List<String> functions,
			List<String> mgmtLevels, List<String> titles, Integer minimumRevenue, Integer maximumRevenue,
			Integer minimumEmployeeCount, Integer maximumEmployeeCount, String campaignId, String status,
			String subStatus, String dispositionStatus, boolean isWithInCampaign) {
		return mongoTemplate.find(
				Query.query(buildBasicMatch(states, industries, functions, mgmtLevels, titles, minimumRevenue,
						maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, status, subStatus,
						dispositionStatus, isWithInCampaign))
						.withHint("idx_camp_mgmtLvl_revAttrib_EmpCnt_title_industry"),
				ProspectCallLog.class, "prospectcalllog");
	}

	private List<String> validateRecord(String campaignId) {
		List<String> list = new ArrayList<String>();
		List<ProspectCallLog> pList = prospectCallLogRepository.findAllRecordByCampaignId(campaignId);
		for (ProspectCallLog prospectCallLog : pList) {
			Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			String combineRecord = prospect.getFirstName() + "@" + prospect.getLastName() + "@" + prospect.getTitle()
					+ "@" + prospect.getCompany() + "@" + prospect.getPhone();
			list.add(combineRecord);
		}
		return list;
	}

	private HashMap<String, HashMap<String, Boolean>> getMap(List<RecycleStatusDTO> listDTO) {
		HashMap<String, HashMap<String, Boolean>> map = new HashMap<String, HashMap<String, Boolean>>();
		HashMap<String, Boolean> inMap = new HashMap<String, Boolean>();
		HashMap<String, Boolean> outMap = new HashMap<String, Boolean>();
		for (RecycleStatusDTO recycleStatusDTO : listDTO) {
			String status = recycleStatusDTO.getStatus().name();
			inMap.put(status, recycleStatusDTO.isWithinCampaignSelected());
			outMap.put(status, recycleStatusDTO.isAcrossCampaignSelected());
		}
		map.put("withInCampaign", inMap);
		map.put("acrossCampaign", outMap);
		return map;
	}

	@Override
	public void saveRecycleRecords(Map<String, List<String>> stringClauses, String campaignId,
			List<RecycleStatusDTO> listDTOs) {
		List<String> uniqueDBRecords = validateRecord(campaignId);
		HashMap<String, HashMap<String, Boolean>> map = getMap(listDTOs);

		// please handle null condition when map is null

		HashMap<String, Boolean> withInCampaignMap = map.get("withInCampaign");
		HashMap<String, Boolean> acrossCampaignMap = map.get("acrossCampaign");
		List<String> industries = null;
		List<String> states = null;
		List<String> functions = null;
		List<String> mgmtLevels = null;
		List<String> titles = null;
		Integer minimumRevenue = 0;
		Integer maximumRevenue = 0;
		Integer minimumEmployeeCount = 0;
		Integer maximumEmployeeCount = 0;
		if (stringClauses.containsKey("industry")) {
			industries = stringClauses.get("industry");
		}
		if (stringClauses.containsKey("states")) {
			states = stringClauses.get("states");
		}
		if (stringClauses.containsKey("department")) {
			functions = stringClauses.get("department");
		}
		if (stringClauses.containsKey("mgmtLevel")) {
			mgmtLevels = stringClauses.get("mgmtLevel");
		}
		if (stringClauses.containsKey("title")) {
			titles = stringClauses.get("title");
		}
		if (stringClauses.containsKey("revMin")) {
			minimumRevenue = Integer.valueOf(stringClauses.get("revMin").get(0));
		}
		if (stringClauses.containsKey("revMax")) {
			maximumRevenue = Integer.valueOf(stringClauses.get("revMax").get(0));
		}
		if (stringClauses.containsKey("empMin")) {
			minimumEmployeeCount = Integer.valueOf(stringClauses.get("empMin").get(0));
		}
		if (stringClauses.containsKey("empMax")) {
			maximumEmployeeCount = Integer.valueOf(stringClauses.get("empMax").get(0));
		}
		boolean isWithInCampaign = true;
		if (isWithInCampaign) {
			Map<String, List<ProspectCallLog>> recycledList = dataListForRecyclePurchased(states, industries, functions,
					mgmtLevels, titles, minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount,
					campaignId, isWithInCampaign, withInCampaignMap, uniqueDBRecords);
			List<ProspectCallLog> queuedList = recycledList.get("QUEUED");
			saveRecordsInDBWithInCampaign(queuedList, campaignId);
			List<ProspectCallLog> maxRetryLimitList = recycledList.get("MAX_RETRY_LIMIT_REACHED");
			saveRecordsInDBWithInCampaign(maxRetryLimitList, campaignId);
			List<ProspectCallLog> unknowErrorList = recycledList.get("UNKNOWN_ERROR");
			saveRecordsInDBWithInCampaign(unknowErrorList, campaignId);
			List<ProspectCallLog> callAbandonedList = recycledList.get("CALL_ABANDONED");
			saveRecordsInDBWithInCampaign(callAbandonedList, campaignId);
			List<ProspectCallLog> failureList = recycledList.get("FAILURE");
			saveRecordsInDBWithInCampaign(failureList, campaignId);
			List<ProspectCallLog> callbackList = recycledList.get("CALLBACK");
			saveRecordsInDBWithInCampaign(callbackList, campaignId);
			List<ProspectCallLog> noAnswerList = recycledList.get("NOANSWER");
			saveRecordsInDBWithInCampaign(noAnswerList, campaignId);
			List<ProspectCallLog> answermachineList = recycledList.get("ANSWERMACHINE");
			saveRecordsInDBWithInCampaign(answermachineList, campaignId);
			List<ProspectCallLog> dialermisdetectList = recycledList.get("DIALERMISDETECT");
			saveRecordsInDBWithInCampaign(dialermisdetectList, campaignId);
			List<ProspectCallLog> gatekeeperAMList = recycledList.get("GATEKEEPER_ANSWERMACHINE");
			saveRecordsInDBWithInCampaign(gatekeeperAMList, campaignId);
			List<ProspectCallLog> busyList = recycledList.get("BUSY");
			saveRecordsInDBWithInCampaign(busyList, campaignId);
			List<ProspectCallLog> deadAirList = recycledList.get("DEADAIR");
			saveRecordsInDBWithInCampaign(deadAirList, campaignId);
			isWithInCampaign = false;
		}
		if (!isWithInCampaign) {
			Map<String, List<ProspectCallLog>> recycledList = dataListForRecyclePurchased(states, industries, functions,
					mgmtLevels, titles, minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount,
					campaignId, isWithInCampaign, acrossCampaignMap, uniqueDBRecords);
			List<ProspectCallLog> queuedList = recycledList.get("QUEUED");
			saveRecordsInDBAcrossCampaign(queuedList, campaignId);
			List<ProspectCallLog> unknowErrorList = recycledList.get("UNKNOWN_ERROR");
			saveRecordsInDBAcrossCampaign(unknowErrorList, campaignId);
			List<ProspectCallLog> maxRetryLimitList = recycledList.get("MAX_RETRY_LIMIT_REACHED");
			saveRecordsInDBAcrossCampaign(maxRetryLimitList, campaignId);
			List<ProspectCallLog> callAbandonedList = recycledList.get("CALL_ABANDONED");
			saveRecordsInDBAcrossCampaign(callAbandonedList, campaignId);
			List<ProspectCallLog> successList = recycledList.get("SUCCESS");
			saveRecordsInDBAcrossCampaign(successList, campaignId);
			List<ProspectCallLog> failureList = recycledList.get("FAILURE");
			saveRecordsInDBAcrossCampaign(failureList, campaignId);
			List<ProspectCallLog> callbackList = recycledList.get("CALLBACK");
			saveRecordsInDBAcrossCampaign(callbackList, campaignId);
			List<ProspectCallLog> noAnswerList = recycledList.get("NOANSWER");
			saveRecordsInDBAcrossCampaign(noAnswerList, campaignId);
			List<ProspectCallLog> answermachineList = recycledList.get("ANSWERMACHINE");
			saveRecordsInDBAcrossCampaign(answermachineList, campaignId);
			List<ProspectCallLog> dialermisdetectList = recycledList.get("DIALERMISDETECT");
			saveRecordsInDBAcrossCampaign(dialermisdetectList, campaignId);
			List<ProspectCallLog> gatekeeperAMList = recycledList.get("GATEKEEPER_ANSWERMACHINE");
			saveRecordsInDBAcrossCampaign(gatekeeperAMList, campaignId);
			List<ProspectCallLog> busyList = recycledList.get("BUSY");
			saveRecordsInDBAcrossCampaign(busyList, campaignId);
			List<ProspectCallLog> deadAirList = recycledList.get("DEADAIR");
			saveRecordsInDBAcrossCampaign(deadAirList, campaignId);
		}

	}

	private void saveRecordsInDBAcrossCampaign(List<ProspectCallLog> prospectCallLogList, String campaignId) {
		CampaignContact tempCampaignContact = null;
		Map<String, String> srcIndustryMap = new HashMap<String, String>();
		List<ContactFunction> functions = null;
		String source = "RECYCLE0";
		Campaign campaign = campaignService.getCampaign(campaignId);
		Set<CampaignContact> campaignContactList = new HashSet<CampaignContact>();
		List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
		List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();
		List<PCIAnalysis> pcianList = new ArrayList<PCIAnalysis>();
		if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
			for (ProspectCallLog ProspectCallLog : prospectCallLogList) {
				Double minrev = 0.0;
				Double maxrev = 0.0;
				Double minemp = 0.0;
				Double maxemp = 0.0;
				Prospect prospect = ProspectCallLog.getProspectCall().getProspect();
				if (null != prospect.getCustomAttributeValue("minRevenue")) {
					minrev = Double.parseDouble(prospect.getCustomAttributeValue("minRevenue").toString());
				}
				if (null != prospect.getCustomAttributeValue("maxRevenue")) {
					maxrev = Double.parseDouble(prospect.getCustomAttributeValue("maxRevenue").toString());
				}
				if (null != prospect.getCustomAttributeValue("minEmployeeCount")) {
					minemp = Double.parseDouble(prospect.getCustomAttributeValue("minEmployeeCount").toString());
				}
				if (null != prospect.getCustomAttributeValue("maxEmployeeCount")) {
					maxemp = Double.parseDouble(prospect.getCustomAttributeValue("maxEmployeeCount").toString());
				}
				tempCampaignContact = new CampaignContact(campaignId, source, prospect.getSourceId(),
						prospect.getSuffix(), prospect.getFirstName(), prospect.getLastName(), prospect.getSuffix(),
						prospect.getTitle(), prospect.getDepartment(), functions, prospect.getCompany(),
						(String) prospect.getCustomAttributeValue("domain"), getIndustires(prospect.getIndustryList()),
						prospect.getTopLevelIndustries(), minrev, maxrev, minemp, maxemp, prospect.getEmail(),
						prospect.getAddressLine1(), prospect.getAddressLine2(), prospect.getCity(),
						prospect.getStateCode(), prospect.getCountry(), prospect.getZipCode(), prospect.getPhone(),
						prospect.getCompanyPhone(), prospect.getSourceCompanyId(), prospect.getManagementLevel(),
						prospect.getCompanyAddress(), campaign.isEncrypted());
				srcIndustryMap.put(prospect.getSourceId(), prospect.getIndustry());
				campaignContactList.add(tempCampaignContact);
			}

			List<CampaignContact> campaignContacts = campaignContactRepository.saveAll(campaignContactList);
			for (CampaignContact campaignContact : campaignContacts) {
				String industry = srcIndustryMap.get(campaignContact.getSourceId());
				Prospect prospect = campaignContact.toProspect(industry);
				prospect.setSource(campaignContact.getSource());
				prospect.setSourceId(campaignContact.getSourceId());
				prospect.setCampaignContactId(campaignContact.getId());
				ProspectCallLog prospectCallLog = new ProspectCallLog();
				ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaignId, prospect);
				prospectCallLog.setProspectCall(prospectCall);
				prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
				prospectCallLogs.add(prospectCallLog);
				ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
				prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
				prospectCallInteraction.setProspectCall(prospectCall);
				prospectCallInteractions.add(prospectCallInteraction);
				PCIAnalysis pcin = new PCIAnalysis();
				// Campaign campaign = campaignService.getCampaign(campaignId);
				pcin = pcin.toPCIAnalysis(pcin, prospectCallInteraction, null, null, campaign);
				pcianList.add(pcin);
				//domoUtilsService.postPciAnalysisWebhook(pcin);

			}
			prospectCallLogRepository.saveAll(prospectCallLogs);
			prospectCallInteractionRepository.saveAll(prospectCallInteractions);
			pCIAnalysisRepository.saveAll(pcianList);
			System.out.println("Recycled record insert successfully !!!!!!!");
		}
	}
	
	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d= new Date();
		//System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid+Long.toString(d.getTime());
		return pcidv;
	}


	private void saveRecordsInDBWithInCampaign(List<ProspectCallLog> prospectCallLogList, String campaignId) {
		List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
		if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
			for (ProspectCallLog ProspectCallLog : prospectCallLogList) {
				ProspectCall prospectCall = ProspectCallLog.getProspectCall();
				prospectCall.setSubStatus(DialerCodeDispositionStatus.BUSY.name());
				prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
				prospectCall.setCallRetryCount(1);
				ProspectCallLog.setStatus(ProspectCallStatus.CALL_BUSY);
				prospectCallLogs.add(ProspectCallLog);
			}
			prospectCallLogRepository.saveAll(prospectCallLogs);
			System.out.println("Recycled record update with in campaign successfully !!!!!!!");
		}
	}

	private List<ContactIndustry> getIndustires(List<String> industryList) {
		List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
		if (industryList != null) {
			for (String industry : industryList) {
				ContactIndustry contactIndustry = new ContactIndustry();
				contactIndustry.setName(XtaasUtils.removeNonAsciiChars(industry));
				industries.add(contactIndustry);
			}
		}
		return industries;
	}

	private Map<String, List<ProspectCallLog>> dataListForRecyclePurchased(List<String> states, List<String> industries,
			List<String> functions, List<String> mgmtLevels, List<String> titles, Integer minimumRevenue,
			Integer maximumRevenue, Integer minimumEmployeeCount, Integer maximumEmployeeCount, String campaignId,
			boolean isWithInCampaign, HashMap<String, Boolean> campaignMap, List<String> uniqueDBRecords) {
		Map<String, List<ProspectCallLog>> mapList = new HashMap<String, List<ProspectCallLog>>();

		// Statue wise
		if (campaignMap.containsKey("QUEUED") && campaignMap.get("QUEUED") == true) {
			List<ProspectCallLog> queuedList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
					ProspectCallStatus.QUEUED.name(), null, null, isWithInCampaign);
			mapList.put("QUEUED", getDistinctResult(queuedList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("UNKNOWN_ERROR") && campaignMap.get("UNKNOWN_ERROR") == true) {
			List<ProspectCallLog> unknownErrorList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
					ProspectCallStatus.UNKNOWN_ERROR.name(), null, null, isWithInCampaign);
			mapList.put("UNKNOWN_ERROR", getDistinctResult(unknownErrorList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("MAX_RETRY_LIMIT_REACHED") && campaignMap.get("MAX_RETRY_LIMIT_REACHED") == true) {
			List<ProspectCallLog> maxRetryLimitList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
					ProspectCallStatus.MAX_RETRY_LIMIT_REACHED.name(), null, null, isWithInCampaign);
			mapList.put("MAX_RETRY_LIMIT_REACHED",
					getDistinctResult(maxRetryLimitList, uniqueDBRecords, isWithInCampaign));

		}

		if (campaignMap.containsKey("CALL_ABANDONED") && campaignMap.get("CALL_ABANDONED") == true) {
			List<ProspectCallLog> callAbandonedList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
					ProspectCallStatus.CALL_ABANDONED.name(), null, null, isWithInCampaign);
			mapList.put("CALL_ABANDONED", getDistinctResult(callAbandonedList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("SUCCESS") && campaignMap.get("SUCCESS") == true) {
			// Disposition status
			List<ProspectCallLog> successList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
					DispositionType.SUCCESS.name(), isWithInCampaign);
			mapList.put("SUCCESS", getDistinctResult(successList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("FAILURE") && campaignMap.get("FAILURE") == true) {
			List<ProspectCallLog> failureList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
					DispositionType.FAILURE.name(), isWithInCampaign);
			mapList.put("FAILURE", getDistinctResult(failureList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("CALLBACK") && campaignMap.get("CALLBACK") == true) {
			List<ProspectCallLog> callbackList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null, null,
					DispositionType.CALLBACK.name(), isWithInCampaign);
			mapList.put("CALLBACK", getDistinctResult(callbackList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("NOANSWER") && campaignMap.get("NOANSWER") == true) {
			// SubStatus wise
			List<ProspectCallLog> noAnswerList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
					DialerCodeDispositionStatus.NOANSWER.name(), null, isWithInCampaign);
			mapList.put("NOANSWER", getDistinctResult(noAnswerList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("ANSWERMACHINE") && campaignMap.get("ANSWERMACHINE") == true) {
			List<ProspectCallLog> answermachineList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
					DialerCodeDispositionStatus.ANSWERMACHINE.name(), null, isWithInCampaign);
			mapList.put("ANSWERMACHINE", getDistinctResult(answermachineList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("DIALERMISDETECT") && campaignMap.get("DIALERMISDETECT") == true) {
			List<ProspectCallLog> dialermisdetectList = searchContacts(states, industries, functions, mgmtLevels,
					titles, minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId,
					null, DialerCodeDispositionStatus.DIALERMISDETECT.name(), null, isWithInCampaign);
			mapList.put("DIALERMISDETECT", getDistinctResult(dialermisdetectList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("GATEKEEPER_ANSWERMACHINE")
				&& campaignMap.get("GATEKEEPER_ANSWERMACHINE") == true) {
			List<ProspectCallLog> gatekeeperAMList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
					DialerCodeDispositionStatus.GATEKEEPER_ANSWERMACHINE.name(), null, isWithInCampaign);
			mapList.put("GATEKEEPER_ANSWERMACHINE",
					getDistinctResult(gatekeeperAMList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("BUSY") && campaignMap.get("BUSY") == true) {
			List<ProspectCallLog> busyList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
					DialerCodeDispositionStatus.BUSY.name(), null, isWithInCampaign);
			mapList.put("BUSY", getDistinctResult(busyList, uniqueDBRecords, isWithInCampaign));
		}

		if (campaignMap.containsKey("DEADAIR") && campaignMap.get("DEADAIR") == true) {
			List<ProspectCallLog> deadAirList = searchContacts(states, industries, functions, mgmtLevels, titles,
					minimumRevenue, maximumRevenue, minimumEmployeeCount, maximumEmployeeCount, campaignId, null,
					DialerCodeDispositionStatus.DEADAIR.name(), null, isWithInCampaign);
			mapList.put("DEADAIR", getDistinctResult(deadAirList, uniqueDBRecords, isWithInCampaign));
		}
		return mapList;
	}

	@Override
	public int findAllCallableRecordCount(Campaign campaign) {
		Criteria criteria = getCallableProspectsCriteria(campaign);
		Query query = new Query(criteria);
		return (int) mongoTemplate.count(query, ProspectCallLog.class);
	}

	@Override
	public List<ProspectCallLog> findAllCallableRecords(Campaign campaign) {
		Criteria criteria = getCallableProspectsCriteria(campaign);
		Query query = new Query(criteria);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	@Override
	public List<ProspectCallLog> findAllCallableRecordsWithLimit(Campaign campaign) {
		Criteria criteria = getCallableProspectsCriteria(campaign);
		Query query = new Query(criteria);
		query.limit(20);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	@Override
	public List<Pivot> generatePivotAggregationQuery(Campaign campaign, String fieldName, boolean isLimit) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(Aggregation.match(getCallableProspectsCriteria(campaign)));
		aggregationOperations.add(generateGroupCriterias(fieldName));
		aggregationOperations.add(Aggregation.sort(Direction.DESC, "count"));
		if (!isLimit) {
			aggregationOperations.add(Aggregation.limit(20));
		}
		AggregationResults<Pivot> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", Pivot.class);
		List<Pivot> result = results.getMappedResults();
		if(isLimit && fieldName.equalsIgnoreCase("title") && campaign.getNumberOfTopRecords() > 0) {
			if(result.size() > campaign.getNumberOfTopRecords()) {
				List<String> removeList = new ArrayList<String>();
				List<Pivot> topList = new ArrayList<Pivot>();
				for(int i = 0; i < result.size(); i++) {
					if(i < campaign.getNumberOfTopRecords()) {
						topList.add(result.get(i));
					}else {
						removeList.add(result.get(i).getId());
					}
				}
				int nukeCount = nukeCallableProspectByPivots(campaign, removeList, XtaasConstants.TITLE,
						XtaasConstants.SOFT_DELETE);
				logger.info("Total Nuke Count : "+nukeCount);
				return topList;
			}
		}
		return result;

	}

	private AggregationOperation generateGroupCriterias(String fieldName) {
		BasicDBObject group = null;
		if (fieldName.equalsIgnoreCase("industry")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.industry");
		}
		if (fieldName.equalsIgnoreCase("managementLevel")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.managementLevel");
		}
		if (fieldName.equalsIgnoreCase("department")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.department");
		}
		if (fieldName.equalsIgnoreCase("employeeCount")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.customAttributes.maxEmployeeCount");
		}
		if (fieldName.equalsIgnoreCase("revenue")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.customAttributes.maxRevenue");
		}
		if (fieldName.equalsIgnoreCase("stateCode")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.stateCode");
		}
		if (fieldName.equalsIgnoreCase("title")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.title");
		}
		if (fieldName.equalsIgnoreCase("country")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.country");
		}
		if (fieldName.equalsIgnoreCase("company")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.company");
		}
		if (fieldName.equalsIgnoreCase("source")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.source");
		}
		if (fieldName.equalsIgnoreCase("isEu")) {
			group = new BasicDBObject("_id", "$prospectCall.prospect.isEu");
		}
		group = group.append("count", new BasicDBObject("$sum", 1));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public Criteria getCallableProspectsCriteria(Campaign campaign) {
		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> andCriteriaList2 = new ArrayList<>();
		List<Criteria> andCriteriaList3 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();

		int callRetryCount = 1;
		if (campaign != null && campaign.getCallMaxRetries() > 0) {
			callRetryCount = campaign.getCallMaxRetries();
		} else {
			callRetryCount = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
		}

		/*
		 * CRITERIA1 - campaignId IN [campaignId] AND subStatus NIN
		 * ["GATEKEEPER_ANSWERMACHINE"]
		 */
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(campaign.getId()));
		andCriteriaList1.add(Criteria.where("prospectCall.subStatus").nin("GATEKEEPER_ANSWERMACHINE"));

		/*
		 * CRITERIA2 - disposition IN ["DIALERCODE", "CALLBACK"] AND callRetryCount IN
		 * [1 - N]
		 */
		andCriteriaList2.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		andCriteriaList2.add(Criteria.where("prospectCall.callRetryCount").gte(1).lte(callRetryCount));
		Criteria andCriteria1 = criteria1.andOperator(andCriteriaList2.toArray(new Criteria[andCriteriaList2.size()]));

		// CRITERIA3 - status IN ["QUEUED"] OR CRITERIA2
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(786,886,444444));
		orCriteriaList.add(andCriteria1);

		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);

		// CRITERIA1 AND CRITERIA2 AND CRITERIA3
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));

		return andCriteria;
	}

	@Override
	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuery(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(generateAggregateMatchCriteriaDuplicateProspects(campaignIds, duplicateCompanyDuration));
		aggregationOperations.add(generateGroupCriteriasDuplicateProspects());
		AggregationResults<DomainCompanyCountPair> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "prospectcalllog", DomainCompanyCountPair.class);
		List<DomainCompanyCountPair> result = results.getMappedResults();
		return result;
	}

	private MatchOperation generateAggregateMatchCriteriaDuplicateProspects(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<Criteria> andCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		andCriteriaList.add(Criteria.where("prospectCall.leadStatus").nin("REJECTED", "QA_REJECTED"));
		andCriteriaList.add(Criteria.where("prospectCall.campaignId").in(campaignIds));
		andCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("SUCCESS"));
		andCriteriaList.add(Criteria.where("prospectCall.callStartTime")
				.gt(XtaasDateUtils.getDaysAfterToday(-duplicateCompanyDuration)));
		return Aggregation.match(criteria1.andOperator(andCriteriaList.toArray(new Criteria[andCriteriaList.size()])));
	}
	
	@Override
	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQueryQA(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(generateAggregateMatchCriteriaDuplicateProspectsQA(campaignIds, duplicateCompanyDuration));
		aggregationOperations.add(generateGroupCriteriasDuplicateProspects());
		AggregationResults<DomainCompanyCountPair> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "prospectcalllog", DomainCompanyCountPair.class);
		List<DomainCompanyCountPair> result = results.getMappedResults();
		return result;
	}

	private MatchOperation generateAggregateMatchCriteriaDuplicateProspectsQA(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<Criteria> andCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		andCriteriaList.add(Criteria.where("prospectCall.leadStatus").in("ACCEPTED", "QA_ACCEPTED"));
		andCriteriaList.add(Criteria.where("prospectCall.campaignId").in(campaignIds));
		andCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("SUCCESS"));
		andCriteriaList.add(Criteria.where("prospectCall.callStartTime")
				.gt(XtaasDateUtils.getDaysAfterToday(-duplicateCompanyDuration)));
		return Aggregation.match(criteria1.andOperator(andCriteriaList.toArray(new Criteria[andCriteriaList.size()])));
	}
	
	@Override
	public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuerySecondaryQA(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations
				.add(generateAggregateMatchCriteriaDuplicateProspectsSecondaryQA(campaignIds, duplicateCompanyDuration));
		aggregationOperations.add(generateGroupCriteriasDuplicateProspects());
		AggregationResults<DomainCompanyCountPair> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "prospectcalllog", DomainCompanyCountPair.class);
		List<DomainCompanyCountPair> result = results.getMappedResults();
		return result;
	}

	private MatchOperation generateAggregateMatchCriteriaDuplicateProspectsSecondaryQA(List<String> campaignIds,
			int duplicateCompanyDuration) {
		List<Criteria> andCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		andCriteriaList.add(Criteria.where("prospectCall.leadStatus").in("ACCEPTED"));
		andCriteriaList.add(Criteria.where("prospectCall.campaignId").in(campaignIds));
		andCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("SUCCESS"));
		andCriteriaList.add(Criteria.where("prospectCall.callStartTime")
				.gt(XtaasDateUtils.getDaysAfterToday(-duplicateCompanyDuration)));
		return Aggregation.match(criteria1.andOperator(andCriteriaList.toArray(new Criteria[andCriteriaList.size()])));
	}

	private AggregationOperation generateGroupCriteriasDuplicateProspects() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.prospect.company");
		group = group.append("count", new BasicDBObject("$sum", 1));
		group = group.append("domain", new BasicDBObject("$first", "$prospectCall.prospect.customAttributes.domain"));
		return new CustomAggregationOperation(new Document("$group", group));
	}
	
	@Override
	public boolean updateInteractionData(List<String> campaignIds, List<String> emails, List<String> phoneNumbers) {
	Query query = new Query();
	Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignIds);
	//query.addCriteria(Criteria.where("prospectCall.campaignId").in(campaignIds));
	List<String> dispositions = new ArrayList<String>();
	dispositions.add("SUCCESS");
	criteria.and("prospectCall.dispositionStatus").nin(dispositions);

	if(emails!=null && emails.size()>0){
	criteria.and("prospectCall.prospect.email").in(emails);
	}else if(phoneNumbers!=null && phoneNumbers.size()>0){
	criteria.and("prospectCall.prospect.phone").in(phoneNumbers);
	}

	query.addCriteria(criteria);
	Update update = new Update();
	update.set("prospectCall.callRetryCount", 757502);
	update.set("status", "MAX_RETRY_LIMIT_REACHED");
	update.set("updatedDate", new Date());




		UpdateResult wr = mongoTemplate.updateFirst(query, update, ProspectCallLog.class);

		if (wr.getModifiedCount() == 0) {
			logger.error("updateCallStatus() : Error occurred while updating Interaction record. Error Msg: Modified Records: " + wr.getModifiedCount());
		}

		return (wr.getModifiedCount() != 0); 
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProspectCallLog> getDistinctCampaignIds(List<String> agentIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Criteria criteria = Criteria.where("prospectCall.dispositionStatus").is("SUCCESS");
		criteria = criteria.and("prospectCall.agentId").in(agentIds);
		if (prospectCallSearchDTO.getFromDate() != null && prospectCallSearchDTO.getToDate() != null) {
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate = date.parse(prospectCallSearchDTO.getFromDate());
			Date toDate = date.parse(prospectCallSearchDTO.getToDate());
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
		}
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctPartnerIds(List<String> campaignIds) {
		List<String> callStatus = new ArrayList<String>();
		callStatus.add("SUCCESS");
		BasicDBObject elemMatch = new BasicDBObject();
		elemMatch.put("prospectCall.campaignId", new BasicDBObject("$in", campaignIds));
		elemMatch.put("prospectCall.dispositionStatus", new BasicDBObject("$in", callStatus));
		DistinctIterable<String> iterable = mongoTemplate.getCollection("prospectcalllog").distinct("prospectCall.partnerId",
				elemMatch, String.class);
		MongoCursor<String> cursor = iterable.iterator();
		List<String> callableList = new ArrayList<>();
		while (cursor.hasNext()) {
			callableList.add(cursor.next());
		}
		return callableList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctTeamIds(List<String> campaignIds) {
		List<String> callStatus = new ArrayList<String>();
		callStatus.add("SUCCESS");
		BasicDBObject elemMatch = new BasicDBObject();
		elemMatch.put("prospectCall.campaignId", new BasicDBObject("$in", campaignIds));
		elemMatch.put("prospectCall.dispositionStatus", new BasicDBObject("$in", callStatus));
		DistinctIterable<String> iterable = mongoTemplate.getCollection("prospectcalllog").distinct("prospectCall.supervisorId",
				elemMatch, String.class);
		MongoCursor<String> cursor = iterable.iterator();
		List<String> teamIds = new ArrayList<>();
		while (cursor.hasNext()) {
			teamIds.add(cursor.next());
		}
		return teamIds;
	}

	@Override
	public int nukeCallableProspectByPivots(Campaign campaign, List<String> list, String checkPoint, String action) {
		Criteria criteria = getCallableProspectsCriteria(campaign);
		List<Criteria> andCriteriaList = new ArrayList<>();
		List<Double> employeeCountList = new ArrayList<Double>();
		List<Double> revenueList = new ArrayList<Double>();
		andCriteriaList.add(criteria);
		if (checkPoint.equalsIgnoreCase(XtaasConstants.MANAGEMENT_LEVELS)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.managementLevel").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.DEPARTMENT)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.department").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.EMPLOYEE_COUNT)) {
			// for(String s : list) {
			// 	if(s != null && s.equals("10K+")) {
			// 		list.remove(s);
			// 		list.add("0");
			// 	}
			// }

			if (list != null && list.contains("10K+")) {
				list.remove("10k+");
				list.add("0");
			}

			if(list.contains(null)) {
				list.removeAll(Collections.singleton(null));
				employeeCountList.addAll(list.stream().map(Double::valueOf).collect(Collectors.toList()));
				employeeCountList.add(null);
			}else {
				employeeCountList.addAll(list.stream().map(Double::valueOf).collect(Collectors.toList()));
			}
			andCriteriaList.add(
					Criteria.where("prospectCall.prospect.customAttributes.maxEmployeeCount").in(employeeCountList));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.REVENUE)) {
			// for(String s : list) {
			// 	if(s != null && s.equals("5B+")) {
			// 		list.remove(s);
			// 		list.add("0");
			// 	}
			// }
			if (list != null && list.contains("5B+")) {
				list.remove("5B+");
				list.add("0");
			}
			if(list.contains(null)) {
				list.removeAll(Collections.singleton(null));
				revenueList.addAll(list.stream().map(Double::valueOf).collect(Collectors.toList()));
				revenueList.add(null);
			}else {
				revenueList.addAll(list.stream().map(Double::valueOf).collect(Collectors.toList()));
			}
			andCriteriaList.add(Criteria.where("prospectCall.prospect.customAttributes.maxRevenue").in(revenueList));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.INDUSTRY)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.industry").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.TITLE)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.title").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.VALID_COUNTRY)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.country").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.STATE_CODE)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.stateCode").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.SIC_CODE)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.sicCode").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.VALID_STATE)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.validState").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.COMPANY)) {
			andCriteriaList.add(Criteria.where("prospectCall.prospect.company").in(list));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.SOURCE)) {
			List<String> sourceList = new ArrayList<String>();
			Map<String,String> dpList = dataProviderService.getXtaasDataProviderByIndex();
			for (String s : list) {
				if (s.contains("Xtaas Data Source")) {
					for (Map.Entry<String,String> entry : dpList.entrySet()) {
						if (s.equals(entry.getValue())) {
							sourceList.add(entry.getKey());
						}
					}
				} else {
					sourceList.add(s);
				}
			}
			andCriteriaList.add(Criteria.where("prospectCall.prospect.source").in(sourceList));
		} else if (checkPoint.equalsIgnoreCase(XtaasConstants.INDUSTRY_LIST)
				|| checkPoint.equalsIgnoreCase(XtaasConstants.SIC)
				|| checkPoint.equalsIgnoreCase(XtaasConstants.NAICS)) {
			// Converting comma seperated string of industrylist into list of industrylist
			// for nuking.
			List<List<String>> pivotsList = new ArrayList<List<String>>();
			if (list.size() > 0) {
				for (String csv : list) {
					JsonParser jsonParser = new JsonParser();
					JsonArray arrayFromString = jsonParser.parse(csv).getAsJsonArray();
					if (arrayFromString != null) {
						List<String> pList = new ArrayList<String>();
						for (int i = 0; i < arrayFromString.size(); i++) {
							pList.add(arrayFromString.get(i).getAsString());
						}
						pivotsList.add(pList);
					}
				}
				if(checkPoint.equalsIgnoreCase(XtaasConstants.INDUSTRY_LIST)) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.industryList").in(pivotsList));
				}else if(checkPoint.equalsIgnoreCase(XtaasConstants.SIC)) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.companySIC").in(pivotsList));
				}else if(checkPoint.equalsIgnoreCase(XtaasConstants.NAICS)) {
					andCriteriaList.add(Criteria.where("prospectCall.prospect.companyNAICS").in(pivotsList));
				}
				
			}
		}else if (checkPoint.equalsIgnoreCase(XtaasConstants.ISEU)) {
			List<Boolean> isEuList = new ArrayList<Boolean>();
			for(String s : list) {
				if(s.equals("Europe Data")) {
					isEuList.add(true);
				}else {
					isEuList.add(false);
				}
			}
			andCriteriaList.add(Criteria.where("prospectCall.prospect.isEu").in(isEuList));
		}
		Query searchQuery = Query
				.query(new Criteria().andOperator(andCriteriaList.toArray(new Criteria[andCriteriaList.size()])));
		Update update = new Update();
		/*if (action.equalsIgnoreCase(XtaasConstants.HARD_DELETE)) {
			update.set("prospectCall.campaignId", campaign.getId() + "_PIVOT_REMOVED");
		}*/
		Date d= new Date();
		String removeCampaignId = campaign.getId() + "_REMOVE_" + Long.toString(d.getTime());
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144144);
		// set the audit trails fields
		update.set("updatedBy", XtaasUserUtils.getCurrentLoggedInUsername());
		update.set("updatedDate", new Date());
		update.set("prospectCall.campaignId", removeCampaignId);
		// System.out.println(mongoTemplate.updateMulti(searchQuery, update,
		// ProspectCallLog.class));
		// mongoTemplate.updateMulti(searchQuery, update, ProspectCallLog.class);
		int count =0;
		long  hcount = 0;
		if (action.equalsIgnoreCase(XtaasConstants.HARD_DELETE)) {
			count =  (int)mongoTemplate.count(searchQuery, ProspectCallLog.class);
			mongoTemplate.remove(searchQuery, ProspectCallLog.class);
		}else {
			count = (int) mongoTemplate.updateMulti(searchQuery, update, ProspectCallLog.class).getModifiedCount();
		}
		return count;
	}

	@Override
	public int getNewlyBuyProspectsOfCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		int count = 0;
		
		Query searchQuery = Query.query(getNewlyBuyProspectsCriteria(campaign));
		Update update = new Update();
		if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign)) {
			if(campaign.getStatus().equals(CampaignStatus.PLANNING)) {
				throw new IllegalArgumentException("Please change the campaign status RUNNING to activate the data.");
			}else {
				if(campaign.getAssetEmailTemplate() != null) {
					update.set("prospectCall.callRetryCount", 786122);
					update.set("updatedBy", XtaasUserUtils.getCurrentLoggedInUsername());
					update.set("updatedDate", new Date());
					count = (int)mongoTemplate.updateMulti(searchQuery, update, ProspectCallLog.class).getModifiedCount();
					SupervisorStats stats = new SupervisorStats();
					stats.setCampaignId(campaignId);
					stats.setStatus("QUEUED");
					stats.setSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
					stats.setType("EmailJob");
					stats.setDate(new Date());
					supervisorStatsRepository.save(stats);
				}else {
					throw new IllegalArgumentException("Please add email template to activate the data.");
				}
			}
		}else {
			update.set("status", ProspectCallStatus.QUEUED);
			update.set("prospectCall.subStatus", "BUSY");
			update.set("prospectCall.callRetryCount", 0);
			update.set("updatedBy", XtaasUserUtils.getCurrentLoggedInUsername());
			update.set("updatedDate", new Date());
			count = (int)mongoTemplate.updateMulti(searchQuery, update, ProspectCallLog.class).getModifiedCount();
		}
		//////////////////////////////////////////////////////////////////////////////////////
		///////////// Initiating emailSends
		////////////////////////////////////////////////////////////////////////////////////// ////////////////////////////////////////////////

		if (campaign.isColdEmails()) {
			logger.debug("start Sending cold Mails(): Starting ColdEmailThread.");
			ColdEmailByCampaignThread coldEmailByCampaignTask = BeanLocator.getBean("coldEmailByCampaignThread",
					ColdEmailByCampaignThread.class);
			// List<String> statuses = new ArrayList<String>();
			ColdEmailByCampaignThread cc = new ColdEmailByCampaignThread(campaign);
			Thread tcc = new Thread(cc, "t" + Math.random());
			tcc.start();
			// coldEmailByCampaignThread = new Thread(coldEmailByCampaignTask,
			// "ColdEmailByCampaignThread");
			// coldEmailByCampaignThread.start();
			logger.debug("startColdEmailByCampaignThread(): Started coldEmailByCampaignThread.");
		}
		//////////// End Email Initiates
		//////////// ///////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		if (organization != null && organization.isLiteMode()) {
			if (campaign != null && campaign.getLowerThreshold() > 0) {
				if (count > campaign.getLowerThreshold()) {
					campaign.setAutoGoldenPass(true);
					campaignRepository.save(campaign);
				}
			}
		}
		if(campaign.isUseInsideviewSmartCache()){
			try{
				Runnable runnable = () -> {
					List<ProspectCallLog> prospectReviwedList= prospectCallLogRepository
							.findProspectsByCampaignIdAndInsideViewBuysAndSource(campaign.getId(), "No","INSIDEVIEW");
					if(prospectReviwedList != null && prospectReviwedList.size() > 0){
						buyInsideViewReviewedData(campaign,prospectReviwedList);
					}
				};
				Thread thread = new Thread(runnable);
				thread.start();
			}catch (Exception e){
				logger.error("Error while activate insideview data. Exception : [{}] ", e);
			}

		}


		if(campaign.isUseLeadIQSmartCache()){
			try{
				Runnable runnable = () -> {
					List<ProspectCallLog> prospectList= prospectCallLogRepository
							.findProspectsByCampaignIdAndLeadIQBuysAndSource(campaign.getId(), "No","LeadIQ");
					if(prospectList != null && prospectList.size() > 0){
						leadIQSearchServiceImpl.leadIQDetailedAPI(getPeopleIdList(prospectList),prospectList,campaign, null);
					}
				};
				Thread thread = new Thread(runnable);
				thread.start();
			}catch (Exception e){
				logger.error("Error while activate leadiq data. Exception : [{}] ", e);
			}

		}

	
		return count;
	}


	public Criteria getNewlyBuyProspectsCriteria(Campaign campaign) {
		Criteria criteria = new Criteria();
		criteria.and("prospectCall.campaignId").in(campaign.getId());
		criteria.and("prospectCall.dispositionStatus").nin("SUCCESS");
		if(campaign.isUseInsideviewSmartCache() || campaign.isUseLeadIQSmartCache()){
			criteria.and("prospectCall.prospect.source").in("ZOOINFO","SALUTORY");
		}
		criteria.and("prospectCall.callRetryCount").in(786, 886, 444444);
		return criteria;
	}

	@Override
	public List<ProspectCallLog> searchFailureProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException {
		Query query = new Query();
		query.addCriteria(buildCriteriaForFailureProspectByQa(agentIds, campaignIds, prospectCallSearchDTO));
		return mongoTemplate.find(query.with(pageRequest), ProspectCallLog.class);
	}

	private Criteria buildCriteriaForFailureProspectByQa(List<String> agentIds, List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Criteria criteria = new Criteria();
		/*
		 * 28-11-2018 : removing status condition and filtering records on leadStatus
		 */
		// if (prospectCallSearchDTO.getLeadStatus() != null &&
		// !prospectCallSearchDTO.getLeadStatus().isEmpty()) {
		// if(!prospectCallSearchDTO.getLeadStatus().contains("FAILURE")) {
		// criteria =
		// Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
		// }
		// } else {
		// ArrayList<String> leadStatuses = new ArrayList<String>();
		// leadStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
		// leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.name());
		// criteria = Criteria.where("prospectCall.leadStatus").in(leadStatuses);
		// }
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		} else if (campaignIds != null && !campaignIds.isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(campaignIds);
		}
		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		} else if (agentIds != null && !agentIds.isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(agentIds);
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}

		Date fromDate = null, toDate = null;
		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		/*
		 * START LAST 72 HOURS SUCCESS DATE : 15/06/2017 REASON : added to fetch last 72
		 * hours SUCCESS records
		 */
		boolean campaignId = false;
		boolean teamIds = false;
		boolean disposition = false;
		boolean subStatus = false;
		boolean prospectCallIds = false;

		if (prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			campaignId = true;
		}
		if (prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) {
			teamIds = true;
		}
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			disposition = true;
		}
		if (prospectCallSearchDTO.getSubStatus() == null || prospectCallSearchDTO.getSubStatus().isEmpty()) {
			subStatus = true;
		}
		if (prospectCallSearchDTO.getProspectCallIds() == null
				|| prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			prospectCallIds = true;
		}
		if (campaignId && teamIds && prospectCallIds && disposition && subStatus
				&& prospectCallSearchDTO.getCallDurationRangeEnd() == null
				&& prospectCallSearchDTO.getCallDurationRangeStart() == null
				&& prospectCallSearchDTO.getCampaignIds() == null && prospectCallSearchDTO.getClientId() == null
				&& prospectCallSearchDTO.getEndHour() == null && prospectCallSearchDTO.getEndMinute() == 0
				&& prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getProspectCallStatus() == null
				&& prospectCallSearchDTO.getStartHour() == null && prospectCallSearchDTO.getStartMinute() == 0
				&& prospectCallSearchDTO.getToDate() == null) {
			criteria = criteria.and("prospectCall.dispositionStatus").in("FAILURE");
		}

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}

		if (prospectCallSearchDTO.getLeadStatus() != null
				&& prospectCallSearchDTO.getLeadStatus().contains("FAILURE")) {
			criteria = criteria.and("prospectCall.dispositionStatus").in("FAILURE");
		}
		if (prospectCallSearchDTO.getSubStatus() != null && !prospectCallSearchDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
		}
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		return criteria;

	}

	@Override
	public List<ProspectCallLog> searchFailutreProspectCallsBySecQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException {
		Query query = new Query();
		query.addCriteria(buildFailureProspectCallCriteriaBySecQa(campaignIds, prospectCallSearchDTO))
				.with(pageRequest);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	private Criteria buildFailureProspectCallCriteriaBySecQa(List<String> campaignIds,
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		Criteria criteria = new Criteria();
		// if (prospectCallSearchDTO.getLeadStatus() != null &&
		// !prospectCallSearchDTO.getLeadStatus().isEmpty()) {
		// if (!prospectCallSearchDTO.getLeadStatus().contains("FAILURE")) {
		// prospectCallSearchDTO.getLeadStatus().remove("FAILURE");
		// criteria =
		// Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
		// } else {
		// criteria =
		// Criteria.where("prospectCall.leadStatus").in(prospectCallSearchDTO.getLeadStatus());
		// }
		// } else {
		// ArrayList<String> leadStatuses = new ArrayList<String>();
		// leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
		// leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.name());
		// criteria = Criteria.where("prospectCall.leadStatus").in(leadStatuses);
		// }
		if (prospectCallSearchDTO.getCampaignIds() != null && !prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(prospectCallSearchDTO.getCampaignIds());
		} else if (campaignIds != null && !campaignIds.isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(campaignIds);
		}
		if (prospectCallSearchDTO.getAgentIds() != null && !prospectCallSearchDTO.getAgentIds().isEmpty()) {
			criteria = criteria.and("prospectCall.agentId").in(prospectCallSearchDTO.getAgentIds());
		}
		// if (prospectCallSearchDTO.getQaIds() != null &&
		// !prospectCallSearchDTO.getQaIds().isEmpty()) {
		// criteria =
		// criteria.and("qaFeedback.qaId").in(prospectCallSearchDTO.getQaIds());
		// }
		if (prospectCallSearchDTO.getPartnerIds() != null && !prospectCallSearchDTO.getPartnerIds().isEmpty()) {
			criteria = criteria.and("prospectCall.partnerId").in(prospectCallSearchDTO.getPartnerIds());
		}
		if (prospectCallSearchDTO.getCallDurationRangeStart() != null
				&& prospectCallSearchDTO.getCallDurationRangeEnd() != null) {
			criteria = criteria.and("prospectCall.callDuration").gte(prospectCallSearchDTO.getCallDurationRangeStart())
					.lte(prospectCallSearchDTO.getCallDurationRangeEnd());
		}

		Date fromDate = null, toDate = null;
		if (prospectCallSearchDTO.getFromDate() != null && !prospectCallSearchDTO.getFromDate().trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getFromDate(), DATE_FORMAT_DEFAULT);
		}
		if (prospectCallSearchDTO.getToDate() != null && !prospectCallSearchDTO.getToDate().trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(prospectCallSearchDTO.getToDate(), DATE_FORMAT_DEFAULT);
		}

		Integer startHour = prospectCallSearchDTO.getStartHour();
		Integer endHour = prospectCallSearchDTO.getEndHour();
		int startMinute = prospectCallSearchDTO.getStartMinute();
		int endMinute = prospectCallSearchDTO.getEndMinute();

		/*
		 * START LAST 72 HOURS SUCCESS DATE : 15/06/2017 REASON : added to fetch last 72
		 * hours SUCCESS records
		 */
		boolean agentIds = false;
		boolean teamIds = false;
		boolean disposition = false;
		boolean subStatus = false;
		boolean prospectCallIds = false;

		if (prospectCallSearchDTO.getAgentIds() == null || prospectCallSearchDTO.getAgentIds().isEmpty()) {
			agentIds = true;
		}
		if (prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) {
			teamIds = true;
		}
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			disposition = true;
		}
		if (prospectCallSearchDTO.getSubStatus() == null || prospectCallSearchDTO.getSubStatus().isEmpty()) {
			subStatus = true;
		}
		if (prospectCallSearchDTO.getProspectCallIds() == null
				|| prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			prospectCallIds = true;
		}
		if (agentIds && teamIds && prospectCallIds && disposition && subStatus
				&& prospectCallSearchDTO.getCallDurationRangeEnd() == null
				&& prospectCallSearchDTO.getCallDurationRangeStart() == null
				&& prospectCallSearchDTO.getCampaignIds() == null && prospectCallSearchDTO.getClientId() == null
				&& prospectCallSearchDTO.getEndHour() == null && prospectCallSearchDTO.getEndMinute() == 0
				&& prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getProspectCallStatus() == null
				&& prospectCallSearchDTO.getStartHour() == null && prospectCallSearchDTO.getStartMinute() == 0
				&& prospectCallSearchDTO.getToDate() == null) {
			criteria = criteria.and("prospectCall.dispositionStatus").in("FAILURE");
		}

		if (fromDate != null && toDate != null && startHour != null && endHour != null) {
			DateTime startDate = getDateTime(fromDate, startHour, startMinute, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, endHour, endMinute, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (fromDate != null && toDate != null && startHour == null && endHour == null) {
			DateTime startDate = getDateTime(fromDate, 0, 0, prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(toDate, 23, 59, prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate).lte(endDate);
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate).lte(endDate);
			}
		}
		if (fromDate == null && toDate == null && startHour != null && endHour != null) {
			Date currentDateInClientTZ = XtaasDateUtils.convertToTimeZone(new Date(),
					prospectCallSearchDTO.getTimeZone());
			DateTime startDate = getDateTime(currentDateInClientTZ, startHour, startMinute,
					prospectCallSearchDTO.getTimeZone());
			DateTime endDate = getDateTime(currentDateInClientTZ, endHour, endMinute,
					prospectCallSearchDTO.getTimeZone());
			if (prospectCallSearchDTO.getProspectCallStatus() != null
					&& (prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("Scored")
							|| prospectCallSearchDTO.getProspectCallStatus().equalsIgnoreCase("In Progress"))) {
				criteria = criteria.and("qaFeedback.feedbackTime").gte(startDate.toDate()).lte(endDate.toDate());
			} else {
				criteria = criteria.and("prospectCall.callStartTime").gte(startDate.toDate()).lte(endDate.toDate());
			}
		}
		if (prospectCallSearchDTO.getLeadStatus() != null
				&& prospectCallSearchDTO.getLeadStatus().contains("FAILURE")) {
			criteria = criteria.and("prospectCall.dispositionStatus").in("FAILURE");
		}
		if (prospectCallSearchDTO.getSubStatus() != null && !prospectCallSearchDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(prospectCallSearchDTO.getSubStatus());
		}
		if (prospectCallSearchDTO.getProspectCallIds() != null
				&& !prospectCallSearchDTO.getProspectCallIds().isEmpty()) {
			criteria = criteria.and("prospectCall.prospectCallId").in(prospectCallSearchDTO.getProspectCallIds());
		}
		return criteria;
	}

	@Override
	public List<ProspectCallLog> findByCampaignIdAndStatus(GlobalContactCriteriaDTO criteriaDTO) {
		Criteria criteria = new Criteria();
		if (criteriaDTO.getCampaignIds() != null && !criteriaDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(criteriaDTO.getCampaignIds());
		} else {
			return new ArrayList<>();
		}
		if (criteriaDTO.getStatuses() != null && !criteriaDTO.getStatuses().isEmpty()) {
			criteria = criteria.and("status").in(criteriaDTO.getStatuses());
		}
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.find(query, ProspectCallLog.class);
	}

	@Override
	public int getbackCallableProspects(CallableGetbackCriteriaDTO criteriaDTO) {
		Criteria criteria = new Criteria();
		if (criteriaDTO.getCampaignIds() != null && !criteriaDTO.getCampaignIds().isEmpty()) {
			criteria = criteria.and("prospectCall.campaignId").in(criteriaDTO.getCampaignIds());
		} else {
			return 0;
		}
		if (criteriaDTO.getStatus() != null && !criteriaDTO.getStatus().isEmpty()) {
			criteria = criteria.and("status").in(criteriaDTO.getStatus());
		} else {
			return 0;
		}
		if (criteriaDTO.getDispositionStatus() != null && !criteriaDTO.getDispositionStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.dispositionStatus").in(criteriaDTO.getDispositionStatus());
		}
		if (criteriaDTO.getSubStatus() != null && !criteriaDTO.getSubStatus().isEmpty()) {
			criteria = criteria.and("prospectCall.subStatus").in(criteriaDTO.getSubStatus());
		}
		if (criteriaDTO.getMinutes() > 0) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, -criteriaDTO.getMinutes());
			criteria = criteria.and("updatedDate").gte(calendar.getTime());
		}
		Query query = new Query();
		query.addCriteria(criteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.QUEUED);
		update.set("prospectCall.dispositionStatus", DispositionType.DIALERCODE.name());
		update.set("prospectCall.subStatus", DialerCodeDispositionStatus.ANSWERMACHINE.name());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return (int)wr.getModifiedCount();
	}

	@Override
	public long getDepartmentHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		// List<Criteria> andCriteriaList2 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("department")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.department")
						.nin(Arrays.asList(campaignCriteria.getDepartment().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.department")
						.in(Arrays.asList(campaignCriteria.getDepartment().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.department").gt(Arrays.asList(campaignCriteria.getDepartment().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.department").lt(Arrays.asList(campaignCriteria.getDepartment().split("\\s*,\\s*"))));
			// break;
			}
		}

		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));

		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);


		// andCriteriaList2.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE",
		// "CALLBACK"));
		// andCriteriaList2.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		// Criteria andCriteria1 = criteria1.andOperator(andCriteriaList2.toArray(new
		// Criteria[andCriteriaList2.size()]));

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));

		// orCriteriaList.add(andCriteria1);

		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);

		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));

		Query query = new Query();
		query.addCriteria(andCriteria);

		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getIndustryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("industry")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.industry")
						.nin(Arrays.asList(campaignCriteria.getIndustry().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.industry")
						.in(Arrays.asList(campaignCriteria.getIndustry().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.industry").gt(Arrays.asList(campaignCriteria.getIndustry().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.industry").lt(Arrays.asList(campaignCriteria.getIndustry().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getCountryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("country")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.country")
						.nin(Arrays.asList(campaignCriteria.getCountry().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.country")
						.in(Arrays.asList(campaignCriteria.getCountry().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.country").gt(Arrays.asList(campaignCriteria.getCountry().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.country").lt(Arrays.asList(campaignCriteria.getCountry().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}
	
	@Override
	public long getNaicsCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("naicsCode")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.naicsCode")
						.nin(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.naicsCode")
						.in(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.sicCode").gt(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.sicCode").lt(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getSicCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("sicCode")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.sicCode")
						.nin(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.sicCode")
						.in(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.sicCode").gt(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.sicCode").lt(Arrays.asList(campaignCriteria.getSicCode().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("validState")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.state")
						.nin(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.state")
						.in(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").gt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").lt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}
	
	@Override
	public long getUSStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("validUSState")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.state")
						.nin(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.state")
						.in(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").gt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").lt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}
	
	
	@Override
	public long getZipHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("zipCode")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.zipCode")
						.nin(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.zipCode")
						.in(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").gt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.validState").lt(Arrays.asList(campaignCriteria.getState().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);

		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}


	@Override
	public long getTitleHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		String operator = null;
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		for (Expression healthChecks : expression) {
			if (healthChecks.getAttribute().equalsIgnoreCase("title")) {
				operator = healthChecks.getOperator();
				break;
			}
		}

		if (operator != null) {
			switch (operator) {
			case "IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.title")
						.nin(Arrays.asList(campaignCriteria.getTitleKeyWord().split("\\s*,\\s*"))));
				break;
			case "NOT IN":
				andCriteriaList1.add(Criteria.where("prospectCall.prospect.title")
						.in(Arrays.asList(campaignCriteria.getTitleKeyWord().split("\\s*,\\s*"))));
				break;
			// case "GREATER THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.title").gt(Arrays.asList(campaignCriteria.getTitleKeyWord().split("\\s*,\\s*"))));
			// break;
			// case "LESS THAN":
			// andCriteriaList1
			// .add(Criteria.where("prospectCall.prospect.title").lt(Arrays.asList(campaignCriteria.getTitleKeyWord().split("\\s*,\\s*"))));
			// break;
			}
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getEmployeeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		if(campaignCriteria.getMaxEmployeeCount()!=null && Integer.parseInt(campaignCriteria.getMaxEmployeeCount())>0) {
			andCriteriaList1.add(Criteria.where("prospectCall.prospect.customAttributes.maxEmployeeCount")
			.gt(Integer.parseInt(campaignCriteria.getMaxEmployeeCount())));
		}
		if(campaignCriteria.getMinEmployeeCount()!=null && Integer.parseInt(campaignCriteria.getMinEmployeeCount())>0) {
			andCriteriaList1.add(Criteria.where("prospectCall.prospect.customAttributes.minEmployeeCount")
			.lt(Integer.parseInt(campaignCriteria.getMinEmployeeCount())));
		}
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);
		
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public long getRevenueHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {

		List<Criteria> andCriteriaList1 = new ArrayList<>();
		List<Criteria> orCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		
		if(campaignCriteria.getMaxRevenue()!=null && Integer.parseInt(campaignCriteria.getMaxRevenue())>0)
		andCriteriaList1.add(Criteria.where("prospectCall.prospect.customAttributes.maxRevenue")
		.gt(Integer.parseInt(campaignCriteria.getMaxRevenue())));
		if(campaignCriteria.getMinRevenue()!=null && Integer.parseInt(campaignCriteria.getMinRevenue())>0)
		andCriteriaList1.add(Criteria.where("prospectCall.prospect.customAttributes.minRevenue")
		.lt(Integer.parseInt(campaignCriteria.getMinRevenue())));
		
		andCriteriaList1.add(Criteria.where("prospectCall.campaignId").in(zoomBuySearchDTO.getCampaignId()));
		List<Integer> callRetryCount = new ArrayList<Integer>();
		callRetryCount.add(1);
		callRetryCount.add(2);
		callRetryCount.add(3);
		callRetryCount.add(4);
		callRetryCount.add(5);
		callRetryCount.add(6);
		callRetryCount.add(7);
		callRetryCount.add(8);
		callRetryCount.add(9);
		callRetryCount.add(786);
		callRetryCount.add(886);
		callRetryCount.add(444444);
		orCriteriaList.add(Criteria.where("status").in("QUEUED"));
		orCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("DIALERCODE", "CALLBACK"));
		orCriteriaList.add(Criteria.where("prospectCall.callRetryCount").in(callRetryCount));
		Criteria criteria2 = new Criteria();
		Criteria orCriteria = criteria2.orOperator(orCriteriaList.toArray(new Criteria[orCriteriaList.size()]));
		andCriteriaList1.add(orCriteria);
		Criteria criteria3 = new Criteria();
		Criteria andCriteria = criteria3.andOperator(andCriteriaList1.toArray(new Criteria[andCriteriaList1.size()]));
		Query query = new Query();
		query.addCriteria(andCriteria);
		Update update = new Update();
		update.set("status", ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		update.set("prospectCall.callRetryCount", 144);
		update.set("updatedDate", new Date());
		UpdateResult wr = mongoTemplate.updateMulti(query, update, ProspectCallLog.class);
		return wr.getModifiedCount();
	}

	@Override
	public List<DistinctCampaignIdsDTO> generateDistinctCampaignIdsAggregationQuery(String startDate, String endDate, List<String> agentIds) throws ParseException {
		Date fromDate = null, toDate = null;
		if (startDate != null && !startDate.trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(startDate, DATE_FORMAT_DEFAULT);
		}
		if (endDate != null && !endDate.trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(endDate, DATE_FORMAT_DEFAULT);
		}
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(generateAggregateMatchCriteriaQaCampaignIds(XtaasDateUtils.getDateAtTime(fromDate, 0, 0, 0, 0), XtaasDateUtils.getDateAtTime(toDate, 23, 59, 59, 0),agentIds));
		aggregationOperations.add(generateGroupCriteriasQaCampaignIds());
		AggregationResults<DistinctCampaignIdsDTO> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcalllog", DistinctCampaignIdsDTO.class);
		List<DistinctCampaignIdsDTO> result = results.getMappedResults();
		return result;
	}

	private MatchOperation generateAggregateMatchCriteriaQaCampaignIds(Date startDate, Date endDate, List<String> agentIds) {
		List<Criteria> andCriteriaList = new ArrayList<>();
		Criteria criteria1 = new Criteria();
		andCriteriaList.add(Criteria.where("prospectCall.callStartTime").gte(startDate).lte(endDate));
		andCriteriaList.add(Criteria.where("prospectCall.dispositionStatus").in("SUCCESS"));
		andCriteriaList.add(Criteria.where("status").in("WRAPUP_COMPLETE"));
		andCriteriaList.add(Criteria.where("prospectCall.agentId").in(agentIds));
		return Aggregation.match(criteria1.andOperator(andCriteriaList.toArray(new Criteria[andCriteriaList.size()])));
	}

	private AggregationOperation generateGroupCriteriasQaCampaignIds() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.campaignId");
		return new CustomAggregationOperation(new Document("$group", group));
	}
	
	public List<ProspectCallLog> getSuccessMissingRecordings(List<String> dispositionList, Date fromDate){
		List<ProspectCallLog> successList = prospectCallLogRepository.findSuccessWithMissingRecordings(dispositionList, fromDate);
		return successList;
	}

	public List<String> getDistinctCampaignIdsForFilter(List<String> agentIds,ProspectCallSearchDTO prospectCallSearchDTO) {
		DistinctIterable<String> cids = null;
		try {
		Document elemMatch = new Document();
		Date fromDate = null, toDate = null;
		String startDate = prospectCallSearchDTO.getFromDate();
		String endDate = prospectCallSearchDTO.getToDate();
		if (startDate != null && !startDate.trim().isEmpty()) {
			fromDate = XtaasDateUtils.convertStringToDate(startDate, DATE_FORMAT_DEFAULT);
		}
		if (endDate != null && !endDate.trim().isEmpty()) {
			toDate = XtaasDateUtils.convertStringToDate(endDate, DATE_FORMAT_DEFAULT);
		}
		if (agentIds == null) {
			agentIds = new ArrayList<String>();
		}
		Login loggedInUser = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		if (loggedInUser != null
				&& loggedInUser.getOrganization() != null) {
			agentIds.add(loggedInUser.getOrganization() + "_EMAIL_AGENT");
		}
		List<String> leadStatuses = new ArrayList<String>();
		leadStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
		leadStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
		List<String> dispositionStatus = new ArrayList<String>();
		dispositionStatus.add("SUCCESS");
		elemMatch.put("prospectCall.leadStatus", new Document("$in", leadStatuses));
		elemMatch.put("prospectCall.dispositionStatus", new Document("$in", dispositionStatus));
		elemMatch.put("prospectCall.callStartTime", new Document("$gte", XtaasDateUtils.getDateAtTime(fromDate, 0, 0, 0, 0)).append("$lte", XtaasDateUtils.getDateAtTime(toDate, 23, 59, 59, 0)));
		elemMatch.put("prospectCall.agentId", new Document("$in", agentIds));
		cids = mongoTemplate.getCollection("successcalllog").distinct("prospectCall.campaignId",
				elemMatch, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		List<String> list = new ArrayList<>();
		if (cids != null) {
			MongoCursor<String> cursor = cids.iterator();
			while (cursor.hasNext()) {
			    list.add(cursor.next());
			 }
		}
		return list;
	}


	@Override
	public void bulkinsert(List<ProspectCallLog> prospectCallLogList) {
		if(CollectionUtils.isNotEmpty(prospectCallLogList)){
			try{
				long start = System.currentTimeMillis();
				logger.info("Bulkinsert >>>>>>>> Campaign ID : "+prospectCallLogList.get(0).getProspectCall().getCampaignId() +" Start : "+getDate(start));
				int maxDocumentSize = propertyService.getIntApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.BSON_DOCUMENTSIZE.name(), 1500000);
				// ObjectSizeCalculator.getObjectSize(prospectCallLogList) it will the size on object in byte
				if (ObjectSizeCalculator.getObjectSize(prospectCallLogList) <= maxDocumentSize) {
					try{
						bulkInsertMultiple(prospectCallLogList);
					}catch (Exception e){
						logger.error("Error while bulk insert. Exception : ", e);
					}
				} else {
					List<ProspectCallLog> mb2List = new ArrayList<>();
					for (int i = 0; i < prospectCallLogList.size(); i++) {
						mb2List.add(prospectCallLogList.get(i));
						if(ObjectSizeCalculator.getObjectSize(mb2List) >= maxDocumentSize){
							mb2List.remove(prospectCallLogList.get(i));
							try{
								bulkInsertMultiple(mb2List);
							}catch (Exception e){
								logger.error("Error while bulk insert. Exception : ", e);
							}
							mb2List.clear();
							mb2List.add(prospectCallLogList.get(i));
						}
					}
					try{
						bulkInsertMultiple(mb2List);
					}catch (Exception e){
						logger.error("Error while bulk insert. Exception : ", e);
					}
				}
				long end = System.currentTimeMillis();
				long dif = end - start;
				logger.info("Bulkinsert total records : "+prospectCallLogList.size()+" >>>>>>>> Campaign ID : "+prospectCallLogList.get(0).getProspectCall().getCampaignId() +" End : "+getDate(end)+" Total time taken to process : " + TimeUnit.MILLISECONDS.toSeconds(dif)+"s");
			} catch (Exception e) {
				logger.error("Error while bulk insert. Exception : ", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void bulkUpdate(List<ProspectCallLog> prospectCallLogList) {
		if(CollectionUtils.isNotEmpty(prospectCallLogList)){
			try{
				long start = System.currentTimeMillis();
				logger.info("BulkUpdate >>>>>>>> Campaign ID : "+prospectCallLogList.get(0).getProspectCall().getCampaignId() +" Start : "+getDate(start));
				int maxDocumentSize = propertyService.getIntApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.BSON_DOCUMENTSIZE.name(), 1500000);
				// ObjectSizeCalculator.getObjectSize(prospectCallLogList) it will the size on object in byte
				if (ObjectSizeCalculator.getObjectSize(prospectCallLogList) <= maxDocumentSize) {
					prospectCallLogRepository.saveAll(prospectCallLogList);
				} else {
					List<ProspectCallLog> mb2List = new ArrayList<>();
					for (int i = 0; i < prospectCallLogList.size(); i++) {
						mb2List.add(prospectCallLogList.get(i));
						if(ObjectSizeCalculator.getObjectSize(mb2List) >= maxDocumentSize){
							mb2List.remove(prospectCallLogList.get(i));
							try{
								prospectCallLogRepository.saveAll(mb2List);
							}catch (Exception e){
								logger.error("Error while BulkUpdate. Exception :  [{}] ", e);
							}
							mb2List.clear();
							mb2List.add(prospectCallLogList.get(i));
						}
					}
					prospectCallLogRepository.saveAll(mb2List);
				}
				long end = System.currentTimeMillis();
				long dif = end - start;
				logger.info("BulkUpdate total records : "+prospectCallLogList.size()+" >>>>>>>> Campaign ID : "+prospectCallLogList.get(0).getProspectCall().getCampaignId() +" End : "+getDate(end)+" Total time taken to process : " + TimeUnit.MILLISECONDS.toSeconds(dif)+"s");
			}catch (Exception e){
				e.printStackTrace();
				logger.error("Error while BulkUpdate. Exception :  [{}] ", e);
			}
		}
	}

	public Date getDate(long timestamp) {
		Timestamp time = new Timestamp(timestamp);
		return time;
	}


/*	@Override
	public void bulkinsert(List<ProspectCallLog> prospectCallLogList) {
		int listSize = prospectCallLogList.size();
		int batchSize = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.BULKINSERT_BATCHSIZE.name(), 2000);
		if (listSize <= batchSize) {
			mongoTemplate.insert(prospectCallLogList, ProspectCallLog.class);
		} else {
			int batches = listSize/batchSize;
			int modSize = listSize%batchSize;
			for (int i = 0; i <= batches; i++) {
				int fromIndex = i*batchSize;
				int toIndex = fromIndex + batchSize;
				if (i == batches) {
					toIndex = fromIndex + modSize;
				}
				List<ProspectCallLog> tempprospectCallLogList = prospectCallLogList.subList(fromIndex, toIndex);
				mongoTemplate.insert(tempprospectCallLogList, ProspectCallLog.class);
			}
		}
	}*/

	public void buyInsideViewReviewedData(Campaign campaign, List<ProspectCallLog> prospectReviwedList){
		Map<String, String[]> revMap = XtaasUtils.getInsideviewRevenueMap();
		List<Long> minRevList = new ArrayList<Long>();
		List<Long> maxRevList = new ArrayList<Long>();
		Long minRev = 0l;
		Long maxRev = 0l;
		List<String> mLevel = new ArrayList<>();
		List<String> departmentList = new ArrayList<>();
		List<String> industryList = new ArrayList<>();
		List<DataBuyQueue> dataBuyQueues = dataBuyQueueRepository.findByCampaignIdAndActivateStatus(campaign.getId(),true);
		if(CollectionUtils.isNotEmpty(dataBuyQueues)){
			for(DataBuyQueue dbq : dataBuyQueues){
				List<Expression> expressions = dbq.getCampaignCriteriaDTO();
				for (Expression exp : expressions) {
					String attribute = exp.getAttribute();
					String value = exp.getValue();
					if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
						List<String> mlist = Arrays.asList(value.split(","));
						mLevel.addAll(mlist);
					}
					if (attribute.equalsIgnoreCase("DEPARTMENT")) {
						List<String> dList = Arrays.asList(value.split(","));
						departmentList.addAll(dList);
					}
					if (attribute.equalsIgnoreCase("REVENUE")) {
						String[] revenueArr = exp.getValue().split(",");
						for (int i = 0; i < revenueArr.length; i++) {
							String revVal = revenueArr[i];
							String[] revArr = revMap.get(revVal);

							minRevList.add(Long.parseLong(revArr[0]));
							maxRevList.add(Long.parseLong(revArr[1]));
						}
						Collections.sort(minRevList);
						Collections.sort(maxRevList, Collections.reverseOrder());
						if (minRevList.get(0) >= 0) {
							if(minRevList.get(0) > minRev){
								minRev = minRevList.get(0);
							}
						}
						if (maxRevList.get(0) > 0) {
							if(maxRevList.get(0) > maxRev){
								maxRev = maxRevList.get(0);
							}
						}
					}
					if (attribute.equalsIgnoreCase("INDUSTRY")) {
						Map<String, List<DataBuyMapping>> dbmIndustryMap = new HashMap<String, List<DataBuyMapping>>();
						List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Industry");
						for (DataBuyMapping dbm : dbmList) {
							List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(dbm.getSearchString());
							if (dbmSubList == null) {
								dbmSubList = new ArrayList<DataBuyMapping>();
							}
							dbmSubList.add(dbm);
							dbmIndustryMap.put(dbm.getSearchString(), dbmSubList);
						}
						List<String> zoomIndustryList = Arrays.asList(exp.getValue().split(","));
						for (String ind : zoomIndustryList) {
							List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(ind);
							if (dbmSubList != null) {
								for (DataBuyMapping sdbm : dbmSubList) {
									industryList.add(sdbm.getChildTaxonamyId());
								}
							}
						}
					}

				}
				dbq.setDataActivate(false);

			}
		}
		dataBuyQueueRepository.saveAll(dataBuyQueues);

		List<String> contactIdListMain = getContectIdList(prospectReviwedList);
		DataBuyQueue dbq = new DataBuyQueue();
		dbq.setCampaignId(campaign.getId());
		dbq.setCampaignName(campaign.getName());
		dbq.setJobRequestTime(new Date());
		dbq.setJobStartTime(new Date());
		if(CollectionUtils.isNotEmpty(mLevel)){
			if(mLevel.contains("VP_EXECUTIVES")){
				mLevel.add("VP-Level");
			}if(mLevel.contains("C_EXECUTIVES")){
				mLevel.add("C-Level");
			}
			dbq.setSearchManagementlevel(String.join(",",mLevel));
		}
		if(CollectionUtils.isNotEmpty(departmentList)){
			String department = insideViewServiceImpl.getInsideViewDepartment(departmentList);
			if(StringUtils.isNotBlank(department)){
				dbq.setSearchDepartment(department);
			}
		}
		if(CollectionUtils.isNotEmpty(industryList)){
			String industry = insideViewServiceImpl.getInsideViewIndustry(industryList);
			if(StringUtils.isNotBlank(industry)){
				dbq.setSearchIndustry(industry);
			}
		}

		if(minRev >= 0 && maxRev > 0){
			dbq.setSearchRevenueMin(String.valueOf(minRev));
			dbq.setSearchRevenueMax(String.valueOf(maxRev));
		}
		if (contactIdListMain.size() > 0) {
			String personDetailJobUrl = "https://api.insideview.com/api/v1/target/contact/job";
			String jobId = insideViewServiceImpl.createContactDetailJob(personDetailJobUrl,contactIdListMain, campaign.getId());
			final String campaignId = campaign.getId();
			if (!jobId.equalsIgnoreCase("Error")) {
				dbq.setInsideViewJobId(jobId);
				dbq.setCampaignId(campaignId);
				dbq.setInsideViewJobStatus("ACCEPTED");
				logger.info("buyInsideViewReviewedData(): Prospects Job initiated for InsideView for campaigId [{}] ", campaign.getId());
			} else {
				dbq.setInsideViewJobStatus("ERROR");
				logger.error("buyInsideViewReviewedData(): Prospects Job initiation error for Insideview with campaign Id : " + campaign.getId());
			}
		}else{
			dbq.setInsideViewJobStatus("NO_JOB");
			logger.info("buyInsideViewReviewedData(): Prospects Job not initiated for Insideview with campaign Id : " + campaign.getId());
		}
		dataBuyQueueRepository.save(dbq);
	}

	public List<String> getContectIdList(List<ProspectCallLog> prospectReviwedList){
		List<String> contactList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(prospectReviwedList)){
			contactList = prospectReviwedList.stream()
					.map(rev -> rev.getProspectCall().getProspect().getInsideViewPurchaseDetailsId())
					.collect(Collectors.toList());
		}
		return contactList;
	}


	public void bulkInsertMultiple(List<ProspectCallLog> prospectCallLogList) {
		BulkOperations action = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, ProspectCallLog.class);
		FindAndReplaceOptions options = FindAndReplaceOptions.options();
		for(ProspectCallLog pNew : prospectCallLogList) {
			Prospect p = pNew.getProspectCall().getProspect();
			Criteria criteria = Criteria.where("prospectCall.prospect.firstName").is(p.getFirstName());
			criteria.and("prospectCall.prospect.lastName").is(p.getLastName());
			criteria.and("prospectCall.prospect.company").is(p.getCompany());
			criteria.and("prospectCall.prospect.title").is(p.getTitle());
			Query query = new Query();
			query.addCriteria(criteria);
			Document doc = new Document();
			mongoTemplate.getConverter().write(pNew,doc);
			action.replaceOne(query, doc, options.upsert());
		}
		action.execute();
	}


	public List<String> getPeopleIdList(List<ProspectCallLog> prospectReviwedList){
		List<String> contactList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(prospectReviwedList)){
			contactList = prospectReviwedList.stream()
					.map(rev -> rev.getProspectCall().getProspect().getSourceId())
					.collect(Collectors.toList());
		}
		return contactList;
	}
}
