package com.xtaas.db.repository;

import com.xtaas.db.entity.InsideviewDataSource;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface InsideviewDataSourceRepository extends MongoRepository<InsideviewDataSource, String> {
  
  @Query("{'peopleId' : ?0}")
  public InsideviewDataSource findByPeopleId(String peopleId);
  
}
