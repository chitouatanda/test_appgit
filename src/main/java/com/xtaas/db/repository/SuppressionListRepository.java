package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SuppressionList;

/**
 * SupressionListRepository
 */
public interface SuppressionListRepository extends MongoRepository<SuppressionList, String> {

	@Query("{ 'campaignId' : ?0 }")
	public List<SuppressionList> findSupressionByCampaignId(String campaignId);

	@Query("{ 'campaignId' : ?0, 'status' : 'ACTIVE' }")
	public SuppressionList findSupressionListByCampaignIdAndStatus(String campaignId);

	@Query("{ 'campaignId' : {'$in': ?0}, 'status' : 'ACTIVE' }")
	public List<SuppressionList> findSupressionListByCampaignIdsAndStatus(List<String> campaignId);
	
	@Query("{ 'organizationId' : ?0, 'status' : 'ACTIVE' }")
	public SuppressionList findGlobalSupressionListByOrganization(String organizationId);

}
