package com.xtaas.db.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.NewClientMapping;

public interface NewClientMappingRepository extends MongoRepository<NewClientMapping, String> {

	@Query("{ 'clientName' : ?0 }")
	public List<NewClientMapping> findByClientName(String clientName);

	@Query("{ 'campaignId' : ?0, 'clientName' : ?1 }")
	public List<NewClientMapping> findByCampaignIdAndClientName(String campaignId, String clientName);

	@Query("{ 'campaignId' : ?0 }")
	public List<NewClientMapping> findAllByCampaignIdAndBySequenceSort(String campaignId,
			Pageable pageRequest);

	@Query("{ 'campaignId' : ?0}")
	public List<NewClientMapping> findClientMappingByCampaignId(String campaignId);
	
	@Query("{ 'clientName' : ?0 , 'campaignId' : {$exists:false} }")
	public List<NewClientMapping> findClientMappingByClientName(String clientName);

}
