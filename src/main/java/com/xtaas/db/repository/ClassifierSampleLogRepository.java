package com.xtaas.db.repository;

import com.xtaas.db.entity.ClassifierSampleLog;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * <p>
 */
public interface ClassifierSampleLogRepository extends MongoRepository<ClassifierSampleLog, String> {
}
