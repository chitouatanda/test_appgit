package com.xtaas.db.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class DataBuyQueueRepositoryImpl implements DataBuyQueueRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public DataBuyQueue findByCampaignAndSortByJobRequestTime(String campaignId) {
		Query query = new Query(Criteria.where("campaignId").is(campaignId))
				.with(Sort.by(Sort.Direction.DESC, "jobRequestTime"));
		return mongoTemplate.findOne(query, DataBuyQueue.class);
	}

}
