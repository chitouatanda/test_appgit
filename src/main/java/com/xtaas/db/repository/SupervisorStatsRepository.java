package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.SupervisorStats;

public interface SupervisorStatsRepository extends MongoRepository<SupervisorStats, String> {

	@Query("{'supervisorId' : ?0}")
	public SupervisorStats findBySupervisorId(String supervisorId);
	
	@Query("{'supervisorId' : ?0, 'type' : ?1, 'status' : ?2}")
	public SupervisorStats findBySupervisorIdAndStatus(String supervisorId, String type, String status);
	
	@Query(value = "{ 'campaignManagerId' : ?0, 'type' : ?2, 'status' : ?3, 'campaignId' : ?1}")
	public SupervisorStats findBycampaignManagerIdAndcampaignId(String campaignManagerId,String campaignId,String type, String status);

	@Query("{'type' : ?0,'status' : ?1}")
	public List<SupervisorStats> findByTypeAndStatus(String type, String status);

	@Query("{'type' : ?0, 'status' : ?2, 'campaignId' : ?1}")
	public SupervisorStats findByCampaignIdTypeAndStatus(String type,String campaignId,String status);
	
	@Query("{'type' : ?0, 'status' : {$in : ?2}, 'campaignId' : ?1}")
	public SupervisorStats findByCampaignIdTypeAndStatusList(String type,String campaignId,List<String> status);
}
