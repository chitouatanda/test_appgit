package com.xtaas.db.repository;

import java.util.HashSet;
import java.util.List;

public interface CampaignContactRepositoryCustom {

	public HashSet<String> findExistingOrganizations(String campaignId, List<String> organizationNames);

}
