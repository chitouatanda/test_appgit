package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.OrgTypeExperienceMap;

public interface OrganizationTypesRepository extends MongoRepository<OrgTypeExperienceMap, String>{
    
    @Query("{ '_id' : ?0 }")
    public OrgTypeExperienceMap findOneById(String id); 
}
