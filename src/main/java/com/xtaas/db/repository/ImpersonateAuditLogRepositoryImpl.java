package com.xtaas.db.repository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.ImpersonateAuditLog;

public class ImpersonateAuditLogRepositoryImpl implements ImpersonateAuditLogRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Optional<ImpersonateAuditLog> findLatest(String supportAdminId, String impersonatedUserId) {
		Query query = new Query();
		query.limit(1);
		Criteria criteria = new Criteria();
		criteria.and("supportAdminId").is(supportAdminId);
		criteria.and("impersonatedUserId").is(impersonatedUserId);
		criteria.and("status").is(ImpersonateAuditLog.ImpersonateStatus.LOGIN);
		query.addCriteria(criteria);
		query.with(Sort.by(Sort.Direction.DESC, "updatedDate"));
		ImpersonateAuditLog impersonateAuditLog = mongoTemplate.findOne(query, ImpersonateAuditLog.class);
		return Optional.of(impersonateAuditLog);
	}

}
