package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.Contact;


public interface ContactRepository extends MongoRepository<Contact, String>, ContactRepositoryCustom {
	@Query("{ 'source' : ?0, 'sourceId': {'$in': ?1} }")
	public List<Contact> findBySourceId(String source, List<String> sourceIds);
	
	@Query("{ 'createdDate' : {$gte : ?0, $lte : ?1}}")
	public List<Contact> findPurchasedContactsInDateRange(Date startDate, Date endDate);

	@Query("{ 'listPurchaseTransactionId' : ?0 }")
	public List<Contact> findByListPurchaseTransactionId(String listPurchaseTransactionId);
	
	@Query("{ 'title' : {$regex: 'Marketing' }, 'hierarchyTitle' : {$in : ['Manager'] }, 'minEmployeeCount' : {$gte : 50}, 'maxEmployeeCount' : {$lte : 250}, 'source' : 'ZOOMINFO' }")
	public List<Contact> findContactsByCustomQuery(Pageable pageable);
}
