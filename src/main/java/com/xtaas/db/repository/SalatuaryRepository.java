package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Salatuary;


public interface SalatuaryRepository extends MongoRepository<Salatuary, String> {

	@Query(value="{}")
	public List<Salatuary> getdata(Pageable page);
	
	
}
