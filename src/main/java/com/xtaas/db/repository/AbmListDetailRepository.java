package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AbmListDetail;

public interface AbmListDetailRepository extends MongoRepository<AbmListDetail, String>, AbmListDetailRepositoryCustom {

	@Query(value = "{'companyId' : ?0 }", count = true)
	public long countByCompanyId(String companyId);

}
