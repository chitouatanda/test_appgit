package com.xtaas.db.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.db.entity.QaRejectReason;

public interface QaRejectReasonRepository extends MongoRepository<QaRejectReason, String> {
   public QaRejectReason findOneByType(String type);
}
