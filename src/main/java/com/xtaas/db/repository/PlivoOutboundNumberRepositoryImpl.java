package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.BasicDBObject;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;

public class PlivoOutboundNumberRepositoryImpl implements PlivoOutboundNumberRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(PlivoOutboundNumberRepositoryImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<CountryDetails> getDistinctCountryDetails() {
		logger.debug("fetching distinct country details.");
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(createGroupCriterias());
		AggregationResults<CountryDetails> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "plivooutboundnumber", CountryDetails.class);
		List<CountryDetails> countryDetails = results.getMappedResults();
		return countryDetails;
	}

	private AggregationOperation createGroupCriterias() {
		BasicDBObject group = new BasicDBObject("_id", new BasicDBObject("countryName", "$countryName")
				.append("countryCode", "$countryCode").append("isdCode", "$isdCode"));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public PlivoOutboundNumber findOneByIsdCode(int isd) {
		Query query = new Query();
		query.addCriteria(Criteria.where("isdCode").is(isd));
		return mongoTemplate.findOne(query, PlivoOutboundNumber.class);
	}

}
