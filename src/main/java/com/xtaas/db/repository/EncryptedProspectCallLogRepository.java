package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.EncryptedProspectCallLog;

/**
 * @author djain
 *
 */
public interface EncryptedProspectCallLogRepository extends MongoRepository<EncryptedProspectCallLog, String>{
	
}
