package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.ProspectStatusCorrection;


public interface ProspectStatusCorrectionRepository extends MongoRepository<ProspectStatusCorrection, String> {

}
