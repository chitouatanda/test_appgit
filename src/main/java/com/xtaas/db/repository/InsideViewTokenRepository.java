 package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.InsideViewToken;


public interface InsideViewTokenRepository extends MongoRepository<InsideViewToken, String> {

	@Query("{'status' : {'$in': ['ACTIVE']}}")
	public List<InsideViewToken> findLatestToken(Pageable pageable);
	
	
}
