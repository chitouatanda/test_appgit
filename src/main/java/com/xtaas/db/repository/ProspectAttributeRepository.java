package com.xtaas.db.repository;

import com.xtaas.domain.entity.ProspectAttribute;

public interface ProspectAttributeRepository extends RetrieveRepository<ProspectAttribute, String> {
}
