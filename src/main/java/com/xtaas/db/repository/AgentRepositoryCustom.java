package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xtaas.domain.valueobject.AgentSearchResult;

public interface AgentRepositoryCustom {
	public List<AgentSearchResult> searchAgents(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerHourPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimulAverageConversion, double minimumAverageQuality, int minimumAvailableHours,
			Pageable pageRequest);

	public List<Object> countAgents(List<String> countries, List<String> states, List<String> cities,
			List<String> timeZones, List<String> languages, List<Double> ratings, String domain,
			double maximumPerHourPrice, int minimumCompletedCampaigns, int minimumTotalVolume,
			double minimulAverageConversion, double minimumAverageQuality, int minimumAvailableHours);

}
