package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Vertical;

public interface VerticalRepository extends MongoRepository<Vertical, String> {
	@Query(value="{}", fields="{'domains' : 0 }")
	public List<Vertical> find();

	@Query("{'name' : {'$in' : ?0 }}")
	public List<Vertical> getByVertical(List<String> vertical);
}
