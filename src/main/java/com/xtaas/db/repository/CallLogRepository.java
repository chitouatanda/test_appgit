/**
 * 
 */
package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CallLog;

/**
 * @author djain
 *
 */
public interface CallLogRepository extends MongoRepository<CallLog, String> {
	@Query("{ '_id' : ?0 }")
	public CallLog findOneById(String id);

	@Query("{ 'callLogMap.CallSid' : {'$in' : ?0 }}")
	public ArrayList<CallLog> findCallLogsBySid(List<String> twilioCallSids);
	
	@Query("{ 'callLogMap.CallSid' : ?0 }")
	public CallLog findCallLogBySid(String twilioCallSid);
	
	@Query("{ '_id' : ?0 }")
	public CallLog findCallLogByCallSid(String callSid);
	
	@Query("{ 'callLogMap.pcid' : ?0 }")
	public List<CallLog> findCallLogByPcid(String pcid);
	
	@Query("{ 'callLogMap.ConferenceSid' : ?0 }")
	public ArrayList<CallLog> findCallLogsByConferenceSid(String conferenceSid);

	
}
