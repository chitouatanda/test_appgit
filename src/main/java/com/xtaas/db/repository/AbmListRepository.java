package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AbmList;


public interface AbmListRepository extends MongoRepository<AbmList, String> {

	@Query("{ 'campaignId' : ?0 }")
	public List<AbmList> findABMListByCampaignId(String campaignId);

	@Query("{ 'campaignId' : ?0 }")
	public AbmList findABMListListByCampaignIdAndStatus(String campaignId);
	
}
