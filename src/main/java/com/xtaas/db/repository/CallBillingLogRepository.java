/**
 * 
 */
package com.xtaas.db.repository;

import java.util.ArrayList;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CallBillingLog;

/**
 * @author djain
 *
 */
public interface CallBillingLogRepository extends MongoRepository<CallBillingLog, String> {
	@Query("{ '_id' : ?0 }")
	public CallBillingLog findOneById(String id);

	@Query("{ }")
	public ArrayList<CallBillingLog> findLatestTimestamp(Pageable pageable);
	
}