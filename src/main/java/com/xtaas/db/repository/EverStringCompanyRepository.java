package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.EverStringCompany;

public interface EverStringCompanyRepository
		extends MongoRepository<EverStringCompany, String>, EverStringCompanyRepositoryCustom {

	@Query(value = "{'_id' : ?0 }", count = true)
	public int countById(String id);

	@Query(value = "{'ecid' : {'$in' : ?0 } }")
	public List<EverStringCompany> findByIdIn(List<Integer> ecid);

	@Query(value = "{status : ?0,campaignId : ?1}")
	public List<EverStringCompany> findByStatusAndIds(String status, String campaignId);
	
	@Query(value = "{status : ?0}")
	public List<EverStringCompany> findByStatus(String status);

	@Query(value = "{ecid : ?0}")
	public EverStringCompany findByEcid(int ecid);

}
