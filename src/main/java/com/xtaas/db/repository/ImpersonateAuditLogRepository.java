package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.ImpersonateAuditLog;

public interface ImpersonateAuditLogRepository
		extends MongoRepository<ImpersonateAuditLog, String>, ImpersonateAuditLogRepositoryCustom {

}
