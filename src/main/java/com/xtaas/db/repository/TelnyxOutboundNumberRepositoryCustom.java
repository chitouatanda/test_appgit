package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.valueobject.CountryDetails;

public interface TelnyxOutboundNumberRepositoryCustom {

  public List<CountryDetails> getDistinctCountryDetails();

}