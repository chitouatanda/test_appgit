package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.EmailBounces;
import com.xtaas.domain.entity.Team;

public interface EmailBouncesRepositoryCustom {

    public int countEmailBounces(String campaignId, Date fromDate, Date toDate);
    public List<EmailBounces> findByEventIdAndMessageId(String prospectInteractionSessionId, String sg_event_id, String sg_message_id);
    
}
