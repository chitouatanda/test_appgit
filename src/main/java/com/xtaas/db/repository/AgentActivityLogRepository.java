/**
 * 
 */
package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityCallStatus;

/**
 * @author djain
 *
 */
public interface AgentActivityLogRepository extends MongoRepository<AgentActivityLog, String>, AgentActivityLogRepositoryCustom {
	
	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE', 'status' : 'IDLE'}")
	public List<AgentActivityLog> getLastLoginTime(String agentId, Pageable pageable);
	
	@Query("{'campaignId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE', 'status' : 'ONLINE'}")
	public List<AgentActivityLog> findLatestOnlineAgent(String campaignId, Pageable pageable);
	
	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE'}")
	public List<AgentActivityLog> getLastStatus(String agentId, Pageable pageable);
	
	@Query(value = "{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE', 'createdDate' : {$gte : ?1, $lte : ?2}}")
	public  List<AgentActivityLog> getLastStatus(String agentId, Date fromDate, Date toDate, Pageable pageable);
	
	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE', 'createdDate' : {$gte : ?1, $lte : ?2}}")
	public List<AgentActivityLog> getStatusesWithinDateRange(String agentId, Date fromDate, Date toDate, Sort sort);
	
	@Query("{'agentId' : ?0, 'status' : 'ONLINE', 'createdDate' : {$gte : ?1, $lte : ?2}}")
	public List<AgentActivityLog> getOnlineStatusesWithinDateRange(String agentId, Date fromDate, Date toDate, Sort sort);
	
	@Query("{'agentId' : ?0, 'activityType' : 'CALL_STATUS_CHANGE', 'pcid' : ?1}")
	public List<AgentActivityLog> getLastCallStatus(String agentId, String prospectCallId, Pageable pageable);
	
	@Query("{'agentId' : ?0, 'status': {'$in': ?1}}")
	public List<AgentActivityLog> getLastAgentActivityLog(String agentId, List<String> statuses, Pageable pageable);
	
	@Query("{'agentId' : ?0, 'activityType' : 'CALL_STATUS_CHANGE', 'pcid' : ?1, 'status' : ?2}")
	public List<AgentActivityLog> fetchLatestAgentCallPreviewStatus(String agentId, String prospectCallId, AgentActivityCallStatus agentActivityCallStatusString, Pageable pageable);
	
	@Query("{'agentId' : ?0, 'status' : {$in : ['ONLINE','ONBREAK','MEETING','TRAINING','OFFLINE','OTHER']}, 'createdDate' : {$gte : ?1, $lte : ?2}}")
	public List<AgentActivityLog> getStatusesForReporting(String agentId, Date fromDate, Date toDate, Sort sort);
	
	@Query("{'pcid' : ?0, 'activityType' : 'CALL_STATUS_CHANGE', 'status' : ?1}")
	public List<AgentActivityLog> findByPcidAndStatus(String prospectCallId, AgentActivityCallStatus agentActivityCallStatusString, Pageable pageable);
	
}
