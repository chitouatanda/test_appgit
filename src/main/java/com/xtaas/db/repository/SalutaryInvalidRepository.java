package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.ProspectSalutaryInvalid;

public interface SalutaryInvalidRepository extends MongoRepository<ProspectSalutaryInvalid, String> {

	@Query(value = "{'prospectCall.campaignId' : ?0}")
	public List<ProspectSalutaryInvalid> findSalutaryProspects(String campaignId, Pageable pageable);
	
	
}
