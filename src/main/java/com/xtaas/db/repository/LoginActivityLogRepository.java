/**
 * 
 */
package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.db.entity.LoginActivityLog;

/**
 * @author djain
 *
 */
public interface LoginActivityLogRepository extends MongoRepository<LoginActivityLog, String> {
	
}
