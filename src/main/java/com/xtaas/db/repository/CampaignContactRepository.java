package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.Contact;

public interface CampaignContactRepository extends MongoRepository<CampaignContact, String>, CampaignContactRepositoryCustom {
	@Query("{ 'campaignId' : ?0 }")
	public List<CampaignContact> findByCampaignId(String campaignId);
	
	@Query("{ 'listPurchaseTransactionId' : ?0 }")
	public List<CampaignContact> findByListPurchaseTransactionId(String listPurchaseTransactionId);
	
	@Query("{ 'sourceId' : ?0, 'source' : ?1 }")
	public CampaignContact findBySourceId(String sourceId, String source);
	
	@Query("{    'maxEmployeeCount' : {$lte : 500}, 'maxEmployeeCount' : {$ne : 0},    'minEmployeeCount' : {$gte : 25},    'title' : {$regex : 'Manager'}, 'title' : {$regex : 'Technology'},    'industries.name' : {$in : ['Software', 'Custom Software & Technical Consulting', 'Software Development & Design','Healthcare',        'Emergency Medical Transportation & Services','Hospitals & Clinics', 'Medical Testing & Clinical Laboratories',        'Pharmaceuticals','Biotechnology','Drug Manufacturing & Research', 'Manufacturing', 'Aerospace & Defense',        'Automobiles', 'Boats & Submarines','Building Materials','Aggregates, Concrete & Cement', 'Lumber & Wood Production',        'Miscellaneous Building Materials - Flooring, Cabinets, etc.', 'Plumbing & HVAC Equipment','Chemicals, Petrochemicals, Glass & Gases',        'Chemicals', 'Gases', 'Glass & Clay', 'Petrochemicals','Computer Equipment & Peripherals','Computer Networking Equipment',        'Computer Storage Equipment','Network Security Hardware & Software','Personal Computers & Peripherals','Consumer Goods','Appliances',        'Cleaning Products','Consumer Electronics','Cosmetics, Beauty Supply & Personal Care Products','Household Goods','Jewelry & Watches Manufacturing',        'Pet Products Manufacturing','Photographic & Optical Equipment','Sporting Goods','Textiles & Apparel','Electronics','Batteries, Power Storage Equipment & Generators',        'Electronic Components','Power Conversion & Protection Equipment','Semiconductor & Semiconductor Equipment','Food, Beverages & Tobacco',        'Food & Beverages','Tobacco','Wineries & Breweries','Furniture Manufacturing','Industrial Machinery & Equipment','Medical Devices & Equipment',        'Plastic, Packaging & Containers','Pulp & Paper','Telecommunication Equipment','Test & Measurement Equipment','Tires & Rubber','Toys & Games Manufacturing',        'Wire & Cable','Finance','Banking','Brokerage','Credit Cards & Transaction Processing','Investment Banking','Venture Capital & Private Equity',        'Business Services','Accounting & Accounting Services','Advertising & Marketing','Auctions','Call Centers & Business Centers','Commercial Printing',        'Debt Collection','Facilities Management & Commercial Cleaning','Human Resources & Staffing','Information & Document Management','Management Consulting',        'Multimedia & Graphic Design','Security Products & Services','Translation & Linguistic Services']},     'createdDate' : {$lte : ?0}    ,'source' : 'ZOOMINFO'    }")
	public List<CampaignContact> findContactsByCustomQuery(Date createdDate, Pageable pageable);
	
	@Query("{ 'firstName' : ?0, 'lastName' : ?1, 'organizationName' : ?2, 'campaignId' : ?3 }")
	public CampaignContact findByPersonNameAndCompany(String firstName, String lastName, String company, String campaignId);
	
	@Query("{ 'firstName' : ?0, 'lastName' : ?1, 'organizationName' : ?2, 'campaignId' : ?3 ,'title' : ?4}")
    public CampaignContact findByPersonNameAndCompanyAndTitle(String firstName, String lastName, String company, String campaignId, String title);
	
	@Query("{ 'firstName' : ?0, 'lastName' : ?1, 'organizationName' : ?2, 'phone' : ?3, 'source' : ?1 }")
    public CampaignContact findByPersonNameAndCompanyAndPhone(String firstName, String lastName, String company, String phone, String source);
	
}
