package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.EmailBounces;

public class EmailBouncesRepositoryImpl implements EmailBouncesRepositoryCustom{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int countEmailBounces(String campaignId, Date fromDate, Date toDate) {
        return mongoTemplate.find(
                Query.query(
                        buildEmailBounceCriteria(campaignId, fromDate, toDate)), EmailBounces.class).size();
    }
   
    
    private Criteria buildEmailBounceCriteria(String campaignId, Date fromDate,Date toDate) {
        Criteria criteria = Criteria.where("campaignId").in(campaignId);
        if (fromDate != null && toDate == null) {
            criteria = criteria.and("createdDate").gte(fromDate);
        }
        if (fromDate == null && toDate != null) {
            criteria = criteria.and("createdDate").lte(toDate); 
        }
        if (fromDate != null && toDate != null) {
            criteria = criteria.and("createdDate").gte(fromDate).lte(toDate);
        }
        return criteria;
    }


	@Override
	public List<EmailBounces> findByEventIdAndMessageId(String prospectCallInteractionId, String sg_event_id,
			String sg_message_id) {
		return mongoTemplate.find(
                Query.query(
                		buildCriteriaByEventIdAndMessageId(prospectCallInteractionId, sg_event_id, sg_message_id)), EmailBounces.class);
	}
	
	private Criteria buildCriteriaByEventIdAndMessageId(String prospectInteractionSessionId, String sg_event_id, String sg_message_id) {
		Criteria criteria = Criteria.where("prospectInteractionSessionId").in(prospectInteractionSessionId);
		criteria = criteria.and("feedbackID").in(sg_event_id);
		criteria = criteria.and("emailBounceMessageID").in(sg_message_id);
        return criteria;
	}

}
