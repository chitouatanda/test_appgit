package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.valueobject.CountResult;

public interface ContactRepositoryCustom {
	public List<Contact> searchContacts(List<String> industries, List<String> functions,
			List<String> mgmtLevels, List<String> titles, List<String> domains, List<String> countries,
			List<String> states, List<String> cities, Integer minimumRevenue, Integer maximumRevenue,
			Integer minimumEmployeeCount, Integer maximumEmployeeCount, int offset, int size);

	public List<CountResult> countContacts(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount);
	
	public String getQueryString(List<String> industries, List<String> functions, List<String> mgmtLevels,
			List<String> titles, List<String> domains, List<String> countries, List<String> states,
			List<String> cities, Integer minimumRevenue, Integer maximumRevenue, Integer minimumEmployeeCount,
			Integer maximumEmployeeCount);
}
