package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author djain
 *
 */
public interface ProspectCallInteractionRepository extends MongoRepository<ProspectCallInteraction, String>, ProspectCallInteractionRepositoryCustom {
	
	@Query("{ '_id' : ?0 }")
	public ProspectCallInteraction findOneById(String id);
	
	@Query("{'prospectCall.campaignId' : ?3, 'status':'WRAPUP_COMPLETE','prospectCall.dispositionStatus' : ?2, 'updatedDate' : {$gte : ?0, $lte : ?1}}")
	public List<ProspectCallInteraction> findBychecksleads(Date startDate, Date endDate, String dispositionStatus, String campaignId);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0, 'prospectCall.twilioCallSid' : ?1 }")
	public List<ProspectCallInteraction> findByProspectCallIdTwilioCallSid(String prospectCallId, String twilioCallSid);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0,  'status' : 'SECONDARY_QA_INITIATED','prospectCall.subStatus' : 'SUBMITLEAD' }")
	 public List<ProspectCallInteraction> findLeadStatus(String pcid);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0, 'status' : 'CALL_INPROGRESS', 'createdDate' : {$gte : ?1} }")
	public List<ProspectCallInteraction> findInProgressCallInteraction(String prospectCallId, Date todaysDate, Pageable pageable);
	
	@Query("{ 'prospectCall.prospectInteractionSessionId' : ?0 }")
	public List<ProspectCallInteraction> findByProspectInteractionSessionId(String prospectInteractionSessionId);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'updatedDate' : {$gte : ?1}, 'updatedBy' : {$in : ['dialer','SYSTEM']}}")
	public List<ProspectCallInteraction> findRecentDialerProcessed(String campaignId, Date date, Pageable pageable);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'updatedDate' : {$gte : ?1}}")
	public List<ProspectCallInteraction> findInteractionByCIDAndDate(String campaignId, Date date);
	
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.prospect.company' : ?1, 'prospectCall.dispositionStatus' :'SUCCESS' }")
	 public List<ProspectCallInteraction> findSucessRecordByCampaignAndCompany(String campaignId, String companyName);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public ProspectCallInteraction findByProspectCallId(String prospectCallId);

	@Query("{ 'status':'CALL_DIALED','prospectCall.prospectCallId' : {$in : ?0 } }")
	public List<ProspectCallInteraction> findByProspectCallIds(List<String> prospectCallIds);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public List<ProspectCallInteraction> findAllByProspectCallId(String prospectCallId, Pageable pageable);
	
	////Below Reporting Queries for Campaign Performance
	@Query(value = "{'prospectCall.dispositionStatus':'SUCCESS','prospectCall.campaignId' : ?0 ,'status':'WRAPUP_COMPLETE','prospectCall.callStartTime' : {$gte : ?1 }}", count = true)
	public int getCampaignSuccessCount(String campaignId, Date startDate);
	

	@Query(value = "{'prospectCall.campaignId':?0, 'status' : 'WRAPUP_COMPLETE','prospectCall.dispositionStatus' : 'SUCCESS'}")
	public List<ProspectCallInteraction> getLeadCount(String campaignId);
	

	@Query(value = "{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : ?0,'status': {$in : ['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE']},'prospectCall.subStatus':{$in:['LEAD_GENERATED','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OUT_OF_COUNTRY','CALLBACK_USER_GENERATED']},'prospectCall.callStartTime' : {$gte : ?1 }}", count = true)	
	public int getCampaignConnectCount(String campaignId, Date startDate);
	
	
	@Query(value = "{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : ?0,'status': {$in : ['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE']} ,'prospectCall.subStatus' :{$in:['LEAD_GENERATED','ANSWERMACHINE','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']},'prospectCall.callStartTime' : {$gte : ?1 }}", count = true)     
	public int getCampaignContactCount(String campaignId, Date startDate);  
	
	@Query(value = "{'status':'CALL_DIALED','prospectCall.campaignId' : ?0,'updatedDate' : {$gte : ?1}}", count = true)     
	public int getCampaignDialedCount(String campaignId, Date startDate);

	//////End of Reporting Queries Campaign Performance
	/////Below Queries for Agent Performance
	
	@Query(value = "{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : ?0},'prospectCall.agentId': ?1,'status': {$in : ['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE']} ,'prospectCall.subStatus' :{$in:['LEAD_GENERATED','ANSWERMACHIN','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']},'prospectCall.callStartTime' : {$gte : ?2}}", count = true)
	public int getCampaignAgentContactedCount(String campaignId,String agentid, Date startDate);

	@Query(value = "{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : ?0},'prospectCall.agentId': ?1,'status': {$in : ['WRAPUP_COMPLETE','QA_WRAPUP_COMPLETE']} ,'prospectCall.subStatus :{$in:['LEAD_GENERATED','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OUT_OF_COUNTRY','CALLBACK_USER_GENERATED']},'prospectCall.callStartTime' : {$gte : ?2}}", count = true)
	public int getCampaignAgentConnectCount(String campaignId,String agentid, Date startDate);
   
	@Query(value = "{'prospectCall.dispositionStatus' : 'SUCCESS','prospectCall.campaignId' : ?0},'prospectCall.agentId': ?1,'status': 'WRAPUP_COMPLETE','prospectCall.callStartTime' : {$gte : ?2}}", count = true)
	public int getCampaignAgentSuccessCount(String campaignId,String agentid, Date startDate);
	
	/////End of Agent Performannce Queries

	// DATE:06/03/2018	REASON:Added to fetch records from DB using following two parameters.
	public ProspectCallInteraction findByProspectCallProspectCallIdAndStatus(String prospectCallId, ProspectCallStatus status);
	
	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospect.email' : ?1 }")
	public ProspectCallInteraction getProspectByCIDAndEmail(String campaignId,String email);
	
	@Query("{ 'prospectCall.campaignId' : ?0, 'prospectCall.prospectCallId' : ?1 , 'prospectCall.dispositionStatus':'SUCCESS', 'status': 'WRAPUP_COMPLETE'}")
	public ProspectCallInteraction getProspectByCIDAndpcid(String campaignId,String pcid);
	
	// queries to get count for campaign report - start
	@Query(value = "{'prospectCall.campaignId' : ?0, 'status' : 'CALL_DIALED', 'updatedDate' : {$gte: ?1}}", count = true)
	public Integer getCampaignRecordsAttemptedCount(String campaignId, Date fromDateStr);
	
	@Query(value = "{'prospectCall.campaignId' : ?1, 'status' : 'WRAPUP_COMPLETE', 'updatedDate' : {$gte: ?0}}", count = true)
	public Integer getCampaignRecordsDialedByAgents(Date fromDateStr, String campaignId);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']}, 'status': {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.subStatus' :{$in:['AUTO','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']}, 'updatedDate' : {$gte: ?1} }", count = true)
	public Integer getCampaignContactedRecordsCount(String campaignId, Date fromDateStr);
	
	@Query(value = "{'prospectCall.campaignId' : ?0, 'status': 'WRAPUP_COMPLETE', 'prospectCall.subStatus': {$in:['OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION', 'NOTINTERESTED', 'NO_CONSENT', 'SUBMITLEAD']}, 'updatedDate' : {$gte: ?1}}", count = true)
	public Integer getCampaignConnectedRecordsCount(String campaignId, Date fromDateStr);
	

	@Query(value = "{'prospectCall.campaignId' : ?0, 'status': 'WRAPUP_COMPLETE', 'prospectCall.subStatus': {$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION', 'NOTINTERESTED', 'NO_CONSENT', 'SUBMITLEAD']}, 'updatedDate' : {$gte: ?1}}")
	public List<ProspectCallInteraction> getCampaignConnectedRecordsCountList(String campaignId, Date fromDateStr);

	@Query(value = "{'prospectCall.campaignId' : ?0, 'prospectCall.dispositionStatus':'SUCCESS', 'status':'WRAPUP_COMPLETE', 'prospectCall.callStartTime' : {$gte: ?1}}",count = true)
	public Integer getCampaignSuccessRecordCount(String campaignId, Date fromDateStr);
	// queries to get count for campaign report - end
	
	// queries to get count for agent report - start
	@Query(value = "{'prospectCall.campaignId' : ?2,'prospectCall.agentId' : ?0, 'status' : 'WRAPUP_COMPLETE', 'updatedDate' : {$gte: ?1}}", count = true)
	public Integer getAgentRecordsAttemptedCount(String agentId, Date fromDateStr, String campaignId);
	
	@Query(value = "{'prospectCall.campaignId' : ?2,'prospectCall.agentId' : ?0, 'prospectCall.dispositionStatus' : {$in : ['OTHER','OUT_OF_COUNTRY','DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']}, 'status': {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.subStatus' :{$in:['AUTO','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE','SUBMITLEAD']}, 'updatedDate' : {$gte: ?1} }", count = true)
	public Integer getAgentContactedRecordsCount(String agentId, Date fromDateStr, String campaignId);
	
	@Query(value = "{'prospectCall.campaignId' : ?2,'prospectCall.agentId' : ?0, 'status': 'WRAPUP_COMPLETE', 'prospectCall.subStatus': {$in:['OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION', 'NOTINTERESTED', 'NO_CONSENT', 'SUBMITLEAD']}, 'updatedDate' : {$gte: ?1}}", count = true)
	public Integer getAgentConnectedRecordsCount(String agentId, Date fromDateStr, String campaignId);

	@Query(value = "{'prospectCall.campaignId' : ?2,'prospectCall.agentId' : ?0, 'status': 'WRAPUP_COMPLETE', 'prospectCall.subStatus': {$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION', 'NOTINTERESTED', 'NO_CONSENT', 'SUBMITLEAD']}, 'updatedDate' : {$gte: ?1}}")
	public List<ProspectCallInteraction> getAgentConnectedRecordsCountList(String agentId, Date fromDateStr, String campaignId);

	
	@Query(value = "{'prospectCall.campaignId' : ?2,'prospectCall.agentId' : ?0, 'prospectCall.dispositionStatus':'SUCCESS', 'status':'WRAPUP_COMPLETE', 'prospectCall.callStartTime' : {$gte: ?1}}",count = true)
	public Integer getAgentSuccessRecordCount(String agentId, Date fromDateStr, String campaignId);
	
	@Query(value= "{ 'prospectCall.prospectCallId' : ?0 }")
	public List<ProspectCallInteraction> findByProspectCallIdWithSort(String prospectCallId, Pageable pageable);
	// queries to get count for agent report - end

	@Query(value= "{'status' : 'CALL_DIALED', 'prospectCall.campaignId' : {$in : ?0 }, 'updatedDate' : {$gte: ?1, $lt: ?2}}", count = true)
	public int findRecordsAttempted(List<String> ids, Date startDate, Date endDate);
	
	@Query(value= "{'status' : {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.dispositionStatus' : {$in : ['OTHER','OUT_OF_COUNTRY','DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']}, 'prospectCall.subStatus' :{$in:['ANSWERMACHINE','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']},'prospectCall.campaignId' : {$in : ?0 }, 'prospectCall.callStartTime' : {$gte: ?1, $lt: ?2}}", count = true)
	public int findContactedRecordsCount(List<String> ids, Date startDate, Date endDate);
	
	@Query(value= "{'status' : {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.subStatus' :{$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION','NOTINTERESTED','NO_CONSENT','SUBMITLEAD']},'prospectCall.campaignId' : {$in : ?0 }, 'prospectCall.callStartTime' : {$gte: ?1, $lt: ?2}}", count = true)
	public int findConnectRecordsCount(List<String> ids, Date startDate, Date endDate);
	
	@Query(value= "{'prospectCall.dispositionStatus': 'SUCCESS','prospectCall.campaignId' : {$in : ?0 }, 'status':'WRAPUP_COMPLETE', 'prospectCall.callStartTime' : {$gte: ?1, $lt: ?2}}", count = true)
	public int findSuccessRecordsCount(List<String> ids, Date startDate, Date endDate);
	
	@Query(value="{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : {$in : ?0},'status': {$in : ['WRAPUP_COMPLETE']},'prospectCall.subStatus' :{$in:['AUTO','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']},'updatedDate': {$gte: ?1,$lt: ?2}}", count = true)
	public int findContactedRecodsForServiceModel(List<String> ids, Date startDate, Date endDate);
	
	@Query(value="{'prospectCall.campaignId' : {$in : ?0},'status': 'WRAPUP_COMPLETE','prospectCall.telcoDuration': {$gte:20},'prospectCall.subStatus': {$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION','NOTINTERESTED','NO_CONSENT','SUBMITLEAD']},'prospectCall.callStartTime' : {$gte: ?1,$lt: ?2}}", count = true)
	public int findConnectedRecodsForServiceModel(List<String> ids, Date startDate, Date endDate);
	
	@Query(value= "{'prospectCall.dispositionStatus' : {$in : ['DIALERCODE','FAILURE','AUTO','SUCCESS','CALLBACK']},'prospectCall.campaignId' : {$in : ?0},'status': {$in : ['WRAPUP_COMPLETE']},'prospectCall.subStatus' :{$in:['ANSWERMACHINE','DNCL','DNRM','FAILED_QUALIFICATION','NO_CONSENT','NOTINTERESTED','OTHER','OUT_OF_COUNTRY','PROSPECT_UNREACHABLE','CALLBACK_USER_GENERATED','CALLBACK_GATEKEEPER','GATEKEEPER_ANSWERMACHINE']},'updatedDate' : {$gte: ?1,$lt: ?2}}", count= true)
	public int findContactedRecodsForPlatformModel(List<String> ids, Date startDate, Date endDate);
	
	@Query(value="{'prospectCall.campaignId' : {$in : ?0},'status': 'WRAPUP_COMPLETE','prospectCall.telcoDuration': {$gte:30},'prospectCall.subStatus': {$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION','NOTINTERESTED','NO_CONSENT','SUBMITLEAD']},'prospectCall.callStartTime' : {$gte: ?1,$lt: ?2}}", count = true)
	public int findConnectedRecodsForPlatformeModel(List<String> ids, Date startDate, Date endDate);
	
	@Query(value="{'prospectCall.campaignId' : {$in : ?0},'status': {$in : ['WRAPUP_COMPLETE']},'prospectCall.subStatus' :{$in:['ANSWERMACHINE','GATEKEEPER_ANSWERMACHINE']},'updatedDate' : {$gte: ?1,$lt: ?2}}", count = true)
	public int findAnswermachineCount(List<String> ids, Date startDate, Date endDate);
	
	@Query(value="{'prospectCall.campaignId' : {$in : ?0},'status':'CALL_ABANDONED','updatedDate' : {$gte: ?1,$lt: ?2}}", count = true)
	public int findAbondonedRecordsCount(List<String> ids, Date startDate, Date endDate);
	
	@Query(value = "{'prospectCall.campaignId' : {$in : ?0},'prospectCall.agentId': ?1,'status': {$in : ['WRAPUP_COMPLETE']},'prospectCall.callStartTime' :  {$gte : ?2, $lte : ?3}}", count = true)
	public int getAgentDialedCount(List<String> campaignIds,String agentid, Date startDate,Date endDate);
	
	@Query(value = "{'prospectCall.campaignId' : {$in : ?0},'prospectCall.agentId': ?1,'prospectCall.dispositionStatus' : {$in : ['FAILURE','SUCCESS']},'status': {$in : ['WRAPUP_COMPLETE']} ,'prospectCall.callStartTime' : {$gte : ?2, $lte : ?3}}", count = true)
	public int getAgentContactCount(List<String> campaignIds,String agentid, Date startDate,Date endDate);
	
	@Query(value = "{'prospectCall.campaignId' : {$in : ?0},'prospectCall.agentId': ?1,'prospectCall.dispositionStatus' : 'SUCCESS','status': 'WRAPUP_COMPLETE','prospectCall.callStartTime' :  {$gte : ?2, $lte : ?3}}", count = true)
	public int getAgentSuccessCount(List<String> campaignIds,String agentid, Date startDate,Date endDate);
	
	@Query(value= "{'prospectCall.campaignId' : {$in : ?0 },'prospectCall.agentId': ?1,'status' : {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.subStatus' :{$in:['OTHER','OUT_OF_COUNTRY','DNCL','FAILED_QUALIFICATION','NOTINTERESTED','NO_CONSENT','SUBMITLEAD']}, 'prospectCall.callStartTime' : {$gte: ?2, $lt: ?3}}", count = true)
	public int getAgentConnectCount(List<String> ids,String agentId, Date startDate, Date endDate);
	
	@Query(value= "{'prospectCall.campaignId' : {$in : ?0 },'prospectCall.agentId': ?1,'status' : {$in : ['WRAPUP_COMPLETE']}, 'prospectCall.subStatus' :{$in:['DNCL','FAILED_QUALIFICATION','NOTINTERESTED','NO_CONSENT','SUBMITLEAD']}, 'prospectCall.callStartTime' : {$gte: ?2, $lt: ?3}}", count = true)
	public List<ProspectCallInteraction> getAgentConnectCountList(List<String> ids,String agentId, Date startDate, Date endDate);
	
	
	@Query("{'prospectCall.prospect.firstName' : ?0, 'prospectCall.prospect.lastName' : ?1, 'prospectCall.prospect.company' : ?2, 'prospectCall.prospect.phone' : ?3, 'prospectCall.campaignId' : {$in : ?4}}")
    public List<ProspectCallInteraction> prospectCallInteractionListStatus(String fname, String lname, String company,String phone,List<String> campaignId);
	
	@Query("{'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?0,$lte: ?1}}")
	public List<ProspectCallInteraction> findByDate(Date startDate, Date endDate, Pageable pageable);
	
	@Query(value = "{'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?0,$lte: ?1}}", count = true)
	public int findByDateCount(Date startDate, Date endDate);
	
	@Query(value = "{'prospectCall.campaignId' : ?0 ,'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?1,$lte: ?2}}", count = true)
	public int findByCampaignDateCount(String campaignId, Date startDate, Date endDate);
	
	@Query("{'prospectCall.campaignId' : ?0 ,'status' : {$nin : ['MAX_RETRY_LIMIT_REACHED','QUEUED','LOCAL_DNC_ERROR','UNKNOWN_ERROR','SUCCESS_DUPLICATE']} ,'updatedDate': {$gte:?1,$lte: ?2}}")
	public List<ProspectCallInteraction> findByCampaignDate(String campaignId,Date startDate, Date endDate, Pageable pageable);
	
}
