/**
 * 
 */
package com.xtaas.db.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.domain.entity.OutboundNumber;

/**
 * @author djain
 *
 */
public interface OutboundNumberRepository
		extends MongoRepository<OutboundNumber, String>, OutboundNumberRepositoryCustom {
	
	@Query("{ '_id' : ?0 }")
	public OutboundNumber findOneById(String id);
	
	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2}")
	public List<OutboundNumber> findByPartnerBucketWithAreaCode(String partnerId, String bucketName, int areaCode);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1}")
	public List<OutboundNumber> findByPartnerBucket(String partnerId, String bucketName);

	@Query("{'partnerId' : ?0}")
	public List<OutboundNumber> findOutboundNumbers(String partnerId);

	@Query("{'partnerId' : ?0, 'bucketName' : ?1, 'areaCode' : ?2, 'pool': ?3}")
	public List<OutboundNumber> findByPartnerBucketAreaCodePool(String partnerId, String bucketName, int areaCode,
			int pool);

	@Query("{'bucketName' : ?0, 'areaCode' : ?1}")
	public List<OutboundNumber> findByBucketWithAreaCode(String bucketName, int areaCode);

	@Query("{'areaCode' : ?0}")
	public List<OutboundNumber> findTimeZone(int areaCode);

	@Query("{'bucketName' : ?0}")
	public List<OutboundNumber> findOutboundNumbersByBucketNumber(String bucketName);
}
