package com.xtaas.db.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.AbmListDetail;

public class AbmListDetailRepositoryImpl implements AbmListDetailRepositoryCustom {
	private static final Logger logger = LoggerFactory.getLogger(AbmListDetailRepositoryImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<AbmListDetail> getSicAndNaicsList(String companyId) {
		Query query = new Query();
		query.addCriteria(buildCriteriaByCompanyId(companyId));
		query.fields().include("companySIC").include("companyNAICS").exclude("_id");
		// return mongoTemplate.find(query.with(pageRequest), AbmListDetail.class);
		return mongoTemplate.find(query, AbmListDetail.class);

	}

	public List<AbmListDetail> getCompanyEmployeeRevenueCount(String companyId) {
		Query query = new Query();
		query.addCriteria(buildCriteriaByCompanyId(companyId));
		query.fields().include("companyEmployeeCount").include("companyRevenue").exclude("_id");
		// return mongoTemplate.find(query.with(pageRequest), AbmListDetail.class);
		return mongoTemplate.find(query, AbmListDetail.class);

	}

	public List<AbmListDetail> getCompanyByDomain(String domain) {
		Query query = new Query();
		query.addCriteria(buildCriteriaByDomain(domain));
		query.fields().include("companyEmployeeCountRange").include("companyRevenueRange").include("companyWebsite")
				.include("companyRevenueIn000s").include("companyEmployeeCount").include("companyId")
				.include("companyNAICS").include("companySIC").exclude("_id");
		// return mongoTemplate.find(query.with(pageRequest), AbmListDetail.class);
		return mongoTemplate.find(query, AbmListDetail.class);

	}

	private Criteria buildCriteriaByCompanyId(String companyId) {
		Criteria criteria = new Criteria();
		criteria = criteria.and("companyId").in(companyId);
		return criteria;
	}

	private Criteria buildCriteriaByDomain(String domain) {
		Criteria criteria = new Criteria();
		criteria = criteria.and("companyWebsite").in(domain);
		criteria = criteria.and("companyName").exists(true);
		return criteria;
	}

	@Override
	public AbmListDetail findOneByCompanyId(String companyId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("companyId").is(companyId));
		return mongoTemplate.findOne(query, AbmListDetail.class);
	}

	@Override
	public AbmListDetail findOneByDomain(String domain) {
		Query query = new Query();
		Criteria criteria = Criteria.where("companyWebsite").is(domain);
		criteria = criteria.and("companyName").exists(true);
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, AbmListDetail.class);
	}

	@Override
	public AbmListDetail findOneByDomainRegex(String domain) {
		Query query = new Query();
		query.addCriteria(Criteria.where("companyWebsite").regex(domain, "i"));
		return mongoTemplate.findOne(query, AbmListDetail.class);
	}

}
