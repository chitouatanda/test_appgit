package com.xtaas.db.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.xtaas.db.entity.GlobalContact;

public class GlobalContactRepositoryImpl implements GlobalContactRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public long countUniqueRecordByFLP(String firstName, String lastName, String phone) {
		Criteria criteria = new Criteria();
		if (firstName != null && lastName != null && phone != null) {
			criteria = criteria.and("firstName").in(firstName);
			criteria = criteria.and("lastName").in(lastName);
			criteria = criteria.and("phone").in(phone);
		} else {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.count(query, GlobalContact.class);
	}

	@Override
	public GlobalContact findUniqueRecordByFLP(String firstName, String lastName, String phone) {
		Criteria criteria = new Criteria();
		if (firstName != null && lastName != null && phone != null) {
			criteria = criteria.and("firstName").in(firstName);
			criteria = criteria.and("lastName").in(lastName);
			criteria = criteria.and("phone").in(phone);
		}
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, GlobalContact.class);
	}

	@Override
	public GlobalContact findUniqueRecordByFLTC(String firstName, String lastName, String title, String company) {
		Criteria criteria = new Criteria();
		if (firstName != null && lastName != null && title != null && company != null) {
			criteria = criteria.and("firstName").in(firstName);
			criteria = criteria.and("lastName").in(lastName);
			criteria = criteria.and("title").in(title);
			criteria = criteria.and("organizationName").in(company);
		}
		Query query = new Query();
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, GlobalContact.class);
	}

}
