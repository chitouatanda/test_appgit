package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.ProspectFeedback;

public interface ProspectFeedbackRepositoryCustom {

	public void deleteAllBySourceAndCreatedDate(String source, Date createdDate);

	public void bulkInsert(List<ProspectFeedback> prospectFeedbackList);

}
