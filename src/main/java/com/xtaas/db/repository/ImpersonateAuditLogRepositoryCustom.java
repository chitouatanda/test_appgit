package com.xtaas.db.repository;

import java.util.Optional;

import com.xtaas.db.entity.ImpersonateAuditLog;

public interface ImpersonateAuditLogRepositoryCustom {

	public Optional<ImpersonateAuditLog> findLatest(String supportAdminId, String impersonatedUserId);

}
