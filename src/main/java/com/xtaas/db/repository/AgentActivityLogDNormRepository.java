package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.AgentActivityLogDNorm;

public interface AgentActivityLogDNormRepository extends MongoRepository<AgentActivityLogDNorm, String> {

	@Query("{'agentId' : ?0, 'activityType' : 'AGENT_STATUS_CHANGE'}")
	public List<AgentActivityLogDNorm> getLastStatus(String agentId, Pageable pageable);

}
