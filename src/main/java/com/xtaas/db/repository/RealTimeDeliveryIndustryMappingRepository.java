package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.RealTimeDeliveryIndustryMapping;

public interface RealTimeDeliveryIndustryMappingRepository extends MongoRepository<RealTimeDeliveryIndustryMapping, String> {

	@Query("{ 'xtaasIndustry' : ?0 }")
	public RealTimeDeliveryIndustryMapping findByXtaasIndustry(String xtaasIndustry);

}
