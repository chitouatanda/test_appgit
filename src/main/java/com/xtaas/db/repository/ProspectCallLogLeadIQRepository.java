package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ProspectCallLogLeadIQ;


public interface ProspectCallLogLeadIQRepository extends MongoRepository<ProspectCallLogLeadIQ, String> {

	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public ProspectCallLogLeadIQ findByProspectCallId(String prospectCallId);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 }")
	public List<ProspectCallLogLeadIQ> findByProspectCallIds(List<String> prospectCallId);
	
	@Query(value = "{'prospectCall.campaignId' : ?0 }" , count = true)
	public int countByCampaignId(String campaignId);
	
	@Query("{'prospectCall.campaignId' : ?0 }")
	public List<ProspectCallLogLeadIQ> getByCampaignId(String campaignId);
	
	

}



