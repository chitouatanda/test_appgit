package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.SubSetProspectCallLog;

public interface SubProspectCallLogRepository extends MongoRepository<SubSetProspectCallLog, String> {

	@Query("{ 'createdDate' : {$exists : true} }")
	public List<SubSetProspectCallLog> findSubProspects(Pageable pageable);
	
}
