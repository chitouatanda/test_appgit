package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.domain.valueobject.CampaignCriteria;

public interface SuccessCallLogRepositoryCustom {
	
	public List<SuccessCallLog> findByCompanies(CampaignCriteria cc,long size,int start);
	
	public List<SuccessCallLog> findByCriteria(CampaignCriteria cc);
	
	public Long findBySuccessCriteriaCount(CampaignCriteria cc);

}
