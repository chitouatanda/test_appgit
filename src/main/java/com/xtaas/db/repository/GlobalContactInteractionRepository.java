package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.GlobalContactInteraction;

public interface GlobalContactInteractionRepository
		extends MongoRepository<GlobalContactInteraction, String>, GlobalContactInteractionRepositoryCustom {

}
