package com.xtaas.db.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import com.mongodb.BasicDBObject;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.service.AgentRegistrationServiceImpl;
import com.xtaas.web.dto.AgentCampaignDTO;

public class AgentActivityLogRepositoryImpl implements AgentActivityLogRepositoryCustom {

	@Autowired
	private AgentRegistrationServiceImpl agentRegistrationServiceImpl;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public int getOnlineAgentsCount(String campaignId, Date fromDate, Date toDate) {

		int count = 0;
		ConcurrentHashMap<String, AgentCall> agentCallMap = agentRegistrationServiceImpl.getAgentCallMap();
		Set<Map.Entry<String, AgentCall>> entrySet = agentCallMap.entrySet();
		Iterator<Entry<String, AgentCall>> iterator = entrySet.iterator();
		while (iterator.hasNext()) {
			Entry<String, AgentCall> element = iterator.next();
			//System.out.println("Key: " + element.getKey() + " ,value: " + element.getValue());
			AgentCall agentCallInfo = (AgentCall) element.getValue();

			if (agentCallInfo != null) {
				AgentCampaignDTO agentCampaignDTO = agentCallInfo.getCurrentCampaign();
				
				if (agentCampaignDTO != null && agentCampaignDTO.getId().equals(campaignId)) {
					switch (agentCallInfo.getCurrentStatus()) {
					case ONLINE:
					case REQUEST_BREAK:
					case REQUEST_MEETING:
					case REQUEST_OFFLINE:
					case REQUEST_OTHER:
					case REQUEST_TRAINING:
                     count++;
                     default:
                    	 break;
						
					
					}
					
				}
			}

			//System.out.println(count);
		}

		//System.out.println("COUNT:" + count);

		return count;

	}

	@Override
	public double calculateOnlineTime(List<String> campaignIds, Date startDate, Date endDate) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(buildBasicMatch(campaignIds, startDate, endDate));
		aggregationOperations.add(buildGroup());
		AggregationResults<Object> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(aggregationOperations), "agentactivitylog", Object.class);
		List<Object> handleOnlineTimeList = results.getMappedResults();
		double count = 0;
		if (!handleOnlineTimeList.isEmpty()) {
			LinkedHashMap<?, ?> handleTimeMap = (LinkedHashMap<?, ?>) handleOnlineTimeList.get(0);
			for (Entry<?, ?> object : handleTimeMap.entrySet()) {
				 if (object.getKey().toString().equalsIgnoreCase("count")) {
					 count = Double.parseDouble(object.getValue().toString());	
				}
			}
		}
		return count;

	}

	private AggregationOperation buildGroup() {
		BasicDBObject group = new BasicDBObject("_id", null);
		group = group.append("count", new BasicDBObject("$sum", "$stateDuration"));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	private AggregationOperation buildBasicMatch(List<String> campaignIds, Date startDate, Date endDate) {
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignIds);
		criteria = criteria.and("status").in("ONLINE");
		criteria = criteria.and("prospectCall.updatedDate").gte(startDate).lte(endDate);
//		criteria = criteria.and(Criteria.where("prospectCall.updatedDate").lte(endDate));
		return Aggregation.match(criteria);
	}
}
