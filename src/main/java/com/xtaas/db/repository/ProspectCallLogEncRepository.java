package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLogEnc;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author djain
 *
 */
public interface ProspectCallLogEncRepository extends MongoRepository<ProspectCallLogEnc, String> {

	@Query("{ 'prospectCall.campaignId' : ?0 }")
	public List<ProspectCallLogEnc> findByCampaignIdPageable(String campaignId, Pageable pageable);

}
