package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.SuccessCallLog;

public interface SuccessCallLogRepository
		extends MongoRepository<SuccessCallLog, String>, SuccessCallLogRepositoryCustom {

	@Query("{ '_id' : ?0 }")
	public SuccessCallLog findOneById(String id);
	
	@Query("{ 'prospectCall.prospectCallId' : ?0 ,'prospectCall.partnerId' : ?1}")
	public SuccessCallLog findByProspectCallId(String id,String partnerId);

}
