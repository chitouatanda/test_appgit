package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.db.entity.CallClassificationJobQueue;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface CallClassificationJobQueueRepository extends MongoRepository<CallClassificationJobQueue, String> {

    public List<CallClassificationJobQueue> findByStatus(CallClassificationJobQueue.Status status, Pageable pageable);

    public List<CallClassificationJobQueue> findByPhone(String phone);

    public List<CallClassificationJobQueue> findByProspectCallId(String prospectCallId);

    @Query("{ 'status' : ?0, 'producer' :  {'$in': ['TranscriptionServiceImpl', 'TRANSCRIPTAPI']}} ")
    public List<CallClassificationJobQueue> findByStatusAndProducer(CallClassificationJobQueue.Status status, Pageable pageable);
}
