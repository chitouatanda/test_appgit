package com.xtaas.db.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.Partner;

public interface PartnerRepository extends MongoRepository<Partner, String> {
	@Query("{ '_id' : ?0 }")
	public Partner findOneById(String id);
}
