package com.xtaas.db.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;

import com.mongodb.BasicDBObject;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;

public class CampaignRepositoryImpl implements CampaignRepositoryCustom {
	private final String COLUMN_CAMPAIGN_MANAGER_ID = "campaignManagerId";
	private final String COLUMN_ORGANIZATION_ID = "organizationId";
	private final String COLUMN_CAMPAIGN_STATUS = "status";
	private final String COLUMN_NAME = "name";

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Campaign> searchCampaigns(String organizationId,String campaignManagerId,
			String campaignName, CampaignStatus status, Pageable pageRequest) {

		return mongoTemplate.find(
				Query.query(
						campaignCriteria(organizationId,campaignManagerId, campaignName,
								status)).with(pageRequest), Campaign.class);
	}

	@Override
	public int searchCampaignCount(String organizationId,String campaignManagerId,
			String campaignName, CampaignStatus status) {
		return mongoTemplate.find(
				Query.query(campaignCriteria(organizationId,campaignManagerId, campaignName,
						status)), Campaign.class).size();
	}

	private Criteria campaignCriteria(String  organizationId,String campaignManagerId,
			String campaignName, CampaignStatus status) {
		/*Criteria criteria = Criteria.where(COLUMN_CAMPAIGN_MANAGER_ID).is(
				campaignManagerId);*/
		Criteria criteria = Criteria.where(COLUMN_ORGANIZATION_ID).is(
				organizationId);
		if (campaignName != null) {
			criteria = criteria.and(COLUMN_NAME).regex("^"+campaignName,"i");
		}
		if (status != null) {
			criteria = criteria.and(COLUMN_CAMPAIGN_STATUS).is(status);
		}
		return criteria;
	}

	@Override
	public List<Object> searchByPreviousWorkedCampaign(String campaignManagerId, String campaignId, int count) throws ParseException {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		aggregationOperations.add(Aggregation.match(buildPreviousWorkedCampaign(campaignManagerId, campaignId)));
		aggregationOperations.add(buildPreviousWorkedCampaignGroup());
		aggregationOperations.add(Aggregation.sort(Direction.DESC, "createdDate"));
		aggregationOperations.add(Aggregation.limit(count));
		
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"campaign", Object.class);
		return results.getMappedResults();
	}
	
	private Criteria buildPreviousWorkedCampaign(String campaignManagerId, String campaignId) throws ParseException {
		Criteria criteria = Criteria.where("campaignManagerId").is(campaignManagerId);
		criteria = criteria.and("team.teamId").exists(true);
		criteria = criteria.and("_id").nin(campaignId);
		return criteria;
	}
	
	private AggregationOperation buildPreviousWorkedCampaignGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$team.teamId");
		group.append("name", new BasicDBObject("$last","$name"));
		group.append("campaignId", new BasicDBObject("$last","$_id"));
		group.append("createdDate", new BasicDBObject("$last","$createdDate"));
		return new CustomAggregationOperation(new Document("$group", group));
	}

	@Override
	public List<CampaignDTO> findActiveWithProjectedFields(List<String> fields) {
		Query query = new Query(Criteria.where("status").is("RUNNING"));
		query.fields().include("_id").include("name");
		if (!CollectionUtils.isEmpty(fields)) {
			for (String field : fields) {
				query.fields().include(field);
			}
		}
		List<CampaignDTO> list = mongoTemplate.find(query, CampaignDTO.class, "campaign");
		return list;
	}

	@Override
	public List<CampaignDTO> findByIdsWithProjectedFields(List<String> ids, List<String> fields) {
		Query query = new Query(Criteria.where("_id").in(ids));
		query.fields().include("_id").include("name");
		if (!CollectionUtils.isEmpty(fields)) {
			for (String field : fields) {
				query.fields().include(field);
			}
		}
		List<CampaignDTO> list = mongoTemplate.find(query, CampaignDTO.class, "campaign");
		return list;
	}

}
