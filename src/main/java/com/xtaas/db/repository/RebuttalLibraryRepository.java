package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.db.entity.RebuttalLibrary;

public interface RebuttalLibraryRepository extends MongoRepository<RebuttalLibrary, String> {
	
}
