package com.xtaas.db.repository;

import com.xtaas.db.entity.Skill;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/23/14
 * Time: 11:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SkillRepository extends MongoRepository<Skill, String> {
}
