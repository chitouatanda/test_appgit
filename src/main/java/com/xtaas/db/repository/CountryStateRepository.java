package com.xtaas.db.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.xtaas.db.entity.CountryState;

public interface CountryStateRepository extends MongoRepository<CountryState, String> {

	@Query("{ 'country' : ?0,'countryRecord' : true }")
	public CountryState findByCountry(String country);

	@Query("{ 'countryRecord' : true }")
	public List<CountryState> findAllCountries();
	
	@Query("{ 'country' : ?0 , 'stateRecord' : true}")
	public List<CountryState> findStatesByCountry(String country);

	@Query("{ 'country' : ?0 ,'stateName' : ?1 ,'cityRecord' : true}")
	public List<CountryState> findCityByState(String country, String stateName);

}
