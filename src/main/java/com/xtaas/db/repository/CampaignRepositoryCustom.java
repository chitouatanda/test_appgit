package com.xtaas.db.repository;

import java.text.ParseException;
import java.util.List;
import org.springframework.data.domain.Pageable;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.web.dto.CampaignDTO;

public interface CampaignRepositoryCustom {

	public List<Campaign> searchCampaigns(String organizationId, String campaignManagerId, String campaignName,
			CampaignStatus status, Pageable pageRequest);

	public int searchCampaignCount(String organizationId, String campaignManagerId, String campaignName,
			CampaignStatus status);

	public List<Object> searchByPreviousWorkedCampaign(String campaignManagerId, String campaignId, int count)
			throws ParseException;

	/**
	 * find active campaigns with projected fields. "id & name" fields will be
	 * projected as default.
	 * 
	 * @param fields extra fields that you want to fetch
	 * @return
	 */
	public List<CampaignDTO> findActiveWithProjectedFields(List<String> fields);

	/**
	 * find campaigns by ids with projected fields. "id & name" fields will be
	 * projected as default.
	 * 
	 * @param ids    list of campaignIds
	 * @param fields extra fields that you want to fetch
	 * @return
	 */
	public List<CampaignDTO> findByIdsWithProjectedFields(List<String> ids, List<String> fields);

}
