package com.xtaas.db.repository;

import java.util.List;

import com.xtaas.db.entity.AbmListNew;

public interface AbmListNewRepositoryCustom {

	public void bulkinsert(List<AbmListNew> abmListNew);

	public long deleteAllByCampaignIdAndStatus(String campaignId);

	public AbmListNew findOneByCampaignIdCompanyName(String campaignId, String company);

	public AbmListNew findOneByCampaignIdCompanyId(String campaignId, String companyId);

}
