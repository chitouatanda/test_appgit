package com.xtaas.db.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CountryNew;

public interface CountryNewRepository extends MongoRepository<CountryNew, String> {

	@Query("{ 'country' : ?0,'countryRecord' : true }")
	public CountryNew findByCountry(String country);

	@Query("{ 'country' : ?0 }")
	public List<CountryNew> findAllByCountry(String country);

}
