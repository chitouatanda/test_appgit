package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ZoomInfoEverstringMapping;

public interface ZoomInfoEverstringMappingRepository extends MongoRepository<ZoomInfoEverstringMapping, String> {

	@Query("{ 'attrubute' : ?0 }")
	public ZoomInfoEverstringMapping findZoomInfoEverstringMappingByAttrubute(String attrubute);
}
