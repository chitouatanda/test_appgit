package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.ProspectFeedback;

/**
 * Repository to perform DB operations on ProspectFeedback collection
 * 
 * @author pranay
 *
 */
public interface ProspectFeedbackRepository
		extends MongoRepository<ProspectFeedback, String>, ProspectFeedbackRepositoryCustom {

	@Query(value = "{'source' : ?0, 'status' : {$in : ?1}, 'callStartTime' : {$gte: ?2}}", count = true)
	public int countBySourceStauseAndCallStartTime(String source, List<String> statuses, Date callStartTime);

	@Query(value = "{'source' : ?0, 'status' : {$in : ?1}, 'callStartTime' : {$gte: ?2}}")
	public List<ProspectFeedback> findBySourceStauseAndCallStartTime(String source, List<String> statuses,
			Date callStartTime, Pageable pageable);

}
