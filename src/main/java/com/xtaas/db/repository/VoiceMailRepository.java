package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.VoiceMail;

public interface VoiceMailRepository extends MongoRepository<VoiceMail, String> {

	@Query("{ 'provider' : ?0, 'partnerId' : ?1, 'updatedDate' : {$gte : ?2, $lte : ?3} }")
	public List<VoiceMail> findByProviderAndUpdatedDate(String provider, String partnerId, Date startDate,
			Date endDate);

	@Query("{ 'isTranscribed' : ?0 }")
	public List<VoiceMail> findByIsTranscribed(boolean isTranscribed);

	@Query("{ 'partnerId' : ?0, 'isTranscribed' : ?1, 'updatedDate' : {$gte : ?2, $lte : ?3}}")
	public List<VoiceMail> getVoiceMailsOfGivenHours(String partnerId, boolean isTranscribed, Date fromDate,
			Date toDate);

}
