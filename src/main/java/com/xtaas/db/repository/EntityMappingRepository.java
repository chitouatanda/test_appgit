package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;

import com.xtaas.domain.entity.EntityMapping;

public interface EntityMappingRepository  extends RetrieveRepository<EntityMapping, String> {
	@Query("{ 'fromSystem' : ?0, 'toSystem': ?1}")
	public List<EntityMapping> findByFromSystemToSystem(String fromSystem, String toSystem);

}
