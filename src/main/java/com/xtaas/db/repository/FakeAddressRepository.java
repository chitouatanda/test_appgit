package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xtaas.domain.entity.FakeAddress;

public interface FakeAddressRepository extends MongoRepository<FakeAddress, String> {
	
}
