package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.CallStatsJobTracker;


public interface CallStatsJobTrackerRepository extends MongoRepository<CallStatsJobTracker, String>{

	
	@Query("{'type' : ?0, 'status' : ?1}")
	public List<CallStatsJobTracker> findLatestCallstatsTracker(String type,String status,Pageable pageable);

}
