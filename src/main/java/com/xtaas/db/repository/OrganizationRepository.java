package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.Organization;

public interface OrganizationRepository extends MongoRepository<Organization, String> {
	@Query("{ '_id' : ?0 }")
	public Organization findOneById(String id);

	@Query("{ '_id' : { '$in' : ?0 }, 'status' : 'ACTIVE'}")
	public List<Organization> findByIdsAndStatus(List<String> ids);

	@Query(value = "{ 'organizationLevel' : {$in : ?0 }}")
	public List<Organization> findByOrganizationLevel(List<String> orgLevels);
}
