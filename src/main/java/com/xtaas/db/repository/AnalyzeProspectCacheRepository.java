package com.xtaas.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xtaas.db.entity.AnalyzeProspectCache;

public interface AnalyzeProspectCacheRepository extends MongoRepository<AnalyzeProspectCache, String> {

}
