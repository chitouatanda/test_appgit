package com.xtaas.db.repository;

import java.util.Date;
import java.util.List;

public interface ProspectCallInteractionRepositoryCustom {
	
	public int getHandleTime(List<String> campaignIds, Date startDate, Date endDate);
}
