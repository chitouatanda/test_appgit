package com.xtaas.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.xtaas.db.entity.EverStringCompanyInfo;

public interface EverStringCompanyInfoRepository extends MongoRepository<EverStringCompanyInfo, String>,EverStringCompanyInfoRepositoryCustom{

	@Query(value = "{'ES_ECID' : ?0 }", count = true)
	public int countById(int ES_ECID);
	
	@Query(value = "{'ES_ECID' : {'$in' : ?0 } }")
	public List<EverStringCompanyInfo> findByIdIn(List<Integer> ES_ECID);
}
