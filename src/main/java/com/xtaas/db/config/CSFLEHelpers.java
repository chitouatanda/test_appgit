package com.xtaas.db.config;

import com.mongodb.ClientEncryptionSettings;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.vault.DataKeyOptions;
import com.mongodb.client.vault.ClientEncryption;
import com.mongodb.client.vault.ClientEncryptions;
import org.bson.BsonBinary;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.*;
import java.util.Map.Entry;

/*
 * Helper methods and sample data for this companion project.
 */
public class CSFLEHelpers {

	// JSON Schema helpers
	static Document buildEncryptedField(String bsonType, Boolean isDeterministic) {
		String DETERMINISTIC_ENCRYPTION_TYPE = "AEAD_AES_256_CBC_HMAC_SHA_512-Deterministic";
		String RANDOM_ENCRYPTION_TYPE = "AEAD_AES_256_CBC_HMAC_SHA_512-Random";

		return new Document().append("encrypt", new Document().append("bsonType", bsonType).append("algorithm",
				(isDeterministic) ? DETERMINISTIC_ENCRYPTION_TYPE : RANDOM_ENCRYPTION_TYPE));
	}

	static Document createEncryptMetadataSchema(String keyId) {
		List<Document> keyIds = new ArrayList<>();
		keyIds.add(new Document().append("$binary", new Document().append("base64", keyId).append("subType", "04")));
		return new Document().append("keyId", keyIds);
	}

	/**
	 * 
	 * @param keyId not null
	 * @param properties not null
	 */
	public static Document createJSONSchema(String keyId, List<String> properties) {
		if (keyId.isEmpty()) {
			throw new IllegalArgumentException("keyId must contain your base64 encryption key id.");
		}
		Node root = insert(properties);
		Document schema = new Document().append("bsonType", "object")
					.append("encryptMetadata", createEncryptMetadataSchema(keyId));

		Deque<DocumentChildrenNodes> stack = new ArrayDeque<>();
		stack.push(new DocumentChildrenNodes(schema, root.children));
		while (!stack.isEmpty()) {
			DocumentChildrenNodes dcn = stack.pop();
			Document tmpDoc = new Document();
			dcn.parentDoc.append("properties", tmpDoc);
			for (Entry<String, Node> child : dcn.children.entrySet()) {
				if (child.getValue().isTerminal()) {
					tmpDoc.append(child.getKey(), buildEncryptedField("string", true));
				} else {	
					Document newParent = new Document().append("bsonType", "object");
					// dcn.parentDoc.append("properties", new Document().append(child.getKey(), newParent));
					tmpDoc.append(child.getKey(), newParent);
					stack.push(new DocumentChildrenNodes(newParent, child.getValue().children));
				}
			}	
		}
		return schema;	
	}

	private static class DocumentChildrenNodes {
		Map<String, Node> children;
		Document parentDoc;

		DocumentChildrenNodes(Document parentDoc, Map<String, Node> children) {
			this.children = children;
			this.parentDoc = parentDoc;
		}
	}
	
	private static class Node {
		Map<String, Node> children;
		boolean terminal = false;

		Node() {
			this.terminal = false;
			this.children =  new LinkedHashMap<>();
		}

		boolean isTerminal() {
			return terminal;
		}
	}

	// properties not null
	private static Node insert(List<String> properties) {
		 Node root = new Node();
		 for (String prop : properties) {
			 Node parent = root;
			 String[] hierarchy = prop.split("\\.");
			for (String el : hierarchy) {
				Node child = parent.children.computeIfAbsent(el, k -> new Node());
				parent = child;
			}
			parent.terminal = true;
		}
		return root;
	}

	// Creates KeyVault which allows you to create a key as well as encrypt and
	// decrypt fields
	private static ClientEncryption createKeyVault(String connectionString, String kmsProvider, byte[] localMasterKey,
			String keyVaultCollection) {
		Map<String, Object> masterKeyMap = new HashMap<>();
		masterKeyMap.put("key", localMasterKey);
		Map<String, Map<String, Object>> kmsProviders = new HashMap<>();
		kmsProviders.put(kmsProvider, masterKeyMap);

		ClientEncryptionSettings clientEncryptionSettings = ClientEncryptionSettings.builder()
				.keyVaultMongoClientSettings(MongoClientSettings.builder()
						.applyConnectionString(new ConnectionString(connectionString)).build())
				.keyVaultNamespace(keyVaultCollection).kmsProviders(kmsProviders).build();

		return ClientEncryptions.create(clientEncryptionSettings);
	}

	// Creates index for keyAltNames in the specified key collection
	public static void createKeyVaultIndex(String connectionString, String keyDb, String keyColl) {
		try (MongoClient mongoClient = MongoClients.create(connectionString)) {
			MongoCollection<Document> collection = mongoClient.getDatabase(keyDb).getCollection(keyColl);

			Bson filterExpr = Filters.exists("keyAltNames", true);
			IndexOptions indexOptions = new IndexOptions().unique(true).partialFilterExpression(filterExpr);

			collection.createIndex(new Document("keyAltNames", 1), indexOptions);
		}
	}

	// Create data encryption key in the specified key collection
	// Call only after checking whether a data encryption key with same keyAltName
	// exists
	public static String createDataEncryptionKey(String connectionString, String kmsProvider, byte[] localMasterKey,
			String keyVaultCollection, String keyAltName) {
		List<String> keyAltNames = new ArrayList<>();
		keyAltNames.add(keyAltName);

		try (ClientEncryption keyVault = createKeyVault(connectionString, kmsProvider, localMasterKey,
				keyVaultCollection)) {
			BsonBinary dataKeyId = keyVault.createDataKey(kmsProvider, new DataKeyOptions().keyAltNames(keyAltNames));

			return Base64.getEncoder().encodeToString(dataKeyId.getData());
		}
	}

	public static String findDataEncryptionKey(String connectionString, String dataEncryptionKeyDB,
			String dataEncryptionKeyCollection, String keyAltName) {
		try (MongoClient mongoClient = MongoClients.create(connectionString)) {
			Document query = new Document("keyAltNames", keyAltName);
			MongoCollection<Document> collection = mongoClient.getDatabase(dataEncryptionKeyDB)
					.getCollection(dataEncryptionKeyCollection);
			BsonDocument doc = collection.withDocumentClass(BsonDocument.class).find(query).first();
			if (doc != null) {
				return Base64.getEncoder().encodeToString(doc.getBinary("_id").getData());
			}
			return null;
		}
	}
}