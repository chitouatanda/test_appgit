package com.xtaas.db.config;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.BsonBinary;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import com.mongodb.AutoEncryptionSettings;
import com.mongodb.ClientEncryptionSettings;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.model.vault.DataKeyOptions;
import com.mongodb.client.vault.ClientEncryption;
import com.mongodb.client.vault.ClientEncryptions;
import com.xtaas.db.entity.EncryptionSchemaConfig;

@Configuration
@EnableMongoRepositories(basePackages = "com.xtaas.db.repository")
@EnableMongoAuditing(auditorAwareRef = "entityAuditor")
public class MongoConfiguration extends AbstractMongoClientConfiguration {

	private final String connectionString;

	private final String dataEncryptionKeyDB;

	private final String dataEncryptionKeyCollection;

	private final String kmsProvider;

	private final String awsEncryptionAccessKey;

	private final String awsEncryptionSecretKey;

	private final String awsEncryptionRegion;

	private final String awsEncryptionArn;

	private final String encryptKeyAltName;

	private final String encryptedMongoConnection;

	public MongoConfiguration(@Value("${database_url}") String connectionString,
			@Value("${data_encryption_key_db}") String dataEncryptionKeyDB,
			@Value("${data_encryption_key_collection}") String dataEncryptionKeyCollection,
			@Value("${kms_provider}") String kmsProvider,
			@Value("${aws_encryption_access_key}") String awsEncryptionAccessKey,
			@Value("${aws_encryption_secret_key}") String awsEncryptionSecretKey,
			@Value("${aws_encryption_region}") String awsEncryptionRegion,
			@Value("${aws_encryption_arn}") String awsEncryptionArn,
			@Value("${encrypt_keyaltname}") String encryptKeyAltName,
			@Value("${encrypted_mongoconnection}") String encryptedMongoConnection) {
		super();
		this.connectionString = connectionString;
		this.dataEncryptionKeyDB = dataEncryptionKeyDB;
		this.dataEncryptionKeyCollection = dataEncryptionKeyCollection;
		this.kmsProvider = kmsProvider;
		this.awsEncryptionAccessKey = awsEncryptionAccessKey;
		this.awsEncryptionSecretKey = awsEncryptionSecretKey;
		this.awsEncryptionRegion = awsEncryptionRegion;
		this.awsEncryptionArn = awsEncryptionArn;
		this.encryptKeyAltName = encryptKeyAltName;
		this.encryptedMongoConnection = encryptedMongoConnection;
	}

	@Override
	public MongoClient mongoClient() {
		if (encryptedMongoConnection.equalsIgnoreCase("true")) {
			MongoTemplate mongoTemplate = new MongoTemplate(MongoClients.create(connectionString), getDatabaseName());
			List<EncryptionSchemaConfig> collectionsFromDB = mongoTemplate.findAll(EncryptionSchemaConfig.class);
			String keyVaultCollection = String.join(".", dataEncryptionKeyDB, dataEncryptionKeyCollection);
			Map<String, Map<String, Object>> kmsProviders = new HashMap<String, Map<String, Object>>();
			Map<String, Object> providerDetails = new HashMap<String, Object>();
			providerDetails.put("accessKeyId", awsEncryptionAccessKey);
			providerDetails.put("secretAccessKey", awsEncryptionSecretKey);
			providerDetails.put("region", awsEncryptionRegion);
			kmsProviders.put(kmsProvider, providerDetails);
			String dataEncryptionKey = CSFLEHelpers.findDataEncryptionKey(connectionString, dataEncryptionKeyDB,
					dataEncryptionKeyCollection, encryptKeyAltName);
			if (dataEncryptionKey == null) {
				dataEncryptionKey = createDataEncryptionKey(keyVaultCollection, kmsProviders, encryptKeyAltName);
			}
			Map<String, BsonDocument> schemaMap = new HashMap<>();
			if (collectionsFromDB != null && collectionsFromDB.size() > 0) {
				for (EncryptionSchemaConfig encryptionSchemaConfig : collectionsFromDB) {
					if (encryptionSchemaConfig.isActive()) {
						Document jsonSchema = CSFLEHelpers.createJSONSchema(dataEncryptionKey,
								encryptionSchemaConfig.getFieldsTobeEncrypted());
						String collection = String.join(".", getDatabaseName(),
								encryptionSchemaConfig.getCollectionName());
						schemaMap.put(collection, BsonDocument.parse(jsonSchema.toJson()));
					}
				}
			}
			Map<String, Object> extraOpts = new HashMap<>();
		        String mongoBinaryPath = "/usr/local/mongocryptd";
			extraOpts.put("mongocryptdSpawnPath", mongoBinaryPath);
			AutoEncryptionSettings autoEncryptionSettings = AutoEncryptionSettings.builder().extraOptions(extraOpts)
					.keyVaultNamespace(keyVaultCollection).kmsProviders(kmsProviders).schemaMap(schemaMap).build();
			MongoClientSettings clientSettings = MongoClientSettings.builder()
					.applyConnectionString(new ConnectionString(connectionString))
					.autoEncryptionSettings(autoEncryptionSettings).build();
			return MongoClients.create(clientSettings);
		} else {
			return MongoClients.create(connectionString);
		}
	}

	private String createDataEncryptionKey(String keyVaultCollection, Map<String, Map<String, Object>> kmsProviders,
			String keyAltName) {
		List<String> keyAltNamesList = new ArrayList<String>();
		keyAltNamesList.add(keyAltName);
		ClientEncryption clientEncryption = ClientEncryptions.create(ClientEncryptionSettings.builder()
				.keyVaultMongoClientSettings(MongoClientSettings.builder()
						.applyConnectionString(new ConnectionString(connectionString)).build())
				.keyVaultNamespace(keyVaultCollection).kmsProviders(kmsProviders).build());
		BsonString masterKeyRegion = new BsonString(awsEncryptionRegion);
		BsonString masterKeyArn = new BsonString(awsEncryptionArn);
		DataKeyOptions dataKeyOptions = new DataKeyOptions().keyAltNames(keyAltNamesList)
				.masterKey(new BsonDocument().append("region", masterKeyRegion).append("key", masterKeyArn));
		BsonBinary dataKeyId = clientEncryption.createDataKey("aws", dataKeyOptions);
		String base64DataKeyId = Base64.getEncoder().encodeToString(dataKeyId.getData());
		return base64DataKeyId;
	}

	@Override
	public String getMappingBasePackage() {
		return "com.xtaas.db";
	}

	@Bean
	MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
		return new MongoTransactionManager(dbFactory);
	}

	@Override
	protected String getDatabaseName() {
		MongoClientURI mongoURI = new MongoClientURI(connectionString);
		return mongoURI.getDatabase();
	}

}