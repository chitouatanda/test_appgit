package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.RealTimePost;

@Document(collection = "realtimedelivery")
public class RealTimeDelivery {

	private String xtaasCampaignId;
	private String campaignCode;
	private String authCodeKey;
	private String authCodeValue;
	private String source;
	private String assetName;
	private String categoryName;
	private String successMailId;
	private String errorMailId;
	private String testMode;
	private String postUrl;
	private RealTimePost realTimePost;
	private String clientName;
	private String categoryId;
	private String iusrc;
	private String ftpServer;
	private int ftpPort;
	private String ftpUserName;
	private String ftpPassword;

	public String getFtpServer() {
		return ftpServer;
	}

	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}

	public int getFtpPort() {
		return ftpPort;
	}

	public void setFtpPort(int ftpPort) {
		this.ftpPort = ftpPort;
	}

	public String getFtpUserName() {
		return ftpUserName;
	}

	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public String getXtaasCampaignId() {
		return xtaasCampaignId;
	}

	public void setXtaasCampaignId(String xtaasCampaignId) {
		this.xtaasCampaignId = xtaasCampaignId;
	}

	public String getCampaignCode() {
		return campaignCode;
	}

	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}

	public String getAuthCodeKey() {
		return authCodeKey;
	}

	public void setAuthCodeKey(String authCodeKey) {
		this.authCodeKey = authCodeKey;
	}
	
	public String getAuthCodeValue() {
		return authCodeValue;
	}

	public void setAuthCodeValue(String authCodeValue) {
		this.authCodeValue = authCodeValue;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSuccessMailId() {
		return successMailId;
	}

	public void setSuccessMailId(String successMailId) {
		this.successMailId = successMailId;
	}

	public String getErrorMailId() {
		return errorMailId;
	}

	public void setErrorMailId(String errorMailId) {
		this.errorMailId = errorMailId;
	}

	public String getTestMode() {
		return testMode;
	}

	public void setTestMode(String testMode) {
		this.testMode = testMode;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public RealTimePost getRealTimePost() {
		return realTimePost;
	}

	public void setRealTimePost(RealTimePost realTimePost) {
		this.realTimePost = realTimePost;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getIusrc() {
		return iusrc;
	}

	public void setIusrc(String iusrc) {
		this.iusrc = iusrc;
	}

	@Override
	public String toString() {
		return "RealTimeDelivery [xtaasCampaignId=" + xtaasCampaignId + ", campaignCode=" + campaignCode + ", authCodeKey="
				+ authCodeKey + ", source=" + source + ", assetName=" + assetName + ", categoryName=" + categoryName
				+ ", successMailId=" + successMailId + ", errorMailId=" + errorMailId + ", testMode=" + testMode
				+ ", postUrl=" + postUrl + ", realTimePost=" + realTimePost + ", clientName=" + clientName
				+ ", categoryId=" + categoryId + ", iusrc=" + iusrc + "]";
	}

}
