package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentstatuslog")
public class AgentStatusLog extends AbstractEntity {

	private String agentId;
	private String activityType;
	private String status;
	private String pcid;
	private String prospectInteractionSessionId;
	private String campaignId;
	private int callStateTransitionDuration;
	private long stateDuration;
	
	protected AgentStatusLog() {
		
	}

	public AgentStatusLog(String agentId, String activityType, String status) {
		setAgentId(agentId);
		setActivityType(activityType);
		setStatus(status);
	}

	public AgentStatusLog(AgentActivityLog log) {
		this.agentId = log.getAgentId();
		this.activityType = log.getActivityType().name();
		this.status = log.getStatus();
		this.pcid = log.getPcid();
		this.prospectInteractionSessionId = log.getProspectInteractionSessionId();
		this.campaignId = log.getCampaignId();
		this.callStateTransitionDuration = log.getCallStateTransitionDuration();
		this.stateDuration = log.getStateDuration();
	}

	public String getAgentId() {
		return agentId;
	}

	public String getActivityType() {
		return activityType;
	}

	public String getStatus() {
		return status;
	}

	private void setActivityType(String activityType) {
		if (activityType == null) {
			throw new IllegalArgumentException("Activity Type is required");
		}
		this.activityType = activityType;
	}

	private void setAgentId(String agentId) {
		if (agentId == null || agentId.isEmpty()) {
			throw new IllegalArgumentException("Agent Id is required");
		}
		this.agentId = agentId;
	}

	private void setStatus(String status) {
		if (status == null || status.isEmpty()) {
			throw new IllegalArgumentException("Agent Activity Status is required");
		}
		this.status = status;
	}

	public String getPcid() {
		return pcid;
	}

	public void setPcid(String pcid) {
		this.pcid = pcid;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public int getCallStateTransitionDuration() {
		return callStateTransitionDuration;
	}

	public void setCallStateTransitionDuration(int callStateTransitionDuration) {
		this.callStateTransitionDuration = callStateTransitionDuration;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public long getStateDuration() {
		return stateDuration;
	}

	public void setStateDuration(long stateDuration) {
		this.stateDuration = stateDuration;
	}

}
