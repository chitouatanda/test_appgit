package com.xtaas.db.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "impersonateauditlog")
public class ImpersonateAuditLog extends AbstractEntity {

	public static enum ImpersonateStatus {
		LOGIN, LOGOUT
	}

	private String supportAdminId;
	private String impersonatedUserId;
	private Date loginTime;
	private Date logoutTime;
	private ImpersonateStatus status;

	protected ImpersonateAuditLog() {
	}

	public ImpersonateAuditLog(String supportAdminId, String impersonatedUserId) {
		this.supportAdminId = supportAdminId;
		this.impersonatedUserId = impersonatedUserId;
		this.loginTime = new Date();
		this.status = ImpersonateStatus.LOGIN;
	}

	public String getSupportAdminId() {
		return supportAdminId;
	}

	public void setSupportAdminId(String supportAdminId) {
		this.supportAdminId = supportAdminId;
	}

	public String getImpersonatedUserId() {
		return impersonatedUserId;
	}

	public void setImpersonatedUserId(String impersonatedUserId) {
		this.impersonatedUserId = impersonatedUserId;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public ImpersonateStatus getStatus() {
		return status;
	}

	public void setStatus(ImpersonateStatus status) {
		this.status = status;
	}

	public ImpersonateAuditLog withLogout() {
		this.logoutTime = new Date();
		this.status = ImpersonateStatus.LOGOUT;
		return this;
	}

	@Override
	public String toString() {
		return "ImpersonateAuditLog [supportAdminId=" + supportAdminId + ", impersonatedUserId=" + impersonatedUserId
				+ ", loginTime=" + loginTime + ", logoutTime=" + logoutTime + ", status=" + status + "]";
	}

}
