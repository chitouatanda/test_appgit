package com.xtaas.db.entity;

import java.util.Date;

import com.xtaas.web.dto.NoteDTO;

public class Note {
	private Date creationDate;
	private String text;
	private String agentId;
	private String agentName;
	private String dispositionStatus;
	private String subStatus;
	private String qaId;
	
	protected Note() {
		// TODO Auto-generated constructor stub
	}

	public Note(Date creationDate, String text, String agentId) {
		this.creationDate = creationDate;
		this.text = text;
		this.agentId = agentId;
	}
	
	public Note(NoteDTO noteDTO) {
		this.creationDate = noteDTO.getCreationDate();
		this.text = noteDTO.getText();
		this.agentId = noteDTO.getAgentId();
		this.qaId = noteDTO.getQaId();
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the dispositionStatus
	 */
	public String getDispositionStatus() {
		return dispositionStatus;
	}

	/**
	 * @param dispositionStatus the dispositionStatus to set
	 */
	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	/**
	 * @return the subStatus
	 */
	public String getSubStatus() {
		return subStatus;
	}

	/**
	 * @param subStatus the subStatus to set
	 */
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getQaId() {
		return qaId;
	}

	public void setQaId(String qaId) {
		this.qaId = qaId;
	}

}
