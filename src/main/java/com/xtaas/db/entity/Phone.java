package com.xtaas.db.entity;

public class Phone {
	
	private String name;
	private String number;
	private boolean isPreferred;
	
	public Phone() {
		
	}
	
	
	public Phone(String name, String number, boolean isPreferred) {
		this.name = name;
		this.number = number;
		this.isPreferred = isPreferred;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public boolean isPreferred() {
		return isPreferred;
	}
	public void setPreferred(boolean isPreferred) {
		this.isPreferred = isPreferred;
	}
	
}
