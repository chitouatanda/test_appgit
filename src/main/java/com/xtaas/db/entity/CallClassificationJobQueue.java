package com.xtaas.db.entity;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.mongodb.core.mapping.Document;

import com.opencsv.bean.CsvBindByName;

@Document(collection = "callclassificationjobqueue")
public class CallClassificationJobQueue extends AbstractEntity {

	public enum Status {
		QUEUED, INPROCESS, COMPLETED, ERROR,TRANSCRIBE_QUEUED
	}

	// Queue attributes
	// private String messageId;

	
	private String messageLifecycleId;
	private int ttl;
	private String producer;
	private String producerServerId;
	private String consumer;
	private String consumerServerId;
	private String namespace;
	private Date jobCreationTime; // first time the job was created
	private String jobTrailId;
	private Date jobStartTime;
	private Date jobEndTime;
	private Status status;
	private String errorMsg;

	// Message Attributes
	// duplicate jobs can be created, add check
	// private String schemaVer;
	private String recordingUrl;
	private String prospectCallLogId;
	private String prospectCallId;
	private String phone;
	private int callDuration; // in sec
	private String fromNumber;
	private String toNumber;
	private String transcript;
	private String fileName;
	private String campaignId;

	public CallClassificationJobQueue decrementTtl() {
		this.ttl--;
		return this;
	}

	public String getMessageLifecycleId() {
		return messageLifecycleId;
	}

	public int getTtl() {
		return ttl;
	}

	public String getProducer() {
		return producer;
	}

	public String getProducerServerId() {
		return producerServerId;
	}

	public String getConsumer() {
		return consumer;
	}

	public String getConsumerServerId() {
		return consumerServerId;
	}

	public String getNamespace() {
		return namespace;
	}

	public Date getJobCreationTime() {
		return jobCreationTime;
	}

	public String getJobTrailId() {
		return jobTrailId;
	}

	public Date getJobStartTime() {
		return jobStartTime;
	}

	public Date getJobEndTime() {
		return jobEndTime;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public String getProspectCallLogId() {
		return prospectCallLogId;
	}

	public String getPhone() {
		return phone;
	}

	public int getCallDuration() {
		return callDuration;
	}

	public Status getStatus() {
		return status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setMessageLifecycleId(String messageLifecycleId) {
		this.messageLifecycleId = messageLifecycleId;
	}

	public void setTtl(int ttl) {
		this.ttl = ttl;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public void setProducerServerId(String producerServerId) {
		this.producerServerId = producerServerId;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	public void setConsumerServerId(String consumerServerId) {
		this.consumerServerId = consumerServerId;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setJobCreationTime(Date jobCreationTime) {
		this.jobCreationTime = jobCreationTime;
	}

	public void setJobTrailId(String jobTrailId) {
		this.jobTrailId = jobTrailId;
	}

	public void setJobStartTime(Date jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public void setJobEndTime(Date jobEndTime) {
		this.jobEndTime = jobEndTime;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public void setProspectCallLogId(String prospectCallLogId) {
		this.prospectCallLogId = prospectCallLogId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getFromNumber() {
		return fromNumber;
	}

	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}

	public String getToNumber() {
		return toNumber;
	}

	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}

	public String getTranscript() {
		return transcript;
	}

	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	// ---------- Fluent Style api---------
	public CallClassificationJobQueue withMessageLifecycleId(String messageLifecycleId) {
		this.messageLifecycleId = messageLifecycleId;
		return this;
	}

	public CallClassificationJobQueue withTtl(int ttl) {
		this.ttl = ttl;
		return this;
	}

	public CallClassificationJobQueue withProducer(String producer) {
		this.producer = producer;
		return this;
	}

	public CallClassificationJobQueue withProducerServerId(String producerServerId) {
		this.producerServerId = producerServerId;
		return this;
	}

	public CallClassificationJobQueue withConsumer(String consumer) {
		this.consumer = consumer;
		return this;
	}

	public CallClassificationJobQueue withConsumerServerId(String consumerServerId) {
		this.consumerServerId = consumerServerId;
		return this;
	}

	public CallClassificationJobQueue withNamespace(String namespace) {
		this.namespace = namespace;
		return this;
	}

	public CallClassificationJobQueue withJobCreationTime(Date jobCreationTime) {
		this.jobCreationTime = jobCreationTime;
		return this;
	}

	public CallClassificationJobQueue withJobTrailId(String jobTrailId) {
		this.jobTrailId = jobTrailId;
		return this;
	}

	public CallClassificationJobQueue withJobStartTime(Date jobStartTime) {
		this.jobStartTime = jobStartTime;
		return this;
	}

	public CallClassificationJobQueue withJobEndTime(Date jobEndTime) {
		this.jobEndTime = jobEndTime;
		return this;
	}

	public CallClassificationJobQueue withRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
		return this;
	}

	public CallClassificationJobQueue withProspectCallLogId(String prospectCallLogId) {
		this.prospectCallLogId = prospectCallLogId;
		return this;
	}
	
	public CallClassificationJobQueue withProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
		return this;
	}

	public CallClassificationJobQueue withPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public CallClassificationJobQueue withCallDuration(int callDuration) {
		this.callDuration = callDuration;
		return this;
	}

	public CallClassificationJobQueue withStatus(Status status) {
		this.status = status;
		return this;
	}

	public CallClassificationJobQueue withErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
		return this;
	}

	public CallClassificationJobQueue withFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
		return this;
	}

	public CallClassificationJobQueue withToNumber(String toNumber) {
		this.toNumber = toNumber;
		return this;
	}

	public CallClassificationJobQueue withTranscript(String transcript) {
		this.transcript = transcript;
		return this;
	}
	
	public CallClassificationJobQueue withFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}
	
	public CallClassificationJobQueue withCampaignId(String campaignId) {
		this.campaignId = campaignId;
		return this;
	}

}