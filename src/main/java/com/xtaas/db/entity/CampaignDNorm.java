package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Asset;

@Document(collection = "campaigndnorm")
public class CampaignDNorm extends AbstractEntity {

	private String organizationId;
	private String campaignManagerId;
	private String status;
	private String name;
	private Date startDate;
	private Date endDate;
	private String type;
	private int deliveryTarget;
	private String domain;
	private int dailyCap;
	private String qualificationClause;
	private String leadFilteringClause;
	private boolean selfManageTeam;
	private String currentAssetId;
	private String dialerMode;
	private boolean enableMachineAnsweredDetection;
	private boolean useSIP;
	private boolean multiProspectCalling;
	private String campaignRequirements;
	private boolean hidePhone;
	// private String partnerSupervisors
	private boolean qaRequired;
	private boolean simpleDisposition;
	private String hostingServer;
	private Float retrySpeed0;
	private Float retrySpeed1;
	private Float retrySpeed2;
	private Float retrySpeed3;
	private Float retrySpeed4;
	private Float retrySpeed5;
	private Float retrySpeed6;
	private Float retrySpeed7;
	private Float retrySpeed8;
	private Float retrySpeed9;
	private Float retrySpeed10;
	private boolean retrySpeedEnabled;
	private int callMaxRetries;
	private int dailyCallMaxRetries;
	private String clientName;
	private boolean localOutboundCalling;
	private int duplicateLeadsDuration;
	private int duplicateCompanyDuration;
	private int leadsPerCompany;
	private String campaignGroupId1;
	private String campaignGroupId2;
	private String campaignGroupId3;
	private String campaignGroupId4;
	private String campaignGroupId5;
	private float costPerLead;
	private boolean isIndirectBuyStarted;
	private boolean isEncrypted;
	private boolean isANDTitleIgnored;
	// private String clientCampaignName;
	private String teamId;
	private String supervisorId;
	private String campaignId;
	private String assetId1;
	private Date campaignCreatedDate;
	private String campaignCreatedBy;
	private Date campaignUpdatedDate;
	private String campaignUpdatedBy;

	public Date getCampaignUpdatedDate() {
		return campaignUpdatedDate;
	}

	public void setCampaignUpdatedDate(Date campaignUpdatedDate) {
		this.campaignUpdatedDate = campaignUpdatedDate;
	}

	public String getCampaignUpdatedBy() {
		return campaignUpdatedBy;
	}

	public void setCampaignUpdatedBy(String campaignUpdatedBy) {
		this.campaignUpdatedBy = campaignUpdatedBy;
	}

	public Date getCampaignCreatedDate() {
		return campaignCreatedDate;
	}

	public void setCampaignCreatedDate(Date campaignCreatedDate) {
		this.campaignCreatedDate = campaignCreatedDate;
	}

	public String getCampaignCreatedBy() {
		return campaignCreatedBy;
	}

	public void setCampaignCreatedBy(String campaignCreatedBy) {
		this.campaignCreatedBy = campaignCreatedBy;
	}

	public String getAssetId1() {
		return assetId1;
	}

	public void setAssetId1(String assetId1) {
		this.assetId1 = assetId1;
	}

	public String getAssetId2() {
		return assetId2;
	}

	public void setAssetId2(String assetId2) {
		this.assetId2 = assetId2;
	}

	public String getAssetId3() {
		return assetId3;
	}

	public void setAssetId3(String assetId3) {
		this.assetId3 = assetId3;
	}

	public String getAssetId4() {
		return assetId4;
	}

	public void setAssetId4(String assetId4) {
		this.assetId4 = assetId4;
	}

	public String getAssetId5() {
		return assetId5;
	}

	public void setAssetId5(String assetId5) {
		this.assetId5 = assetId5;
	}

	public String getAssetName1() {
		return assetName1;
	}

	public void setAssetName1(String assetName1) {
		this.assetName1 = assetName1;
	}

	public String getAssetName2() {
		return assetName2;
	}

	public void setAssetName2(String assetName2) {
		this.assetName2 = assetName2;
	}

	public String getAssetName3() {
		return assetName3;
	}

	public void setAssetName3(String assetName3) {
		this.assetName3 = assetName3;
	}

	public String getAssetName4() {
		return assetName4;
	}

	public void setAssetName4(String assetName4) {
		this.assetName4 = assetName4;
	}

	public String getAssetName5() {
		return assetName5;
	}

	public void setAssetName5(String assetName5) {
		this.assetName5 = assetName5;
	}

	public String getActiveAssetName1() {
		return activeAssetName1;
	}

	public void setActiveAssetName1(String activeAssetName1) {
		this.activeAssetName1 = activeAssetName1;
	}

	public String getActiveAssetName2() {
		return activeAssetName2;
	}

	public void setActiveAssetName2(String activeAssetName2) {
		this.activeAssetName2 = activeAssetName2;
	}

	public String getActiveAssetName3() {
		return activeAssetName3;
	}

	public void setActiveAssetName3(String activeAssetName3) {
		this.activeAssetName3 = activeAssetName3;
	}

	public String getActiveAssetName4() {
		return activeAssetName4;
	}

	public void setActiveAssetName4(String activeAssetName4) {
		this.activeAssetName4 = activeAssetName4;
	}

	public String getActiveAssetName5() {
		return activeAssetName5;
	}

	public void setActiveAssetName5(String activeAssetName5) {
		this.activeAssetName5 = activeAssetName5;
	}

	public String getActiveAssetId1() {
		return activeAssetId1;
	}

	public void setActiveAssetId1(String activeAssetId1) {
		this.activeAssetId1 = activeAssetId1;
	}

	public String getActiveAssetId2() {
		return activeAssetId2;
	}

	public void setActiveAssetId2(String activeAssetId2) {
		this.activeAssetId2 = activeAssetId2;
	}

	public String getActiveAssetId3() {
		return activeAssetId3;
	}

	public void setActiveAssetId3(String activeAssetId3) {
		this.activeAssetId3 = activeAssetId3;
	}

	public String getActiveAssetId4() {
		return activeAssetId4;
	}

	public void setActiveAssetId4(String activeAssetId4) {
		this.activeAssetId4 = activeAssetId4;
	}

	public String getActiveAssetId5() {
		return activeAssetId5;
	}

	public void setActiveAssetId5(String activeAssetId5) {
		this.activeAssetId5 = activeAssetId5;
	}

	private String assetId2;
	private String assetId3;
	private String assetId4;
	private String assetId5;
	private String assetName1;
	private String assetName2;
	private String assetName3;
	private String assetName4;
	private String assetName5;

	private String activeAssetName1;
	private String activeAssetName2;
	private String activeAssetName3;
	private String activeAssetName4;
	private String activeAssetName5;

	private String activeAssetId1;
	private String activeAssetId2;
	private String activeAssetId3;
	private String activeAssetId4;
	private String activeAssetId5;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getDeliveryTarget() {
		return deliveryTarget;
	}

	public void setDeliveryTarget(int deliveryTarget) {
		this.deliveryTarget = deliveryTarget;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getDailyCap() {
		return dailyCap;
	}

	public void setDailyCap(int dailyCap) {
		this.dailyCap = dailyCap;
	}

	public String getQualificationClause() {
		return qualificationClause;
	}

	public void setQualificationClause(String qualificationClause) {
		this.qualificationClause = qualificationClause;
	}

	public String getLeadFilteringClause() {
		return leadFilteringClause;
	}

	public void setLeadFilteringClause(String leadFilteringClause) {
		this.leadFilteringClause = leadFilteringClause;
	}

	public boolean getSelfManageTeam() {
		return selfManageTeam;
	}

	public void setSelfManageTeam(boolean selfManageTeam) {
		this.selfManageTeam = selfManageTeam;
	}

	public String getCurrentAssetId() {
		return currentAssetId;
	}

	public void setCurrentAssetId(String currentAssetId) {
		this.currentAssetId = currentAssetId;
	}

	public String getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(String dialerMode) {
		this.dialerMode = dialerMode;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean isUseSIP() {
		return useSIP;
	}

	public void setUseSIP(boolean useSIP) {
		this.useSIP = useSIP;
	}

	public boolean isMultiProspectCalling() {
		return multiProspectCalling;
	}

	public void setMultiProspectCalling(boolean multiProspectCalling) {
		this.multiProspectCalling = multiProspectCalling;
	}

	public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}

	public boolean isHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}

	public boolean isQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(boolean qaRequired) {
		this.qaRequired = qaRequired;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public Float getRetrySpeed0() {
		return retrySpeed0;
	}

	public void setRetrySpeed0(Float retrySpeed0) {
		this.retrySpeed0 = retrySpeed0;
	}

	public Float getRetrySpeed1() {
		return retrySpeed1;
	}

	public void setRetrySpeed1(Float retrySpeed1) {
		this.retrySpeed1 = retrySpeed1;
	}

	public Float getRetrySpeed2() {
		return retrySpeed2;
	}

	public void setRetrySpeed2(Float retrySpeed2) {
		this.retrySpeed2 = retrySpeed2;
	}

	public Float getRetrySpeed3() {
		return retrySpeed3;
	}

	public void setRetrySpeed3(Float retrySpeed3) {
		this.retrySpeed3 = retrySpeed3;
	}

	public Float getRetrySpeed4() {
		return retrySpeed4;
	}

	public void setRetrySpeed4(Float retrySpeed4) {
		this.retrySpeed4 = retrySpeed4;
	}

	public Float getRetrySpeed5() {
		return retrySpeed5;
	}

	public void setRetrySpeed5(Float retrySpeed5) {
		this.retrySpeed5 = retrySpeed5;
	}

	public Float getRetrySpeed6() {
		return retrySpeed6;
	}

	public void setRetrySpeed6(Float retrySpeed6) {
		this.retrySpeed6 = retrySpeed6;
	}

	public Float getRetrySpeed7() {
		return retrySpeed7;
	}

	public void setRetrySpeed7(Float retrySpeed7) {
		this.retrySpeed7 = retrySpeed7;
	}

	public Float getRetrySpeed8() {
		return retrySpeed8;
	}

	public void setRetrySpeed8(Float retrySpeed8) {
		this.retrySpeed8 = retrySpeed8;
	}

	public Float getRetrySpeed9() {
		return retrySpeed9;
	}

	public void setRetrySpeed9(Float retrySpeed9) {
		this.retrySpeed9 = retrySpeed9;
	}

	public Float getRetrySpeed10() {
		return retrySpeed10;
	}

	public void setRetrySpeed10(Float retrySpeed10) {
		this.retrySpeed10 = retrySpeed10;
	}

	public boolean isRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(boolean retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public int getCallMaxRetries() {
		return callMaxRetries;
	}

	public void setCallMaxRetries(int callMaxRetries) {
		this.callMaxRetries = callMaxRetries;
	}

	public int getDailyCallMaxRetries() {
		return dailyCallMaxRetries;
	}

	public void setDailyCallMaxRetries(int dailyCallMaxRetries) {
		this.dailyCallMaxRetries = dailyCallMaxRetries;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public boolean isLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public int getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}

	public void setDuplicateLeadsDuration(int duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}

	public int getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}

	public void setDuplicateCompanyDuration(int duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}

	public int getLeadsPerCompany() {
		return leadsPerCompany;
	}

	public void setLeadsPerCompany(int leadsPerCompany) {
		this.leadsPerCompany = leadsPerCompany;
	}

	public String getCampaignGroupId1() {
		return campaignGroupId1;
	}

	public void setCampaignGroupId1(String campaignGroupId1) {
		this.campaignGroupId1 = campaignGroupId1;
	}

	public String getCampaignGroupId2() {
		return campaignGroupId2;
	}

	public void setCampaignGroupId2(String campaignGroupId2) {
		this.campaignGroupId2 = campaignGroupId2;
	}

	public String getCampaignGroupId3() {
		return campaignGroupId3;
	}

	public void setCampaignGroupId3(String campaignGroupId3) {
		this.campaignGroupId3 = campaignGroupId3;
	}

	public String getCampaignGroupId4() {
		return campaignGroupId4;
	}

	public void setCampaignGroupId4(String campaignGroupId4) {
		this.campaignGroupId4 = campaignGroupId4;
	}

	public String getCampaignGroupId5() {
		return campaignGroupId5;
	}

	public void setCampaignGroupId5(String campaignGroupId5) {
		this.campaignGroupId5 = campaignGroupId5;
	}

	public float getCostPerLead() {
		return costPerLead;
	}

	public void setCostPerLead(float costPerLead) {
		this.costPerLead = costPerLead;
	}

	public boolean isIndirectBuyStarted() {
		return isIndirectBuyStarted;
	}

	public void setIndirectBuyStarted(boolean isIndirectBuyStarted) {
		this.isIndirectBuyStarted = isIndirectBuyStarted;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public boolean isANDTitleIgnored() {
		return isANDTitleIgnored;
	}

	public void setANDTitleIgnored(boolean isANDTitleIgnored) {
		this.isANDTitleIgnored = isANDTitleIgnored;
	}

	/*
	 * public String getClientCampaignName() { return clientCampaignName; } public
	 * void setClientCampaignName(String clientCampaignName) {
	 * this.clientCampaignName = clientCampaignName; }
	 */
	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public CampaignDNorm toCampaignDNorm(Campaign campaign, CampaignDNorm cdn) {
		// CampaignDNorm cdn = new CampaignDNorm();
		if (cdn == null)
			cdn = new CampaignDNorm();

		cdn.setANDTitleIgnored(campaign.isANDTitleIgnored());
		List<Asset> assets = campaign.getAssets();
		if (assets != null && assets.size() > 0) {
			int i = 1;
			for (Asset aset : assets) {
				if (i == 1) {
					cdn.setAssetId1(aset.getAssetId());
					cdn.setAssetName1(aset.getName());
				}
				if (i == 2) {
					cdn.setAssetId2(aset.getAssetId());
					cdn.setAssetName2(aset.getName());
				}
				if (i == 3) {
					cdn.setAssetId3(aset.getAssetId());
					cdn.setAssetName3(aset.getName());
				}
				if (i == 4) {
					cdn.setAssetId4(aset.getAssetId());
					cdn.setAssetName4(aset.getName());
				}
				if (i == 5) {
					cdn.setAssetId5(aset.getAssetId());
					cdn.setAssetName5(aset.getName());
				}
				i++;
			}

		}

		List<Asset> activeAssets = campaign.getActiveAsset();
		if (activeAssets != null && activeAssets.size() > 0) {
			int i = 1;
			for (Asset activeaset : activeAssets) {
				if (i == 1) {
					cdn.setActiveAssetId1(activeaset.getAssetId());
					cdn.setActiveAssetName1(activeaset.getName());
				}
				if (i == 2) {
					cdn.setActiveAssetId2(activeaset.getAssetId());
					cdn.setActiveAssetName2(activeaset.getName());
				}
				if (i == 3) {
					cdn.setActiveAssetId3(activeaset.getAssetId());
					cdn.setActiveAssetName3(activeaset.getName());
				}
				if (i == 4) {
					cdn.setActiveAssetId4(activeaset.getAssetId());
					cdn.setActiveAssetName4(activeaset.getName());
				}
				if (i == 5) {
					cdn.setActiveAssetId5(activeaset.getAssetId());
					cdn.setActiveAssetName5(activeaset.getName());
				}

				i++;
			}

		}
		if (cdn.getActiveAssetId1() == null || cdn.getActiveAssetId1().isEmpty()) {
			cdn.setActiveAssetId1("NOASSET");
			cdn.setActiveAssetName1("NOASSET");
		}
		if (cdn.getActiveAssetId2() == null || cdn.getActiveAssetId2().isEmpty()) {
			cdn.setActiveAssetId2("NOASSET");
			cdn.setActiveAssetName2("NOASSET");
		}
		if (cdn.getActiveAssetId3() == null || cdn.getActiveAssetId3().isEmpty()) {
			cdn.setActiveAssetId3("NOASSET");
			cdn.setActiveAssetName3("NOASSET");
		}
		if (cdn.getActiveAssetId4() == null || cdn.getActiveAssetId4().isEmpty()) {
			cdn.setActiveAssetId4("NOASSET");
			cdn.setActiveAssetName4("NOASSET");
		}
		if (cdn.getActiveAssetId5() == null || cdn.getActiveAssetId5().isEmpty()) {
			cdn.setActiveAssetId5("NOASSET");
			cdn.setActiveAssetName5("NOASSET");
		}

		if (cdn.getAssetId1() == null || cdn.getAssetId1().isEmpty()) {
			cdn.setAssetId1("NOASSET");
			cdn.setAssetName1("NOASSET");
		}
		if (cdn.getAssetId2() == null || cdn.getAssetId2().isEmpty()) {
			cdn.setAssetId2("NOASSET");
			cdn.setAssetName2("NOASSET");
		}
		if (cdn.getAssetId3() == null || cdn.getAssetId3().isEmpty()) {
			cdn.setAssetId3("NOASSET");
			cdn.setAssetName3("NOASSET");
		}
		if (cdn.getAssetId4() == null || cdn.getAssetId4().isEmpty()) {
			cdn.setAssetId4("NOASSET");
			cdn.setAssetName4("NOASSET");
		}
		if (cdn.getAssetId5() == null || cdn.getAssetId5().isEmpty()) {
			cdn.setAssetId5("NOASSET");
			cdn.setAssetName5("NOASSET");
		}

		cdn.setCallMaxRetries(campaign.getCallMaxRetries());
		List<String> cgids = campaign.getCampaignGroupIds();

		if (cgids != null && cgids.size() > 0) {
			int i = 1;
			for (String gid : cgids) {
				if (i == 1)
					cdn.setCampaignGroupId1(gid);
				if (i == 2)
					cdn.setCampaignGroupId2(gid);
				if (i == 3)
					cdn.setCampaignGroupId3(gid);
				if (i == 4)
					cdn.setCampaignGroupId4(gid);
				if (i == 5)
					cdn.setCampaignGroupId5(gid);
				i++;
			}
		}

		cdn.setCampaignId(campaign.getId());
		cdn.setCampaignManagerId(campaign.getCampaignManagerId());
		cdn.setCampaignRequirements(campaign.getCampaignRequirements());
		// cdn.setClientCampaignName(campaign.getClientCampaignName());
		if (campaign.getClientCampaignName() != null && !campaign.getClientCampaignName().isEmpty())
			cdn.setClientName(campaign.getClientCampaignName());
		else
			cdn.setClientName("NOT_SPECIFIED");

		cdn.setCostPerLead(campaign.getCostPerLead());
		cdn.setCampaignCreatedBy(campaign.getCreatedBy());
		cdn.setCampaignCreatedDate(campaign.getCreatedDate());
		cdn.setCampaignUpdatedBy(campaign.getUpdatedBy());
		cdn.setCampaignUpdatedDate(campaign.getUpdatedDate());
		if (campaign.getCurrentAssetId() != null && !campaign.getCurrentAssetId().isEmpty())
			cdn.setCurrentAssetId(campaign.getCurrentAssetId());

		cdn.setDailyCallMaxRetries(campaign.getDailyCallMaxRetries());
		cdn.setDailyCap(campaign.getDailyCap());
		cdn.setDeliveryTarget(campaign.getDeliveryTarget());
		cdn.setDialerMode(campaign.getDialerMode().toString());
		cdn.setDomain(campaign.getDomain());
		cdn.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration());
		cdn.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration());
		cdn.setEnableMachineAnsweredDetection(campaign.isEnableMachineAnsweredDetection());
		cdn.setEncrypted(campaign.isEncrypted());
		cdn.setEndDate(campaign.getEndDate());
		cdn.setHidePhone(campaign.isHidePhone());
		cdn.setHostingServer(campaign.getHostingServer());
		cdn.setIndirectBuyStarted(campaign.isIndirectBuyStarted());
		cdn.setLeadFilteringClause(campaign.getLeadFilteringClause());
		cdn.setLeadsPerCompany(campaign.getLeadsPerCompany());
		cdn.setLocalOutboundCalling(campaign.isLocalOutboundCalling());

		cdn.setMultiProspectCalling(campaign.isMultiProspectCalling());
		cdn.setName(campaign.getName());
		cdn.setOrganizationId(campaign.getOrganizationId());
		cdn.setQaRequired(campaign.getQaRequired());
		cdn.setQualificationClause(campaign.getQualificationClause());
		Map<String, Float> retryMap = campaign.getRetrySpeed();
		if (retryMap != null && retryMap.size() > 0) {
			int i = 0;
			for (Map.Entry<String, Float> entry : retryMap.entrySet()) {
				if (i == 0)
					cdn.setRetrySpeed0(entry.getValue());
				if (i == 1)
					cdn.setRetrySpeed1(entry.getValue());
				if (i == 2)
					cdn.setRetrySpeed2(entry.getValue());
				if (i == 3)
					cdn.setRetrySpeed3(entry.getValue());
				if (i == 4)
					cdn.setRetrySpeed4(entry.getValue());
				if (i == 5)
					cdn.setRetrySpeed5(entry.getValue());
				if (i == 6)
					cdn.setRetrySpeed6(entry.getValue());
				if (i == 7)
					cdn.setRetrySpeed7(entry.getValue());
				if (i == 8)
					cdn.setRetrySpeed8(entry.getValue());
				if (i == 9)
					cdn.setRetrySpeed9(entry.getValue());
				if (i == 10)
					cdn.setRetrySpeed10(entry.getValue());

				i++;
			}
		}

		if (cdn.getRetrySpeed0() == null)
			cdn.setRetrySpeed0(Float.parseFloat("0"));
		if (cdn.getRetrySpeed1() == null)
			cdn.setRetrySpeed1(Float.parseFloat("0"));
		if (cdn.getRetrySpeed2() == null)
			cdn.setRetrySpeed2(Float.parseFloat("0"));
		if (cdn.getRetrySpeed3() == null)
			cdn.setRetrySpeed3(Float.parseFloat("0"));
		if (cdn.getRetrySpeed3() == null)
			cdn.setRetrySpeed4(Float.parseFloat("0"));
		if (cdn.getRetrySpeed5() == null)
			cdn.setRetrySpeed5(Float.parseFloat("0"));
		if (cdn.getRetrySpeed6() == null)
			cdn.setRetrySpeed6(Float.parseFloat("0"));
		if (cdn.getRetrySpeed7() == null)
			cdn.setRetrySpeed7(Float.parseFloat("0"));
		if (cdn.getRetrySpeed8() == null)
			cdn.setRetrySpeed8(Float.parseFloat("0"));
		if (cdn.getRetrySpeed9() == null)
			cdn.setRetrySpeed9(Float.parseFloat("0"));
		if (cdn.getRetrySpeed10() == null)
			cdn.setRetrySpeed10(Float.parseFloat("0"));
		cdn.setRetrySpeedEnabled(campaign.isRetrySpeedEnabled());
		cdn.setSelfManageTeam(campaign.isSelfManageTeam());
		cdn.setSimpleDisposition(campaign.isSimpleDisposition());
		cdn.setStartDate(campaign.getStartDate());
		if (campaign.getStatus() != null)
			cdn.setStatus(campaign.getStatus().toString());
		if (campaign.getTeam() != null && campaign.getTeam().getSupervisorId() != null)
			cdn.setSupervisorId(campaign.getTeam().getSupervisorId());
		if (campaign.getTeam() != null && campaign.getTeam().getTeamId() != null)
			cdn.setTeamId(campaign.getTeam().getTeamId());
		if (campaign.getType() != null)
			cdn.setType(campaign.getType().toString());
		if (campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
			cdn.setUseSIP(true);
		} else {
			cdn.setUseSIP(false);
		}
		// cdn.setUseSIP(campaign.isUseSIP());
		// cdn.setVersion(campaign.getVersion());

		return cdn;

	}

	@Override
	public String toString() {
		return "CampaignDNorm [organizationId=" + organizationId + ", campaignManagerId=" + campaignManagerId
				+ ", status=" + status + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", type=" + type + ", deliveryTarget=" + deliveryTarget + ", domain=" + domain + ", dailyCap="
				+ dailyCap + ", qualificationClause=" + qualificationClause + ", leadFilteringClause="
				+ leadFilteringClause + ", selfManageTeam=" + selfManageTeam + ", currentAssetId=" + currentAssetId
				+ ", dialerMode=" + dialerMode + ", enableMachineAnsweredDetection=" + enableMachineAnsweredDetection
				+ ", useSIP=" + useSIP + ", multiProspectCalling=" + multiProspectCalling + ", campaignRequirements="
				+ campaignRequirements + ", hidePhone=" + hidePhone + ", qaRequired=" + qaRequired
				+ ", simpleDisposition=" + simpleDisposition + ", hostingServer=" + hostingServer + ", retrySpeed0="
				+ retrySpeed0 + ", retrySpeed1=" + retrySpeed1 + ", retrySpeed2=" + retrySpeed2 + ", retrySpeed3="
				+ retrySpeed3 + ", retrySpeed4=" + retrySpeed4 + ", retrySpeed5=" + retrySpeed5 + ", retrySpeed6="
				+ retrySpeed6 + ", retrySpeed7=" + retrySpeed7 + ", retrySpeed8=" + retrySpeed8 + ", retrySpeed9="
				+ retrySpeed9 + ", retrySpeed10=" + retrySpeed10 + ", retrySpeedEnabled=" + retrySpeedEnabled
				+ ", callMaxRetries=" + callMaxRetries + ", dailyCallMaxRetries=" + dailyCallMaxRetries
				+ ", clientName=" + clientName + ", localOutboundCalling=" + localOutboundCalling
				+ ", duplicateLeadsDuration=" + duplicateLeadsDuration + ", duplicateCompanyDuration="
				+ duplicateCompanyDuration + ", leadsPerCompany=" + leadsPerCompany + ", campaignGroupId1="
				+ campaignGroupId1 + ", campaignGroupId2=" + campaignGroupId2 + ", campaignGroupId3=" + campaignGroupId3
				+ ", campaignGroupId4=" + campaignGroupId4 + ", campaignGroupId5=" + campaignGroupId5 + ", costPerLead="
				+ costPerLead + ", isIndirectBuyStarted=" + isIndirectBuyStarted + ", isEncrypted=" + isEncrypted
				+ ", isANDTitleIgnored=" + isANDTitleIgnored
				+ /* ", clientCampaignName=" + clientCampaignName + */ ", teamId=" + teamId + ", supervisorId="
				+ supervisorId + ", campaignId=" + campaignId + ", assetId1=" + assetId1 + ", campaignCreatedDate="
				+ campaignCreatedDate + ", campaignCreatedBy=" + campaignCreatedBy + ", campaignUpdatedDate="
				+ campaignUpdatedDate + ", campaignUpdatedBy=" + campaignUpdatedBy + ", assetId2=" + assetId2
				+ ", assetId3=" + assetId3 + ", assetId4=" + assetId4 + ", assetId5=" + assetId5 + ", assetName1="
				+ assetName1 + ", assetName2=" + assetName2 + ", assetName3=" + assetName3 + ", assetName4="
				+ assetName4 + ", assetName5=" + assetName5 + ", activeAssetName1=" + activeAssetName1
				+ ", activeAssetName2=" + activeAssetName2 + ", activeAssetName3=" + activeAssetName3
				+ ", activeAssetName4=" + activeAssetName4 + ", activeAssetName5=" + activeAssetName5
				+ ", activeAssetId1=" + activeAssetId1 + ", activeAssetId2=" + activeAssetId2 + ", activeAssetId3="
				+ activeAssetId3 + ", activeAssetId4=" + activeAssetId4 + ", activeAssetId5=" + activeAssetId5 + "]";
	}

}
