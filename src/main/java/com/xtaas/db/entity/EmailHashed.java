package com.xtaas.db.entity;


import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.AbstractEntity;



@Document(collection="emailhashed")
public class EmailHashed extends AbstractEntity {
	

	private String email;
	private String emailHash;
	private int count;
	private Date callStartTime;
	private int callDuration;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailHash() {
		return emailHash;
	}
	public void setEmailHash(String emailHash) {
		this.emailHash = emailHash;
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getCallStartTime() {
		return callStartTime;
	}
	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}
	public int getCallDuration() {
		return callDuration;
	}
	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	
	
}

