package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.PreferenceId;
@Document(collection="supervisorpreference")
public class SupervisorPreference extends AbstractEntity {
	@JsonProperty
	private String supervisorId;
	
	@JsonProperty
	private PreferenceId preferenceId;
	
	@JsonProperty
	private List<String> values;
		
	public SupervisorPreference() {
		
	}
	
	
	public SupervisorPreference(String supervisorId, PreferenceId preferenceId,
			List<String> values) {
		this.supervisorId = supervisorId;
		this.preferenceId = preferenceId;
		this.values = values;
	}


	public String getSupervisorId() {
		return supervisorId;
	}


	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}


	public PreferenceId getPreferenceId() {
		return preferenceId;
	}


	public void setPreferenceId(PreferenceId preferenceId) {
		this.preferenceId = preferenceId;
	}


	public List<String> getValues() {
		return values;
	}


	public void setValues(List<String> values) {
		this.values = values;
	}

	
}
