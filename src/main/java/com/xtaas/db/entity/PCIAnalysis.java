/**
 * 
 */
package com.xtaas.db.entity;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.utils.XtaasDateUtils;

/**
 * @author djain
 *
 */
@Document(collection = "pcianalysis")
public class PCIAnalysis extends AbstractEntity {
	private String agentId;
	private String agentName;
	private String assetName;
	private int call_attempted;
	private int call_connected;
	private int call_contacted;
	private Date call_date;
	private Date call_time;
	private int callduration;
	private int callretrycount;
	private String campaign_status;
	private String campaignid;
	private String campaignName;
	private String clientName;
	private String company;
	private String country1;
	private String country2;
	private String country3;
	private String country4;
	private String country5;
	private String cq1_certifyLink;
	private String cq1_question;
	private String cq1_response;
	private String cq1_validationresp;
	private String cq2_certifyLink;
	private String cq2_question;
	private String cq2_response;
	private String cq2_validationresp;
	private String cq3_certifyLink;
	private String cq3_question;
	private String cq3_response;
	private String cq3_validationresp;
	private String cq4_certifyLink;
	private String cq4_question;
	private String cq4_response;
	private String cq4_validationresp;
	private String cq5_certifyLink;
	private String cq5_question;
	private String cq5_response;
	private String cq5_validationresp;
	private Date created_date;
	private Date created_datetime;
	private String department;
	private String dispositionstatus;
	private int dnc_added;
	private String firstname;
	// private int handletime;
	private Double employeeCountMax;
	private Double employeeCountMin;
	private String industry;
	private String industry1;
	private String industry2;
	private String industry3;
	private String industry4;
	private int interaction_answer_machine;
	private int interaction_busy;
	private int interaction_call_abandoned;
	private int interaction_dead_air;
	private int interaction_FAILED_QUALIFICATION;
	private int interaction_failure;
	private int interaction_gatekeeper_answer_machine;
	private int interaction_local_dnc_error;
	private int interaction_max_retry_limit_reached;
	private int interaction_national_dnc_error;
	private int interaction_no_answer;
	private int interaction_no_consent;
	private int interaction_not_interested;
	private int auto_machine_answered;
	// private String interaction_pcid;
	private int interaction_success;
	private int interaction_unreachable;
	private String lastname;
	private String lead_validation_notes;
	private String lead_validation_valid;
	private String leadStatus;
	private String management_level;
	private int mgt_level_c_level;
	private int mgt_level_director;
	private int mgt_level_manager;
	private int mgt_level_non_management;
	private int mgt_level_president_principal;
	private int mgt_level_vice_president;
	private String organizationid;
	private String outboundnumber;
	private String p_domain;
	private String partnerid;
	private String phone;
	private String prefix;
	private String prospectcallid;
	private int prospecthandleduration;
	private String qaid;
	private String qaname;
	private String qualitybucket;
	private String recordingurl;
	private Double revenuemax;
	private Double revenuemin;
	private String statecode;
	private String status;
	private String substatus;
	private String suffix;
	private String timezone;
	private String title;
	private String transactiondate_day;
	private int transactiondate_dayofweek;
	private int transactiondate_dayofyear;
	private int transactiondate_hour;
	private int transactiondate_minute;
	private int transactiondate_month;
	private int transactiondate_second;
	private int transactiondate_week;
	private int transactiondate_year;
	private int voiceduration;
	private String twiliocallsid;
	private String zipcode;
	private String datasource;
	private int unknownerror;
	private int directPhone;
	private String email;
	private String dataSlice;
	private String qualityBucket;
	private int qualityBucketSort;
	private String sourceType;
	private String companyPhone;
	private int dailyRetryCount;
	private String rejectionReason;
	private boolean leadDelivered;
	private String clientDelivered;
	private long agentOnlineTime;
	private Date retentationDate;
	private int leadStatusUpdated;
	private Date lastUpdatedDate;
	private String voiceProvider;
	private String callDetectedAs;
	private long amdTimeDiffInSec;
	private long amdTimeDiffInMilliSec;
	private long createdDateQLIK;
	private String campaignManagerId;
	private int costPerLead;
	private int deliveryTarget;
	private int dailyCap;
	private String organizationName;
	private boolean simpleDisposition;
	private String dialerMode;
	private String campaignType;
	private boolean usePlivo;
	private boolean useAMDetection;
	private Date campaignStartDate;
	private Date campaignEndDate;
	private String callControlPRovider;
	private String telnyxCallDetectedAs;
	private boolean useSlice;
	private boolean emailRevealed;
	private Date callbackDate;
	private boolean researchData;
	private boolean prospectingData;
	private boolean autoMachineDetected;
	private String notes;
	private String agentLoginURL;
	private String customFieldQuestion01;
	private String customFieldQuestion02;
	private String customFieldQuestion03;
	private String customFieldQuestion04;
	private String customFieldQuestion05;
	private String customFieldQuestion06;
	private String customFieldQuestion07;
	private String customFieldQuestion08;
	private String customFieldQuestion09;
	private String customFieldQuestion10;
	private String customFieldAnswer01;
	private String customFieldAnswer02;
	private String customFieldAnswer03;
	private String customFieldAnswer04;
	private String customFieldAnswer05;
	private String customFieldAnswer06;
	private String customFieldAnswer07;
	private String customFieldAnswer08;
	private String customFieldAnswer09;
	private String customFieldAnswer10;
	private String sipProvider;

	
	public PCIAnalysis() {
		this.call_attempted = 0;
		this.call_connected = 0;
		this.call_contacted = 0;
		this.call_date = new Date();
		this.call_time = new Date();
		this.callretrycount = 0;
		this.campaign_status = "RUNNING";
		this.campaignid = "5e676d2be4b08ed83fc90692";
		this.campaignName = "MA MIR";
		this.company = "ABCD Company";
		this.country1 = "United States";
		this.created_date = new Date();
		this.created_datetime = new Date();
		this.department = "General Management";
		this.dispositionstatus = "NA";
		this.dnc_added = 0;
		this.firstname = "NA";
		this.employeeCountMax = 0.0;
		this.employeeCountMin = 0.0;
		this.industry = "NA";
		this.industry1 = "NA";
		this.industry2 = "NA";
		this.industry3 = "NA";
		this.industry4 = "NA";
		this.interaction_answer_machine = 0;
		this.interaction_busy = 0;
		this.interaction_call_abandoned = 0;
		this.interaction_dead_air = 0;
		this.interaction_FAILED_QUALIFICATION = 0;
		this.interaction_failure = 0;
		this.interaction_gatekeeper_answer_machine = 0;
		this.interaction_local_dnc_error = 0;
		this.interaction_no_answer = 0;
		this.interaction_no_consent = 0;
		this.interaction_not_interested = 0;
		this.interaction_success = 0;
		this.interaction_unreachable = 0;
		this.lastname = "NA";
		this.management_level = "NA";
		this.mgt_level_c_level = 0;
		this.mgt_level_director = 0;
		this.mgt_level_manager = 0;
		this.mgt_level_non_management = 0;
		this.mgt_level_president_principal = 0;
		this.mgt_level_vice_president = 0;
		this.outboundnumber = "Null";
		this.p_domain = "NA";
		this.phone = "Null";
		this.prospectcallid = "1234567a-8b90-12c3-4d56-78e910fd01g2";
		this.qualitybucket = "NA|NA|NA";
		this.revenuemin = 0.0;
		this.status = "NA";
		this.substatus = "NA";
		this.timezone = "US/Pacific";
		this.title = "NA";
		this.transactiondate_day = "Sun";
		this.transactiondate_dayofweek = 0;
		this.transactiondate_dayofyear = 0;
		this.transactiondate_hour = 0;
		this.transactiondate_minute = 0;
		this.transactiondate_month = 0;
		this.transactiondate_second = 0;
		this.transactiondate_week = 0;
		this.transactiondate_year = 0;
		this.twiliocallsid = "a12345-6789-1011-bcde-fghijklmnop";
		this.datasource = "NA";
		this.unknownerror = 0;
		this.directPhone = 0;
		this.email = "email@email";
		this.lastUpdatedDate = new Date();
//		super.setUpdatedBy("NA");
//		super.setVersion(0);
		this.auto_machine_answered = 0;
		this.qualityBucketSort = 0;
		this.dailyRetryCount = 0;
		this.agentId = "NA";
		this.agentName = "NA";
		this.partnerid = "NA";
		this.recordingurl = "NA";
		this.cq1_question = "NA";
		this.cq1_response = "NA";
		this.leadStatus = "NA";
		this.callduration = 0;
		this.prospecthandleduration = 0;
		this.voiceduration = 0;
		this.revenuemax = 0.0;
		this.assetName = "NA";
		this.qaid = "NA";
		this.qaname = "NA";
		this.lead_validation_notes = "NA";
		this.lead_validation_valid = "NA";
		this.statecode = "NULL";
		this.rejectionReason = "NA";
		this.leadDelivered = false;
		this.clientDelivered = "NA";
		this.organizationid = "XTAAS CALL CENTER";
		this.agentOnlineTime = 0;
		this.leadStatusUpdated = 0;
//		super.setCreatedDate(new Date());
//		super.setCreatedBy("NA");
		this.dataSlice = "NA";
		this.sourceType = "NA";
		this.companyPhone = "Null";
		this.clientName = "NA";
		this.cq2_question = "NA";
		this.cq2_response = "NA";
		this.cq3_question = "NA";
		this.cq3_response = "NA";
		this.cq4_question = "NA";
		this.cq4_response = "NA";
		this.cq5_question = "NA";
		this.cq5_response = "NA";
		this.cq1_validationresp = "NA";
		this.cq1_certifyLink = "NA";
		this.cq2_validationresp = "NA";
		this.cq2_certifyLink = "NA";
		this.cq3_validationresp = "NA";
		this.cq3_certifyLink = "NA";
		this.cq4_validationresp = "NA";
		this.cq4_certifyLink = "NA";
		this.cq5_validationresp = "NA";
		this.cq5_certifyLink = "NA";
		this.prefix = "NA";
		this.suffix = "NA";
		this.lastUpdatedDate = new Date();
		this.voiceProvider = "Telnyx";
		this.telnyxCallDetectedAs = "machine";
		this.amdTimeDiffInSec = 0;
		this.campaignManagerId = "xtaascm";
		this.costPerLead = 0;
		this.deliveryTarget = 1;
		this.dailyCap = 1;
		this.organizationName = "XTAAS CALL CENTER";
		this.simpleDisposition = true;
		this.dialerMode = "HYBRID";
		this.campaignType = "LeadGeneration";
		this.usePlivo = true;
		this.callControlPRovider = "Plivo";
		this.campaignStartDate = XtaasDateUtils.yesterdayDate();
		this.campaignEndDate = new Date();
		this.useAMDetection = false;
		this.useSlice = false;
		this.emailRevealed = false;
		this.callbackDate = new Date();
		this.researchData = false;
		this.prospectingData = false;
		this.autoMachineDetected = false;
		this.notes = "NA";
		this.agentLoginURL = "NA";
		this.customFieldQuestion01 = "NA";
		this.customFieldQuestion02 = "NA";
		this.customFieldQuestion03 = "NA";
		this.customFieldQuestion04 = "NA";
		this.customFieldQuestion05 = "NA";
		this.customFieldQuestion06 = "NA";
		this.customFieldQuestion07 = "NA";
		this.customFieldQuestion08 = "NA";
		this.customFieldQuestion09 = "NA";
		this.customFieldQuestion10 = "NA";
		this.customFieldAnswer01 = "NA";
		this.customFieldAnswer02 = "NA";
		this.customFieldAnswer03 = "NA";
		this.customFieldAnswer04 = "NA";
		this.customFieldAnswer05 = "NA";
		this.customFieldAnswer06 = "NA";
		this.customFieldAnswer07 = "NA";
		this.customFieldAnswer08 = "NA";
		this.customFieldAnswer09 = "NA";
		this.customFieldAnswer10 = "NA";
		this.sipProvider = "Telnyx";


	}

	public PCIAnalysis PCIAnalysisdumb(PCIAnalysis pciAnalysisInput) {
		pciAnalysisInput.agentId = pciAnalysisInput.agentId == null ? "NA" : pciAnalysisInput.getAgentId();
		pciAnalysisInput.agentName = pciAnalysisInput.agentName == null ? "NA" : pciAnalysisInput.getAgentName();
		pciAnalysisInput.assetName = pciAnalysisInput.assetName == null ? "NA" : pciAnalysisInput.getAssetName();
		pciAnalysisInput.call_date = pciAnalysisInput.call_date == null ? new Date() : pciAnalysisInput.getCall_date();
		pciAnalysisInput.call_time = pciAnalysisInput.call_time == null ? new Date() : pciAnalysisInput.getCall_time();
		pciAnalysisInput.campaign_status = pciAnalysisInput.campaign_status == null ? "NA" : pciAnalysisInput.getCampaign_status();
		pciAnalysisInput.campaignid = pciAnalysisInput.campaignid == null ? "NA" : pciAnalysisInput.getCampaignid();
		pciAnalysisInput.campaignName= pciAnalysisInput.campaignName == null ? "NA" : pciAnalysisInput.getCampaignName();
		pciAnalysisInput.clientName= pciAnalysisInput.clientName == null ? "NA" : pciAnalysisInput.getClientName();
		pciAnalysisInput.company= pciAnalysisInput.company == null ? "NA" : pciAnalysisInput.getCompany();
		pciAnalysisInput.country1= pciAnalysisInput.country1 == null ? "NA" : pciAnalysisInput.getCountry1();
		pciAnalysisInput.country2= pciAnalysisInput.country2 == null ? "NA" : pciAnalysisInput.getCountry2();
		pciAnalysisInput.country3= pciAnalysisInput.country3 == null ? "NA" : pciAnalysisInput.getCountry3();
		pciAnalysisInput.country4= pciAnalysisInput.country4 == null ? "NA" : pciAnalysisInput.getCountry4();
		pciAnalysisInput.country5= pciAnalysisInput.country5 == null ? "NA" : pciAnalysisInput.getCountry5();
		pciAnalysisInput.cq1_certifyLink= pciAnalysisInput.cq1_certifyLink == null ? "NA" : pciAnalysisInput.getCq1_certifyLink();
		pciAnalysisInput.cq1_question= pciAnalysisInput.cq1_question == null ? "NA" : pciAnalysisInput.getCq1_question();
		pciAnalysisInput.cq1_response= pciAnalysisInput.cq1_response == null ? "NA" : pciAnalysisInput.getCq1_response();
		pciAnalysisInput.cq1_validationresp= pciAnalysisInput.cq1_validationresp == null ? "NA" : pciAnalysisInput.getCq1_validationresp();
		pciAnalysisInput.cq2_certifyLink= pciAnalysisInput.cq2_certifyLink == null ? "NA" : pciAnalysisInput.getCq2_certifyLink();
		pciAnalysisInput.cq2_question= pciAnalysisInput.cq2_question == null ? "NA" : pciAnalysisInput.getCq2_question();
		pciAnalysisInput.cq2_response= pciAnalysisInput.cq2_response == null ? "NA" : pciAnalysisInput.getCq2_response();
		pciAnalysisInput.cq2_validationresp= pciAnalysisInput.cq2_validationresp == null ? "NA" : pciAnalysisInput.getCq2_validationresp();
		pciAnalysisInput.cq3_certifyLink= pciAnalysisInput.cq3_certifyLink == null ? "NA" : pciAnalysisInput.getCq3_certifyLink();
		pciAnalysisInput.cq3_question= pciAnalysisInput.cq3_question == null ? "NA" : pciAnalysisInput.getCq3_question();
		pciAnalysisInput.cq3_response= pciAnalysisInput.cq3_response == null ? "NA" : pciAnalysisInput.getCq3_response();
		pciAnalysisInput.cq3_validationresp= pciAnalysisInput.cq3_validationresp == null ? "NA" : pciAnalysisInput.getCq3_validationresp();
		pciAnalysisInput.cq4_certifyLink= pciAnalysisInput.cq4_certifyLink == null ? "NA" : pciAnalysisInput.getCq4_certifyLink();
		pciAnalysisInput.cq4_question= pciAnalysisInput.cq4_question == null ? "NA" : pciAnalysisInput.getCq4_question();
		pciAnalysisInput.cq4_response= pciAnalysisInput.cq4_response == null ? "NA" : pciAnalysisInput.getCq4_response();
		pciAnalysisInput.cq4_validationresp= pciAnalysisInput.cq4_validationresp == null ? "NA" : pciAnalysisInput.getCq4_validationresp();
		pciAnalysisInput.cq5_certifyLink= pciAnalysisInput.cq5_certifyLink == null ? "NA" : pciAnalysisInput.getCq5_certifyLink();
		pciAnalysisInput.cq5_question= pciAnalysisInput.cq5_question == null ? "NA" : pciAnalysisInput.getCq5_question();
		pciAnalysisInput.cq5_response= pciAnalysisInput.cq5_response == null ? "NA" : pciAnalysisInput.getCq5_response();
		pciAnalysisInput.cq5_validationresp= pciAnalysisInput.cq5_validationresp == null ? "NA" : pciAnalysisInput.getCq5_validationresp();
		pciAnalysisInput.created_date= pciAnalysisInput.created_date == null ? new Date() : pciAnalysisInput.getCreated_date();
		pciAnalysisInput.created_datetime= pciAnalysisInput.created_datetime == null ? new Date() : pciAnalysisInput.getCreated_datetime();
		pciAnalysisInput.department= pciAnalysisInput.department == null ? "NA" : pciAnalysisInput.getDepartment();
		pciAnalysisInput.dispositionstatus= pciAnalysisInput.dispositionstatus == null ? "NA" : pciAnalysisInput.getDispositionstatus();
		pciAnalysisInput.firstname= pciAnalysisInput.firstname == null ? "NA" : pciAnalysisInput.getFirstname();
		pciAnalysisInput.campaignStartDate= pciAnalysisInput.campaignStartDate == null ? new Date() : pciAnalysisInput.getCampaignStartDate();
		pciAnalysisInput.campaignEndDate= pciAnalysisInput.campaignEndDate == null ? new Date() : pciAnalysisInput.getCampaignEndDate();
		pciAnalysisInput.lastUpdatedDate= pciAnalysisInput.lastUpdatedDate == null ? new Date() : pciAnalysisInput.getLastUpdatedDate();
		pciAnalysisInput.retentationDate= pciAnalysisInput.retentationDate == null ? new Date() : pciAnalysisInput.getRetentationDate();
		pciAnalysisInput.industry = pciAnalysisInput.industry == null ? "NA" : pciAnalysisInput.getIndustry();
		pciAnalysisInput.industry1 = pciAnalysisInput.industry1 == null ? "NA" : pciAnalysisInput.getIndustry1();
		pciAnalysisInput.industry2 = pciAnalysisInput.industry2 == null ? "NA" : pciAnalysisInput.getIndustry2();
		pciAnalysisInput.industry3 = pciAnalysisInput.industry3 == null ? "NA" : pciAnalysisInput.getIndustry3();
		pciAnalysisInput.industry4 = pciAnalysisInput.industry4 == null ? "NA" : pciAnalysisInput.getIndustry4();
		pciAnalysisInput.lastname = pciAnalysisInput.lastname == null ? "NA" : pciAnalysisInput.getLastname();
		pciAnalysisInput.lead_validation_notes = pciAnalysisInput.lead_validation_notes == null ? "NA" : pciAnalysisInput.getLead_validation_notes();
		pciAnalysisInput.lead_validation_valid = pciAnalysisInput.lead_validation_valid == null ? "NA" : pciAnalysisInput.getLead_validation_valid();
		pciAnalysisInput.leadStatus = pciAnalysisInput.leadStatus == null ? "NA" : pciAnalysisInput.getLeadStatus();
		pciAnalysisInput.management_level = pciAnalysisInput.management_level == null ? "NA" : pciAnalysisInput.getManagement_level();
		pciAnalysisInput.recordingurl = pciAnalysisInput.recordingurl == null ? "NA" : pciAnalysisInput.getRecordingurl();
		pciAnalysisInput.qualitybucket = pciAnalysisInput.qualitybucket == null ? "NA" : pciAnalysisInput.getQualitybucket();
		pciAnalysisInput.qaname = pciAnalysisInput.qaname == null ? "NA" : pciAnalysisInput.getQaname();
		pciAnalysisInput.qaid = pciAnalysisInput.qaid == null ? "NA" : pciAnalysisInput.getQaid();
		pciAnalysisInput.prospectcallid = pciAnalysisInput.prospectcallid == null ? "NA" : pciAnalysisInput.getProspectcallid();
		pciAnalysisInput.prefix = pciAnalysisInput.prefix == null ? "NA" : pciAnalysisInput.getPrefix();
		pciAnalysisInput.phone = pciAnalysisInput.phone == null ? "NA" : pciAnalysisInput.getPhone();
		pciAnalysisInput.partnerid = pciAnalysisInput.partnerid == null ? "NA" : pciAnalysisInput.getPartnerid();
		pciAnalysisInput.p_domain = pciAnalysisInput.p_domain == null ? "NA" : pciAnalysisInput.getP_domain();
		pciAnalysisInput.outboundnumber = pciAnalysisInput.outboundnumber == null ? "NA" : pciAnalysisInput.getOutboundnumber();
		pciAnalysisInput.organizationid = pciAnalysisInput.organizationid == null ? "NA" : pciAnalysisInput.getOrganizationid();
		pciAnalysisInput.transactiondate_day = pciAnalysisInput.transactiondate_day == null ? "NA" : pciAnalysisInput.getTransactiondate_day();
		pciAnalysisInput.title = pciAnalysisInput.title == null ? "NA" : pciAnalysisInput.getTitle();
		pciAnalysisInput.timezone = pciAnalysisInput.timezone == null ? "NA" : pciAnalysisInput.getTimezone();
		pciAnalysisInput.suffix = pciAnalysisInput.suffix == null ? "NA" : pciAnalysisInput.getSuffix();
		pciAnalysisInput.substatus = pciAnalysisInput.substatus == null ? "NA" : pciAnalysisInput.getSubstatus();
		pciAnalysisInput.status = pciAnalysisInput.status == null ? "NA" : pciAnalysisInput.getStatus();
		pciAnalysisInput.statecode = pciAnalysisInput.statecode == null ? "NA" : pciAnalysisInput.getStatecode();
		pciAnalysisInput.zipcode = pciAnalysisInput.zipcode == null ? "NA" : pciAnalysisInput.getZipcode();
		pciAnalysisInput.twiliocallsid = pciAnalysisInput.twiliocallsid == null ? "NA" : pciAnalysisInput.getTwiliocallsid();
		pciAnalysisInput.email = pciAnalysisInput.email == null ? "NA" : pciAnalysisInput.getEmail();
		pciAnalysisInput.dataSlice = pciAnalysisInput.dataSlice == null ? "NA" : pciAnalysisInput.getDataSlice();
		pciAnalysisInput.qualityBucket = pciAnalysisInput.qualityBucket == null ? "NA" : pciAnalysisInput.getQualityBucket();
		pciAnalysisInput.datasource = pciAnalysisInput.datasource == null ? "NA" : pciAnalysisInput.getDatasource();
		pciAnalysisInput.clientDelivered = pciAnalysisInput.clientDelivered == null ? "NA" : pciAnalysisInput.getClientDelivered();
		pciAnalysisInput.rejectionReason = pciAnalysisInput.rejectionReason == null ? "NA" : pciAnalysisInput.getRejectionReason();
		pciAnalysisInput.companyPhone = pciAnalysisInput.companyPhone == null ? "NA" : pciAnalysisInput.getCompanyPhone();
		pciAnalysisInput.sourceType = pciAnalysisInput.sourceType == null ? "NA" : pciAnalysisInput.getSourceType();
		pciAnalysisInput.campaignManagerId = pciAnalysisInput.campaignManagerId == null ? "NA" : pciAnalysisInput.getCampaignManagerId();
		pciAnalysisInput.callDetectedAs = pciAnalysisInput.callDetectedAs == null ? "NA" : pciAnalysisInput.getCallDetectedAs();
		pciAnalysisInput.voiceProvider = pciAnalysisInput.voiceProvider == null ? "NA" : pciAnalysisInput.getVoiceProvider();
		pciAnalysisInput.telnyxCallDetectedAs = pciAnalysisInput.telnyxCallDetectedAs == null ? "NA" : pciAnalysisInput.getTelnyxCallDetectedAs();
		pciAnalysisInput.callControlPRovider = pciAnalysisInput.callControlPRovider == null ? "NA" : pciAnalysisInput.getCallControlPRovider();
		pciAnalysisInput.campaignType = pciAnalysisInput.campaignType == null ? "NA" : pciAnalysisInput.getCampaignType();
		pciAnalysisInput.dialerMode = pciAnalysisInput.dialerMode == null ? "NA" : pciAnalysisInput.getDialerMode();
		pciAnalysisInput.organizationName = pciAnalysisInput.organizationName == null ? "NA" : pciAnalysisInput.getOrganizationName();
		pciAnalysisInput.employeeCountMax = pciAnalysisInput.employeeCountMax == null ? 0.0 : pciAnalysisInput.getEmployeeCountMax();
		pciAnalysisInput.employeeCountMin = pciAnalysisInput.employeeCountMin == null ? 0.0 : pciAnalysisInput.getEmployeeCountMin();
		pciAnalysisInput.revenuemax = pciAnalysisInput.revenuemax == null ? 0.0 : pciAnalysisInput.getRevenuemax();
		pciAnalysisInput.revenuemin = pciAnalysisInput.revenuemin == null ? 0.0 : pciAnalysisInput.getRevenuemin();
		pciAnalysisInput.callbackDate = pciAnalysisInput.callbackDate == null ? new Date() : pciAnalysisInput.getCallbackDate();
		pciAnalysisInput.notes = pciAnalysisInput.notes == null ? "NA" : pciAnalysisInput.getNotes();
		pciAnalysisInput.agentLoginURL = pciAnalysisInput.agentLoginURL == null ? "NA" : pciAnalysisInput.getAgentLoginURL();
		pciAnalysisInput.customFieldQuestion01 = pciAnalysisInput.customFieldQuestion01 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion01();
		pciAnalysisInput.customFieldQuestion02 = pciAnalysisInput.customFieldQuestion02 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion02();
		pciAnalysisInput.customFieldQuestion03 = pciAnalysisInput.customFieldQuestion03 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion03();
		pciAnalysisInput.customFieldQuestion04 = pciAnalysisInput.customFieldQuestion04 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion04();
		pciAnalysisInput.customFieldQuestion05 = pciAnalysisInput.customFieldQuestion05 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion05();
		pciAnalysisInput.customFieldQuestion06 = pciAnalysisInput.customFieldQuestion06 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion06();
		pciAnalysisInput.customFieldQuestion07 = pciAnalysisInput.customFieldQuestion07 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion07();
		pciAnalysisInput.customFieldQuestion08 = pciAnalysisInput.customFieldQuestion08 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion08();
		pciAnalysisInput.customFieldQuestion09 = pciAnalysisInput.customFieldQuestion09 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion09();
		pciAnalysisInput.customFieldQuestion10 = pciAnalysisInput.customFieldQuestion10 == null ? "NA" : pciAnalysisInput.getCustomFieldQuestion10();
		pciAnalysisInput.customFieldAnswer01 = pciAnalysisInput.customFieldAnswer01 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer01();
		pciAnalysisInput.customFieldAnswer02 = pciAnalysisInput.customFieldAnswer02 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer02();
		pciAnalysisInput.customFieldAnswer03 = pciAnalysisInput.customFieldAnswer03 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer03();
		pciAnalysisInput.customFieldAnswer04 = pciAnalysisInput.customFieldAnswer04 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer04();
		pciAnalysisInput.customFieldAnswer05 = pciAnalysisInput.customFieldAnswer05 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer05();
		pciAnalysisInput.customFieldAnswer06 = pciAnalysisInput.customFieldAnswer06 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer06();
		pciAnalysisInput.customFieldAnswer07 = pciAnalysisInput.customFieldAnswer07 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer07();
		pciAnalysisInput.customFieldAnswer08 = pciAnalysisInput.customFieldAnswer08 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer08();
		pciAnalysisInput.customFieldAnswer09 = pciAnalysisInput.customFieldAnswer09 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer09();
		pciAnalysisInput.customFieldAnswer10 = pciAnalysisInput.customFieldAnswer10 == null ? "NA" : pciAnalysisInput.getCustomFieldAnswer10();
		pciAnalysisInput.sipProvider = pciAnalysisInput.sipProvider == null ? "NA" : pciAnalysisInput.getSipProvider();

		return pciAnalysisInput;
	}

	public PCIAnalysis(String agentId, String agentName, String assetName, int call_attempted, int call_connected,
			int call_contacted, Date call_date, Date call_time, int callduration, int callretrycount,
			String campaign_status, String campaignid, String campaignName, String clientName, String company,
			String country1, String country2, String country3, String country4, String country5, String cq1_certifyLink,
			String cq1_question, String cq1_response, String cq1_validationresp, String cq2_certifyLink,
			String cq2_question, String cq2_response, String cq2_validationresp, String cq3_certifyLink,
			String cq3_question, String cq3_response, String cq3_validationresp, String cq4_certifyLink,
			String cq4_question, String cq4_response, String cq4_validationresp, String cq5_certifyLink,
			String cq5_question, String cq5_response, String cq5_validationresp, Date created_date,
			Date created_datetime, String department, String dispositionstatus, int dnc_added, String firstname,
			Double employeeCountMax, Double employeeCountMin, String industry, String industry1, String industry2,
			String industry3, String industry4, int interaction_answer_machine, int interaction_busy,
			int interaction_call_abandoned, int interaction_dead_air, int interaction_failed_QUALIFICATION,
			int interaction_failure, int interaction_gatekeeper_answer_machine, int interaction_local_dnc_error,
			int interaction_max_retry_limit_reached, int interaction_national_dnc_error, int interaction_no_answer,
			int interaction_no_consent, int interaction_not_interested, int interaction_success,
			int interaction_unreachable, String lastname, String lead_validation_notes, String lead_validation_valid,
			String leadStatus, String management_level, int mgt_level_c_level, int mgt_level_director,
			int mgt_level_manager, int mgt_level_non_management, int mgt_level_president_principal,
			int mgt_level_vice_president, String organizationid, String outboundnumber, String p_domain,
			String partnerid, String phone, String prefix, String prospectcallid, int prospecthandleduration,
			String qaid, String qaname, String qualitybucket, String recordingurl, Double revenuemax, Double revenuemin,
			String statecode, String status, String substatus, String suffix, String timezone, String title,
			String transactiondate_day, int transactiondate_dayofweek, int transactiondate_dayofyear,
			int transactiondate_hour, int transactiondate_minute, int transactiondate_month, int transactiondate_second,
			int transactiondate_week, int transactiondate_year, int voiceduration, String twiliocallsid, String zipcode,
			String datasource, int unknownerror, int directPhone, String email, String dataSlice, String qualityBucket,
			int qualityBucketSort, String sourceType, int auto_machine_answered, String companyPhone,
			int dailyRetryCount, String rejectionReason, String clientDelivered, boolean leadDelivered,
			Date lastUpdatedDate, String callDetectedAs, long amdTimeDiffInSec, long amdTimeDiffInMilliSec, boolean emailRevealed,Date callbackDate,boolean researchData,boolean prospectingData,boolean autoMachineDetected,
			String notes,String agentLoginURL, String voiceProvider, String sipProvider) {
		super();
		this.agentId = agentId;
		this.agentName = agentName;
		this.assetName = assetName;
		this.call_attempted = call_attempted;
		this.call_connected = call_connected;
		this.call_contacted = call_contacted;
		this.call_date = call_date;
		this.call_time = call_time;
		this.callduration = callduration;
		this.callretrycount = callretrycount;
		this.campaign_status = campaign_status;
		this.campaignid = campaignid;
		this.campaignName = campaignName;
		this.clientName = clientName;
		this.company = company;
		this.country1 = country1;
		this.country2 = country2;
		this.country3 = country3;
		this.country4 = country4;
		this.country5 = country5;
		this.cq1_certifyLink = cq1_certifyLink;
		this.cq1_question = cq1_question;
		this.cq1_response = cq1_response;
		this.cq1_validationresp = cq1_validationresp;
		this.cq2_certifyLink = cq2_certifyLink;
		this.cq2_question = cq2_question;
		this.cq2_response = cq2_response;
		this.cq2_validationresp = cq2_validationresp;
		this.cq3_certifyLink = cq3_certifyLink;
		this.cq3_question = cq3_question;
		this.cq3_response = cq3_response;
		this.cq3_validationresp = cq3_validationresp;
		this.cq4_certifyLink = cq4_certifyLink;
		this.cq4_question = cq4_question;
		this.cq4_response = cq4_response;
		this.cq4_validationresp = cq4_validationresp;
		this.cq5_certifyLink = cq5_certifyLink;
		this.cq5_question = cq5_question;
		this.cq5_response = cq5_response;
		this.cq5_validationresp = cq5_validationresp;
		this.created_date = created_date;
		this.created_datetime = created_datetime;
		this.department = department;
		this.dispositionstatus = dispositionstatus;
		this.dnc_added = dnc_added;
		this.firstname = firstname;
		this.employeeCountMax = employeeCountMax;
		this.employeeCountMin = employeeCountMin;
		this.industry = industry;
		this.industry1 = industry1;
		this.industry2 = industry2;
		this.industry3 = industry3;
		this.industry4 = industry4;
		this.interaction_answer_machine = interaction_answer_machine;
		this.interaction_busy = interaction_busy;
		this.interaction_call_abandoned = interaction_call_abandoned;
		this.interaction_dead_air = interaction_dead_air;
		this.interaction_FAILED_QUALIFICATION = interaction_failed_QUALIFICATION;
		this.interaction_failure = interaction_failure;
		this.interaction_gatekeeper_answer_machine = interaction_gatekeeper_answer_machine;
		this.interaction_local_dnc_error = interaction_local_dnc_error;
		this.interaction_max_retry_limit_reached = interaction_max_retry_limit_reached;
		this.interaction_national_dnc_error = interaction_national_dnc_error;
		this.interaction_no_answer = interaction_no_answer;
		this.interaction_no_consent = interaction_no_consent;
		this.interaction_not_interested = interaction_not_interested;
		this.interaction_success = interaction_success;
		this.interaction_unreachable = interaction_unreachable;
		this.lastname = lastname;
		this.lead_validation_notes = lead_validation_notes;
		this.lead_validation_valid = lead_validation_valid;
		this.leadStatus = leadStatus;
		this.management_level = management_level;
		this.mgt_level_c_level = mgt_level_c_level;
		this.mgt_level_director = mgt_level_director;
		this.mgt_level_manager = mgt_level_manager;
		this.mgt_level_non_management = mgt_level_non_management;
		this.mgt_level_president_principal = mgt_level_president_principal;
		this.mgt_level_vice_president = mgt_level_vice_president;
		this.organizationid = organizationid;
		this.organizationName = organizationid;
		this.outboundnumber = outboundnumber;
		this.p_domain = p_domain;
		this.partnerid = partnerid;
		this.phone = phone;
		this.prefix = prefix;
		this.prospectcallid = prospectcallid;
		this.prospecthandleduration = prospecthandleduration;
		this.qaid = qaid;
		this.qaname = qaname;
		this.qualitybucket = qualitybucket;
		this.recordingurl = recordingurl;
		this.revenuemax = revenuemax;
		this.revenuemin = revenuemin;
		this.statecode = statecode;
		this.status = status;
		this.substatus = substatus;
		this.suffix = suffix;
		this.timezone = timezone;
		this.title = title;
		this.transactiondate_day = transactiondate_day;
		this.transactiondate_dayofweek = transactiondate_dayofweek;
		this.transactiondate_dayofyear = transactiondate_dayofyear;
		this.transactiondate_hour = transactiondate_hour;
		this.transactiondate_minute = transactiondate_minute;
		this.transactiondate_month = transactiondate_month;
		this.transactiondate_second = transactiondate_second;
		this.transactiondate_week = transactiondate_week;
		this.transactiondate_year = transactiondate_year;
		this.voiceduration = voiceduration;
		this.twiliocallsid = twiliocallsid;
		this.zipcode = zipcode;
		this.datasource = datasource;
		this.unknownerror = unknownerror;
		this.directPhone = directPhone;
		this.email = email;
		this.dataSlice = dataSlice;
		this.qualitybucket = qualityBucket;
		this.qualityBucketSort = qualityBucketSort;
		this.sourceType = sourceType;
		this.auto_machine_answered = auto_machine_answered;
		this.companyPhone = companyPhone;
		this.dailyRetryCount = dailyRetryCount;
		this.rejectionReason = rejectionReason;
		this.clientDelivered = clientDelivered;
		this.leadDelivered = leadDelivered;
		this.lastUpdatedDate = lastUpdatedDate;
		this.voiceProvider = voiceProvider;
		this.callDetectedAs = callDetectedAs;
		this.amdTimeDiffInSec = amdTimeDiffInSec;
		this.amdTimeDiffInMilliSec = amdTimeDiffInMilliSec;
		Date qlikDate = new Date();
		this.createdDateQLIK = qlikDate.getTime();
		this.emailRevealed = emailRevealed;
		this.callbackDate =callbackDate;
		this.researchData = researchData;
		this.prospectingData= prospectingData;
		this.autoMachineDetected =autoMachineDetected;
		this.notes= notes;
		this.agentLoginURL = agentLoginURL;
		this.sipProvider = sipProvider;
		// pcia.setCreatedDateQLIK(qlikDate.getTime());
		this.PCIAnalysisdumb(this);
	}

	public String getVoiceProvider() {
		return voiceProvider;
	}

	public void setVoiceProvider(String voiceProvider) {
		this.voiceProvider = voiceProvider;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Date getRetentationDate() {
		return retentationDate;
	}

	public void setRetentationDate(Date retentationDate) {
		this.retentationDate = retentationDate;
	}

	public long getAgentOnlineTime() {
		return agentOnlineTime;
	}

	public void setAgentOnlineTime(long agentOnlineTime) {
		this.agentOnlineTime = agentOnlineTime;
	}

	public boolean isLeadDelivered() {
		return leadDelivered;
	}

	public void setLeadDelivered(boolean leadDelivered) {
		this.leadDelivered = leadDelivered;
	}

	public String getClientDelivered() {
		return clientDelivered;
	}

	public void setClientDelivered(String clientDelivered) {
		this.clientDelivered = clientDelivered;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public int getDailyRetryCount() {
		return dailyRetryCount;
	}

	public void setDailyRetryCount(int dailyRetryCount) {
		this.dailyRetryCount = dailyRetryCount;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public String getQualityBucket() {
		return qualityBucket;
	}

	public void setQualityBucket(String qualityBucket) {
		this.qualityBucket = qualityBucket;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public int getDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(int directPhone) {
		this.directPhone = directPhone;
	}

	public int getUnknownerror() {
		return unknownerror;
	}

	public void setUnknownerror(int unknownerror) {
		this.unknownerror = unknownerror;
	}

	public String getDatasource() {
		return datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public long getAmdTimeDiffInSec() {
		return amdTimeDiffInSec;
	}

	public void setAmdTimeDiffInSec(long amdTimeDiffInSec) {
		this.amdTimeDiffInSec = amdTimeDiffInSec;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public int getCall_attempted() {
		return call_attempted;
	}

	public void setCall_attempted(int call_attempted) {
		this.call_attempted = call_attempted;
	}

	public int getCall_connected() {
		return call_connected;
	}

	public void setCall_connected(int call_connected) {
		this.call_connected = call_connected;
	}

	public int getCall_contacted() {
		return call_contacted;
	}

	public void setCall_contacted(int call_contacted) {
		this.call_contacted = call_contacted;
	}

	public Date getCall_date() {
		return call_date;
	}

	public void setCall_date(Date call_date) {
		this.call_date = call_date;
	}

	public Date getCall_time() {
		return call_time;
	}

	public void setCall_time(Date call_time) {
		this.call_time = call_time;
	}

	public int getCallduration() {
		return callduration;
	}

	public void setCallduration(int callduration) {
		this.callduration = callduration;
	}

	public int getCallretrycount() {
		return callretrycount;
	}

	public void setCallretrycount(int callretrycount) {
		this.callretrycount = callretrycount;
	}

	public String getCampaign_status() {
		return campaign_status;
	}

	public void setCampaign_status(String campaign_status) {
		this.campaign_status = campaign_status;
	}

	public String getCampaignid() {
		return campaignid;
	}

	public void setCampaignid(String campaignid) {
		this.campaignid = campaignid;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCountry1() {
		return country1;
	}

	public void setCountry1(String country1) {
		this.country1 = country1;
	}

	public String getCountry2() {
		return country2;
	}

	public void setCountry2(String country2) {
		this.country2 = country2;
	}

	public String getCountry3() {
		return country3;
	}

	public void setCountry3(String country3) {
		this.country3 = country3;
	}

	public String getCountry4() {
		return country4;
	}

	public void setCountry4(String country4) {
		this.country4 = country4;
	}

	public String getCountry5() {
		return country5;
	}

	public void setCountry5(String country5) {
		this.country5 = country5;
	}

	public String getCq1_certifyLink() {
		return cq1_certifyLink;
	}

	public void setCq1_certifyLink(String cq1_certifyLink) {
		this.cq1_certifyLink = cq1_certifyLink;
	}

	public String getCq1_question() {
		return cq1_question;
	}

	public void setCq1_question(String cq1_question) {
		this.cq1_question = cq1_question;
	}

	public String getCq1_response() {
		return cq1_response;
	}

	public void setCq1_response(String cq1_response) {
		this.cq1_response = cq1_response;
	}

	public String getCq1_validationresp() {
		return cq1_validationresp;
	}

	public void setCq1_validationresp(String cq1_validationresp) {
		this.cq1_validationresp = cq1_validationresp;
	}

	public String getCq2_certifyLink() {
		return cq2_certifyLink;
	}

	public void setCq2_certifyLink(String cq2_certifyLink) {
		this.cq2_certifyLink = cq2_certifyLink;
	}

	public String getCq2_question() {
		return cq2_question;
	}

	public void setCq2_question(String cq2_question) {
		this.cq2_question = cq2_question;
	}

	public String getCq2_response() {
		return cq2_response;
	}

	public void setCq2_response(String cq2_response) {
		this.cq2_response = cq2_response;
	}

	public String getCq2_validationresp() {
		return cq2_validationresp;
	}

	public void setCq2_validationresp(String cq2_validationresp) {
		this.cq2_validationresp = cq2_validationresp;
	}

	public String getCq3_certifyLink() {
		return cq3_certifyLink;
	}

	public void setCq3_certifyLink(String cq3_certifyLink) {
		this.cq3_certifyLink = cq3_certifyLink;
	}

	public String getCq3_question() {
		return cq3_question;
	}

	public void setCq3_question(String cq3_question) {
		this.cq3_question = cq3_question;
	}

	public String getCq3_response() {
		return cq3_response;
	}

	public void setCq3_response(String cq3_response) {
		this.cq3_response = cq3_response;
	}

	public String getCq3_validationresp() {
		return cq3_validationresp;
	}

	public void setCq3_validationresp(String cq3_validationresp) {
		this.cq3_validationresp = cq3_validationresp;
	}

	public String getCq4_certifyLink() {
		return cq4_certifyLink;
	}

	public void setCq4_certifyLink(String cq4_certifyLink) {
		this.cq4_certifyLink = cq4_certifyLink;
	}

	public String getCq4_question() {
		return cq4_question;
	}

	public void setCq4_question(String cq4_question) {
		this.cq4_question = cq4_question;
	}

	public String getCq4_response() {
		return cq4_response;
	}

	public void setCq4_response(String cq4_response) {
		this.cq4_response = cq4_response;
	}

	public String getCq4_validationresp() {
		return cq4_validationresp;
	}

	public void setCq4_validationresp(String cq4_validationresp) {
		this.cq4_validationresp = cq4_validationresp;
	}

	public String getCq5_certifyLink() {
		return cq5_certifyLink;
	}

	public void setCq5_certifyLink(String cq5_certifyLink) {
		this.cq5_certifyLink = cq5_certifyLink;
	}

	public String getCq5_question() {
		return cq5_question;
	}

	public void setCq5_question(String cq5_question) {
		this.cq5_question = cq5_question;
	}

	public String getCq5_response() {
		return cq5_response;
	}

	public void setCq5_response(String cq5_response) {
		this.cq5_response = cq5_response;
	}

	public String getCq5_validationresp() {
		return cq5_validationresp;
	}

	public void setCq5_validationresp(String cq5_validationresp) {
		this.cq5_validationresp = cq5_validationresp;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getCreated_datetime() {
		return created_datetime;
	}

	public void setCreated_datetime(Date created_datetime) {
		this.created_datetime = created_datetime;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDispositionstatus() {
		return dispositionstatus;
	}

	public void setDispositionstatus(String dispositionstatus) {
		this.dispositionstatus = dispositionstatus;
	}

	public int getDnc_added() {
		return dnc_added;
	}

	public void setDnc_added(int dnc_added) {
		this.dnc_added = dnc_added;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/*
	 * public int getHandletime() { return handletime; } public void
	 * setHandletime(int handletime) { this.handletime = handletime; }
	 */
	public Double getEmployeeCountMax() {
		return employeeCountMax;
	}

	public void setEmployeeCountMax(Double employeeCountMax) {
		this.employeeCountMax = employeeCountMax;
	}

	public Double getEmployeeCountMin() {
		return employeeCountMin;
	}

	public void setEmployeeCountMin(Double employeeCountMin) {
		this.employeeCountMin = employeeCountMin;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIndustry1() {
		return industry1;
	}

	public void setIndustry1(String industry1) {
		this.industry1 = industry1;
	}

	public String getIndustry2() {
		return industry2;
	}

	public void setIndustry2(String industry2) {
		this.industry2 = industry2;
	}

	public String getIndustry3() {
		return industry3;
	}

	public void setIndustry3(String industry3) {
		this.industry3 = industry3;
	}

	public String getIndustry4() {
		return industry4;
	}

	public void setIndustry4(String industry4) {
		this.industry4 = industry4;
	}

	public int getInteraction_answer_machine() {
		return interaction_answer_machine;
	}

	public void setInteraction_answer_machine(int interaction_answer_machine) {
		this.interaction_answer_machine = interaction_answer_machine;
	}

	public int getInteraction_busy() {
		return interaction_busy;
	}

	public void setInteraction_busy(int interaction_busy) {
		this.interaction_busy = interaction_busy;
	}

	public int getInteraction_call_abandoned() {
		return interaction_call_abandoned;
	}

	public void setInteraction_call_abandoned(int interaction_call_abandoned) {
		this.interaction_call_abandoned = interaction_call_abandoned;
	}

	public int getInteraction_dead_air() {
		return interaction_dead_air;
	}

	public void setInteraction_dead_air(int interaction_dead_air) {
		this.interaction_dead_air = interaction_dead_air;
	}

	public int getInteraction_FAILED_QUALIFICATION() {
		return interaction_FAILED_QUALIFICATION;
	}

	public void setInteraction_FAILED_QUALIFICATION(int interaction_failed_QUALIFICATION) {
		interaction_FAILED_QUALIFICATION = interaction_failed_QUALIFICATION;
	}

	public int getInteraction_failure() {
		return interaction_failure;
	}

	public void setInteraction_failure(int interaction_failure) {
		this.interaction_failure = interaction_failure;
	}

	public int getInteraction_gatekeeper_answer_machine() {
		return interaction_gatekeeper_answer_machine;
	}

	public void setInteraction_gatekeeper_answer_machine(int interaction_gatekeeper_answer_machine) {
		this.interaction_gatekeeper_answer_machine = interaction_gatekeeper_answer_machine;
	}

	public int getAuto_machine_answered() {
		return auto_machine_answered;
	}

	public void setAuto_machine_answered(int auto_machine_answered) {
		this.auto_machine_answered = auto_machine_answered;
	}

	public int getInteraction_local_dnc_error() {
		return interaction_local_dnc_error;
	}

	public void setInteraction_local_dnc_error(int interaction_local_dnc_error) {
		this.interaction_local_dnc_error = interaction_local_dnc_error;
	}

	public int getInteraction_max_retry_limit_reached() {
		return interaction_max_retry_limit_reached;
	}

	public void setInteraction_max_retry_limit_reached(int interaction_max_retry_limit_reached) {
		this.interaction_max_retry_limit_reached = interaction_max_retry_limit_reached;
	}

	public int getInteraction_national_dnc_error() {
		return interaction_national_dnc_error;
	}

	public void setInteraction_national_dnc_error(int interaction_national_dnc_error) {
		this.interaction_national_dnc_error = interaction_national_dnc_error;
	}

	public int getInteraction_no_answer() {
		return interaction_no_answer;
	}

	public void setInteraction_no_answer(int interaction_no_answer) {
		this.interaction_no_answer = interaction_no_answer;
	}

	public int getInteraction_no_consent() {
		return interaction_no_consent;
	}

	public void setInteraction_no_consent(int interaction_no_consent) {
		this.interaction_no_consent = interaction_no_consent;
	}

	public int getInteraction_not_interested() {
		return interaction_not_interested;
	}

	public void setInteraction_not_interested(int interaction_not_interested) {
		this.interaction_not_interested = interaction_not_interested;
	}

//	public String getInteraction_pcid() {
//		return interaction_pcid;
//	}
//	public void setInteraction_pcid(String interaction_pcid) {
//		this.interaction_pcid = interaction_pcid;
//	}
	public int getInteraction_success() {
		return interaction_success;
	}

	public void setInteraction_success(int interaction_success) {
		this.interaction_success = interaction_success;
	}

	public int getInteraction_unreachable() {
		return interaction_unreachable;
	}

	public void setInteraction_unreachable(int interaction_unreachable) {
		this.interaction_unreachable = interaction_unreachable;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLead_validation_notes() {
		return lead_validation_notes;
	}

	public void setLead_validation_notes(String lead_validation_notes) {
		this.lead_validation_notes = lead_validation_notes;
	}

	public String getLead_validation_valid() {
		return lead_validation_valid;
	}

	public void setLead_validation_valid(String lead_validation_valid) {
		this.lead_validation_valid = lead_validation_valid;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getManagement_level() {
		return management_level;
	}

	public void setManagement_level(String management_level) {
		this.management_level = management_level;
	}

	public int getMgt_level_c_level() {
		return mgt_level_c_level;
	}

	public void setMgt_level_c_level(int mgt_level_c_level) {
		this.mgt_level_c_level = mgt_level_c_level;
	}

	public int getMgt_level_director() {
		return mgt_level_director;
	}

	public void setMgt_level_director(int mgt_level_director) {
		this.mgt_level_director = mgt_level_director;
	}

	public int getMgt_level_manager() {
		return mgt_level_manager;
	}

	public void setMgt_level_manager(int mgt_level_manager) {
		this.mgt_level_manager = mgt_level_manager;
	}

	public int getMgt_level_non_management() {
		return mgt_level_non_management;
	}

	public void setMgt_level_non_management(int mgt_level_non_management) {
		this.mgt_level_non_management = mgt_level_non_management;
	}

	public int getMgt_level_president_principal() {
		return mgt_level_president_principal;
	}

	public void setMgt_level_president_principal(int mgt_level_president_principal) {
		this.mgt_level_president_principal = mgt_level_president_principal;
	}

	public int getMgt_level_vice_president() {
		return mgt_level_vice_president;
	}

	public void setMgt_level_vice_president(int mgt_level_vice_president) {
		this.mgt_level_vice_president = mgt_level_vice_president;
	}

	public String getOrganizationid() {
		return organizationid;
	}

	public void setOrganizationid(String organizationid) {
		this.organizationid = organizationid;
	}

	public String getOutboundnumber() {
		return outboundnumber;
	}

	public void setOutboundnumber(String outboundnumber) {
		this.outboundnumber = outboundnumber;
	}

	public String getP_domain() {
		return p_domain;
	}

	public void setP_domain(String p_domain) {
		this.p_domain = p_domain;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getProspectcallid() {
		return prospectcallid;
	}

	public void setProspectcallid(String prospectcallid) {
		this.prospectcallid = prospectcallid;
	}

	public int getProspecthandleduration() {
		return prospecthandleduration;
	}

	public void setProspecthandleduration(int prospecthandleduration) {
		this.prospecthandleduration = prospecthandleduration;
	}

	public String getQaid() {
		return qaid;
	}

	public void setQaid(String qaid) {
		this.qaid = qaid;
	}

	public String getQaname() {
		return qaname;
	}

	public void setQaname(String qaname) {
		this.qaname = qaname;
	}

	public String getQualitybucket() {
		return qualitybucket;
	}

	public void setQualitybucket(String qualitybucket) {
		this.qualitybucket = qualitybucket;
	}

	public String getRecordingurl() {
		return recordingurl;
	}

	public void setRecordingurl(String recordingurl) {
		this.recordingurl = recordingurl;
	}

	public Double getRevenuemax() {
		return revenuemax;
	}

	public void setRevenuemax(Double revenuemax) {
		this.revenuemax = revenuemax;
	}

	public Double getRevenuemin() {
		return revenuemin;
	}

	public void setRevenuemin(Double revenuemin) {
		this.revenuemin = revenuemin;
	}

	public String getStatecode() {
		return statecode;
	}

	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubstatus() {
		return substatus;
	}

	public void setSubstatus(String substatus) {
		this.substatus = substatus;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTransactiondate_day() {
		return transactiondate_day;
	}

	public void setTransactiondate_day(String transactiondate_day) {
		this.transactiondate_day = transactiondate_day;
	}

	public int getTransactiondate_dayofweek() {
		return transactiondate_dayofweek;
	}

	public void setTransactiondate_dayofweek(int transactiondate_dayofweek) {
		this.transactiondate_dayofweek = transactiondate_dayofweek;
	}

	public int getTransactiondate_dayofyear() {
		return transactiondate_dayofyear;
	}

	public void setTransactiondate_dayofyear(int transactiondate_dayofyear) {
		this.transactiondate_dayofyear = transactiondate_dayofyear;
	}

	public int getTransactiondate_hour() {
		return transactiondate_hour;
	}

	public void setTransactiondate_hour(int transactiondate_hour) {
		this.transactiondate_hour = transactiondate_hour;
	}

	public int getTransactiondate_minute() {
		return transactiondate_minute;
	}

	public void setTransactiondate_minute(int transactiondate_minute) {
		this.transactiondate_minute = transactiondate_minute;
	}

	public int getTransactiondate_month() {
		return transactiondate_month;
	}

	public void setTransactiondate_month(int transactiondate_month) {
		this.transactiondate_month = transactiondate_month;
	}

	public int getTransactiondate_second() {
		return transactiondate_second;
	}

	public void setTransactiondate_second(int transactiondate_second) {
		this.transactiondate_second = transactiondate_second;
	}

	public int getTransactiondate_week() {
		return transactiondate_week;
	}

	public void setTransactiondate_week(int transactiondate_week) {
		this.transactiondate_week = transactiondate_week;
	}

	public int getTransactiondate_year() {
		return transactiondate_year;
	}

	public void setTransactiondate_year(int transactiondate_year) {
		this.transactiondate_year = transactiondate_year;
	}

	public int getVoiceduration() {
		return voiceduration;
	}

	public void setVoiceduration(int voiceduration) {
		this.voiceduration = voiceduration;
	}

	public String getTwiliocallsid() {
		return twiliocallsid;
	}

	public void setTwiliocallsid(String twiliocallsid) {
		this.twiliocallsid = twiliocallsid;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public String getCustomFieldQuestion01() {
		return customFieldQuestion01;
	}

	public void setCustomFieldQuestion01(String customFieldQuestion01) {
		this.customFieldQuestion01 = customFieldQuestion01;
	}

	public String getCustomFieldQuestion02() {
		return customFieldQuestion02;
	}

	public void setCustomFieldQuestion02(String customFieldQuestion02) {
		this.customFieldQuestion02 = customFieldQuestion02;
	}

	public String getCustomFieldQuestion03() {
		return customFieldQuestion03;
	}

	public void setCustomFieldQuestion03(String customFieldQuestion03) {
		this.customFieldQuestion03 = customFieldQuestion03;
	}

	public String getCustomFieldQuestion04() {
		return customFieldQuestion04;
	}

	public void setCustomFieldQuestion04(String customFieldQuestion04) {
		this.customFieldQuestion04 = customFieldQuestion04;
	}

	public String getCustomFieldQuestion05() {
		return customFieldQuestion05;
	}

	public void setCustomFieldQuestion05(String customFieldQuestion05) {
		this.customFieldQuestion05 = customFieldQuestion05;
	}

	public String getCustomFieldQuestion06() {
		return customFieldQuestion06;
	}

	public void setCustomFieldQuestion06(String customFieldQuestion06) {
		this.customFieldQuestion06 = customFieldQuestion06;
	}

	public String getCustomFieldQuestion07() {
		return customFieldQuestion07;
	}

	public void setCustomFieldQuestion07(String customFieldQuestion07) {
		this.customFieldQuestion07 = customFieldQuestion07;
	}

	public String getCustomFieldQuestion08() {
		return customFieldQuestion08;
	}

	public void setCustomFieldQuestion08(String customFieldQuestion08) {
		this.customFieldQuestion08 = customFieldQuestion08;
	}

	public String getCustomFieldQuestion09() {
		return customFieldQuestion09;
	}

	public void setCustomFieldQuestion09(String customFieldQuestion09) {
		this.customFieldQuestion09 = customFieldQuestion09;
	}

	public String getCustomFieldQuestion10() {
		return customFieldQuestion10;
	}

	public void setCustomFieldQuestion10(String customFieldQuestion10) {
		this.customFieldQuestion10 = customFieldQuestion10;
	}

	public String getCustomFieldAnswer01() {
		return customFieldAnswer01;
	}

	public void setCustomFieldAnswer01(String customFieldAnswer01) {
		this.customFieldAnswer01 = customFieldAnswer01;
	}

	public String getCustomFieldAnswer02() {
		return customFieldAnswer02;
	}

	public void setCustomFieldAnswer02(String customFieldAnswer02) {
		this.customFieldAnswer02 = customFieldAnswer02;
	}

	public String getCustomFieldAnswer03() {
		return customFieldAnswer03;
	}

	public void setCustomFieldAnswer03(String customFieldAnswer03) {
		this.customFieldAnswer03 = customFieldAnswer03;
	}

	public String getCustomFieldAnswer04() {
		return customFieldAnswer04;
	}

	public void setCustomFieldAnswer04(String customFieldAnswer04) {
		this.customFieldAnswer04 = customFieldAnswer04;
	}

	public String getCustomFieldAnswer05() {
		return customFieldAnswer05;
	}

	public void setCustomFieldAnswer05(String customFieldAnswer05) {
		this.customFieldAnswer05 = customFieldAnswer05;
	}

	public String getCustomFieldAnswer06() {
		return customFieldAnswer06;
	}

	public void setCustomFieldAnswer06(String customFieldAnswer06) {
		this.customFieldAnswer06 = customFieldAnswer06;
	}

	public String getCustomFieldAnswer07() {
		return customFieldAnswer07;
	}

	public void setCustomFieldAnswer07(String customFieldAnswer07) {
		this.customFieldAnswer07 = customFieldAnswer07;
	}

	public String getCustomFieldAnswer08() {
		return customFieldAnswer08;
	}

	public void setCustomFieldAnswer08(String customFieldAnswer08) {
		this.customFieldAnswer08 = customFieldAnswer08;
	}

	public String getCustomFieldAnswer09() {
		return customFieldAnswer09;
	}

	public void setCustomFieldAnswer09(String customFieldAnswer09) {
		this.customFieldAnswer09 = customFieldAnswer09;
	}

	public String getCustomFieldAnswer10() {
		return customFieldAnswer10;
	}

	public void setCustomFieldAnswer10(String customFieldAnswer10) {
		this.customFieldAnswer10 = customFieldAnswer10;
	}

	public int getLeadStatusUpdated() {
		return leadStatusUpdated;
	}

	public void setLeadStatusUpdated(int leadStatusUpdated) {
		this.leadStatusUpdated = leadStatusUpdated;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public long getCreatedDateQLIK() {
		return createdDateQLIK;
	}

	public void setCreatedDateQLIK(long createdDateQLIK) {
		this.createdDateQLIK = createdDateQLIK;
	}

	public PCIAnalysis toPCIAnalysis(PCIAnalysis pcia, ProspectCallInteraction pci, String agentName, String qaName,
			Campaign campaign) {
		// PCIAnalysis pcia = new PCIAnalysis();
		pcia.setLastUpdatedDate(pci.getProspectCall().getProspect().getLastUpdatedDate());
		pcia.setAgentId(pci.getProspectCall().getAgentId());
		Date qlikDate = new Date();
		pcia.setCreatedDateQLIK(qlikDate.getTime());
		if (pci.getProspectCall().getProspect().getSourceType() == null
				|| pci.getProspectCall().getProspect().getSourceType().isEmpty()) {
			pcia.setSourceType("INTERNAL");
		} else {
			pcia.setSourceType(pci.getProspectCall().getProspect().getSourceType());
		}
		if (pci.getProspectCall().getCallDetectedAs() != null && !pci.getProspectCall().getCallDetectedAs().isEmpty()) {
			pcia.setCallDetectedAs(pci.getProspectCall().getCallDetectedAs());
		}
		pcia.setAmdTimeDiffInSec(pci.getProspectCall().getAmdTimeDiffInSec());
		pcia.setAmdTimeDiffInMilliSec(pci.getProspectCall().getAmdTimeDiffInMilliSec());
		if (pci.getProspectCall().isLeadDelivered()) {
			pcia.setLeadDelivered(true);
		} else {
			pcia.setLeadDelivered(false);
		}
		if (pci.getProspectCall().getClientDelivered() != null
				&& !pci.getProspectCall().getClientDelivered().isEmpty()) {
			pcia.setClientDelivered(pci.getProspectCall().getClientDelivered());
		} else {
			pcia.setClientDelivered("null");
		}

		if (pci.getProspectCall().getQualityBucket() != null && !pci.getProspectCall().getQualityBucket().isEmpty()) {
			pcia.setQualityBucket(pci.getProspectCall().getQualityBucket());
		}
		// if(pci.getProspectCall().getQualityBucketSort()!=null &&
		// !pci.getProspectCall().getQualityBucketSort().isEmpty()) {
		pcia.setQualityBucketSort(pci.getProspectCall().getQualityBucketSort());
		// }
		if (pci.getProspectCall().getDataSlice() != null && !pci.getProspectCall().getDataSlice().isEmpty())
			pcia.setDataSlice(pci.getProspectCall().getDataSlice());

		pcia.setAgentName(agentName);

		if (pci.getProspectCall().getDeliveredAssetId() != null
				&& !pci.getProspectCall().getDeliveredAssetId().isEmpty()) {
			List<com.xtaas.domain.valueobject.Asset> assetList = campaign.getActiveAsset();
			List<com.xtaas.domain.valueobject.Asset> allAssetList = campaign.getAssets();
			if (assetList != null && assetList.size() > 0) {
				for (Asset asset : assetList) {
					if (asset.getAssetId().equalsIgnoreCase(pci.getProspectCall().getDeliveredAssetId())) {
						pcia.setAssetName(asset.getName());
					}
				}
			} else if (allAssetList != null && allAssetList.size() > 0) {
				for (Asset asset : assetList) {
					if (asset.getAssetId().equalsIgnoreCase(pci.getProspectCall().getDeliveredAssetId())) {
						pcia.setAssetName(asset.getName());
					}
				}
			}

		}

		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE)) {
			if (pci.getProspectCall().getCallDuration() > 0)
				pcia.setCallduration(pci.getProspectCall().getCallDuration());
			if (pci.getProspectCall().getProspectHandleDuration() != null)
				pcia.setProspecthandleduration(pci.getProspectCall().getProspectHandleDuration());
		}

		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_DIALED))
			pcia.setCall_attempted(1);
		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE)
				&& (pci.getProspectCall().getDispositionStatus() != null
						&& (pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")
								|| pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("FAILURE")
								// ||
								// pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("DIALERCODE")
								|| pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("CALLBACK")
								|| pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("AUTO")))
		/*
		 * && (pci.getProspectCall().getSubStatus()!=null &&
		 * (pci.getProspectCall().getSubStatus().equalsIgnoreCase("ANSWERMACHINE") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNRM") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("FAILED_QUALIFICATION")
		 * || pci.getProspectCall().getSubStatus().equalsIgnoreCase("NO_CONSENT") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("NOTINTERESTED") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("OTHER") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("OUT_OF_COUNTRY") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("PROSPECT_UNREACHABLE")
		 * || pci.getProspectCall().getSubStatus().equalsIgnoreCase(
		 * "CALLBACK_USER_GENERATED") ||
		 * pci.getProspectCall().getSubStatus().equalsIgnoreCase("CALLBACK_GATEKEEPER")
		 * || pci.getProspectCall().getSubStatus().equalsIgnoreCase(
		 * "GATEKEEPER_ANSWERMACHINE") ))
		 */)
			pcia.setCall_contacted(1);
		/*
		 * if(pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE)
		 * &&
		 * (pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("CALLBACK")))
		 * pcia.setCall_contacted(1);
		 */
		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE) &&

				(pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL")
								|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("FAILED_QUALIFICATION")
								|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("NOTINTERESTED")
								|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("NO_CONSENT")
								|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("SUBMITLEAD")
								|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("OTHER")
						// || pci.getProspectCall().getSubStatus().equalsIgnoreCase("OUT_OF_COUNTRY")

						))/* && pci.getProspectCall().getCallDuration() >=20 */ )
			pcia.setCall_connected(1);

		if (pci.getProspectCall().getCallRetryCount() > 0)
			pcia.setCallretrycount(pci.getProspectCall().getCallRetryCount());
		pcia.setCampaign_status(CampaignStatus.RUNNING.toString());
		pcia.setCampaignid(pci.getProspectCall().getCampaignId());
		pcia.setCampaignName(campaign.getName());
		pcia.setClientName(campaign.getClientName());
		if(campaign.getTeam()!=null && campaign.getTeam().getSupervisorId()!=null) {
			pcia.setCampaignManagerId(campaign.getTeam().getSupervisorId());
		}
		pcia.setCostPerLead(campaign.getCostPerLead());
		pcia.setDeliveryTarget(campaign.getDeliveryTarget());
		pcia.setDailyCap(campaign.getDailyCap());
		pcia.setOrganizationid(campaign.getOrganizationId());
		pcia.setOrganizationName(campaign.getOrganizationId());
		pcia.setSimpleDisposition(campaign.isSimpleDisposition());
		pcia.setDialerMode(campaign.getDialerMode().toString());
		pcia.setCampaignType(campaign.getType().toString());
		pcia.setUsePlivo(false);
		pcia.setEmailRevealed(pci.getProspectCall().isEmailRevealed());///
		pcia.setCallbackDate(pci.getCallbackDate());
		pcia.setResearchData(pci.isResearchData());
		pcia.setProspectingData(pci.isProspectingData());
		pcia.setAutoMachineDetected(pci.getProspectCall().isAutoMachineDetected());
		List<String> nList = new ArrayList<String>();
		if (pci != null && pci.getProspectCall() != null && pci.getProspectCall().getNotes() != null) {
			for (Note n : pci.getProspectCall().getNotes()) {
			    nList.add("Created Date : "+n.getCreationDate()+", Notes : "+n.getText()+", Agent ID : "+ n.getAgentId());
		    }
		}
		if(nList != null && !nList.isEmpty()) {
			pcia.setNotes(String.join(";\n ", nList));
		}
		pcia.setAgentLoginURL(pci.getProspectCall().getAgentLoginURL());
		pcia.setSipProvider(campaign.getSipProvider());
		pcia.setUseAMDetection(campaign.isEnableMachineAnsweredDetection());
		pcia.setCampaignStartDate(campaign.getStartDate());
		pcia.setCampaignEndDate(campaign.getEndDate());
		pcia.setCallControlPRovider(campaign.getCallControlProvider());
		pcia.setCompany(pci.getProspectCall().getProspect().getCompany());
		if (pci.getProspectCall().getProspect().getCountry() != null
				&& !pci.getProspectCall().getProspect().getCountry().isEmpty()) {
			String pcountry = pci.getProspectCall().getProspect().getCountry();
			int i = 1;
			List<String> countries = Arrays.asList(pcountry.split(","));
			for (String country : countries) {
				if (i == 1)
					pcia.setCountry1(country);
				if (i == 2)
					pcia.setCountry2(country);
				if (i == 3)
					pcia.setCountry3(country);
				if (i == 4)
					pcia.setCountry4(country);
				if (i == 5)
					pcia.setCountry5(country);

				i++;
			}

		}
		if (pci.getProspectCall().getAnswers() != null) {
			int i = 1;
			for (AgentQaAnswer agentQaAnswer : pci.getProspectCall().getAnswers()) {

				if (i == 1) {
					pcia.setCq1_question(agentQaAnswer.getQuestion());
					pcia.setCq1_response(agentQaAnswer.getAnswer());
				}
				if (i == 2) {
					pcia.setCq2_question(agentQaAnswer.getQuestion());
					pcia.setCq2_response(agentQaAnswer.getAnswer());
				}
				if (i == 3) {
					pcia.setCq3_question(agentQaAnswer.getQuestion());
					pcia.setCq3_response(agentQaAnswer.getAnswer());
				}
				if (i == 4) {
					pcia.setCq4_question(agentQaAnswer.getQuestion());
					pcia.setCq4_response(agentQaAnswer.getAnswer());
				}
				if (i == 5) {
					pcia.setCq5_question(agentQaAnswer.getQuestion());
					pcia.setCq5_response(agentQaAnswer.getAnswer());
				}
				i++;

			}

		}

		if (pci.getQaFeedback() != null && pci.getQaFeedback().getQaAnswers() != null) {
			int i = 1;
			for (AgentQaAnswer agentQaAnswer : pci.getQaFeedback().getQaAnswers()) {
				if (i == 1) {
					pcia.setCq1_certifyLink(agentQaAnswer.getAnswerCertification());
					pcia.setCq1_validationresp(agentQaAnswer.getIsAnswerValid());
				}
				if (i == 2) {
					pcia.setCq2_certifyLink(agentQaAnswer.getAnswerCertification());
					pcia.setCq2_validationresp(agentQaAnswer.getIsAnswerValid());
				}
				if (i == 3) {
					pcia.setCq3_certifyLink(agentQaAnswer.getAnswerCertification());
					pcia.setCq3_validationresp(agentQaAnswer.getIsAnswerValid());
				}
				if (i == 4) {
					pcia.setCq4_certifyLink(agentQaAnswer.getAnswerCertification());
					pcia.setCq4_validationresp(agentQaAnswer.getIsAnswerValid());
				}
				if (i == 5) {
					pcia.setCq5_certifyLink(agentQaAnswer.getAnswerCertification());
					pcia.setCq5_validationresp(agentQaAnswer.getIsAnswerValid());
				}
				i++;
			}

		}

		DateFormat df = new SimpleDateFormat("MM/dd/yy");
		Date dateobj = new Date();
		// System.out.println(df.format(dateobj));
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		String formattedDate = dateFormat.format(date);
		// System.out.println("Current time of the day using Calendar - 24 hour format:
		// "+ formattedDate);
		try {
			pcia.setCreated_date(df.parse(df.format(dateobj)));
			pcia.setCall_date(df.parse(df.format(dateobj)));
			Time time = Time.valueOf(formattedDate);
			// System.out.println(time);
			pcia.setCreated_datetime(dateobj);// TODO need time here
			pcia.setCall_time(dateobj);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (pci.getProspectCall().getProspect().isDirectPhone())
			pcia.setDirectPhone(1);

		pcia.setCreatedBy(pci.getCreatedBy());
		pcia.setCreatedDate(dateobj);
//		if(pcia.getDatasource()!=null  && !pcia.getDatasource().isEmpty())
		pcia.setDatasource(pci.getProspectCall().getProspect().getSource());

		if (pci.getProspectCall().getProspect().getDepartment() != null
				&& !pci.getProspectCall().getProspect().getDepartment().isEmpty())
			pcia.setDepartment(pci.getProspectCall().getProspect().getDepartment());

		if (pci.getProspectCall().getDispositionStatus() != null
				&& !pci.getProspectCall().getDispositionStatus().isEmpty())
			pcia.setDispositionstatus(pci.getProspectCall().getDispositionStatus());

		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL")
						|| pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNRM")))
			pcia.setDnc_added(1);
		if (pci.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null && !pci
				.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString().isEmpty())
			pcia.setEmployeeCountMin(new Double(
					pci.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString()));
		if (pci.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") != null && !pci
				.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty())
			pcia.setEmployeeCountMax(new Double(
					pci.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString()));

		pcia.setFirstname(pci.getProspectCall().getProspect().getFirstName());
		// pcia.setHandletime(pci.getProspectCall().getProspectHandleDuration());
		pcia.setIndustry(pci.getProspectCall().getProspect().getIndustry());

		if (pci.getProspectCall().getProspect().getIndustryList() != null
				&& pci.getProspectCall().getProspect().getIndustryList().size() > 0) {

			int i = 1;
			List<String> industries = pci.getProspectCall().getProspect().getIndustryList();
			for (String ind : industries) {
				if (i == 1)
					pcia.setIndustry1(ind);
				if (i == 2)
					pcia.setIndustry2(ind);
				if (i == 3)
					pcia.setIndustry3(ind);
				if (i == 4)
					pcia.setIndustry4(ind);

				i++;
			}
		}
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("ANSWERMACHINE")))
			pcia.setInteraction_answer_machine(1);
		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_ABANDONED))
				|| (pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("ABANDONED"))))
			pcia.setInteraction_call_abandoned(1);
		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_BUSY))
				|| (pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("BUSY"))))
			pcia.setInteraction_busy(1);
		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_FAILED))
				|| (pci.getProspectCall().getDispositionStatus() != null
						&& (pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("FAILURE"))))
			pcia.setInteraction_failure(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("GATEKEEPER_ANSWERMACHINE")))
			pcia.setInteraction_gatekeeper_answer_machine(1);
		if (pci.getStatus() != null
				&& (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_MACHINE_ANSWERED)))
			pcia.setAuto_machine_answered(1);

		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.LOCAL_DNC_ERROR))
				|| (pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL"))))
			pcia.setInteraction_local_dnc_error(1);
		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.NATIONAL_DNC_ERROR))
				|| (pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("DNCRM"))))
			pcia.setInteraction_national_dnc_error(1);
		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED))
			pcia.setInteraction_max_retry_limit_reached(1);
		if ((pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_NO_ANSWER))
				|| (pci.getProspectCall().getSubStatus() != null
						&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("NOANSWER"))))
			pcia.setInteraction_no_answer(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("NO_CONSENT")))
			pcia.setInteraction_no_consent(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("NOTINTERESTED")))
			pcia.setInteraction_not_interested(1);
		if (pci.getStatus() != null && pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE)
				&& pci.getProspectCall().getDispositionStatus() != null
				&& (pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")))
			pcia.setInteraction_success(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("PROSPECT_UNREACHABLE")))
			pcia.setInteraction_unreachable(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("DEADAIR")))
			pcia.setInteraction_dead_air(1);
		if (pci.getProspectCall().getSubStatus() != null
				&& (pci.getProspectCall().getSubStatus().equalsIgnoreCase("FAILED_QUALIFICATION")))
			pcia.setInteraction_FAILED_QUALIFICATION(1);

		if (pci.getStatus().equals(ProspectCallLog.ProspectCallStatus.UNKNOWN_ERROR))
			pcia.setUnknownerror(1);

		pcia.setLastname(pci.getProspectCall().getProspect().getLastName());

		String qaReason = "";

		if (pci.getQaFeedback() != null && pci.getQaFeedback().getFeedbackResponseList() != null) {
			List<FeedbackSectionResponse> fsrList = pci.getQaFeedback().getFeedbackResponseList();
			for (FeedbackSectionResponse fsr : fsrList) {
				if (fsr.getSectionName() != null && fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
					List<FeedbackResponseAttribute> fsraList = fsr.getResponseAttributes();
					if (fsraList != null && fsraList.size() > 0) {
						for (FeedbackResponseAttribute fsra : fsraList) {
							if (fsra.getAttribute() != null
									&& fsra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
								pcia.setLead_validation_valid(fsra.getFeedback());
								if (fsra.getAttributeComment() != null) {
									pcia.setLead_validation_notes(fsra.getAttributeComment());
								}
								if (fsra.getRejectionReason() != null) {
									qaReason = qaReason + fsra.getRejectionReason();
								}
							}
						}
					}
				}
			}
		}
		if (qaReason != null && !qaReason.isEmpty()) {
			pcia.setRejectionReason(qaReason);
		} else {
			pcia.setRejectionReason("null");
		}

		if (pci.getProspectCall().getLeadStatus() != null)
			pcia.setLeadStatus(pci.getProspectCall().getLeadStatus());

		// pcia.setInteraction_pcid(pci.getProspectCall().getProspectCallId());

		pcia.setManagement_level(pci.getProspectCall().getProspect().getManagementLevel());
		if (pci.getProspectCall().getProspect().getManagementLevel() != null
				&& pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("Manager"))
			pcia.setMgt_level_manager(1);
		else if (pci.getProspectCall().getProspect().getManagementLevel() != null
				&& pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("Director"))
			pcia.setMgt_level_director(1);
		else if (pci.getProspectCall().getProspect().getManagementLevel() != null
				&& (pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("VP-Level")
						|| pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("VP_EXECUTIVES")))
			pcia.setMgt_level_vice_president(1);
		else if (pci.getProspectCall().getProspect().getManagementLevel() != null
				&& (pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("C-Level")
						|| pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("C_EXECUTIVES")))
			pcia.setMgt_level_c_level(1);
		else if (pci.getProspectCall().getProspect().getManagementLevel() != null
				&& pci.getProspectCall().getProspect().getManagementLevel().equalsIgnoreCase("executiv"))
			pcia.setMgt_level_president_principal(1);
		else if (pci.getProspectCall().getProspect().getManagementLevel() != null)
			pcia.setMgt_level_non_management(1);

		//pcia.setOrganizationid(campaign.getOrganizationId());
		pcia.setOutboundnumber(pci.getProspectCall().getOutboundNumber());
		if (pci.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !pci.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty())
			pcia.setP_domain(pci.getProspectCall().getProspect().getCustomAttributeValue("domain").toString());

		pcia.setPartnerid(pci.getProspectCall().getPartnerId());
		pcia.setPhone(pci.getProspectCall().getProspect().getPhone());
		if (pci.getProspectCall().getProspect().getCompanyPhone() != null
				&& !pci.getProspectCall().getProspect().getCompanyPhone().isEmpty()) {
			pcia.setCompanyPhone(pci.getProspectCall().getProspect().getCompanyPhone());
		}

		if (pci.getProspectCall().getDailyCallRetryCount() != null
				&& pci.getProspectCall().getDailyCallRetryCount() > 0) {

			String anum = String.valueOf(pci.getProspectCall().getDailyCallRetryCount());
			int a = Integer.parseInt(anum.substring(anum.length() - 2));
			pcia.setDailyRetryCount(a);
		}
		pcia.setPrefix(pci.getProspectCall().getProspect().getPrefix());
		pcia.setProspectcallid(pci.getProspectCall().getProspectCallId());

		if (pci.getQaFeedback() != null && pci.getQaFeedback().getQaId() != null)
			pcia.setQaid(pci.getQaFeedback().getQaId());

		if (qaName != null && !qaName.isEmpty())
			pcia.setQaname(qaName);

		if (pci.getProspectCall().getQualityBucket() != null)
			pcia.setQualitybucket(pci.getProspectCall().getQualityBucket());

		if (pci.getProspectCall().getRecordingUrlAws() != null)
			pcia.setRecordingurl(pci.getProspectCall().getRecordingUrlAws());
		else if (pci.getProspectCall().getRecordingUrl() != null)
			pcia.setRecordingurl(pci.getProspectCall().getRecordingUrl());

		if (pci.getProspectCall().getProspect().getCustomAttributeValue("minRevenue") != null
				&& !pci.getProspectCall().getProspect().getCustomAttributeValue("minRevenue").toString().isEmpty())
			pcia.setRevenuemin(
					new Double(pci.getProspectCall().getProspect().getCustomAttributeValue("minRevenue").toString()));
		else
			pcia.setRevenuemax(0.0);
		if (pci.getProspectCall().getProspect().getCustomAttributeValue("maxRevenue") != null
				&& !pci.getProspectCall().getProspect().getCustomAttributeValue("maxRevenue").toString().isEmpty())
			pcia.setRevenuemin(
					new Double(pci.getProspectCall().getProspect().getCustomAttributeValue("maxRevenue").toString()));
		else
			pcia.setRevenuemin(0.0);

		if (pci.getProspectCall().getProspect().getStateCode() != null)
			pcia.setStatecode(pci.getProspectCall().getProspect().getStateCode());

		pcia.setStatus(pci.getStatus().toString());
		if (pci.getProspectCall().getSubStatus() != null)
			pcia.setSubstatus(pci.getProspectCall().getSubStatus());

		pcia.setSuffix(pci.getProspectCall().getProspect().getSuffix());

		if (pci.getProspectCall().getProspect().getTimeZone() != null)
			pcia.setTimezone(pci.getProspectCall().getProspect().getTimeZone());

		if (pci.getProspectCall().getProspect().getTitle() != null)
			pcia.setTitle(pci.getProspectCall().getProspect().getTitle());

		Date datepci = pci.getUpdatedDate();
		if (datepci == null)
			datepci = new Date();

		SimpleDateFormat day = new SimpleDateFormat("E"); // the day of the week abbreviated
		pcia.setTransactiondate_day(day.format(datepci));

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(datepci);
		pcia.setTransactiondate_dayofweek(calendar.get(Calendar.DAY_OF_WEEK));

		pcia.setTransactiondate_dayofyear(calendar.get(Calendar.DAY_OF_YEAR));
		pcia.setTransactiondate_hour(calendar.get(Calendar.HOUR));
		pcia.setTransactiondate_minute(calendar.get(Calendar.MINUTE));
		pcia.setTransactiondate_month(calendar.get(Calendar.DAY_OF_MONTH));
		pcia.setTransactiondate_second(calendar.get(Calendar.SECOND));
		pcia.setTransactiondate_week(calendar.get(Calendar.WEEK_OF_YEAR));
		pcia.setTransactiondate_year(calendar.get(Calendar.YEAR));
		if (pci.getProspectCall().getTwilioCallSid() != null)
			pcia.setTwiliocallsid(pci.getProspectCall().getTwilioCallSid());
		pcia.setVoiceProvider(pci.getProspectCall().getVoiceProvider());
		pcia.setSipProvider(pci.getProspectCall().getSipProvider());

		/*
		 * pcia.setUpdatedBy(pci.getUpdatedBy());
		 * pcia.setUpdatedDate(pci.getUpdatedDate());
		 */

		// pcia.setVersion(pci.getVersion());
		if (pci.getProspectCall().getTelcoDuration() != null)
			pcia.setVoiceduration(pci.getProspectCall().getTelcoDuration());
		else
			pcia.setVoiceduration(0);

		if (pci.getProspectCall().getCallStartTime() != null) {
			DateFormat dfcall = new SimpleDateFormat("MM/dd/yy");
			Date datecall = pci.getProspectCall().getCallStartTime();
			Calendar caltelco = Calendar.getInstance();
			Date telcotime = caltelco.getTime();
			DateFormat dateFormattelco = new SimpleDateFormat("HH:mm:ss");
			String telcoformattime = dateFormattelco.format(telcotime);
			try {
				pcia.setCall_date(dfcall.parse(dfcall.format(datecall)));
				Time timetelco = Time.valueOf(telcoformattime);
				pcia.setCall_time(datecall);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		pcia.setEmail(pci.getProspectCall().getProspect().getEmail());
		if(pci.getProspectCall().getProspect().getCustomFields()!=null && pci.getProspectCall().getProspect().getCustomFields().size()>0) {
			for(AgentQaAnswer aqa : pci.getProspectCall().getProspect().getCustomFields()) {
				String customAttrib = campaign.getCustomFieldAttribute(aqa.getQuestion(), campaign.getQualificationCriteria().getExpressions());
				if(customAttrib!=null && !customAttrib.isEmpty()) {
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_01")) {
						pcia.setCustomFieldAnswer01(aqa.getAnswer());
						pcia.setCustomFieldQuestion01(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_02")) {
						pcia.setCustomFieldAnswer02(aqa.getAnswer());
						pcia.setCustomFieldQuestion02(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_03")) {
						pcia.setCustomFieldAnswer03(aqa.getAnswer());
						pcia.setCustomFieldQuestion03(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_04")) {
						pcia.setCustomFieldAnswer04(aqa.getAnswer());
						pcia.setCustomFieldQuestion04(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_05")) {
						pcia.setCustomFieldAnswer05(aqa.getAnswer());
						pcia.setCustomFieldQuestion05(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_06")) {
						pcia.setCustomFieldAnswer06(aqa.getAnswer());
						pcia.setCustomFieldQuestion06(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_07")) {
						pcia.setCustomFieldAnswer07(aqa.getAnswer());
						pcia.setCustomFieldQuestion07(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_08")) {
						pcia.setCustomFieldAnswer08(aqa.getAnswer());
						pcia.setCustomFieldQuestion08(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_09")) {
						pcia.setCustomFieldAnswer09(aqa.getAnswer());
						pcia.setCustomFieldQuestion09(aqa.getQuestion());
					}
					if(customAttrib.equalsIgnoreCase("CUSTOMFIELD_10")) {
						pcia.setCustomFieldAnswer10(aqa.getAnswer());
						pcia.setCustomFieldQuestion10(aqa.getQuestion());
					}
				}
			}
		}
		return this.PCIAnalysisdumb(pcia);
	}

	public String getCallDetectedAs() {
		return callDetectedAs;
	}

	public void setCallDetectedAs(String callDetectedAs) {
		this.callDetectedAs = callDetectedAs;
	}

	public long getAmdTimeDiffInMilliSec() {
		return amdTimeDiffInMilliSec;
	}

	public void setAmdTimeDiffInMilliSec(long amdTimeDiffInMilliSec) {
		this.amdTimeDiffInMilliSec = amdTimeDiffInMilliSec;
	}

	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(String dialerMode) {
		this.dialerMode = dialerMode;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public boolean isUsePlivo() {
		return usePlivo;
	}

	public void setUsePlivo(boolean usePlivo) {
		this.usePlivo = usePlivo;
	}

	public boolean isUseAMDetection() {
		return useAMDetection;
	}

	public void setUseAMDetection(boolean useAMDetection) {
		this.useAMDetection = useAMDetection;
	}

	public Date getCampaignStartDate() {
		return campaignStartDate;
	}

	public void setCampaignStartDate(Date campaignStartDate) {
		this.campaignStartDate = campaignStartDate;
	}

	public Date getCampaignEndDate() {
		return campaignEndDate;
	}

	public void setCampaignEndDate(Date campaignEndDate) {
		this.campaignEndDate = campaignEndDate;
	}

	public String getCallControlPRovider() {
		return callControlPRovider;
	}

	public void setCallControlPRovider(String callControlPRovider) {
		this.callControlPRovider = callControlPRovider;
	}

	public int getCostPerLead() {
		return costPerLead;
	}

	public void setCostPerLead(int costPerLead) {
		this.costPerLead = costPerLead;
	}

	public int getDeliveryTarget() {
		return deliveryTarget;
	}

	public void setDeliveryTarget(int deliveryTarget) {
		this.deliveryTarget = deliveryTarget;
	}

	public int getDailyCap() {
		return dailyCap;
	}

	public void setDailyCap(int dailyCap) {
		this.dailyCap = dailyCap;
	}

	public String getTelnyxCallDetectedAs() {
		return telnyxCallDetectedAs;
	}

	public void setTelnyxCallDetectedAs(String telnyxCallDetectedAs) {
		this.telnyxCallDetectedAs = telnyxCallDetectedAs;
	}

	public boolean isUseSlice() {
		return useSlice;
	}

	public void setUseSlice(boolean useSlice) {
		this.useSlice = useSlice;
	}

	public boolean isEmailRevealed() {
		return emailRevealed;
	}

	public void setEmailRevealed(boolean emailRevealed) {
		this.emailRevealed = emailRevealed;
	}

	public Date getCallbackDate() {
		return callbackDate;
	}

	public void setCallbackDate(Date callbackDate) {
		this.callbackDate = callbackDate;
	}

	public boolean isResearchData() {
		return researchData;
	}

	public void setResearchData(boolean researchData) {
		this.researchData = researchData;
	}

	public boolean isProspectingData() {
		return prospectingData;
	}

	public void setProspectingData(boolean prospectingData) {
		this.prospectingData = prospectingData;
	}

	public boolean isAutoMachineDetected() {
		return autoMachineDetected;
	}

	public void setAutoMachineDetected(boolean autoMachineDetected) {
		this.autoMachineDetected = autoMachineDetected;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getAgentLoginURL() {
		return agentLoginURL;
	}

	public void setAgentLoginURL(String agentLoginURL) {
		this.agentLoginURL = agentLoginURL;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	@Override
	public String toString() {
		return "PCIAnalysis [agentId=" + agentId + ", agentLoginURL=" + agentLoginURL + ", agentName=" + agentName
				+ ", agentOnlineTime=" + agentOnlineTime + ", amdTimeDiffInMilliSec=" + amdTimeDiffInMilliSec
				+ ", amdTimeDiffInSec=" + amdTimeDiffInSec + ", assetName=" + assetName + ", autoMachineDetected="
				+ autoMachineDetected + ", auto_machine_answered=" + auto_machine_answered + ", callControlPRovider="
				+ callControlPRovider + ", callDetectedAs=" + callDetectedAs + ", call_attempted=" + call_attempted
				+ ", call_connected=" + call_connected + ", call_contacted=" + call_contacted + ", call_date="
				+ call_date + ", call_time=" + call_time + ", callbackDate=" + callbackDate + ", callduration="
				+ callduration + ", callretrycount=" + callretrycount + ", campaignEndDate=" + campaignEndDate
				+ ", campaignManagerId=" + campaignManagerId + ", campaignName=" + campaignName + ", campaignStartDate="
				+ campaignStartDate + ", campaignType=" + campaignType + ", campaign_status=" + campaign_status
				+ ", campaignid=" + campaignid + ", clientDelivered=" + clientDelivered + ", clientName=" + clientName
				+ ", company=" + company + ", companyPhone=" + companyPhone + ", costPerLead=" + costPerLead
				+ ", country1=" + country1 + ", country2=" + country2 + ", country3=" + country3 + ", country4="
				+ country4 + ", country5=" + country5 + ", cq1_certifyLink=" + cq1_certifyLink + ", cq1_question="
				+ cq1_question + ", cq1_response=" + cq1_response + ", cq1_validationresp=" + cq1_validationresp
				+ ", cq2_certifyLink=" + cq2_certifyLink + ", cq2_question=" + cq2_question + ", cq2_response="
				+ cq2_response + ", cq2_validationresp=" + cq2_validationresp + ", cq3_certifyLink=" + cq3_certifyLink
				+ ", cq3_question=" + cq3_question + ", cq3_response=" + cq3_response + ", cq3_validationresp="
				+ cq3_validationresp + ", cq4_certifyLink=" + cq4_certifyLink + ", cq4_question=" + cq4_question
				+ ", cq4_response=" + cq4_response + ", cq4_validationresp=" + cq4_validationresp + ", cq5_certifyLink="
				+ cq5_certifyLink + ", cq5_question=" + cq5_question + ", cq5_response=" + cq5_response
				+ ", cq5_validationresp=" + cq5_validationresp + ", createdDateQLIK=" + createdDateQLIK
				+ ", created_date=" + created_date + ", created_datetime=" + created_datetime + ", customFieldAnswer01="
				+ customFieldAnswer01 + ", customFieldAnswer02=" + customFieldAnswer02 + ", customFieldAnswer03="
				+ customFieldAnswer03 + ", customFieldAnswer04=" + customFieldAnswer04 + ", customFieldAnswer05="
				+ customFieldAnswer05 + ", customFieldAnswer06=" + customFieldAnswer06 + ", customFieldAnswer07="
				+ customFieldAnswer07 + ", customFieldAnswer08=" + customFieldAnswer08 + ", customFieldAnswer09="
				+ customFieldAnswer09 + ", customFieldAnswer10=" + customFieldAnswer10 + ", customFieldQuestion01="
				+ customFieldQuestion01 + ", customFieldQuestion02=" + customFieldQuestion02
				+ ", customFieldQuestion03=" + customFieldQuestion03 + ", customFieldQuestion04="
				+ customFieldQuestion04 + ", customFieldQuestion05=" + customFieldQuestion05
				+ ", customFieldQuestion06=" + customFieldQuestion06 + ", customFieldQuestion07="
				+ customFieldQuestion07 + ", customFieldQuestion08=" + customFieldQuestion08
				+ ", customFieldQuestion09=" + customFieldQuestion09 + ", customFieldQuestion10="
				+ customFieldQuestion10 + ", dailyCap=" + dailyCap + ", dailyRetryCount=" + dailyRetryCount
				+ ", dataSlice=" + dataSlice + ", datasource=" + datasource + ", deliveryTarget=" + deliveryTarget
				+ ", department=" + department + ", dialerMode=" + dialerMode + ", directPhone=" + directPhone
				+ ", dispositionstatus=" + dispositionstatus + ", dnc_added=" + dnc_added + ", email=" + email
				+ ", emailRevealed=" + emailRevealed + ", employeeCountMax=" + employeeCountMax + ", employeeCountMin="
				+ employeeCountMin + ", firstname=" + firstname + ", industry=" + industry + ", industry1=" + industry1
				+ ", industry2=" + industry2 + ", industry3=" + industry3 + ", industry4=" + industry4
				+ ", interaction_FAILED_QUALIFICATION=" + interaction_FAILED_QUALIFICATION
				+ ", interaction_answer_machine=" + interaction_answer_machine + ", interaction_busy="
				+ interaction_busy + ", interaction_call_abandoned=" + interaction_call_abandoned
				+ ", interaction_dead_air=" + interaction_dead_air + ", interaction_failure=" + interaction_failure
				+ ", interaction_gatekeeper_answer_machine=" + interaction_gatekeeper_answer_machine
				+ ", interaction_local_dnc_error=" + interaction_local_dnc_error
				+ ", interaction_max_retry_limit_reached=" + interaction_max_retry_limit_reached
				+ ", interaction_national_dnc_error=" + interaction_national_dnc_error + ", interaction_no_answer="
				+ interaction_no_answer + ", interaction_no_consent=" + interaction_no_consent
				+ ", interaction_not_interested=" + interaction_not_interested + ", interaction_success="
				+ interaction_success + ", interaction_unreachable=" + interaction_unreachable + ", lastUpdatedDate="
				+ lastUpdatedDate + ", lastname=" + lastname + ", leadDelivered=" + leadDelivered + ", leadStatus="
				+ leadStatus + ", leadStatusUpdated=" + leadStatusUpdated + ", lead_validation_notes="
				+ lead_validation_notes + ", lead_validation_valid=" + lead_validation_valid + ", management_level="
				+ management_level + ", mgt_level_c_level=" + mgt_level_c_level + ", mgt_level_director="
				+ mgt_level_director + ", mgt_level_manager=" + mgt_level_manager + ", mgt_level_non_management="
				+ mgt_level_non_management + ", mgt_level_president_principal=" + mgt_level_president_principal
				+ ", mgt_level_vice_president=" + mgt_level_vice_president + ", notes=" + notes + ", organizationName="
				+ organizationName + ", organizationid=" + organizationid + ", outboundnumber=" + outboundnumber
				+ ", p_domain=" + p_domain + ", partnerid=" + partnerid + ", phone=" + phone + ", prefix=" + prefix
				+ ", prospectcallid=" + prospectcallid + ", prospecthandleduration=" + prospecthandleduration
				+ ", prospectingData=" + prospectingData + ", qaid=" + qaid + ", qaname=" + qaname + ", qualityBucket="
				+ qualityBucket + ", qualityBucketSort=" + qualityBucketSort + ", qualitybucket=" + qualitybucket
				+ ", recordingurl=" + recordingurl + ", rejectionReason=" + rejectionReason + ", researchData="
				+ researchData + ", retentationDate=" + retentationDate + ", revenuemax=" + revenuemax + ", revenuemin="
				+ revenuemin + ", simpleDisposition=" + simpleDisposition + ", sipProvider=" + sipProvider
				+ ", sourceType=" + sourceType + ", statecode=" + statecode + ", status=" + status + ", substatus="
				+ substatus + ", suffix=" + suffix + ", telnyxCallDetectedAs=" + telnyxCallDetectedAs + ", timezone="
				+ timezone + ", title=" + title + ", transactiondate_day=" + transactiondate_day
				+ ", transactiondate_dayofweek=" + transactiondate_dayofweek + ", transactiondate_dayofyear="
				+ transactiondate_dayofyear + ", transactiondate_hour=" + transactiondate_hour
				+ ", transactiondate_minute=" + transactiondate_minute + ", transactiondate_month="
				+ transactiondate_month + ", transactiondate_second=" + transactiondate_second
				+ ", transactiondate_week=" + transactiondate_week + ", transactiondate_year=" + transactiondate_year
				+ ", twiliocallsid=" + twiliocallsid + ", unknownerror=" + unknownerror + ", useAMDetection="
				+ useAMDetection + ", usePlivo=" + usePlivo + ", useSlice=" + useSlice
				+ ", voiceProvider=" + voiceProvider + ", voiceduration=" + voiceduration
				+ ", sipProvider=" + sipProvider + ", zipcode=" + zipcode + "]";
	}

}
