package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "everstringcompany")
public class EverStringCompany extends AbstractEntity {

	private int ecid;
	private String name;
	private String domain;
	private String campaignId;
	private String status;

	public EverStringCompany() {
		// TODO Auto-generated constructor stub
	}

	public EverStringCompany(int ecid, String name, String domain, String campaignId, String status) {
		super();
		this.ecid = ecid;
		this.name = name;
		this.domain = domain;
		this.campaignId = campaignId;
		this.status = status;
	}

	public int getEcid() {
		return ecid;
	}

	public void setEcid(int ecid) {
		this.ecid = ecid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EverStringCompany [ecid=" + ecid + ", name=" + name + ", domain=" + domain + ", campaignId="
				+ campaignId + ", status=" + status + "]";
	}

}
