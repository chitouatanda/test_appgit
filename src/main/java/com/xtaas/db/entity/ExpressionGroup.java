/**
 * 
 */
package com.xtaas.db.entity;

import java.util.ArrayList;

/**
 * @author djain
 * 
 */
public class ExpressionGroup {
	private String groupClause; // AND, OR
	private ArrayList<Expression> expressions;
	private ArrayList<ExpressionGroup> expressionGroups;

	/**
	 * @return the groupClause
	 */
	public String getGroupClause() {
		return groupClause;
	}

	/**
	 * @param groupClause
	 *            the groupClause to set
	 */
	public void setGroupClause(String groupClause) {
		this.groupClause = groupClause;
	}

	/**
	 * @return the expressions
	 */
	public ArrayList<Expression> getExpressions() {
		return expressions;
	}

	/**
	 * @param expressions
	 *            the expressions to set
	 */
	public void setExpressions(ArrayList<Expression> expressions) {
		this.expressions = expressions;
	}

	/**
	 * @return the expressionGroups
	 */
	public ArrayList<ExpressionGroup> getExpressionGroups() {
		return expressionGroups;
	}

	/**
	 * @param expressionGroups
	 *            the expressionGroups to set
	 */
	public void setExpressionGroups(ArrayList<ExpressionGroup> expressionGroups) {
		this.expressionGroups = expressionGroups;
	}
}
