package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 4/9/14
 * Time: 10:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class TeamBid {
  private String teamId;
  private String status; //invited/responded/accepted
  private long bid; //bid * 10000
  private String bidType;
  private long budgetCap;
  private int leadCap;

  public String getTeamId() {
    return teamId;
  }

  public void setTeamId(String teamId) {
    this.teamId = teamId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public long getBid() {
    return bid;
  }

  public void setBid(long bid) {
    this.bid = bid;
  }

  public String getBidType() {
    return bidType;
  }

  public void setBidType(String bidType) {
    this.bidType = bidType;
  }

  public long getBudgetCap() {
    return budgetCap;
  }

  public void setBudgetCap(long budgetCap) {
    this.budgetCap = budgetCap;
  }

  public int getLeadCap() {
    return leadCap;
  }

  public void setLeadCap(int leadCap) {
    this.leadCap = leadCap;
  }

  @Override
  public String toString() {
    return "TeamBid{" +
      "teamId='" + teamId + '\'' +
      ", status='" + status + '\'' +
      ", bid=" + bid +
      ", bidType='" + bidType + '\'' +
      ", budgetCap=" + budgetCap +
      ", leadCap=" + leadCap +
      "} " + super.toString();
  }
}
