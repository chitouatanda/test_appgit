package com.xtaas.db.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "globalcontactinteraction")
public class GlobalContactInteraction extends AbstractEntity {

	private String source;
	private String sourceId;
	private String prefix;
	private String firstName;
	private String lastName;
	private String suffix;
	private String email;
	private String phone;
	private String companyPhone;
	private String extension;
	private boolean directPhone;
	private String title;
	private String department;
	private String managementLevel;
	private Double minRevenue;
	private Double maxRevenue;
	private Double minEmployeeCount;
	private Double maxEmployeeCount;
	private Double revCount;
	private Double revCountIn000s;
	private Double empCount;
	private String domain;
	private String organizationName;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String stateCode;
	private String country;
	private String countryCode;
	private String postalCode;
	private String sourceCompanyId;
	private Map<String, String> companyAddress;
	private List<String> industries;
	private String industry;
	private boolean isEu;
	private String timezone;
	private List<String> companySIC;
	private List<String> companyNAICS;
	private String zoomCompanyUrl;
	private String zoomPersonUrl;
	private String status;
	private String globalContactId;
	private String prospectCallId;
	private String changeLog;

	public GlobalContactInteraction toGlobalContactInteraction(GlobalContact globalContact) {
		this.source = globalContact.getSource();
		this.sourceId = globalContact.getSourceId();
		this.prefix = globalContact.getPrefix();
		this.firstName = globalContact.getFirstName();
		this.lastName = globalContact.getLastName();
		this.suffix = globalContact.getSuffix();
		this.email = globalContact.getEmail();
		this.phone = globalContact.getPhone();
		this.companyPhone = globalContact.getCompanyPhone();
		this.extension = globalContact.getExtension();
		this.directPhone = globalContact.isDirectPhone();
		this.title = globalContact.getTitle();
		this.department = globalContact.getDepartment();
		this.managementLevel = globalContact.getManagementLevel();
		this.minRevenue = globalContact.getMinRevenue();
		this.maxRevenue = globalContact.getMaxRevenue();
		this.minEmployeeCount = globalContact.getMinEmployeeCount();
		this.maxEmployeeCount = globalContact.getMaxEmployeeCount();
		this.revCount = globalContact.getRevCount();
		this.revCountIn000s = globalContact.getRevCountIn000s();
		this.empCount = globalContact.getEmpCount();
		this.domain = globalContact.getDomain();
		this.organizationName = globalContact.getOrganizationName();
		this.addressLine1 = globalContact.getAddressLine1();
		this.addressLine2 = globalContact.getAddressLine2();
		this.city = globalContact.getCity();
		this.state = globalContact.getStateCode();
		this.stateCode = globalContact.getStateCode();
		this.country = globalContact.getCountry();
		this.countryCode = globalContact.getCountry();
		this.postalCode = globalContact.getPostalCode();
		this.sourceCompanyId = globalContact.getSourceCompanyId();
		this.companyAddress = globalContact.getCompanyAddress();
		this.industries = globalContact.getIndustries();
		this.industry = globalContact.getIndustry();
		this.isEu = globalContact.isEu();
		this.timezone = globalContact.getTimezone();
		this.companySIC = globalContact.getCompanySIC();
		this.companyNAICS = globalContact.getCompanyNAICS();
		this.zoomCompanyUrl = globalContact.getZoomCompanyUrl();
		this.zoomPersonUrl = globalContact.getZoomPersonUrl();
		this.status = globalContact.getStatus();
		this.globalContactId = globalContact.getGlobalContactId();
		this.prospectCallId = globalContact.getProspectCallId();
		this.changeLog = globalContact.getChangeLog();
		return this;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public boolean isDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(boolean directPhone) {
		this.directPhone = directPhone;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getManagementLevel() {
		return managementLevel;
	}

	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}

	public Double getMinRevenue() {
		return minRevenue;
	}

	public void setMinRevenue(Double minRevenue) {
		this.minRevenue = minRevenue;
	}

	public Double getMaxRevenue() {
		return maxRevenue;
	}

	public void setMaxRevenue(Double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	public Double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public void setMinEmployeeCount(Double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	public Double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public void setMaxEmployeeCount(Double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}

	public Double getRevCount() {
		return revCount;
	}

	public void setRevCount(Double revCount) {
		this.revCount = revCount;
	}

	public Double getRevCountIn000s() {
		return revCountIn000s;
	}

	public void setRevCountIn000s(Double revCountIn000s) {
		this.revCountIn000s = revCountIn000s;
	}

	public Double getEmpCount() {
		return empCount;
	}

	public void setEmpCount(Double empCount) {
		this.empCount = empCount;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getSourceCompanyId() {
		return sourceCompanyId;
	}

	public void setSourceCompanyId(String sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	public List<String> getIndustries() {
		return industries;
	}

	public void setIndustries(List<String> industries) {
		this.industries = industries;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public boolean isEu() {
		return isEu;
	}

	public void setEu(boolean isEu) {
		this.isEu = isEu;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public List<String> getCompanySIC() {
		return companySIC;
	}

	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}

	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}

	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGlobalContactId() {
		return globalContactId;
	}

	public void setGlobalContactId(String globalContactId) {
		this.globalContactId = globalContactId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}

}
