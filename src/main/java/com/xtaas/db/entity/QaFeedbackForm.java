package com.xtaas.db.entity;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "qafeedbackform")
public class QaFeedbackForm extends AbstractEntity {
	private String campaignId;
	private LinkedHashMap<String, List<FeedbackFormAttribute>> formAtrributes;
	
	/**
	 * @param campaignId
	 * @param formAtrributes
	 */
	public QaFeedbackForm(String campaignId,
			LinkedHashMap<String, List<FeedbackFormAttribute>> formAtrributes) {
		this.campaignId = campaignId;
		this.formAtrributes = formAtrributes;
	}
	
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the formAtrributes
	 */
	public LinkedHashMap<String, List<FeedbackFormAttribute>> getFormAtrributes() {
		return formAtrributes;
	}
	/**
	 * @param formAtrributes the formAtrributes to set
	 */
	public void setFormAtrributes(
			LinkedHashMap<String, List<FeedbackFormAttribute>> formAtrributes) {
		this.formAtrributes = formAtrributes;
	}
	
}