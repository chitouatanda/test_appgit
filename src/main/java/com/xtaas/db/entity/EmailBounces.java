package com.xtaas.db.entity;

import java.util.Date;
import java.util.HashMap;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.web.dto.WebHookEventDTO;

@Document(collection = "emailbounces")
public class EmailBounces extends AbstractEntity {
	private String campaignId;
	private String prospectCallId;
	private String prospectInteractionSessionId;
	private String emailAddress;
	private Date emailSentDate;
	private HashMap postmarkBounceMap;
	private String fromEmailAddress;
	private String feedbackID;
	private String action;
	private String status;
	private String emailBounceMessageID;
	private String partnerId;
	private WebHookEventDTO webHookEvent;

	public WebHookEventDTO getWebHookEvent() {
		return webHookEvent;
	}

	public void setWebHookEvent(WebHookEventDTO webHookEvent) {
		this.webHookEvent = webHookEvent;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Date getEmailSentDate() {
		return emailSentDate;
	}

	public void setEmailSentDate(Date emailSentDate) {
		this.emailSentDate = emailSentDate;
	}

	public HashMap getPostmarkBounceMap() {
		return postmarkBounceMap;
	}

	public void setPostmarkBounceMap(HashMap postmarkBounceMap) {
		this.postmarkBounceMap = postmarkBounceMap;
	}

	public String getFromEmailAddress() {
		return fromEmailAddress;
	}

	public void setFromEmailAddress(String fromEmailAddress) {
		this.fromEmailAddress = fromEmailAddress;
	}

	public String getFeedbackID() {
		return feedbackID;
	}

	public void setFeedbackID(String feedbackID) {
		this.feedbackID = feedbackID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailBounceMessageID() {
		return emailBounceMessageID;
	}

	public void setEmailBounceMessageID(String emailBounceMessageID) {
		this.emailBounceMessageID = emailBounceMessageID;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

}
