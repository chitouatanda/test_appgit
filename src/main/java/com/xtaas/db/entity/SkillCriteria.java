package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/14/14
 * Time: 10:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class SkillCriteria extends Criteria{
  private String skillId;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    SkillCriteria that = (SkillCriteria) o;

    if (skillId != null ? !skillId.equals(that.skillId) : that.skillId != null) return false;

    return super.equals(o);
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (skillId != null ? skillId.hashCode() : 0);
    return 31 * result + super.hashCode();
  }

  @Override
  public String toString() {
    return "SkillCriteria{" +
      "skillId='" + skillId + '\'' +
      "} " + super.toString();
  }

  public String getSkillId() {

    return skillId;
  }

  public void setSkillId(String skillId) {
    this.skillId = skillId;
  }
}
