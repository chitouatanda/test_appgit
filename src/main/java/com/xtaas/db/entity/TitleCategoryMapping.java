package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection="titlecategorymapping")
public class TitleCategoryMapping extends AbstractEntity {

    private String department;
    private String managementLevel;
    private String categoryName;
    private List<String> titles;

    public String getManagementLevel() {
        return managementLevel;
    }

    public void setManagementLevel(String managementLevel) {
        this.managementLevel = managementLevel;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "TitleCategoryMapping{" +
                "department='" + department + '\'' +
                ", managementLevel='" + managementLevel + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", titles=" + titles +
                '}';
    }
}
