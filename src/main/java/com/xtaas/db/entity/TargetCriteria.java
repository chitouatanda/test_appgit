package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/14/14
 * Time: 10:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class TargetCriteria extends Criteria{
  private String type; //geo, age, industry, ...

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    TargetCriteria that = (TargetCriteria) o;

    if (type != null ? !type.equals(that.type) : that.type != null) return false;

    return super.equals(o);
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (type != null ? type.hashCode() : 0);
    return 31 * result + super.hashCode();
  }

  @Override
  public String toString() {
    return "TargetCriteria{" +
      "type='" + type + '\'' +
      "} " + super.toString();
  }

  public String getType() {

    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
