package com.xtaas.db.entity;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.util.CollectionUtils;

public class CRMAttribute {
	private String crmName; // attribute name in CRM
	private String systemName; // attribute name in XTaaS
	private String crmDataType; // attribute data type in CRM
	private String systemDataType; // attribute data type in XTaaS
	private int length;
	private String label;
	private ArrayList<PickListItem> pickList;
	private HashMap<String, Object> extraAttributes;
	private String prospectBeanPath; // Path in Prospect Bean where this value is stored
	private String questionSkin;
	private String attributeType;
	private boolean fromOrganization;
	private String sequence;
	private String campaignId;
	private String value;
	private boolean agentValidationRequired;
	private String operator;
	private boolean organizationQuestion;
	private boolean defaultQuestion;
	private ArrayList<PickListItem> defaultValues;

	/**
	 * 
	 * @param crmAttribute
	 * @param campaignId
	 * @return
	 */
	public CRMAttribute getCRMAttributeClone(CRMAttribute crmAttribute, String campaignId) {
		this.setCrmName(crmAttribute.getCrmName());
		this.setSystemName(crmAttribute.getSystemName());
		this.setCrmDataType(crmAttribute.getCrmDataType());
		this.setSystemDataType(crmAttribute.getSystemDataType());
		this.setLength(crmAttribute.getLength());
		this.setLabel(crmAttribute.getLabel());
		this.setPickList(crmAttribute.getPickList());
		this.setExtraAttributes(crmAttribute.getExtraAttributes());
		this.setProspectBeanPath(crmAttribute.getProspectBeanPath());
		this.setQuestionSkin(crmAttribute.getQuestionSkin());
		this.setAttributeType(crmAttribute.getAttributeType());
		this.setFromOrganization(crmAttribute.isFromOrganization());
		this.setSequence(crmAttribute.getSequence());
		this.setCampaignId(campaignId);
		return this;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public boolean isFromOrganization() {
		return fromOrganization;
	}

	public void setFromOrganization(boolean fromOrganization) {
		this.fromOrganization = fromOrganization;
	}

	/**
	 * @return the crmName
	 */
	public String getCrmName() {
		return crmName;
	}

	/**
	 * @param crmName
	 *            the crmName to set
	 */
	public void setCrmName(String crmName) {
		this.crmName = crmName;
	}

	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @param systemName
	 *            the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @return the crmDataType
	 */
	public String getCrmDataType() {
		return crmDataType;
	}

	/**
	 * @param crmDataType
	 *            the crmDataType to set
	 */
	public void setCrmDataType(String crmDataType) {
		this.crmDataType = crmDataType;
	}

	/**
	 * @return the systemDataType
	 */
	public String getSystemDataType() {
		return systemDataType;
	}

	/**
	 * @param systemDataType
	 *            the systemDataType to set
	 */
	public void setSystemDataType(String systemDataType) {
		this.systemDataType = systemDataType;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the pickList
	 */
	public ArrayList<PickListItem> getPickList() {
		return pickList;
	}

	/**
	 * @param pickList
	 *            the pickList to set
	 */
	public void setPickList(ArrayList<PickListItem> pickList) {
		this.pickList = pickList;
	}

	/**
	 * @return the extraAttributes
	 */
	public HashMap<String, Object> getExtraAttributes() {
		return extraAttributes;
	}

	/**
	 * @param extraAttributes
	 *            the extraAttributes to set
	 */
	public void setExtraAttributes(HashMap<String, Object> extraAttributes) {
		this.extraAttributes = extraAttributes;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the prospectBeanPath
	 */
	public String getProspectBeanPath() {
		return prospectBeanPath;
	}

	/**
	 * @param prospectBeanPath
	 *            the prospectBeanPath to set
	 */
	public void setProspectBeanPath(String prospectBeanPath) {
		this.prospectBeanPath = prospectBeanPath;
	}

	/**
	 * @return the questionSkin
	 */
	public String getQuestionSkin() {
		return questionSkin;
	}

	/**
	 * @param questionSkin
	 *            the questionSkin to set
	 */
	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}

	/**
	 * @return the attributeType
	 */
	public String getAttributeType() {
		return attributeType;
	}

	/**
	 * @param attributeType
	 *            the attributeType to set
	 */
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isAgentValidationRequired() {
		return agentValidationRequired;
	}

	public void setAgentValidationRequired(boolean agentValidationRequired) {
		this.agentValidationRequired = agentValidationRequired;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		if (!CollectionUtils.isEmpty(this.pickList)) {
			this.operator = operator;
		} else {
			this.operator = "FREE_TEXT";
		}
	}

	public boolean isOrganizationQuestion() {
		return organizationQuestion;
	}

	public void setOrganizationQuestion(boolean organizationQuestion) {
		this.organizationQuestion = organizationQuestion;
	}

	public boolean isDefaultQuestion() {
		return defaultQuestion;
	}

	public void setDefaultQuestion(boolean defaultQuestion) {
		this.defaultQuestion = defaultQuestion;
	}

	public ArrayList<PickListItem> getDefaultValues() {
		return defaultValues;
	}

	public void setDefaultValues(ArrayList<PickListItem> defaultValues) {
		this.defaultValues = defaultValues;
	}

	@Override
	public String toString() {
		return "CRMAttribute [crmName=" + crmName + ", systemName=" + systemName + ", crmDataType=" + crmDataType
				+ ", systemDataType=" + systemDataType + ", length=" + length + ", label=" + label + ", pickList="
				+ pickList + ", extraAttributes=" + extraAttributes + ", prospectBeanPath=" + prospectBeanPath
				+ ", questionSkin=" + questionSkin + ", attributeType=" + attributeType + ", fromOrganization="
				+ fromOrganization + ", sequence=" + sequence + ", campaignId=" + campaignId + ", value=" + value
				+ ", agentValidationRequired=" + agentValidationRequired + ", operator=" + operator
				+ ", organizationQuestion=" + organizationQuestion + ", defaultQuestion=" + defaultQuestion
				+ ", defaultValues=" + defaultValues + "]";
	}

}
