package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/14/14
 * Time: 10:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Criteria {
  private String criteria; //machine readable criteria expression that should evaluate to true. Should be parseable by SpelExpressionParser

  public String getCriteria() {
    return criteria;
  }

  public void setCriteria(String criteria) {
    this.criteria = criteria;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Criteria criteria1 = (Criteria) o;

    if (criteria != null ? !criteria.equals(criteria1.criteria) : criteria1.criteria != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return criteria != null ? criteria.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Criteria{" +
      "criteria='" + criteria + '\'' +
      '}';
  }
}
