package com.xtaas.db.entity;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.xtaas.valueobjects.KeyValuePair;

@Document(collection = "qarejectreason")
public class QaRejectReason {
	@Id
	private String type;
	private List<KeyValuePair<String, String>> qaRejectReason;

	public String getType() {
		return type;
	}

	public List<KeyValuePair<String, String>> getQaRejectReason() {
		return qaRejectReason;
	}

}
