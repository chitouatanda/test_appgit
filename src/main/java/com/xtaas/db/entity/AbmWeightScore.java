package com.xtaas.db.entity;

public class AbmWeightScore {
	private String domain;
	private String companyName;
	private int ratioScore;
	private int weightedRatioScore;
	private int distanceScore;
	private String dataSource;
	
	protected AbmWeightScore() {
		// TODO Auto-generated constructor stub
	}

	public AbmWeightScore(String dataSource,String domain, String companyName,int ratioScore,int weightedRatioScore,int distanceScore ) {
		this.domain = domain;
		this.companyName = companyName;
		this.ratioScore = ratioScore;
		this.weightedRatioScore = weightedRatioScore;
		this.distanceScore = distanceScore;
		this.dataSource = dataSource;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getRatioScore() {
		return ratioScore;
	}

	public void setRatioScore(int ratioScore) {
		this.ratioScore = ratioScore;
	}

	public int getWeightedRatioScore() {
		return weightedRatioScore;
	}

	public void setWeightedRatioScore(int weightedRatioScore) {
		this.weightedRatioScore = weightedRatioScore;
	}

	public int getDistanceScore() {
		return distanceScore;
	}

	public void setDistanceScore(int distanceScore) {
		this.distanceScore = distanceScore;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	

}


