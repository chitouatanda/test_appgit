package com.xtaas.db.entity;

import java.util.ArrayList;

public class TeamSearch {
	private String vertical;
	private String domain;
	private ArrayList<String> countries;
	private boolean excludeContries;
	private ArrayList<String> states;
	private boolean excludeStates;
	private ArrayList<String> cities;
	private boolean excludeCities;
	private String timeZone;
	private int rating;
	private ArrayList<String> languages;
	
	/**
	 * @return the vertical
	 */
	public String getVertical() {
		return vertical;
	}
	/**
	 * @param vertical the vertical to set
	 */
	public void setVertical(String vertical) {
		this.vertical = vertical;
	}
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}
	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}
	/**
	 * @return the countries
	 */
	public ArrayList<String> getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(ArrayList<String> countries) {
		this.countries = countries;
	}
	/**
	 * @return the excludeContries
	 */
	public boolean isExcludeContries() {
		return excludeContries;
	}
	/**
	 * @param excludeContries the excludeContries to set
	 */
	public void setExcludeContries(boolean excludeContries) {
		this.excludeContries = excludeContries;
	}
	/**
	 * @return the states
	 */
	public ArrayList<String> getStates() {
		return states;
	}
	/**
	 * @param states the states to set
	 */
	public void setStates(ArrayList<String> states) {
		this.states = states;
	}
	/**
	 * @return the excludeStates
	 */
	public boolean isExcludeStates() {
		return excludeStates;
	}
	/**
	 * @param excludeStates the excludeStates to set
	 */
	public void setExcludeStates(boolean excludeStates) {
		this.excludeStates = excludeStates;
	}
	/**
	 * @return the cities
	 */
	public ArrayList<String> getCities() {
		return cities;
	}
	/**
	 * @param cities the cities to set
	 */
	public void setCities(ArrayList<String> cities) {
		this.cities = cities;
	}
	/**
	 * @return the excludeCities
	 */
	public boolean isExcludeCities() {
		return excludeCities;
	}
	/**
	 * @param excludeCities the excludeCities to set
	 */
	public void setExcludeCities(boolean excludeCities) {
		this.excludeCities = excludeCities;
	}
	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}
	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}
	/**
	 * @return the languages
	 */
	public ArrayList<String> getLanguages() {
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(ArrayList<String> languages) {
		this.languages = languages;
	}
	
	

}
