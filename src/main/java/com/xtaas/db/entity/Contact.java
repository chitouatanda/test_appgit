package com.xtaas.db.entity;

import java.util.ArrayList;

import com.amazonaws.util.StringUtils;

public class Contact {
	private String type;
	private String status;
	private String salutation;
	private String firstName;
	private String lastName;
	private String title;
	private Address address;
	private ArrayList<Phone> phones;
	private String email;
	
	public Contact() {
		
	}
	
	public Contact(String type, String status, String salutation,
			String firstName, String lastName, String title, Address address,
			ArrayList<Phone> phones, String email) {
		this.type = type;
		this.status = status;
		this.salutation = salutation;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.address = address;
		this.setPhones(phones);
		this.email = email;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ArrayList<Phone> getPhones() {
		return phones;
	}
	public void setPhones(ArrayList<Phone> phones) {
		if (phones.size() == 0 || StringUtils.isNullOrEmpty(phones.get(0).getNumber())) {
			throw new IllegalArgumentException("Atleast primary phone number is required");
		}
		this.phones = phones;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
