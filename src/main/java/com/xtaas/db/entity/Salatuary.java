package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.AbstractEntity;
import com.xtaas.valueobjects.KeyValuePair;


@Document(collection="salatuary")
public class Salatuary extends AbstractEntity {
	private String emailDomain;
	private String emailAddres;
	private String firstName;
	private String lastName;
	private String jobTitle;
	private String jobLevel;
	private String jobFunction;
	private String phone1;
	private String phone2;
	private String orgName;
	private String orgDomain;
	private String revenueRange;
	private String employeeCountRange;
	private String sicCode;
	private String sicDescription;
	private String address1_line1;
	private String address1_city;
	private String address1_state;
	private String address1_postal;
	private String address2_line1;
	private String address2_city;
	private String address2_state;
	private String address2_postal;
	private String LinkedIn_url;
	private String NAICS;
	private String NAICS_description;
	private String phoneC;
	private String phoneC_dnc;
	private String emailDisposition;
	public String getEmailDomain() {
		return emailDomain;
	}
	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}
	public String getEmailAddres() {
		return emailAddres;
	}
	public void setEmailAddres(String emailAddres) {
		this.emailAddres = emailAddres;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}
	public String getJobFunction() {
		return jobFunction;
	}
	public void setJobFunction(String jobFunction) {
		this.jobFunction = jobFunction;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgDomain() {
		return orgDomain;
	}
	public void setOrgDomain(String orgDomain) {
		this.orgDomain = orgDomain;
	}
	public String getRevenueRange() {
		return revenueRange;
	}
	public void setRevenueRange(String revenueRange) {
		this.revenueRange = revenueRange;
	}
	public String getEmployeeCountRange() {
		return employeeCountRange;
	}
	public void setEmployeeCountRange(String employeeCountRange) {
		this.employeeCountRange = employeeCountRange;
	}
	public String getSicCode() {
		return sicCode;
	}
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}
	public String getSicDescription() {
		return sicDescription;
	}
	public void setSicDescription(String sicDescription) {
		this.sicDescription = sicDescription;
	}
	public String getAddress1_line1() {
		return address1_line1;
	}
	public void setAddress1_line1(String address1_line1) {
		this.address1_line1 = address1_line1;
	}
	public String getAddress1_city() {
		return address1_city;
	}
	public void setAddress1_city(String address1_city) {
		this.address1_city = address1_city;
	}
	public String getAddress1_state() {
		return address1_state;
	}
	public void setAddress1_state(String address1_state) {
		this.address1_state = address1_state;
	}
	public String getAddress1_postal() {
		return address1_postal;
	}
	public void setAddress1_postal(String address1_postal) {
		this.address1_postal = address1_postal;
	}
	public String getAddress2_line1() {
		return address2_line1;
	}
	public void setAddress2_line1(String address2_line1) {
		this.address2_line1 = address2_line1;
	}
	public String getAddress2_city() {
		return address2_city;
	}
	public void setAddress2_city(String address2_city) {
		this.address2_city = address2_city;
	}
	public String getAddress2_state() {
		return address2_state;
	}
	public void setAddress2_state(String address2_state) {
		this.address2_state = address2_state;
	}
	public String getAddress2_postal() {
		return address2_postal;
	}
	public void setAddress2_postal(String address2_postal) {
		this.address2_postal = address2_postal;
	}
	public String getLinkedIn_url() {
		return LinkedIn_url;
	}
	public void setLinkedIn_url(String linkedIn_url) {
		LinkedIn_url = linkedIn_url;
	}
	public String getNAICS() {
		return NAICS;
	}
	public void setNAICS(String nAICS) {
		NAICS = nAICS;
	}
	public String getNAICS_description() {
		return NAICS_description;
	}
	public void setNAICS_description(String nAICS_description) {
		NAICS_description = nAICS_description;
	}
	public String getPhoneC() {
		return phoneC;
	}
	public void setPhoneC(String phoneC) {
		this.phoneC = phoneC;
	}
	public String getPhoneC_dnc() {
		return phoneC_dnc;
	}
	public void setPhoneC_dnc(String phoneC_dnc) {
		this.phoneC_dnc = phoneC_dnc;
	}

	public String getEmailDisposition() {
		return emailDisposition;
	}

	public void setEmailDisposition(String emailDisposition) {
		this.emailDisposition = emailDisposition;
	}
}
