package com.xtaas.db.entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/10/14
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class IdList {
  private String type;
  private List<String> ids;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<String> getIds() {
    return ids;
  }

  public void setIds(List<String> ids) {
    this.ids = ids;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("IdList{");
    sb.append("type='").append(type).append('\'');
    sb.append(", ids=").append(ids);
    sb.append('}');
    return sb.toString();
  }
}
