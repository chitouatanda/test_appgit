package com.xtaas.db.entity;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xtaas.domain.valueobject.LoginStatus;
import com.xtaas.infra.security.Roles;
import com.xtaas.utils.EncryptionUtils;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * Entity bean to represent Login document in DB.
 * User: hashimca
 * Date: 3/10/14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Document
public class Login extends AbstractEntity{
	@NotNull
	@NotEmpty
	private String password;

	private LoginStatus status;
	
	@NotNull
	@NotEmpty
	private List<String> roles;
	
	private List<IdList> allowed;
	private List<IdList> denied;
	
    // Name of the Organization to which this user belongs
    private String organization;
    
    //profile pic url
    private String profilePicUrl;
    
    @NotNull
    @NotEmpty
    private String firstName;
    
    @NotNull
    @NotEmpty
    private String lastName;
    
    @NotNull
    @NotEmpty
    private String email;

    private String adminRole;
    
    private List<String> reportEmails;
    
	private boolean dialerSetting;
	
	private boolean networkStatus;
	
	private boolean showManageAgent;
	
	private int noOfAttempt; 
	
	private boolean userLock;
	
	private Date lastPasswordUpdated;
	
	private boolean activateUser;
	
	private Set<String> recentPasswords;
	
	private boolean activateMFAUser;
	
	private String multiFactorKey;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getName() {
    	String name = this.firstName;
    	if (this.lastName != null) {
    		name = name + " " + this.lastName;
    	}
    	return name;
    }

	public String getUsername() {
		return getId();
	}

	public void setUsername(String username) {
		if (username == null || username.isEmpty()) {
			throw new IllegalArgumentException("Username is required");
		}
		setId(username.toLowerCase());
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<IdList> getAllowed() {
		return allowed;
	}

	public void setAllowed(List<IdList> allowed) {
		this.allowed = allowed;
	}

	public List<IdList> getDenied() {
		return denied;
	}

	public void setDenied(List<IdList> denied) {
		this.denied = denied;
	}

	public LoginStatus getStatus() {
		return status;
	}

	public void setStatus(LoginStatus status) {
		this.status = status;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		if (organization == null || organization.isEmpty()) {
			boolean isAgentRoleAvailable = false;
			if (roles.contains(Roles.ROLE_AGENT.toString())) {
				isAgentRoleAvailable = true;
			}
			if (!isAgentRoleAvailable) {
				throw new IllegalArgumentException("Organization is required");
			}
		} else {
			this.organization = organization.toUpperCase();
		}
	}

	public String getPasswordHash(String newPassword) {
		StringBuilder passwordWithSalt = new StringBuilder(newPassword).append("{").append(this.getId()).append("}");
		return EncryptionUtils.generateMd5Hash(passwordWithSalt.toString());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Login [id=");
		builder.append(getId());
		builder.append(", status=");
		builder.append(status);
		builder.append(", roles=");
		builder.append(roles);
		builder.append(", organization=");
		builder.append(organization);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the profilePicUrl
	 */
	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	/**
	 * @param profilePicUrl the profilePicUrl to set
	 */
	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	public List<String> getReportEmails() {
		return reportEmails;
	}

	public void setReportEmails(List<String> reportEmails) {
		this.reportEmails = reportEmails;
	}

	public boolean isDialerSetting() {
		return dialerSetting;
	}

	public void setDialerSetting(boolean dialerSetting) {
		this.dialerSetting = dialerSetting;
	}

	public boolean isNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(boolean networkStatus) {
		this.networkStatus = networkStatus;
	}

	public boolean isShowManageAgent() {
		return showManageAgent;
	}

	public void setShowManageAgent(boolean showManageAgent) {
		this.showManageAgent = showManageAgent;
	}
	
	public int getNoOfAttempt() {
		return noOfAttempt;
	}

	public void setNoOfAttempt(int noOfAttempt) {
		this.noOfAttempt = noOfAttempt;
	}

	public boolean isUserLock() {
		return userLock;
	}

	public void setUserLock(boolean userLock) {
		this.userLock = userLock;
	}

	public Date getLastPasswordUpdated() {
		return lastPasswordUpdated;
	}

	public void setLastPasswordUpdated(Date lastPasswordUpdated) {
		this.lastPasswordUpdated = lastPasswordUpdated;
	}

	public boolean isActivateUser() {
		return activateUser;
	}

	public void setActivateUser(boolean activateUser) {
		this.activateUser = activateUser;
	}

	public Set<String> getRecentPasswords() {
		return recentPasswords;
	}

	public void setRecentPasswords(Set<String> recentPasswords) {
		this.recentPasswords = recentPasswords;
	}

	public boolean isActivateMFAUser() {
		return activateMFAUser;
	}

	public void setActivateMFAUser(boolean activateMFAUser) {
		this.activateMFAUser = activateMFAUser;
	}

	public String getMultiFactorKey() {
		return multiFactorKey;
	}

	public void setMultiFactorKey(String multiFactorKey) {
		this.multiFactorKey = multiFactorKey;
	}
}
