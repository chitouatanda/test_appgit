/**
 * 
 */
package com.xtaas.db.entity;

import java.util.Date;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.QaFeedback;

/**
 * @author hbaba
 *
 */
@Document(collection="reviewdatabought")
public class ReviewDataBought extends AbstractEntity {
	

	
	private String reason;
	
	private ProspectCall prospectCall;
	
	private String status;
	
	private Date callbackDate;

	private QaFeedback qaFeedback;
	
	private Map<String, String> companyAddress;
	
	private String dataSlice;
	
	private boolean researchData;
	
	private boolean prospectingData;

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	/**
	 * @return the prospectCall
	 */
	public ProspectCall getProspectCall() {
		return prospectCall;
	}

	/**
	 * @param prospectCall the prospectCall to set
	 */
	public void setProspectCall(ProspectCall prospectCall) {
		this.prospectCall = prospectCall;
	}

	/**
	 * @return the qaFeedback
	 */
	public QaFeedback getQaFeedback() {
		return qaFeedback;
	}

	/**
	 * @param qaFeedback the qaFeedback to set
	 */
	public void setQaFeedback(QaFeedback qaFeedback) {
		this.qaFeedback = qaFeedback;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the callbackDate
	 */
	public Date getCallbackDate() {
		return callbackDate;
	}

	/**
	 * @param callbackDate the callbackDate to set
	 */
	public void setCallbackDate(Date callbackDate) {
		this.callbackDate = callbackDate;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public boolean isResearchData() {
		return researchData;
	}

	public void setResearchData(boolean researchData) {
		this.researchData = researchData;
	}

	public boolean isProspectingData() {
		return prospectingData;
	}

	public void setProspectingData(boolean prospectingData) {
		this.prospectingData = prospectingData;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
