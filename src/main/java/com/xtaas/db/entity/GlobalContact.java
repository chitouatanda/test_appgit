package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "globalcontact")
public class GlobalContact extends AbstractEntity {

	private String source;
	private String sourceId;
	private String status;
	private String prefix;
	private String firstName;
	private String lastName;
	private String suffix;
	private String company;
	private String email;
	private String phone;
	private String companyPhone;
	private String timeZone;
	private String extension;
	private boolean directPhone;
	private boolean directClassificationML;
	private String title;
	private String department;
	private String managementLevel;
	private Double minRevenue;
	private Double maxRevenue;
	private Double minEmployeeCount;
	private Double maxEmployeeCount;
	private Double revCount;
	private Double revCountIn000s;
	private Double empCount;
	private String domain;
	private String organizationName;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String stateCode;
	private String country;
	private String zipCode;
	private String countryCode;
	private String postalCode;
	private String sourceCompanyId;
	private Map<String, String> companyAddress;
	private List<String> industries;
	private String industry;
	private boolean isEu;
	private String timezone;
	private List<String> companySIC;
	private List<String> companyNAICS;
	private String zoomCompanyUrl;
	private String zoomPersonUrl;
	private String globalContactId;
	private String prospectCallId;
	private String changeLog;
	private String organizationId;
	private Date lastUpdatedDate;
	private String inputDepartment;
	private boolean optedIn;// default false
	private List<String> topLevelIndustries;
	private boolean isEncrypted;
	private String prospectType;
	private String sourceType;
	private String phoneType;
	private boolean emailValidated;
	private String dataSource;
	private String linkedInURL;

	public GlobalContact toGlobalContact(ProspectCall prospectCall) {
		Prospect prospect = prospectCall.getProspect();
		this.source = prospect.getSource();
		this.linkedInURL = prospect.getLinkedInURL();
		this.phoneType = prospect.getPhoneType();
		this.dataSource = prospect.getDataSource();
		this.lastUpdatedDate = prospect.getLastUpdatedDate();
		this.emailValidated = prospect.isEmailValidated();
		this.prospectType = prospect.getProspectType();
		this.sourceType = prospect.getSourceType();
		this.isEncrypted = prospect.getIsEncrypted();
		this.optedIn = prospect.isOptedIn();
		this.topLevelIndustries = prospect.getTopLevelIndustries();
		this.zipCode = prospect.getZipCode();
		this.timeZone = prospect.getTimeZone();
		this.status = prospect.getStatus();
		this.inputDepartment = prospect.getInputDepartment();
		this.company = prospect.getCompany();
		this.lastUpdatedDate = prospect.getLastUpdatedDate();
		this.sourceId = prospect.getSourceId();
		this.prefix = prospect.getPrefix();
		this.firstName = prospect.getFirstName();
		this.lastName = prospect.getLastName();
		this.suffix = prospect.getSuffix();
		this.email = prospect.getEmail();
		this.phone = prospect.getPhone();
		this.companyPhone = prospect.getCompanyPhone();
		this.extension = prospect.getExtension();
		this.directPhone = prospect.isDirectPhone();
		this.title = prospect.getTitle();
		this.department = prospect.getDepartment();
		this.managementLevel = prospect.getManagementLevel();
		if (prospect.getCustomAttributes().get("minRevenue") != null) {
			this.minRevenue = new Double(prospect.getCustomAttributes().get("minRevenue").toString());
		} else {
			this.minRevenue = 0.0;
		}
		if (prospect.getCustomAttributes().get("maxRevenue") != null) {
			this.maxRevenue = new Double(prospect.getCustomAttributes().get("maxRevenue").toString());
		} else {
			this.maxRevenue = 0.0;
		}
		if (prospect.getCustomAttributes().get("minEmployeeCount") != null) {
			this.minEmployeeCount = new Double(prospect.getCustomAttributes().get("minEmployeeCount").toString());
		} else {
			this.minEmployeeCount = 0.0;
		}
		if (prospect.getCustomAttributes().get("maxEmployeeCount") != null) {
			this.maxEmployeeCount = new Double(prospect.getCustomAttributes().get("maxEmployeeCount").toString());
		} else {
			this.maxEmployeeCount = 0.0;
		}
		if (prospect.getCustomAttributes().get("revCount") != null) {
			this.revCount = new Double(prospect.getCustomAttributes().get("revCount").toString());
		} else {
			this.revCount = 0.0;
		}
		if (prospect.getCustomAttributes().get("revCountIn000s") != null) {
			this.revCountIn000s = new Double(prospect.getCustomAttributes().get("revCountIn000s").toString());
		} else {
			this.revCountIn000s = 0.0;
		}
		if (prospect.getCustomAttributes().get("empCount") != null) {
			this.empCount = new Double(prospect.getCustomAttributes().get("empCount").toString());
		} else {
			this.empCount = 0.0;
		}
		if (prospect.getCustomAttributes().get("domain") != null) {
			this.domain = (String) prospect.getCustomAttributes().get("domain");
		} else {
			this.domain = "";
		}
		this.organizationName = prospect.getCompany();
		this.addressLine1 = prospect.getAddressLine1();
		this.addressLine2 = prospect.getAddressLine2();
		this.city = prospect.getCity();
		this.state = prospect.getStateCode();
		this.stateCode = prospect.getStateCode();
		this.country = prospect.getCountry();
		this.countryCode = prospect.getCountry();
		this.postalCode = prospect.getZipCode();
		this.sourceCompanyId = prospect.getSourceCompanyId();
		this.companyAddress = prospect.getCompanyAddress();
		this.industries = prospect.getIndustryList();
		this.industry = prospect.getIndustry();
		this.isEu = prospect.getIsEu();
		this.timezone = prospect.getTimeZone();
		this.companySIC = prospect.getCompanySIC();
		this.companyNAICS = prospect.getCompanyNAICS();
		if (prospect.getZoomCompanyUrl() != null) {
			this.zoomCompanyUrl = prospect.getZoomCompanyUrl();
		}
		if (prospect.getZoomPersonUrl() != null) {
			this.zoomPersonUrl = prospect.getZoomPersonUrl();
		}
		this.status = "QUEUED";
		this.globalContactId = UUID.randomUUID().toString(); // set new UUID as globalContactId
		this.prospectCallId = prospectCall.getProspectCallId();
		this.changeLog = "QUEUED"; // set QUEUED for new contact
		this.organizationId = prospectCall.getOrganizationId();
		return this;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public boolean isDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(boolean directPhone) {
		this.directPhone = directPhone;
	}

	public boolean isDirectClassificationML() {
		return directClassificationML;
	}

	public void setDirectClassificationML(boolean directClassificationML) {
		this.directClassificationML = directClassificationML;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getManagementLevel() {
		return managementLevel;
	}

	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}

	public Double getMinRevenue() {
		return minRevenue;
	}

	public void setMinRevenue(Double minRevenue) {
		this.minRevenue = minRevenue;
	}

	public Double getMaxRevenue() {
		return maxRevenue;
	}

	public void setMaxRevenue(Double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	public Double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public void setMinEmployeeCount(Double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	public Double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public void setMaxEmployeeCount(Double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}

	public Double getRevCount() {
		return revCount;
	}

	public void setRevCount(Double revCount) {
		this.revCount = revCount;
	}

	public Double getRevCountIn000s() {
		return revCountIn000s;
	}

	public void setRevCountIn000s(Double revCountIn000s) {
		this.revCountIn000s = revCountIn000s;
	}

	public Double getEmpCount() {
		return empCount;
	}

	public void setEmpCount(Double empCount) {
		this.empCount = empCount;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getSourceCompanyId() {
		return sourceCompanyId;
	}

	public void setSourceCompanyId(String sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	public List<String> getIndustries() {
		return industries;
	}

	public void setIndustries(List<String> industries) {
		this.industries = industries;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public boolean isEu() {
		return isEu;
	}

	public void setEu(boolean isEu) {
		this.isEu = isEu;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public List<String> getCompanySIC() {
		return companySIC;
	}

	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}

	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}

	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGlobalContactId() {
		return globalContactId;
	}

	public void setGlobalContactId(String globalContactId) {
		this.globalContactId = globalContactId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	// ------------ Fluent api ------------
	public GlobalContact withId(String id) {
		setId(id);
		return this;
	}

	public GlobalContact withFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public GlobalContact withLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public GlobalContact withPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public GlobalContact withDirectPhone(boolean isDirect) {
		this.directPhone = isDirect;
		return this;
	}

	public GlobalContact withDirectClassificationML(boolean directClassificationML) {
		this.directClassificationML = directClassificationML;
		return this;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getInputDepartment() {
		return inputDepartment;
	}

	public void setInputDepartment(String inputDepartment) {
		this.inputDepartment = inputDepartment;
	}

	public boolean isOptedIn() {
		return optedIn;
	}

	public void setOptedIn(boolean optedIn) {
		this.optedIn = optedIn;
	}

	public List<String> getTopLevelIndustries() {
		return topLevelIndustries;
	}

	public void setTopLevelIndustries(List<String> topLevelIndustries) {
		this.topLevelIndustries = topLevelIndustries;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean encrypted) {
		isEncrypted = encrypted;
	}

	public String getProspectType() {
		return prospectType;
	}

	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public boolean isEmailValidated() {
		return emailValidated;
	}

	public void setEmailValidated(boolean emailValidated) {
		this.emailValidated = emailValidated;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getLinkedInURL() {
		return linkedInURL;
	}

	public void setLinkedInURL(String linkedInURL) {
		this.linkedInURL = linkedInURL;
	}
}
