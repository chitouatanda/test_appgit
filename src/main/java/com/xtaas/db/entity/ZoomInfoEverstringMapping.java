package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.web.dto.ZoomInfoEverstringKeyValueDTO;

@Document(collection = "zoominfoeverstringmapping")
public class ZoomInfoEverstringMapping extends AbstractEntity {

	private String attrubute;
	private List<ZoomInfoEverstringKeyValueDTO> mapping;

	public List<ZoomInfoEverstringKeyValueDTO> getMapping() {
		return mapping;
	}

	public void setMapping(List<ZoomInfoEverstringKeyValueDTO> mapping) {
		this.mapping = mapping;
	}

	public String getAttrubute() {
		return attrubute;
	}

	public void setAttrubute(String attrubute) {
		this.attrubute = attrubute;
	}

}
