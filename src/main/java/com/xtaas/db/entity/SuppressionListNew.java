package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * SuppressionList
 */
@Document(collection = "suppressionlistnew")
public class SuppressionListNew extends AbstractEntity {

	private String campaignId;
	private String domain;
	private String company;
	private String emailId;
	private String status;
	private String organizationId;
	private String companyName;

	/**
	 * Used to clone the campaign. When you add any new field in the
	 * SuppressionListNew then set it here as well
	 * 
	 * @param suppressionListNew
	 * @param campaignId
	 * @return
	 */
	public SuppressionListNew getSuppressionListNewClone(SuppressionListNew suppressionListNew, String campaignId) {
		this.setCampaignId(campaignId);
		this.setDomains(suppressionListNew.getDomains());
		this.setCompany(suppressionListNew.getCompany());
		this.setEmailId(suppressionListNew.getEmailId());
		this.setStatus(suppressionListNew.getStatus());
		this.setOrganizationId(suppressionListNew.getOrganizationId());
		this.setCompanyName(suppressionListNew.getCompanyName());
		return this;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getDomains() {
		return domain;
	}

	public void setDomains(String domain) {
		this.domain = domain;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
