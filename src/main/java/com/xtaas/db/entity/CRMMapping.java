package com.xtaas.db.entity;

import java.util.ArrayList;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "crmmapping")
public class CRMMapping extends AbstractEntity {
	private String parent;
	private String ownerId;
	private ArrayList<CRMAttribute> attributeMapping;
	private boolean inherit;

	/**
	 * @return the name of parent mapping
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @return the attributeMapping
	 */
	public ArrayList<CRMAttribute> getAttributeMapping() {
		return attributeMapping;
	}

	/**
	 * @param attributeMapping
	 *            the attributeMapping to set
	 */
	public void setAttributeMapping(ArrayList<CRMAttribute> attributeMapping) {
		this.attributeMapping = attributeMapping;
	}

	/**
	 * @return the inherit
	 */
	public boolean isInherit() {
		return inherit;
	}

	/**
	 * @param inherit the inherit to set
	 */
	public void setInherit(boolean inherit) {
		this.inherit = inherit;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
}