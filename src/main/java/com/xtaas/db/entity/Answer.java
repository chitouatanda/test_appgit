package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 5/11/14
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class Answer {
    private String questionid;
    private String answer;
    private String source;
    private int confidence;

    @Override
    public String toString() {
        return "Answer{" +
          "questionid='" + questionid + '\'' +
          ", answer='" + answer + '\'' +
          ", source='" + source + '\'' +
          ", confidence=" + confidence +
          "} " + super.toString();
    }

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }
}
