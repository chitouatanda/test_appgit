package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "countrynew")
public class CountryNew extends AbstractEntity {

	private String country; // country name
	private String continent;
	private String fetchUri;
	private String longitude;
	private String geonameid;
	private String population;
	private String countryCode;
	private String toponymName;
	private String latitude;
	private boolean countryRecord;
	private String stateName;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getFetchUri() {
		return fetchUri;
	}

	public void setFetchUri(String fetchUri) {
		this.fetchUri = fetchUri;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getGeonameid() {
		return geonameid;
	}

	public void setGeonameid(String geonameid) {
		this.geonameid = geonameid;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getToponymName() {
		return toponymName;
	}

	public void setToponymName(String toponymName) {
		this.toponymName = toponymName;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public boolean isCountryRecord() {
		return countryRecord;
	}

	public void setCountryRecord(boolean countryRecord) {
		this.countryRecord = countryRecord;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Override
	public String toString() {
		return "CountryNew [country=" + country + ", continent=" + continent + ", fetchUri=" + fetchUri + ", longitude="
				+ longitude + ", geonameid=" + geonameid + ", population=" + population + ", countryCode=" + countryCode
				+ ", toponymName=" + toponymName + ", latitude=" + latitude + ", countryRecord=" + countryRecord
				+ ", stateName=" + stateName + "]";
	}

}
