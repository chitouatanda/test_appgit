package com.xtaas.db.entity;

public class TeamPrice {
	private double generation;
	private double verification;
	private double qualification;
	private double highQualification;
	/**
	 * @return the generation
	 */
	public double getGeneration() {
		return generation;
	}
	/**
	 * @param generation the generation to set
	 */
	public void setGeneration(double generation) {
		this.generation = generation;
	}
	/**
	 * @return the verification
	 */
	public double getVerification() {
		return verification;
	}
	/**
	 * @param verification the verification to set
	 */
	public void setVerification(double verification) {
		this.verification = verification;
	}
	/**
	 * @return the qualification
	 */
	public double getQualification() {
		return qualification;
	}
	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(double qualification) {
		this.qualification = qualification;
	}
	/**
	 * @return the highQualification
	 */
	public double getHighQualification() {
		return highQualification;
	}
	/**
	 * @param highQualification the highQualification to set
	 */
	public void setHighQualification(double highQualification) {
		this.highQualification = highQualification;
	}
	
	
}
