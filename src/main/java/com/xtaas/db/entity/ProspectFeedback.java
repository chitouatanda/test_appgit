package com.xtaas.db.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Collection to store prospect feedback
 * 
 * @author pranay
 *
 */
@Document(collection = "prospectfeedback")
public class ProspectFeedback extends AbstractEntity {
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String title;
	private String company;
	private String source;
	private String dispositionStatus;
	private String subStatus;
	private String status;
	private String recordingUrl;
	private Date callStartTime;
	private String domain;
	private double maxEmployeeCount;
	private double minEmployeeCount;
	private double maxRevenue;
	private double minRevenue;
	private String campaignId;
	private String prospectCallLogId;

	public ProspectFeedback toProspectFeedback(ProspectCallLog prospectCallLog) {
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		Prospect prospect = prospectCall.getProspect();
		setFirstName(prospect.getFirstName());
		setLastName(prospect.getLastName());
		setPhone(prospect.getPhone());
		setEmail(prospect.getEmail());
		setTitle(prospect.getTitle());
		setCompany(prospect.getCompany());
		setSource(prospect.getSource());
		setDispositionStatus(prospectCall.getDispositionStatus());
		setSubStatus(prospectCall.getSubStatus());
		setStatus(prospectCallLog.getStatus().name());
		setRecordingUrl(prospectCall.getRecordingUrl());
		setCallStartTime(prospectCall.getCallStartTime());
		if (prospect.getCustomAttributes().get("minRevenue") != null) {
			setMinRevenue(new Double(prospect.getCustomAttributes().get("minRevenue").toString()));
		} else {
			setMinRevenue(0.0);
		}
		if (prospect.getCustomAttributes().get("maxRevenue") != null) {
			setMaxRevenue(new Double(prospect.getCustomAttributes().get("maxRevenue").toString()));
		} else {
			setMaxRevenue(0.0);
		}
		if (prospect.getCustomAttributes().get("minEmployeeCount") != null) {
			setMinEmployeeCount(new Double(prospect.getCustomAttributes().get("minEmployeeCount").toString()));
		} else {
			setMinEmployeeCount(0.0);
		}
		if (prospect.getCustomAttributes().get("maxEmployeeCount") != null) {
			setMaxEmployeeCount(new Double(prospect.getCustomAttributes().get("maxEmployeeCount").toString()));
		} else {
			setMaxEmployeeCount(0.0);
		}
		if (prospect.getCustomAttributes().get("domain") != null) {
			setDomain((String) prospect.getCustomAttributes().get("domain"));
		} else {
			setDomain("");
		}
		setCampaignId(prospectCall.getCampaignId());
		setProspectCallLogId(prospectCallLog.getId());
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public Date getCallStartTime() {
		return callStartTime;
	}

	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public void setMaxEmployeeCount(double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}

	public double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public void setMinEmployeeCount(double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	public double getMaxRevenue() {
		return maxRevenue;
	}

	public void setMaxRevenue(double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	public double getMinRevenue() {
		return minRevenue;
	}

	public void setMinRevenue(double minRevenue) {
		this.minRevenue = minRevenue;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getProspectCallLogId() {
		return prospectCallLogId;
	}

	public void setProspectCallLogId(String prospectCallLogId) {
		this.prospectCallLogId = prospectCallLogId;
	}

	@Override
	public String toString() {
		return "ProspectFeedback [firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + ", email="
				+ email + ", title=" + title + ", company=" + company + ", source=" + source + ", dispositionStatus="
				+ dispositionStatus + ", subStatus=" + subStatus + ", status=" + status + ", recordingUrl="
				+ recordingUrl + ", callStartTime=" + callStartTime + ", domain=" + domain + ", maxEmployeeCount="
				+ maxEmployeeCount + ", minEmployeeCount=" + minEmployeeCount + ", maxRevenue=" + maxRevenue
				+ ", minRevenue=" + minRevenue + ", campaignId=" + campaignId + ", prospectCallLogId="
				+ prospectCallLogId + "]";
	}

}
