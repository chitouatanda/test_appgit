package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection="enrichsourcepriority")
public class EnrichSourcePriority extends AbstractEntity {

    private String source;
    private boolean upload;
    private boolean domainEnrich;
    private boolean companyEnrich;
    private boolean qualityBucket;

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isDomainEnrich() {
        return domainEnrich;
    }

    public void setDomainEnrich(boolean domainEnrich) {
        this.domainEnrich = domainEnrich;
    }

    public boolean isCompanyEnrich() {
        return companyEnrich;
    }

    public void setCompanyEnrich(boolean companyEnrich) {
        this.companyEnrich = companyEnrich;
    }

    public boolean isQualityBucket() {
        return qualityBucket;
    }

    public void setQualityBucket(boolean qualityBucket) {
        this.qualityBucket = qualityBucket;
    }
}
