package com.xtaas.db.entity;

public class FeedbackResponseAttribute {
	private String attribute;
	private String attributeComment;//free form text
	private String feedback;//stores YES, NO, N/A
	private String rejectionReason; 
	private Integer score;//for YES feedback, score is its attribute weight
						 //for NO feedback, score is 0
						//for NA feedback, score is -1
	
	private String noReason;
	
	
	public String getAttributeComment() {
		return attributeComment;
	}

	public void setAttributeComment(String attributeComment) {
		this.attributeComment = attributeComment;
	}

	public String getAttribute() {
		return attribute;
	}
	
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
		
	public String getFeedback() {
		return feedback;
	}
	
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	
	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

    public String getNoReason() {
        return noReason;
    }

    public void setNoReason(String noReason) {
        this.noReason = noReason;
    }

	
}
