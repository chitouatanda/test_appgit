package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "country")
public class Country extends AbstractEntity {

	private String country;
	private String continent;
	private String countryCode;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
