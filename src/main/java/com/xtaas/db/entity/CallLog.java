package com.xtaas.db.entity;

import java.util.HashMap;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "calllog")
public class CallLog extends AbstractEntity {

	// key value pair for all the request parameters sent by TWILIO / PLIVO
	private HashMap<String, String> callLogMap;

	public HashMap<String, String> getCallLogMap() {
		return callLogMap;
	}

	public void setCallLogMap(HashMap<String, String> callLogMap) {
		this.callLogMap = callLogMap;
		if (callLogMap.get("CallSid") != null) {
			// TWILIO - has CallSid so use it as "_id"
			this.setId(callLogMap.get("CallSid"));
		} else {
			// PLIVO - has CallUUID so use it as "_id"
			this.setId(callLogMap.get("CallUUID"));
		}
	}

}
