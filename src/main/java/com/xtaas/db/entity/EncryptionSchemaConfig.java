package com.xtaas.db.entity;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "encryptionschemaconfig")
public class EncryptionSchemaConfig {

	private String collectionName;
	private List<String> fieldsTobeEncrypted;
	private boolean active;

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public List<String> getFieldsTobeEncrypted() {
		return fieldsTobeEncrypted;
	}

	public void setFieldsTobeEncrypted(List<String> fieldsTobeEncrypted) {
		this.fieldsTobeEncrypted = fieldsTobeEncrypted;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "EncryptionSchemaConfig [collectionName=" + collectionName + ", fieldsTobeEncrypted="
				+ fieldsTobeEncrypted + ", active=" + active + "]";
	}

}
