/**
 * 
 */
package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author djain
 *
 */
@Document(collection="loginactivitylog")
public class LoginActivityLog extends AbstractEntity {
	public enum LoginActivityType {
		LOGIN, LOGOUT;
	}
	
	private String userId;
	private LoginActivityType activityType;
	
	public LoginActivityLog(String userId, LoginActivityType activityType) {
		setUserId(userId);
		setActivityType(activityType);
	}
	
	public LoginActivityType getActivityType() {
		return activityType;
	}

	/**
	 * @param action the action to set
	 */
	private void setActivityType(LoginActivityType activityType) {
		if (activityType == null) {
			throw new IllegalArgumentException("Activity Type is required");
		}
		this.activityType = activityType;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		if (userId == null || userId.isEmpty()) {
			throw new IllegalArgumentException("User Id is required");
		}
		this.userId = userId;
	}
}