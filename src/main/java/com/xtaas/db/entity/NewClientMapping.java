package com.xtaas.db.entity;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "newclientmapping")
public class NewClientMapping {

	private String id;
	private String clientName;
	private String campaignId;
	private int sequence;
	private String colHeader;
	private String stdAttribute;
	private List<String> clientValues;
	private boolean internalUse;

	public NewClientMapping() {

	}

	/**
	 * Used to clone the campaign. When you add any new field in the
	 * NewClientMapping then set it here as well
	 * 
	 * @param mapping
	 * @param campaignId
	 * @return
	 */
	public NewClientMapping getNewClientMappingClone(NewClientMapping mapping, String campaignId) {
		this.setClientName(mapping.getClientName());
		this.setCampaignId(campaignId);
		this.setSequence(mapping.getSequence());
		this.setColHeader(mapping.getColHeader());
		this.setStdAttribute(mapping.getStdAttribute());
		this.setClientValues(mapping.getClientValues());
		this.setInternalUse(mapping.isInternalUse());
		return this;
	}

	public String getId() {
		return id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getColHeader() {
		return colHeader;
	}

	public void setColHeader(String colHeader) {
		this.colHeader = colHeader;
	}

	public String getStdAttribute() {
		return stdAttribute;
	}

	public void setStdAttribute(String stdAttribute) {
		this.stdAttribute = stdAttribute;
	}

	public List<String> getClientValues() {
		return clientValues;
	}

	public void setClientValues(List<String> clientValues) {
		this.clientValues = clientValues;
	}

	public boolean isInternalUse() {
		return internalUse;
	}

	public void setInternalUse(boolean internalUse) {
		this.internalUse = internalUse;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "NewClientMapping [id=" + id + ", clientName=" + clientName + ", campaignId=" + campaignId
				+ ", sequence=" + sequence + ", colHeader=" + colHeader + ", stdAttribute=" + stdAttribute
				+ ", clientValues=" + clientValues + "]";
	}

}
