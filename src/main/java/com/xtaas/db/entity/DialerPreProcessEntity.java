/**
 * 
 */
package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author djain
 *
 */
@Document(collection="dialer_preprocess_queue")
public class DialerPreProcessEntity extends AbstractEntity {
	
	private ProspectCall prospectCall;

	/**
	 * @return the prospect
	 */
	public ProspectCall getProspectCall() {
		return prospectCall;
	}

	/**
	 * @param prospect the prospect to set
	 */
	public void setProspectCall(ProspectCall prospectCall) {
		this.prospectCall = prospectCall;
	}
}