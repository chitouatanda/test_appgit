package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;
import com.xtaas.web.dto.IndirectProspectsCacheDTO;
import com.xtaas.web.dto.ProspectCacheDTO;
import com.xtaas.web.dto.ProspectCallCacheDTO;

@Document(collection = "analyzeprospectcache")
public class AnalyzeProspectCache {

	private String prospectCallId;

	private ProspectCacheDTO prospect;

	private String twilioCallSid;

	private String campaignId;

	private String outboundNumber;

	private int callRetryCount;

	private Long dailyCallRetryCount;

	private String dataSlice;

	private List<IndirectProspectsCacheDTO> indirectProspects;

	private Date createdDate;

	private Date updatedDate;

	private int qualityBucketSort;

	public AnalyzeProspectCache() {
	}

	public AnalyzeProspectCache(ProspectCallCacheDTO prospectCallCacheDTO) {
		this.prospectCallId = prospectCallCacheDTO.getProspectCallId();
		this.prospect = prospectCallCacheDTO.getProspect();
		this.twilioCallSid = prospectCallCacheDTO.getTwilioCallSid();
		this.campaignId = prospectCallCacheDTO.getCampaignId();
		this.outboundNumber = prospectCallCacheDTO.getOutboundNumber();
		this.callRetryCount = prospectCallCacheDTO.getCallRetryCount();
		this.dailyCallRetryCount = prospectCallCacheDTO.getDailyCallRetryCount();
		this.dataSlice = prospectCallCacheDTO.getDataSlice();
		this.indirectProspects = prospectCallCacheDTO.getIndirectProspects();
		this.createdDate = prospectCallCacheDTO.getCreatedDate();
		this.updatedDate = prospectCallCacheDTO.getUpdatedDate();
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public ProspectCacheDTO getProspect() {
		return prospect;
	}

	public void setProspect(ProspectCacheDTO prospect) {
		this.prospect = prospect;
	}

	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public List<IndirectProspectsCacheDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsCacheDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + callRetryCount;
		result = prime * result + ((campaignId == null) ? 0 : campaignId.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((dailyCallRetryCount == null) ? 0 : dailyCallRetryCount.hashCode());
		result = prime * result + ((dataSlice == null) ? 0 : dataSlice.hashCode());
		result = prime * result + ((indirectProspects == null) ? 0 : indirectProspects.hashCode());
		result = prime * result + ((outboundNumber == null) ? 0 : outboundNumber.hashCode());
		result = prime * result + ((prospect == null) ? 0 : prospect.hashCode());
		result = prime * result + ((prospectCallId == null) ? 0 : prospectCallId.hashCode());
		result = prime * result + qualityBucketSort;
		result = prime * result + ((twilioCallSid == null) ? 0 : twilioCallSid.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnalyzeProspectCache other = (AnalyzeProspectCache) obj;
		if (callRetryCount != other.callRetryCount)
			return false;
		if (campaignId == null) {
			if (other.campaignId != null)
				return false;
		} else if (!campaignId.equals(other.campaignId))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (dailyCallRetryCount == null) {
			if (other.dailyCallRetryCount != null)
				return false;
		} else if (!dailyCallRetryCount.equals(other.dailyCallRetryCount))
			return false;
		if (dataSlice == null) {
			if (other.dataSlice != null)
				return false;
		} else if (!dataSlice.equals(other.dataSlice))
			return false;
		if (indirectProspects == null) {
			if (other.indirectProspects != null)
				return false;
		} else if (!indirectProspects.equals(other.indirectProspects))
			return false;
		if (outboundNumber == null) {
			if (other.outboundNumber != null)
				return false;
		} else if (!outboundNumber.equals(other.outboundNumber))
			return false;
		if (prospect == null) {
			if (other.prospect != null)
				return false;
		} else if (!prospect.equals(other.prospect))
			return false;
		if (prospectCallId == null) {
			if (other.prospectCallId != null)
				return false;
		} else if (!prospectCallId.equals(other.prospectCallId))
			return false;
		if (qualityBucketSort != other.qualityBucketSort)
			return false;
		if (twilioCallSid == null) {
			if (other.twilioCallSid != null)
				return false;
		} else if (!twilioCallSid.equals(other.twilioCallSid))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnalyzeProspectCache [prospectCallId=" + prospectCallId + ", prospect=" + prospect + ", twilioCallSid="
				+ twilioCallSid + ", campaignId=" + campaignId + ", outboundNumber=" + outboundNumber
				+ ", callRetryCount=" + callRetryCount + ", dailyCallRetryCount=" + dailyCallRetryCount + ", dataSlice="
				+ dataSlice + ", indirectProspects=" + indirectProspects + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + ", qualityBucketSort=" + qualityBucketSort + "]";
	}

}
