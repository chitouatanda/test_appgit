/**
 * 
 */
package com.xtaas.db.entity;

import java.util.HashMap;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author djain
 *
 */
@Document(collection="callabandonoptoutrequest")
public class CallAbandonOptOutRequest extends AbstractEntity {

	private HashMap<String, String> callAbandonOptOutRequestMap; //key value pair for all the request parameters sent by Twilio for Opt Out requests

	/**
	 * @return the callAbandonOptOutRequestMap
	 */
	public HashMap<String, String> getCallAbandonOptOutRequestMap() {
		return callAbandonOptOutRequestMap;
	}

	/**
	 * @param callAbandonOptOutRequestMap the callAbandonOptOutRequestMap to set
	 */
	public void setCallAbandonOptOutRequestMap(
			HashMap<String, String> callAbandonOptOutRequestMap) {
		this.callAbandonOptOutRequestMap = callAbandonOptOutRequestMap;
		this.setId(callAbandonOptOutRequestMap.get("CallSid")); //using CallSid as _Id
	}
}
