/**
 * 
 */
package com.xtaas.db.entity;

import java.util.HashMap;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author djain
 *
 */
@Document(collection="agentcalllog")
public class AgentCallLog extends AbstractEntity {
	private String agentId; //Agent UserName
	private String callSid;
	private AgentProspectLog prospectLog ;
	private String campaignId;
	private String closingNote;
	
	/**
	 * An inner class for logging Prospect details by Agent. 
	 * This class holds the fields collected by Agent for a Prospect and can be persisted.
	 * @author djain
	 *
	 */
	public class AgentProspectLog {
		private String prospectId;
		private String phone;
		private boolean qualified;
		private HashMap<String, String> qualificationAttributeNameValueMap;
		private HashMap<String, String> informationAttributeNameValueMap;
		
		/**
		 * @return the prospectId
		 */
		public String getProspectId() {
			return prospectId;
		}
		/**
		 * @param prospectId the prospectId to set
		 */
		public void setProspectId(String prospectId) {
			this.prospectId = prospectId;
		}
		/**
		 * @return the phone
		 */
		public String getPhone() {
			return phone;
		}
		/**
		 * @param phone the phone to set
		 */
		public void setPhone(String phone) {
			this.phone = phone;
		}
		
		/**
		 * @return the qualified
		 */
		public boolean isQualified() {
			return qualified;
		}
		/**
		 * @param qualified the qualified to set
		 */
		public void setQualified(boolean qualified) {
			this.qualified = qualified;
		}
		/**
		 * @return the qualificationAttributeNameValueMap
		 */
		public HashMap<String, String> getQualificationAttributeNameValueMap() {
			return qualificationAttributeNameValueMap;
		}
		/**
		 * @param qualificationAttributeNameValueMap the qualificationAttributeNameValueMap to set
		 */
		public void setQualificationAttributeNameValueMap(
				HashMap<String, String> qualificationAttributeNameValueMap) {
			this.qualificationAttributeNameValueMap = qualificationAttributeNameValueMap;
		}
		/**
		 * @return the informationAttributeNameValueMap
		 */
		public HashMap<String, String> getInformationAttributeNameValueMap() {
			return informationAttributeNameValueMap;
		}
		/**
		 * @param informationAttributeNameValueMap the informationAttributeNameValueMap to set
		 */
		public void setInformationAttributeNameValueMap(
				HashMap<String, String> informationAttributeNameValueMap) {
			this.informationAttributeNameValueMap = informationAttributeNameValueMap;
		}
	}
	
	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}
	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	/**
	 * @return the callSid
	 */
	public String getCallSid() {
		return callSid;
	}
	/**
	 * @param callSid the callSid to set
	 */
	public void setCallSid(String callSid) {
		this.callSid = callSid;
		this.setId(callSid); //using CallSid as _Id
	}
	/**
	 * @return the prospectLog
	 */
	public AgentProspectLog getProspectLog() {
		return prospectLog;
	}
	/**
	 * @param prospectLog the prospectLog to set
	 */
	public void setProspectLog(AgentProspectLog prospectLog) {
		this.prospectLog = prospectLog;
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the closingNote
	 */
	public String getClosingNote() {
		return closingNote;
	}
	/**
	 * @param closingNote the closingNote to set
	 */
	public void setClosingNote(String closingNote) {
		this.closingNote = closingNote;
	}
}

