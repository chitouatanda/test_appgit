package com.xtaas.db.entity;

import java.util.ArrayList;

public class State {
	private String stateCode;
	private String stateName;
	private ArrayList<String> cities;
	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @return the cities
	 */
	public ArrayList<String> getCities() {
		return cities;
	}
	/**
	 * @param cities the cities to set
	 */
	public void setCities(ArrayList<String> cities) {
		this.cities = cities;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "State [stateCode=" + stateCode + ", stateName=" + stateName
				+ ", cities=" + cities + "]";
	}
	
	
}
