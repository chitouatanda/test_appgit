package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Application level DNC List
 * 
 * @author hbaba
 *
 */
@Document(collection="prospectcallspeedlog")
public class ProspectCallSpeedLog extends AbstractEntity {
    private String campaignId;
    private String callSpeed;
    private String recordsAttempt;
    private String recordsContact;
    private String abandonedRatio;
    private String connectRatio;
    private String recordsAbandoned;
   
    public String getRecordsAbandoned() {
		return recordsAbandoned;
	}

	public void setRecordsAbandoned(String recordsAbandoned) {
		this.recordsAbandoned = recordsAbandoned;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getCallSpeed() {
		return callSpeed;
	}

	public void setCallSpeed(String callSpeed) {
		this.callSpeed = callSpeed;
	}

	public String getRecordsAttempt() {
		return recordsAttempt;
	}

	public void setRecordsAttempt(String recordsAttempt) {
		this.recordsAttempt = recordsAttempt;
	}

	public String getRecordsContact() {
		return recordsContact;
	}

	public void setRecordsContact(String recordsContact) {
		this.recordsContact = recordsContact;
	}

	public String getAbandonedRatio() {
		return abandonedRatio;
	}

	public void setAbandonedRatio(String abandonedRatio) {
		this.abandonedRatio = abandonedRatio;
	}

	public String getConnectRatio() {
		return connectRatio;
	}

	public void setConnectRatio(String connectRatio) {
		this.connectRatio = connectRatio;
	}

	@Override
    public String toString() {
        return "ProspectCallSpeedLog{" +
          "campaignId='" + campaignId + '\'' +
          ", callSpeed=" + callSpeed +
          ", recordsAttempt=" + recordsAttempt +
          ", recordsContact=" + recordsContact +
          ", abandonedRatio=" + abandonedRatio +
          ", connectRatio=" + connectRatio +
          "} " + super.toString();
    }

   
}
