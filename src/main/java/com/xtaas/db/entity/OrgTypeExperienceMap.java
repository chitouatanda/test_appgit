package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.ExperienceTypes;

@Document(collection="orgtypeexperiencemap")
public class OrgTypeExperienceMap {

    private String id;
    
    private ExperienceTypes experienceTypes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ExperienceTypes getExperienceTypes() {
        return experienceTypes;
    }

    public void setExperienceTypes(ExperienceTypes experienceTypes) {
        this.experienceTypes = experienceTypes;
    }

}
