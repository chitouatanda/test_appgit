package com.xtaas.db.entity;

import com.xtaas.domain.valueobject.QaFeedback;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

@Document(collection="prospectsalutary_Q3_2021")
public class ProspectSalutaryQ3 extends AbstractEntity{

    public static enum ProspectCallStatus {
        QUEUED, //Ready for calling
        LOCAL_DNC_ERROR, //Phone number could not pass local DNC check
        NATIONAL_DNC_ERROR, //Phone number could not pass National DNC check
        UNKNOWN_ERROR, //Runtime Error Occurred
        MAX_RETRY_LIMIT_REACHED, //Retried for max limit
        CALL_TIME_RESTRICTION, //TZ and Holiday restrictions did not pass. Callback again later
        CALL_NO_ANSWER, //Call not answered
        CALL_BUSY, //call busy
        CALL_CANCELED, //Call canceled
        CALL_FAILED, //Call Failed
        CALL_MACHINE_ANSWERED, //Call Answered by Machine
        CALL_ABANDONED, //Call has been Abandoned due to Agent's unavailability
        CALL_PREVIEW, //Only applicable for Preview Mode where Dialer has pushed the prospect to agent but call not yet initiated
        CALL_DIALED, //Call has been dialed but not yet picked up
        CALL_INPROGRESS, //Call in progress
        CALL_COMPLETE, //Conversation finished
        WRAPUP_COMPLETE, //Agent submitted the prospect details after filling in call information
        QA_INITIATED, //Prospect Call's QA initiated
        QA_COMPLETE, //Prospect Call QAed
        SECONDARY_QA_INITIATED,	// DATE : 05/06/2017	REASON : Prospect Call's Secondary QA initiated
        QA_WRAPUP_COMPLETE,
        CLIENT_DELIVERED,
        CLIENT_REJECTED,
        QA_REJECTED,
        PRIMARY_REVIEW,
        SECONDARY_REVIEW,
        INDIRECT_CALLING,
        SUCCESS_DUPLICATE
    }

    private ProspectCall prospectCall;

    private ProspectCallStatus status;

    private Date callbackDate;

    private QaFeedback qaFeedback;

    private Map<String, String> companyAddress;

    private String dataSlice;

    private boolean tagDirectIndirect;

    private String enrichedStatus;
    private String companyMaster;

    private boolean cleanupStatus;

    public Map<String, String> getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(Map<String, String> companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * @return the prospectCall
     */
    public ProspectCall getProspectCall() {
        return prospectCall;
    }

    /**
     * @param prospectCall the prospectCall to set
     */
    public void setProspectCall(ProspectCall prospectCall) {
        this.prospectCall = prospectCall;
    }

    /**
     * @return the qaFeedback
     */
    public QaFeedback getQaFeedback() {
        return qaFeedback;
    }

    /**
     * @param qaFeedback the qaFeedback to set
     */
    public void setQaFeedback(QaFeedback qaFeedback) {
        this.qaFeedback = qaFeedback;
    }

    /**
     * @return the status
     */
    public ProspectCallStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ProspectCallStatus status) {
        this.status = status;
    }

    /**
     * @return the callbackDate
     */
    public Date getCallbackDate() {
        return callbackDate;
    }

    /**
     * @param callbackDate the callbackDate to set
     */
    public void setCallbackDate(Date callbackDate) {
        this.callbackDate = callbackDate;
    }

    public String getDataSlice() {
        return dataSlice;
    }

    public void setDataSlice(String dataSlice) {
        this.dataSlice = dataSlice;
    }

    public boolean isTagDirectIndirect() {
        return tagDirectIndirect;
    }

    public void setTagDirectIndirect(boolean tagDirectIndirect) {
        this.tagDirectIndirect = tagDirectIndirect;
    }

    public String getEnrichedStatus() {
        return enrichedStatus;
    }

    public void setEnrichedStatus(String enrichedStatus) {
        this.enrichedStatus = enrichedStatus;
    }

    public String getCompanyMaster() {
        return companyMaster;
    }

    public void setCompanyMaster(String companyMaster) {
        this.companyMaster = companyMaster;
    }
}
