package com.xtaas.db.entity;

public class FeedbackFormAttribute {
	private String attribute;
	private String label;
	private String questionSkin;
	private Integer weight;
	private String description;
	
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getQuestionSkin() {
		return questionSkin;
	}

	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
