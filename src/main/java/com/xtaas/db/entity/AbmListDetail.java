package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.AbstractEntity;
import com.xtaas.infra.zoominfo.companysearch.valueobject.Hashtags;
import com.xtaas.infra.zoominfo.companysearch.valueobject.SimilarCompanies;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;


@Document(collection="abmlistdetail")
public class AbmListDetail extends AbstractEntity {
	
	//private List<KeyValuePair<String, String>> companyList;
	private String campaignId;
	private String status;
	private String companyName;
	private String companyId;
	private String companyWebsite;
    private String zoomCompanyUrl;
    private String companyDetailXmlUrl;
    private boolean isDefunct;
    private String phone;
    private CompanyAddress companyAddress;
    private String companyRevenue;
    private Long companyRevenueIn000s;
    private String companyRevenueRange;
    private Long companyEmployeeCount;
    private String companyEmployeeCountRange;
    private String companyType;
    private List<String> companySIC;
    private List<String> companyNAICS;
    private List<String> companyProductsAndServices;
    private Hashtags hashtags;
    private SimilarCompanies similarCompanies;
    private String companyTicker;
    private String companyDescription;
    private List<String> companyTopLevelIndustry;
    private List<String> companyIndustry;
    private List<String> companyRanking;
    
	
	

	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}
	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}
	public String getCompanyDetailXmlUrl() {
		return companyDetailXmlUrl;
	}
	public void setCompanyDetailXmlUrl(String companyDetailXmlUrl) {
		this.companyDetailXmlUrl = companyDetailXmlUrl;
	}
	public boolean isDefunct() {
		return isDefunct;
	}
	public void setDefunct(boolean isDefunct) {
		this.isDefunct = isDefunct;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public CompanyAddress getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(CompanyAddress companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyRevenue() {
		return companyRevenue;
	}
	public void setCompanyRevenue(String companyRevenue) {
		this.companyRevenue = companyRevenue;
	}
	public Long getCompanyRevenueIn000s() {
		return companyRevenueIn000s;
	}
	public void setCompanyRevenueIn000s(Long companyRevenueIn000s) {
		this.companyRevenueIn000s = companyRevenueIn000s;
	}
	public String getCompanyRevenueRange() {
		return companyRevenueRange;
	}
	public void setCompanyRevenueRange(String companyRevenueRange) {
		this.companyRevenueRange = companyRevenueRange;
	}
	public Long getCompanyEmployeeCount() {
		return companyEmployeeCount;
	}
	public void setCompanyEmployeeCount(Long companyEmployeeCount) {
		this.companyEmployeeCount = companyEmployeeCount;
	}
	public String getCompanyEmployeeCountRange() {
		return companyEmployeeCountRange;
	}
	public void setCompanyEmployeeCountRange(String companyEmployeeCountRange) {
		this.companyEmployeeCountRange = companyEmployeeCountRange;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public List<String> getCompanySIC() {
		return companySIC;
	}
	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}
	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}
	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}
	public List<String> getCompanyProductsAndServices() {
		return companyProductsAndServices;
	}
	public void setCompanyProductsAndServices(
			List<String> companyProductsAndServices) {
		this.companyProductsAndServices = companyProductsAndServices;
	}
	public Hashtags getHashtags() {
		return hashtags;
	}
	public void setHashtags(Hashtags hashtags) {
		this.hashtags = hashtags;
	}
	public SimilarCompanies getSimilarCompanies() {
		return similarCompanies;
	}
	public void setSimilarCompanies(SimilarCompanies similarCompanies) {
		this.similarCompanies = similarCompanies;
	}
	public String getCompanyTicker() {
		return companyTicker;
	}
	public void setCompanyTicker(String companyTicker) {
		this.companyTicker = companyTicker;
	}
	public String getCompanyDescription() {
		return companyDescription;
	}
	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}
	public List<String> getCompanyTopLevelIndustry() {
		return companyTopLevelIndustry;
	}
	public void setCompanyTopLevelIndustry(List<String> companyTopLevelIndustry) {
		this.companyTopLevelIndustry = companyTopLevelIndustry;
	}
	public List<String> getCompanyIndustry() {
		return companyIndustry;
	}
	public void setCompanyIndustry(List<String> companyIndustry) {
		this.companyIndustry = companyIndustry;
	}
	public List<String> getCompanyRanking() {
		return companyRanking;
	}
	public void setCompanyRanking(List<String> companyRanking) {
		this.companyRanking = companyRanking;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	/*public List<KeyValuePair<String, String>> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(List<KeyValuePair<String, String>> companyList) {
		this.companyList = companyList;
	}*/
	
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	
}
