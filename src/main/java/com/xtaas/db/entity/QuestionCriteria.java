package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/14/14
 * Time: 10:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class QuestionCriteria extends Criteria{
  private String questionId;
  private String guide;     //Tips for agent to ask and select the right answer

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    QuestionCriteria that = (QuestionCriteria) o;

    if (guide != null ? !guide.equals(that.guide) : that.guide != null) return false;
    if (questionId != null ? !questionId.equals(that.questionId) : that.questionId != null) return false;

    return super.equals(o);
  }

  @Override
  public int hashCode() {
    int result = questionId != null ? questionId.hashCode() : 0;
    result = 31 * result + (guide != null ? guide.hashCode() : 0);
    return 31 * result + super.hashCode();
  }

  @Override
  public String toString() {
    return "QuestionCriteria{" +
      "questionId='" + questionId + '\'' +
      ", guide='" + guide + '\'' +
      "} " + super.toString();
  }

  public String getQuestionId() {

    return questionId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getGuide() {
    return guide;
  }

  public void setGuide(String guide) {
    this.guide = guide;
  }
}
