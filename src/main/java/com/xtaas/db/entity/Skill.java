package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/23/14
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Skill extends AbstractEntity{
  private String name;
  private String description;
  private String vertical;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getVertical() {
    return vertical;
  }

  public void setVertical(String vertical) {
    this.vertical = vertical;
  }

  @Override
  public String toString() {
    return "Skill{" +
      "name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", vertical='" + vertical + '\'' +
      "} " + super.toString();
  }
}
