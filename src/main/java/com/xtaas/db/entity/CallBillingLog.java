package com.xtaas.db.entity;

import java.util.Date;
import java.util.HashMap;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="callbillinglog")
public class CallBillingLog extends AbstractEntity {

	private HashMap<String, Object> callBillingLogMap; //key value pair of call log sent by Twilio
	
	private Date calllogCreatedDate;
	
	private String campaignId;
	
	private String prospectCallId;
	
	private String prospectInteractionSessionId;
	
	private CallBillingType callBillingType;
	
	private String twilioConferenceSid;
	
	public enum CallBillingType {
		CLIENT_MINUTES, VOICE_MINUTES, UNKNOWN;
	}

	public HashMap<String, Object> getCallBillingLogMap() {
		return callBillingLogMap;
	}

	public void setCallBillingLogMap(HashMap<String, Object> callBillingLogMap) {
		this.callBillingLogMap = callBillingLogMap;
		this.setId(callBillingLogMap.get("Sid").toString()); //using CallSid as _Id
	}

	public Date getCalllogCreatedDate() {
		return calllogCreatedDate;
	}

	public void setCalllogCreatedDate(Date calllogCreatedDate) {
		this.calllogCreatedDate = calllogCreatedDate;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the prospectInteractionSessionId
	 */
	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	/**
	 * @param prospectInteractionSessionId the prospectInteractionSessionId to set
	 */
	public void setProspectInteractionSessionId(
			String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	/**
	 * @return the callBillingType
	 */
	public CallBillingType getCallBillingType() {
		return callBillingType;
	}

	/**
	 * @param callBillingType the callBillingType to set
	 */
	public void setCallBillingType(CallBillingType callBillingType) {
		this.callBillingType = callBillingType;
	}

	/**
	 * @return the twilioConferenceSid
	 */
	public String getTwilioConferenceSid() {
		return twilioConferenceSid;
	}

	/**
	 * @param twilioConferenceSid the twilioConferenceSid to set
	 */
	public void setTwilioConferenceSid(String twilioConferenceSid) {
		this.twilioConferenceSid = twilioConferenceSid;
	}
}
