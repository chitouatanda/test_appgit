package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.Address;

@Document
public class Partner extends AbstractEntity {
	private String firstName;
	private String lastName;
	private String emailId;
	private String userName;
	private String companyName;
	private String companySize;
	private String industry;
	private String phoneNumber;
	private Address address;
	private String assetDeliveryEmail;
	
	public Partner(String firstName, String lastName, String emailId,
			String userName, String companyName, String companySize,
			String industry, String phoneNumber, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.userName = userName;
		this.companyName = companyName;
		this.companySize = companySize;
		this.industry = industry;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanySize() {
		return companySize;
	}
	
	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}
	
	public String getIndustry() {
		return industry;
	}
	
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

    public String getAssetDeliveryEmail() {
        return assetDeliveryEmail;
    }

    public void setAssetDeliveryEmail(String assetDeliveryEmail) {
        this.assetDeliveryEmail = assetDeliveryEmail;
    }
	
}
