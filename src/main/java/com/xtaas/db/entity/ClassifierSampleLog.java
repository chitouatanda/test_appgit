package com.xtaas.db.entity;

public class ClassifierSampleLog extends AbstractEntity {
    public enum SampleType {
        FAILED_SAMPLE, RANDOM_SAMPLE
    }

    private String recordingId;
    private String className;
    private String transcript;
    private SampleType sampleType;
    private float score;
    private String mimeType;

    public String getRecordingId() {
        return recordingId;
    }

    public String getClassName() {
        return className;
    }

    public String getTranscript() {
        return transcript;
    }

    public SampleType getSampleType() {
        return sampleType;
    }

    public float getScore() {
        return score;
    }

    public void setRecordingId(String recordingId) {
        this.recordingId = recordingId;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setTranscript(String transcript) {
        this.transcript = transcript;
    }

    public void setSampleType(SampleType sampleType) {
        this.sampleType = sampleType;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }


//---------- Fluent Style api---------

    public ClassifierSampleLog withRecordingId(String recordingId) {
        this.recordingId = recordingId;
        return this;
    }

    public ClassifierSampleLog withClassName(String className) {
        this.className = className;
        return this;
    }

    public ClassifierSampleLog withTranscript(String transcript) {
        this.transcript = transcript;
        return this;
    }

    public ClassifierSampleLog withSampleType(SampleType sampleType) {
        this.sampleType = sampleType;
        return this;
    }

    public ClassifierSampleLog withScore(float score) {
        this.score = score;
        return this;
    }

	public ClassifierSampleLog withMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClassifierSampleLog{");
        sb.append("recordingId=\'").append(this.recordingId).append("\'");
        sb.append(", ").append("className=\'").append(this.className).append("\'");
        sb.append(", ").append("transcript=\'").append(this.transcript).append("\'");
        sb.append(", ").append("sampleType=").append(this.sampleType);
        sb.append(", ").append("score=").append(this.score);
        sb.append('}');
        return sb.toString();
    }
}
