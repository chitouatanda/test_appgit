package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.web.dto.DncScrubResponseDTO;

@Document(collection = "dncusdncscrub")
public class DncUsDncScrub extends AbstractEntity {

	private String phone;
	private String resultCode;
	private String reserved;
	private String reason;
	private String regionAbbrev;
	private String country;
	private String locale;
	private String carrierInfo;
	private String newReassignedAreaCode;
	private String tzCode;
	private String callingWindow;
	private String utcOffset;
	private String doNotCallToday;
	private String callingTimeRestrictions;
	private String ebrType;
	private String isWirelessOrVoIP;
	private String lineType;
	private boolean isDNC;

	protected DncUsDncScrub() {
	}

	public DncUsDncScrub(DncScrubResponseDTO dto) {
		setPhone(dto.getPhone());
		setResultCode(dto.getResultCode());
		setReserved(dto.getReserved());
		setReason(dto.getReason());
		setRegionAbbrev(dto.getRegionAbbrev());
		setCountry(dto.getCountry());
		setLocale(dto.getLocale());
		setCarrierInfo(dto.getCarrierInfo());
		setNewReassignedAreaCode(dto.getNewReassignedAreaCode());
		setTzCode(dto.getTzCode());
		setCallingWindow(dto.getCallingWindow());
		setUtcOffset(dto.getUtcOffset());
		setDoNotCallToday(dto.getDoNotCallToday());
		setCallingTimeRestrictions(dto.getCallingTimeRestrictions());
		setEbrType(dto.getEbrType());
		setIsWirelessOrVoIP(dto.getIsWirelessOrVoIP());
		setLineType(dto.getLineType());
		setDNC(dto.isDNC());
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRegionAbbrev() {
		return regionAbbrev;
	}

	public void setRegionAbbrev(String regionAbbrev) {
		this.regionAbbrev = regionAbbrev;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCarrierInfo() {
		return carrierInfo;
	}

	public void setCarrierInfo(String carrierInfo) {
		this.carrierInfo = carrierInfo;
	}

	public String getNewReassignedAreaCode() {
		return newReassignedAreaCode;
	}

	public void setNewReassignedAreaCode(String newReassignedAreaCode) {
		this.newReassignedAreaCode = newReassignedAreaCode;
	}

	public String getTzCode() {
		return tzCode;
	}

	public void setTzCode(String tzCode) {
		this.tzCode = tzCode;
	}

	public String getCallingWindow() {
		return callingWindow;
	}

	public void setCallingWindow(String callingWindow) {
		this.callingWindow = callingWindow;
	}

	public String getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	public String getDoNotCallToday() {
		return doNotCallToday;
	}

	public void setDoNotCallToday(String doNotCallToday) {
		this.doNotCallToday = doNotCallToday;
	}

	public String getCallingTimeRestrictions() {
		return callingTimeRestrictions;
	}

	public void setCallingTimeRestrictions(String callingTimeRestrictions) {
		this.callingTimeRestrictions = callingTimeRestrictions;
	}

	public String getEbrType() {
		return ebrType;
	}

	public void setEbrType(String ebrType) {
		this.ebrType = ebrType;
	}

	public String getIsWirelessOrVoIP() {
		return isWirelessOrVoIP;
	}

	public void setIsWirelessOrVoIP(String isWirelessOrVoIP) {
		this.isWirelessOrVoIP = isWirelessOrVoIP;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public boolean isDNC() {
		return isDNC;
	}

	public void setDNC(boolean isDNC) {
		this.isDNC = isDNC;
	}

	@Override
	public String toString() {
		return "DncUsDncScrub [phone=" + phone + ", resultCode=" + resultCode + ", reserved=" + reserved + ", reason="
				+ reason + ", regionAbbrev=" + regionAbbrev + ", country=" + country + ", locale=" + locale
				+ ", carrierInfo=" + carrierInfo + ", newReassignedAreaCode=" + newReassignedAreaCode + ", tzCode="
				+ tzCode + ", callingWindow=" + callingWindow + ", utcOffset=" + utcOffset + ", doNotCallToday="
				+ doNotCallToday + ", callingTimeRestrictions=" + callingTimeRestrictions + ", ebrType=" + ebrType
				+ ", isWirelessOrVoIP=" + isWirelessOrVoIP + ", lineType=" + lineType + ", isDNC=" + isDNC + "]";
	}

}
