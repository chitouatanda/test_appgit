package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.Location;

@Document
public class Geography extends AbstractEntity {
	private String locationType;
	private List<Location> locations;
	
	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}



	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}



	/**
	 * @return the locations
	 */
	public List<Location> getLocations() {
		return locations;
	}



	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Geography [locationType=" + locationType + ", locations="
				+ locations + "]";
	}


}
