package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.valueobjects.KeyValuePair;

@Document(collection="masterdata")
public class MasterData {
	@Id
	private String type;
	private List<KeyValuePair<String, String>> data;

	public String getType() {
		return type;
	}

	public List<KeyValuePair<String, String>> getData() {
		return data;
	}

}
