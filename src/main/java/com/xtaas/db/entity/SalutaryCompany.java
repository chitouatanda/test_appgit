package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "salutarycompany")
public class SalutaryCompany extends AbstractEntity {

	private String companyId;
	private String companyName;
	private String companyDomain;
	private String city;
	private String country;
	private String stateCode;
	private String industry;
	private Double minRevenue;
	private Double maxRevenue;
	private Double minEmployeeCount;
	private Double maxEmployeeCount;
	private List<String> companySIC;
	private List<String> companyNAICS;

	public SalutaryCompany() {
		// TODO Auto-generated constructor stub
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyDomain() {
		return companyDomain;
	}

	public void setCompanyDomain(String companyDomain) {
		this.companyDomain = companyDomain;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public Double getMinRevenue() {
		return minRevenue;
	}

	public void setMinRevenue(Double minRevenue) {
		this.minRevenue = minRevenue;
	}

	public Double getMaxRevenue() {
		return maxRevenue;
	}

	public void setMaxRevenue(Double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	public Double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public void setMinEmployeeCount(Double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	public Double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public void setMaxEmployeeCount(Double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}

	public List<String> getCompanySIC() {
		return companySIC;
	}

	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}

	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}

	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}
}
