package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "clientmapping")
public class ClientMapping {

	private String id;
	private String clientName;
	private String campaignId;
	private String stdAttribute;
	private String key;
	private String value;
	private int sequence;
	private String colHeader;
	private String defaultValue;
	private String titleMgmtLevel;

	public String getTitleMgmtLevel() {
		return titleMgmtLevel;
	}

	public void setTitleMgmtLevel(String titleMgmtLevel) {
		this.titleMgmtLevel = titleMgmtLevel;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getColHeader() {
		return colHeader;
	}

	public void setColHeader(String colHeader) {
		this.colHeader = colHeader;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getStdAttribute() {
		return stdAttribute;
	}

	public void setStdAttribute(String stdAttribute) {
		this.stdAttribute = stdAttribute;
	}
	

}
