package com.xtaas.db.entity;

import java.util.Date;

public class CampaignTeamInvitation {
	private String teamRef;
	private Date invitationDate;

	public String getTeamRef() {
		return teamRef;
	}

	public void setTeamRef(String teamRef) {
		this.teamRef = teamRef;
	}

	public Date getInvitationDate() {
		return invitationDate;
	}

	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

}
