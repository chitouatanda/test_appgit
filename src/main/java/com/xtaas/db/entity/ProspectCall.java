/**
 * 
 */
package com.xtaas.db.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.Transient;

import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.ProspectIdentityChangeLog;
import com.xtaas.domain.valueobject.ProspectSortIndex;
import com.xtaas.domain.valueobject.SendGirdEmailEvents;
import com.xtaas.web.dto.IndirectProspectsCacheDTO;
import com.xtaas.web.dto.IndirectProspectsDTO;
import com.xtaas.web.dto.ProspectCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;

/**
 * Entity which represents a prospect
 * 
 * @author djain
 */
public class ProspectCall implements Serializable {
	private static final long serialVersionUID = 1L;
	private String contactId;
	private String sfdcCampaignId;
	private String prospectCallId;
	private Prospect prospect;
	private String twilioCallSid;
	private String agentId;
	private String partnerId;
	private String qualityBucket;
	private int qualityBucketSort;
	private String multiProspect;
	private ArrayList<Note> notes;
	private ArrayList<Note> previousNotes;
	private List<AgentQaAnswer> answers;
	private List<IndirectProspectsDTO> indirectProspects;
	// private HashMap<String, String> answers;
	private String previousProspectCallId;
	private String deliveredAssetId;
	private String dispositionStatus;
	private String subStatus;
	private String leadStatus;
	private String campaignId;
	private int callRetryCount; // Number of times a prospect has been retried
	private Date callStartTime; // time when call started in UTC
	private int callDuration; // duration of calls in secs as initiated/disconnected by agent
	private String outboundNumber; // outbound number used to call this prospect
	private String recordingUrl; // call recording url
	private String recordingUrlAws; // call recording aws url
	private Integer prospectHandleDuration; // indicate total time spent by agent in handling this prospect from begin
											// to end
	private Integer telcoDuration; // actual conversation duration as returned by telco
	private ArrayList<ProspectIdentityChangeLog> prospectIdentityChangeLogs; // list of changes occurred on prospect
																				// identity fields
	private Integer latestProspectIdentityChangeLogId; // Id of latest prospect identity change log
	private String prospectInteractionSessionId; // unique session id for the prospect call interaction. Will be same
													// value for the entire interaction lifecycle i.e from
													// Call_PREVIEW/CALL_DIALED to CALL_WRAPPED_UP
	private Long dailyCallRetryCount; // Number of calls made to a prospect in a day (XP-972)
	private ProspectSortIndex prospectSortIndex; // Prospect's field level sort index (XP-972)
	private Map<String, String> extraAttributes;
	private boolean emailBounce;
	private int prospectVersion;
	private String zoomValidated;
	private boolean leadDelivered;
	private String clientDelivered;
	private String supervisorId;
	private String dataSlice;
	private int machineDetectedIncrementer;
	private String organizationId;
	private String zoomPersonUrl;
	private String zoomCompanyUrl;
	private boolean autoMachineDetected;
	private SendGirdEmailEvents sendGirdEmailEvents;
	private String voiceProvider;
	private String callDetectedAs;
	private long amdTimeDiffInSec;
	private Date updatedDate;
	private String updatedSource;
	private long amdTimeDiffInMilliSec;
	private String campaignMode;
	private boolean emailRevealed;
	private String agentLoginURL;
	private Integer sortOrder;
	private String sipProvider;
	
	@Transient
	private DialerMode dialerMode; // set by CampaignJob, to be used by Dialer
	@Transient
	private long callbackTime; // set the callback time in Millis
	@Transient
	private Date createdDate; // date when prospect was created in system

	public ProspectCall(String prospectCallId, String campaignId, Prospect prospect) {
		this.prospectCallId = prospectCallId;
		setCampaignId(campaignId);
		setProspect(prospect);
	}
	
	public ProspectCall(String prospectCallId, String campaignId, Prospect prospect, boolean isEmail) {
		this.prospectCallId = prospectCallId;
		if(isEmail) {
			this.sendGirdEmailEvents = new SendGirdEmailEvents();
		}
		setCampaignId(campaignId);
		setProspect(prospect);
	}
	
	public ProspectCacheDTO getProspectCacheDTO(ProspectCall prospectCall) {
		ProspectCacheDTO prospectCacheDTO = new ProspectCacheDTO();
		if (prospectCall != null && prospectCall.getProspect() != null) {
			prospectCacheDTO.setCountry(prospectCall.getProspect().getCountry());
			prospectCacheDTO.setTimeZone(prospectCall.getProspect().getTimeZone());
			prospectCacheDTO.setFirstName(prospectCall.getProspect().getFirstName());
			prospectCacheDTO.setLastName(prospectCall.getProspect().getLastName());
			prospectCacheDTO.setPhone(prospectCall.getProspect().getPhone());
			prospectCacheDTO.setCustomAttributes(prospectCall.getProspect().getCustomAttributes());
			prospectCacheDTO.setProspectType(prospectCall.getProspect().getProspectType());
			prospectCacheDTO.setCompany(prospectCall.getProspect().getCompany());
			prospectCacheDTO.setEmail(prospectCall.getProspect().getEmail());
			prospectCacheDTO.setStateCode(prospectCall.getProspect().getStateCode());
			prospectCacheDTO.setExtension(prospectCall.getProspect().getExtension());
			prospectCacheDTO.setSource(prospectCall.getProspect().getSource());
			prospectCacheDTO.setDepartment(prospectCall.getProspect().getDepartment());
			prospectCacheDTO.setTitle(prospectCall.getProspect().getTitle());
		}
		return prospectCacheDTO;
	}
	
	public List<IndirectProspectsCacheDTO> getIndirectProspectCacheDTO(List<IndirectProspectsDTO> indirectProspects) {
		List<IndirectProspectsCacheDTO> indirectList = new ArrayList<IndirectProspectsCacheDTO>();
		for (IndirectProspectsDTO indirectProspectDTO : indirectProspects) {
			if (indirectProspectDTO != null) {
				IndirectProspectsCacheDTO indirectProspectsCacheDTO = new IndirectProspectsCacheDTO();
				indirectProspectsCacheDTO.setFirstName(indirectProspectDTO.getFirstName());
				indirectProspectsCacheDTO.setProspectCallId(indirectProspectDTO.getProspectCallId());
				indirectProspectsCacheDTO.setLastName(indirectProspectDTO.getLastName());
				indirectProspectsCacheDTO.setDepartment(indirectProspectDTO.getDepartment());
				indirectProspectsCacheDTO.setTitle(indirectProspectDTO.getTitle());
				indirectProspectsCacheDTO.setCompany(indirectProspectDTO.getCompany());
				indirectList.add(indirectProspectsCacheDTO);
			}
		}
		return indirectList;
	}
	
	public ProspectCall() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the twilioCallSid
	 */
	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	/**
	 * @param twilioCallSids the twilioCallSid to set
	 */
	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	/**
	 * @return the notes
	 */
	public ArrayList<Note> getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(ArrayList<Note> notes) {
		this.notes = notes;
	}

	/**
	 * @return the previousNotes
	 */
	public ArrayList<Note> getPreviousNotes() {
		return previousNotes;
	}

	/**
	 * @param previousNotes the previousNotes to set
	 */
	public void setPreviousNotes(ArrayList<Note> previousNotes) {
		this.previousNotes = previousNotes;
	}

	public List<IndirectProspectsDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	/**
	 * @return the answers
	 */
	public List<AgentQaAnswer> getAnswers() {
		return answers;
	}

	/**
	 * @param answers the answers to set
	 */
	public void setAnswers(List<AgentQaAnswer> answers) {
		this.answers = answers;
	}

	/**
	 * @return the previousId
	 */
	public String getPreviousProspectCallId() {
		return previousProspectCallId;
	}

	/**
	 * @param previousId the previousId to set
	 */
	public void setPreviousProspectCallId(String previousProspectCallId) {
		this.previousProspectCallId = previousProspectCallId;
	}

	/**
	 * @return the deliveredAssetId
	 */
	public String getDeliveredAssetId() {
		return deliveredAssetId;
	}

	/**
	 * @param deliveredAssetId the deliveredAssetId to set
	 */
	public void setDeliveredAssetId(String deliveredAssetId) {
		this.deliveredAssetId = deliveredAssetId;
	}

	/**
	 * @return the dispositionStatus
	 */
	public String getDispositionStatus() {
		return dispositionStatus;
	}

	/**
	 * @param dispositionStatus the dispositionStatus to set
	 */
	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	/**
	 * @return the prospect
	 */
	public Prospect getProspect() {
		return prospect;
	}

	public void setProspect(Prospect prospect) {
		if (prospect == null) {
			throw new IllegalArgumentException("Prospect is required");
		}
		this.prospect = prospect;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		if (campaignId == null) {
			throw new IllegalArgumentException("CampaignId is required");
		}
		this.campaignId = campaignId;
	}

	/**
	 * @return the callRetryCount
	 */
	public int getCallRetryCount() {
		return callRetryCount;
	}

	/**
	 * @param callRetryCount the callRetryCount to set
	 */
	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	/**
	 * @return the contactId
	 */
	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the sfdcCampaignId
	 */
	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the qualityBucket
	 */
	public String getQualityBucket() {
		return qualityBucket;
	}

	/**
	 * @param qualityBucket the qualityBucket to set
	 */
	public void setQualityBucket(String qualityBucket) {
		this.qualityBucket = qualityBucket;
	}

	/**
	 * @return the multiProspect
	 */
	public String getMultiProspect() {
		return multiProspect;
	}

	/**
	 * @param multiProspect the multiProspect to set
	 */
	public void setMultiProspect(String multiProspect) {
		this.multiProspect = multiProspect;
	}

	/**
	 * @return the qualityBucket
	 */
	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	/**
	 * @param qualityBucket the qualityBucket to set
	 */
	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public long getCallbackTime() {
		return callbackTime;
	}

	public void setCallbackTime(long callbackTime) {
		this.callbackTime = callbackTime;
	}

	/**
	 * @return the dialerMode
	 */
	public DialerMode getDialerMode() {
		return dialerMode;
	}

	/**
	 * @param dialerMode the dialerMode to set
	 */
	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	/**
	 * @return the callStartTime
	 */
	public Date getCallStartTime() {
		return callStartTime;
	}

	/**
	 * @param callStartTime the callStartTime to set
	 */
	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}

	/**
	 * @return the callDuration
	 */
	public int getCallDuration() {
		return callDuration;
	}

	/**
	 * @param callDuration the callDuration to set
	 */
	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	/**
	 * @return the subStatus
	 */
	public String getSubStatus() {
		return subStatus;
	}

	/**
	 * @param subStatus the subStatus to set
	 */
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	/**
	 * @return the outboundNumber
	 */
	public String getOutboundNumber() {
		return outboundNumber;
	}

	/**
	 * @param outboundNumber the outboundNumber to set
	 */
	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	/**
	 * @return the recordingUrl
	 */
	public String getRecordingUrl() {
		return recordingUrl;
	}

	/**
	 * @param recordingUrl the recordingUrl to set
	 */
	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	/**
	 * @return the recordingUrlAws
	 */
	public String getRecordingUrlAws() {
		return recordingUrlAws;
	}

	/**
	 * @param recordingUrlAws the recordingUrlAws to set
	 */
	public void setRecordingUrlAws(String recordingUrlAws) {
		this.recordingUrlAws = recordingUrlAws;
	}

	/**
	 * @return the prospectHandleDuration
	 */
	public Integer getProspectHandleDuration() {
		return prospectHandleDuration;
	}

	/**
	 * @param prospectHandleDuration the prospectHandleDuration to set
	 */
	public void setProspectHandleDuration(Integer prospectHandleDuration) {
		this.prospectHandleDuration = prospectHandleDuration;
	}

	/**
	 * @return the telcoDuration
	 */
	public Integer getTelcoDuration() {
		return telcoDuration;
	}

	/**
	 * @param telcoDuration the telcoDuration to set
	 */
	public void setTelcoDuration(Integer telcoDuration) {
		this.telcoDuration = telcoDuration;
	}

	/**
	 * @return the prospectIdentityChanged
	 */
	public boolean isProspectIdentityChanged() {
		return (latestProspectIdentityChangeLogId != null && latestProspectIdentityChangeLogId > 0);
	}

	public void logProspectIdentityChangeByName(String oldFirstName, String oldLastName) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog(latestProspectIdentityChangeLogId,
				oldFirstName, oldLastName, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}
	
	public void logProspectIdentityChangeByEmail(String oldEmail) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByEmail(latestProspectIdentityChangeLogId, oldEmail, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}
	
	public void logProspectIdentityChangeByDepartment(String oldDepartment) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByDepartment(latestProspectIdentityChangeLogId, oldDepartment, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}
	
	public void logProspectIdentityChangeByCompany(String oldCompany) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByCompany(latestProspectIdentityChangeLogId, oldCompany, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}
	
	public void logProspectIdentityChangeByRole(String oldRole) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByRole(latestProspectIdentityChangeLogId, oldRole, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}
	
	public void logProspectIdentityChangeByDomain(String oldDomain) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByDomain(latestProspectIdentityChangeLogId, oldDomain, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}

	public void logProspectIdentityChangeByExtension(String oldExtension) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByExtension(latestProspectIdentityChangeLogId, oldExtension, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}

	public void logProspectIdentityChangeByAgentDialedExtension(String oldAgentDialedExtension) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
		piChangeLog.setProspectIdentityChangeLogByAgentDialedExtension(latestProspectIdentityChangeLogId, oldAgentDialedExtension, new Date());
		if (prospectIdentityChangeLogs == null) {
			prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
		}
		prospectIdentityChangeLogs.add(piChangeLog);
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the prospectInteractionSessionId
	 */
	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	/**
	 * @param prospectInteractionSessionId the prospectInteractionSessionId to set
	 */
	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	/**
	 * @return the dailyCallRetryCount
	 */
	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	/**
	 * @param dailyCallRetryCount the dailyCallRetryCount to set
	 */
	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	/**
	 * @return the prospectSortIndex
	 */
	public ProspectSortIndex getProspectSortIndex() {
		return prospectSortIndex;
	}

	/**
	 * @param prospectSortIndex the prospectSortIndex to set
	 */
	public void setProspectSortIndex(ProspectSortIndex prospectSortIndex) {
		this.prospectSortIndex = prospectSortIndex;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public Map<String, String> getExtraAttributes() {
		return extraAttributes;
	}

	public void setExtraAttributes(Map<String, String> extraAttributes) {
		this.extraAttributes = extraAttributes;
	}

	public boolean isEmailBounce() {
		return emailBounce;
	}

	public void setEmailBounce(boolean emailBounce) {
		this.emailBounce = emailBounce;
	}

	public int getProspectVersion() {
		return prospectVersion;
	}

	public void setProspectVersion(int prospectVersion) {
		this.prospectVersion = prospectVersion;
	}

	public String getZoomValidated() {
		return zoomValidated;
	}

	public void setZoomValidated(String zoomValidated) {
		this.zoomValidated = zoomValidated;
	}

	public boolean isLeadDelivered() {
		return leadDelivered;
	}

	public void setLeadDelivered(boolean leadDelivered) {
		this.leadDelivered = leadDelivered;
	}

	public String getClientDelivered() {
		return clientDelivered;
	}

	public void setClientDelivered(String clientDelivered) {
		this.clientDelivered = clientDelivered;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public int getMachineDetectedIncrementer() {
		return machineDetectedIncrementer;
	}

	public void setMachineDetectedIncrementer(int machineDetectedIncrementer) {
		this.machineDetectedIncrementer = machineDetectedIncrementer;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public boolean isAutoMachineDetected() {
		return autoMachineDetected;
	}

	public void setAutoMachineDetected(boolean autoMachineDetected) {
		this.autoMachineDetected = autoMachineDetected;
	}

	public SendGirdEmailEvents getSendGirdEmailEvents() {
		return sendGirdEmailEvents;
	}

	public void setSendGirdEmailEvents(SendGirdEmailEvents sendGirdEmailEvents) {
		this.sendGirdEmailEvents = sendGirdEmailEvents;
	}

	public String getVoiceProvider() {
		return voiceProvider;
	}

	public void setVoiceProvider(String voiceProvider) {
		this.voiceProvider = voiceProvider;
	}

	public long getAmdTimeDiffInSec() {
		return amdTimeDiffInSec;
	}

	public void setAmdTimeDiffInSec(long amdTimeDiffInSec) {
		this.amdTimeDiffInSec = amdTimeDiffInSec;
	}

	public String getCallDetectedAs() {
		return callDetectedAs;
	}

	public void setCallDetectedAs(String callDetectedAs) {
		this.callDetectedAs = callDetectedAs;
	}

	public long getAmdTimeDiffInMilliSec() {
		return amdTimeDiffInMilliSec;
	}

	public void setAmdTimeDiffInMilliSec(long amdTimeDiffInMilliSec) {
		this.amdTimeDiffInMilliSec = amdTimeDiffInMilliSec;
	}
	

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedSource() {
		return updatedSource;
	}

	public void setUpdatedSource(String updatedSource) {
		this.updatedSource = updatedSource;
	}

	public String getCampaignMode() {
		return campaignMode;
	}

	public void setCampaignMode(String campaignMode) {
		this.campaignMode = campaignMode;
	}
	
	public boolean isEmailRevealed() {
		return emailRevealed;
	}

	public void setEmailRevealed(boolean emailRevealed) {
		this.emailRevealed = emailRevealed;
	}

	public String getAgentLoginURL() {
		return agentLoginURL;
	}

	public void setAgentLoginURL(String agentLoginURL) {
		this.agentLoginURL = agentLoginURL;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	public boolean isCustomFieldsAnswerChanged(ProspectCallDTO prospectCallDTO) {
		boolean isAnswerChanged = false;
		if (prospectCallDTO == null) {
			return false;
		}
		if (prospectCallDTO != null && prospectCallDTO.getAnswers() != null
				&& prospectCallDTO.getAnswers().size() > 0 && answers != null && answers.size() > 0) {
			for (AgentQaAnswer agentQaAnswer : answers) {
				List<AgentQaAnswer> question = null;
				if (prospectCallDTO != null && prospectCallDTO.getAnswers() != null
						&& prospectCallDTO.getAnswers().size() > 0) {
					question = prospectCallDTO.getAnswers().stream()
							.filter(c -> c.getQuestion().equalsIgnoreCase(agentQaAnswer.getQuestion()))
							.collect(Collectors.toList());
				}
				if (question != null && question.size() > 0 && agentQaAnswer != null && agentQaAnswer.getAnswer() != null && !agentQaAnswer.getAnswer().isEmpty()
						&& agentQaAnswer.getAnswer().equalsIgnoreCase(question.get(0).getAnswer())) {
					isAnswerChanged = true;
				} else {
					isAnswerChanged = false;
					return isAnswerChanged;
				}
			}
		}
		return isAnswerChanged;
	}

	public void logProspectIdentityChangeByQaAnswers(List<AgentQaAnswer> answerFromDB,
			List<AgentQaAnswer> answerFromAgent) {
		if (latestProspectIdentityChangeLogId == null) {
			latestProspectIdentityChangeLogId = 1; // initialize with 1
		} else {
			latestProspectIdentityChangeLogId++;
		}
		if (answerFromDB != null && answerFromAgent != null && answerFromDB.size() > 0 && answerFromAgent.size() > 0) {
			List<AgentQaAnswer> qa = new ArrayList<AgentQaAnswer>();
			for (AgentQaAnswer agentQaAnswer : answerFromAgent) {
				qa = answerFromDB.stream()
						.filter(answer -> answer.getQuestion().equalsIgnoreCase(agentQaAnswer.getQuestion()))
						.collect(Collectors.toList());
				if (agentQaAnswer != null && agentQaAnswer.getAnswer() != null && qa != null && qa.size() > 0
						&& qa.get(0) != null && qa.get(0).getAnswer() != null && !qa.get(0).getAnswer().equalsIgnoreCase(agentQaAnswer.getAnswer())) {
					ProspectIdentityChangeLog piChangeLog = new ProspectIdentityChangeLog();
					piChangeLog.setProspectIdentityChangeLogAnswer(latestProspectIdentityChangeLogId,
							agentQaAnswer.getQuestion(), qa.get(0).getAnswer(), agentQaAnswer.getAnswer(),
							new Date());
					if (prospectIdentityChangeLogs == null) {
						prospectIdentityChangeLogs = new ArrayList<ProspectIdentityChangeLog>();
					}
					prospectIdentityChangeLogs.add(piChangeLog);
					latestProspectIdentityChangeLogId++;
				}
			}
		}
	}
}
