package com.xtaas.db.entity;

import java.util.Date;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.QaFeedback;
import com.xtaas.utils.XtaasConstants.ProspectCallStatus;

@Document(collection = "prospectmovement")
public class ProspectMovement extends AbstractEntity {

	private ProspectCall prospectCall;

	private ProspectCallStatus status;

	private Date callbackDate;

	private QaFeedback qaFeedback;

	private Map<String, String> companyAddress;

	private String dataSlice;

	public ProspectMovement toProspectMovement(ProspectCallLog callLog) {
		setProspectCall(callLog.getProspectCall());
		setStatus(ProspectCallStatus.valueOf(callLog.getStatus().toString()));
		setCallbackDate(callLog.getCallbackDate());
		setQaFeedback(callLog.getQaFeedback());
		setCompanyAddress(callLog.getCompanyAddress());
		setDataSlice(callLog.getDataSlice());
		return this;
	}

	public ProspectCall getProspectCall() {
		return prospectCall;
	}

	public void setProspectCall(ProspectCall prospectCall) {
		this.prospectCall = prospectCall;
	}

	public ProspectCallStatus getStatus() {
		return status;
	}

	public void setStatus(ProspectCallStatus status) {
		this.status = status;
	}

	public Date getCallbackDate() {
		return callbackDate;
	}

	public void setCallbackDate(Date callbackDate) {
		this.callbackDate = callbackDate;
	}

	public QaFeedback getQaFeedback() {
		return qaFeedback;
	}

	public void setQaFeedback(QaFeedback qaFeedback) {
		this.qaFeedback = qaFeedback;
	}

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

}
