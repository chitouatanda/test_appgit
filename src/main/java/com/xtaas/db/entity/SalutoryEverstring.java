package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "salutory_everstring")
public class SalutoryEverstring extends AbstractEntity {

	private String searchDomain;
	private String salutoryDomain;
	private String status;
	private Object ES_AdvancedInsights;
	private int ES_AlexaRank;
	private String ES_City;
	private String ES_CompanyListNames;
	private String ES_CompanyPhone;
	private String ES_Country;
	private int ES_ECID;
	private int ES_Employee;
	private String ES_EmployeeBand;
	private int ES_EstimatedAge;
	private String ES_FacebookUrl;
	private String ES_Industry;
	private Object ES_Intent;
	private int ES_IntentAggregateScore;
	private Object ES_IntentByTier;
	private Object ES_IntentNumByTier;
	private String ES_IntentStr;
	private String ES_IntentTime;
	private String ES_Keywords;
	private String ES_LinkedInUrl;
	private String ES_LocationID;
	private String ES_MatchName;
	private String ES_MatchReasonBuildingName;
	private String ES_MatchReasonBuildingNumber;
	private String ES_MatchReasonBusinessType;
	private String ES_MatchReasonCity;
	private String ES_MatchReasonCompanyPhone;
	private String ES_MatchReasonCountry;
	private String ES_MatchReasonDirectional;
	private String ES_MatchReasonName;
	private String ES_MatchReasonRoadName;
	private String ES_MatchReasonRoadType;
	private String ES_MatchReasonState;
	private String ES_MatchReasonUnit;
	private String ES_MatchReasonWebsite;
	private String ES_MatchReasonZip;
	private int ES_MatchScore;
	private Object ES_Models;
	private String ES_NAICS2;
	private String ES_NAICS2Description;
	private String ES_NAICS4;
	private String ES_NAICS4Description;
	private String ES_NAICS6;
	private String ES_NAICS6Description;
	private String ES_Name;
	private int ES_NumLocations;
	private int ES_NumSurgingTopics;
	private String ES_PrimaryWebsite;
	private int ES_Revenue;
	private String ES_RevenueBand;
	private String ES_SIC2;
	private String ES_SIC2Description;
	private String ES_SIC3;
	private String ES_SIC3Description;
	private String ES_SIC4;
	private String ES_SIC4Description;
	private Object ES_SimilarCompanies;
	private String ES_State;
	private String ES_Street;
	private List<String> ES_Top5NAICS;
	private String ES_TwitterUrl;
	private String ES_YearStarted;
	private String ES_Zip;
	private List<String> error_messages;
	private int inputId;
	private String pointer;

	public SalutoryEverstring() {
		// TODO Auto-generated constructor stub
	}

	public SalutoryEverstring(Object eS_AdvancedInsights, int eS_AlexaRank, String eS_City,
			String eS_CompanyListNames, String eS_CompanyPhone, String eS_Country, int eS_ECID, int eS_Employee,
			String eS_EmployeeBand, int eS_EstimatedAge, String eS_FacebookUrl, String eS_Industry, Object eS_Intent,
			int eS_IntentAggregateScore, Object eS_IntentByTier, Object eS_IntentNumByTier, String eS_IntentStr,
			String eS_IntentTime, String eS_Keywords, String eS_LinkedInUrl, String eS_LocationID, String eS_MatchName,
			String eS_MatchReasonBuildingName, String eS_MatchReasonBuildingNumber, String eS_MatchReasonBusinessType,
			String eS_MatchReasonCity, String eS_MatchReasonCompanyPhone, String eS_MatchReasonCountry,
			String eS_MatchReasonDirectional, String eS_MatchReasonName, String eS_MatchReasonRoadName,
			String eS_MatchReasonRoadType, String eS_MatchReasonState, String eS_MatchReasonUnit,
			String eS_MatchReasonWebsite, String eS_MatchReasonZip, int eS_MatchScore, Object eS_Models,
			String eS_NAICS2, String eS_NAICS2Description, String eS_NAICS4, String eS_NAICS4Description,
			String eS_NAICS6, String eS_NAICS6Description, String eS_Name, int eS_NumLocations, int eS_NumSurgingTopics,
			String eS_PrimaryWebsite, int eS_Revenue, String eS_RevenueBand, String eS_SIC2, String eS_SIC2Description,
			String eS_SIC3, String eS_SIC3Description, String eS_SIC4, String eS_SIC4Description,
			Object eS_SimilarCompanies, String eS_State, String eS_Street, List<String> eS_Top5NAICS,
			String eS_TwitterUrl, String eS_YearStarted, String eS_Zip, List<String> error_messages, int inputId) {
		super();
		ES_AdvancedInsights = eS_AdvancedInsights;
		ES_AlexaRank = eS_AlexaRank;
		ES_City = eS_City;
		ES_CompanyListNames = eS_CompanyListNames;
		ES_CompanyPhone = eS_CompanyPhone;
		ES_Country = eS_Country;
		ES_ECID = eS_ECID;
		ES_Employee = eS_Employee;
		ES_EmployeeBand = eS_EmployeeBand;
		ES_EstimatedAge = eS_EstimatedAge;
		ES_FacebookUrl = eS_FacebookUrl;
		ES_Industry = eS_Industry;
		ES_Intent = eS_Intent;
		ES_IntentAggregateScore = eS_IntentAggregateScore;
		ES_IntentByTier = eS_IntentByTier;
		ES_IntentNumByTier = eS_IntentNumByTier;
		ES_IntentStr = eS_IntentStr;
		ES_IntentTime = eS_IntentTime;
		ES_Keywords = eS_Keywords;
		ES_LinkedInUrl = eS_LinkedInUrl;
		ES_LocationID = eS_LocationID;
		ES_MatchName = eS_MatchName;
		ES_MatchReasonBuildingName = eS_MatchReasonBuildingName;
		ES_MatchReasonBuildingNumber = eS_MatchReasonBuildingNumber;
		ES_MatchReasonBusinessType = eS_MatchReasonBusinessType;
		ES_MatchReasonCity = eS_MatchReasonCity;
		ES_MatchReasonCompanyPhone = eS_MatchReasonCompanyPhone;
		ES_MatchReasonCountry = eS_MatchReasonCountry;
		ES_MatchReasonDirectional = eS_MatchReasonDirectional;
		ES_MatchReasonName = eS_MatchReasonName;
		ES_MatchReasonRoadName = eS_MatchReasonRoadName;
		ES_MatchReasonRoadType = eS_MatchReasonRoadType;
		ES_MatchReasonState = eS_MatchReasonState;
		ES_MatchReasonUnit = eS_MatchReasonUnit;
		ES_MatchReasonWebsite = eS_MatchReasonWebsite;
		ES_MatchReasonZip = eS_MatchReasonZip;
		ES_MatchScore = eS_MatchScore;
		ES_Models = eS_Models;
		ES_NAICS2 = eS_NAICS2;
		ES_NAICS2Description = eS_NAICS2Description;
		ES_NAICS4 = eS_NAICS4;
		ES_NAICS4Description = eS_NAICS4Description;
		ES_NAICS6 = eS_NAICS6;
		ES_NAICS6Description = eS_NAICS6Description;
		ES_Name = eS_Name;
		ES_NumLocations = eS_NumLocations;
		ES_NumSurgingTopics = eS_NumSurgingTopics;
		ES_PrimaryWebsite = eS_PrimaryWebsite;
		ES_Revenue = eS_Revenue;
		ES_RevenueBand = eS_RevenueBand;
		ES_SIC2 = eS_SIC2;
		ES_SIC2Description = eS_SIC2Description;
		ES_SIC3 = eS_SIC3;
		ES_SIC3Description = eS_SIC3Description;
		ES_SIC4 = eS_SIC4;
		ES_SIC4Description = eS_SIC4Description;
		ES_SimilarCompanies = eS_SimilarCompanies;
		ES_State = eS_State;
		ES_Street = eS_Street;
		ES_Top5NAICS = eS_Top5NAICS;
		ES_TwitterUrl = eS_TwitterUrl;
		ES_YearStarted = eS_YearStarted;
		ES_Zip = eS_Zip;
		this.error_messages = error_messages;
		this.inputId = inputId;
	}

	public Object getES_AdvancedInsights() {
		return ES_AdvancedInsights;
	}

	public void setES_AdvancedInsights(Object eS_AdvancedInsights) {
		ES_AdvancedInsights = eS_AdvancedInsights;
	}

	public int getES_AlexaRank() {
		return ES_AlexaRank;
	}

	public void setES_AlexaRank(int eS_AlexaRank) {
		ES_AlexaRank = eS_AlexaRank;
	}

	public String getES_City() {
		return ES_City;
	}

	public void setES_City(String eS_City) {
		ES_City = eS_City;
	}

	public String getES_CompanyListNames() {
		return ES_CompanyListNames;
	}

	public void setES_CompanyListNames(String eS_CompanyListNames) {
		ES_CompanyListNames = eS_CompanyListNames;
	}

	public String getES_CompanyPhone() {
		return ES_CompanyPhone;
	}

	public void setES_CompanyPhone(String eS_CompanyPhone) {
		ES_CompanyPhone = eS_CompanyPhone;
	}

	public String getES_Country() {
		return ES_Country;
	}

	public void setES_Country(String eS_Country) {
		ES_Country = eS_Country;
	}

	public int getES_ECID() {
		return ES_ECID;
	}

	public void setES_ECID(int eS_ECID) {
		ES_ECID = eS_ECID;
	}

	public int getES_Employee() {
		return ES_Employee;
	}

	public void setES_Employee(int eS_Employee) {
		ES_Employee = eS_Employee;
	}

	public String getES_EmployeeBand() {
		return ES_EmployeeBand;
	}

	public void setES_EmployeeBand(String eS_EmployeeBand) {
		ES_EmployeeBand = eS_EmployeeBand;
	}

	public int getES_EstimatedAge() {
		return ES_EstimatedAge;
	}

	public void setES_EstimatedAge(int eS_EstimatedAge) {
		ES_EstimatedAge = eS_EstimatedAge;
	}

	public String getES_FacebookUrl() {
		return ES_FacebookUrl;
	}

	public void setES_FacebookUrl(String eS_FacebookUrl) {
		ES_FacebookUrl = eS_FacebookUrl;
	}

	public String getES_Industry() {
		return ES_Industry;
	}

	public void setES_Industry(String eS_Industry) {
		ES_Industry = eS_Industry;
	}

	public Object getES_Intent() {
		return ES_Intent;
	}

	public void setES_Intent(Object eS_Intent) {
		ES_Intent = eS_Intent;
	}

	public int getES_IntentAggregateScore() {
		return ES_IntentAggregateScore;
	}

	public void setES_IntentAggregateScore(int eS_IntentAggregateScore) {
		ES_IntentAggregateScore = eS_IntentAggregateScore;
	}

	public Object getES_IntentByTier() {
		return ES_IntentByTier;
	}

	public void setES_IntentByTier(Object eS_IntentByTier) {
		ES_IntentByTier = eS_IntentByTier;
	}

	public Object getES_IntentNumByTier() {
		return ES_IntentNumByTier;
	}

	public void setES_IntentNumByTier(Object eS_IntentNumByTier) {
		ES_IntentNumByTier = eS_IntentNumByTier;
	}

	public String getES_IntentStr() {
		return ES_IntentStr;
	}

	public void setES_IntentStr(String eS_IntentStr) {
		ES_IntentStr = eS_IntentStr;
	}

	public String getES_IntentTime() {
		return ES_IntentTime;
	}

	public void setES_IntentTime(String eS_IntentTime) {
		ES_IntentTime = eS_IntentTime;
	}

	public String getES_Keywords() {
		return ES_Keywords;
	}

	public void setES_Keywords(String eS_Keywords) {
		ES_Keywords = eS_Keywords;
	}

	public String getES_LinkedInUrl() {
		return ES_LinkedInUrl;
	}

	public void setES_LinkedInUrl(String eS_LinkedInUrl) {
		ES_LinkedInUrl = eS_LinkedInUrl;
	}

	public String getES_LocationID() {
		return ES_LocationID;
	}

	public void setES_LocationID(String eS_LocationID) {
		ES_LocationID = eS_LocationID;
	}

	public String getES_MatchName() {
		return ES_MatchName;
	}

	public void setES_MatchName(String eS_MatchName) {
		ES_MatchName = eS_MatchName;
	}

	public String getES_MatchReasonBuildingName() {
		return ES_MatchReasonBuildingName;
	}

	public void setES_MatchReasonBuildingName(String eS_MatchReasonBuildingName) {
		ES_MatchReasonBuildingName = eS_MatchReasonBuildingName;
	}

	public String getES_MatchReasonBuildingNumber() {
		return ES_MatchReasonBuildingNumber;
	}

	public void setES_MatchReasonBuildingNumber(String eS_MatchReasonBuildingNumber) {
		ES_MatchReasonBuildingNumber = eS_MatchReasonBuildingNumber;
	}

	public String getES_MatchReasonBusinessType() {
		return ES_MatchReasonBusinessType;
	}

	public void setES_MatchReasonBusinessType(String eS_MatchReasonBusinessType) {
		ES_MatchReasonBusinessType = eS_MatchReasonBusinessType;
	}

	public String getES_MatchReasonCity() {
		return ES_MatchReasonCity;
	}

	public void setES_MatchReasonCity(String eS_MatchReasonCity) {
		ES_MatchReasonCity = eS_MatchReasonCity;
	}

	public String getES_MatchReasonCompanyPhone() {
		return ES_MatchReasonCompanyPhone;
	}

	public void setES_MatchReasonCompanyPhone(String eS_MatchReasonCompanyPhone) {
		ES_MatchReasonCompanyPhone = eS_MatchReasonCompanyPhone;
	}

	public String getES_MatchReasonCountry() {
		return ES_MatchReasonCountry;
	}

	public void setES_MatchReasonCountry(String eS_MatchReasonCountry) {
		ES_MatchReasonCountry = eS_MatchReasonCountry;
	}

	public String getES_MatchReasonDirectional() {
		return ES_MatchReasonDirectional;
	}

	public void setES_MatchReasonDirectional(String eS_MatchReasonDirectional) {
		ES_MatchReasonDirectional = eS_MatchReasonDirectional;
	}

	public String getES_MatchReasonName() {
		return ES_MatchReasonName;
	}

	public void setES_MatchReasonName(String eS_MatchReasonName) {
		ES_MatchReasonName = eS_MatchReasonName;
	}

	public String getES_MatchReasonRoadName() {
		return ES_MatchReasonRoadName;
	}

	public void setES_MatchReasonRoadName(String eS_MatchReasonRoadName) {
		ES_MatchReasonRoadName = eS_MatchReasonRoadName;
	}

	public String getES_MatchReasonRoadType() {
		return ES_MatchReasonRoadType;
	}

	public void setES_MatchReasonRoadType(String eS_MatchReasonRoadType) {
		ES_MatchReasonRoadType = eS_MatchReasonRoadType;
	}

	public String getES_MatchReasonState() {
		return ES_MatchReasonState;
	}

	public void setES_MatchReasonState(String eS_MatchReasonState) {
		ES_MatchReasonState = eS_MatchReasonState;
	}

	public String getES_MatchReasonUnit() {
		return ES_MatchReasonUnit;
	}

	public void setES_MatchReasonUnit(String eS_MatchReasonUnit) {
		ES_MatchReasonUnit = eS_MatchReasonUnit;
	}

	public String getES_MatchReasonWebsite() {
		return ES_MatchReasonWebsite;
	}

	public void setES_MatchReasonWebsite(String eS_MatchReasonWebsite) {
		ES_MatchReasonWebsite = eS_MatchReasonWebsite;
	}

	public String getES_MatchReasonZip() {
		return ES_MatchReasonZip;
	}

	public void setES_MatchReasonZip(String eS_MatchReasonZip) {
		ES_MatchReasonZip = eS_MatchReasonZip;
	}

	public int getES_MatchScore() {
		return ES_MatchScore;
	}

	public void setES_MatchScore(int eS_MatchScore) {
		ES_MatchScore = eS_MatchScore;
	}

	public Object getES_Models() {
		return ES_Models;
	}

	public void setES_Models(Object eS_Models) {
		ES_Models = eS_Models;
	}

	public String getES_NAICS2() {
		return ES_NAICS2;
	}

	public void setES_NAICS2(String eS_NAICS2) {
		ES_NAICS2 = eS_NAICS2;
	}

	public String getES_NAICS2Description() {
		return ES_NAICS2Description;
	}

	public void setES_NAICS2Description(String eS_NAICS2Description) {
		ES_NAICS2Description = eS_NAICS2Description;
	}

	public String getES_NAICS4() {
		return ES_NAICS4;
	}

	public void setES_NAICS4(String eS_NAICS4) {
		ES_NAICS4 = eS_NAICS4;
	}

	public String getES_NAICS4Description() {
		return ES_NAICS4Description;
	}

	public void setES_NAICS4Description(String eS_NAICS4Description) {
		ES_NAICS4Description = eS_NAICS4Description;
	}

	public String getES_NAICS6() {
		return ES_NAICS6;
	}

	public void setES_NAICS6(String eS_NAICS6) {
		ES_NAICS6 = eS_NAICS6;
	}

	public String getES_NAICS6Description() {
		return ES_NAICS6Description;
	}

	public void setES_NAICS6Description(String eS_NAICS6Description) {
		ES_NAICS6Description = eS_NAICS6Description;
	}

	public String getES_Name() {
		return ES_Name;
	}

	public void setES_Name(String eS_Name) {
		ES_Name = eS_Name;
	}

	public int getES_NumLocations() {
		return ES_NumLocations;
	}

	public void setES_NumLocations(int eS_NumLocations) {
		ES_NumLocations = eS_NumLocations;
	}

	public int getES_NumSurgingTopics() {
		return ES_NumSurgingTopics;
	}

	public void setES_NumSurgingTopics(int eS_NumSurgingTopics) {
		ES_NumSurgingTopics = eS_NumSurgingTopics;
	}

	public String getES_PrimaryWebsite() {
		return ES_PrimaryWebsite;
	}

	public void setES_PrimaryWebsite(String eS_PrimaryWebsite) {
		ES_PrimaryWebsite = eS_PrimaryWebsite;
	}

	public int getES_Revenue() {
		return ES_Revenue;
	}

	public void setES_Revenue(int eS_Revenue) {
		ES_Revenue = eS_Revenue;
	}

	public String getES_RevenueBand() {
		return ES_RevenueBand;
	}

	public void setES_RevenueBand(String eS_RevenueBand) {
		ES_RevenueBand = eS_RevenueBand;
	}

	public String getES_SIC2() {
		return ES_SIC2;
	}

	public void setES_SIC2(String eS_SIC2) {
		ES_SIC2 = eS_SIC2;
	}

	public String getES_SIC2Description() {
		return ES_SIC2Description;
	}

	public void setES_SIC2Description(String eS_SIC2Description) {
		ES_SIC2Description = eS_SIC2Description;
	}

	public String getES_SIC3() {
		return ES_SIC3;
	}

	public void setES_SIC3(String eS_SIC3) {
		ES_SIC3 = eS_SIC3;
	}

	public String getES_SIC3Description() {
		return ES_SIC3Description;
	}

	public void setES_SIC3Description(String eS_SIC3Description) {
		ES_SIC3Description = eS_SIC3Description;
	}

	public String getES_SIC4() {
		return ES_SIC4;
	}

	public void setES_SIC4(String eS_SIC4) {
		ES_SIC4 = eS_SIC4;
	}

	public String getES_SIC4Description() {
		return ES_SIC4Description;
	}

	public void setES_SIC4Description(String eS_SIC4Description) {
		ES_SIC4Description = eS_SIC4Description;
	}

	public Object getES_SimilarCompanies() {
		return ES_SimilarCompanies;
	}

	public void setES_SimilarCompanies(Object eS_SimilarCompanies) {
		ES_SimilarCompanies = eS_SimilarCompanies;
	}

	public String getES_State() {
		return ES_State;
	}

	public void setES_State(String eS_State) {
		ES_State = eS_State;
	}

	public String getES_Street() {
		return ES_Street;
	}

	public void setES_Street(String eS_Street) {
		ES_Street = eS_Street;
	}

	public List<String> getES_Top5NAICS() {
		return ES_Top5NAICS;
	}

	public void setES_Top5NAICS(List<String> eS_Top5NAICS) {
		ES_Top5NAICS = eS_Top5NAICS;
	}

	public String getES_TwitterUrl() {
		return ES_TwitterUrl;
	}

	public void setES_TwitterUrl(String eS_TwitterUrl) {
		ES_TwitterUrl = eS_TwitterUrl;
	}

	public String getES_YearStarted() {
		return ES_YearStarted;
	}

	public void setES_YearStarted(String eS_YearStarted) {
		ES_YearStarted = eS_YearStarted;
	}

	public String getES_Zip() {
		return ES_Zip;
	}

	public void setES_Zip(String eS_Zip) {
		ES_Zip = eS_Zip;
	}

	public List<String> getError_messages() {
		return error_messages;
	}

	public void setError_messages(List<String> error_messages) {
		this.error_messages = error_messages;
	}

	public int getInputId() {
		return inputId;
	}

	public void setInputId(int inputId) {
		this.inputId = inputId;
	}

	public String getSearchDomain() {
		return searchDomain;
	}

	public void setSearchDomain(String searchDomain) {
		this.searchDomain = searchDomain;
	}

	public String getSalutoryDomain() {
		return salutoryDomain;
	}

	public void setSalutoryDomain(String salutoryDomain) {
		this.salutoryDomain = salutoryDomain;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPointer() {
		return pointer;
	}

	public void setPointer(String pointer) {
		this.pointer = pointer;
	}
}
