package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.utils.XtaasConstants;

/**
 * Maintains a list of call sid currently active for preview mode. These CallSids are monitored by PreviewCallMonitorJob
 * 
 * @author djain
 *
 */
@Document(collection="activepreviewcallqueue")
public class ActivePreviewCall extends AbstractEntity {
	
	private String callStatus;
	
	private String agentId;
	
	private boolean processing;
	
	private String prospectCallId;
	
	private String parentCallSid;
	
	public ActivePreviewCall(String id, String callStatus, String agentId, String prospectCallId, String parentCallSid) {
		this.setCallsid(id);
		this.setCallStatus(callStatus);
		this.setAgentId(agentId);
		this.setProspectCallId(prospectCallId);
		this.setParentCallSid(parentCallSid);
	}
	
	private void setCallsid(String callsid) {
		this.setId(callsid);
	}
	
	public String getCallsid() {
		return getId();
	}

	/**
	 * @return the callStatus
	 */
	public String getCallStatus() {
		return callStatus;
	}

	/**
	 * @param callStatus the callStatus to set
	 */
	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}
	
	public boolean isCallCompletedOrCancelled() {
		return XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(this.callStatus) || XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(this.callStatus);
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	private void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	/**
	 * @return the processing
	 */
	public boolean isProcessing() {
		return processing;
	}

	/**
	 * @param processing the processing to set
	 */
	public void setProcessing(boolean processing) {
		this.processing = processing;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	private void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the parentCallSid
	 */
	public String getParentCallSid() {
		return parentCallSid;
	}

	/**
	 * @param parentCallSid the parentCallSid to set
	 */
	private void setParentCallSid(String parentCallSid) {
		this.parentCallSid = parentCallSid;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActivePreviewCall [callStatus=");
		builder.append(callStatus);
		builder.append(", agentId=");
		builder.append(agentId);
		builder.append(", processing=");
		builder.append(processing);
		builder.append(", prospectCallId=");
		builder.append(prospectCallId);
		builder.append(", parentCallSid=");
		builder.append(parentCallSid);
		builder.append("]");
		return builder.toString();
	}
}
