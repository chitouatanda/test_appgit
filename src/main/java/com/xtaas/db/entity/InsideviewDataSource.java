package com.xtaas.db.entity;

import com.xtaas.insideview.DetailedContactJobResultInsideView;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "insideviewdatasource")
public class InsideviewDataSource {
  
  private String peopleId;
  
  private Prospect prospect;
  
  private DetailedContactJobResultInsideView detailedContact;

  public String getPeopleId() {
    return peopleId;
  }

  public void setPeopleId(String peopleId) {
    this.peopleId = peopleId;
  }

  public Prospect getProspect() {
    return prospect;
  }

  public void setProspect(Prospect prospect) {
    this.prospect = prospect;
  }

  public DetailedContactJobResultInsideView getDetailedContact() {
    return detailedContact;
  }

  public void setDetailedContact(DetailedContactJobResultInsideView detailedContact) {
    this.detailedContact = detailedContact;
  } 
  
}
