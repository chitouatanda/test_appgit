package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.AbstractEntity;
import com.xtaas.valueobjects.KeyValuePair;


@Document(collection="abmlist")
public class AbmList extends AbstractEntity {
	
	private List<KeyValuePair<String, String>> companyList;
	private String campaignId;
	private String status;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<KeyValuePair<String, String>> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(List<KeyValuePair<String, String>> companyList) {
		this.companyList = companyList;
	}
	
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	
}
