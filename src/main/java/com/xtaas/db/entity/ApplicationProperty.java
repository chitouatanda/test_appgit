/**
 * 
 */
package com.xtaas.db.entity;

import com.xtaas.valueobjects.KeyValuePair;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * A collection of application level properties key value pair
 * 
 * @author djain
 *
 */
@Document(collection="applicationproperty")
public class ApplicationProperty {
	@Id
	private String name;
	
	private String value;

	private List<KeyValuePair<String, Double>> rateLimitPriority;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public List<KeyValuePair<String, Double>> getRateLimitPriority() {
		return rateLimitPriority;
	}

	public void setRateLimitPriority(List<KeyValuePair<String, Double>> rateLimitPriority) {
		this.rateLimitPriority = rateLimitPriority;
	}
}
