package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "countrystate")
public class CountryState extends AbstractEntity {

	private String country; // country name
	private String continent;
	private String countryIso3;
	private String countryIso2;

	private String phoneCode;
	private String countryCapital;
	
	private String countryCurrency;
	private String region;
	private String subRegion;
	private String timeZoneName;
	private String timeZoneAbbr;
	private String timeZoneStdName;
	private String timeZoneOffset;



	private String countryLatitude;
	private String countryLongitude;
	private String stateLatitude;
	private String stateLongitude;
	private String cityLatitude;
	private String cityLongitude;

	private boolean countryRecord;
	private boolean stateRecord;
	private boolean cityRecord;

	private String stateName;
	private String stateCode;
	
	private String cityName;
	
	private int countryId;
	private int stateId;
	private int cityId;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public String getCountryIso3() {
		return countryIso3;
	}
	public void setCountryIso3(String countryIso3) {
		this.countryIso3 = countryIso3;
	}
	public String getCountryIso2() {
		return countryIso2;
	}
	public void setCountryIso2(String countryIso2) {
		this.countryIso2 = countryIso2;
	}
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public String getCountryCapital() {
		return countryCapital;
	}
	public void setCountryCapital(String countryCapital) {
		this.countryCapital = countryCapital;
	}
	public String getCountryCurrency() {
		return countryCurrency;
	}
	public void setCountryCurrency(String countryCurrency) {
		this.countryCurrency = countryCurrency;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getSubRegion() {
		return subRegion;
	}
	public void setSubRegion(String subRegion) {
		this.subRegion = subRegion;
	}
	public String getTimeZoneName() {
		return timeZoneName;
	}
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}
	public String getTimeZoneAbbr() {
		return timeZoneAbbr;
	}
	public void setTimeZoneAbbr(String timeZoneAbbr) {
		this.timeZoneAbbr = timeZoneAbbr;
	}
	public String getTimeZoneStdName() {
		return timeZoneStdName;
	}
	public void setTimeZoneStdName(String timeZoneStdName) {
		this.timeZoneStdName = timeZoneStdName;
	}
	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}
	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}
	public String getCountryLatitude() {
		return countryLatitude;
	}
	public void setCountryLatitude(String countryLatitude) {
		this.countryLatitude = countryLatitude;
	}
	public String getCountryLongitude() {
		return countryLongitude;
	}
	public void setCountryLongitude(String countryLongitude) {
		this.countryLongitude = countryLongitude;
	}
	public String getStateLatitude() {
		return stateLatitude;
	}
	public void setStateLatitude(String stateLatitude) {
		this.stateLatitude = stateLatitude;
	}
	public String getStateLongitude() {
		return stateLongitude;
	}
	public void setStateLongitude(String stateLongitude) {
		this.stateLongitude = stateLongitude;
	}
	public String getCityLatitude() {
		return cityLatitude;
	}
	public void setCityLatitude(String cityLatitude) {
		this.cityLatitude = cityLatitude;
	}
	public String getCityLongitude() {
		return cityLongitude;
	}
	public void setCityLongitude(String cityLongitude) {
		this.cityLongitude = cityLongitude;
	}
	public boolean isCountryRecord() {
		return countryRecord;
	}
	public void setCountryRecord(boolean countryRecord) {
		this.countryRecord = countryRecord;
	}
	public boolean isStateRecord() {
		return stateRecord;
	}
	public void setStateRecord(boolean stateRecord) {
		this.stateRecord = stateRecord;
	}
	
	public boolean isCityRecord() {
		return cityRecord;
	}
	public void setCityRecord(boolean cityRecord) {
		this.cityRecord = cityRecord;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	@Override
	public String toString() {
		return "CountryState [country=" + country + ", continent=" + continent + ", countryIso3=" + countryIso3
				+ ", countryIso2=" + countryIso2 + ", phoneCode=" + phoneCode + ", countryCapital=" + countryCapital
				+ ", countryCurrency=" + countryCurrency + ", region=" + region + ", subRegion=" + subRegion
				+ ", timeZoneName=" + timeZoneName + ", timeZoneAbbr=" + timeZoneAbbr + ", timeZoneStdName="
				+ timeZoneStdName + ", timeZoneOffset=" + timeZoneOffset + ", countryLatitude=" + countryLatitude
				+ ", countryLongitude=" + countryLongitude + ", stateLatitude=" + stateLatitude + ", stateLongitude="
				+ stateLongitude + ", cityLatitude=" + cityLatitude + ", cityLongitude=" + cityLongitude
				+ ", countryRecord=" + countryRecord + ", stateRecord=" + stateRecord + ", cityRecord=" + cityRecord
				+ ", stateName=" + stateName + ", stateCode=" + stateCode + ", cityName=" + cityName + ", countryId="
				+ countryId + ", stateId=" + stateId + ", cityId=" + cityId + "]";
	}
	
	
	


}
