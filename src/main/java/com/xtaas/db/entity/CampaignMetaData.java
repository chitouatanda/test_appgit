package com.xtaas.db.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.valueobjects.KeyValuePair;

@Document(collection = "campaignmetadata")
public class CampaignMetaData extends AbstractEntity {

	private Map<String, Float> retrySpeed;
	private boolean enableMachineAnsweredDetection;
	private int dailyCallMaxRetries;
	private int callMaxRetries;
	private boolean hidePhone;
	private boolean hideEmail;
	private boolean qaRequired;
	private boolean supervisorAgentStatusApprove;
	private boolean retrySpeedEnabled;
	private boolean localOutboundCalling;
	private int leadsPerCompany;
	private int duplicateLeadsDuration;
	private int duplicateCompanyDuration;
	private boolean multiProspectCalling;
	private boolean useSlice;
	private boolean isEmailSuppressed;
	private boolean isABM;
	private String callControlProvider;
	private boolean isNotConference;
	private String recordProspect;
	private boolean isLeadIQ;
	private boolean simpleDisposition;
	private CallSpeed callSpeedPerMinPerAgent;
	private float campaignSpeed;
	private LeadSortOrder leadSortOrder;
	private String hostingServer;
	private List<KeyValuePair<String, Integer>> dataSourcePriority;
	private List<KeyValuePair<String, Integer>> manualDataSourcePriority;
	private List<KeyValuePair<String, Integer>> similarDataSourcePriority;
	private boolean isIndirectBuyStarted;
	private boolean mdFiveSuppressionCheck;
	private String callableEvent;
	private boolean autoMachineHangup;
	private boolean ignorePrimaryIndustries;
	private boolean primaryIndustriesOnly;
	private int prospectCacheAgentRange;
	private int prospectCacheNoOfProspectsPerAgent;
	private boolean enableProspectCaching;
	private boolean removeMobilePhones;

	private String autoModeCallProvider;
	private String manualModeCallProvider;
	private boolean revealEmail;
	private int upperThreshold;
	private int lowerThreshold;
	// private boolean telnyxHold;
	private boolean hideNonSuccessPII;
	// private String holdAudioUrl;
	private boolean tagSuccess;
	private boolean enableCustomFields;
	private boolean enableInternetCheck;
	private boolean ukPhoneDncCheck;
	private boolean usPhoneDncCheck;
	private boolean callableVanished;
	private String sipProvider;
	private boolean enableCallbackFeature;

	public boolean isEnableCustomFields() {
		return enableCustomFields;
	}

	public void setEnableCustomFields(boolean enableCustomFields) {
		this.enableCustomFields = enableCustomFields;
	}

	
	
	
	public List<KeyValuePair<String, Integer>> getDataSourcePriority() {
		return dataSourcePriority;
	}

	public List<KeyValuePair<String, Integer>> getManualDataSourcePriority() {
		return manualDataSourcePriority;
	}

	public void setManualDataSourcePriority(List<KeyValuePair<String, Integer>> manualDataSourcePriority) {
		this.manualDataSourcePriority = manualDataSourcePriority;
	}

	public void setDataSourcePriority(List<KeyValuePair<String, Integer>> dataSourcePriority) {
		this.dataSourcePriority = dataSourcePriority;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public int getDailyCallMaxRetries() {
		return dailyCallMaxRetries;
	}

	public void setDailyCallMaxRetries(int dailyCallMaxRetries) {
		this.dailyCallMaxRetries = dailyCallMaxRetries;
	}

	public int getCallMaxRetries() {
		return callMaxRetries;
	}

	public void setCallMaxRetries(int callMaxRetries) {
		this.callMaxRetries = callMaxRetries;
	}

	public boolean isHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}

	public boolean isHideEmail() {
		return hideEmail;
	}

	public void setHideEmail(boolean hideEmail) {
		this.hideEmail = hideEmail;
	}

	public boolean isQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(boolean qaRequired) {
		this.qaRequired = qaRequired;
	}

	public boolean isSupervisorAgentStatusApprove() {
		return supervisorAgentStatusApprove;
	}

	public void setSupervisorAgentStatusApprove(boolean supervisorAgentStatusApprove) {
		this.supervisorAgentStatusApprove = supervisorAgentStatusApprove;
	}

	public boolean isRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(boolean retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public boolean isLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public int getLeadsPerCompany() {
		return leadsPerCompany;
	}

	public void setLeadsPerCompany(int leadsPerCompany) {
		this.leadsPerCompany = leadsPerCompany;
	}

	public int getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}

	public void setDuplicateLeadsDuration(int duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}

	public int getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}

	public void setDuplicateCompanyDuration(int duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}

	public boolean isMultiProspectCalling() {
		return multiProspectCalling;
	}

	public void setMultiProspectCalling(boolean multiProspectCalling) {
		this.multiProspectCalling = multiProspectCalling;
	}

	public boolean isUseSlice() {
		return useSlice;
	}

	public void setUseSlice(boolean useSlice) {
		this.useSlice = useSlice;
	}

	public boolean isEmailSuppressed() {
		return isEmailSuppressed;
	}

	public void setEmailSuppressed(boolean isEmailSuppressed) {
		this.isEmailSuppressed = isEmailSuppressed;
	}

	public boolean isABM() {
		return isABM;
	}

	public void setABM(boolean isABM) {
		this.isABM = isABM;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public boolean isNotConference() {
		return isNotConference;
	}

	public void setNotConference(boolean isNotConference) {
		this.isNotConference = isNotConference;
	}

	public String getRecordProspect() {
		return recordProspect;
	}

	public void setRecordProspect(String recordProspect) {
		this.recordProspect = recordProspect;
	}

	public boolean isLeadIQ() {
		return isLeadIQ;
	}

	public void setLeadIQ(boolean isLeadIQ) {
		this.isLeadIQ = isLeadIQ;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public float getCampaignSpeed() {
		return campaignSpeed;
	}

	public void setCampaignSpeed(float campaignSpeed) {
		this.campaignSpeed = campaignSpeed;
	}

	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public boolean isIndirectBuyStarted() {
		return isIndirectBuyStarted;
	}

	public void setIndirectBuyStarted(boolean isIndirectBuyStarted) {
		this.isIndirectBuyStarted = isIndirectBuyStarted;
	}

	public boolean isMdFiveSuppressionCheck() {
		return mdFiveSuppressionCheck;
	}

	public void setMdFiveSuppressionCheck(boolean mdFiveSuppressionCheck) {
		this.mdFiveSuppressionCheck = mdFiveSuppressionCheck;
	}

	public String getCallableEvent() {
		return callableEvent;
	}

	public void setCallableEvent(String callableEvent) {
		this.callableEvent = callableEvent;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}

	public boolean isIgnorePrimaryIndustries() {
		return ignorePrimaryIndustries;
	}

	public void setIgnorePrimaryIndustries(boolean ignorePrimaryIndustries) {
		this.ignorePrimaryIndustries = ignorePrimaryIndustries;
	}

	public boolean isPrimaryIndustriesOnly() {
		return primaryIndustriesOnly;
	}

	public void setPrimaryIndustriesOnly(boolean primaryIndustriesOnly) {
		this.primaryIndustriesOnly = primaryIndustriesOnly;
	}

	public int getProspectCacheAgentRange() {
		return prospectCacheAgentRange;
	}

	public void setProspectCacheAgentRange(int prospectCacheAgentRange) {
		this.prospectCacheAgentRange = prospectCacheAgentRange;
	}

	public int getProspectCacheNoOfProspectsPerAgent() {
		return prospectCacheNoOfProspectsPerAgent;
	}

	public void setProspectCacheNoOfProspectsPerAgent(int prospectCacheNoOfProspectsPerAgent) {
		this.prospectCacheNoOfProspectsPerAgent = prospectCacheNoOfProspectsPerAgent;
	}

	public boolean isEnableProspectCaching() {
		return enableProspectCaching;
	}

	public void setEnableProspectCaching(boolean enableProspectCaching) {
		this.enableProspectCaching = enableProspectCaching;
	}

	public boolean isRemoveMobilePhones() {
		return removeMobilePhones;
	}

	public void setRemoveMobilePhones(boolean removeMobilePhones) {
		this.removeMobilePhones = removeMobilePhones;
	}
	
	public String getAutoModeCallProvider() {
		return autoModeCallProvider;
	}

	public void setAutoModeCallProvider(String autoModeCallProvider) {
		this.autoModeCallProvider = autoModeCallProvider;
	}

	public String getManualModeCallProvider() {
		return manualModeCallProvider;
	}

	public void setManualModeCallProvider(String manualModeCallProvider) {
		this.manualModeCallProvider = manualModeCallProvider;
	}

	public int getUpperThreshold() {
		return upperThreshold;
	}

	public void setUpperThreshold(int upperThreshold) {
		this.upperThreshold = upperThreshold;
	}

	public int getLowerThreshold() {
		return lowerThreshold;
	}

	public void setLowerThreshold(int lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}
	
	public boolean isRevealEmail() {
		return revealEmail;
	}

	public void setRevealEmail(boolean revealEmail) {
		this.revealEmail = revealEmail;
	}
	
	public boolean isHideNonSuccessPII() {
		return hideNonSuccessPII;
	}

	public void setHideNonSuccessPII(boolean hideNonSuccessPII) {
		this.hideNonSuccessPII = hideNonSuccessPII;
	}

	public boolean isTagSuccess() {
		return tagSuccess;
	}

	public void setTagSuccess(boolean tagSuccess) {
		this.tagSuccess = tagSuccess;
	}

	public boolean isEnableInternetCheck() {
		return enableInternetCheck;
	}

	public void setEnableInternetCheck(boolean enableInternetCheck) {
		this.enableInternetCheck = enableInternetCheck;
	}

	public List<KeyValuePair<String, Integer>> getSimilarDataSourcePriority() {
		return similarDataSourcePriority;
	}

	public void setSimilarDataSourcePriority(List<KeyValuePair<String, Integer>> similarDataSourcePriority) {
		this.similarDataSourcePriority = similarDataSourcePriority;
	}

	public boolean isUkPhoneDncCheck() {
		return ukPhoneDncCheck;
	}

	public void setUkPhoneDncCheck(boolean ukPhoneDncCheck) {
		this.ukPhoneDncCheck = ukPhoneDncCheck;
	}

	public boolean isUsPhoneDncCheck() {
		return usPhoneDncCheck;
	}

	public void setUsPhoneDncCheck(boolean usPhoneDncCheck) {
		this.usPhoneDncCheck = usPhoneDncCheck;
	}

	public boolean isCallableVanished() {
		return callableVanished;
	}

	public void setCallableVanished(boolean callableVanished) {
		this.callableVanished = callableVanished;
	}
	
	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	public boolean isEnableCallbackFeature() {
		return enableCallbackFeature;
	}

	public void setEnableCallbackFeature(boolean enableCallbackFeature) {
		this.enableCallbackFeature = enableCallbackFeature;
	}

}