/**
 * 
 */
package com.xtaas.db.entity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author djain
 * 
 */
public abstract class AbstractCriteria {
	private String groupClause;
	private ArrayList<Expression> expressions;
	private ArrayList<ExpressionGroup> expressionGroups;
	// Map to collect response from Agent for each of the unique attributes
	private HashMap<String, String> uniqueAttributeMap ; 
	
	public HashMap<String, String> getUniqueAttributeMap() {
		if (this.uniqueAttributeMap != null) return this.uniqueAttributeMap;
		
		this.uniqueAttributeMap = new HashMap<String, String>();
		//traverse expressions list/expression Groups and add attributes to set
		this.uniqueAttributeMap.putAll(getExpressionAttributes(expressions));
		this.uniqueAttributeMap.putAll(getExpressionGroupAttributes(expressionGroups));
		
		return this.uniqueAttributeMap;
	}
	
	private HashMap<String, String> getExpressionAttributes(ArrayList<Expression> expressions) {
		HashMap<String, String> attributeMap = new HashMap<String, String>();
		if (expressions == null) return attributeMap;
		for (Expression expr : expressions) {
			attributeMap.put(expr.getAttribute(), null);
		}
		return attributeMap;
	}
	
	private HashMap<String, String> getExpressionGroupAttributes(ArrayList<ExpressionGroup> expressionGroups) {
		HashMap<String, String> attributeMap = new HashMap<String, String>();
		if (null == expressionGroups) return attributeMap;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.putAll(getExpressionAttributes(exprGroup.getExpressions()));
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.putAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return attributeMap;
	}
	
	/**
	 * @return the groupClause
	 */
	public String getGroupClause() {
		return groupClause;
	}

	/**
	 * @param groupClause
	 *            the groupClause to set
	 */
	public void setGroupClause(String groupClause) {
		this.groupClause = groupClause;
	}

	/**
	 * @return the expressions
	 */
	public ArrayList<Expression> getExpressions() {
		return expressions;
	}

	/**
	 * @param expressions
	 *            the expressions to set
	 */
	public void setExpressions(ArrayList<Expression> expressions) {
		this.expressions = expressions;
	}

	/**
	 * @return the expressionGroups
	 */
	public ArrayList<ExpressionGroup> getExpressionGroups() {
		return expressionGroups;
	}

	/**
	 * @param expressionGroups
	 *            the expressionGroups to set
	 */
	public void setExpressionGroups(ArrayList<ExpressionGroup> expressionGroups) {
		this.expressionGroups = expressionGroups;
	}

}
