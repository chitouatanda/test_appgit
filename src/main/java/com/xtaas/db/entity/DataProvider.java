package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dataprovider")
public class DataProvider extends AbstractEntity {

	private String dataProvider;
	private String sourceIndex;

	public DataProvider(String dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(String dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getSourceIndex() {
		return sourceIndex;
	}

	public void setSourceIndex(String sourceIndex) {
		this.sourceIndex = sourceIndex;
	}
}