package com.xtaas.db.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author pranay
 *
 */
@Document(collection = "feedbackjobtracker")
public class FeedbackJobTracker extends AbstractEntity {

	public enum JobStatus {
		BACKUP_INPROCESS, REPORTING_INPROCESS, BACKUP_COMPLETED, REPORTING_COMPLETED, BACKUP_ERROR, REPORTING_ERROR
	}

	private String jobId;
	private JobStatus status;
	private Date reportingJobStartTime;
	private Date reportingJobEndTime;
	private Date backupJobStartTime;
	private Date backupJobEndTime;
	private String serverUrl;
	private String errorMsg;

	protected FeedbackJobTracker() {
	}

	public FeedbackJobTracker(String jobId, JobStatus status, String serverUrl) {
		super();
		this.jobId = jobId;
		this.status = status;
		this.serverUrl = serverUrl;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public JobStatus getStatus() {
		return status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	public Date getReportingJobStartTime() {
		return reportingJobStartTime;
	}

	public void setReportingJobStartTime(Date reportingJobStartTime) {
		this.reportingJobStartTime = reportingJobStartTime;
	}

	public Date getReportingJobEndTime() {
		return reportingJobEndTime;
	}

	public void setReportingJobEndTime(Date reportingJobEndTime) {
		this.reportingJobEndTime = reportingJobEndTime;
	}

	public Date getBackupJobStartTime() {
		return backupJobStartTime;
	}

	public void setBackupJobStartTime(Date backupJobStartTime) {
		this.backupJobStartTime = backupJobStartTime;
	}

	public Date getBackupJobEndTime() {
		return backupJobEndTime;
	}

	public void setBackupJobEndTime(Date backupJobEndTime) {
		this.backupJobEndTime = backupJobEndTime;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	// chain
	public FeedbackJobTracker withJobId(String jobId) {
		this.jobId = jobId;
		return this;
	}

	public FeedbackJobTracker withStatus(JobStatus status) {
		this.status = status;
		return this;
	}

	public FeedbackJobTracker withReportingJobStartTime(Date reportingJobStartTime) {
		this.reportingJobStartTime = reportingJobStartTime;
		return this;
	}

	public FeedbackJobTracker withReportingJobEndTime(Date reportingJobEndTime) {
		this.reportingJobEndTime = reportingJobEndTime;
		return this;
	}

	public FeedbackJobTracker withBackupJobStartTime(Date backupJobStartTime) {
		this.backupJobStartTime = backupJobStartTime;
		return this;
	}

	public FeedbackJobTracker withBackupJobEndTime(Date backupJobEndTime) {
		this.backupJobEndTime = backupJobEndTime;
		return this;
	}

	public FeedbackJobTracker withServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
		return this;
	}

	public FeedbackJobTracker withErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
		return this;
	}

}
