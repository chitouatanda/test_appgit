/**
 * 
 */
package com.xtaas.db.entity;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * @author djain
 *
 */
public class QualificationCriteria extends ExpressionGroup {
	private static final Logger logger = LoggerFactory.getLogger(QualificationCriteria.class);
	
	private HashMap<String, String> uniqueAttributeMap ;
	
	public HashMap<String, String> getUniqueAttributeMap() {
		if (this.uniqueAttributeMap != null) return this.uniqueAttributeMap;
		
		this.uniqueAttributeMap = new HashMap<String, String>();
		//traverse expressions list/expression Groups and add attributes to set
		this.uniqueAttributeMap.putAll(getExpressionAttributes(getExpressions()));
		this.uniqueAttributeMap.putAll(getExpressionGroupAttributes(getExpressionGroups()));
		
		return this.uniqueAttributeMap;
	}

	private HashMap<String, String> getExpressionAttributes(ArrayList<Expression> expressions) {
		HashMap<String, String> attributeMap = new HashMap<String, String>();
		if (expressions == null) return attributeMap;
		for (Expression expr : expressions) {
			attributeMap.put(expr.getAttribute(), null);
		}
		return attributeMap;
	}
	
	private HashMap<String, String> getExpressionGroupAttributes(ArrayList<ExpressionGroup> expressionGroups) {
		HashMap<String, String> attributeMap = new HashMap<String, String>();
		if (null == expressionGroups) return attributeMap;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.putAll(getExpressionAttributes(exprGroup.getExpressions()));
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.putAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return attributeMap;
	}
	
	public boolean isProspectQualify(Prospect prospect, HashMap<String, CRMAttribute> prospectAttributeMap) {
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext simpleContext = new StandardEvaluationContext(prospect);
		
		String springExpression = toSpringExpression(prospectAttributeMap);
		boolean result = parser.parseExpression(springExpression).getValue(simpleContext, Boolean.class);
			
		logger.debug("evaluate() : " + springExpression + " Result : " + result);
		return result;
	}
	
	public String toSpringExpression(HashMap<String, CRMAttribute> prospectAttributeMap) {
		//build Spel expression from Qualification Expression
        StringBuilder springExpression = buildSpringExpression(getExpressions(), prospectAttributeMap, getGroupClause());
        StringBuilder springExpressionFromGroup = buildSpringExpressionFromGroup(getExpressionGroups(), prospectAttributeMap, getGroupClause());
		
        StringBuilder completeSpringExpression = new StringBuilder();
        if (springExpression != null) {
        	completeSpringExpression.append(springExpression);
        	if (springExpressionFromGroup != null) {
        		completeSpringExpression.append(" ").append(getGroupClause()).append(" ").append(springExpressionFromGroup).append(" ");
        	}
        } else {
        	if (springExpressionFromGroup != null) {
        		completeSpringExpression.append(springExpressionFromGroup);
        	}
        }
        
		String spelExpression = StringUtils.replace(completeSpringExpression.toString(), "Prospect.", "");
		logger.debug("toSpringExpression() : " + spelExpression);
		return spelExpression;
	}
	
	private StringBuilder buildSpringExpressionFromGroup(ArrayList<ExpressionGroup> expressionGroupList, HashMap<String, CRMAttribute> prospectAttributeMap, String groupClause) {
		if (expressionGroupList == null) return null;
		StringBuilder springGroupExpr = new StringBuilder();
		
		for (ExpressionGroup expressionGroup : expressionGroupList) {
			if (springGroupExpr.length() > 0) springGroupExpr.append(" ").append(groupClause).append(" "); //join multiple expressionGroups with the group clause
			springGroupExpr.append("("); //enclose entire expression group into a pair of brackets
			springGroupExpr.append(buildSpringExpression(expressionGroup.getExpressions(), prospectAttributeMap, expressionGroup.getGroupClause()));
			if (expressionGroup.getExpressionGroups() != null && expressionGroup.getExpressionGroups().size() > 0) {
				springGroupExpr.append(" ").append(expressionGroup.getGroupClause()).append(" ");
				springGroupExpr.append(buildSpringExpressionFromGroup(expressionGroup.getExpressionGroups(), prospectAttributeMap, expressionGroup.getGroupClause()));
			}
			springGroupExpr.append(")");
		}
		
		return springGroupExpr;
	}
	
	private StringBuilder buildSpringExpression(ArrayList<Expression> expressionList, HashMap<String, CRMAttribute> prospectAttributeMap, String groupClause) {
		if (expressionList == null) return null;
		StringBuilder springExpr = new StringBuilder();
		
		for (Expression qualificationExpression : expressionList) {
			//if not the first condition then append group clause
			if (springExpr.length() > 0) springExpr.append(" ").append(groupClause).append(" ");
			
			//find if attribute is a standard attribute or custom attribute
			CRMAttribute prospectAttribute = prospectAttributeMap.get(qualificationExpression.getAttribute());
			if (prospectAttribute.getProspectBeanPath() != null && !prospectAttribute.getProspectBeanPath().toLowerCase().contains("customattribute")) {
				//it is a standard attribute
				springExpr.append(prospectAttribute.getProspectBeanPath());
			} else {
				//it is a custom attribute
				//check the attribute data type. 
				//if int or any sort of numeric data type then cast the object to Java Integer object
				if (prospectAttribute.getSystemDataType().equals("double")) { 
					springExpr.append(" new Double(CustomAttributes['" + prospectAttribute.getCrmName() + "'])");
				} else { //else "string" data type
					springExpr.append(" CustomAttributes['" + prospectAttribute.getCrmName() + "']");
				}
			}
			
			if (qualificationExpression.getOperator().equalsIgnoreCase("IN")) {
				String inOperatorValues = qualificationExpression.getValue().replace("(", "").replace(")", "").replace(",", "|"); //converting values to regex format
				springExpr.append(" matches '").append("(?i:").append(inOperatorValues).append(")").append("'"); //case insensitive
			} else if(qualificationExpression.getOperator().equalsIgnoreCase("LIKE")) {
				String likeOperatorValues = qualificationExpression.getValue().replace("%", "[A-Za-z0-9]*");
				springExpr.append(" matches ").append(likeOperatorValues);
			} else if (qualificationExpression.getOperator().matches("=|!=|>|>=|<|<=")) {
				if (qualificationExpression.getOperator().equals("=")) {
					if (prospectAttribute.getSystemDataType().equals("string")) {
						springExpr.append(" matches '").append("(?i:").append(qualificationExpression.getValue()).append(")").append("'"); //case insenstive regex
					} else {
						springExpr.append(" == ");
					}
				} else {
					springExpr.append(" ").append(qualificationExpression.getOperator()).append(" ");
					springExpr.append(qualificationExpression.getValue());
				}
				
			}
		} //end for
		return springExpr;
	}
}
