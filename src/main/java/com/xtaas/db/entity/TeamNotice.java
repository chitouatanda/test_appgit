package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.NoticePriority;
import com.xtaas.domain.valueobject.NoticeType;

@Document(collection="teamnotice")
public class TeamNotice extends AbstractEntity{
	
	private NoticeType type;
	
	private String notice;
	
	private NoticePriority priority;
	
	private Date startDate;
	
	private Date endDate;
	
	private List<String> forTeams;  //list of teamIds, in case it needs to be published to entire team
	
	private List<String> forResources;  //list of agentIds or qaIds, in case it needs to be published to a bunch of resources 
	
	private String requestorId;
	
	private boolean isRead;

	/**
	 * @return the type
	 */
	public NoticeType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(NoticeType type) {
		this.type = type;
	}

	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/**
	 * @return the priority
	 */
	public NoticePriority getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(NoticePriority priority) {
		this.priority = priority;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the forTeams
	 */
	public List<String> getForTeams() {
		return forTeams;
	}

	/**
	 * @param forTeams the forTeams to set
	 */
	public void setForTeams(List<String> forTeams) {
		this.forTeams = forTeams;
	}

	/**
	 * @return the forResources
	 */
	public List<String> getForResources() {
		return forResources;
	}

	/**
	 * @param forResources the forResources to set
	 */
	public void setForResources(List<String> forResources) {
		this.forResources = forResources;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	
}
