package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.valueobjects.KeyValuePair;

@Document(collection = "datasufficiencyresult")
public class DataSufficiencyResult {

	private String id;
	private String campaignId;
	private String campaignName;
	private String criteria;
	private Long total;
	private List<KeyValuePair<String, Integer>> directTotalResult;
	private List<KeyValuePair<String, Integer>> directActualResult;
	private List<KeyValuePair<String, Integer>> indirectTotalResult;
	private List<KeyValuePair<String, Integer>> indirectActualResult;
	private Long actualTotal;
	private Long directDailsToSuccess;
	private Long inDirectDailsToSuccess;
	private Long leadsFromDirect;
	private Long leadsFromInDirect;
	private Long estimatedDeliveryTotal;
	

	

	

	public Long getLeadsFromDirect() {
		return leadsFromDirect;
	}

	public void setLeadsFromDirect(Long leadsFromDirect) {
		this.leadsFromDirect = leadsFromDirect;
	}

	public Long getLeadsFromInDirect() {
		return leadsFromInDirect;
	}

	public void setLeadsFromInDirect(Long leadsFromInDirect) {
		this.leadsFromInDirect = leadsFromInDirect;
	}

	public Long getEstimatedDeliveryTotal() {
		return estimatedDeliveryTotal;
	}

	public void setEstimatedDeliveryTotal(Long estimatedDeliveryTotal) {
		this.estimatedDeliveryTotal = estimatedDeliveryTotal;
	}

	public Long getDirectDailsToSuccess() {
		return directDailsToSuccess;
	}

	public void setDirectDailsToSuccess(Long directDailsToSuccess) {
		this.directDailsToSuccess = directDailsToSuccess;
	}

	public Long getInDirectDailsToSuccess() {
		return inDirectDailsToSuccess;
	}

	public void setInDirectDailsToSuccess(Long inDirectTotalDailsToSuccess) {
		this.inDirectDailsToSuccess = inDirectTotalDailsToSuccess;
	}

	public Long getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Long actualTotal) {
		this.actualTotal = actualTotal;
	}

	public List<KeyValuePair<String, Integer>> getDirectTotalResult() {
		return directTotalResult;
	}

	public void setDirectTotalResult(
			List<KeyValuePair<String, Integer>> directTotalResult) {
		this.directTotalResult = directTotalResult;
	}

	public List<KeyValuePair<String, Integer>> getDirectActualResult() {
		return directActualResult;
	}

	public void setDirectActualResult(
			List<KeyValuePair<String, Integer>> directActualResult) {
		this.directActualResult = directActualResult;
	}

	public List<KeyValuePair<String, Integer>> getIndirectTotalResult() {
		return indirectTotalResult;
	}

	public void setIndirectTotalResult(
			List<KeyValuePair<String, Integer>> indirectTotalResult) {
		this.indirectTotalResult = indirectTotalResult;
	}

	public List<KeyValuePair<String, Integer>> getIndirectActualResult() {
		return indirectActualResult;
	}

	public void setIndirectActualResult(
			List<KeyValuePair<String, Integer>> indirectActualResult) {
		this.indirectActualResult = indirectActualResult;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	
	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	
	

}
