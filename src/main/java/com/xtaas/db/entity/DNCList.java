/**
 * 
 */
package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Application level DNC List
 * 
 * @author djain
 *
 */
@Document(collection="dnclist")
public class DNCList extends AbstractEntity {
	public enum DNCTrigger {
		CALL, DIRECT
	}
	
	private String phoneNumber; //phone number in DNC list
	private String note; //note related to this number addition to IDNC
	private String partnerId;
	private String campaignId;
	private String prospectCallId;
	private String firstName;
	private String lastName;
	private String recordingUrl;
	private String recordingUrlAws;
	private DNCTrigger trigger; //what event triggered addition to DNC List
	private String domain;
	private String company;
	private String dncLevel;
	private boolean prospectDNC;
	private boolean companyDNC;
	
	protected DNCList() {}

	public DNCList(String phoneNumber, DNCTrigger trigger) {
		setPhoneNumber(phoneNumber);
		setTrigger(trigger);
	}
	
	public DNCList(String phoneNumber, String firstName, String lastName, DNCTrigger trigger) {
		setPhoneNumber(phoneNumber);
		setFirstName(firstName);
		setLastName(lastName);
		setTrigger(trigger);
	}
	
	public DNCList(String firstName, String lastName, String phoneNumber, String organization) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setPhoneNumber(phoneNumber);
		this.setPartnerId(organization);
	}
	
	public DNCList(String firstName, String lastName, String phoneNumber, String organization, String company,
			String domain, String dncLevel, DNCTrigger trigger) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setPhoneNumber(phoneNumber);
		this.setPartnerId(organization);
		this.setCompany(company);
		this.setDomain(domain);
		this.setDncLevel(dncLevel);
		this.setTrigger(trigger);
		if (dncLevel != null && !dncLevel.isEmpty() && dncLevel.equalsIgnoreCase("COMPANY")) {
			this.setCompanyDNC(true);
		} else {
			this.setProspectDNC(true);
		}
	}
		
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	private void setPhoneNumber(String phoneNumber) {
//		if (phoneNumber.length() < 10) {
//			throw new IllegalArgumentException("Phone Number must be a 10 digit number");
//		}
		if (phoneNumber != null && !phoneNumber.matches("\\d*")) { //phone number contains any other character than digits
			throw new IllegalArgumentException("Phone Number must contain only digits");
		}
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	/**
	 * @return the partnerId
	 */
	public String getPartnerId() {
		return partnerId;
	}

	/**
	 * @param partnerId the partnerId to set
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the recordingUrl
	 */
	public String getRecordingUrl() {
		return recordingUrl;
	}

	/**
	 * @param recordingUrl the recordingUrl to set
	 */
	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}
	
	/**
	 * @return the recordingUrlAws
	 */
	public String getRecordingUrlAws() {
		return recordingUrlAws;
	}

	/**
	 * @param recordingUrlAws the recordingUrlAws to set
	 */
	public void setRecordingUrlAws(String recordingUrlAws) {
		this.recordingUrlAws = recordingUrlAws;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DNCList [phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", note=");
		builder.append(note);
		builder.append(", trigger=");
		builder.append(trigger);
		builder.append(", partnerId=");
		builder.append(partnerId);
		builder.append(", campaignId=");
		builder.append(campaignId);
		builder.append(", prospectCallId=");
		builder.append(prospectCallId);
		builder.append(", recordingUrl=");
		builder.append(recordingUrl);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the trigger
	 */
	public DNCTrigger getTrigger() {
		return trigger;
	}

	/**
	 * @param trigger the trigger to set
	 */
	private void setTrigger(DNCTrigger trigger) {
		if (trigger == null) {
			throw new IllegalArgumentException("Trigger for DNC is required : [DIRECT, CALL]");
		}
		this.trigger = trigger;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDncLevel() {
		return dncLevel;
	}

	public void setDncLevel(String dncLevel) {
		this.dncLevel = dncLevel;
	}

	public boolean isProspectDNC() {
		return prospectDNC;
	}

	public void setProspectDNC(boolean prospectDNC) {
		this.prospectDNC = prospectDNC;
	}

	public boolean isCompanyDNC() {
		return companyDNC;
	}

	public void setCompanyDNC(boolean companyDNC) {
		this.companyDNC = companyDNC;
	}
	
}
