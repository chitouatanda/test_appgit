package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.web.dto.TpsDncResponseDTO;

@Document(collection = "dncuktps")
public class DncUkTps extends AbstractEntity {

	private String phoneFormatStatus;
	private String error;
	private String telNo;
	private String national;
	private String international;
	private String lineType;
	private String location;
	private String originalCarrier;
	private String onTps;
	private String onCtps;
	private String checkDate;
	private String originalCarrierAllocationDate;
	private String remainingCredits;
	private boolean isDNC;

	protected DncUkTps() {
	}

	public DncUkTps(TpsDncResponseDTO dto) {
		setPhoneFormatStatus(dto.getPhoneFormatStatus());
		setError(dto.getError());
		if (dto.getData() != null) {
			setTelNo(dto.getData().getTelNo());
			setNational(dto.getData().getNational());
			setInternational(dto.getData().getInternational());
			setLineType(dto.getData().getLineType());
			setLocation(dto.getData().getLocation());
			setOriginalCarrier(dto.getData().getOriginalCarrier());
			setOnTps(dto.getData().getOnTps());
			setOnCtps(dto.getData().getOnCtps());
			setCheckDate(dto.getData().getCheckDate());
			setOriginalCarrierAllocationDate(dto.getData().getOriginalCarrierAllocationDate());
			setRemainingCredits(dto.getData().getRemainingCredits());
		}
		setDNC(dto.isDNC());
	}

	public String getPhoneFormatStatus() {
		return phoneFormatStatus;
	}

	public void setPhoneFormatStatus(String phoneFormatStatus) {
		this.phoneFormatStatus = phoneFormatStatus;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getNational() {
		return national;
	}

	public void setNational(String national) {
		this.national = national;
	}

	public String getInternational() {
		return international;
	}

	public void setInternational(String international) {
		this.international = international;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOriginalCarrier() {
		return originalCarrier;
	}

	public void setOriginalCarrier(String originalCarrier) {
		this.originalCarrier = originalCarrier;
	}

	public String getOnTps() {
		return onTps;
	}

	public void setOnTps(String onTps) {
		this.onTps = onTps;
	}

	public String getOnCtps() {
		return onCtps;
	}

	public void setOnCtps(String onCtps) {
		this.onCtps = onCtps;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getOriginalCarrierAllocationDate() {
		return originalCarrierAllocationDate;
	}

	public void setOriginalCarrierAllocationDate(String originalCarrierAllocationDate) {
		this.originalCarrierAllocationDate = originalCarrierAllocationDate;
	}

	public String getRemainingCredits() {
		return remainingCredits;
	}

	public void setRemainingCredits(String remainingCredits) {
		this.remainingCredits = remainingCredits;
	}

	public boolean isDNC() {
		return isDNC;
	}

	public void setDNC(boolean isDNC) {
		this.isDNC = isDNC;
	}

	@Override
	public String toString() {
		return "DncUkTps [phoneFormatStatus=" + phoneFormatStatus + ", error=" + error + ", telNo=" + telNo
				+ ", national=" + national + ", international=" + international + ", lineType=" + lineType
				+ ", location=" + location + ", originalCarrier=" + originalCarrier + ", onTps=" + onTps + ", onCtps="
				+ onCtps + ", checkDate=" + checkDate + ", originalCarrierAllocationDate="
				+ originalCarrierAllocationDate + ", remainingCredits=" + remainingCredits + ", isDNC=" + isDNC + "]";
	}

}
