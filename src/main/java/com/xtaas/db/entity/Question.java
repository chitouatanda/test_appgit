package com.xtaas.db.entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 5/11/14
 * Time: 1:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class Question extends AbstractEntity {
    private String question;
    private List<String> choices;

    @Override
    public String toString() {
        return "Question{" +
          "question='" + question + '\'' +
          ", choices=" + choices +
          "} " + super.toString();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }
}
