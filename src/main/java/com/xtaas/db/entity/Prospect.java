package com.xtaas.db.entity;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.web.dto.LeadIQResult;
import com.xtaas.web.dto.ProspectCallDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Prospect implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final String SECRET_KEY = "KqBJ0i0pvi5e2gZzPV4eLg==";
	private static final byte[] decodedKey = Base64.getDecoder().decode(SECRET_KEY);
	// rebuild key using SecretKeySpec
	private static final SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

	private String source;
	private String sourceId;
	private String campaignContactId;
	private String status;
	private String prefix;
	private String firstName;
	private String lastName;
	private String suffix;
	private String company;
	private String title; // C, VP, DIR, MGR, OTHER
	private String department; // Marketing, Finance, Legal
	private String inputDepartment;
	private String phone;
	private String companyPhone;
	private String industry;
	private List<String> industryList;
	private String email;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String zipCode;
	private String country;
	private String timeZone;
	private String extension;
	private String stateCode; // 2 letter code -- must field
	private boolean optedIn;// default false
	private boolean directPhone;
	private boolean isEu;
	private Map<String, Object> customAttributes = new HashMap<String, Object>();
	private String sourceCompanyId;
	private String managementLevel;
	private List<String> topLevelIndustries;
	private Map<String, String> companyAddress;
	private boolean isEncrypted;
	private String zoomCompanyUrl;
	private String zoomPersonUrl;
	private String prospectType;
	private List<String> companySIC;
	private List<String> companyNAICS;
	private String sourceType;
	private String phoneType;
	private boolean emailSent;
	private boolean emailValidated;
	private Date lastUpdatedDate;;
	private LeadIQResult leadIQResult;
	private String confidenceScore;
	private String dataSource;
	private String companyId;

	private String linkedInURL;
	private List<AgentQaAnswer> customFields;
	private String insideViewPurchaseDetailsId;
	private String agentDialedExtension;

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public boolean getIsEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	// Prospect maintenance fields -- used by platform. TODO: NO LONGER USED, clean
	// up after checking its reference
	private Long callbackTimeInMS; // if lead is not available to pick up OR cannot be called due to call hour
																	// restriction/holidays etc, then set callback time.

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Prospect [ source=");
		builder.append(source);
		builder.append(", sourceId=");
		builder.append(sourceId);
		builder.append(", campaignContactId=");
		builder.append(campaignContactId);
		builder.append(", status=");
		builder.append(status);
		builder.append(", prefix=");
		builder.append(prefix);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", suffix=");
		builder.append(suffix);
		builder.append(", company=");
		builder.append(company);
		builder.append(", sourceCompanyId=");
		builder.append(sourceCompanyId);
		builder.append(", title=");
		builder.append(title);
		builder.append(", department=");
		builder.append(department);
		builder.append(", inputDepartment=");
		builder.append(inputDepartment);
		builder.append(", managementLevel=");
		builder.append(managementLevel);
		builder.append(", topLevelIndustries=");
		builder.append(topLevelIndustries);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", companyPhone=");
		builder.append(companyPhone);
		builder.append(", extension=");
		builder.append(extension);
		builder.append(", industry=");
		builder.append(industry);
		builder.append(", email=");
		builder.append(email);
		builder.append(", addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", city=");
		builder.append(city);
		builder.append(", zipCode=");
		builder.append(zipCode);
		builder.append(", country=");
		builder.append(country);
		builder.append(", stateCode=");
		builder.append(stateCode);
		builder.append(", customAttributes=");
		builder.append(customAttributes);
		builder.append(", callbackTimeInMS=");
		builder.append(callbackTimeInMS);
		builder.append(", optedIn=");
		builder.append(optedIn);
		builder.append(", directPhone=");
		builder.append(directPhone);
		builder.append(", isEu=");
		builder.append(isEu);
		builder.append(", timeZone=");
		builder.append(timeZone);
		builder.append(", zoomCompanyUrl=");
		builder.append(zoomCompanyUrl);
		builder.append(", zoomPersonUrl=");
		builder.append(zoomPersonUrl);
		builder.append(", emailSet=");
		builder.append(emailSent);
		builder.append(", lastUpdatedDate=");
		builder.append(lastUpdatedDate);
		builder.append(", emailValidated=");
		builder.append(emailValidated);
		builder.append("]");
		return builder.toString();
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @return the campaignContactId
	 */
	public String getCampaignContactId() {
		return campaignContactId;
	}

	/**
	 * @param campaignContactId the campaignContactId to set
	 */
	public void setCampaignContactId(String campaignContactId) {
		this.campaignContactId = campaignContactId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrefix() {
		return prefix;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		if (this.isEncrypted)
			return decryptText(firstName);
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (this.isEncrypted)
			this.firstName = encryptText(firstName);
		else
			this.firstName = firstName;
	}

	public String getLastName() {
		if (this.isEncrypted)
			return decryptText(lastName);
		return lastName;
	}

	public void setLastName(String lastName) {
		if (this.isEncrypted)
			this.lastName = encryptText(lastName);
		else
			this.lastName = lastName;
	}

	public String getName() {
		String name = this.firstName.trim();
		if (this.isEncrypted)
			name = decryptText(this.firstName.trim());
		if (this.lastName != null) {
			if (this.isEncrypted) {
				name = name + " " + decryptText(this.lastName.trim());
			} else {
				name = name + " " + this.lastName.trim();
			}
		}
		return name;
	}

	public boolean isNameEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getName() != null && newProspect.getName() != null && this.getName().equalsIgnoreCase(newProspect.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isEmailEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getEmail() != null && newProspect.getEmail() != null && this.getEmail().equalsIgnoreCase(newProspect.getEmail())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isDepartmentEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getDepartment() != null && newProspect.getDepartment() != null && this.getDepartment().equalsIgnoreCase(newProspect.getDepartment())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isCompanyEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getCompany() != null && newProspect.getCompany() != null && this.getCompany().equalsIgnoreCase(newProspect.getCompany())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isRoleEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getTitle() != null && newProspect.getTitle() != null && this.getTitle().equalsIgnoreCase(newProspect.getTitle())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isDomainEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getCustomAttributeValue("domain") != null && newProspect.getCustomAttributeValue("domain") != null) {
			if (this.getCustomAttributeValue("domain").toString().equalsIgnoreCase(newProspect.getCustomAttributeValue("domain").toString())) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public boolean isExtensionEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getExtension() != null && newProspect.getExtension() != null && this.getExtension().equalsIgnoreCase(newProspect.getExtension())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isAgentDialedExtensionEquals(Prospect newProspect) {
		if (newProspect == null)
			return false;
		if (this.getAgentDialedExtension() != null && newProspect.getAgentDialedExtension() != null && this.getAgentDialedExtension().equalsIgnoreCase(newProspect.getAgentDialedExtension())) {
			return true;
		} else {
			return false;
		}
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		if (this.isEncrypted)
			return decryptText(phone);
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		if (this.isEncrypted)
			this.phone = encryptText(phone);
		else
			this.phone = phone;
	}

	/**
	 * @return the companyPhone
	 */
	public String getCompanyPhone() {
		if (this.isEncrypted)
			return decryptText(companyPhone);
		return companyPhone;
	}

	/**
	 * @param companyPhone the companyPhone to set
	 */
	public void setCompanyPhone(String companyPhone) {
		if (this.isEncrypted)
			this.companyPhone = encryptText(companyPhone);
		else
			this.companyPhone = companyPhone;
	}

	/**
	 * @return the callbackInMS
	 */
	public Long getCallbackTimeInMS() {
		return callbackTimeInMS;
	}

	/**
	 * @param callbackTimeInMS the callbackTimeInMS to set
	 */
	public void setCallbackTimeInMS(Long callbackTimeInMS) {
		this.callbackTimeInMS = callbackTimeInMS;
	}

	public boolean isOptedIn() {
		return optedIn;
	}

	public void setOptedIn(boolean optedIn) {
		this.optedIn = optedIn;
	}

	public boolean isDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(boolean directPhone) {
		this.directPhone = directPhone;
	}

	public boolean getIsEu() {
		return isEu;
	}

	public void setEu(boolean isEu) {
		this.isEu = isEu;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateCode() {
		if (this.stateCode == null)
			return null;
		return this.stateCode.trim();
	}

	/**
	 * @return the customAttributes
	 */
	public Map<String, Object> getCustomAttributes() {
		return customAttributes;
	}

	/**
	 * @param customAttributes the customAttributes to set
	 */
	public void setCustomAttributes(Map<String, Object> customAttributes) {
		this.customAttributes = customAttributes;
	}

	public Object getCustomAttributeValue(String attributeKey) {
		return customAttributes.get(attributeKey);
	}

	public void setCustomAttribute(String attributeKey, String attributeVal) {
		this.customAttributes.put(attributeKey, attributeVal);
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		if (this.isEncrypted)
			return decryptText(email);
		return email;

	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		if (this.isEncrypted)
			this.email = encryptText(email);
		else
			this.email = email;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @param inputDepartment the inputDepartment to set
	 */
	public void setInputDepartment(String inputDepartment) {
		this.inputDepartment = inputDepartment;
	}

	/**
	 * @return the inputDepartment
	 */
	public String getInputDepartment() {
		return inputDepartment;
	}

	/**
	 * @return the industryList
	 */
	public List<String> getIndustryList() {
		return industryList;
	}

	/**
	 * @param industryList the industryList to set
	 */
	public void setIndustryList(List<String> industryList) {
		this.industryList = industryList;
	}

	public String getSourceCompanyId() {
		return sourceCompanyId;
	}

	public void setSourceCompanyId(String sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}

	public String getManagementLevel() {
		return managementLevel;
	}

	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}

	public List<String> getTopLevelIndustries() {
		return topLevelIndustries;
	}

	public void setTopLevelIndustries(List<String> topLevelIndustries) {
		this.topLevelIndustries = topLevelIndustries;
	}

	/**
	 * Encrypts plainText in AES using the secret key
	 * 
	 * @param plainText
	 * @param secKey
	 * @return
	 * @throws Exception
	 */
	public String encryptText(String plainText) {
		if (plainText == null || plainText.isEmpty()) {
			return plainText;
		}
		String enText = "";
		try {
			// AES defaults to AES/ECB/PKCS5Padding in Java 7
			Cipher aesCipher = Cipher.getInstance("AES");

			aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

			byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
			enText = Base64.getEncoder().encodeToString(byteCipherText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "XTAAS-" + enText;

	}

	/**
	 * Decrypts encrypted byte array using the key used for encryption.
	 * 
	 * @param byteCipherText
	 * @param secKey
	 * @return
	 * @throws Exception
	 */
	public String decryptText(String cipherText) {
		if (cipherText == null || cipherText.isEmpty()) {
			return cipherText;
		}
		String deText = "";
		try {
			if (cipherText.startsWith("XTAAS-")) {
				cipherText = cipherText.substring(6);
				// AES defaults to AES/ECB/PKCS5Padding in Java 7
				Cipher aesCipher = Cipher.getInstance("AES");
				aesCipher.init(Cipher.DECRYPT_MODE, secKey);
				byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(cipherText));
				deText = new String(bytePlainText);
			} else {
				deText = cipherText;
			}
			/*
			 * byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
			 * return new String(original);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return deText;

	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public String getProspectType() {
		return prospectType;
	}

	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	public List<String> getCompanySIC() {
		return companySIC;
	}

	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}

	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}

	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	public boolean isEmailValidated() {
		return emailValidated;
	}

	public void setEmailValidated(boolean emailValidated) {
		this.emailValidated = emailValidated;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public LeadIQResult getLeadIQResult() {
		return leadIQResult;
	}

	public void setLeadIQResult(LeadIQResult leadIQResult) {
		this.leadIQResult = leadIQResult;
	}

	public String getConfidenceScore() {
		return confidenceScore;
	}

	public void setConfidenceScore(String confidenceScore) {
		this.confidenceScore = confidenceScore;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getLinkedInURL() {
		return linkedInURL;
	}

	public void setLinkedInURL(String linkedInURL) {
		this.linkedInURL = linkedInURL;
	}

	public List<AgentQaAnswer> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<AgentQaAnswer> customFields) {
		this.customFields = customFields;
	}

	public boolean isCustomFieldsAnswerChanged(ProspectCallDTO prospectCallDTO) {
		boolean isAnswerChanged = false;
		if (prospectCallDTO == null) {
			return false;
		}
		if (prospectCallDTO != null && prospectCallDTO.getProspect() != null && prospectCallDTO.getProspect().getCustomFields() != null
				&& prospectCallDTO.getProspect().getCustomFields().size() > 0 && customFields != null && customFields.size() > 0) {
			for (AgentQaAnswer agentQaAnswer : customFields) {
				List<AgentQaAnswer> question = null;
				if (prospectCallDTO != null && prospectCallDTO.getProspect().getCustomFields() != null
						&& prospectCallDTO.getProspect().getCustomFields().size() > 0) {
					question = prospectCallDTO.getProspect().getCustomFields().stream()
							.filter(c -> c.getQuestion().equalsIgnoreCase(agentQaAnswer.getQuestion()))
							.collect(Collectors.toList());
				}
				if (question != null && question.size() > 0 && agentQaAnswer != null && agentQaAnswer.getAnswer() != null && !agentQaAnswer.getAnswer().isEmpty()
						&& agentQaAnswer.getAnswer().equalsIgnoreCase(question.get(0).getAnswer())) {
					isAnswerChanged = true;
				} else {
					isAnswerChanged = false;
					return isAnswerChanged;
				}
			}
		}
		return isAnswerChanged;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getInsideViewPurchaseDetailsId() {
		return insideViewPurchaseDetailsId;
	}

	public void setInsideViewPurchaseDetailsId(String insideViewPurchaseDetailsId) {
		this.insideViewPurchaseDetailsId = insideViewPurchaseDetailsId;
	}

	public String getAgentDialedExtension() {
		return agentDialedExtension;
	}

	public void setAgentDialedExtension(String agentDialedExtension) {
		this.agentDialedExtension = agentDialedExtension;
	}
}