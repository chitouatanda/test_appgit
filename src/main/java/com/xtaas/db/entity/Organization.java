package com.xtaas.db.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.OrganizationTypes;
import com.xtaas.web.dto.CampaignDefaultSetting;

@Document
public class Organization extends AbstractEntity {

	public static enum OrganizationLevel {
		INTERNAL, // for XTaaS
		CLIENT, // for XTaaS Clients
		PARTNER // for XTaaS partners
	}

	public static enum ClientType {
		CONNECT, PLATFORM, ENTERPRISE, INTERNAL
	}

	private String name;
	private boolean agency;
	private String description;
	private String companySize;
	private String url;
	private String organizationLoginId;
	private String accountOwnerLoginId;
	private String status;
	private String vertical;
	private int marketCap;
	private CRMSystem crm;
	private OrganizationTypes organizationType;
	private ArrayList<Contact> contacts;
	private String timezone;
	private boolean enableMachineAnsweredDetection;
	private OrganizationLevel organizationLevel;
	private List<String> partnerSupervisors;
	private String defaultDialerMode;
	private boolean failureEnable;
	private boolean enableRedial;
	private String userName;
	private String password;
	private boolean agentOnlineReport;
	private String agentOnlineReportEmail;
	private String organizationEmail;
	private boolean simpleDisposition;
	private String hostingServer;
	private boolean sendAssetEmail;
	private List<String> reportingEmail;
	private List<String> fromInstance;
	private CampaignDefaultSetting campaignDefaultSetting;
	private ClientType clientType;
	private String telnyxConnectionId;
	private String redirectUrl;
	private boolean enableAgentInactivity;
	private int manualAgentIdleTime;
	private int agentAutoDisposeCount;
	private int lowCallableAlertThreshold;
	private boolean unHideSocialLink;
	private boolean passwordPolicy;
	private int lockPasswordAttempts;
	private boolean downloadCallBackReport;
	private boolean multiFactorRequired;
	private boolean enforeMFA;
	private boolean prospectUploadEnabled;
	private boolean liteMode;
	private List<String> allowMFAUsers;
	private boolean showAlternateLink;

	protected Organization() {
		// TODO Auto-generated constructor stub
	}

	public Organization(String name, boolean agency, String description, String companySize, String url,
			String organizationLoginId, String accountOwnerLoginId, String vertical, int marketCap, CRMSystem crm,
			ArrayList<Contact> contacts, OrganizationLevel organizationLevel, List<String> partnerSupervisors) {
		this.setName(name);
		this.setAgency(isAgency());
		this.setDescription(description);
		this.setCompanySize(companySize);
		this.setUrl(url);
		this.setOrganizationLoginId(organizationLoginId);
		this.setAccountOwnerLoginId(accountOwnerLoginId);
		this.setVertical(vertical);
		this.setMarketCap(marketCap);
		this.setCrm(crm);
		this.setContacts(contacts);
		this.setOrganizationLevel(organizationLevel);
		this.setPartnerSupervisors(partnerSupervisors);
		this.setEnableMachineAnsweredDetection(false);
		this.setDefaultDialerMode("POWER");
		this.setFailureEnable(false);
		this.setTimezone("US/Pacific");
		this.setEnableRedial(false);
		this.setAgentOnlineReport(false);
		this.setAgentOnlineReportEmail("kchugh@xtaascorp.com");
		this.setSimpleDisposition(true);
		this.setPasswordPolicy(false);
		this.setLockPasswordAttempts(5);
		this.setMultiFactorRequired(false);
		this.setEnforeMFA(true);
		this.setProspectUploadEnabled(false);
		this.setLiteMode(false);
		this.allowMFAUsers = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Organization name is required");
		}
		this.name = name;
	}

	/**
	 * @return the agency
	 */
	public boolean isAgency() {
		return agency;
	}

	/**
	 * @param agency the agency to set
	 */
	public void setAgency(boolean agency) {
		this.agency = agency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOrganizationLoginId() {
		return organizationLoginId;
	}

	public void setOrganizationLoginId(String organizationLoginId) {
		this.organizationLoginId = organizationLoginId;
	}

	public String getAccountOwnerLoginId() {
		return accountOwnerLoginId;
	}

	public void setAccountOwnerLoginId(String accountOwnerLoginId) {
		this.accountOwnerLoginId = accountOwnerLoginId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		if (vertical == null || vertical.isEmpty()) {
			throw new IllegalArgumentException("vertical is required");
		}
		this.vertical = vertical;
	}

	public int getMarketCap() {
		return marketCap;
	}

	public void setMarketCap(int marketCap) {
		this.marketCap = marketCap;
	}

	public CRMSystem getCrm() {
		return crm;
	}

	public void setCrm(CRMSystem crm) {
		this.crm = crm;
	}

	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		if (contacts.isEmpty() || contacts == null) {
			throw new IllegalArgumentException("contacts is required");
		}
		this.contacts = contacts;
	}

	public OrganizationTypes getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(OrganizationTypes organizationType) {
		this.organizationType = organizationType;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public OrganizationLevel getOrganizationLevel() {
		return organizationLevel;
	}

	public void setOrganizationLevel(OrganizationLevel organizationLevel) {
		this.organizationLevel = organizationLevel;
	}

	public List<String> getPartnerSupervisors() {
		return partnerSupervisors;
	}

	public void setPartnerSupervisors(List<String> partnerSupervisors) {
		this.partnerSupervisors = partnerSupervisors;
	}

	public String getDefaultDialerMode() {
		return defaultDialerMode;
	}

	public void setDefaultDialerMode(String defaultDialerMode) {
		this.defaultDialerMode = defaultDialerMode;
	}

	public boolean isFailureEnable() {
		return failureEnable;
	}

	public void setFailureEnable(boolean failureEnable) {
		this.failureEnable = failureEnable;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnableRedial() {
		return enableRedial;
	}

	public void setEnableRedial(boolean enableRedial) {
		this.enableRedial = enableRedial;
	}

	public boolean isAgentOnlineReport() {
		return agentOnlineReport;
	}

	public void setAgentOnlineReport(boolean agentOnlineReport) {
		this.agentOnlineReport = agentOnlineReport;
	}

	public String getAgentOnlineReportEmail() {
		return agentOnlineReportEmail;
	}

	public void setAgentOnlineReportEmail(String agentOnlineReportEmail) {
		this.agentOnlineReportEmail = agentOnlineReportEmail;
	}

	public String getOrganizationEmail() {
		return organizationEmail;
	}

	public void setOrganizationEmail(String organizationEmail) {
		this.organizationEmail = organizationEmail;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public boolean isSendAssetEmail() {
		return sendAssetEmail;
	}

	public void setSendAssetEmail(boolean sendAssetEmail) {
		this.sendAssetEmail = sendAssetEmail;
	}

	public List<String> getReportingEmail() {
		return reportingEmail;
	}

	public void setReportingEmail(List<String> reportingEmail) {
		this.reportingEmail = reportingEmail;
	}

	public CampaignDefaultSetting getCampaignDefaultSetting() {
		return campaignDefaultSetting;
	}

	public void setCampaignDefaultSetting(CampaignDefaultSetting campaignDefaultSetting) {
		this.campaignDefaultSetting = campaignDefaultSetting;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public ClientType getClientType() {
		return clientType;
	}

	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}

	public String getTelnyxConnectionId() {
		return telnyxConnectionId;
	}

	public void setTelnyxConnectionId(String telnyxConnectionId) {
		this.telnyxConnectionId = telnyxConnectionId;
	}

	public boolean isUnHideSocialLink() {
		return unHideSocialLink;
	}

	public void setUnHideSocialLink(boolean unHideSocialLink) {
		this.unHideSocialLink = unHideSocialLink;
	}

	public boolean isPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(boolean passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public List<String> getFromInstance() {
		return fromInstance;
	}

	public void setFromInstance(List<String> fromInstance) {
		this.fromInstance = fromInstance;
	}

	public int getLockPasswordAttempts() {
		return lockPasswordAttempts;
	}

	public void setLockPasswordAttempts(int lockPasswordAttempts) {
		this.lockPasswordAttempts = lockPasswordAttempts;
	}

	public boolean isDownloadCallBackReport() {
		return downloadCallBackReport;
	}

	public void setDownloadCallBackReport(boolean downloadCallBackReport) {
		this.downloadCallBackReport = downloadCallBackReport;
	}

	public boolean isMultiFactorRequired() {
		return multiFactorRequired;
	}

	public void setMultiFactorRequired(boolean multiFactorRequired) {
		this.multiFactorRequired = multiFactorRequired;
	}

	public boolean isEnforeMFA() {
		return enforeMFA;
	}

	public void setEnforeMFA(boolean enforeMFA) {
		this.enforeMFA = enforeMFA;
	}

	public boolean isProspectUploadEnabled() {
		return prospectUploadEnabled;
	}

	public void setProspectUploadEnabled(boolean prospectUploadEnabled) {
		this.prospectUploadEnabled = prospectUploadEnabled;
	}

	public int getManualAgentIdleTime() {
		return manualAgentIdleTime;
	}

	public void setManualAgentIdleTime(int manualAgentIdleTime) {
		this.manualAgentIdleTime = manualAgentIdleTime;
	}

	public int getAgentAutoDisposeCount() {
		return agentAutoDisposeCount;
	}

	public void setAgentAutoDisposeCount(int agentAutoDisposeCount) {
		this.agentAutoDisposeCount = agentAutoDisposeCount;
	}

	public int getLowCallableAlertThreshold() {
		return lowCallableAlertThreshold;
	}

	public void setLowCallableAlertThreshold(int lowCallableAlertThreshold) {
		this.lowCallableAlertThreshold = lowCallableAlertThreshold;
	}

	public boolean isLiteMode() {
		return liteMode;
	}

	public void setLiteMode(boolean liteMode) {
		this.liteMode = liteMode;
	}

	public boolean isEnableAgentInactivity() {
		return enableAgentInactivity;
	}

	public void setEnableAgentInactivity(boolean enableAgentInactivity) {
		this.enableAgentInactivity = enableAgentInactivity;
	}

	public List<String> getAllowMFAUsers() {
		return allowMFAUsers;
	}

	public void setAllowMFAUsers(List<String> allowMFAUsers) {
		this.allowMFAUsers = allowMFAUsers;
	}

	public boolean isShowAlternateLink() {
		return showAlternateLink;
	}

	public void setShowAlternateLink(boolean showAlternateLink) {
		this.showAlternateLink = showAlternateLink;
	}
}