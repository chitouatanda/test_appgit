package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.web.dto.HealthChecksCriteriaDTO;

@Document(collection = "healthchecks")
public class HealthChecks extends AbstractEntity {

	private String campaignId;
	private List<HealthChecksCriteriaDTO> criteria;

	protected HealthChecks() {

	}

	public HealthChecks(String campaignId, List<HealthChecksCriteriaDTO> criteria) {
		this.setCampaignId(campaignId);
		this.setCriteria(criteria);
	}

	public String getCampaignId() {
		return campaignId;
	}

	private void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public List<HealthChecksCriteriaDTO> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<HealthChecksCriteriaDTO> criteria) {
		this.criteria = criteria;
	}

	@Override
	public String toString() {
		return "HealthChecks [campaignId=" + campaignId + ", criteria=" + criteria + "]";
	}
	
}
