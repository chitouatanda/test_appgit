/**
 * 
 */
package com.xtaas.db.entity;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;

/**
 * @author djain
 *
 */
@Component
public class EntityAuditor implements AuditorAware<String> {
	@Override
	public Optional<String> getCurrentAuditor() {
		String username = XtaasUserUtils.getCurrentLoggedInUsername();
		if (username == null) { //this will only happen for standalone/un-authenticated requests
			username = XtaasConstants.SYSTEM_USERNAME;
		}
		return Optional.of(username); 
	}

}
