package com.xtaas.db.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentonlinetime")
public class AgentOnlineTime extends AbstractEntity {

	private String agentId;
	//private String activityType;
	private String status;
	//private String pcid;
	//private String prospectInteractionSessionId;
	//private String campaignId;
	//private float callStateTransitionDuration;
	private float stateDuration;
	private Date jobDate;
	private String partnerId;
	
	public String getPartnerId() {
		return partnerId;
	}



	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}



	public AgentOnlineTime() {
		
	}



	public String getAgentId() {
		return agentId;
	}

	
	public String getStatus() {
		return status;
	}

	public void setAgentId(String agentId) {
		if (agentId == null || agentId.isEmpty()) {
			throw new IllegalArgumentException("Agent Id is required");
		}
		this.agentId = agentId;
	}

	public void setStatus(String status) {
		if (status == null || status.isEmpty()) {
			throw new IllegalArgumentException("Agent Activity Status is required");
		}
		this.status = status;
	}

	
	public float getStateDuration() {
		return stateDuration;
	}

	public void setStateDuration(float stateDuration) {
		this.stateDuration = stateDuration;
	}



	public Date getJobDate() {
		return jobDate;
	}



	public void setJobDate(Date jobDate) {
		this.jobDate = jobDate;
	}
	
	

}
