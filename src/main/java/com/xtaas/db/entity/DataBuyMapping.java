package com.xtaas.db.entity;

import java.util.List;

import com.xtaas.valueobjects.KeyValuePair;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "databuymapping")
public class DataBuyMapping extends AbstractEntity {

	String source;
	String attribute;// for salutary
	List<KeyValuePair<String, String>> mapping;// for salutary
	String inputType;
	String searchValue;
	String searchString;
	String rootTaxonamyId;
	String childTaxonamyId;
	String rootTaxonamyValue;
	String childTaxonamyValue;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public List<KeyValuePair<String, String>> getMapping() {
		return mapping;
	}

	public void setMapping(List<KeyValuePair<String, String>> mapping) {
		this.mapping = mapping;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getRootTaxonamyId() {
		return rootTaxonamyId;
	}

	public void setRootTaxonamyId(String rootTaxonamyId) {
		this.rootTaxonamyId = rootTaxonamyId;
	}

	public String getChildTaxonamyId() {
		return childTaxonamyId;
	}

	public void setChildTaxonamyId(String childTaxonamyId) {
		this.childTaxonamyId = childTaxonamyId;
	}

	public String getRootTaxonamyValue() {
		return rootTaxonamyValue;
	}

	public void setRootTaxonamyValue(String rootTaxonamyValue) {
		this.rootTaxonamyValue = rootTaxonamyValue;
	}

	public String getChildTaxonamyValue() {
		return childTaxonamyValue;
	}

	public void setChildTaxonamyValue(String childTaxonamyValue) {
		this.childTaxonamyValue = childTaxonamyValue;
	}

}