package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "failedrecordingstoaws")
public class FailedRecordingsToAWS {

	private String id;
	private String campaignId;
	private String twilioRecordingUrl;
	private String prospectCallId;
	

	

	public String getTwilioRecordingUrl() {
		return twilioRecordingUrl;
	}

	public void setTwilioRecordingUrl(String twilioRecordingUrl) {
		this.twilioRecordingUrl = twilioRecordingUrl;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	
	

}
