package com.xtaas.db.entity;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 2/23/14
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Budget {
  private String type; //daily/weekly/monthly/lifetime
  private long amount; //budget*10000
  private long amountSoFar; //if((amount-amountSoFar)<0) no more leads until the next cutoff
  private long lastUpdated; //used to reset amountSoFar for the new day/week/etc.

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public long getAmount() {
    return amount;
  }

  public void setAmount(long amount) {
    this.amount = amount;
  }

  public long getAmountSoFar() {
    return amountSoFar;
  }

  public void setAmountSoFar(long amountSoFar) {
    this.amountSoFar = amountSoFar;
  }

  public long getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(long lastUpdated) {
    this.lastUpdated = lastUpdated;
  }
}
