package com.xtaas.db.entity;

import com.google.common.math.Stats;

public class ClassifierStatisticsLog extends AbstractEntity {
    public static enum ScoreType {
        LOW_RANGE, HIGH_RANGE;

        public static ScoreType getScoreType(double score) {
            return score <= 0.50f ? LOW_RANGE : HIGH_RANGE;
        }
        
        public static boolean isLow(ScoreType type) {
            return type == LOW_RANGE;   
        }

        public static boolean isHigh(ScoreType type) {
            return type == HIGH_RANGE;   
        }
    }

    private String className;
    private ScoreType scoreType;
    private long count;
    private float mean;
    private float min;
    private float max;
    private float stdDev;
    private float percentile90;
    private float percentile95;
    private float percentile99;
    private float sumOfSquaresDelta;

    public String getClassName() {
        return className;
    }

    public ScoreType getScoreType() {
        return scoreType;
    }

    public long getCount() {
        return count;
    }

    public float getMean() {
        return mean;
    }

    public float getMin() {
        return min;
    }

    public float getMax() {
        return max;
    }

    public float getStdDev() {
        return stdDev;
    }

    public float getPercentile90() {
        return percentile90;
    }

    public float getPercentile95() {
        return percentile95;
    }

    public float getPercentile99() {
        return percentile99;
    }

    public float getSumOfSquaresDelta() {
        return sumOfSquaresDelta;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setScoreType(ScoreType scoreType) {
        this.scoreType = scoreType;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public void setStdDev(float stdDev) {
        this.stdDev = stdDev;
    }

    public void setPercentile90(float percentile90) {
        this.percentile90 = percentile90;
    }

    public void setPercentile95(float percentile95) {
        this.percentile95 = percentile95;
    }

    public void setPercentile99(float percentile99) {
        this.percentile99 = percentile99;
    }

    public void setSumOfSquaresDelta(float sumOfSquaresDelta) {
        this.sumOfSquaresDelta = sumOfSquaresDelta;
    }


//---------- Fluent Style api---------

    public ClassifierStatisticsLog withClassName(String className) {
        this.className = className;
        return this;
    }

    public ClassifierStatisticsLog withScoreType(ScoreType scoreType) {
        this.scoreType = scoreType;
        return this;
    }

    public ClassifierStatisticsLog withCount(long count) {
        this.count = count;
        return this;
    }

    public ClassifierStatisticsLog withMean(float mean) {
        this.mean = mean;
        return this;
    }

    public ClassifierStatisticsLog withMin(float min) {
        this.min = min;
        return this;
    }

    public ClassifierStatisticsLog withMax(float max) {
        this.max = max;
        return this;
    }

    public ClassifierStatisticsLog withStdDev(float stdDev) {
        this.stdDev = stdDev;
        return this;
    }

    public ClassifierStatisticsLog withPercentile90(float percentile90) {
        this.percentile90 = percentile90;
        return this;
    }

    public ClassifierStatisticsLog withPercentile95(float percentile95) {
        this.percentile95 = percentile95;
        return this;
    }

    public ClassifierStatisticsLog withPercentile99(float percentile99) {
        this.percentile99 = percentile99;
        return this;
    }

    public ClassifierStatisticsLog withSumOfSquaresDelta(float sumOfSquaresDelta) {
        this.sumOfSquaresDelta = sumOfSquaresDelta;
        return this;
    }

    public static ClassifierStatisticsLog create(String className, ScoreType type, Stats stats) {
        int multiplier = ScoreType.isLow(type) ? 1 : -1;
        float stddev = (float) stats.populationStandardDeviation();
        return new ClassifierStatisticsLog()
                .withClassName(className)
                .withScoreType(type)
                .withCount(stats.count())
                .withMean((float) stats.mean())
                .withMin((float) stats.min())
                .withMax((float) stats.max())
                .withStdDev((float) stats.populationStandardDeviation())
                .withPercentile90((float) (stats.mean() + multiplier * 1.282 * stddev))
                .withPercentile95((float) (stats.mean() + multiplier * 1.645 * stddev))
                .withPercentile99((float) (stats.mean() + multiplier * 2.326 * stddev));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClassifierStatisticsLog{");
        sb.append("className=\'").append(this.className).append("\'");
        sb.append(", ").append("scoreType=").append(this.scoreType);
        sb.append(", ").append("count=").append(this.count);
        sb.append(", ").append("mean=").append(this.mean);
        sb.append(", ").append("min=").append(this.min);
        sb.append(", ").append("max=").append(this.max);
        sb.append(", ").append("stdDev=").append(this.stdDev);
        sb.append(", ").append("percentile90=").append(this.percentile90);
        sb.append(", ").append("percentile95=").append(this.percentile95);
        sb.append(", ").append("percentile99=").append(this.percentile99);
        sb.append(", ").append("sumOfSquaresDelta=").append(this.sumOfSquaresDelta);
        sb.append('}');
        return sb.toString();
    }
}
