package com.xtaas.db.entity;

import java.util.HashMap;

public class CRMSystem {
	private String name;
	private String version;
	private HashMap<String, String> connectionString;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * @return the connectionString
	 */
	public HashMap<String, String> getConnectionString() {
		return connectionString;
	}
	/**
	 * @param connectionString the connectionString to set
	 */
	public void setConnectionString(HashMap<String, String> connectionString) {
		this.connectionString = connectionString;
	}
	
	
	
	
}
