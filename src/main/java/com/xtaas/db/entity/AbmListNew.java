package com.xtaas.db.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.db.entity.AbstractEntity;
import com.xtaas.valueobjects.KeyValuePair;


@Document(collection="abmlistnew")
public class AbmListNew extends AbstractEntity {
	
	private String companyName;
	private String companyId;
	private String campaignId;
	private String domain;
	private String status;
	List<AbmWeightScore> responseList;

	/**
	 * Used to clone the campaign. When you add any new field in the AbmListNew then
	 * set it here as well
	 * 
	 * @param abmListNew
	 * @param campaignId
	 * @return
	 */
	public AbmListNew getAbmListNewClone(AbmListNew abmListNew, String campaignId) {
		this.setCompanyName(abmListNew.getCompanyName());
		this.setCompanyId(abmListNew.getCompanyId());
		this.setCampaignId(campaignId);
		this.setDomain(abmListNew.getDomain());
		this.setStatus(abmListNew.getStatus());
		return this;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public List<AbmWeightScore> getResponseList() {
		return responseList;
	}
	public void setResponseList(List<AbmWeightScore> responseList) {
		this.responseList = responseList;
	}
	
	
}
