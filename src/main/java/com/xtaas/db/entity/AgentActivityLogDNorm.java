package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentactivitylogdnorm")
public class AgentActivityLogDNorm extends AbstractEntity {

	public enum AgentActivityType {
		AGENT_STATUS_CHANGE, CALL_STATUS_CHANGE;
	}

	public enum AgentActivityCallStatus {
		CALL_WRAPPED_UP, CALL_CONNECTED, CALL_DISCONNECTED, CALL_PREVIEW, CALL_DIALED, CALL_ONMUTE, CALL_UNMUTE,
		CALL_ONHOLD, CALL_UNHOLD;
	}

	private String agentId;
	private String activityType;
	private String status;
	private String pcid;
	// prospect interaction session id. Reference to session id in
	// ProspectCallInteraction
	private String prospectInteractionSessionId;
	private String campaignId;
	/*
	 * represents duration in secs indicating time it took to come to this CALL
	 * STATE from the previous CALL STATE (ex: it will store 10 secs if call state
	 * took 10 secs to get in CALL_DIALED from CALL_PREVIEW
	 */
	private int callStateTransitionDuration;
	private long stateDuration;

	public AgentActivityLogDNorm(String agentId, String activityType, String status) {
		setAgentId(agentId);
		setActivityType(activityType);
		setStatus(status);
	}

	public String getAgentId() {
		return agentId;
	}

	public String getActivityType() {
		return activityType;
	}

	public String getStatus() {
		return status;
	}

	private void setActivityType(String activityType) {
		if (activityType == null) {
			throw new IllegalArgumentException("Activity Type is required");
		}
		this.activityType = activityType;
	}

	private void setAgentId(String agentId) {
		if (agentId == null || agentId.isEmpty()) {
			throw new IllegalArgumentException("Agent Id is required");
		}
		this.agentId = agentId;
	}

	private void setStatus(String status) {
		if (status == null || status.isEmpty()) {
			throw new IllegalArgumentException("Agent Activity Status is required");
		}
		this.status = status;
	}

	public String getPcid() {
		return pcid;
	}

	public void setPcid(String pcid) {
		this.pcid = pcid;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public int getCallStateTransitionDuration() {
		return callStateTransitionDuration;
	}

	public void setCallStateTransitionDuration(int callStateTransitionDuration) {
		this.callStateTransitionDuration = callStateTransitionDuration;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public long getStateDuration() {
		return stateDuration;
	}

	public void setStateDuration(long stateDuration) {
		this.stateDuration = stateDuration;
	}

}
