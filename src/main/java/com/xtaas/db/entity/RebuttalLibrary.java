package com.xtaas.db.entity;

import java.util.ArrayList;
import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.Rebuttal;

@Document(collection = "rebuttallibrary")
public class RebuttalLibrary extends AbstractEntity {
	private String parent;
	private String ownerId;
	private ArrayList<Rebuttal> rebuttals;
	private boolean inherit;

	public RebuttalLibrary(String ownerId, String parent, boolean inherit, ArrayList<Rebuttal> rebuttals) {
		this.setOwnerId(ownerId);
		this.setParent(parent);
		this.setInherit(inherit);
		this.setRebuttals(rebuttals);
	}
	
	/**
	 * @return the name of parent mapping
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @return the inherit
	 */
	public boolean isInherit() {
		return inherit;
	}

	/**
	 * @param inherit the inherit to set
	 */
	public void setInherit(boolean inherit) {
		this.inherit = inherit;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		if (ownerId == null) {
			throw new IllegalArgumentException("OwnerId is required. Owner is any Partner Org.");
		}
		this.ownerId = ownerId;
	}

	/**
	 * @return the rebuttals
	 */
	public ArrayList<Rebuttal> getRebuttals() {
		return rebuttals;
	}

	/**
	 * @param rebuttals the rebuttals to set
	 */
	public void setRebuttals(ArrayList<Rebuttal> rebuttals) {
		if (rebuttals == null || rebuttals.isEmpty()) {
			throw new IllegalArgumentException("No Rebuttals specified.");
		}
		this.rebuttals = rebuttals;
	}
}