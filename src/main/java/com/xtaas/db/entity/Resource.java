package com.xtaas.db.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Resource extends AbstractEntity {
	private String overview;
	private String status;
	private double pricePerHour;
	private Address address;
	private String timezone;
	private List<String> languages;
	private List<String> domains;
	private WorkDays workDays;
	
	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}
	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the pricePerHour
	 */
	public double getPricePerHour() {
		return pricePerHour;
	}
	/**
	 * @param pricePerHour the pricePerHour to set
	 */
	public void setPricePerHour(double pricePerHour) {
		this.pricePerHour = pricePerHour;
	}
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}
	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	/**
	 * @return the languages
	 */
	public List<String> getLanguages() {
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	/**
	 * @return the domains
	 */
	public List<String> getDomains() {
		return domains;
	}
	/**
	 * @param domains the domains to set
	 */
	public void setDomains(List<String> domains) {
		this.domains = domains;
	}
	/**
	 * @return the workDays
	 */
	public WorkDays getWorkDays() {
		return workDays;
	}
	/**
	 * @param workDays the workDays to set
	 */
	public void setWorkDays(WorkDays workDays) {
		this.workDays = workDays;
	}
}
