/**
 * 
 */
package com.xtaas.db.entity;

import java.util.ArrayList;
import java.util.HashSet;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entity class which holds State specific Call configuration as in Hours to Call, Holidays, TZ etc.
 * 
 * @author djain
 */
@Document(collection="statecallconfig")
public class StateCallConfig extends AbstractEntity {
	
	private String stateCode;  // CA
	private String stateName;  // California
	private String countryName; //United State
	private String countryCode; //US
	private String timezone;   // US/Pacific
	private int startCallHour; // Time in 24 HR format. Ex: 9 (for 9 AM)
	private int endCallHour;   // Time in 24 HR format. Ex: 21 (for 9 PM)
	private boolean weekendCallAllowed; //true or false
	private HashSet<String> holidayDates; //List of holidays in current year
	private ArrayList<StateCallOffHours> stateCallOffHours; //List of hours in which calls should not be made by dialer 

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}
	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	/**
	 * @return the startCallHour
	 */
	public int getStartCallHour() {
		return startCallHour;
	}
	/**
	 * @param startCallHour the startCallHour to set
	 */
	public void setStartCallHour(int startCallHour) {
		this.startCallHour = startCallHour;
	}
	/**
	 * @return the endCallHour
	 */
	public int getEndCallHour() {
		return endCallHour;
	}
	/**
	 * @param endCallHour the endCallHour to set
	 */
	public void setEndCallHour(int endCallHour) {
		this.endCallHour = endCallHour;
	}
	/**
	 * @return the weekendcallallowed
	 */
	public boolean isWeekendCallAllowed() {
		return weekendCallAllowed;
	}
	/**
	 * @param weekendcallallowed the weekendcallallowed to set
	 */
	public void setWeekendCallAllowed(boolean weekendCallAllowed) {
		this.weekendCallAllowed = weekendCallAllowed;
	}
	/**
	 * @return the holidayDates
	 */
	public HashSet<String> getHolidayDates() {
		return holidayDates;
	}
	/**
	 * @param holidayDates the holidayDates to set
	 */
	public void setHolidayDates(HashSet<String> holidayDates) {
		this.holidayDates = holidayDates;
	}
	
	/**
	 * @return the stateCallOffHours
	 */
	public ArrayList<StateCallOffHours> getStateCallOffHours() {
		return stateCallOffHours;
	}
	/**
	 * @param stateCallOffHours the stateCallOffHours to set
	 */
	public void setStateCallOffHours(ArrayList<StateCallOffHours> stateCallOffHours) {
		this.stateCallOffHours = stateCallOffHours;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StateCallConfig [stateCode=");
		builder.append(stateCode);
		builder.append(", stateName=");
		builder.append(stateName);
		builder.append(", countryName=");
		builder.append(countryName);
		builder.append(", countryCode=");
		builder.append(countryCode);
		builder.append(", timezone=");
		builder.append(timezone);
		builder.append(", startCallHour=");
		builder.append(startCallHour);
		builder.append(", endCallHour=");
		builder.append(endCallHour);
		builder.append(", weekendCallAllowed=");
		builder.append(weekendCallAllowed);
		builder.append(", holidayDates=");
		builder.append(holidayDates);
		builder.append(", stateCallOffHours=");
		builder.append(stateCallOffHours);
		builder.append("]");
		return builder.toString();
	}
}
