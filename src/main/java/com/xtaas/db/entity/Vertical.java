package com.xtaas.db.entity;

import java.util.ArrayList;

public class Vertical {
	private String name;
	private ArrayList<Domain> domains;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}
}
