package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "realtimedeliveryindustrymapping")
public class RealTimeDeliveryIndustryMapping {

	private String xtaasIndustry;
	private String realTimeDeliveryIndustry;

	public String getXtaasIndustry() {
		return xtaasIndustry;
	}

	public void setXtaasIndustry(String xtaasIndustry) {
		this.xtaasIndustry = xtaasIndustry;
	}

	public String getRealTimeDeliveryIndustry() {
		return realTimeDeliveryIndustry;
	}

	public void setRealTimeDeliveryIndustry(String realTimeDeliveryIndustry) {
		this.realTimeDeliveryIndustry = realTimeDeliveryIndustry;
	}

}
