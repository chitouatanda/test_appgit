package com.xtaas.db.entity;

import java.util.List;

public class WorkDays {
	private List<WorkHours> mondayHours;
	private List<WorkHours> tuesdayHours;
	private List<WorkHours> wednesdayHours;
	private List<WorkHours> thursdayHours;
	private List<WorkHours> fridayHours;
	/**
	 * @return the mondayHours
	 */
	public List<WorkHours> getMondayHours() {
		return mondayHours;
	}
	/**
	 * @param mondayHours the mondayHours to set
	 */
	public void setMondayHours(List<WorkHours> mondayHours) {
		this.mondayHours = mondayHours;
	}
	/**
	 * @return the tuesdayHours
	 */
	public List<WorkHours> getTuesdayHours() {
		return tuesdayHours;
	}
	/**
	 * @param tuesdayHours the tuesdayHours to set
	 */
	public void setTuesdayHours(List<WorkHours> tuesdayHours) {
		this.tuesdayHours = tuesdayHours;
	}
	/**
	 * @return the wednesdayHours
	 */
	public List<WorkHours> getWednesdayHours() {
		return wednesdayHours;
	}
	/**
	 * @param wednesdayHours the wednesdayHours to set
	 */
	public void setWednesdayHours(List<WorkHours> wednesdayHours) {
		this.wednesdayHours = wednesdayHours;
	}
	/**
	 * @return the thursdayHours
	 */
	public List<WorkHours> getThursdayHours() {
		return thursdayHours;
	}
	/**
	 * @param thursdayHours the thursdayHours to set
	 */
	public void setThursdayHours(List<WorkHours> thursdayHours) {
		this.thursdayHours = thursdayHours;
	}
	/**
	 * @return the fridayHours
	 */
	public List<WorkHours> getFridayHours() {
		return fridayHours;
	}
	/**
	 * @param fridayHours the fridayHours to set
	 */
	public void setFridayHours(List<WorkHours> fridayHours) {
		this.fridayHours = fridayHours;
	}
}
