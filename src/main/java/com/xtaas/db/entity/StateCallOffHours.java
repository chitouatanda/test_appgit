/**
 * 
 */
package com.xtaas.db.entity;

/**
 * Sub entity of StateCallConfig indicating call off hours in a day
 * @author djain
 *
 */
public class StateCallOffHours {
	private Integer startOffCallHour; // Time in 24 HR format. Ex: 9 (for 9 AM). Dialer not to make any call in Off Call Hours
	private Integer endOffCallHour; // Time in 24 HR format. Ex: 21 (for 9 PM). Dialer not to make any call in Off Call Hours
	
	public StateCallOffHours(Integer startOffCallHour, Integer endOffCallHour) {
		this.startOffCallHour = startOffCallHour;
		this.endOffCallHour = endOffCallHour;
	}
	
	/**
	 * @return the startOffCallHour
	 */
	public Integer getStartOffCallHour() {
		return startOffCallHour;
	}
	
	/**
	 * @return the endOffCallHour
	 */
	public Integer getEndOffCallHour() {
		return endOffCallHour;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StateCallOffHours [startOffCallHour=");
		builder.append(startOffCallHour);
		builder.append(", endOffCallHour=");
		builder.append(endOffCallHour);
		builder.append("]");
		return builder.toString();
	}
}
