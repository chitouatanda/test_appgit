package com.xtaas.db.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.entity.AbstractEntity;

@Document(collection = "globalcontactcampaignstatus")
public class GlobalContactCampaignStatus extends AbstractEntity {

	private String campaignId;
	private String name;
	private String status;
	private int totalProspectCount;
	private String errorReason;

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTotalProspectCount() {
		return totalProspectCount;
	}

	public void setTotalProspectCount(int totalProspectCount) {
		this.totalProspectCount = totalProspectCount;
	}

	public String getErrorReason() {
		return errorReason;
	}

	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}

}
