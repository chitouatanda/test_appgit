package com.xtaas.batch.reader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.CallBillingLog;
import com.xtaas.db.repository.CallBillingLogRepository;

@Component
public class CustomBillingQueryParams extends MongoQueryParamSetter{

	@Autowired
	private CallBillingLogRepository callBillingLogRepository;
	
	@Override
	List<Object> getParameterList() {
		List<Object> parameterValues = new ArrayList<Object>();
		List<CallBillingLog> callBillingLogs = callBillingLogRepository.findLatestTimestamp(PageRequest.of(0, 1, Sort.by(Direction.DESC, "calllogCreatedDate")));
		if (callBillingLogs == null || callBillingLogs.isEmpty()) {
			parameterValues.add((Object) new Date(0)); //date since epoch
		} else {
			parameterValues.add((Object) callBillingLogs.get(0).getCalllogCreatedDate());
		}
		return parameterValues;
	}

}
