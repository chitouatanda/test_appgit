package com.xtaas.batch.reader;

import java.util.List;

import org.springframework.batch.item.data.MongoItemReader;
import com.xtaas.BeanLocator;


public abstract class MongoQueryParamSetter {
	
	public void setQueryParams(String readerBeanName) {
		MongoItemReader<?> reader = BeanLocator.getBean(readerBeanName, MongoItemReader.class);
		reader.setParameterValues(getParameterList());
	}

	abstract List<Object> getParameterList();
}
