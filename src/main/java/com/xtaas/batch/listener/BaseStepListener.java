/**
 * 
 */
package com.xtaas.batch.listener;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.AuthTokenGeneratorService;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;

/**
 * A batch step listener class whose methods will be invoked before and after step.
 * 
 * @author djain
 *
 */
@Component
public class BaseStepListener {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseStepListener.class);
	
	@Autowired
	private RestClientService restClientService;
	
	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private AuthTokenGeneratorService authTokenGeneratorService;
	
	private String activeProfile = (System.getenv("spring_profiles_active") != null) ? System.getenv("spring_profiles_active") : System.getProperty("spring.profiles.active");
	
	@BeforeStep
    public void beforeStep(StepExecution stepExecution) {
		logger.debug("[{}] STEP STARTED", stepExecution.getStepName());
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("token", getAuthToken());
		headers.put("content-type", "application/json"); //content-type: application/json
		
		EmailMessage emailMsg = new EmailMessage(toEmails(), StringUtils.capitalize(stepExecution.getStepName()) + " " + stepExecution.getStatus().name() + ", Env: " + activeProfile, stepExecution.getSummary()); 
		
		try {
			restClientService.makePostRequest(getNotifierServiceUrl(), headers, JSONUtils.toJson(emailMsg));
		} catch (IOException e) {
			logger.error("beforeStep() : Error occurred while making a rest call to Email Notifier Service", e);
		}
    }

    @AfterStep
    public ExitStatus afterStep(StepExecution stepExecution) {
        //set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("token", getAuthToken());
		headers.put("content-type", "application/json"); //content-type: application/json
		EmailMessage emailMsg = new EmailMessage(toEmails(), StringUtils.capitalize(stepExecution.getStepName()) + " " + stepExecution.getStatus().name() + ", Env: " + activeProfile, stepExecution.getSummary());
		
		try {
			restClientService.makePostRequest(getNotifierServiceUrl(), headers, JSONUtils.toJson(emailMsg));
		} catch (IOException e) {
			logger.error("beforeStep() : Error occurred while making a rest call to Email Notifier Service", e);
		}
		
		logger.debug("[{}] STEP COMPLETED. Step Summary [{}]", stepExecution.getStepName(), stepExecution.getSummary());
        return stepExecution.getExitStatus();
    }
    
    private String getNotifierServiceUrl() {
    	StringBuilder bssServerBaseUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getBssServerBaseUrl()).append("/api/notifier");
    	return bssServerBaseUrl.toString();
    }
    
    private String getAuthToken() {
    	return authTokenGeneratorService.generateSystemToken(App.BACKEND_SUPPORT_SYSTEM.name(), "batch");
    }
    
    private List<String> toEmails() {
    	String notifyEmailAddresses = propertyService.getApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.BATCH_JOB_NOTIFICATION_EMAILS.name(), "djain@xtaascorp.com");
    	List<String> toEmails = Arrays.asList(notifyEmailAddresses.split("\\s*,\\s*"));
    	
		return toEmails;
    }
}
