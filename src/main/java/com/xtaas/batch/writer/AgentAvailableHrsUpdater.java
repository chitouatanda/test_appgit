package com.xtaas.batch.writer;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.xtaas.domain.entity.Agent;

public class AgentAvailableHrsUpdater extends MongoItemUpdater<Agent> {
	
	@Override
	public void updateItems(List<Agent> agents) {
		for (Agent agent : agents) {
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(agent.getId()));
			Update update = new Update();
			update.set("averageAvailableHours", agent.getAverageAvailableHours());
			System.out.println("Updating Agent Id: " + agent.getId());
			updateFirst(query, update, Agent.class);
		}
	}
}
