package com.xtaas.batch.writer;

import java.util.ArrayList;
import java.util.List;
import org.springframework.batch.item.data.MongoItemWriter;

public class MongoListItemWriter<T> extends MongoItemWriter<T>{
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doWrite(List<? extends T> items) {
		if (items != null && !items.isEmpty()) {
			super.doWrite((ArrayList<T>)items.get(0));
		}
	}
}
