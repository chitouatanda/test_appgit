package com.xtaas.batch.writer;

import java.util.List;

import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.WriteResult;
import com.mongodb.client.result.UpdateResult;
import com.xtaas.domain.entity.AbstractEntity;

public abstract class MongoItemUpdater<T extends AbstractEntity> extends MongoItemWriter<AbstractEntity> {
	
	private MongoOperations template;
	
	/**
	 * Set the {@link MongoOperations} to be used to save items to be written.
	 *
	 * @param template the template implementation to be used.
	 */
	@Override
	public void setTemplate(MongoOperations template) {
		super.setTemplate(template);
		this.template = template;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected final void doWrite(List<? extends AbstractEntity> items) {
		updateItems((List<T>) items);
	}
	
	public abstract void updateItems(List<T> items);
	
	public boolean updateFirst(Query query, Update update, Class<?> entityClass) {
		UpdateResult wr = template.updateFirst(query, update, entityClass);
		//System.out.println(wr);
		
		return (wr.getModifiedCount() != 0);  //returns TRUE if no error else False
	}
}
