package com.xtaas.batch.processor;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentCalendar;
import com.xtaas.domain.valueobject.CampaignResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.domain.valueobject.WorkSlot;

@Component
public class IndependentAgentAvailableHrsProcessor implements ItemProcessor<Agent, Agent> {
	
	private static final Logger logger = LoggerFactory.getLogger(IndependentAgentAvailableHrsProcessor.class);
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Override
	public Agent process(Agent agent) throws Exception {
		logger.debug("process() : Processing Agent -> Id: " + agent.getId() + ", Name: "+ agent.getName());
		
		final int AGENT_CALENDAR_NUM_DAYS = 90;
		
		//build Calendar for Agent
		LocalDate startDate = new LocalDate();
		LocalDate endDate = startDate.plusDays(AGENT_CALENDAR_NUM_DAYS);
		AgentCalendar agentCalendar = new AgentCalendar(startDate, endDate, agent.getId());
		for (WorkSchedule ws : agent.getWorkSchedules()) {
			agentCalendar.setAvailableSchedule(ws);
		}
		
		//find out the campaigns to which this Agent is allocated
		List<Campaign> allocatedToCampaigns = campaignRepository.findByAllocatedAgent(agent.getId(), new LocalDate().toDate(), new LocalDate().toDate());
		for (Campaign campaign : allocatedToCampaigns) {
			CampaignResource campaignResource = campaign.getTeam().getCampaignResource(agent.getId()); //find out the current agent and mark the allocated schedule as busy in Agent Calendar
			for (WorkSchedule ws : campaignResource.getWorkSchedules()) {
				agentCalendar.setBusySchedule(ws, campaign.getId());
			}
		}
		Map<LocalDate, TreeSet<WorkSlot>> aCalendar = agentCalendar.getAgentCalendar();
		logger.debug("process() : Generated Calendar for Agent [{}] : [{}]", agent.getId(), aCalendar.toString());
		
		int totalAvailableHrs = agentCalendar.getTotalAvailableHrs();
		logger.debug("Total Available Hrs for Agent [{}] is [{}] in [" + AGENT_CALENDAR_NUM_DAYS + "] days", agent.getId(), totalAvailableHrs);
		double monthlyAvgAvailableHrs = Math.round(((double)totalAvailableHrs/3)*100.0)/100.0; //30 days avg
		agent.setAverageAvailableHours(monthlyAvgAvailableHrs); //get the avg available hrs 
		logger.debug("Monthly Avg Available Hrs for Agent [{}] is [{}]", agent.getId(), agent.getAverageAvailableHours());
		
		return agent;
	}
}