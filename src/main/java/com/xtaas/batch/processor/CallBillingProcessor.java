package com.xtaas.batch.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.CallBillingLog;
import com.xtaas.db.entity.CallBillingLog.CallBillingType;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.repository.CallBillingLogRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;

@Component
public class CallBillingProcessor implements ItemProcessor<CallLog, List<CallBillingLog>>{

	private static final Logger logger = LoggerFactory.getLogger(CallBillingProcessor.class);
	private Channel channelTwilio ;
	private Account account ;
	
	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;
	
	@Autowired
	private CallBillingLogRepository callBillingLogRepository;
	
	@Override
	public List<CallBillingLog> process(CallLog callLog) throws Exception {
		if (account == null) {
			channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
			account = channelTwilio.getChannelConnection();
		}
		List<CallBillingLog> callBillingLogs= new ArrayList<CallBillingLog>();
		
		//find prospectCallId, prospectInteractionSessionId and campaign id
		String campaignId = null;
		String prospectCallId = callLog.getCallLogMap().get("pcid");
		String prospectInteractionSessionId = callLog.getCallLogMap().get("pisid");
		HashMap<String, String> callLogMap = callLog.getCallLogMap();
		String dialCallSid = callLogMap.get("DialCallSid");
		String twilioConferenceSid = callLogMap.get("ConferenceSid");
		
		List<ProspectCallInteraction> prospectCallInteractions = null;
		if (prospectInteractionSessionId != null) { //if interaction session id is already there, then just find campaign id
			prospectCallInteractions = prospectCallInteractionRepository.findByProspectInteractionSessionId(prospectInteractionSessionId);
		} else {
			prospectCallInteractions = prospectCallInteractionRepository.findByProspectCallIdTwilioCallSid(prospectCallId, callLog.getId());
			if (prospectCallInteractions == null || prospectCallInteractions.isEmpty()) { //if interactions cannot be found based on CallSid, then find it using DialCallSid
				if (dialCallSid != null && !dialCallSid.isEmpty()) {
					prospectCallInteractions = prospectCallInteractionRepository.findByProspectCallIdTwilioCallSid(prospectCallId, dialCallSid);
				}
			}
		}
		
		if (prospectCallInteractions != null && !prospectCallInteractions.isEmpty()) { //prospectCallInteractions should not be null now
			prospectInteractionSessionId = prospectCallInteractions.get(0).getProspectCall().getProspectInteractionSessionId();
			campaignId = prospectCallInteractions.get(0).getProspectCall().getCampaignId();
		}
		
		if (campaignId == null) { //check if it is there calllogmap
			campaignId = callLog.getCallLogMap().get("campaignId");
		}
		
		if (twilioConferenceSid == null) {
			//generate a dummy conference sid, to link parent and child call together
			twilioConferenceSid = generateXTaaSConferenceSid(); //This is for backward compatibility, when calls were not made in conference
		}
		
		//prepare callbillinglog object
		try {
			CallBillingLog callBillingLog = makeCallBillingLog(account.getCall(callLog.getId()), callLog, campaignId, prospectCallId, prospectInteractionSessionId, twilioConferenceSid); 
			if (callBillingLog != null) {
				callBillingLogs.add(callBillingLog);
			}
		} catch (Exception e) {
			logger.error("Call billing Log not found for id "+callLog.getId(), e);
		}
		
		if (dialCallSid != null && !dialCallSid.isEmpty()) {
			try {
				CallBillingLog callBillingLog = makeCallBillingLog(account.getCall(dialCallSid), callLog, campaignId, prospectCallId, prospectInteractionSessionId, twilioConferenceSid);
				if (callBillingLog != null) {
					callBillingLogs.add(callBillingLog);
				}
			} catch (Exception e) {
				logger.error("Call billing Log not found for id "+dialCallSid, e);
			}
		}
		return callBillingLogs;
	}

	@SuppressWarnings("unused")
	private CallBillingLog makeCallBillingLog(Call call, CallLog callLog, String campaignId, String prospectCallId, String prospectInteractionSessionId, String twilioConferenceSid) {
		logger.debug("Batch Processing call Sid = "+call.getSid());
		//Duplicate check. Make sure, this CallSid is not already in the CallBillingLog collection
		CallBillingLog callBillingLogFromDB = callBillingLogRepository.findOneById(call.getSid());
		if (callBillingLogFromDB != null) { //call billing log already exist for this call sid
			logger.debug("CallBillingLog already exist for Call Sid [{}] in database. Skipping this record.",call.getSid());
			return null; //so return NULL
		}
		if (call != null) {
			HashMap<String, Object> callBillingMap = new HashMap<String, Object>();
			callBillingMap.put("AccountSid", call.getAccountSid());
			callBillingMap.put("AnsweredBy", call.getAnsweredBy());
			callBillingMap.put("CallerName", call.getCallerName());
			callBillingMap.put("DateCreated", call.getDateCreated());
			if (call.getDuration() != null) {
				callBillingMap.put("Duration", Integer.parseInt(call.getDuration()));
			} else {
				callBillingMap.put("Duration", 0);
			}
			callBillingMap.put("Direction", call.getDirection());
			callBillingMap.put("EndTime", call.getEndTime());
			callBillingMap.put("ForwardedFrom", call.getForwardedFrom());
			callBillingMap.put("From", call.getFrom());
			callBillingMap.put("ParentCallSid", call.getParentCallSid());
			callBillingMap.put("PhoneNumberSid", call.getPhoneNumberSid());
			if (call.getPrice() == null) {
				callBillingMap.put("Price", -0d);
			} else {
				callBillingMap.put("Price", Double.parseDouble(call.getPrice()));
			}
			callBillingMap.put("PriceUnit", call.getProperty("price_unit"));
			callBillingMap.put("Sid", call.getSid());
			callBillingMap.put("StartTime", call.getStartTime());
			callBillingMap.put("Status", call.getStatus());
			callBillingMap.put("To", call.getTo());
			callBillingMap.put("DateUpdated", call.getDateUpdated());
			callBillingMap.put("URI", call.getProperty("uri"));
			callBillingMap.put("Annotation", call.getProperty("annotation"));
			callBillingMap.put("FromFormatted", call.getProperty("from_formatted"));
			callBillingMap.put("ToFormatted", call.getProperty("to_formatted"));
			callBillingMap.put("GroupSid", call.getProperty("group_sid"));
			callBillingMap.put("ApiVersion", call.getProperty("api_version"));
			@SuppressWarnings("unchecked")
			LinkedHashMap<String, String> subresource_urisMap = (LinkedHashMap<String, String>) call.getObject("subresource_uris");
			DBObject subResource = new BasicDBObject();
			subResource.put("Notifications", subresource_urisMap.get("notifications").toString());
			subResource.put("Recordings", subresource_urisMap.get("recordings").toString());
			callBillingMap.put("SubresourceUris", subResource);
			CallBillingLog callBillingLog = new CallBillingLog();
			callBillingLog.setCallBillingLogMap(callBillingMap);
			callBillingLog.setCalllogCreatedDate(callLog.getCreatedDate());
			
			//set prospectInteractionSessionId, pcid and campaign id
			callBillingLog.setProspectCallId(prospectCallId);
			callBillingLog.setProspectInteractionSessionId(prospectInteractionSessionId);
			callBillingLog.setCampaignId(campaignId);
			
			//set CallBillingType
			CallBillingType callBillingType = CallBillingType.UNKNOWN;
			if (callBillingMap.get("Direction").equals("outbound-dial") || callBillingMap.get("Direction").equals("outbound-api")) {
				callBillingType = CallBillingType.VOICE_MINUTES;
			} else if (callBillingMap.get("Direction").equals("inbound")) {
				callBillingType = CallBillingType.CLIENT_MINUTES;
			}
			callBillingLog.setCallBillingType(callBillingType);
			
			//set ConferenceSid
			callBillingLog.setTwilioConferenceSid(twilioConferenceSid);
			
			return callBillingLog;
		} else {
			return null;
		}
	}
	
	private String generateXTaaSConferenceSid() {
		return "XCF" + StringUtils.replace(UUID.randomUUID().toString(), "-", ""); //generating a new conference sid, if twilio does not have it. This is for backward compatibility, when calls were not made in conference 
	}
}
