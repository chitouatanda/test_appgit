package com.xtaas.batch.processor;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentCalendar;
import com.xtaas.domain.valueobject.CampaignResource;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.domain.valueobject.WorkSlot;

@Component
public class TeamAvailableHrsProcessor implements ItemProcessor<Team, Team> {
	
	private static final Logger logger = LoggerFactory.getLogger(TeamAvailableHrsProcessor.class);
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Override
	public Team process(Team team) throws Exception {
		logger.debug("******** Processing Team -> Id: " + team.getId() + ", Name: "+ team.getName());
		
		final int AGENT_CALENDAR_NUM_DAYS = 90;
		double teamAvgAvailableHrs = 0;
		//get all the agents in that team
		for (TeamResource teamResource : team.getAgents()) {
			//logger.info("---- Building Calendar for Agent -> Id: " + teamResource.getResourceId());
			
			//build Calendar for Agent
			LocalDate startDate = new LocalDate();
			LocalDate endDate = startDate.plusDays(AGENT_CALENDAR_NUM_DAYS);
			AgentCalendar agentCalendar = new AgentCalendar(startDate, endDate, teamResource.getResourceId());
			for (WorkSchedule ws : teamResource.getWorkSchedules()) {
				agentCalendar.setAvailableSchedule(ws);
			}
			
			//find out the campaigns to which this Agent is allocated
			List<Campaign> allocatedToCampaigns = campaignRepository.findByAllocatedAgent(teamResource.getResourceId(), new LocalDate().toDate(), new LocalDate().toDate());
			for (Campaign campaign : allocatedToCampaigns) {
				CampaignResource campaignResource = campaign.getTeam().getCampaignResource(teamResource.getResourceId()); //find out the current agent and mark the allocated schedule as busy in Agent Calendar
				for (WorkSchedule ws : campaignResource.getWorkSchedules()) {
					agentCalendar.setBusySchedule(ws, campaign.getId());
				}
			}
			Map<LocalDate, TreeSet<WorkSlot>> aCalendar = agentCalendar.getAgentCalendar();
			logger.debug("Generated Calendar for Agent [{}] : [{}]", teamResource.getResourceId(), aCalendar.toString());
			
			int totalAvailableHrs = agentCalendar.getTotalAvailableHrs();
			logger.debug("Total Available Hrs for Agent [{}] is [{}] in [" + AGENT_CALENDAR_NUM_DAYS + "] days", teamResource.getResourceId(), totalAvailableHrs);
			double monthlyAvgAvailableHrs = Math.round(((double)totalAvailableHrs/3)*100.0)/100.0; //30 days avg
			teamResource.setAverageAvailableHours(monthlyAvgAvailableHrs); //get the avg available hrs 
			teamAvgAvailableHrs += teamResource.getAverageAvailableHours();
			logger.debug("---- Monthly Avg Available Hrs for Agent [{}] is [{}]", teamResource.getResourceId(), teamResource.getAverageAvailableHours());
		} //end for
		
		//calculate team's average available hrs which is sum of avg available hrs of agents 
		team.setAverageAvailableHours(teamAvgAvailableHrs);
		logger.debug("Team [{}] Average Available Hrs is [{}]", team.getId(), teamAvgAvailableHrs);
		logger.debug("******** Finished Processing Team -> Id: " + team.getId() + ", Name: "+ team.getName());
		return team;
	}
}