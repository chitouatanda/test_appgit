/**
 * 
 */
package com.xtaas.channel;

/**
 * A wrapper over actual Channel Connection object
 * 
 * @author djain
 *
 */
public class ChannelConnection<T> {
	private T channelConnection;
	
	public ChannelConnection(T channelConnection) {
		this.channelConnection =  channelConnection;
	}

	public T getChannelConnection() {
		return this.channelConnection ;
	}
}
