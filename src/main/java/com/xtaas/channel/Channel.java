/**
 * 
 */
package com.xtaas.channel;

/**
 * @author djain
 *
 */
public abstract class Channel {
	private ChannelType channelType;
	private ChannelConnection<?> channelConnection;
	
	public Channel(ChannelType channelType) {
		this.setChannelType(channelType);
		this.channelConnection = makeConnection();
	}
	
	protected abstract <T> ChannelConnection<T> makeConnection() ;

	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getChannelConnection() {
		return (T) this.channelConnection.getChannelConnection();
	}

}
