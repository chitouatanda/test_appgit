/**
 * 
 */
package com.xtaas.channel;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.Account;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;

/**
 * @author djain
 *
 */
public class ChannelTwilio extends Channel {
	private static String ACCOUNT_SID = ApplicationEnvironmentPropertyUtils.getTwilioAccountSid();
    private static String AUTH_TOKEN = ApplicationEnvironmentPropertyUtils.getTwilioAuthToken();
    
    //Early instantiation of Service class. It's a Singleton object 
  	private final static ChannelTwilio instance = new ChannelTwilio(ChannelType.TWILIO);
    
	private ChannelTwilio(ChannelType channelType) {
		super(channelType);
	}
	
	public static ChannelTwilio getInstance() {
		return instance;
	}

	/* (non-Javadoc)
	 * @see com.xtaas.channel.Channel#makeConnection(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected ChannelConnection<Account> makeConnection() {
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        Account account = client.getAccount();
		return new ChannelConnection<Account>(account);
	}
}
