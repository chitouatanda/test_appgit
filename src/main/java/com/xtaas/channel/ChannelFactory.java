/**
 * 
 */
package com.xtaas.channel;

/**
 * @author djain
 *
 */
public class ChannelFactory {
	
	public static Channel createChannel(ChannelType type) {
		Channel channel = null;
		switch (type) {
			case TWILIO : channel = ChannelTwilio.getInstance(); break;  //Returns a singleton object, to use pre-connected Twilio instance
		}
		return channel;
	}

}
