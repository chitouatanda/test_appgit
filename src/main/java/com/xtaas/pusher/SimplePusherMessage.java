/**
 * 
 */
package com.xtaas.pusher;

/**
 * A object which is passed as a Pusher message to subscribed channels 
 * 
 * @author djain
 *
 */
public class SimplePusherMessage {
	private String id;
	private String message;
	private String action;
	
	public SimplePusherMessage(String id, String message, String action) {
		this.setId(id);
		this.setMessage(message);
		this.setAction(action);
	}
	
	public String getId() {
		return id;
	}
	/**
	 * @param message the message to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/*
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
}

