package com.xtaas.datagenarator.specification;

public class TeamSpecification {
	private int count;
	private int minAverageAvailableHours;
	private int maxAverageAvailableHours;
	private int stepAverageAvailableHours;
	private int totalCampaigns;
	private int minTotalReviews;
	private int maxTotalReviews;
	private int minPerLeadPrice;
	private int maxPerLeadPrice;
	private int minAverageConversion;
	private int maxAverageConversion;
	private int minAverageQuality;
	private int maxAverageQuality;
	private int volumePerCampaignPerResource;
	
	public TeamSpecification(int count, int minAverageAvailableHours, int maxAverageAvailableHours,
			int stepAverageAvailableHours, int totalCampaigns, int minTotalReviews, int maxTotalReviews,
			int minPerLeadPrice, int maxPerLeadPrice, int minAverageConversion, int maxAverageConversion,
			int minAverageQuality, int maxAverageQuality, int volumePerCampaignPerResource) {
		this.count = count;
		this.minAverageAvailableHours = minAverageAvailableHours;
		this.maxAverageAvailableHours = maxAverageAvailableHours;
		this.stepAverageAvailableHours = stepAverageAvailableHours;
		this.totalCampaigns = totalCampaigns;
		this.minTotalReviews = minTotalReviews;
		this.maxTotalReviews = maxTotalReviews;
		this.minPerLeadPrice = minPerLeadPrice;
		this.maxPerLeadPrice = maxPerLeadPrice;
		this.minAverageConversion = minAverageConversion;
		this.maxAverageConversion = maxAverageConversion;
		this.minAverageQuality = minAverageQuality;
		this.maxAverageQuality = maxAverageQuality;
		this.volumePerCampaignPerResource = volumePerCampaignPerResource;
	}

	public int getCount() {
		return count;
	}

	public int getMinAverageAvailableHours() {
		return minAverageAvailableHours;
	}

	public int getMaxAverageAvailableHours() {
		return maxAverageAvailableHours;
	}

	public int getStepAverageAvailableHours() {
		return stepAverageAvailableHours;
	}

	public int getTotalCampaigns() {
		return totalCampaigns;
	}

	public int getMinTotalReviews() {
		return minTotalReviews;
	}

	public int getMaxTotalReviews() {
		return maxTotalReviews;
	}

	public int getMinPerLeadPrice() {
		return minPerLeadPrice;
	}

	public int getMaxPerLeadPrice() {
		return maxPerLeadPrice;
	}

	public int getMinAverageConversion() {
		return minAverageConversion;
	}

	public int getMaxAverageConversion() {
		return maxAverageConversion;
	}

	public int getMinAverageQuality() {
		return minAverageQuality;
	}

	public int getMaxAverageQuality() {
		return maxAverageQuality;
	}

	public int getVolumePerCampaignPerResource() {
		return volumePerCampaignPerResource;
	}

}
