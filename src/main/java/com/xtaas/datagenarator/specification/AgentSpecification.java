package com.xtaas.datagenarator.specification;

public class AgentSpecification {
	private int count;
	private int minAverageAvailableHours;
	private int maxAverageAvailableHours;
	private int stepAverageAvailableHours;
	private int totalCompletedCampaigns;
	private int minTotalReviews;
	private int maxTotalReviews;
	private int minPerHourPrice;
	private int maxPerHourPrice;
	private int minAverageConversion;
	private int maxAverageConversion;
	private int minAverageQuality;
	private int maxAverageQuality;
	private int volumePerCampaign;

	public AgentSpecification(int count, int minAverageAvailableHours, int maxAverageAvailableHours,
			int stepAverageAvailableHours, int totalCompletedCampaigns, int minTotalReviews,
			int maxTotalReviews, int minPerHourPrice, int maxPerHourPrice, int minAverageConversion,
			int maxAverageConversion, int minAverageQuality, int maxAverageQuality, int volumePerCampaign) {
		this.count = count;
		this.minAverageAvailableHours = minAverageAvailableHours;
		this.maxAverageAvailableHours = maxAverageAvailableHours;
		this.stepAverageAvailableHours = stepAverageAvailableHours;
		this.totalCompletedCampaigns = totalCompletedCampaigns;
		this.minTotalReviews = minTotalReviews;
		this.maxTotalReviews = maxTotalReviews;
		this.minPerHourPrice = minPerHourPrice;
		this.maxPerHourPrice = maxPerHourPrice;
		this.minAverageConversion = minAverageConversion;
		this.maxAverageConversion = maxAverageConversion;
		this.minAverageQuality = minAverageQuality;
		this.maxAverageQuality = maxAverageQuality;
		this.volumePerCampaign = volumePerCampaign;
	}

	public int getCount() {
		return count;
	}

	public int getMinAverageAvailableHours() {
		return minAverageAvailableHours;
	}

	public int getMaxAverageAvailableHours() {
		return maxAverageAvailableHours;
	}

	public int getStepAverageAvailableHours() {
		return stepAverageAvailableHours;
	}

	public int getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}

	public int getMinTotalReviews() {
		return minTotalReviews;
	}

	public int getMaxTotalReviews() {
		return maxTotalReviews;
	}

	public int getMinPerHourPrice() {
		return minPerHourPrice;
	}

	public int getMaxPerHourPrice() {
		return maxPerHourPrice;
	}

	public int getMinAverageConversion() {
		return minAverageConversion;
	}

	public int getMaxAverageConversion() {
		return maxAverageConversion;
	}

	public int getMinAverageQuality() {
		return minAverageQuality;
	}

	public int getMaxAverageQuality() {
		return maxAverageQuality;
	}

	public int getVolumePerCampaign() {
		return volumePerCampaign;
	}

}
