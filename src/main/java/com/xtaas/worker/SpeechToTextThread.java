package com.xtaas.worker;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
//import com.amazonaws.services.transcribe.AmazonTranscribe;
//import com.amazonaws.services.transcribe.AmazonTranscribeClient;
//import com.amazonaws.services.transcribe.AmazonTranscribeClientBuilder;
//import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest;
//import com.amazonaws.services.transcribe.model.LanguageCode;
//import com.amazonaws.services.transcribe.model.Media;
//import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest;
//import com.amazonaws.services.transcribe.model.TranscriptionJob;
//import com.amazonaws.services.transcribe.model.TranscriptionJobStatus;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.VoiceMailRepository;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.valueobjects.AmazonTranscription;
import com.xtaas.valueobjects.Transcript;

//@Component
public class SpeechToTextThread  {
//	private static final Logger logger = LoggerFactory.getLogger(CatchOSSignalThread.class);
//	
//	String clientRegion = "us-west-2";//This should be part of campaign object
//	 String bucketName = "insideup";// This should be part of campaign object.
//	 String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
//	 String awsSecretKey = "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";
//	 String speechBucket = "xtaasspeechtotext";
//		String recordingBucketName = "xtaasrecordings";
//		/////////////// Totem urls
//		String totem_base_url = "https://www.totemify.com";
//		String totme_reate_url = "/api/v1/graph/campaign/create";
//		String totem_score_url = "/api/v1/graph/campaign/score";
//		String totem_update_url = "/api/v1/graph/campaign/update";
//		String totem_list_url = "/api/v1/graph/list";
//		String totem_delete_url = "/api/v1/graph/delete";
//		///////// Totem urls
//		
//		String totem_token = "3854b6909c4d8f74411e9802f0aa041c63c06";
//		String totem_api_key = "6a02c13cf74411e9802f0aa041c63c066a02c13df74411e9802f0aa041c63c06";
//
//
//	 private static final String SUFFIX = "/";
//	 boolean flag=false;
//
//	@Autowired
//	private ProspectCallLogRepository prospectCallLogRepository;
//	
//	@Autowired
//	private VoiceMailRepository voiceMailRepository;
//
//	@Autowired
//	private PropertyService propertyService;
//	
//	 private String startDateStr = "1 Days";//1 Months, 1 Weeks, 1 Years
//
//	@SuppressWarnings("restriction")
//	@Override
//	public void run() {
//		logger.debug("run() : speech to text  STARTED");
//		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
//		SecurityContextHolder.getContext()
//				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
//		//int retry = 0;
//		while (!Thread.interrupted()) {
//			try {
//				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
//				String instance = propertyService.getApplicationPropertyValue(
//						XtaasConstants.APPLICATION_PROPERTY.ZOOM_TOKEN_THREAD_INSTANCE.name(), "xtaasupgrade");
//				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
//					  convertSpeechToText();
//			       
//					//updateDataBuyQueue(retry);
//				}
//				Thread.sleep(1000l);
//			} catch (Exception e) {
//				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
//			}
//		}
//	}
//
//	private void convertSpeechToText() throws Exception {
//		
//	
//		
//		System.setProperty("aws.accessKeyId", "AKIAJZ5EWMT6OAKGOGNQ");
//		System.setProperty("aws.secretKey", "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");
//		//String oAuth = "";
//		AmazonS3 s3client = createAWSConnection(clientRegion);
//		AmazonTranscribe client = AmazonTranscribeClient.builder().withRegion(clientRegion).build();
//		
//		/* Date startDate = XtaasDateUtils.getDate(getStartDate(startDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
//	        Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
//	        System.out.println(startDateInUTC);
//		
//		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();*/
//	
//		/*ProspectCallLog p1 = prospectCallLogRepository.findByProspectCallId("ecb67440-accb-4f85-ab14-97731e505464");
//		ProspectCallLog p2 = prospectCallLogRepository.findByProspectCallId("0b15454c-5545-46f5-b9aa-044863789b33");
//		
//		prospectCallLogList.add(p1);
//		prospectCallLogList.add(p2);*/
//		//List<String> campaignIds = new ArrayList<String>();
//
//		/*campaignIds.add("5b755900e4b066e7dba3080b");
//		campaignIds.add("5ca4a9a4e4b04d84f28b3d71");
//		campaignIds.add("5cfebb64e4b052fb1ce14511");
//		campaignIds.add("5d48aa60e4b0152590eb27c5");
//		campaignIds.add("5d6e5b46e4b0ffdcea0ed070");
//		campaignIds.add("5d7a9e25e4b0de39963eb494");
//		campaignIds.add("5d9390b5e4b06c5fd3fa8420");
//		campaignIds.add("5d9bb208e4b0e1ff2f80e068");
//		campaignIds.add("5d9c71e4e4b0755cbbfb5301");*/
//		
//		//prospectCallLogList = prospectCallLogRepository.findContactsBySuccess("5b755900e4b066e7dba3080b",startDateInUTC);
//		//prospectCallLogList = prospectCallLogRepository.findContactsBySuccess("5d47e5aae4b0152590e46905",startDateInUTC);
//		
//		//TODO doing this just for voicemail collection, later need to update this.
//		List<VoiceMail> voiceMails = voiceMailRepository.findByIsTranscribed(false);
//		
//		if(voiceMails!=null && voiceMails.size()>0) {
//			for(VoiceMail vm : voiceMails) {
//				String voiceUrl = "";
//				if(vm.getCallParams()!=null && vm.getCallParams().containsKey("RecordUrl")) {//This is for plivo
//					voiceUrl = vm.getCallParams().get("RecordUrl");
//				}else if(vm.getCallParams()!=null && vm.getCallParams().containsKey("RecordingUrl")) {// This is for twilio
//					voiceUrl = vm.getCallParams().get("RecordingUrl");
//				}
//				if(voiceUrl!=null && !voiceUrl.isEmpty()) {
//					StringBuffer speechText = new StringBuffer();
//
//					
//					//campaignList.add(campaignx);
//			        String awsurl = voiceUrl;
//			        String rFolder = "";
//			        String recordingFileName = "";
//					if (awsurl != null) {
//						String[] urlarr = awsurl.split(SUFFIX);
//						rFolder = urlarr[4];
//						recordingFileName = urlarr[5];
//					}
//			        boolean isFileExistsInS3 = s3client.doesObjectExist(recordingBucketName, "recordings" + SUFFIX + recordingFileName);
//			        if (isFileExistsInS3) {
//			        	
//			            String transcriptionJobName = recordingFileName+Math.random()+"x1";
//			        	//int endIndex = recordingFileName.length()-4;
//	                	//String speechFile = recordingFileName.substring(0, endIndex);
//	                	//speechFile = speechFile+".json";
//	                	//System.out.println(pcl.getProspectCall().getProspectCallId()+"--->"+speechFile);
//						//boolean fileDownloaded = s3client.doesObjectExist(speechBucket, speechFile+"1");
//						//if(!fileDownloaded) {
//							StartTranscriptionJobRequest request = new StartTranscriptionJobRequest();
//		
//				            Media media = new Media();
//		
//				            media.setMediaFileUri(s3client.getUrl(recordingBucketName, "recordings" + SUFFIX + recordingFileName).toString());
//		
//				            request.withMedia(media).withMediaSampleRateHertz(8000);
//							
////							request.withLanguageCode(LanguageCode.EnUS);
//				            request.setTranscriptionJobName(transcriptionJobName);
//				            request.withMediaFormat("wav");
//		
//				            client.startTranscriptionJob(request);
//				            
//				            
//				            GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest();
//				            jobRequest.setTranscriptionJobName(transcriptionJobName);
//				            TranscriptionJob transcriptionJob;
//		
//				            while( true ){
//				                transcriptionJob = client.getTranscriptionJob(jobRequest).getTranscriptionJob();
//				                if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.COMPLETED.name()) ){
//		
//				                	AmazonTranscription transcription = this.download( transcriptionJob.getTranscript().getTranscriptFileUri());
//				                	if(transcription!=null && transcription.getResults()!=null && transcription.getResults().getTranscripts() !=null 
//				                			&& transcription.getResults().getTranscripts().size()>0) {
//				                		List<Transcript> transcripts = transcription.getResults().getTranscripts();
//				                		if(transcripts !=null && transcripts.size()>0) {
//				                			for(Transcript transcript : transcripts) {
//				                				//speechText.append("{");
//				                				//speechText.append("\"transcript\": ");
//				                				//speechText.append("\"");
//				                				speechText.append(transcript.getTranscript());
//				                				//speechText.append("\"");
//				                				//speechText.append("}");
//
//				                			}
//				                		}
//				                		
//				                	}
//				                
//				                   // System.out.println("------->"+speechText.toString());
//				                    
//				                  //  s3client.putObject(speechBucket, speechFile, speechText.toString());
//				                	if(speechText!=null && !speechText.toString().isEmpty()) {
//				                		vm.setSpeech(speechText.toString());
//				                	}else {
//				                		vm.setSpeech("EMPTY");
//				                	}
//				                   voiceMailRepository.save(vm);
//				                    
//				                    break;
//		
//				                }else if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.FAILED.name()) ){
//				                	vm.setSpeech("ERROR OCCURED WHY CONVERTING VOICE TO TEXT!");
//					                   voiceMailRepository.save(vm);
//
//				                        break;
//				                }
//				                // to not be so anxious
//				                synchronized ( this ) {
//				                    try {
//				                        this.wait(50);
//				                    } catch (InterruptedException e) { }
//				                }
//		
//				            }
//						//}
//			        }else{
//			        	vm.setSpeech("RECORDING FILE NOT FOUND ON S3 STORAGE!");
//		                   voiceMailRepository.save(vm);
//
//			       	 //createFolder(bucketName,dateFolder,s3client);
//			        }
//					
//					
//				}else {
//					vm.setSpeech("EMPTY");
//	                   voiceMailRepository.save(vm);
//
//				}
//			}
//		}
//		
//		/*if(prospectCallLogList!=null) {
//			for(ProspectCallLog pcl : prospectCallLogList) {
//				StringBuffer speechText = new StringBuffer();
//
//				
//				//campaignList.add(campaignx);
//		        String awsurl = pcl.getProspectCall().getRecordingUrlAws();
//		        String rFolder = "";
//		        String recordingFileName = "";
//				if (awsurl != null) {
//					String[] urlarr = awsurl.split(SUFFIX);
//					rFolder = urlarr[4];
//					recordingFileName = urlarr[5];
//				}
//		        boolean isFileExistsInS3 = s3client.doesObjectExist(recordingBucketName, "recordings" + SUFFIX + recordingFileName);
//		        if (isFileExistsInS3) {
//		        	
//		            String transcriptionJobName = recordingFileName+"x4";
//		        	int endIndex = recordingFileName.length()-4;
//                	String speechFile = recordingFileName.substring(0, endIndex);
//                	speechFile = speechFile+".json";
//                	System.out.println(pcl.getProspectCall().getProspectCallId()+"--->"+speechFile);
//					boolean fileDownloaded = s3client.doesObjectExist(speechBucket, speechFile+"1");
//					if(!fileDownloaded) {
//						StartTranscriptionJobRequest request = new StartTranscriptionJobRequest();
//	
//			            Media media = new Media();
//	
//			            media.setMediaFileUri(s3client.getUrl(recordingBucketName, "recordings" + SUFFIX + recordingFileName).toString());
//	
//			            request.withMedia(media).withMediaSampleRateHertz(8000);
//						
//						request.withLanguageCode(LanguageCode.EnUS);
//			            request.setTranscriptionJobName(transcriptionJobName);
//			            request.withMediaFormat("wav");
//	
//			            client.startTranscriptionJob(request);
//			            
//			            
//			            GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest();
//			            jobRequest.setTranscriptionJobName(transcriptionJobName);
//			            TranscriptionJob transcriptionJob;
//	
//			            while( true ){
//			                transcriptionJob = client.getTranscriptionJob(jobRequest).getTranscriptionJob();
//			                if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.COMPLETED.name()) ){
//	
//			                	AmazonTranscription transcription = this.download( transcriptionJob.getTranscript().getTranscriptFileUri());
//			                	if(transcription!=null && transcription.getResults()!=null && transcription.getResults().getTranscripts() !=null 
//			                			&& transcription.getResults().getTranscripts().size()>0) {
//			                		List<Transcript> transcripts = transcription.getResults().getTranscripts();
//			                		if(transcripts !=null && transcripts.size()>0) {
//			                			for(Transcript transcript : transcripts) {
//			                				speechText.append("{");
//			                				speechText.append("\"transcript\": ");
//			                				speechText.append("\"");
//			                				speechText.append(transcript.getTranscript());
//			                				speechText.append("\"");
//			                				speechText.append("}");
//
//			                			}
//			                		}
//			                		
//			                	}
//			                
//			                    System.out.println("------->"+speechText.toString());
//			                    
//			                    s3client.putObject(speechBucket, speechFile, speechText.toString());
//	
//			                   
//			                    
//			                    break;
//	
//			                }else if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.FAILED.name()) ){
//	
//			                        break;
//			                }
//			                // to not be so anxious
//			                synchronized ( this ) {
//			                    try {
//			                        this.wait(50);
//			                    } catch (InterruptedException e) { }
//			                }
//	
//			            }
//					}
//		        }else{
//		       	 //createFolder(bucketName,dateFolder,s3client);
//		        }
//				
//				
//			}
//		}*/
//		
//	}
//	
//	private AmazonTranscription getTranscription(String response) {
//		AmazonTranscription amazonTranscription = null;
//		try {
//		  if (response != null) {
//	            ObjectMapper mapper= new ObjectMapper().setVisibility(JsonMethod.FIELD,Visibility.ANY);
//	            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
//	            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
//	            amazonTranscription=mapper.readValue(response.toString(), AmazonTranscription.class);
//	           
//	        }
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		  return amazonTranscription;
//	}
//	
//	private AmazonTranscription download( String uri ){
//		CloseableHttpClient httpclient = HttpClients.createDefault();
//		AmazonTranscription transcription = null;
//		try {
//	     /* HttpGet httpget = new HttpGet(uri);
//      
//	      HttpResponse httpresponse = httpclient.execute(httpget);
//	      httpresponse.setHeader("Accept-Encoding","UTF-8");
//	     result = httpresponse.charset("UTF-8").bodyText(); */
//			URL url = new URL(uri);
//			URLConnection con = url.openConnection();
//			InputStream in = con.getInputStream();
//			String encoding = con.getContentEncoding();
//			encoding = encoding == null ? "UTF-8" : encoding;
//		    String result = IOUtils.toString(in, encoding);
//			System.out.println(result);
//			transcription = getTranscription(result);
//
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//	    // result is a json 
//	    return transcription;
//	}
//	
//	 private String getStartDate(String startDate){
//		   
//		 //String toDate = "02/10/2018 00:00:00";
//          // mm/dd/yyyy
//		   String toDate="";
//		   Calendar c = Calendar.getInstance();
//		   System.out.println("Current date : " + (c.get(Calendar.MONTH) + 1) +
//				   "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));
//
//		   	String periodActual = startDate;
//	    	 int beginIndex = periodActual.indexOf(" ");
//	    	 String periodStr = periodActual.substring(0, beginIndex);
//	    	 int period = Integer.parseInt(periodStr);
//	    	 //logger.debug(period);
//		   
//		   
//		   
//		   if(startDate.contains("Years")){
//			   c.add(Calendar.YEAR, -period);
//			   toDate = (c.get(Calendar.MONTH) + 1) +
//					   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
//	    	}else if(startDate.contains("Months")){
//	    		  c.add(Calendar.MONTH, -period);
//	    		  toDate = (c.get(Calendar.MONTH) + 1) +
//	   				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
//	    	}else if(startDate.contains("Weeks")){
//	    		 c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
//	    		 toDate = (c.get(Calendar.MONTH) + 1) +
//	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
//	    	}else if(startDate.contains("Days")){
//	    		 c.add(Calendar.DAY_OF_MONTH, -period);
//	    		 toDate = (c.get(Calendar.MONTH) + 1) +
//	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
//	    	}
//		   
//		
//		  
//		   return toDate;
//				   
//				   
//	   }
//	 
//	 private AmazonS3 createAWSConnection(String clientRegion) {
//			AmazonS3 s3client = null;
//			try {
//				BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
//				 s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
//				return s3client;
//			}
//			catch(Exception e){
//				e.printStackTrace();
//				   System.out.println("Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :" +e);
//				return s3client;
//			}
//		}
//	 
//	 private AmazonTranscribe createTranscribeConnection(String clientRegion) {
//		 AmazonTranscribe transcribe = null;
//			try {
//				BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
//				transcribe = AmazonTranscribeClientBuilder.standard().withRegion(clientRegion).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
//				return transcribe;
//			}
//			catch(Exception e){
//				e.printStackTrace();
//				   System.out.println("Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :" +e);
//				return transcribe;
//			}
//		}
//	 
//	 private void sendPost(String url) throws Exception {
//
//	        HttpPost post = new HttpPost("https://www.totemify.com");
//
//	        // add request parameter, form parameters
//	        List<NameValuePair> urlParameters = new ArrayList<>();
//	        urlParameters.add(new BasicNameValuePair("username", "abc"));
//	        urlParameters.add(new BasicNameValuePair("password", "123"));
//	        urlParameters.add(new BasicNameValuePair("custom", "secret"));
//
//	        post.setEntity(new UrlEncodedFormEntity(urlParameters));
//
//	        try (CloseableHttpClient httpClient = HttpClients.createDefault();
//	             CloseableHttpResponse response = httpClient.execute(post)) {
//
//	            System.out.println(EntityUtils.toString(response.getEntity()));
//	        }
//
//	    }

}
