package com.xtaas.worker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.utils.XtaasConstants;

@Component
public class ProspectCallDetailsUpdateThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ProspectCallDetailsUpdateThread.class);

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private CallLogRepository callLogRepository;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private CampaignService campaignService;

	@Override
	public void run() {
		logger.debug("run() : ProspectCallDetailsUpdateThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken("prospect_call_details_update", "secret", authorities));
		while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.PROSPECT_CALL_DETAILS_UPDATE_THREAD_INSTANCE.name(),
						"xtaasupgrade");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				List<Campaign> campaigns = campaignService.getActiveCampaigns();
				if (campaigns != null && campaigns.size() > 0) {
					logger.debug("run() : Found [{}] Active Campaigns.", campaigns.size());
					for (Campaign activeCampaign : campaigns) {
						// if (activeCampaign.isCallStatusEntry()) {
						processProspects(activeCampaign.getId());
						// }
					}
				}
				}
				Thread.sleep(300000);
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
			}
		}
	}

	private void processProspects(String activeCampaignId) {
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -5);
		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.MINUTE, -10);
		List<ProspectCallLog> prospectCallLogs = prospectCallLogRepository.findAllByUpdatedDate(activeCampaignId,
				startDate.getTime(), endDate.getTime());
		if (prospectCallLogs != null && prospectCallLogs.size() > 0) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				boolean isModified = false;
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
					ProspectCall prospectCall = prospectCallLog.getProspectCall();
					int maxDialerCodeLimit = propertyService.getIntApplicationPropertyValue(
							XtaasConstants.APPLICATION_PROPERTY.MAX_DIALERCODE_LIMIT.name(), 3);
					int maxMultiProspectDialerCodeLimit = propertyService.getIntApplicationPropertyValue(
							XtaasConstants.APPLICATION_PROPERTY.MAX_MULTIPROSPECT_DIALERCODE_LIMIT.name(), 3);
					if (prospectCall.getProspect() != null) {
						if (prospectCall.getProspect().isDirectPhone()
								&& prospectCall.getCallRetryCount() >= maxDialerCodeLimit
								&& prospectCall.getDispositionStatus() != null && prospectCall.getDispositionStatus()
										.equalsIgnoreCase(DispositionType.DIALERCODE.name())) {
							prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							prospectCallLog.getProspectCall().setProspectInteractionSessionId(null);
							isModified = true;
							logger.debug(
									"ProspectCallDetailsUpdateThread() - ProspectCallId [{}] modified for [MAX_RETRY_LIMIT_REACHED].",
									prospectCall.getProspectCallId());
						} else if (!prospectCall.getProspect().isDirectPhone()
								&& prospectCall.getCallRetryCount() >= maxMultiProspectDialerCodeLimit
								&& prospectCall.getDispositionStatus() != null
								&& !prospectCall.getDispositionStatus().equalsIgnoreCase(DispositionType.SUCCESS.name())
								&& !prospectCall.getDispositionStatus()
										.equalsIgnoreCase(DispositionType.FAILURE.name())) {
							prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							prospectCallLog.getProspectCall().setProspectInteractionSessionId(null);
							isModified = true;
							logger.debug(
									"ProspectCallDetailsUpdateThread() - ProspectCallId [{}] modified for [MAX_RETRY_LIMIT_REACHED].",
									prospectCall.getProspectCallId());
						}
					}
					if (prospectCall.getRecordingUrl() == null || prospectCall.getRecordingUrl().isEmpty()
							|| prospectCall.getTelcoDuration() == null || prospectCall.getTelcoDuration() == 0) {
						if (prospectCall.getTwilioCallSid() != null) {
							CallLog callLog = callLogRepository.findOneById(prospectCall.getTwilioCallSid());
							if (callLog != null) {
								prospectCallLog.getProspectCall()
										.setRecordingUrl(callLog.getCallLogMap().get("RecordingUrl"));
								String telcoDuration = (callLog.getCallLogMap().get("DialCallDuration") == null)
										? callLog.getCallLogMap().get("RecordingDuration")
										: callLog.getCallLogMap().get("DialCallDuration");
								if (telcoDuration != null && !telcoDuration.isEmpty()) {
									prospectCallLog.getProspectCall().setTelcoDuration(Integer.valueOf(telcoDuration));
								} else {
									prospectCallLog.getProspectCall().setTelcoDuration(null);
								}
								isModified = true;
								logger.debug(
										"ProspectCallDetailsUpdateThread() - ProspectCallId [{}] modified for [RecordingUrl].",
										prospectCall.getProspectCallId());
							}
						}
					}
					if (isModified) {
						prospectCallLogService.save(prospectCallLog, false);
					}
				}
			}
		}
	}
}
