package com.xtaas.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Login;
import com.xtaas.service.DownloadClientMappingReportServiceImpl;
import com.xtaas.service.LeadReportServiceImpl;
import com.xtaas.service.MultiCampaignLeadReportServiceImpl;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Component
public class LeadReportThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(LeadReportThread.class);

	private ProspectCallSearchDTO prospectCallSearchDTO;

	private Login login;

	private String version;

	@Autowired
	private LeadReportServiceImpl leadReportServiceImpl;

	@Autowired
	private MultiCampaignLeadReportServiceImpl multiCampaignLeadReportServiceImpl;

	@Autowired
	private DownloadClientMappingReportServiceImpl downloadClientMappingReportServiceImpl;

	public LeadReportThread() {

	}

	public LeadReportThread(ProspectCallSearchDTO prospectCallSearchDTO, Login login, String version) {
		this.prospectCallSearchDTO = prospectCallSearchDTO;
		this.login = login;
		this.version = version;
		leadReportServiceImpl = BeanLocator.getBean("leadReportServiceImpl", LeadReportServiceImpl.class);
		multiCampaignLeadReportServiceImpl = BeanLocator.getBean("multiCampaignLeadReportServiceImpl",
				MultiCampaignLeadReportServiceImpl.class);
		downloadClientMappingReportServiceImpl = BeanLocator.getBean("downloadClientMappingReportServiceImpl",
				DownloadClientMappingReportServiceImpl.class);
	}

	@Override
	public void run() {
		try {
			if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
				if (!this.version.isEmpty() && this.version.equalsIgnoreCase("old")) {
					leadReportServiceImpl.downloadClientReportByFilter(prospectCallSearchDTO, this.login);
				} else {
					downloadClientMappingReportServiceImpl.downloadLeadReportWithClientMappings(prospectCallSearchDTO,
							this.login);
				}
			} else {
				multiCampaignLeadReportServiceImpl.downloadMultiCampaignLeadReport(prospectCallSearchDTO, login);
			}
		} catch (Exception e) {
			logger.error("==========> Error occurred in LeadReportThread <==========");
			logger.error("Expection : ", e);
		}
	}
}
