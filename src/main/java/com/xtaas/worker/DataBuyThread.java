package com.xtaas.worker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.util.concurrent.RateLimiter;
import com.xtaas.db.repository.*;
import com.xtaas.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.ZoomBuySearchDTO;

@Component
public class DataBuyThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DataBuyThread.class);

	@Autowired
	private ZoomInfoContactListProviderNewImpl zoomInfoContactListProviderNewImpl;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private ApplicationPropertyService applicationPropertyService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Autowired
	private ApplicationPropertyRepository applicationPropertyRepository;

	public RateLimiter rateLimiter = null;

	boolean isServerGettingStarted;

	public boolean isServerGettingStarted() {
		return isServerGettingStarted;
	}

	public void setServerGettingStarted(boolean isServerGettingStarted) {
		this.isServerGettingStarted = isServerGettingStarted;
	}

	@Override
	public void run() {
		logger.debug("run() : DataBuyThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DATA_BUY_THREAD_INSTANCE.name(), "xtaascorp.herokuapp.com");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {

					// below logic for rate limit for zoomdata buy
					List<KeyValuePair<String, Double>> rateLimitPriority = propertyService.getDataBuyRateLimiterValue();
					if(CollectionUtils.isNotEmpty(rateLimitPriority) && rateLimiter == null){
						for(KeyValuePair<String, Double> kv : rateLimitPriority){
							if(serverUrl.toLowerCase().contains(kv.getKey())){
								rateLimiter = RateLimiter.create(kv.getValue());
							}
						}
					}

					plivoOutboundNumberService.cacheAllPlivoOutboundNumberForDatabuy();
					purchaseRecordsJob(isServerGettingStarted);


				}
				this.setServerGettingStarted(false);

				Thread.sleep(12000);
			} catch (Exception e) {
				try {
					logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
					Thread.sleep(12000);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public String purchaseRecordsJob(boolean isServerGettingStarted) {
		int restrictSalutryBuy = 5;
		String statusMessage = "";
		int qcounter = 0;
		int pcounter = 0;
		int salutaryinprogress = 0;
		logger.debug("In PurchaseRecrodJob");
		try {
			List<String> databuycleanuplist = new ArrayList<String>();
			databuycleanuplist.add(DataBuyQueueStatus.READY_FOR_CLEANUP.toString());

			Pageable pageable1 = PageRequest.of(0, 10, Direction.ASC, "jobRequestTime");
			List<DataBuyQueue> dataCleanList = dataBuyQueueRepository.findByStatus(databuycleanuplist, pageable1);

			if(dataCleanList!=null)
				for (DataBuyQueue dbqclean : dataCleanList) {
					Campaign campaign = campaignRepository.findOneById(dbqclean.getCampaignId());
					dbqclean.setStatus(DataBuyQueueStatus.CLEANUP_INPROCESS.toString());
					dbqclean = dataBuyQueueRepository.save(dbqclean);
					DataBuyCleanup dbc = new DataBuyCleanup(dbqclean, campaign, dbqclean.getTotalBought());
					Thread thread = new Thread(dbc);
					thread.start();
					//dataBuyCleanup.dataCleanupAfterBuy(dbqclean, campaign, dbqclean.getTotalBought());
				}


		}catch(Exception e) {
			e.printStackTrace();
		}


		List<DataBuyQueue> queuedList = new ArrayList<DataBuyQueue>();
		try {
			List<DataBuyQueue> dataBuyQueueList = getCampaignsFromQueue();

			for (DataBuyQueue dbq : dataBuyQueueList) {

				if (dbq.getStatus().equals(DataBuyQueueStatus.PAUSED.toString())) {
					queuedList.add(dbq);
					qcounter++;
				} 
					  // else if (dbq.getStatus().equals("PASS")) { queuedList.add(dbq); qcounter++; }
				else if (dbq.getStatus().equals(DataBuyQueueStatus.PAUSEDZOOM.toString())) {
					queuedList.add(dbq);
					qcounter++;
				} else if (dbq.getStatus().equals(DataBuyQueueStatus.QUEUED.toString())) {
					queuedList.add(dbq);
					qcounter++;
				} else if (dbq.getStatus().equals(DataBuyQueueStatus.INPROCESS.toString())) {
					pcounter++;
					if(dbq.getSalutaryRequestCount()!=null && dbq.getSalutaryPurchasedCount()==null) {
						salutaryinprogress++;
					}
				}
			}
			int maxjobbuys = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_BUY_JOBS.name(), 10);
			if (pcounter < maxjobbuys) {
				if (qcounter > 0 && salutaryinprogress<=restrictSalutryBuy) {
					DataBuyQueue dbq = queuedList.get(0);
					if (dbq.isSearchCompany()) {
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
						dataBuyQueueRepository.save(dbq);
						ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
						logger.debug("Starting Search Company");
						zoomBuySearchDTO.setDataBuyQueue(dbq);
						ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(zoomBuySearchDTO);
						Thread t1 = new Thread(zilp, "t" + Math.random());
						t1.start();
					} else if (dbq.getStatus().equals(DataBuyQueueStatus.PAUSEDZOOM.toString())) {
						// This i have kept if the server restart or stuck, to start from where it has
						// been stopped
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
						dataBuyQueueRepository.save(dbq);
						logger.debug("Starting ZoomInfo Buy");
						ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
						zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
						zoomBuySearchDTO.setTotalCount(dbq.getRequestCount());
						zoomBuySearchDTO.setDataBuyQueue(dbq);
						ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(zoomBuySearchDTO);
						Thread t1 = new Thread(zilp, "t" + Math.random());
						t1.start();
					} else {
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
						dataBuyQueueRepository.save(dbq);
						buyFromOtherSources(dbq);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusMessage;
	}

	public List<DataBuyQueue> getCampaignsFromError() {

		List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>();
		List<String> dataBuyStatusList = new ArrayList<String>();
		try {
			dataBuyStatusList.add(DataBuyQueueStatus.ERROR.toString());
			Pageable pageable = PageRequest.of(0, 100, Direction.ASC, "jobRequestTime");
			dataBuyList = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataBuyList;
	}

	public List<DataBuyQueue> getCampaignsFromQueue() {
		List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>();
		List<DataBuyQueue> dataBuyListStarting = new ArrayList<DataBuyQueue>();
		List<String> dataBuyStatusList = new ArrayList<String>();

		dataBuyStatusList.add(DataBuyQueueStatus.INPROCESS.toString());

		if (this.isServerGettingStarted) {
			Pageable pageableQ = PageRequest.of(0, 500, Direction.ASC, "jobRequestTime");
			dataBuyListStarting = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageableQ);
			if (dataBuyListStarting != null) {
				for (DataBuyQueue dbq : dataBuyListStarting) {
					dbq.setStatus(DataBuyQueueStatus.PAUSED.toString());
					dbq.setInterupted(true);
					dataBuyQueueRepository.save(dbq);
				}
			}
		}

		try {
			dataBuyStatusList.add(DataBuyQueueStatus.QUEUED.toString());
			dataBuyStatusList.add(DataBuyQueueStatus.PAUSED.toString());
			// dataBuyStatusList.add("PASS");
			dataBuyStatusList.add(DataBuyQueueStatus.PAUSEDZOOM.toString());

			Pageable pageable = PageRequest.of(0, 500, Direction.ASC, "jobRequestTime");
			dataBuyList = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataBuyList;
	}

	private void buyFromOtherSources(DataBuyQueue dbq) {
		BuyMultiSourceDataThread bmsdt = new BuyMultiSourceDataThread(dbq);
		Thread t1 = new Thread(bmsdt, "t" + Math.random());
		t1.start();
	}

}
