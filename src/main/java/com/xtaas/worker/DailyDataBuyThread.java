package com.xtaas.worker;

import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.job.DailyDataBuyJob;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.ZoomBuySearchDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DailyDataBuyThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DailyDataBuyThread.class);

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private DailyDataBuyJob dailyDataBuyJob;

	boolean isServerGettingStarted;

	public boolean isServerGettingStarted() {
		return isServerGettingStarted;
	}

	public void setServerGettingStarted(boolean isServerGettingStarted) {
		this.isServerGettingStarted = isServerGettingStarted;
	}

	@Override
	public void run() {
		logger.debug("run() : DailyDataBuyThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DATA_BUY_THREAD_INSTANCE.name(), "xtaascorp.herokuapp.com");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
					dailyDataBuyJob(isServerGettingStarted);
				}
				this.setServerGettingStarted(false);

				Thread.sleep(12000);
			} catch (Exception e) {
				try {
					logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
					Thread.sleep(12000);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public void dailyDataBuyJob(boolean isServerGettingStarted) {
		logger.debug("In DailyDataBuyJob");
		dailyDataBuyJob.dailyDataBuy();

	}



}
