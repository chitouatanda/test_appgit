package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import com.xtaas.BeanLocator;

import com.xtaas.db.entity.Login;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.SupervisorServiceImpl;

@Component
public class CampaignReportThread implements Runnable{

	private static final Logger logger = LoggerFactory.getLogger(CampaignReportThread.class);

	@Autowired
	private SupervisorServiceImpl supervisorServiceImpl;

	private Login login;

	public CampaignReportThread(Login login) {
		this.login = login;
		supervisorServiceImpl = BeanLocator.getBean("supervisorServiceImpl",
				SupervisorServiceImpl.class);
	}
	
	@SuppressWarnings("unused")
	private CampaignReportThread() {

	}

	@Override
	public void run() {
		try {
			logger.debug("run() : CampaignReportThread STARTED...");
			supervisorServiceImpl.downloadCampaignReport(login);
			logger.debug("run() : CampaignReportThread COMPLETED...");
		} catch (Exception e) {
			logger.error("run() : CampaignReportThread failed with error: {}", e.getMessage());
			e.printStackTrace();
		}

	}

}
