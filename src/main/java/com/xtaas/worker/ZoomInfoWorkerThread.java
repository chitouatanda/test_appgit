package com.xtaas.worker;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import com.xtaas.BeanLocator;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.web.dto.ZoomBuySearchDTO;

@ComponentScan("com.xtaas.worker")
public class ZoomInfoWorkerThread extends DataBuyMultiSourceThread {
	private static final Logger logger = LoggerFactory.getLogger(ZoomInfoWorkerThread.class);
	
	private ZoomInfoDataBuy zoomInfoDataBuy;
	
	private DataBuyQueueRepository dataBuyQueueRepository;


	public ZoomInfoWorkerThread(CountDownLatch latch,Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
		super("ZoomInfo Service", latch,campaign,dbq,buyRequestCount);
		zoomInfoDataBuy = BeanLocator.getBean("zoomInfoDataBuy", ZoomInfoDataBuy.class);
		dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);
	}
	
	 @Override
	 public void buyData(Campaign campaign,DataBuyQueue dbq, Long zoominfoBuyRequest) 
	 {
		 logger.info("Checking " + this.getServiceName());
	        try
	        {
	        	if(zoominfoBuyRequest!=null && zoominfoBuyRequest>0) {
		        	//Thread.sleep(6000);
		        	ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
		    		zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
		    		zoomBuySearchDTO.setTotalCount(zoominfoBuyRequest);
		    		zoomBuySearchDTO.setDataBuyQueue(dbq);
		    		/*ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(
		    		zoomBuySearchDTO);
		    		Thread t1 = new Thread(zilp, "t" + Math.random());
		    		t1.start();*/
		    		
		    		
		    		logger.debug("In RUN method for ZoomInfoContactListProviderNewImpl");
		    		if(zoomBuySearchDTO.getDataBuyQueue()!=null){
		    			//DataBuyQueue dbq = zoomBuySearchDTO.getDataBuyQueue();
		    			if(dbq.isSearchCompany()){
		    				zoomInfoDataBuy.companySearchFromQueue(zoomBuySearchDTO.getDataBuyQueue());
		    			}else{
		    				zoomInfoDataBuy.PurchaseRecordsFromQueue(zoomBuySearchDTO);
		    			}
		    		}else {
			    		logger.error("Nothing found in the dbq");
	
		    		}
	        	}
		}catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	        logger.info(this.getServiceName() + " is UP");
	    }
}
