package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.AllCampaignsDownloadReportServiceImpl;
import com.xtaas.service.DownloadClientMappingReportServiceImpl;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Component
public class NewLeadReportThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(LeadReportThread.class);

	private ProspectCallSearchDTO prospectcallSearchDTO;

	private Login login;
	
	private Organization organization;

	private String version;

	private HttpServletRequest request;

	private HttpServletResponse response;

	@Autowired
	private DownloadClientMappingReportServiceImpl downloadClientMappingReportServiceImpl;

	@Autowired
	AllCampaignsDownloadReportServiceImpl allCampaignsDownloadReportServiceImpl;

	@Autowired
	private NewClientMappingRepository newClientMappingRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	public NewLeadReportThread() {

	}

	public NewLeadReportThread(ProspectCallSearchDTO prospectCallSearchDTO, Login login, Organization organization, String version,
			HttpServletRequest request, HttpServletResponse response) {
		this.prospectcallSearchDTO = prospectCallSearchDTO;
		this.login = login;
		this.organization = organization;
		this.version = version;
		this.request = request;
		this.response = response;

		downloadClientMappingReportServiceImpl = BeanLocator.getBean("downloadClientMappingReportServiceImpl",
				DownloadClientMappingReportServiceImpl.class);
		allCampaignsDownloadReportServiceImpl = BeanLocator.getBean("allCampaignsDownloadReportServiceImpl",
				AllCampaignsDownloadReportServiceImpl.class);
		newClientMappingRepository = BeanLocator.getBean("newClientMappingRepository",
				NewClientMappingRepository.class);
		campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
	}

	@Override
	public void run() {
		try {
			if (prospectcallSearchDTO.getCampaignIds() != null && prospectcallSearchDTO.getCampaignIds().size() == 1) {
				if (!version.isEmpty() && version.equalsIgnoreCase("new")) {
					Campaign campaign = campaignRepository.findOneById(prospectcallSearchDTO.getCampaignIds().get(0));
					if (campaign != null) {
						List<NewClientMapping> cMListByCampaignId = newClientMappingRepository
								.findClientMappingByCampaignId(campaign.getId());
						if (cMListByCampaignId != null && cMListByCampaignId.size() > 0) {
							// campaign level template
							downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
									prospectcallSearchDTO, login, cMListByCampaignId, "SINGLE");
							logger.info("downloading campaign level template... NewClientMapping : "
									+ cMListByCampaignId.size());
						} else {
							List<NewClientMapping> cMListByClientName = newClientMappingRepository
									.findClientMappingByClientName(campaign.getOrganizationId());
							if (cMListByClientName != null && cMListByClientName.size() > 0) {
								// organization level template
								downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
										prospectcallSearchDTO, login, cMListByClientName, "ORG");
								logger.info("downloading organization level template... NewClientMapping : "
										+ cMListByClientName.size());
							} else {
								// default system template
								List<NewClientMapping> cMListBy = newClientMappingRepository
										.findClientMappingByClientName("SYSTEM_DEFAULT");
								downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
										prospectcallSearchDTO, login, cMListBy, "DEFAULT");
								logger.info("downloading default level template...");
							}
						}
					}
				}
			} else if (prospectcallSearchDTO.getCampaignIds() != null
					&& prospectcallSearchDTO.getCampaignIds().size() > 1) {
				Campaign campaign = campaignRepository.findOneById(prospectcallSearchDTO.getCampaignIds().get(0));
				if (campaign != null) {
					List<NewClientMapping> cMListByClientName = newClientMappingRepository
							.findClientMappingByClientName(campaign.getOrganizationId());
					if (cMListByClientName != null && cMListByClientName.size() > 0) {
						// organization level template
						downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
								prospectcallSearchDTO, login, cMListByClientName, "ORG" );
						logger.info("downloading organization level template... NewClientMapping : "
								+ cMListByClientName.size());
					} else {
						// default system template
						List<NewClientMapping> cMListBy = newClientMappingRepository
								.findClientMappingByClientName("SYSTEM_DEFAULT");
						downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
								prospectcallSearchDTO, login, cMListBy,"DEFAULT");
						logger.info("downloading default level template...");
					}
				}
			} else if (prospectcallSearchDTO.getCampaignIds() == null
					|| prospectcallSearchDTO.getCampaignIds().size() == 0) {

				if (this.organization != null) {
					List<String> agentIds = downloadClientMappingReportServiceImpl.getAgentIds(login);
					prospectcallSearchDTO.setAgentIds(agentIds);
					if (prospectcallSearchDTO.getCampaignIds() == null
							|| prospectcallSearchDTO.getCampaignIds().isEmpty()) {
						List<String> campaignIds = new ArrayList<String>();
						List<ProspectCallLog> prospects = new ArrayList<>();
						prospects = prospectCallLogRepository.getDistinctCampaignIds(agentIds, prospectcallSearchDTO);
						for (ProspectCallLog prospectCallLog : prospects) {
							if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
									&& !campaignIds.contains(prospectCallLog.getProspectCall().getCampaignId())) {
								campaignIds.add(prospectCallLog.getProspectCall().getCampaignId());
							}
						}
						prospectcallSearchDTO.setCampaignIds(campaignIds);
					}
					Campaign campaign = campaignRepository.findOneById(prospectcallSearchDTO.getCampaignIds().get(0));
					if (campaign != null) {
						List<NewClientMapping> cMListByClientName = newClientMappingRepository
								.findClientMappingByClientName(campaign.getOrganizationId());
						if (cMListByClientName != null && cMListByClientName.size() > 0) {
							// organization level template
							downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
									prospectcallSearchDTO, login, cMListByClientName, "ORG");
							logger.info("downloading organization level template... NewClientMapping : "
									+ cMListByClientName.size());
						} else {
							// default system template
							List<NewClientMapping> cMListBy = newClientMappingRepository
									.findClientMappingByClientName("SYSTEM_DEFAULT");
							downloadClientMappingReportServiceImpl.downloadLeadReportWithNewClientMappings(
									prospectcallSearchDTO, login, cMListBy, "DEFAULT");
							logger.info("downloading default level template...");
						}
					}
				}
			
			}
		} catch (Exception e) {
			logger.error("==========> Error occurred in NewLeadReportThread <==========");
			logger.error("Expection : ", e);
		}
	}
}
