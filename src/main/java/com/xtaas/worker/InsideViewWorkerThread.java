package com.xtaas.worker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import com.xtaas.BeanLocator;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.InsideViewServiceImpl;
import com.xtaas.service.SalutaryServiceImpl;

@ComponentScan("com.xtaas.worker")
public class InsideViewWorkerThread extends DataBuyMultiSourceThread {
	private static final Logger logger = LoggerFactory.getLogger(InsideViewWorkerThread.class);

	@Autowired
	private InsideViewServiceImpl insideViewServiceImpl;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	private Campaign campaign;
	private DataBuyQueue dbq;
	private Long buyRequestCount;

	public InsideViewWorkerThread(CountDownLatch latch, Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
		super("InsideView Service", latch, campaign, dbq, buyRequestCount);
		insideViewServiceImpl = BeanLocator.getBean("insideViewServiceImpl", InsideViewServiceImpl.class);
		dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);
		this.campaign = campaign;
		this.dbq = dbq;
		this.buyRequestCount = buyRequestCount;
	}

	@Override
	public void buyData(Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
		logger.info("Checking " + this.getServiceName());
		try {
			if (buyRequestCount != null && buyRequestCount.intValue() > 0) {
				insideViewServiceImpl.getData(campaign, dbq, buyRequestCount);	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info(this.getServiceName() + " is UP");
	}
	
}
