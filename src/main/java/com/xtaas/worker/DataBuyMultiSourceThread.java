package com.xtaas.worker;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;


import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.domain.entity.Campaign;

@ComponentScan("com.xtaas.worker")
public  class DataBuyMultiSourceThread implements Runnable {

	private CountDownLatch _latch;
	private String _serviceName;
	private ThreadLocal<Boolean> _serviceUp  = new ThreadLocal();
	private Campaign campaign;
	private DataBuyQueue dataBuyQueue;
	private Long buyRequestCount;

	//Get latch object in constructor so that after completing the task, thread can countDown() the latch
	public DataBuyMultiSourceThread(String serviceName, CountDownLatch latch,Campaign campaign,DataBuyQueue dbq, Long buyRequestCount)
	{
		super();
		this.campaign = campaign;
		this.dataBuyQueue = dbq;
		this.buyRequestCount = buyRequestCount;
		this._latch = latch;
		this._serviceName = serviceName;
		this.set_serviceUp( ThreadLocal.withInitial(() -> false));
	}


	@Override
	public void run() {
		try {
			this.set_serviceUp( ThreadLocal.withInitial(() -> true));
			buyData(campaign, dataBuyQueue, buyRequestCount);

		} catch (Throwable t) {
			t.printStackTrace(System.err);
			this.set_serviceUp( ThreadLocal.withInitial(() -> false));
		} finally {
			if(_latch != null) {
				_latch.countDown();
			}
		}
	}

	public ThreadLocal<Boolean> get_serviceUp() {
		return _serviceUp;
	}

	public void set_serviceUp(ThreadLocal<Boolean> _serviceUp) {
		this._serviceUp = _serviceUp;
	}

	public String getServiceName() {
		return _serviceName;
	}

	//This methos needs to be implemented by all specific service checker
	public  void buyData(Campaign campaign,DataBuyQueue dbq, Long buyRequestCount) {

	}
}
