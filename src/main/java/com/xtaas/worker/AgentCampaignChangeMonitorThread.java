/**
 * 
 */
package com.xtaas.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.AgentService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.pusher.Pusher;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.AgentCampaignDTO;

/**
 * This thread monitors campaign to be worked by agents currently online as it
 * is set in their workschedule. If there is any change then notify agents by
 * sending a campaign_change event via Pusher
 * 
 * @author djain
 */
@Component
public class AgentCampaignChangeMonitorThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(AgentCampaignChangeMonitorThread.class);

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private AgentService agentService;

	@Override
	public void run() {
		logger.debug("run() : AgentCampaignChangeMonitorThread STARTED");

		// Set up the GLOBAL security context
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		// SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL);
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken("agent_campaign_change_monitor", "secret", authorities));
		while (!Thread.interrupted()) {
			try {
				// traverse thru agentcall map in agentRegistrationService
				for (AgentCall agentCall : agentRegistrationService.getAgentCallList()) {
					// check if agent heartbeat recd is within 30 secs
					/*
					 * last heartbeat is more than 30*3 (giving 3 chances) secs older then make the
					 * agent OFFLINE
					 */
					if ((System.currentTimeMillis() - agentCall.getAgentHeartbeatTime().getTime()) > (30000 * 3)) {
						agentService.changeStatus(agentCall.getAgentId(), AgentStatus.OFFLINE,
								agentCall.getAgentType().name(), agentCall.getCampaignId());
						continue;
					}

					boolean isCampaignChanged = false;
					// and for each agent find the current campaign
					AgentCampaignDTO currentAgentCampaign = agentService
							.getCurrentAllocatedCampaign(agentCall.getAgentId());
					/*
					 * if there is any change in current campaign and campaign already allocated
					 * then send the campaign_change event
					 */

					// both are NULL, there is no change so move to next
					if (agentCall.getCampaignId() == null
							&& (currentAgentCampaign == null || currentAgentCampaign.getId() == null)) {
						continue;
					}
					// there was no campaign alloted earlier but now there is a campaign alloted
					if (agentCall.getCampaignId() == null
							&& (currentAgentCampaign != null && currentAgentCampaign.getId() != null)) {
						isCampaignChanged = true;
					}
					// there was campaign alloted earlier but now there is no campaign alloted
					if (agentCall.getCampaignId() != null
							&& (currentAgentCampaign == null || currentAgentCampaign.getId() == null)) {
						isCampaignChanged = true;
						currentAgentCampaign = new AgentCampaignDTO();
					}
					// campaign alloted earlier is not same as campaign retrieved now
					if (agentCall.getCampaignId() != null && currentAgentCampaign != null
							&& currentAgentCampaign.getId() != null
							&& !agentCall.getCampaignId().equals(currentAgentCampaign.getId())) {
						isCampaignChanged = true;
					}

					if (isCampaignChanged) {
						/*
						 * START DATE : 09/10/2017 REASON : Setting below 3 parameters null to handle
						 * agent campaign change issue when campaign has large text size. PUSHER has 10
						 * KB size limit that's why reducing campaign text size to pass through PUSHER
						 */
						currentAgentCampaign.setQualificationCriteria(null);
						currentAgentCampaign.setCallGuide(null);
						currentAgentCampaign.setQuestionAttributeMap(null);
						/* END */
						String currentAgentCampaignJson = JSONUtils.toJson(currentAgentCampaign);
						pushMessageToAgent(agentCall.getAgentId(), "campaign_change", currentAgentCampaignJson);
					}
				}
				Thread.sleep(5000);
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
			}
		}
	}

	private void pushMessageToAgent(String agentId, String event, String message) {
		try {
			Pusher.triggerPush(agentId, event, message);
			logger.debug("pushMessageToAgent() : Sent [{}] event to Agent [" + agentId + "]. Message: [{}]", event,
					message);
		} catch (IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing [" + event + "] event to agent ["
					+ agentId + "] Msg: [" + message + "]", e);
		}
	}
}