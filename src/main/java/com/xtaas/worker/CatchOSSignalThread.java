package com.xtaas.worker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import java.lang.reflect.*;

import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

@Component
public class CatchOSSignalThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(CatchOSSignalThread.class);

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private PropertyService propertyService;

	@SuppressWarnings("restriction")
	@Override
	public void run() {
//		logger.info("run() : FailureRecordingsDownloadThread STARTED");
//		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
//		SecurityContextHolder.getContext()
//				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
//		//int retry = 0;
//		while (!Thread.interrupted()) {
//			try {
//				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
//				String instance = propertyService.getApplicationPropertyValue(
//						XtaasConstants.APPLICATION_PROPERTY.ZOOM_TOKEN_THREAD_INSTANCE.name(), "xtaascorp2");
//				
//				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
//					Signal.handle(new Signal("HUP"), signal -> {
//			            //System.out.println(signal.getName() + " (" + signal.getNumber() + ")");
//						logger.info("SIGTERM Signal, ..." + signal.toString());
//			            if (signal.toString().trim().equals("SIGTERM")) {
//			            	logger.info("SIGTERM raised, terminating...");
//			            	try {
//			            		updateDataBuyQueue();
//			            	}catch(Exception e) {
//			            		e.printStackTrace();
//			            	}
//			                System.exit(1);
//			             }
//			        });
//					//updateDataBuyQueue(retry);
//				}
//				Thread.sleep(1000l);
//			} catch (Exception e) {
//				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
//			}
//		}
//	}
//
//	private void updateDataBuyQueue() throws Exception {
//		//String oAuth = "";
//		
//		List<String> dataBuyStatusList = new ArrayList<String>();
//	
//		//dataBuyStatusList.add(DataBuyQueueStatus.QUEUED.toString());
//		dataBuyStatusList.add(DataBuyQueueStatus.INPROCESS.toString());
//		// dataBuyStatusList.add(DataBuyQueueStatus.KILL.toString());
//		Pageable pageable = PageRequest.of(0, 500, Direction.ASC, "jobRequestTime");
//		
//		List<DataBuyQueue> dbqList = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageable);
//		if(dbqList!=null && dbqList.size()>0) {
//			for(DataBuyQueue dbq : dbqList) {
//				dbq.setStatus(DataBuyQueueStatus.PAUSED.toString());
//				dbq.setInterupted(true);
//				dataBuyQueueRepository.save(dbq);
//			}
//		}
//		
//	}

	}
}