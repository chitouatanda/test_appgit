package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.FailedRecordingsToAWS;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.FailedRecordingToAWSRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

@Component
public class FailureRecordingsDownloadThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(FailureRecordingsDownloadThread.class);

	@Autowired
	private FailedRecordingToAWSRepository failedRecordingToAWSRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private PropertyService propertyService;

	@Override
	public void run() {
		logger.debug("run() : FailureRecordingsDownloadThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.FAILURE_RECORDINGS_DOWNLOAD_THREAD_INSTANCE.name(),
						"xtaasupgrade");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
					failureDownload();
				}
				Thread.sleep(600000);
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);

			}
		}
	}

	private void failureDownload() throws Exception {
		List<FailedRecordingsToAWS> fraList = failedRecordingToAWSRepository.findAll();
		if (fraList != null) {
			logger.debug("run() : Found [{}] FailedRecordingsToAWS. ", fraList.size());
			for (FailedRecordingsToAWS fra : fraList) {
				ProspectCallLog prospectCallLog = prospectCallLogRepository
						.findByProspectCallId(fra.getProspectCallId());
				if (prospectCallLog == null) {
					try {
						failedRecordingToAWSRepository.delete(fra);
					} catch (DataIntegrityViolationException e) {
						logger.debug("history already exist");
						e.printStackTrace();
						Thread.sleep(600000);
					}
				} else {
					String awsRecordingUrl = "";
					String recordingUrl = fra.getTwilioRecordingUrl();
					int lastIndex = recordingUrl.lastIndexOf("/");
					String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
					String rfileName = fileName + ".wav";
					awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					downloadRecordingsToAws.downloadRecording(recordingUrl, fra.getProspectCallId(),
							fra.getCampaignId(), true);
					if (awsRecordingUrl != null && !awsRecordingUrl.isEmpty()) {
						try {
							prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
							prospectCallLogRepository.save(prospectCallLog);
							failedRecordingToAWSRepository.delete(fra);
						} catch (DataIntegrityViolationException e) {
							logger.debug("history already exist");
							e.printStackTrace();
							Thread.sleep(600000);
						}
					}
				}
			}
		}
	}

}
