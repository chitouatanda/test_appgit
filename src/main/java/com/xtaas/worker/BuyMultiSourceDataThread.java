package com.xtaas.worker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.service.DataBuyCleanup;
import com.xtaas.service.SalutaryServiceImpl;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.ZoomBuySearchDTO;

@ComponentScan("com.xtaas.worker")
public class BuyMultiSourceDataThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DataBuyThread.class);

	private ZoomInfoContactListProviderNewImpl zoomInfoContactListProviderNewImpl;

	private DataBuyQueueRepository dataBuyQueueRepository;

	private CampaignRepository campaignRepository;

	private OrganizationRepository organizationRepository;

	private CampaignMetaDataRepository campaignMetaDataRepository;

	private LoginRepository loginRepository;
	
	private DataBuyCleanup dataBuyCleanup;

	boolean isServerGettingStarted;

	// List of DataBuyMultiSourceThread checkers
	private static List<DataBuyMultiSourceThread> _services;

	// This latch will be used to wait on
	private static CountDownLatch _latch;

	private DataBuyQueue dataBuyQueue;

	public BuyMultiSourceDataThread(DataBuyQueue dbq) {
		super();
		dataBuyQueue = dbq;
		zoomInfoContactListProviderNewImpl = BeanLocator.getBean("zoomInfoContactListProviderNewImpl",
				ZoomInfoContactListProviderNewImpl.class);
		organizationRepository = BeanLocator.getBean("organizationRepository", OrganizationRepository.class);
		campaignMetaDataRepository = BeanLocator.getBean("campaignMetaDataRepository", CampaignMetaDataRepository.class);
		dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);
		loginRepository = BeanLocator.getBean("loginRepository", LoginRepository.class);
		campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		dataBuyCleanup = BeanLocator.getBean("dataBuyCleanup", DataBuyCleanup.class);
	}

	@Override
	public void run() {
		logger.debug("run() :MultiSource Data Buy STARTED");
//		while (!Thread.interrupted()) {
			try {
				buyFromOtherSources(dataBuyQueue);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
//		}
	}

	private void buyFromOtherSources(DataBuyQueue dbq) {
		// Initialize the latch with number of service checkers
		_latch = new CountDownLatch(4);
		boolean purchaseFlag = true;// this is temporary flag used to stop buying data for a specific source
		// All add checker in lists
		_services = new ArrayList<DataBuyMultiSourceThread>();
		List<String> services = new ArrayList<String>();
		long totalRequestCount = dbq.getRequestCount();
		Campaign campaign = campaignRepository.findOneById(dbq.getCampaignId());
		List<KeyValuePair<String, Integer>> dataSourcePriorityMap = new ArrayList<>();
		Long salutaryBought = 0L;
		Long insideViewBought = 0L;
		Long zoomBought = 0L;
		Long leadIqBought = 0L;

		if (campaign.getDataSourcePriority() != null) {
			dataSourcePriorityMap = campaign.getDataSourcePriority();
		} else {
			Organization org = organizationRepository.findOneById(campaign.getOrganizationId());
			if (org.getCampaignDefaultSetting() != null && org.getCampaignDefaultSetting().getDataSourcePriority() != null) {
				dataSourcePriorityMap = org.getCampaignDefaultSetting().getDataSourcePriority();
			} else {
				CampaignMetaData campaignMetaData = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
				dataSourcePriorityMap = campaignMetaData.getDataSourcePriority();
			}
		}
		if (dataSourcePriorityMap != null && dataSourcePriorityMap.size() > 0) {
			for (KeyValuePair<String, Integer> keyValuePair : dataSourcePriorityMap) {
				if (keyValuePair.getKey().equalsIgnoreCase("Salutary")) {
					if (keyValuePair.getValue() > 0) {
						Long salutaryBuyRequest = Double.valueOf((keyValuePair.getValue() / 100.00) * dbq.getRequestCount())
								.longValue();
						dbq.setSalutaryRequestCount(salutaryBuyRequest);
						dbq = dataBuyQueueRepository.save(dbq);
						_services.add(new SalutaryWorkerThread(_latch, campaign, dbq, salutaryBuyRequest));
						services.add("Salutary");
					}
				} else if (keyValuePair.getKey().equalsIgnoreCase("InsideView") ) {
					//TODO dontPurchaseFlag  is used temporarily to elminate buying for auto mode, as insideview have limited credits we dont 
					//want insideview to be bought for bulk data buy.
					if (keyValuePair.getValue() > 0) {
						Long insideViewBuyRequest = Double.valueOf((keyValuePair.getValue() / 100.00) * dbq.getRequestCount())
								.longValue();
						dbq.setInsideViewRequestCount(insideViewBuyRequest);
						dbq = dataBuyQueueRepository.save(dbq);
						_services.add(new InsideViewWorkerThread(_latch, campaign, dbq, insideViewBuyRequest));
						services.add("InsideView");
						// dbq = buyDataFrominsideView(campaign,dbq,insideViewBuyRequest);
						// if(dbq.getInsideViewPurchasedCount()!=null)
						// insideViewBought = dbq.getInsideViewPurchasedCount();
					}
				} else if (keyValuePair.getKey().equalsIgnoreCase("ZoomInfo")) {
					if (keyValuePair.getValue() > 0) {
						Long zoomCount = Double.valueOf((keyValuePair.getValue() / 100.00) * dbq.getRequestCount()).longValue();
						dbq.setZoomInfoRequestCount(zoomCount);
						dbq = dataBuyQueueRepository.save(dbq);
						_services.add(new ZoomInfoWorkerThread(_latch, campaign, dbq, zoomCount));
						services.add("ZoomInfo");
					}
				}
				else if (keyValuePair.getKey().equalsIgnoreCase("LeadIQ")) {
					if (keyValuePair.getValue() > 0) {
						Long leadIqCount = Double.valueOf((keyValuePair.getValue() / 100.00) * dbq.getRequestCount()).longValue();
						dbq.setLeadIqRequestCount(leadIqCount);
						dbq = dataBuyQueueRepository.save(dbq);
						_services.add(new LeadIQWorkerThread(_latch, campaign, dbq, leadIqCount));
						services.add("LeadIQ");
					}
				}

			}

			try {
				if (!services.contains("Salutary")) {
					_services.add(new SalutaryWorkerThread(_latch, campaign, dbq, 0L));
				}
				if (!services.contains("InsideView")) {
					_services.add(new InsideViewWorkerThread(_latch, campaign, dbq, 0L));
				}
				if (!services.contains("ZoomInfo")) {
					_services.add(new ZoomInfoWorkerThread(_latch, campaign, dbq, 0L));
				}
				if (!services.contains("LeadIQ")) {
					_services.add(new LeadIQWorkerThread(_latch, campaign, dbq, 0L));
				}

				// Start service checkers using executor framework
				Executor executor = Executors.newFixedThreadPool(_services.size());

				for (final DataBuyMultiSourceThread v : _services) {
					executor.execute(v);
				}

				// Now wait till all services are checked
				_latch.await();

				boolean allBuyCompleted = true;
				// Services are file and now proceed startup
				for (final DataBuyMultiSourceThread v : _services) {
					if (!v.get_serviceUp().get()) {
						allBuyCompleted = false;
					}
				}

				if (allBuyCompleted) {
					dbq = dataBuyQueueRepository.findByCampaignIdAndStatus(campaign.getId(), "INPROCESS");

					if (dbq.getSalutaryPurchasedCount() != null) {
						salutaryBought = dbq.getSalutaryPurchasedCount();
					}
					if (dbq.getZoomInfoPurchasedCount() != null) {
						zoomBought = dbq.getZoomInfoPurchasedCount();
					}
					if (dbq.getInsideViewPurchasedCount() != null) {
						insideViewBought = dbq.getInsideViewPurchasedCount();
					}
					if (dbq.getLeadIqPurchasedCount() != null) {
						leadIqBought = dbq.getLeadIqPurchasedCount();
					}
					Long totalbought = zoomBought + salutaryBought + insideViewBought + leadIqBought;

					//dataBuyCleanup.dataCleanupAfterBuy(dbq, campaign, totalbought);
					Long zoomBuyRequest = totalRequestCount - (totalbought);
					// commented to test salutary databuy
					if (zoomBuyRequest != null && zoomBuyRequest > 0 && !campaign.isABM() && isZoomDefaultBuyRequired(dbq)) {
						logger.info("Requested data not buy for campaign [{}], Starting default ZoomInfo Buy. Total request count [{}] and total data bought [{}] ",
											dbq.getCampaignId(), totalRequestCount, totalbought);
						if(dbq.getZoomInfoRequestCount() != null){
							dbq.setZoomInfoRequestCount(dbq.getZoomInfoRequestCount() + zoomBuyRequest);
						}else{
							dbq.setZoomInfoRequestCount(zoomBuyRequest);
						}
						dbq.setTotalBought(totalbought);
						dbq = dataBuyQueueRepository.save(dbq);
						ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
						zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
						zoomBuySearchDTO.setTotalCount(zoomBuyRequest);
						zoomBuySearchDTO.setDataBuyQueue(dbq);
						ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(zoomBuySearchDTO);
						Thread t1 = new Thread(zilp, "t" + Math.random());
						t1.start();
					} else {
						dbq.setPurchasedCount(zoomBought + salutaryBought + insideViewBought + leadIqBought);
						dbq.setStatus(DataBuyQueueStatus.READY_FOR_CLEANUP.toString());
						dbq.setJobEndTime(new Date());
						dbq.setTotalBought(totalbought);
						dataBuyQueueRepository.save(dbq);
						List<String> toemails = new ArrayList<String>();
						toemails.add("data@xtaascorp.com");
						if (dbq.getRequestorId() != null && !dbq.getRequestorId().isEmpty()) {
							Login l = loginRepository.findOneById(dbq.getRequestorId());
							if (l.getEmail() != null && !l.getEmail().isEmpty())
								toemails.add(l.getEmail());
						}
						XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com",
								campaign.getName() + " Data purchase job completed",
								dbq.getTotalBought() + " records purchased for this job");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * This method will check, can we go for default data buy for zoom or not
	 * if in data source priority , zoom purchase request is 100% then , will not go default zoom data buy
	 * if zoom bought data lesser than zoom request count irrespective of data source priority, will not go default zoom data buy
	 * @param dbq
	 * @return
	 */
	public boolean isZoomDefaultBuyRequired(DataBuyQueue dbq){
		boolean val = true;
		try{
			if(dbq !=null && dbq.getRequestCount() == dbq.getZoomInfoRequestCount()){
				return false;
			}
			if(dbq !=null && dbq.getZoomInfoPurchasedCount() != null && dbq.getZoomInfoPurchasedCount() < dbq.getZoomInfoRequestCount()){
				return false;
			}
		}catch (Exception e){
			logger.error("Error while checking default zoom buy required or not for campaign [{}] . Exception [{}] ",dbq.getCampaignId(), e);
			e.printStackTrace();
		}

		return val;
	}


}
