package com.xtaas.worker;

import java.util.*;
import java.util.concurrent.TimeUnit;

import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.ZoomTokenDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ZoomTokenRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.infra.zoominfo.service.ZoomOAuth;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;

@Component
public class ZoomOAuthTokenThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(ZoomOAuthTokenThread.class);

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;

	@Autowired
	private ZoomOAuth zoomOAuth;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private RestClientService restClientService;
	

	@Override
	public void run() {
		logger.debug("run() : FailureRecordingsDownloadThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		////////////////////
		//insertPCIAnalysis();//TODO remove or commentthis once done
		////////////////////
		
		int retry = 0;
		while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.ZOOM_TOKEN_THREAD_INSTANCE.name(), "xtaasupgrade");
				String dbInstance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DB_INSTANCE.name(), "XTAAS");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())
						&& dbInstance.toLowerCase().equalsIgnoreCase("XTAAS")) {
					generateZoomToken(retry);
				//}else if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())){
					//TODO while pusing  to demandshore instance  uncomment  the above line and delete the below line
					//TODO and  also  point  ZOOM_TOKEN_THREAD_INSTANCE to demandshoreqa  instance in applicationproperty collection
					// TODO in demandshore db.
				} /*else if (serverUrl != null && instance != null &&
						((serverUrl.toLowerCase().contains("demandshoreqa") && dbInstance.toLowerCase().equalsIgnoreCase("DEMANDSHORE")))
						|| 	(serverUrl.toLowerCase().contains("corplive") && dbInstance.toLowerCase().equalsIgnoreCase("CORPLIVE"))){
					downloadRecordingsToAws.getZoomTokenFromS3(retry);
				}*/
				Thread.sleep(60000);
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
			}
		}
	}

	private void generateZoomToken(int retry) throws Exception {
		String oAuth = "";
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		Date currDate = new Date();
		List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
		ZoomToken ezt = zoomTokenList.get(0);
		long duration = currDate.getTime() - ezt.getCreatedDate().getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			oAuth = zoomOAuth.createJWT();
			if (oAuth != null && !oAuth.isEmpty()) {
				// tokenKey =oAuth;
				ezt.setStatus("INACTIVE");
				zoomTokenRepository.save(ezt);
				ZoomToken newZoomToken = new ZoomToken();
				newZoomToken.setOauthToken(oAuth);
				newZoomToken.setStatus("ACTIVE");
				newZoomToken = zoomTokenRepository.save(newZoomToken);

				//downloadRecordingsToAws.updateZoomTokenToS3(newZoomToken.getOauthToken());
				updateTokenInOtherEnv(newZoomToken);


			} else {
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
				retry = 1;

				//System.out.println("Retrying");
				Thread.sleep(60000);
			}

		} /*
			 * else{ // oAuth = zt.getOauthToken(); // tokenKey = oAuth; }
			 */
		// return oAuth;

	}


	public void updateTokenInOtherEnv(ZoomToken newZoomToken){
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.UPDATE_ZOOM_TOKEN.name(), "xtaasupgrade");
		if(StringUtils.isNotBlank(instance)){
			ZoomTokenDTO ztoken = new ZoomTokenDTO();
			ztoken.setOauthToken(newZoomToken.getOauthToken());
			ztoken.setStatus("ACTIVE");
			List<String> instanceList = Arrays.asList(instance.split(","));
			for(String url : instanceList){
				String host = null;
				try {
					if(url.contains("localhost")){
						host = "http://localhost:8080/spr/campaign/update/zoom/token";
					}else{
						host = "https://"+url+".herokuapp.com/spr/campaign/update/zoom/token";
					}
					String response = restClientService.makePostRequest(host, new HashMap<>(), JSONUtils.toJson(ztoken));
					logger.info("Zoom token updated successfully for environment : [{}] and response : [{}] ", url, response);

				} catch (Exception e) {
					logger.error("Error occured while update zoom token in other evvironment [{}] . Exception [{}] ", host, e);
				}
			}
		}
	}
	
	
}
