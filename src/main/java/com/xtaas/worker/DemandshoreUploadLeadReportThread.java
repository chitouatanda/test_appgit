package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.BeanLocator;
import com.xtaas.service.DemandshoreUploadReportServiceImpl;

@Component
public class DemandshoreUploadLeadReportThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DemandshoreUploadLeadReportThread.class);

	@Autowired
	private DemandshoreUploadReportServiceImpl demandshoreUploadReportServiceImpl;

	private MultipartFile multipartFile;

	public DemandshoreUploadLeadReportThread(MultipartFile file) {
		this.multipartFile = file;
		demandshoreUploadReportServiceImpl = BeanLocator.getBean("demandshoreUploadReportServiceImpl",
				DemandshoreUploadReportServiceImpl.class);
	}
	
	@SuppressWarnings("unused")
	private DemandshoreUploadLeadReportThread() {

	}

	@Override
	public void run() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		try {
			logger.debug("run() : Upload Demandshore LeadReportThread STARTED...");
			demandshoreUploadReportServiceImpl.updateLeadDetails(multipartFile);
			logger.debug("run() : Upload Demandshore LeadReportThread COMPLETED...");
		} catch (Exception e) {
			logger.error("run() : Upload Demandshore LeadReportThread failed with error: {}", e.getMessage());
			e.printStackTrace();
		}

	}
}
