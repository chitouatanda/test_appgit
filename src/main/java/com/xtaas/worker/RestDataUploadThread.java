package com.xtaas.worker;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.RestCampaignContactServiceImpl;
import com.xtaas.web.dto.RestCampaignContactDTO;

public class RestDataUploadThread implements Runnable{

	private static final Logger logger = LoggerFactory.getLogger(RestDataUploadThread.class);
	
	@Autowired 
	private RestCampaignContactServiceImpl restCampaignContactServiceImpl;
	
	private List<RestCampaignContactDTO>  campaignContacts;
	
	public RestDataUploadThread(List<RestCampaignContactDTO>  campaignContacts) {
		this.campaignContacts = campaignContacts;
		restCampaignContactServiceImpl = BeanLocator.getBean("restCampaignContactServiceImpl", RestCampaignContactServiceImpl.class);
	}
	
	@Override
	public void run() {
//		final int maxThreads = 4;
//		
//		ExecutorService executorService = new ThreadPoolExecutor(
//				maxThreads, // core thread pool size
//				maxThreads, // maximum thread pool size
//				1, // time to wait before resizing pool
//				TimeUnit.MINUTES,
//				new ArrayBlockingQueue<Runnable>(maxThreads, true),
//				new ThreadPoolExecutor.CallerRunsPolicy());
//		
//		try {
//			logger.info("run() : RestDataUploadThread STARTED...");
//			restCampaignContactServiceImpl.uploadRestData(campaignContacts);
//			logger.info("run() : RestDataUploadThread COMPLETED...");
//		} catch (Exception e) {
//			logger.error("run() : RestDataUploadThread failed with error: {}", e.getMessage());
//			e.printStackTrace();
//		}
//		
	}

}
