package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.BeanLocator;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;

@Component
public class ColdEmailByCampaignThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ColdEmailByCampaignThread.class);
	
	private Campaign campaign;
//	private List<String> statuses;
//	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

//	@Autowired
	//private PropertyService propertyService;
	
	public ColdEmailByCampaignThread() {
		
	}
	
	public ColdEmailByCampaignThread(Campaign campaign) {
		this.campaign = campaign;
   	 prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);

	//	this.statuses = statuses;
	}




	@Override
	public void run() {
		logger.debug("run() : ColdEmailByCampaignThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		//while (!Thread.interrupted()) {
			try {
				SendColdEmails();
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);

			}
		//}
	}

	private void SendColdEmails() throws Exception {
		List<ProspectCallLog> pciList = new ArrayList<ProspectCallLog>();
		if(campaign.isSendMailToAllCallables())
			pciList = prospectCallLogRepository.findProspectsForColdEmail(campaign.getId());
		else
			pciList = prospectCallLogRepository.findQueuedProspectsForColdEmail(campaign.getId());
		if (pciList != null && pciList.size()>0) {
			for(ProspectCallLog pcLog : pciList) {
				if(!pcLog.getProspectCall().getProspect().isEmailSent() && 
						pcLog.getProspectCall().getProspect().getEmail()!=null && 
						!pcLog.getProspectCall().getProspect().getEmail().isEmpty()) {
					 try {
						 	String email = pcLog.getProspectCall().getProspect().getEmail();
						 	String subject = campaign.getColdEmailSubject();
						 	String message = campaign.getColdEmailMessage();
						 	if(subject!=null && !subject.isEmpty()  && message!=null && !message.isEmpty()) {
						 		message = message.replace("[PROSPECT]", pcLog.getProspectCall().getProspect().getFirstName());
						 		XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",  subject,  message);
						 		pcLog.getProspectCall().getProspect().setEmailSent(true);
						 		prospectCallLogRepository.save(pcLog);
						 	}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
			}
			
		}
	}
	
	

}
