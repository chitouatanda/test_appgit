package com.xtaas.worker;

import com.xtaas.BeanLocator;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.InsideViewServiceImpl;
import com.xtaas.service.LeadIQSearchServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import java.util.concurrent.CountDownLatch;

@ComponentScan("com.xtaas.worker")
public class LeadIQWorkerThread extends DataBuyMultiSourceThread {
    private static final Logger logger = LoggerFactory.getLogger(LeadIQWorkerThread.class);

    @Autowired
    private LeadIQSearchServiceImpl leadIQSearchServiceImpl;

    @Autowired
    private DataBuyQueueRepository dataBuyQueueRepository;

    private Campaign campaign;
    private DataBuyQueue dbq;
    private Long buyRequestCount;

    public LeadIQWorkerThread(CountDownLatch latch, Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
        super("LeadIQ Service", latch, campaign, dbq, buyRequestCount);
        leadIQSearchServiceImpl = BeanLocator.getBean("leadIQSearchServiceImpl", LeadIQSearchServiceImpl.class);
        dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);
        this.campaign = campaign;
        this.dbq = dbq;
        this.buyRequestCount = buyRequestCount;
    }

    @Override
    public void buyData(Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
        logger.info("Checking " + this.getServiceName());
        try {
            if (buyRequestCount != null && buyRequestCount.intValue() > 0) {
                leadIQSearchServiceImpl.getData(campaign, dbq, buyRequestCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info(this.getServiceName() + " is UP");
    }

}
