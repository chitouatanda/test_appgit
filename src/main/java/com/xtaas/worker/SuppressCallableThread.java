package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xtaas.BeanLocator;
//import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.service.SuppressCallableServiceImpl;

/**
 * DATE :- 08/01/2020 Below thread removes the prospects which are in
 * suppression list from calling queue. updates those prospects with
 * callRetryCount 144.
 * 
 * @author PRATAP YEMUL
 */
@Component
public class SuppressCallableThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(SuppressCallableThread.class);

	private String campaignId;

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	SuppressCallableServiceImpl suppressCallableServiceImpl;

	protected SuppressCallableThread() {

	}

	public SuppressCallableThread(String campaignId) {
		this.campaignId = campaignId;
		suppressCallableServiceImpl = BeanLocator.getBean("suppressCallableServiceImpl",
				SuppressCallableServiceImpl.class);
		suppressionListNewRepository = BeanLocator.getBean("suppressionListNewRepository", SuppressionListNewRepository.class);
	}

	@Override
	public void run() {
		try {
			List<String> cids  =  new ArrayList<String>();
			cids.add(this.campaignId);
			int supCount = suppressionListNewRepository
					.countSupressionListByCampaignIds(cids);
			if (supCount >0) {
				suppressCallableServiceImpl.suppressCallable(this.campaignId);
			}
		} catch (Exception e) {
			logger.error("==========> Error occurred in SuppressCallableThread <==========");
			logger.error("Expection : ", e);
		}
	}
}
