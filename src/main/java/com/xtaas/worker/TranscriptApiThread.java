package com.xtaas.worker;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.BeanLocator;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.ml.transcribe.service.TranscriberFactory;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.AdhocTranscribeJobIO;

@Component
public class TranscriptApiThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(TranscriptApiThread.class);

	@Autowired
	private CallClassificationJobQueueRepository callClassificationJobQueueRepository;

	private MultipartFile file = null;

	private HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	private String xlsFlieName;

	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;

	private List<String> errorList = new ArrayList<String>();

	private Workbook book;

	private List<String> errorsInFile;

	private HashMap<Integer, List<String>> errorMap = new HashMap<>();

	private HashMap<String, List<String>> jobUpdateMap = new HashMap<>();

	private List<String> emails;

	public TranscriptApiThread() {

	}

	public TranscriptApiThread(MultipartFile file, List<String> emails) {
		this.file = file;
		this.emails = emails;
		callClassificationJobQueueRepository = BeanLocator.getBean("callClassificationJobQueueRepository",
				CallClassificationJobQueueRepository.class);
	}

	@Override
	public void run() {
		try {
			readTranscriptFile(file);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String readTranscriptFile(MultipartFile file) throws IOException {
		xlsFlieName = file.getOriginalFilename();
		if (!file.isEmpty()) {
			errorList = new ArrayList<String>();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					System.out.println(sheet.getLastRowNum());
					if (totalRowsCount < 1) {
						book.close();
						logger.error(xlsFlieName + " file is empty");
						throw new IllegalArgumentException("File Can not be Empty.");
					} else if (totalRowsCount <= 1) {
						book.close();
						logger.error("No records found in file " + xlsFlieName);
						throw new IllegalArgumentException("No Records Found in File.");
					}
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords();
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + xlsFlieName);
					return "Problem in reading the file " + xlsFlieName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);
		int colCounts = row.getPhysicalNumberOfCells();
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell == null || cell.getCellType() == CellType.BLANK) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
				return false;
			}
			if (cell.getCellType() != CellType.STRING) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
				return false;
			} else {
				headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
				fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
			}
		}
		return true;
	}

	private void validateExcelFileRecords() {
		int rowsCount = ErrorHandlindutils.getNonBlankRowCount(sheet);
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processRecords(0, rowsCount, colCounts);
	}

	private void processRecords(int startPos, int EndPos, int colCounts) {
		List<AdhocTranscribeJobIO> adhocTranscribeList = new ArrayList<AdhocTranscribeJobIO>();
		boolean invalidFile = false;
		for (int i = startPos; i < EndPos; i++) {
			AdhocTranscribeJobIO adhocTranscribeJobIO = new AdhocTranscribeJobIO();
			errorsInFile = new ArrayList<>();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);

			if (i == 0) {
				continue;
			}
			if (row == null) {
				errorsInFile.add(" record is empty ");
				errorMap.put(i, errorsInFile);
				EndPos++;
				continue;
			}
			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorsInFile.add(" Record is missing ");
				errorMap.put(i, errorsInFile);
				EndPos++;
				continue;
			}

			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("_id") || columnName.equalsIgnoreCase("id*")) {
					if (cell != null) {
						String id = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (!id.equalsIgnoreCase("") && id != null && !id.isEmpty()) {
							adhocTranscribeJobIO.set_id(id);
						}
					}
				} else if (columnName.equalsIgnoreCase("fromNumber") || columnName.equalsIgnoreCase("fromNumber*")
						|| columnName.equalsIgnoreCase("from Number")) {
					if (cell != null) {
						DataFormatter formatter = new DataFormatter();
						String fromNumber = formatter.formatCellValue(cell);
						String from = XtaasUtils.removeNonAsciiChars(fromNumber).trim();
						adhocTranscribeJobIO.setFromNumber(from);
					}
				} else if (columnName.equalsIgnoreCase("toNumber") || columnName.equalsIgnoreCase("toNumber*")
						|| columnName.equalsIgnoreCase("to Number")) {
					if (cell != null) {
						DataFormatter formatter = new DataFormatter();
						String toNumber = formatter.formatCellValue(cell);
						String to = XtaasUtils.removeNonAsciiChars(toNumber).trim();
						adhocTranscribeJobIO.setToNumber(to);
					}
				} else if (columnName.equalsIgnoreCase("messageAudioUrl")) {
					if (cell != null) {
						String msgAudioUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (!msgAudioUrl.equalsIgnoreCase("") && msgAudioUrl != null && !msgAudioUrl.isEmpty()) {
							adhocTranscribeJobIO.setMessageAudioUrl(msgAudioUrl);
						} else {
							errorsInFile.add(columnName + " is Empty ");
						}
					} else {
						errorsInFile.add(columnName + " is Empty ");
					}
				} else if (columnName.equalsIgnoreCase("prospectCallId")) {
					if (cell != null) {
						String prospectCallId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (!prospectCallId.equalsIgnoreCase("") && prospectCallId != null
								&& !prospectCallId.isEmpty()) {
							adhocTranscribeJobIO.setProspectCallId(prospectCallId);
						} else {
							errorsInFile.add(columnName + " is Empty ");
						}
					} else {
						errorsInFile.add(columnName + " is Empty ");
					}
				} else if (columnName.equalsIgnoreCase("awsAudioUrl")) {
					if (cell != null) {
						String awsAudioUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (!awsAudioUrl.equalsIgnoreCase("") && awsAudioUrl != null && !awsAudioUrl.isEmpty()) {
							adhocTranscribeJobIO.setAwsAudioUrl(awsAudioUrl);
						} else {
							errorsInFile.add(columnName + " is Empty ");
						}
					} else {
						errorsInFile.add(columnName + " is Empty ");
					}
				} else if (columnName.equalsIgnoreCase("callParams__RecordingDuration")) {
					if (cell != null) {
						DataFormatter formatter = new DataFormatter();
						String duration = formatter.formatCellValue(cell);
						String callDuration = XtaasUtils.removeNonAsciiChars(duration).trim();
						adhocTranscribeJobIO.setCallParams__RecordingDuration(callDuration);
					}
				}
			}

			if (errorsInFile != null && errorsInFile.size() > 0) {
				invalidFile = true;
			}

			if (!adhocTranscribeList.contains(adhocTranscribeJobIO)) {
				adhocTranscribeList.add(adhocTranscribeJobIO);
			}
			errorMap.put(i, errorsInFile);
			jobUpdateMap.put(adhocTranscribeJobIO.getProspectCallId(), errorsInFile);
		}

		String msgLifeCycleId = UUID.randomUUID().toString();
		if (adhocTranscribeList != null && adhocTranscribeList.size() > 0) {
			for (AdhocTranscribeJobIO adhocTranscribeJob : adhocTranscribeList) {
				try {
					callClassificationJobQueueRepository.save(new CallClassificationJobQueue()
							.withStatus(CallClassificationJobQueue.Status.TRANSCRIBE_QUEUED)
							.withMessageLifecycleId(msgLifeCycleId).withProducer("TRANSCRIPTAPI")
							.withProducerServerId(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
							.withNamespace(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
							.withJobCreationTime(new Date()).withFromNumber(adhocTranscribeJob.getFromNumber())
							.withToNumber(adhocTranscribeJob.getToNumber())
							.withProspectCallId(adhocTranscribeJob.getProspectCallId())
							.withRecordingUrl(adhocTranscribeJob.getAwsAudioUrl())
							.withCallDuration(Integer.parseInt(adhocTranscribeJob.getCallParams__RecordingDuration()))
							.withFileName(xlsFlieName));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		if (errorMap != null && invalidFile) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			updateErrorRecords();
			try {
				ErrorHandlindutils.sendExcelwithMsg(null, book, xlsFlieName, "Errors in transcript file", "", emails);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.error("Please Check Your Email There is Some Error in " + xlsFlieName + " Transcript File");
		}
	}

	private void updateErrorRecords() {
		if (jobUpdateMap != null && jobUpdateMap.size() > 0) {
			for (Entry<String, List<String>> entry : jobUpdateMap.entrySet()) {
				if (entry.getValue() != null && !entry.getValue().isEmpty()) {
					List<CallClassificationJobQueue> jobQueue = callClassificationJobQueueRepository
							.findByProspectCallId(entry.getKey());
					if (jobQueue != null && jobQueue.size() > 0) {
						jobQueue.get(0).setErrorMsg(StringUtils.join(entry.getValue(), " |"));
						jobQueue.get(0).setStatus(CallClassificationJobQueue.Status.ERROR);
						callClassificationJobQueueRepository.save(jobQueue.get(0));
					}
				}
			}
			jobUpdateMap = new HashMap<>();
		}
	}

}
