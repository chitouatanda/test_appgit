package com.xtaas.worker;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import com.xtaas.BeanLocator;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.service.SalutaryServiceImpl;

@ComponentScan("com.xtaas.worker")
public class SalutaryWorkerThread extends DataBuyMultiSourceThread {
	private static final Logger logger = LoggerFactory.getLogger(SalutaryWorkerThread.class);
	
	
	private SalutaryServiceImpl salutaryServiceImpl;
	
	private DataBuyQueueRepository dataBuyQueueRepository;


	
	
	public SalutaryWorkerThread(CountDownLatch latch,Campaign campaign, DataBuyQueue dbq, Long buyRequestCount) {
		super("Salutary Service", latch,campaign,dbq,buyRequestCount);
		salutaryServiceImpl = BeanLocator.getBean("salutaryServiceImpl", SalutaryServiceImpl.class);
		dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);

		
	}
	
	 @Override
	 public void buyData(Campaign campaign,DataBuyQueue dbq, Long salutaryBuyRequest) 
	 {
		 logger.info("Checking " + this.getServiceName());
	        try
	        {
	        	if(salutaryBuyRequest!=null && salutaryBuyRequest>0) {
	        		
		        	Long salutaryPurchaseCount = salutaryServiceImpl.databuySaltary(dbq.getCampaignCriteriaDTO(), salutaryBuyRequest, dbq.getCampaignId(),true);
		    		/*if(salutaryBuyRequest>salutaryPurchaseCount) {
			        	 salutaryPurchaseCount = salutaryServiceImpl.databuySaltary(dbq.getCampaignCriteriaDTO(), salutaryBuyRequest-salutaryPurchaseCount, dbq.getCampaignId(),false);
		    		}*/
		        	logger.info("Salutary databuy purchase count : " + salutaryPurchaseCount);
					dbq = dataBuyQueueRepository.findByCampaignIdAndStatus(campaign.getId(), "INPROCESS");
					if(dbq != null){
						if(salutaryBuyRequest!=null && salutaryBuyRequest >  0 && salutaryPurchaseCount!=null && salutaryPurchaseCount >  0) {
							dbq.setSalutaryPurchasedCount(salutaryPurchaseCount);
							if(dbq.getPurchasedCount()!=null) {
								dbq.setPurchasedCount(dbq.getPurchasedCount()-salutaryPurchaseCount);
							}else {
								dbq.setPurchasedCount(salutaryPurchaseCount);
							}
							dbq = dataBuyQueueRepository.save(dbq);
						}else {
							salutaryPurchaseCount = 0L;
							dbq.setSalutaryPurchasedCount(salutaryPurchaseCount);
							if(dbq.getPurchasedCount()!=null) {
								dbq.setPurchasedCount(dbq.getPurchasedCount()-salutaryPurchaseCount);
							}else {
								dbq.setPurchasedCount(salutaryPurchaseCount);
							}
							dbq = dataBuyQueueRepository.save(dbq);
						}
					}

	        	}
	        } 
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	        logger.info(this.getServiceName() + " is UP");
	    }
}
