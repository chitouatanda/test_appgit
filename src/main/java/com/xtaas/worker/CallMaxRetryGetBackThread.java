package com.xtaas.worker;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.BeanLocator;
import com.xtaas.service.ProspectCallLogServiceImpl;


@Component
public class CallMaxRetryGetBackThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(CallMaxRetryGetBackThread.class);

	private String campaignId;
	private int dbCallRetries;
	private int newCallRetries;
	
	@Autowired
	private ProspectCallLogServiceImpl prospectCallLogServiceImpl;
	
	public CallMaxRetryGetBackThread() {
		
	}

	public CallMaxRetryGetBackThread(String campaignId, int dbCallRetries, int newCallRetries) {
		this.campaignId = campaignId;
		this.dbCallRetries = dbCallRetries;
		this.newCallRetries = newCallRetries;
		prospectCallLogServiceImpl = BeanLocator.getBean("prospectCallLogServiceImpl", ProspectCallLogServiceImpl.class);
	}

	@Override
	public void run() {
		logger.debug("run() : CallMaxRetryGetBackThread STARTED");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		try {
			prospectCallLogServiceImpl.getBackProspectsFromMaxRetries(campaignId,dbCallRetries, newCallRetries);
		} catch (Exception e) {
			logger.error("==========> Error occurred in CallMaxRetryGetBackThread <==========");
			logger.error("Expection : ", e);
		}
	}

	/*
	 * private void downloadReport(){ try{
	 * leadReportService.downloadClientReportByFilter(prospectCallSearchDTO);
	 * }catch(Exception e){ e.printStackTrace(); } }
	 */
}
