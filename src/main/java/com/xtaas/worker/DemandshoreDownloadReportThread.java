package com.xtaas.worker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Login;
import com.xtaas.service.DemandshoreDownloadReportServiceImpl;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Component
public class DemandshoreDownloadReportThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DemandshoreDownloadReportThread.class);

	private ProspectCallSearchDTO prospectCallSearchDTO;
	
	private Login login;

	@Autowired
	private DemandshoreDownloadReportServiceImpl demandshoreDownloadReportServiceImpl;

	public DemandshoreDownloadReportThread(ProspectCallSearchDTO prospectCallSearchDTO, Login login) {
		this.prospectCallSearchDTO = prospectCallSearchDTO;
		this.login = login;
		demandshoreDownloadReportServiceImpl = BeanLocator.getBean("demandshoreDownloadReportServiceImpl",
				DemandshoreDownloadReportServiceImpl.class);
	}
	
	@SuppressWarnings("unused")
	private DemandshoreDownloadReportThread() {
		
	}

	@Override
	public void run() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("SYSTEM", "secret", authorities));
		try {
			logger.debug("run() : Download Demandshore LeadReportThread STARTED...");
			demandshoreDownloadReportServiceImpl.downloadDemandshoreLeadReport(prospectCallSearchDTO,login);
			logger.debug("run() : Download Demandshore LeadReportThread COMPLETED...");
		} catch (ParseException e) {
			logger.error("run() : Download Demandshore LeadReportThread failed with error: {}", e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("run() : Download Demandshore LeadReportThread failed with error: {}", e.getMessage());
			e.printStackTrace();
		}

	}
}
