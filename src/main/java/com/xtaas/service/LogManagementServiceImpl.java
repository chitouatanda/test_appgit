package com.xtaas.service;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.xtaas.logging.SplunkLoggingUtils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

@Service
public class LogManagementServiceImpl implements LogManagementService {

	private static final Logger log = LoggerFactory.getLogger(LogManagementServiceImpl.class);

	@Override
	public String changeLoglevel(String classpath, String level) {
		if (StringUtils.isEmpty(classpath) || StringUtils.isEmpty(level)) {
			return "classpath and level should not be empty";
		}
		ch.qos.logback.classic.Logger logger = getLogger(classpath);
		String msg = setLogLevel(logger, level.toLowerCase());

		log.info(new SplunkLoggingUtils("changeLoglevel", UUID.randomUUID().toString()).eventDescription(msg)
				.processName("changeLoglevel").methodName("changeLoglevel").build());

		return msg;
	}

	private ch.qos.logback.classic.Logger getLogger(String classpath) {
		ch.qos.logback.classic.Logger logger = null;
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		try {
			Class<?> className = Class.forName(classpath);
			logger = loggerContext.getLogger(className);
		} catch (ClassNotFoundException ex) {
			log.error("Class [{}] not found exception: ", classpath, ex);
			logger = loggerContext.getLogger(classpath);
		}
		return logger;
	}

	private String setLogLevel(ch.qos.logback.classic.Logger logger, String level) {
		switch (level) {
		case "off":
			logger.setLevel(Level.OFF);
			break;
		case "error":
			logger.setLevel(Level.ERROR);
			break;
		case "warn":
			logger.setLevel(Level.WARN);
			break;
		case "info":
			logger.setLevel(Level.INFO);
			break;
		case "debug":
			logger.setLevel(Level.DEBUG);
			break;
		case "trace":
			logger.setLevel(Level.TRACE);
			break;
		case "all":
			logger.setLevel(Level.ALL);
			break;
		default:
			return String.format("invalid log level [%s]", level);
		}
		return String.format("log level changed for [%s] to [%s]", logger.getName(), level);
	}

}
