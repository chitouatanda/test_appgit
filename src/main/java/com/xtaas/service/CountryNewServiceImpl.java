package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.CountryNew;
import com.xtaas.db.repository.CountryNewRepository;

@Service
public class CountryNewServiceImpl implements CountryNewService {

	private static final Logger logger = LoggerFactory.getLogger(CountryNewServiceImpl.class);

	@Autowired
	private CountryNewRepository countryNewRepository;

	@Override
	public List<String> getStatesOfCountry(String countryName) {
		List<String> stateNames = new ArrayList<String>();
		List<CountryNew> countriesFromDB = countryNewRepository.findAllByCountry(countryName);
		if (countriesFromDB != null && countriesFromDB.size() > 0) {
			stateNames = countriesFromDB.stream().map(country -> country.getStateName()).filter(Objects::nonNull).collect(Collectors.toList());
		}
		return stateNames;
	}
}
