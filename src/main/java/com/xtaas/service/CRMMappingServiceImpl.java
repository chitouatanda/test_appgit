package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CRMMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.repository.CRMMappingRepository;
import com.xtaas.web.dto.CRMAttributeDTO;

@Service
public class CRMMappingServiceImpl implements CRMMappingService {
	@Autowired
	private CRMMappingRepository crmMappingRepository;

	/**
	 * This method does hierarchical merging of CRM and Organization specific
	 * attributes. CRM attributes being parent is overridden by Organization (child)
	 * attributes
	 */
	@Override
	public CRMMapping getCRMMapping(Organization organization) {
		String crmName = organization.getCrm().getName().toUpperCase();
		return mergeOrgToCrmMappings(crmMappingRepository.findOneById(crmName),
				crmMappingRepository.findOneById(organization.getId()));
		// return new CRMMapping();
	}

	@Override
	public CRMMapping getSystemMapping() {
		return crmMappingRepository.findOneById("SYSTEM");
	}

	@Override
	public CRMMapping findByOrgId(String organizationId) {
		return crmMappingRepository.findOneById(organizationId);
	}

	@Override
	public HashMap<String, CRMAttribute> getCRMAttributeMap(String organizationId) {
		CRMMapping systemMapping = getSystemMapping();
		HashMap<String, CRMAttribute> prospectAttributeMap;

		CRMMapping orgMapping = crmMappingRepository.findOneById(organizationId);
		// build org's crmmapping based on inheritance
		CRMMapping inheritedCRMMapping = inheritCRMMapping(orgMapping);

		// Merge CRM attribute with System level attribute mapping. This copies Prospect
		// bean path to CRM level attribute mapping
		CRMMapping mergedCRMMapping = mergeCRMWithSystemMapping(systemMapping, inheritedCRMMapping);
		prospectAttributeMap = new HashMap<String, CRMAttribute>(mergedCRMMapping.getAttributeMapping().size());
		for (CRMAttribute mergedCRMAttribute : mergedCRMMapping.getAttributeMapping()) {
			prospectAttributeMap.put(mergedCRMAttribute.getCrmName(), mergedCRMAttribute); // map of attribute name with
																							// attribute object
		}
		for (int i = 0; i < orgMapping.getAttributeMapping().size(); i++) {
			String systemName = orgMapping.getAttributeMapping().get(i).getSystemName();
			CRMAttribute crmAttribute = prospectAttributeMap.get(systemName);
			if (crmAttribute.getCrmName().equals(systemName)) {
				prospectAttributeMap.get(systemName).setFromOrganization(true);
			}
		}
		return prospectAttributeMap;
	}

	private CRMMapping inheritCRMMapping(CRMMapping crmMapping) {
		if (crmMapping == null)
			return null;

		if (!crmMapping.isInherit())
			return crmMapping; // if inheritance is set to false then return the same crm mapping as it cannot
								// go up

		CRMMapping parentCRMMapping = getParentCRMMapping(crmMapping);
		CRMMapping mergedCRMMapping = mergeOrgToCrmMappings(parentCRMMapping, crmMapping);
		if (parentCRMMapping.getParent() == null)
			return mergedCRMMapping;
		else
			return inheritCRMMapping(mergedCRMMapping);
	}

	private CRMMapping getParentCRMMapping(CRMMapping crmMapping) {
		if (crmMapping == null)
			return null;

		return crmMappingRepository.findOneById(crmMapping.getParent());
	}

	private CRMMapping mergeCRMWithSystemMapping(CRMMapping systemMapping, CRMMapping orgMapping) {
		if (orgMapping != null) {
			for (CRMAttribute crmAttribute : orgMapping.getAttributeMapping()) {
				boolean found = false;
				for (CRMAttribute systemAttribute : systemMapping.getAttributeMapping()) {
					if (systemAttribute.getSystemName().equals(crmAttribute.getSystemName())) { // match System
																								// Attribute and Org
																								// Attribute
						crmAttribute.setProspectBeanPath(systemAttribute.getProspectBeanPath()); // copy over
																									// prospectbean path
																									// from system to
																									// CRM
						if (crmAttribute.getCrmName() == null)
							crmAttribute.setCrmName(systemAttribute.getSystemName()); // copy system name as crm name
						if (crmAttribute.getCrmDataType() == null)
							crmAttribute.setCrmDataType(systemAttribute.getSystemDataType()); // copy system data type
																								// as crm data type
						found = true;
						break;
					}
				}
				if (!found) {
					if (crmAttribute.getProspectBeanPath() == null)
						crmAttribute.setProspectBeanPath(
								"Prospect.CustomAttributes['" + crmAttribute.getSystemName() + "']"); // copy system
																										// name in
																										// prospect
																										// custom
																										// attributes
					if (crmAttribute.getCrmName() == null)
						crmAttribute.setCrmName(crmAttribute.getSystemName()); // copy system name as crm name
					if (crmAttribute.getCrmDataType() == null)
						crmAttribute.setCrmDataType(crmAttribute.getSystemDataType()); // copy system data type as crm
																						// data type
				}
			}
			return orgMapping;
		} else {
			for (CRMAttribute systemAttribute : systemMapping.getAttributeMapping()) {
				systemAttribute.setCrmName(systemAttribute.getSystemName()); // copy system name as crm name
				systemAttribute.setCrmDataType(systemAttribute.getSystemDataType()); // copy system data type as crm
																						// data type
			}
			return systemMapping; // if no org specific mapping then default to system level mappings
		}
	}

	private CRMMapping mergeOrgToCrmMappings(CRMMapping crmMapping, CRMMapping orgMapping) {
		if (orgMapping != null) {
			crmMapping.setId(orgMapping.getId());
			crmMapping.setCreatedBy(orgMapping.getCreatedBy());
			crmMapping.setCreatedDate(orgMapping.getCreatedDate());
			crmMapping.setUpdatedBy(orgMapping.getUpdatedBy());
			crmMapping.setUpdatedDate(orgMapping.getUpdatedDate());
			crmMapping.setOwnerId(orgMapping.getOwnerId());

			for (CRMAttribute childAttribute : orgMapping.getAttributeMapping()) {
				boolean found = false;
				for (CRMAttribute parentAttribute : crmMapping.getAttributeMapping()) {
					if (parentAttribute.getSystemName().equals(childAttribute.getSystemName())) {
						copyAttributeMapping(parentAttribute, childAttribute);
						found = true;
						break;
					}
				}
				if (!found) {
					CRMAttribute newMapping = new CRMAttribute();
					copyAttributeMapping(newMapping, childAttribute);
					crmMapping.getAttributeMapping().add(newMapping);
				}
			}
		}
		return crmMapping;
	}

	private void copyAttributeMapping(CRMAttribute destination, CRMAttribute source) {
		destination.setCrmName(source.getCrmName());
		destination.setLabel(source.getLabel());
		destination.setSystemName(source.getSystemName());
		destination.setCrmDataType(source.getCrmDataType());
		destination.setSystemDataType(source.getSystemDataType());
		destination.setLength(source.getLength());
		destination.setPickList(source.getPickList());
		destination.setQuestionSkin(source.getQuestionSkin());
		destination.setAttributeType(source.getAttributeType());
	}

	@Override
	public void addCRMAttribute(String organizationId, CRMAttribute crmAttribute) {
		CRMMapping orgMapping = crmMappingRepository.findOneById(organizationId);
		CRMMapping orgMappingSystem =crmMappingRepository.findOneById("SYSTEM");
		if (orgMapping == null) {
			// create default orgMapping
			orgMapping = new CRMMapping();
			orgMapping.setId(organizationId);
			orgMapping.setParent("SYSTEM");
			orgMapping.setOwnerId(organizationId);
			orgMapping.setAttributeMapping(new ArrayList<CRMAttribute>());
			orgMapping.setInherit(true);
		}
		List<CRMAttribute> attributeMapping = orgMapping.getAttributeMapping();
		if(attributeMapping != null && !attributeMapping.isEmpty() && attributeMapping.size() > 0) {
			List<CRMAttribute> campaignSkin = attributeMapping.stream()
					.filter(o -> (o.getCampaignId() != null && o.getCampaignId().equals(crmAttribute.getCampaignId()))).collect(Collectors.toList());
			List<String> qSkin = campaignSkin.stream().map(o -> o.getQuestionSkin()).collect(Collectors.toList());
			if(qSkin.contains(crmAttribute.getQuestionSkin())) {
				throw new IllegalArgumentException("Already same question text exist in this campaign.");
			}
			List<String> questionLabel = campaignSkin.stream().map(o -> o.getLabel()).collect(Collectors.toList());
			if(questionLabel.contains(crmAttribute.getLabel().toUpperCase())) {
				throw new IllegalArgumentException("Already same question exist in this campaign.");
			}
			List<CRMAttribute> attributeMappingSystem = orgMappingSystem.getAttributeMapping();
			List<String> questionLabelSystem = attributeMappingSystem.stream().map(o -> o.getLabel()).collect(Collectors.toList());
			if(questionLabelSystem.contains(crmAttribute.getLabel().toUpperCase())) {
				throw new IllegalArgumentException("Already same question exist in this campaign.");
			}
		}
		if (crmAttribute != null) {
			orgMapping.getAttributeMapping().add(crmAttribute);
		}
		crmMappingRepository.save(orgMapping);
	}

	@Override
	public HashMap<String, CRMAttribute> getAllQuestions(String organizationId,
			List<CRMAttributeDTO> crmAttributeDTOs) {
		CRMMapping systemMapping = getSystemMapping();
		HashMap<String, CRMAttribute> prospectAttributeMap;

		CRMMapping orgMapping = crmMappingRepository.findOneById(organizationId);
		if (orgMapping == null) {
			// create default orgMapping
			orgMapping = new CRMMapping();
			orgMapping.setId(organizationId);
			orgMapping.setParent("SYSTEM");
			orgMapping.setOwnerId(organizationId);
			orgMapping.setAttributeMapping(new ArrayList<CRMAttribute>());
			orgMapping.setInherit(true);
		}
		for (int i = 0; i < crmAttributeDTOs.size(); i++) {
			orgMapping.getAttributeMapping().add(crmAttributeDTOs.get(i).toCRMAttribute());
		}
		// build org's crmmapping based on inheritance
		CRMMapping inheritedCRMMapping = inheritCRMMapping(orgMapping);

		// Merge CRM attribute with System level attribute mapping. This copies Prospect
		// bean path to CRM level attribute mapping
		CRMMapping mergedCRMMapping = mergeCRMWithSystemMapping(systemMapping, inheritedCRMMapping);
		prospectAttributeMap = new HashMap<String, CRMAttribute>(mergedCRMMapping.getAttributeMapping().size());
		for (CRMAttribute mergedCRMAttribute : mergedCRMMapping.getAttributeMapping()) {
			prospectAttributeMap.put(mergedCRMAttribute.getCrmName(), mergedCRMAttribute); // map of attribute name with
																							// attribute object
		}
		return prospectAttributeMap;
	}

	@Override
	public HashMap<String, CRMAttribute> getCRMAttributeMap(String organizationId, String campaignId) {
		CRMMapping systemMapping = getSystemMapping();
		CRMMapping orgMapping = crmMappingRepository.findOneById(organizationId);
		HashMap<String, CRMAttribute> prospectAttributeMap = new HashMap<>();
		List<CRMAttribute> attributeMappings = new ArrayList<>();
		if (orgMapping.getAttributeMapping() != null && orgMapping.getAttributeMapping().size() > 0) {
			attributeMappings = orgMapping.getAttributeMapping().stream()
					.filter(attribute -> (attribute.getCampaignId() != null
							&& attribute.getCampaignId().equalsIgnoreCase(campaignId))
							|| attribute.isOrganizationQuestion())
					.collect(Collectors.toList());
		}
		for (CRMAttribute crmAttribute : systemMapping.getAttributeMapping()) {
			String key = crmAttribute.getCrmName();
			if (crmAttribute.getCrmName() == null || crmAttribute.getCrmName().isEmpty()) {
				key = crmAttribute.getSystemName();
			}
			prospectAttributeMap.put(key, crmAttribute);
		}
		for (CRMAttribute crmAttribute : attributeMappings) {
			String key = crmAttribute.getCrmName();
			if (crmAttribute.getCrmName() == null || crmAttribute.getCrmName().isEmpty()) {
				key = crmAttribute.getSystemName();
			}
			prospectAttributeMap.put(key, crmAttribute);
		}

		return prospectAttributeMap;
	}
	
	@Override
	public HashMap<String, CRMAttribute> getSystemCRMAttrubuteMap() {
		CRMMapping systemMapping = getSystemMapping();
		HashMap<String, CRMAttribute> systemMap = new HashMap<>();
		if (systemMapping != null) {
			for (CRMAttribute systemAttribute : systemMapping.getAttributeMapping()) {
				systemMap.put(systemAttribute.getSystemName(), systemAttribute);
			}
		}
		return systemMap;
	}

}
