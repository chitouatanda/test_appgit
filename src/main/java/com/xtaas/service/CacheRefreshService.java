package com.xtaas.service;

import java.util.List;
import java.util.Map;

import com.xtaas.domain.entity.Campaign;

public interface CacheRefreshService {

	public void refreshCache(String campaignId);

	public void refreshAllCache();

	public Boolean loadApplicationPropertyCacheForcefully();

	public Boolean refreshTwilioOutboundNumbers();

	public Boolean refreshPlivoOutboundNumbers();
	
	public Boolean refreshOrganizations();
	
	public Boolean refreshAgents();
	
	public Boolean refreshTelnyxOutboundNumbers();

	public Boolean updateTenCompanies(String campaignId);

	public String resetSupervisorOverrideCampaignId(String campaignId);

	public String resetSupervisorOverrideCampaignIdBySupervisorId(String supervisorId);

	void clearCache(Campaign campaign);

	public Boolean resetAgentCampaignByCampaignId(String campaignId);

	public Boolean resetAgentCampaignForMultipleCampaigns(List<String> campaignIds);
	
	public void getCountOfProspectsLoadedInMemory(String email);
	
	public Boolean refreshStateCodeCache();

	public String refreshCacheForCampaignIds(List<String> campaignIds, String environment);

	public void hardCacheRefreshCountryIsdCOdeMapForDatabuy();
	
	public Boolean refreshSignalwireOutboundNumbers();

	public List<String> removeProspectCacheInactiveCampaigns();

}
