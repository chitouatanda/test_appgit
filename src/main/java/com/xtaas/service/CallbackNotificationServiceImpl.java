package com.xtaas.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.AgentCampaignDTO;

@Service
public class CallbackNotificationServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(CallbackNotificationServiceImpl.class);

	final private ProspectCacheServiceImpl prospectCacheServiceImpl;

	final private TeamNoticeService teamNoticeService;

	final private AgentService agentService;

	final private AgentActivityLogRepository agentActivityLogrepository;

	public final Map<String, String> callBackAgentAssociationMap = new ConcurrentHashMap<String, String>();

	public CallbackNotificationServiceImpl(@Autowired ProspectCacheServiceImpl prospectCacheServiceImpl,
			@Autowired TeamNoticeService teamNoticeService, @Autowired AgentService agentService,
			@Autowired AgentActivityLogRepository agentActivityLogrepository) {
		this.prospectCacheServiceImpl = prospectCacheServiceImpl;
		this.teamNoticeService = teamNoticeService;
		this.agentService = agentService;
		this.agentActivityLogrepository = agentActivityLogrepository;
	}

	public void sendCallbackNotificationToAgentBefore10Minutes() throws InterruptedException, ParseException {
		CallbackProspectCacheDTO callBackDTO = prospectCacheServiceImpl.getCallbackRecordsFromCache();
		if (callBackDTO != null) {
			 if (!callBackAgentAssociationMap.containsKey(callBackDTO.getProspectCallId())) {
				AgentCampaignDTO agentCampaignDTO = agentService.getCurrentAllocatedCampaign(callBackDTO.getAgentId());
				if (agentCampaignDTO != null
						&& agentCampaignDTO.getId().equalsIgnoreCase(callBackDTO.getCampaignId())) {
//					AgentCall agentCall = agentRegistrationService.getAgentCall(callBackDTO.getAgentId());
//					if (agentCall != null && agentCall.getCurrentStatus().equals(AgentStatus.ONLINE)) {
					addInmapAndNotifyAgent(callBackDTO);
//					}
				} else {
//					AgentCall agentCall = agentRegistrationService.getAvailableAgent(callBackDTO.getCampaignId());
					Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
					List<AgentActivityLog> lastLoggedInAgent = agentActivityLogrepository
							.findLatestOnlineAgent(callBackDTO.getCampaignId(), pageable);
					if (lastLoggedInAgent != null && lastLoggedInAgent.size() > 0) {
						callBackDTO.setAgentId(lastLoggedInAgent.get(0).getAgentId());
						addInmapAndNotifyAgent(callBackDTO);
					}
				}
			 }
		}
	}

	private void addInmapAndNotifyAgent(CallbackProspectCacheDTO callBackDTO) throws ParseException {
		StringBuffer prospectName = new StringBuffer();
		 callBackAgentAssociationMap.put(callBackDTO.getProspectCallId(), callBackDTO.getAgentId());
		prospectName = prospectName.append(callBackDTO.getProspect().getFirstName()).append(" ")
				.append(callBackDTO.getProspect().getLastName());
		notifyAgent(callBackDTO.getAgentId(), prospectName.toString(), callBackDTO);
	}

	private void notifyAgent(String agentId, String prospectName, CallbackProspectCacheDTO callBackDTO)
			throws ParseException {
		if (agentId != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
			Date callbackInIST = XtaasDateUtils
					.convertToTimeZone(callBackDTO.getCallbackDate(), "IST"); // hack for
																											// now
			String time = formatter.format(callbackInIST);
			PusherUtils.pushMessageToUser(agentId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
			String notice = " There is callback scheduled at " + time + " with " + prospectName;
			teamNoticeService.saveUserNotices(agentId, notice);
			prospectCacheServiceImpl.insertCallbackRecordsInEventCache(callBackDTO);
			logger.info(
					"sendCallbackNotificationToAgentBefore10Minutes() :: Sent callback notification to agent [{}], campaignId [{}], prospectCallId [{}]",
					callBackDTO.getAgentId(), callBackDTO.getCampaignId(), callBackDTO.getProspectCallId());
		} else {
			logger.info("AgentId is not available in prospect with prospectCallId [{}]",
					callBackDTO.getProspectCallId());
		}
	}

	 public String getCallbackNotifiedAgentId(String prospectCallId) {
	 	return callBackAgentAssociationMap.get(prospectCallId);
	 }

	 public String removeCallbackNotifiedAgentId(String prospectCallId) {
	 	return callBackAgentAssociationMap.remove(prospectCallId);
	 }

}