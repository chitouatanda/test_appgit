package com.xtaas.service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.LoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value="externalUserDetailsServiceImpl")
public class ExternalUserDetailsServiceImpl implements UserDetailsService {
	  private static final Logger log = LoggerFactory.getLogger(ExternalUserDetailsServiceImpl.class);
	
	  @Autowired
	  LoginRepository loginRepository;
	
	  @Override
	  public UserDetails loadUserByUsername(String secretKey) throws UsernameNotFoundException {
		  Login login = loginRepository.findByExternalSecretKey(secretKey);
		  log.debug("loadUserByUsername() : SecretKey: " + secretKey + ", Login:"+login);
		  if(login == null) throw new UsernameNotFoundException(secretKey+" not found");
		  List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(login.getRoles()==null? 0 : login.getRoles().size());
		  if(login.getRoles() != null) for(String role : login.getRoles()) authorities.add(new SimpleGrantedAuthority("ROLE_"+role));
		  return new User(login.getUsername(), login.getPassword(), authorities);
	  }
}
