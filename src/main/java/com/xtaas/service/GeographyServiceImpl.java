package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Country;
import com.xtaas.db.entity.Geography;
import com.xtaas.db.entity.State;
import com.xtaas.db.repository.GeographyRepository;
import com.xtaas.domain.valueobject.GeographyType;
import com.xtaas.domain.valueobject.Location;

@Service
public class GeographyServiceImpl implements GeographyService {
	@Autowired
	private GeographyRepository geographyRepository; 
	
	@Override
	public List<Location> getAreas() {
		List<Geography> geographies = geographyRepository.findByType(GeographyType.AREA);
		if (!geographies.isEmpty()) {
			return geographies.get(0).getLocations();
		}
		return null;
	}
	
	@Override
	public List<Location> getCountries() {
		List<Geography> geographies = geographyRepository.findByType(GeographyType.COUNTRY);
		List<Location> countries = new ArrayList<Location>();
		for (Geography geography : geographies) {
			countries.addAll(geography.getLocations());
		}
		return countries;
	}
	
	@Override
	public List<Location> getStates() {
		List<Geography> geographies = geographyRepository.findByType(GeographyType.STATE);
		List<Location> states = new ArrayList<Location>();
		for (Geography geography : geographies) {
			states.addAll(geography.getLocations());
		}
		return states;
	}

	@Override
	public List<Location> getCities() {
		//countryCodes = getIncludedCountries(countryCodes, excludeCountries);
		//stateCodes = getIncludedStates(countryCodes, stateCodes, excludeStates);
		List<Geography> geographies = geographyRepository.findByType(GeographyType.CITY);
		List<Location> cities = new ArrayList<Location>();
		for (Geography geography : geographies) {
			cities.addAll(geography.getLocations());
		}
		return cities;
	}

	@Override
	public List<String> getIncludedCountries(List<String> countries, boolean excludeCountries) {
		if (countries != null && countries.size() != 0) {
			if (excludeCountries) {
				List<String> includedCountries = new ArrayList<String>();
				/*for (Country country : getRemainingCountries(countries)) {
					includedCountries.add(country.getCountryCode());
				}*/
				return includedCountries;
			}
		}
		return countries;
	}

	@Override
	public List<String> getIncludedStates(List<String> countries, List<String> states, boolean excludeStates) {
		if (states != null && states.size() != 0) {
			if (excludeStates) {
				List<String> includedStates = new ArrayList<String>();
				/*for (State state : getRemainingStates(countries, states)) {
					includedStates.add(state.getStateCode());
				}*/
				return includedStates;
			}
		}
		return states;
	}

	@Override
	public List<String> getIncludedCities(List<String> countries, List<String> states, List<String> cities,
			boolean excludeCities) {
		if (cities != null && cities.size() != 0) {
			if (excludeCities) {
				List<String> includedCities = new ArrayList<String>();
				/*for (String city : getRemainingCities(countries, states, cities)) {
					includedCities.add(city);
				}*/
				return includedCities;
			}
		}
		return cities;
	}

	/*private List<Country> getRemainingCountries(List<String> negativeCodes) {
		List<Geography> geographies = geographyRepository.getByNegativeCountries(negativeCodes);
		List<Country> countries = new ArrayList<Country>();
		for (Geography geography : geographies) {
			Country country = new Country();
			country.setCountryCode(geography.getCountryCode());
			country.setCountryName(geography.getCountryName());
			countries.add(country);
		}
		return countries;
	}

	private List<State> getRemainingStates(List<String> countryCodes, List<String> negativeCodes) {
		List<Geography> geographies = geographyRepository.getByNegativeStates(countryCodes, negativeCodes);
		List<State> states = new ArrayList<State>();
		for (Geography geography : geographies) {
			for (State state : geography.getStates()) {
				if (!negativeCodes.contains(state.getStateCode())) {
					states.add(state);
				}
			}
		}
		return states;
	}

	private List<String> getRemainingCities(List<String> countryCodes, List<String> stateCodes, List<String> negativeCodes) {
		List<Geography> geographies = geographyRepository.getByNegativeCities(countryCodes, stateCodes, negativeCodes);
		List<String> cities = new ArrayList<String>();
		for (Geography geography : geographies) {
			for (State state : geography.getStates()) {
				if (stateCodes.contains(state.getStateCode())) {
					for (String city : state.getCities()) {
						if (!negativeCodes.contains(city)) {
							cities.add(city);
						}
					}
				}
			}
		}
		return cities;
	}*/
}
