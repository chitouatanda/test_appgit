package com.xtaas.service;

import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

public interface LoginUploadService {

	public String uploadLoginDetails( MultipartFile file, HttpSession httpSession);

}
