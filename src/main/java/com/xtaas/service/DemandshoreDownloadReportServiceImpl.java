package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Recording;
import com.twilio.sdk.resource.list.RecordingList;
import com.xtaas.application.service.CampaignService;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.FailedRecordingsToAWS;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.FailedRecordingToAWSRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Service
public class DemandshoreDownloadReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DemandshoreDownloadReportServiceImpl.class);
	
	private static final String DMS_ACCOUNT_SID = "AC517a9586ed1afe81d4fda44f90778d53";
	private static final String DMS_AUTH_TOKEN = "e9eda3a5a24028435e472992be403dc1";
	String clientRegion = "us-west-2";
	String bucketName = "insideup";
	String recordingBucketName = "xtaasrecordings";
	String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
	String awsSecretKey = "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";
	private static final String SUFFIX = "/";

	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;

	private static final String PRIMARY_CONTACT = "Primary Contact";

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private FailedRecordingToAWSRepository failedRecordingToAWSRepository;

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	public void downloadDemandshoreLeadReport(ProspectCallSearchDTO prospectCallSearchDTO, Login login)
			throws ParseException, Exception {
		try {
			ProspectCallSearchDTO tempProspectCallSearchDTO = prospectCallSearchDTO;
			logger.debug("downloadDemandshoreLeadReport() : Request to download lead report for demandshore");
			List<Campaign> campaigns = campaignService.getCampaignByIds(prospectCallSearchDTO.getCampaignIds());
			if (prospectCallSearchDTO.getCampaignIds().size() == 0) {
				throw new IllegalArgumentException("Campaign is mandatory for the lead report.");
			}
			ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
			ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
			List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();

			if (prospectCallSearchDTO.getLeadStatus() != null && prospectCallSearchDTO.getLeadStatus().size() > 0) {
				List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
				List<String> successLeadStatus = new ArrayList<String>();
				List<String> failureLeadStatus = new ArrayList<String>();
				for (String lStatus : leadStatusList) {
					if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())) {
						successLeadStatus.add(lStatus);
					} else {
						failureLeadStatus.add(lStatus);
					}
				}
				if (successLeadStatus.size() > 0) {
					List<String> dispositionList = new ArrayList<String>(1);
					dispositionList.add("SUCCESS");
					successPCSDTO = prospectCallSearchDTO;
					successPCSDTO.setLeadStatus(successLeadStatus);
					successPCSDTO.setDisposition(dispositionList);
					successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
					List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
							.searchProspectCallByReport(successPCSDTO);
					prospectCallLogList.addAll(prospectCallLogSuccess);
				}
				if (failureLeadStatus.size() > 0) {
					List<String> dispositionList = new ArrayList<String>(1);
					dispositionList.add("FAILURE");
					failurePCSDTO = prospectCallSearchDTO;
					failurePCSDTO.setLeadStatus(null);
					failurePCSDTO.setDisposition(dispositionList);
					failurePCSDTO.setSubStatus(failureLeadStatus);
					successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
					List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl
							.searchProspectCallByReport(failurePCSDTO);
					prospectCallLogList.addAll(prospectCallLogFailure);
				}
			} else {
				successPCSDTO = prospectCallSearchDTO;
				successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(successPCSDTO);
				prospectCallLogList.addAll(prospectCallLogSuccess);
			}

			if (prospectCallLogList.size() <= 0) {
				logger.debug("No records found for given campaigns. ");
			}
			AmazonS3 s3client = createAWSConnection(clientRegion);
			Workbook workbook = new XSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("Leads");
			List<Expression> agentValidationQuestions = new ArrayList<>();
			//Map<String, List<String>> customQuestionMap = new HashMap<String, List<String>>();
			Map<String, Map<String,String>> customQuestionMap = new HashMap<String, Map<String,String>>();

			Map<String, List<Expression>> agentValidationQuestionsByCamp = new HashMap<String, List<Expression>>();
			//int headerCount = 0;
			if (campaigns.size() > 0) {
				for (Campaign singleCampaign : campaigns) {
					int campaignHeaderCount = 0;
					List<Expression> campaignQuestions = singleCampaign.getQualificationCriteria().getExpressions();
					if (campaignQuestions.size() > 0) {
						List<Expression> questions = campaignQuestions.stream()
								.filter(question -> question.getAgentValidationRequired() == true)
								.collect(Collectors.toList());
						agentValidationQuestions.addAll(questions);
						agentValidationQuestionsByCamp.put(singleCampaign.getId(), questions);
					}
				}
			}
			Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
			
			int headerCount = 0;
			int counter=1;
			 for (Map.Entry<String,List<Expression>> entry : agentValidationQuestionsByCamp.entrySet()) {
				 List<Expression> qlist = entry.getValue();
				 //int counter = 1;
				 for(Expression ex : qlist){
					 Map<String,String> cvalue = new HashMap<String,String>();
					 cvalue.put("Custom Question "+counter, ex.getQuestionSkin());
					 customQuestionMap.put(entry.getKey(), cvalue);
				 }
				 if(qlist.size()>headerCount)
					 headerCount = qlist.size(); 
			}
			 boolean dncFlag = false;
			 if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1 && tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
				 dncFlag = true;
	            }
			String[] columns = createExcelHeaders(agentValidationQuestions, headerCount, dncFlag);
			writeLeadsToExcelFile(columns, workbook, createHelper, sheet, campaigns, campaign, prospectCallLogList,
					customQuestionMap, agentValidationQuestions, dncFlag);
			String fileName = null;
			if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1 && tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
				fileName = System.currentTimeMillis() + "_DNCReport.xlsx";
            } else if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1 && tempProspectCallSearchDTO.getSubStatus().contains("DNCL") ) {
            	fileName = System.currentTimeMillis() + "_FailureReport.xlsx";
			} else {
				fileName = System.currentTimeMillis() + "_LeadReport.xlsx";
			}
			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			String message = "Please find the attached lead file.";
			workbook.close();
			boolean leadReportSent = false;
			String subject;
            if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1 && tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
            	subject = "DNC Report File Attached.";
            } else if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1 && tempProspectCallSearchDTO.getSubStatus().contains("DNCL") ) {
            	subject = "Failure Report File Attached.";
			} else {
				subject = "Lead Report File Attached.";
			}
            
            try {
    			String leadfileName = "DEMANDSHORE_LEAD_REPORTS" + SUFFIX + file;
    			s3client.putObject(new PutObjectRequest(bucketName, leadfileName, file)
    					.withCannedAcl(CannedAccessControlList.PublicRead));
    			logger.debug("Recording File uploaded successfully to s3 storage.");
    		} catch (Exception ex) {
    			ex.printStackTrace();
    		}
            
            if (login != null) {
   			List<String> emails = login.getReportEmails();
   			if (emails != null && emails.size() > 0) {
   				for (String email : emails) {
   					if (email != null && !email.isEmpty()) {
   						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
   								campaign.getName() + subject, message, fileNames);
   						logger.debug("Demandshore lead report for the campaign [{}] sent  successfully to [{}] ",
   								campaign.getName(), email);
   						leadReportSent = true;
   					}
   				}
   			}
   			
   			if (!leadReportSent) {
					XtaasEmailUtils.sendEmail("kchugh@xtaascorp.com", "data@xtaascorp.com",
							fileName + subject, message, fileNames);
					logger.debug("Lead report sent successfully to kchugh@xtaascorp.com");
				}
    		}
			/*Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (organization.getContacts() != null && organization.getContacts().size() > 0) {
				List<Contact> contacts = organization.getContacts();
				for (Contact contact : contacts) {
					if (contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) && contact.getEmail() != null
							&& !contact.getEmail().isEmpty()) {
						XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com",
								fileName + subject, message, fileNames);
						logger.debug("Lead report sent successfully to : " + contact.getEmail());
						leadReportSent = true;
					}
				}
				if (!leadReportSent) {
					XtaasEmailUtils.sendEmail("kchugh@xtaascorp.com", "data@xtaascorp.com",
							fileName + subject, message, fileNames);
					logger.debug("Lead report sent successfully to kchugh@xtaascorp.com");
				}
			}*/
           
			logger.debug("Lead report file sent as attachment in the email");
		} catch (Exception e) {
			logger.debug("downloadDemandshoreLeadReport() : Error in CsvFileWriter!!!");
			List<String> emailList = new ArrayList<>();
			emailList.add(login.getEmail());
			emailList.add("data@xtaascorp.com");
			StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Follwing error occured.");
			stringBuilder.append("<br />");
			stringBuilder.append("<br />");
			stringBuilder.append(exceptionAsString);
			stringBuilder.append("<br />");
			stringBuilder.append("<br />");
			stringBuilder.append("Please contact your administrator.");
			XtaasEmailUtils.sendEmail(emailList, "data@xtaascorp.com", "Exception occured during lead report download", stringBuilder.toString());
			logger.debug("Download lead report email sent successfully to : " + login.getEmail() + ", data@xtaascorp.com" );
			e.printStackTrace();
		}
	}
	
	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
			s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			return s3client;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(
					"Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :"
							+ e);
			return s3client;
		}
	}

	private String[] createExcelHeaders(List<Expression> agentValidationQuestions,
			 int headerCount, boolean dncFlag) {
		Map<String, String> colNames = new HashMap<String, String>();
		int i = 0;
		String[] columns = new String[1000];
		colNames.put("Date", "Date");
		columns[i] = "Date";
		i++;
		if (dncFlag) {
			colNames.put("Call Start Time", "Call Start Time");
			columns[i] = "Call Start Time";
			i++;
		}
		colNames.put("Agent Name", "Agent Name");
		columns[i] = "Agent Name";
		i++;
		colNames.put("Agent Emp ID", "Agent Emp ID");
		columns[i] = "Agent Emp ID";
		i++;
		colNames.put("Team Lead", "Team Lead");
		columns[i] = "Team Lead";
		i++;
		colNames.put("First Name", "First Name");
		columns[i] = "First Name";
		i++;
		colNames.put("Last Name", "Last Name");
		columns[i] = "Last Name";
		i++;
		colNames.put("Job Title", "Job Title");
		columns[i] = "Job Title";
		i++;
		colNames.put("Email ID", "Email ID");
		columns[i] = "Email ID";
		i++;
		colNames.put("Phone No.", "Phone No.");
		columns[i] = "Phone No.";
		i++;
		colNames.put("Company Name", "Company Name");
		columns[i] = "Company Name";
		i++;
		colNames.put("Address Line 1", "Address Line 1");
		columns[i] = "Address Line 1";
		i++;
		colNames.put("Address Line 2", "Address Line 2");
		columns[i] = "Address Line 2";
		i++;
		colNames.put("City", "City");
		columns[i] = "City";
		i++;
		colNames.put("State", "State");
		columns[i] = "State";
		i++;
		colNames.put("Postal Code", "Postal Code");
		columns[i] = "Postal Code";
		i++;
		colNames.put("Country", "Country");
		columns[i] = "Country";
		i++;
		colNames.put("Employee Size", "Employee Size");
		columns[i] = "Employee Size";
		i++;
		colNames.put("Industry Type", "Industry Type");
		columns[i] = "Industry Type";
		i++;
		//int recordCounter = 1;
		for (int j = 1; j <= headerCount; j++) {
			columns[i] = "Custom Question" + " " + j;
			//customQuestionMap.put(columns[i], question.getQuestionSkin());
			i++;
			columns[i] = "Answer" + " " + j;
			i++;
			//recordCounter++;
		}
		colNames.put("Whitepaper 1", "Whitepaper 1");
		columns[i] = "Whitepaper 1";
		i++;
		colNames.put("Whitepaper 2", "Whitepaper 2");
		columns[i] = "Whitepaper 2";
		i++;
		colNames.put("Campaign Name", "Campaign Name");
		columns[i] = "Campaign Name";
		i++;
		colNames.put("Email Status", "Email Status");
		columns[i] = "Email Status";
		i++;
		colNames.put("Pseudo Name", "Pseudo Name");
		columns[i] = "Pseudo Name";
		i++;
		colNames.put("Call Recording Link", "Call Recording Link");
		columns[i] = "Call Recording Link";
		i++;
		colNames.put("Lead Source", "Lead Source");
		columns[i] = "Lead Source";
		i++;
		colNames.put("Qualified", "Qualified");
		columns[i] = "Qualified";
		i++;
		colNames.put("Disq Parameter", "Disq Parameter");
		columns[i] = "Disq Parameter";
		i++;
		colNames.put("Disq Sub-Parameter", "Disq Sub-Parameter");
		columns[i] = "Disq Sub-Parameter";
		i++;
		colNames.put("QaId", "QaId");
		columns[i] = "QaId";
		i++;
		colNames.put("Comments", "Comments");
		columns[i] = "Comments";
		i++;
		colNames.put("Revenue Size", "Revenue Size");
		columns[i] = "Revenue Size";
		i++;
		colNames.put("Call Start Time", "Call Start Time");
		columns[i] = "Call Start Time";
		i++;
		colNames.put("Agent Notes", "Agent Notes");
		columns[i] = "Agent Notes";
		i++;
		colNames.put("Client Delivered", "Client Delivered");
		columns[i] = "Client Delivered";
		i++;
		colNames.put("Company Validation Link", "Company Validation Link");
		columns[i] = "Company Validation Link";
		i++;
		colNames.put("Title Validation Link", "Title Validation Link");
		columns[i] = "Title Validation Link";
		i++;
		colNames.put("Action", "Action");
		columns[i] = "Action";
		
		return columns;
	}

	private void writeLeadsToExcelFile(String[] columns, Workbook workbook, CreationHelper createHelper, Sheet sheet,
			List<Campaign> campaigns, Campaign campaign, List<ProspectCallLog> prospectCallLogList,
			Map<String, Map<String,String>> customQuestionMap, List<Expression> agentValidationQuestions,boolean dncFlag) throws ParseException {
		int recordsCounter = 0;
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		Row headerRow = sheet.createRow(0);

		for (int x = 0; x < columns.length; x++) {
			if (columns[x] != null && !columns[x].isEmpty()) {
				Cell cell = headerRow.createCell(x);
				cell.setCellValue(columns[x]);
				cell.setCellStyle(headerCellStyle);
			}
		}

		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		int rowNum = 1;
		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			Row row = sheet.createRow(rowNum++);
			ProspectCall prospectCall = prospectCallLog.getProspectCall();
			Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			Optional<Campaign> matchCampaign = campaigns.stream()
					.filter(filteredCampaign -> filteredCampaign.getId().equalsIgnoreCase(prospectCall.getCampaignId()))
					.findAny();

			for (int col = 0; col < columns.length; col++) {
				if (columns[col] != null && !columns[col].isEmpty()) {

					if (columns[col].contentEquals("Date")) {
						Cell cellx = row.createCell(col);
						DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
						pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
						if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
							String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
							Date pstDate = pstFormat.parse(pstStr);
							cellx.setCellValue(pstFormat.format(pstDate));
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (dncFlag && columns[col].contentEquals("Call Start Time")) {
							Cell cellx = row.createCell(col);
							DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
							pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
							if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
								String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
								Date pstDate = pstFormat.parse(pstStr);
								cellx.setCellValue(pstFormat.format(pstDate));
								cellx.setCellStyle(dateCellStyle);
							}
					} else if (columns[col].contentEquals("Agent Name")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getProspectCall().getAgentId() != null) {
							cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Agent Emp ID")) {
						Cell cellx = row.createCell(col);
						cellx.setCellValue("");
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Team Lead")) {
						if (prospectCallLog.getProspectCall().getAgentId() != null) {
							Team team = teamRepository.findByAgent(prospectCallLog.getProspectCall().getAgentId());
							if (team != null) {
								Cell cellx = row.createCell(col);
								if (team.getSupervisor() != null) {
									cellx.setCellValue(team.getSupervisor().getResourceId());
									cellx.setCellStyle(dateCellStyle);
								}
							}
						}
					} else if (columns[col].contentEquals("First Name")) {
						if (prospect.getFirstName() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getFirstName());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Last Name")) {
						if (prospect.getLastName() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getLastName());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Job Title")) {
						if (prospect.getTitle() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getTitle());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Email ID")) {
						if (prospect.getEmail() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getEmail());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Phone No.")) {
						if (prospect.getPhone() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getPhone());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Company Name")) {
						if (prospect.getCompany() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getCompany());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Address Line 1")) {
						if (prospect.getAddressLine1() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getAddressLine1());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Address Line 2")) {
						if (prospect.getAddressLine2() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getAddressLine2());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("City")) {
						if (prospect.getCity() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getCity());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("State")) {
						if (prospect.getStateCode() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getStateCode());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Postal Code")) {
						if (prospect.getZipCode() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getZipCode());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Country")) {
						if (prospect.getCountry() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getCountry());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Employee Size")) {
						Cell cellx = row.createCell(col);
						if (prospect.getCustomAttributeValue("minEmployeeCount") != null
								&& prospect.getCustomAttributeValue("maxEmployeeCount") != null) {
							String employeeSize = prospect.getCustomAttributeValue("minEmployeeCount").toString() + "-"
									+ prospect.getCustomAttributeValue("maxEmployeeCount").toString();
							cellx.setCellValue(employeeSize);
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Industry Type")) {
						if (prospect.getIndustry() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getIndustry());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Whitepaper 1")) {
						Cell cellx = row.createCell(col);
						String assetName = "";
						if (prospectCall.getDeliveredAssetId() != null
								&& !prospectCall.getDeliveredAssetId().isEmpty()) {
							assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
						}
						cellx.setCellValue(assetName);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Whitepaper 2")) {
						Cell cellx = row.createCell(col);
						cellx.setCellValue("");
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Campaign Name")) {
						String campaignName = null;
						Cell cellx = row.createCell(col);
						campaignName = (matchCampaign.isPresent()) ? matchCampaign.get().getName() : "";
						cellx.setCellValue(campaignName);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Email Status")) {
						String isMailBounced = "";
						if (prospectCall.isEmailBounce()) {
							isMailBounced = "BOUNCED";
						} else {
							isMailBounced = "NOT-BOUNCED";
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(isMailBounced);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Client Delivered")) {
						Cell cellx = row.createCell(col);
						if(prospectCallLog.getProspectCall().getClientDelivered() != null && !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
							cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
						else
							cellx.setCellValue("");
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Pseudo Name")) {
						Cell cellx = row.createCell(col);
						cellx.setCellValue("");
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Call Recording Link")) {
						if (prospectCallLog.getProspectCall().getRecordingUrlAws() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
							cellx.setCellStyle(dateCellStyle);
						}else if(prospectCallLog.getProspectCall().getRecordingUrl() != null){
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrl());
							cellx.setCellStyle(dateCellStyle);
							FailedRecordingsToAWS fra = new FailedRecordingsToAWS();
							fra.setCampaignId(campaign.getId());
							fra.setProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
							fra.setTwilioRecordingUrl(prospectCallLog.getProspectCall().getRecordingUrl());
							failedRecordingToAWSRepository.save(fra);
						}else{
							
							try {
								Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
								TwilioRestClient client = new TwilioRestClient(DMS_ACCOUNT_SID, DMS_AUTH_TOKEN);
						        Account account = client.getAccount();
								//Account account = channelTwilio.getChannelConnection();
								Map<String, String> filter = new HashMap<String, String>();
								filter.put("CallSid", prospectCallLog.getProspectCall().getTwilioCallSid());
								RecordingList recordings = account.getRecordings(filter);
								String awsRecordingUrl = "";
								for (Recording recording : recordings) {
									/*
									 * api returns with extension as .json..removing it so that it becomes playable.
									 * Ex: /2010-04-01/Accounts/ACab4d611aa1465a246cb58290ba930a5c/Recordings/
									 * REb64d147f6a4008ae4e308486e4029a5a.json
									 */
									String recordingUrl = "https://api.twilio.com"
											+ StringUtils.replace(recording.getProperty("uri"), ".json", "");
									
									prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
									prospectCallLogRepository.save(prospectCallLog);
									
									FailedRecordingsToAWS fra = new FailedRecordingsToAWS();
									fra.setCampaignId(campaign.getId());
									fra.setProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
									fra.setTwilioRecordingUrl(recordingUrl);
									failedRecordingToAWSRepository.save(fra);
									Cell cellx = row.createCell(col);
									cellx.setCellValue(recordingUrl);
									cellx.setCellStyle(dateCellStyle);
									
									// String prospectCallId = paramMap.get("pcid");
									// downloadRecordingsToAws.downloadRecording(recordingUrl, prospectCallId, "", true);
									break; // return the first recording only. support for multiple call recordings
								}
							}catch (Exception e) {
								logger.error("Error occured in TwilioServiceImpl. Exception is [{}]", e);
							}
							
						}
					} else if (columns[col].contentEquals("Lead Source")) {
						if (prospect != null && prospect.getZoomPersonUrl() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getZoomPersonUrl());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Qualified")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getProspectCall().getLeadStatus() != null) {
							cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus().toString());
							cellx.setCellStyle(dateCellStyle);
						} else {
							cellx.setCellValue(prospectCallLog.getProspectCall().getSubStatus().toString());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Disq Parameter")) {
						String qaReason = "";
						if (prospectCallLog.getQaFeedback() != null
								&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
							List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
									.getFeedbackResponseList();
							if (fsrList.size() > 0) {
								for (FeedbackSectionResponse fsr : fsrList) {
									if (fsr != null) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											if (fraList.size() > 0) {
												for (FeedbackResponseAttribute fra : fraList) {
													if (fra!=null && fra.getAttribute()!=null && 
															fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
														if (fra.getRejectionReason() != null) {
															qaReason = qaReason + fra.getRejectionReason();
														}
													}
												}
											}
										}
									}
								}
							}
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(qaReason);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Disq Sub-Parameter")) {
						Cell cellx = row.createCell(col);
						cellx.setCellValue("");
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("QaId")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getQaFeedback() != null
								&& prospectCallLog.getQaFeedback().getQaId() != null) {
							cellx.setCellValue(prospectCallLog.getQaFeedback().getQaId());
						} else {
							cellx.setCellValue("");
						}
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Comments")) {
						String callnotes = "";
						if (prospectCallLog.getQaFeedback() != null
								&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
							List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
									.getFeedbackResponseList();
							if (fsrList.size() > 0) {
								for (FeedbackSectionResponse fsr : fsrList) {
									if (fsr != null) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											if (fraList.size() > 0) {
												for (FeedbackResponseAttribute fra : fraList) {
													if (fra != null) {
														if (fra.getAttribute()!=null && fra.getAttribute()
																.equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
															if (fra.getAttributeComment() != null) {
																callnotes = fra.getAttributeComment();
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(callnotes);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Revenue Size")) {
						Cell cellx = row.createCell(col);
						if (prospect.getCustomAttributeValue("minRevenue") != null
								&& prospect.getCustomAttributeValue("maxRevenue") != null) {
							String revenueSize = prospect.getCustomAttributeValue("minRevenue").toString() + "-"
									+ prospect.getCustomAttributeValue("maxRevenue").toString();
							cellx.setCellValue(revenueSize);
							cellx.setCellStyle(dateCellStyle);
						}
					}  else if (columns[col].contentEquals("Call Start Time")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog != null && prospectCallLog.getProspectCall() != null && prospectCallLog.getProspectCall().getCallStartTime() != null) {
							DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
							pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
							String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
							Date pstDate = pstFormat.parse(pstStr);
							cellx.setCellValue(pstFormat.format(pstDate));
							cellx.setCellStyle(dateCellStyle);
						}
					}
					else if (columns[col].contentEquals("Agent Notes")) {
						Cell cellx = row.createCell(col);
						StringBuffer stringBuffer = new StringBuffer();
						if (prospectCallLog.getProspectCall().getNotes() != null
								&& prospectCallLog.getProspectCall().getNotes().size() > 0) {
							List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
							for (Note note : notesArr) {
								if (note != null) {
									stringBuffer.append(note.getAgentId() + " : " + note.getText());
								}
							}
						}
						cellx.setCellValue(stringBuffer.toString());
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Company Validation Link")) {
						Cell cellx = row.createCell(col);
						String companyUrl = null;
						if (prospect != null && prospect.getZoomCompanyUrl() != null) {
							companyUrl = prospect.getZoomCompanyUrl();
						}
						cellx.setCellValue(companyUrl);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Title Validation Link")) {
						Cell cellx = row.createCell(col);
						String personUrl = null;
						if (prospect != null && prospect.getZoomPersonUrl() != null) {
							personUrl = prospect.getZoomPersonUrl();
						} 
						cellx.setCellValue(personUrl);
						cellx.setCellStyle(dateCellStyle);
					} else {
						if (customQuestionMap != null && customQuestionMap.size() > 0) {
							Map<String, String> innerCQMap = customQuestionMap.get(prospectCall.getCampaignId());
							if (innerCQMap != null) {
								for (Map.Entry<String, String> entryMap : innerCQMap.entrySet()) {
									// Map<String,String> questionsList = entryMap.getValue();
									if (columns[col].equalsIgnoreCase(entryMap.getKey())) {
										if (prospectCall.getAnswers() != null) {
											for (AgentQaAnswer agentAnswer : prospectCall.getAnswers()) {
												// for(String quest : entryMap.getValue()){
												if (entryMap.getValue().equalsIgnoreCase(agentAnswer.getQuestion())) {
													if (agentAnswer.getQuestion() != null) {
														Cell cellx = row.createCell(col);
														cellx.setCellValue(agentAnswer.getQuestion());
														cellx.setCellStyle(dateCellStyle);
													}
													col++;
													Optional<Expression> matchQuestion = agentValidationQuestions
															.stream()
															.filter(filteredQuestion -> filteredQuestion
																	.getQuestionSkin()
																	.equalsIgnoreCase(agentAnswer.getQuestion()))
															.findAny();
													if (matchQuestion.isPresent()) {
														ArrayList<PickListItem> pickList = matchQuestion.get()
																.getPickList();
														Optional<PickListItem> answer = pickList.stream()
																.filter(pickListItem -> pickListItem.getValue()
																		.equalsIgnoreCase(agentAnswer.getAnswer()))
																.findAny();
														if (answer.isPresent()) {
															Cell cellAnswer = row.createCell(col);
															cellAnswer.setCellValue(answer.get().getLabel());
															cellAnswer.setCellStyle(dateCellStyle);
														}
													}
												}
												// }
											}
										}
									}
								}
							}
						}
					}
					/////
				}
			}
			recordsCounter++;
		}
	}

	private String getAssetNameFromId(Campaign campaign, String assetId) {
		String assetName = "";
		if (campaign.getAssets() != null && campaign.getAssets().size() > 0) {
			List<Asset> assets = campaign.getAssets();
			
			Optional<Asset> matchAsset = assets.stream().filter(asset -> asset.getAssetId().equalsIgnoreCase(assetId))
					.findAny();
			if (matchAsset.isPresent()) {
				assetName = matchAsset.get().getName();
			}
		}
		return assetName;
	}
}
