package com.xtaas.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.CustomAggregationOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import com.mongodb.BasicDBObject;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.ConnectDetailsDTO;
import com.xtaas.web.dto.DailyBillingDTO;

@Service
public class ProspectCallInteractionServiceImpl implements ProspectCallInteractionService {

	private final Logger logger = LoggerFactory.getLogger(ProspectCallInteractionServiceImpl.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private ProspectCallInteractionRepository prospectLogInteractionRepository;
	
	@Autowired
	private CampaignRepository campaignRepository; 
	
	@Override
	public ProspectCallInteraction getRecentDialerProcessed(String campaignId) {
		Date todaysDate = XtaasDateUtils.getCurrentDateInTZ(0, XtaasDateUtils.TZ_UTC).toDate();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		List<ProspectCallInteraction> prospectCallInteractionList = prospectLogInteractionRepository.findRecentDialerProcessed(campaignId, todaysDate, pageable);
		if (prospectCallInteractionList == null || prospectCallInteractionList.isEmpty()) return null;
		return prospectCallInteractionList.get(0);
	}
	
	
	@Override
	public Map<String, Integer> getAgentCallStats(String campaignId,String agentId, Date fromDate/*, Date toDate*/) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		Map<String, Integer> dispostionMap = new HashMap<String, Integer>();
		aggregationOperations.add(buildBasicMatch(campaignId,agentId, fromDate/*, toDate*/));
		aggregationOperations.add(buildGroup());
		AggregationResults<Object> results =  mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcallinteraction", Object.class);
		List<Object> dispositionStatusMetrics = results.getMappedResults();
		
		if (!dispositionStatusMetrics.isEmpty()) {
			//construct QA metrics of failure, callback , success, dialer-code disposition status
			HashMap<String, Integer> metrics = new HashMap<String, Integer>();
			for (Object dispositionStatusMetric : dispositionStatusMetrics) {
				LinkedHashMap<?, ?> dispositionStatusMetricsMap = (LinkedHashMap<?, ?>) dispositionStatusMetric;
				String dispositionStatus = (String) dispositionStatusMetricsMap.get("_id");
				Integer dispositionStatusCount = (Integer) dispositionStatusMetricsMap.get("count");
				dispostionMap.put(dispositionStatus, dispositionStatusCount);
			}
		}
		return dispostionMap;
		
	}
	
	private AggregationOperation buildBasicMatch(String campaignId, String agentId, Date fromDate/*, Date toDate*/) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		criterias.add(Criteria.where("prospectCall.agentId").in(agentId));
		criterias.add(Criteria.where("updatedDate").gte(fromDate)); 
		return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
	}
	
	private AggregationOperation buildGroup() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.dispositionStatus");
		group = group.append("count", new BasicDBObject("$sum", 1));
		/*group = group.append("leadsDelivered", new BasicDBObject("$sum", 
					new BasicDBObject("$cond", new Object[] {
							new BasicDBObject("$eq", new Object[] { "$prospectCall.dispositionStatus", "SUCCESS" }), 1, 0 })));*/
		//group = group.append("averageTalkTime", new BasicDBObject("$avg", "$prospectCall.callDuration"));
		
		return new CustomAggregationOperation(new Document("$group", group));
	}
	
	
	///////////////////////////////////////
	
	@Override
	public List<String> getDistinctAgents(String campaignId, Date fromDate/*, Date toDate*/) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		Map<String, Integer> dispostionMap = new HashMap<String, Integer>();
		List<String> agentIds = new ArrayList<String>();
		aggregationOperations.add(buildBasicMatch(campaignId, fromDate/*, toDate*/));
		aggregationOperations.add(buildGroupAgent());
		AggregationResults<Object> results =  mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcallinteraction", Object.class);
		List<Object> dispositionStatusMetrics = results.getMappedResults();
		
		if (!dispositionStatusMetrics.isEmpty()) {
			//construct QA metrics of failure, callback , success, dialer-code disposition status
			HashMap<String, Integer> metrics = new HashMap<String, Integer>();
			for (Object dispositionStatusMetric : dispositionStatusMetrics) {
				LinkedHashMap<?, ?> dispositionStatusMetricsMap = (LinkedHashMap<?, ?>) dispositionStatusMetric;
				String agentId = (String) dispositionStatusMetricsMap.get("_id");
				Integer count = (Integer) dispositionStatusMetricsMap.get("count");
				dispostionMap.put(agentId, count);
				agentIds.add(agentId);
			}
		}
		return agentIds;
		
	}
	
	private AggregationOperation buildBasicMatch(String campaignId, Date fromDate/*, Date toDate*/) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		//criterias.add(Criteria.where("prospectCall.agentId").in(agentId));
		criterias.add(Criteria.where("updatedDate").gte(fromDate)); 
		return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
	}
	
	private AggregationOperation buildGroupAgent() {
		BasicDBObject group = new BasicDBObject("_id", "$prospectCall.agentId");
		group = group.append("count", new BasicDBObject("$sum", 1));
		/*group = group.append("leadsDelivered", new BasicDBObject("$sum", 
					new BasicDBObject("$cond", new Object[] {
							new BasicDBObject("$eq", new Object[] { "$prospectCall.dispositionStatus", "SUCCESS" }), 1, 0 })));*/
		//group = group.append("averageTalkTime", new BasicDBObject("$avg", "$prospectCall.callDuration"));
		
		return new CustomAggregationOperation(new Document("$group", group));
	}


	@Override
	public DailyBillingDTO getConnectData(String span) {
		logger.debug("getConnectData() : fetching data for [{}] report.", span);
		DailyBillingDTO dailyBillingDTO = new DailyBillingDTO();
		List<ConnectDetailsDTO> connectDetailsDTOs = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Calendar cal1 = Calendar.getInstance();
		cal1.set(Calendar.HOUR_OF_DAY, 23);
		cal1.set(Calendar.MINUTE, 59);
		cal1.set(Calendar.SECOND, 59);
		Date startDate = null;
		Date endDate = null;
		if (span.equalsIgnoreCase("monthly")) {
			cal.set(Calendar.DAY_OF_MONTH, 1);
			startDate = cal.getTime();
			endDate = cal1.getTime();
		} else if (span.equalsIgnoreCase("daily")) {
			startDate = cal.getTime();
			endDate = cal1.getTime();
			
		}
		int recordsAttempted = 0;
		int contacts = 0;
		int connects = 0;
		int success = 0;
		int totalAttempts = 0;
		int totalContacts = 0;
		int totalConnects = 0;
		int totalSuccess = 0;
		double totalCost = 0;
		List<Campaign> campaigns = campaignRepository.getConnectCampaigns();
		if (campaigns != null && campaigns.size() > 0) {
			for (Campaign campaign : campaigns) {
				ConnectDetailsDTO connectDetailsDTO = new ConnectDetailsDTO();
				List<String> campaignIds = new ArrayList<>();
				campaignIds.add(campaign.getId());
				recordsAttempted = prospectLogInteractionRepository.findRecordsAttempted(campaignIds,
						startDate, endDate);
				contacts = prospectLogInteractionRepository.findContactedRecordsCount(campaignIds, startDate,
						endDate);
				connects = prospectLogInteractionRepository.findConnectRecordsCount(campaignIds, startDate,
						endDate);
				success = prospectLogInteractionRepository.findSuccessRecordsCount(campaignIds, startDate,
						endDate);
				totalAttempts = totalAttempts + recordsAttempted;
				totalContacts = totalContacts + contacts;
				totalConnects = totalConnects + connects;
				totalSuccess = totalSuccess + success;
				connectDetailsDTO.setCampaignName(campaign.getName());
				connectDetailsDTO.setRecordsAttempted(recordsAttempted);
				connectDetailsDTO.setContacted(contacts);
				connectDetailsDTO.setConnected(connects);
				connectDetailsDTO.setSuccessRecords(success);
				connectDetailsDTO.setCostPerConnect(0.5);
				connectDetailsDTO.setConnectCost(connects * 0.5);
				totalCost = totalCost + connectDetailsDTO.getConnectCost();
				connectDetailsDTOs.add(connectDetailsDTO);
			}
			if (connectDetailsDTOs != null && connectDetailsDTOs.size() > 0) {
				ConnectDetailsDTO detailsDTO = new ConnectDetailsDTO();
				detailsDTO.setCampaignName("Total");
				detailsDTO.setRecordsAttempted(totalAttempts);
				detailsDTO.setContacted(totalContacts);
				detailsDTO.setConnected(totalConnects);
				detailsDTO.setSuccessRecords(totalSuccess);
				detailsDTO.setCostPerConnect(0.5);
				detailsDTO.setConnectCost(totalCost);
				connectDetailsDTOs.add(detailsDTO);
			}
			dailyBillingDTO.setConnectDetailsDTOs(connectDetailsDTOs);
			dailyBillingDTO.setTotalAttempts(totalAttempts);
			dailyBillingDTO.setTotalConnects(totalConnects);
			dailyBillingDTO.setTotalContacts(totalContacts);
			dailyBillingDTO.setTotalSuccess(totalSuccess);
			dailyBillingDTO.setTotalCost(totalCost);
		}
		return dailyBillingDTO;
	}


	@Override
	public Map<String, Integer> getTelcoDuration(List<String> campaignId, String agentId, Date startDate,
			Date endDate) {
		List<AggregationOperation> aggregationOperations = new ArrayList<AggregationOperation>();
		Map<String, Integer> dispostionMap = new HashMap<String, Integer>();
		aggregationOperations.add(buildBasicMatchTelco(campaignId, agentId, startDate, endDate));
		aggregationOperations.add(buildGroupTelco());
		AggregationResults<Object> results = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations),
				"prospectcallinteraction", Object.class);
		List<Object> dispositionStatusMetrics = results.getMappedResults();
		if (!dispositionStatusMetrics.isEmpty()) {
			int count = 0;
			// construct QA metrics of failure, callback , success, dialer-code disposition
			// status
			HashMap<String, Integer> metrics = new HashMap<String, Integer>();
			for (Object dispositionStatusMetric : dispositionStatusMetrics) {
				LinkedHashMap<?, ?> dispositionStatusMetricsMap = (LinkedHashMap<?, ?>) dispositionStatusMetric;
				// String dispositionStatus = (String) dispositionStatusMetricsMap.get("_id");
				Integer dispositionStatusCount = (Integer) dispositionStatusMetricsMap.get("count");
				if(dispositionStatusCount==null)
					dispositionStatusCount = 0;
				count = count + dispositionStatusCount;
				dispostionMap.put("callDuration", count);
			}
		}
		return dispostionMap;
	}

	private AggregationOperation buildBasicMatchTelco(List<String> campaignId, String agentId, Date fromDate,
			Date toDate) {
		List<Criteria> criterias = new ArrayList<Criteria>();
		Criteria criteria = Criteria.where("prospectCall.campaignId").in(campaignId);
		criterias.add(Criteria.where("prospectCall.agentId").in(agentId));
		criterias.add(Criteria.where("status").in("WRAPUP_COMPLETE"));
		criterias.add(Criteria.where("prospectCall.callStartTime").gte(fromDate).lte(toDate));
		return Aggregation.match(criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])));
	}

	private AggregationOperation buildGroupTelco() {
		BasicDBObject group = new BasicDBObject("_id", "handletime");
		group = group.append("count", new BasicDBObject("$sum", "$prospectCall.prospectHandleDuration"));
		return new CustomAggregationOperation(new Document("$group", group));
	}
}
