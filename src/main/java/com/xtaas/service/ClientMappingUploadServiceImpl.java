package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.domain.entity.Campaign;

@Service
public class ClientMappingUploadServiceImpl implements ClientMappingUploadService {

	@Autowired
	ClientMappingRepository clientMappingRepository;

	@Autowired
	CampaignRepository campaignRepository;

	private static final Logger logger = LoggerFactory.getLogger(ClientMappingUploadServiceImpl.class);

	private InputStream fileInputStream;
	
	private String fileName;

	private Sheet sheet; // this is the excel sheet to be read

	@Override
	public String createClientMappingEntries(String campaignId, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				Workbook book;
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					createClientMappingInDatabase(campaignId, sheet, book);
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}

	}

	private void createClientMappingInDatabase(String campaignId, Sheet sheet, Workbook book) throws IOException {
		Campaign campaignFromDB = campaignRepository.findOneById(campaignId);
		List<ClientMapping> clientMappings = new ArrayList<>();
		if (campaignFromDB != null) {
			clientMappings = clientMappingRepository.findAllByCampaignIdAndClientNameAndBySequenceSort(
					campaignFromDB.getId(), campaignFromDB.getClientName(),
					PageRequest.of(0, Integer.MAX_VALUE, Direction.ASC, "sequence"));
			if (clientMappings.size() > 0 || clientMappings != null) {
				for (ClientMapping clientMapping : clientMappings) {
					clientMappingRepository.deleteById(clientMapping.getId());
				}
			}
		}
		DataFormatter dataFormatter = new DataFormatter();
		int rowCounter = 0;
		//System.out.println("\n\n Iterating over Rows and Columns using for-each loop \n ");
		for (Row row : sheet) {
			if (rowCounter == 0) {
				rowCounter++;
				continue;
			}
			ClientMapping clientMapping = new ClientMapping();
			if (campaignId != null) {
				clientMapping.setCampaignId(campaignId);
			}
			else {
				throw new IllegalArgumentException("Campaign ID is required to upload client mapping file");
			}
			if (campaignFromDB.getClientName() != null) {
				clientMapping.setClientName(campaignFromDB.getClientName());
			}
			else {
				clientMapping.setClientName("");
			}
			Cell c0 = row.getCell(0);
			if (c0 == null || c0.getCellType() == CellType.BLANK) {
				//System.out.println(rowCounter + " ==> Excel column sequence Can't be empty");
			} else {
				String seq = dataFormatter.formatCellValue(c0).trim();
				clientMapping.setSequence(Integer.parseInt(seq.trim()));
			}
			Cell c1 = row.getCell(1);
			if (c1 == null || c1.getCellType() == CellType.BLANK) {
				//System.out.println(rowCounter + " ==> Excel Header Name Can't be empty");
			} else {
				String colhead = dataFormatter.formatCellValue(c1).trim();
				clientMapping.setColHeader(colhead.trim());
			}
			Cell c2 = row.getCell(2);
			if (c2 == null || c2.getCellType() == CellType.BLANK) {
				//System.out.println(rowCounter + " ==> StdAttribute Can't be empty");
			} else {
				String stdattrib = dataFormatter.formatCellValue(c2).trim();
				clientMapping.setStdAttribute(stdattrib.trim());
			}
			Cell c3 = row.getCell(3);
			if (c3 == null || c3.getCellType() == CellType.BLANK) {
				//System.out.println(rowCounter + " ==> Key Can't be empty");
			} else {
				String xkey = dataFormatter.formatCellValue(c3).trim();

				String tempStdAttrib = dataFormatter.formatCellValue(c2).trim();
				if(tempStdAttrib.equalsIgnoreCase("prospect.revenueRange")){
       			 Map<String,String[]> revMap = getRevenueMap();
       			 for (Map.Entry<String, String[]> entry : revMap.entrySet()) {
       				 if(entry.getKey().equalsIgnoreCase(xkey)){
       					 String[] keyArrrev = entry.getValue();
       					 clientMapping.setKey(keyArrrev[1]);
       				 }
       			 }
       			 
       		 }else if(tempStdAttrib.equalsIgnoreCase("prospect.employeeRange")){
       			 Map<String,String[]> empMap = getEmployeeMap();
       			 for (Map.Entry<String, String[]> entry : empMap.entrySet()) {
       				 if(entry.getKey().equalsIgnoreCase(xkey)){
       					 String[] keyArremp = entry.getValue();
       					 clientMapping.setKey(keyArremp[1]);
       				 }
       			 }
       		 }else{
       			 clientMapping.setKey(xkey.trim());
       		 }
			}
			Cell c4 = row.getCell(4);
			if (c4 == null || c4.getCellType() == CellType.BLANK) {
				clientMapping.setTitleMgmtLevel("");
			} else {
				String val = dataFormatter.formatCellValue(c4).trim();
				clientMapping.setTitleMgmtLevel(val.trim());
			}

			Cell c5 = row.getCell(5);
			if (c5 == null || c5.getCellType() == CellType.BLANK) {
				clientMapping.setValue("");
			} else {
				String val = dataFormatter.formatCellValue(c5).trim();
				clientMapping.setValue(val.trim());
			}
			Cell c6 = row.getCell(6);
			if (c6 == null || c6.getCellType() == CellType.BLANK) {
				clientMapping.setDefaultValue("");
			} else {
				String defval = dataFormatter.formatCellValue(c6).trim();
				clientMapping.setDefaultValue(defval.trim());
			}
			clientMappingRepository.save(clientMapping);
		}
		// Closing the workbook
		book.close();
	}

	private Map<String, String[]> getRevenueMap(){
		Map<String,String[]> revMap = new HashMap<String, String[]>();
	
		String[] revArray21 = new String[2];
		 revArray21[0]="0";
		 revArray21[1]="5000000";
		 revMap.put("Under $500K", revArray21);
		 
		 String[] revArray22 = new String[2];
		 revArray22[0]="0";
		 revArray22[1]="10000000";
		 revMap.put("$500K-$1M", revArray22);
		 
		 String[] revArray23 = new String[2];
		 revArray23[0]="0";
		 revArray23[1]="10000000";
		 revMap.put("$1M-$5M", revArray23);
		 
		
		 String[] revArray2 = new String[2];
		 revArray2[0]="5000000";
		 revArray2[1]="10000000";
		 revMap.put("$5M-$10M", revArray2);
		 
		 String[] revArray3 = new String[2];
		 revArray3[0]="10000000";
		 revArray3[1]="25000000";
		 revMap.put("$10M-$25M", revArray3);
		 
		 String[] revArray4 = new String[2];
		 revArray4[0]="25000000";
		 revArray4[1]="50000000";
		 revMap.put("$25M-$50M", revArray4);
		 
		 String[] revArray5 = new String[2];
		 revArray5[0]="50000000";
		 revArray5[1]="100000000";
		 revMap.put("$50M-$100M", revArray5);
		 
		 String[] revArray6 = new String[2];
		 revArray6[0]="100000000";
		 revArray6[1]="250000000";
		 revMap.put("$100M-$250M", revArray6);
		 
		 String[] revArray7 = new String[2];
		 revArray7[0]="250000000";
		 revArray7[1]="500000000";
		 revMap.put("$250M-$500M", revArray7);
		 
		 String[] revArray8 = new String[2];
		 revArray8[0]="500000000";
		 revArray8[1]="1000000000";
		 revMap.put("$500M-$1B", revArray8);
		 
		 String[] revArray9 = new String[2];
		 revArray9[0]="1000000000";
		 revArray9[1]="5000000000";
		 revMap.put("$1B-$5B", revArray9);
		 
		 String[] revArray10 = new String[2];
		 revArray10[0]="5000000000";
		 revArray10[1]="0";
		 revMap.put("Over $5B", revArray10);
		
		
		return revMap;
	}

	private Map<String, String[]> getEmployeeMap(){
		Map<String,String[]> empMap = new HashMap<String, String[]>();
		/////
		 String[] empArray1 = new String[2];
		 empArray1[0]="0";
		 empArray1[1]="5";
		 empMap.put("1-5", empArray1);
		
		String[] empArray2 = new String[2];
		empArray2[0]="5";
		empArray2[1]="10";
		empMap.put("5-10", empArray2);
		
		String[] empArray3 = new String[2];
		empArray3[0]="10";
		empArray3[1]="20";
		empMap.put("10-20", empArray3);
		
		String[] empArray4 = new String[2];
		empArray4[0]="20";
		empArray4[1]="50";
		empMap.put("20-50", empArray4);
		
		String[] empArray5 = new String[2];
		empArray5[0]="50";
		empArray5[1]="100";
		empMap.put("50-100", empArray5);
		
		String[] empArray6 = new String[2];
		empArray6[0]="100";
		empArray6[1]="250";
		empMap.put("100-250", empArray6);
		
		String[] empArray7 = new String[2];
		empArray7[0]="250";
		empArray7[1]="500";
		empMap.put("250-500", empArray7);
		
		String[] empArray8 = new String[2];
		empArray8[0]="500";
		empArray8[1]="1000";
		empMap.put("500-1,000", empArray8);
		
		String[] empArray9 = new String[2];
		empArray9[0]="1000";
		empArray9[1]="5000";
		empMap.put("1,000-5,000", empArray9);
		
		String[] empArray10 = new String[2];
		empArray10[0]="5000";
		empArray10[1]="10000";
		empMap.put("5,000-10,000", empArray10);
		
		String[] empArray11 = new String[2];
		empArray11[0]="10000";
		empArray11[1]="0";
		empMap.put("Over 10,000", empArray11);
		return empMap;
	}
	
	private Map<String, String> getDownloadRevenueMap(){
		Map<String,String> revMap = new HashMap<String, String>();
	
		 revMap.put("5000000", "Under $500K");

		 revMap.put("10000000","$500K-$1M");

		 revMap.put("10000000", "$1M-$5M");
	
		 revMap.put("10000000","$5M-$10M");

		 revMap.put("25000000","$10M-$25M");

		 revMap.put("50000000","$25M-$50M");

		 revMap.put("100000000","$50M-$100M");

		 revMap.put("250000000","$100M-$250M");

		 revMap.put("500000000","$250M-$500M");

		 revMap.put("1000000000","$500M-$1B");

		 revMap.put("5000000000","$1B-$5B");

		 revMap.put("0","Over $5B");
		
		
		return revMap;
	}

	private Map<String, String> getDownloadEmployeeMap(){
		Map<String,String> empMap = new HashMap<String, String>();

		 empMap.put("5","1-5");

		empMap.put("10","5-10");

		empMap.put("20","10-20");

		empMap.put("50","20-50");

		empMap.put("100","50-100");

		empMap.put("250","100-250");

		empMap.put("500","250-500");

		empMap.put("1000","500-1,000");

		empMap.put("5000","1,000-5,000");

		empMap.put("10000","5,000-10,000");

		empMap.put("0","Over 10,000");
	
		return empMap;
	}


	@Override
	public void downloadTemplate(String campaignId, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		if (campaignId == null || campaignId.equals("")) {
			throw new IllegalArgumentException("Campaign ID is required to download the client mapping file");
		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		String fileName = null;
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeaders(headerRow, workbook);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign != null) {
			List<ClientMapping> clientMappings = clientMappingRepository
					.findAllByCampaignIdAndClientNameAndBySequenceSort(campaign.getId(), campaign.getClientName(),
							PageRequest.of(0, Integer.MAX_VALUE, Direction.ASC, "sequence"));
			if (clientMappings.size() > 0) {
				for (ClientMapping clientMapping : clientMappings) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(clientMapping, row);
				}
			}
		}
		fileName = (campaign != null) ? campaign.getName() + "_mapping.xlsx" : "client_mapping.xlsx";
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	private void writeHeaders(Row headerRow, Workbook workbook) {
		CellStyle backgroundStyle = applyBackgroundColourToCell(workbook);
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Report Sequence");
		cell.setCellStyle(backgroundStyle);
		cell = headerRow.createCell(1);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("Report Column Header");
		cell = headerRow.createCell(2);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("DB Standard Attribute");
		cell = headerRow.createCell(3);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("XTaaS Key");
		cell.setCellStyle(backgroundStyle);
		cell = headerRow.createCell(4);
		cell.setCellValue("Title/Industry Extras");
		cell.setCellStyle(backgroundStyle);
		cell = headerRow.createCell(5);
		cell.setCellValue("Client Value");
		cell.setCellStyle(backgroundStyle);
		cell = headerRow.createCell(6);
		cell.setCellValue("Default Value");
		cell.setCellStyle(backgroundStyle);
	}

	private CellStyle applyBackgroundColourToCell(Workbook workbook) {
		CellStyle backgroundStyle = workbook.createCellStyle();
		backgroundStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		backgroundStyle.setBorderBottom(BorderStyle.THIN);
		backgroundStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderLeft(BorderStyle.THIN);
		backgroundStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderRight(BorderStyle.THIN);
		backgroundStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderTop(BorderStyle.THIN);
		backgroundStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return backgroundStyle;
	}

	private void writeToExcel(ClientMapping clientMapping, Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue(clientMapping.getSequence());
		cell = row.createCell(1);
		cell.setCellValue(clientMapping.getColHeader());
		cell = row.createCell(2);
		cell.setCellValue(clientMapping.getStdAttribute());
		if(clientMapping.getStdAttribute() != null && clientMapping.getStdAttribute().equalsIgnoreCase("prospect.revenueRange")){
			cell = row.createCell(3);
			Map<String, String> revMap = getDownloadRevenueMap();
			cell.setCellValue(revMap.get(clientMapping.getKey()));
		}else if(clientMapping.getStdAttribute() != null && clientMapping.getStdAttribute().equalsIgnoreCase("prospect.employeeRange")){
			cell = row.createCell(3);
			Map<String, String> empMap = getDownloadEmployeeMap();
			cell.setCellValue(empMap.get(clientMapping.getKey()));
		}else{
			cell = row.createCell(3);
			cell.setCellValue(clientMapping.getKey());
		}
		cell = row.createCell(4);
		cell.setCellValue(clientMapping.getTitleMgmtLevel());
		cell = row.createCell(5);
		cell.setCellValue(clientMapping.getValue());
		cell = row.createCell(6);
		cell.setCellValue(clientMapping.getDefaultValue());
	}

}