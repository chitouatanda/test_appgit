package com.xtaas.service;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.CampaignDNorm;
import com.xtaas.db.repository.CampaignDNormRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.web.dto.CampaignSettingDTO;
import com.xtaas.worker.CallMaxRetryGetBackThread;

@Service
public class CampaignSettingServiceImpl implements CampaignSettingService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignSettingServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private CampaignDNormRepository campaignDNormRepository;

	@Autowired
	private RestClientService restClientService;

	@Override
	public void updateCampaignSettings(String campaignId, CampaignSettingDTO campaignSettingDTO,
			HttpServletRequest httpServletRequest) {
		Campaign campaignFromDB = campaignRepository.findOneById(campaignId);
		logger.debug("updateCampaignSettings() : Updating campaign settings for [{}] campaign.",
				campaignFromDB.getName());
		// The cache base url will be as per campaign hosting server url. e.g. if hosting server is "xtaascorp" then that instance cache will be cleared. 
		String hostingServerUrl = campaignFromDB.getHostingServer();
		// if (hostingServerUrl == null || hostingServerUrl.isEmpty() || hostingServerUrl == "") {
		// 	throw new IllegalArgumentException("Hosting server url cannot be empty in campaign.");
		// }
		if (campaignFromDB != null) {
			updateCampaign(campaignFromDB, campaignSettingDTO, httpServletRequest, hostingServerUrl);
			createCampaignDnormEntry(campaignFromDB);
			callCacheRefreshUrls(campaignFromDB.getId(), httpServletRequest, hostingServerUrl);
		}
	}

	private void updateCampaign(Campaign campaignFromDB, CampaignSettingDTO campaignSettingDTO,
			HttpServletRequest httpServletRequest, String hostingServerUrl) {
		campaignFromDB.setSipProvider(campaignSettingDTO.getSipProvider());
		campaignFromDB.setHidePhone(campaignSettingDTO.getHidePhone());
		campaignFromDB.setEmailSuppressed(campaignSettingDTO.isEmailSuppressed());
		campaignFromDB.setQaRequired(campaignSettingDTO.getQaRequired());
		campaignFromDB.getTeam().setLeadSortOrder(campaignSettingDTO.getLeadSortOrder());
		campaignFromDB.getTeam().setCallSpeedPerMinPerAgent(campaignSettingDTO.getCallSpeedPerMinPerAgent());
		campaignFromDB.setRetrySpeedEnabled(campaignSettingDTO.getRetrySpeedEnabled());
		campaignFromDB.setSimpleDisposition(campaignSettingDTO.getSimpleDisposition());
		if (campaignSettingDTO.getCampaignType().equalsIgnoreCase("LeadGeneration")) {
			campaignFromDB.setType(CampaignTypes.LeadGeneration);
		} else if (campaignSettingDTO.getCampaignType().equalsIgnoreCase("LeadVerification")) {
			campaignFromDB.setType(CampaignTypes.LeadVerification);
		} else if (campaignSettingDTO.getCampaignType().equalsIgnoreCase("LeadQualification")) {
			campaignFromDB.setType(CampaignTypes.LeadQualification);
		} else if (campaignSettingDTO.getCampaignType().equalsIgnoreCase("TeleMailCampaign")) {
			campaignFromDB.setType(CampaignTypes.TeleMailCampaign);
		} else if (campaignSettingDTO.getCampaignType().equalsIgnoreCase("Prospecting")) {
			campaignFromDB.setType(CampaignTypes.Prospecting);
		}
		
// DATE:- 25/12/19 :- Commented below code as voice platform option removed from Admin setting UI 
// If voiceplatform setting changed then only call refreshAgentsCampaignCache() method.
//		String voicePlatform = "";
//		if (campaignFromDB.isUsePlivo()) {
//			voicePlatform = "PLIVO";
//		} else {
//			voicePlatform = "TWILIO";
//		}
//		if (!campaignSettingDTO.getVoicePlatform().equalsIgnoreCase(voicePlatform)) {
//			try {
//				refreshAgentsCampaignCache(campaignFromDB.getId(), httpServletRequest, hostingServerUrl);
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.error(
//						"Error occurred while refreshing agents campaign cache of main application. Exception is {}",
//						e);
//			}
//		}
//		if (campaignSettingDTO.getVoicePlatform().equalsIgnoreCase("PLIVO")) {
//			campaignFromDB.setUsePlivo(true);
//	    } else if (campaignSettingDTO.getVoicePlatform().equalsIgnoreCase("TWILIO")) {
//			campaignFromDB.setUsePlivo(false);
//	    }
		campaignFromDB.setEnableMachineAnsweredDetection(campaignSettingDTO.getEnableMachineAnsweredDetection());
		if (campaignFromDB.getCallMaxRetries() < campaignSettingDTO.getCallMaxRetries()) {
			CallMaxRetryGetBackThread lrt = new CallMaxRetryGetBackThread(campaignFromDB.getId(),
					campaignFromDB.getCallMaxRetries(), campaignSettingDTO.getCallMaxRetries());
			Thread t1 = new Thread(lrt, "t" + Math.random());
			t1.start();
			// prospectCallLogServiceImpl.getBackProspectsFromMaxRetries(campaignId,campaignFromDB.getCallMaxRetries(),campaignSettingDTO.getCallMaxRetries());
		}
		campaignFromDB.setCallMaxRetries(campaignSettingDTO.getCallMaxRetries());
		campaignFromDB.setDailyCallMaxRetries(campaignSettingDTO.getDailyCallMaxRetries());
		campaignFromDB.setLocalOutboundCalling(campaignSettingDTO.getLocalOutboundCalling());
		campaignFromDB.setDialerMode(campaignSettingDTO.getDialerMode());
		if (campaignSettingDTO.getDialerMode() != null && campaignSettingDTO.getDialerMode().equals(DialerMode.PREVIEW)) {
			campaignFromDB.setRetrySpeedEnabled(false);
		} else {
			campaignFromDB.setRetrySpeedEnabled(true);
		}
		if (campaignSettingDTO.getRetrySpeed() != null && !campaignSettingDTO.getRetrySpeed().isEmpty()) {
			campaignFromDB.setRetrySpeed(campaignSettingDTO.getRetrySpeed());
		}
		campaignFromDB.setRetrySpeedFactor(campaignSettingDTO.getRetrySpeedFactor());
		campaignRepository.save(campaignFromDB);
		logger.debug("Campaign settings updated successfully having campaign name : " + campaignFromDB.getName());
	}

	private void createCampaignDnormEntry(Campaign campaignFromDB) {
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaignFromDB.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaignFromDB, cdn);
		campaignDNormRepository.save(cdn);
		logger.debug("Campaign dnorm entry created successfully for campaign having name : " + campaignFromDB.getName());
	}

	private void callCacheRefreshUrls(String campaignId, HttpServletRequest httpServletRequest,
			String hostingServerUrl) {
		try {
			refreshCampaignCacheofDialer(campaignId, httpServletRequest, hostingServerUrl);
			refreshCampaignCacheofMainApp(campaignId, httpServletRequest, hostingServerUrl);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occurred while refreshing campaign cache. Exception is {}", e);
		}
	}

	/**
	 * Empty/Refresh in memory cache of given campaign of dialer application.
	 * 
	 * @param campaignId
	 * @param httpServletRequest
	 */

	@Async
	private void refreshCampaignCacheofDialer(String campaignId, HttpServletRequest httpServletRequest,
			String hostingServerUrl) {
		if (hostingServerUrl != null && !hostingServerUrl.isEmpty()) {
			StringBuilder campaignsRefreshRestUrl = new StringBuilder();
			// String url = httpServletRequest.getRequestURL().toString();
			if (hostingServerUrl.contains("xtaascorp")) {
				campaignsRefreshRestUrl.append("https://xtaasdialer.herokuapp.com/api/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("xtaascorp2")) {
				campaignsRefreshRestUrl.append("https://xtaascorp1.herokuapp.com/api/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("demandshore")) {
				campaignsRefreshRestUrl.append("https://demandshoredialer.herokuapp.com/api/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("peaceful")) {
				campaignsRefreshRestUrl.append("https://peaceful-dialer.herokuapp.com/api/cacherefresh/campaign/")
						.append(campaignId);
			}
			HashMap<String, String> headers = new HashMap<String, String>();
			restClientService.makeGetRequest(campaignsRefreshRestUrl.toString(), headers);
			logger.debug("refreshed campaign cache of dialer having campaignId : [{}]", campaignId);
		}		
	}

	/**
	 * Empty/Refresh in memory cache of given campaign of main XTaaS application.
	 * 
	 * @param campaignId
	 * @param httpServletRequest
	 */
	@Async
	private void refreshCampaignCacheofMainApp(String campaignId, HttpServletRequest httpServletRequest,
			String hostingServerUrl) {
		if (hostingServerUrl != null && !hostingServerUrl.isEmpty()) {
			StringBuilder campaignsRefreshRestUrl = new StringBuilder();
			// String url = httpServletRequest.getRequestURL().toString();
			if (hostingServerUrl.contains("xtaascorp")) {
				campaignsRefreshRestUrl.append("https://xtaascorp.herokuapp.com/spr/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("xtaascorp2")) {
				campaignsRefreshRestUrl.append("https://xtaascorp2.herokuapp.com/spr/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("demandshore")) {
				campaignsRefreshRestUrl.append("https://demandshore.herokuapp.com/spr/cacherefresh/campaign/")
						.append(campaignId);
			} else if (hostingServerUrl.contains("peaceful")) {
				campaignsRefreshRestUrl.append("https://peaceful-castle-6000.herokuapp.com/spr/cacherefresh/campaign/")
						.append(campaignId);
			}
			HashMap<String, String> headers = new HashMap<String, String>();
			restClientService.makeGetRequest(campaignsRefreshRestUrl.toString(), headers);
			logger.debug("refreshed Campaign Cache of main app having campaignId : [{}]", campaignId);
		}		
	}

	/**
	 * Notifies the agents of given assigned campaign with message as "campaign
	 * settings has been changed.Please relogin".
	 * 
	 * @param campaignId
	 * @param httpServletRequest
	 */
	@Async
	private void refreshAgentsCampaignCache(String campaignId, HttpServletRequest httpServletRequest,
			String hostingServerUrl) {
		StringBuilder campaignsRefreshRestUrl = new StringBuilder();
		// String url = httpServletRequest.getRequestURL().toString();
		if (hostingServerUrl.contains("xtaascorp")) {
			campaignsRefreshRestUrl.append("https://xtaascorp.herokuapp.com/spr/cacherefresh/resetagentcampaign/")
					.append(campaignId);
		} else if (hostingServerUrl.contains("xtaascorp2")) {
			campaignsRefreshRestUrl.append("https://xtaascorp2.herokuapp.com/spr/cacherefresh/resetagentcampaign/")
					.append(campaignId);
		} else if (hostingServerUrl.contains("demandshore")) {
			campaignsRefreshRestUrl.append("https://demandshore.herokuapp.com/spr/cacherefresh/resetagentcampaign/")
					.append(campaignId);
		} else if (hostingServerUrl.contains("peaceful")) {
			campaignsRefreshRestUrl
					.append("https://peaceful-castle-6000.herokuapp.com/spr/cacherefresh/resetagentcampaign/")
					.append(campaignId);
		}
		HashMap<String, String> headers = new HashMap<String, String>();
		restClientService.makeGetRequest(campaignsRefreshRestUrl.toString(), headers);
		logger.debug("refreshed agents campaign cache of main app having campaignId : [{}]", campaignId);
	}

}
