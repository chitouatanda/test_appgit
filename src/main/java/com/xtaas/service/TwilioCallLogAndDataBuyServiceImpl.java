package com.xtaas.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.plivo.api.models.call.LiveCallListGetter;
import com.plivo.api.models.call.QueuedCallListGetter;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.AvailablePhoneNumber;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Recording;
import com.twilio.sdk.resource.list.AvailablePhoneNumberList;
import com.twilio.sdk.resource.list.CallList;
import com.twilio.sdk.resource.list.RecordingList;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.TwilioCallLogFilterDTO;
import com.xtaas.web.dto.TwilioNumberBuyDTO;

@Service
public class TwilioCallLogAndDataBuyServiceImpl implements TwilioCallLogAndDataBuyService {

	private static final Logger logger = LoggerFactory.getLogger(TwilioCallLogAndDataBuyServiceImpl.class);
	private static final String MASTER_ACCOUNT_SID = "ACab4d611aa1465a246cb58290ba930a5c";
	private static final String MASTER_AUTH_TOKEN = "8096913a4808bc8287d5a7b0b8332d22";
	private static final String PROD_ACCOUNT_SID = "AC2a76c665eb3c39046214a9a6a2bd3935";
	private static final String PROD_AUTH_TOKEN = "2fc001a00ad725a9616bf4a6d5132ffc";
	private static final String DMS_ACCOUNT_SID = "AC517a9586ed1afe81d4fda44f90778d53";
	private static final String DMS_AUTH_TOKEN = "e9eda3a5a24028435e472992be403dc1";
	private static final String QA_ACCOUNT_SID = "ACbf52b47c38d9b03c72360c748b90309e";
	private static final String QA_AUTH_TOKEN = "25bb8b444f7befe0dfc9effd9a6c28bd";
	private static final String BUCKET_NAME = "twiliocalllog";
	private static final String FOLDER_NAME = "twiliocalllog";
	private static final String DM_PROFILE = "DEMANDSHORE";
	private static final String MASTER_PROFILE = "MASTER";
	private static final String PROD_PROFILE = "PROD";
	private static final String QA_PROFILE = "QA";
	private static final String SUFFIX = "/";
	private AmazonS3 s3client;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Override
	@Async
	public void generateCallLog(TwilioCallLogFilterDTO filterDTO, boolean showPrice) {
		logger.info("Started Twilio call logs fetch API for [{}]", filterDTO.toString());
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeaders(headerRow, showPrice);

		TwilioRestClient client = null;
		Account account = null;
		if (filterDTO.getProfile() != null && !filterDTO.getProfile().isEmpty()) {
			if (filterDTO.getProfile().equalsIgnoreCase(MASTER_PROFILE)) {
				client = new TwilioRestClient(MASTER_ACCOUNT_SID, MASTER_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(PROD_PROFILE)) {
				client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
				client = new TwilioRestClient(DMS_ACCOUNT_SID, DMS_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(QA_PROFILE)) {
				client = new TwilioRestClient(QA_ACCOUNT_SID, QA_AUTH_TOKEN);
				account = client.getAccount();
			}
		} else {
			client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
			account = client.getAccount();
		}

		Map<String, String> filter = new HashMap<String, String>();
		if (filterDTO.getStartDate() != null && !filterDTO.getStartDate().isEmpty()) {
			filter.put("StartTime>", filterDTO.getStartDate());
		}
		if (filterDTO.getEndDate() != null && !filterDTO.getEndDate().isEmpty()) {
			filter.put("StartTime<", filterDTO.getEndDate());
		}
		if (filterDTO.getFromNumber() != null && !filterDTO.getFromNumber().isEmpty()) {
			filter.put("From", filterDTO.getFromNumber());
		}
		if (filterDTO.getToNumber() != null && !filterDTO.getToNumber().isEmpty()) {
			filter.put("To", filterDTO.getToNumber());
		}
		if (filterDTO.getStatus() != null && !filterDTO.getStatus().isEmpty()) {
			filter.put("Status", filterDTO.getStatus());
		}
		if (filterDTO.getCallSid() != null && !filterDTO.getCallSid().isEmpty()) {
			filter.put("CallSid", filterDTO.getCallSid());
		}
		filter.put("PageSize", new Integer(1000).toString());
		/*
		 * STATUS (possible values) : queued, ringing, in-progress, canceled, completed,
		 * failed, busy or no-answer
		 */
		try {
			CallList callList = account.getCalls(filter);
			for (Call call : callList) {
				if (call.getFrom() != null && !call.getFrom().startsWith("client") && call.getDirection() != null
						&& call.getDirection().contains(filterDTO.getDirection())) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(account, call, row, workbook, filterDTO.getProfile(), showPrice);
				}
			}
		} catch (Exception e) {
			logger.error("Error occured in TwilioServiceImpl. Exception is [{}]", e);
		}
		AmazonS3 s3client = createAWSConnection(XtaasConstants.CLIENT_REGION);
		FileOutputStream outputStream = null;
		try {
			File dirFile = new File("twiliocalllog");
			if (!dirFile.exists()) {
				if (dirFile.mkdirs()) {
					System.out.println("Directory is created!");
				} else {
					System.out.println("Failed to create directory!");
				}
			}
			String fileName = "incoming_voicemail.xlsx";
			File rfile = new File(FOLDER_NAME + SUFFIX + fileName);
			outputStream = new FileOutputStream(rfile);
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
			List<File> filePaths = new ArrayList<File>();
			filePaths.add(rfile);
			// send mail to delivery team.
			sendTwilioCallLogEmail(filterDTO, filePaths);
			s3client.putObject(new PutObjectRequest(BUCKET_NAME, FOLDER_NAME + SUFFIX + fileName, rfile)
					.withCannedAcl(CannedAccessControlList.PublicRead));
			rfile.delete();
			logger.info("Finished Twilio call logs fetch API for [{}]", filterDTO.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeHeaders(Row headerRow, boolean showPrice) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("CallSid");
		cell = headerRow.createCell(1);
		cell.setCellValue("From");
		cell = headerRow.createCell(2);
		cell.setCellValue("To");
		cell = headerRow.createCell(3);
		cell.setCellValue("Duration");
		cell = headerRow.createCell(4);
		cell.setCellValue("Direction");
		cell = headerRow.createCell(5);
		cell.setCellValue("Start Time");
		cell = headerRow.createCell(6);
		cell.setCellValue("End Time");
		cell = headerRow.createCell(7);
		cell.setCellValue("Status");
		cell = headerRow.createCell(8);
		cell.setCellValue("Recording URL");
		if (showPrice) {
			cell = headerRow.createCell(9);
			cell.setCellValue("Price");
		}
	}

	private void writeToExcel(Account account, Call call, Row row, Workbook workbook, String profile,
			boolean showPrice) {
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));

		Cell cell = row.createCell(0);
		cell.setCellValue(call.getSid());
		cell = row.createCell(1);
		cell.setCellValue(call.getFrom());
		cell = row.createCell(2);
		cell.setCellValue(call.getTo());
		cell = row.createCell(3);
		cell.setCellValue(call.getDuration());
		cell = row.createCell(4);
		cell.setCellValue(call.getDirection());
		cell = row.createCell(5);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(call.getStartTime());
		cell = row.createCell(6);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(call.getEndTime());
		cell = row.createCell(7);
		cell.setCellValue(call.getStatus());
		cell = row.createCell(8);
		RecordingList recordingList = call.getRecordings();
		List<Recording> recordings = recordingList.getPageData();
		if (recordings != null && recordings.size() > 0) {
			/* Set Twilio recording url. */
			/*
			 * String recordingUrl = "https://api.twilio.com" +
			 * StringUtils.replace(recordings.get(0).getProperty("uri"), ".json", "");
			 * cell.setCellValue(recordingUrl);
			 */
			/* Below code uploads each recording url to AWS S3 bucket. */
			
			String recordingUrl = "https://api.twilio.com" + recordings.get(0).getProperty("uri").replace(".json", "");
			InputStream inputStream = null;
			try {
				inputStream = new URL(recordingUrl).openStream();
				String fileName = recordingUrl.substring(recordingUrl.length() - 34, recordingUrl.length());
				fileName = fileName + ".wav";
				downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream,
						true);
				cell.setCellValue(createAwsUrl(recordingUrl));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			 
			/* Set S3 bucket AWS recording url. */
//			String recordingUrl = recordings.get(0).getProperty("uri").replace(".json", "");
//			int lastIndex = recordingUrl.lastIndexOf("/");
//			String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
//			String rfileName = fileName + ".wav";
//			String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
//			cell.setCellValue(awsRecordingUrl);
		} else {
			cell.setCellValue("");
		}
		if (showPrice) {
			cell = row.createCell(9);
			cell.setCellValue(call.getPrice());
		}
	}
	
	private String createAwsUrl(String recordingUrl) {
		int lastIndex = recordingUrl.lastIndexOf("/");
		String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
		String rfileName = fileName + ".wav";
		String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
		return awsRecordingUrl;
	}

	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		try {
			if (s3client == null) {
				BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_ACCESS_KEY_ID,
						XtaasConstants.AWS_SECRET_KEY);
				s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
						.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			}
		} catch (Exception ex) {
			logger.error("Error while creating the connection with AWS stroage : {}", ex);
		}
		return s3client;
	}

	private void sendTwilioCallLogEmail(TwilioCallLogFilterDTO filterDTO, List<File> filePaths) {
		logger.info("Sending twilio incoming voicemail : To-{}, Profile-[{}]", filterDTO.getToEmails(),
				filterDTO.getProfile());
		String subject = "Incoming VM " + filterDTO.getStartDate();
		if (filterDTO.getProfile().equalsIgnoreCase(MASTER_PROFILE)) {
			subject = "XTaaS Master Incoming VM " + filterDTO.getStartDate();
		} else if (filterDTO.getProfile().equalsIgnoreCase(PROD_PROFILE)) {
			subject = "XTaaS Incoming VM " + filterDTO.getStartDate();
		} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
			subject = "Demandshore Incoming VM " + filterDTO.getStartDate();
		} else if (filterDTO.getProfile().equalsIgnoreCase(QA_PROFILE)) {
			subject = "XTaaS QA Incoming VM " + filterDTO.getStartDate();
		}
		for (String toEmail : filterDTO.getToEmails()) {
			String message = "Please find attachment for incoming voicemail.";
			XtaasEmailUtils.sendEmail(toEmail, filterDTO.getFromEmail(), subject, message, filePaths);
		}
	}

	@Override
	@Async
	public void purchaseNumbers(TwilioNumberBuyDTO filterDTO) {
		logger.info("API request to purchase phone numbers for ", filterDTO.toString());
		TwilioRestClient client = null;
		Account account = null;
		if (filterDTO.getProfile() != null && !filterDTO.getProfile().isEmpty()) {
			if (filterDTO.getProfile().equalsIgnoreCase(MASTER_PROFILE)) {
				client = new TwilioRestClient(MASTER_ACCOUNT_SID, MASTER_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(PROD_PROFILE)) {
				client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
				client = new TwilioRestClient(DMS_ACCOUNT_SID, DMS_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(QA_PROFILE)) {
				client = new TwilioRestClient(QA_ACCOUNT_SID, QA_AUTH_TOKEN);
				account = client.getAccount();
			}
		} else {
			client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
			account = client.getAccount();
		}
		Map<String, String> filter = new HashMap<String, String>();
		String isoCountry = "US";
		if (filterDTO.getIsoCountry() != null && !filterDTO.getIsoCountry().isEmpty()) {
			isoCountry = filterDTO.getIsoCountry();
		}
		if (filterDTO.getVoiceEnabled() != null) {
			filter.put("VoiceEnabled", filterDTO.getVoiceEnabled().toString());
		}
		if (filterDTO.getSmsEnabled() != null) {
			filter.put("SmsEnabled", filterDTO.getSmsEnabled().toString());
		}
		if (filterDTO.getMmsEnabled() != null) {
			filter.put("MmsEnabled", filterDTO.getMmsEnabled().toString());
		}
		if (filterDTO.getFaxEnabled() != null) {
			filter.put("FaxEnabled", filterDTO.getFaxEnabled().toString());
		}
		if (filterDTO.getExcludeAllAddressRequired() != null) {
			filter.put("ExcludeAllAddressRequired", filterDTO.getExcludeAllAddressRequired().toString());
		}
		if (filterDTO.getExcludeForeignAddressRequired() != null) {
			filter.put("ExcludeForeignAddressRequired", filterDTO.getExcludeForeignAddressRequired().toString());
		}
		if (filterDTO.getExcludeLocalAddressRequired() != null) {
			filter.put("ExcludeLocalAddressRequired", filterDTO.getExcludeLocalAddressRequired().toString());
		}
		List<String> numbersPurchased = new ArrayList<>();
		if (filterDTO.getAreaCodes() != null && filterDTO.getAreaCodes().size() > 0) {
			for (String areaCode : filterDTO.getAreaCodes()) {
				if (areaCode != null && !areaCode.isEmpty() && areaCode.length() == 3) {
					filter.put("AreaCode", areaCode);
					AvailablePhoneNumberList availablePhoneNumberList = account.getAvailablePhoneNumbers(filter,
							isoCountry, "Local");
					List<AvailablePhoneNumber> availablePhoneNumbers = availablePhoneNumberList.getPageData();
					if (availablePhoneNumbers != null && availablePhoneNumbers.size() > 0) {
						logger.info("Found [] numbers for [{}]", availablePhoneNumbers.size(), areaCode);
						if (filterDTO.getBuyCountPerAreaCode() > 0) {
							for (int i = 0; i < filterDTO.getBuyCountPerAreaCode(); i++) {
								AvailablePhoneNumber availablePhoneNumber = availablePhoneNumbers.get(i);
								if (availablePhoneNumber != null) {
									// Purchase the number on the list.
									// List<NameValuePair> purchaseParams = new ArrayList<NameValuePair>();
									// purchaseParams.add(new BasicNameValuePair("PhoneNumber",
									// availablePhoneNumber.getPhoneNumber()));
									// try {
									// account.getIncomingPhoneNumberFactory().create(purchaseParams);
									// numbersPurchased.add(availablePhoneNumber.getPhoneNumber());
									// } catch (TwilioRestException e) {
									// logger.error("Error occured while phone number purchase. Exception is [{}]",
									// e);
									// }
									numbersPurchased.add(availablePhoneNumber.getPhoneNumber());
								}
							}
						}
					} else {
						logger.info("Found [] numbers for [{}]", 0, areaCode);
					}
				}
			}
		}
		sendNumberPurchasedEmail(filterDTO, numbersPurchased);
	}

	private void sendNumberPurchasedEmail(TwilioNumberBuyDTO filterDTO, List<String> numbersPurchased) {
		List<String> to = Arrays.asList("kchugh@xtaascorp.com", "hbaba@xtaascorp.com", "mpanjiar@xtaascorp.com");
		// List<String> to = Arrays.asList("pranay.lonkar@invimatic.com");
		String from = "kchugh@xtaascorp.com";
		String subject = "Twilio Number Purchased";
		String message = "Twilio phone number purchase request for [" + filterDTO.toString()
				+ "]. Following twilio numbers are purchased : " + numbersPurchased.toString();
		XtaasEmailUtils.sendEmail(to, from, subject, message);
	}

	@Override
	public Map<String, Integer> getCallStats(TwilioCallLogFilterDTO filterDTO) {
		logger.info("Started Twilio call stats for [{}]", filterDTO.toString());
		TwilioRestClient client = null;
		Account account = null;
		Map<String, Integer> map = new HashMap<>();
		if (filterDTO.getProfile() != null && !filterDTO.getProfile().isEmpty()) {
			if (filterDTO.getProfile().equalsIgnoreCase(MASTER_PROFILE)) {
				client = new TwilioRestClient(MASTER_ACCOUNT_SID, MASTER_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(PROD_PROFILE)) {
				client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
				client = new TwilioRestClient(DMS_ACCOUNT_SID, DMS_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(QA_PROFILE)) {
				client = new TwilioRestClient(QA_ACCOUNT_SID, QA_AUTH_TOKEN);
				account = client.getAccount();
			}
		} else {
			client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
			account = client.getAccount();
		}

		Map<String, String> filter = new HashMap<String, String>();
		if (filterDTO.getStartDate() != null && !filterDTO.getStartDate().isEmpty()) {
			filter.put("StartTime>", filterDTO.getStartDate());
		}
		if (filterDTO.getEndDate() != null && !filterDTO.getEndDate().isEmpty()) {
			filter.put("StartTime<", filterDTO.getEndDate());
		}
		filter.put("Status", "queued");
		filter.put("PageSize", new Integer(5000).toString());
		/*
		 * STATUS (possible values) : queued, ringing, in-progress, canceled, completed,
		 * failed, busy or no-answer
		 */
		try {
			// get queued calls count
			CallList callList = account.getCalls(filter);
			int queuedCallCount = callList.getPageData().size();
			// get live calls count
			filter.put("Status", "in-progress");
			callList = account.getCalls(filter);
			int liveCallCount = callList.getPageData().size();
			map.put("QUEUED", queuedCallCount);
			map.put("LIVE", liveCallCount);
		} catch (Exception e) {
			logger.error("Error occured in TwilioServiceImpl. Exception is [{}]", e);
		}
		return map;
	}

}
