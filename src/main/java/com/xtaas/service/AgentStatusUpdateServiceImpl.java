package com.xtaas.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import com.mongodb.BasicDBObject;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.xtaas.db.entity.AgentStatusLog;
import com.xtaas.db.repository.AgentStatusLogRepository;
import com.xtaas.utils.XtaasDateUtils;

@Service
public class AgentStatusUpdateServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(AgentStatusUpdateServiceImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private AgentStatusLogRepository agentStatusLogRepository;
	
	@Autowired
	private DomoUtilsService domoUtilsService;

	public void updateAgentStatusLogCollection() {
		List<String> agentIds = getDistinctAgentIdsForToday();
		if (agentIds != null && agentIds.size() > 0) {
			updateRecords(agentIds);
		}
	}

	@SuppressWarnings("unchecked")
	private List<String> getDistinctAgentIdsForToday() {
		BasicDBObject elemMatch = new BasicDBObject();
		elemMatch.put("updatedDate", new BasicDBObject("$gte", XtaasDateUtils.getDateAtTime(new Date(), 0, 0, 0, 0)));
		elemMatch.put("updatedDate",
				new BasicDBObject("$lte", XtaasDateUtils.getDateAtTime(new Date(), 23, 59, 59, 0)));
		DistinctIterable<String> iterable = mongoTemplate.getCollection("agentstatuslog").distinct("agentId", elemMatch, String.class);
		
		List<String> agentIds = new ArrayList<>();
		MongoCursor<String> cursor = iterable.iterator();
		while (cursor.hasNext()) {
			agentIds.add(cursor.next());
		} 
		
		return agentIds;
	}

	private void updateRecords(List<String> agents) {
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		for (String agentId : agents) {
			List<AgentStatusLog> agentLatestRecord = agentStatusLogRepository.getLastStatus(agentId,
					pageable);
			if (agentLatestRecord != null) {
				long agentCreatedTimeInSeconds = XtaasDateUtils
						.getTimeInSeconds(agentLatestRecord.get(0).getCreatedDate());
				long currentTimeInSeconds = XtaasDateUtils.getTimeInSeconds(new Date());
				long timeDiffrence = currentTimeInSeconds - agentCreatedTimeInSeconds;
				agentLatestRecord.get(0).setStateDuration(agentLatestRecord.get(0).getStateDuration() + timeDiffrence);
				agentStatusLogRepository.saveAll(agentLatestRecord);
				domoUtilsService.postAgentStatusLogWebhook(agentLatestRecord.get(0));

				logger.debug("updateRecords() :: Successfully updated record in agentstatuslog collection of agentId: "
						+ agentId);
			}
		}
	}
}