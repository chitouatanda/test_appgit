package com.xtaas.service;

import com.google.common.util.concurrent.RateLimiter;
import com.xtaas.BeanLocator;
import com.xtaas.ml.transcribe.service.TrafficManager;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.worker.DataBuyThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class DataBuyRateLimiterService  implements TrafficManager {

    private static final Logger logger = LoggerFactory.getLogger(DataBuyRateLimiterService.class);

    @Autowired
    private PropertyService propertyService;

    DecimalFormat df=new DecimalFormat("#.##");

    @Override
    public void acquire(String serviceName) {
         boolean isRateLimitAllow = propertyService.getBooleanApplicationPropertyValue(
                 XtaasConstants.APPLICATION_PROPERTY.DATA_BUY_RATE_LIMITER.name(), false);
         if(isRateLimitAllow){
             DataBuyThread dataBuyThreadTask = BeanLocator.getBean("dataBuyThread", DataBuyThread.class);
             RateLimiter rateLimiter = dataBuyThreadTask.rateLimiter;
             if(rateLimiter != null) {
                 try {
                     double rate = 1000/ rateLimiter.getRate();
                     String rateStr =  df.format(rate);
                     long start = System.currentTimeMillis();
                     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                     Date date = new Date(start);
                     String currentTime =  sdf.format(date);
                     rateLimiter.acquire();
                     logger.info("Zoom API call time [{}] . Next API call time should be equal or greater then [{}] milli second. Service name [{}] " ,currentTime, rateStr,serviceName );
                   /*  long end = System.currentTimeMillis();
                     double  totalTimeInSec= (end - start)/1000;
                     double totalTimeInMiliSec = end - start;
                     logger.info("Wait time to call the Zoom Api. Wait time in Seconds : [{}] and Wait time in MilliSeconds [{}] . Start time [{}] and End time : [{}] . Rate Limit : [{}] . Service name [{}]"
                             ,totalTimeInSec,totalTimeInMiliSec,start,end,rateLimiter.getRate() , serviceName);*/
                 }catch (Exception e){
                     logger.error("Error while rate limit. Exception [{}] ", e);
                 }


             }
         }

    }

    @Override
    public void acquire() {

    }

    @Override
    public void release() {

    }

    @Override
    public String getServiceProvider() {
        return null;
    }

    private String getTime(Long time){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        Date date = new Date(time);
        return simpleDateFormat.format(date);
    }
}
