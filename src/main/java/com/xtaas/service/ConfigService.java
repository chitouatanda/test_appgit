package com.xtaas.service;

import java.util.HashMap;

import com.xtaas.db.entity.OrgTypeExperienceMap;
import com.xtaas.db.entity.StateCallConfig;

/**
 * @author djain
 *
 */
public interface ConfigService {
	
	public void addStateCallConfig (StateCallConfig stateCallConfig);
	
	public StateCallConfig getStateCallConfig(String stateCode);
	
	public OrgTypeExperienceMap getOrgTypeExperienceMap(String organizationType);
	
}
