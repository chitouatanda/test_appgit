package com.xtaas.service;

import com.xtaas.web.dto.ProspectCallDTO;

public interface CampaignTypeProspectingService {

	public ProspectCallDTO createProspectingLead(ProspectCallDTO prospectCallDTO);

}