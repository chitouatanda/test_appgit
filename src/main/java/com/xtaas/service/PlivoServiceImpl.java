package com.xtaas.service;

import java.io.IOException;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.plivo.api.exceptions.PlivoRestException;
import com.plivo.api.exceptions.PlivoXmlException;
import com.plivo.api.models.base.ListResponse;
import com.plivo.api.models.call.Call;
import com.plivo.api.models.call.LegSpecifier;
import com.plivo.api.models.call.LiveCallListGetter;
import com.plivo.api.models.call.QueuedCallListGetter;
import com.plivo.api.models.call.actions.CallRecordCreateResponse;
import com.plivo.api.models.conference.ConferenceRecordCreateResponse;
import com.plivo.api.models.conference.Member;
import com.plivo.api.models.endpoint.Endpoint;
import com.plivo.api.models.endpoint.EndpointCreateResponse;
import com.plivo.api.models.number.NumberType;
import com.plivo.api.models.number.PhoneNumber;
import com.plivo.api.models.number.PhoneNumberCreateResponse;
import com.plivo.api.models.recording.Recording;
import com.plivo.api.xml.Conference;
import com.plivo.api.xml.Dial;
import com.plivo.api.xml.Redirect;
import com.plivo.api.xml.Response;
import com.plivo.api.xml.Speak;
import com.plivo.api.xml.User;
import com.plivo.api.xml.Number;
import com.plivo.api.xml.Record;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.CountryNew;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityCallStatus;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.VoiceMail.VoiceMessageStatus;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.pusher.Pusher;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.MCrypt;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.CountryWiseDetailsDTO;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;
import com.xtaas.web.dto.TelnyxPhoneNumberPurchaseDTO;
import com.github.javafaker.Faker;
import com.plivo.api.*;

@Service
public class PlivoServiceImpl implements PlivoService {

	private final Logger logger = LoggerFactory.getLogger(PlivoServiceImpl.class);

	private static final String COUNTRYDIALCODE_US = "+1";

	private static final String COUNTRYDIALCODE_US_VIVA = "1";

	private HashMap<String, String> plivoAgentIdAndEndpointMap = new HashMap<String, String>();
	
	// private Map<String, String> plivoPcidAndAgentIdMap = new ConcurrentHashMap<String, String>();

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private CallLogService callLogService;

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;

	@Autowired
	private VoiceMailService voicemailService;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private DialerService dialerService;

	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private MCrypt mCrypt;
	
	@Autowired
	private ProspectCallLogService prospectLogService;
	
	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	public String encryptStringForVIVA(String strToEncrypt, String secretKey) {
		String hexencrypt = null;
		try {
			hexencrypt = mCrypt.bytesToHex(mCrypt.encrypt(strToEncrypt, secretKey));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hexencrypt;
	}

	/**
	 * This method is used to handle Plivo Webhook/Callback.
	 *
	 * @return xml string
	 */
	@Override
	public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response) {
		String toNumber = request.getParameter("To");
		String fromNumber = request.getParameter("From");
		String agentType = request.getParameter("X-PH-agentType");
		String dialerMode = request.getParameter("X-PH-dialerMode");
		String requestFromDialer = request.getParameter("dialer");
		String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
		logger.debug("==========> PlivoServiceImpl - handleVoiceCall - From: [{}] and To: [{}] <==========", fromNumber,
				toNumber);
		String xmlString = "";
		if (dialerMode != null && (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode))
				|| DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode))
				|| DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode))
				|| (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode)))
						&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
			xmlString = handleManualAgentCall(request, response);
		} else if ((fromNumber.startsWith("sip:")) && isSupervisorJoinRequest == null) {
			xmlString = handleAutoAgentCall(request, response);
		} else {
			String answeredByMachine = request.getParameter("Machine");
			String machineAnsweredDetection = request.getParameter("MachineAnsweredDetection");
			String autoMachineHangup = request.getParameter("AutoMachineHangup");
			if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
				/**
				 * phone is agentId in case of supervisorJoinRequest. NOTE : this is not used so
				 * will be removed.
				 */
				String agentId = request.getParameter("pn");
				AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
				DialerMode agentsCampaignDialerMode = agentCall.getCurrentCampaign().getDialerMode();
				xmlString = addSupervisorInConference(request, response, agentsCampaignDialerMode);
			} else if (answeredByMachine != null && answeredByMachine.equalsIgnoreCase("true") && machineAnsweredDetection != null && machineAnsweredDetection.equalsIgnoreCase("true")) {
				xmlString = handleAsynMachineDetection(request, response);
			} else if (requestFromDialer != null) {
				boolean machineDetectionMode = false;
				if (machineAnsweredDetection != null && machineAnsweredDetection.equalsIgnoreCase("true") && autoMachineHangup != null && !autoMachineHangup.equalsIgnoreCase("true")) {
					machineDetectionMode = true;
				}
				xmlString = handleDialerProspectCall(request, response, machineDetectionMode);
			}
		}
		if (xmlString == null || xmlString.isEmpty()) {
			xmlString = speak("Sorry there are some technical issue. Please contact your support team.");
		}
		return xmlString;
	}

	/**
	 * This method will be called when prospect call is ended. NOTE - This prospect
	 * call is initiated from the backend and hangup_url is specified while
	 * initiating call.
	 *
	 * @return xml string
	 */
	@Override
	public String handleProspectCallHangup(HttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			logger.debug(param + " = " + request.getParameter(param));
			paramMap.put(param, request.getParameter(param));
		}
		String xmlString = speak("");
		return xmlString;
	}

	@Override
	public String handleFallback(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		xmlString = speak("Sorry there are some technical issue. Please contact your support team.");
		return xmlString;
	}

	/**
	 * This will be called when answering machine feature is ON. It is used in
	 * Synchronous Answering Machine Detection. It is called from dialer to save
	 * call details in CallLog collection.
	 */
	@Override
	public String handleSyncMachineDetection(HashMap<String, String> paramMap) {
		CallLog callLog = callLogService.findCallLog(paramMap.get("CallUUID"));
		if (paramMap.containsKey("RecordingDuration") && paramMap.get("RecordingDuration").equals("-1")) {
			paramMap.remove("RecordingDuration");
		}
		if (callLog == null) {
			callLog = new CallLog();
			callLog.setCallLogMap(paramMap);
		} else {
			HashMap<String, String> callLogMap = callLog.getCallLogMap();
			paramMap.forEach((key, value) -> {
				if (callLogMap.get(key) == null) {
					callLogMap.put(key, value);
				}
			});
			callLog.setCallLogMap(callLogMap);
		}
		logger.info("Adding SyncAMD call details in CallLog for CallUUID [{}].", callLog.getId());
		callLogService.saveCallLog(callLog);
		return "";
	}

	/**
	 * This method is used handle incoming calls and record user message.
	 */
	@Override
	public String handleIncomingCall(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		String toNumber = request.getParameter("To");
		toNumber = toNumber.replaceAll("[^\\d]", "");
		String message = "Hi, You have reached the sales associate voicemail at " + toNumber
				+ ". We are terribly sorry we missed your call, please leave us a message and we will return your call as soon as we can.  We hope you enjoy the rest of your day!";
		List<PlivoOutboundNumber> plivoOutboundNumbers = plivoOutboundNumberService.findByDisplayPhoneNumber(toNumber);
		if (plivoOutboundNumbers != null && plivoOutboundNumbers.size() > 0) {
			message = plivoOutboundNumbers.get(new Random().nextInt(plivoOutboundNumbers.size())).getVoiceMessage();
			if (message != null && !message.isEmpty() && message.equalsIgnoreCase("{FAKE MESSAGE}")) {
				Faker faker = new Faker(new Locale("en-US"));
				int min = 20;
				int max = 50;
				int rand = (int) (Math.random() * ((max - min) + 1)) + min;
				message = faker.lorem().sentence(rand).toString();
			}
		}

		// Added for Signalwire testing
		// message = "To talk to the operator please dial 0.";
		Response xmlResponse = new Response();
		try {
			String recordingActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/plivo/voicemail";
			String recordingCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/recording/callback";
			xmlResponse.children(new Speak(message), new Record(recordingActionUrl).method("POST").redirect(false)
					.fileFormat("wav").maxLength(120).callbackUrl(recordingCallbackUrl).callbackMethod("POST"));
			xmlString = xmlResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("handleIncomingCall", toNumber)
					.eventDescription("Error occurred in handleIncomingCall").methodName("handleIncomingCall")
					.processName("handleIncomingCall").toNumber(toNumber).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * This method is used handle incoming messages.
	 */
	@Override
	public String handleIncomingMessage(HttpServletRequest request, HttpServletResponse response) {
		return speak("Thanks for your message. We hope you enjoy the rest of your day!");
	}

	/**
	 * This method is used handle incoming calls and save voicemails.
	 */
	@Override
	public String handleVoicemail(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		Response xmlResponse = new Response();
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			paramMap.put(param, request.getParameter(param));
		}
		String recordingUrl = request.getParameter("RecordUrl");
		String fromNumber = request.getParameter("From");
		String toNumber = request.getParameter("To");
		String callSid = request.getParameter("CallUUID");
		try {
			if (recordingUrl != null) {
				toNumber = toNumber.replaceAll("[^\\d]", "");
				PlivoOutboundNumber plivoOutboundNumber = null;
				List<PlivoOutboundNumber> plivoOutboundNumbers = plivoOutboundNumberService
						.findByDisplayPhoneNumber(toNumber);
				if (plivoOutboundNumbers != null && plivoOutboundNumbers.size() > 0) {
					plivoOutboundNumber = plivoOutboundNumbers.get(new Random().nextInt(plivoOutboundNumbers.size()));
				}
				String partnerId = null;
				if (plivoOutboundNumber != null) {
					partnerId = plivoOutboundNumber.getPartnerId();
				}
				logger.debug("parternerId == " + partnerId + "  toNumber = " + toNumber);
				VoiceMail voicemail = new VoiceMail(fromNumber, toNumber, callSid, recordingUrl, partnerId,
						XtaasConstants.PLIVO);
				voicemail.setCallParams(paramMap);
				voicemail.setMessageStatus(VoiceMessageStatus.NEW);
				voicemail.setAwsAudioUrl(uploadVoiceMailRecordingToAws(recordingUrl));
				voicemailService.add(voicemail);
			}
			xmlResponse.children(new Speak("Thanks for your message. Goodbye"));
			xmlString = xmlResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("handleVoicemail", toNumber)
					.eventDescription("Error occurred in handleVoicemail").methodName("handleVoicemail")
					.processName("handleVoicemail").callSid(callSid).outboundNumber(fromNumber).toNumber(toNumber)
					.addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}


	private String uploadVoiceMailRecordingToAws(String recordingUrl) {
		InputStream inputStream = null;
		String awsRecordingUrl = null;
		try {
			inputStream = new URL(recordingUrl).openStream();
			String recordingIdWithFileExtension = recordingUrl.substring(recordingUrl.length() - 40);
			awsRecordingUrl = downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME,
					recordingIdWithFileExtension, inputStream, true);
		} catch (IOException e) {
			logger.error(new SplunkLoggingUtils("uploadVoiceMailRecordingToAws", UUID.randomUUID().toString())
					.eventDescription("Error occurred in uploadVoiceMailRecordingToAws")
					.methodName("uploadVoiceMailRecordingToAws").processName("uploadVoiceMailRecordingToAws")
					.addThrowableWithStacktrace(e).build());
		}
		return awsRecordingUrl;
	}

	/**
	 * This method is used to handle recording callback and move recording to AWS.
	 */
	@Override
	public String handleRecordingCallback(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		String recordingUrl = request.getParameter("RecordUrl");
		String recordingID = request.getParameter("RecordingID");
//		String callUUID = request.getParameter("CallUUID");
		try {
			if (recordingUrl != null && recordingID != null) {
				InputStream inputStream = new URL(recordingUrl).openStream();
				String fileName = request.getParameter("RecordingID");
				String fileFormat = recordingUrl.substring(recordingUrl.length() - 4, recordingUrl.length());
				fileName = fileName + fileFormat;
				downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream,
						true);
//				deletePlivoCallRecording(recordingID, callUUID);
				HashMap<String, String> paramMap = new HashMap<String, String>();
				for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
					String param = params.nextElement();
					logger.debug(param + " = " + request.getParameter(param));
					paramMap.put(param, request.getParameter(param));
				}
				String prospectCallRedirect = paramMap.get("prospectCallRedirect");
				CallLog callLog = callLogService.findCallLog(paramMap.get("CallUUID"));
				if (paramMap.containsKey("RecordingDuration") && paramMap.get("RecordingDuration").equals("-1")) {
					paramMap.remove("RecordingDuration");
				}
				if (callLog == null) {
					callLog = new CallLog();
					callLog.setCallLogMap(paramMap);
				} else {
					HashMap<String, String> callLogMap = callLog.getCallLogMap();
					paramMap.forEach((key, value) -> {
						if (callLogMap.get(key) == null) {
							callLogMap.put(key, value);
						}
					});
					callLog.setCallLogMap(callLogMap);
				}
				callLogService.saveCallLog(callLog);
			}
		} catch (Exception e) {
			logger.error(new SplunkLoggingUtils("handleRecordingCallback", recordingID)
					.eventDescription("Error occurred in handleRecordingCallback")
					.methodName("handleRecordingCallback").processName("handleRecordingCallback")
					.addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * This method is used to create an endpoint for an agent.
	 *
	 * NOTE - Endpoint username must be alphanumeric.
	 *
	 * @return Map
	 */
	@Override
	public Map<String, String> createEndpoint(String username) {
		Map<String, String> plivoEndpointCredentials = new HashMap<>();
		String randomPassword = RandomStringUtils.random(10, true, true);
		try {
			username = username.replaceAll("[^a-zA-Z0-9]", "");
			EndpointCreateResponse endpointResponse = Endpoint.creator(username, randomPassword, username)
					.appId(ApplicationEnvironmentPropertyUtils.getPlivoAppId()).create();
			plivoEndpointCredentials.put(XtaasConstants.PLIVO_ENDPOINT_USERNAME, endpointResponse.getUsername());
			plivoEndpointCredentials.put(XtaasConstants.PLIVO_ENDPOINT_PASSWORD, randomPassword);
			plivoEndpointCredentials.put(XtaasConstants.PLIVO_ENDPOINT_ALIAS, username);
			plivoEndpointCredentials.put(XtaasConstants.PLIVO_ENDPOINT_ID, endpointResponse.getEndpointId());
			if (plivoAgentIdAndEndpointMap.containsKey(username)) {
				plivoAgentIdAndEndpointMap.replace(username, generatePlivoSIPUrl(endpointResponse.getUsername()));
			} else {
				plivoAgentIdAndEndpointMap.put(username, generatePlivoSIPUrl(endpointResponse.getUsername()));
			}
			logger.debug("Endpoint [{}] created for [{}] agent.", endpointResponse.getEndpointId(), username);
		} catch (PlivoRestException | IOException e) {
			logger.error(new SplunkLoggingUtils("createEndpoint", username)
					.eventDescription("Error occurred in createEndpoint").methodName("createEndpoint")
					.processName("createEndpoint").agentId(username).addThrowableWithStacktrace(e).build());
			throw new IllegalArgumentException("An error occurred while creating plivo endpoint.");
		}
		return plivoEndpointCredentials;
	}

	private String generatePlivoSIPUrl(String agentUserName) {

		StringBuilder agentNumber = new StringBuilder("sip:").append(agentUserName).append("@")
				.append("phone.plivo.com");
		return agentNumber.toString();
	}

	/**
	 * This method is used to delete endpoint created for an agent.
	 *
	 * @return boolean
	 */
	@Override
	public boolean deleteEndpoint(String endpointId) {
		boolean isDeleted = false;
		try {
			Endpoint.deleter(endpointId).delete();
			isDeleted = true;
			logger.debug("Endpoint [{}] deleted successfully.", endpointId);
		} catch (PlivoRestException | IOException e) {
			logger.error(new SplunkLoggingUtils("deleteEndpoint", endpointId)
					.eventDescription("Error occurred in deleteEndpoint").methodName("deleteEndpoint")
					.processName("deleteEndpoint").agentId(endpointId).addThrowableWithStacktrace(e).build());
		}
		return isDeleted;
	}

	@Override
	public boolean deleteAllEndpoints() {
		try {
			ListResponse<Endpoint> endpointResponse = Endpoint.lister().list();
			if (endpointResponse != null && endpointResponse.getObjects() != null
					&& endpointResponse.getObjects().size() > 0) {
				List<Endpoint> endpoints = endpointResponse.getObjects();
				if (endpoints != null && endpoints.size() > 0) {
					for (Endpoint endpoint : endpoints) {
						Endpoint.deleter(endpoint.getEndpointId()).delete();
						logger.debug("EndpointId [{}], EndpointAlias [{}] deleted.", endpoint.getEndpointId(),
								endpoint.getAlias());
					}
				}
			}
		} catch (PlivoRestException | IOException e) {
			logger.error(new SplunkLoggingUtils("deleteAllEndpoints", UUID.randomUUID().toString())
					.eventDescription("Error occurred in deleteAllEndpoints").methodName("deleteAllEndpoints")
					.processName("deleteAllEndpoints").addThrowableWithStacktrace(e).build());
		}
		return true;
	}

	@Override
	public String saveCallMetrics(HttpServletRequest request, HttpServletResponse response) {
		// logger.info("saveCallMetrics() : Get Call Metrics from PLivo and persist in DB.");
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			logger.debug(param + " = " + request.getParameter(param));
			paramMap.put(param, request.getParameter(param));
		}
		Response xmlResponse = new Response();
		String xmlString = "";
		try {
			xmlString = xmlResponse.toXmlString(); // set empty response xml
			String prospectCallRedirect = paramMap.get("prospectCallRedirect");
			CallLog callLog = callLogService.findCallLog(paramMap.get("CallUUID"));
			if (paramMap.containsKey("RecordingDuration") && paramMap.get("RecordingDuration").equals("-1")) {
				paramMap.remove("RecordingDuration");
			}
			if (callLog == null) {
				callLog = new CallLog();
				callLog.setCallLogMap(paramMap);
			} else {
				HashMap<String, String> callLogMap = callLog.getCallLogMap();
				paramMap.forEach((key, value) -> {
					if (callLogMap.get(key) == null) {
						callLogMap.put(key, value);
					}
				});
				callLog.setCallLogMap(callLogMap);
			}
			callLogService.saveCallLog(callLog);

			String agentId = paramMap.get("agentId");
			String prospectCallId = paramMap.get("pcid");
			String isAbandoned = paramMap.get("abandoned");
			String isSupervisorJoinRequest = paramMap.get("supervisorjoin");
			String answeredBy = paramMap.get("Machine");
			// String machineAnsweredDetection = paramMap.get("MachineAnsweredDetection");
			if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
				// just return, no further processing needed
				xmlString = xmlResponse.toXmlString(); // set empty response xml
			} else if (isAbandoned != null) {
				// prospect was abandoned
				prospectCallLogService.updateCallStatus(prospectCallId, ProspectCallStatus.CALL_ABANDONED,
						paramMap.get("CallSid"), null, null);
			} else if (answeredBy != null && answeredBy.equalsIgnoreCase("true")) {
				// update the prospect as "CALL_MACHINE_ANSWERED" in case of ASYNC AMD
//				prospectCallLogService.updateCallStatus(prospectCallId, ProspectCallStatus.CALL_MACHINE_ANSWERED,
//						paramMap.get("CallSid"), null, null);
				//commented below for ASYNC AMD - message popping aLWAYS FOR MACHINE detected
				// pushMessageToAgent(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "CALL_MACHINE_ANSWERED");
			} else {
				String caller = paramMap.get("From");
				String callerId = paramMap.get("X-PH-callerId") != null ? paramMap.get("X-PH-callerId")
						: paramMap.get("callerId");
				if ((caller != null && !caller.startsWith("sip:"))
						|| (callerId != null && !callerId.startsWith("sip"))) {
					/*
					 * plivo caller is a outbound number for outgoing calls. Caller is sip: for
					 * calls initiated by agent.
					 */
					// hangup agent call if prospect has disconnected
					if (agentId != null) {
						// Fixed the issue where hold was disconnecting the call
						List<AgentActivityLog> agentLastCallStatusList = agentActivityLogRepository.getLastCallStatus(
								agentId, prospectCallId, PageRequest.of(0, 1, Direction.DESC, "createdDate"));
						AgentActivityLog agentLastCallStatus = null;
						if (agentLastCallStatusList != null && agentLastCallStatusList.size() > 0) {
							agentLastCallStatus = agentLastCallStatusList.get(0);
						}
						if (agentLastCallStatus != null && !agentLastCallStatus.getStatus()
								.equals(AgentActivityCallStatus.CALL_ONHOLD.name())) {
							// disconnect only when call is not on hold
							logger.debug("saveCallMetrics() : Call is ended by prospect.");
							if (prospectCallRedirect != null && prospectCallRedirect.equalsIgnoreCase("true")) {
								pushMessageToAgent(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in saveCallMetrics <==========");
			logger.error("Expection : {} ", e);
		}
		return xmlString;
	}

	/**
	 * It will kick/remove participant from conference.
	 */
	@Override
	public void kickParticipantFromConference(String conferenceName, String callUUID, boolean isConferenceCall) {
		logger.info(new SplunkLoggingUtils("kickParticipantFromConference", callUUID).eventDescription("Kicking participant from conference ")
				.callSid(callUUID).agentId(conferenceName).voiceProvider("PLIVO").addField("isConference", isConferenceCall).build());
		try {
			if (isConferenceCall) {
				com.plivo.api.models.conference.Conference conference = com.plivo.api.models.conference.Conference
						.getter(conferenceName).get();
				if (conference != null) {
					List<Member> members = conference.getMembers();
					if (members != null) {
						for (Member member : members) {
							if (member.getCallUuid().equalsIgnoreCase(callUUID)) {
								com.plivo.api.models.conference.Conference
										.memberKicker(conferenceName, member.getMemberId()).kick();
								logger.debug("Kick participant [{}] from conference [{}].", callUUID, conference);
							}
						}
					}
				}
			} else {
				Call.canceller(callUUID).cancel();
			}
		} catch (PlivoRestException | IOException e) {
			logger.error(new SplunkLoggingUtils("kickParticipantFromConference", callUUID).eventDescription("Error occured in Kicking participant from conference ")
				.callSid(callUUID).agentId(conferenceName).voiceProvider("PLIVO").addField("isConference", isConferenceCall).addThrowableWithStacktrace(e).build());
		}
	}

	@Override
	public boolean holdParticipantInConference(String conferenceName, String callUUID, boolean isConferenceCall) {
		boolean success = false;
		try {
			if (isConferenceCall) {
				com.plivo.api.models.conference.Conference conference = com.plivo.api.models.conference.Conference
						.getter(conferenceName).get();
				if (conference != null) {
					List<Member> members = conference.getMembers();
					if (members != null) {
						for (Member member : members) {
							if (member.getCallUuid().equalsIgnoreCase(callUUID)) {
								com.plivo.api.models.conference.Conference
										.memberPlayer(conferenceName, member.getMemberId(),
												"http://com.twilio.music.classical.s3.amazonaws.com/BusyStrings.mp3")
										.create();
								com.plivo.api.models.conference.Conference
										.memberMuter(conferenceName, member.getMemberId()).mute();
								com.plivo.api.models.conference.Conference
										.memberDeafer(conferenceName, member.getMemberId()).deaf();
								success = true;
								logger.debug("Hold participant [{}] from conference [{}].", callUUID, conference);
							}
						}

					}
				}
			} else {
				Call.player(callUUID,
						Collections.singletonList("http://com.twilio.music.classical.s3.amazonaws.com/BusyStrings.mp3"))
						.legs(LegSpecifier.BOTH).loop(true).mix(false).play();
				success = true;
			}
		} catch (PlivoRestException | IOException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in holdParticipantInConference <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
		return success;
	}

	@Override
	public boolean unholdParticipantInConference(String conferenceName, String callUUID, boolean isConferenceCall) {
		boolean success = false;
		try {
			if (isConferenceCall) {
				com.plivo.api.models.conference.Conference conference = com.plivo.api.models.conference.Conference
						.getter(conferenceName).get();
				if (conference != null) {
					List<Member> members = conference.getMembers();
					if (members != null) {
						for (Member member : members) {
							if (member.getCallUuid().equalsIgnoreCase(callUUID)) {
								com.plivo.api.models.conference.Conference
										.memberPlayStopper(conferenceName, member.getMemberId()).stop();
								com.plivo.api.models.conference.Conference
										.memberMuteStopper(conferenceName, member.getMemberId()).stop();
								com.plivo.api.models.conference.Conference
										.memberDeafStopper(conferenceName, member.getMemberId()).stop();
								success = true;
								logger.debug("Unhold participant [{}] from conference [{}].", callUUID, conference);
							}
						}
					}
				}
			} else {
				Call.playStopper(callUUID).playStop();
				success = true;
			}
		} catch (PlivoRestException | IOException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in unholdParticipantInConference <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
		return success;
	}
	
	@Override
	public List<PlivoOutboundNumber> purchasePhoneNumbers(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<PlivoOutboundNumber> plivoOutboundNumbers = new ArrayList<PlivoOutboundNumber>();
		if (phoneDTO != null && phoneDTO.getStates() != null) {
			NumberType numberType = NumberType.valueOf(phoneDTO.getType());
			for (String state : phoneDTO.getStates()) {
				try {
					ListResponse<PhoneNumber> listResponse = PhoneNumber.lister(phoneDTO.getCountryISO()).region(state)
							.services(phoneDTO.getServices()).type(numberType).limit(phoneDTO.getLimit())
							.offset(phoneDTO.getOffset()).list();
					if (listResponse != null && listResponse.getObjects() != null
							&& listResponse.getObjects().size() > 0) {
						PhoneNumber phoneNumber = listResponse.getObjects().get(0);
						PhoneNumberCreateResponse createResponse = PhoneNumber.creator(phoneNumber.getNumber())
								.appId(phoneDTO.getAppId()).create();
						if (createResponse != null && createResponse.getNumbers() != null
								&& createResponse.getNumbers().size() > 0) {
							String number = createResponse.getNumbers().get(0).getNumber();
							//logger.info("Purchased Plivo Phone Number - [{}].", number);
							String partnerId = phoneDTO.getPartnerId();
							String region = phoneDTO.getRegion();
							int areaCode = Integer.valueOf(number.substring(1, 4));
							String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
							PlivoOutboundNumber plivoOutboundNumber = new PlivoOutboundNumber(number, number, areaCode,
									phoneDTO.getIsdCode(), "PLIVO", partnerId, partnerId, voiceMessage,
									phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(), region);
							plivoOutboundNumber = plivoOutboundNumberService.save(plivoOutboundNumber);
							plivoOutboundNumbers.add(plivoOutboundNumber);
							// update alias name & sub-account of plivo number
							com.plivo.api.models.number.Number.updater(number).alias(number)
									.subaccount(phoneDTO.getSubAccountId()).appId(phoneDTO.getAppId()).update();
							// logger.info("Plivo Phone Number [{}] is attached to sub-account [{}] & App [{}].",
							// 		new Object[] { number, phoneDTO.getSubAccountId(), phoneDTO.getAppId() });
						} else {
							logger.info("==========> Plivo Phone Number not found for [{}] <==========", state);
						}
					} else {
						logger.info("==========> Plivo Phone Number not found for [{}] <==========", state);
					}
				} catch (Exception e) {
					logger.error("==========> PlivoServiceImpl - Error occurred in purchasePhoneNumbers <==========");
					logger.error("Expection : {} ", e);
				}
			}
		}
		return plivoOutboundNumbers;
	}
	
	@Override
	public List<PlivoOutboundNumber> purchaseUSPhoneNumbersForOtherCountry(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		Plivo.init("MAYMZIYJC1YZMXZGE2N2", "MGQ1NmNhY2EzMTRmNWYzYzRkNzk3YTZiYWU1Y2Rm");
		List<PlivoOutboundNumber> plivoOutboundNumbers = new ArrayList<PlivoOutboundNumber>();
		NumberType numberType = NumberType.valueOf(phoneDTO.getType());
		if (phoneDTO != null && phoneDTO.getPattern() != null) {
			for (String pattern : phoneDTO.getPattern()) {
				ListResponse<PhoneNumber> listResponse;
				try {
//					listResponse = PhoneNumber.lister(phoneDTO.getCountryISO()).pattern(pattern)
//							.services(phoneDTO.getServices()).type(numberType).limit(phoneDTO.getLimit())
//							.offset(phoneDTO.getOffset()).list();
					PhoneNumber phoneNumber = plivoOutboundNumberService.searchNumber(phoneDTO);
					System.out.println(phoneNumber);
//					if (listResponse != null && listResponse.getObjects() != null
//							&& listResponse.getObjects().size() > 0) {
//						PhoneNumber phoneNumber = listResponse.getObjects().get(0);
						PhoneNumberCreateResponse createResponse;

						createResponse = PhoneNumber.creator(phoneNumber.getNumber())
								.appId(phoneDTO.getAppId()).create();

						if (createResponse != null && createResponse.getNumbers() != null
								&& createResponse.getNumbers().size() > 0) {
							String number = createResponse.getNumbers().get(0).getNumber();
							logger.info("Purchased Plivo Phone Number - [{}].", number);
							String partnerId = phoneDTO.getPartnerId();
							String region = phoneDTO.getRegion();
							int areaCode = Integer.valueOf(number.substring(1, 4));
							String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
							PlivoOutboundNumber plivoOutboundNumber = new PlivoOutboundNumber(number, number, areaCode,
									phoneDTO.getIsdCode(), "PLIVO", partnerId, partnerId, voiceMessage,
									phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(), region);
//							plivoOutboundNumber = plivoOutboundNumberService.save(plivoOutboundNumber);
							plivoOutboundNumbers.add(plivoOutboundNumber);
							// update alias name & sub-account of plivo number

//							com.plivo.api.models.number.Number.updater(number).alias(number)
//									.subaccount(phoneDTO.getSubAccountId()).appId(phoneDTO.getAppId()).update();

							logger.info("Plivo Phone Number [{}] is attached to sub-account [{}] & App [{}].",
									new Object[] { number, phoneDTO.getSubAccountId(), phoneDTO.getAppId() });
						} else {
							logger.info("==========> Plivo Phone Number not found for [{}] <==========");
						}
//					} 
					
				} catch (Exception e) {
					logger.error("==========> PlivoServiceImpl - Error occurred in purchasePhoneNumbers <==========");
					logger.error("Expection : {} ", e);
					e.printStackTrace();
				}
			}
		}
		addPlivoNumbersInCountries(plivoOutboundNumbers, phoneDTO);
		return plivoOutboundNumbers;
	}
	
	private void addPlivoNumbersInCountries(List<PlivoOutboundNumber> plivoNumbers, PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
		for (PlivoOutboundNumber plivoNumber : plivoNumbers) {
			for (CountryWiseDetailsDTO countryWiseDetailsDTO : countryDetails) {
				if (!countryWiseDetailsDTO.getCountryCode().equalsIgnoreCase("US")) {
					PlivoOutboundNumber plivoOutboundNumber = new PlivoOutboundNumber(plivoNumber.getDisplayPhoneNumber(),
							plivoNumber.getDisplayPhoneNumber(), plivoNumber.getAreaCode(), plivoNumber.getIsdCode(), "PLIVO",
							plivoNumber.getPartnerId(), plivoNumber.getPartnerId(), plivoNumber.getVoiceMessage(),
							plivoNumber.getPool(), countryWiseDetailsDTO.getTimeZone(), countryWiseDetailsDTO.getCountryCode(),
							countryWiseDetailsDTO.getCountryName());
					plivoOutboundNumberService.save(plivoOutboundNumber);
				}
			} 
		}
		logger.info("==========> Successfully added plivo number in all countries. <==========");
	}
	
	private List<CountryWiseDetailsDTO> getUniqueCountryDetails() {
		List<CountryWiseDetailsDTO> countryWiseList = new ArrayList<CountryWiseDetailsDTO>();
		List<StateCallConfig> stateCallConfigDetails = stateCallConfigRepository.findAll();
		if (stateCallConfigDetails != null && stateCallConfigDetails.size() > 0) {
			for (StateCallConfig stateCallConfig : stateCallConfigDetails) {
				if (stateCallConfig.getCountryName() != "United States") {
					List<CountryWiseDetailsDTO> filterList = new ArrayList<CountryWiseDetailsDTO>();
					if (countryWiseList != null && countryWiseList.size() > 0) {
						filterList = countryWiseList.stream()
								.filter(stateCall -> stateCall.getCountryName()
										.equalsIgnoreCase(stateCallConfig.getCountryName()))
								.collect(Collectors.toList());
					}
					if (filterList.size() == 0) {
						CountryWiseDetailsDTO countryDTO = new CountryWiseDetailsDTO();
						countryDTO.setCountryCode(stateCallConfig.getCountryCode());
						countryDTO.setCountryName(stateCallConfig.getCountryName());
						countryDTO.setTimeZone(stateCallConfig.getTimezone());
						countryWiseList.add(countryDTO);
					}
				}
			}
		}
		return countryWiseList;
	}

	/**
	 * Get recording URLs by CallUUID
	 *
	 * @param conferenceName
	 */
	public Map<String, List<String>> getRecordingUrlsByCallUUID(String callUUID) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		if (callUUID != null) {
			try {
				List<Recording> recordings = Recording.lister().callUuid(callUUID).list().getObjects();
				for (Recording recording : recordings) {
					List<String> list = new ArrayList<String>();
					list.add(recording.getCallUuid());
					list.add(recording.getRecordingUrl());
					list.add(recording.getRecordingType());
					list.add(recording.getRecordingStartMs());
					list.add(recording.getRecordingEndMs());
					list.add(recording.getRecordingDurationMs());
					map.put(recording.getRecordingId(), list);
				}
			} catch (PlivoRestException | IOException e) {
				logger.error("==========> PlivoServiceImpl - Error occurred in getRecordingUrlsByCallUUID <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
		return map;
	}

	@Override
	public Map<String, Integer> getCallStats() {
		Map<String, Integer> map = new HashMap<>();
		try {
			int queuedCallCount = new QueuedCallListGetter().get().getCalls().size();
			int liveCallCount = new LiveCallListGetter().get().getCalls().size();
			map.put("QUEUED", queuedCallCount);
			map.put("LIVE", liveCallCount);
		} catch (Exception e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in getCallStats <==========");
			logger.error("Expection : {} ", e);
		}
		return map;
	}

	@Override
	public String handleManualcallCallback(HttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			logger.debug(param + " = " + request.getParameter(param));
			paramMap.put(param, request.getParameter(param));
		}
		logger.debug("handleManualcallCallBack - {}", paramMap.toString());
		if (paramMap.get("DialBLegHangupCauseName") != null) {
			checkHangupCause(paramMap.get("DialBLegHangupCauseName"), paramMap.get("agentId"), paramMap.get("DialBLegHangupCause"));
		}
		String causeString = speak("");
		return causeString;
	}

	/**
	 * This will be called when call is dialed from dialer application.
	 *
	 * @return xml string
	 */
	private String handleDialerProspectCall(HttpServletRequest request, HttpServletResponse response, boolean machineDetectionMode) {
		/*
		 * This is Plivo callback. 1)This will be called when prospects phone is dialed
		 * from dialer. 2) First it will search available agent, if found then add
		 * prospect in agent's conference otherwise play abandoned message.
		 */
		String callSid = request.getParameter("CallUUID");
		String campaignId = request.getParameter("campaignId");
		String prospectCallId = request.getParameter("pcid");
		String callerId = request.getParameter("callerId");
		AgentCall allocatedAgent = null;
		String prospectCallAsJson = null;
		ProspectCall prospectCall = null;
		String xmlString = "";
		// find the available agent to connect with the prospect
		logger.debug(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Finding available agent")
				.prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId).build());
		allocatedAgent = agentRegistrationService.allocateAgent(callSid);
		if (allocatedAgent != null) {
			prospectCall = allocatedAgent.getProspectCall();
			String campaignInfoId = prospectCall.getCampaignId();
			Campaign campInfo = campaignService.getCampaign(campaignInfoId);
			Login login = loginRepository.findOneById(allocatedAgent.getAgentId());
			try {
				prospectCallAsJson = JSONUtils.toJson(prospectCall);
				PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
						prospectCallAsJson);
				if (campInfo.isNotConference() || (campInfo != null && campInfo.isBandwidthCheck() && login != null && login.isNetworkStatus())) {
					xmlString = callAgentForAutoDial(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid);
				} else {
					xmlString = addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid, campaignInfoId);
				}
				// if (machineDetectionMode) {
				// 	plivoPcidAndAgentIdMap.put(prospectCallId, allocatedAgent.getAgentId());
				// }
			} catch (IOException e) {
				logger.error(new SplunkLoggingUtils("AutoCall", callSid)
						.eventDescription("Error occurred in handleDialerProspectCall")
						.methodName("handleDialerProspectCall").processName("AutoCall").prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
			}
			logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Allocated Agent to Prospect")
					.methodName("handleDialerProspectCall").processName("AutoCall")
					.agentId(allocatedAgent.getAgentId()).prospectCallId(prospectCallId)
					.callSid(callSid).outboundNumber(callerId).build());
		} else {
			try {
				// wait for a second and attempt again to find an agent
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error(new SplunkLoggingUtils("AutoCall", callSid)
						.eventDescription("Error occurred in handleDialerProspectCall")
						.methodName("handleDialerProspectCall").processName("AutoCall").prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId)
						.addThrowableWithStacktrace(e).build());
			}
			// re-attempting to find the available agent
			allocatedAgent = agentRegistrationService.allocateAgent(callSid);
			if (allocatedAgent == null) {
				// still no agent available then play the abandon message
				logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("No Agent available")
						.methodName("handleDialerProspectCall").processName("AutoCall")
						.prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId).build());
				xmlString = playAbandonMessage(callSid, prospectCallId, campaignId, callerId);
			} else {
				prospectCall = allocatedAgent.getProspectCall();
				String campaignInfoId = prospectCall.getCampaignId();
				Campaign campInfo = campaignService.getCampaign(campaignInfoId);
				try {
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
							prospectCallAsJson);
					if (campInfo != null && campInfo.isNotConference()) {
						xmlString = callAgentForAutoDial(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid);
					} else {
						xmlString = addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid, campaignInfoId);
					}
					// if (machineDetectionMode) {
					// 	plivoPcidAndAgentIdMap.put(prospectCallId, allocatedAgent.getAgentId());
					// }
				} catch (IOException e) {
					logger.error(new SplunkLoggingUtils("AutoCall", callSid)
							.eventDescription("Error occurred in handleDialerProspectCall")
							.methodName("handleDialerProspectCall").processName("AutoCall")
							.prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId)
							.addThrowableWithStacktrace(e).build());
				}
				logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Allocated Agent to Prospect")
						.methodName("handleDialerProspectCall").processName("AutoCall").prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).build());
			}
		}
		// remove Call UUID from plivoCallStatusMap on dialer
		try {
			dialerService.removeAnsweredCallSidFromDialer(callSid);
		} catch (Exception e) {
			logger.error(new SplunkLoggingUtils("AutoCall", callSid)
					.eventDescription("Error occurred in handleDialerProspectCall")
					.methodName("handleDialerProspectCall").processName("AutoCall").prospectCallId(prospectCallId)
					.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}
	
	/**
	 * Plivo auto dialing without using conference
	 * call agent (transfer call to agent) when prospect answers call
	 * @return
	 */

	private String callAgentForAutoDial(String agentId, String pcid, String callerId, String callUUID) {
		Response response = new Response();
		String xmlString = "";
		try {
			String recordingActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + pcid + "&amp;agentId=" + agentId;
			String recordingCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/recording/callback";
			String dialActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/plivo/savecallmetrics?pcid="
					+ pcid + "&amp;agentId=" + agentId + "&amp;prospectCallRedirect=true";
			// response = new Response().children(new Record(recordingActionUrl).redirect(false).fileFormat("wav").maxLength(7200).recordSession(true).callbackUrl(recordingCallbackUrl).callbackMethod("POST"), new Conference(agentId).action(dialActionUrl).method("POST").muted(false).exitSound("beep:1").startConferenceOnEnter(true).endConferenceOnExit(false));

			String toNumber = plivoAgentIdAndEndpointMap.get(agentId);
			if (toNumber != null) {
				Record record = new Record(recordingActionUrl).method("POST").redirect(false).fileFormat("wav")
					.maxLength(7200).recordSession(false).startOnDialAnswer(true).callbackUrl(recordingCallbackUrl)
					.callbackMethod("POST");
				Dial dial = new Dial();
				// Speak spk = new Speak("Connecting your call..");
				// response.children(spk);
				User user = new User(toNumber);
				dial.children(user);
				dial.action(dialActionUrl);
				response.children(record, dial);
				xmlString = response.toXmlString();
			}

		} catch (PlivoXmlException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in addProspectInConference <==========");
			logger.error("Expection : {} ", e);
		}
		return xmlString;

	}

	/**
	 * It will return speak xml element
	 *
	 * @param message
	 * @return
	 */
	private String speak(String message) {
		Response speakResponse = new Response();
		String xmlString = "";
		try {
			speakResponse.children(new Speak(message));
			xmlString = speakResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in speak <==========");
			logger.error("Expection {} : ", e.getMessage());
		}
		return xmlString;
	}

	/**
	 * This will be called when agent dial prospect's phone manually from agent
	 * screen.
	 */
	private String handleManualAgentCall(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		try {
			// pn is prospect phone number
			String toNumber = request.getParameter("X-PH-pn");
			toNumber = toNumber.replaceAll("[^\\d]", "");
			String campaignId = request.getParameter("X-PH-campaignId");
			String prospectCallId = request.getParameter("X-PH-pcid");
			String agentId = request.getParameter("X-PH-agentId");
			// callerId is Plivo outbound number
			String fromNumber = request.getParameter("X-PH-callerId");
//			String fname = request.getParameter("X-PH-fname");
//			String lname = request.getParameter("X-PH-lname");
//			String domain = request.getParameter("X-PH-domain");
			Campaign campaign = campaignService.getCampaign(campaignId);
			if (prospectCallId == null) {
				logger.info("***** prospectCallId is NULL for campaignId [{}] & phone [{}] *****", campaignId,
						toNumber);
			}
			ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
			ProspectCall prospectCall = null;
			Prospect prospect = null;
			if (prospectCallLog != null) {
				prospectCall = prospectCallLog.getProspectCall();
				prospect = prospectCallLog.getProspectCall().getProspect();
			}
			if (fromNumber == null) {
				logger.info("***** fromNumber is NULL for campaignId [{}] & phone [{}] *****", campaignId, toNumber);
				campaign = campaignService.getCampaign(prospectCall.getCampaignId());
				fromNumber = plivoOutboundNumberService.getOutboundNumber(prospect.getPhone(),
						prospectCall.getCallRetryCount(), prospect.getCountry(), campaign.getOrganizationId());
			}
			String countryName = "United States";
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getCountry() != null) {
				countryName = prospectCallLog.getProspectCall().getProspect().getCountry();
			}
			String dialActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + prospectCallId + "&amp;agentId=" + agentId
					+ "&amp;campaignId=" + campaignId + "&amp;prospectCallRedirect=true";
			String callbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/manualcall/callback?pcid=" + prospectCallId + "&amp;agentId=" + agentId
					+ "&amp;campaignId=" + campaignId;
			String recordingActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + prospectCallId + "&amp;campaignId=" + campaignId;
			String recordingCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/recording/callback";
			Response plivoResponse = new Response();

			// check DNC while initiating manual call
//			if (!checkLocalDNCList(campaign, new ProspectCallDTO(prospectCall), toNumber, agentId, fname, lname,
//					domain)) {
//				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "prospect-dnc");
//				return plivoResponse.toXmlString();
//			}

			Record record = new Record(recordingActionUrl).method("POST").redirect(false).fileFormat("wav")
					.maxLength(7200).recordSession(false).startOnDialAnswer(true).callbackUrl(recordingCallbackUrl)
					.callbackMethod("POST");
			Dial dial = new Dial();
			dial.action(dialActionUrl);
			dial.callbackUrl(callbackUrl);
			dial.callbackMethod("POST");
			dial.method("POST");
			dial.redirect(true);
			dial.callerId(fromNumber);
			Integer callTimeLimit = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.PLIVO_MAX_CALL_TIME_LIMIT.name(), 1800);
			dial.timeLimit(callTimeLimit);
			Integer maxCallRingingDuration = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_MANUALDIAL.name(), 60);
			dial.timeout(maxCallRingingDuration);
			if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
				String sipHeaders = "thinQid=" + ApplicationEnvironmentPropertyUtils.getThinQId() + ",thinQtoken="
						+ ApplicationEnvironmentPropertyUtils.getThinQToken();
				dial.sipHeaders(sipHeaders);
				toNumber = createThinQNumber(toNumber, countryName);
				dial.children(new User(toNumber));
			} else if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Telnyx")) {
				String sipHeaders = "Telnyx-Token=" + ApplicationEnvironmentPropertyUtils.getTelnyxToken();
				dial.sipHeaders(sipHeaders);
				toNumber = createTelnyxNumber(toNumber, countryName);
				User user = new User(toNumber);
				user.sipHeaders(sipHeaders);
				dial.children(user);
			} else if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Viva")) {
				String encryptedAccountIdString = null;
				String secretKey = mCrypt.randomAlphaNumeric(16);
				encryptedAccountIdString = encryptStringForVIVA(ApplicationEnvironmentPropertyUtils.getVivaAccountId(),
				secretKey);
				String encryptedPasscodeString = null;
				encryptedPasscodeString = encryptStringForVIVA(ApplicationEnvironmentPropertyUtils.getVivaPasscode(),
				secretKey);
				if (encryptedPasscodeString != null && encryptedAccountIdString != null) {
					String sipHeaders = "VIVA_ACCOUNTID=" + encryptedAccountIdString + ",VIVA_PASSCODE="
							+ encryptedPasscodeString + ",VIVA_KEY=" + secretKey + ",DIAL_TYPE=MANUAL";
					dial.sipHeaders(sipHeaders);
				}
				toNumber = createVivaNumber(toNumber, countryName);
				User user = new User(toNumber);
				dial.children(user);
			} else {
				dial.children(new Number(toNumber));
			}
			setVoiceAndSIPProvider(campaign.getCallControlProvider(), prospectCallLog, campaign.getSipProvider());
			plivoResponse.children(record, dial);
			xmlString = plivoResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in handleManualAgentCall <==========");
			logger.error("Expection {} : ", e);
		}
		return xmlString;
	}
	
	private void setVoiceAndSIPProvider(String voiceProvider, ProspectCallLog prospectCallLog, String sipProvider) {
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
			if (voiceProvider != null && !voiceProvider.isEmpty()) {
				prospectCallLog.getProspectCall().setVoiceProvider(voiceProvider);
				if (sipProvider != null && !sipProvider.isEmpty()) {
					prospectCallLog.getProspectCall().setSipProvider(sipProvider);
				}
				prospectCallLogRepository.save(prospectCallLog);
			}
		} else {
			new RuntimeException("Voice and SIP Provider not stamped in the Prospect.");
		}
	}

	/**
	 * This will be add prospect into agent's conference.
	 *
	 * @throws PlivoXmlException
	 */
	private String addProspectInConference(String agentId, String pcid, String callerId, String callUUID, String campaignId) {
		Response response = new Response();
		String xmlString = "";
		try {
			String recordingActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + pcid + "&amp;agentId=" + agentId + "&amp;campaignId=" + campaignId;
			String recordingCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/recording/callback";
			String conferenceActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + pcid + "&amp;agentId=" + agentId
					+ "&amp;prospectCallRedirect=true";
			response = new Response().children(
					new Record(recordingActionUrl).redirect(false).fileFormat("wav").maxLength(7200).recordSession(true)
							.callbackUrl(recordingCallbackUrl).callbackMethod("POST"),
					new Conference(agentId).action(conferenceActionUrl).method("POST").muted(false).exitSound("beep:1")
							.startConferenceOnEnter(true).endConferenceOnExit(false));
			xmlString = response.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in addProspectInConference <==========");
			logger.error("Expection : {} ", e);
			logger.error(new SplunkLoggingUtils("addProspectInConference", pcid)
					.eventDescription("Error occurred in addProspectInConference").methodName("addProspectInConference")
					.processName("addProspectInConference").callSid(callUUID).prospectCallId(pcid).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * Play abandon message if agent is not available.
	 */
	private String playAbandonMessage(String callSid, String prospectCallId, String campaignId, String callerId) {
		String actionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/plivo/savecallmetrics?pcid="
				+ prospectCallId;
		if (campaignId != null) {
			actionUrl = actionUrl + "&amp;campaignId=" + campaignId;
		}
		if (callSid != null) {
			actionUrl = actionUrl + "&amp;callSid=" + callSid;
		}
		actionUrl = actionUrl + "&amp;abandoned=1";
		Response speakResponse = new Response();
		String xmlString = "";
		try {
			speakResponse.children(new Speak(XtaasConstants.PLIVO_ABANDON_OPTOUT_MSG), new Redirect(actionUrl));
			xmlString = speakResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("playAbandonMessage", prospectCallId)
					.eventDescription("Error occurred in playAbandonMessage").methodName("playAbandonMessage")
					.processName("playAbandonMessage").callSid(callSid).prospectCallId(prospectCallId)
					.campaignId(campaignId).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * This will be called in POWER or HYBRID (agentType - AUTO) mode when agent
	 * goes online.
	 */
	private String handleAutoAgentCall(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
		String agentId = request.getParameter("X-PH-agentId");
		String campaignId = request.getParameter("X-PH-campaignId");
		String prospectCallId = request.getParameter("X-PH-pcid");
		Campaign campInfo = campaignService.getCampaign(campaignId);
		if (campInfo.isNotConference()) {
			return xmlString;
		}
		logger.info(new SplunkLoggingUtils("handleAutoAgentCall", agentId).eventDescription("Creating agent conference")
				.campaignId(campaignId).agentId(agentId).voiceProvider("PLIVO").build());
		try {
			String conferenceActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
					+ "/spr/plivo/savecallmetrics?pcid=" + prospectCallId + "&amp;agentId=" + agentId
					+ "&amp;campaignId=" + campaignId;
			Response plivoXmlResponse = new Response()
					.children(new Conference(agentId).muted(false).startConferenceOnEnter(true)
							.endConferenceOnExit(true).record(false).action(conferenceActionUrl).method("POST"));
			xmlString = plivoXmlResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("handleAutoAgentCall", prospectCallId)
					.eventDescription("Error occurred in handleAutoAgentCall").methodName("handleAutoAgentCall")
					.processName("handleAutoAgentCall").prospectCallId(prospectCallId).agentId(agentId)
					.campaignId(campaignId).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * This will be called when supervisor request to join call.
	 */
	private String addSupervisorInConference(HttpServletRequest request, HttpServletResponse response,
			DialerMode dialerMode) {
		String xmlString = "";
		String confName = request.getParameter("pn"); // pn is agentId
		String conferenceActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
				+ "/spr/plivo/savecallmetrics?supervisorjoin=" + true;
		try {
			Response plivoXmlResponse = new Response()
					.children(new Conference(confName).action(conferenceActionUrl).method("POST"));
			xmlString = plivoXmlResponse.toXmlString();
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("addSupervisorInConference", confName)
					.eventDescription("Error occurred in handleAutoAgentCall").methodName("addSupervisorInConference")
					.processName("addSupervisorInConference").addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	private void pushMessageToAgent(String agentId, String event, String message) {
		logger.debug("pushMessageToAgent() : Event = [{}], Message = [{}], Agent = " + agentId, event, message);
		try {
			Pusher.triggerPush(agentId, event, message);
		} catch (IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing message to agent: " + agentId + " Msg:"
					+ message, e);
		}
	}

	private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getThinQDomainUrl());
		return thinQNumber.toString();
	}

	private String createTelnyxNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getTelnyxDomainUrl());
		return telnyxNumber.toString();
	}

	private String createVivaNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedVivaUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			// toNumber = "+" + toNumber;
		}
		StringBuilder vivaNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getVivaDomain());
		return vivaNumber.toString();
	}

	/*
	 * SIP forwarding to ThinQ from Plivo Rest Client seems to reject number w/o
	 * country code and other formatting. This method should strip all formatting
	 * and add country code. For now it will check whether the country is US and add
	 * 1 else return null. Assumption. given number is a valid US number. doesn't
	 * validate.
	 */
	private String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US + normalizedPhoneNumber;
		} else {
			formattedPhoneNumber = "+" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	private String getSimpleFormattedVivaUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US_VIVA + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	/**
	 * This will be called when answering machine feature is ON. It is used in
	 * Asynchronous Answering Machine Detection. It will hangup call then redirect
	 * to URL to handle call details.
	 */
	private String handleAsynMachineDetection(HttpServletRequest request, HttpServletResponse response) {
		String xmlString = "";
//		String pcid = request.getParameter("pcid");
//		String redirectActionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()
//				+ "/spr/plivo/savecallmetrics?pcid=" + pcid + "&amp;prospectCallRedirect=true";
		String prospectCallId = request.getParameter("pcid");
		String callSID = request.getParameter("CallUUID");
		String autoMachineHangup = request.getParameter("AutoMachineHangup");
		try {
//			Response plivoXmlResponse = new Response().children(new Redirect(redirectActionUrl));
			Response plivoXmlResponse = new Response();
			xmlString = plivoXmlResponse.toXmlString();
			String agentId = null;
			// String agentId = plivoPcidAndAgentIdMap.get(prospectCallId);
			long amdTimeDiffInMilliSec = 0L;
			List<AgentActivityLog> agentActivityLogs = agentActivityLogRepository.findByPcidAndStatus(prospectCallId, AgentActivityCallStatus.CALL_CONNECTED, PageRequest.of(0, 1, Direction.DESC, "createdDate"));
			if (agentActivityLogs != null && agentActivityLogs.size() > 0) {
				AgentActivityLog agentLastActivityLog = agentActivityLogs.get(0);
				agentId = agentLastActivityLog.getAgentId();
				amdTimeDiffInMilliSec = (System.currentTimeMillis() - agentLastActivityLog.getCreatedDate().getTime());
			}
			prospectLogService.saveCallDetectedByInProspectForMiliSecond(callSID, "machine", amdTimeDiffInMilliSec);
			if (autoMachineHangup != null && !autoMachineHangup.equalsIgnoreCase("true")) {
				if (agentId != null && !agentId.isEmpty()) {
					PusherUtils.pushMessageToUser(agentId, XtaasConstants.AMD_DETECTED_EVENT, "");
				} else {
					logger.error("==========> PlivoServiceImpl - handleAsynMachineDetection : No Agent found for pcid : " + prospectCallId + " <==========");
				}
			}
		} catch (PlivoXmlException e) {
			logger.error(new SplunkLoggingUtils("handleAsynMachineDetection", callSID)
					.eventDescription("Error occurred in handleAsynMachineDetection")
					.methodName("handleAsynMachineDetection").processName("handleAsynMachineDetection")
					.prospectCallId(prospectCallId).addThrowableWithStacktrace(e).build());
		}
		return xmlString;
	}

	/**
	 * Check the Plivo hangup cause and send proper message to agent to show on
	 * screen
	 *
	 * @param cause
	 * @param agentId
	 */
	private void checkHangupCause(String hangupCauseName, String agentId, String hangupCause) {
		List<String> normalHangupCauses = Arrays.asList("Normal Hangup", "End Of XML Instructions");
		List<String> busyHangupCauses = Arrays.asList("Rejected", "Busy Line");
		List<String> noAnswerHangupCauses = Arrays.asList("Cancelled", "No Answer", "Scheduled Hangup",
				"Ring Timeout Reached");
		List<String> sipFailureCauses = Arrays.asList("Endpoint Not Registered");
		List<String> invalidPhoneHangupCauses = Arrays.asList("Invalid Destination Address",
				"Destination Out Of Service", "Destination Country Barred", "Destination Number Barred");
		List<String> plivoFailureHangupCauses = Arrays.asList("Error Reaching Answer URL", "Error Reaching Action URL",
				"Error Reaching Transfer URL", "Error Reaching Redirect URL", "Invalid Action URL",
				"Invalid Transfer URL", "Invalid Redirect URL", "Invalid Method For Action URL",
				"Invalid Method For Transfer URL", "Invalid Method For Redirect URL", "Invalid Answer XML",
				"Invalid Action XML", "Invalid Transfer XML", "Invalid Redirect XML");
		List<String> failureHangupCauses = Arrays.asList("Unknown", "Network Error", "Internal Error", "Routing Error");
		if (hangupCause != null && hangupCauseName.equalsIgnoreCase("Endpoint Not Registered")
				&& (hangupCause.equalsIgnoreCase("UNALLOCATED_NUMBER") || hangupCause.equalsIgnoreCase("RECOVERY_ON_TIMER_EXPIRE"))) {
			PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "dest-unavailable");
		} else {
			if (normalHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
			} else if (invalidPhoneHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "invalid-phone");
			} else if (sipFailureCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "sip-failure");
			} else if (failureHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "failed");
			} else if (busyHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "busy");
			} else if (noAnswerHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "no-answer");
			} else if (plivoFailureHangupCauses.contains(hangupCauseName)) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "server-failure");
			}
		}

		// String message = "";
		// if (cause.equalsIgnoreCase("Invalid Destination Address")) {
		// message = "This call could not be completed - Phone number Invalid";
		// } else if (cause.equalsIgnoreCase("Destination Out Of Service")) {
		// message = "This call could not be completed - Phone Out of Service";
		// } else if (cause.equalsIgnoreCase("Destination Number Barred")) {
		// message = "This call could not be completed - Phone Number Barred";
		// } else if (cause.equalsIgnoreCase("No Answer")) {
		// message = "Call Not Answered by Prospect";
		// } else if (cause.equalsIgnoreCase("Busy Line")) {
		// message = "Prospect Number Busy";
		// } else if (cause.equalsIgnoreCase("Rejected")) {
		// message = "Call Rejected by Prospect";
		// } else if (cause.equalsIgnoreCase("Network Error")) {
		// message = "This call could not be completed - Network Error";
		// } else if (cause.equalsIgnoreCase("Ring Timeout Reached")) {
		// message = "Call was not answered by Prospect";
		// } else if (cause.equalsIgnoreCase("Cancelled")) {
		// message = "Call Cancelled - Disconnected by Agent";
		// } else if (cause.equalsIgnoreCase("Normal Hangup")) {
		// message = "Call ended";
		// } else if (cause.equalsIgnoreCase("Cancelled (Out Of Credits)")) {
		// message = "This call could not be completed";
		// } else if (cause.equalsIgnoreCase("Endpoint Not Registered") ||
		// cause.equalsIgnoreCase("Loop Detected") || cause.equalsIgnoreCase("End Of XML
		// Instructions") || cause.equalsIgnoreCase("Routing Error")) {
		// message = "This call could not be completed - SIP Error";
		// } else if (cause.equalsIgnoreCase("Destination Country Barred")) {
		// message = "This call could not be completed - Country Barred";
		// } else if (cause.equalsIgnoreCase("Unknown")) {
		// message = "This call could not be completed - Error Unknown";
		// } else if (cause.equalsIgnoreCase("Machine Detected")) {
		// message = "Answer Machine Detected";
		// } else if (cause.equalsIgnoreCase("Scheduled Hangup")) {
		// message = "This call could not be completed";
		// } else if (cause.equalsIgnoreCase("Media Timeout")) {
		// message = "This call could not be completed - Media Timeout";
		// } else if (cause.equalsIgnoreCase("Error Reaching Answer URL")) {
		// message = "This call could not be completed - Error Reaching Answer URL";
		// } else if (cause.equalsIgnoreCase("Error Reaching Action URL")) {
		// message = "This call could not be completed - Error Reaching Action URL";
		// } else if (cause.equalsIgnoreCase("Error Reaching Transfer URL")) {
		// message = "This call could not be completed - Error Reaching Transfer URL";
		// } else if (cause.equalsIgnoreCase("Error Reaching Redirect URL")) {
		// message = "This call could not be completed - Error Reaching Redirect URL";
		// } else if (cause.equalsIgnoreCase("Invalid Action URL")) {
		// message = "This call could not be completed - Invalid Action URL";
		// } else if (cause.equalsIgnoreCase("Invalid Transfer URL")) {
		// message = "This call could not be completed - Invalid Transfer URL";
		// } else if (cause.equalsIgnoreCase("Invalid Redirect URL")) {
		// message = "This call could not be completed - Invalid Redirect URL";
		// } else if (cause.equalsIgnoreCase("Invalid Method For Action URL")) {
		// message = "This call could not be completed - Invalid Method For Action URL";
		// } else if (cause.equalsIgnoreCase("Invalid Method For Transfer URL")) {
		// message = "This call could not be completed - Invalid Method For Transfer
		// URL";
		// } else if (cause.equalsIgnoreCase("Invalid Method For Redirect URL")) {
		// message = "This call could not be completed - Invalid Method For Redirect
		// URL";
		// } else if (cause.equalsIgnoreCase("Invalid Answer XML")) {
		// message = "This call could not be completed - Invalid Answer XML";
		// } else if (cause.equalsIgnoreCase("Invalid Action XML")) {
		// message = "This call could not be completed - Invalid Action XML";
		// } else if (cause.equalsIgnoreCase("Invalid Transfer XML")) {
		// message = "This call could not be completed - Invalid Transfer XML";
		// } else if (cause.equalsIgnoreCase("Invalid Redirect XML")) {
		// message = "This call could not be completed - Invalid Redirect XML";
		// } else if (cause.equalsIgnoreCase("Lost Race")) {
		// message = "This call could not be completed - Lost Race";
		// }

		// PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT,
		// message);
	}

	private boolean checkLocalDNCList(Campaign campaign, ProspectCallDTO prospectCall, String toNumber, String agentId,
			String fname, String lname, String domain) {
		boolean isUSorCanada = false;
		if (prospectCall.getProspect().getCountry() != null
				&& (prospectCall.getProspect().getCountry().equalsIgnoreCase("USA")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("UNITED STATES")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("US")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CANADA")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CA"))) {
			isUSorCanada = true;
		}
		String phone = toNumber.replaceAll("[^\\d]", "");
		boolean localDNCListCheckPassed = prospectCallLogService.checkLocalDNCList(fname, lname, phone, domain,
				isUSorCanada, campaign.getOrganizationId(), prospectCall, null);
		return localDNCListCheckPassed;
	}

	/**
	 * This is method is used to delete Recording from Plivo
	 *
	 * @param recordingID
	 * @param callUUID
	 */
	private void deletePlivoCallRecording(String recordingID, String callUUID) {
		try {
			Recording.deleter(recordingID).delete();
			logger.debug("Recording [{}] for CallUUID [{}] deleted successfully.", recordingID, callUUID);
		} catch (PlivoRestException | IOException e) {
			logger.error("==========> PlivoServiceImpl - Error occurred in deletePlivoCallRecording <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
	}

	/**
	 * Start call recording
	 *
	 * @param callUUID
	 * @return
	 */
	private Map<String, String> startCallRecording(String callUUID) {
		Map<String, String> callRecordingMap = new HashMap<>();
		if (callUUID != null && !callUUID.isEmpty()) {
			try {
				CallRecordCreateResponse response = Call.recorder(callUUID).record();
				callRecordingMap.put("RecordingId", response.getRecordingId());
				callRecordingMap.put("RecordingUrl", response.getUrl());
				logger.info("Recording started for call [{}]", callUUID);
			} catch (PlivoRestException | IOException e) {
				logger.error("==========> PlivoServiceImpl - Error occurred in startCallRecording <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
		return callRecordingMap;
	}

	/**
	 * Stop call recording
	 *
	 * @param callUUID
	 */
	private void stopCallRecording(String callUUID) {
		if (callUUID != null && !callUUID.isEmpty()) {
			try {
				Call.recordStopper(callUUID).recordStop();
				logger.debug("Recording started for call [{}]", callUUID);
			} catch (PlivoRestException | IOException e) {
				logger.error("==========> PlivoServiceImpl - Error occurred in stopCallRecording <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
	}

	/**
	 * Start conference recording
	 *
	 * @param conferenceName
	 * @return
	 */
	private Map<String, String> startConferenceRecording(String conferenceName) {
		Map<String, String> conferenceRecordingMap = new HashMap<>();
		if (conferenceName != null && !conferenceName.isEmpty()) {
			try {
				ConferenceRecordCreateResponse response = com.plivo.api.models.conference.Conference
						.recorder(conferenceName).record();
				conferenceRecordingMap.put("RecordingId", response.getRecordingId());
				conferenceRecordingMap.put("RecordingUrl", response.getUrl());
				logger.debug("Recording started for [{}] conference.", conferenceName);
			} catch (PlivoRestException | IOException e) {
				logger.error("==========> PlivoServiceImpl - Error occurred in startConferenceRecording <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
		return conferenceRecordingMap;
	}

	/**
	 * Stop conference recording
	 *
	 * @param conferenceName
	 */
	private void stopConferenceRecording(String conferenceName) {
		if (conferenceName != null && !conferenceName.isEmpty()) {
			try {
				com.plivo.api.models.conference.Conference.recordStopper(conferenceName).stop();
				logger.debug("Recording stopped for [{}] conference.", conferenceName);
			} catch (PlivoRestException | IOException e) {
				logger.error("==========> PlivoServiceImpl - Error occurred in stopConferenceRecording <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
	}

	@Override
	public Map<String, String> getPlivoOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO) {
		Map<String, String> outboundNumberMap = new HashMap<String, String>();
		String outboundNumber = null;
		if (redialNumberDTO != null && redialNumberDTO.getPhone() != null && redialNumberDTO.getCountry() != null
				&& redialNumberDTO.getOrganizationId() != null) {
			outboundNumber = plivoOutboundNumberService.getOutboundNumber(redialNumberDTO.getPhone(),
					redialNumberDTO.getCallRetryCount(), redialNumberDTO.getCountry(),
					redialNumberDTO.getOrganizationId());
		}
		outboundNumberMap.put("redialoutboundnumber", outboundNumber);
		return outboundNumberMap;
	}

	// @Override
	// public void removePcidFromMap(String pcid) {
	// 	if (pcid != null && !pcid.isEmpty()) {
	// 		plivoPcidAndAgentIdMap.remove(pcid);
	// 		logger.info("Removed pcid from pcidagentIdMap : " + pcid);
	// 	}

	// }

}
