package com.xtaas.service;

public interface ApplicationEnvironmentPropertyService {

	/**
	 * Message Queue URL
	 *
	 */
	public String getAMQPUrl();

	/**
	 * @return the pusherApplicationId
	 */
	public String getPusherApplicationId();

	/**
	 * @return the pusherApplicationKey
	 */
	public String getPusherApplicationKey();

	/**
	 * @return the pusherApplicationSecret
	 */
	public String getPusherApplicationSecret();

	/**
	 * Returns the base url of the server where this application is hosted
	 */
	public String getServerBaseUrl();

	public String getDialerUrl();

	/**
	 * Returns the TWIML DIAL APP SID to be used to make outgoing call by browser
	 */
	public String getTwimlDialAppSID();

	/**
	 * Returns the TWIML WHISPER APP SID to be used to make outgoing call by browser
	 */
	public String getTwimlWhisperAppSID();

	/**
	 * Returns XTaaS APP's Consumer Key for Salesforce
	 */
	public String getSalesforceAppConsumerKey();

	/**
	 * Returns XTaaS APP's Consumer Secret Key for Salesforce
	 */
	public String getSalesforceAppConsumerSecret();

	/**
	 * Returns the TWIML DIAL XML URL to be configured as Call Param to make
	 * outgoing call
	 */
	public String getTwimlDialXmlUrl();

	/**
	 * Returns the BSS Server Base URL
	 */
	public String getBssServerBaseUrl();

	public String getTwilioAccountSid();

	public String getTwilioAuthToken();

	public String getPlivoAuthId();

	public String getPlivoAuthToken();

	public String getPlivoAppId();

	public String getThinQDomainUrl();

	public String getThinQId();

	public String getThinQToken();

	public String getTelnyxDomainUrl();

	public String getTelnyxToken();

	public String getGeoCodeApiUrl();

	public String getAwsAccessKeyId();

	public String getAwsSecretKey();

	public String getVivaAccountId();

	public String getVivaPasscode();

	public String getVivaDomain();

	public String getTelnyxConnectionId();
	
	public String getLeadIQAPIKey();
	
	public String getLeadIQUrl();
	
	public String getVivaTelnyxAccountId();

	public String getVivaTelnyxPasscode();

	public String getVivaTelnyxDomain();
	
	public String getSamespaceUsername();
	
	public String getSamespacePassword();
	
	public String getSamespaceDomain();

	public String getSignalwireDomainApp();

	public String getSignalwireProjectId();

	public String getSignalwireAuthToken();

	public String getSignalwireTelnyxToken();

	public String getPlivoSIPDomain();

	public String getPlivoSIPUsername();

	public String getPlivoSIPPassword();

	public String getSignalwireVivaSIPDomain();

	public String getSignalwireVivaSIPAccountId();

	public String getSignalwireVivaSIPPassword();
	
	public String getSignalwireThinQSipToken();
	
}
