package com.xtaas.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.db.entity.DncUkTps;
import com.xtaas.db.entity.DncUsDncScrub;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.repository.DncUkTpsRepository;
import com.xtaas.db.repository.DncUsDncScrubRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.DncScrubResponseDTO;
import com.xtaas.web.dto.TpsDncResponseDTO;

@Service
public class PhoneDncServiceImpl implements PhoneDncService {

	private final Logger logger = LoggerFactory.getLogger(PhoneDncServiceImpl.class);

	@Autowired
	private HttpService httpService;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private DncUkTpsRepository dncUkTpsRepository;

	@Autowired
	private DncUsDncScrubRepository dncUsDncScrubRepository;

	@Value("${TPSMYDATA_URL:https://api.tpsmydata.co.uk?api_key=dummy_api_key&tel=%s}")
	private String TPSMYDATA_URL;

	@Value("${DNCSCRUB_LOGINID:dummy_login_id}")
	private String DNCSCRUB_LOGINID;

	private final String DNCSCRUB_URL = "https://www.dncscrub.com/app/main/rpc/scrub?phoneList=%s&version=5&output=json";

	@Override
	public boolean ukPhoneDncCheck(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		try {
			if (StringUtils.isNotBlank(TPSMYDATA_URL)) {
				isDNC = tpsUkPhoneDncCheck(isDNC, prospect, campaign);
			}
		} catch (Exception ex) {
			logger.error(new SplunkLoggingUtils("ukPhoneDncCheck", prospect.getPhone())
					.eventDescription("Failed to check UK phone DNC").addField("phoneNumber", prospect.getPhone())
					.addThrowableWithStacktrace(ex).build());
		}
		return isDNC;
	}

	private boolean tpsUkPhoneDncCheck(boolean isDNC, Prospect prospect, Campaign campaign) {
		String phoneNumber = prospect.getPhone();
		phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		if (phoneNumber.startsWith("44") && phoneNumber.length() == 12) {
			phoneNumber = "0" + phoneNumber.substring(2);
		} else if (phoneNumber.length() == 10) {
			phoneNumber = "0" + phoneNumber;
		}
		String url = String.format(TPSMYDATA_URL, phoneNumber);
		DncUkTps dncUkTps = dncUkTpsRepository.findByTelNo(phoneNumber);
		if (dncUkTps != null) {
			if (dncUkTps.isDNC()) {
				isDNC = true;
			} else {
				isDNC = false;
			}
		} else {
			logger.debug(new SplunkLoggingUtils("tpsUkPhoneDncCheck", phoneNumber)
					.eventDescription("check phone number is in TPS DNC or not").build());
			TpsDncResponseDTO tpsDncResponseDTO = httpService.get(url, new HttpHeaders(),
					new ParameterizedTypeReference<TpsDncResponseDTO>() {
					});
			if (tpsDncResponseDTO != null && tpsDncResponseDTO.getPhoneFormatStatus() != null
					&& tpsDncResponseDTO.getData() != null) {
				if (tpsDncResponseDTO.getPhoneFormatStatus().equalsIgnoreCase("VALID")
						&& (tpsDncResponseDTO.getData().getOnTps().equalsIgnoreCase("YES")
								|| tpsDncResponseDTO.getData().getOnCtps().equalsIgnoreCase("YES"))) {
					isDNC = true;
					tpsDncResponseDTO.setDNC(true);
					dncUkTpsRepository.save(new DncUkTps(tpsDncResponseDTO));
					internalDNCListService.createIDNCNumberDTO(prospect, campaign, XtaasConstants.TPS_UK_DNC);
					logger.debug(new SplunkLoggingUtils("tpsUkPhoneDncCheck", phoneNumber)
							.eventDescription("Phone number is in TPS DNC list").build());
				} else {
					// means phone is not in DNC list of TPS
					tpsDncResponseDTO.setDNC(false);
					if (StringUtils.isEmpty(tpsDncResponseDTO.getData().getTelNo())) {
						tpsDncResponseDTO.getData().setTelNo(phoneNumber);
					}
					dncUkTpsRepository.save(new DncUkTps(tpsDncResponseDTO));
				}
			}
		}
		return isDNC;
	}

	@Override
	public boolean usPhoneDncCheck(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		try {
			isDNC = dncScrubUSPhoneDncCheck(isDNC, prospect, campaign);
		} catch (Exception ex) {
			logger.error(new SplunkLoggingUtils("usPhoneDncCheck", prospect.getPhone())
					.eventDescription("Failed to check US phone DNC").addField("phoneNumber", prospect.getPhone())
					.addThrowableWithStacktrace(ex).build());
		}
		return isDNC;
	}

	private boolean dncScrubUSPhoneDncCheck(boolean isDNC, Prospect prospect, Campaign campaign) {
		String phoneNumber = prospect.getPhone();
		phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		if (phoneNumber.startsWith("1") && phoneNumber.length() == 11) {
			phoneNumber = phoneNumber.substring(1, 11);
		} else if (phoneNumber.length() == 10) {
			// don't do anything, phone is in correct format
		}
		String url = String.format(DNCSCRUB_URL, phoneNumber);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("loginid", DNCSCRUB_LOGINID);
		DncUsDncScrub dncUsDncScrub = dncUsDncScrubRepository.findByPhone(phoneNumber);
		if (dncUsDncScrub != null) {
			if (dncUsDncScrub.isDNC()) {
				isDNC = true;
			} else {
				isDNC = false;
			}
		} else {
			logger.debug(new SplunkLoggingUtils("dncScrubUSPhoneDncCheck", phoneNumber)
					.eventDescription("check phone number is in DNCScrub DNC or not").build());
			List<DncScrubResponseDTO> dncScrubResponseDTOList = httpService.getAsList(url, httpHeaders,
					new ParameterizedTypeReference<DncScrubResponseDTO[]>() {
					});
			if (!CollectionUtils.isEmpty(dncScrubResponseDTOList)) {
				DncScrubResponseDTO dncScrubResponseDTO = dncScrubResponseDTOList.get(0);
				if (!XtaasConstants.DNCSCRUB_CALLABLE_RESULTCODE.contains(dncScrubResponseDTO.getResultCode())) {
					isDNC = true;
					dncScrubResponseDTO.setDNC(true);
					dncUsDncScrubRepository.save(new DncUsDncScrub(dncScrubResponseDTO));
					internalDNCListService.createIDNCNumberDTO(prospect, campaign, XtaasConstants.DNCSCRUB_US_DNC);
					logger.debug(new SplunkLoggingUtils("dncScrubUSPhoneDncCheck", phoneNumber)
							.eventDescription("Phone number is in DNCScrub DNC list").build());
				} else {
					// means phone is not in DNC list of DNCScrub
					dncScrubResponseDTO.setDNC(false);
					dncUsDncScrubRepository.save(new DncUsDncScrub(dncScrubResponseDTO));
				}
			}
		}
		return isDNC;
	}

}
