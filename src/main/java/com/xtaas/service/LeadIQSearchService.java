package com.xtaas.service;

import java.util.List;

import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.web.dto.ManualProspectSearchDTO;

public interface LeadIQSearchService {

	public ManualProspectSearchDTO manualProspectSearch(Prospect prospectInfo, String campaignId, String agentId);

	public Prospect searchContactDetails(Prospect prospectInfo);

	public List<Prospect> searchContactDetailsMultiple(Prospect prospect, Campaign campaign);

}
