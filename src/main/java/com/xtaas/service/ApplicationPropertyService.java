package com.xtaas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ApplicationProperty;
import com.xtaas.db.repository.ApplicationPropertyRepository;

@Service
public class ApplicationPropertyService {

	@Autowired
	private ApplicationPropertyRepository applicationPropertyRepository;

	public List<ApplicationProperty> getAllApplicationProperties() {
		List<ApplicationProperty> applicationProperties = applicationPropertyRepository.findAll();
		return applicationProperties;
	}
	
	public ApplicationProperty getSingleApplicationProperty(String name) {
		ApplicationProperty appProperty = applicationPropertyRepository.findOneById(name);
		return appProperty;
	}

}
