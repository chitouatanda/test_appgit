package com.xtaas.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xtaas.domain.entity.SignalwireOutboundNumber;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;

public interface SignalwireService {

    public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response);

    public void handleVoiceStatusCallback(HttpServletRequest request, HttpServletResponse response);

    public void kickParticipantFromConference(String agentId, String callUUID, boolean isConferenceCall);

    public Map<String, String> createEndpointForAgent(String username);

    public void handleRecordingCallback(HttpServletRequest request, HttpServletResponse response);

    public void deleteEndpoint(String endpointId);
    
    public String purchasePhoneNumbers(PlivoPhoneNumberPurchaseDTO phoneDTO);

    public String purchaseUSPhoneNumbersForOtherCountry(PlivoPhoneNumberPurchaseDTO phoneDTO);

    public String purchaseUSPhoneNumbersForOtherCountryUsingLimit (PlivoPhoneNumberPurchaseDTO phoneDTO);

    public Map<String, String> getSignalwireOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO);
    
}
