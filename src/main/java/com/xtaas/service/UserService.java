package com.xtaas.service;

import java.io.IOException;
import java.util.List;

import org.springframework.dao.DuplicateKeyException;

import com.google.zxing.WriterException;
import com.xtaas.db.entity.Login;
import com.xtaas.web.dto.PasswordDTO;
import com.xtaas.web.dto.PasswordPolicyDTO;
import com.xtaas.web.dto.SupervisorDTO;
import com.xtaas.web.dto.UserImpersonateResultDTO;
import com.xtaas.web.dto.UserImpersonateSearchDTO;

/**
 * @author djain
 *
 */
public interface UserService {
	
	public void updateSalesforceOAuthTokens(String userId, String refreshToken, String accessToken, String instanceUrl);

	public Login getUser(String username);
	
	public boolean updatePassword(String userId, PasswordDTO passwordDTO);
	
	public boolean createUser(Login user) throws DuplicateKeyException;
	
	public boolean sendResetPasswordLink(String username);
	
	public boolean validateResetPasswordLink(String token);
	
	public boolean updateForgotPassword(String newPassword);
	
	public List<SupervisorDTO> getsupervisorList(String organizationId);
	
	public String removeUserFromCache(String username);
	
	public Login getAgentNetworkStatus(String username);
	
	public Login postAgentNetworkStatus(String username,boolean networkstatus);
	
	public void resetUserAfterFiveAttempts(String username);
	
	public void lockUserAfterFiveAttempts(String username);
	
	public PasswordPolicyDTO getUserFromDB(String username);
	
	public String createQRCodeMFA(String username) throws WriterException, IOException;
	
	public boolean getTOTPCode(String username,int topt);
	
	public UserImpersonateResultDTO getUserList(UserImpersonateSearchDTO userImpersonateSearchDTO);

}