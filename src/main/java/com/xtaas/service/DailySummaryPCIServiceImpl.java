package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CallStats;
import com.xtaas.db.entity.CallStatsJobTracker;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.CallStatsJobTrackerRepository;
import com.xtaas.db.repository.CallStatsRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.CallStatsDTO;

@Service
public class DailySummaryPCIServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DailySummaryPCIServiceImpl.class);
	
	private static final String SERVICEMODEL = "ServiceModel" ;
	
	private static final String PLATFORMMODEL = "PlatformModel";
	
	private static final String CONNECTMODEL = "ConnectModel";
	
	private static final String INPROGRESS = "INPROGRESS";
	
	private static final String COMPLETED = "COMPLETED";

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private CallStatsRepository callStatsRepository;
	
	@Autowired
	private CallStatsJobTrackerRepository callStatsJobTrackerRepository;

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	public void saveCallstats() {
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -15);
		cal.set(Calendar.SECOND, 0);
		cal1.set(Calendar.SECOND, 0);
		Date startDate = cal.getTime();
		Date endDate = cal1.getTime();
		saveCallStats(SERVICEMODEL,startDate, endDate);
		saveCallStats(PLATFORMMODEL,startDate, endDate);
		saveCallStats(CONNECTMODEL,startDate, endDate);
	}
	
	private CallStats getLatestCallStats(String type){
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(Direction.DESC, "updatedDate"));
		Pageable pageable = PageRequest.of(0, 1, Sort.by(orders));
		List<CallStats> callstats = callStatsRepository.findLatestCallstatsRecord(type, pageable);
		if(callstats!= null && callstats.size()>0)
			return callstats.get(0);
		
		return null;
		
	}
	
	private CallStatsJobTracker getCallStatsJobTracker(String type){
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(Direction.DESC, "updatedDate"));
		Pageable pageable = PageRequest.of(0, 1, Sort.by(orders));
		List<CallStatsJobTracker> csjtList =callStatsJobTrackerRepository.findLatestCallstatsTracker(type,INPROGRESS,pageable);
		if(csjtList!= null && csjtList.size()>0)
			return csjtList.get(0);
		
		return null;
		
	}


	public void createDailySummaryPCIReport() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -1);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 0);
		Calendar cal1 = Calendar.getInstance();
//		cal1.add(Calendar.HOUR_OF_DAY, -1);
//		cal1.set(Calendar.MINUTE, 59);
//		cal1.set(Calendar.SECOND, 59);
		Date startDate = cal.getTime();
		Date endDate = cal1.getTime();
		getRecordsForSummaryReport(SERVICEMODEL, startDate, endDate);
		getRecordsForSummaryReport(PLATFORMMODEL, startDate, endDate);
		getRecordsForSummaryReport(CONNECTMODEL, startDate, endDate);
	}

	private void saveCallStats(String type,Date startDate, Date endDate) {

		CallStatsJobTracker csjt = new CallStatsJobTracker();
		csjt.setType(type);
		csjt.setStatus(INPROGRESS);
		csjt = callStatsJobTrackerRepository.save(csjt);
		logger.debug("marked to INPROGRESS callstatstracker : start"+type);
		
		
		logger.debug(type+" : saveCallStats : start for [{}]",type);
		CallStats latestCallStats = getLatestCallStats(type);
		if(latestCallStats!=null && latestCallStats.getJobInitiatedTime()!=null){
			startDate = latestCallStats.getJobInitiatedTime();
		}
		
		List<Campaign> campaigns = new ArrayList<Campaign>();
		if(type.equalsIgnoreCase(SERVICEMODEL))
			campaigns = campaignRepository.getCampaignsForServiceModel();
		if(type.equalsIgnoreCase(PLATFORMMODEL))
			campaigns = campaignRepository.getCampaignsForPlatFormModel();
		if(type.equalsIgnoreCase(CONNECTMODEL))
			campaigns = campaignRepository.getConnectCampaigns();
		List<CallStats> callStatsList = new ArrayList<>();
		int recordsAttempted = 0;
		int answerMachineRecords = 0;
		int recordsContacted = 0;
		int recordsConnected = 0;
		int abandonedRecords = 0;
		int successRecords = 0;
		double dialsPerSuccess = 0;
		double abandoned_percent = 0;
		double contactPercent = 0;
		double connectPercent = 0;
		double successPerDials = 0;
		double successPerContacts = 0;
		double successPerConnects = 0;
		double contactPerDial = 0;
		double connectsPerDial = 0;
		double connectsPerContacts = 0;
		double leadPerHt = 0;
		if (campaigns != null && campaigns.size() > 0) {
			int ht = 0;
			double ot = 0;
			for (Campaign campaign : campaigns) {
				CallStats callStats = new CallStats();
				List<String> campaignIds = new ArrayList<String>();
				campaignIds.add(campaign.getId());
				recordsAttempted = prospectCallInteractionRepository.findRecordsAttempted(campaignIds, startDate,
						endDate);
				answerMachineRecords = prospectCallInteractionRepository.findAnswermachineCount(campaignIds, startDate,
						endDate);
					
				if(type.equalsIgnoreCase(SERVICEMODEL)){
					recordsContacted = prospectCallInteractionRepository.findContactedRecodsForServiceModel(campaignIds,
						startDate, endDate);
					recordsConnected = prospectCallInteractionRepository.findConnectedRecodsForServiceModel(campaignIds,
							startDate, endDate);
				}
				if(type.equalsIgnoreCase(PLATFORMMODEL)){
					recordsContacted = prospectCallInteractionRepository.findContactedRecodsForPlatformModel(campaignIds,
							startDate, endDate);
					recordsConnected = prospectCallInteractionRepository.findConnectedRecodsForPlatformeModel(campaignIds,
							startDate, endDate);
					
				}
				if(type.equalsIgnoreCase(CONNECTMODEL)){
					recordsContacted = prospectCallInteractionRepository.findContactedRecordsCount(campaignIds, startDate,
							endDate);
					recordsConnected = prospectCallInteractionRepository.findConnectRecordsCount(campaignIds, startDate,
							endDate);
				}
				
				
				abandonedRecords = prospectCallInteractionRepository.findAbondonedRecordsCount(campaignIds, startDate,
						endDate);
				successRecords = prospectCallInteractionRepository.findSuccessRecordsCount(campaignIds, startDate,
						endDate);

				ht = prospectCallInteractionRepository.getHandleTime(campaignIds, startDate, endDate);
				ot = agentActivityLogRepository.calculateOnlineTime(campaignIds, startDate, endDate);
				HashMap<String, Integer> callableCountMap = prospectCallLogService
						.getCallableRecordsCount(campaign.getId());
				dialsPerSuccess = calculatePercentage(recordsAttempted, successRecords);
				abandoned_percent = calculatePercentage(abandonedRecords, recordsContacted);
				contactPercent = calculatePercentage(recordsContacted, recordsAttempted);
				connectPercent = calculatePercentage(recordsConnected, recordsAttempted);
				successPerDials = calculatePercentage(successRecords, recordsAttempted);
				successPerContacts = calculatePercentage(successRecords, recordsContacted);
				successPerConnects = calculatePercentage(successRecords, recordsConnected);
				contactPerDial = calculatePercentage(recordsContacted, recordsAttempted);
				connectsPerDial = calculatePercentage(recordsConnected, recordsAttempted);
				connectsPerContacts = calculatePercentage(recordsConnected, recordsContacted);
				leadPerHt = calculatePercentage(successRecords, ht);

				
				callStats.setLeads(successRecords);
				callStats.setAttempt(recordsAttempted);
				callStats.setContact(recordsContacted);
				callStats.setAnswerMachine(answerMachineRecords);
				callStats.setConnect(recordsConnected);
				callStats.setAbandoned(abandonedRecords);
				callStats.setDialsPerSuccess(dialsPerSuccess);
				callStats.setAbandonedPercent(abandoned_percent);
				callStats.setContactPercent(contactPercent);
				callStats.setConnectPercent(connectPercent);
				callStats.setSuccessPerDials(successPerDials);
				callStats.setSuccessPerContacts(successPerContacts);
				callStats.setSuccessPerConnects(successPerConnects);
				callStats.setContactPerDial(contactPerDial);
				callStats.setConnectsPerDial(connectsPerDial);
				callStats.setConnectsPerContacts(connectsPerContacts);
				callStats.setHandleTimeHoursVal(ht);
				callStats.setOnlineTimeHoursVal(ot);
				callStats.setLeadPerHandleTimeHour(leadPerHt);
				callStats.setJobInitiatedTime(endDate);
				callStats.setType(type);
				callStats.setCampaignId(campaign.getId());
				callStats.setCampaignName(campaign.getName());
				if (callableCountMap != null) {
					callStats.setCallableCount(callableCountMap.get(XtaasConstants.CALLABLE_COUNT));
				}
				callStatsList.add(callStats);
			}
			logger.debug("saveCallStats() : Saving records for [{}]", type);
			callStatsRepository.saveAll(callStatsList);
			logger.debug("saveCallStats() : end for [{}]",type);
			
		}
		csjt.setStatus(COMPLETED);
		callStatsJobTrackerRepository.save(csjt);
		logger.debug("marked to completed callstatstracker : end"+type);
	}

	private double calculatePercentage(int count1, int count2) {
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		double percentage = 0;
		if (count1 > 0 && count2 > 0) {
			percentage = ((double) count1 / (double) count2) * 100;
		}
		return Double.parseDouble(decimalFormat.format(percentage));
	}

	
	

	private void writeToExcel(CallStatsDTO callStat, DecimalFormat decimalFormat, Row row, XSSFWorkbook workbook, String hours) {
		Cell cell = row.createCell(0);
		if (hours != null && !hours.isEmpty()) {
			cell.setCellValue(hours);
		} else if(callStat.getHours() != null && !callStat.getHours().isEmpty() && callStat.getHours().equalsIgnoreCase("Total")){
			cell.setCellValue(callStat.getHours());
		} else {
			cell.setCellValue(callStat.getHours());
		}
		cell = row.createCell(1);
		cell.setCellValue(callStat.getCampaignName());
		cell = row.createCell(2);
		cell.setCellValue(callStat.getLeads());
		cell = row.createCell(3);
		cell.setCellValue(callStat.getDialsPerSuccess());
		cell = row.createCell(4);
		cell.setCellValue(callStat.getAttempt());
		cell = row.createCell(5);
		cell.setCellValue(callStat.getContact());
		cell = row.createCell(6);
		cell.setCellValue(callStat.getAnswerMachine());
		cell = row.createCell(7);
		cell.setCellValue(callStat.getConnect());
		cell = row.createCell(8);
		cell.setCellValue(callStat.getAbandoned());
		cell = row.createCell(9);
		cell.setCellValue(callStat.getAbandonedPercent());
		cell = row.createCell(10);
		cell.setCellValue(callStat.getContactPercent());
		cell = row.createCell(11);
		cell.setCellValue(callStat.getConnectPercent());
		cell = row.createCell(12);
		cell.setCellValue(callStat.getOnlineTimeHoursVal());
		cell = row.createCell(13);
		cell.setCellValue(callStat.getHandleTimeHoursVal());
		cell = row.createCell(14);
		cell.setCellValue(callStat.getLeadPerHandleTimeHour());
		cell = row.createCell(15);
		cell.setCellValue(callStat.getSuccessPerDials());
		cell = row.createCell(16);
		cell.setCellValue(callStat.getSuccessPerContacts());
		cell = row.createCell(17);
		cell.setCellValue(callStat.getSuccessPerConnects());
		cell = row.createCell(18);
		cell.setCellValue(callStat.getContactPerDial());
		cell = row.createCell(19);
		cell.setCellValue(callStat.getConnectsPerDial());
		cell = row.createCell(20);
		cell.setCellValue(callStat.getConnectsPerContacts());
	}

	private void writeHeaders(Row headerRow, XSSFCellStyle labelCellStyle) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Hour");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(1);
		cell.setCellValue("Camapign Name");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(2);
		cell.setCellValue("Leads");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(3);
		cell.setCellValue("DPS");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(4);
		cell.setCellValue("Attempt");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(5);
		cell.setCellValue("Contact");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(6);
		cell.setCellValue("AM");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(7);
		cell.setCellValue("Connect");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(8);
		cell.setCellValue("Abd");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(9);
		cell.setCellValue("Abd%");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(10);
		cell.setCellValue("Contact%");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(11);
		cell.setCellValue("Connect%");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(12);
		cell.setCellValue("OT");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(13);
		cell.setCellValue("HT");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(14);
		cell.setCellValue("Leads/Ht");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(15);
		cell.setCellValue("Success/Dials");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(16);
		cell.setCellValue("Success/Contacts");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(17);
		cell.setCellValue("Success/Connect");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(18);
		cell.setCellValue("contact/Dials");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(19);
		cell.setCellValue("Connects/Dials");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(20);
		cell.setCellValue("Connects/Contacts");
		cell.setCellStyle(labelCellStyle);

	}

	private void getRecordsForSummaryReport(String type, Date startDate, Date endDate) {
		
		CallStatsJobTracker csjt =getCallStatsJobTracker(type);
		if(csjt!=null){
			logger.debug("getRecordsForSummaryReport() : 15 min Report Job is in Progrss [{}] summary report. :- WAITING", type);
			getRecordsForSummaryReport(type,startDate,endDate);
		}
		
		
		String hours = String.valueOf(startDate.getHours()) + " _ " + String.valueOf(startDate.getHours() + 1);
		List<String> campaignIds = new ArrayList<>();
		logger.debug("getRecordsForSummaryReport() : fetching records for [{}] summary report. :- start", type);
		List<CallStats> listCallStats = callStatsRepository.findCallstatsRecords(type, startDate, endDate);
		if (listCallStats != null && listCallStats.size() > 0) {
			campaignIds = listCallStats.stream().map(CallStats::getCampaignId).collect(Collectors.toList());
			List<CallStatsDTO> callStatsDTOs = createListForExcel(campaignIds, listCallStats, hours);
			try {
				createAndSendReport(callStatsDTOs, type, hours);
			} catch (IOException e) {
				logger.debug("getRecordsForSummaryReport() : Error occured while creating report for [{}]", type);
				e.printStackTrace();
			}
		} else {
			logger.debug("getRecordsForSummaryReport() : records not found for [{}] summary report.", type);
		}
		logger.debug("getRecordsForSummaryReport() : fetching records for [{}] summary report. :- end", type);
	}

	private List<CallStatsDTO> createListForExcel(List<String> campaignIds, List<CallStats> listCallStats,
			String hours) {
		logger.debug("createListForExcel() : creating list of records for excel - start");
		List<CallStatsDTO> callStatsDTOs = new ArrayList<>();
		List<CallStatsDTO> tempCallStatsDTOs = new ArrayList<>();
		Map<String, CallStatsDTO> callStatsMap = new HashMap<>();
		int totalLeads = 0;
		int totalAttempts = 0;
		int totalContact = 0;
		int totalAm = 0;
		int totalConnect = 0;
		double totalOt = 0;
		int totalHt = 0;
		int totalAbandoned = 0;
		for (String campaignId : campaignIds) {
			for (CallStats callStats : listCallStats) {
				if (callStats.getCampaignId().equalsIgnoreCase(campaignId)) {
					CallStatsDTO callStatsDTO = new CallStatsDTO();
					callStatsDTO.setCampaignName(callStats.getCampaignName());
					callStatsDTO.setLeads(callStats.getLeads());
					callStatsDTO.setDialsPerSuccess(callStats.getDialsPerSuccess());
					callStatsDTO.setAttempt(callStats.getAttempt());
					callStatsDTO.setContact(callStats.getContact());
					callStatsDTO.setAnswerMachine(callStats.getAnswerMachine());
					callStatsDTO.setConnect(callStats.getConnect());
					callStatsDTO.setAbandoned(callStats.getAbandoned());
					callStatsDTO.setAbandonedPercent(callStats.getAbandonedPercent());
					callStatsDTO.setContactPercent(callStats.getContactPercent());
					callStatsDTO.setConnectPercent(callStats.getConnectPercent());
					callStatsDTO.setOnlineTimeHoursVal(callStats.getOnlineTimeHoursVal());
					callStatsDTO.setHandleTimeHoursVal(callStats.getHandleTimeHoursVal());
					callStatsDTO.setLeadPerHandleTimeHour(callStats.getLeadPerHandleTimeHour());
					callStatsDTO.setSuccessPerDials(callStats.getSuccessPerDials());
					callStatsDTO.setSuccessPerContacts(callStats.getSuccessPerContacts());
					callStatsDTO.setSuccessPerConnects(callStats.getSuccessPerConnects());
					callStatsDTO.setContactPerDial(callStats.getContactPerDial());
					callStatsDTO.setConnectsPerDial(callStats.getConnectsPerDial());
					callStatsDTO.setConnectsPerContacts(callStats.getConnectsPerContacts());
					if (!Integer.toString(callStats.getLeads()).equalsIgnoreCase("Infinit")) {
						totalLeads = totalLeads + callStats.getLeads();
					}
					if (!Integer.toString(callStats.getAttempt()).equalsIgnoreCase("Infinit")) {
						totalAttempts = totalAttempts + callStats.getAttempt();
					}
					if (!Integer.toString(callStats.getContact()).equalsIgnoreCase("Infinit")) {
						totalContact = totalContact + callStats.getContact();
					}
					if (!Integer.toString(callStats.getAnswerMachine()).equalsIgnoreCase("Infinit")) {
						totalAm = totalAm + callStats.getAnswerMachine();
					}
					if (!Integer.toString(callStats.getConnect()).equalsIgnoreCase("Infinit")) {
						totalConnect = totalConnect + callStats.getConnect();
					}
					if (!Double.toString(callStats.getOnlineTimeHoursVal()).equalsIgnoreCase("Infinit")) {
						totalOt = totalOt + callStats.getOnlineTimeHoursVal();
					}
					if (!Integer.toString(callStats.getHandleTimeHoursVal()).equalsIgnoreCase("Infinit")) {
						totalHt = totalHt + callStats.getHandleTimeHoursVal();
					}
					if (!Integer.toString(callStats.getAbandoned()).equalsIgnoreCase("Infinit")) {
						totalAbandoned = totalAbandoned + callStats.getAbandoned();
					}
					tempCallStatsDTOs.add(callStatsDTO);
					if (callStatsMap.size() < 1) {
						callStatsMap.put(campaignId, callStatsDTO);
					} else {
						CallStatsDTO tempDto = callStatsMap.get(campaignId);
						if (tempDto != null) {
							tempDto.setLeads(tempDto.getLeads() + callStats.getLeads());
							tempDto.setAttempt(tempDto.getAttempt() + callStats.getAttempt());
							tempDto.setContact(tempDto.getContact() + callStats.getContact());
							tempDto.setAnswerMachine(tempDto.getAnswerMachine() + callStats.getAnswerMachine());
							tempDto.setConnect(tempDto.getConnect() + callStats.getConnect());
							tempDto.setAbandoned(tempDto.getAbandoned() + callStats.getAbandoned());
							tempDto.setOnlineTimeHoursVal(tempDto.getOnlineTimeHoursVal() + callStats.getOnlineTimeHoursVal());
							tempDto.setHandleTimeHoursVal(tempDto.getHandleTimeHoursVal() + callStats.getHandleTimeHoursVal());
							tempDto.setDialsPerSuccess(tempDto.getAttempt() +  tempDto.getLeads());
							tempDto.setAbandonedPercent(calculatePercentage(tempDto.getAbandoned(), tempDto.getContact()));
							tempDto.setContactPercent(calculatePercentage(tempDto.getContact(), tempDto.getAttempt()));
							tempDto.setConnectPercent(calculatePercentage(tempDto.getConnect(), tempDto.getAttempt()));
							tempDto.setSuccessPerDials(calculatePercentage(tempDto.getLeads(), tempDto.getAttempt()));
							tempDto.setSuccessPerContacts(calculatePercentage(tempDto.getLeads(), tempDto.getContact()));
							tempDto.setSuccessPerConnects(calculatePercentage(tempDto.getLeads(), tempDto.getConnect()));
							tempDto.setContactPerDial(calculatePercentage(tempDto.getContact(), tempDto.getAttempt()));
							tempDto.setConnectsPerDial(calculatePercentage(tempDto.getConnect(), tempDto.getAttempt()));
							tempDto.setConnectsPerContacts(calculatePercentage(tempDto.getConnect(), tempDto.getContact()));
							tempDto.setLeadPerHandleTimeHour(calculatePercentage(tempDto.getLeads(), tempDto.getHandleTimeHoursVal()));
							callStatsMap.put(campaignId, tempDto);
						} else {
							callStatsMap.put(campaignId, callStatsDTO);
						}
					}
				}
			}
		}
		if (tempCallStatsDTOs != null && tempCallStatsDTOs.size() > 0) {
			CallStatsDTO statsDTO = new CallStatsDTO();
			statsDTO.setHours("Total");
			statsDTO.setLeads(totalLeads);
			statsDTO.setAttempt(totalAttempts);
			statsDTO.setContact(totalContact);
			statsDTO.setAnswerMachine(totalAm);
			statsDTO.setConnect(totalConnect);
			statsDTO.setOnlineTimeHoursVal(totalOt);
			statsDTO.setHandleTimeHoursVal(totalHt);
			statsDTO.setSuccessPerDials(calculatePercentage(totalLeads, totalAttempts));
			statsDTO.setSuccessPerContacts(calculatePercentage(totalLeads, totalContact));
			statsDTO.setSuccessPerConnects(calculatePercentage(totalLeads, totalConnect));
			statsDTO.setContactPerDial(calculatePercentage(totalContact, totalAttempts));
			statsDTO.setConnectsPerDial(calculatePercentage(totalConnect, totalAttempts));
			statsDTO.setConnectsPerContacts(calculatePercentage(totalConnect, totalContact));
			statsDTO.setAbandoned(totalAbandoned);
			statsDTO.setLeadPerHandleTimeHour(calculatePercentage(totalLeads, totalHt));
			tempCallStatsDTOs.add(statsDTO);
//			callStatsMap.put("total", statsDTO);
			for (Entry<String, CallStatsDTO> callStatsDTO2 : callStatsMap.entrySet()) {
				callStatsDTOs.add(callStatsDTO2.getValue());
			}
			callStatsDTOs.add(statsDTO);
		}
		logger.debug("createListForExcel() : creating list of records for excel - start");
		return callStatsDTOs;
	}

	private void createAndSendReport(List<CallStatsDTO> listCallStats, String type,String tempHours) throws IOException {
		logger.debug("createAndSendReport() : creating excel for [{}]- start", type);
		String hours = tempHours; 
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		// sheet.setColumnWidth(0, 4000);
		sheet.setColumnWidth(1, 8000);
		// sheet.setColumnWidth(2, 4000);
		XSSFFont headerCellFont = workbook.createFont();
		headerCellFont.setFontHeightInPoints((short) 12);
		headerCellFont.setFontName("IMPACT");
		XSSFFont labelCellFont = workbook.createFont();
		labelCellFont.setFontHeightInPoints((short) 12);
		labelCellFont.setFontName("ARIAL");
		XSSFCellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFillForegroundColor(HSSFColorPredefined.YELLOW.getIndex());
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFont(headerCellFont);
		XSSFCellStyle labelCellStyle = workbook.createCellStyle();
		labelCellStyle.setFillForegroundColor(HSSFColorPredefined.LIGHT_YELLOW.getIndex());
		labelCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		labelCellStyle.setFont(labelCellFont);

		int rowCount = 0;
		if (listCallStats != null && listCallStats.size() > 0) {
			Row row = sheet.createRow(rowCount);
			row = sheet.createRow(rowCount);
			writeHeaders(row, headerCellStyle);
			rowCount++;
			Iterator<CallStatsDTO> iterator = listCallStats.iterator();
			int i = 1;
			while (iterator.hasNext()) {
				if(i == 1) {
					hours = tempHours;
					i++;
				} else {
					hours = "";
				}
				Row newRow = sheet.createRow(rowCount);
				CallStatsDTO callStat = iterator.next();
				writeToExcel(callStat, decimalFormat, newRow, workbook, hours);
				rowCount++;
			}

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
			String dateStr = sdf.format(date);
			int totalRows = sheet.getPhysicalNumberOfRows();
			Row row1 = sheet.createRow(totalRows + 1);
			String fileName = "Daily summary Interactions_" + dateStr + ".xlsx";
			String message = "Please find the attached file.";
			String subject = "Daily summary Interactions";
			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			workbook.close();
			List<String> contacts = new ArrayList<>();
			contacts.add("anaikshelar@xtaascorp.com");
			contacts.add("kchugh@xtaascorp.com");
			contacts.add("amane@xtaascorp.com");
			contacts.add("sudhir.gholap@invimatic.com");
			for (String contact : contacts) {
				XtaasEmailUtils.sendEmail(contact, "data@xtaascorp.com", subject, message,
						fileNames);
				logger.debug("[{}] Daily summary report succesfully send to [{}]", type, contact);				
			}
		}
		logger.debug("createAndSendReport() : creating excel for [{}]- end", type);
	}

}
