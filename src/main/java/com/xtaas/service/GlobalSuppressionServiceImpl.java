package com.xtaas.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.SuppressionListNewRepository;

@Service
public class GlobalSuppressionServiceImpl implements GlobalSuppressionService {

	private static final Logger logger = LoggerFactory.getLogger(GlobalSuppressionServiceImpl.class);

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Override
	public String uploadEmailSuppressionList(MultipartFile file) throws Exception {
		if (!file.isEmpty()) {
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				private static final long serialVersionUID = 1L;
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				String fileName = file.getName();
				try {
					Workbook book = WorkbookFactory.create(file.getInputStream());
					Sheet sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					// check if nothing found in file
					if (totalRowsCount < 1) {
						book.close();
						logger.error(fileName + " file is empty");
						return fileName + " file is empty";
					}
					readFileRecords(book, sheet, totalRowsCount);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					return "Failed to upload file. Error: " + e.getMessage();
				}
			} else {
				throw new IllegalArgumentException("Invalid file format. Only .xls, .xlsx accepted.");
			}
		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
		return "File uploaded successfully";
	}

	private void readFileRecords(Workbook book, Sheet sheet, int totalRowsCount) {
		LinkedList<String> headers = new LinkedList<String>();
		// read header row & take all headers
		Row headerRow = sheet.getRow(0);
		for (int i = 0; i < headerRow.getPhysicalNumberOfCells(); i++) {
			headers.add(headerRow.getCell(i).getStringCellValue());
		}
		List<SuppressionListNew> list = new ArrayList<SuppressionListNew>();
		// read all rows except header row
		for (int i = 1; i < totalRowsCount; i++) {
			Row row = sheet.getRow(i);
			boolean isValidRow = true;
			SuppressionListNew suppressionListNew = new SuppressionListNew();
			// read all cells in the row
			for (int j = 0; j < headers.size(); j++) {
				Cell cell = row.getCell(j);
				// skip row if any cell's value is empty
				if (cell == null || cell.getCellType() == CellType.BLANK
						|| StringUtils.isEmpty(cell.getStringCellValue())) {
					isValidRow = false;
					break;
				}
				if (headers.get(j).equalsIgnoreCase("email") || headers.get(j).equalsIgnoreCase("email address")) {
					suppressionListNew.setEmailId(cell.getStringCellValue());
				}
				if (headers.get(j).equalsIgnoreCase("organization")) {
					suppressionListNew.setOrganizationId(cell.getStringCellValue());
				}
			}
			if (isValidRow) {
				int exists = suppressionListNewRepository.countByOrganizationEmail(
						suppressionListNew.getOrganizationId(), suppressionListNew.getEmailId());
				if (exists == 0) {
					suppressionListNew.setStatus("ACTIVE");
					list.add(suppressionListNew);
				}
			}
		}
		suppressionListNewRepository.bulkinsert(list);
	}

}
