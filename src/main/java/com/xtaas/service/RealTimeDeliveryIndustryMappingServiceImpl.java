package com.xtaas.service;

import com.xtaas.db.entity.RealTimeDeliveryIndustryMapping;
import com.xtaas.db.repository.RealTimeDeliveryIndustryMappingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RealTimeDeliveryIndustryMappingServiceImpl implements RealTimeDeliveryIndustryMappingService {

	@Autowired
	private RealTimeDeliveryIndustryMappingRepository RealTimeDeliveryIndustryMappingRepository;

	public RealTimeDeliveryIndustryMapping getRealTimeDeliveryIndustryMapping(String xtaasIndustry) {
		return RealTimeDeliveryIndustryMappingRepository.findByXtaasIndustry(xtaasIndustry);
	}
}
