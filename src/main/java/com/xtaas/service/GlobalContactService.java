package com.xtaas.service;

import java.util.List;

import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;

public interface GlobalContactService {

	public void generateGlobalContact();

	public void generateGlobalContact(List<ProspectCallLog> calllogs);

	public GlobalContact saveGlobalContactAndInteraction(GlobalContact globalContact,
			boolean createGlobalContactInteraction);

	public GlobalContact checkContactIdentity(GlobalContact globalContact, GlobalContact newGlobalContact);

	public long countUniqueRecordByFLP(ProspectCall prospectCall);

	public GlobalContact findUniqueRecordByFLP(ProspectCall prospectCall);

	public GlobalContact findUniqueRecordByFLP(String firstName, String lastName, String phone);
	
	public void pciAnalysisSave(List<PCIAnalysis> pcianList);

}
