package com.xtaas.service;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Login;
import com.xtaas.web.dto.ProspectCallSearchDTO;

public interface LeadReportService {
	
	//public void downloadLeadReportByCampaignId(String campaignId) throws ParseException,Exception;

	//public void downloadLeadReport() throws ParseException,Exception;
	
	//public void downloadLeadInternalReportByCid(String campaignId) throws ParseException,Exception;
	
	//public void downloadLeadInternalReport() throws ParseException,Exception;
	
	//public void downloadClientReport() throws ParseException,Exception;
	
	//public void downloadClientReportByCampaignId(String campaignId) throws ParseException,Exception;

	public void downloadClientReportByFilter(ProspectCallSearchDTO prospectcallSearchDTO,Login login) throws ParseException,Exception;
	
	public void downloadConnectReportByFilter(ProspectCallSearchDTO prospectcallSearchDTO,HttpServletRequest request, HttpServletResponse response,Login login) throws ParseException,Exception;
	
	public void downloadZoomReport(String date) throws ParseException,Exception;
	
	public String updateLeadDetails(String campaignId, MultipartFile file)
			throws IOException;
	
	public String updateMultiCampaignLeadDetails(MultipartFile file)
			throws IOException;

	//void downloadClientReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException, Exception;

	public void downloadDemandshoreReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO, Login login) throws ParseException;

}
