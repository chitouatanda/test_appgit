package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.NewClientMapping;

public interface NewClientMappingUploadService {

	public void downloadNewTemplate(String campaignId, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException;

	public String createNewClientMappings(String campaignId, MultipartFile file) throws IOException;
	
	public List<NewClientMapping> getNewCampaignClientMappings(String campaignId);

}
