package com.xtaas.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.xtaas.db.entity.AgentStatusLog;
import com.xtaas.db.entity.PCIAnalysis;

public interface DomoUtilsService {

	public  void  postAgentStatusLogWebhook(AgentStatusLog AgentStatusLog) ;
	
	public  void  postPciAnalysisWebhook(PCIAnalysis pcian);
	
}
