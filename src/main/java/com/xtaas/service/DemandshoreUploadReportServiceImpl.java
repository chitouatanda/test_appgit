package com.xtaas.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.QaRejectReason;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.QaRejectReasonRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.domain.valueobject.QaFeedback;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.utils.XtaasConstants.LEAD_STATUS;
import com.xtaas.valueobjects.KeyValuePair;

@Service
public class DemandshoreUploadReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DemandshoreUploadReportServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	private static final String PRIMARY_CONTACT = "Primary Contact";

	@Autowired
	private QaRejectReasonRepository qaRejectReasonRepository;
	
	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;
	
	@Autowired 
	PCIAnalysisRepository pciAnalysisRepository;
	
	@Autowired
	private DomoUtilsService domoUtilsService;

	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;

	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();
	private List<String> errorList = new ArrayList<String>();
	private InputStream fileInputStream;
	private String fileName;
	private Sheet sheet;
	private 	Workbook book;
	private HashMap<Integer,List<String>>  errorMap= new HashMap<>();
	private String xlsFlieName;

	public String updateLeadDetails(MultipartFile file) throws IOException {
		
		if (!file.isEmpty()) {
			xlsFlieName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
			
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {
						book.close();
						logger.error(fileName + " file is empty");
						throw new IllegalArgumentException("File cannot be empty.");
					}
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords();
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);
		int colCounts = row.getPhysicalNumberOfCells();
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell == null || cell.getCellType() == CellType.BLANK) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
				return false;
			}
			if (cell.getCellType() != CellType.STRING) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
				return false;
			} else {
				headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
				fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
			}
		}
		return true;
	}

	private void validateExcelFileRecords() {
		errorList = new ArrayList<String>();
	//	int rowsCount = sheet.getPhysicalNumberOfRows();
		int rowsCount = ErrorHandlindutils.getNonBlankRowCount(sheet);
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processRecords(0, rowsCount, colCounts);
	}

	public void processRecords(int startPos, int EndPos, int colCounts) {
		List<LeadReportUploadDTO> leadReportDTOs = new ArrayList<LeadReportUploadDTO>();
		boolean invalidFile=false;
		for (int i = startPos; i < EndPos; i++) {
			List<String> errors = new ArrayList<String>();
			LeadReportUploadDTO leadReportDTO = new LeadReportUploadDTO();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			if (i == 0) {
				continue;
			}
			if (row == null) {
				errors.add("Record is Empty");
				errorMap.put(i, errors);
				EndPos++;
				continue;
			}

			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
//				errors.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				errors.add(" Record is empty ");
				errorMap.put(i, errors);
				EndPos++;
				continue;
			}
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("Qualified")) {
					if (cell != null) {
						String leadStatus = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (leadStatus != null && leadStatus != "") {
							if (!isLeadStatusInStandardFormat(leadStatus)) {
								errors.add("Qualified is not specified as per standard lead status list. Row No:- "
										+ (row.getRowNum() + 1));
							} else {
								if (XtaasConstants.LEAD_STATUS.ACCEPTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.ACCEPTED.toString());
								if (XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString());
								if (XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString());
								if (XtaasConstants.LEAD_STATUS.NOT_SCORED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
								if (XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString());
								if (XtaasConstants.LEAD_STATUS.REJECTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.REJECTED.toString());
								if (XtaasConstants.LEAD_STATUS.QA_REJECTED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString());
								if (XtaasConstants.LEAD_STATUS.QA_RETURNED.toString().equalsIgnoreCase(leadStatus))
									leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
							}
						}
					}
					
				} else if (columnName.equalsIgnoreCase("Disq Parameter")) {
					if (cell != null) {
						String rejectReason = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (leadReportDTO.getLeadStatus() != null) {
							if (leadReportDTO.getLeadStatus().equalsIgnoreCase("Rejected")
									|| leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_REJECTED")) {
								if (rejectReason.equalsIgnoreCase("") || rejectReason.equalsIgnoreCase(null)
										|| rejectReason.isEmpty()) {
									errors.add("Reject reason is mandatory as lead status is rejected. Row No:- "
											+ (row.getRowNum() + 1));
								} else {
									if (!setQaRejectReason(leadReportDTO, rejectReason)) {
										errors.add(
												"Reject reason is not specified as per qa rejectreason standard list. Row No:-"
														+ (row.getRowNum() + 1));
									}
								}
							}
						} /*else {
							errors.add("Lead status can't be empty for row:- " + (row.getRowNum() + 1));
						}*/
					}
					
				} else if (columnName.equalsIgnoreCase("Comments")) {
					if (cell != null) {
						String notes = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setNotes(notes);
					}
				} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("EmailAddress")
						|| columnName.equalsIgnoreCase("Email ID")) {
					if (cell != null) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (email == null || email == "" || email.isEmpty()) {
							errors.add("Email is mandatory for updating the leads. Row No:- " + (row.getRowNum() + 1));
						} else {
							leadReportDTO.setEmail(email);
						}
					}
				} else if (columnName.equalsIgnoreCase("Client Delivered")) {
					if (cell != null) {
						String clientDelivered = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setClientDelivered(clientDelivered);
					}
				} else if (columnName.equalsIgnoreCase("Campaign Name")) {
					if (cell != null) {
						String campaignName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (campaignName == null || campaignName == "" || campaignName.isEmpty()) {
							errors.add("Campaign name is mandatory for updating the leads. Row No:- "
									+ (row.getRowNum() + 1));
						} else {
							leadReportDTO.setCampaignName(campaignName);
						}
					}
				} else if (columnName.equalsIgnoreCase("QaId")) {
					if (cell != null) {
						String qaId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setQaId(qaId);
					}
				} else if (columnName.equalsIgnoreCase("Email Status")) {
					if (cell != null) {
						String emailStatus = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setEmailStatus(emailStatus);
					}
				} else if (columnName.equalsIgnoreCase("Action")) {
					if (cell != null) {
						String action = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						errorList.addAll(errors);
						leadReportDTO.setAction(action);
					}
				} else if (columnName.equalsIgnoreCase("ProspectCallId (for internal use only)")) {
					if (cell != null) {
						String prospectcallid = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setProspectCallId(prospectcallid);
					}
				}
				
				
				if (errors !=null && errors.isEmpty() && errors.size() > 0) {
					invalidFile=true;
				}
			}
			errorMap.put(i, errors);
			leadReportDTOs.add(leadReportDTO);
		}
		
		if (errorMap != null && invalidFile) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(null,book,xlsFlieName,"UploadData",null,null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//throw new IllegalArgumentException("Please Check Your Email There is Some Error in File");
		}
		else {
			for (LeadReportUploadDTO leadFileDTO : leadReportDTOs) {
				if (leadFileDTO.getAction() != null && leadFileDTO.getAction().equalsIgnoreCase("update")) {
					Campaign campaignFromDB = campaignRepository.findByExactName(leadFileDTO.getCampaignName());
					if (campaignFromDB != null) {
						updateCollection("ProspectCallLog", leadFileDTO, campaignFromDB.getId());
						updateCollection("ProspectCallLogInteraction", leadFileDTO, campaignFromDB.getId());
					}
				}
			}
		}
		
		/*
		 * if (!errorList.isEmpty() && errorList.size() != 0) { StringBuilder
		 * stringBuilder = new StringBuilder(); for (String error : errorList) {
		 * stringBuilder.append(error); stringBuilder.append("<br />"); } Organization
		 * organization = organizationRepository.findOne("DEMANDSHORE"); if
		 * (organization.getContacts() != null && organization.getContacts().size() > 0)
		 * { List<Contact> contacts = organization.getContacts(); for (Contact contact :
		 * contacts) { if (contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) &&
		 * contact.getEmail() != null && !contact.getEmail().isEmpty()) {
		 * XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com",
		 * "Lead Upload Errorlist", stringBuilder.toString());
		 * logger.debug("Lead report errorlist mail sent successfully to : " +
		 * contact.getEmail()); } } } throw new
		 * IllegalArgumentException("File not uploaded.Please check your email"); } else
		 * { for (LeadReportUploadDTO leadFileDTO : leadReportDTOs) { if
		 * (leadFileDTO.getAction() != null &&
		 * leadFileDTO.getAction().equalsIgnoreCase("update")) { Campaign campaignFromDB
		 * = campaignRepository.findByName(leadFileDTO.getCampaignName()); if
		 * (campaignFromDB != null) { updateCollection("ProspectCallLog", leadFileDTO,
		 * campaignFromDB.getId()); updateCollection("ProspectCallLogInteraction",
		 * leadFileDTO, campaignFromDB.getId()); } } } String message =
		 * "Leads updated successfully."; boolean leadReportSent = false; Organization
		 * organization = organizationRepository.findOne("DEMANDSHORE"); if
		 * (organization.getContacts() != null && organization.getContacts().size() > 0)
		 * { List<Contact> contacts = organization.getContacts(); for (Contact contact :
		 * contacts) { if (contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) &&
		 * contact.getEmail() != null && !contact.getEmail().isEmpty()) {
		 * XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com",
		 * "Lead report upload", message);
		 * logger.debug("Lead report upload mail sent successfully to : " +
		 * contact.getEmail()); leadReportSent = true; } } if (!leadReportSent) {
		 * XtaasEmailUtils.sendEmail("kchugh@xtaascorp.com",
		 * "data@xtaascorp.com", "Lead report upload", message); logger.
		 * info("Lead report upload mail sent successfully to kchugh@xtaascorp.com"); }
		 * } logger.debug("Update lead report mail sent successfully."); }
		 */
	}

	private void updateCollection(String collectionName, LeadReportUploadDTO leadReportUploadDTO, String campaignId) {
		if (collectionName.equals("ProspectCallLog")) {
			saveProspectCallLog(leadReportUploadDTO, campaignId);
		} else {
			saveProspectCallInteraction(leadReportUploadDTO, campaignId);
		}
	}

	private boolean setQaRejectReason(LeadReportUploadDTO leadReportDTO, String rejectReason) {
		List<QaRejectReason> rejectReasonsFromDB = qaRejectReasonRepository.findAll();
		QaRejectReason qaRejectReason = rejectReasonsFromDB.get(0);
		List<KeyValuePair<String, String>> rejectReasonValues = qaRejectReason.getQaRejectReason();
		for (KeyValuePair<String, String> keyValuePair : rejectReasonValues) {
			if (rejectReason.replaceAll("\\s","").equalsIgnoreCase(keyValuePair.getValue().replaceAll("\\s",""))) {
				leadReportDTO.setRejectReason(rejectReason);
				return true;
			}
		}
		return false;
	}

	private boolean isLeadStatusInStandardFormat(String leadStatus) {
		List<String> stdLeadStatuses = new ArrayList<String>();
		stdLeadStatuses.add("NOT_SCORED");
		stdLeadStatuses.add("QA_ACCEPTED");
		stdLeadStatuses.add("QA_REJECTED");
		stdLeadStatuses.add("QA_RETURNED");
		stdLeadStatuses.add("ACCEPTED");
		stdLeadStatuses.add("REJECTED");
		if (stdLeadStatuses.contains(leadStatus)) {
			return true;
		}
		return false;
	}

	private void saveProspectCallLog(LeadReportUploadDTO leadReportUploadDTO, String campaignId) {
		ProspectCallLog prospectFromDB = new ProspectCallLog();
		List<ProspectCallLog> prospects = new ArrayList<ProspectCallLog>();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		prospects = prospectCallLogRepository.findAllByProspectCallId(leadReportUploadDTO.getProspectCallId(),pageable);
		if (prospects != null && prospects.size() > 0) {
			prospectFromDB = prospects.get(0);
		}
		if (prospectFromDB != null) {

			if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_RETURNED.toString())
					|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())
					|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
				if (prospectFromDB.getQaFeedback() != null) {
					List<FeedbackSectionResponse> feedbackSectionList = prospectFromDB.getQaFeedback()
							.getFeedbackResponseList();
					if (feedbackSectionList.size() > 0) {
						Optional<FeedbackSectionResponse> rejectReasonFeedback = feedbackSectionList.stream().filter(
								leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
								.findAny();
						List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
								.getResponseAttributes();
						if (rejectReasonAttribute.size() > 0) {
							// rejectReasonAttribute.get(0).setFeedback("NO");
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());
							// rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
						}
					}
				}
			}

			if (prospectFromDB.getQaFeedback() != null) {
				List<FeedbackSectionResponse> feedbackSectionList = prospectFromDB.getQaFeedback()
						.getFeedbackResponseList();
				if (feedbackSectionList.size() > 0) {
					Optional<FeedbackSectionResponse> rejectReasonFeedback = feedbackSectionList.stream().filter(
							leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
							.findAny();
					List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
							.getResponseAttributes();
					if (leadReportUploadDTO.getNotes() != null && !leadReportUploadDTO.getNotes().isEmpty()) {
						rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
					}
					if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_ACCEPTED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())) {
						rejectReasonAttribute.get(0).setFeedback("YES");
					} else if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_RETURNED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())
							|| leadReportUploadDTO.getLeadStatus()
									.equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())) {
						rejectReasonAttribute.get(0).setFeedback("NO");
					}
				}
			}

			if (prospectFromDB.getQaFeedback() == null) {
				List<FeedbackSectionResponse> feedbackResponseList = new ArrayList<FeedbackSectionResponse>();
				List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
				QaFeedback qf = new QaFeedback(leadReportUploadDTO.getQaId(), null, feedbackResponseList, 0.0, null,
						qaAnswers, "");
				prospectFromDB.setQaFeedback(qf);
				List<FeedbackSectionResponse> feedbackSectionList = prospectFromDB.getQaFeedback()
						.getFeedbackResponseList();
				// if (feedbackSectionList.size() > 0) {
				// Optional<FeedbackSectionResponse> rejectReasonFeedback = new Optiona();
				List<FeedbackResponseAttribute> rejectReasonAttribute = new ArrayList<FeedbackResponseAttribute>();
				// if (rejectReasonAttribute.size() > 0) {
				FeedbackResponseAttribute fra = new FeedbackResponseAttribute();
				if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_ACCEPTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())) {
					fra.setFeedback("YES");
				} else if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_RETURNED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())) {
					fra.setFeedback("NO");
				}
				fra.setRejectionReason(leadReportUploadDTO.getRejectReason());
				fra.setAttributeComment(leadReportUploadDTO.getNotes());
				// rejectReasonAttribute.get(0).setFeedback("NO");
				fra.setRejectionReason(leadReportUploadDTO.getRejectReason());
				rejectReasonAttribute.add(fra);
				FeedbackSectionResponse fsr = new FeedbackSectionResponse("Lead Validation", rejectReasonAttribute);
				List<FeedbackSectionResponse> fsrList = new ArrayList<FeedbackSectionResponse>();
				fsrList.add(fsr);
				prospectFromDB.getQaFeedback().setFeedbackResponseList(fsrList);
				// rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
				// }
				// }
				// QaFeedback qf = new QaFeedback(qaId, secQaId, feedbackResponseList,
				// overallScore, feedbackTime, qaAnswers);
			}

			if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("YES")) {
				prospectFromDB.getProspectCall().setClientDelivered("YES");
			} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("NO")) {
				prospectFromDB.getProspectCall().setClientDelivered("NO");
			} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("IN HAND")) {
				prospectFromDB.getProspectCall().setClientDelivered("IN HAND");
			}

			if (leadReportUploadDTO.getEmailStatus() != null && !leadReportUploadDTO.getEmailStatus().isEmpty()) {
				if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("NOT-BOUNCED"))
					prospectFromDB.getProspectCall().setEmailBounce(false);
				else if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("BOUNCED"))
					prospectFromDB.getProspectCall().setEmailBounce(true);
			}

			prospectFromDB.getProspectCall().setLeadStatus(leadReportUploadDTO.getLeadStatus());
			
			/**
			 * DATE :- 06/01/2020 
			 * When leadstatus is ACCEPTED or REJECTED then prospect status in DB should be stamped as QA_COMPLETE.
			 */
			if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())
					|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
				prospectFromDB.setStatus(ProspectCallStatus.QA_COMPLETE);
			}
			prospectCallLogRepository.save(prospectFromDB);
			logger.debug("saveProspectCallLog(): Updated prospectCallLog having id: [{}]", prospectFromDB.getId());
		}
	}

	private void saveProspectCallInteraction(LeadReportUploadDTO leadReportUploadDTO, String campaignId) {
		ProspectCallInteraction prospectInteractionFromDB = new ProspectCallInteraction();
		prospectInteractionFromDB = prospectCallInteractionRepository.getProspectByCIDAndpcid(campaignId,
				leadReportUploadDTO.getProspectCallId());
		//PCIAnalysis pcian = pciAnalysisRepository.getProspectByCIDAndEmail(campaignId,leadReportUploadDTO.getEmail());
		if (prospectInteractionFromDB != null) {
			if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())
					|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
				if (prospectInteractionFromDB.getQaFeedback() != null) {
					List<FeedbackSectionResponse> feedbackSectionList = prospectInteractionFromDB.getQaFeedback()
							.getFeedbackResponseList();
					if (feedbackSectionList.size() > 0) {
						Optional<FeedbackSectionResponse> rejectReasonFeedback = feedbackSectionList.stream().filter(
								leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
								.findAny();
						List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
								.getResponseAttributes();
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setFeedback("NO");
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());
						}
					}
				}
			}
			if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("YES")) {
				prospectInteractionFromDB.getProspectCall().setClientDelivered("YES");
			} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("NO")) {
				prospectInteractionFromDB.getProspectCall().setClientDelivered("NO");
			} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("IN HAND")) {
				prospectInteractionFromDB.getProspectCall().setClientDelivered("IN HAND");
			}
			if (leadReportUploadDTO.getEmailStatus() != null && !leadReportUploadDTO.getEmailStatus().isEmpty()) {
				if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("NOT-BOUNCED"))
					prospectInteractionFromDB.getProspectCall().setEmailBounce(false);
				else if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("BOUNCED"))
					prospectInteractionFromDB.getProspectCall().setEmailBounce(true);
			}
			/**
			 * DATE :- 06/01/2020 
			 * When leadstatus is ACCEPTED or REJECTED then prospect status in DB should be stamped as QA_COMPLETE.
			 */
			if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())
					|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
				prospectInteractionFromDB.setStatus(ProspectCallStatus.QA_COMPLETE);
			}
			prospectInteractionFromDB.getProspectCall().setLeadStatus(leadReportUploadDTO.getLeadStatus());
			prospectCallInteractionRepository.save(prospectInteractionFromDB);
			String agentName = null;
			String qaName = null;
			if(prospectInteractionFromDB.getQaFeedback()!=null && prospectInteractionFromDB.getQaFeedback().getQaId()!=null){
				Login login = loginRepository.findOneById(prospectInteractionFromDB.getQaFeedback().getQaId());
				if(login!=null){
					qaName = login.getName();
				}
			}
			if(prospectInteractionFromDB.getProspectCall()!=null && prospectInteractionFromDB.getProspectCall().getAgentId()!=null){
				Login login= loginRepository.findOneById(prospectInteractionFromDB.getProspectCall().getAgentId());
				if(login!=null){
					agentName = login.getName();
				}
			}
			Campaign campaign = campaignRepository.findOneById(prospectInteractionFromDB.getProspectCall().getCampaignId());
			PCIAnalysis pcian = new PCIAnalysis();

			pcian = pcian.toPCIAnalysis(pcian,prospectInteractionFromDB, agentName, qaName, campaign);

			pcian.setCall_attempted(0);
			pcian.setCall_connected(0);
			pcian.setCall_contacted(0);
			pcian.setInteraction_success(0);
			pcian.setLeadStatusUpdated(1);

			pciAnalysisRepository.save(pcian);
			domoUtilsService.postPciAnalysisWebhook(pcian);

			logger.debug("saveProspectCallInteraction(): Updated prospectCallInteraction having id: [{}]",
					prospectInteractionFromDB.getId());
		}
	}

	public class LeadReportUploadDTO {
		public String  prospectCallId;
		public String campaignName;
		public String leadStatus;
		public String rejectReason;
		public String notes;
		public String action;
		public String email;
		public String clientDelivered;
		public String qaId;
		public String emailStatus;

		public String getQaId() {
			return qaId;
		}

		public void setQaId(String qaId) {
			this.qaId = qaId;
		}

		public String getCampaignName() {
			return campaignName;
		}

		public void setCampaignName(String campaignName) {
			this.campaignName = campaignName;
		}

		public String getLeadStatus() {
			return leadStatus;
		}

		public void setLeadStatus(String leadStatus) {
			this.leadStatus = leadStatus;
		}

		public String getRejectReason() {
			return rejectReason;
		}

		public void setRejectReason(String rejectReason) {
			this.rejectReason = rejectReason;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getClientDelivered() {
			return clientDelivered;
		}

		public void setClientDelivered(String clientDelivered) {
			this.clientDelivered = clientDelivered;
		}

		public String getEmailStatus() {
			return emailStatus;
		}

		public void setEmailStatus(String emailStatus) {
			this.emailStatus = emailStatus;
		}

		public String getProspectCallId() {
			return prospectCallId;
		}

		public void setProspectCallId(String prospectCallId) {
			this.prospectCallId = prospectCallId;
		}
		
		
	}

}
