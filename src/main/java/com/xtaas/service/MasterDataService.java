package com.xtaas.service;

import com.xtaas.db.entity.MasterData;

public interface MasterDataService {
	public MasterData getMasterData(String id);
}
