package com.xtaas.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.utils.XtaasUtils;

@Service
public class SuppressionListServiceImpl implements SuppressionListService {

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Override
	public boolean isEmailSuppressed(String emailId, String campaignId, String organizationId,
			boolean isMdFiveSuppressionCheck) {
		boolean isSuppressed = false;
		if (!StringUtils.isEmpty(emailId) && !StringUtils.isEmpty(campaignId) && !StringUtils.isEmpty(organizationId)) {
			int emailSuppressCount = suppressionListNewRepository.countByCampaignEmailOrganization(campaignId, emailId,
					organizationId);
			if (emailSuppressCount == 0 && isMdFiveSuppressionCheck) {
				emailSuppressCount = suppressionListNewRepository.countByCampaignEmailOrganization(campaignId,
						XtaasUtils.convertStringtoMD5Hex(emailId), organizationId);
			}
			if (emailSuppressCount > 0) {
				isSuppressed = true;
			}
		}
		return isSuppressed;
	}

	@Override
	public boolean isEmailSuppressed(String emailId, String campaignId, String organizationId) {
		boolean isSuppressed = false;
		if (!StringUtils.isEmpty(emailId) && !StringUtils.isEmpty(campaignId) && !StringUtils.isEmpty(organizationId)) {
			int emailSuppressCount = suppressionListNewRepository.countByCampaignEmailOrganization(campaignId, emailId,
					organizationId);
			if (emailSuppressCount > 0) {
				isSuppressed = true;
			}
		}
		return isSuppressed;
	}

}
