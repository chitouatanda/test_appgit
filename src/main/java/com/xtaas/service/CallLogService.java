/**
 * 
 */
package com.xtaas.service;

import java.util.List;

import com.xtaas.db.entity.CallAbandonOptOutRequest;
import com.xtaas.db.entity.CallLog;

/**
 * @author djain
 *
 */
public interface CallLogService {
	
	public void saveCallLog(CallLog callLog);
	
	public void saveCallLogs(List<CallLog> callLogs);
	
	public void saveCallAbandonOptOutRequest(CallAbandonOptOutRequest optOutRequest);
	
	public CallLog findCallLog(String callSid);
	
	public List<CallLog> findCallLogsByConferenceSid(String conferenceSid);
}
