package com.xtaas.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xtaas.web.dto.TwilioCallLogFilterRequestDTO;

public interface TwilioService {

	public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response);

	public String handleConferenceWait(HttpServletRequest request, HttpServletResponse response);

	public String handleProspectCallPreviewMode(HttpServletRequest request, HttpServletResponse response);

	public String handleCallStatus(HttpServletRequest request, HttpServletResponse response);

	public String holdCall(HttpServletRequest request, HttpServletResponse response);

	public String unHoldCall(HttpServletRequest request, HttpServletResponse response);

	public String dialProspectPhone(String prospectCallId);

	public String tagLeadForNumber(TwilioCallLogFilterRequestDTO twilioCallLogFilterDTO);

}
