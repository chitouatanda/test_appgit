package com.xtaas.service;

import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.entity.SupervisorStats;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public interface ManualProspectSearchService {

	List<Prospect> searchManualProspectDetails(String campaignId, Prospect prospectInfo);

	Prospect searchProspectFromGlobalContact(String sourceId);
}
