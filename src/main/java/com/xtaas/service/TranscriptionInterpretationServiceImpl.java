package com.xtaas.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.web.dto.InterpreterInputDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class TranscriptionInterpretationServiceImpl implements TranscriptionInterpretationService {

    private final Logger logger = LoggerFactory.getLogger(TranscriptionInterpretationServiceImpl.class);
    private final static String interpreterAPIUrl = "https://api.us-south.assistant.watson.cloud.ibm.com/instances/ddc3780e-1b5a-4a4e-bff6-b98406af751d/v2/assistants/7bf5ec64-aebb-46d3-b1b9-df00a62703f1//message?version=2020-04-01";
    private HashMap<String, String> commands = new HashMap<>();

    @Autowired
	HttpService httpService;

    private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "*/*");
		headers.add("content-type", "application/json");
		headers.add("Authorization", "Basic YXBpa2V5OmJ2aFlMeHV3U24wSmlRUHZ2SnF5QlRZNkVwVGFINEIzT1VHQmp2X0xpbGhN");
		return headers;
	}

    public TranscriptionInterpretationServiceImpl() {
        this.commands.put("DialByZero", "0");
    }

    @Override
    public String getInterpretationCommandTest(String transcription) {
        return findCommandValue("DialByZero", null);
    }

    @Override
    public String getInterpretationCommand(String transcription) {
        InterpreterInputDTO data = new InterpreterInputDTO(transcription);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map<String, Object> output = (Map<String, Object>) httpService.post(interpreterAPIUrl, headers(),
            			new ParameterizedTypeReference<Map<String, Object>>() {}, mapper.writeValueAsString(data)).get("output");
            if (output != null) {
                if (output.containsKey("intents")) {
                    List<Map<String, String>> intents = (List<Map<String, String>>) output.get("intents");
                    if (intents != null && intents.size() > 0) {
                        Map<String, String> intent = intents.get(0);
                        String command = intent.get("intent");
                        String commandValue = findCommandValue(command, output);
                        logger.info("TranscriptionInterpretationServiceImpl===> Command : " + command + " Command value : " + commandValue);
                        return commandValue;
                    }
                }
            } 
        } catch (Exception e) {
            logger.error("Error in TranscriptionInterationServiceImpl ========> " + e.getStackTrace().toString());
            return null;
        }
        logger.info("TranscriptionInterpretationServiceImpl===> No Command recieved");
        return null;
    }

    private String findCommandValue (String command, Map<String, Object> output) {
        switch(command){
            case "DialForAssistance" :
                if (output.containsKey("entities")) {
                    List<Map<String, String>> entities = (List<Map<String, String>>) output.get("entities");
                    if (entities != null && entities.size() > 0) {
                        Map<String, String> entity = entities.get(0);
                        String entityType = entity.get("entity");
                        if (entityType.equalsIgnoreCase("Number")){
                            String value = entity.get("value");
                            if (value != null && !value.isEmpty()) {
                                try {
                                    Integer.parseInt(value);
                                    return value;
                                } catch (NumberFormatException nfe) {
                                    return getIntegerValueFromString(value);
                                }
                            }
                        }
                    }
                }
                break;
            case "DialExtension" :
                return "extension";
            case "HoldLine" :
                return "hold";
            default :
                return null;

        }
        // String value = commands.get(command);
        return null;
    }

    private String getIntegerValueFromString (String numbeString) {
        String intValue = null;
        switch(numbeString) {
            case "zero" :
                intValue = "0";
                break;
            case "one" :
                intValue = "1";
                break;
            case "two" :
                intValue = "2";
                break;
            case "three" :
                intValue = "3";
                break;
            case "four" :
                intValue = "4";
                break;
            case "five" :
                intValue = "5";
                break;
            case "six" :
                intValue = "6";
                break;
            case "seven" :
                intValue = "7";
                break;
            case "eight" :
                intValue = "8";
                break;
            case "9" :
                intValue = "9";
                break;
            default:
                intValue = null;

        }
        return intValue;
    }
    
}
