package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.AgentMetricsService;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.SupervisorService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.QaFeedbackForm;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryCustom;
import com.xtaas.db.repository.QaFeedbackFormRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.pusher.Pusher;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentAllocationDTO;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentMetricsDTO;
import com.xtaas.web.dto.AgentReportDTO;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.CampaignReportDTO;
import com.xtaas.web.dto.QaFeedbackFormDTO;
import com.xtaas.web.dto.StatsDTO;
import com.xtaas.worker.AgentReportThread;
import com.xtaas.worker.CampaignReportThread;

@Service
public class SupervisorServiceImpl implements SupervisorService {
	private static final Logger logger = LoggerFactory.getLogger(SupervisorServiceImpl.class);
	
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private AgentRegistrationService agentRegistrationService;
	
	@Autowired
	private QaFeedbackFormRepository qaFeedbackFormRepository;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private AgentMetricsService agentMetricsService;
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;	
	
	private String bucketName = "xtaasreports";
	
	private static final String SUFFIX = "/";
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public AgentAllocationDTO getAgentAllocation() {
		AgentAllocationDTO agentAllocation = new AgentAllocationDTO();
		List<AgentSearchResult> agents = agentService.searchBySupervisor();
		for (AgentSearchResult agent : agents) {
			AgentCampaignDTO campaign = agentService.getCurrentAllocatedCampaign(agent.getId());
			
			//find the current status
			AgentCall agentCall = agentRegistrationService.getAgentCall(agent.getId());
			if (agentCall != null) {
				agent.setCurrentStatus(agentCall.getCurrentStatus());
			} else {
				agent.setCurrentStatus(AgentStatus.OFFLINE); //if agent is not in agentRegistrationService then he is OFFLINE
			}
			
			if (campaign == null || campaign.getId() == null) {
				agentAllocation.addAllocation("UNALLOCATED", agent); //TODO: Return AgentMetricsDTO instead of AgentSearchResult
			} else {
				agent.setCampaignTemporaryAllocated(campaign.isTempAllocation());
				agentAllocation.addAllocation(campaign.getId(), agent);
			}
		}
		return agentAllocation;
	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public boolean createFeedbackForm(QaFeedbackFormDTO qaFeedbackFormDTO)
			throws DuplicateKeyException {
		QaFeedbackForm qaFeedbackForm = qaFeedbackFormDTO.toQaFeedbackForm();
		qaFeedbackForm.setId(qaFeedbackFormDTO.getCampaignId());
		qaFeedbackFormRepository.save(qaFeedbackForm);
		return true;
	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_AGENT})
	public void inviteSupervisorToJoinCall(String agentId) {
		Team team = teamRepository.findByAgent(agentId);
		if (team != null) {
			pushMessageToSupervisor(team.getSupervisor().getResourceId(), "join_call_invitation", "{\"agentId\": \"" + agentId + "\"}");
		}
	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_AGENT})
	public void pushAgentMetricsToSupervisor(String agentId, CallStatusDTO callStatusDTO) {
		AgentMetricsDTO agentMetricsDTO = agentMetricsService.getAgentMetrics(agentId, callStatusDTO);
		
		Team team = teamRepository.findByAgent(agentMetricsDTO.getAgentId());
		if (team == null) {
			logger.error("pushAgentMetricsToSupervisor() : Agent [{}] does not have any supervisor", agentId);
			return;
		}
		try {
			pushMessageToSupervisor(team.getSupervisor().getResourceId(), "agent_list", JSONUtils.toJson(agentMetricsDTO));
		} catch (IOException e) {
			logger.error("pushAgentMetricsToSupervisor() : Error occurred.", e);
		}
	}
	
	private void pushMessageToSupervisor(String supervisorId, String event, String message) {
		logger.debug("pushMessageToSupervisor() : Event = [{}], Message = [{}], Supervisor = " + supervisorId , event, message);
		try {
			Pusher.triggerPush(supervisorId, event, message);
		} catch (IOException e) {
			logger.error("pushMessageToSupervisor() : Error occurred while pushing message to agent: " + supervisorId + " Msg:" + message, e);
		}
	}

	@Override
	public void downloadCampaignReport(Login login) throws IOException {
		int srNo = 1;
		int totalRecordsAttempted = 0;
		int totalConnects = 0;
		int totalContacts = 0;
		int totalSuccess = 0;
		int totalDialedRecords = 0;
		
		List<CampaignReportDTO> campaignReportDTOs = new ArrayList<CampaignReportDTO>();
		
		List<StatsDTO> stats = campaignRepository.getActiveCampaignsForSupervisorStats(login.getId());
		SupervisorStats supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Campaign Report","QUEUED");
		supervisorStats.setStatus("INPROCESS");
		supervisorStatsRepository.save(supervisorStats);
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Date date = calendar.getTime();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date fromDateStr = calendar.getTime();
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		for (StatsDTO stat : stats) {
			int connectCounter=0;
			Campaign campaign =campaignRepository.findOneById(stat.getId());
			float dialsToSuccess = 0;
			float connectPercentage = 0;
			CampaignReportDTO campaignReportDTO = new CampaignReportDTO();
			int recordsAttempted = prospectCallInteractionRepository.getCampaignRecordsAttemptedCount(stat.getId(),
					fromDateStr);
			int agentDailedByCampaign = prospectCallInteractionRepository.getCampaignRecordsDialedByAgents(
					fromDateStr,stat.getId());
			int recordsContacted = prospectCallInteractionRepository.getCampaignContactedRecordsCount(stat.getId(),
					fromDateStr);
			int recordsConnected = prospectCallInteractionRepository.getCampaignConnectedRecordsCount(stat.getId(),
					fromDateStr);
			List<ProspectCallInteraction> pciList = prospectCallInteractionRepository.getCampaignConnectedRecordsCountList(stat.getId(),
					fromDateStr);
			if(campaign.isSimpleDisposition())  {
				for(ProspectCallInteraction pci : pciList) {
					if(pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
						connectCounter =connectCounter+1;
					}else if(pci.getProspectCall().getProspect().isDirectPhone() && pci.getProspectCall().getCallDuration()>15) {
						connectCounter =connectCounter+1;
					}else if(!pci.getProspectCall().getProspect().isDirectPhone() 
								&& pci.getProspectCall().getCallDuration()>30) {
						connectCounter =connectCounter+1;
					}else if(pci.getProspectCall().getProspect().isDirectPhone() && pci.getProspectCall().getCallDuration()<=15
							&& pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
						connectCounter =connectCounter+1;
					}else if(!pci.getProspectCall().getProspect().isDirectPhone() 
							&& pci.getProspectCall().getCallDuration()<=30 
							&& pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
						connectCounter =connectCounter+1;
					}

				}
			}else {
				connectCounter  = recordsConnected;
			}
			
			int successRecords = prospectCallInteractionRepository.getCampaignSuccessRecordCount(stat.getId(),
					fromDateStr);

			if (successRecords > 0) {
				dialsToSuccess = (float)recordsAttempted / (float)successRecords;
			}
			if (agentDailedByCampaign > 0 ) {
				connectPercentage = ((float)connectCounter / (float)agentDailedByCampaign) * 100;
			}
			campaignReportDTO.setCampaignName(stat.getName());
			campaignReportDTO.setRecordsAttempted(recordsAttempted);
			campaignReportDTO.setAgentDailedinCampaign(agentDailedByCampaign);
			campaignReportDTO.setContacts(recordsContacted);
			campaignReportDTO.setConnects(connectCounter);
			campaignReportDTO.setSuccess(successRecords);
			campaignReportDTO.setDialsToSuccess(Float.parseFloat(decimalFormat.format(dialsToSuccess)));
			campaignReportDTO.setConnectPercentage(Float.parseFloat(decimalFormat.format(connectPercentage)));
			if (recordsAttempted > 0) {
				if(stat.getId().equalsIgnoreCase("5eefa4f2e4b0ae3051964154")) {
					System.out.println("Here");
				}
				campaignReportDTOs.add(campaignReportDTO);	
			}
		}
		Collections.sort(campaignReportDTOs,
				Comparator.comparingInt(CampaignReportDTO::getRecordsAttempted).reversed());
		for (CampaignReportDTO campaignReportDTO2 : campaignReportDTOs) {
			campaignReportDTO2.setSrNo(srNo++);
			totalRecordsAttempted = totalRecordsAttempted + campaignReportDTO2.getRecordsAttempted();
			totalDialedRecords = totalDialedRecords + campaignReportDTO2.getAgentDailedinCampaign();
			totalContacts = totalContacts + campaignReportDTO2.getContacts();
			totalConnects = totalConnects + campaignReportDTO2.getConnects();
			totalSuccess = totalSuccess + campaignReportDTO2.getSuccess();
		}
		try {
			createCampaignReport(campaignReportDTOs, login, totalRecordsAttempted,totalDialedRecords, totalContacts, totalConnects, totalSuccess);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Campaign Report","INPROCESS");
			supervisorStats.setStatus("COMPLETED");
			supervisorStatsRepository.save(supervisorStats);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while deleting supervisor stats.");
		}

	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public String downloadCampaignReportCheckAndSave(Login login) throws ParseException, Exception {
		try {
			SupervisorStats supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Campaign Report","QUEUED");
			if(supervisorStats == null) {
				supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Campaign Report","INPROCESS");
			}
			if(supervisorStats == null) {
				SupervisorStats tempSupervisorStats = new SupervisorStats();
				tempSupervisorStats.setSupervisorId(login.getId());
				tempSupervisorStats.setStatus("QUEUED");
				tempSupervisorStats.setType("Campaign Report");
				tempSupervisorStats.setDate(new Date());
				supervisorStatsRepository.save(tempSupervisorStats);
			} else {
				return "Already a process is running to download stats, Try after sometime.";
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while saving supervisor stats for Campaign report.");
		}
		CampaignReportThread campaignReportThread = new CampaignReportThread(login);
		Thread thread = new Thread(campaignReportThread);
		thread.start();
		return "Success";
	}
	
	public void createCampaignReport(List<CampaignReportDTO> campaignReportDTOs, Login login,Integer totalRecordsAttempted, Integer totalDailedRecords, Integer totalContacts, Integer totalConnects, Integer totalSuccess) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeaders(headerRow);
		if (campaignReportDTOs.size() > 0) {
			for (CampaignReportDTO campaignReportDto : campaignReportDTOs) {
				Row row = sheet.createRow(++rowCount);
				writeToExcel(campaignReportDto, row, workbook);
			}
		}
		int totalRows = sheet.getPhysicalNumberOfRows();
		Row row = sheet.createRow(totalRows + 1);
		writeCampaignTotals(row,workbook,totalRecordsAttempted,totalDailedRecords,totalContacts,totalConnects,totalSuccess);
		////////
		Date date1 = Calendar.getInstance().getTime();  
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
		String strDate = dateFormat.format(date1); 
		//////////
		String fileName = login.getOrganization() + "_" + UUID.randomUUID() + "_" + strDate + "_CampaignReport.xlsx";
		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
//		List<File> fileNames = new ArrayList<File>();
//		fileNames.add(file);
		workbook.close();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		String link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketName, fileName,
				file);
		String linkrefNotice = "<" + link + ">";
		String linkref = "<a href=\" "+link+" \">Download here</a>";
		String subject = "XTaaS - Campaign Stats - " + dateStr + " - " + login.getId();
		String message = "Campaign Report Download Link : " + linkref;
		
		PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Campaign Report Download Link : Download here" + linkrefNotice;
		teamNoticeSerive.saveUserNotices(login.getId(), notice);
		
		Organization organization = organizationRepository.findOneById(login.getOrganization());
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

		if (login.getReportEmails() != null && login.getReportEmails().size() > 0) {
			for(String email : login.getReportEmails()) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, message);
			}
		}else {
			XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", subject, message);
		}
		logger.debug("Campaign report sent successfully to : " + login.getEmail());
	}
	
	private void writeHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Sr. No");
		cell = headerRow.createCell(1);
		cell.setCellValue("Campaign");
		cell = headerRow.createCell(2);
		cell.setCellValue("Records Attempted");
		cell = headerRow.createCell(3);
		cell.setCellValue("Agent Dialed");
		cell = headerRow.createCell(4);
		cell.setCellValue("Contacts");
		cell = headerRow.createCell(5);
		cell.setCellValue("Connects");
		cell = headerRow.createCell(6);
		cell.setCellValue("Success");
		cell = headerRow.createCell(7);
		cell.setCellValue("Dials to Success");
		cell = headerRow.createCell(8);
		cell.setCellValue("Connect%");
	}
	
	private void writeToExcel(CampaignReportDTO campaignReportDto, Row row, Workbook workbook) {
		Cell cell = row.createCell(0);
		cell.setCellValue(campaignReportDto.getSrNo());
		cell = row.createCell(1);
		cell.setCellValue(campaignReportDto.getCampaignName());
		cell = row.createCell(2);
		cell.setCellValue(campaignReportDto.getRecordsAttempted());
		cell = row.createCell(3);
		cell.setCellValue(campaignReportDto.getAgentDailedinCampaign());
		cell = row.createCell(4);
		cell.setCellValue(campaignReportDto.getContacts());
		cell = row.createCell(5);
		cell.setCellValue(campaignReportDto.getConnects());
		cell = row.createCell(6);
		cell.setCellValue(campaignReportDto.getSuccess());
		cell = row.createCell(7);
		cell.setCellValue(campaignReportDto.getDialsToSuccess());
		cell = row.createCell(8);
		cell.setCellValue(campaignReportDto.getConnectPercentage());

	}

	private void writeCampaignTotals(Row row,Workbook workbook,Integer totalRecordsAttempted,Integer totalDailedRecords, Integer totalContacts, Integer totalConnects, Integer totalSuccess) {
		Cell cell = row.createCell(2);
		cell.setCellValue("Total= " + totalRecordsAttempted);
		cell = row.createCell(3);
		cell.setCellValue("Total= " + totalDailedRecords);
		cell = row.createCell(4);
		cell.setCellValue("Total= " + totalContacts);
		cell = row.createCell(5);
		cell.setCellValue("Total= " + totalConnects);
		cell = row.createCell(6);
		cell.setCellValue("Total= " + totalSuccess);
	}

	@Override
	public void downloadAgentReport(Login login) {
		int srNo = 1;
		int totalRecordsAttempted = 0;
		int totalConnects = 0;
		int totalContacts = 0;
		int totalSuccess = 0;
		List<AgentReportDTO> agentReportDTOs = new ArrayList<AgentReportDTO>();
		List<StatsDTO> activeCampaigns = campaignRepository.getActiveCampaignsForSupervisorStats(login.getId());
		SupervisorStats supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Agent Report","QUEUED");
		supervisorStats.setStatus("INPROCESS");
		supervisorStatsRepository.save(supervisorStats);
		Map<String, CampaignDTO> cidMap = new HashMap<String, CampaignDTO>();
		List<String> cids = new ArrayList<String>();
		for(StatsDTO stat : activeCampaigns) {
			cids.add(stat.getId());
		}
		List<CampaignDTO> campaingList = campaignRepository.findByIdsWithProjectedFields(cids, Arrays.asList("simpleDisposition"));
		for(CampaignDTO c : campaingList) {
			cidMap.put(c.getId(), c);
		}
		Team team = teamRepository.getAgents(login.getId());
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Date date = calendar.getTime();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date fromDateStr = calendar.getTime();
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		for (StatsDTO activeCampaign : activeCampaigns) {
			CampaignDTO campaign = cidMap.get(activeCampaign.getId());

			for (TeamResource stat : team.getAgents()) {
				int connectCounter =0;
				float dialsToSuccess = 0;
				float connectPercentage = 0;
				AgentReportDTO agentReportDTO = new AgentReportDTO();
				int recordsAttempted = prospectCallInteractionRepository
						.getAgentRecordsAttemptedCount(stat.getResourceId(), fromDateStr, activeCampaign.getId());
				int recordsContacted = prospectCallInteractionRepository
						.getAgentContactedRecordsCount(stat.getResourceId(), fromDateStr, activeCampaign.getId());
				int recordsConnected = prospectCallInteractionRepository
						.getAgentConnectedRecordsCount(stat.getResourceId(), fromDateStr, activeCampaign.getId());
				List<ProspectCallInteraction>  pciList  = prospectCallInteractionRepository
				.getAgentConnectedRecordsCountList(stat.getResourceId(), fromDateStr, activeCampaign.getId());
				if(campaign.isSimpleDisposition())  {
					for(ProspectCallInteraction pci : pciList) {
						if(pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
							connectCounter =connectCounter+1;
						}else if(pci.getProspectCall().getProspect().isDirectPhone() && pci.getProspectCall().getCallDuration()>15) {
							connectCounter =connectCounter+1;
						}else if(!pci.getProspectCall().getProspect().isDirectPhone() 
									&& pci.getProspectCall().getCallDuration()>30) {
							connectCounter =connectCounter+1;
						}else if(pci.getProspectCall().getProspect().isDirectPhone() && pci.getProspectCall().getCallDuration()<=15
								&& pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
							connectCounter =connectCounter+1;
						}else if(!pci.getProspectCall().getProspect().isDirectPhone() 
								&& pci.getProspectCall().getCallDuration()<=30 
								&& pci.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
							connectCounter =connectCounter+1;
						}
	
					}
				}else {
					connectCounter  = recordsConnected;
				}
				int successRecords = prospectCallInteractionRepository.getAgentSuccessRecordCount(stat.getResourceId(),
						fromDateStr, activeCampaign.getId());

				if (successRecords > 0) {
					dialsToSuccess = (float) recordsAttempted / (float) successRecords;
				}
				if(recordsAttempted > 0) {
					connectPercentage = ((float) connectCounter / (float) recordsAttempted) * 100;
				}
				agentReportDTO.setCampaignName(activeCampaign.getName());
				agentReportDTO.setAgentName(stat.getResourceId());
				agentReportDTO.setRecordsAttempted(recordsAttempted);
				agentReportDTO.setContacts(recordsContacted);
				agentReportDTO.setConnects(connectCounter);
				agentReportDTO.setSuccess(successRecords);
				agentReportDTO.setDialsToSuccess(Float.parseFloat(decimalFormat.format(dialsToSuccess)));
				agentReportDTO.setConnectPercentage(Float.parseFloat(decimalFormat.format(connectPercentage)));
				if (recordsAttempted > 0) {
					agentReportDTOs.add(agentReportDTO);
				}
			}
		}
		Collections.sort(agentReportDTOs, 
			    Comparator.comparingInt(AgentReportDTO::getRecordsAttempted).reversed());
		for (AgentReportDTO agentReportDTO2 : agentReportDTOs) {
			agentReportDTO2.setSrNo(srNo++);
			totalRecordsAttempted = totalRecordsAttempted + agentReportDTO2.getRecordsAttempted();
			totalContacts = totalContacts + agentReportDTO2.getContacts();
			totalConnects = totalConnects + agentReportDTO2.getConnects();
			totalSuccess = totalSuccess + agentReportDTO2.getSuccess();
		}
		try {
			createAgentReport(agentReportDTOs, login, totalRecordsAttempted, totalContacts, totalConnects, totalSuccess);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Agent Report","INPROCESS");
			supervisorStats.setStatus("COMPLETED");
			supervisorStatsRepository.save(supervisorStats);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while deleting supervisor stats.");
		}
	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public String downloadAgentReportCheckAndSave(Login login) throws ParseException, Exception {
		try {
			SupervisorStats supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Agent Report","QUEUED");
			if(supervisorStats == null) {
				supervisorStats = supervisorStatsRepository.findBySupervisorIdAndStatus(login.getId(),"Agent Report","INPROCESS");
			}			if(supervisorStats == null) {
				SupervisorStats tempSupervisorStats = new SupervisorStats();
				tempSupervisorStats.setSupervisorId(login.getId());
				tempSupervisorStats.setStatus("QUEUED");
				tempSupervisorStats.setType("Agent Report");
				tempSupervisorStats.setDate(new Date());
				supervisorStatsRepository.save(tempSupervisorStats);
			} else {
				return "Already a process is running to download stats, Try after sometime.";
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while saving supervisor stats for Agent report.");
		}
		AgentReportThread agentReportThread = new AgentReportThread(login);
		Thread thread = new Thread(agentReportThread);
		thread.start();
		return "Success";
	}
	
	public void createAgentReport(List<AgentReportDTO> agentReportDTOs, Login login, Integer totalRecordsAttempted, Integer totalContacts, Integer totalConnects, Integer totalSuccess) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeadersForAgentReport(headerRow);
		if (agentReportDTOs.size() > 0) {
			for (AgentReportDTO agentReportDto : agentReportDTOs) {
				Row row = sheet.createRow(++rowCount);
				writeToExcelFile(agentReportDto, row, workbook);
			}
		}
		int totalRows = sheet.getPhysicalNumberOfRows();
		Row row = sheet.createRow(totalRows + 1);
		writeAgentTotals(row,workbook,totalRecordsAttempted,totalContacts,totalConnects,totalSuccess);
		////////
		Date date1 = Calendar.getInstance().getTime();  
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
		String strDate = dateFormat.format(date1); 
		//////////
		String fileName = login.getOrganization() + "_" + UUID.randomUUID() + "_" + strDate + "_AgentReport.xlsx";
		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
//		List<File> fileNames = new ArrayList<File>();
//		fileNames.add(file);
		workbook.close();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		
		String link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketName, fileName,
				file);
		String linkrefNotice = "<" + link + ">";
		String linkref = "<a href=\" "+link+" \">Download here</a>";
		String subject = "XTaaS - Agent Stats - " + dateStr + " - " + login.getId();
		String message = "Agent Report Download Link : " + linkref;
		PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Agent Report Download Link : Download here" + linkrefNotice;
		teamNoticeSerive.saveUserNotices(login.getId(), notice);
		Organization organization = organizationRepository.findOneById(login.getOrganization());
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

		if (login.getReportEmails() != null && login.getReportEmails().size() > 0) {
			for(String email : login.getReportEmails()) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
						subject, message);	
			}
		}else {
			XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com",
					subject, message);	
		}
		logger.debug("Agent report sent successfully to : " + login.getEmail());
	}
	
	private void writeHeadersForAgentReport(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Sr. No");
		cell = headerRow.createCell(1);
		cell.setCellValue("Agent");
		cell = headerRow.createCell(2);
		cell.setCellValue("Campaign");
		cell = headerRow.createCell(3);
		cell.setCellValue("Agent Dailed");
		cell = headerRow.createCell(4);
		cell.setCellValue("Contacts");
		cell = headerRow.createCell(5);
		cell.setCellValue("Connects");
		cell = headerRow.createCell(6);
		cell.setCellValue("Success");
		cell = headerRow.createCell(7);
		cell.setCellValue("Dials to Success");
		cell = headerRow.createCell(8);
		cell.setCellValue("Connect%");
	}
	
	private void writeToExcelFile(AgentReportDTO agentReportDto, Row row, Workbook workbook) {
		Cell cell = row.createCell(0);
		cell.setCellValue(agentReportDto.getSrNo());
		cell = row.createCell(1);
		cell.setCellValue(agentReportDto.getAgentName());
		cell = row.createCell(2);
		cell.setCellValue(agentReportDto.getCampaignName());
		cell = row.createCell(3);
		cell.setCellValue(agentReportDto.getRecordsAttempted());
		cell = row.createCell(4);
		cell.setCellValue(agentReportDto.getContacts());
		cell = row.createCell(5);
		cell.setCellValue(agentReportDto.getConnects());
		cell = row.createCell(6);
		cell.setCellValue(agentReportDto.getSuccess());
		cell = row.createCell(7);
		cell.setCellValue(agentReportDto.getDialsToSuccess());
		cell = row.createCell(8);
		cell.setCellValue(agentReportDto.getConnectPercentage());

	}
	
	private void writeAgentTotals(Row row,Workbook workbook,Integer totalRecordsAttempted, Integer totalContacts, Integer totalConnects, Integer totalSuccess) {
		Cell cell = row.createCell(3);
		cell.setCellValue("Total= " + totalRecordsAttempted);
		cell = row.createCell(4);
		cell.setCellValue("Total= " + totalContacts);
		cell = row.createCell(5);
		cell.setCellValue("Total= " + totalConnects);
		cell = row.createCell(6);
		cell.setCellValue("Total= " + totalSuccess);
	}

	@Override
	public int moveDataToCallables(String campaignId) {
		logger.debug("moveDataToCallables() : Move newly bought data to callables of campaignId : " + campaignId);
		return prospectCallLogRepository.getNewlyBuyProspectsOfCampaign(campaignId);
	}
}
