package com.xtaas.service;

import com.xtaas.db.entity.SupervisorPreference;
import com.xtaas.domain.valueobject.PreferenceId;

public interface SupervisorPreferenceService {
	
	SupervisorPreference getPreference(PreferenceId preferenceId);
	boolean updateSupervisorPreference(SupervisorPreference supervisorPreference);
}
