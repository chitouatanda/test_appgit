package com.xtaas.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import com.xtaas.web.dto.ProspectCallCacheDTO;

public interface ProspectCacheOperationService {

	public void insert(String campaignId, List<ProspectCallCacheDTO> prospects, Comparator<ProspectCallCacheDTO> prospectSortOrder);

	public List<ProspectCallCacheDTO> remove(String campaignId, int count);

	public List<ProspectCallCacheDTO> removeProspectsByRatio(String key, Float size, List<Float> ratios);

	public List<ProspectCallCacheDTO> getCurrentState(String campaignId);
	
	public void clearCampaignCache(String campaignId);

	public Map<String, PriorityQueue<ProspectCallCacheDTO>> getWholeCache();

	public int getCacheSize(String campaignId);

}