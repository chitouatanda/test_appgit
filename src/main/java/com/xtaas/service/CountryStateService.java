package com.xtaas.service;

import java.util.List;
import java.util.Map;

public interface CountryStateService {

	public List<String> getStatesOfCountry(String countryName);
	
	public List<String> getCountries();

	public Map<String, String> getCountryDetails();

	public void updateCountryDetails();

}
