/**
 * 
 */
package com.xtaas.service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.xtaas.valueobjects.KeyValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ApplicationProperty;
import com.xtaas.db.repository.ApplicationPropertyRepository;

/**
 * @author djain
 *
 */
@Service
public class PropertyServiceImpl implements PropertyService {

	private static final Logger logger = LoggerFactory.getLogger(PropertyServiceImpl.class);
	
	private ConcurrentHashMap<String, String> applicationPropertyCache = null;

	public List<KeyValuePair<String, Double>> rateLimitPriorityList = null;
	
	@Autowired
	private ApplicationPropertyRepository applicationPropertyRepository;

	@Override
	public void addApplicationProperty(ApplicationProperty property) {
		applicationPropertyRepository.save(property);
	}

	@Override
	public String getApplicationPropertyValue(String propertyName) {
		if (applicationPropertyCache == null) {
			loadApplicationPropertyCache();
		}
		return applicationPropertyCache.get(propertyName);
	}
	
	@Override
	public String getApplicationPropertyValue(String propertyName, String defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : value;
	}

	private synchronized void loadApplicationPropertyCache() {
		if (applicationPropertyCache == null) { //double check incase it is already loaded by other thread.
			logger.info("loadApplicationPropertyCache() : Loading ApplicationProperty cache.");
			applicationPropertyCache = new ConcurrentHashMap<String, String>();
			List<ApplicationProperty> applicationPropertyList = applicationPropertyRepository.findAll();
			for (ApplicationProperty property : applicationPropertyList) {
				applicationPropertyCache.put(property.getName(), property.getValue());
			}
			logger.info("loadApplicationPropertyCache() : Loaded ApplicationProperty cache. Cache size: " + applicationPropertyCache.size());
		}
	}

	@Override
	public boolean getBooleanApplicationPropertyValue(String propertyName, boolean defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Boolean.parseBoolean(value);
	}

	@Override
	public int getIntApplicationPropertyValue(String propertyName, int defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Integer.parseInt(value);
	}

	@Override
	public long getLongApplicationPropertyValue(String propertyName, long defaultValue) {
		String value = getApplicationPropertyValue(propertyName);
		return (value == null) ? defaultValue : Long.parseLong(value);
	}

	@Override
	public void loadApplicationPropertyCacheForcefully() {
		logger.info("Loading ApplicationProperty cache forcefully...");
		List<ApplicationProperty> applicationPropertyList = applicationPropertyRepository.findAll();
		applicationPropertyCache = new ConcurrentHashMap<String, String>();
		for (ApplicationProperty property : applicationPropertyList) {
			applicationPropertyCache.put(property.getName(), property.getValue());
		}
		logger.info("Loaded ApplicationProperty cache forcefully. Cache size: " + applicationPropertyCache.size());
	}

	@Override
	public List<KeyValuePair<String, Double>> getDataBuyRateLimiterValue() {
		if(rateLimitPriorityList == null){
			ApplicationProperty applicationProperty = applicationPropertyRepository.findOneById("DATA_BUY_RATE_LIMITER");
			if(applicationProperty != null) {
				rateLimitPriorityList = applicationProperty.getRateLimitPriority();
			}
		}
		return rateLimitPriorityList;
	}

}
