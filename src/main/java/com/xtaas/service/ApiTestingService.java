package com.xtaas.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import com.xtaas.web.dto.CustomBuyDTO;
import com.xtaas.web.dto.ZoomTokenDTO;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.web.dto.CampaignPerformanceDTO;
import com.xtaas.web.dto.OneOffDTO;

public interface ApiTestingService {

	public String getCallLogRecordsForCampaign(OneOffDTO oneOffDTO);

	public String tagdirectindirect(OneOffDTO oneOffDTO);

	public String testQualityBuckeet(OneOffDTO oneOffDTO);
	
	public String testQualityBucketForProspectSalutary(OneOffDTO oneOffDTO);
	
	public String testQualityBucketForProspectSalutaryTemp(OneOffDTO oneOffDTO);

	public String setSalutaryTimeZone(OneOffDTO oneOffDTO);
	
	public List<ZoomInfoResponse> buildQueryByCriteria(OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException;

	public List<ZoomInfoResponse> buildQueryByIndustries(OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException;

	public String buildQueryPurchaseSuccess(OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException;

	public String removeDuplicateProspects(OneOffDTO oneOffDTO);

	public String removeSpacesFromPhone(OneOffDTO oneOffDTO);

	public String removeSuccessDuplicates(OneOffDTO oneOffDTO) throws ParseException;

	public String getTenProspectsForACompany(OneOffDTO oneOffDTO);
	
	public String moveSalutaryData(String counter);
	
	public String updateMissingRecording(String days);
	
	public String  getPCIAnalysisTemp(OneOffDTO oneOffDTO);
	
	public String  telnyxMissingRecordings(OneOffDTO oneOffDTO);
	
	public String deliverCampaignAsset(OneOffDTO oneOffDTO);

	public  List<ProspectCallLog> readBooksFromCSV(String campaignId, MultipartFile file);
	
	public  String createZoomToken();
	
	public  String copySCLToPCL(String campaignId, String source, MultipartFile file);
	
	public  String copyPCLToPCL(String campaignId, String source, MultipartFile file);
	
	public  String copyPCIToPCL(String campaignId, String source, MultipartFile file);
	
	public String movedatatonewsuppression(OneOffDTO oneOffDTO);
	
	public String downloadDataSubProspect(OneOffDTO oneOffDTO);
	
	public String moveDataToNewAbm(OneOffDTO oneOffDTO);
	
	public String changeCampaignSettings(OneOffDTO oneOffDTO);
	
	public String getCampaignCache(OneOffDTO oneOffDTO);

	public  String uploadOldSuppression(String organization,String type, MultipartFile file);
	
	public  String copyDATAToPCL(String campaignId, String source, MultipartFile file);

	public  String createProspectUsingFaker(String campaignId,int number);
	
	public String moveInvalidSalutary();
	
	public String convertProspectcalllogToEncryptedProspectcalllog(OneOffDTO oneOffDTO);
	
	public String updateProspect(OneOffDTO oneOffDTO);
	
	public String getProspect(OneOffDTO oneOffDTO);

	public String updateMultipleProspects(OneOffDTO oneOffDTO);
	
	public String getEncryptProspect(OneOffDTO oneOffDTO);

	public String removeCallables(OneOffDTO oneOffDTO);

	public String getBackCallabes(OneOffDTO oneOffDTO);
	
	public String getCampaign(OneOffDTO oneOffDTO);

	public String killdatabuyqueue(OneOffDTO oneOffDTO);
	
	public String changeBuyStatus(OneOffDTO oneOffDTO);
	
	public String startEmail(OneOffDTO oneOffDTO);
	
	public String startCampaignDataBuy(OneOffDTO oneOffDTO);
	
	public String emailToMD5(OneOffDTO oneOffDTO);
	
	public String updateLatestStatus(OneOffDTO oneOffDTO);
	
	public String fixCrmMapping(OneOffDTO oneOffDTO);
	
	public String getCompanyInfoFromEverstring(MultipartFile file);
	
	public String createSalutaryCompany();
	
	public String getCompanyInfoFromZoomInfo(MultipartFile file);

	public String convertEverstringCompanyToMasterCompay();
	
	public String updateProspectSalutaryCompanyDetails();
	
	public String findByProspectCallId(OneOffDTO oneOffDTO);
	
	public String downloadData(OneOffDTO oneOffDTO);
	
	public List<CampaignPerformanceDTO> getCampaignPerformance(OneOffDTO oneOffDTO);
	
	public String suppressMD5Emails(OneOffDTO oneOffDTO);
	
	public String supstatschange(OneOffDTO oneOffDTO);
	
	public String extractCID(OneOffDTO oneOffDTO);

	public String fetchSignalwireRecordingUrl(String callSid);
	
	public void fetchAndSaveRecordingUrl(String callSid, String email, boolean isSend);

	public String fetchAndAddCountryDetailsInDB();

	public String moveSalutaryProspect();

	public String moveSalutaryDataLive(int pageNo);

	public String uploadSource(String source);

	public String apiDataBuy(CustomBuyDTO createCustomBuyDTO);

	public String updateZoomToken(ZoomTokenDTO zoomTokenDTO);
	
	public String readSalutaryData(int startIndex);

}
