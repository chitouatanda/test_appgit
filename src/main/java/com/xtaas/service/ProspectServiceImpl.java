package com.xtaas.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.xtaas.db.entity.AgentCallLog;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.repository.AgentCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.mvc.model.Agent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author djain
 *
 */
@Service
public class ProspectServiceImpl implements ProspectService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProspectServiceImpl.class);
	
	@Autowired
	private AgentCallLogRepository agentCallLogRepository;

	@Override
	public void processAgentSubmission(Agent agentResponse, HashMap<String, String> attributeResponseMap, HashMap<String, CRMAttribute> prospectAttributeMap) {
		Prospect prospect = agentResponse.getProspect();
		Campaign campaign = agentResponse.getCurrentCampaign();
		
		//1. evaluate the Qualification Expression against prospect object
		boolean isQualified = campaign.getQualificationCriteria().isProspectQualify(prospect, prospectAttributeMap);
		logger.debug("processAgentSubmission() : Prospect : " + prospect.getPhone() + ", Qualify: " + isQualified);
		
		//2. TODO: push the Information collected against Information Expression to CRM -- This needs to be asynchronously
		
		//3. persist reference of this information in AgentCallLog collection
		AgentCallLog agentCallLog = new AgentCallLog();
		agentCallLog.setAgentId(agentResponse.getScreenName());
		agentCallLog.setCallSid(agentResponse.getCallSid());
		agentCallLog.setCampaignId(campaign.getId());
		agentCallLog.setClosingNote(agentResponse.getClosingNote());
		
		//3.1 Create prospectlog object 
		AgentCallLog.AgentProspectLog prospectLog = agentCallLog.new AgentProspectLog();
		prospectLog.setProspectId(prospect.getPhone()); //for now, using Phone Number as ProspectId
		prospectLog.setPhone(prospect.getPhone());
		
		//3.2 set the attributes value collected for a prospect by an agent
		HashMap<String, String> qualificationAttributeNameValueMap = setAttributeNameValueCollectedByAgent(campaign.getQualificationCriteria().getUniqueAttributeMap(), attributeResponseMap);
		HashMap<String, String> informationAttributeNameValueMap = setAttributeNameValueCollectedByAgent(new HashSet<String>(campaign.getInformationCriteria()), attributeResponseMap);
		prospectLog.setQualificationAttributeNameValueMap(qualificationAttributeNameValueMap); //Qualification Attributes Name and Value as collected by Agent during call
		prospectLog.setInformationAttributeNameValueMap(informationAttributeNameValueMap); //Information Attributes Name and Value as collected by Agent during call
		
		//3.3 set the Qualification Evaluation flag
		prospectLog.setQualified(isQualified);
		
		//3.4 stitch prospectLog to agentCallLog
		agentCallLog.setProspectLog(prospectLog);
		
		//3.5 Save it in db
		agentCallLogRepository.save(agentCallLog);
	}
	
	private HashMap<String, String> setAttributeNameValueCollectedByAgent(Set<String> criteriaAttributes, HashMap<String, String> attributeResponseMap) {
		HashMap<String, String> attributeNameValueMap = new HashMap<String, String>();
    	if (criteriaAttributes != null) {
	    	for (String attributeName : criteriaAttributes) {
	    		attributeNameValueMap.put(attributeName, attributeResponseMap.get(attributeName));
	    	}
    	}
    	return attributeNameValueMap;
    }
}
