package com.xtaas.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;

@Service
public class NotScoredCountServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(NotScoredCountServiceImpl.class);

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	private int totalNotScoredLeads; // Total count of NOT_SCORED leads of all partners.

	private int totalQaReturnedLeads; // Total count of QA_RETURNED leads of all partners.

	public void countPartnerNotScoredLeads() {
		List<String> orgNames = getPartnerNames();
		List<PartnerLeadDetail> count = getEachPartnerDetails(orgNames);
		sendEmail(count);
	}

	private List<String> getPartnerNames() {
		List<String> orgLevels = new ArrayList<String>();
		List<String> orgs = new ArrayList<String>();
		orgLevels.add("PARTNER");
		List<Organization> organizations = organizationRepository.findByOrganizationLevel(orgLevels);
		if (organizations != null && organizations.size() > 0) {
			orgs = organizations.stream().map(Organization::getId).collect(Collectors.toList());
		}
		return orgs;
	}

	/**
	 * Counts details of each partner.
	 * 
	 * @param partners
	 * @return list of partner details
	 */
	private List<PartnerLeadDetail> getEachPartnerDetails(List<String> partners) {
		Date startDate = XtaasDateUtils.getStartDateOfMonth();
		Date endDate = new Date();
		int totalNotScoredCount = 0;
		int totalQaReturnedCount = 0;
		List<PartnerLeadDetail> partnerDetails = new ArrayList<PartnerLeadDetail>();
		for (String partnerId : partners) {
			PartnerLeadDetail partnerLeadDetail = new PartnerLeadDetail();
			partnerLeadDetail.setName(partnerId);
			int notScoredCount = prospectCallLogRepository.getPartnerNotScoredLeadsCount(partnerId,
					XtaasDateUtils.getDateAtTime(startDate, 0, 0, 0, 0),
					XtaasDateUtils.getDateAtTime(endDate, 23, 59, 59, 0));
			partnerLeadDetail.setNotScoredCount(notScoredCount);
			totalNotScoredCount += notScoredCount;
			int qaReturnedCount = prospectCallLogRepository.getPartnerQaReturnedLeadsCount(partnerId,
					XtaasDateUtils.getDateAtTime(startDate, 0, 0, 0, 0),
					XtaasDateUtils.getDateAtTime(endDate, 23, 59, 59, 0));
			partnerLeadDetail.setQaReturnedCount(qaReturnedCount);
			totalQaReturnedCount += qaReturnedCount;
			partnerDetails.add(partnerLeadDetail);
		}
		this.totalNotScoredLeads = totalNotScoredCount;
		this.totalQaReturnedLeads = totalQaReturnedCount;
		return partnerDetails;
	}

	/**
	 * Sends email with partner details.
	 * 
	 * @param partnerDetails
	 */
	private void sendEmail(List<PartnerLeadDetail> partnerDetails) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ");
		String dateStr = sdf.format(date);
		String subject = dateStr + " - Partnerwise Not Scored and Qa Returned Leads Count";
		String text = "<table width='100%' border='1' align='center'>" + "<tr align='center'>"
				+ "<td><b>Partner Name <b></td>" + "<td><b>Not Scored Lead Count<b></td>"
				+ "<td><b>Qa Returned Lead Count<b></td>" + "</tr>";
		for (PartnerLeadDetail partnerLeadDetail : partnerDetails) {
			text = text + "<tr align='center'>" + "<td>" + partnerLeadDetail.getName() + "</td>" + "<td>"
					+ partnerLeadDetail.getNotScoredCount() + "</td>" + "<td>" + partnerLeadDetail.getQaReturnedCount()
					+ "</td>" + "</tr>";
		}
		text = text + "<tr align='center'>" + "<td>" + "Total" + "</td>" + "<td>" + this.totalNotScoredLeads + "</td>"
				+ "<td>" + this.totalQaReturnedLeads + "</td>" + "</tr>";
		XtaasEmailUtils.sendEmail("reports@xtaascorp.com", "data@xtaascorp.com", subject, text);
		this.resetTotalCountVariables();
		logger.debug("Partnerwise NOT_SCORED/QA_RETURNED leads count sent successfully to reports@xtaascorp.com");
	}

	private void resetTotalCountVariables() {
		this.totalNotScoredLeads = 0;
		this.totalQaReturnedLeads = 0;
	}

	private class PartnerLeadDetail {
		String name;
		int notScoredCount;
		int qaReturnedCount;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getNotScoredCount() {
			return notScoredCount;
		}

		public void setNotScoredCount(int notScoredCount) {
			this.notScoredCount = notScoredCount;
		}

		public int getQaReturnedCount() {
			return qaReturnedCount;
		}

		public void setQaReturnedCount(int qaReturnedCount) {
			this.qaReturnedCount = qaReturnedCount;
		}
	}

}
