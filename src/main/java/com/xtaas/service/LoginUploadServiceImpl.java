package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.QaService;
import com.xtaas.application.service.TeamService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Organization.OrganizationLevel;
import com.xtaas.db.entity.Partner;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.OutboundNumberRepository;
import com.xtaas.db.repository.PartnerRepository;
import com.xtaas.db.repository.QaRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.entity.Qa;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.LoginStatus;
import com.xtaas.domain.valueobject.Status;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.TeamStatus;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.service.LoginUploadServiceImpl.LoginFileUploader.UploadFileDTO;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.BillingGroupOutputDTO;
import com.xtaas.web.dto.OrganizationDTO;
import com.xtaas.web.dto.QaDTO;
import com.xtaas.web.dto.TeamDTO;
import com.xtaas.web.dto.TelnyxPhoneNumberPurchaseDTO;

@Service
public class LoginUploadServiceImpl implements LoginUploadService {

	private static final Logger logger = LoggerFactory.getLogger(LoginUploadServiceImpl.class);

	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	HashMap<String, List<UploadFileDTO>> loginHashMap = new HashMap<String, List<UploadFileDTO>>();

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private QaRepository qaRepository;

	@Autowired
	private QaService qaService;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private OutboundNumberRepository outboundNumberRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private AgentService agentService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private CRMMappingService crmMappingService;
	
	@Autowired
	TelnyxService telnyxService;

	private List<String> errorlist1;

	private HashMap<Integer, List<String>> errorMap = new HashMap<>();

	private Workbook book;
	
	private String xlsFlieName;

	@Override
	public String uploadLoginDetails(MultipartFile file, HttpSession httpSession) {
		if (!file.isEmpty()) {
			xlsFlieName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				LoginFileUploader loginFileUploader = new LoginFileUploader();
				try {
					loginFileUploader.setFileInputStream(file.getInputStream());
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
				SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
				DelegatingSecurityContextExecutor contactFileUploadThreadExecutor = new DelegatingSecurityContextExecutor(
						delegateExecutor, SecurityContextHolder.getContext());
				contactFileUploadThreadExecutor.execute(loginFileUploader);
				logger.debug("uploadLogin(): LOGIN-FILE UPLOADING-THREAD is started...");
			} else {
				logger.error("uploadFile() : Error: UNSUPPORTED FILE TYPE " + file);
				return "{\"code\": \"ERROR\", \"msg\" : \"UNSUPPORTED FILE TYPE : Only .xls, .xlsx Filetypes are supported.\", \"file\" : \""
						+ file.getOriginalFilename() + "\"}";
			}
			return "{\"code\": \"UPLOADING INn PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		} else {
			return "{\"code\": \"ERROR\", \"msg\" : \"EMPTY FILE\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		}
	}

	class LoginFileUploader implements Runnable {
		private final Logger logger = LoggerFactory.getLogger(LoginFileUploader.class);
		private InputStream fileInputStream;
		private String fileName;
		private Sheet sheet;// this is the excel sheet to be read
		private List<String> errorList = new ArrayList<String>();
		private List<String> mandatoryFields; // contain mandatory fields
		private String organizationLevel;

		public InputStream getFileInputStream() {
			return fileInputStream;
		}

		public void setFileInputStream(InputStream fileInputStream) {
			this.fileInputStream = fileInputStream;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public List<String> getErrorList() {
			return errorList;
		}

		public void setErrorList(List<String> errorList) {
			this.errorList = errorList;
		}

		private boolean isUniqueInLogin(String userName) {
			if (userName != null) {
				return (loginRepository.findUser(userName.toLowerCase()) == null) ? false : true;
			}
			return false;
		}

		@Override
		public void run() {
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			// XSSFWorkbook book;

			try {
				// book = new XSSFWorkbook(fileInputStream);
				book = WorkbookFactory.create(fileInputStream);
				sheet = book.getSheetAt(0);
				int totalRowsCount = sheet.getPhysicalNumberOfRows();
				if (totalRowsCount < 1) {// check if nothing found in file
					book.close();
					logger.error(fileName + " file is empty");
					return;
				}
				mandatoryFields = new ArrayList<String>() {
					private static final long serialVersionUID = 1L;
					{
						add("User Name");
						add("Password");
						add("Roles");
						add("Organization Id");
						add("First Name");
						add("Last Name");
						add("Email");
					}
				};
				if (!validateExcelFileheader())
					return;
				validateExcelFileRecords();
//				book.close();
			} catch (IOException e) {
				logger.error("Problem in reading the file " + fileName);
				return;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private boolean validateExcelFileheader() {
			Set<String> fileColumns = new HashSet<String>();
			final Row row = sheet.getRow(0);// get first sheet in file
			int colCounts = row.getPhysicalNumberOfCells();
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell == null || cell.getCellType() == CellType.BLANK) {
					logger.error(
							"Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
					return false;
				}
				if (cell.getCellType() != CellType.STRING) {
					logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
							+ " Header must contain only String values");
					return false;
				} else {
					headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
					// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
					fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
				}
			}
			return true;
		}

		private void validateExcelFileRecords() {
			// int rowsCount = sheet.getPhysicalNumberOfRows();
			int rowsCount = ErrorHandlindutils.getNonBlankRowCount(sheet);
			if (rowsCount < 2) {
				logger.error("No records found in file " + fileName);
				errorList.add("No records found in file " + fileName);
				return;
			}
			final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
			int colCounts = tempRow.getPhysicalNumberOfCells();
			processRecords(0, rowsCount, colCounts);
		}

		public void processRecords(int startPos, int EndPos, int colCounts) {
			List<UploadFileDTO> uploadFileDTOs = new ArrayList<UploadFileDTO>();
			boolean invalidFile = false;
			for (int i = startPos; i < EndPos; i++) {
				errorlist1 = new ArrayList<>();
				boolean isRecordValid = true;
				UploadFileDTO uploadFile = new UploadFileDTO();
				logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
				Row row = sheet.getRow(i);
				if (i == 0) {
					continue;
				}
				if (row == null) {
					errorlist1.add("Record not found");
					errorMap.put(i, errorlist1);
					invalidFile = true;
					EndPos++;
					continue;
				}
				boolean emptyCell = true;
				for (int j = 0; j < colCounts; j++) {
					Cell cell = row.getCell(j);
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						emptyCell = false;
						break;
					}
				}
				if (emptyCell) {
					errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
					errorlist1.add(" Record is Empty ");
					errorMap.put(i, errorlist1);
					invalidFile = true;
					EndPos++;
					continue;
				}
				for (int j = 0; j < colCounts; j++) {
					String columnName = headerIndex.get(j).replace("*", "").trim();
					// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
					Cell cell = row.getCell(j);
					if (cell == null || cell.getCellType() == CellType.BLANK) {
						logger.debug("Cell is empty");
						if (mandatoryFields.contains(columnName)) {
							errorlist1.add(columnName + " is missing ");
						}
					} else if (columnName.equalsIgnoreCase("User Name")) {
						String userName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						//uploadFile.setUsername(userName);
						if(userName != null) {
							if(userName.length() <= 22) {
								if(!userName.matches("[^a-zA-Z0-9]") && !userName.contains("_")) {
									uploadFile.setUsername(userName);
								}else {
									errorlist1.add("Special characters not allowed in the UserName");
								}
							}else {
								errorlist1.add("Max 22 character is allowed");
							}
						}
					}

					else if (columnName.equalsIgnoreCase("Password")) {
						String password = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setPassword(password);
					} else if (columnName.equalsIgnoreCase("Role")) {
						String role = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						List<String> roles = new ArrayList<String>();
						roles.add(role.toUpperCase());
						uploadFile.setRoles(roles);
					} else if (columnName.equalsIgnoreCase("Organization ID")) {
						String organization = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setOrganization(organization.toUpperCase());
					}else if (columnName.equalsIgnoreCase("Admin Email ID")) {
						String adminEmail = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setOrganizationEmail(adminEmail);
					} else if (columnName.equalsIgnoreCase("First Name")) {
						String firstName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setFirstName(firstName);
					} else if (columnName.equalsIgnoreCase("Last Name")) {
						String lastName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setLastName(lastName);
					} else if (columnName.equalsIgnoreCase("Email")) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setEmail(email);
					} else if (columnName.equalsIgnoreCase("Supervisor Admin Role")
							|| columnName.equalsIgnoreCase("Supervisor Admin")) {
						String adminRole = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (adminRole != null && !adminRole.isEmpty()) {
							uploadFile.setSupervisorAdminRole(adminRole);
						}
					} else if (columnName.equalsIgnoreCase("Organization Level")
							|| columnName.equalsIgnoreCase("Organization")) {
						String organizationLevel = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (organizationLevel != null && !organizationLevel.isEmpty()) {
							this.organizationLevel = organizationLevel;
						}
					} else if (columnName.equalsIgnoreCase("Action")) {
						String action = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setAction(action);
					} else if (columnName.equalsIgnoreCase("Sub Action")) {
						String subAction = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setSubAction(subAction);
					} else if (columnName.equalsIgnoreCase("Supervisor ID")) {
						String teamId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setSupervisorId(teamId);
					} else if (columnName.equalsIgnoreCase("Admin Supervisors")) {
						String supervisors = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (!StringUtils.isEmpty(supervisors)) {
							List<String> adminSupervisors = Arrays.asList(supervisors.split(",")).stream()
									.map(sup -> sup.replaceAll("\\s", "")).collect(Collectors.toList());
							uploadFile.setAdminSupervisorIds(adminSupervisors);
						} else {
							uploadFile.setAdminSupervisorIds(new ArrayList<String>());
						}
					} else if (columnName.equalsIgnoreCase("Team Action")) {
						String teamAction = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setTeamAction(teamAction);
					} else if (columnName.equalsIgnoreCase("Telnyx Voice Account Setup and Number Purchase")) {
						String telnyxAccountSetup = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setTelnyxAccountSetUpAndBuyNumbers(telnyxAccountSetup);
					} else if (columnName.equalsIgnoreCase("Telnyx Purchase Local Dialling Numbers")) {
						String telnyxLocalDiallingNumbers = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setTelnyxBuyLocalDiallingNumbers(telnyxLocalDiallingNumbers);
					}

					if (errorlist1 != null && !errorlist1.isEmpty() && errorlist1.size() > 0) {
						invalidFile = true;
					}
					errorMap.put(i, errorlist1);
				}

				if (uploadFileDTOs != null && !uploadFileDTOs.isEmpty()) {
					List<UploadFileDTO> newLoginList = new ArrayList<UploadFileDTO>();
					assert (uploadFile.getUsername() != null);
					newLoginList = uploadFileDTOs.stream().filter(cc -> cc.getUsername() != null)
							.filter(cc -> cc.getUsername().equalsIgnoreCase(uploadFile.getUsername()))
							.collect(Collectors.toList());
					if (!newLoginList.isEmpty()) {
						isRecordValid = false;
					}
				}
				if (isRecordValid) {
					uploadFileDTOs.add(uploadFile);
				}
			}
			if (errorMap != null && invalidFile) {
				ErrorHandlindutils.excelFileChecking(book, errorMap);
				try {
					ErrorHandlindutils.sendExcelwithMsg(null,book,xlsFlieName,"Login",null,null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				throw new IllegalArgumentException("Please Check Your Email There is Some Error in File");
			} else {
				try {
					sendEmailWithLoginFileAttached(book, xlsFlieName);
				} catch (IOException e) {
					e.printStackTrace();
				}
				List<UploadFileDTO> supervisorList = new ArrayList<UploadFileDTO>();
				List<UploadFileDTO> agentList = new ArrayList<UploadFileDTO>();
				List<UploadFileDTO> qaList = new ArrayList<UploadFileDTO>();
				List<UploadFileDTO> secondaryQaList = new ArrayList<UploadFileDTO>();
				List<UploadFileDTO> partnerList = new ArrayList<UploadFileDTO>();
				List<UploadFileDTO> campaignManagerList = new ArrayList<UploadFileDTO>();

				for (UploadFileDTO uploadFileDTO : uploadFileDTOs) {
					if (uploadFileDTO.getRoles().contains("AGENT")) {
						agentList.add(uploadFileDTO);
					} else if (uploadFileDTO.getRoles().contains("QA")) {
						qaList.add(uploadFileDTO);
					} else if (uploadFileDTO.getRoles().contains("SECONDARYQA")) {
						secondaryQaList.add(uploadFileDTO);
					} else if (uploadFileDTO.getRoles().contains("SUPERVISOR")) {
						supervisorList.add(uploadFileDTO);
					} else if (uploadFileDTO.getRoles().contains("PARTNER")) {
						partnerList.add(uploadFileDTO);
					} else if (uploadFileDTO.getRoles().contains("CAMPAIGN_MANAGER")) {
						campaignManagerList.add(uploadFileDTO);
					}

					if (uploadFileDTO.getAction().equalsIgnoreCase("create")) {
						if (!isUniqueInLogin(uploadFileDTO.getUsername())) {
							Login login = createLogin(uploadFileDTO);
							Login loginDB = loginRepository.save(login);
							logger.debug("Login [{}] created successfully.", loginDB.getId());
						}
						if (uploadFileDTO.getRoles().contains("AGENT")) {
							createAgent(uploadFileDTO);
							if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
								addAgentInTeam(uploadFileDTO);
							} else if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
								removeAgentFromTeam(uploadFileDTO);
							}
						} else if (uploadFileDTO.getRoles().contains("QA")
								|| uploadFileDTO.getRoles().contains("SECONDARYQA")) {
							createQa(uploadFileDTO);
							if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
								addQaInTeam(uploadFileDTO);
							} else if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
								removeQaFromTeam(uploadFileDTO);
							}
						} else if (uploadFileDTO.getRoles().contains("PARTNER")) {
							createPartner(uploadFileDTO);
							createCrmMapping(uploadFileDTO);
						}
					} else if (uploadFileDTO.getAction().equalsIgnoreCase("update")) {
						if (uploadFileDTO.getRoles().contains("AGENT")) {
							updateAgent(uploadFileDTO);
							if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
								addAgentInTeam(uploadFileDTO);
							} else if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
								removeAgentFromTeam(uploadFileDTO);
							}
						} else if (uploadFileDTO.getRoles().contains("QA")
								|| uploadFileDTO.getRoles().contains("SECONDARYQA")) {
							updateQa(uploadFileDTO);
							if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
								addQaInTeam(uploadFileDTO);
							} else if (uploadFileDTO.getTeamAction() != null
									&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
								removeQaFromTeam(uploadFileDTO);
							}
						} else if (uploadFileDTO.getRoles().contains("SUPERVISOR")) {
							updateSupervisorPassword(uploadFileDTO);
						}
					}
				}
				loginHashMap.put("AGENT", agentList);
				loginHashMap.put("QA", qaList);
				loginHashMap.put("SECONDARYQA", secondaryQaList);
				loginHashMap.put("SUPERVISOR", supervisorList);
				loginHashMap.put("PARTNER", partnerList);
				loginHashMap.put("CAMPAIGN_MANAGER", campaignManagerList);

				for (UploadFileDTO uploadFileDTO : uploadFileDTOs) {
					if (uploadFileDTO.getRoles().contains("PARTNER")) {
						createOrganization(uploadFileDTO, this.organizationLevel);
						BillingGroupOutputDTO billingDTO = new BillingGroupOutputDTO();
						if (uploadFileDTO.getTelnyxAccountSetUpAndBuyNumbers() != null
								&& uploadFileDTO.getTelnyxAccountSetUpAndBuyNumbers() != ""
								&& uploadFileDTO.getTelnyxAccountSetUpAndBuyNumbers().equalsIgnoreCase("YES")) {
							billingDTO = telnyxService.createBillingGroup(uploadFileDTO.getOrganization() + "_ACCOUNT",
									uploadFileDTO.getOrganization());
						}
						if (uploadFileDTO.getTelnyxBuyLocalDiallingNumbers() != null
								&& uploadFileDTO.getTelnyxBuyLocalDiallingNumbers() != ""
								&& uploadFileDTO.getTelnyxBuyLocalDiallingNumbers().equalsIgnoreCase("YES")) {
							if (billingDTO.getBillingGroupId() != null && !billingDTO.getBillingGroupId().isEmpty()
									&& billingDTO.getConnectionId() != null
									&& !billingDTO.getConnectionId().isEmpty()) {
								telnyxService.purchasePhoneNumbers(
										createDTOForLocalDiallingPurchase(uploadFileDTO.getOrganization(),
												billingDTO.getConnectionId(), billingDTO.getBillingGroupId()));
							}
						}
						createOutboundNumber(uploadFileDTO);
					}
					if (uploadFileDTO.getRoles().contains("SUPERVISOR")) {
						createTeam(uploadFileDTO);
					}
				}
				System.out.println("Login List Added successfully");
			}
		}
		
		private TelnyxPhoneNumberPurchaseDTO createDTOForLocalDiallingPurchase(String partnerId, String connectionId,
				String billingGroupId) {
			TelnyxPhoneNumberPurchaseDTO phonePurchaseDTO = new TelnyxPhoneNumberPurchaseDTO();
			List<String> features = new ArrayList<String>();
			features.add("voice");
			features.add("sms");
			phonePurchaseDTO.setStates(XtaasUtils.getUSStateList());
			phonePurchaseDTO.setCountryISO("US");
			phonePurchaseDTO.setRegion("United States");
			phonePurchaseDTO.setFeatures(features);
			phonePurchaseDTO.setConnectionId(connectionId);
			phonePurchaseDTO.setBillingAccountId(billingGroupId);
			phonePurchaseDTO.setTimezone("US/Pacific");
			phonePurchaseDTO.setPartnerId(partnerId);
			phonePurchaseDTO.setIsdCode(1);
			phonePurchaseDTO.setPool(1);
			phonePurchaseDTO.setLimit(1);
			phonePurchaseDTO.setAddNumberInOtherCountries(false);
			return phonePurchaseDTO;
		}

		public class UploadFileDTO {
			public String username;
			public String password;
			public List<String> roles;
			public String firstName;
			public String lastName;
			public String organization;
			public String organizationEmail;
			public String email;
			public String supervisorAdminRole;
			public String action;
			public String subAction;
			public String supervisorId;
			public String teamAction;
			public List<String> adminSupervisorIds;
			public String telnyxAccountSetUpAndBuyNumbers;
			public String telnyxBuyLocalDiallingNumbers;

			public String getUsername() {
				return username;
			}

			public void setUsername(String username) {
				this.username = username.toLowerCase();
			}

			public String getPassword() {
				return password;
			}

			public void setPassword(String password) {
				this.password = password;
			}

			public List<String> getRoles() {
				return roles;
			}

			public void setRoles(List<String> roles) {
				this.roles = roles;
			}

			public String getFirstName() {
				return firstName;
			}

			public void setFirstName(String firstName) {
				this.firstName = firstName;
			}

			public String getLastName() {
				return lastName;
			}

			public void setLastName(String lastName) {
				this.lastName = lastName;
			}

			public String getOrganization() {
				return organization;
			}

			public void setOrganization(String organization) {
				this.organization = organization;
			}
			
			public String getOrganizationEmail() {
				return organizationEmail;
			}

			public void setOrganizationEmail(String organizationEmail) {
				this.organizationEmail = organizationEmail;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}

			public String getAction() {
				return action;
			}

			public void setAction(String action) {
				this.action = action.toLowerCase();
			}

			public String getSubAction() {
				return subAction;
			}

			public void setSubAction(String subAction) {
				this.subAction = subAction.toLowerCase();
			}

			public String getSupervisorId() {
				return supervisorId;
			}

			public void setSupervisorId(String supervisorId) {
				this.supervisorId = supervisorId;
			}

			public String getTeamAction() {
				return teamAction;
			}

			public void setTeamAction(String teamAction) {
				this.teamAction = teamAction.toLowerCase();
			}

			public String getSupervisorAdminRole() {
				return supervisorAdminRole;
			}

			public void setSupervisorAdminRole(String supervisorAdminRole) {
				this.supervisorAdminRole = supervisorAdminRole;
			}

			public List<String> getAdminSupervisorIds() {
				return adminSupervisorIds;
			}

			public void setAdminSupervisorIds(List<String> adminSupervisorIds) {
				if (this.adminSupervisorIds == null) {
					this.adminSupervisorIds = new ArrayList<String>();
				}
				this.adminSupervisorIds.addAll(adminSupervisorIds);
			}

			public String getTelnyxAccountSetUpAndBuyNumbers() {
				return telnyxAccountSetUpAndBuyNumbers;
			}

			public void setTelnyxAccountSetUpAndBuyNumbers(String telnyxAccountSetUpAndBuyNumbers) {
				this.telnyxAccountSetUpAndBuyNumbers = telnyxAccountSetUpAndBuyNumbers;
			}

			public String getTelnyxBuyLocalDiallingNumbers() {
				return telnyxBuyLocalDiallingNumbers;
			}

			public void setTelnyxBuyLocalDiallingNumbers(String telnyxBuyLocalDiallingNumbers) {
				this.telnyxBuyLocalDiallingNumbers = telnyxBuyLocalDiallingNumbers;
			}
		}
	}

	private Login createLogin(UploadFileDTO uploadFileDTO) {
		Login login = new Login();
		login.setUsername(uploadFileDTO.getUsername());
		login.setStatus(LoginStatus.ACTIVE);
		login.setPassword(getPasswordHash(uploadFileDTO.getUsername(), uploadFileDTO.getPassword()));
		login.setRoles(uploadFileDTO.getRoles());
		login.setDialerSetting(false);
		login.setNetworkStatus(false);
		login.setUserLock(false);
		login.setNoOfAttempt(0);
		login.setActivateUser(false);
		login.setLastPasswordUpdated(new Date());
		if (uploadFileDTO.getRoles() != null && uploadFileDTO.getRoles().contains("SUPERVISOR")
				&& (uploadFileDTO.getSupervisorAdminRole() != null
						&& uploadFileDTO.getSupervisorAdminRole().equalsIgnoreCase("YES"))) {
			login.setAdminRole("ADMIN_SUPERVISOR");
		}
		if (uploadFileDTO.getRoles() != null
				&& (uploadFileDTO.getRoles().contains("QA") || uploadFileDTO.getRoles().contains("SECONDARYQA"))) {
			List<String> reportEmails = new ArrayList<String>();
			reportEmails.add("leadreports@xtaascorp.com");
			// reportEmails.add("anaikshelar@xtaascorp.com");
			reportEmails.add(uploadFileDTO.getEmail());
			login.setReportEmails(reportEmails);
		}
		login.setFirstName(uploadFileDTO.getFirstName());
		login.setLastName(uploadFileDTO.getLastName());
		login.setEmail(uploadFileDTO.getEmail());
		login.setOrganization(uploadFileDTO.getOrganization());
		return login;
	}

	private String getPasswordHash(String username, String password) {
		if (password == null) {
			password = "";
		}
		StringBuilder passwordWithSalt = new StringBuilder(password).append("{").append(username).append("}");
		return EncryptionUtils.generateMd5Hash(passwordWithSalt.toString());
	}

	private void changePassword(String id, String password, String status) {
		Login login = loginRepository.findOneById(id);
		if (status.equalsIgnoreCase("active")) {
			login.setPassword(getPasswordHash(id, password));
			login.setStatus(LoginStatus.ACTIVE);
		} else if (status.equalsIgnoreCase("inactive")) {
			login.setPassword("TERMINATE");
			login.setStatus(LoginStatus.INACTIVE);
		}
		loginRepository.save(login);
	}

	private void createAgent(UploadFileDTO uploadFileDTO) {
		Agent agentFromDB = agentRepository.findOneById(uploadFileDTO.getUsername());
		if (agentFromDB == null) {
			agentFromDB = agentRepository.findOneById("donotdeleteagent");
			if (agentFromDB != null) {
				agentFromDB.setUsername(uploadFileDTO.getUsername());
				agentFromDB.setPartnerId(uploadFileDTO.getOrganization());
				agentFromDB.setFirstName(uploadFileDTO.getFirstName());
				agentFromDB.setLastName(uploadFileDTO.getLastName());
				agentFromDB.setSupervisorId(uploadFileDTO.getSupervisorId());
				agentFromDB.setAgentType(agentSkill.AUTO);
				AgentDTO agentDTO = new AgentDTO(agentFromDB);
				AgentDTO dto = agentService.createAgent(agentDTO);
				logger.debug("Agent [{}] created successfully.", dto.getId());
			}
		}
	}

	private void updateAgent(UploadFileDTO uploadFileDTO) {
		Agent agentFromDB = agentRepository.findOneById(uploadFileDTO.getUsername());
		if (agentFromDB != null && uploadFileDTO.getSubAction() != null) {
			if (uploadFileDTO.getSubAction().equalsIgnoreCase("active")) {
				agentFromDB.setStatus(AgentStatus.ACTIVE);
				changePassword(agentFromDB.getId(), uploadFileDTO.getPassword(), "active");
			} else if (uploadFileDTO.getSubAction().equalsIgnoreCase("inactive")) {
				agentFromDB.setStatus(AgentStatus.INACTIVE);
				changePassword(agentFromDB.getId(), uploadFileDTO.getPassword(), "inactive");
			}
			Agent agentDB = agentRepository.save(agentFromDB);
			logger.debug("Agent [{}] status updated successfully.", agentDB.getId());
		}
	}

	private void updateSupervisorPassword(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSubAction() != null || !uploadFileDTO.getSubAction().isEmpty()) {
			if (uploadFileDTO.getSubAction().equalsIgnoreCase("active")) {
				changePassword(uploadFileDTO.getUsername(), uploadFileDTO.getPassword(), "active");
			} else if (uploadFileDTO.getSubAction().equalsIgnoreCase("inactive")) {
				changePassword(uploadFileDTO.getUsername(), uploadFileDTO.getPassword(), "inactive");
			}
		}
	}

	private void createQa(UploadFileDTO uploadFileDTO) {
		Qa qaFromDB = qaRepository.findOneById(uploadFileDTO.getUsername());
		if (qaFromDB == null) {
			qaFromDB = qaRepository.findQaById("donotdeleteqa");
			if (qaFromDB != null) {
				qaFromDB.setUsername(uploadFileDTO.getUsername());
				qaFromDB.setPartnerId(uploadFileDTO.getOrganization());
				qaFromDB.setFirstName(uploadFileDTO.getFirstName());
				qaFromDB.setLastName(uploadFileDTO.getLastName());
				QaDTO qaDTO = new QaDTO(qaFromDB);
				QaDTO qaDB = qaService.createQa(qaDTO);
				logger.debug("Qa [{}] created successfully.", qaDB.getId());
			}
		}
	}

	private void updateQa(UploadFileDTO uploadFileDTO) {
		Qa qaFromDB = qaRepository.findOneById(uploadFileDTO.getUsername());

		if (qaFromDB != null && uploadFileDTO.getSubAction() != null) {
			if (uploadFileDTO.getSubAction().equalsIgnoreCase("active")) {
				qaFromDB.setStatus(Status.ACTIVE);
				changePassword(qaFromDB.getId(), uploadFileDTO.getPassword(), "active");
			} else if (uploadFileDTO.getSubAction().equalsIgnoreCase("inactive")) {
				qaFromDB.setStatus(Status.INACTIVE);
				changePassword(qaFromDB.getId(), uploadFileDTO.getPassword(), "inactive");
			}
			Qa qaDB = qaRepository.save(qaFromDB);
			logger.debug("Qa [{}] status updated successfully.", qaDB.getId());
		}
	}

	private void addAgentInTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.addAgent(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void removeAgentFromTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.removeAgent(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void addQaInTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.addQa(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void removeQaFromTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.removeQa(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void createPartner(UploadFileDTO uploadFileDTO) {
		Partner partnerFromDB = partnerRepository.findOneById(uploadFileDTO.getOrganization());
		if (partnerFromDB == null) {
			partnerFromDB = partnerRepository.findOneById("XTAAS CALL CENTER");
			if (partnerFromDB != null) {
				Partner partner = new Partner(uploadFileDTO.getFirstName(), uploadFileDTO.getLastName(),
						uploadFileDTO.getEmail(), uploadFileDTO.getUsername(), uploadFileDTO.getOrganization(),
						partnerFromDB.getCompanySize(), partnerFromDB.getIndustry(), partnerFromDB.getPhoneNumber(),
						partnerFromDB.getAddress());
				partner.setId(uploadFileDTO.getOrganization());
				Partner partnerDB = partnerRepository.save(partner);
				logger.debug("Partner [{}] created successfully.", partnerDB.getId());
			}
		}
	}

	private void createOrganization(UploadFileDTO uploadFileDTO, String organizationLevel) {
		List<UploadFileDTO> campaignManagerList = loginHashMap.get("CAMPAIGN_MANAGER");
		Organization organizationgFromDB = organizationRepository.findOneById(uploadFileDTO.getOrganization());
		if (organizationgFromDB == null) {
			String campaignManagerId = "";
			if (campaignManagerList != null && !campaignManagerList.isEmpty()) {
				for (UploadFileDTO fileDTO : campaignManagerList) {
					if (fileDTO.getOrganization().equalsIgnoreCase(uploadFileDTO.getOrganization())) {
						campaignManagerId = campaignManagerList.get(0).getUsername();
						break;
					}
				}
			}
			organizationgFromDB = organizationRepository.findOneById("DONOTDELETE");
			if (organizationgFromDB != null) {
				organizationgFromDB.setId(uploadFileDTO.getOrganization());
				organizationgFromDB.setOrganizationLevel(getOrganizationLevel(organizationLevel));
				organizationgFromDB.setName(uploadFileDTO.getOrganization());
				if(uploadFileDTO.getOrganizationEmail() != null) {
					String stringEmail = uploadFileDTO.getOrganizationEmail();
					List<String> listOfEmail = Arrays.asList(stringEmail.split("\\s*,\\s*"));
					organizationgFromDB.setOrganizationEmail(listOfEmail.get(0));
					organizationgFromDB.setReportingEmail(listOfEmail);	
				}
				organizationgFromDB.setDescription(uploadFileDTO.getOrganization());
				organizationgFromDB.setAccountOwnerLoginId(campaignManagerId);
				organizationgFromDB.setOrganizationLoginId(campaignManagerId);
				OrganizationDTO organizationDTO = new OrganizationDTO(organizationgFromDB);
				organizationService.createOrganization(organizationDTO);
			}
		}
	}

	private OrganizationLevel getOrganizationLevel(String organizationLevel) {
		OrganizationLevel orgLevel = null;
		if (organizationLevel.equalsIgnoreCase("INTERNAL")) {
			orgLevel = OrganizationLevel.INTERNAL;
		} else if (organizationLevel.equalsIgnoreCase("CLIENT")) {
			orgLevel = OrganizationLevel.CLIENT;
		} else if (organizationLevel.equalsIgnoreCase("PARTNER")) {
			orgLevel = OrganizationLevel.PARTNER;
		}
		return orgLevel;
	}

	private void createOutboundNumber(UploadFileDTO uploadFileDTO) {
		List<OutboundNumber> outboundNumbersFromDB = outboundNumberRepository
				.findOutboundNumbers(uploadFileDTO.getOrganization());
		if (outboundNumbersFromDB == null || outboundNumbersFromDB.isEmpty()) {
			outboundNumbersFromDB = outboundNumberRepository.findOutboundNumbers("DONOTDELETE");
			if (outboundNumbersFromDB != null && !outboundNumbersFromDB.isEmpty()) {
				OutboundNumber outboundNumber = outboundNumbersFromDB.get(0);
				OutboundNumber newOutbountNumber = new OutboundNumber(outboundNumber.getPhoneNumber(),
						outboundNumber.getDisplayPhoneNumber(), outboundNumber.getAreaCode(),
						outboundNumber.getTimeZone(), outboundNumber.getIsdCode(), outboundNumber.getProvider(),
						uploadFileDTO.getOrganization(), outboundNumber.getBucketName(),
						outboundNumber.getCountryName());
				newOutbountNumber.setVoiceMessage("Thank you for calling. " + newOutbountNumber.getDisplayPhoneNumber()
						+ " You may leave us a message at the tone. Thank You.");
				newOutbountNumber.setCountryCode("US");
				outboundNumberRepository.save(newOutbountNumber);
				logger.debug("Outboundnumber [{}] for [{}] created successfully.",
						newOutbountNumber.getDisplayPhoneNumber(), newOutbountNumber.getPartnerId());
			}
		}
	}

	private void createCrmMapping(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getOrganization() != null) {
			crmMappingService.addCRMAttribute(uploadFileDTO.getOrganization(), null);
			logger.debug("CrmMapping [{}] created successfully.", uploadFileDTO.getOrganization());
		}
	}

	private void createTeam(UploadFileDTO uploadFileDTO) {
		List<Team> teamsFromDB = teamRepository.findBySupervisorId(uploadFileDTO.getUsername());
		if (teamsFromDB == null || teamsFromDB.isEmpty()) {
			teamsFromDB = teamRepository.findBySupervisorId("donotdeletesup");
			if (teamsFromDB != null) {
				Team teamFromDB = teamsFromDB.get(0);
				Address address = teamFromDB.getAddress();
				TeamResource supervisorTeamResource = new TeamResource(uploadFileDTO.getUsername());
				String teamName = uploadFileDTO.getOrganization() + " TEAM";
				Team team = new Team(uploadFileDTO.getOrganization(), supervisorTeamResource, teamName, address);
				team.setOverview("A team skilled in Technology selling space with proficiency in English and Spanish.");
				team.setStatus(TeamStatus.ACTIVE);
				team.setAdminSupervisorIds(uploadFileDTO.getAdminSupervisorIds());
				List<TeamResource> agentTeamResourceList = new ArrayList<TeamResource>();
				agentTeamResourceList = createAgentTeamResource(uploadFileDTO);
				for (TeamResource teamResource : agentTeamResourceList) {
					team.addAgent(teamResource);
				}

				List<TeamResource> qaTeamResourceList = new ArrayList<TeamResource>();
				qaTeamResourceList = createQaTeamResource(uploadFileDTO);
				for (TeamResource teamResource : qaTeamResourceList) {
					team.addQA(teamResource);
				}

				List<TeamResource> secQaTeamResourceList = new ArrayList<TeamResource>();
				secQaTeamResourceList = createSecQaTeamResource(uploadFileDTO);
				for (TeamResource teamResource : secQaTeamResourceList) {
					team.addQA(teamResource);
				}

				if (!agentTeamResourceList.isEmpty()
						&& (!qaTeamResourceList.isEmpty() || !secQaTeamResourceList.isEmpty())) {
					TeamDTO teamDB = teamService.createTeam(new TeamDTO(team));
					logger.debug("Team [{}] created successfully.", teamDB.getName());
				}
			}
		}
	}

	private List<TeamResource> createAgentTeamResource(UploadFileDTO uploadFileDTO) {
		List<UploadFileDTO> agentList = loginHashMap.get("AGENT");
		List<TeamResource> agentTeamResourceList = new ArrayList<TeamResource>();
		if (agentList != null && !agentList.isEmpty()) {
			for (UploadFileDTO agentLogin : agentList) {
				if (uploadFileDTO.getUsername().equalsIgnoreCase(agentLogin.getSupervisorId())) {
					Agent agent = agentRepository.findOneById(agentLogin.getUsername());
					if (agent != null) {
						TeamResource agentResource = new TeamResource(agent.getId());
						agentResource.setAddress(agent.getAddress());
						if (agent.getDomains() != null) {
							for (String domain : agent.getDomains()) {
								agentResource.addDomain(domain);
							}
						}
						if (agent.getLanguages() != null) {
							for (String language : agent.getLanguages()) {
								agentResource.addLanguage(language);
							}
						}
						if (agent.getWorkSchedules() != null) {
							for (WorkSchedule workSchedule : agent.getWorkSchedules()) {
								agentResource.addWorkSchedule(workSchedule);
							}
						}
						agentTeamResourceList.add(agentResource);
					}
				}
			}
		}
		return agentTeamResourceList;
	}

	private List<TeamResource> createQaTeamResource(UploadFileDTO uploadFileDTO) {
		List<UploadFileDTO> qaList = loginHashMap.get("QA");
		List<TeamResource> qaTeamResourceList = new ArrayList<TeamResource>();
		if (qaList != null && !qaList.isEmpty()) {
			for (UploadFileDTO qaLogin : qaList) {
				if (uploadFileDTO.getUsername().equalsIgnoreCase(qaLogin.getSupervisorId())) {
					Qa qa = qaRepository.findOneById(qaLogin.getUsername());
					if (qa != null) {
						TeamResource qaResource = new TeamResource(qa.getId());
						qaResource.setAddress(qa.getAddress());
						if (qa.getDomains() != null) {
							for (String domain : qa.getDomains()) {
								qaResource.addDomain(domain);
							}
						}
						if (qa.getLanguages() != null) {
							for (String language : qa.getLanguages()) {
								qaResource.addLanguage(language);
							}
						}
						if (qa.getWorkSchedules() != null) {
							for (WorkSchedule workSchedule : qa.getWorkSchedules()) {
								qaResource.addWorkSchedule(workSchedule);
							}
						}
						qaTeamResourceList.add(qaResource);
					}
				}
			}
		}
		return qaTeamResourceList;
	}

	private List<TeamResource> createSecQaTeamResource(UploadFileDTO uploadFileDTO) {
		List<UploadFileDTO> qaList = loginHashMap.get("SECONDARYQA");
		List<TeamResource> qaTeamResourceList = new ArrayList<TeamResource>();
		if (qaList != null && !qaList.isEmpty()) {
			for (UploadFileDTO qaLogin : qaList) {
				if (uploadFileDTO.getOrganization().equalsIgnoreCase(qaLogin.getOrganization())) {
					Qa qa = qaRepository.findOneById(qaLogin.getUsername());
					if (qa != null) {
						TeamResource qaResource = new TeamResource(qa.getId());
						qaResource.setAddress(qa.getAddress());
						if (qa.getDomains() != null) {
							for (String domain : qa.getDomains()) {
								qaResource.addDomain(domain);
							}
						}
						if (qa.getLanguages() != null) {
							for (String language : qa.getLanguages()) {
								qaResource.addLanguage(language);
							}
						}
						if (qa.getWorkSchedules() != null) {
							for (WorkSchedule workSchedule : qa.getWorkSchedules()) {
								qaResource.addWorkSchedule(workSchedule);
							}
						}
						qaTeamResourceList.add(qaResource);
					}
				}
			}
		}
		return qaTeamResourceList;
	}
	
	private void sendEmailWithLoginFileAttached(Workbook book, String xlsFileName)
			throws IOException {
		Date date = new Date();
		String subject = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY ");
		String dateStr = sdf.format(date);
		String fileName = dateStr + xlsFileName;
		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		try {
			book.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		book.close();// ABM Upload Status for MA-MIR-file uploaded with error
		subject = "Login Upload Status Success";
		String message = " Please Find The Attached Team Creation File.";
		XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com", subject, message,
				fileNames);
	}

}
