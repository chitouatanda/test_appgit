package com.xtaas.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.BeanLocator;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.application.service.PlivoOutboundNumberServiceImpl;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.insideview.*;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.*;
import com.xtaas.web.dto.Companies;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class LeadIQManualSearchProspectServiceImpl implements Runnable {

	CampaignRepository campaignRepository;

	CompanyMasterRepository companyMasterRepository;

	private ManualProspectSearchServiceImpl manualProspectSearchServiceImpl;

	private ProspectCallLogRepository prospectCallLogRepository;

	private Prospect prospect;

	private String agentId;

	private CountDownLatch latch;

	private Campaign campaign;

	private GlobalContactRepository globalContactRepository;

	PlivoOutboundNumberRepository plivoOutboundNumberRepository;

	StateCallConfigRepository stateCallConfigRepository;

	AbmListServiceImpl abmListServiceImpl;

	private boolean manualSearch;

	private PlivoOutboundNumberService plivoOutboundNumberService;

	private ZoomInfoDataBuy zoomInfoDataBuy;

	private List<DomainCompanyCountPair> successCompanyData;

	public LeadIQManualSearchProspectServiceImpl(Prospect prospect, Campaign campaign, String agentId, CountDownLatch latch,
												 boolean manualSearch,List<DomainCompanyCountPair> successCompanyData) {
		this.prospect = prospect;
		this.successCompanyData = successCompanyData;
		this.agentId = agentId;
		this.latch = latch;
		this.campaign = campaign;
		this.manualSearch = manualSearch;
		campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		companyMasterRepository = BeanLocator.getBean("companyMasterRepository", CompanyMasterRepository.class);
		manualProspectSearchServiceImpl = BeanLocator.getBean("manualProspectSearchServiceImpl", ManualProspectSearchServiceImpl.class);
		prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
		globalContactRepository = BeanLocator.getBean("globalContactRepository", GlobalContactRepository.class);
		plivoOutboundNumberRepository = BeanLocator.getBean("plivoOutboundNumberRepository", PlivoOutboundNumberRepository.class);
		stateCallConfigRepository = BeanLocator.getBean("stateCallConfigRepository", StateCallConfigRepository.class);
		abmListServiceImpl = BeanLocator.getBean("abmListServiceImpl", AbmListServiceImpl.class);
		plivoOutboundNumberService = BeanLocator.getBean("plivoOutboundNumberServiceImpl", PlivoOutboundNumberService.class);
		zoomInfoDataBuy = BeanLocator.getBean("zoomInfoDataBuy", ZoomInfoDataBuy.class);
	}

	@Override
	public void run() {
		try {
			getLeadIQProspect(prospect);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.latch.countDown();
	}

	public void getLeadIQProspect(Prospect prospect){
		try {
			//if ( prospect != null && StringUtils.isNotBlank(prospect.getTitle())) {
				String country = null;
				String title = null;
				CampaignCriteria cc = getCampaignCriteria(campaign);
				if(CollectionUtils.isNotEmpty(cc.getCountryList())){
					country = "\"" + StringUtils.join(cc.getCountryList(), "\",\"") + "\"";
					if(country.contains("USA")) {
						country = country.replace("USA","United States");
					}
				}
				if(StringUtils.isNotBlank(cc.getManagementLevel())){
					List<String> titleList = Arrays.asList(cc.getManagementLevel().split(","));
					title = "\"" + StringUtils.join(titleList, "\",\"") + "\"";
				}
				boolean isFull = true;
				AdvanceLeadIQSearchResponse advanceLeadIQSearchResponse= null;
				boolean isDomain =true;
				if(campaign.isABM()){
					if(manualSearch){
						if(prospect.getCustomAttributes().get("domain") != null &&
								StringUtils.isNotBlank(prospect.getCustomAttributes().get("domain").toString())){
							advanceLeadIQSearchResponse = leadIQAdvancedSearch(prospect.getCustomAttributes().get("domain").toString(),prospect.getTitle(), isDomain,country);
						}else if(advanceLeadIQSearchResponse == null &&  StringUtils.isNotBlank(prospect.getCompany())) {
							isDomain = false;
							advanceLeadIQSearchResponse = leadIQAdvancedSearch(prospect.getCompany(),prospect.getTitle(), isDomain,country);
						}
					}

					/*Pageable pageable = PageRequest.of(0, 9000);
					List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
					if(CollectionUtils.isNotEmpty(abmList)){
						List<String> abmDomains = abmList.stream().map(AbmListNew ::getDomain).collect(Collectors.toList());
						cc.setAbmDomains(abmDomains);
						String abmDomainsList  = StringUtils.join(abmDomains, "\",\"") ;
						advanceLeadIQSearchResponse = leadIQAdvancedSearchForABM(abmDomainsList,country);
					}*/
				}else{
					if(manualSearch){
						if(prospect.getCustomAttributes().get("domain") != null &&
								StringUtils.isNotBlank(prospect.getCustomAttributes().get("domain").toString())){
							advanceLeadIQSearchResponse = leadIQAdvancedSearch(prospect.getCustomAttributes().get("domain").toString(),prospect.getTitle(), isDomain,country);
						}else if(advanceLeadIQSearchResponse == null &&  StringUtils.isNotBlank(prospect.getCompany())) {
							isDomain = false;
							advanceLeadIQSearchResponse = leadIQAdvancedSearch(prospect.getCompany(),prospect.getTitle(), isDomain,country);
						}
					}/*else {
						if(StringUtils.isNotBlank(prospect.getCompany()) && StringUtils.isNotBlank(title)){
							advanceLeadIQSearchResponse = leadIQAdvancedSearchByCampaign(country,title, prospect.getCompany());
						}

					}*/
				}

				if(advanceLeadIQSearchResponse != null && advanceLeadIQSearchResponse.getData() != null && advanceLeadIQSearchResponse.getData().getGroupedAdvancedSearch() != null
						&& advanceLeadIQSearchResponse.getData().getGroupedAdvancedSearch().getTotalCompanies() > 0){
					List<Companies> companiesList = advanceLeadIQSearchResponse.getData().getGroupedAdvancedSearch().getCompanies();
					if(CollectionUtils.isNotEmpty(companiesList)) {
						for(Companies companies : companiesList){
							if(companies != null && companies.getPeople() != null && companies.getCompany() != null){
								Company company = companies.getCompany();
								for(People people : companies.getPeople()){
									if(people != null){
										LeadIQSearchResponse leadIQSearchResponse = null;
										if (StringUtils.isNotBlank(people.getName()) && StringUtils.isNotBlank(company.getDomain())) {
											leadIQSearchResponse = searchContactByNameAndDomain(people.getName(), company.getDomain());
										} else if (StringUtils.isNotBlank(people.getName()) && StringUtils.isNotBlank(company.getName())) {
											leadIQSearchResponse = searchContactByNameAndCompany(people.getName(), company.getName());
										} else {
											leadIQSearchResponse = searchContactByLinkedIn(people.getLinkedinUrl());
										}
										if (leadIQSearchResponse != null && leadIQSearchResponse.getData() != null && leadIQSearchResponse.getData().getSearchPeople() != null) {
											if (leadIQSearchResponse.getData().getSearchPeople().getTotalResults() > 0) {
												for (LeadIQResult result : leadIQSearchResponse.getData().getSearchPeople().getResults()) {
													try {
														Prospect prospectObj = createProspectObj(result);
														if (prospectObj != null && prospectObj.getSourceId() != null){
															if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null &&
																	manualProspectSearchServiceImpl.manualProspectMap.get(agentId).size() >= manualProspectSearchServiceImpl.SIMILAR_PROSPECT_SIZE) {
																isFull = false;
																return;
															}
															int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordBySourceId(campaign.getId(), prospectObj.getSourceId());
															boolean isValidCountry = isValidCountry(cc, prospectObj);
															boolean isValidCountryPhone  = isValidCountryPhone(prospectObj);
															boolean isValidDomain = zoomInfoDataBuy.isValidDomain(prospectObj,cc, campaign);
															if (uniqueRecord == 0 && isValidCountry && isValidCountryPhone) {
																if (isValidDomain && prospectObj.getPhone() != null) {
																	List<Prospect> listDto = manualProspectSearchServiceImpl.manualProspectMap.get(agentId);
																	if (CollectionUtils.isNotEmpty(listDto)) {
																		List<Prospect> plist = manualProspectSearchServiceImpl.manualProspectMap.get(agentId);
																		boolean itemExists = plist.stream().anyMatch(c -> c.getPhone().equals(prospectObj.getPhone()));
																		if(!itemExists){
																			manualProspectSearchServiceImpl.manualProspectMap.get(agentId).add(prospectObj);
																		}
																	} else {
																		List<Prospect> emptyListDto = new ArrayList<>();
																		emptyListDto.add(prospectObj);
																		manualProspectSearchServiceImpl.manualProspectMap.put(agentId, emptyListDto);
																	}
																	saveGlobalContact(prospectObj);
																}
																if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null &&
																		manualProspectSearchServiceImpl.manualProspectMap.get(agentId).size() >= manualProspectSearchServiceImpl.SIMILAR_PROSPECT_SIZE) {
																	isFull = false;
																	return;
																}
															}
														}
													}catch (Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}
									if(!isFull) {
										return;
									}
								}
							}
							if(!isFull) {
								return;
							}
						}
					}
				}else {
					return;
				}
			//}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private boolean isValidCountry(CampaignCriteria cc, Prospect prospectObj) {
		boolean isValid = false;
		String prospectCountry = prospectObj.getCountry();
		List<String> ccCountryList = cc.getCountryList();
		List<String> newList  = new ArrayList<>();
		newList.addAll(ccCountryList);
		boolean containsUSA = newList.stream().anyMatch("usa"::equalsIgnoreCase);
		boolean containsUnitedStates = newList.stream().anyMatch("United States"::equalsIgnoreCase);
		if(containsUSA){
			newList.add("United States");
		}else if(containsUnitedStates){
			newList.add("usa");
		}
		boolean isValidCntry = newList.stream().anyMatch(prospectCountry::equalsIgnoreCase);
		if(isValidCntry){
			isValid = true;
		}
		return isValid;
	}


	private Prospect createProspectObj(LeadIQResult result) {
		try {
			Prospect prospectInfo = new Prospect();
			prospectInfo.setLeadIQResult(result);
			prospectInfo.setSource("LeadIQ");
			prospectInfo.setDataSource("Xtaas Data Source");
			prospectInfo.setSourceType("INTERNAL");
			prospectInfo.setPhone("");
			prospectInfo.setPhoneType("");
			// LeadIQResult result = responseObj.getData().getSearchPeople().getResults().get(0);
			String personIDLeadIQ = result.getId();
			prospectInfo.setSourceId(personIDLeadIQ);
			if (result.getCurrentPositions() != null && result.getCurrentPositions().size() > 0) {
				LeadIQCurrentPosition contactDetails = result.getCurrentPositions().get(0);
				prospectInfo.setCompany(contactDetails.getCompanyInfo().getName());
				prospectInfo.setIndustry(contactDetails.getCompanyInfo().getIndustry());
				prospectInfo.setFirstName(result.getName().getFirst());
				prospectInfo.setLastName(result.getName().getLast());
				for (LeadIQEmail email : contactDetails.getEmails()) {
					if (email.getStatus().equalsIgnoreCase("Verified")
							&& email.getType().equalsIgnoreCase("workemail") && !email.getValue().equalsIgnoreCase("*redacted*")) {
						prospectInfo.setEmail(email.getValue());
						break;
					}
				}
//				if (prospectInfo.getEmail() == null || prospectInfo.getEmail().isEmpty()) {
//					if (prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
//						prospectInfo.setEmail(prospect.getEmail());
//					}
//				}
//				if (prospect.getLinkedInURL() != null && !prospect.getLinkedInURL().isEmpty()) {
//					prospectInfo.setLinkedInURL(prospect.getLinkedInURL());
//				}
				if (result.getPersonalPhones() != null) {
					prospectInfo.setPhone(result.getPersonalPhones().getValue());
					prospectInfo.setPhoneType(result.getPersonalPhones().getType());
				} else {
					boolean first = true;
					for (LeadIQPhone phone : contactDetails.getPhones()) {
						//System.out.println("phone number" + phone);
						if (phone.getStatus().equalsIgnoreCase("Verified")) {
							if (phone.getType().toLowerCase().contains("personalphone")) {
								prospectInfo.setPhone(phone.getValue());
								prospectInfo.setPhoneType(phone.getType());
								break;
							}
							if (phone.getType().equalsIgnoreCase("workphone") || first) {
								first = false;
								prospectInfo.setPhone(phone.getValue());
								prospectInfo.setPhoneType(phone.getType());
							}
						}
					}
				}

				if (contactDetails.getCompanyInfo().getPhones() != null && contactDetails.getCompanyInfo().getPhones().size() > 0) {
					prospectInfo.setCompanyPhone(contactDetails.getCompanyInfo().getPhones().get(0));
				}


				prospectInfo.setTitle(contactDetails.getTitle());

				if (prospectInfo.getPhone() != null && !prospectInfo.getPhone().isEmpty()) {
					String extension = "";
					String countryCode = getCountryCode(prospectInfo);
					String phoneNumber = getPhoneNumber(prospectInfo);
					if (prospectInfo.getPhone().contains("ext")) {
						extension = getExtension(prospectInfo);
					}
//						if (countryCode != null) {
//							prospectInfo.setPhone("+".concat(countryCode.concat(" " + phoneNumber)));
//						}
					if (!prospectInfo.getPhone().contains("+")) {
						prospectInfo.setPhone("+".concat(" " + phoneNumber));
					} else {
						prospectInfo.setPhone(phoneNumber);
					}
					//System.out.println("country Code" + countryCode + "PhoneNumber" + phoneNumber + "Extension" + extension);
//						if (!countryCode.equals("1")) {
//							prospectInfo.setPhone(countryCode.concat(phoneNumber));
//						} else {
//							prospectInfo.setPhone(phoneNumber);
//						}
					//setCountry
					if (contactDetails.getCompanyInfo().getCountry() != null && !contactDetails.getCompanyInfo().getCountry().isEmpty()) {
						prospectInfo.setCountry(contactDetails.getCompanyInfo().getCountry());
					} else {
						prospectInfo.setCountry(getCountryName(countryCode));
					}
					//  get state code and timezone
					if (contactDetails.getCompanyInfo().getLocationInfo() != null && contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1() != null && prospectInfo.getCountry() != null) {
						StateCallConfig scc = getStateCodeAndTimeZone(contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1(), prospectInfo.getCountry());
						if (scc != null) {
							prospectInfo.setTimeZone(scc.getTimezone());
							prospectInfo.setStateCode(scc.getStateCode());
						}
					}
					if (contactDetails.getCompanyInfo() != null && contactDetails.getCompanyInfo().getLocationInfo() != null) {
						Map<String, String> companyAddress = insertCompanyAddressInMap(contactDetails);  //added company address
						prospectInfo.setCompanyAddress(companyAddress);
					}
					if (contactDetails.getCompanyInfo() != null && contactDetails.getCompanyInfo().getNumberOfEmployees() != null && !contactDetails.getCompanyInfo().getNumberOfEmployees().toString().isEmpty()) {
						prospectInfo.setCustomAttribute("numberOfEmployees", contactDetails.getCompanyInfo().getNumberOfEmployees().toString());
					}
					prospectInfo.setZoomPersonUrl(result.getLinkedin().getLinkedinUrl());
					prospectInfo.setCustomAttribute("domain", contactDetails.getCompanyInfo().getDomain()); // added domain
					if (result.getLinkedin() != null) {
						prospectInfo.setCustomAttribute("linkedinId", result.getLinkedin().getLinkedinId());
						prospectInfo.setCustomAttribute("linkedinUrl", result.getLinkedin().getLinkedinUrl());
					}

					return prospectInfo;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getCountryName(String countryCode) {
		PlivoOutboundNumber plivoOutboundNumber = plivoOutboundNumberRepository.findOneByIsdCode(Integer.parseInt(countryCode.trim()));
		if (plivoOutboundNumber != null) {
			return plivoOutboundNumber.getCountryName();
		}
		return null;
	}

	private Map<String, String> insertCompanyAddressInMap(LeadIQCurrentPosition contactDetails) {
		Map<String, String> companyAddress = new HashedMap<>();
		companyAddress.put("areaLevel1", contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1());
		companyAddress.put("city", contactDetails.getCompanyInfo().getLocationInfo().getCity());
		companyAddress.put("country", contactDetails.getCompanyInfo().getLocationInfo().getCountry());
		companyAddress.put("formattedAddress", contactDetails.getCompanyInfo().getLocationInfo().getFormattedAddress());
		companyAddress.put("postalCode", contactDetails.getCompanyInfo().getLocationInfo().getPostalCode());
		companyAddress.put("street1", contactDetails.getCompanyInfo().getLocationInfo().getStreet1());
		companyAddress.put("street2", contactDetails.getCompanyInfo().getLocationInfo().getStreet2());
		return companyAddress;
	}

	private StateCallConfig getStateCodeAndTimeZone(String stateName,String countryName) {
		StateCallConfig stateCallConfigFromDB = null;

		if (stateName != null && !stateName.isEmpty()) {
			List<StateCallConfig> stateCallConfigFromDBList = stateCallConfigRepository.findByStateName(stateName);
			if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
				stateCallConfigFromDB = stateCallConfigFromDBList.get(0);
			} else {
				stateCallConfigFromDBList = stateCallConfigRepository.findByCountryName(countryName);
				if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
					stateCallConfigFromDB = stateCallConfigFromDBList.get(0);
				}
			}
		}
		return stateCallConfigFromDB;
	}

	private String getExtension(Prospect prospectinfo) {
		String phone = prospectinfo.getPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			int beginIndex = phone.indexOf("ext");
			ext = phone.substring(beginIndex);
			ext = ext.replaceAll("[^\\d]", "");
		}
		return ext;
	}

	private String getPhoneNumber(Prospect prospectInfo) {
		String phone = prospectInfo.getPhone();
		if (phone != null && phone.contains("ext")) {
			int beginIndex = phone.indexOf("ext");
			String validphone = phone.substring(0, beginIndex);
			validphone = validphone.replaceAll("[^\\d]", "");
			phone = "+" + validphone.trim();
		} else {
			phone = phone.replaceAll("[^\\d]", "");
			phone = "+" + phone.trim();
		}
		return phone;
	}

	private String getCountryCode(Prospect prospectinfo) {
		String phone = prospectinfo.getPhone();
		int firstPosition = -1;
		if (phone != null && phone.contains("-")) {
			firstPosition = phone.indexOf("-");
		}
		if (phone != null && phone.contains(" ")) {
			firstPosition = phone.indexOf(" ");
		}
		if (firstPosition != -1) {
			return phone.substring(0, firstPosition).replace("+", "");
		}
		return null;
	}

	private AdvanceLeadIQSearchResponse leadIQAdvancedSearchByCampaign(String country,String title, String company) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{\"query\": \"query AdvancedSearch($input: GroupedSearchInput!) {  groupedAdvancedSearch(input: $input) { totalCompanies companies { company { id name industry linkedinId domain employeeCount country state } people { id name title linkedinId linkedinUrl workEmails workPhones personalEmails personalPhones } } }}\",\"variables\": {\"input\":{\"companyFilter\":{\"names\":[\"");
		payload.append(company);
		payload.append("\"]");
		if(StringUtils.isNotBlank(country)){
			payload.append("\"countries\":[");
			payload.append(country);
			payload.append("]");
		}else {
			payload.append("\"countries\":[");
			payload.append("");
			payload.append("]");
		}
		payload.append("}, \"contactFilter\":{");
		if(StringUtils.isNotBlank(title)){
			payload.append("\"titles\":[");
			payload.append(title);
			payload.append("]");
		}else{
			payload.append("\"titles\":[");
			payload.append("");
			payload.append("]");
		}

		payload.append("},\"limitPerCompany\":40,\"sortContactsBy\":[\"NameAsc\"]}},\"operationName\":\"AdvancedSearch\"} }");
		AdvanceLeadIQSearchResponse responseObj = getAdvanceLeadIQSearchResponse(payload.toString());
		return responseObj;
	}

	private AdvanceLeadIQSearchResponse leadIQAdvancedSearch(String domain,String title, boolean isDomain,String country) {
		StringBuilder payload = new StringBuilder();
		if(isDomain){
			payload.append(
					"{\"query\": \"query AdvancedSearch($input: GroupedSearchInput!) {  groupedAdvancedSearch(input: $input) { totalCompanies companies { company { id name industry linkedinId domain employeeCount country state } people { id name title linkedinId linkedinUrl workEmails workPhones personalEmails personalPhones } } }}\",\"variables\": {\"input\":{\"companyFilter\":{\"domains\":[\"");
		}else {
			payload.append(
					"{\"query\": \"query AdvancedSearch($input: GroupedSearchInput!) {  groupedAdvancedSearch(input: $input) { totalCompanies companies { company { id name industry linkedinId domain employeeCount country state } people { id name title linkedinId linkedinUrl workEmails workPhones personalEmails personalPhones } } }}\",\"variables\": {\"input\":{\"companyFilter\":{\"names\":[\"");
		}
		payload.append(domain);
		payload.append("\"]");
		if(StringUtils.isNotBlank(country)){
			payload.append(",");
			payload.append("\"countries\":[");
			payload.append(country);
			payload.append("]");
		}
		payload.append("}, \"contactFilter\":{\"titles\":[\"");
		payload.append(title);
		payload.append("\"]},\"limitPerCompany\":20,\"sortContactsBy\":[\"NameAsc\"]}},\"operationName\":\"AdvancedSearch\"} }");
		AdvanceLeadIQSearchResponse responseObj = getAdvanceLeadIQSearchResponse(payload.toString());
		return responseObj;
	}

	private AdvanceLeadIQSearchResponse getAdvanceLeadIQSearchResponse(String payload) {
		String apiKey = ApplicationEnvironmentPropertyUtils.getLeadIQAPIKey();
		String leadIQAPIURL = ApplicationEnvironmentPropertyUtils.getLeadIQUrl();
		String apiKeyEncoded = Base64.getEncoder().encodeToString(apiKey.getBytes());
		String responseJsonString = null;
		try{
			URL url =new URL(leadIQAPIURL);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty ("Authorization", "Basic " + apiKeyEncoded);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_TYPE.toString());
			connection.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
				wr.writeBytes(payload);
				wr.flush();
			}
			int responseCode = connection.getResponseCode();

			BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response1= new StringBuffer();
			while((inputline=bufferedReader.readLine())!=null)
			{
				response1.append(inputline);
			}
			bufferedReader.close();
			responseJsonString = response1.toString();
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}

		AdvanceLeadIQSearchResponse reponseObj = null;
		if (StringUtils.isNotBlank(responseJsonString)) {
			try {
				reponseObj = new com.fasterxml.jackson.databind.ObjectMapper().readValue(responseJsonString, AdvanceLeadIQSearchResponse.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reponseObj;
	}

	private LeadIQSearchResponse searchContactByNameAndCompany(String name,String company) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }\",  \"variables\": {  \"input\": {      \"fullName\": \"");
		payload.append(name);
		payload.append("\",  \"company\": { \"name\": \"");
		payload.append(company);
		payload.append("\" }}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactByNameAndDomain(String name, String domain) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }\",    \"variables\": { \"input\": {        \"fullName\": \"");
		payload.append(name);
		payload.append("\", \"company\": { \"domain\": \"");
		payload.append(domain);
		payload.append("\" }}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactByLinkedIn(String linkedInURL) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo { name industry } emails { value type status } phones { value type status } } } } }\", \"variables\": {\"input\": { \"linkedinUrl\": \"");
		payload.append(linkedInURL); // need to append linkedinurl
		payload.append("\"}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse getContactSearchResponse(String payload) {
		String apiKey = ApplicationEnvironmentPropertyUtils.getLeadIQAPIKey();
		String leadIQAPIURL = ApplicationEnvironmentPropertyUtils.getLeadIQUrl();
		String apiKeyEncoded = Base64.getEncoder().encodeToString(apiKey.getBytes());
		String responseJsonString = null;
		try {
			URL url =new URL(leadIQAPIURL);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty ("Authorization", "Basic " + apiKeyEncoded);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_TYPE.toString());
			connection.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
				wr.writeBytes(payload);
				wr.flush();
			}
			int responseCode = connection.getResponseCode();

			BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response1= new StringBuffer();
			while((inputline=bufferedReader.readLine())!=null)
			{
				response1.append(inputline);
			}
			bufferedReader.close();
			responseJsonString = response1.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		LeadIQSearchResponse reponseObj = null;
		if (StringUtils.isNotBlank(responseJsonString)) {
			try {
				reponseObj = new com.fasterxml.jackson.databind.ObjectMapper().readValue(responseJsonString, LeadIQSearchResponse.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reponseObj;
	}

	private void saveGlobalContact(Prospect prospect){
		zoomInfoDataBuy.saveGlobalContact(prospect);
	}

	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();
			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
		}
		return cc;
	}



	private AdvanceLeadIQSearchResponse leadIQAdvancedSearchForABM(String domains, String country) {
		StringBuilder payload = new StringBuilder();
			payload.append(
					"{\"query\": \"query AdvancedSearch($input: GroupedSearchInput!) {  groupedAdvancedSearch(input: $input) { totalCompanies companies { company { id name industry linkedinId domain employeeCount country state } people { id name title linkedinId linkedinUrl workEmails workPhones personalEmails personalPhones } } }}\",\"variables\": {\"input\":{\"companyFilter\":{\"domains\":[\"");

		payload.append(domains);
		payload.append("\"]");
		if(StringUtils.isNotBlank(country)){
			payload.append(",");
			payload.append("\"countries\":[");
			payload.append(country);
			payload.append("]");
		}else {
			payload.append("\"countries\":[");
			payload.append("");
			payload.append("]");
		}
		payload.append("},\"limitPerCompany\":40,\"sortContactsBy\":[\"NameAsc\"]}},\"operationName\":\"AdvancedSearch\"} }");
		AdvanceLeadIQSearchResponse responseObj = getAdvanceLeadIQSearchResponse(payload.toString());
		return responseObj;
	}


	private boolean isValidCountryPhone(Prospect prospectObj) {
		boolean isValid = false;
		if(prospectObj != null && prospectObj.getPhone()!= null){
			String phone  =  prospectObj.getPhone();
			String tempPhone = phone.replaceAll("[^A-Za-z0-9]", "");
			String prospectCountry = prospectObj.getCountry();
			Integer isdCode = plivoOutboundNumberService.getIsdCodeFromCountryName(prospectCountry);
			if (isdCode != null && tempPhone.startsWith(Integer.toString(isdCode))) {
				isValid = true;
			}
		}
		return isValid;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
}
