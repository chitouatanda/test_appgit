package com.xtaas.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.mvc.model.AutoApproveAgentRequestDTO;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import java.util.stream.Collectors;

import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;

@Service
public class AutoApproveAgentRequestServiceImpl implements AutoApproveAgentRequestService {

	private final Logger logger = LoggerFactory.getLogger(AutoApproveAgentRequestServiceImpl.class);

	@Autowired
	PropertyService propertyService;

	@Autowired
	AgentService agentService;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	LoginRepository loginRepository;

	@Autowired
	TeamNoticeService teamNoticeService;
	
	@Autowired
	AgentActivityLogRepository agentActivityLogRepository;

	private Map<String, AutoApproveAgentRequestDTO> agentRequestMap = new HashMap<String, AutoApproveAgentRequestDTO>();

	@Override
	public void insertAgentRequest(String agentId, String campaignId, String agentType, AgentStatus agentStatus) {
		if (agentId != null && !agentId.isEmpty() && campaignId != null && !campaignId.isEmpty() && agentType != null
				&& !agentType.isEmpty()) {
			agentRequestMap.put(agentId,
					new AutoApproveAgentRequestDTO(campaignId, agentType, agentStatus, new Date()));
			logger.debug(new SplunkLoggingUtils("insertAgentRequest", agentId)
					.eventDescription("Inserted agent status change request in map.").build());
		}
	}

	@Override
	public void removeApprovedRequest(String agentId) {
		if (!agentId.isEmpty()) {
			agentRequestMap.remove(agentId);
			logger.debug(new SplunkLoggingUtils("removeApprovedRequest", agentId)
					.eventDescription("Removed supervisor approved request.").build());
		}
	}

	@Override
	public void autoApproveAgentRequest() {
		try {
			String approvalTime = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.AGENT_REQUEST_APPROVAL_TIME.name(), "30");
			if (!agentRequestMap.isEmpty()) {
				for (Map.Entry<String, AutoApproveAgentRequestDTO> entry : agentRequestMap.entrySet()) {
					long diffInMin = XtaasDateUtils.getTimeDiffrenceInMinutes(new Date(), entry.getValue().getRequestedTime());
					if (diffInMin >= Long.parseLong(approvalTime)) {
						agentService.approveAgentStatusChangeRequestBySupervisor(entry.getKey(),
								entry.getValue().getAgentStatus(), entry.getValue().getAgentType(),
								entry.getValue().getCampaignId(), true);
						agentRequestMap.remove(entry.getKey());
						notifySupervisor(entry.getKey(), entry.getValue().getAgentStatus());
					}
					logger.debug(new SplunkLoggingUtils("autoApproveAgentRequest", entry.getKey())
							.eventDescription("Auto approved agent request.").build());
				}
			}
		} catch (Exception e) {
			logger.error(new SplunkLoggingUtils("autoApproveAgentRequest", "")
					.eventDescription("Failed to auto approved agent request").addThrowableWithStacktrace(e).build());
		}
	}

	public String notifySupervisor(String agentId, AgentStatus agentStatus) {
		List<Team> teamsFromDB = teamRepository.findAllTeamsByAgentId(agentId);
		Agent agentFromDB = agentService.getAgent(agentId);
		if (teamsFromDB != null && teamsFromDB.size() > 0) {
			List<String> supervisors = teamsFromDB.stream().map(team -> team.getSupervisor().getResourceId())
					.collect(Collectors.toList());
			if (supervisors != null && supervisors.size() > 0) {
				String notice = "";
				for (String supervisor : supervisors) {
					PusherUtils.pushMessageToUser(supervisor, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					notice = agentStatus.toString() + " requested by Agent Name " + agentFromDB.getFirstName() + " " + agentFromDB.getLastName()
							+ " Agent ID " + agentId + " from organization " + agentFromDB.getPartnerId()
							 + " is auto approved";
					teamNoticeService.saveUserNotices(supervisor, notice);
					sendNotificationEmail(supervisor, notice);
				}

			}
		}
		return null;
	}

	private void sendNotificationEmail(String supervisor, String notice) {
		Login loginUser = loginRepository.findOneById(supervisor);
		if (loginUser != null) {
			List<String> reportingEmail = loginUser.getReportEmails();
			if (reportingEmail != null && reportingEmail.size() > 0) {
				for (String email : reportingEmail) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", "Auto approve agent break request", notice);
				}
			} else {
				if (loginUser.getEmail() != null && !loginUser.getEmail().isEmpty()) {
					XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",
							"Auto approve agent break request", notice);
				}
			}
			XtaasEmailUtils.sendEmail("supportteam@xtaascorp.com", "data@xtaascorp.com",
					"Auto approve agent break request", notice);
		}
	}
}
