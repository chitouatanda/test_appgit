/**
 * 
 */
package com.xtaas.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CallAbandonOptOutRequest;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.repository.CallAbandonOptOutRequestRepository;
import com.xtaas.db.repository.CallLogRepository;

/**
 * @author djain
 *
 */
@Service
public class CallLogServiceImpl implements CallLogService {
	
	private static final Logger logger = LoggerFactory.getLogger(CallLogServiceImpl.class);
	
	@Autowired
	private CallLogRepository callLogRepository;

	@Autowired
	private CallAbandonOptOutRequestRepository callAbandonOptOutRequestRepository;
	
		
	/* (non-Javadoc)
	 * @see com.xtaas.service.CallLogService#saveCallLog(com.xtaas.db.entity.CallLog)
	 */
	@Override
	public void saveCallLog(CallLog callLog) {
		callLogRepository.save(callLog);
		logger.debug("CallLog saved in DB: " + callLog.getId());
	}
	
	@Override
	public void saveCallLogs(List<CallLog> callLogs) {
		callLogRepository.saveAll(callLogs);
	}

	@Override
	public void saveCallAbandonOptOutRequest(CallAbandonOptOutRequest optOutRequest) {
		callAbandonOptOutRequestRepository.save(optOutRequest);
		logger.debug("Opt-Out request saved in DB. " + optOutRequest.getId());
	}
	
	@Override
	public CallLog  findCallLog(String callSid) {
		return callLogRepository.findCallLogByCallSid(callSid);
	}

	@Override
	public List<CallLog> findCallLogsByConferenceSid(String conferenceSid) {
		return callLogRepository.findCallLogsByConferenceSid(conferenceSid);
	}
}
