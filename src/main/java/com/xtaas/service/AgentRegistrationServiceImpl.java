package com.xtaas.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentCampaignDTO;

@Service
public class AgentRegistrationServiceImpl implements AgentRegistrationService {
	
	private static final Logger logger = LoggerFactory.getLogger(AgentRegistrationService.class);

	//campaign wise queue of available agent, used to find available agent to connect with prospect by Dialer
	private ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>> campaignAgentQueue = new ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>>();  
	
	//campaign wise queue of available agent, used to find available agent to connect with prospect by Dialer
	private ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>> campaignAgentManualQueue = new ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>>();  
	
	//campaign wise queue of available agent, used to find available agent to connect with prospect by Dialer
	private ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>> campaignBusyAgentQueue = new ConcurrentHashMap<String, ConcurrentLinkedQueue<AgentCall>>();

	//this collection is to map agentId with AgentCall object. it is cleaned up if heartbeat is not recd
	private ConcurrentHashMap<String, AgentCall> agentCallMap = new ConcurrentHashMap<String, AgentCall>();

	private ConcurrentHashMap<String, ProspectCall> callsidProspectMap = new ConcurrentHashMap<String, ProspectCall>(); //map of callSid with the Prospect
	
	@Override
	public ConcurrentHashMap<String, AgentCall> getAgentCallMap() {
		return agentCallMap;
	}

	public void setAgentCallMap(ConcurrentHashMap<String, AgentCall> agentCallMap) {
		this.agentCallMap = agentCallMap;
	}
	
	@Override
	public AgentCall getAvailableAgent(String campaignId) {
		ConcurrentLinkedQueue<AgentCall> agentQueue = getAgentQueue(campaignId);
		if (agentQueue != null) {
			AgentCall agentCall = agentQueue.poll();
			// add agent into campaign busy agent queue.
			/*if (campaignBusyAgentQueue.containsKey(campaignId)) {
				ConcurrentLinkedQueue<AgentCall> busyAgentQueue = campaignBusyAgentQueue.get(campaignId);
				if (busyAgentQueue != null) {
					busyAgentQueue.offer(agentCall);
				} else {
					busyAgentQueue = new ConcurrentLinkedQueue<AgentCall>(); 
					busyAgentQueue.offer(agentCall);
				}
				logger.info("<----- campaignBusyAgentQueue - busyAgentQueue size [{}], added agentCall [{}] ----->", campaignBusyAgentQueue.get(campaignId).size(), agentCall.toString());
			} else {
				ConcurrentLinkedQueue<AgentCall> newBusyAgentQueue = new ConcurrentLinkedQueue<AgentCall>();
				newBusyAgentQueue.offer(agentCall);
				campaignBusyAgentQueue.put(campaignId, newBusyAgentQueue);
				logger.info("<----- campaignBusyAgentQueue - busyAgentQueue size [{}], added new agentCall [{}] ----->", campaignBusyAgentQueue.get(campaignId).size(), agentCall.toString());
			}*/
			if (agentCall != null && agentCall.getAgentId() != null) {
				logger.info("AgentRegistrationServiceImpl====>Available agent polled AgentId : " + agentCall.getAgentId());
			}
			return agentCall;
		} else {
			logger.info("AgentRegistrationServiceImpl====>Campaign : " + campaignId + ", agentQueue : null");
			return null;
		}
	}
	
	private ConcurrentLinkedQueue<AgentCall> getAgentQueue(String campaignId) {
		return campaignAgentQueue.get(campaignId);
	}
	
	private ConcurrentLinkedQueue<AgentCall> getManualAgentQueue(String campaignId) { 
		return campaignAgentManualQueue.get(campaignId);
	}
	
	@Override
	public int getAvailableAgentsCount(String campaignId) {
		ConcurrentLinkedQueue<AgentCall> agentQueue = getAgentQueue(campaignId);
		if (agentQueue == null) {
			return 0;
		} else {
			return agentQueue.size();
		}
	}
	
	@Override
	public void registerHeartbeat(String agentId, AgentStatus newStatus) {
		AgentCall agentCall = getAgentCall(agentId);
		if (!AgentStatus.OFFLINE.equals(newStatus)) {
			if (agentCall != null) {
				agentCall.setAgentHeartbeatTime(new Date()); //if agent status is anything but OFFLINE, then set the heartbeat. Agent client keeps on sending heartbeat every 30 secs
			}
		}
	}

	@Override
	public void manageAgent(String agentId, String agentType, DialerMode campaignType,AgentStatus newStatus, AgentCampaignDTO agentCampaign) {
		AgentCall agentCall = getAgentCall(agentId);
		if (agentCall == null) {
			agentCall = new AgentCall(agentId, agentType, campaignType, newStatus);
			setAgentCall(agentId, agentCall);
		} else {
			if (!agentCampaign.isSupervisorAgentStatusApprove()) {
				agentCall.setCurrentStatus(newStatus);
			}
		}
		
		agentCall.setCurrentCampaign(agentCampaign); //agentCampaign might be null in case agent-status is NON-ONLINE. Override with NULL and the background thread will find out current campaign and push a campaing_change event
		
		if (AgentStatus.OFFLINE.equals(newStatus)) {
			if (agentCall.getProspectCall() != null) unsetProspectByCallSid(agentCall.getProspectCall().getTwilioCallSid()); //remove the callsid from callsidProspectMap
			unregisterAgent(agentCall); //unregister this agent from available agent queue
			unsetAgentCall(agentId); //remove agent from agentCallMap..only when Agent is OFFLINE
			agentCall = null; //destroy the instance, freeup the memory
			return;
		} else {
			registerHeartbeat(agentId, newStatus); 
		}
		
//		agentCall.setCurrentCampaign(agentCampaign); //agentCampaign might be null in case agent-status is NON-ONLINE. Override with NULL and the background thread will find out current campaign and push a campaing_change event
		
		if (AgentStatus.ONLINE.equals(newStatus)) {
			//if new status is ONLINE then register this agent as available agent for DIALER to pick up
			logger.info("AgentRegistrationServiceImpl====>Registering agent AgentId : " + agentCall.getAgentId());
			registerAgent(agentCall);
		} else {
			//if new status is anything but ONLINE then unregister this agent from available agent queue
			if (!agentCampaign.isSupervisorAgentStatusApprove()) {
				unregisterAgent(agentCall);
			}
		}
	}
	
	private void registerAgent(AgentCall agentCall) {
		if (agentCall.getCampaignId() == null) return; //no campaign allocated so no need to put it in available agent queue
		if (agentCall.getProspectCall() != null) return; //if agent is already working on prospect then no need to put it in available agent queue 
		
		if (agentCall.getCurrentStatus().equals(AgentStatus.ONLINE)) {
			ConcurrentLinkedQueue<AgentCall> agentQueue = campaignAgentQueue.computeIfAbsent(agentCall.getCampaignId(), k -> new ConcurrentLinkedQueue<AgentCall>());
			// commented for new computeIfAbsent implementation
			// ConcurrentLinkedQueue<AgentCall> agentQueue = getAgentQueue(agentCall.getCampaignId());
			// if (agentQueue == null) {
			// 	agentQueue = new ConcurrentLinkedQueue<AgentCall>();
			// 	campaignAgentQueue.put(agentCall.getCampaignId(), agentQueue);
			// 	logger.info("AgentRegistrationServiceImpl====>Agent added in new queue AgentId : " + agentCall.getAgentId() + ", CampaignId : " + agentCall.getCampaignId());
			// 	// remove agent from campaign busy agent queue.
			// 	/*ConcurrentLinkedQueue<AgentCall> busyAgentQueue = campaignBusyAgentQueue.get(agentCall.getCampaignId());
			// 	if (busyAgentQueue != null) {
			// 		busyAgentQueue.remove(agentCall);
			// 		if (busyAgentQueue.isEmpty()) {
			// 			campaignBusyAgentQueue.remove(agentCall.getCampaignId());
			// 		}
			// 	}*/
			// }
			ConcurrentLinkedQueue<AgentCall> manualAgentQueue = getManualAgentQueue(agentCall.getCampaignId());
			if(manualAgentQueue == null){
				manualAgentQueue = new ConcurrentLinkedQueue<AgentCall>();
				campaignAgentManualQueue.put(agentCall.getCampaignId(), manualAgentQueue);
			}
						
			synchronized(agentQueue) {
				if (!agentQueue.contains(agentCall) && agentCall.getAgentType().equals(agentSkill.AUTO)) {
					agentQueue.offer(agentCall);
					logger.info("AgentRegistrationServiceImpl====>registerAgent() : Agent registered as an available Agent. AgentId: [{}]", agentCall.getAgentId());
					
					// remove agent from campaign busy agent queue.
					/*ConcurrentLinkedQueue<AgentCall> busyAgentQueue = campaignBusyAgentQueue.get(agentCall.getCampaignId());
					if (busyAgentQueue != null) {
						busyAgentQueue.remove(agentCall);
						if (busyAgentQueue.isEmpty()) {
							campaignBusyAgentQueue.remove(agentCall.getCampaignId());
						}
					}*/
				}
			}
			if(!manualAgentQueue.contains(agentCall) && agentCall.getAgentType().equals(agentSkill.MANUAL)){
				manualAgentQueue.offer(agentCall);
				logger.info("registerAgent() : Agent registerd as an available Manaul Agent. AgentId: [{}]", agentCall.getAgentId());
			}
			
			/*if (campaignBusyAgentQueue.get(agentCall.getCampaignId()) != null) {
				logger.info("<----- campaignBusyAgentQueue - busyAgentQueue size [{}], removed agentCall [{}] ----->", campaignBusyAgentQueue.get(agentCall.getCampaignId()).size(), agentCall.toString());
			} else {
				logger.info("<----- campaignBusyAgentQueue - busyAgentQueue size [{}], removed agentCall [{}] ----->", 0, agentCall.toString());
			}*/
		}
	}
	
	@Override
	public void unregisterAgent(AgentCall agentCall) {
		if (agentCall.getCampaignId() != null) {
			ConcurrentLinkedQueue<AgentCall> agentQueue = getAgentQueue(agentCall.getCampaignId());
			if (agentQueue != null) {
				agentQueue.remove(agentCall);
				// commented to keep empty queue in map
				// if (agentQueue.isEmpty()) {
				// 	campaignAgentQueue.remove(agentCall.getCampaignId());
				// }
			}
			ConcurrentLinkedQueue<AgentCall> manualAgentQueue = getManualAgentQueue(agentCall.getCampaignId());
			if (manualAgentQueue != null) {
				manualAgentQueue.remove(agentCall);
				if (manualAgentQueue.isEmpty()) {
					campaignAgentManualQueue.remove(agentCall.getCampaignId());
				}
			}
		}
	}

	/**
	 * Allocates Agent to a Call to which a prospect is connected
	 */
	@Override
	public AgentCall allocateAgent(String prospectCallSid) {
		ProspectCall prospectCall = getProspectByCallSid(prospectCallSid);
		if (prospectCall == null) {
			logger.info("allocateAgent() : ProspectCall not found for CallSid [{}]", prospectCallSid);
			return null;
		}
		AgentCall agentCall = getAvailableAgent(prospectCall.getCampaignId());
		if (agentCall != null) {
			prospectCall.setCallStartTime(new Date());//current time
			prospectCall.setAgentId(agentCall.getAgentId()); //set the allocated agent id
			agentCall.setProspectCall(prospectCall);
			setAgentCall(agentCall.getAgentId(), agentCall);
			logger.info("AgentRegistrationServiceImpl====>Campaign : " + prospectCall.getCampaignId() + " AgentId : " + agentCall.getAgentId() + " ProspectCallId : " + prospectCallSid + " Time : " + DateTime.now());
		} else {
			logger.info("AgentRegistrationServiceImpl====>AllocateAgent() : No agent available for Campaign [{}], campaignAgentQueue [{}]", prospectCall.getCampaignId(), campaignAgentQueue);
		}
		return agentCall;
	}

	@Override
	public void unallocateAgent(AgentCall agentCall) {
		if (agentCall == null) {
			logger.info("AgentRegistrationServiceImpl====>UnallocateLead() : AgentCall is null. ");		
			return;	
		}
		logger.info("AgentRegistrationServiceImpl====>UnallocateLead() : Unallocated Agent: " + agentCall.getAgentId() + ". Agent now available to receive next call. ");
		agentCall.setProspectCall(null);
		setAgentCall(agentCall.getAgentId(), agentCall); //update agentcall map with the new agentcall object
		registerAgent(agentCall); //put agent back into Queue
	}

	@Override
	public void setProspectByCallSid(String callSid, ProspectCall prospectCall) {
		prospectCall.setTwilioCallSid(callSid);
		callsidProspectMap.put(callSid, prospectCall);
	}
	
	@Override
	public ProspectCall getProspectByCallSid(String callSid) {
		return callsidProspectMap.get(callSid);
	}
	
	@Override
	public void unsetProspectByCallSid(String callSid) {
		callsidProspectMap.remove(callSid);
	}

	@Override
	public AgentCall getAgentCall(String agentId) {
		return agentCallMap.get(agentId);
	}
	
	public void setAgentCall(String agentId, AgentCall agentCall) {
		agentCallMap.put(agentCall.getAgentId(), agentCall);
	}
	
	private void unsetAgentCall(String agentId) {
		agentCallMap.remove(agentId);
	}

	/**
	 * @return 
	 * @return the agentCallMap
	 */
	@Override
	public  Collection<AgentCall> getAgentCallList() {
		return Collections.unmodifiableCollection(agentCallMap.values()); //TODO : Make the AgentCall object also unmodifiable
	}
	
	@Override
	public HashMap<String, Integer> getCampaignActiveAgentCount() {
		HashMap<String, Integer> campaignActiveAgentCountMap = new HashMap<String, Integer>();
		for (String campaignId : campaignAgentQueue.keySet()) {
			int activeAgentsCount = getAgentQueue(campaignId).size();
			if (activeAgentsCount > 0) {
				campaignActiveAgentCountMap.put(campaignId, activeAgentsCount);
			}
		}
		
		/*for (String campaignId : campaignAgentManualQueue.keySet()) {
			int activeManualAgentsCount = getManualAgentQueue(campaignId).size();
			if (activeManualAgentsCount > 0) {
				int activeAgentsCount = campaignActiveAgentCountMap.get(campaignId);
				activeAgentsCount = activeAgentsCount+activeManualAgentsCount;
				campaignActiveAgentCountMap.put(campaignId, activeAgentsCount);
			}
		}*/
		return campaignActiveAgentCountMap;
	}
	
	@Override
	public HashMap<String, Integer> getCampaignBusyAgentCount() {
		HashMap<String, Integer> campaignBusyAgentCountMap = new HashMap<String, Integer>();
		for (String campaignId : campaignBusyAgentQueue.keySet()) {
			int activeAgentsCount = getAgentQueue(campaignId).size();
			if (activeAgentsCount > 0) {
				campaignBusyAgentCountMap.put(campaignId, activeAgentsCount);
			}
		}
		return campaignBusyAgentCountMap;
	}

	@Override
	public AgentCall allocateAgentFromUI(String prospectCallSid, String agentId, String dialerMode, String agentType) {
		ProspectCall prospectCall = getProspectByCallSid(prospectCallSid);
		if (prospectCall == null) {
			logger.debug("allocateAgent() : ProspectCall not found for CallSid [{}]", prospectCallSid);
			return null;
		}
		AgentCall agentCall = null;
		ConcurrentLinkedQueue<AgentCall> agentQueue = null;
		if (dialerMode.equalsIgnoreCase(DialerMode.HYBRID.name())
				&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name())) {
			agentQueue = getManualAgentQueue(prospectCall.getCampaignId());
		} else {
			agentQueue = getAgentQueue(prospectCall.getCampaignId());
		}
		if (agentQueue != null) {
			Iterator<AgentCall> iterator = agentQueue.iterator();
			while (iterator.hasNext()) {
				AgentCall tempAgentCall = iterator.next();
				if (tempAgentCall != null && tempAgentCall.getAgentId().equalsIgnoreCase(agentId)) {
					agentCall = tempAgentCall;
					iterator.remove();
					break;
				}
			}
		}
		if (agentCall != null) {
			prospectCall.setCallStartTime(new Date());// current time
			prospectCall.setAgentId(agentCall.getAgentId()); // set the allocated agent id
			agentCall.setProspectCall(prospectCall);
			setAgentCall(agentCall.getAgentId(), agentCall);
		} else {
			logger.debug("allocateAgent() : No agent available for Campaign [{}], campaignAgentQueue [{}]",
					prospectCall.getCampaignId(), campaignAgentQueue);
		}
		return agentCall;
	}

	public void removePreviousProspect() {
		String loggedInUser = XtaasUserUtils.getCurrentLoggedInUsername();
		if (loggedInUser != null && !loggedInUser.isEmpty()) {
			unsetAgentCall(loggedInUser);
		}
	}

}
