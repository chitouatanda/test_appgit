package com.xtaas.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ProspectCallSpeedLog;
import com.xtaas.db.repository.ProspectCallSpeedRepository;


@Service
public class ProspectCallSpeedServiceImpl  implements ProspectCallSpeedService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProspectCallSpeedServiceImpl.class);
	
	@Autowired
	private ProspectCallSpeedRepository prospectCallSpeedRepository;
	
	@Override
	public ProspectCallSpeedLog save(ProspectCallSpeedLog prospectCallSpeedLog) {
		ProspectCallSpeedLog savedProspectCallSpeedLog = new ProspectCallSpeedLog();
		try {
			savedProspectCallSpeedLog = prospectCallSpeedRepository.save(prospectCallSpeedLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return savedProspectCallSpeedLog;
	}

}
