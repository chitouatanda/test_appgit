package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.Login;


public interface AbmListService {

	public void downloadABMListTemplate(HttpServletRequest request, HttpServletResponse response, String campaignId)
			throws FileNotFoundException, IOException;

	public String createCampaignABMList(String campaignId,  MultipartFile file)
			throws IOException;
	
	public String getABMListCountOfCampaign(String campaignId);
	
	public List<AbmListNew> searchABMListByCompanyName(String campaignId,String companyName);
	
	public boolean removeABMListByCampaignId(String campaignId);
	
	public String multiDownloadABMlist(String campaignId,String loginId);
	
	public List<AbmListNew> getABMListByCampaignIdByPage(String campaignId,Pageable pageable);

	public String uploadCampaignABMFile(String campaignId, MultipartFile file, Login login) throws IOException;

	public String multiDownloadABMlistCheckAndSave(String campaignId, Login login) throws FileNotFoundException, IOException;
	
	public List<AbmListNew> getABMListByPagination(String campaignId,int pageSize);

}
