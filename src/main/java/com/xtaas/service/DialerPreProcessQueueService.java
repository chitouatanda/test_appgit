/**
 * 
 */
package com.xtaas.service;

import java.util.List;

import com.xtaas.db.entity.DialerPreProcessEntity;

/**
 * @author djain
 *
 */
public interface DialerPreProcessQueueService {
	
	public DialerPreProcessEntity add(DialerPreProcessEntity entity);
	
	public void addAll(List<DialerPreProcessEntity> entityList);
	
	public DialerPreProcessEntity getNext(String campaignId);
	
	public DialerPreProcessEntity getNext(String campaignId, String sortFieldPath);
	
	public void delete(String id);

}
