package com.xtaas.service;

import com.xtaas.db.entity.RealTimeDeliveryIndustryMapping;

public interface RealTimeDeliveryIndustryMappingService {

	public RealTimeDeliveryIndustryMapping getRealTimeDeliveryIndustryMapping(String key);

}
