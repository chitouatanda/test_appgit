package com.xtaas.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.stream.JsonParser;
import javax.net.ssl.HttpsURLConnection;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.domain.valueobject.*;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.web.dto.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.javafaker.Faker;
import com.google.common.util.concurrent.RateLimiter;
import com.monitorjbl.xlsx.StreamingReader;
import com.plivo.api.models.recording.Recording;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.AbmList;
import com.xtaas.db.entity.AbmListDetail;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.CRMMapping;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.AnalyzeProspectCache;
import com.xtaas.db.entity.CompanyMaster;
import com.xtaas.db.entity.EmailDistinctHash;
import com.xtaas.db.entity.EmailHashed;
import com.xtaas.db.entity.EncryptedProspectCallLog;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PCIAnalysisTemp;
import com.xtaas.db.entity.Partner;
import com.xtaas.db.entity.PhoneDirectAndIndirect;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallInteractionTemp;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectMovement;
import com.xtaas.db.entity.ProspectSalutary;
import com.xtaas.db.entity.ProspectSalutaryInvalid;
import com.xtaas.db.entity.ProspectSalutaryNew;
import com.xtaas.db.entity.ProspectStatusCorrection;
import com.xtaas.db.entity.QAProspectReport;
import com.xtaas.db.entity.Salatuary;
import com.xtaas.db.entity.SalutaryCompany;
import com.xtaas.db.entity.SalutoryEverstring;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.SubSetProspectCallLog;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.AbmListDetailRepository;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.AbmListRepository;
import com.xtaas.db.repository.CRMMappingRepository;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.db.repository.AnalyzeProspectCacheRepository;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.CompanyMasterRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.infra.zoominfo.service.PhoneTypeList;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.infra.zoominfo.service.ZoomOAuth;
import com.xtaas.job.TelnyxMissingRecordingsJob;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;

@Service
public class ApiTestingServiceImpl implements ApiTestingService, TrafficManagerService {

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;

	@Autowired
	private CallLogRepository callLogRepository;

	@Autowired
	private ProspectStatusCorrectionRepository prospectStatusCorrectionRepository;

	@Autowired
	private EmailDistinctHashRepository emailDistinctHashRepository;

	@Autowired
	private EmailHashedRepository emailHashedRepository;

	@Autowired
	private SendGridServiceImpl sendGridServiceImpl;

	@Autowired
	private CRMMappingRepository crmMappingRepository;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private ProspectCallInteractionTempRepository prospectCallInteractionTempRepository;

	@Autowired
	private PhoneDirectAndIndirectRepository phoneDirectAndIndirectRepository;

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;

	@Autowired
	private AbmListDetailRepository abmListDetailRepository;

	@Autowired
	private EntityMappingRepository entityMappingRepository;

	@Autowired
	private CampaignContactRepository campaignContactRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private AbmListRepository abmListRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PCIAnalysisRepositoryTemp pciAnalysisRepository;

	@Autowired
	private SuccessCallLogRepository successCallLogRepository;

	@Autowired
	private SuppressionListRepository suppressionListRepository;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private AbmListNewRepository abmListNewRepository;

	@Autowired
	private SalatuaryRepository salatuaryRepository;

	@Autowired
	private ProspectSalutaryRepository prospectSalutaryRepository;

	@Autowired
	private SalutaryInvalidRepository salutaryInvalidRepository;

	@Autowired
	private ProspectMovementRepository prospectMovementRepository;

	@Autowired
	private ZoomOAuth zoomOAuth;

	@Autowired
	private SubProspectCallLogRepository subProspectCallLogRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private TelnyxMissingRecordingsJob telnyxMissingRecordingsJob;

	@Autowired
	private EncryptedProspectCallLogRepository encryptedProspectCallLogRepository;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private SalutaryCompanyRepository salutaryCompanyRepository;

	@Autowired
	HttpService httpService;

	@Autowired
	private SalutoryEverstringRepository salutoryEverstringRepository;

	@Autowired
	private CompanyMasterRepository companyMasterRepository;

	@Autowired
	private ProspectSalutaryNewRepository prospectSalutaryNewRepository;

	@Autowired
	private ProspectSalutaryQ3Repository prospectSalutaryRepositoryQ3;

	@Autowired
	PriorityQueueCacheOperationService priorityQueueCacheOperationService;

	@Autowired
	AnalyzeProspectCacheRepository analyzeProspectCacheRepository;

	@Autowired
	QAProspectReportRepository qaProspectReportRepository;

	@Autowired
	private InsideviewService insideviewService;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private PlivoOutboundNumberRepository plivoOutboundNumberRepository;

	@Autowired
	private ProspectSalutaryRepositoryTemp prospectSalutaryRepositoryTemp;


	@Autowired
	PropertyService propertyService;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	EnrichSourcePriorityRepository enrichSourcePriorityRepository;

	@Autowired
	private LeadIQSearchServiceImpl leadIQSearchServiceImpl;

	@Autowired
	private InsideViewServiceImpl insideViewServiceImpl;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	private Map<String, CompanyMaster> companyMasterList = new HashMap<String, CompanyMaster>();
	static Properties properties = new Properties();
	private Sheet sheet;
	private Workbook book;
	private RateLimiter rateLimiter = RateLimiter.create(0.33);
	private static final Logger logger = LoggerFactory.getLogger(ApiTestingServiceImpl.class);
	/*
	 * private String campaignId = "5e187525c720b73c9700dbe2"; static Properties
	 * properties = new Properties();
	 */
	private String OAuth = "";
	private String partnerKey = "Xtaascorp.partner";
	private int rowCount = 0;
	private int rowCountFailedRecord = 0;
	private Map<String, Long> recordsBoughtPerOrg = new HashMap<String, Long>();
	ArrayList<String> personIdList = new ArrayList<String>();
	private boolean personIdFlag = false;
	private static final String NON_CALLABLE = "_NON_CALLABLE";
	private int dbrowCount = 0;
	Date cachedDate;
	String tokenKey = "";
	private String EASTERN = "Virginia,Vermont,West Virginia,Kentucky,Massachusetts,Maryland,Maine,Michigan,North Carolina,New Hampshire,New Jersey,New York,Ohio,Pennsylvania,Rhode Island,South Carolina,Connecticut,District of Columbia,Delaware,Florida,Georgia,Iowa";
	private String CENTRAL = "Wisconsin,Illinois,Indiana,Kansas,Louisiana,Minnesota,Missouri,Mississippi,North Dakota,Nebraska,Oklahoma,South Dakota,Tennessee,Texas,Alabama,Arkansas";
	private String MOUNTAIN = "Utah,Wyoming,Montana,New Mexico,Colorado,Alaska,Hawaii";
	private String PDT = "Idaho,Washington,Nevada,Oregon,Arizona,California";
	private String toEmail = "data@xtaascorp.com";

	private final static String signalWireBaseUrl = "https://xtaascorp.signalwire.com/api/laml/2010-04-01/Accounts/c49a511a-2d7a-4816-b139-302ae7d7c74d/";

	@Override
	public String downloadData(OneOffDTO oneOffDTO) {
		String error = "Done";
		List<String> campaigns = oneOffDTO.getCampaignIdList();
		// List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		for (String c : campaigns) {
			List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findAllRecordByCampaignId(c);
			for (ProspectCallLog p : prospectCallLogList) {
				QAProspectReport qapr = new QAProspectReport();
				qapr.setCallbackDate(p.getCallbackDate());
				qapr.setCompanyAddress(p.getCompanyAddress());
				// qapr.setCreatedDate(createdDate);
				qapr.setDataSlice(p.getDataSlice());
				qapr.setProspectCall(p.getProspectCall());
				// qapr.setProspectingData(p.get);
				qapr.setQaFeedback(p.getQaFeedback());
				// qapr.setResearchData(p.getre);
				qapr.setStatus(p.getStatus().toString());
				qapr.setUpdatedBy(p.getUpdatedBy());
				qapr.setCreatedBy(p.getCreatedBy());

				qaProspectReportRepository.save(qapr);
			}
		}
		// multiCampaignLeadReportServiceImpl.downloadData(oneOffDTO);
		return error;

	}

	@Override
	public String getCallLogRecordsForCampaign(OneOffDTO oneOffDto) {
		String error = null;
		List<ProspectCallLog> toUpdate = new ArrayList<ProspectCallLog>();
		Map<String, List<ProspectCallLog>> toUpdateMap = new HashMap<String, List<ProspectCallLog>>();
		// existing campaign id from which you want to get the answer machine status.
		// String srcCampaignId = "58b895c1e4b0eeaa7f1fa0dc";
		// List<ProspectCallLog> prospectCallLogList = null;

		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository
				.findContactsByQueued(oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList786 = prospectCallLogRepository.findContactsByRetryCount(786,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList886 = prospectCallLogRepository.findContactsByRetryCount(886,
				oneOffDto.getPrimaryCampaignId());
		List<ProspectCallLog> prospectCallLogList444444 = prospectCallLogRepository.findContactsByRetryCount(444444,
				oneOffDto.getPrimaryCampaignId());

		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		prospectCallLogList.addAll(prospectCallLogList0);
		prospectCallLogList.addAll(prospectCallLogList1);
		prospectCallLogList.addAll(prospectCallLogList2);
		prospectCallLogList.addAll(prospectCallLogList3);
		prospectCallLogList.addAll(prospectCallLogList4);
		prospectCallLogList.addAll(prospectCallLogList5);
		prospectCallLogList.addAll(prospectCallLogList6);
		prospectCallLogList.addAll(prospectCallLogList7);
		prospectCallLogList.addAll(prospectCallLogList8);
		prospectCallLogList.addAll(prospectCallLogList9);
		prospectCallLogList.addAll(prospectCallLogList10);
		prospectCallLogList.addAll(prospectCallLogList786);
		prospectCallLogList.addAll(prospectCallLogList886);
		prospectCallLogList.addAll(prospectCallLogList444444);

		FileWriter fileWriter = null;
		FileWriter fileWriter1 = null;

		try {
			String fileName = "prospectCallInteraction_Adverity 28-2.txt";
			fileWriter = new FileWriter(fileName);
			fileWriter1 = new FileWriter("COUNT_" + fileName);
			boolean alreadyExists = false;
			// CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
			int counter = 1;
			for (ProspectCallLog prospectCallLog : prospectCallLogList) {
				boolean b = false;
				String fname = prospectCallLog.getProspectCall().getProspect().getFirstName();
				String lname = prospectCallLog.getProspectCall().getProspect().getLastName();
				String company = prospectCallLog.getProspectCall().getProspect().getCompany();
				String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
				// String subStatus = "ANSWERMACHINE";
				if (fname != null && lname != null && company != null) {
					// Click below to change in query criteria
					List<ProspectCallInteraction> prospectCallInteractionList = prospectCallInteractionRepository
							.prospectCallInteractionListStatus(fname, lname, company, phone,
									oneOffDto.getCampaignIdList());
					for (ProspectCallInteraction prospect : prospectCallInteractionList) {
						if (prospect.getProspectCall().getProspect().getPhone() == null)
							logger.debug("NULL PHONE-------------> " + prospect.getProspectCall().getProspectCallId());
						logger.debug(prospect.getProspectCall().getProspectCallId());
						String prospectCallId = prospectCallLog.getProspectCall().getProspectCallId();
						Prospect prospect2 = prospectCallLog.getProspectCall().getProspect();
						// String campaignId = this.campaignId;
						if (prospect.getProspectCall().getSubStatus() != null) {
							ProspectCall p2 = new ProspectCall(prospectCallId, oneOffDto.getPrimaryCampaignId(),
									prospect2);
							p2.setSubStatus(prospect.getProspectCall().getSubStatus());
							prospectCallLog.setProspectCall(p2);
							List<ProspectCallLog> ls = toUpdateMap
									.get(prospect.getProspectCall().getProspect().getPhone() + "|"
											+ prospect.getProspectCall().getProspect().getEmail() + "|"
											+ prospect.getProspectCall().getProspect().getSourceId());
							if (ls == null) {
								ls = new ArrayList<ProspectCallLog>();
							}
							ls.add(prospectCallLog);
							toUpdateMap.put(prospect.getProspectCall().getProspect().getPhone() + "|"
									+ prospect.getProspectCall().getProspect().getEmail() + "|"
									+ prospect.getProspectCall().getProspect().getSourceId(), ls);
						} else {
							continue;
						}

					}
					counter++;
				}

			}

			// use FileWriter constructor that specifies open for appending

			List<String> deleteInteractionEmailRecords = new ArrayList<String>();
			List<String> deleteInteractionPhoneRecords = new ArrayList<String>();

			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists) {
				fileWriter.append("counter");
				fileWriter.append("|");
				fileWriter.append("phone");
				fileWriter.append("|");
				fileWriter.append("status1");
				fileWriter.append("|");
				fileWriter.append("status2");
				fileWriter.append("|");
				fileWriter.append("status3");
				fileWriter.append("|");
				fileWriter.append("status4");
				fileWriter.append("|");
				fileWriter.append("status5");
				fileWriter.append("|");
				fileWriter.append("status6");

				fileWriter1.append("counter");
				fileWriter1.append("|");
				fileWriter1.append("Total Records");
				/*
				 * fileWriter.append("|"); fileWriter.append("phone"); fileWriter.append("|");
				 * fileWriter.append("title"); fileWriter.append("|");
				 * fileWriter.append("sourceid"); fileWriter.append("|");
				 * fileWriter.append("updateddate"); fileWriter.append("|");
				 * fileWriter.append("status");
				 */
				fileWriter.append("\n");
				alreadyExists = true;
				// csvOutput.endRecord();
			}
			List<String> toRemovePhone = new ArrayList<String>();
			Map<String, Integer> toRemovePhoneMap = new HashMap<String, Integer>();
			// else assume that the file already has the correct header line
			for (Map.Entry<String, List<ProspectCallLog>> entry : toUpdateMap.entrySet()) {
				String phone = entry.getKey();
				String toRemovePhonestr = "";
				String status1 = "";
				String status2 = "";
				String status3 = "";
				String status4 = "";
				String status5 = "";
				String status6 = "";
				List<ProspectCallLog> pclList = entry.getValue();
				int counterpcl = 0;
				int counterout = 0;
				for (ProspectCallLog pcl : pclList) {
					if (counterpcl <= 4) {
						String subStatus = pcl.getProspectCall().getSubStatus();
						if (subStatus != null && (subStatus.equalsIgnoreCase("NOTINTERESTED")
								|| subStatus.equalsIgnoreCase("NO_CONSENT") || subStatus.equalsIgnoreCase("DNCL")
								|| subStatus.equalsIgnoreCase("DNRM") || subStatus.equalsIgnoreCase("BUSY")
								|| subStatus.equalsIgnoreCase("DEADAIR")
								|| subStatus.equalsIgnoreCase("PROSPECT_UNREACHABLE")
								|| subStatus.equalsIgnoreCase("NOANSWER")
								|| subStatus.equalsIgnoreCase("DIALERMISDETECT")
								|| subStatus.equalsIgnoreCase("LOCAL_DNC_ERROR")
								|| subStatus.equalsIgnoreCase("ANSWERMACHINE"))) {
							if (counterpcl == 0) {
								status1 = subStatus;
							}
							if (counterpcl == 1) {
								status2 = subStatus;
							}
							if (counterpcl == 2) {
								status3 = subStatus;
							}
							if (counterpcl == 3) {
								status4 = subStatus;
							}
							if (counterpcl == 4) {
								status5 = subStatus;
							}
							if (counterpcl == 5) {
								status6 = subStatus;
							}
							if (counterpcl == 4) {
								toRemovePhone.add(phone);
								toRemovePhonestr = phone;
							}
							counterpcl++;
						}
					} else {
						counterout++;
					}

				}
				if (toRemovePhonestr != null && !"".equalsIgnoreCase(toRemovePhonestr)) {
					int deleteCounter = counterpcl + counterout;
					toRemovePhoneMap.put(toRemovePhonestr, counterpcl + counterout);
					fileWriter.append(Integer.toString(counterpcl + counterout));
					fileWriter.append("|");
					fileWriter.append(toRemovePhonestr);
					fileWriter.append("|");
					fileWriter.append(status1);
					fileWriter.append("|");
					fileWriter.append(status2);
					fileWriter.append("|");
					fileWriter.append(status3);
					fileWriter.append("|");
					fileWriter.append(status4);
					fileWriter.append("|");
					fileWriter.append(status5);
					fileWriter.append("|");
					fileWriter.append(status6);
					fileWriter.append("\n");
					if (deleteCounter > 8) {
						if (pclList.size() > 0) {
							ProspectCallLog pclog = pclList.get(0);
							// pclog.getProspectCall().setca
							/*
							 * update.set("prospectCall.callRetryCount", 757502); update.set("status",
							 * "MAX_RETRY_LIMIT_REACHED"); update.set("updatedDate", new Date());
							 */
							if (pclog.getProspectCall().getProspect().getEmail() != null
									&& !pclog.getProspectCall().getProspect().getEmail().isEmpty()) {
								deleteInteractionEmailRecords.add(pclog.getProspectCall().getProspect().getEmail());
							} else {
								deleteInteractionPhoneRecords.add(pclog.getProspectCall().getProspect().getPhone());

							}
						}
					}
				}
			}
			// write out a few records
			Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
			for (Map.Entry<String, Integer> entry : toRemovePhoneMap.entrySet()) {
				Integer phCount = countMap.get(entry.getValue());
				if (phCount == null) {
					phCount = 0;
				}
				countMap.put(entry.getValue(), phCount + 1);
			}

			for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
				fileWriter1.append(Integer.toString(entry.getKey()));
				fileWriter1.append("|");
				fileWriter1.append(Integer.toString(entry.getValue()));
				fileWriter1.append("\n");
			}

			fileWriter.append("\n");
			logger.debug("Deleting records by email " + deleteInteractionEmailRecords.size());
			logger.debug("Deleting records by phone " + deleteInteractionPhoneRecords.size());

			int page = 20;
			List<String> dPids = new ArrayList<String>();
			List<String> dispStats = new ArrayList<String>();
			dispStats.add("SUCCESS");

			if (deleteInteractionEmailRecords.size() > 0) {
				String[] demails = new String[deleteInteractionEmailRecords.size()];
				demails = deleteInteractionEmailRecords.toArray(demails);
				int chunk = 20; // chunk size to divide
				for (int i = 0; i < demails.length; i += chunk) {
					logger.debug(Arrays.toString(Arrays.copyOfRange(demails, i, Math.min(demails.length, i + chunk))));
					// deleteEmailProspets(Arrays.copyOfRange(demails, i,
					// Math.min(demails.length,i+chunk)), deleteInteractionFromCampaigns(),
					// dispStats );
				}
				// prospectCallLogRepository.findByInteractionsByEmail(deleteInteractionFromCampaigns(),
				// dispStats, emails);
				// }
			}
			if (deleteInteractionPhoneRecords.size() > 0) {
				String[] dphones = new String[deleteInteractionPhoneRecords.size()];
				dphones = deleteInteractionPhoneRecords.toArray(dphones);
				int chunk = 20; // chunk size to divide
				for (int i = 0; i < dphones.length; i += chunk) {
					logger.debug(Arrays.toString(Arrays.copyOfRange(dphones, i, Math.min(dphones.length, i + chunk))));
					// deletePhoneProspets(Arrays.copyOfRange(dphones, i,
					// Math.min(dphones.length,i+chunk)), deleteInteractionFromCampaigns(),
					// dispStats );
				}
				// prospectCallLogRepository.findByInteractionsByEmail(deleteInteractionFromCampaigns(),
				// dispStats, emails);
				// }
			}
			List<String> deleteGroupCampaigns = new ArrayList<String>();
			if (oneOffDto.getDeleteGroupCampaignIds() != null && oneOffDto.getDeleteGroupCampaignIds().size() > 0) {
				deleteGroupCampaigns = oneOffDto.getDeleteGroupCampaignIds();
			}
			prospectCallLogRepositoryImpl.updateInteractionData(deleteGroupCampaigns, deleteInteractionEmailRecords,
					null);
			logger.debug("Deleted records by email " + deleteInteractionEmailRecords.size());
			// prospectCallLogRepository.findByInteractionsByPhone(deleteInteractionFromCampaigns(),
			// dispStats, emails);

			prospectCallLogRepositoryImpl.updateInteractionData(deleteGroupCampaigns, null,
					deleteInteractionPhoneRecords);
			logger.debug("Deleted records by phone " + deleteInteractionPhoneRecords.size());

			// csvOutput.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		} finally {

			try {

				fileWriter.flush();

				fileWriter.close();
				fileWriter1.flush();

				fileWriter1.close();

			} catch (IOException e) {

				logger.debug("Error while flushing/closing fileWriter !!!");

				e.printStackTrace();

			}

		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Interactions Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Interactions Report Status", error);
		}

		// prospectCallLogRepository.save(toUpdate);
		logger.debug("total uppp=  =  " + toUpdate.size());
		return error;
	}

	@Override
	public String tagdirectindirect(OneOffDTO oneOffDTO) {
		Map<String, List<ProspectCallLog>> pcMap = new HashMap<String, List<ProspectCallLog>>();
		String error = null;
		List<String> campaigns = new ArrayList<String>();

		campaigns = oneOffDTO.getCampaignIdList();

		try {
			/*
			 * String ph = "650-253-0000"; ph=ph.replaceAll("[^A-Za-z0-9]", "");
			 */
			Map<String, String> pdiMap = new HashMap<String, String>();

			List<PhoneDirectAndIndirect> pdiList = phoneDirectAndIndirectRepository.findAll();
			for (PhoneDirectAndIndirect pd : pdiList) {
				// pdiMap.put(pd.getNumber(), pd.getLine_type());
				pdiMap.put(pd.getLocal_format(), pd.getLine_type());
			}
			List<ProspectCallLog> pcLogs = prospectCallLogRepository.findByDirectNumbers(campaigns);
			for (ProspectCallLog pc : pcLogs) {
				List<ProspectCallLog> pclList = pcMap.get(pc.getProspectCall().getProspect().getPhone());
				if (pclList == null) {
					pclList = new ArrayList<ProspectCallLog>();
					if (pc.getProspectCall().getProspect().getPhoneType() != null) {

					} else {
						String ph = pc.getProspectCall().getProspect().getPhone();
						ph = ph.replaceAll("[^A-Za-z0-9]", "");
						PhoneTypeList ptl = null;
						if (pdiMap.get(ph) != null) {
							// Dont Do anything
							pc.getProspectCall().getProspect().setPhoneType(pdiMap.get(ph));

						} else {
							ptl = sendGet(ph);
						}
						if (ptl != null && ptl.getLine_type() != null) {
							pc.getProspectCall().getProspect().setPhoneType(ptl.getLine_type());
							PhoneDirectAndIndirect pdi = new PhoneDirectAndIndirect();
							pdi.setCarrier(ptl.getCarrier());
							pdi.setCountry_code(ptl.getCountry_code());
							pdi.setCountry_name(ptl.getCountry_name());
							pdi.setCountry_prefix(ptl.getCountry_prefix());
							pdi.setInternationl_format(ptl.getInternationl_format());
							pdi.setLine_type(ptl.getLine_type());
							pdi.setLocal_format(ptl.getLocal_format());
							pdi.setLocation(ptl.getLocation());
							pdi.setNumber(ptl.getNumber());
							pdi.setValid(ptl.isValid());
							phoneDirectAndIndirectRepository.save(pdi);
						}
					}
				} else {
					ProspectCallLog pcTemp = pclList.get(0);
					pc.getProspectCall().getProspect()
							.setPhoneType(pcTemp.getProspectCall().getProspect().getPhoneType());
				}
				pclList.add(pc);

				pcMap.put(pc.getProspectCall().getProspect().getPhone(), pclList);
			}

			for (Map.Entry<String, List<ProspectCallLog>> entry : pcMap.entrySet()) {
				List<ProspectCallLog> pcList = entry.getValue();
				prospectCallLogRepository.saveAll(pcList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Direct Indirect Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Direct Indirect Report Status", error);
		}
		return error;
	}

	private PhoneTypeList sendGet(String phone) throws Exception {

		String url = "http://apilayer.net/api/validate?access_key=eb7239e08e9805ad56c5668f503be3b6&number=" + phone
				+ "&country_code=US&format=1";

		HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();

		// optional default is GET
		httpClient.setRequestMethod("GET");

		// add request header
		httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = httpClient.getResponseCode();
		logger.debug("\nSending 'GET' request to URL : " + url);
		logger.debug("Response Code : " + responseCode);

		try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {

			StringBuilder response = new StringBuilder();
			String line;

			while ((line = in.readLine()) != null) {
				response.append(line);
			}

			// print result
			logger.debug(response.toString());

			PhoneTypeList zoomInfoResponse = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				zoomInfoResponse = mapper.readValue(response.toString(), PhoneTypeList.class);
				// return zoomInfoResponse;
			}
			return zoomInfoResponse;

		}
		// return null;
	}

	private List<String> verifyInGroupCampaigns() {
		List<String> groupCampaigns = new ArrayList<String>();
		// Adverity - 5e303be1e4b083d4923a63b4
		groupCampaigns.add("5dd2f4f4e4b0472552bd39de");
		groupCampaigns.add("5e303be1e4b083d4923a63b4");
		groupCampaigns.add("5dd2e7eae4b0472552bd38c9");
		groupCampaigns.add("5dd2f290e4b0472552bd39c3");
		groupCampaigns.add("5dd2e9e5e4b0472552bd38e3");
		groupCampaigns.add("5d5bf9b1e4b00a016d7b28df");
		groupCampaigns.add("5dc083c1e4b09485f009dc06");

		// groupCampaigns.add("5ca4a9a4e4b04d84f28b3d71");// Contact center
		/*
		 * groupCampaigns.add("5b755900e4b066e7dba3080b");// Voip Man-Dir
		 * groupCampaigns.add("5d9c71e4e4b0755cbbfb5301");// Voip MQL New
		 * groupCampaigns.add("5e42c365e4b0d2a8ae0454bb");// Voip Conversion
		 */

		// groupCampaigns.add("5e38a973e4b050d58f8d42c8");//Netsuite Manufacturing
		// groupCampaigns.add("5e38aa33e4b050d58f8d42d4");//Netsuite Retail
		// For Call Analytics
		/*
		 * groupCampaigns.add("5e1582c3e4b04baeb46389f7");// Call Analytics
		 * groupCampaigns.add("5d47e5aae4b0152590e46905");//
		 * groupCampaigns.add("5dd2f9ebe4b0472552bd3a1d");//
		 * groupCampaigns.add("5dc08320e4b09485f009dbd9");//
		 */

		// groupCampaigns.add("5d0b3afce4b0649e638bcc7d");// Sharpspring

		// groupCampaigns.add("5e303ac1e4b0f2bcf71750f5");// ARM Treasure Data
		// groupCampaigns.add("5e303be1e4b083d4923a63b4");// Adverity

		// groupCampaigns.add("5ca4a9a4e4b04d84f28b3d71");// Contact center

		// groupCampaigns.add("5d7a9e25e4b0de39963eb494");//Cloud Comm
		// groupCampaigns.add("5e0d81a5e4b01800263c9fbe");// Cloud Comm 10 to 99

		return groupCampaigns;

	}

	@Override
	public String testQualityBuckeet(OneOffDTO oneOffDTO) {
		Runnable runnable = () -> {
			String error = null;
			StringBuffer sbError = new StringBuffer();
			try {
				List<ProspectCallDTO> prospectCallList = new ArrayList<ProspectCallDTO>(0);
				/*
				 * FileReader reader = new
				 * FileReader("src/main/resources/properties/timezonareacodemapping.properties")
				 * ; properties.load(reader);
				 */

				List<String> campaigns = new ArrayList<String>();

				// DatasliceJava ran on 13-Mar-2019

				// campaigns.add("5e4f1b56e4b0f710a1c72609"); //Digital Commerce
				// campaigns.add("5e5c1c14e4b0bdb7138b89ce"); //CDP Mar-20
				// campaigns.add("5e67be4ee4b08ed83fcb8aae");//Sharpspring WP Mar-20
				// campaigns.add("5e6f7d81e4b04bb1e3799fc4");//Site Improve UK

				// campaigns.add("5e61f859e4b03ebf585c2db8"); //Digital Commerce - ABM
				// campaigns.add("5e676d2be4b08ed83fc90692");//MA MIR
				campaigns = oneOffDTO.getCampaignIdList(); // Dyspatch
				for (String campaignID : campaigns) {
					// moveDataToNonCallable(campaignID);

					//Old implementation
					// List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository
					// 		.findContactsTenByQueued(campaignID);
					// List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(1, campaignID);
					// List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(2, campaignID);
					// List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(3, campaignID);
					// List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(4, campaignID);
					// List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(5, campaignID);
					// List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(6, campaignID);
					// List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(7, campaignID);
					// List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(8, campaignID);
					// List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(9, campaignID);
					// List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository
					// 		.findContactsTenByRetryCount(10, campaignID);
					// List<ProspectCallLog> prospectCallLogList786 = prospectCallLogRepository
					// 		.findContactsByMaxRetryCount(786, campaignID);
					// List<ProspectCallLog> prospectCallLogList886 = prospectCallLogRepository
					// 		.findContactsByMaxRetryCount(886, campaignID);
					// List<ProspectCallLog> prospectCallLogList444444 = prospectCallLogRepository
					// 		.findContactsByMaxRetryCount(444444, campaignID);
					
					//New query to fetch callables
					List<ProspectCallLog> pList = prospectCallLogRepository.findCallableContacts(campaignID);
					
					//Old implementation
					// List<ProspectCallLog> pList = new ArrayList<ProspectCallLog>();// prospectCallLogRepository.findAllRecordByCampaignId(cid);

					// pList.addAll(prospectCallLogList0);
					// pList.addAll(prospectCallLogList1);
					// pList.addAll(prospectCallLogList2);
					// pList.addAll(prospectCallLogList3);
					// pList.addAll(prospectCallLogList4);
					// pList.addAll(prospectCallLogList5);
					// pList.addAll(prospectCallLogList6);
					// pList.addAll(prospectCallLogList7);
					// pList.addAll(prospectCallLogList8);
					// pList.addAll(prospectCallLogList9);
					// pList.addAll(prospectCallLogList10);
					// pList.addAll(prospectCallLogList786);
					// pList.addAll(prospectCallLogList886);
					// pList.addAll(prospectCallLogList444444);
					logger.debug("Started");

					Map<String, List<ProspectCallLog>> pMap = new HashMap<String, List<ProspectCallLog>>();

					for (ProspectCallLog p : pList) {

						List<ProspectCallLog> prospectList = pMap.get(p.getProspectCall().getProspect().getPhone());
						if (prospectList != null && prospectList.size() > 0) {
							prospectList.add(p);
						} else {
							prospectList = new ArrayList<ProspectCallLog>();
							prospectList.add(p);
						}
						pMap.put(p.getProspectCall().getProspect().getPhone(), prospectList);
					}
					List<ProspectCallLog> updateProspectList = new ArrayList<ProspectCallLog>();
					for (Map.Entry<String, List<ProspectCallLog>> entry : pMap.entrySet()) {
						List<ProspectCallLog> prospectList = entry.getValue();
						boolean directFlag = true;
						if (prospectList.size() > 1)
							directFlag = false;
						for (ProspectCallLog p : prospectList) {
							try {
								//Old Implementation
								// if (prospectList.size() == 1) {
								// 	// DO nothing TODO
								// 	List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository
								// 			.findByPhoneNumber(p.getProspectCall().getProspect().getPhone());
								// 	if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
								// 		boolean x = false;
								// 		for (ProspectCallLog plog : prospectCallLogList) {

								// 			if (!plog.getProspectCall().getProspect().getFirstName()
								// 					.equalsIgnoreCase(p.getProspectCall().getProspect().getFirstName())
								// 					&& !plog.getProspectCall().getProspect().getLastName().equalsIgnoreCase(
								// 							p.getProspectCall().getProspect().getLastName())) {
								// 				x = true;
								// 			}
								// 		}
								// 		if (x)
								// 			p.getProspectCall().getProspect().setDirectPhone(false);
								// 		else
								// 			p.getProspectCall().getProspect().setDirectPhone(true);

								// 	}
								// } else {
								// 	p.getProspectCall().getProspect().setDirectPhone(false);
								// }
								p.getProspectCall().getProspect().setDirectPhone(directFlag);
								StringBuffer sb = new StringBuffer();
								/*
								 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
								 */

								String emp = "";
								String mgmt = "";
								String direct = "";
								if (directFlag) {
									direct = "1";
								} else {
									direct = "2";
								}

								String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
								sb.append(p.getStatus());
								sb.append("|");
								if (p.getProspectCall().getProspect()
										.getCustomAttributeValue("minEmployeeCount") instanceof Number) {
									Number n1 = (Number) p.getProspectCall().getProspect()
											.getCustomAttributeValue("minEmployeeCount");
									sb.append(getBoundry(n1, "min"));
									sb.append("-");
									emp = getEmployeeWeight(n1);
								} else {
									String x = "0";
									if (p.getProspectCall().getProspect()
											.getCustomAttributeValue("minEmployeeCount") != null)
										x = p.getProspectCall().getProspect()
												.getCustomAttributeValue("minEmployeeCount").toString();
									Double a = Double.parseDouble(x);
									sb.append(getBoundry(a, "min"));
									sb.append("-");
									emp = getEmployeeWeight(a);
								}

								if (p.getProspectCall().getProspect()
										.getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
									Number n2 = (Number) p.getProspectCall().getProspect()
											.getCustomAttributeValue("maxEmployeeCount");
									sb.append(getBoundry(n2, "max"));
								} else {

									String x = "0";
									if (p.getProspectCall().getProspect()
											.getCustomAttributeValue("maxEmployeeCount") != null)
										x = p.getProspectCall().getProspect()
												.getCustomAttributeValue("maxEmployeeCount").toString();
									if (x == null || x.isEmpty()) {
										x = "0";
									}
									Double a = Double.valueOf(x);
									sb.append(getBoundry(a, "max"));
									/*
									 * int a =
									 * Integer.parseInt(p.getProspectCall().getProspect().getCustomAttributeValue(
									 * "maxEmployeeCount").toString()); sb.append(getBoundry(a,"max"));
									 */
									// sb.append("-");
									// emp = getEmployeeWeight(a);
								}
								/*
								 * sb.append(p.getProspectCall().getProspect().getCustomAttributeValue(
								 * "minEmployeeCount")); sb.append("-");
								 * sb.append(p.getProspectCall().getProspect().getCustomAttributeValue(
								 * "maxEmployeeCount"));
								 */
								sb.append("|");
								sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
								mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());

								int qbSort = 0;
								if (!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty()) {
									String sortString = direct + tz + mgmt + emp;
									Map<String, Integer> dataSourceMap = oneOffDTO.getDataSourceMap();
									if (dataSourceMap != null && dataSourceMap.size() > 0) {
										if (dataSourceMap.get(p.getProspectCall().getProspect().getSource()) != null) {
											int sourceWeight = dataSourceMap
													.get(p.getProspectCall().getProspect().getSource());
											if (sourceWeight > 0) {

											} else {
												int dsSizeMap = dataSourceMap.size();
												sourceWeight = dsSizeMap + 1;
											}
											String qbstr = String.valueOf(sourceWeight) + sortString;
											qbSort = Integer.parseInt(qbstr);
										}
									} else {
										qbSort = Integer.parseInt(sortString);
									}
								}

								if (qbSort == 0) {
									logger.debug("ZERO");
								}
								String slice = "";
								p.getProspectCall().setQualityBucket(sb.toString());
								p.getProspectCall().setQualityBucketSort(qbSort);
								p.getProspectCall().getProspect().setDirectPhone(directFlag);
								Long employee = new Long(emp);
								Long management = new Long(mgmt);
								// if(directFlag /*&& p.getDataSlice()!=null && !p.getDataSlice().isEmpty() &&
								// !p.getDataSlice().equalsIgnoreCase("slice0")*/){
								// slice = "slice1";
								// }else if(directFlag &&
								// (p.getDataSlice()==null || !p.getDataSlice().equalsIgnoreCase("slice0")) /*&&
								// employee<=8 && management<=3*/){
								// slice = "slice1";
								// }else if(!directFlag //&& //TODO this is added to move slice0 indirect
								// numbers to slice10
								// s /* p.getDataSlice()!=null && !p.getDataSlice().isEmpty() &&
								// p.getDataSlice().equalsIgnoreCase("slice0")*/){
								// s slice = "slice10";
								// }
								/*
								 * if(directFlag && employee<=8 && management<=3){ slice = "slice1"; }else
								 * if(directFlag && employee<=10 && management<=3){ slice = "slice2"; }else
								 * if(directFlag && employee>10 && management<=3){ slice = "slice3"; }else
								 * if(directFlag && employee<=8 && management>=4){ slice = "slice4"; }else
								 * if(directFlag && employee<=10 && management>=4){ slice = "slice5"; }else
								 * if(directFlag && employee>10 && f>=4){ slice = "slice6"; }
								 */
								// if(!slice.isEmpty()/* && p.getDataSlice()!=null &&
								// !p.getDataSlice().isEmpty() &&
								// !p.getDataSlice().equalsIgnoreCase("slice0")*/) {
								if (directFlag) {

									if (management >= 4 || employee >= 12) {
										p.setDataSlice("slice2");
										p.getProspectCall().setDataSlice("slice2");
									} else {
										p.setDataSlice("slice1");
										p.getProspectCall().setDataSlice("slice1");
									}
									try {
										//Old Implementation
										// prospectCallLogRepository.save(p);
									} catch (Exception e) {
										// TODO: handle exception
									}
								} else {
									p.setDataSlice("slice10");
									p.getProspectCall().setDataSlice("slice10");
									try {
										//Old Implementation
										// prospectCallLogRepository.save(p);
									} catch (Exception e) {
										// TODO: handle exception
									}
								}
								// }
								p = prospectCallLogRepository
										.findByProspectCallId(p.getProspectCall().getProspectCallId());
								if (!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty()
										|| p.getDataSlice() == null || p.getDataSlice().isEmpty()) {
									logger.debug(p.getProspectCall().getProspectCallId() + "--->" + p.getDataSlice());
									if (p.getDataSlice() == null || !p.getDataSlice().isEmpty()
											|| !p.getDataSlice().equalsIgnoreCase("slice10")) {
										p.setDataSlice("slice10");
										p.getProspectCall().setDataSlice("slice10");
										// p.getProspectCall().setCallRetryCount(696969);
										// p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
										try {
											//Old Implementation
											// prospectCallLogRepository.save(p);
										} catch (Exception e) {
											// TODO: handle exception
										}
									}
								} // p.getProspectCall().getProspect().setTimeZone(timezone);
									// p.getProspectCall().getProspect().setTimeZone(timezone);

								//Old Implementation
								// p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								if (p.getProspectCall().getDataSlice() == null && p.getDataSlice() != null) {
									p.getProspectCall().setDataSlice(p.getDataSlice());
									try {
										//Old Implementation
										// prospectCallLogRepository.save(p);
									} catch (Exception e) {
										// TODO: handle exception
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								sbError.append("Error in CampaignId : " + campaignID + " Error : "
										+ ExceptionUtils.getStackTrace(e) + "\n");
							}
							updateProspectList.add(p);
						}
						//updateProspectList.addAll(prospectList);
					}
					prospectCallLogRepository.bulkUpdate(updateProspectList);

					// moveDataToNonCallable(campaignID);
					logger.debug("Done");
				}
				logger.debug("This Program is completed");
			} catch (Exception e) {
				e.printStackTrace();
				sbError.append(ExceptionUtils.getStackTrace(e));
			}
			if (sbError != null && sbError.toString() != null && !sbError.toString().isEmpty()) {
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Test Quality Buckeet Report Status - Errors",
						sbError.toString());
			} else {
				error = "Success CampaignId : \n";
				StringBuffer sb = new StringBuffer();
				for (String cam : oneOffDTO.getCampaignIdList()) {
					sb.append(cam);
					sb.append("\n");
				}
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Test Quality Buckeet Report Status",
						error + sb.toString());
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		return "Success";
	}

	@Override
	public String setSalutaryTimeZone(OneOffDTO oneOffDTO) {
		try {
			FileReader reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
			properties.load(reader);
			int salutaryCount = prospectSalutaryRepository.countByMissingState("N/A");
			int batchsize = 0;
			if (salutaryCount > 0) {
				batchsize = (int) (salutaryCount / 5000);
				int modSize = salutaryCount % 5000;
				if (modSize > 0) {
					batchsize = batchsize + 1;
				}
			}
			for (int i = 0; i < batchsize; i++) {
				logger.info("salutary State Code sorting: " + i);

				Pageable pageable = PageRequest.of(i, 5000);
				List<ProspectSalutary> salutaryFromDB = prospectSalutaryRepository.findMissingStates("N/A", pageable);
				for (ProspectSalutary p : salutaryFromDB) {
					String st = "";
					String timezzzz = properties
							.getProperty(findAreaCode(p.getProspectCall().getProspect().getPhone()));
					if (timezzzz != null && !timezzzz.isEmpty()) {
						if (timezzzz.equalsIgnoreCase("US/Pacific")) {
							st = "CA";
						} else if (timezzzz.equalsIgnoreCase("US/Eastern")) {
							st = "DE";
						} else if (timezzzz.equalsIgnoreCase("US/Mountain")) {
							st = "CO";
						} else if (timezzzz.equalsIgnoreCase("US/Central")) {
							st = "FL";
						}
					} else {
						logger.info("salutary State Code NOT Found for Number: "
								+ p.getProspectCall().getProspect().getPhone());
					}

					// if(p.getProspectCall().getProspect().getStateCode().equalsIgnoreCase("N/A"))
					// {
					if (!st.isEmpty()) {
						p.getProspectCall().getProspect().setStateCode(st);
						if (timezzzz != null && !timezzzz.isEmpty()) {
							p.getProspectCall().getProspect().setTimeZone(timezzzz);
							prospectSalutaryRepository.save(p);
						}
					}
					// }

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}

	@Override
	public String testQualityBucketForProspectSalutaryTemp(OneOffDTO oneOffDTO) {
		Runnable runnable = () -> {
			String error = null;
			try {
				FileReader reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
				properties.load(reader);

				logger.error("Prospect Salutary thread started.");
				for (String campaignID : oneOffDTO.getCampaignIdList()) {
					Map<String, List<ProspectSalutary>> phoneProspectsMap = new HashMap<String, List<ProspectSalutary>>();
					long salutaryCount = prospectSalutaryRepository.count();
					int batchsize = 1255;
					/*
					 * if (salutaryCount > 0) { batchsize = (int) (salutaryCount / 10000); long
					 * modSize = salutaryCount % 10000; if (modSize > 0) { batchsize = batchsize +
					 * 1; } }
					 */
					if (batchsize > 0) {
						for (int i = 0; i < batchsize; i++) {
							logger.info("salutary bucket sorting batch number : " + i);
							Pageable pageable = PageRequest.of(i, 10000);
							List<ProspectSalutary> salutaryFromDB = prospectSalutaryRepository
									.findSalutaryProspectsPage(pageable);
							/*
							 * if (salutaryFromDB != null && salutaryFromDB.size() > 0) { for
							 * (ProspectSalutary prospectSalutary : salutaryFromDB) { List<ProspectSalutary>
							 * prospectList = phoneProspectsMap
							 * .get(prospectSalutary.getProspectCall().getProspect().getPhone()); if
							 * (prospectList != null && prospectList.size() > 0) {
							 * prospectList.add(prospectSalutary); } else { prospectList = new
							 * ArrayList<ProspectSalutary>(); prospectList.add(prospectSalutary); }
							 * phoneProspectsMap.put(prospectSalutary.getProspectCall().getProspect().
							 * getPhone(), prospectList); } }
							 */
							boolean directFlag = true;
							for (ProspectSalutary p : salutaryFromDB) {
								if (p.getProspectCall().getProspect().getPhone() != null
										&& !p.getProspectCall().getProspect().getPhone().isEmpty()) {
									String st = "";
									String timezzzz = properties
											.getProperty(findAreaCode(p.getProspectCall().getProspect().getPhone()));
									if (timezzzz == null || timezzzz.isEmpty()
											|| timezzzz.equalsIgnoreCase("US/Pacific")) {
										st = "CA";
									} else if (timezzzz.equalsIgnoreCase("US/Eastern")) {
										st = "DE";
									} else if (timezzzz.equalsIgnoreCase("US/Mountain")) {
										st = "CO";
									} else if (timezzzz.equalsIgnoreCase("US/Central")) {
										st = "FL";
									}

									if (p.getProspectCall().getProspect().getStateCode().equalsIgnoreCase("N/A")) {
										if (!st.isEmpty()) {
											p.getProspectCall().getProspect().getStateCode().equalsIgnoreCase(st);
											if (timezzzz != null && !timezzzz.isEmpty())
												p.getProspectCall().getProspect().getTimeZone()
														.equalsIgnoreCase(timezzzz);
										}

									}
									////

									if (p.getProspectCall().getProspect().getPhone() != null
											&& p.getProspectCall().getProspect().getCompanyPhone() != null
											&& p.getProspectCall().getProspect().getPhone().equalsIgnoreCase(
													p.getProspectCall().getProspect().getCompanyPhone())) {

										if (p.getProspectCall().getProspect().getExtension() != null
												&& !p.getProspectCall().getProspect().getExtension().isEmpty()) {

										} else {
											directFlag = false;

										}
									}

									StringBuffer sb = new StringBuffer();
									String emp = "";
									String mgmt = "";
									String direct = "";
									if (directFlag) {
										direct = "1";
									} else {
										direct = "2";
									}
									String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
									sb.append(p.getStatus());
									sb.append("|");
									if (p.getProspectCall().getProspect()
											.getCustomAttributeValue("minEmployeeCount") instanceof Number) {
										Number n1 = (Number) p.getProspectCall().getProspect()
												.getCustomAttributeValue("minEmployeeCount");
										sb.append(getBoundry(n1, "min"));
										sb.append("-");
										emp = getEmployeeWeight(n1);
									} else {
										String x = "0";
										if (p.getProspectCall().getProspect()
												.getCustomAttributeValue("minEmployeeCount") != null)
											x = p.getProspectCall().getProspect()
													.getCustomAttributeValue("minEmployeeCount").toString();
										Double a = Double.parseDouble(x);
										sb.append(getBoundry(a, "min"));
										sb.append("-");
										emp = getEmployeeWeight(a);
									}
									if (p.getProspectCall().getProspect()
											.getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
										Number n2 = (Number) p.getProspectCall().getProspect()
												.getCustomAttributeValue("maxEmployeeCount");
										sb.append(getBoundry(n2, "max"));
									} else {
										String x = "0";
										if (p.getProspectCall().getProspect()
												.getCustomAttributeValue("maxEmployeeCount") != null)
											x = p.getProspectCall().getProspect()
													.getCustomAttributeValue("maxEmployeeCount").toString();
										Double a = Double.parseDouble(x);
										sb.append(getBoundry(a, "max"));
									}
									sb.append("|");
									sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
									mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());
									int qbSort = 0;
									if (!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty()) {
										String sortString = direct + tz + mgmt + emp;
										qbSort = Integer.parseInt(sortString);
									}
									if (qbSort == 0) {
										logger.debug("ZERO");
									}
									String slice = "";
									p.getProspectCall().setQualityBucket(sb.toString());
									p.getProspectCall().setQualityBucketSort(qbSort);
									p.getProspectCall().getProspect().setDirectPhone(directFlag);
									Long employee = new Long(emp);
									Long management = new Long(mgmt);
									if (directFlag) {
										if (management >= 4 || employee >= 12) {
											p.setDataSlice("slice2");
											p.getProspectCall().setDataSlice("slice2");
										} else {
											p.setDataSlice("slice1");
											p.getProspectCall().setDataSlice("slice1");
										}
										try {
											prospectSalutaryRepository.save(p);
										} catch (Exception ex1) {
											ex1.printStackTrace();
										}
									} else {
										p.setDataSlice("slice10");
										p.getProspectCall().setDataSlice("slice10");
										try {
											prospectSalutaryRepository.save(p);
										} catch (Exception ex1) {
											ex1.printStackTrace();
										}
									}

									if (!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty()
											|| p.getDataSlice() == null || p.getDataSlice().isEmpty()) {
										logger.debug(
												p.getProspectCall().getProspectCallId() + "--->" + p.getDataSlice());
										if (p.getDataSlice() == null || !p.getDataSlice().isEmpty()
												|| !p.getDataSlice().equalsIgnoreCase("slice10")) {
											p.setDataSlice("slice10");
											p.getProspectCall().setDataSlice("slice10");
											try {
												prospectSalutaryRepository.save(p);
											} catch (Exception ex1) {
												ex1.printStackTrace();
											}
										}
									}
									if (p.getProspectCall().getDataSlice() == null && p.getDataSlice() != null) {
										p.getProspectCall().setDataSlice(p.getDataSlice());
										p.setTagDirectIndirect(true);
										try {
											prospectSalutaryRepository.save(p);
										} catch (Exception ex1) {
											ex1.printStackTrace();
										}
									}
									/*
									 * logger.info(
									 * "=========> Updated record in prospectsalutary collection having prospectCallId : [{}] <=========== "
									 * , p.getProspectCall().getProspectCallId());
									 */
								}
							}

						}
					}
				}

				logger.info("This Program is completed for prospect salutary collection.");
			} catch (Exception e) {
				e.printStackTrace();
				error = ExceptionUtils.getStackTrace(e);
			}
			if (error != null) {
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
						"Test Quality Bucket Report Status of salutary collection - Errors", error);
			} else {
				error = "Success";
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
						"Test Quality Bucket Report Status of salutary collection", error);
			}

		};
		Thread t = new Thread(runnable);
		t.start();
		return "Direct/Indirect identification thread started.";
	}

	@Override
	public String testQualityBucketForProspectSalutary(OneOffDTO oneOffDTO) {
		Runnable runnable = () -> {
			String error = null;
			try {
				FileReader reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
				properties.load(reader);

				logger.error("Prospect Salutary thread started.");
				for (String campaignID : oneOffDTO.getCampaignIdList()) {
					Map<String, List<ProspectSalutary>> phoneProspectsMap = new HashMap<String, List<ProspectSalutary>>();
					long salutaryCount = prospectSalutaryRepository.count();
					int batchsize = 0;
					if (salutaryCount > 0) {
						batchsize = (int) (salutaryCount / 5000);
						long modSize = salutaryCount % 5000;
						if (modSize > 0) {
							batchsize = batchsize + 1;
						}
					}
					if (batchsize > 0) {
						for (int i = 0; i < batchsize; i++) {
							logger.info("salutary bucket sorting batch number : " + i);
							Pageable pageable = PageRequest.of(i, 10000);
							List<ProspectSalutary> salutaryFromDB = prospectSalutaryRepository
									.findSalutaryProspectsPage(pageable);
							if (salutaryFromDB != null && salutaryFromDB.size() > 0) {
								for (ProspectSalutary prospectSalutary : salutaryFromDB) {
									List<ProspectSalutary> prospectList = phoneProspectsMap
											.get(prospectSalutary.getProspectCall().getProspect().getPhone());
									if (prospectList != null && prospectList.size() > 0) {
										prospectList.add(prospectSalutary);
									} else {
										prospectList = new ArrayList<ProspectSalutary>();
										prospectList.add(prospectSalutary);
									}
									phoneProspectsMap.put(prospectSalutary.getProspectCall().getProspect().getPhone(),
											prospectList);
								}
							}
						}
					}

					for (Map.Entry<String, List<ProspectSalutary>> entry : phoneProspectsMap.entrySet()) {
						logger.info("salutary bucket size phone number : " + entry.getValue().size());
						List<ProspectSalutary> prospectList = entry.getValue();
						boolean directFlag = true;
						if (prospectList.size() > 1)
							directFlag = false;
						for (ProspectSalutary p : prospectList) {
							String st = "";
							String timezzzz = properties
									.getProperty(findAreaCode(p.getProspectCall().getProspect().getPhone()));
							if (timezzzz.equalsIgnoreCase("US/Pacific")) {
								st = "CA";
							} else if (timezzzz.equalsIgnoreCase("US/Eastern")) {
								st = "DE";
							} else if (timezzzz.equalsIgnoreCase("US/Mountain")) {
								st = "CO";
							} else if (timezzzz.equalsIgnoreCase("US/Central")) {
								st = "FL";
							}

							if (prospectList.size() == 1) {
								List<ProspectSalutary> prospectCallLogList = prospectSalutaryRepository
										.findByPhoneNumber(p.getProspectCall().getProspect().getPhone());
								if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
									boolean x = false;
									for (ProspectSalutary plog : prospectCallLogList) {
										if (p.getProspectCall().getProspect().getStateCode().equalsIgnoreCase("N/A")) {
											if (!st.isEmpty()) {
												p.getProspectCall().getProspect().getStateCode().equalsIgnoreCase(st);
												if (timezzzz != null && !timezzzz.isEmpty())
													p.getProspectCall().getProspect().getTimeZone()
															.equalsIgnoreCase(timezzzz);
											}

										}

										if (plog != null && plog.getProspectCall() != null
												&& plog.getProspectCall().getProspect() != null
												&& !plog.getProspectCall().getProspect().getFirstName()
														.equalsIgnoreCase(
																p.getProspectCall().getProspect().getFirstName())
												&& !plog.getProspectCall().getProspect().getLastName().equalsIgnoreCase(
														p.getProspectCall().getProspect().getLastName())) {
											x = true;
										}
									}
									if (x)
										p.getProspectCall().getProspect().setDirectPhone(false);
									else
										p.getProspectCall().getProspect().setDirectPhone(true);
								}
							} else {
								p.getProspectCall().getProspect().setDirectPhone(false);
							}
							StringBuffer sb = new StringBuffer();
							String emp = "";
							String mgmt = "";
							String direct = "";
							if (directFlag) {
								direct = "1";
							} else {
								direct = "2";
							}
							String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
							sb.append(p.getStatus());
							sb.append("|");
							if (p.getProspectCall().getProspect()
									.getCustomAttributeValue("minEmployeeCount") instanceof Number) {
								Number n1 = (Number) p.getProspectCall().getProspect()
										.getCustomAttributeValue("minEmployeeCount");
								sb.append(getBoundry(n1, "min"));
								sb.append("-");
								emp = getEmployeeWeight(n1);
							} else {
								String x = "0";
								if (p.getProspectCall().getProspect()
										.getCustomAttributeValue("minEmployeeCount") != null)
									x = p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount")
											.toString();
								Double a = Double.parseDouble(x);
								sb.append(getBoundry(a, "min"));
								sb.append("-");
								emp = getEmployeeWeight(a);
							}
							if (p.getProspectCall().getProspect()
									.getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
								Number n2 = (Number) p.getProspectCall().getProspect()
										.getCustomAttributeValue("maxEmployeeCount");
								sb.append(getBoundry(n2, "max"));
							} else {
								String x = "0";
								if (p.getProspectCall().getProspect()
										.getCustomAttributeValue("maxEmployeeCount") != null)
									x = p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")
											.toString();
								Double a = Double.parseDouble(x);
								sb.append(getBoundry(a, "max"));
							}
							sb.append("|");
							sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
							mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());
							int qbSort = 0;
							if (!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty()) {
								String sortString = direct + tz + mgmt + emp;
								qbSort = Integer.parseInt(sortString);
							}
							if (qbSort == 0) {
								logger.debug("ZERO");
							}
							String slice = "";
							p.getProspectCall().setQualityBucket(sb.toString());
							p.getProspectCall().setQualityBucketSort(qbSort);
							p.getProspectCall().getProspect().setDirectPhone(directFlag);
							Long employee = new Long(emp);
							Long management = new Long(mgmt);
							if (directFlag) {
								if (management >= 4 || employee >= 12) {
									p.setDataSlice("slice2");
									p.getProspectCall().setDataSlice("slice2");
								} else {
									p.setDataSlice("slice1");
									p.getProspectCall().setDataSlice("slice1");
								}
								prospectSalutaryRepository.save(p);
							} else {
								p.setDataSlice("slice10");
								p.getProspectCall().setDataSlice("slice10");
								prospectSalutaryRepository.save(p);
							}

							if (!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty()
									|| p.getDataSlice() == null || p.getDataSlice().isEmpty()) {
								logger.debug(p.getProspectCall().getProspectCallId() + "--->" + p.getDataSlice());
								if (p.getDataSlice() == null || !p.getDataSlice().isEmpty()
										|| !p.getDataSlice().equalsIgnoreCase("slice10")) {
									p.setDataSlice("slice10");
									p.getProspectCall().setDataSlice("slice10");
									prospectSalutaryRepository.save(p);
								}
							}
							if (p.getProspectCall().getDataSlice() == null && p.getDataSlice() != null) {
								p.getProspectCall().setDataSlice(p.getDataSlice());
								prospectSalutaryRepository.save(p);
							}
							logger.error(
									"=========> Updated record in prospectsalutary collection having prospectCallId : [{}] <=========== ",
									p.getProspectCall().getProspectCallId());
						}
					}
					logger.debug("Done");
					phoneProspectsMap = new HashMap<String, List<ProspectSalutary>>(); // Clear the map after operation
																						// completed.
				}
				logger.info("This Program is completed for prospect salutary collection.");
			} catch (Exception e) {
				e.printStackTrace();
				error = ExceptionUtils.getStackTrace(e);
			}
			if (error != null) {
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
						"Test Quality Bucket Report Status of salutary collection - Errors", error);
			} else {
				error = "Success";
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
						"Test Quality Bucket Report Status of salutary collection", error);
			}

		};
		Thread t = new Thread(runnable);
		t.start();
		return "Direct/Indirect identification thread started.";
	}

	public String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replace("-", "");
		int length = removeSpclChar.length();
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = phone.substring(startPoint, endPoint);
		return areaCode;
	}

	public String getStateWeight(String timeZone) {

		String est = "US/Eastern";
		String cst = "US/Central";
		String mst = "US/Mountain";
		String pst = "US/Pacific";

		if (timeZone != null && timeZone.equalsIgnoreCase(pst)) {
			return "1";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(mst)) {
			return "2";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(cst)) {
			return "3";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(est)) {
			return "4";
		}
		return "5";
	}

	public String getEmployeeWeight(Number n) {

		if (n.intValue() == 0) {
			return "12";
		}
		if (n.intValue() < 5) {
			return "01";
		}
		if (n.intValue() < 10) {
			return "02";
		}
		if (n.intValue() < 20) {
			return "03";
		}
		if (n.intValue() < 50) {
			return "04";
		}
		if (n.intValue() < 100) {
			return "05";
		}
		if (n.intValue() < 250) {
			return "06";
		}
		if (n.intValue() < 500) {
			return "07";
		}
		if (n.intValue() < 1000) {
			return "08";
		}
		if (n.intValue() < 5000) {
			return "09";
		}
		if (n.intValue() < 10000) {
			return "10";
		}

		if (n.intValue() < 11000) {
			return "11";
		}

		return "12";
	}

	public String getMgmtLevelWeight(String managementLevel) {
		if (managementLevel == null)
			return "6";
		if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "4";
		} else if (managementLevel.equalsIgnoreCase("Board Members")) {
			return "6";
		} else if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "5";
		} else if (managementLevel.toLowerCase().contains("Director".toLowerCase())) {
			return "3";
		} else if (managementLevel.toLowerCase().contains("MANAGER".toLowerCase())) {
			return "2";
		} else {
			return "1";
		}

		// return "";

	}

	public String getBoundry(Number n, String minMax) {
		if (minMax.equalsIgnoreCase("min")) {
			if (n.intValue() < 5) {
				return "1";
			}
			if (n.intValue() < 10) {
				return "5";
			}
			if (n.intValue() < 20) {
				return "10";
			}
			if (n.intValue() < 50) {
				return "20";
			}
			if (n.intValue() < 100) {
				return "50";
			}
			if (n.intValue() < 250) {
				return "100";
			}
			if (n.intValue() < 500) {
				return "250";
			}
			if (n.intValue() < 1000) {
				return "500";
			}
			if (n.intValue() < 5000) {
				return "1000";
			}
			if (n.intValue() < 10000) {
				return "5000";
			}

			if (n.intValue() < 11000) {
				return "10000";
			}

		} else if (minMax.equalsIgnoreCase("max")) {
			if (n.intValue() <= 5) {
				return "5";
			}
			if (n.intValue() <= 10) {
				return "10";
			}
			if (n.intValue() <= 20) {
				return "20";
			}
			if (n.intValue() <= 50) {
				return "50";
			}
			if (n.intValue() <= 100) {
				return "100";
			}
			if (n.intValue() <= 250) {
				return "250";
			}
			if (n.intValue() <= 500) {
				return "500";
			}
			if (n.intValue() <= 1000) {
				return "1000";
			}
			if (n.intValue() <= 5000) {
				return "5000";
			}
			if (n.intValue() <= 10000) {
				return "10000";
			}

			if (n.intValue() <= 100000) {
				return "11000";
			}

		}

		return "1";
		/*
		 * 1-5 5-10 10-20 20-50 50-100 100-250 250-500 500-1000 1000-5000 5000-10000
		 * 10000-11000
		 */
	}

	public String getMgmtLevel(String managementLevel) {
		if (managementLevel == null)
			return "VP-Level";
		if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "VP-Level";
		} else if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "C-Level";
		} else if (managementLevel.toLowerCase().contains("Director".toLowerCase())) {
			return "DIRECTOR";
		} else if (managementLevel.toLowerCase().contains("MANAGER".toLowerCase())) {
			return "MANAGER";
		} else {

			return "Non-Manager";
		}

	}

	@Override
	public List<ZoomInfoResponse> buildQueryByCriteria(OneOffDTO oneOffDTO)
			throws NoSuchAlgorithmException, IOException {
		String error = null;
		/*
		 * FileReader reader = new
		 * FileReader("src/main/resources/properties/rebuy-data.properties");
		 * properties.load(reader);
		 */
		OAuth = getZoomOAuth();
		List<ZoomInfoResponse> companyList = new ArrayList<ZoomInfoResponse>();

		String employeeCount = "";
		String revenue = "";

		// String fileNameWithTime = getCurrentDate();
		// please set no of record per org.
		int purchaseRecordPerOrg = 6;

		// BufferedReader br = null;
		int size = 25;
		/*
		 * XSSFWorkbook workbook = null; XSSFWorkbook workbookFailedRecord = null;
		 * FileWriter writerFind = null; FileWriter writerNotFindFile = null;
		 * XSSFWorkbook workbookComapName = null;
		 */

		try {

			/*
			 * RecordFindFile.createNewFile(); RecordNotFindFile.createNewFile();
			 * addressFile.createNewFile(); writerFind = new FileWriter(RecordFindFile);
			 * writerNotFindFile = new FileWriter(RecordNotFindFile);
			 */
			URL url;
			String sCurrentLine;
			int counter = 1;

			// sbr = new BufferedReader(new FileReader("C:\\ex\\Voip 10 to 99
			// Companies.txt")); // Enter location of your file where doamin name is there
			int companyCount = 0;
			// workbookComapName = new XSSFWorkbook();
			/*
			 * XSSFSheet sheetComapNameXL = workbookComapName.createSheet("FirstSheet");
			 * createFirstRowForCompanyId(sheetComapNameXL); workbook = new XSSFWorkbook();
			 * workbookFailedRecord = new XSSFWorkbook(); XSSFSheet sheet =
			 * workbook.createSheet("FirstSheet"); XSSFSheet sheetFailedRecord =
			 * workbookFailedRecord.createSheet("FirstSheet"); createFirstRow(sheet);
			 * createFirstRowFailedRecord(sheetFailedRecord); writerFind.write(
			 * "InputValue|domain|companyId|EmpRange|RevenueRange|SIC|NACIS" + "\n");
			 */

//            while ((sCurrentLine = br.readLine()) != null) {
			//////////////////////////////////////////////////////

			// String data[] = sCurrentLine.split("@");
			// companyDomainName = URLEncoder.encode(data[0], XtaasConstants.UTF8);
			/*
			 * String lname = URLEncoder.encode(data[1], XtaasConstants.UTF8); String
			 * company = URLEncoder.encode(data[2], XtaasConstants.UTF8); String phone =
			 * URLEncoder.encode(data[3], XtaasConstants.UTF8);
			 */

			// List<ProspectCallLog> psList =
			// prospectCallLogRepository.getData(fname,lname,phone);
			// if(psList!=null &&psList.size()>0){}

			//////////////////////////////////////////////////////////
			List<PersonRecord> personRecordList = new ArrayList<PersonRecord>();
			int pageNumber = 1;
			int offset = 1;
			companyCount++;
			// logger.debug(sCurrentLine + " Company Number = "+companyCount);
			// String companyId = sCurrentLine;
			// String companyDomain = URLEncoder.encode(sCurrentLine, XtaasConstants.UTF8);
			// StringBuilder queryString= createCriteria(companyDomain);
			// for(int i =1; i<=count; i++){
			/*
			 * StringBuilder sb = new StringBuilder(); if(i==count){
			 * 
			 * int x = ziparr.length; int p = 14200; while(p<x){ sb.append(ziparr[p]);
			 * sb.append(","); p++; }
			 * 
			 * }else{
			 * 
			 * int x = i*100; int p = x-100; while(p<x){ sb.append(ziparr[p]); if(p<(x-1))
			 * sb.append(","); p++; } }
			 */
			// StringBuilder queryString= createCriteriaForString(sb.toString());
			StringBuilder queryString = createCriteriaForString(oneOffDTO);
			for (int pageCount = 1; pageCount <= offset; pageCount++) {
				ZoomInfoResponse zoomInfoResponseCompanyResponse = getCompanySearchResponse(queryString, pageCount);
				String companyId = "";
				if (zoomInfoResponseCompanyResponse != null && zoomInfoResponseCompanyResponse
						.getCompanySearchResponse().getCompanySearchResults() != null) {
					logger.debug("###############################################################" + pageCount);
					String sic = "";
					String nacis = "";
					if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getMaxResults() > 5000) {
						offset = 5000;
					} else {
						offset = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getMaxResults();
					}
					if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyNAICS() != null)
						nacis = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanyNAICS().toString();

					if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanySIC() != null)
						sic = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanySIC().toString();
					companyId = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyWebsite()
							+ "|"
							+ zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
									.getCompanyRecord().get(0).getCompanyId()
							+ "|"
							+ zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
									.getCompanyRecord().get(0).getCompanyEmployeeCountRange()
							+ "|" + zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
									.getCompanyRecord().get(0).getCompanyRevenueRange()
							+ "|" + sic + "|" + nacis;
					companyList.add(zoomInfoResponseCompanyResponse);
					if (zoomInfoResponseCompanyResponse != null
							&& zoomInfoResponseCompanyResponse.getCompanySearchResponse().getMaxResults() > 0) {
						if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord() != null
								&& zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().size() > 0
								&& zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanyId() != null) {
							String zoomCompanyId = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
									.getCompanySearchResults().getCompanyRecord().get(0).getCompanyId();
							//List<AbmListDetail> abmdetailList = abmListDetailRepository
								//	.findABMListListByCompanyId(zoomCompanyId);
							long companyExists = abmListDetailRepository.countByCompanyId(zoomCompanyId);
							if (companyExists > 0) {
								// do nothing
							} else {
								insertAbmListDetail(zoomInfoResponseCompanyResponse);
							}
						}
					}
					// writerFind.write(companyId + "\n");
				}

			} // page loop
			insertAbmList(oneOffDTO.getPrimaryCampaignId(), companyList, "CompaniesByCriteria");

			// }
			// } // company list loop

			/*
			 * FileOutputStream outputStream = new FileOutputStream( new
			 * File("PurchaseDataByCompany"+fileNameWithTime+"_"+campaignId+".xlsx")) ;
			 * workbook.write(outputStream); FileOutputStream outputStreamCompanyName = new
			 * FileOutputStream( new
			 * File("CompanyDetail"+fileNameWithTime+"_"+campaignId+".xlsx")) ;
			 * workbookComapName.write(outputStreamCompanyName); FileOutputStream
			 * outputStreamFailedRecord = new FileOutputStream( new
			 * File("PurchaseDataByCompanyFailedRecordInPreview"+fileNameWithTime+"_"+
			 * campaignId+".xlsx")) ; workbookFailedRecord.write(outputStreamFailedRecord);
			 */

			/*
			 * writerFind.flush(); writerFind.close(); writerNotFindFile.flush();
			 * writerNotFindFile.close();
			 */
		} catch (Exception e) {
			/*
			 * FileOutputStream outputStream = new FileOutputStream( new
			 * File("PurchaseDataByCompany" + fileNameWithTime + "_" + campaignId +
			 * ".xlsx")); workbook.write(outputStream); FileOutputStream
			 * outputStreamFailedRecord = new FileOutputStream(new File(
			 * "PurchaseDataByCompanyFailedRecordInPreview" + fileNameWithTime + "_" +
			 * campaignId + ".xlsx")); workbookFailedRecord.write(outputStreamFailedRecord);
			 * logger.debug("Error while purchasing Record : " + e); writerFind.flush();
			 * writerFind.close(); writerNotFindFile.flush(); writerNotFindFile.close();
			 */
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "CompaniesByCriteria- Errors", error);
		} else {
			error = "Success";
			// XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Companies by search
			// Criteria Status- SUCCESS", error);
		}
		logger.debug("purchasing Record Successfully done. Records Purchased: " + rowCount);
		logger.debug("Records failed in Preview mode : " + rowCountFailedRecord);
		return companyList;
	}

	public String getZoomOAuth() {
		String oAuth = "";

		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (cachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		// Date dateold = zt.getUpdatedDate();

		Date currDate = new Date();

		long duration = currDate.getTime() - cachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		return tokenKey;
	}

	private String getCurrentDate() {
		String currentDate = new Date().toString();
		String removeSpaceStr = currentDate.replaceAll(" ", "_");
		String removeColunStr = removeSpaceStr.replaceAll(":", "_");
		return removeColunStr;
	}

	private void createFirstRowForCompanyId(XSSFSheet sheet) {
		int defaultCol = 0;
		Cell newCell = null;
		Row firstRow = sheet.createRow(0);
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Company Name");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Company Id");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Company Domain");
	}

	private void createFirstRow(XSSFSheet sheet) {
		int defaultCol = 0;
		Cell newCell = null;
		Row firstRow = sheet.createRow(0);
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("First Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Last Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Title");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Department*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Email*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Phone*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Organization Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("State Code*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Country*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 1");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 2");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 3");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 4");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Minimum Revenue");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Maximum Revenue");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Minimum Employee Count");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Maximum Employee Count");
		// newCell = firstRow.createCell(defaultCol++);
		// newCell.setCellValue("Extension");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Source");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("SourceId");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("OrganizationId");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("ManagementLevel");
	}

	private void createFirstRowFailedRecord(XSSFSheet sheet) {
		int defaultCol = 0;
		Cell newCell = null;
		Row firstRow = sheet.createRow(0);
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("First Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Last Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Title");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Department*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Email*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Phone*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Organization Name*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("State Code*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Country*");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 1");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 2");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 3");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Industry 4");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Minimum Revenue");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Maximum Revenue");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Minimum Employee Count");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Maximum Employee Count");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Source");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("SourceId");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("OrganizationId");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("ManagementLevel");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Reason For Reject");
		newCell = firstRow.createCell(defaultCol++);
		newCell.setCellValue("Total Result");
	}

	private StringBuilder createCriteriaForString(OneOffDTO oneOffDTO) throws UnsupportedEncodingException {
		/*
		 * String data[] = companyValue.split("@"); String fname =
		 * URLEncoder.encode(data[0], XtaasConstants.UTF8); String lname =
		 * URLEncoder.encode(data[1], XtaasConstants.UTF8); String company =
		 * URLEncoder.encode(data[2], XtaasConstants.UTF8);
		 */
		// String title = URLEncoder.encode(data[3], XtaasConstants.UTF8);

		// TODO LMA BUYING $25M to 250 M
		StringBuilder queryString = new StringBuilder();

		/*
		 * String minemp = "5000"; String maxemp = "9999"; String minempitr = "499";
		 * String minrev = "1000000"; String maxrev = "5000000";
		 * 
		 * String finance = "2314,67850,133386,198922,264458,329994";//ma in String
		 * medical = "2826,133898,68362,199434,264970,17042186,33819402"; String legal =
		 * "3594";//ma in String advertising = "131594";//ma in String edu =
		 * "1802,67338,132874,198410"; String telecomm = "5898,71434,136970,202506";//ma
		 * in String bizService =
		 * "522,66058,131594,197130,262666,524810,328202,459274,590346,655882,786954,852490,918026,721418"
		 * ;//ma in String manufacturing=
		 * "3850,69386,790282,200458,16977674,33754890,67309322,50532106,265994,17043210,33820426,50597642,67374858,331530,50663178,33885962,67440394,17108746,17108746,17174282,33951498,67505930,50728714,101060362,201723658,117837578,134614794,151392010,184946442,462602,34017034,17239818,50794250,67571466,528138,17305354,34082570,50859786,659210,724746,986890,921354,1117962,1183498,1052426,1249034,1314570";
		 * String retail =
		 * "5386,70922,136458,201994,267530,398602,464138,333066,529674,595210,660746,791818,857354,988426,922890,1185034,1250570,1316106,1053962,1119498";
		 * String Education = "1802,67338,198410"; String it = "5642,136714,71178";
		 */
		String hashtagField = oneOffDTO.getHashtagField();
		String industryClassification = oneOffDTO.getIndustryClassification();
		String industryCode = oneOffDTO.getIndustryCode();
		String minEmp = oneOffDTO.getMinEmp();
		String maxEmp = oneOffDTO.getMaxEmp();
		String minRev = oneOffDTO.getMinRev();
		String maxRev = oneOffDTO.getMaxRev();
		String zip = oneOffDTO.getZip();
		String country = oneOffDTO.getCountry();
		// String industryCode = properties.getProperty("IndustryCode");

		if (hashtagField != null)
			queryString.append("hashtagField=" + hashtagField.trim());

		if (hashtagField != null && industryCode != null)
			queryString.append("&IndustryCode=" + industryCode.trim());
		else if (industryCode != null)
			queryString.append("IndustryCode=" + industryCode.trim());

		if (hashtagField != null && industryClassification != null)
			queryString.append(
					"&IndustryClassification=" + URLEncoder.encode(industryClassification, XtaasConstants.UTF8));
		else if (industryClassification != null)
			queryString
					.append("IndustryClassification=" + URLEncoder.encode(industryClassification, XtaasConstants.UTF8));

		if ((hashtagField != null || industryCode != null || industryClassification != null) && minRev != null)
			queryString.append("&RevenueClassificationMin=" + URLEncoder.encode(minRev, XtaasConstants.UTF8));
		else if (minRev != null)
			queryString.append("RevenueClassificationMin=" + URLEncoder.encode(minRev, XtaasConstants.UTF8));

		if ((hashtagField != null || industryCode != null || industryClassification != null || minRev != null)
				&& maxRev != null)
			queryString.append("&RevenueClassificationMax=" + URLEncoder.encode(maxRev, XtaasConstants.UTF8));
		else if (maxRev != null)
			queryString.append("RevenueClassificationMax=" + URLEncoder.encode(maxRev, XtaasConstants.UTF8));

		if ((hashtagField != null || industryCode != null || industryClassification != null || minRev != null
				|| maxRev != null) && minEmp != null)
			queryString.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(minEmp, XtaasConstants.UTF8));
		else if (minEmp != null)
			queryString.append("EmployeeSizeClassificationMin=" + URLEncoder.encode(minEmp, XtaasConstants.UTF8));

		if ((hashtagField != null || industryCode != null || industryClassification != null || minRev != null
				|| maxRev != null || minEmp != null) && maxEmp != null)
			queryString.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(maxEmp, XtaasConstants.UTF8));
		else if (maxEmp != null)
			queryString.append("EmployeeSizeClassificationMax=" + URLEncoder.encode(maxEmp, XtaasConstants.UTF8));

		// queryString = queryString.append("companyName="+companyValue);
		// queryString.append("&lastName="+lname);
		// queryString.append("&personTitle="+title);
		// queryString.append("&titleSeniority=MANAGER,DIRECTOR,VP_EXECUTIVES");
		// queryString.append("companyName="+companyValue);
		// queryString.append("&Country=Country.USA&locationSearchType=Person");
		queryString.append("&Country=" + URLEncoder.encode(country, XtaasConstants.UTF8));
		// queryString.append("&ZipCode="+zip);

		// queryString.append("&companyDomainName="+companyValue);
		// queryString.append("&companyPastOrPresent=1&ContactRequirements=5");

		return queryString;
	}

	private ZoomInfoResponse getCompanySearchResponse(StringBuilder queryString, int pageCount) throws IOException {
		int size = 1;
		try {
			URL url;
			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			logger.debug(previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse zoomInfoResponse = getResponse(response);
			return zoomInfoResponse;
		} catch (Exception e) {
			logger.debug("Exception in building URL ; " + e);
			return null;
		}

	}

	private String buildCompanyUrl(String queryString, /* int offset, int size, */String personId)
			throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;

		// logger.debug("Key : "+key);
		if (personId != null) {
			/*
			 * String
			 * baseUrl="https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			 * String url = baseUrl + "pc="+partnerKey +"&CompanyID="+personId +
			 * "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key="
			 * +key; return url;
			 */
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companysic,companyType&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		}

	}

	private ZoomInfoResponse getResponse(StringBuffer response) throws IOException {
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null) {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
			return zoomInfoResponse;
		} else {
			return null;
		}
	}

	private void writeABMExcelHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("domain");
		cell = headerRow.createCell(1);
		cell.setCellValue("companyId");
		cell = headerRow.createCell(2);
	}

	private void writeToABMExcel(AbmListNew abm, Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue(abm.getCompanyName());
		cell = row.createCell(1);
		cell.setCellValue(abm.getCompanyId());
		cell = row.createCell(2);
	}

	private void insertAbmList(String campaignId, List<ZoomInfoResponse> companyResponse, String type) {
		// List<AbmList> abmListList =
		// abmListRepository.findABMListByCampaignId(campaignId);
		// List<AbmListNew> companyList = new ArrayList<KeyValuePair<String,String>>();
		// Map<String,String> companyMap = new HashMap<String, String>();
		try {
			/////////////////
			XSSFWorkbook workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("Abm List for Campaign Id : " + campaignId);
			int rowCount = 0;
			Row headerRow = sheet.createRow(rowCount);
			writeABMExcelHeaders(headerRow);
			///////////////////////

			if (companyResponse != null && companyResponse.size() > 0) {
				for (ZoomInfoResponse comp : companyResponse) {
					// KeyValuePair<String, String> kv = new KeyValuePair<String, String>();
					String dom = "";
					String cid = "";
					if (comp.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
							.getCompanyWebsite() != null) {
						dom = comp.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
								.getCompanyWebsite();
						cid = comp.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
								.getCompanyId();
					} else {
						dom = comp.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
								.getCompanyName();
						cid = comp.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
								.getCompanyId();
					}
					long abmListList = abmListNewRepository.countByCampaignCompanyName(campaignId,
							dom);
					if (abmListList > 0) {
						// Do Nothing
					} else {
						AbmListNew abmListNew = new AbmListNew();
						abmListNew.setCampaignId(campaignId);
						abmListNew.setCompanyId(cid);
						abmListNew.setCompanyName(dom);
						abmListNew.setStatus("ACTIVE");
						Row row = sheet.createRow(++rowCount);
						writeToABMExcel(abmListNew, row);
						// companyList.add(kv);
					}
				}
			}
			// abmList.setCompanyList(companyList);
			// abmListRepository.save(abmList);

			String fileName = campaignId + "_" + type + ".xlsx";
			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			workbook.close();
			XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com", type, "SUCCESS", fileNames);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void insertAbmListDetail(ZoomInfoResponse companyResponse) {
		AbmListDetail abm = new AbmListDetail();
		if (companyResponse != null && companyResponse.getCompanySearchResponse().getMaxResults() > 0) {
			String zoomCompanyId = companyResponse.getCompanySearchResponse().getCompanySearchResults()
					.getCompanyRecord().get(0).getCompanyId();

			// TODO add all the repsonse from company
			abm.setCompanyName(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
					.get(0).getCompanyName());
			abm.setCompanyId(zoomCompanyId);
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyAddress() != null) {
				CompanyAddress ca = new CompanyAddress();
				ca = companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyAddress();
				abm.setCompanyAddress(ca);
			}
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyDescription() != null)
				abm.setCompanyDescription(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyDescription());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyDetailXmlUrl() != null)
				abm.setCompanyDetailXmlUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyDetailXmlUrl());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyEmployeeCount() != null)
				abm.setCompanyEmployeeCount(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyEmployeeCount());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyEmployeeCountRange() != null)
				abm.setCompanyEmployeeCountRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyEmployeeCountRange());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyIndustry() != null)
				abm.setCompanyIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyIndustry());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyNAICS() != null)
				abm.setCompanyNAICS(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyNAICS());
			/*
			 * if(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getCompanyName()!=null)
			 * abm.setZoomCompanyName(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getCompanyName());
			 */
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyProductsAndServices() != null)
				abm.setCompanyProductsAndServices(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyProductsAndServices());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRanking() != null)
				abm.setCompanyRanking(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRanking());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenueIn000s() != null)
				abm.setCompanyRevenueIn000s(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRevenueIn000s());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenue() != null)
				abm.setCompanyRevenue(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRevenue());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenueRange() != null)
				abm.setCompanyRevenueRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRevenueRange());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanySIC() != null)
				abm.setCompanySIC(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanySIC());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyTicker() != null)
				abm.setCompanyTicker(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyTicker());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyTopLevelIndustry() != null)
				abm.setCompanyTopLevelIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyTopLevelIndustry());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyType() != null)
				abm.setCompanyType(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyType());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyWebsite() != null)
				abm.setCompanyWebsite(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyWebsite());
			abm.setDefunct(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
					.get(0).isDefunct());
			/*
			 * if(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getHashtags()!=null)
			 * abm.setHashtags(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getHashtags());
			 */
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getPhone() != null)
				abm.setPhone(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getPhone());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getSimilarCompanies() != null)
				abm.setSimilarCompanies(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getSimilarCompanies());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getZoomCompanyUrl() != null)
				abm.setZoomCompanyUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getZoomCompanyUrl());

			abmListDetailRepository.save(abm);
		} else {
			abm.setCompanyId("NOT_FOUND");
			abmListDetailRepository.save(abm);
		}

	}

	@Override
	public List<ZoomInfoResponse> buildQueryByIndustries(OneOffDTO oneOffDto)
			throws NoSuchAlgorithmException, IOException {
		String error = null;
		/*
		 * FileReader reader = new
		 * FileReader("src/main/resources/properties/rebuy-data.properties");
		 * properties.load(reader);
		 */
		OAuth = getZoomOAuth();
		List<ZoomInfoResponse> companyList = new ArrayList<ZoomInfoResponse>();
		/*
		 * FileReader reader = new
		 * FileReader("src/main/resources/properties/rebuyzip.properties");
		 * properties.load(reader); String zipcodes =
		 * properties.getProperty("ZIPCODES"); String []ziparr = zipcodes.split(",");
		 * int ziplength = ziparr.length; int count = ziplength/100; int zipmod =
		 * ziplength %100; if(zipmod>0){ count++; }
		 */
		// HashMap<String, Object> map = agentController.getCallbackConfig("GL");

		String employeeCount = "";
		String revenue = "";

		// String fileNameWithTime = getCurrentDate();
		// please set no of record per org.
		int purchaseRecordPerOrg = 6;

		// BufferedReader br = null;
		int size = 25;

		try {

			URL url;
			String sCurrentLine;
			int counter = 1;

			// s br = new BufferedReader(new FileReader("C:\\ex\\Cloud Comm 10 to 19.txt"));
			// // Enter location of your file where doamin name is there
			int companyCount = 0;

			/////////////////////////////////////////
			String EST = EASTERN;
			String CST = CENTRAL;
			String MST = MOUNTAIN;
			String PST = PDT;
			StringBuffer sb = new StringBuffer();
			if (EST != null && !EST.isEmpty()) {
				sb.append(EST);
			}
			if (CST != null && !CST.isEmpty()) {
				if (EST != null && !EST.isEmpty())
					sb.append(",");
				sb.append(CST);
			}
			if (MST != null && !MST.isEmpty()) {
				if ((EST != null && !EST.isEmpty()) || (CST != null && !CST.isEmpty()))
					sb.append(",");
				sb.append(MST);
			}
			if (PST != null && !PST.isEmpty()) {
				if ((EST != null && !EST.isEmpty()) || (CST != null && !CST.isEmpty())
						|| (MST != null && !MST.isEmpty()))
					sb.append(",");
				sb.append(PST);
			}

			String allStates = sb.toString();
			List<String> stateList = new ArrayList<String>();
			String stateArray[] = allStates.split(",");
			for (int i = 0; i < stateArray.length; i++) {
				stateList.add(stateArray[i]);
			}
			////////////////////////////////////////////////////

//            while ((sCurrentLine = br.readLine()) != null) {
			//////////////////////////////////////////////////////

			// String data[] = sCurrentLine.split("@");
			// companyDomainName = URLEncoder.encode(data[0], XtaasConstants.UTF8);
			/*
			 * String lname = URLEncoder.encode(data[1], XtaasConstants.UTF8); String
			 * company = URLEncoder.encode(data[2], XtaasConstants.UTF8); String phone =
			 * URLEncoder.encode(data[3], XtaasConstants.UTF8);
			 */

			// List<ProspectCallLog> psList =
			// prospectCallLogRepository.getData(fname,lname,phone);
			// if(psList!=null &&psList.size()>0){}

			//////////////////////////////////////////////////////////
			List<PersonRecord> personRecordList = new ArrayList<PersonRecord>();
			int pageNumber = 1;
			int offset = 1;
			companyCount++;
			// logger.debug(sCurrentLine + " Company Number = "+companyCount);
			// String companyId = sCurrentLine;
			// String companyDomain = URLEncoder.encode(sCurrentLine, XtaasConstants.UTF8);
			// StringBuilder queryString= createCriteria(companyDomain);
			// for(int i =1; i<=count; i++){
			/*
			 * StringBuilder sb = new StringBuilder(); if(i==count){
			 * 
			 * int x = ziparr.length; int p = 14200; while(p<x){ sb.append(ziparr[p]);
			 * sb.append(","); p++; }
			 * 
			 * }else{
			 * 
			 * int x = i*100; int p = x-100; while(p<x){ sb.append(ziparr[p]); if(p<(x-1))
			 * sb.append(","); p++; } }
			 */
			// StringBuilder queryString= createCriteriaForString(sb.toString());
			boolean isIndustryCodes = false;
			String industryClassification = oneOffDto.getIndustryClassification();
			String industryCodes = oneOffDto.getIndustryCode();
			List<String> indArr = new ArrayList<String>();
			if (industryClassification != null && !industryClassification.isEmpty()) {
				String[] industryArray = industryClassification.split(",");
				indArr = Arrays.asList(industryArray);
			}

			List<String> indCodeArr = new ArrayList<String>();
			if (industryCodes != null && !industryCodes.isEmpty()) {
				String[] industryCodeArray = industryCodes.split(",");
				indCodeArr = Arrays.asList(industryCodeArray);
				isIndustryCodes = true;
				indArr = indCodeArr;
			}

			for (String industry : indArr) {
				boolean maxFlag = false;
				StringBuilder queryString = createCriteriaForStringq(oneOffDto, null);
				ZoomInfoResponse totalResponse = getCompanySearchResponse(queryString, 1);
				if (totalResponse == null || totalResponse.getCompanySearchResponse() == null) {
					continue;
				}
				int maxCount = totalResponse.getCompanySearchResponse().maxResults;
				// logger.debug(maxCount);
				// IndustryCount.write(industry + " : " + maxCount + "\n");
				if (maxCount > 5000) {
					maxFlag = true;
				}
				if (maxFlag) {
					for (String state : stateList) {
						queryString = createCriteriaForStringq(oneOffDto, state);
						for (int pageCount = 1; pageCount <= offset; pageCount++) {
							ZoomInfoResponse zoomInfoResponseCompanyResponse = getCompanySearchResponse(queryString,
									pageCount);
							logger.debug("State:" + state + "__Count:"
									+ zoomInfoResponseCompanyResponse.getCompanySearchResponse().maxResults);
							// IndustryStateCount.write(industry + " : " + state + " : " + maxCount + "\n");
							if (zoomInfoResponseCompanyResponse != null && zoomInfoResponseCompanyResponse
									.getCompanySearchResponse().getCompanySearchResults() != null) {
								logger.debug(
										"###############################################################" + pageCount);
								offset = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getTotalResults();
								// logger.debug("###############Total Result
								// ##############################"+offset);
								// writerFind.write(offset + "\n");
								String sic = "";
								String nacis = "";
								if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanyNAICS() != null)
									nacis = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS()
											.toString();

								if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanySIC() != null)
									sic = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC()
											.toString();
								String s = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
										.getCompanySearchResults().getCompanyRecord().get(0).getCompanyWebsite()
										// +"|"+sCurrentLine
										+ "|"
										+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().get(0).getCompanyName()
										+ "|"
										+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().get(0).getCompanyId()
										+ "|"
										+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().get(0)
												.getCompanyEmployeeCountRange()
										+ "|"
										+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().get(0)
												.getCompanyRevenueRange()
										+ "|" + sic + "|" + nacis;

								companyList.add(zoomInfoResponseCompanyResponse);
								if (zoomInfoResponseCompanyResponse != null && zoomInfoResponseCompanyResponse
										.getCompanySearchResponse().getMaxResults() > 0) {
									if (zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord() != null
											&& zoomInfoResponseCompanyResponse.getCompanySearchResponse()
													.getCompanySearchResults().getCompanyRecord().size() > 0
											&& zoomInfoResponseCompanyResponse.getCompanySearchResponse()
													.getCompanySearchResults().getCompanyRecord().get(0)
													.getCompanyId() != null) {
										String zoomCompanyId = zoomInfoResponseCompanyResponse
												.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
												.get(0).getCompanyId();
										long companyExists = abmListDetailRepository.countByCompanyId(zoomCompanyId);

										if (companyExists > 0) {
											// do nothing
										} else {
											insertAbmListDetail(zoomInfoResponseCompanyResponse);
										}
									}
								}
								// writerFind.write(s + "\n");
							}

						} // page loop
					} // state Loop
					insertAbmList(oneOffDto.getPrimaryCampaignId(), companyList, "CompaniesByIndustries");
				} else {
					for (int pageCount = 1; pageCount <= offset; pageCount++) {
						ZoomInfoResponse zoomInfoResponseCompanyResponse = getCompanySearchResponse(queryString,
								pageCount);
						if (zoomInfoResponseCompanyResponse != null && zoomInfoResponseCompanyResponse
								.getCompanySearchResponse().getCompanySearchResults() != null) {
							logger.debug("###############################################################" + pageCount);
							offset = zoomInfoResponseCompanyResponse.getCompanySearchResponse().getTotalResults();
							// logger.debug("###############Total Result
							// ##############################"+offset);
							// writerFind.write(offset + "\n");
							String sic = "";
							String nacis = "";
							if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
									.getCompanyRecord().get(0).getCompanyNAICS() != null)
								nacis = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
										.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS()
										.toString();

							if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
									.getCompanyRecord().get(0).getCompanySIC() != null)
								sic = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
										.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC().toString();
							String s = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
									.getCompanySearchResults().getCompanyRecord().get(0).getCompanyWebsite()
									// +"|"+sCurrentLine
									+ "|"
									+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanyName()
									+ "|"
									+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanyId()
									+ "|"
									+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0)
											.getCompanyEmployeeCountRange()
									+ "|"
									+ zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0)
											.getCompanyRevenueRange()
									// +"|"+zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0).getCompanyIndustry().toString()
									+ "|" + sic + "|" + nacis;

							companyList.add(zoomInfoResponseCompanyResponse);
							if (zoomInfoResponseCompanyResponse != null
									&& zoomInfoResponseCompanyResponse.getCompanySearchResponse().getMaxResults() > 0) {
								if (zoomInfoResponseCompanyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord() != null
										&& zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().size() > 0
										&& zoomInfoResponseCompanyResponse.getCompanySearchResponse()
												.getCompanySearchResults().getCompanyRecord().get(0)
												.getCompanyId() != null) {
									String zoomCompanyId = zoomInfoResponseCompanyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanyId();
									long companyExists = abmListDetailRepository.countByCompanyId(zoomCompanyId);

									if (companyExists > 0) {
										// do nothing
									} else {
										insertAbmListDetail(zoomInfoResponseCompanyResponse);
									}
								}
							}
							// writerFind.write(s + "\n");
						}

					} // page loop
					insertAbmList(oneOffDto.getPrimaryCampaignId(), companyList, "CompaniesByIndustries");

				}
				maxFlag = false;
			} // industry loop
				// } // company list loop

			/*
			 * FileOutputStream outputStream = new FileOutputStream( new
			 * File("PurchaseDataByCompany"+fileNameWithTime+"_"+campaignId+".xlsx")) ;
			 * workbook.write(outputStream); FileOutputStream outputStreamCompanyName = new
			 * FileOutputStream( new
			 * File("CompanyDetail"+fileNameWithTime+"_"+campaignId+".xlsx")) ;
			 * workbookComapName.write(outputStreamCompanyName); FileOutputStream
			 * outputStreamFailedRecord = new FileOutputStream( new
			 * File("PurchaseDataByCompanyFailedRecordInPreview"+fileNameWithTime+"_"+
			 * campaignId+".xlsx")) ; workbookFailedRecord.write(outputStreamFailedRecord);
			 */

		} catch (Exception e) {

			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		logger.debug("purchasing Record Successfully done. Records Purchased: " + rowCount);
		logger.debug("Records failed in Preview mode : " + rowCountFailedRecord);
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "CompaniesByIndustries - ERROR", error);
		} else {
			error = "Success";
			// XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Build Query By
			// Industries Report Status", error);
		}
		return companyList;
	}

	private StringBuilder createCriteriaForStringq(OneOffDTO oneOffDto, String state)
			throws UnsupportedEncodingException {
		/*
		 * String data[] = companyValue.split("@"); String fname =
		 * URLEncoder.encode(data[0], XtaasConstants.UTF8); String lname =
		 * URLEncoder.encode(data[1], XtaasConstants.UTF8); String company =
		 * URLEncoder.encode(data[2], XtaasConstants.UTF8);
		 */
		// String title = URLEncoder.encode(data[3], XtaasConstants.UTF8);

		// TODO LMA BUYING $25M to 250 M
		StringBuilder queryString = new StringBuilder();

		/*
		 * String minemp = "5000"; String maxemp = "9999"; String minempitr = "499";
		 * String minrev = "1000000"; String maxrev = "5000000";
		 * 
		 * String finance = "2314,67850,133386,198922,264458,329994";//ma in String
		 * medical = "2826,133898,68362,199434,264970,17042186,33819402"; String legal =
		 * "3594";//ma in String advertising = "131594";//ma in String edu =
		 * "1802,67338,132874,198410"; String telecomm = "5898,71434,136970,202506";//ma
		 * in String bizService =
		 * "522,66058,131594,197130,262666,524810,328202,459274,590346,655882,786954,852490,918026,721418"
		 * ;//ma in String manufacturing=
		 * "3850,69386,790282,200458,16977674,33754890,67309322,50532106,265994,17043210,33820426,50597642,67374858,331530,50663178,33885962,67440394,17108746,17108746,17174282,33951498,67505930,50728714,101060362,201723658,117837578,134614794,151392010,184946442,462602,34017034,17239818,50794250,67571466,528138,17305354,34082570,50859786,659210,724746,986890,921354,1117962,1183498,1052426,1249034,1314570";
		 * String retail =
		 * "5386,70922,136458,201994,267530,398602,464138,333066,529674,595210,660746,791818,857354,988426,922890,1185034,1250570,1316106,1053962,1119498";
		 * String Education = "1802,67338,198410"; String it = "5642,136714,71178";
		 */
		String hashtagField = oneOffDto.getHashtagField();
		String industryClassification = oneOffDto.getIndustryClassification();
		// properties.getProperty("IndustryClassification");
		String industryCode = oneOffDto.getIndustryCode();
		String minEmp = oneOffDto.getMinEmp();
		String maxEmp = oneOffDto.getMaxEmp();
		String minRev = oneOffDto.getMinRev();
		String maxRev = oneOffDto.getMaxRev();
		String Country = oneOffDto.getCountry();
		String zip = oneOffDto.getZip();
		String primaryIndustriesOnly = oneOffDto.getPrimaryCampaignId();
		// String industryCode = properties.getProperty("IndustryCode");
		boolean isIndustryCodes = false;
		if (industryCode != null && !industryCode.isEmpty())
			isIndustryCodes = true;

		if (hashtagField != null)
			queryString.append("hashtagField=" + hashtagField.trim());

		if (hashtagField != null && isIndustryCodes)
			queryString.append("&IndustryCode=" + industryCode.trim());
		else if (industryCode != null && isIndustryCodes)
			queryString.append("IndustryCode=" + industryCode.trim());

		if (hashtagField != null && industryClassification != null && !isIndustryCodes)
			queryString.append(
					"&IndustryClassification=" + URLEncoder.encode(industryClassification, XtaasConstants.UTF8));
		else if (industryClassification != null && !isIndustryCodes)
			queryString
					.append("IndustryClassification=" + URLEncoder.encode(industryClassification, XtaasConstants.UTF8));

		// primaryIndustriesOnly

		if ((hashtagField != null || industryClassification != null) && primaryIndustriesOnly != null)
			queryString
					.append("&primaryIndustriesOnly=" + URLEncoder.encode(primaryIndustriesOnly, XtaasConstants.UTF8));
		else if (primaryIndustriesOnly != null)
			queryString
					.append("primaryIndustriesOnly=" + URLEncoder.encode(primaryIndustriesOnly, XtaasConstants.UTF8));

		if ((hashtagField != null || industryClassification != null) && minRev != null)
			queryString.append("&RevenueClassificationMin=" + URLEncoder.encode(minRev, XtaasConstants.UTF8));
		else if (minRev != null)
			queryString.append("RevenueClassificationMin=" + URLEncoder.encode(minRev, XtaasConstants.UTF8));

		if ((hashtagField != null || industryClassification != null || minRev != null) && maxRev != null)
			queryString.append("&RevenueClassificationMax=" + URLEncoder.encode(maxRev, XtaasConstants.UTF8));
		else if (maxRev != null)
			queryString.append("RevenueClassificationMax=" + URLEncoder.encode(maxRev, XtaasConstants.UTF8));

		if ((hashtagField != null || industryClassification != null || minRev != null || maxRev != null)
				&& minEmp != null)
			queryString.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(minEmp, XtaasConstants.UTF8));
		else if (minEmp != null)
			queryString.append("EmployeeSizeClassificationMin=" + URLEncoder.encode(minEmp, XtaasConstants.UTF8));

		if ((hashtagField != null || industryClassification != null || minRev != null || maxRev != null
				|| minEmp != null) && maxEmp != null)
			queryString.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(maxEmp, XtaasConstants.UTF8));
		else if (maxEmp != null)
			queryString.append("EmployeeSizeClassificationMax=" + URLEncoder.encode(maxEmp, XtaasConstants.UTF8));

		// queryString = queryString.append("companyName="+companyValue);
		// queryString.append("&lastName="+lname);
		// queryString.append("&personTitle="+title);
		// queryString.append("&titleSeniority=MANAGER,DIRECTOR,VP_EXECUTIVES");
		// queryString.append("companyName="+companyValue);
		// queryString.append("&Country=Country.USA&locationSearchType=Person");
		queryString.append("&Country=" + URLEncoder.encode(Country, XtaasConstants.UTF8));
		if (state != null)
			queryString.append("&location=" + URLEncoder.encode(state, XtaasConstants.UTF8));

		// queryString.append("&ZipCode="+zip);

		// queryString.append("&companyDomainName="+companyValue);
		// queryString.append("&companyPastOrPresent=1&ContactRequirements=5");

		return queryString;
	}

	@Override
	public String buildQueryPurchaseSuccess(OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException {
		String error = null;
		String campaignId = oneOffDTO.getPrimaryCampaignId();

		int purchaseRecordPerOrg = 10;
		int totalPurchaseLimit = oneOffDTO.getTotalPurchaseCount();// Integer.parseInt(properties.getProperty("TotalPurchaseLimit"));

		org.springframework.data.domain.Pageable pagebale = null;
		List<String> excludeStates = new ArrayList<String>();
		excludeStates.add("XX");
		Map<String, List<String>> existingProspects = new HashMap<String, List<String>>();
		// List<ProspectCallLog> prospectsCallingList =
		// prospectCallLogRepository.findProspectsForDialer(campaignId,excludeStates,
		// new Long(5), 1, pagebale);
		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsTenByQueued(campaignId);
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsTenByRetryCount(1,
				campaignId);
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsTenByRetryCount(2,
				campaignId);
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsTenByRetryCount(3,
				campaignId);
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsTenByRetryCount(4,
				campaignId);
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsTenByRetryCount(5,
				campaignId);
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsTenByRetryCount(6,
				campaignId);
		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsTenByRetryCount(7,
				campaignId);
		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsTenByRetryCount(8,
				campaignId);
		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsTenByRetryCount(9,
				campaignId);
		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsTenByRetryCount(10,
				campaignId);
		List<ProspectCallLog> prospectCallLogList786 = prospectCallLogRepository.findContactsTenByRetryCount(786,
				campaignId);
		List<ProspectCallLog> prospectCallLogList886 = prospectCallLogRepository.findContactsTenByRetryCount(886,
				campaignId);
		List<ProspectCallLog> prospectCallLogList444444 = prospectCallLogRepository.findContactsTenByRetryCount(444444,
				campaignId);
		List<ProspectCallLog> prospectsCallingList = new ArrayList<ProspectCallLog>();
		prospectsCallingList.addAll(prospectCallLogList0);
		prospectsCallingList.addAll(prospectCallLogList1);
		prospectsCallingList.addAll(prospectCallLogList2);
		prospectsCallingList.addAll(prospectCallLogList3);
		prospectsCallingList.addAll(prospectCallLogList4);
		prospectsCallingList.addAll(prospectCallLogList5);
		prospectsCallingList.addAll(prospectCallLogList6);
		prospectsCallingList.addAll(prospectCallLogList7);
		prospectsCallingList.addAll(prospectCallLogList8);
		prospectsCallingList.addAll(prospectCallLogList9);
		prospectsCallingList.addAll(prospectCallLogList10);
		prospectsCallingList.addAll(prospectCallLogList786);
		prospectsCallingList.addAll(prospectCallLogList886);
		prospectsCallingList.addAll(prospectCallLogList444444);

		for (ProspectCallLog prospectCLog : prospectsCallingList) {
			String value = prospectCLog.getProspectCall().getProspect().getFirstName() + "|"
					+ prospectCLog.getProspectCall().getProspect().getLastName() + "|"
					+ prospectCLog.getProspectCall().getProspect().getCompany();
			String key = prospectCLog.getProspectCall().getProspect().getCompany();
			if (existingProspects.get(key) == null) {
				List<String> pVal = new ArrayList<String>();
				pVal.add(value);
				existingProspects.put(key, pVal);
			} else {
				List<String> cVal = existingProspects.get(key);
				cVal.add(value);
				existingProspects.put(key, cVal);
			}
			Long cou = recordsBoughtPerOrg.get(prospectCLog.getProspectCall().getProspect().getSourceCompanyId());
			if (cou != null)
				// logger.debug(cou);
				if (cou == null)
					cou = new Long(0);

			recordsBoughtPerOrg.put(prospectCLog.getProspectCall().getProspect().getSourceCompanyId(), cou + 1);

		}

		Set<CampaignContact> campaignContactList = new HashSet<CampaignContact>();
		BufferedReader br = null;
		int size = 25;

		try {
			List<String> companyIds = new ArrayList<String>();
			// companyIds.add("594a5c53e4b0c806cd1825d4");//ADOBEME
			companyIds.add(campaignId);// ADOBEMEClone

			List<String> distinctCompanyList = new ArrayList<String>();
			////////////////////////////////////////////////////////////////////////////
			int companyCount = 0;

			URL url;

			for (String emailid : oneOffDTO.getEmailIds()) {
				// String emailid = sCurrentLine;
				// for(String companyId : distinctCompanyList){
				List<PersonRecord> personRecordList = new ArrayList<PersonRecord>();
				int pageNumber = 1;
				int offset = 1;
				companyCount++;
				if (companyCount == 47)
					logger.debug("47");
				logger.debug(emailid + " Company Number = " + companyCount);
				// String companyId = sCurrentLine;
				// String companyDomain = URLEncoder.encode(sCurrentLine, XtaasConstants.UTF8);

				if (rowCount <= totalPurchaseLimit) {
					// String title = "";
					// for(String title : titles){
					StringBuilder queryString = new StringBuilder();

					if (personIdFlag) {
						queryString = createCriteria(emailid, true);
					} else {
						queryString = createCriteria(emailid, false);
					}

					for (int pageCount = 1; pageCount <= offset; pageCount++) {
						if (personRecordList.size() < purchaseRecordPerOrg) {
							String previewUrl = buildUrl(queryString.toString(), pageNumber, size, null);
							logger.debug(previewUrl);
							url = new URL(previewUrl);
							// Thread.sleep(15000);
							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							connection.setRequestMethod("GET");
							int responseCode = connection.getResponseCode();
							BufferedReader bufferedReader = new BufferedReader(
									new InputStreamReader(connection.getInputStream()));
							String inputline;
							StringBuffer response = new StringBuffer();
							while ((inputline = bufferedReader.readLine()) != null) {
								response.append(inputline);
							}
							bufferedReader.close();
							ZoomInfoResponse zoomInfoResponse = getResponse(response);

							if (zoomInfoResponse != null
									&& zoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults() != null) {
								int totalResult = zoomInfoResponse.getPersonSearchResponse().getTotalResults();
								logger.debug("Total Results --->" + totalResult);
								logger.debug("PAge Number  --->" + pageNumber);
								// int totalPageCount = totalResult/size;
								int totalPageCount = (int) Math.ceil((double) totalResult / size);
								offset = totalPageCount;
								if (rowCount <= totalPurchaseLimit) {
									Set<CampaignContact> pCampaignContactList = purchaseDetailRecord(oneOffDTO,
											zoomInfoResponse, emailid, personRecordList, purchaseRecordPerOrg, null);
									campaignContactList.addAll(pCampaignContactList);
								}
								pageNumber++; // increase the page number
								// getCompanyIdXL(zoomInfoResponse,sheetComapNameXL);
							} else {
								logger.debug("NO DATA FOUND-----------------" + emailid);
								// writerNotFindFile.write(companyId+ "\n");
							}
						} // no of purchased per org
					} // page loop
						// }//for email flag
						// }
				}
			} // company list loop

		} catch (Exception e) {
			error = ExceptionUtils.getStackTrace(e);
			e.printStackTrace();
		}
		logger.debug("purchasing Record Successfully done. Records Purchased: " + rowCount);
		logger.debug("Records failed in Preview mode : " + rowCountFailedRecord);
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
					"Build Query Purchase Success Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Build Query Purchase Success Report Status",
					error);
		}

		return error;
		////////////////////////// insert data in DB //////////////////////
		/*
		 * Boolean insertInDB =
		 * Boolean.parseBoolean(properties.getProperty("UpdateInDB")); if(insertInDB){
		 * List<CampaignContact> campaignContacts =
		 * campaignContactRepository.save(campaignContactList); if
		 * (!campaignContacts.isEmpty()) { List<ProspectCallLog> prospectCallLogs = new
		 * ArrayList<ProspectCallLog>(); List<ProspectCallInteraction>
		 * prospectCallInteractions = new ArrayList<ProspectCallInteraction>(); for
		 * (CampaignContact campaignContact : campaignContacts) { Prospect prospect =
		 * campaignContact.toProspect();
		 * prospect.setSource(campaignContact.getSource());
		 * prospect.setSourceId(campaignContact.getSourceId());
		 * prospect.setCampaignContactId(campaignContact.getId()); ProspectCallLog
		 * prospectCallLog = new ProspectCallLog(); ProspectCall prospectCall = new
		 * ProspectCall(UUID.randomUUID().toString(), campaignId, prospect);
		 * //prospectCallLog.setProspectCall(new
		 * ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));
		 * prospectCallLog.setProspectCall(prospectCall);
		 * prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
		 * prospectCallLogs.add(prospectCallLog); ProspectCallInteraction
		 * prospectCallInteraction = new ProspectCallInteraction();
		 * prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
		 * prospectCallInteraction.setProspectCall(prospectCall);
		 * prospectCallInteractions.add(prospectCallInteraction); }
		 * prospectCallLogRepository.save(prospectCallLogs);
		 * prospectCallInteractionRepository.save(prospectCallInteractions); } }
		 */

		//////////////////////// END of Insert Data in DB //////////////////////
	}

	private StringBuilder createCriteria(String emaild, boolean personFlag) throws UnsupportedEncodingException {
		StringBuilder queryString = new StringBuilder();

		if (!personFlag)
			queryString = queryString.append("EmailAddress=" + URLEncoder.encode(emaild, XtaasConstants.UTF8));
		else
			queryString = queryString.append("personIds=" + URLEncoder.encode(emaild, XtaasConstants.UTF8));

		return queryString;
	}

	private String buildUrl(String queryString, int offset, int size, String personId) throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;
		if (personId != null) {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&PersonID=" + personId
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction&key=" + key;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,companyAddress,companyRevenueNumeric,companyRevenueRange,companyEmployeeRange,companyEmployeeCount&key="
					+ key + "&rpp=" + size + "&page=" + offset;
			return url;
		}
	}

	private Set<CampaignContact> purchaseDetailRecord(OneOffDTO oneOffDTO, ZoomInfoResponse zoomInfoResponse,
			String companyName, List<PersonRecord> personRecordList, int purchaseRecordPerOrg, String searchTitle)
			throws NoSuchAlgorithmException, IOException {
		double minRevenue = 0;
		double maxRevenue = 0.0;
		double minEmployeeCount = 0;
		double maxEmployeeCount = 0;
		Set<CampaignContact> pCampaignContactList = new HashSet<CampaignContact>();

		List<PersonRecord> personRecordListForPurchased = getPersonRecordList(oneOffDTO, zoomInfoResponse, companyName,
				personRecordList, purchaseRecordPerOrg);

		if (personRecordListForPurchased.size() == 0) {
			return pCampaignContactList;
		}
		for (int i = 0; i < personRecordListForPurchased.size(); i++) {
			PersonRecord personRecord = personRecordListForPurchased.get(i);
			String personId = personRecord.getPersonId();
			String title = personRecord.getCurrentEmployment().getJobTitle();
			String managementLevel = "Manager";
			;
			List<String> mgmtlevelList = personRecord.getCurrentEmployment().getManagementLevel();
			if (mgmtlevelList != null && mgmtlevelList.size() > 0) {
				managementLevel = mgmtlevelList.get(0);
			}
			if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange() != null) {
				String[] revenueParts = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange()
						.split(" - ");

				if (revenueParts.length == 2) { // Ex response from Zoominfo:
					minRevenue = getRevenue(revenueParts[0]);
					maxRevenue = getRevenue(revenueParts[1]);

				} else if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange().contains("Over")) {
					String revenuePart = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
					minRevenue = getRevenue(revenuePart);

				} else if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange()
						.contains("Under")) {
					String revenuePart = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
					maxRevenue = getRevenue(revenuePart);
				}
			} else {
				minRevenue = 0;
				maxRevenue = 0;
			}
			if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange() != null) {
				String[] employeeCountParts = personRecord.getCurrentEmployment().getCompany()
						.getCompanyEmployeeCountRange().split(" - ");
				if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
					minEmployeeCount = Double.parseDouble(employeeCountParts[0].trim().replace(",", ""));
					maxEmployeeCount = Double.parseDouble(employeeCountParts[1].trim().replace(",", ""));

				} else if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange()
						.contains("Over")) {
					String employeeCountPart = personRecord.getCurrentEmployment().getCompany()
							.getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
					minEmployeeCount = Double.parseDouble(employeeCountPart);

				} else if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange()
						.contains("Under")) {
					String employeeCountPart = personRecord.getCurrentEmployment().getCompany()
							.getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
					maxEmployeeCount = Double.parseDouble(employeeCountPart);
				}
			} else {
				minEmployeeCount = 0;
				maxEmployeeCount = 0;
			}
			int totalResult = zoomInfoResponse.getPersonSearchResponse().getTotalResults();
			if (validateCompanyData(minRevenue, maxRevenue, minEmployeeCount, maxEmployeeCount, personRecord,
					totalResult) && validatePersonData(title)) {
				if (personId != null) {
					logger.debug("PersonId = " + personId);
					if (personIdList.contains(personId)) {
						logger.debug("Already Bought in this campaign");
						continue;
					}

					logger.debug("Total no of Record Purchased for this Job : " + rowCount);
					String personDetailRequestUrl = buildUrl(null, 0, 0, personId);
					logger.debug("Person Detail Request " + personDetailRequestUrl);
					URL url = new URL(personDetailRequestUrl); // purchase request
					ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(url);
					if (zoomInfoResponseDetail != null) {
						if (!isPersonRecordAlreadyPurchasedPCL(personRecord, oneOffDTO.getPrimaryCampaignId())) {
							String updateInDB = "true";
							if (updateInDB != null && updateInDB.equalsIgnoreCase("true")) {
								String ph = getPhone(zoomInfoResponseDetail.getPersonDetailResponse());
								String sc = getStateCode(zoomInfoResponseDetail.getPersonDetailResponse());
								if (ph != null && !ph.isEmpty() && sc != null && !sc.isEmpty()) {
									ProspectCallLog prospetCallLog = getPCLObject(oneOffDTO, zoomInfoResponseDetail,
											title, minRevenue, maxRevenue, minEmployeeCount, maxEmployeeCount,
											searchTitle, personRecord.getFirstName(), personRecord.getLastName(),
											personRecord.getCurrentEmployment().getCompany().getCompanyName(),
											personRecord.getCurrentEmployment().getJobTitle());
								}
							}
							// createCSVFile(zoomInfoResponseDetail,minRevenue,maxRevenue,minEmployeeCount,maxEmployeeCount,title,managementLevel,sheet);
							personIdList.add(personId);
						}
					}
				} else {
					logger.debug("PersonId is null");
				}
			}

		}
		return pCampaignContactList;
	}

	private List<PersonRecord> getPersonRecordList(OneOffDTO oneOffDTO, ZoomInfoResponse zoomInfoResponse,
			String companyName, List<PersonRecord> personRecordList, int purchaseRecordPerOrg) {
		// String purchaseRecordPerOrganization =
		// propertyService.getApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.PURCHASE_RECORDS_PER_ORG.name());
		// int purchaseRecordPerOrg = Integer.valueOf(purchaseRecordPerOrganization);//
		// Purchase Record per Organization.
		if (zoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults().getPersonRecord() != null) {
			List<PersonRecord> list = zoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
					.getPersonRecord();
			for (int i = 0; i < list.size(); i++) {
				if (personRecordList.size() < purchaseRecordPerOrg) {
					PersonRecord personRecord = list.get(i);
					if (personRecord != null) {
						int totalResults = zoomInfoResponse.getPersonSearchResponse().getTotalResults();
						try {
							// check if this personId is not already purchased
							/*
							 * if (isSourceIdAlreadyPurchased(personRecord.getPersonID())) {
							 * logger.debug("PersonId already purchased. Skipping to next one. " +
							 * personRecord.getPersonID()); String reason = "PersonId already purchased :" +
							 * personRecord.getPersonID();
							 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
							 * totalResults); personRecord = null; continue; } else
							 *//*
								 * if (!isCompanyMatch(personRecord,companyName)) { //check if person company is
								 * now match with the zoominfo company String reason = "Company Name"
								 * +companyName+ "doesn't match with the zoominfo company "+
								 * personRecord.getCurrentEmployment().getCompany().getCompanyName();
								 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
								 * totalResults); personRecord = null; continue; }else if
								 * (!isPersonBelongstoUS(personRecord)) { //check if the record belong to
								 * Country US //String reason = "Person does not belong to US. Belongs to " +
								 * personRecord.getLocalAddress().getCountry(); //
								 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
								 * totalResults); personRecord = null; continue; // }else
								 * if(!isStateExist(zoomInfoResponse)){
								 * 
								 * }else
								 */ if (personIdList.contains(personRecord.getPersonId())) { // check if the record is
																								// purchased in this job
																								// only
								String reason = "PPersonId already purchased for this job only "
										+ personRecord.getLocalAddress().getCountry();
								// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason,
								// totalResults);
								personRecord = null;
								continue;
							} else if (isPersonRecordAlreadyPurchased(oneOffDTO.getPrimaryCampaignId(), personRecord)) { // check
																															// if
																															// person
																															// is
																															// already
								// bought by name and company
								String reason = "First and Last name is already purchased";
								String sourceIdDB = getPersonIDB(oneOffDTO.getPrimaryCampaignId(), personRecord);
								// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason + "|" +
								// sourceIdDB, totalResults);
								personRecord = null;
								continue;
							}
							/*
							 * }else if (!isIndustryMatch(personRecord)) { //check if industry is metch or
							 * not String reason =
							 * "Industry is not match. Not match industry are :"+personRecord.getIndustry();
							 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
							 * totalResults); personRecord = null; continue; }else if
							 * (!isDepartmentMatch(personRecord)) { //check if person is already bought by
							 * name and company String reason = "Person Department doesn't match. Its "+
							 * personRecord.getCurrentEmployment().getJobFunction();
							 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
							 * totalResults); personRecord = null; continue; }else if
							 * (personRecord.getCurrentEmployment().getCompany().getCompanyId()!=null &&
							 * recordsBoughtPerOrg.containsKey(personRecord.getCurrentEmployment().
							 * getCompany().getCompanyId())) { //check if person is already bought by name
							 * and company String reason =
							 * "Already bought the specified number of prospects for this company "; Long c
							 * = recordsBoughtPerOrg.size();//.get(personRecord.getCurrentEmployment().
							 * getCompany().getCompanyID()); if(purchaseRecordPerOrg<=c){
							 * logger.debug(reason); // reason = reason; //
							 * createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,
							 * totalResults,companyRecord); personRecord = null; continue; }else{
							 * recordsBoughtPerOrg.put(personRecord.getCurrentEmployment().getCompany().
							 * getCompanyID(), c++); personRecordList.add(personRecord);
							 * personIdList.add(personRecord.getPersonID()); } }
							 */else {
								long c = 1;
								recordsBoughtPerOrg.put(personRecord.getCurrentEmployment().getCompany().getCompanyId(),
										c);
								personRecordList.add(personRecord);
								/* personIdList.add(personRecord.getPersonID()); */
							}

						} catch (Exception e) {
							logger.debug("Exception in validation  - :  " + e);
							e.printStackTrace();
						}
					}
				} else {
					break;
				}
			}

		}
		return personRecordList;
	}

	private void createXLForFailedSearchRecord(PersonRecord personRecord, XSSFSheet sheetFailedRecord, String reason,
			int totalResults) {
		try {
			Row row = sheetFailedRecord.getRow(0);
			Iterator<Cell> cellIterator = row.cellIterator();
			rowCountFailedRecord = rowCountFailedRecord + 1;
			Row dataRow = sheetFailedRecord.createRow(rowCountFailedRecord);
			int columnCount = -1;
			if (personRecord != null) {
				List<Double> list = getRevenueAndEmp(personRecord);
				List<String> industryList = personRecord.getIndustry();
				int industryListSize = 0;
				if (industryList != null) {
					industryListSize = industryList.size();
				}
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					String columnName = cell.getStringCellValue();
					if (columnName.equals("First Name") || columnName.equals("First Name*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = personRecord.getFirstName();
						newCell.setCellValue(val);
					} else if (columnName.equals("Last Name") || columnName.equals("Last Name*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = personRecord.getLastName();
						newCell.setCellValue(val);
					} else if (columnName.equals("Title")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = getTitle(personRecord);
						newCell.setCellValue(val);
					} else if (columnName.equals("Department") || columnName.equals("Department*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = getJobFunction(personRecord);
						newCell.setCellValue(val);
					} else if (columnName.equals("Email") || columnName.equals("Email*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = personRecord.getEmail();
						newCell.setCellValue(val);
					} else if (columnName.equals("Phone") || columnName.equals("Phone*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = personRecord.getPhone();
						if (val == null || "".equalsIgnoreCase(val)) {
							val = personRecord.getCurrentEmployment().getCompany().getCompanyPhone();
						}
						newCell.setCellValue(val);
					} else if (columnName.equals("Organization Name") || columnName.equals("Organization Name*")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = getCompanyName(personRecord);
						newCell.setCellValue(val);
					} else if (columnName.equals("State Code") || columnName.equals("State Code*")) {
						String val = getStateCode(personRecord);
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(val);
					} else if (columnName.equals("Country") || columnName.equals("Country*")) {
						String val = "";
						if (personRecord.getLocalAddress() != null) {
							val = personRecord.getLocalAddress().getCountry();
						}
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(val);
					} else if (columnName.equals("Industry 1")) {
						Cell newCell = dataRow.createCell(++columnCount);
						if (industryListSize > 0) {
							newCell.setCellValue(industryList.get(0));
						} else {
							newCell.setCellValue("");
						}
					} else if (columnName.equals("Industry 2")) {
						Cell newCell = dataRow.createCell(++columnCount);
						if (industryListSize > 1) {
							newCell.setCellValue(industryList.get(1));
						} else {
							newCell.setCellValue("");
						}
					} else if (columnName.equals("Industry 3")) {
						Cell newCell = dataRow.createCell(++columnCount);
						if (industryListSize > 2) {
							newCell.setCellValue(industryList.get(2));
						} else {
							newCell.setCellValue("");
						}
					} else if (columnName.equals("Industry 4")) {
						Cell newCell = dataRow.createCell(++columnCount);
						if (industryListSize > 3) {
							newCell.setCellValue(industryList.get(3));
						} else {
							newCell.setCellValue("");
						}
					} else if (columnName.equals("Minimum Revenue")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(list.get(0));
					} else if (columnName.equals("Maximum Revenue")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(list.get(1));
					} else if (columnName.equals("Minimum Employee Count")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(list.get(2));
					} else if (columnName.equals("Maximum Employee Count")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(list.get(3));
					} else if (columnName.equals("Source")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String source = "ZOOMI";
						newCell.setCellValue(source);
					} else if (columnName.equals("SourceId")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(personRecord.getPersonId());
					} else if (columnName.equals("OrganizationId")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = getCompanyId(personRecord);
						newCell.setCellValue(val);
					} else if (columnName.equals("ManagementLevel")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(personRecord.getManagementLevel());
					} else if (columnName.equals("AddressLine1")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(
								personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getStreet());
					} else if (columnName.equals("AddressLine2")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue("");
					} else if (columnName.equals("Postal Code")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(
								personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getZip());
					} else if (columnName.equals("City")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(
								personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCity());
						/*
						 * }else if (columnName.equals("Extension")) { Cell newCell =
						 * dataRow.createCell(++columnCount);
						 * newCell.setCellValue(personDetailRequest.getCurrentEmployment().get(0).
						 * getCompany().get);
						 */
					} else if (columnName.equals("Domain")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue(personRecord.getCurrentEmployment().getCompany().getWebsite());
					} else if (columnName.equals("SearchParam")) {
						Cell newCell = dataRow.createCell(++columnCount);
						newCell.setCellValue("");
					} else if (columnName.equals("Reason For Reject")) {
						Cell newCell = dataRow.createCell(++columnCount);
						String val = reason;
						newCell.setCellValue(val);
					} else if (columnName.equals("Total Result")) {
						Cell newCell = dataRow.createCell(++columnCount);
						int val = totalResults;
						newCell.setCellValue(val);
					}
				}
			}

		} catch (Exception e) {
			logger.debug("Exception " + e);
			e.printStackTrace();
		}
	}

	private String getTitle(PersonRecord personRecord) {
		String title = null;
		if (personRecord.getCurrentEmployment() != null) {
			title = personRecord.getCurrentEmployment().getJobTitle();
		}
		return title;
	}

	private String getJobFunction(PersonRecord personRecord) {
		String jobFunction = null;
		try {
			if (personRecord.getCurrentEmployment() != null) {
				if (personRecord.getCurrentEmployment().getJobFunction() != null) {
					jobFunction = personRecord.getCurrentEmployment().getJobFunction();
				}
			}

		} catch (NullPointerException e) {
			logger.debug("Department is Null : " + e);
			jobFunction = "_";
		}

		return jobFunction;
	}

	private String getCompanyName(PersonRecord personRecord) {
		String companyName = null;
		if (personRecord.getCurrentEmployment() != null) {
			if (personRecord.getCurrentEmployment().getCompany() != null) {
				companyName = personRecord.getCurrentEmployment().getCompany().getCompanyName();
			}
		}
		return companyName;
	}

	private String getStateCode(PersonRecord personRecord) {
		if (personRecord.getLocalAddress() != null) {
			String state = personRecord.getLocalAddress().getState();
			String stateCode = getState(state);
			return stateCode;
		}
		return null;
	}

	private String getCompanyId(PersonRecord personRecord) {
		String companyName = null;
		if (personRecord.getCurrentEmployment() != null) {
			if (personRecord.getCurrentEmployment().getCompany() != null) {
				companyName = personRecord.getCurrentEmployment().getCompany().getCompanyId();
			}
		}
		return companyName;
	}

	private String getState(String state) {
		EntityMapping toMapping = getEntityMapping();
		AttributeMapping attributeMapping = getReverseAttributeMapping("state", toMapping);
		List<ValueMapping> stateCode = attributeMapping.getValues();
		Iterator<ValueMapping> iterator = stateCode.iterator();
		while (iterator.hasNext()) {
			ValueMapping valueMapping = (ValueMapping) iterator.next();
			String to = valueMapping.getTo().replaceAll("%20", " ");
			if (to.equals(state)) {
				return valueMapping.getFrom();
			}
		}
		return null;

	}

	private EntityMapping getEntityMapping() {
		List<EntityMapping> toMappings = entityMappingRepository.findByFromSystemToSystem("xtaas",
				ContactListProviderType.ZOOMINFO.name());
		if (toMappings.size() > 0) {
			return toMappings.get(0);
		}
		return null;
	}

	private AttributeMapping getReverseAttributeMapping(String netProspexAttribute, EntityMapping toMapping) {
		for (AttributeMapping attributeMapping : toMapping.getAttributes()) {
			if (attributeMapping.getTo().equals(netProspexAttribute)) {
				return attributeMapping;
			}
		}
		return null;
	}

	private List<Double> getRevenueAndEmp(PersonRecord personRecord) {
		double minRevenue = 0;
		double maxRevenue = 0;
		double minEmployeeCount = 0;
		double maxEmployeeCount = 0;
		List<Double> list = new ArrayList<Double>();
		if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange() != null) {
			String[] revenueParts = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange()
					.split(" - ");

			if (revenueParts.length == 2) { // Ex response from Zoominfo:
				minRevenue = getRevenue(revenueParts[0]);
				maxRevenue = getRevenue(revenueParts[1]);

			} else if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange().contains("Over")) {
				String revenuePart = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
				minRevenue = getRevenue(revenuePart);

			} else if (personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange().contains("Under")) {
				String revenuePart = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
				maxRevenue = getRevenue(revenuePart);
			}
		} else {
			minRevenue = 0;
			maxRevenue = 0;
		}
		if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange() != null) {
			String[] employeeCountParts = personRecord.getCurrentEmployment().getCompany()
					.getCompanyEmployeeCountRange().split(" - ");
			if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
				minEmployeeCount = Double.parseDouble(employeeCountParts[0].trim().replace(",", ""));
				maxEmployeeCount = Double.parseDouble(employeeCountParts[1].trim().replace(",", ""));

			} else if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange()
					.contains("Over")) {
				String employeeCountPart = personRecord.getCurrentEmployment().getCompany()
						.getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
				minEmployeeCount = Double.parseDouble(employeeCountPart);

			} else if (personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange()
					.contains("Under")) {
				String employeeCountPart = personRecord.getCurrentEmployment().getCompany()
						.getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
				maxEmployeeCount = Double.parseDouble(employeeCountPart);
			}
		} else {
			minEmployeeCount = 0;
			maxEmployeeCount = 0;
		}
		list.add(minRevenue);
		list.add(maxRevenue);
		list.add(minEmployeeCount);
		list.add(maxEmployeeCount);
		return list;
	}

	private double getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			return (value * million);

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			return (value * billion);
		}
		return 0;
	}

	private String getPersonIDB(String pCampaignId, PersonRecord personRecord) {

		if (personRecord == null)
			return "0";
		String firstName = XtaasUtils.removeNonAsciiChars(personRecord.getFirstName());
		String lastName = XtaasUtils.removeNonAsciiChars(personRecord.getLastName());

		String title = XtaasUtils.removeNonAsciiChars(personRecord.getCurrentEmployment().getJobTitle());
		String company = XtaasUtils
				.removeNonAsciiChars(personRecord.getCurrentEmployment().getCompany().getCompanyName());
		List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findUinqueRecord(pCampaignId, firstName,
				lastName, title, company);
		boolean isSales = false;
		String tit = title.toLowerCase();
		if (tit.contains("sales") || tit.contains("Sales")) {
			isSales = true;
		}

		if ((prospectCallLogList == null || prospectCallLogList.size() == 0) && !isSales) {
			/*
			 * List<ProspectCallLog> cloneProspectCallLogList =
			 * prospectCallLogRepository.findUinqueRecord(cloneCampaignId, firstName,
			 * lastName, title, company); if (cloneProspectCallLogList == null ||
			 * cloneProspectCallLogList.size() == 0) {
			 */
			return "0";
			/*
			 * } else { logger.debug("Person with firstname: " + firstName + ", lastName: "
			 * + lastName + ", company: " + company +
			 * " already purchased in clone. Skipping to next one. "); return true; }
			 */

		} else {
			logger.debug("Person with firstname: " + firstName + ", lastName: " + lastName + ", company: " + company
					+ " already purchased. Skipping to next one. ");
			if (prospectCallLogList.size() > 0) {
				ProspectCallLog pl = prospectCallLogList.get(0);
				String s = pl.getProspectCall().getProspect().getSourceId();
				return s;
			}
			return "peronnid already exist";
		}
	}

	private boolean isPersonRecordAlreadyPurchasedPCL(PersonRecord personRecord, String campaignId) {
		String firstName = personRecord.getFirstName();
		String lastName = personRecord.getLastName();
		String company = personRecord.getCurrentEmployment().getCompany().getCompanyName();
		String title = personRecord.getCurrentEmployment().getJobTitle();
		// List<ProspectCallLog> uniqueRecord =
		// prospectCallLogRepository.findUinqueRecord(campaignId,firstName, lastName,
		// title, company);
		List<String> campaignIds = new ArrayList<String>();
		campaignIds.add(campaignId);
		campaignIds.add(campaignId + NON_CALLABLE);
		// TODO remove after buying ringcentral mm
		// campaignIds.add("5cedb5fee4b0023f192dd97a");
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName, title,
				company);
		// if (uniqueRecord.size() == 0) {
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean isPersonRecordAlreadyPurchased(String pCampaignId, PersonRecord personRecord) {
		if (personRecord == null)
			return false;
		if (personRecord.getPersonId().equalsIgnoreCase("-910826797")) {
			return true;
		}
		String firstName = XtaasUtils.removeNonAsciiChars(personRecord.getFirstName());
		String lastName = XtaasUtils.removeNonAsciiChars(personRecord.getLastName());
		if (firstName.equalsIgnoreCase("sarah")) {
			logger.debug("sarah");
		}
		String title = XtaasUtils.removeNonAsciiChars(personRecord.getCurrentEmployment().getJobTitle());
		String company = XtaasUtils
				.removeNonAsciiChars(personRecord.getCurrentEmployment().getCompany().getCompanyName());
		List<String> campaignIds = new ArrayList<String>();
		campaignIds.add(pCampaignId);
		campaignIds.add(pCampaignId + NON_CALLABLE);
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName, title,
				company);
		if (uniqueRecord == 0) {
			return false;
		} else {
			return false;
		}
	}

	private ZoomInfoResponse getZoomInfoResponse(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		int responseCode = connection.getResponseCode();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputline;
		StringBuffer response = new StringBuffer();
		while ((inputline = bufferedReader.readLine()) != null) {
			response.append(inputline);
		}
		bufferedReader.close();

		String respString = response.toString();
		if (respString != null && respString.length() > 0 && respString.length() <= 30) {
			HttpURLConnection connection1 = (HttpURLConnection) url.openConnection();
			connection1.setRequestMethod("GET");
			int responseCode1 = connection1.getResponseCode();
			response = new StringBuffer();
			BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
			while ((inputline = bufferedReader1.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader1.close();
		}
		/*
		 * if(respString.contains("Company\":\"\"")){
		 * logger.debug("PersonId = "+response); return null; } String ignore =
		 * "currentEmployment\":[\"\","; //String ignorewith = "CurrentEmployment\":[";
		 * if(response.toString().contains(ignore)){ int xindex =
		 * response.indexOf(ignore); String sub = response.substring(xindex); String
		 * sub1 = sub.substring(0, sub.indexOf("]")+1); int startindex =
		 * response.indexOf(sub1); int endindex = startindex+sub1.length();
		 * 
		 * String sub2= sub1.replace("[\"\",", "["); //sub2= sub2.replace("]", "");
		 * 
		 * 
		 * response.replace(startindex, endindex, sub2); }
		 */

		ZoomInfoResponse zoomInfoResponse = getResponse(response);
		return zoomInfoResponse;
	}

	private String getStateCode(PersonDetailResponse personDetailResponse) {
		String stateCode = "CA";
		if (personDetailResponse.getLocalAddress() != null
				&& personDetailResponse.getLocalAddress().getState() != null) {
			String state = personDetailResponse.getLocalAddress().getState();
			stateCode = getState(state);
			// return stateCode;
		}
		if (stateCode == null && personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
						.getState() != null) {
			String state = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
					.getState();
			stateCode = getState(state);
			// return stateCode;
		}
		return stateCode;
	}

	private static String getPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		// String contactReq = properties.getProperty("ContactRequirements");
		// if(!contactReq.equalsIgnoreCase("5") && (phone == null ||
		// "".equalsIgnoreCase(phone))){
		if (personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().size() > 0) {
			if (personDetailResponse.getCurrentEmployment().get(0).getCompany() != null) {
				if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyPhone() != null)
					phone = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyPhone();
				else
					phone = personDetailResponse.getCompanyPhone();
			}
		} else {
			phone = personDetailResponse.getCompanyPhone();
		}
		// }
		if (phone != null) {
			// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d = new Date();
		// System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid + Long.toString(d.getTime());
		return pcidv;
	}

	private ProspectCallLog getPCLObject(OneOffDTO oneOffDTO, ZoomInfoResponse zoomInfoResponseDetail, String title,
			Double minRev, Double maxRev, Double minEmp, Double maxEmp, String searchTitle, String firstName,
			String lastName, String company, String titl) {
		dbrowCount = dbrowCount + 1;
		// titleCounter = titleCounter+1;
		logger.debug("########## dbrowCount #############==" + dbrowCount);
		ProspectCallLog prospectCallLog = new ProspectCallLog();
		/*
		 * List<Double> revenueEmployeeList = findCompanyData(revRange,empRange); Double
		 * minRev = revenueEmployeeList.get(0); Double maxRev =
		 * revenueEmployeeList.get(1); Double minEmp = revenueEmployeeList.get(2);
		 * Double maxEmp = revenueEmployeeList.get(3);
		 */
		CampaignContact campaignContact = createCampaignContact(oneOffDTO.getPrimaryCampaignId(),
				zoomInfoResponseDetail, minRev, maxRev, minEmp, maxEmp, searchTitle, firstName, lastName, company,
				titl);
		List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
		List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();

		Prospect prospect = campaignContact.toProspect(null);
		prospect.setSource(campaignContact.getSource());
		prospect.setSourceId(campaignContact.getSourceId());
		prospect.setCampaignContactId(campaignContact.getId());
		prospect.setCompanyPhone(getCompanyPhone(zoomInfoResponseDetail.getPersonDetailResponse()));
		ProspectCallLog pcLog = new ProspectCallLog();
		ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), oneOffDTO.getPrimaryCampaignId(), prospect);
		prospectCall.setZoomPersonUrl(zoomInfoResponseDetail.getPersonDetailResponse().getZoomPersonUrl());
		prospectCall.setZoomCompanyUrl(zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0)
				.getCompany().getZoomCompanyUrl());
		// prospectCallLog.setProspectCall(new
		// ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));

		// prospectCall.setProspectVersion(contacts.get().getProspectVersion());

		///////////////////////////////////////////////////////////////////

		// TODO 1 below line is commented as the area code and the statecode matching is
		// completely off, 408 area code is pacific
		// but the state is NY , so this needs to be verfied well

		/*
		 * String timezone = getTimeZone(prospectCall.getProspect().getPhone());
		 * 
		 * if(timezone==null) timezone =
		 * properties.getProperty(findAreaCode(prospectCall.getProspect().getPhone()));
		 */

		// if(timezone==null){
		if (prospectCall.getProspect().getStateCode() == null || prospectCall.getProspect().getStateCode().isEmpty()) {
			prospectCall.getProspect().setStateCode(getStateCode(zoomInfoResponseDetail.getPersonDetailResponse()));
		}
		StateCallConfig scc = stateCallConfigRepository.findByStateCode(prospectCall.getProspect().getStateCode());
		prospectCall.getProspect().setTimeZone(scc.getTimezone());

		/*
		 * }else{ prospectCall.getProspect().setTimeZone(timezone); }
		 */

		String tz = getStateWeight(scc.getTimezone());

		/*
		 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
		 */
		StringBuffer sb = new StringBuffer();
		String emp = "";
		String mgmt = "";
		sb.append(prospectCallLog.getStatus());
		sb.append("|");
		if (prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number) {
			Number n1 = (Number) prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount");
			sb.append(getBoundry(n1, "min"));
			sb.append("-");
			emp = getEmployeeWeight(n1);
		}

		if (prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
			Number n2 = (Number) prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount");
			sb.append(getBoundry(n2, "max"));
		}

		/*
		 * sb.append(prospectCall.getProspect().getCustomAttributeValue(
		 * "minEmployeeCount")); sb.append("-");
		 * sb.append(prospectCall.getProspect().getCustomAttributeValue(
		 * "maxEmployeeCount"));
		 */
		sb.append("|");
		sb.append(getMgmtLevel(prospectCall.getProspect().getManagementLevel()));
		mgmt = getMgmtLevelWeight(prospectCall.getProspect().getManagementLevel());

		int qbSort = 0;
		if (!emp.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
			String sortString = tz + mgmt + emp;
			qbSort = Integer.parseInt(sortString);
		}

		///////////////////////////////////////////////////////////////////

		prospectCall.setQualityBucket(sb.toString());
		prospectCall.setQualityBucketSort(qbSort);
		prospectCallLog.setProspectCall(prospectCall);
		prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		prospectCallLog.getProspectCall().setCallRetryCount(886);
		prospectCallLog.setDataSlice("slice0");
		prospectCallLogs.add(prospectCallLog);
		ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
		prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
		prospectCallInteraction.setProspectCall(prospectCall);
		prospectCallInteractions.add(prospectCallInteraction);
		try {
			prospectCallLogRepository.saveAll(prospectCallLogs);
			prospectCallInteractionRepository.saveAll(prospectCallInteractions);
		} catch (DataIntegrityViolationException e) {
			logger.debug("history already exist");
			e.printStackTrace();
		}
		return prospectCallLog;
	}

	private CampaignContact createCampaignContact(String pCampaignId, ZoomInfoResponse zoomInfoResponseDetail,
			Double minRev, Double maxRev, Double minEmp, Double maxEmp, String searchTitle, String firstName,
			String lastName, String company, String titl) {

		List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
		List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
		int industryListSize = 0;
		if (industryList != null) {
			industryListSize = industryList.size();
			for (String industry : industryList) {
				ContactIndustry contactIndustry = new ContactIndustry();
				contactIndustry.setName(XtaasUtils.removeNonAsciiChars(industry));
				industries.add(contactIndustry);
			}
		}
		if (industryListSize == 0) {
			ContactIndustry contactIndustry = new ContactIndustry();
			contactIndustry.setName(XtaasUtils.removeNonAsciiChars("Default Industry"));// Because i didnt find industry
																						// here.
			industries.add(contactIndustry);
		}
		String city = "";
		String country = "";
		String zip = "";
		String street = "";
		if (zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getCity() != null) {
			city = zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getCity();
		} else if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCity() != null) {
			city = zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
					.getCompanyAddress().getCity();

		}

		if (zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getCountry() != null) {
			country = zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getCountry();
		} else if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCountry() != null) {
			country = zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
					.getCompanyAddress().getCountry();

		}

		if (zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getZip() != null) {
			zip = zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getZip();
		} else if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getZip() != null) {
			zip = zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
					.getCompanyAddress().getZip();

		}
		if (zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getStreet() != null) {
			street = zoomInfoResponseDetail.getPersonDetailResponse().getLocalAddress().getStreet();
		} else if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getStreet() != null) {
			street = zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
					.getCompanyAddress().getStreet();

		}

		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getJobTitle() == null) {
			logger.debug("X");
		}
		List<ContactFunction> functions = new ArrayList<ContactFunction>();
		List<String> topLevelIndustries = new ArrayList<String>();
		Map<String, String> companyAddr = new HashMap<String, String>();
		double randomNumber = Math.random();
		String st = getStateCode(zoomInfoResponseDetail.getPersonDetailResponse());
		String mgmt = "Manager";
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getManagementLevel() != null
				&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getManagementLevel()
						.get(0) != null) {
			mgmt = zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getManagementLevel()
					.get(0);
		}
		CampaignContact campaignContact = new CampaignContact(pCampaignId, "ZOOMI_SUCCESS",
				zoomInfoResponseDetail.getPersonDetailResponse().getPersonId(), "", firstName, lastName, searchTitle,
				titl, zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getJobFunction(),
				functions, company,
				zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany()
						.getCompanyWebsite(),
				industries, topLevelIndustries, minRev, maxRev, minEmp, maxEmp,
				zoomInfoResponseDetail.getPersonDetailResponse().getEmailAddress(), street, "", city, st, country, zip,
				getPhone(zoomInfoResponseDetail.getPersonDetailResponse()),
				getPhone(zoomInfoResponseDetail.getPersonDetailResponse()), zoomInfoResponseDetail
						.getPersonDetailResponse().getCurrentEmployment().get(0).getCompany().getCompanyId(),
				mgmt, companyAddr, false);

		CampaignContact c = campaignContactRepository.save(campaignContact);
		return c;
	}

	private static String getCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private boolean validateCompanyData(double minRevenue, double maxRevenue, double minEmployeeCount,
			double maxEmployeeCount, PersonRecord personRecord, int totalResult) {
		// please set company MinRevenue, MaxRevenue, MinEmployeeCount, MaxEmployeeCount
		// if not applicable then don't change
		double searchMinRevenue = 0.0;
		double searchMaxRevenue = 0.0;
		double searchMinEmployeeCount = 0.0;
		double searchMaxEmployeeCount = 0.0;

		boolean conditionCheck = true;
		if (searchMinRevenue != 0 && searchMinRevenue > minRevenue) {
			String convertSearchMinRevenue = String.format("%1$.2f", minRevenue);
			String reason = "Record reject because of MinRevenue. MinRevenue is : " + convertSearchMinRevenue;
			// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason,
			// totalResult);
			logger.debug("Comapany Min Revenue " + convertSearchMinRevenue + " is greater then criteria Min Revenue. "
					+ searchMinRevenue + " Skipping to next one.");
			return false;
		}
		if (searchMaxRevenue != 0 && searchMaxRevenue < maxRevenue) {
			String convertSearchMaxRevenue = String.format("%1$.2f", maxRevenue);
			String reason = "Record reject because of MaxRevenue. MaxRevenue is : " + convertSearchMaxRevenue;
			// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason,
			// totalResult);
			logger.debug("Comapany Max Revenue " + convertSearchMaxRevenue + " is greater then criteria Max Revenue. "
					+ searchMaxRevenue + " Skipping to next one.");
			return false;
		}
		if (searchMinEmployeeCount != 0 && searchMinEmployeeCount > minEmployeeCount) {
			String reason = "Record reject because of MinEmployeeCount. MinEmployeeCount is : " + minEmployeeCount;
			// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason,
			// totalResult);
			logger.debug("Comapany Min Emp " + minEmployeeCount + " is greater then criteria Min Emp. "
					+ searchMinEmployeeCount + " Skipping to next one.");
			return false;
		}
		if (searchMaxEmployeeCount != 0 && searchMaxEmployeeCount < maxEmployeeCount) {
			String reason = "Record reject because of MaxEmployeeCount. MaxEmployeeCount is : " + maxEmployeeCount;
			// createXLForFailedSearchRecord(personRecord, sheetFailedRecord, reason,
			// totalResult);
			logger.debug("Comapany Max Emp " + maxEmployeeCount + " is greater then criteria Max Emp. "
					+ searchMaxEmployeeCount + " Skipping to next one.");
			return false;
		}

		return conditionCheck;
	}

	private boolean validatePersonData(String title) {
		String titleSeniority = null;
		boolean conditionCheck = true;
		if (titleSeniority != null && !titleSeniority.equals(title)) {
			logger.debug(
					"Criteria title is  " + titleSeniority + " is not metch with zoominfo response title  " + title);
			conditionCheck = false;
		}
		return conditionCheck;
	}

	@Override
	public String removeDuplicateProspects(OneOffDTO oneOffDTO) {
		// primaryCampaign = campaignId;
		String error = null;
		List<String> campaignsMap = new ArrayList<String>(); // Digital comm - ABM
		campaignsMap = oneOffDTO.getCampaignIdList(); // Digital comm
//		List<ProspectCallLog> cloneProspectCallLogList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();

		// for(String campaignId : campaignsMap){

		// String campaignId = campaign.getKey();

		// String cloneCampaignId = campaign.getValue();

		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueuedList(campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList0);
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCountList(1,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList1);
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCountList(2,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList2);
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCountList(3,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList3);
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCountList(4,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList4);
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCountList(5,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList5);
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCountList(6,
				campaignsMap);
		prospectCallLogList.addAll(prospectCallLogList6);
		/*
		 * List<ProspectCallLog> prospectCallLogList7 =
		 * prospectCallLogRepository.findContactsByQueued(cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList7); List<ProspectCallLog>
		 * prospectCallLogList8 =
		 * prospectCallLogRepository.findContactsByRetryCount(1,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList8); List<ProspectCallLog>
		 * prospectCallLogList9 =
		 * prospectCallLogRepository.findContactsByRetryCount(2,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList9); List<ProspectCallLog>
		 * prospectCallLogList10 =
		 * prospectCallLogRepository.findContactsByRetryCount(3,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList10); List<ProspectCallLog>
		 * prospectCallLogList11 =
		 * prospectCallLogRepository.findContactsByRetryCount(4,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList11); List<ProspectCallLog>
		 * prospectCallLogList12 =
		 * prospectCallLogRepository.findContactsByRetryCount(5,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList12);
		 */
		/*
		 * List<ProspectCallLog> prospectCallLogList13 =
		 * prospectCallLogRepository.findContactsByRetryCount(6,cloneCampaignId);
		 * cloneProspectCallLogList.addAll(prospectCallLogList13);
		 */

		Map<String, List<ProspectCallLog>> allProspectCallLogMap = new HashMap<String, List<ProspectCallLog>>();
		// Map<String,List<ProspectCallLog>> cloneAllProspectCallLogMap = new
		// HashMap<String, List<ProspectCallLog>>();
		List<String> prospectCallLogIdNotDelete = new ArrayList<String>();
		List<String> prospectCallLogIdToDelete = new ArrayList<String>();
		List<ProspectCallLog> toRemoveProspectCallLog = new ArrayList<ProspectCallLog>();
		Map<String, List<ProspectCallLog>> toKeepProspectLog = new HashMap<String, List<ProspectCallLog>>();
		Map<String, List<ProspectCallLog>> deleteCount = new HashMap<String, List<ProspectCallLog>>();

		try {

			/*
			 * for(ProspectCallLog prospectCallLog : cloneProspectCallLogList){
			 * 
			 * String fname =
			 * prospectCallLog.getProspectCall().getProspect().getFirstName(); String lname
			 * = prospectCallLog.getProspectCall().getProspect().getLastName(); String
			 * company = prospectCallLog.getProspectCall().getProspect().getCompany();
			 * String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
			 * String key = fname+"_"+lname+"_"+company+"_"+phone;
			 * if(cloneAllProspectCallLogMap.containsKey(key)){ List<ProspectCallLog>
			 * pclList = cloneAllProspectCallLogMap.get(key);
			 * 
			 * pclList.add(prospectCallLog); cloneAllProspectCallLogMap.put(key, pclList);
			 * }else{ List<ProspectCallLog> pclList = new ArrayList<ProspectCallLog>();
			 * pclList.add(prospectCallLog); cloneAllProspectCallLogMap.put(key, pclList); }
			 * 
			 * }
			 */

			for (ProspectCallLog prospectCallLog : prospectCallLogList) {

				String fname = prospectCallLog.getProspectCall().getProspect().getFirstName();
				String lname = prospectCallLog.getProspectCall().getProspect().getLastName();
				String company = prospectCallLog.getProspectCall().getProspect().getCompany();
				String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
				String key = fname + "_" + lname + "_" + company + "_" + phone;

				if (allProspectCallLogMap.containsKey(key)) {
					List<ProspectCallLog> pclList = allProspectCallLogMap.get(key);
					pclList.add(prospectCallLog);
					allProspectCallLogMap.put(key, pclList);
				} else {
					List<ProspectCallLog> pclList = new ArrayList<ProspectCallLog>();
					pclList.add(prospectCallLog);
					allProspectCallLogMap.put(key, pclList);
				}

			}

			/*
			 * for (Map.Entry<String, List<ProspectCallLog>> entry :
			 * cloneAllProspectCallLogMap.entrySet()) { String mainkey = entry.getKey();
			 * if(allProspectCallLogMap.containsKey(mainkey)){ List<ProspectCallLog> pcl =
			 * allProspectCallLogMap.get(mainkey); pcl.addAll(entry.getValue());
			 * allProspectCallLogMap.put(mainkey, pcl); }else{
			 * allProspectCallLogMap.put(mainkey, entry.getValue()); } }
			 */

			for (Map.Entry<String, List<ProspectCallLog>> finalEntry : allProspectCallLogMap.entrySet()) {
				List<ProspectCallLog> finalPCList = finalEntry.getValue();
				if (finalPCList != null && finalPCList.size() > 1) {
					for (ProspectCallLog pcll : finalPCList) {
						if (pcll.getProspectCall().getCampaignId().equalsIgnoreCase(oneOffDTO.getPrimaryCampaignId())) {
							String notDelete = pcll.getId() + "," + pcll.getProspectCall().getProspect().getPhone();
							prospectCallLogIdNotDelete.add(notDelete);
							List<ProspectCallLog> keepList = toKeepProspectLog.get(finalEntry.getKey());
							if (keepList != null && keepList.size() > 0) {
								keepList.add(pcll);
							} else {
								keepList = new ArrayList<ProspectCallLog>();
								keepList.add(pcll);
							}
							toKeepProspectLog.put(finalEntry.getKey(), keepList);
						} else {
							String delete = pcll.getId() + "," + pcll.getProspectCall().getProspect().getPhone();
							prospectCallLogIdToDelete.add(delete);
							toRemoveProspectCallLog.add(pcll);
							List<ProspectCallLog> deleteCountList = deleteCount
									.get(pcll.getProspectCall().getCampaignId());
							if (deleteCountList != null && deleteCountList.size() > 0) {
								deleteCountList.add(pcll);
							} else {
								deleteCountList = new ArrayList<ProspectCallLog>();
								deleteCountList.add(pcll);
							}
							deleteCount.put(pcll.getProspectCall().getCampaignId(), deleteCountList);

						}
					}

				}

			}

			for (Map.Entry<String, List<ProspectCallLog>> dupPrimary : toKeepProspectLog.entrySet()) {
				List<ProspectCallLog> finalPCList = dupPrimary.getValue();
				if (finalPCList != null && finalPCList.size() > 1) {
					for (int j = 1; j < finalPCList.size(); j++) {
						/*
						 * String delete =
						 * finalPCList.getId()+","+pcll.getProspectCall().getProspect().getPhone();
						 * prospectCallLogIdToDelete.add(delete);
						 */
						// ProspectCallLog pc = finalPCList[j];
						ProspectCallLog pc = finalPCList.get(j);
						toRemoveProspectCallLog.add(pc);
						List<ProspectCallLog> deleteCountList = deleteCount.get(pc.getProspectCall().getCampaignId());
						if (deleteCountList != null && deleteCountList.size() > 0) {
							deleteCountList.add(pc);
						} else {
							deleteCountList = new ArrayList<ProspectCallLog>();
							deleteCountList.add(pc);
						}
						deleteCount.put(pc.getProspectCall().getCampaignId(), deleteCountList);
					}
				}

			}

			for (Map.Entry<String, List<ProspectCallLog>> delC : deleteCount.entrySet()) {
				logger.debug(delC.getKey() + " : " + delC.getValue().size());
			}

			///////////////////////
			List<ProspectCallLog> delProspectCallLogList = new ArrayList<ProspectCallLog>();
			for (ProspectCallLog pc : toRemoveProspectCallLog) {
				// ProspectCallLog prospectCallLog = list.get(i);
				pc.getProspectCall().setCallRetryCount(111111);
				pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				delProspectCallLogList.add(pc);
			}
			logger.debug("Please wait we are updating Record, It will take some time ......");
			prospectCallLogRepository.saveAll(delProspectCallLogList);
			logger.debug("duplicates found :" + delProspectCallLogList.size());
			////////////////////////

		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		// }
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
					"Remove Duplicate Prospects Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Remove Duplicate Prospects Report Status", error);
		}

		return error;

	}

	public String downloadDataSubProspect(OneOffDTO oneOffDTO) {
		String error = null;
		int i = 0;
		try {
			boolean found = true;

			Date d = new Date();

			String fileName = "subprospectdata" + d.getTime() + ".txt";
//String filePath = "/" + fileName;
			File file = new File(fileName);

//StringWriter writer = new StringWriter();
			FileOutputStream fos = new FileOutputStream(file);

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
//FileWriter writer = new FileWriter(file);
//CSVWriter csvWriter = new CSVWriter(writer, '\t', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER,
// CSVWriter.DEFAULT_LINE_END);
			/*
			 * CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER,
			 * CSVWriter.DEFAULT_LINE_END
			 */
//List<String[]> header = new ArrayList<String[]>();

// adding header record
			String header = "First Name*$Last Name*$Title$Department*$Email$Phone*$Organization Name*"
					+ "$State Code*$Country*$Industry 1$Industry 2$Industry 3$Industry 4$Minimum Revenue"
					+ "$Maximum Revenue$Minimum Employee Count$Maximum Employee Count$Source$ManagementLevel"
					+ "$Address1$Address2$City$Extension$PostalCode$Domain$Direct Phone*$Company Validation Link$Title Validation Link$isEuropean";
//osw.write(header);
			bw.write(header);
			bw.newLine();
			while (found) {
				Pageable pageable = PageRequest.of(i, 5000);
				List<SubSetProspectCallLog> pList = subProspectCallLogRepository.findSubProspects(pageable);
				i++;
				if (pList != null && pList.size() > 0) {
					List<String> dataList = toStringSubProspect(pList);
					for (String data : dataList) {
						bw.write(data);
						bw.newLine();
					}
				} else {
					found = false;
				}
			}
			bw.close();

// csvWriter.writeAll(header);
//csvWriter.close();
//System.out.println(writer);
//byte[] bytes = writer.toString().getBytes("UTF-8");
//String filelocation = downloadRecordingsToAws.createCSVFile("xtaas-excel-transactions", fileName, bytes, true);
			String filelocation = downloadRecordingsToAws.uploadCSVFile("xtaas-excel-transactions", file, fileName,
					fileName);
			System.out.println(filelocation);
			String link = "<a href= " + filelocation + "> Download File here </a>";
			XtaasEmailUtils.sendEmail(oneOffDTO.getEmailIds(), "data@xtaascorp.com",
					"Please find file for subprospectcall", link);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return error;
	}

	private List<String> toStringSubProspect(List<SubSetProspectCallLog> splList) {
		List<String> splStringArr = new ArrayList<String>();
// "First Name*","Last Name*","Title","Department*","Email*","Phone*","Organization Name*","OrganizationId","State Code*",
// "Country*","Industry 1","Industry 2","Industry 3","Industry 4","Minimum Revenue","Maximum Revenue","Minimum Employee Count",
// "Maximum Employee Count","Source","SourceId","ManagementLevel","Address1","Address2","City","PostalCode","Domain",
// "SearchParam","zoomCompanyUrl","zoomPersonUrl" });
		String retStr = "";
		for (SubSetProspectCallLog spl : splList) {
			ProspectCall spc = spl.getProspectCall();
			Prospect spp = spl.getProspectCall().getProspect();
			String department = "";
			String email = "";

			if (spp.getDepartment() != null && !spp.getDepartment().isEmpty()) {
				department = spp.getDepartment();
			} else {
				department = "N/A";
			}

			if (spp.getEmail() != null && !spp.getEmail().isEmpty()) {
				email = spp.getEmail();
			} else {
				email = "N/A";
			}
			String industry1 = "";
			String industry2 = "";
			String industry3 = "";
			String industry4 = "";

			if (spp.getIndustryList() != null) {
//fileWriter.append(COMMA_DELIMITER);
				if (spp.getIndustryList().size() >= 1) {
					industry1 = spp.getIndustryList().get(0);
				}

				if (spp.getIndustryList().size() >= 2) {
					industry2 = spp.getIndustryList().get(1);
				}
				if (spp.getIndustryList().size() >= 3) {
					industry3 = spp.getIndustryList().get(2);
				}
				if (spp.getIndustryList().size() >= 4) {
					industry4 = spp.getIndustryList().get(3);
				}
			} else {
				industry1 = spp.getIndustry();

			}

			splStringArr.add(spp.getFirstName() + "$" + spp.getLastName() + "$" + spp.getTitle() + "$" + department
					+ "$" + email + "$" + spp.getPhone() + "$" + spp.getCompany() + "$" + spp.getStateCode() + "$"
					+ spp.getCountry() + "$" + industry1 + "$" + industry2 + "$" + industry3 + "$" + industry4 + "$"
					+ spp.getCustomAttributeValue("minRevenue") + "" + "$" + spp.getCustomAttributeValue("maxRevenue")
					+ "" + "$" + spp.getCustomAttributeValue("minEmployeeCount") + "" + "$"
					+ spp.getCustomAttributeValue("maxEmployeeCount") + "" + "$" + "XTAASSUB" + "$"
					+ spp.getManagementLevel() + "$" + spp.getAddressLine1() + "$" + spp.getAddressLine2() + "$"
					+ spp.getCity() + "$" + spp.getExtension() + "$" + spp.getZipCode() + "$"
					+ spp.getCustomAttributeValue("domain") + "" + "$" + spp.isDirectPhone() + "" + "$"
					+ spp.getZoomCompanyUrl() + "$" + spp.getZoomPersonUrl() + "$" + spp.getIsEu());
		}
		return splStringArr;
	}

	@Override
	public String movedatatonewsuppression(OneOffDTO oneOffDTO) {
		String error = null;

		try {
			List<SuppressionListNew> newList = new ArrayList<SuppressionListNew>();
			List<String> campaigns = new ArrayList<String>();
			campaigns = oneOffDTO.getCampaignIdList();
			for (String campaignId : campaigns) {
				SuppressionList suppressionList = suppressionListRepository
						.findSupressionListByCampaignIdAndStatus(campaignId);
				if (suppressionList != null) {
					String organizationId = suppressionList.getOrganizationId();
					List<String> companies = suppressionList.getCompanies();
					List<String> emails = suppressionList.getEmailIds();
					List<String> domains = suppressionList.getDomains();
					if (companies != null && companies.size() > 0) {
						for (String company : companies) {
							SuppressionListNew newSup = new SuppressionListNew();
							newSup.setCampaignId(campaignId);
							newSup.setOrganizationId(organizationId);
							newSup.setCompany(company);
							newSup.setStatus("ACTIVE");
							newList.add(newSup);
							if (newList.size() == 100) {
								suppressionListNewRepository.bulkinsert(newList);
								newList = new ArrayList<SuppressionListNew>();
							}
						}
					}
					if (emails != null && emails.size() > 0) {
						for (String email : emails) {
							SuppressionListNew newSup = new SuppressionListNew();
							newSup.setCampaignId(campaignId);
							newSup.setOrganizationId(organizationId);
							newSup.setEmailId(email);
							newSup.setStatus("ACTIVE");
							newList.add(newSup);
							if (newList.size() == 100) {
								suppressionListNewRepository.bulkinsert(newList);
								newList = new ArrayList<SuppressionListNew>();
							}
						}
					}
					if (domains != null && domains.size() > 0) {
						for (String domain : domains) {
							SuppressionListNew newSup = new SuppressionListNew();
							newSup.setCampaignId(campaignId);
							newSup.setOrganizationId(organizationId);
							newSup.setDomains(domain);
							newSup.setStatus("ACTIVE");
							newList.add(newSup);
							if (newList.size() == 100) {
								suppressionListNewRepository.bulkinsert(newList);
								newList = new ArrayList<SuppressionListNew>();
							}
						}
					}
				}

				if (newList != null && newList.size() > 0) {
					suppressionListNewRepository.bulkinsert(newList);
					newList = new ArrayList<SuppressionListNew>();
				}
				XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com",
						"Moved  Data  to new  suppression list for campaingn" + campaignId, campaignId);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return error;
	}

	@Override
	public String removeSpacesFromPhone(OneOffDTO oneOffDTO) {
		String error = null;
		try {
			List<String> campaigns = new ArrayList<String>();
			int ccc = 1;
			campaigns = oneOffDTO.getCampaignIdList();// 11 to 25 -- change campaign id for RemoveSpace program
			for (String campaignID : campaigns) {
				List<ProspectCallLog> pList = prospectCallLogRepository.findAllRecordByCampaignId(campaignID);
				for (ProspectCallLog p : pList) {
					String phone = p.getProspectCall().getProspect().getPhone();
					String ph = phone.replaceAll("\\s+", "");
					/*
					 * if (ph.startsWith("+33") && ph.startsWith("+39") && ph.startsWith("+46") &&
					 * ph.startsWith("+48") && ph.startsWith("+49") && ph.startsWith("+34") &&
					 * ph.startsWith("+44") && p.getProspectCall().getCallRetryCount() != 148) {
					 * 
					 * p.getProspectCall().setCallRetryCount(148);
					 * p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED); logger.debug(ph);
					 * logger.debug(ccc++); }
					 */
					if (ph.contains("-") && p.getProspectCall().getCallRetryCount() != 148) {

						// p.getProspectCall().setCallRetryCount(148);
						// p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						String x = ph.replaceAll("\\-", "");
						ph = x;
						p.getProspectCall().getProspect().setPhone(x);
						logger.debug(ph);
					}

					if (ph.contains("(") && p.getProspectCall().getCallRetryCount() != 148) {

						p.getProspectCall().setCallRetryCount(148);
						p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						logger.debug(ph);
					}

					/*
					 * if((ph.length()<13 || ph.length()>13 ) &&
					 * p.getProspectCall().getCallRetryCount()!=148){
					 * 
					 * p.getProspectCall().setCallRetryCount(148);
					 * p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					 * 
					 * logger.debug(counter++); }
					 */

					p.getProspectCall().getProspect().setPhone(ph);
					prospectCallLogRepository.save(p);
					p.getProspectCall().getProspect().setTimeZone("Europe/London");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Remove Spaces From Phone Report Status - Errors",
					error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Remove Spaces From Phone Report Status", error);
		}

		return error;
	}

//	private String toDate = "09/17/2019 00:00:00";

	@Override
	public String removeSuccessDuplicates(OneOffDTO oneOffDTO) throws ParseException {
		String error = null;
		List<ProspectCallLog> toUpdate = new ArrayList<ProspectCallLog>();
		Map<String, List<ProspectCallLog>> toUpdateMap = new HashMap<String, List<ProspectCallLog>>();
		List<ProspectCallLog> cloneProspectCallLogList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> prospectCallLogSuccessList = new ArrayList<ProspectCallLog>();
		List<String> campaignNames = oneOffDTO.getCampaignIdList();
		// existing campaign id from which you want to get the answer machine status.
		// String srcCampaignId = "58b895c1e4b0eeaa7f1fa0dc";
		// List<ProspectCallLog> prospectCallLogList = null;
		/*
		 * logger.debug("Fetching prospects for main =  =  "); prospectCallLogList =
		 * prospectCallLogRepository.getCallLogRecordsForCampaign(campaignId);
		 * logger.debug("End Fetching prospects for main =  =  ");
		 * logger.debug("Fetching prospects for clone =  =  "); cloneProspectCallLogList
		 * = prospectCallLogRepository.getCallLogRecordsForCampaign(cloneCampaignId);
		 * logger.debug("End Fetching prospects for clone =  =  ");
		 * logger.debug("Processing records =  =  ");
		 */

		Date startDate = XtaasDateUtils.getDate(oneOffDTO.getFromDate(), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
		// logger.debug(startDateInUTC);
		for (String cid : campaignNames) {
			List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(cid);
			prospectCallLogList.addAll(prospectCallLogList0);
			List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1, cid);
			prospectCallLogList.addAll(prospectCallLogList1);
			List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2, cid);
			prospectCallLogList.addAll(prospectCallLogList2);
			List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3, cid);
			prospectCallLogList.addAll(prospectCallLogList3);
			List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4, cid);
			prospectCallLogList.addAll(prospectCallLogList4);
			List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5, cid);
			prospectCallLogList.addAll(prospectCallLogList5);
			List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6, cid);
			prospectCallLogList.addAll(prospectCallLogList6);
			List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7, cid);
			prospectCallLogList.addAll(prospectCallLogList7);
			List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8, cid);
			prospectCallLogList.addAll(prospectCallLogList8);
			List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9, cid);
			prospectCallLogList.addAll(prospectCallLogList9);
			List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10, cid);
			prospectCallLogList.addAll(prospectCallLogList10);

			/*
			 * List<ProspectCallLog> prospectCallLogList7 =
			 * prospectCallLogRepository.findContactsByQueued(cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList7); List<ProspectCallLog>
			 * prospectCallLogList8 =
			 * prospectCallLogRepository.findContactsByRetryCount(1,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList8); List<ProspectCallLog>
			 * prospectCallLogList9 =
			 * prospectCallLogRepository.findContactsByRetryCount(2,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList9); List<ProspectCallLog>
			 * prospectCallLogList10 =
			 * prospectCallLogRepository.findContactsByRetryCount(3,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList10); List<ProspectCallLog>
			 * prospectCallLogList11 =
			 * prospectCallLogRepository.findContactsByRetryCount(4,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList11); List<ProspectCallLog>
			 * prospectCallLogList12 =
			 * prospectCallLogRepository.findContactsByRetryCount(5,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList12); List<ProspectCallLog>
			 * prospectCallLogList13 =
			 * prospectCallLogRepository.findContactsByRetryCount(6,cloneCampaignId);
			 * cloneProspectCallLogList.addAll(prospectCallLogList13);
			 */

			List<ProspectCallLog> prospectCallLogListSUCCESS = prospectCallLogRepository.findContactsBySuccess(cid,
					startDateInUTC);
			prospectCallLogSuccessList.addAll(prospectCallLogListSUCCESS);

			/*
			 * List<ProspectCallLog> prospectCallLogListSUCCESSClone =
			 * prospectCallLogRepository.findContactsBySuccess(cloneCampaignId,
			 * startDateInUTC);
			 * prospectCallLogSuccessList.addAll(prospectCallLogListSUCCESSClone);
			 */
		}

		// FileWriter fileWriter = null;
		Map<String, List<ProspectCallLog>> successProspectCallLogMap = new HashMap<String, List<ProspectCallLog>>();
		Map<String, List<ProspectCallLog>> allProspectCallLogMap = new HashMap<String, List<ProspectCallLog>>();
		Map<String, List<ProspectCallLog>> cloneAllProspectCallLogMap = new HashMap<String, List<ProspectCallLog>>();
		List<String> prospectCallLogIdNotDelete = new ArrayList<String>();
		List<String> prospectCallLogIdToDelete = new ArrayList<String>();
		List<ProspectCallLog> toRemoveProspectCallLog = new ArrayList<ProspectCallLog>();

		try {
			// fileWriter = new FileWriter("duplicateremovefile.txt");
			boolean alreadyExists = false;
			// CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
			int counter = 1;
			for (ProspectCallLog prospectCallLog : cloneProspectCallLogList) {

				String fname = prospectCallLog.getProspectCall().getProspect().getFirstName();
				String lname = prospectCallLog.getProspectCall().getProspect().getLastName();
				String company = prospectCallLog.getProspectCall().getProspect().getCompany();
				String title = prospectCallLog.getProspectCall().getProspect().getTitle();
				String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
				String email = prospectCallLog.getProspectCall().getProspect().getEmail();
				String key = email;
				String key1 = fname + "_" + lname + "_" + phone;
				// String key = fname+"_"+lname+"_"+company+"_"+phone;
				// +"_"+title;
				if (key != null && cloneAllProspectCallLogMap.containsKey(key)) {
					List<ProspectCallLog> pclList = cloneAllProspectCallLogMap.get(key);

					pclList.add(prospectCallLog);
					if (key != null) {
						cloneAllProspectCallLogMap.put(key, pclList);
					} else {
						cloneAllProspectCallLogMap.put(key1, pclList);
					}
				} else {
					List<ProspectCallLog> pclList = new ArrayList<ProspectCallLog>();
					if (key != null) {
						pclList.add(prospectCallLog);
						cloneAllProspectCallLogMap.put(key, pclList);
					} else {
						pclList.add(prospectCallLog);
						cloneAllProspectCallLogMap.put(key1, pclList);
					}
				}

			}

			for (ProspectCallLog prospectCallLog : prospectCallLogList) {
				boolean b = false;
				String fname = prospectCallLog.getProspectCall().getProspect().getFirstName();
				String lname = prospectCallLog.getProspectCall().getProspect().getLastName();
				String company = prospectCallLog.getProspectCall().getProspect().getCompany();
				String title = prospectCallLog.getProspectCall().getProspect().getTitle();
				String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
				String email = prospectCallLog.getProspectCall().getProspect().getEmail();
				String pcid = prospectCallLog.getProspectCall().getProspectCallId();

				String key = email;
				String key1 = fname + "_" + lname + "_" + phone;

				// String key = fname+"_"+lname+"_"+company+"_"+phone;
				// title;
				// List<ProspectCallLog> allProspectCallLogList =
				// prospectCallLogRepository.findUniqueRecordByCFLPC(fname, lname, phone,
				// company);
				if (allProspectCallLogMap.containsKey(key)) {
					List<ProspectCallLog> pclList = allProspectCallLogMap.get(key);
					pclList.add(prospectCallLog);
					if (key != null) {
						allProspectCallLogMap.put(key, pclList);
					} else {
						allProspectCallLogMap.put(key1, pclList);
					}
				} else {
					List<ProspectCallLog> pclList = new ArrayList<ProspectCallLog>();
					pclList.add(prospectCallLog);
					if (key != null) {
						allProspectCallLogMap.put(key, pclList);
					} else {
						allProspectCallLogMap.put(key1, pclList);
					}
				}

			}

			for (Map.Entry<String, List<ProspectCallLog>> entry : cloneAllProspectCallLogMap.entrySet()) {
				String mainkey = entry.getKey();
				if (allProspectCallLogMap.containsKey(mainkey)) {
					List<ProspectCallLog> pcl = allProspectCallLogMap.get(mainkey);
					pcl.addAll(entry.getValue());
					allProspectCallLogMap.put(mainkey, pcl);
				} else {
					allProspectCallLogMap.put(mainkey, entry.getValue());
				}
			}

			for (ProspectCallLog prospectCallLog : prospectCallLogSuccessList) {
				boolean b = false;
				String fname = prospectCallLog.getProspectCall().getProspect().getFirstName();
				String lname = prospectCallLog.getProspectCall().getProspect().getLastName();
				String company = prospectCallLog.getProspectCall().getProspect().getCompany();
				String title = prospectCallLog.getProspectCall().getProspect().getTitle();
				String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
				String email = prospectCallLog.getProspectCall().getProspect().getEmail();
				String key = email;
				String key1 = fname + "_" + lname + "_" + phone;
				// String key = fname+"_"+lname+"_"+company+"_"+phone;
				// title;
				// List<ProspectCallLog> allProspectCallLogList =
				// prospectCallLogRepository.findUniqueRecordByCFLPC(fname, lname, phone,
				// company);
				/*
				 * if(successProspectCallLogMap.containsKey(key)){ List<ProspectCallLog> pclList
				 * = successProspectCallLogMap.get(key); pclList.add(prospectCallLog);
				 * successProspectCallLogMap.put(key, pclList); }else{
				 */
				List<ProspectCallLog> pclList = new ArrayList<ProspectCallLog>();
				pclList.add(prospectCallLog);
				if (key != null) {
					successProspectCallLogMap.put(key, pclList);
				} else {
					successProspectCallLogMap.put(key1, pclList);
				}
				// }

			}

			for (Map.Entry<String, List<ProspectCallLog>> finalEntry : successProspectCallLogMap.entrySet()) {

				String key = finalEntry.getKey();
				List<ProspectCallLog> pcList = allProspectCallLogMap.get(key);
				if (pcList != null && pcList.size() > 0)
					toRemoveProspectCallLog.addAll(pcList);
				/////////////////////
				//////////////////
				///////////////
			}

			// use FileWriter constructor that specifies open for appending

			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists) {
				/*
				 * fileWriter.append("counter"); fileWriter.append("|");
				 */
				/*
				 * fileWriter.append("prospectcalllogid"); fileWriter.append("|");
				 * fileWriter.append("phone");
				 */
				/*
				 * fileWriter.append("|"); fileWriter.append("status2"); fileWriter.append("|");
				 * fileWriter.append("status3"); fileWriter.append("|");
				 * fileWriter.append("status4");
				 */
				/*
				 * fileWriter.append("|"); fileWriter.append("phone"); fileWriter.append("|");
				 * fileWriter.append("title"); fileWriter.append("|");
				 * fileWriter.append("sourceid"); fileWriter.append("|");
				 * fileWriter.append("updateddate"); fileWriter.append("|");
				 * fileWriter.append("status");
				 */
				// fileWriter.append("\n");
				alreadyExists = true;
				// csvOutput.endRecord();
			}
			List<String> toRemovePhone = new ArrayList<String>();
			Map<String, Integer> toRemovePhoneMap = new HashMap<String, Integer>();
			// else assume that the file already has the correct header line
			// for(Map.Entry<String, List<ProspectCallLog>> entry : toUpdateMap.entrySet()){

			///////////////////////
			List<ProspectCallLog> delProspectCallLogList = new ArrayList<ProspectCallLog>();
			for (ProspectCallLog pc : toRemoveProspectCallLog) {
				// ProspectCallLog prospectCallLog = list.get(i);
				pc.getProspectCall().setCallRetryCount(111111);
				pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				delProspectCallLogList.add(pc);
			}
			logger.debug("Please wait we are updating Record, It will take some time ......");
			prospectCallLogRepository.saveAll(delProspectCallLogList);
			logger.debug("duplicates found :" + delProspectCallLogList.size());
			////////////////////////

			for (String str : prospectCallLogIdToDelete) {
				String[] split = str.split(",");
				/*
				 * String phone = entry.getKey(); String toRemovePhonestr = ""; String status1 =
				 * ""; String status2 = ""; String status3 = ""; String status4 = "";
				 */
				/*
				 * List<ProspectCallLog> pclList = entry.getValue(); int counterpcl = 0; int
				 * counterout = 0; for(ProspectCallLog pcl : pclList){ if(counterpcl<=3){ String
				 * subStatus = pcl.getProspectCall().getSubStatus(); if(subStatus != null &&
				 * (subStatus.equalsIgnoreCase("NOTINTERESTED") ||
				 * subStatus.equalsIgnoreCase("NO_CONSENT") ||
				 * subStatus.equalsIgnoreCase("DNCL") || subStatus.equalsIgnoreCase("DNRM") ||
				 * subStatus.equalsIgnoreCase("BUSY") || subStatus.equalsIgnoreCase("DEADAIR")
				 * || subStatus.equalsIgnoreCase("PROSPECT_UNREACHABLE") ||
				 * subStatus.equalsIgnoreCase("NOANSWER") ||
				 * subStatus.equalsIgnoreCase("DIALERMISDETECT") ||
				 * subStatus.equalsIgnoreCase("LOCAL_DNC_ERROR"))){ if(counterpcl == 0){ status1
				 * = subStatus; } if(counterpcl == 1){ status2 = subStatus; } if(counterpcl ==
				 * 2){ status3 = subStatus; } if(counterpcl == 3){ status4 = subStatus; }
				 * if(counterpcl==3 && counterout==0 ){ toRemovePhone.add(phone);
				 * toRemovePhonestr = phone; } counterpcl++; } }else { counterout++; }
				 * 
				 * }
				 */
				/*
				 * if(toRemovePhonestr!=null && !"".equalsIgnoreCase(toRemovePhonestr)){
				 * toRemovePhoneMap.put(toRemovePhonestr, counterpcl+counterout);
				 * fileWriter.append(Integer.toString(counterpcl+counterout));
				 * fileWriter.append("|");
				 */
				// fileWriter.append(split[0]);
				// fileWriter.append("|");
				// fileWriter.append(split[1]);
				/*
				 * fileWriter.append("|"); fileWriter.append(status2); fileWriter.append("|");
				 * fileWriter.append(status3); fileWriter.append("|");
				 * fileWriter.append(status4);
				 */
				// fileWriter.append("\n");

			}
			// write out a few records
			/*
			 * fileWriter.append(Integer.toString(counter)); fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getSubStatus());
			 */
			/*
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getDispositionStatus());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getFirstName());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getLastName());
			 * fileWriter.append("|"); String s =
			 * prospect.getProspectCall().getProspect().getCompany();
			 * fileWriter.append(prospect.getProspectCall().getProspect().getCompany());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getPhone());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getTitle());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getSourceId());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getUpdatedDate().toString());
			 * fileWriter.append("|");
			 * fileWriter.append(prospect.getProspectCall().getProspect().getStatus());
			 */
			// fileWriter.append("\n");
			// csvOutput.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Remove Success Duplicates Report Status - Errors",
					error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Remove Success Duplicates Report Status", error);
		}

		return error;
		// prospectCallLogRepository.save(toUpdate);
		// logger.debug("total uppp= = "+toUpdate.size());
	}

	private String getUSPhone(String phone) {
		// String phone = personDetailResponse.getDirectPhone();
		// if(phone != null){
		// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
		phone = phone.replaceAll("[^A-Za-z0-9]", "");
		if (phone.length() >= 10) {
			phone = phone.substring(0, 10);
		}
		return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		// }
	}

	private String getUSSalutaryPhone(String phone) {
		// String phone = personDetailResponse.getDirectPhone();
		// if(phone != null){
		// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
		phone = phone.replaceAll("[^A-Za-z0-9]", "");
		if (phone.length() == 10) {
			phone = phone.substring(0, 10);
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		}
		return "INVALID_PHONE";
		// }
	}

	public String getTimeZone(String state) {
		// List<OutboundNumber> outboundNumberList;
		// int areaCode = Integer.parseInt(findAreaCode(prospect.getPhone()));
		// if(prospect!=null && prospect.getStateCode()!=null){

		StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(state);
		return stateCallConfig.getTimezone();
		// }

		// if(stateCallConfig==null){
		// return null;
		// }

		// find a random number from the list

	}

	private Map<String, String[]> getEmployeeMap() {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		/////
		String[] empArray1 = new String[2];
		empArray1[0] = "100";
		empArray1[1] = "500";
		empMap.put("100 to 499", empArray1);

		String[] empArray2 = new String[2];
		empArray2[0] = "5000";
		empArray2[1] = "10000";
		empMap.put("5000+", empArray2);

		String[] empArray3 = new String[2];
		empArray3[0] = "20";
		empArray3[1] = "100";
		empMap.put("20 to 99", empArray3);

		String[] empArray4 = new String[2];
		empArray4[0] = "1000";
		empArray4[1] = "5000";
		empMap.put("1000 to 4999", empArray4);

		String[] empArray5 = new String[2];
		empArray5[0] = "1";
		empArray5[1] = "20";
		empMap.put("1 to 19", empArray5);

		String[] empArray6 = new String[2];
		empArray6[0] = "400";
		empArray6[1] = "1000";
		empMap.put("500 to 999", empArray6);

		String[] empArray7 = new String[2];
		empArray7[0] = "1000";
		empArray7[1] = "5000";
		empMap.put("1000 To 4999", empArray7);

		return empMap;
	}

	private Map<String, String[]> getRevenueMap() {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		/////
		String[] empArray1 = new String[2];
		empArray1[0] = "10000000";
		empArray1[1] = "50000000";
		empMap.put("$10M to <$50M", empArray1);

		String[] empArray2 = new String[2];
		empArray2[0] = "1000000000";
		empArray2[1] = "5000000000";
		empMap.put("$1B+", empArray2);

		String[] empArray3 = new String[2];
		empArray3[0] = "100000000";
		empArray3[1] = "1000000000";
		empMap.put("$100M to <$1B", empArray3);

		String[] empArray4 = new String[2];
		empArray4[0] = "50000000";
		empArray4[1] = "100000000";
		empMap.put("$50M to <$100M", empArray4);

		String[] empArray5 = new String[2];
		empArray5[0] = "1000000";
		empArray5[1] = "5000000";
		empMap.put("<$1M", empArray5);

		String[] empArray6 = new String[2];
		empArray6[0] = "1000000";
		empArray6[1] = "1000000";
		empMap.put("$1M to <$10M", empArray6);

		return empMap;
	}

	@Override()
	public String updateMissingRecording(String days) {
		int numOfDays = Integer.parseInt(days);
		telnyxMissingRecordingsJob.getForPostMan(numOfDays);
		return "success";

	}

	@Override()
	public String moveSalutaryData(String counter) {
		int pcounter = Integer.parseInt(counter);
		long scount = salatuaryRepository.count();
		long batchSize = 0;
		if (scount > 0) {
			batchSize = scount / 10000;
			long abatch = batchSize * 10000;
			if (abatch < scount) {
				batchSize = batchSize + 1;
			}
			logger.info("batch Size salutary : " + batchSize);

		}
		for (int k = pcounter; k < batchSize; k++) {
			logger.info("batch number salutary : " + k);
			Pageable pageable = PageRequest.of(k, 10000);
			List<Salatuary> sList = salatuaryRepository.getdata(pageable);
			for (Salatuary s : sList) {
				try {
					ProspectSalutary ps = new ProspectSalutary();
					Prospect p = new Prospect();
					p.setFirstName(s.getFirstName());
					p.setLastName(s.getLastName());
					if (s.getAddress1_state() != null && !s.getAddress1_state().isEmpty()) {
						p.setStateCode(s.getAddress1_state());
						p.setCity(s.getAddress1_city());
						p.setAddressLine1(s.getAddress1_line1());
						p.setZipCode(s.getAddress1_postal());
						p.setTimeZone(getTimeZone(s.getAddress1_state()));
					} else if (s.getAddress2_state() != null && !s.getAddress2_state().isEmpty()) {
						p.setStateCode(s.getAddress2_state());
						p.setCity(s.getAddress2_city());
						p.setAddressLine1(s.getAddress2_line1());
						p.setZipCode(s.getAddress2_postal());
						p.setTimeZone(getTimeZone(s.getAddress2_state()));
					} else {
						p.setStateCode("N/A");
						p.setCity(s.getAddress1_city());
						p.setAddressLine1(s.getAddress1_line1());
						p.setZipCode(s.getAddress1_postal());
						p.setTimeZone("N/A");
					}
					p.setSource("SALUTORY");
					p.setDataSource("Xtaas Data Source");
					p.setSourceId(UUID.randomUUID().toString());
					p.setCompany(s.getOrgName());
					p.setTitle(s.getJobTitle());
					p.setDepartment(s.getJobFunction());
					String phn = "INVALID_PHONE";
					String phnReason = "INVALID_PHONE";

					if (s.getPhone1() != null && !s.getPhone1().isEmpty()) {
						phn = getUSSalutaryPhone(s.getPhone1());
						if (!phn.equalsIgnoreCase("INVALID_PHONE")) {
							p.setPhone(phn);
							phnReason = "VALID";
						} else if (s.getPhone2() != null && !s.getPhone2().isEmpty()) {
							phn = getUSSalutaryPhone(s.getPhone2());
							if (!phn.equalsIgnoreCase("INVALID_PHONE")) {
								p.setPhone(phn);
								phnReason = "VALID";
							}
						} else {
							p.setPhone(s.getPhone1());
						}
					} else if (s.getPhone2() != null && !s.getPhone2().isEmpty()) {
						phn = getUSSalutaryPhone(s.getPhone2());
						if (!phn.equalsIgnoreCase("INVALID_PHONE")) {
							p.setPhone(phn);
						} else {
							p.setPhone(s.getPhone1());
						}
					} else {
						p.setPhone(s.getPhone1());
						phnReason = "NO PHONE NUMBER";
					}

					if (s.getPhone2() != null && !s.getPhone2().isEmpty()) {
						String cphn = getUSSalutaryPhone(s.getPhone2());
						if (!cphn.equalsIgnoreCase("INVALID_PHONE")) {
							p.setCompanyPhone(cphn);
						} else {
							p.setCompanyPhone(s.getPhone2());
						}
					}

					Map<String, Object> customAttributes = new HashMap<String, Object>();
					if (s.getRevenueRange() != null && !s.getRevenueRange().isEmpty()) {
						Map<String, String[]> revMap = getRevenueMap();
						String[] revArr = revMap.get(s.getRevenueRange());
						customAttributes.put("minRevenue", new Double(Integer.parseInt(revArr[0])));
						customAttributes.put("maxRevenue", new Double(revArr[1]));
					}
					if (s.getEmployeeCountRange() != null && !s.getEmployeeCountRange().isEmpty()) {
						Map<String, String[]> empMap = getEmployeeMap();
						String[] empArr = empMap.get(s.getEmployeeCountRange());

						customAttributes.put("maxEmployeeCount", new Double(Integer.parseInt(empArr[1])));
						customAttributes.put("minEmployeeCount", new Double(Integer.parseInt(empArr[0])));
					}

					if (s.getOrgDomain() != null && !s.getOrgDomain().isEmpty())
						customAttributes.put("domain", s.getOrgDomain());
					else
						customAttributes.put("domain", s.getEmailDomain());

					p.setCustomAttributes(customAttributes);
					p.setSourceCompanyId(s.getId());
					p.setManagementLevel(s.getJobLevel());

					p.setZoomPersonUrl(s.getLinkedIn_url());
					p.setSourceType("INTERNAL");
					List<String> sicList = new ArrayList<String>(1);
					sicList.add(s.getSicCode());
					p.setCompanySIC(sicList);
					List<String> naicsList = new ArrayList<String>(1);
					naicsList.add(s.getNAICS());
					p.setCompanyNAICS(naicsList);

					if (s.getSicDescription() != null && !s.getSicDescription().isEmpty())
						p.setIndustry(s.getSicDescription());
					else if (s.getNAICS_description() != null && !s.getNAICS_description().isEmpty())
						p.setIndustry(s.getNAICS_description());
					p.setEmail(s.getEmailAddres());
					p.setCountry("United States");

					ProspectCall pc = new ProspectCall(p.getSourceId(), "SALUTARY_DATA", p);
					ps.setDataSlice("slice10");
					pc.setCampaignId("SALUTARY_DATA");
					pc.setDataSlice("slice10");
					pc.setCallRetryCount(786);
					pc.setCallDuration(0);
					pc.setOrganizationId("XTAAS CALL CENTER");

					ps.setStatus(com.xtaas.db.entity.ProspectSalutary.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					ps.setProspectCall(pc);
					if (phn.equalsIgnoreCase("INVALID_PHONE")) {
						ProspectSalutaryInvalid psi = new ProspectSalutaryInvalid();
						psi.setProspectCall(ps.getProspectCall());
						psi.getProspectCall().setProspect(ps.getProspectCall().getProspect());
						psi.setDataSlice(ps.getDataSlice());
						psi.setReason(phnReason);
						psi.setStatus(
								com.xtaas.db.entity.ProspectSalutaryInvalid.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						salutaryInvalidRepository.save(psi);
					} else {
						try {
							prospectSalutaryRepository.save(ps);
						} catch (Exception ex) {
							ProspectSalutaryInvalid psi = new ProspectSalutaryInvalid();
							psi.setProspectCall(ps.getProspectCall());
							psi.getProspectCall().setProspect(ps.getProspectCall().getProspect());
							psi.setDataSlice(ps.getDataSlice());
							psi.setReason("Duplicate");
							psi.setStatus(
									com.xtaas.db.entity.ProspectSalutaryInvalid.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							salutaryInvalidRepository.save(psi);
							// ex.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return "Success";

	}

	private int allowDuplicates = 4;
	private int allowDirectDuplicates = 40;// This values should be 4 for multiprospect campaigns

	@Override
	public String getTenProspectsForACompany(OneOffDTO oneOffDto) {
		List<String> campaignids = new ArrayList<String>();
		String error = null;
		try {

			campaignids = oneOffDto.getCampaignIdList();
			//
			int counterx = 0;
			// List<String> companies =
			// prospectCallLogRepositoryImpl.findDistinctCompaniesName(campaignids);
			for (String cid : campaignids) {
				int repcountner = 0;
				List<ProspectCallLog> repProspectCallLogList = prospectCallLogRepository.findReplinishData(cid);
				for (ProspectCallLog rpc : repProspectCallLogList) {
					rpc.getProspectCall().setCallRetryCount(1);
					rpc.setStatus(ProspectCallStatus.CALL_BUSY);
					rpc.getProspectCall().setSubStatus("BUSY");
					rpc.getProspectCall().setDispositionStatus("DIALERCODE");
					// logger.debug("state code is null");
					prospectCallLogRepository.save(rpc);
					repcountner++;
				}
				logger.debug("total data replenish before tencompanies : " + repcountner);

				List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsTenByQueued(cid);
				List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsTenByRetryCount(1,
						cid);
				List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsTenByRetryCount(2,
						cid);
				List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsTenByRetryCount(3,
						cid);
				List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsTenByRetryCount(4,
						cid);
				List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsTenByRetryCount(5,
						cid);
				List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsTenByRetryCount(6,
						cid);
				List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsTenByRetryCount(7,
						cid);
				List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsTenByRetryCount(8,
						cid);
				List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsTenByRetryCount(9,
						cid);
				List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsTenByRetryCount(10,
						cid);
				List<ProspectCallLog> prospectCallLogList786 = prospectCallLogRepository
						.findContactsTenByRetryCount(786, cid);
				List<ProspectCallLog> prospectCallLogList886 = prospectCallLogRepository
						.findContactsTenByRetryCount(886, cid);
				List<ProspectCallLog> prospectCallLogList444444 = prospectCallLogRepository
						.findContactsTenByRetryCount(444444, cid);
				// List<ProspectCallLog> prospectCallLogList11 =
				// prospectCallLogRepository.findContactsTenByRetryCount(222222,cid);

				List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
				prospectCallLogList.addAll(prospectCallLogList0);
				prospectCallLogList.addAll(prospectCallLogList1);
				prospectCallLogList.addAll(prospectCallLogList2);
				prospectCallLogList.addAll(prospectCallLogList3);
				prospectCallLogList.addAll(prospectCallLogList4);
				prospectCallLogList.addAll(prospectCallLogList5);
				prospectCallLogList.addAll(prospectCallLogList6);
				prospectCallLogList.addAll(prospectCallLogList7);
				prospectCallLogList.addAll(prospectCallLogList8);
				prospectCallLogList.addAll(prospectCallLogList9);
				prospectCallLogList.addAll(prospectCallLogList10);
				prospectCallLogList.addAll(prospectCallLogList786);
				prospectCallLogList.addAll(prospectCallLogList886);
				prospectCallLogList.addAll(prospectCallLogList444444);
				// prospectCallLogList.addAll(prospectCallLogList11);
				Map<String, List<ProspectCallLog>> prospectCallMap = new java.util.HashMap<String, List<ProspectCallLog>>();
				Map<String, Map<String, List<ProspectCallLog>>> prospectCallCompanyMap = new java.util.HashMap<String, Map<String, List<ProspectCallLog>>>();

				// logger.debug(prospectCallLogList.size());
				int updatecounter = 0;
				for (ProspectCallLog pc : prospectCallLogList) {
					// for(String comp : companies){
					// if(pc.getProspectCall().getProspect().getCompany().equalsIgnoreCase(comp)){
					String key = pc.getProspectCall().getProspect().getPhone();
					if (key == null)
						logger.debug("Here");
					List<ProspectCallLog> prospectl = prospectCallMap.get(key);
					if (prospectl != null && prospectl.size() > 0) {
						ProspectCallLog ep = prospectl.get(0);
						if (ep.getProspectCall().getProspect().getStateCode() != null
								&& !ep.getProspectCall().getProspect().getStateCode().isEmpty()) {

							if (pc.getProspectCall().getProspect().getStateCode() != null
									&& !pc.getProspectCall().getProspect().getStateCode().isEmpty()
									&& ep.getProspectCall().getProspect().getStateCode()
											.equalsIgnoreCase(pc.getProspectCall().getProspect().getStateCode())) {
								prospectl.add(pc);
							} else {
								if (pc.getProspectCall().getProspect().getStateCode() != null
										&& !pc.getProspectCall().getProspect().getStateCode().isEmpty()) {
									pc.getProspectCall().getProspect()
											.setStateCode(ep.getProspectCall().getProspect().getStateCode());
									prospectl.add(pc);
								} else {
									pc.getProspectCall().setCallRetryCount(5005);
									pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
									// logger.debug(updatecounter++);
									if (pc.getDataSlice() != null && pc.getDataSlice().equalsIgnoreCase("slice0")) {
										// DOnothing
									} else {
										prospectCallLogRepository.save(pc);
									}
									updatecounter++;

								}

							}
						} else {
							ep.getProspectCall().setCallRetryCount(5005);
							ep.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							// logger.debug(updatecounter++);
							prospectCallLogRepository.save(ep);
							updatecounter++;

						}

						prospectCallMap.put(key, prospectl);
						Map<String, List<ProspectCallLog>> pclcompMap = prospectCallCompanyMap
								.get(pc.getProspectCall().getProspect().getSourceCompanyId());
						if (pclcompMap == null) {
							pclcompMap = new java.util.HashMap<String, List<ProspectCallLog>>();
							pclcompMap.put(key, prospectl);
							prospectCallCompanyMap.put(pc.getProspectCall().getProspect().getSourceCompanyId(),
									pclcompMap);
						} else {
							pclcompMap.put(key, prospectl);
							prospectCallCompanyMap.put(pc.getProspectCall().getProspect().getSourceCompanyId(),
									pclcompMap);
						}
					} else {
						// logger.debug("-->"+pc.getProspectCall().getProspectCallId());
						/*
						 * if(pc.getProspectCall().getProspect().getStateCode()==null){
						 * logger.debug("HERE"); }
						 */
						if (pc.getProspectCall().getProspect().getStateCode() != null
								&& !pc.getProspectCall().getProspect().getStateCode().isEmpty()) {
							prospectl = new ArrayList<ProspectCallLog>();
							prospectl.add(pc);
							prospectCallMap.put(key, prospectl);
							Map<String, List<ProspectCallLog>> pclcompMap = prospectCallCompanyMap
									.get(pc.getProspectCall().getProspect().getSourceCompanyId());
							if (pclcompMap == null) {
								pclcompMap = new java.util.HashMap<String, List<ProspectCallLog>>();
								pclcompMap.put(key, prospectl);
								prospectCallCompanyMap.put(pc.getProspectCall().getProspect().getSourceCompanyId(),
										pclcompMap);
							} else {
								pclcompMap.put(key, prospectl);
								prospectCallCompanyMap.put(pc.getProspectCall().getProspect().getSourceCompanyId(),
										pclcompMap);
							}
						} else {
							pc.getProspectCall().setCallRetryCount(5005);
							pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							// logger.debug(updatecounter++);
							prospectCallLogRepository.save(pc);
						}

					}
					// }
					// }
				}
				List<ProspectCallLog> toRemove = new ArrayList<ProspectCallLog>();
				// List<ProspectCallLog> toRemoveDirect = new ArrayList<ProspectCallLog>();
				// below for direct numbers
				for (Map.Entry<String, Map<String, List<ProspectCallLog>>> entry : prospectCallCompanyMap.entrySet()) {
					Map<String, List<ProspectCallLog>> plMap = entry.getValue();
					int countera = 1;
					for (Map.Entry<String, List<ProspectCallLog>> entry1 : plMap.entrySet()) {
						List<ProspectCallLog> plList = entry1.getValue();
						if (entry1.getValue().size() == 1) {
							for (ProspectCallLog p : plList) {
								if (countera > allowDirectDuplicates) {
									// logger.debug(entry.getValue().size());
									toRemove.add(p);
								}
								countera++;
							}

						}
					}
				}

				///////// Below for indirect numbers
				for (Map.Entry<String, List<ProspectCallLog>> entry : prospectCallMap.entrySet()) {
					List<ProspectCallLog> pl = entry.getValue();
					if (entry.getKey().equalsIgnoreCase("979-845-7800"))
						logger.debug("Here");
					if (entry.getValue().size() > allowDuplicates) {
						int counter = 1;
						for (ProspectCallLog p : pl) {
							if (counter > allowDuplicates) {
								// logger.debug(entry.getValue().size());
								toRemove.add(p);
							}
							counter++;
						}

					}
				}
				List<ProspectCallLog> upDateList = new ArrayList<ProspectCallLog>();
				for (ProspectCallLog removeP : toRemove) {
					removeP.getProspectCall().setCallRetryCount(222222);
					removeP.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					upDateList.add(removeP);
					updatecounter++;
				}

				logger.debug("Please wait we are updating Record, It will take some time ......");
				prospectCallLogRepository.saveAll(upDateList);
				logger.debug("Total record  updated - " + updatecounter);
				// totalCount = prospectCallLogList.size() + totalCount;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}

		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Ten Prospects For Company Report Status - Errors",
					error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Ten Prospects For Company Report Status", error);
		}

		return error;
	}

	@Override
	public List<ProspectCallLog> readBooksFromCSV(String campaignId, MultipartFile file1) {
		List<ProspectCallLog> prospectList = new ArrayList<ProspectCallLog>();
		String error = null;
		int count = 0;
		try {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
					// add("application/octet-stream"); // HACK: added to upload files.
				}
			};
			if (excelSupportedContentTypes.contains(file1.getContentType())) {

			}
			////////////////////////////////////////
			FileInputStream file = new FileInputStream(convertMultiPartToFile(file1));
			int cellcounter = 0;
			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				String[] metaData = new String[100];
				Row row = rowIterator.next();
				if (count == 0) {
					count++;
					continue;
				}
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();

				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					// Check the cell type and format accordingly

					if (cell.getCellType() == CellType.STRING) {
						System.out.print(cell.getStringCellValue() + "t");
						metaData[cell.getColumnIndex()] = cell.getStringCellValue().toString();
					} else if (cell.getCellType() == CellType.NUMERIC) {
						System.out.print(cell.getNumericCellValue() + "t");
						metaData[cell.getColumnIndex()] = new Double(cell.getNumericCellValue()).toString();
					}
//					switch (cell.getCellType()) {
//
//					case Cell.CELL_TYPE_NUMERIC:
//						System.out.print(cell.getNumericCellValue() + "t");
//						metaData[cell.getColumnIndex()] = new Double(cell.getNumericCellValue()).toString();
//						break;
//					case Cell.CELL_TYPE_STRING:
//						System.out.print(cell.getStringCellValue() + "t");
//						metaData[cell.getColumnIndex()] = cell.getStringCellValue().toString();
//						break;
//
//					}

				}
				///////////////////////////////////////////
				ProspectCallLog prospect = createProspect(campaignId, metaData);
				if (!isPersonRecordAlreadyPurchasedEv(campaignId,
						prospect.getProspectCall().getProspect().getFirstName(),
						prospect.getProspectCall().getProspect().getLastName(),
						prospect.getProspectCall().getProspect().getTitle(),
						prospect.getProspectCall().getProspect().getCompany())) {

					ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
					prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
					prospectCallInteraction.setProspectCall(prospect.getProspectCall());
					prospect.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					prospect.getProspectCall().setCallRetryCount(786);
					try {
						prospectCallLogRepository.save(prospect);
						prospectCallInteractionRepository.save(prospectCallInteraction);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

				} else {
					logger.debug("Already Purchased");
				}
				// adding book into ArrayList
				prospectList.add(prospect);

				// read next line before looping
				// if end of file reached, line would be null
				// line = br.readLine();
				/////////////////////////////////
				logger.debug("");
			}
			file.close();
			//////////////////////////////////////
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Read Everstring Data Report Status - Errors",
					error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Read Everstring Data Report Status", error);
		}
		// create an instance of BufferedReader
		// using try with resource, Java 7 feature to close resources
		/*
		 * try (BufferedReader br = Files.newBufferedReader(pathToFile,
		 * StandardCharsets.US_ASCII)) {
		 * 
		 * // read the first line from the text file String line = br.readLine();
		 * 
		 * // loop until all lines are read while (line != null) { if(count==0){
		 * count++; line = br.readLine(); continue; } if(count == 166)
		 * logger.debug("HI"); count++; logger.debug(line); // use string.split to load
		 * a string array with the values from // each line of // the file, using a
		 * comma as the delimiter String[] attributes = line.split("\\|");
		 * 
		 * ProspectCallLog prospect = createProspect(attributes); if(
		 * isPersonRecordAlreadyPurchasedEv(prospect.getProspectCall().getProspect().
		 * getFirstName() ,prospect.getProspectCall().getProspect().getLastName() ,
		 * prospect.getProspectCall().getProspect().getTitle() ,
		 * prospect.getProspectCall().getProspect().getCompany() )){
		 * 
		 * ProspectCallInteraction prospectCallInteraction = new
		 * ProspectCallInteraction();
		 * prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
		 * prospectCallInteraction.setProspectCall(prospect.getProspectCall());
		 * prospect.setStatus(ProspectCallStatus.QUEUED);
		 * prospectCallLogRepository.save(prospect);
		 * prospectCallInteractionRepository.save(prospectCallInteraction);
		 * 
		 * }else{ logger.debug("Already Purchased"); } // adding book into ArrayList
		 * prospectList.add(prospect);
		 * 
		 * // read next line before looping // if end of file reached, line would be
		 * null line = br.readLine(); }
		 * 
		 * } catch (IOException ioe) { ioe.printStackTrace(); }
		 */

		return prospectList;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private ProspectCallLog createProspect(String campaignId, String[] metadata) {
		double minEmp = 0;
		double maxEmp = 0;
		String firstname = metadata[0];
		String lastname = metadata[1];
		String email = metadata[2];
		String companyName = metadata[3];
		String domain = metadata[4];
		String phone = metadata[5];
		String mgmtlevel = metadata[6];
		String personurl = metadata[7];
		String department = metadata[8];
		String title = metadata[9];
		String industry = metadata[10];
		List<String> industryList = new ArrayList<String>();
		industryList.add(industry);
		String empString = metadata[11];
		if (empString != null && !empString.isEmpty()) {
			empString = empString.replace("'", "");

			if (empString.equalsIgnoreCase("0-5")) {
				minEmp = 0;
				maxEmp = 5;
			} else if (empString.equalsIgnoreCase("6-10")) {
				minEmp = 5;
				maxEmp = 10;
			} else if (empString.equalsIgnoreCase("11-20")) {
				minEmp = 10;
				maxEmp = 19;
			} else if (empString.equalsIgnoreCase("21-50")) {
				minEmp = 20;
				maxEmp = 50;
			} else if (empString.equalsIgnoreCase("51-100")) {
				minEmp = 50;
				maxEmp = 100;
			} else if (empString.equalsIgnoreCase("101-200")) {
				minEmp = 100;
				maxEmp = 250;
			} else if (empString.equalsIgnoreCase("201-500")) {
				minEmp = 250;
				maxEmp = 500;
			} else if (empString.equalsIgnoreCase("5,001,000")) {
				minEmp = 500;
				maxEmp = 1000;
			} else if (empString.equalsIgnoreCase("1,001-2,000")) {
				minEmp = 1000;
				maxEmp = 5000;
			} else if (empString.equalsIgnoreCase("2,001-5,000")) {
				minEmp = 1000;
				maxEmp = 5000;
			} else if (empString.equalsIgnoreCase("5,001-10,000")) {
				minEmp = 5000;
				maxEmp = 10000;
			} else if (empString.equalsIgnoreCase("10,000+")) {
				minEmp = 10000;
				maxEmp = 0;
			}
		}

		String revString = metadata[12];

		Double minrev = 0.0;
		Double maxrev = 0.0;

		if (revString != null && !revString.isEmpty()) {
			revString = revString.replace("'", "");

			if (revString.equalsIgnoreCase("$10M-$25M")) {
				minrev = 10000000.0;
				maxrev = 25000000.0;
			} else if (revString.equalsIgnoreCase("$25M-$50M")) {
				minrev = 25000000.0;
				maxrev = 50000000.0;
			} else if (revString.equalsIgnoreCase("$50M-$100M")) {
				minrev = 50000000.0;
				maxrev = 100000000.0;
			} else if (revString.equalsIgnoreCase("$100M-$250M")) {
				minrev = 100000000.0;
				maxrev = 250000000.0;
			} else if (revString.equalsIgnoreCase("$250M-$500M")) {
				minrev = 250000000.0;
				maxrev = 500000000.0;
			} else if (revString.equalsIgnoreCase("$500M-$1B")) {
				minrev = 500000000.0;
				maxrev = 1000000000.0;
			} else if (revString.equalsIgnoreCase("$1B-$5B")) {
				minrev = 1000000000.0;
				maxrev = 5000000000.0;
			} else if (revString.equalsIgnoreCase("$5B+")) {
				minrev = 5000000000.0;
				maxrev = 0.0;
			}
		}
		String street = metadata[13];
		String city = metadata[14];
		String state = metadata[15];
		String country = metadata[16];
		String zip = metadata[17];
		String companyPhone = metadata[18];
		String companyUrl = metadata[19];

		ProspectCallLog pclog = new ProspectCallLog();
		Prospect prospect = new Prospect();
		prospect.setFirstName(firstname);
		prospect.setLastName(lastname);
		prospect.setEmail(email);
		prospect.setCompany(companyName);
		if (phone != null && !phone.isEmpty()) {
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() > 10) {
				phone = phone.substring(1, 11);
			} else if (phone.length() == 10) {
				phone = phone;

			}
			phone = String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		}

		prospect.setPhone(phone);
		prospect.setAddressLine1(street);
		prospect.setCity(city);

		if (companyPhone != null && !companyPhone.isEmpty()) {
			companyPhone = companyPhone.replaceAll("[^A-Za-z0-9]", "");
			if (companyPhone.length() > 10) {
				companyPhone = companyPhone.substring(1, 11);
			} else if (companyPhone.length() == 10) {
				companyPhone = companyPhone;
			}
			companyPhone = String.valueOf(companyPhone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		}
		prospect.setCompanyPhone(companyPhone);
		prospect.setCountry(country);// TODO country hardcoded
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		customAttributes.put("minRevenue", minrev);
		customAttributes.put("maxRevenue", maxrev);

		customAttributes.put("maxEmployeeCount", maxEmp);
		customAttributes.put("minEmployeeCount", minEmp);
		customAttributes.put("domain", domain);
		prospect.setCustomAttributes(customAttributes);
		prospect.setDepartment(department);
		prospect.setDirectPhone(true);
		prospect.setEu(false);
		prospect.setIndustry(industry);
		prospect.setIndustryList(industryList);
		if (mgmtlevel.equalsIgnoreCase("Vice President")) {
			mgmtlevel = "VP-Level";
		}
		prospect.setManagementLevel(mgmtlevel);
		prospect.setSource("EverString");
		prospect.setSourceId(UUID.randomUUID().toString());
		// prospect.setStatus("QUEUED");
		String stateCode = getState(state);
		if (stateCode == null)
			stateCode = "CA";
		prospect.setStateCode(stateCode);
		StateCallConfig scc = stateCallConfigRepository.findByStateCode(stateCode);

		prospect.setTimeZone(scc.getTimezone());
		prospect.setTitle(title);
		prospect.setZipCode(zip);

		ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaignId, prospect);
		prospectCall.setZoomCompanyUrl(companyUrl);
		prospectCall.setZoomPersonUrl(personurl);
		pclog.setProspectCall(prospectCall);

		// create and return book of this metadata
		return pclog;
	}

	private boolean isPersonRecordAlreadyPurchasedEv(String campaignId, String fn, String ln, String tit, String com) {
		String firstName = "fn";
		String lastName = "ln";
		String title = "tit";
		String company = "com";
		// }
		if (fn != null && ln != null && tit != null && com != null) {
			firstName = XtaasUtils.removeNonAsciiChars(fn);
			lastName = XtaasUtils.removeNonAsciiChars(ln);

			title = XtaasUtils.removeNonAsciiChars(tit);
			company = XtaasUtils.removeNonAsciiChars(com);
		} else {
			return true;
		}
		List<String> campaignIds = new ArrayList<String>();
		campaignIds.add(campaignId);
		campaignIds.add(campaignId + NON_CALLABLE);
		// campaignIds.add("3080v");

		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName, title,
				company);
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;

		}
		// return true;
	}

	public String getBackCallabes(OneOffDTO oneOffDTO) {
		String NON_CALLABLE = "_NON_CALLABLE";

		String returnmessage = "Campaign Not Found/ no prospect movement defined";
		/*
		 * DIRECT_QUEUED DIRECT_1 DIRECT_2 ...
		 * 
		 * INDIRECT_QUEUED INDIRECT_1 INDIRECT_2 ...
		 */
		if (oneOffDTO.getCampaignName() != null && !oneOffDTO.getCampaignName().isEmpty()
				&& oneOffDTO.getOrganizationId() != null && !oneOffDTO.getOrganizationId().isEmpty()) {
			try {
				Campaign campaign = campaignRepository.findByNameAndOrganizationId(oneOffDTO.getCampaignName(),
						oneOffDTO.getOrganizationId());
				if (campaign != null && campaign.getName() != null && oneOffDTO.getMoveProspects() != null
						&& oneOffDTO.getMoveProspects().size() > 0) {
					List<ProspectCallLog> prospectCallLogList = getByRetryDirect(campaign.getId() + NON_CALLABLE,
							oneOffDTO);
					if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
						int counter = 0;
						for (ProspectCallLog pcl : prospectCallLogList) {
							pcl.getProspectCall().setCampaignId(campaign.getId());
							prospectCallLogRepository.save(pcl);
							counter++;
						}
						return "Added " + counter + " Records";
					} else {
						return "No Prospects Found";
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnmessage;
	}

	public String removeCallables(OneOffDTO oneOffDTO) {
		String NON_CALLABLE = "_NON_CALLABLE";

		String returnmessage = "Campaign Not Found";
		/*
		 * DIRECT_QUEUED DIRECT_1 DIRECT_2 ...
		 * 
		 * INDIRECT_QUEUED INDIRECT_1 INDIRECT_2 ...
		 */
		if (oneOffDTO.getCampaignName() != null && !oneOffDTO.getCampaignName().isEmpty()
				&& oneOffDTO.getOrganizationId() != null && !oneOffDTO.getOrganizationId().isEmpty()) {
			try {
				Campaign campaign = campaignRepository.findByNameAndOrganizationId(oneOffDTO.getCampaignName(),
						oneOffDTO.getOrganizationId());
				if (campaign != null && campaign.getName() != null && oneOffDTO.getMoveProspects() != null
						&& oneOffDTO.getMoveProspects().size() > 0) {
					List<ProspectCallLog> prospectCallLogList = getByRetryDirect(campaign.getId(), oneOffDTO);
					if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
						int counter = 0;
						for (ProspectCallLog pcl : prospectCallLogList) {
							pcl.getProspectCall().setCampaignId(campaign.getId() + NON_CALLABLE);
							prospectCallLogRepository.save(pcl);
							counter++;
						}
						return "Removed " + counter + " Records";
					} else {
						return "No Prospects Found";
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnmessage;
	}

	private List<ProspectCallLog> getByRetryDirect(String campaignId, OneOffDTO oneOffDTO) {

		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		List<String> clist = new ArrayList<String>(1);
		clist.add(campaignId);
		// findContactsByQueuedByCampaigns
		/*
		 * if(oneOffDTO.getRemoveProspects()==null ||
		 * oneOffDTO.getRemoveProspects().size()==0) { List<ProspectCallLog>
		 * prospectCallLogList0 =
		 * prospectCallLogRepository.findContactsByQueuedByCampaigns(clist);
		 * prospectCallLogList.addAll(prospectCallLogList0); List<ProspectCallLog>
		 * prospectCallLogList1 =
		 * prospectCallLogRepository.findContactsByRetryCount(1,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList1); List<ProspectCallLog>
		 * prospectCallLogList2 =
		 * prospectCallLogRepository.findContactsByRetryCount(2,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList2); List<ProspectCallLog>
		 * prospectCallLogList3 =
		 * prospectCallLogRepository.findContactsByRetryCount(3,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList3); List<ProspectCallLog>
		 * prospectCallLogList4 =
		 * prospectCallLogRepository.findContactsByRetryCount(4,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList4); List<ProspectCallLog>
		 * prospectCallLogList5 =
		 * prospectCallLogRepository.findContactsByRetryCount(5,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList5); List<ProspectCallLog>
		 * prospectCallLogList6 =
		 * prospectCallLogRepository.findContactsByRetryCount(6,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList6); List<ProspectCallLog>
		 * prospectCallLogList7 =
		 * prospectCallLogRepository.findContactsByRetryCount(7,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList7); List<ProspectCallLog>
		 * prospectCallLogList8 =
		 * prospectCallLogRepository.findContactsByRetryCount(8,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList8); List<ProspectCallLog>
		 * prospectCallLogList9 =
		 * prospectCallLogRepository.findContactsByRetryCount(9,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList9); List<ProspectCallLog>
		 * prospectCallLogList10 =
		 * prospectCallLogRepository.findContactsByRetryCount(10,campaignId);
		 * prospectCallLogList.addAll(prospectCallLogList10); }else {
		 */
		if (oneOffDTO.getMoveProspects().contains("DIRECT_QUEUED")) {
			List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository
					.findContactsByQueuedDirect(campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList0);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_QUEUED")) {
			List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository
					.findContactsByQueuedDirect(campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList0);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_1")) {
			List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCountDirect(1,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList1);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_1")) {
			List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCountDirect(1,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList1);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_2")) {
			List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCountDirect(2,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList2);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_2")) {
			List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCountDirect(2,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList2);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_3")) {
			List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCountDirect(3,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList3);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_3")) {
			List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCountDirect(3,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList3);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_4")) {
			List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCountDirect(4,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList4);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_4")) {
			List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCountDirect(4,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList4);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_5")) {
			List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCountDirect(5,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList5);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_5")) {
			List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCountDirect(5,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList5);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_6")) {
			List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCountDirect(6,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList6);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_6")) {
			List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCountDirect(6,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList6);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_7")) {
			List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCountDirect(7,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList7);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_7")) {
			List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCountDirect(7,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList7);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_8")) {
			List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCountDirect(8,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList8);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_8")) {
			List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCountDirect(8,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList8);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_9")) {
			List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCountDirect(9,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList9);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_9")) {
			List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCountDirect(9,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList9);
		}
		if (oneOffDTO.getMoveProspects().contains("DIRECT_10")) {
			List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCountDirect(10,
					campaignId, true);
			prospectCallLogList.addAll(prospectCallLogList10);
		} else if (oneOffDTO.getMoveProspects().contains("INDIRECT_10")) {
			List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCountDirect(10,
					campaignId, false);
			prospectCallLogList.addAll(prospectCallLogList10);
		}
		// }

		return prospectCallLogList;

	}

	public String killdatabuyqueue(OneOffDTO oneOffDTO) {
		DataBuyQueue dbq = dataBuyQueueRepository.findByCampaignIdAndStatus(oneOffDTO.getPrimaryCampaignId(),
				"INPROCESS");
		if (dbq != null) {
			dbq.setStatus("KILL");
			dataBuyQueueRepository.save(dbq);
			return "SUCCESS";
		}
		return "FAILED";
	}

	public String changeBuyStatus(OneOffDTO oneOffDTO) {
		DataBuyQueue dbq = dataBuyQueueRepository.findOneById(oneOffDTO.getDataBuyId());
		if (dbq != null) {
			dbq.setStatus(oneOffDTO.getToBuyStatus());
			dataBuyQueueRepository.save(dbq);
			return "SUCCESS";
		}
		return "FAILED";
	}

	public String getCampaign(OneOffDTO oneOffDTO) {
		String returnmessage = "Campaign Not Found";
		if (oneOffDTO.getCampaignName() != null && !oneOffDTO.getCampaignName().isEmpty()
				&& oneOffDTO.getOrganizationId() != null && !oneOffDTO.getOrganizationId().isEmpty()) {
			try {
				Campaign campaign = campaignRepository.findByNameAndOrganizationId(oneOffDTO.getCampaignName(),
						oneOffDTO.getOrganizationId());
				List<CampaignAgentInvitation> caiList = new ArrayList<CampaignAgentInvitation>();
				// if(campaign.getAgentInvitations()!=null) {
				// campaign.setAgentInvitations(caiList);
				// }else {
				campaign.setAgentInvitations(caiList);
				// }
				if (campaign != null && campaign.getName() != null) {
					ObjectMapper mapper = new ObjectMapper();
					// Converting the Object to JSONString
					String jsonString = mapper.writeValueAsString(campaign);
					return jsonString;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return returnmessage;

	}
	
	public String extractCID(OneOffDTO oneOffDTO) {
		String returnmessage = "SUCCESS";
		if(oneOffDTO.getCampaignIdList() !=null && oneOffDTO.getCampaignIdList().size()>0) {
			StringBuilder sb = new StringBuilder();
			try {
				List<String> campaignIds = oneOffDTO.getCampaignIdList();
				int page = 0;
				for(String campaignId : campaignIds) {
					sb.append(campaignId);
					sb.append("|");
					Pageable pageable = PageRequest.of(page, 1000);
	
					List<AbmListNew>  abmlistList = abmListNewRepository.findABMListByPagination(campaignId, pageable);
					if(abmlistList==null || abmlistList.size()==0) {
						break;
					}
					page++;
					for(AbmListNew abm : abmlistList) {
						AbmListDetail abmListDetail = abmListDetailRepository.findOneByDomainRegex(abm.getCompanyName());
						if(abmListDetail != null) {
							String companyId = abmListDetail.getCompanyId();
							abm.setCompanyId(companyId);
						}else {
							abm.setCompanyId("NOT_FOUND");
						}
						abmListNewRepository.save(abm);
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				XtaasEmailUtils.sendEmail(oneOffDTO.getEmailIds(), "data@xtaascorp.com",
						"Error Occured abm fetch", "Error occured. "+sb.toString());
			}
			
			XtaasEmailUtils.sendEmail(oneOffDTO.getEmailIds(), "data@xtaascorp.com",
					"ABM Fetching finished from internal DB", "ABM Fetching finished from internal DB. "+sb.toString());
			
		}

		return returnmessage;

		
	}
	
	public String supstatschange(OneOffDTO oneOffDTO) {
		String returnmessage = "SUCCESS";
		if(oneOffDTO.getCampaignIdList() !=null && oneOffDTO.getCampaignIdList().size()>0) {
			List<String> campaignIds = oneOffDTO.getCampaignIdList();
			for(String campaignId : campaignIds) {
				SupervisorStats supStats = supervisorStatsRepository.findByCampaignIdTypeAndStatus(oneOffDTO.getSupervisorStatsType(), campaignId, oneOffDTO.getSupervisorStatsStatus());
				supStats.setStatus("COMPLETED");
				supervisorStatsRepository.save(supStats);
			}
		}
		
		return returnmessage;
	}
	
	public String suppressMD5Emails(OneOffDTO oneOffDTO) {
		String returnmessage = "SUCCESS";
		/*String rehaan = "52a3c549ff44811af45488d4ce54f6d8";
		
		String enc =  DigestUtils
  		      .md5Hex("rehaan");
		
		if(rehaan.equals(enc)) {
			System.out.println("true");
		}else {
			System.out.println("false");

		}*/

		if(oneOffDTO.getCampaignIdList() !=null && oneOffDTO.getCampaignIdList().size()>0) {
			List<String> campaignIds = oneOffDTO.getCampaignIdList();
			for(String campaignId : campaignIds) {
				Campaign campaign = campaignRepository.findOneById(campaignId);
				List<ProspectCallLog> pList = prospectCallLogRepository.findCallableContacts(campaignId);
				for(ProspectCallLog plog : pList) {
					if(plog.getProspectCall().getProspect().getEmail()!=null &&
							!plog.getProspectCall().getProspect().getEmail().isEmpty()) {
						String md5Email = DigestUtils
					  		      .md5Hex(plog.getProspectCall().getProspect().getEmail());
						if(plog.getProspectCall().getProspect().getEmail().equalsIgnoreCase("nsmith@daventrydc.gov.uk")) {
							System.out.println("");
						}
						boolean emailsup = suppressionListService.isEmailSuppressed(md5Email, campaignId,
								campaign.getOrganizationId());
						 if(emailsup) {
							// suppressed = true;
							 plog.getProspectCall().setCallRetryCount(144);
								plog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
								prospectCallLogRepository.save(plog);
						 }
					}
				}
			}
		}
		return returnmessage;
	}

	public String findByProspectCallId(OneOffDTO oneOffDTO) {
		String returnmessage = "Some Error Occured";
		if (oneOffDTO.getProspectCallIds() != null && !oneOffDTO.getProspectCallIds().isEmpty()) {
			List<ProspectCallLog> pclList = prospectCallLogRepository
					.findByProspectCallIds(oneOffDTO.getProspectCallIds());
			StringBuffer sb = new StringBuffer();
			for (ProspectCallLog pc : pclList) {
				sb.append("pcid : ");
				sb.append(pc.getProspectCall().getProspectCallId());
				sb.append("firstName : ");
				sb.append(pc.getProspectCall().getProspect().getFirstName());
				sb.append("lastName : ");
				sb.append(pc.getProspectCall().getProspect().getLastName());
				sb.append("email : ");
				sb.append(pc.getProspectCall().getProspect().getEmail());
				sb.append("phone : ");
				sb.append(pc.getProspectCall().getProspect().getPhone());
				sb.append("\n");
			}
			return sb.toString();
		}

		return returnmessage;

	}

	public String getProspect(OneOffDTO oneOffDTO) {
		String returnmessage = "Campaign Not Found";
		if (oneOffDTO.getCampaignName() != null && !oneOffDTO.getCampaignName().isEmpty()
				&& oneOffDTO.getOrganizationId() != null && !oneOffDTO.getOrganizationId().isEmpty()
				&& (oneOffDTO.getFirstName() != null && !oneOffDTO.getFirstName().isEmpty()
						|| oneOffDTO.getLastName() != null && !oneOffDTO.getLastName().isEmpty())) {
			try {
				Campaign campaign = campaignRepository.findByNameAndOrganizationId(oneOffDTO.getCampaignName(),
						oneOffDTO.getOrganizationId());
				if (campaign != null && campaign.getName() != null) {
					List<ProspectCallLog> pcLogList = prospectCallLogRepositoryImpl.getProspectByName(campaign.getId(),
							oneOffDTO.getFirstName(), oneOffDTO.getLastName());
					if (pcLogList != null && pcLogList.size() > 0) {
						ObjectMapper mapper = new ObjectMapper();
						// Converting the Object to JSONString
						String jsonString = mapper.writeValueAsString(pcLogList);
						return jsonString;
					} else
						return "Prospect Not Found";
				} else {
					return returnmessage;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return returnmessage;
	}

	@Override
	public String getEncryptProspect(OneOffDTO oneOffDTO) {
		String returnmessage = "Campaign Not Found";
		if (oneOffDTO.getCampaignName() != null && !oneOffDTO.getCampaignName().isEmpty()
				&& oneOffDTO.getOrganizationId() != null && !oneOffDTO.getOrganizationId().isEmpty()
				&& (oneOffDTO.getFirstName() != null && !oneOffDTO.getFirstName().isEmpty()
						|| oneOffDTO.getLastName() != null && !oneOffDTO.getLastName().isEmpty())) {
			try {
				Campaign campaign = campaignRepository.findByNameAndOrganizationId(oneOffDTO.getCampaignName(),
						oneOffDTO.getOrganizationId());
				if (campaign != null && campaign.getName() != null) {
					List<ProspectCallLog> pcLogList = prospectCallLogRepositoryImpl.getProspectByFirstNameOrLastName(
							campaign.getId(), oneOffDTO.getFirstName(), oneOffDTO.getLastName());
					if (pcLogList != null && pcLogList.size() > 0) {
						ObjectMapper mapper = new ObjectMapper();
						// Converting the Object to JSONString
						String jsonString = mapper.writeValueAsString(pcLogList);
						return jsonString;
					} else
						return "Prospect Not Found";
				} else {
					return returnmessage;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return returnmessage;
	}

	public String updateProspect(OneOffDTO oneOffDTO) {
        String returnmessage = "not updated.";
        if (oneOffDTO.getUpdateProspectInfo() != null
                && oneOffDTO.getUpdateProspectInfo().getProspectCallId() != null) {
        	returnmessage = tagProspect(oneOffDTO, oneOffDTO.getUpdateProspectInfo().getProspectCallId());
        }
        return returnmessage;
    }


	@Override
    public String updateMultipleProspects(OneOffDTO oneOffDTO) {
        String returnMessage = "not updated.";
        if (oneOffDTO.getUpdateProspectInfo() != null && oneOffDTO.getUpdateProspectInfo().getProspectCallIds() != null
                && oneOffDTO.getUpdateProspectInfo().getProspectCallIds().size() > 0) {
            for (String prospectCallId : oneOffDTO.getUpdateProspectInfo().getProspectCallIds()) {
                returnMessage = tagProspect(oneOffDTO, prospectCallId);
            }
        }
        return returnMessage;
    }
    
    private String tagProspect(OneOffDTO oneOffDTO, String prospectCallId) {
        String returnMessage = "";
        try {
            UpdateProspectInfo settings = oneOffDTO.getUpdateProspectInfo();
            ProspectCallLog pcl = prospectCallLogRepository.findByProspectCallId(prospectCallId);
            if (pcl == null) {
                returnMessage = "Prospect not found in the system.";
                return returnMessage;
            }
            Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
            List<ProspectCallInteraction> pciList = prospectCallInteractionRepository
                    .findAllByProspectCallId(prospectCallId, pageable);
            ProspectCallInteraction pci = new ProspectCallInteraction();
            if (pciList.size() > 0) {
                pci = pciList.get(0);
            }
            if (settings.getAgentId() != null && !settings.getAgentId().isEmpty()) {
                pcl.getProspectCall().setAgentId(settings.getAgentId());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setAgentId(settings.getAgentId());
                }
            }
            if (settings.getRecordingUrlAws() != null && !settings.getRecordingUrlAws().isEmpty()) {
                pcl.getProspectCall().setRecordingUrlAws(settings.getRecordingUrlAws());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setRecordingUrlAws(settings.getRecordingUrlAws());
                }
            }
            if (settings.getDispositionStatus() != null && !settings.getDispositionStatus().isEmpty()) {
                pcl.getProspectCall().setDispositionStatus(settings.getDispositionStatus());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setDispositionStatus(settings.getDispositionStatus());
                }
            }
            if (settings.getLeadStatus() != null && !settings.getLeadStatus().isEmpty()) {
                pcl.getProspectCall().setLeadStatus(settings.getLeadStatus());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setLeadStatus(settings.getLeadStatus());
                }
            }
            if (settings.getRecordingUrl() != null && !settings.getRecordingUrl().isEmpty()) {
                pcl.getProspectCall().setRecordingUrl(settings.getRecordingUrl());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setRecordingUrl(settings.getRecordingUrl());
                }
            }
            if (settings.getStatus() != null && !settings.getStatus().isEmpty()) {
                pcl.setStatus(ProspectCallStatus.valueOf(settings.getStatus().toString()));
                if (pci != null && pci.getProspectCall() != null) {
                	pci.setStatus(ProspectCallStatus.valueOf(settings.getStatus().toString()));
                }
            }
            if (settings.getSubStatus() != null && !settings.getSubStatus().isEmpty()) {
                pcl.getProspectCall().setSubStatus(settings.getSubStatus());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setSubStatus(settings.getSubStatus());
                }
            }
            if (settings.getSupervisorId() != null && !settings.getSupervisorId().isEmpty()) {
                pcl.getProspectCall().setSupervisorId(settings.getSupervisorId());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setSupervisorId(settings.getSupervisorId());
                }
            }
            if (settings.getPartnerId() != null && !settings.getPartnerId().isEmpty()) {
                pcl.getProspectCall().setPartnerId(settings.getPartnerId());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setPartnerId(settings.getPartnerId());
                }
            }
            if (settings.getCallDuration() != 0) {
                pcl.getProspectCall().setCallDuration(settings.getCallDuration());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setCallDuration(settings.getCallDuration());
                }
            }
            if (settings.getProspectHandleDuration() != null) {
                pcl.getProspectCall().setProspectHandleDuration(settings.getProspectHandleDuration());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setProspectHandleDuration(settings.getProspectHandleDuration());
                }
            }
            if (settings.getCallStartTime() != null) {
                pcl.getProspectCall().setCallStartTime(settings.getCallStartTime());
                if (pci != null && pci.getProspectCall() != null) {
                	pci.getProspectCall().setCallStartTime(settings.getCallStartTime());
                }
            }
            if (settings.getDispositionStatus() != null && !settings.getDispositionStatus().isEmpty()
                    && settings.getSubStatus() != null && !settings.getSubStatus().isEmpty()
                    && settings.getDispositionStatus().equalsIgnoreCase("SUCCESS")
                    && settings.getSubStatus().equalsIgnoreCase("SUBMITLEAD")) {
                SuccessCallLog successCallLog = new SuccessCallLog();
                successCallLog = successCallLog.toSuccessCallLog(pcl);
                successCallLogRepository.save(successCallLog);
            }
            prospectCallLogRepository.save(pcl);
            if (pci != null && pci.getProspectCall() != null) {
            	prospectCallInteractionRepository.save(pci);
            }
            returnMessage = "Updated Successfuly";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnMessage;
    }

	public String createZoomToken() {
		String oAuth = "";
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		Date currDate = new Date();
		List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
		ZoomToken ezt = zoomTokenList.get(0);
		long duration = currDate.getTime() - ezt.getCreatedDate().getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			oAuth = zoomOAuth.createJWT();
			if (oAuth != null && !oAuth.isEmpty()) {
				// tokenKey =oAuth;
				ezt.setStatus("INACTIVE");
				zoomTokenRepository.save(ezt);
				ZoomToken newZoomToken = new ZoomToken();
				newZoomToken.setOauthToken(oAuth);
				newZoomToken.setStatus("ACTIVE");
				zoomTokenRepository.save(newZoomToken);
				downloadRecordingsToAws.updateZoomTokenToS3(newZoomToken.getOauthToken());
				// cachedDate = null;
			} else {
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return "zoom token created";
	}
	/////////////////////////////

	public String deliverCampaignAsset(OneOffDTO oneOffDto) {
		String error = null;
		List<String> prospectCallIds = oneOffDto.getProspectCallIds();
		String assetID = oneOffDto.getAssetId();// add asset id here

		try {
			List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository
					.findByProspectCallIds(prospectCallIds);
			if (prospectCallLogList != null && prospectCallLogList.size() > 0) {
				for (ProspectCallLog plog : prospectCallLogList) {
					deliverActiveAsset(plog.getProspectCall(), assetID);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Deliver Campaign Asset Report Status - Errors",
					error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Deliver Campaign Asset Report Status", error);
		}

		return error;
	}

	private String deliverActiveAsset(ProspectCall prospectCall, String assetId) {
		Prospect prospect = prospectCall.getProspect();
		String prospectInteractionSessionId = prospectCall.getProspectInteractionSessionId();
		Campaign campaign = campaignRepository.findOneById(prospectCall.getCampaignId());
		Team team = teamRepository.findOneById(campaign.getTeam().getTeamId());
		Partner partner = partnerRepository.findOneById(team.getPartnerId());
		// String assetId = prospectCall.getDeliveredAssetId();
		Asset activeAsset = null;
		List<Asset> activeAssets = campaign.getAssets();
		if (activeAssets != null) {
			for (Asset asset2 : activeAssets) {
				if (assetId != null) {
					if (assetId.equals(asset2.getAssetId())) {
						activeAsset = asset2;
						break;
					}
				} else {
					activeAsset = asset2;
					break;
				}
			}
		}
		EmailMessage emailMessageTemplate = campaign.getAssetEmailTemplate();
		if (activeAsset == null)
			return null;
		String msgBody;
		if (emailMessageTemplate == null) {
			// use a default template
			msgBody = "Hi, As per our conversation please access the white paper at [ASSET_URL]";
			emailMessageTemplate = new EmailMessage(activeAsset.getName(), msgBody);
		}
		String emailAddress = prospect.getEmail();
		List<String> to = new ArrayList<String>();
		to.add(emailAddress);
		if (partner != null && partner.getAssetDeliveryEmail() != null) {
			to.add(partner.getAssetDeliveryEmail());
		} else {
			to.add("assets@xtaascorp.com");
		}
		if (emailMessageTemplate.getReplyTo() != null && !to.contains(emailMessageTemplate.getReplyTo())) {
			to.add(emailMessageTemplate.getReplyTo());
		}
		// Replace tags with Values
		msgBody = emailMessageTemplate.getMessage();
		////// TODO remove this code in future, as this is one of case where we are
		////// sharing 2 assets at a time. once the campaign is gone, remove it
		String url1 = "https://s3.amazonaws.com/xtaas-assets/591e1870e4b0931b8f03ffee/Internet+of+Things+and+the+Rise+of+300+Gbps+DDoS+Attacks.pdf";
		String url2 = "https://s3.amazonaws.com/xtaas-assets/591e1870e4b0931b8f03ffee/Bots+to+the+Future.pdf";

		String url1name = "Internet of Things and the Rise of 300 Gbps DDoS Attacks";
		String url2name = "Bots to the Future";
		if (prospectCall.getCampaignId().equalsIgnoreCase("591e1870e4b0931b8f03ffee")) {
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL1]", "<a href='" + url1 + "'>" + url1name + "</a>");
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL2]", "<a href='" + url2 + "'>" + url2name + "</a>");
		} else {
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL]",
					"<a href='" + activeAsset.getUrl() + "'>" + activeAsset.getName() + "</a>");
		}
		msgBody = StringUtils.replace(msgBody, "[PROSPECT_NAME]", prospect.getFirstName());
		EmailMessage message = new EmailMessage(to, emailMessageTemplate.getSubject(), msgBody);
		if (emailMessageTemplate.getFromEmail() != null) {
			message.setFromEmail(emailMessageTemplate.getFromEmail());
		} else {
			message.setFromEmail("support+asset@xtaascorp.com");
		}
		if (emailMessageTemplate.getFromName() != null)
			message.setFromName(emailMessageTemplate.getFromName());
		if (emailMessageTemplate.getReplyTo() != null)
			message.setReplyTo(emailMessageTemplate.getReplyTo());
		// send email
		// XtaasEmailUtils.sendEmail(message,prospectInteractionSessionId);

		/*
		 * DATE : 07/11/2017 REASON : send disposition success mail via sendgrid
		 */
		try {
			XtaasEmailUtils.sendEmailSendGrid(message, prospectInteractionSessionId, prospectCall.getCampaignId(),
					prospectCall.getProspectCallId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// logger.info("deliverActiveAsset() : Asset [{}] delivered to Email address
		// [{}] ", activeAsset.getAssetId(), emailAddress);
		return activeAsset.getAssetId();
	}

	@Override
	public String getPCIAnalysisTemp(OneOffDTO oneOffDto) {
		return insertPCIAnalysis(oneOffDto);

	}

	private String insertPCIAnalysis(OneOffDTO oneOffDto) {
		String error = null;
		try {
			// for(int i = 20;i<=30;i++) {
			String fromDate = oneOffDto.getFromDate();
			String toDate = oneOffDto.getToDate();
			List<String> campaignIds = oneOffDto.getCampaignIdList();
			Date startDate = XtaasDateUtils.getDate(fromDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			// logger.debug(startDateInUTC);
			Date endDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
			if (campaignIds != null && campaignIds.size() > 0) {
				for (String campaignId : campaignIds) {
					insertPCIAnalysis(startDateInUTC, endDateInUTC, campaignId, oneOffDto.isUseTempPCI());
				}
			} else {
				insertPCIAnalysis(startDateInUTC, endDateInUTC, null, oneOffDto.isUseTempPCI());
			}
			// }
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		logger.debug("PCIANALYSIS DONE");
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "PCI Analysis Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "PCI Analysis Report Status", error);
		}

		return error;

	}

	private String insertPCIAnalysis(Date startDateInUTC, Date endDateInUTC, String campaignId, boolean isUseTempPci) {
		// "03/20/2020 00:01:00"

		List<ProspectCallInteraction> pciList = new ArrayList<ProspectCallInteraction>();
		List<ProspectCallInteractionTemp> pciTempList = new ArrayList<ProspectCallInteractionTemp>();

		// logger.debug(endDateInUTC);
		int counter = 0;
		try {
			// boolean isTrue = true;
			// while(isTrue) {
			int pciCount = 0;

			if (campaignId != null && !campaignId.isEmpty()) {
				if (isUseTempPci) {
					pciCount = prospectCallInteractionTempRepository.findByCampaignDateCount(campaignId, startDateInUTC,
							endDateInUTC);
				} else {
					pciCount = prospectCallInteractionRepository.findByCampaignDateCount(campaignId, startDateInUTC,
							endDateInUTC);
				}
			} else {
				if (isUseTempPci) {
					pciCount = prospectCallInteractionTempRepository.findByDateCount(startDateInUTC, endDateInUTC);
				} else {
					pciCount = prospectCallInteractionRepository.findByDateCount(startDateInUTC, endDateInUTC);
				}
			}
			int batchSize = 0;
			if (pciCount > 0) {
				batchSize = pciCount / 10000;
				int abatch = batchSize * 10000;
				if (abatch < pciCount) {
					batchSize = batchSize + 1;
				}
			}
			for (int k = 0; k < batchSize; k++) {
				Pageable pageable = PageRequest.of(k, 10000, Direction.ASC, "updatedDate");
				if (campaignId != null && !campaignId.isEmpty()) {
					if (isUseTempPci) {
						pciList = new ArrayList<ProspectCallInteraction>();
						pciTempList = prospectCallInteractionTempRepository.findByCampaignDate(campaignId,
								startDateInUTC, endDateInUTC, pageable);
						for (ProspectCallInteractionTemp pciTemp : pciTempList) {
							ProspectCallInteraction pciNew = new ProspectCallInteraction();
							pciNew.setCallbackDate(pciTemp.getCallbackDate());
							pciNew.setDataSlice(pciTemp.getDataSlice());
							pciNew.setProspectCall(pciTemp.getProspectCall());
							pciNew.setQaFeedback(pciTemp.getQaFeedback());
							pciNew.setStatus(pciTemp.getStatus());
							pciNew.setCreatedBy(pciTemp.getCreatedBy());
							pciNew.setCreatedDate(pciTemp.getCreatedDate());
							pciNew.setUpdatedBy(pciTemp.getUpdatedBy());
							pciNew.setUpdatedDate(pciTemp.getUpdatedDate());
							pciNew.setVersion(pciTemp.getVersion());
							pciList.add(pciNew);
						}
					} else {
						pciList = prospectCallInteractionRepository.findByCampaignDate(campaignId, startDateInUTC,
								endDateInUTC, pageable);
					}
				} else {
					if (isUseTempPci) {
						pciList = new ArrayList<ProspectCallInteraction>();
						pciTempList = prospectCallInteractionTempRepository.findByDate(startDateInUTC, endDateInUTC,
								pageable);
						for (ProspectCallInteractionTemp pciTemp : pciTempList) {
							ProspectCallInteraction pciNew = new ProspectCallInteraction();
							pciNew.setCallbackDate(pciTemp.getCallbackDate());
							pciNew.setDataSlice(pciTemp.getDataSlice());
							pciNew.setProspectCall(pciTemp.getProspectCall());
							pciNew.setQaFeedback(pciTemp.getQaFeedback());
							pciNew.setStatus(pciTemp.getStatus());
							pciNew.setCreatedBy(pciTemp.getCreatedBy());
							pciNew.setCreatedDate(pciTemp.getCreatedDate());
							pciNew.setUpdatedBy(pciTemp.getUpdatedBy());
							pciNew.setUpdatedDate(pciTemp.getUpdatedDate());
							pciNew.setVersion(pciTemp.getVersion());
							pciList.add(pciNew);
						}
					} else {
						pciList = prospectCallInteractionRepository.findByDate(startDateInUTC, endDateInUTC, pageable);
					}
				}
				if (pciList != null && pciList.size() > 0) {
					for (ProspectCallInteraction prospectCallInteraction : pciList) {

						Campaign campaign = campaignRepository
								.findOneById(prospectCallInteraction.getProspectCall().getCampaignId());
						String agentName = null;
						String qaName = null;
						PCIAnalysisTemp latestPciAnalysis = null;
						if (prospectCallInteraction.getQaFeedback() != null
								&& prospectCallInteraction.getQaFeedback().getQaId() != null) {
							Login login = loginRepository
									.findOneById(prospectCallInteraction.getQaFeedback().getQaId());
							if (login != null) {
								qaName = login.getName();
							}
						}
						if (prospectCallInteraction.getProspectCall() != null
								&& prospectCallInteraction.getProspectCall().getAgentId() != null) {
							Login login = loginRepository
									.findOneById(prospectCallInteraction.getProspectCall().getAgentId());
							if (login != null) {
								agentName = login.getName();
							}
						}
						PCIAnalysisTemp pcian = new PCIAnalysisTemp();
						pcian.setCreated_date(prospectCallInteraction.getCreatedDate());
						pcian.setCreated_datetime(prospectCallInteraction.getCreatedDate());
						pcian.toPCIAnalysis(pcian, prospectCallInteraction, agentName, qaName, campaign);
						Pageable pageable1 = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
						List<PCIAnalysisTemp> latestPciList = pciAnalysisRepository
								.findByProspectCallIdWithSort(pcian.getProspectcallid(), pageable1);
						if (latestPciList != null && latestPciList.size() > 0) {
							latestPciAnalysis = latestPciList.get(0);
						}
						if (prospectCallInteraction.getStatus()
								.equals(ProspectCallLog.ProspectCallStatus.CALL_DIALED)) {
							pcian.setCall_attempted(1);
							pcian.setCall_connected(0);
							pcian.setCall_contacted(0);
							if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
									&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
								// dont do anything
							} else {
								pciAnalysisRepository.save(pcian);
							}
						} else if (/*
									 * latestPciAnalysis!=null && latestPciAnalysis.getInteraction_success()==0 &&
									 */ pcian.getInteraction_success() > 0) {

							// getClonedConnectedPci
							PCIAnalysisTemp pcian_connected = getClonedConnectedPci(pcian);
							PCIAnalysisTemp pcian_contacted = getClonedContactedPci(pcian_connected);
							pcian.setCall_attempted(0);
							pcian.setCall_connected(0);
							pcian.setCall_contacted(0);
							if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
									&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
								// dont do anything
							} else {
								pciAnalysisRepository.save(pcian);
							}

						} else if (/* latestPciAnalysis!=null && latestPciAnalysis.getCall_contacted()==0 && */ pcian
								.getCall_connected() > 0) {
							PCIAnalysisTemp pcian_connected = new PCIAnalysisTemp();
							pcian_connected = pcian;

							pcian_connected.setCall_attempted(0);
							pcian_connected.setCall_connected(1);
							pcian_connected.setCall_contacted(0);
							if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
									&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
								// dont do anything
							} else {
								pciAnalysisRepository.save(pcian_connected);
							}

							PCIAnalysisTemp pcian_contacted = getClonedContactedPci(pcian_connected);

						} else if (/* latestPciAnalysis!=null && latestPciAnalysis.getCall_connected() == 0 && */ pcian
								.getCall_contacted() > 0) {
							PCIAnalysisTemp pcian_connected = new PCIAnalysisTemp();
							pcian_connected = pcian;

							pcian_connected.setCall_attempted(0);
							pcian_connected.setCall_connected(0);
							pcian_connected.setCall_contacted(1);
							if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
									&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
								// dont do anything
							} else {
								pciAnalysisRepository.save(pcian_connected);
							}

						} else {
							pcian.setCall_attempted(0);
							pcian.setCall_connected(0);
							pcian.setCall_contacted(0);
							pcian.setInteraction_success(0);
							if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
									&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
								// dont do anything
							} else {
								pciAnalysisRepository.save(pcian);
							}
						}
					}
				}
			} // end for loop
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	private PCIAnalysisTemp getClonedContactedPci(PCIAnalysisTemp pcian) {
		PCIAnalysisTemp pcian_contacted = new PCIAnalysisTemp(pcian.getAgentId(), pcian.getAgentName(),
				pcian.getAssetName(), pcian.getCall_attempted(), pcian.getCall_connected(), pcian.getCall_contacted(),
				pcian.getCall_date(), pcian.getCall_time(), pcian.getCallduration(), pcian.getCallretrycount(),
				pcian.getCampaign_status(), pcian.getCampaignid(), pcian.getCampaignName(), pcian.getClientName(),
				pcian.getCompany(), pcian.getCountry1(), pcian.getCountry2(), pcian.getCountry3(), pcian.getCountry4(),
				pcian.getCountry5(), pcian.getCq1_certifyLink(), pcian.getCq1_question(), pcian.getCq1_response(),
				pcian.getCq1_validationresp(), pcian.getCq2_certifyLink(), pcian.getCq2_question(),
				pcian.getCq2_response(), pcian.getCq2_validationresp(), pcian.getCq3_certifyLink(),
				pcian.getCq3_question(), pcian.getCq3_response(), pcian.getCq3_validationresp(),
				pcian.getCq4_certifyLink(), pcian.getCq4_question(), pcian.getCq4_response(),
				pcian.getCq4_validationresp(), pcian.getCq4_certifyLink(), pcian.getCq5_question(),
				pcian.getCq5_response(), pcian.getCq5_validationresp(), pcian.getCreated_date(),
				pcian.getCreated_datetime(), pcian.getDepartment(), pcian.getDispositionstatus(), pcian.getDnc_added(),
				pcian.getFirstname(), pcian.getEmployeeCountMax(), pcian.getEmployeeCountMin(), pcian.getIndustry(),
				pcian.getIndustry1(), pcian.getIndustry2(), pcian.getIndustry3(), pcian.getIndustry4(),
				pcian.getInteraction_answer_machine(), pcian.getInteraction_busy(),
				pcian.getInteraction_call_abandoned(), pcian.getInteraction_dead_air(),
				pcian.getInteraction_FAILED_QUALIFICATION(), pcian.getInteraction_failure(),
				pcian.getInteraction_gatekeeper_answer_machine(), pcian.getInteraction_local_dnc_error(),
				pcian.getInteraction_max_retry_limit_reached(), pcian.getInteraction_national_dnc_error(),
				pcian.getInteraction_no_answer(), pcian.getInteraction_no_consent(),
				pcian.getInteraction_not_interested(), pcian.getInteraction_success(),
				pcian.getInteraction_unreachable(), pcian.getLastname(), pcian.getLead_validation_notes(),
				pcian.getLead_validation_valid(), pcian.getLeadStatus(), pcian.getManagement_level(),
				pcian.getMgt_level_c_level(), pcian.getMgt_level_director(), pcian.getMgt_level_manager(),
				pcian.getMgt_level_non_management(), pcian.getMgt_level_president_principal(),
				pcian.getMgt_level_vice_president(), pcian.getOrganizationid(), pcian.getOutboundnumber(),
				pcian.getP_domain(), pcian.getPartnerid(), pcian.getPhone(), pcian.getPrefix(),
				pcian.getProspectcallid(), pcian.getProspecthandleduration(), pcian.getQaid(), pcian.getQaname(),
				pcian.getQualitybucket(), pcian.getRecordingurl(), pcian.getRevenuemax(), pcian.getRevenuemin(),
				pcian.getStatecode(), pcian.getStatus(), pcian.getSubstatus(), pcian.getSuffix(), pcian.getTimezone(),
				pcian.getTitle(), pcian.getTransactiondate_day(), pcian.getTransactiondate_dayofweek(),
				pcian.getTransactiondate_dayofyear(), pcian.getTransactiondate_hour(),
				pcian.getTransactiondate_minute(), pcian.getTransactiondate_month(), pcian.getTransactiondate_second(),
				pcian.getTransactiondate_week(), pcian.getTransactiondate_year(), pcian.getVoiceduration(),
				pcian.getTwiliocallsid(), pcian.getZipcode(), pcian.getDatasource(), pcian.getUnknownerror(),
				pcian.getDirectPhone(), pcian.getEmail(), pcian.getDataSlice(), pcian.getQualitybucket(),
				pcian.getQualityBucketSort(), pcian.getSourceType(), pcian.getAuto_machine_answered(),
				pcian.getCompanyPhone(), pcian.getDailyRetryCount(), pcian.getRejectionReason(),
				pcian.getClientDelivered(), pcian.isLeadDelivered(), pcian.getLastUpdatedDate(),
				pcian.getCallDetectedAs(), pcian.getAmdTimeDiffInSec(), pcian.getAmdTimeDiffInMilliSec(),
				pcian.isEmailRevealed(),pcian.getCallbackDate(),pcian.isResearchData(),
				pcian.isProspectingData(),pcian.isAutoMachineDetected(),pcian.getNotes(),pcian.getAgentLoginURL(), pcian.getVoiceProvider(), pcian.getSipProvider());
		pcian_contacted.setCall_attempted(0);
		pcian_contacted.setCall_connected(0);
		pcian_contacted.setCall_contacted(1);
		pcian_contacted.setInteraction_success(0);
		if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
				&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
			// dont do anything
		} else {
			pciAnalysisRepository.save(pcian_contacted);
		}
		return pcian_contacted;
	}

	private PCIAnalysisTemp getClonedConnectedPci(PCIAnalysisTemp pcian) {
		PCIAnalysisTemp pcian_connected = new PCIAnalysisTemp(pcian.getAgentId(), pcian.getAgentName(),
				pcian.getAssetName(), pcian.getCall_attempted(), pcian.getCall_connected(), pcian.getCall_contacted(),
				pcian.getCall_date(), pcian.getCall_time(), pcian.getCallduration(), pcian.getCallretrycount(),
				pcian.getCampaign_status(), pcian.getCampaignid(), pcian.getCampaignName(), pcian.getClientName(),
				pcian.getCompany(), pcian.getCountry1(), pcian.getCountry2(), pcian.getCountry3(), pcian.getCountry4(),
				pcian.getCountry5(), pcian.getCq1_certifyLink(), pcian.getCq1_question(), pcian.getCq1_response(),
				pcian.getCq1_validationresp(), pcian.getCq2_certifyLink(), pcian.getCq2_question(),
				pcian.getCq2_response(), pcian.getCq2_validationresp(), pcian.getCq3_certifyLink(),
				pcian.getCq3_question(), pcian.getCq3_response(), pcian.getCq3_validationresp(),
				pcian.getCq4_certifyLink(), pcian.getCq4_question(), pcian.getCq4_response(),
				pcian.getCq4_validationresp(), pcian.getCq4_certifyLink(), pcian.getCq5_question(),
				pcian.getCq5_response(), pcian.getCq5_validationresp(), pcian.getCreated_date(),
				pcian.getCreated_datetime(), pcian.getDepartment(), pcian.getDispositionstatus(), pcian.getDnc_added(),
				pcian.getFirstname(), pcian.getEmployeeCountMax(), pcian.getEmployeeCountMin(), pcian.getIndustry(),
				pcian.getIndustry1(), pcian.getIndustry2(), pcian.getIndustry3(), pcian.getIndustry4(),
				pcian.getInteraction_answer_machine(), pcian.getInteraction_busy(),
				pcian.getInteraction_call_abandoned(), pcian.getInteraction_dead_air(),
				pcian.getInteraction_FAILED_QUALIFICATION(), pcian.getInteraction_failure(),
				pcian.getInteraction_gatekeeper_answer_machine(), pcian.getInteraction_local_dnc_error(),
				pcian.getInteraction_max_retry_limit_reached(), pcian.getInteraction_national_dnc_error(),
				pcian.getInteraction_no_answer(), pcian.getInteraction_no_consent(),
				pcian.getInteraction_not_interested(), pcian.getInteraction_success(),
				pcian.getInteraction_unreachable(), pcian.getLastname(), pcian.getLead_validation_notes(),
				pcian.getLead_validation_valid(), pcian.getLeadStatus(), pcian.getManagement_level(),
				pcian.getMgt_level_c_level(), pcian.getMgt_level_director(), pcian.getMgt_level_manager(),
				pcian.getMgt_level_non_management(), pcian.getMgt_level_president_principal(),
				pcian.getMgt_level_vice_president(), pcian.getOrganizationid(), pcian.getOutboundnumber(),
				pcian.getP_domain(), pcian.getPartnerid(), pcian.getPhone(), pcian.getPrefix(),
				pcian.getProspectcallid(), pcian.getProspecthandleduration(), pcian.getQaid(), pcian.getQaname(),
				pcian.getQualitybucket(), pcian.getRecordingurl(), pcian.getRevenuemax(), pcian.getRevenuemin(),
				pcian.getStatecode(), pcian.getStatus(), pcian.getSubstatus(), pcian.getSuffix(), pcian.getTimezone(),
				pcian.getTitle(), pcian.getTransactiondate_day(), pcian.getTransactiondate_dayofweek(),
				pcian.getTransactiondate_dayofyear(), pcian.getTransactiondate_hour(),
				pcian.getTransactiondate_minute(), pcian.getTransactiondate_month(), pcian.getTransactiondate_second(),
				pcian.getTransactiondate_week(), pcian.getTransactiondate_year(), pcian.getVoiceduration(),
				pcian.getTwiliocallsid(), pcian.getZipcode(), pcian.getDatasource(), pcian.getUnknownerror(),
				pcian.getDirectPhone(), pcian.getEmail(), pcian.getDataSlice(), pcian.getQualitybucket(),
				pcian.getQualityBucketSort(), pcian.getSourceType(), pcian.getAuto_machine_answered(),
				pcian.getCompanyPhone(), pcian.getDailyRetryCount(), pcian.getRejectionReason(),
				pcian.getClientDelivered(), pcian.isLeadDelivered(), pcian.getLastUpdatedDate(),
				pcian.getCallDetectedAs(), pcian.getAmdTimeDiffInSec(), pcian.getAmdTimeDiffInMilliSec(),
				pcian.isEmailRevealed(),pcian.getCallbackDate(),pcian.isResearchData(),
				pcian.isProspectingData(),pcian.isAutoMachineDetected(),pcian.getNotes(),pcian.getAgentLoginURL(), pcian.getVoiceProvider(), pcian.getSipProvider());
		pcian_connected.setCall_attempted(0);
		pcian_connected.setCall_connected(1);
		pcian_connected.setCall_contacted(0);
		pcian_connected.setInteraction_success(0);
		if (pcian.getUpdatedBy() != null && !pcian.getUpdatedBy().isEmpty()
				&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
			// dont do anything
		} else {
			pciAnalysisRepository.save(pcian_connected);
		}
		return pcian_connected;
	}

	@Override
	public String copySCLToPCL(String campaignId, String source, MultipartFile file) {
		String error = null;
		InputStream inputStream;
		int counter = 1;
		try {
			inputStream = file.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			while (line != null) {
				logger.debug(line);
				SuccessCallLog sclog = successCallLogRepository.findOneById(line);
				if (sclog == null) {
					line = reader.readLine();
					continue;
				}
				ProspectCallLog pcl = new ProspectCallLog();
				pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				pcl.setDataSlice("slice0");
				Prospect prospect = sclog.getProspectCall().getProspect();
				prospect.setSource(source);
				ProspectCall pc = new ProspectCall(getUniqueProspect(), campaignId, prospect);
				pc.setQualityBucket(sclog.getProspectCall().getQualityBucket());
				pc.setQualityBucketSort(sclog.getProspectCall().getQualityBucketSort());
				pc.setCallRetryCount(786);
				pc.setDataSlice("slice0");
				pcl.setProspectCall(pc);

				if (!isPersonRecordAlreadyPurchasedPCLog(pcl, campaignId)) {
					try {
						prospectCallLogRepository.save(pcl);
						logger.debug(line + "----->Saved--------->" + counter);
						counter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					logger.debug(line + "----->SKIPPING ALREADY PURCHASED---------");
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy SCL To PCL Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy SCL To PCL Report Report Status", error);
		}

		return error;

	}

	public String copyDATAToPCL(String campaignId, String source, MultipartFile file) {

		String error = null;
		InputStream inputStream;
		int counter = 1;
		try {
			inputStream = file.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			while (line != null) {
				System.out.println(line);
				ProspectMovement sclog = prospectMovementRepository.findOneById(line);
				// prospectMovementRepository.findById(line);
				if (sclog == null) {
					line = reader.readLine();
					continue;
				}
				ProspectCallLog pcl = new ProspectCallLog();
				pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				pcl.setDataSlice("slice0");
				Prospect prospect = sclog.getProspectCall().getProspect();
				prospect.setSource(source);
				ProspectCall pc = new ProspectCall(getUniqueProspect(), campaignId, prospect);
				pc.setQualityBucket(sclog.getProspectCall().getQualityBucket());
				pc.setQualityBucketSort(sclog.getProspectCall().getQualityBucketSort());
				pc.setCallRetryCount(786);
				pc.setDataSlice("slice0");
				pcl.setProspectCall(pc);

				if (!isPersonRecordAlreadyPurchasedPCLog(pcl, campaignId)) {
					try {
						prospectCallLogRepository.save(pcl);
						System.out.println(line + "----->Saved--------->" + counter);
						counter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					System.out.println(line + "----->SKIPPING ALREADY PURCHASED---------");
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy SCL To PCL Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy SCL To PCL Report Report Status", error);
		}

		return error;

	}

	@Override
	public String copyPCLToPCL(String campaignId, String source, MultipartFile file) {
		String error = null;
		int counter = 1;
		InputStream inputStream;
		try {
			inputStream = file.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			while (line != null) {
				logger.debug(line);
				ProspectCallLog sclog = prospectCallLogRepository.findOneById(line);
				if (sclog == null) {
					line = reader.readLine();
					continue;
				}
				ProspectCallLog pcl = new ProspectCallLog();
				pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				pcl.setDataSlice("slice0");
				Prospect prospect = sclog.getProspectCall().getProspect();
				prospect.setSource(source);
				ProspectCall pc = new ProspectCall(getUniqueProspect(), campaignId, prospect);
				pc.setQualityBucket(sclog.getProspectCall().getQualityBucket());
				pc.setQualityBucketSort(sclog.getProspectCall().getQualityBucketSort());
				pc.setCallRetryCount(786);
				pc.setDataSlice("slice0");
				pcl.setProspectCall(pc);

				if (!isPersonRecordAlreadyPurchasedPCLog(pcl, campaignId)) {
					try {
						prospectCallLogRepository.save(pcl);
						logger.debug(line + "----->Saved--------->" + counter);
						counter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					logger.debug(line + "----->SKIPPING ALREADY PURCHASED---------");
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCL To PCL Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCL To PCL Report Status", error);
		}

		return error;
	}

	private InputStream fileInputStream1;
	private Sheet sheet1;
	private String fileName1;

	@Override
	public String uploadOldSuppression(String organization, String type, MultipartFile file) {
		String error = "";

		try {
			if (!file.isEmpty()) {
				String filename = file.getOriginalFilename();
				filename = filename.substring(0, filename.indexOf("."));
				List<Campaign> campaigns = campaignRepository.findCampaignsByName(filename, organization);
				if (campaigns == null || campaigns.size() == 0) {
					return "no campaign found." + filename;
				}
				Campaign campaign = campaigns.get(0);
				@SuppressWarnings("serial")
				final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
					{
						// add("application/octet-stream");
						add("application/vnd.ms-excel");
						add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
						add("application/ms-excel");
					}
				};

				if (excelSupportedContentTypes.contains(file.getContentType())) {
					fileInputStream1 = file.getInputStream();
					// XSSFWorkbook book;
					Workbook book;
					try {
						book = WorkbookFactory.create(fileInputStream1);
						sheet1 = book.getSheetAt(0);
						int totalRowsCount = sheet1.getPhysicalNumberOfRows();
						if (totalRowsCount < 1) {// check if nothing found in file
							book.close();
							// logger.error(fileName + " file is empty");
							return fileName1 + " file is empty";
						}
					} catch (Exception er) {
					}

					List<String> supList = new ArrayList<String>();
					Set<String> set = new LinkedHashSet<String>();
					// String error = null;

					int startPos = 1;
					int rowsCount = sheet1.getLastRowNum();
					if (rowsCount < 1) {
						// logger.error("No records found in file " + fileName);
						return "Empty";
					}
					int colCounts = 1;
					while (startPos < rowsCount) {
						Row row = sheet1.getRow(startPos);
						if (row != null && row.getCell(0) != null) {
							supList.add(row.getCell(0).getStringCellValue().trim());
						}
						startPos = startPos + 1;

						// Campaign campaign = campaignRepository.findOne(campaignId);
					}

					List<SuppressionList> lsuppressionList = suppressionListRepository
							.findSupressionByCampaignId(campaign.getId());
					if (lsuppressionList != null && lsuppressionList.size() > 0) {
						SuppressionList suppressionList = lsuppressionList.get(0);
						List<String> supListDB = new ArrayList<String>();
						if (type.equalsIgnoreCase("company")) {
							supListDB = suppressionList.getDomains();
							supList.addAll(supListDB);
							set.addAll(supList);
							supList.clear();
							supList.addAll(set);
							suppressionList.setDomains(supList);
						} else {
							supListDB = suppressionList.getEmailIds();
							supList.addAll(supListDB);
							set.addAll(supList);
							supList.clear();
							supList.addAll(set);
							suppressionList.setEmailIds(supList);
						}

						suppressionListRepository.save(suppressionList);

					} else {
						SuppressionList suppressionList = new SuppressionList();
						suppressionList.setCampaignId(campaign.getId());
						// Campaign campaign = campaignRepository.findOne(campaignId);
						suppressionList.setOrganizationId(campaign.getOrganizationId());
						suppressionList.setStatus("ACTIVE");
						if (type.equalsIgnoreCase("company")) {
							set.addAll(supList);
							supList.clear();
							supList.addAll(set);
							suppressionList.setDomains(supList);
						} else {
							set.addAll(supList);
							supList.clear();
							supList.addAll(set);
							suppressionList.setEmailIds(supList);
						}

						suppressionListRepository.save(suppressionList);
					}
				}

			} // }

		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null && !error.isEmpty()) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "suppression old upload - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCI To PCL Report Status", error);
		}

		return error;
	}

	@Override
	public String copyPCIToPCL(String campaignId, String source, MultipartFile file) {
		String error = null;
		InputStream inputStream;
		int counter = 1;
		try {
			inputStream = file.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			while (line != null) {
				logger.debug(line);
				ProspectCallInteraction sclog = prospectCallInteractionRepository.findOneById(line);
				if (sclog == null) {
					line = reader.readLine();
					continue;
				}
				ProspectCallLog pcl = new ProspectCallLog();
				pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				pcl.setDataSlice("slice0");
				Prospect prospect = sclog.getProspectCall().getProspect();
				prospect.setSource(source);
				ProspectCall pc = new ProspectCall(getUniqueProspect(), campaignId, prospect);
				pc.setQualityBucket(sclog.getProspectCall().getQualityBucket());
				pc.setQualityBucketSort(sclog.getProspectCall().getQualityBucketSort());
				pc.setCallRetryCount(786);
				pc.setDataSlice("slice0");
				pcl.setProspectCall(pc);

				if (!isPersonRecordAlreadyPurchasedPCLog(pcl, campaignId)) {
					try {
						prospectCallLogRepository.save(pcl);
						logger.debug(line + "----->Saved--------->" + counter);
						counter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					logger.debug(line + "----->SKIPPING ALREADY PURCHASED---------");
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}
		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCI To PCL Report Status - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCI To PCL Report Status", error);
		}

		return error;
	}

	public boolean isPersonRecordAlreadyPurchasedPCLog(ProspectCallLog personRecord, String campaignId) {
		String firstName = personRecord.getProspectCall().getProspect().getFirstName();
		String lastName = personRecord.getProspectCall().getProspect().getLastName();
		String company = personRecord.getProspectCall().getProspect().getCompany();
		String title = personRecord.getProspectCall().getProspect().getTitle();
		List<String> campaignIds = new ArrayList<String>();
		// campaignIds.add(campaignId);
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignId, firstName, lastName, title,
				company);
		if (uniqueRecord == 0) {
			return false;
		} else {
			logger.debug(firstName + " " + lastName + " " + company
					+ " | Prospect #### : Already purchased in campaign : " + campaignId);
			return true;
		}
	}

	@Override
	public String getCampaignCache(OneOffDTO oneOffDto) {
// String returnmsg = "0";
// StringBuilder sb = new StringBuilder();
// if(oneOffDto.getCampaignIdList()!=null && oneOffDto.getCampaignIdList().size()>0) {
// List<String> campaignIds = oneOffDto.getCampaignIdList();
// int queuedCallbackProspectCallLogCount = 0;
// LinkedHashMap<String, List<ProspectCallDTO>> linkedProspectCallLog = prospectCacheServiceImpl
// .getCachedProspectCallLog();
// for(String cid : campaignIds) {
// if (linkedProspectCallLog != null && linkedProspectCallLog.size() != 0) {
// if (linkedProspectCallLog.get(cid) != null) {
// queuedCallbackProspectCallLogCount = queuedCallbackProspectCallLogCount
// + linkedProspectCallLog.get(cid).size();
// sb.append(cid).append(" : ").append(queuedCallbackProspectCallLogCount);
// }
// }
// }
// returnmsg = sb.toString();
// }
// return returnmsg;
		String returnStatement = null;
		if (oneOffDto.getCampaignIdList() != null && oneOffDto.getCampaignIdList().size() > 0) {
			for (String campaignId : oneOffDto.getCampaignIdList()) {
				List<ProspectCallCacheDTO> prospectsFromCache = priorityQueueCacheOperationService
						.getCurrentState(campaignId);
				if (prospectsFromCache != null && prospectsFromCache.size() > 0) {
					List<AnalyzeProspectCache> analyzeProspects = prospectsFromCache.stream()
							.map(s -> new AnalyzeProspectCache(s)).collect(Collectors.toList());
					if (oneOffDto.getNoOfProspectsToBeCopiedFromCache() == 0) {
						analyzeProspectCacheRepository.saveAll(analyzeProspects);
						returnStatement = "Successfully added " + analyzeProspects.size()
								+ " Prospects in AnalyzeProspectCache Collection.";
					} else {
						List<AnalyzeProspectCache> list = analyzeProspects.subList(0,
								oneOffDto.getNoOfProspectsToBeCopiedFromCache() + 1);
						analyzeProspectCacheRepository.saveAll(list);
						returnStatement = "Successfully added " + list.size()
								+ " Prospects in AnalyzeProspectCache Collection.";
					}
				} else {
					returnStatement = "No data found for this campaign in cache.";
				}
			}
		}
		return returnStatement;
	}

	@Override
	public String changeCampaignSettings(OneOffDTO oneOffDto) {
		String returnmsg = "Failed to update Campaigns";
		if (oneOffDto.getCampaignIdList() != null && oneOffDto.getCampaignIdList().size() > 0
				&& oneOffDto.getUpdateCampaignSettings() != null) {
			List<String> campaignIds = oneOffDto.getCampaignIdList();
			UpdateCampaignSettings settings = oneOffDto.getUpdateCampaignSettings();
			for (String cid : campaignIds) {
				try {
					Campaign campaign = campaignRepository.findOneById(cid);

					if (settings.getLeadSortOrder() != null && !settings.getLeadSortOrder().toString().isEmpty()) {
						if (campaign.getTeam() != null) {
							campaign.getTeam().setLeadSortOrder(settings.getLeadSortOrder());
						}
					}
					if (settings.getCallSpeedPerMinPerAgent() != null
							&& !settings.getCallSpeedPerMinPerAgent().toString().isEmpty()) {
						if (campaign.getTeam() != null) {
							campaign.getTeam().setCallSpeedPerMinPerAgent(settings.getCallSpeedPerMinPerAgent());
						}
					}
					if (settings.getCampaignSpeed() > 0.0) {
						if (campaign.getTeam() != null) {
							campaign.getTeam().setCampaignSpeed(settings.getCampaignSpeed());
						}
					}
					if (settings.getDialerMode() != null && settings.getDialerMode().toString() != null
							&& !settings.getDialerMode().toString().isEmpty()) {
						campaign.setDialerMode(settings.getDialerMode());
					}
					if (settings.getHostingServer() != null && !settings.getHostingServer().isEmpty()) {
						campaign.setHostingServer(settings.getHostingServer());
					}
					if (settings.getRetrySpeed() != null && !settings.getRetrySpeed().isEmpty()) {
						campaign.setRetrySpeed(settings.getRetrySpeed());
					}
					if (settings.getCallControlProvider() != null && !settings.getCallControlProvider().isEmpty()) {
						campaign.setCallControlProvider(settings.getCallControlProvider());
					}
					if (settings.getDataSourcePriority() != null && !settings.getDataSourcePriority().isEmpty()) {
						campaign.setDataSourcePriority(settings.getDataSourcePriority());
					}
					if (settings.getEnableMachineAnsweredDetection() != null
							&& !settings.getEnableMachineAnsweredDetection().isEmpty()) {
						campaign.setEnableMachineAnsweredDetection(
								Boolean.valueOf(settings.getEnableMachineAnsweredDetection()));
					}
					if (settings.getDisableAMDOnQueued() != null && !settings.getDisableAMDOnQueued().isEmpty()) {
						campaign.setDisableAMDOnQueued(Boolean.valueOf(settings.getDisableAMDOnQueued()));
					}
					if (settings.getSipProvider() != null && !settings.getSipProvider().isEmpty()) {
						campaign.setSipProvider(settings.getSipProvider());
					}
					if (settings.getUseSlice() != null && !settings.getUseSlice().isEmpty()) {
						campaign.setUseSlice(Boolean.valueOf(settings.getUseSlice()));
					}
					if (settings.getHideEmail() != null && !settings.getHideEmail().isEmpty()) {
						campaign.setHideEmail(Boolean.valueOf(settings.getHideEmail()));
					}
					if (settings.getHidePhone() != null && !settings.getHidePhone().isEmpty()) {
						campaign.setHidePhone(Boolean.valueOf(settings.getHidePhone()));
					}
					if (settings.getQaRequired() != null && !settings.getQaRequired().isEmpty()) {
						campaign.setQaRequired(Boolean.valueOf(settings.getQaRequired()));
					}
					if (settings.getSimpleDisposition() != null && !settings.getSimpleDisposition().isEmpty()) {
						campaign.setSimpleDisposition(Boolean.valueOf(settings.getSimpleDisposition()));
					}
					if (settings.getRetrySpeedEnabled() != null && !settings.getRetrySpeedEnabled().isEmpty()) {
						campaign.setRetrySpeedEnabled(Boolean.valueOf(settings.getRetrySpeedEnabled()));
					}
					if (settings.getIndirectBuyStarted() != null && !settings.getIndirectBuyStarted().isEmpty()) {
						campaign.setIndirectBuyStarted(Boolean.valueOf(settings.getIndirectBuyStarted()));
					}
					if (settings.getLeadIQ() != null && !settings.getLeadIQ().isEmpty()) {
						campaign.setLeadIQ(Boolean.valueOf(settings.getLeadIQ()));
					}
					if (settings.getAbm() != null && !settings.getAbm().isEmpty()) {
						campaign.setABM(Boolean.valueOf(settings.getAbm()));
					}
					if (settings.getType() != null && !settings.getType().isEmpty()) {
						campaign.setType(CampaignTypes.valueOf(settings.getType()));
					}
					if (settings.getCallableEvent() != null && !settings.getCallableEvent().isEmpty()) {
						campaign.setCallableEvent(settings.getCallableEvent());
					}
					if (settings.getSourceCallingPriority() != null && !settings.getSourceCallingPriority().isEmpty()) {
						campaign.setSourceCallingPriority(settings.getSourceCallingPriority());
					}
					if (settings.getCampaignGroupIds() != null && settings.getCampaignGroupIds().size() > 0) {
						campaign.setCampaignGroupIds(settings.getCampaignGroupIds());
						;
					}
					if (settings.getNumberOfTopRecords() > 0) {
						campaign.setNumberOfTopRecords(settings.getNumberOfTopRecords());
					}
					campaignRepository.save(campaign);
				} catch (Exception e) {
					e.printStackTrace();
				}
				returnmsg = "updated campaigns";
			}
		} else if (oneOffDto.getUpdateType().equalsIgnoreCase("Organization")) {
			returnmsg = "Failed to update Organization";
			if (oneOffDto.getOrganizationId() != null && !oneOffDto.getOrganizationId().isEmpty()) {
				Organization org = organizationRepository.findOneById(oneOffDto.getOrganizationId());
				if (org != null) {
					if (oneOffDto.getProspectUploadEnabled() != null
							&& !oneOffDto.getProspectUploadEnabled().isEmpty()) {
						org.setProspectUploadEnabled(Boolean.valueOf(oneOffDto.getProspectUploadEnabled()));
					}
					if (oneOffDto.getUpperThreshold() > 0) {
						org.getCampaignDefaultSetting().setUpperThreshold(oneOffDto.getUpperThreshold());
					}
					if (oneOffDto.getLowerThreshold() > 0) {
						org.getCampaignDefaultSetting().setLowerThreshold(oneOffDto.getLowerThreshold());
					}
					organizationRepository.save(org);
				}
			}
			returnmsg = "updated Organization";
		}
		return returnmsg;
	}

	@Override
	public String moveDataToNewAbm(OneOffDTO oneOffDto) {
		String returnmsg = "";
		if (oneOffDto.getCampaignIdList() != null && oneOffDto.getCampaignIdList().size() > 0) {
			List<String> campaignIds = oneOffDto.getCampaignIdList();
			for (String cid : campaignIds) {
				AbmList abmList = abmListRepository.findABMListListByCampaignIdAndStatus(cid);
				if (abmList != null && abmList.getCompanyList() != null && abmList.getCompanyList().size() > 0) {
					List<KeyValuePair<String, String>> kvList = abmList.getCompanyList();
					for (KeyValuePair<String, String> kv : kvList) {
						AbmListNew abmNew = new AbmListNew();
						abmNew.setStatus("ACTIVE");
						abmNew.setCampaignId(cid);
						if (kv.getValue() != null)
							abmNew.setCompanyId(kv.getValue().toString());
						else
							abmNew.setCompanyId("NOT_FOUND");

						if (kv.getKey() != null && !kv.getKey().isEmpty()) {
							abmNew.setCompanyName(kv.getKey().toString());
							abmListNewRepository.save(abmNew);
						}
					}
				} else {
					returnmsg = returnmsg + "No ABM found for campaignId" + cid;

				}

			}
		} else {
			returnmsg = returnmsg + "No ABM found";
		}

		if (returnmsg != null && !returnmsg.isEmpty()) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCI To PCL Report Status - Errors",
					returnmsg);
		} else {
			returnmsg = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Copy PCI To PCL Report Status", returnmsg);
		}
		return returnmsg;
	}

	@Override
	public String createProspectUsingFaker(String campaignId, int number) {
		String error = null;
		try {
			List<String> tempNumber = Arrays.asList("+12314621123", "+18127022035", "+13308178408", "+17659815065");
			Campaign campaign = campaignRepository.findOneById(campaignId);
			Faker f = new Faker(new Locale("en-US"));
			for (int i = 0; i < number; i++) {
				String randomUUID = getUniqueProspect();
				String randomeUUIDWithoutHyphon = StringUtils.replace(randomUUID.toString(), "-", "");
				ProspectCallLog prospectCallLog = new ProspectCallLog();

				ProspectCall prospectCall = new ProspectCall();
				prospectCall.setProspectCallId(randomUUID);

				Prospect prospect = new Prospect();
				prospect.setSource("TEST");
				prospect.setSourceId(randomeUUIDWithoutHyphon);
				prospect.setFirstName(f.name().firstName());
				prospect.setLastName(f.name().lastName());
				prospect.setCompany(f.name().firstName() + ".com");
				prospect.setTitle(f.name().title());
				prospect.setPhone(tempNumber.get(new Random().nextInt(tempNumber.size())));
				prospect.setCountry("United States");
				prospect.setTimeZone("US/Pacific");
				prospect.setStateCode("ZZ");
				prospect.setCustomAttribute("domain", "www." + f.name().firstName() + ".com");
				prospect.setSourceCompanyId(randomeUUIDWithoutHyphon);
				prospect.setEncrypted(campaign.isEncrypted());
				prospect.setDirectPhone(false);
				prospect.setEu(false);
				prospect.setEmail(f.internet().safeEmailAddress());
				prospectCall.setProspect(prospect);

				prospectCall.setCampaignId(campaignId);
				prospectCall.setPartnerId(campaign.getOrganizationId());
				// String strCurrentDateYMD = null;
				// try {
				// strCurrentDateYMD = XtaasDateUtils.convertToTimeZone(new Date(),
				// "US/Pacific", "yyyyMMdd");
				// } catch (ParseException e) {
				// logger.error("Error occurred in converting date into string");
				// }
				// prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01"));
				// prospectCall.setQualityBucket("QUEUED||C-Level");
				// prospectCall.setQualityBucketSort(2551);
				// prospectCall.setCallRetryCount(0);

				
				prospectCallLog.setProspectCall(prospectCall);
				prospectCallLog.setStatus(ProspectCallStatus.QUEUED);

				prospectCallLogRepository.save(prospectCallLog);
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = ExceptionUtils.getStackTrace(e);
		}

		if (error != null) {
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Create Prospect Using Faker - Errors", error);
		} else {
			error = "Success";
			XtaasEmailUtils.sendEmail(toEmail, "data@xtaascorp.com", "Create Prospect Using Faker Status", error);
		}

		return "Success";
	}

	@Override
	public String moveInvalidSalutary() {
		logger.info("salutary bucket sorting batch number : ");
		boolean found = true;
		int i = 0;
		while (found) {
			// ,Direction.ASC, "prospectCall.prospect.customAttributes.domain
			Pageable pageable = PageRequest.of(i, 10000);
			List<ProspectSalutary> salutaryFromDB = prospectSalutaryRepository.findMissingStates("N/A", pageable);
			if (salutaryFromDB != null && salutaryFromDB.size() > 0) {
				List<ProspectSalutaryInvalid> slInvaliList = new ArrayList<ProspectSalutaryInvalid>();
				for (ProspectSalutary ps : salutaryFromDB) {
					ProspectSalutaryInvalid psi = new ProspectSalutaryInvalid();
					psi.setProspectCall(ps.getProspectCall());
					psi.setReason("Invalid State");
					psi.setStatus(
							com.xtaas.db.entity.ProspectSalutaryInvalid.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					slInvaliList.add(psi);
				}
				if (slInvaliList != null && slInvaliList.size() > 0) {
					salutaryInvalidRepository.saveAll(slInvaliList);
					prospectSalutaryRepository.deleteAll(salutaryFromDB);
					logger.info("Salutary Moved and deleted" + i);

				}
				i++;
			} else {
				found = false;
			}
		}
		return "Success";
	}

	@Override
	public String telnyxMissingRecordings(OneOffDTO oneOffDTO) {
		Map<String, ProspectCallLog> telnyxUidMap = new HashMap<String, ProspectCallLog>();
		boolean found = true;
		int i = 0;
		while (found) {
			Pageable pageable = PageRequest.of(i, 1000);
			List<ProspectCallLog> pclogList = prospectCallLogRepository.findTelnyxMissingRecordings(
					oneOffDTO.getCampaignIdList(), oneOffDTO.getDispositionStatus(), pageable);
			if (pclogList != null && pclogList.size() > 0) {
				for (ProspectCallLog pclog : pclogList) {
					if (pclog.getProspectCall().getTwilioCallSid() != null
							&& !pclog.getProspectCall().getTwilioCallSid().isEmpty()) {
						telnyxUidMap.put(pclog.getProspectCall().getTwilioCallSid(), pclog);
					}
				}
				if (telnyxUidMap.size() > 0) {
					getRecordingsFromTelnyx(telnyxUidMap);
				}
				i++;
			} else {
				found = false;
			}
		}
		return "Success";
	}

	public void getRecordingsFromTelnyx(Map<String, ProspectCallLog> telnyxUidMap) {

		TelnyxRecordingObject telnyxObject = null;
		String url = "https://api.telnyx.com/recordings";
		try {
			for (Map.Entry<String, ProspectCallLog> entry : telnyxUidMap.entrySet()) {
				// url =url+"?call_id="+entry.getKey();
				StringBuilder response = new StringBuilder();
				String urlWithParams = url + "?call_id=" + entry.getKey();
				;
				ProspectCallLog pclog = entry.getValue();
				HttpsURLConnection httpClient = (HttpsURLConnection) new URL(urlWithParams).openConnection();
				// add request header
				httpClient.setRequestMethod("GET");
				httpClient.setRequestProperty("x-api-user", "kchugh@xtaascorp.com");
				httpClient.setRequestProperty("x-api-token", "p4o-wVSBSoy-EmjsOdE2Fg");

				httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
				httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
				httpClient.setRequestProperty("Accept", "application/json");
				int responseCode = httpClient.getResponseCode();
				logger.debug("\nSending 'POST' request to URL : " + urlWithParams);
				logger.debug("Post parameters : " + urlWithParams);
				logger.debug("Response Code : " + responseCode);
				try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
					String line;

					while ((line = in.readLine()) != null) {
						response.append(line);
					}
					logger.debug(response.toString());
				}

				ProspectCallLog prospect = prospectCallLogRepository
						.findByProspectCallId(pclog.getProspectCall().getProspectCallId());

				if (response != null) {
					ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
					mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
					telnyxObject = mapper.readValue(response.toString(), TelnyxRecordingObject.class);
					if (telnyxObject.getData() != null && telnyxObject.getData().size() > 0) {
						Data data = telnyxObject.getData().get(0);
						if (data.getDownload_urls() != null && data.getDownload_urls().getWav() != null) {
							String awsUrl = downloadRecordingsToAws.downloadTelnyxRecording(
									data.getDownload_urls().getWav(), pclog.getProspectCall().getProspectCallId(),
									pclog.getProspectCall().getCampaignId(), true);
							prospect.getProspectCall().setRecordingUrlAws(awsUrl);
						} else {
							prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
						}
					} else {
						prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
					}
				} else {
					prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
				}
				prospectCallLogRepository.save(prospect);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return response;
	}

	@Override
	public String startEmail(OneOffDTO oneOffDTO) {
		Runnable runnable = () -> {
			try {
				logger.info("This Program is started for send email for email campaigns.");

				sendGridServiceImpl.sendSendGridEmail();
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		return "Success";
	}

	@Override
	public String convertProspectcalllogToEncryptedProspectcalllog(OneOffDTO oneOffDTO) {
		Runnable runnable = () -> {
			try {
				logger.info("This Program is started for Encrypt Prospectcalllog collection.");
				boolean found = true;
				int i = 0;
				while (found) {
					Pageable pageable = PageRequest.of(i, 1000);
					List<ProspectCallLog> pclogList = prospectCallLogRepository
							.findProspectCallLogByCampaignId(oneOffDTO.getCampaignIdList(), pageable);
					if (pclogList != null && pclogList.size() > 0) {
						for (ProspectCallLog pclog : pclogList) {
							pclog.getProspectCall().getProspect().setEncrypted(true);
							EncryptedProspectCallLog epcl = new EncryptedProspectCallLog();
							epcl.setProspectCall(pclog.getProspectCall());
							epcl.setStatus(com.xtaas.db.entity.EncryptedProspectCallLog.ProspectCallStatus
									.valueOf(pclog.getStatus().toString()));
							epcl.setDataSlice(pclog.getDataSlice());
							epcl.setQaFeedback(pclog.getQaFeedback());
							epcl.setCompanyAddress(pclog.getCompanyAddress());
							epcl.setCallbackDate(pclog.getCallbackDate());
							// Encrypt FirstName LastName Phone Email CompanyPhone
							epcl.getProspectCall().getProspect()
									.setFirstName(pclog.getProspectCall().getProspect().getFirstName());
							epcl.getProspectCall().getProspect()
									.setLastName(pclog.getProspectCall().getProspect().getLastName());
							epcl.getProspectCall().getProspect()
									.setPhone(pclog.getProspectCall().getProspect().getPhone());
							epcl.getProspectCall().getProspect()
									.setEmail(pclog.getProspectCall().getProspect().getEmail());
							epcl.getProspectCall().getProspect()
									.setCompanyPhone(pclog.getProspectCall().getProspect().getCompanyPhone());
							encryptedProspectCallLogRepository.save(epcl);
						}
						i++;
					} else {
						found = false;
					}
				}
				logger.info("This Program is completed for Encrypt Prospectcalllog collection.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		return "Success";
	}

	@Override
	public String fixCrmMapping(OneOffDTO of) {
		try {
			int counter = 1;
			String organizationId = of.getOrganizationId();
			CRMMapping orgMapping = crmMappingRepository.findOneById(organizationId);
			List<CRMAttribute> attributeMapping = orgMapping.getAttributeMapping();
			List<CRMAttribute> fixAttributeMapping = new ArrayList<CRMAttribute>();
			for (CRMAttribute cm : attributeMapping) {
				if (cm.getCampaignId() != null && !cm.getCampaignId().isEmpty()) {
					fixAttributeMapping.add(cm);
				} else {
					cm.setCampaignId("NOCAMPAIGNIDFOUND");
					fixAttributeMapping.add(cm);
					logger.info("CAMPAIGN ID NOT FOUND FOR ORG : " + organizationId + " counter L " + counter++);
				}
			}
			if (fixAttributeMapping != null && fixAttributeMapping.size() > 0) {
				orgMapping.setAttributeMapping(new ArrayList<CRMAttribute>(fixAttributeMapping));
			}
			crmMappingRepository.save(orgMapping);

		} catch (Exception e) {

		}

		return "SUCCESS";
	}

	@Override
	public String updateLatestStatus(OneOffDTO of) {
		try {

			String fromDate = of.getFromDate();
			String toDate = of.getToDate();
			String campaignId = of.getPrimaryCampaignId();
			String dispositionStatus = of.getDispositionStatus().get(0);
			Date startDate = XtaasDateUtils.getDate(fromDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			// logger.debug(startDateInUTC);
			Date endDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");

			List<ProspectCallInteraction> pciList = prospectCallInteractionRepository.findBychecksleads(startDate,
					endDate, dispositionStatus, campaignId);

			/*
			 * Comparator<ProspectCallInteraction> byupdatedDate = new
			 * Comparator<ProspectCallInteraction>() { public int
			 * compare(ProspectCallInteraction c1, ProspectCallInteraction c2) { return
			 * Long.valueOf(c1.getUpdatedDate().getTime()).compareTo(c2.getUpdatedDate().
			 * getTime()); } };
			 */

			Map<String, List<ProspectCallInteraction>> pciMap = new HashMap<String, List<ProspectCallInteraction>>();

			for (ProspectCallInteraction pci : pciList) {
				Pageable pageable = PageRequest.of(0, 100, Direction.DESC, "updatedDate");
				List<ProspectCallInteraction> prospectInteractionFromDB = prospectCallInteractionRepository
						.findAllByProspectCallId(pci.getProspectCall().getProspectCallId(), pageable);

				/*
				 * ProspectCallInteraction pci = pciList.get(i); List<ProspectCallInteraction>
				 * pclist = new ArrayList<ProspectCallInteraction>();
				 * if(pciMap.get(pci.getProspectCall().getProspectCallId())!=null) { pclist =
				 * pciMap.get(pci.getProspectCall().getProspectCallId()); pclist.add(pci); }else
				 * { pclist.add(pci); }
				 */
				pciMap.put(pci.getProspectCall().getProspectCallId(), prospectInteractionFromDB);
			}

			StringBuilder sb = new StringBuilder();
			for (Map.Entry<String, List<ProspectCallInteraction>> pmap : pciMap.entrySet()) {
				List<ProspectCallInteraction> pList = pmap.getValue();
				// Collections.sort(pList, byupdatedDate);

				ProspectCallLog pcalllog = prospectCallLogRepository.findByProspectCallId(pmap.getKey());
				if (pcalllog.getProspectCall().getDispositionStatus() != null
						&& !pcalllog.getProspectCall().getDispositionStatus().equalsIgnoreCase(dispositionStatus)) {

					sb.append("prospectCalId : " + pcalllog.getProspectCall().getProspectCallId());
					for (ProspectCallInteraction pinter : pList) {
						if (pinter.getProspectCall().getDispositionStatus() != null && pinter.getProspectCall()
								.getDispositionStatus().equalsIgnoreCase(dispositionStatus)) {
							ProspectStatusCorrection psc = new ProspectStatusCorrection();
							psc.setDataSlice(pinter.getDataSlice());
							psc.setInteractionDate(pinter.getUpdatedDate());
							psc.setInteractionBy(pinter.getUpdatedBy());
							psc.setProspectCall(pinter.getProspectCall());
							prospectStatusCorrectionRepository.save(psc);
						}
					}

				}
			}
			XtaasEmailUtils.sendEmail("hbaba@xtaascorp.com", "data@xtaascorp.com",
					"correction in prospectcalllog for success leads", sb.toString());

			return sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "SUCCESS";
	}

	@Override
	public String emailToMD5(OneOffDTO oneOffDTO) {
		/*
		 * Prospect pro = new Prospect(); pro.setEmail("abaker@eqr.com"); Prospect p =
		 * insideviewService.getManualProspect(pro);
		 * 
		 * Prospect pro1 = new Prospect(); pro1.setFirstName("Umberto");
		 * pro1.setLastName("Milletti"); pro1.setCompany("Sand Hill Group");
		 * 
		 * Prospect p1 = insideviewService.getManualProspect(pro1);
		 * 
		 * Prospect pro2 = new Prospect(); pro2.setFirstName("Umberto");
		 * pro2.setLastName("Milletti"); pro2.setCustomAttribute("domain",
		 * "insideview.com"); Prospect p2 = insideviewService.getManualProspect(pro2);
		 */

		boolean exists = true;
		int i = 0;
		while (exists) {
			Pageable pageableQ = PageRequest.of(i, 10000);
			List<EmailDistinctHash> emailsList = emailDistinctHashRepository.findByStatus(false, pageableQ);
			if (emailsList != null && emailsList.size() > 0) {
				for (EmailDistinctHash em : emailsList) {
					if (em != null && em.getEmail() != null && !em.getEmail().isEmpty()) {
						String md5Hex = DigestUtils.md5Hex(em.getEmail());
						// em.setEmailHash(md5Hex);
						EmailHashed emailHashed = new EmailHashed();
						emailHashed.setCallDuration(em.getCallDuration());
						emailHashed.setCallStartTime(em.getCallStartTime());
						emailHashed.setCount(em.getCount());
						emailHashed.setEmail(em.getEmail());
						emailHashed.setEmailHash(md5Hex);
						// em.setStatus(true);
						emailHashedRepository.save(emailHashed);
					}
				}
				i++;
			} else {
				XtaasEmailUtils.sendEmail("hbaba@xtaascorp.com", "data@xtaascorp.com", "EMAIL HASH job is DONE.",
						"done email hashing");
				exists = false;
			}
		}

		return "success";
	}

	@Override
	public String startCampaignDataBuy(OneOffDTO oneOffDTO) {
		List<Campaign> campaigns = campaignRepository.findCampaignByIds(oneOffDTO.getCampaignIdList());
		if (campaigns != null && campaigns.size() > 0) {
			for (Campaign campaign : campaigns) {
				DataBuyQueue dataBuyQueue = dataBuyQueueRepository.findByCampaignIdAndStatus(campaign.getId(),
						DataBuyQueueStatus.INPROCESS.toString());
				if (dataBuyQueue != null) {
					throw new IllegalArgumentException("Data Buy Already in Process Please Check After Sometime.");
				}
				DataBuyQueue dbq = new DataBuyQueue();
				dbq.setCampaignId(campaign.getId());
				dbq.setCampaignName(campaign.getName());
				dbq.setStatus(DataBuyQueueStatus.QUEUED.toString());
				dbq.setBuyProspect(true);
				dbq.setJobRequestTime(new Date());
				dbq.setRequestCount(new Long(oneOffDTO.getTotalPurchaseCount()));
				dbq.setOrganizationId(campaign.getOrganizationId());
				QualificationCriteria cc = campaign.getQualificationCriteria();
				List<Expression> expressions = cc.getExpressions();
				dbq.setCampaignCriteriaDTO(expressions);
				dataBuyQueueRepository.save(dbq);
				logger.info("Data buy thread started via API");
			}
		} else {
			throw new IllegalArgumentException("Campaigns doesnt exists in the system.");
		}
		return "Ok";
	}

	@Override
	public String getCompanyInfoFromEverstring(MultipartFile file) {
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				try {
					File f = convertMultiPartToFile(file);
					Runnable r = () -> {
						try {
							book = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(f);
							sheet = book.getSheetAt(0);
							readExcelFile();
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("Error while reading xlsx file : ", e);
						}
					};
					Thread t = new Thread(r);
					t.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				return "Please Upload Xlsx File";
			}
		} else {
			return "File cannot be empty.";
		}
		return "Success";
	}

	private void readExcelFile() throws IOException, InterruptedException {
		int totalRowsCount = sheet.getLastRowNum() + 1;
		int batchsize = 0;
		if (totalRowsCount > 0) {
			batchsize = totalRowsCount / 500;
			long modSize = totalRowsCount % 500;
			if (modSize > 0) {
				batchsize = batchsize + 1;
			}
		}
		logger.info("Everstring batch size : " + batchsize);
		if (batchsize > 0) {
			for (int i = 0; i < batchsize; i++) {
				logger.info("Everstring processing batch no : " + i);
				Map<String, String> searchDomainList = new HashMap<String, String>();
				for (Row row : sheet) {
					for (int j = 0; j < 1; j++) {
						Cell searchCell = row.getCell(j);
						if (searchCell != null && searchCell.getCellType() != CellType.BLANK
								&& searchCell.getCellType() == CellType.STRING) {
							Cell salutoryCell = row.getCell(1);
							String salutoryDomain = "";
							if (salutoryCell != null && salutoryCell.getCellType() != CellType.BLANK
									&& salutoryCell.getCellType() == CellType.STRING) {
								salutoryDomain = salutoryCell.getStringCellValue().trim();
							}
							searchDomainList.put(searchCell.getStringCellValue().toLowerCase().trim(), salutoryDomain);
						}
					}
				}
				if (searchDomainList != null && !searchDomainList.isEmpty() && searchDomainList.size() > 0) {
					List<Map<String, String>> batchSearchDomain = noOfBatches(searchDomainList, 25);
					callEverStringMultipleCompanySearchAPI(batchSearchDomain, i);
				}
			}
		}
	}

	private void callEverStringMultipleCompanySearchAPI(List<Map<String, String>> batchSearchDomain, int batchNo) {
		for (Map<String, String> batchMap : batchSearchDomain) {
			List<EverStringDomainDTO> requestDomainList = new ArrayList<EverStringDomainDTO>();
			for (Map.Entry<String, String> entry : batchMap.entrySet()) {
				requestDomainList.add(new EverStringDomainDTO(entry.getKey()));
			}
			EverStringMultipleCompanyRequest dto = new EverStringMultipleCompanyRequest();
			dto.setCompanies(requestDomainList);
			EverStringRealTimeResponseDTO responseDTO = null;
			try {
				acquire();
				responseDTO = httpService.post(XtaasConstants.REALTIMEURL, headers(),
						new ParameterizedTypeReference<EverStringRealTimeResponseDTO>() {
						}, new ObjectMapper().writeValueAsString(dto));
				if (responseDTO != null && responseDTO.getData() != null) {
					saveCompanyInfoListInDB(responseDTO.getData(), batchMap);
					setDomainStatus(batchMap, "NOT_FOUND");
				} else {
					setDomainStatus(batchMap, "ERROR");
				}
			} catch (Exception e) {
				e.printStackTrace();
				setDomainStatus(batchMap, "ERROR");
				logger.error("Everstring error in batch : " + batchNo, e);
			}
		}
	}

	private void setDomainStatus(Map<String, String> batchMap, String status) {
		if (batchMap != null && !batchMap.isEmpty() && batchMap.size() > 0) {
			for (Map.Entry<String, String> entry : batchMap.entrySet()) {
				SalutoryEverstring seObj = new SalutoryEverstring();
				seObj.setSearchDomain(entry.getKey());
				seObj.setSalutoryDomain(entry.getValue());
				seObj.setStatus(status);
				salutoryEverstringRepository.save(seObj);
			}
		}
	}

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");
		headers.add("Authorization", XtaasConstants.TOKEN);
		return headers;
	}

	private void saveCompanyInfoListInDB(List<EverStringRealTimeCompanyDTO> companyDTO, Map<String, String> batchMap) {
		try {
			for (EverStringRealTimeCompanyDTO realTimeCompany : companyDTO) {
				if (realTimeCompany != null && realTimeCompany.getES_PrimaryWebsite() != null) {
					SalutoryEverstring company = new SalutoryEverstring(realTimeCompany.getES_AdvancedInsights(),
							realTimeCompany.getES_AlexaRank(), realTimeCompany.getES_City(),
							realTimeCompany.getES_CompanyListNames(), realTimeCompany.getES_CompanyPhone(),
							realTimeCompany.getES_Country(), realTimeCompany.getES_ECID(),
							realTimeCompany.getES_Employee(), realTimeCompany.getES_EmployeeBand(),
							realTimeCompany.getES_EstimatedAge(), realTimeCompany.getES_FacebookUrl(),
							realTimeCompany.getES_Industry(), realTimeCompany.getES_Intent(),
							realTimeCompany.getES_IntentAggregateScore(), realTimeCompany.getES_IntentByTier(),
							realTimeCompany.getES_IntentNumByTier(), realTimeCompany.getES_IntentStr(),
							realTimeCompany.getES_IntentTime(), realTimeCompany.getES_Keywords(),
							realTimeCompany.getES_LinkedInUrl(), realTimeCompany.getES_LocationID(),
							realTimeCompany.getES_MatchName(), realTimeCompany.getES_MatchReasonBuildingName(),
							realTimeCompany.getES_MatchReasonBuildingNumber(),
							realTimeCompany.getES_MatchReasonBusinessType(), realTimeCompany.getES_MatchReasonCity(),
							realTimeCompany.getES_MatchReasonCompanyPhone(), realTimeCompany.getES_MatchReasonCountry(),
							realTimeCompany.getES_MatchReasonDirectional(), realTimeCompany.getES_MatchReasonName(),
							realTimeCompany.getES_MatchReasonRoadName(), realTimeCompany.getES_MatchReasonRoadType(),
							realTimeCompany.getES_MatchReasonState(), realTimeCompany.getES_MatchReasonUnit(),
							realTimeCompany.getES_MatchReasonWebsite(), realTimeCompany.getES_MatchReasonZip(),
							realTimeCompany.getES_MatchScore(), realTimeCompany.getES_Models(),
							realTimeCompany.getES_NAICS2(), realTimeCompany.getES_NAICS2Description(),
							realTimeCompany.getES_NAICS4(), realTimeCompany.getES_NAICS4Description(),
							realTimeCompany.getES_NAICS6(), realTimeCompany.getES_NAICS6Description(),
							realTimeCompany.getES_Name(), realTimeCompany.getES_NumLocations(),
							realTimeCompany.getES_NumSurgingTopics(), realTimeCompany.getES_PrimaryWebsite(),
							realTimeCompany.getES_Revenue(), realTimeCompany.getES_RevenueBand(),
							realTimeCompany.getES_SIC2(), realTimeCompany.getES_SIC2Description(),
							realTimeCompany.getES_SIC3(), realTimeCompany.getES_SIC3Description(),
							realTimeCompany.getES_SIC4(), realTimeCompany.getES_SIC4Description(),
							realTimeCompany.getES_SimilarCompanies(), realTimeCompany.getES_State(),
							realTimeCompany.getES_Street(), realTimeCompany.getES_Top5NAICS(),
							realTimeCompany.getES_TwitterUrl(), realTimeCompany.getES_YearStarted(),
							realTimeCompany.getES_Zip(), realTimeCompany.getError_messages(),
							realTimeCompany.getInputId());
					if (batchMap.containsKey(realTimeCompany.getES_PrimaryWebsite())) {
						company.setSearchDomain(realTimeCompany.getES_PrimaryWebsite());
						company.setSalutoryDomain(batchMap.get(realTimeCompany.getES_PrimaryWebsite()));
						company.setStatus("FOUND");
						batchMap.remove(realTimeCompany.getES_PrimaryWebsite());
					}
					salutoryEverstringRepository.save(company);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static List<Map<String, String>> noOfBatches(Map<String, String> list, int size) {
		List<Map<String, String>> listOfBetchList = new ArrayList<Map<String, String>>();
		Map<String, String> betchList = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : list.entrySet()) {
			betchList.put(entry.getKey(), entry.getValue());
			if (betchList.size() == size) {
				listOfBetchList.add(betchList);
				betchList = new HashMap<String, String>();
			}
		}
		if (betchList.isEmpty() == false) {
			listOfBetchList.add(betchList);
		}
		return listOfBetchList;
	}

	@Override
	public void acquire() {
		rateLimiter.acquire();
	}

	public String getHostName(String url) {
		if (url != null && !url.isEmpty()) {
			url = url.toLowerCase();
			if (url.contains("http://") || url.contains("https://") || url.contains("HTTP://")
					|| url.contains("HTTPS://")) {

			} else {
				url = "http://" + url;
			}
			try {
				URI uri = new URI(url);
				String hostname = uri.getHost();
				if (hostname != null && !hostname.isEmpty()) {
					if (hostname.contains("www.") || hostname.contains("WWW.")) {
						return hostname = hostname.substring(4, hostname.length());
					} else {
						return hostname;
					}
				}
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	@Override
	public String createSalutaryCompany() {
		try {
			logger.info("This Program is started for Create Salutary Company.");
			boolean found = true;
			int i = 0;
			while (found) {
				Pageable pageable = PageRequest.of(i, 1000);
				List<ProspectSalutary> sList = prospectSalutaryRepository.findSalutaryProspectsPage(pageable);
				if (sList != null && !sList.isEmpty() && sList.size() > 0) {
					for (ProspectSalutary s : sList) {
						try {
							if (s.getProspectCall().getProspect().getCustomAttributeValue("domain") != null) {
								List<SalutaryCompany> dbList = salutaryCompanyRepository.findCompanyDomain(
										s.getProspectCall().getProspect().getCustomAttributeValue("domain").toString());
								if (dbList == null || dbList.isEmpty()) {
									saveSalutaryCompanyInfo(s);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					i++;
				} else {
					found = false;
				}
			}
			logger.info("This Program is completed for Create Salutary Company");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Success";
	}

	private void saveSalutaryCompanyInfo(ProspectSalutary ps) {
		SalutaryCompany sc = new SalutaryCompany();
		if (ps.getProspectCall().getProspect().getSourceCompanyId() != null
				&& !ps.getProspectCall().getProspect().getSourceCompanyId().isEmpty()) {
			sc.setCompanyId(ps.getProspectCall().getProspect().getSourceCompanyId());
		}
		if (ps.getProspectCall().getProspect().getCompany() != null
				&& !ps.getProspectCall().getProspect().getCompany().isEmpty()) {
			sc.setCompanyName(ps.getProspectCall().getProspect().getCompany());
		}
		if (ps.getProspectCall().getProspect().getCustomAttributeValue("domain") != null) {
			sc.setCompanyDomain(ps.getProspectCall().getProspect().getCustomAttributeValue("domain").toString());
		}
		if (ps.getProspectCall().getProspect().getCity() != null
				&& !ps.getProspectCall().getProspect().getCity().isEmpty()) {
			sc.setCity(ps.getProspectCall().getProspect().getCity());
		}
		if (ps.getProspectCall().getProspect().getCountry() != null
				&& !ps.getProspectCall().getProspect().getCountry().isEmpty()) {
			sc.setCountry(ps.getProspectCall().getProspect().getCountry());
		}
		if (ps.getProspectCall().getProspect().getStateCode() != null
				&& !ps.getProspectCall().getProspect().getStateCode().isEmpty()) {
			sc.setStateCode(ps.getProspectCall().getProspect().getStateCode());
		}
		if (ps.getProspectCall().getProspect().getIndustry() != null
				&& !ps.getProspectCall().getProspect().getIndustry().isEmpty()) {
			sc.setIndustry(ps.getProspectCall().getProspect().getIndustry());
		}
		if (ps.getProspectCall().getProspect().getCompanySIC() != null
				&& !ps.getProspectCall().getProspect().getCompanySIC().isEmpty()
				&& ps.getProspectCall().getProspect().getCompanySIC().size() > 0) {
			sc.setCompanySIC(ps.getProspectCall().getProspect().getCompanySIC());
		}
		if (ps.getProspectCall().getProspect().getCompanyNAICS() != null
				&& !ps.getProspectCall().getProspect().getCompanyNAICS().isEmpty()
				&& ps.getProspectCall().getProspect().getCompanyNAICS().size() > 0) {
			sc.setCompanyNAICS(ps.getProspectCall().getProspect().getCompanyNAICS());
		}
		if (ps.getProspectCall().getProspect().getCustomAttributeValue("minRevenue") != null) {
			sc.setMinRevenue(Double
					.valueOf(ps.getProspectCall().getProspect().getCustomAttributeValue("minRevenue").toString()));
		}
		if (ps.getProspectCall().getProspect().getCustomAttributeValue("maxRevenue") != null) {
			sc.setMaxRevenue(Double
					.valueOf(ps.getProspectCall().getProspect().getCustomAttributeValue("maxRevenue").toString()));
		}
		if (ps.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null) {
			sc.setMinEmployeeCount(Double.valueOf(
					ps.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString()));
		}
		if (ps.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") != null) {
			sc.setMaxEmployeeCount(Double.valueOf(
					ps.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString()));
		}
		salutaryCompanyRepository.save(sc);
	}

	private void getCompanyInfoFromZoomInfo(String companyName, String salutoryDomain) throws IOException {
		try {
			URL url;
			String previewUrl = buildCompanyUrl(companyName);
			logger.debug(previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse zoomInfoResponse = getResponse(response);
			insertCompanyMasterDetail(zoomInfoResponse, salutoryDomain);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String buildCompanyUrl(String companyName) {
		StringBuilder queryString = new StringBuilder();

		if (companyName != null)
			queryString.append("companyName=" + companyName.trim());

		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;

		String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
		String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
				+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
				+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
				+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
				+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
				+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
		return url;
	}

	private void insertCompanyMasterDetail(ZoomInfoResponse companyResponse, String salutoryDomain) {
		double minRevenue = 0;
		double maxRevenue = 0.0;
		double minEmployeeCount = 0;
		double maxEmployeeCount = 0;
		Map<String, Double> sortedZoomEmployeeAndRevenue = new HashMap<String, Double>();
		CompanyMaster cm = new CompanyMaster();
		if (companyResponse != null && companyResponse.getCompanySearchResponse().getMaxResults() > 0) {
			String zoomCompanyId = companyResponse.getCompanySearchResponse().getCompanySearchResults()
					.getCompanyRecord().get(0).getCompanyId();
			List<CompanyMaster> dbList = companyMasterRepository.findCompanyMasterListByCompanyId(zoomCompanyId);
			if (dbList == null || dbList.isEmpty()) {
				cm.setCompanyName(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyName());
				cm.setCompanyId(zoomCompanyId);
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyAddress() != null) {
					CompanyAddress ca = new CompanyAddress();
					ca = companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
							.getCompanyAddress();
					cm.setCompanyAddress(ca);
				}
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyDescription() != null)
					cm.setCompanyDescription(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyDescription());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyDetailXmlUrl() != null)
					cm.setCompanyDetailXmlUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyDetailXmlUrl());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyEmployeeCount() != null)
					cm.setCompanyEmployeeCount(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyEmployeeCount());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyEmployeeCountRange() != null)
					cm.setCompanyEmployeeCountRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyEmployeeCountRange());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyIndustry() != null)
					cm.setCompanyIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyIndustry());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyNAICS() != null)
					cm.setCompanyNAICS(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyNAICS());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyProductsAndServices() != null)
					cm.setCompanyProductsAndServices(companyResponse.getCompanySearchResponse()
							.getCompanySearchResults().getCompanyRecord().get(0).getCompanyProductsAndServices());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyRanking() != null)
					cm.setCompanyRanking(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyRanking());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyRevenueIn000s() != null)
					cm.setCompanyRevenueIn000s(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyRevenueIn000s());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyRevenue() != null)
					cm.setCompanyRevenue(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyRevenue());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyRevenueRange() != null)
					cm.setCompanyRevenueRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyRevenueRange());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanySIC() != null)
					cm.setCompanySIC(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanySIC());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyTicker() != null)
					cm.setCompanyTicker(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyTicker());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyTopLevelIndustry() != null)
					cm.setCompanyTopLevelIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyTopLevelIndustry());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyType() != null)
					cm.setCompanyType(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyType());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyWebsite() != null)
					cm.setCompanyWebsite(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyWebsite());
				cm.setDefunct(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).isDefunct());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getPhone() != null)
					cm.setPhone(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
							.get(0).getPhone());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getSimilarCompanies() != null)
					cm.setSimilarCompanies(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getSimilarCompanies());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getZoomCompanyUrl() != null)
					cm.setZoomCompanyUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getZoomCompanyUrl());
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyRevenueRange() != null) {
					String[] revenueParts = companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyRevenueRange().split(" - ");

					if (revenueParts.length == 2) {
						minRevenue = getRevenue(revenueParts[0]);
						maxRevenue = getRevenue(revenueParts[1]);

					} else if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
							.get(0).getCompanyRevenueRange().contains("Over")) {
						String revenuePart = companyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanyRevenueRange();
						minRevenue = getRevenue(revenuePart);

					} else if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
							.get(0).getCompanyRevenueRange().contains("Under")) {
						String revenuePart = companyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanyRevenueRange();
						maxRevenue = getRevenue(revenuePart);
					}

					sortedZoomEmployeeAndRevenue.put("minRevenue", minRevenue);
					sortedZoomEmployeeAndRevenue.put("maxRevenue", maxRevenue);
				}
				if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyEmployeeCountRange() != null) {
					String[] employeeCountParts = companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyEmployeeCountRange().split(" - ");
					if (employeeCountParts.length == 2) {
						minEmployeeCount = Double.parseDouble(employeeCountParts[0].trim().replace(",", ""));
						maxEmployeeCount = Double.parseDouble(employeeCountParts[1].trim().replace(",", ""));

					} else if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
							.get(0).getCompanyEmployeeCountRange().contains("Over")) {
						String employeeCountPart = companyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
						minEmployeeCount = Double.parseDouble(employeeCountPart);

					} else if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
							.get(0).getCompanyEmployeeCountRange().contains("Under")) {
						String employeeCountPart = companyResponse.getCompanySearchResponse().getCompanySearchResults()
								.getCompanyRecord().get(0).getCompanyEmployeeCountRange().replaceAll("[^0-9]", "");
						maxEmployeeCount = Double.parseDouble(employeeCountPart);
					}
					sortedZoomEmployeeAndRevenue.put("minEmployeeCount", minEmployeeCount);
					sortedZoomEmployeeAndRevenue.put("maxEmployeeCount", maxEmployeeCount);
				}
				if (sortedZoomEmployeeAndRevenue != null && !sortedZoomEmployeeAndRevenue.isEmpty()
						&& sortedZoomEmployeeAndRevenue.size() > 0) {
					cm.setSortedZoomEmployeeAndRevenue(sortedZoomEmployeeAndRevenue);
				}
				cm.setSalutoryDomain(salutoryDomain);
				cm.setEnrichSource("ZOOMINFO");
				cm.setStatus("FOUND");
				companyMasterRepository.save(cm);
			}
		} else {
			cm.setCompanyName(
					companyResponse.getCompanySearchResponse().getSearchParameters().getParameter().get(0).getValue());
			cm.setStatus("NOT_FOUND");
			cm.setSalutoryDomain(salutoryDomain);
			companyMasterRepository.save(cm);
		}

	}

	@Override
	public String getCompanyInfoFromZoomInfo(MultipartFile file) {
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				try {
					File f = convertMultiPartToFile(file);
					Runnable r = () -> {
						try {
							book = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(f);
							sheet = book.getSheetAt(0);
							readZoomInfoExcelFile();
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("Error while reading xlsx file : ", e);
						}
					};
					Thread t = new Thread(r);
					t.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				return "Please Upload Xlsx File";
			}
		} else {
			return "File cannot be empty.";
		}
		return "Success";
	}

	private void readZoomInfoExcelFile() throws IOException, InterruptedException {
		try {
			int totalRowsCount = sheet.getLastRowNum() + 1;
			int batchsize = 0;
			if (totalRowsCount > 0) {
				batchsize = totalRowsCount / 500;
				long modSize = totalRowsCount % 500;
				if (modSize > 0) {
					batchsize = batchsize + 1;
				}
			}
			if (batchsize > 0) {
				for (int i = 0; i < batchsize; i++) {
					for (Row row : sheet) {
						for (int j = 0; j < 1; j++) {
							Cell searchCell = row.getCell(j);
							if (searchCell != null && searchCell.getCellType() != CellType.BLANK
									&& searchCell.getCellType() == CellType.STRING) {
								Cell salutoryCell = row.getCell(1);
								String salutoryDomain = "";
								if (salutoryCell != null && salutoryCell.getCellType() != CellType.BLANK
										&& salutoryCell.getCellType() == CellType.STRING) {
									salutoryDomain = salutoryCell.getStringCellValue().trim();
								}
								getCompanyInfoFromZoomInfo(
										"www." + searchCell.getStringCellValue().toLowerCase().trim(), salutoryDomain);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String convertEverstringCompanyToMasterCompay() {
		try {
			logger.info("This Program is started for convert EverstringCompany To MasterCompay.");
			boolean found = true;
			int i = 0;
			while (found) {
				Pageable pageable = PageRequest.of(i, 1000);
				List<SalutoryEverstring> dbList = salutoryEverstringRepository
						.findSalutoryEverstringDomainListByPointer(pageable);
				if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
					for (SalutoryEverstring s : dbList) {
						try {
							saveMasterCompanyInfo(s);
							s.setPointer("Enriched");
							salutoryEverstringRepository.save(s);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					i++;
				} else {
					found = false;
				}
			}
			logger.info("This Program is completed for convert EverstringCompany To MasterCompay.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Success";
	}

	private void saveMasterCompanyInfo(SalutoryEverstring s) {
		List<CompanyMaster> dbList = companyMasterRepository
				.findCompanyMasterListByCompanyId(String.valueOf(s.getES_ECID()));
		if (dbList == null || dbList.isEmpty()) {
			CompanyMaster cm = new CompanyMaster();
			Map<String, Double> sortedZoomEmployeeAndRevenue = new HashMap<String, Double>();
			if (s.getES_ECID() > 0) {
				cm.setCompanyId(String.valueOf(s.getES_ECID()));
			}
			if (s.getES_Name() != null && !s.getES_Name().isEmpty()) {
				cm.setCompanyName(s.getES_Name());
			}
			if (s.getES_PrimaryWebsite() != null && !s.getES_PrimaryWebsite().isEmpty()) {
				cm.setCompanyWebsite("www." + s.getES_PrimaryWebsite());
			}
			if (s.getES_CompanyPhone() != null && !s.getES_CompanyPhone().isEmpty()) {
				cm.setPhone(s.getES_CompanyPhone());
			}
			if (s.getES_Street() != null && s.getES_City() != null && s.getES_State() != null
					&& s.getES_Country() != null && s.getES_Zip() != null) {
				CompanyAddress companyAddress = new CompanyAddress();
				companyAddress.setStreet(s.getES_Street());
				companyAddress.setCity(s.getES_City());
				companyAddress.setState(s.getES_State());
				companyAddress.setCountry(s.getES_Country());
				companyAddress.setZip(s.getES_Zip());
				cm.setCompanyAddress(companyAddress);
			}
			if (s.getES_RevenueBand() != null && !s.getES_RevenueBand().isEmpty()) {
				cm.setCompanyRevenueRange(s.getES_RevenueBand());
				Map<String, String[]> revMap = getEverStringRevenueMap(s.getES_RevenueBand());
				sortedZoomEmployeeAndRevenue.put("minRevenue", Double.valueOf(revMap.get(s.getES_RevenueBand())[0]));
				sortedZoomEmployeeAndRevenue.put("maxRevenue", Double.valueOf(revMap.get(s.getES_RevenueBand())[1]));
			}
			if (s.getES_EmployeeBand() != null && !s.getES_EmployeeBand().isEmpty()) {
				cm.setCompanyEmployeeCountRange(s.getES_EmployeeBand());
				Map<String, String[]> revMap = getEverStringEmployeeMap(s.getES_EmployeeBand());
				sortedZoomEmployeeAndRevenue.put("minEmployeeCount",
						Double.valueOf(revMap.get(s.getES_EmployeeBand())[0]));
				sortedZoomEmployeeAndRevenue.put("maxEmployeeCount",
						Double.valueOf(revMap.get(s.getES_EmployeeBand())[1]));
			}
			if (s.getES_Employee() > 0) {
				cm.setCompanyEmployeeCount(new Long(s.getES_Employee()));
			}
			if (s.getES_Revenue() > 0) {
				cm.setCompanyRevenueIn000s(new Long(s.getES_Revenue()));
			}
			if (s.getES_SIC2() != null && s.getES_SIC3() != null && s.getES_SIC4() != null) {
				List<String> listOfSIC = new ArrayList<String>();
				listOfSIC.add(s.getES_SIC2());
				listOfSIC.add(s.getES_SIC3());
				listOfSIC.add(s.getES_SIC4());
				cm.setCompanySIC(listOfSIC);
			}
			if (s.getES_NAICS2() != null && s.getES_NAICS4() != null && s.getES_NAICS6() != null) {
				List<String> listOfNAICS = new ArrayList<String>();
				listOfNAICS.add(s.getES_NAICS2());
				listOfNAICS.add(s.getES_NAICS4());
				listOfNAICS.add(s.getES_NAICS6());
				cm.setCompanyNAICS(listOfNAICS);
			}
			if (s.getES_NumLocations() > 0) {
				cm.setNumOfCompany(s.getES_NumLocations());
			}
			if (sortedZoomEmployeeAndRevenue != null && !sortedZoomEmployeeAndRevenue.isEmpty()
					&& sortedZoomEmployeeAndRevenue.size() > 0) {
				cm.setSortedZoomEmployeeAndRevenue(sortedZoomEmployeeAndRevenue);
			}
			cm.setStatus("FOUND");
			cm.setSalutoryDomain(s.getSalutoryDomain());
			cm.setEnrichSource("EVERSTRING");
			companyMasterRepository.save(cm);
		}
	}

	private Map<String, String[]> getEverStringRevenueMap(String revenueBand) {
		Map<String, String[]> revMap = new HashMap<String, String[]>();
		// $0M-$1M
		if (revenueBand.equals("$0M-$1M")) {
			String[] revArray1 = new String[2];
			revArray1[0] = "0";
			revArray1[1] = "10000000";
			revMap.put("$0M-$1M", revArray1);// 0-$10M
		}
		// $1M-$5M
		if (revenueBand.equals("$1M-$5M")) {
			String[] revArray2 = new String[2];
			revArray2[0] = "0";
			revArray2[1] = "10000000";
			revMap.put("$1M-$5M", revArray2);// 0-$10M
		}
		// $5M-$10M
		if (revenueBand.equals("$5M-$10M")) {
			String[] revArray3 = new String[2];
			revArray3[0] = "5000000";
			revArray3[1] = "10000000";
			revMap.put("$5M-$10M", revArray3);// $5M-$10M
		}
		// $10M-$25M
		if (revenueBand.equals("$10M-$25M")) {
			String[] revArray4 = new String[2];
			revArray4[0] = "10000000";
			revArray4[1] = "25000000";
			revMap.put("$10M-$25M", revArray4);// $10M-$25M
		}
		// $25M-$50M
		if (revenueBand.equals("$25M-$50M")) {
			String[] revArray5 = new String[2];
			revArray5[0] = "25000000";
			revArray5[1] = "50000000";
			revMap.put("$25M-$50M", revArray5);// $25M-$50M
		}
		// $50M-$100M
		if (revenueBand.equals("$50M-$100M")) {
			String[] revArray6 = new String[2];
			revArray6[0] = "50000000";
			revArray6[1] = "100000000";
			revMap.put("$50M-$100M", revArray6);// $50M-$100M
		}
		// $100M-$250M
		if (revenueBand.equals("$100M-$250M")) {
			String[] revArray7 = new String[2];
			revArray7[0] = "100000000";
			revArray7[1] = "250000000";
			revMap.put("$100M-$250M", revArray7);// $100M-$250M
		}
		// $250M-$500M
		if (revenueBand.equals("$250M-$500M")) {
			String[] revArray8 = new String[2];
			revArray8[0] = "250000000";
			revArray8[1] = "500000000";
			revMap.put("$250M-$500M", revArray8);// $250M-$500M
		}
		// $500M-$1B
		if (revenueBand.equals("$500M-$1B")) {
			String[] revArray9 = new String[2];
			revArray9[0] = "500000000";
			revArray9[1] = "1000000000";
			revMap.put("$500M-$1B", revArray9);// $500M-$1B
		}
		// $1B-$5B
		if (revenueBand.equals("$1B-$5B")) {
			String[] revArray10 = new String[2];
			revArray10[0] = "1000000000";
			revArray10[1] = "5000000000";
			revMap.put("$1B-$5B", revArray10);// $1B-$5B
		}
		// $5B+
		if (revenueBand.equals("$5B+")) {
			String[] revArray11 = new String[2];
			revArray11[0] = "5000000000";
			revArray11[1] = "0";
			revMap.put("$5B+", revArray11);// More than $5Billion
		}
		return revMap;
	}

	private Map<String, String[]> getEverStringEmployeeMap(String employeeBand) {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		// 1-10
		if (employeeBand.equals("1-10")) {
			String[] empArray1 = new String[2];
			empArray1[0] = "1";
			empArray1[1] = "5";
			empMap.put("1-10", empArray1);// 1-5
		}
		// 11-20
		if (employeeBand.equals("11-20")) {
			String[] empArray2 = new String[2];
			empArray2[0] = "10";
			empArray2[1] = "20";
			empMap.put("11-20", empArray2);// 10-20
		}
		// 21-50
		if (employeeBand.equals("21-50")) {
			String[] empArray3 = new String[2];
			empArray3[0] = "20";
			empArray3[1] = "50";
			empMap.put("21-50", empArray3);// 20-50
		}
		// 51-100
		if (employeeBand.equals("51-100")) {
			String[] empArray4 = new String[2];
			empArray4[0] = "50";
			empArray4[1] = "100";
			empMap.put("51-100", empArray4);// 50-100
		}
		// 101-200
		if (employeeBand.equals("101-200")) {
			String[] empArray5 = new String[2];
			empArray5[0] = "100";
			empArray5[1] = "250";
			empMap.put("101-200", empArray5);// 100-250
		}
		// 201-500
		if (employeeBand.equals("201-500")) {
			String[] empArray6 = new String[2];
			empArray6[0] = "250";
			empArray6[1] = "500";
			empMap.put("201-500", empArray6);// 250-500
		}
		// 501-1,000
		if (employeeBand.equals("501-1,000")) {
			String[] empArray7 = new String[2];
			empArray7[0] = "500";
			empArray7[1] = "1000";
			empMap.put("501-1,000", empArray7);// 500-1000
		}
		// 1,001-2,000
		if (employeeBand.equals("1,001-2,000")) {
			String[] empArray8 = new String[2];
			empArray8[0] = "1000";
			empArray8[1] = "5000";
			empMap.put("1,001-2,000", empArray8);// 1000-5000
		}
		// 2,001-5,000
		if (employeeBand.equals("2,001-5,000")) {
			String[] empArray9 = new String[2];
			empArray9[0] = "1000";
			empArray9[1] = "5000";
			empMap.put("2,001-5,000", empArray9);// 1000-5000
		}
		// 5,001-10,000
		if (employeeBand.equals("5,001-10,000")) {
			String[] empArray10 = new String[2];
			empArray10[0] = "5000";
			empArray10[1] = "10000";
			empMap.put("5,001-10,000", empArray10);// 5000-10000
		}
		// 10,000+
		if (employeeBand.equals("10,000+")) {
			String[] empArray11 = new String[2];
			empArray11[0] = "10000";
			empArray11[1] = "0";
			empMap.put("10,000+", empArray11);// Over 10000
		}
		return empMap;
	}

	@Override
	public String updateProspectSalutaryCompanyDetails() {
		/*try {
			if (companyMasterList == null || companyMasterList.isEmpty()) {
				companyMasterList = getCompanyInfoMap();
				logger.info("This Program is started for Company Master List in Memory." + companyMasterList.size());
			}
			if (companyMasterList != null && !companyMasterList.isEmpty() && companyMasterList.size() > 0) {
				logger.info("This Program is started for update Prospect Salutary Company Details."
						+ companyMasterList.size());
				boolean found = true;
				int i = 0;
				while (found) {
					Pageable pageable = PageRequest.of(i, 10000);
					List<ProspectSalutary> psList = prospectSalutaryRepository.findSalutaryProspectsByPointer(pageable);
					if (psList != null && !psList.isEmpty() && psList.size() > 0) {
						List<ProspectSalutary> updatePsList = new ArrayList<ProspectSalutary>();
						List<ProspectSalutaryNew> psNewList = new ArrayList<ProspectSalutaryNew>();
						for (ProspectSalutary ps : psList) {
							try {
								if (ps.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
										&& ps.getProspectCall().getProspect().getCustomAttributeValue("domain")
												.toString() != null) {
									String psDomain = getHostName(ps.getProspectCall().getProspect()
											.getCustomAttributeValue("domain").toString());
									if (psDomain != null && !psDomain.isEmpty()) {
										CompanyMaster cm = companyMasterList.get("www." + psDomain);
										if (cm != null) {
											saveProspectSalutaryNewDetails(cm, ps, psNewList);
											ps.setEnrichedStatus("FOUND");
										} else {
											ps.setEnrichedStatus("NOT_FOUND");
										}
									} else {
										ps.setEnrichedStatus("NOT_FOUND");
									}
									updatePsList.add(ps);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						i++;
						prospectSalutaryNewRepository.bulkInsert(psNewList);
						prospectSalutaryRepository.saveAll(updatePsList);
					} else {
						found = false;
					}
				}
				logger.info("This Program is completed for update Prospect Salutary Company Details.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return "Success";
	}

	public String updateProspectSalutaryCompanyDetailsForSalOneClick() {
		try {
			/*if (companyMasterList == null || companyMasterList.isEmpty()) {
				companyMasterList = getCompanyInfoMap();
				logger.info("This Program is started for Company Master List in Memory." + companyMasterList.size());
			}*/
			//if (companyMasterList != null && !companyMasterList.isEmpty() && companyMasterList.size() > 0) {
				/*logger.info("This Program is started for update Prospect Salutary Company Details."
						+ companyMasterList.size());*/
			boolean found = true;
			int i = 0;
			while (found) {
				Pageable pageable = PageRequest.of(i, 10000);
				logger.info("Record fetched till page [{}] ", i);
				List<ProspectSalutaryQ3> psList = prospectSalutaryRepositoryQ3.findSalutaryProspectsByPointer(pageable);
				if (psList != null && !psList.isEmpty() && psList.size() > 0) {
					List<ProspectSalutaryQ3> updatePsList = new ArrayList<ProspectSalutaryQ3>();
					List<ProspectSalutaryQ3> psNewList = new ArrayList<ProspectSalutaryQ3>();
					for (ProspectSalutaryQ3 ps : psList) {
						try {
							if (ps.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
									&& ps.getProspectCall().getProspect().getCustomAttributeValue("domain")
									.toString() != null) {
								String psDomain = getHostName(ps.getProspectCall().getProspect()
										.getCustomAttributeValue("domain").toString());
								if (psDomain != null && !psDomain.isEmpty()) {
									CompanyMaster cm = companyMasterList.get("www." + psDomain);
									if (cm != null) {
										saveProspectSalutaryNewDetails(cm, ps, psNewList);
										logger.info("Record fetched from map");
									} else {
										List<CompanyMaster> cmList = companyMasterRepository.findCompanyMasterListByCompanyWebsite("www." + psDomain);
										if(cmList!=null && cmList.size()>0){
											companyMasterList.put(cmList.get(0).getCompanyWebsite(), cmList.get(0));
											saveProspectSalutaryNewDetails(cmList.get(0), ps, psNewList);
											logger.info("Record fetched from DB");
										}else{
											ps.setEnrichedStatus("NOT_FOUND");
											updatePsList.add(ps);
											logger.info("Record not present in DB");
										}

									}
								} else {
									ps.setEnrichedStatus("NOT_FOUND");
									updatePsList.add(ps);
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					i++;
						prospectSalutaryRepositoryQ3.bulkInsert(psNewList);
					    prospectSalutaryRepositoryQ3.saveAll(updatePsList);
				} else {
					found = false;
				}
			}
			logger.info("This Program is completed for update Prospect Salutary Company Details.");
			//	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Success";
	}


	private Map<String, CompanyMaster> getCompanyInfoMap() {
		Map<String, CompanyMaster> cmList = new HashMap<String, CompanyMaster>();
		boolean found = true;
		int i = 0;
		while (found) {
			Pageable pageable = PageRequest.of(i, 10000);
			List<CompanyMaster> dbList = companyMasterRepository.findCompanyMasterListByPage(pageable);
			if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
				for (CompanyMaster cm : dbList) {
					cmList.put(cm.getCompanyWebsite(), cm);
				}
				i++;
			} else {
				found = false;
			}
		}
		return cmList;
	}

	private void saveProspectSalutaryNewDetails(CompanyMaster cm, ProspectSalutaryQ3 pc,
			List<ProspectSalutaryQ3> psNewList) {
		try {
			ProspectSalutaryQ3 psObj = new ProspectSalutaryQ3();
			Prospect oldp = pc.getProspectCall().getProspect();
			Prospect p = new Prospect();
			p.setSource(oldp.getSource());
			p.setSourceId(oldp.getSourceId());
			p.setFirstName(oldp.getFirstName());
			p.setLastName(oldp.getLastName());
			p.setCompany(oldp.getCompany());
			p.setTitle(oldp.getTitle());
			p.setDepartment(oldp.getDepartment());
			p.setPhone(oldp.getPhone());
			p.setIndustry(oldp.getIndustry());
			p.setEmail(oldp.getEmail());
			p.setAddressLine1(oldp.getAddressLine1());
			p.setCity(oldp.getCity());
			p.setCountry(oldp.getCountry());
			p.setTimeZone(oldp.getTimeZone());
			p.setStateCode(oldp.getStateCode());
			p.setOptedIn(oldp.isOptedIn());
			p.setDirectPhone(oldp.isDirectPhone());
			p.setEu(oldp.getIsEu());
			p.setCustomAttributes(oldp.getCustomAttributes());
			p.setSourceCompanyId(oldp.getSourceCompanyId());
			p.setManagementLevel(oldp.getManagementLevel());
			p.setEncrypted(oldp.getIsEncrypted());
			p.setZoomPersonUrl(oldp.getZoomPersonUrl());
			p.setCompanySIC(oldp.getCompanySIC());
			p.setCompanyNAICS(oldp.getCompanyNAICS());
			p.setSourceType(oldp.getSourceType());
			p.setPhoneType(oldp.getPhoneType());
			p.setEmailSent(oldp.isEmailSent());
			p.setEmailValidated(oldp.isEmailValidated());
			p.setLinkedInURL(oldp.getLinkedInURL());


			Map<String, Object> customAttributes = new HashMap<String, Object>();

			if (cm.getCompanyEmployeeCountRange() != null && !cm.getCompanyEmployeeCountRange().isEmpty()) {
				customAttributes.put("companyEmployeeRange", cm.getCompanyEmployeeCountRange());
			}
			if (cm.getCompanyRevenueRange() != null && !cm.getCompanyRevenueRange().isEmpty()) {
				customAttributes.put("companyRevenueRange", cm.getCompanyRevenueRange());
			}
			if (cm.getCompanyWebsite() != null && !cm.getCompanyWebsite().isEmpty()) {
				customAttributes.put("companyWebsite", cm.getCompanyWebsite());
			}
			if (cm.getZoomCompanyUrl() != null && !cm.getZoomCompanyUrl().isEmpty()) {
				customAttributes.put("zoomCompanyUrl", cm.getZoomCompanyUrl());
				p.setZoomPersonUrl(cm.getZoomCompanyUrl());
			}
			if (cm.getEnrichSource().equals("EVERSTRING") && cm.getNumOfCompany() > 0) {
				customAttributes.put("numLocations", cm.getNumOfCompany());
			}
			if (cm.getCompanyRevenueIn000s() != null && cm.getCompanyRevenueIn000s() > 0) {
				customAttributes.put("exactRevenue", cm.getCompanyRevenueIn000s() * 1000);
			}
			if (cm.getCompanyEmployeeCount() != null && cm.getCompanyEmployeeCount() > 0) {
				customAttributes.put("exactEmployee", cm.getCompanyEmployeeCount());
			}
			customAttributes.put("domain", oldp.getCustomAttributeValue("domain"));

			if (cm.getSortedZoomEmployeeAndRevenue() != null && !cm.getSortedZoomEmployeeAndRevenue().isEmpty()
					&& cm.getSortedZoomEmployeeAndRevenue().size() > 0) {
				if (cm.getSortedZoomEmployeeAndRevenue().containsKey("minEmployeeCount")) {
					customAttributes.put("minEmployeeCount",
							cm.getSortedZoomEmployeeAndRevenue().get("minEmployeeCount"));
				}
				if (cm.getSortedZoomEmployeeAndRevenue().containsKey("maxEmployeeCount")) {
					customAttributes.put("maxEmployeeCount",
							cm.getSortedZoomEmployeeAndRevenue().get("maxEmployeeCount"));
				}
				if (cm.getSortedZoomEmployeeAndRevenue().containsKey("minRevenue")) {
					customAttributes.put("minRevenue", cm.getSortedZoomEmployeeAndRevenue().get("minRevenue"));
				}
				if (cm.getSortedZoomEmployeeAndRevenue().containsKey("maxRevenue")) {
					customAttributes.put("maxRevenue", cm.getSortedZoomEmployeeAndRevenue().get("maxRevenue"));
				}
			}
			p.setCustomAttributes(customAttributes);
			p.setSourceCompanyId(cm.getCompanyId());
			List<String> companySIC = new ArrayList<String>();
			if (cm.getCompanySIC() != null && !cm.getCompanySIC().isEmpty() && cm.getCompanySIC().size() > 0) {
				companySIC.addAll(cm.getCompanySIC());
				companySIC.addAll(oldp.getCompanySIC());
				p.setCompanySIC(companySIC);
			}

			List<String> companyNAICS = new ArrayList<String>();
			if (cm.getCompanyNAICS() != null && !cm.getCompanyNAICS().isEmpty() && cm.getCompanyNAICS().size() > 0) {
				companyNAICS.addAll(cm.getCompanyNAICS());
				companyNAICS.addAll(oldp.getCompanyNAICS());
				p.setCompanyNAICS(companyNAICS);
			}

			ProspectCall pcall = new ProspectCall(pc.getProspectCall().getProspectCallId(),
					pc.getProspectCall().getCampaignId(), p);
			pcall.setQualityBucket(pc.getProspectCall().getQualityBucket());
			pcall.setQualityBucketSort(pc.getProspectCall().getQualityBucketSort());
			pcall.setDataSlice(pc.getProspectCall().getDataSlice());
			pcall.setCallRetryCount(pc.getProspectCall().getCallRetryCount());
			pcall.setOrganizationId(pc.getProspectCall().getOrganizationId());
			pcall.setUpdatedDate(new Date());
			pcall.setUpdatedSource(cm.getEnrichSource());
			psObj.setProspectCall(pcall);
			psObj.setStatus(psObj.getStatus().MAX_RETRY_LIMIT_REACHED);
			psObj.setDataSlice(pc.getDataSlice());
			psObj.setTagDirectIndirect(pc.isTagDirectIndirect());
			psObj.setEnrichedStatus("FOUND");
			psObj.setCompanyMaster(pc.getCompanyMaster());

			psObj.setCreatedDate(pc.getCreatedDate());
			psObj.setCreatedBy(pc.getCreatedBy());
			psObj.setUpdatedDate(pc.getUpdatedDate());
			psObj.setUpdatedBy(pc.getUpdatedBy());
			psObj.setVersion(pc.getVersion());


			psNewList.add(psObj);
		} catch (DuplicateKeyException e) {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<CampaignPerformanceDTO> getCampaignPerformance(OneOffDTO oneOffDTO) {
		List<CampaignPerformanceDTO> pList = new ArrayList<CampaignPerformanceDTO>();
		try {
			String fromDate = oneOffDTO.getFromDate();
			String toDate = oneOffDTO.getToDate();
			String orgId = oneOffDTO.getOrganizationId();
			if (fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty() && orgId != null
					&& !orgId.isEmpty()) {
				Date startDate = XtaasDateUtils.getDate(fromDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
				Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
				Date endDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
				Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
				List<Campaign> cList = campaignRepository.getCampaignPerformanceList(orgId,startDateInUTC,endDateInUTC);
				if(cList != null && !cList.isEmpty() && cList.size() > 0) {
					for(Campaign cam : cList) {
						CampaignPerformanceDTO dto = new CampaignPerformanceDTO();
						dto.setId(cam.getId());
						dto.setType(cam.getType().toString());
						dto.setTeam(cam.getTeam().getSupervisorId());
						dto.setCampaignName(cam.getName());
						dto.setStartDate(XtaasDateUtils.convertDateToString(cam.getStartDate()));
						dto.setEndDate(XtaasDateUtils.convertDateToString(cam.getEndDate()));
						dto.setPacing("Even	Pacing");
						dto.setBudget(cam.getDeliveryTarget());
						int generatedCount = prospectCallLogRepository.getSuccessCount(cam.getId(),startDateInUTC,endDateInUTC);
						dto.setGenerated(generatedCount);
						int rejectedCount = prospectCallLogRepository.getRejectedCount(cam.getId(),startDateInUTC,endDateInUTC);
						dto.setRejected(rejectedCount);
						int inQACount = prospectCallLogRepository.getInQACount(cam.getId(),startDateInUTC,endDateInUTC);
						dto.setInQA(inQACount);
						pList.add(dto);
					}
				}
			}else {
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pList;
	}


	private HttpHeaders signalwireHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "*/*");
		headers.add("content-type", "application/json");
		// headers.add("Authorization",
		// "Basic
		// YzQ5YTUxMWEtMmQ3YS00ODE2LWIxMzktMzAyYWU3ZDdjNzRkOlBUMTc2ZGE5Y2ZiMGRlYTk0ZGRlYTU0Njg1OWYyZjJlYjVlY2U4ZjYwN2Q3OTNiMThm");
		headers.add("Authorization", "Basic " + getBase64EncodedAuthorizationString());
		return headers;
	}

	private String getBase64EncodedAuthorizationString() {
		String signalwireProjectId = ApplicationEnvironmentPropertyUtils.getSignalwireProjectId();
		String signalwireAuthToken = ApplicationEnvironmentPropertyUtils.getSignalwireAuthToken();
		String encodedString = Base64.getEncoder()
				.encodeToString((signalwireProjectId + ":" + signalwireAuthToken).getBytes());
		return encodedString;
	}

	@Override
	public String fetchSignalwireRecordingUrl(String callSid) {
		try {
			String url = signalWireBaseUrl + "/Calls/" + callSid + "/Recordings";
			List<Map<String,String>> recordings = (List<Map<String, String>>) httpService.get(url, signalwireHeaders(), new ParameterizedTypeReference<Map<String, Object>>() {
			}).get("recordings");
			if (recordings != null && recordings.size() > 0) {
				Map<String, String> recordingObj = recordings.get(0);
				String recordingSid = recordingObj.get("sid");
				if (recordingSid != null && !recordingSid.isEmpty()) {
					String recordingUrl = url + "/" + recordingSid;
					return recordingUrl;
				}
			}
		} catch (Exception e) {
			logger.error("ApiTestingServiceImpl exception: " + e.getStackTrace());
		}
		return "Not found";
	}

	public String fetchAndAddCountryDetailsInDB() {
		try {
			List<CountryDetailsISDCodeDTO> response = httpService.get("https://restcountries.eu/rest/v2/all", headersCountryDetails(), new ParameterizedTypeReference<List<CountryDetailsISDCodeDTO>>() {});
			List<PlivoOutboundNumber> outboundNumbers = plivoOutboundNumberRepository.findAll();
			Map<String, List<String>> countryCodeAndIsdCodeMap = new HashMap<>();
			String errorStrings = "";
			String successStrings = "";
			for (CountryDetailsISDCodeDTO countryDetailsISDCodeDTO : response) {
				if (countryDetailsISDCodeDTO.getAltSpellings() != null && countryDetailsISDCodeDTO.getAltSpellings().size() > 0){
					for(String altSpelling : countryDetailsISDCodeDTO.getAltSpellings()) {
						countryCodeAndIsdCodeMap.put(altSpelling, countryDetailsISDCodeDTO.getCallingCodes());
					}
				}
			}
			int counter = 0;
			if (response != null && response.size() > 0) {
				for (PlivoOutboundNumber plivoOutboundNumber : outboundNumbers) {
					// if ( counter == 10000) {
					// 	break;
					// }
					List<String> callingCodes = countryCodeAndIsdCodeMap.get(plivoOutboundNumber.getCountryCode());
					if (callingCodes != null && callingCodes.size() == 1 && !callingCodes.get(0).isEmpty()) {
						try {
							int newIsdCode = Integer.parseInt(callingCodes.get(0));
							 plivoOutboundNumber.setIsdCode(newIsdCode);
							successStrings += "Updated isdcode for Country Code " + plivoOutboundNumber.getCountryCode() + " || Old isdCode: " + plivoOutboundNumber.getIsdCode() + ", New isdCode: " + callingCodes.get(0) + "\n";
						} catch (Exception e) {
							errorStrings += "Exception for Country Code " + plivoOutboundNumber.getCountryCode() + " CountryName: " + plivoOutboundNumber.getCountryName() + " || Exception: " + e.getMessage() + "\n";
						}
					} else if (callingCodes != null && callingCodes.size() > 0 && callingCodes.get(0).isEmpty()) {
						errorStrings += "Empty calling codes found for Country Code " + plivoOutboundNumber.getCountryCode() + " CountryName: " + plivoOutboundNumber.getCountryName() + "\n";
					} else if (callingCodes == null || (callingCodes != null && callingCodes.size() == 0)) {
						errorStrings += "Country Code not found for " + plivoOutboundNumber.getCountryCode() + " CountryName: " + plivoOutboundNumber.getCountryName() + "\n";
					} else {
						errorStrings += "Multiple Isd codes for Country Code " + plivoOutboundNumber.getCountryCode() + " CountryName: " + plivoOutboundNumber.getCountryName() + "\n";
					}
					counter++;
				}

				 int listSize = outboundNumbers.size();
				 if (listSize <= 4000) {
				 	plivoOutboundNumberRepository.saveAll(outboundNumbers);
				 } else {
				 	int batchSize = listSize/4000;
				 	int modSize = listSize%4000;
				 	for (int i = 0; i <= batchSize; i++) {
				 		int fromIndex = i*4000;
				 		int toIndex = fromIndex + 4000;
				 		if (i == batchSize) {
				 			toIndex = fromIndex + modSize -1;
				 		}
				 		List<PlivoOutboundNumber> tempOutboundNumbersList = outboundNumbers.subList(fromIndex, toIndex);
				 		plivoOutboundNumberRepository.saveAll(tempOutboundNumbersList);
				 	}
				 }


				// Old implementation
				// for (CountryDetailsISDCodeDTO countryDetailsISDCodeDTO : response) {
				// 	CountryISDCodes obj = new CountryISDCodes();
				// 	obj.setName(countryDetailsISDCodeDTO.getName());
				// 	obj.setCallingCodes(countryDetailsISDCodeDTO.getCallingCodes());
				// 	obj.setNativeName(countryDetailsISDCodeDTO.getNativeName());
				// 	list.add(obj);
				// }
			}
			return "{ \"Success\" : \"" + successStrings + "\" , \"Errors\" : \"" + errorStrings + "\" }";
		} catch(Exception e) {
			logger.error("ApiTestingServiceImpl fetchAndAddCountryDetailsInDB error ======> " + e.getStackTrace());
		}
		return "fail";
	}

	private HttpHeaders headersCountryDetails() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "application/json");
		return headers;
	}


	@Override
	public String moveSalutaryProspect() {
		Runnable runnable = () -> {
			insertJsonToProspectCallOneClick();
		};
		Thread thread = new Thread(runnable);
		thread.start();
		logger.info("Salutary data move in temp collection thread started at [{}] ", new Date());
		return "Salutary data move in temp collection thread started";

	}

	@Override
	public String moveSalutaryDataLive(int pageNo) {
		Runnable runnable = () -> {
			moveDataTempToLiveCollection(pageNo);
		};
		Thread thread = new Thread(runnable);
		thread.start();
		logger.info("Salutary live data movement thread started at [{}] ", new Date());
		return "Salutary live data movement thread started";
	}


	private void insertJsonToProspectCall() {
		updateProspectSalutaryCompanyDetailsOneClick();
		logger.info("Prospect Salutary Company Details done. Step 1 done" );

		markSliceAndQualityScore();
		logger.info("Slice And Quality Score done. Step 2 done" );
		logger.info("Saluatary updation done" );

	}



	public String updateProspectSalutaryCompanyDetailsOneClick() {
		/*try {
			if (companyMasterList == null || companyMasterList.isEmpty()) {
				companyMasterList = getCompanyInfoMapOneClick();
				logger.info("This Program is started for Company Master List in Memory." + companyMasterList.size());
			}
			if (companyMasterList != null && !companyMasterList.isEmpty() && companyMasterList.size() > 0) {
				logger.info("This Program is started for update Prospect Salutary Company Details."
						+ companyMasterList.size());
				boolean found = true;
				int i = 0;
				while (found) {
					Pageable pageable = PageRequest.of(i, 10000);
					List<ProspectSalutary> psList = prospectSalutaryRepository.findSalutaryProspectsByPointer(pageable);
					if (psList != null && !psList.isEmpty() && psList.size() > 0) {
						List<ProspectSalutary> updatePsList = new ArrayList<ProspectSalutary>();
						List<ProspectSalutaryNew> psNewList = new ArrayList<ProspectSalutaryNew>();
						for (ProspectSalutary ps : psList) {
							try {
								if (ps.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
										&& ps.getProspectCall().getProspect().getCustomAttributeValue("domain")
										.toString() != null) {
									String psDomain = getHostName(ps.getProspectCall().getProspect()
											.getCustomAttributeValue("domain").toString());
									if (psDomain != null && !psDomain.isEmpty()) {
										CompanyMaster cm = companyMasterList.get("www." + psDomain);
										if (cm != null) {
											saveProspectSalutaryNewDetails(cm, ps, psNewList);
											ps.setEnrichedStatus("FOUND");
										} else {
											ps.setEnrichedStatus("NOT_FOUND");
										}
									} else {
										ps.setEnrichedStatus("NOT_FOUND");
									}
									updatePsList.add(ps);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						i++;
						//	prospectSalutaryNewRepository.bulkInsert(psNewList);
						prospectSalutaryRepository.saveAll(updatePsList);
					} else {
						found = false;
					}
				}
				logger.info("This Program is completed for update Prospect Salutary Company Details.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return "Success";
	}


	private Map<String, CompanyMaster> getCompanyInfoMapOneClick() {
		Map<String, CompanyMaster> cmList = new HashMap<String, CompanyMaster>();
		boolean found = true;
		int i = 0;
		try{
			while (found) {
				Pageable pageable = PageRequest.of(i, 10000);
				List<CompanyMaster> dbList = companyMasterRepository.findCompanyMasterListByPage(pageable);
				if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
					for (CompanyMaster cm : dbList) {
						cmList.put(cm.getCompanyWebsite(), cm);
					}
					i++;
					logger.info("Fetch Record of page no : [{}]", i);
				} else {
					logger.info("No record found  : [{}]", i);
					found = false;
				}
			}
		}catch(Exception e){
			logger.error("Error while fetching or creating map ERROR!!  : [{}]", i);
			e.printStackTrace();
		}

		return cmList;
	}



	public void markSliceAndQualityScore(){
		try {
			boolean found = true;
			int i = 0;
			Map<String, String> searchDomainList = new HashMap<String, String>();
			while (found) {
				Pageable pageable = PageRequest.of(i, 10000);
				List<ProspectSalutaryQ3> psList = prospectSalutaryRepositoryQ3.findSalutaryProspectsPage(pageable);
				if (psList != null && !psList.isEmpty() && psList.size() > 0) {
					tagSliceAndQualityScoreQ3(psList);
					i++;
					logger.info("Slice and score done till page: [{}]", i);
				} else {
					found = false;
				}

			}

		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void tagSliceAndQualityScore(List<ProspectSalutary> pList) {
		try {
			Map<String, List<ProspectSalutary>> pMap = new HashMap<String,List<ProspectSalutary>>();

			for (ProspectSalutary p : pList) {
				List<ProspectSalutary> prospectList = pMap.get(p.getProspectCall().getProspect().getPhone());
				if(prospectList!=null && prospectList.size()>0){
					prospectList.add(p);
				}else{
					prospectList = new ArrayList<ProspectSalutary>();
					prospectList.add(p);
				}
				pMap.put(p.getProspectCall().getProspect().getPhone(), prospectList);
			}
			List<ProspectSalutary> updateProspectList = new ArrayList<ProspectSalutary>();
			for (Map.Entry<String, List<ProspectSalutary>> entry : pMap.entrySet()) {
				List<ProspectSalutary> prospectList = entry.getValue();
				boolean directFlag= true;
				if(prospectList.size()>1)
					directFlag = false;
				for (ProspectSalutary p : prospectList) {
					if(p.getProspectCall().getProspect().getExtension()!=null && !p.getProspectCall().getProspect().getExtension().isEmpty()) {
						p.getProspectCall().getProspect().setDirectPhone(true);
					}
					if(prospectList.size()==1 && !p.getProspectCall().getProspect().isDirectPhone()){
						//DO nothing
					}else if(p.getProspectCall().getProspect().getExtension()!=null && !p.getProspectCall().getProspect().getExtension().isEmpty()){
						p.getProspectCall().getProspect().setDirectPhone(directFlag);
					}else{
						p.getProspectCall().getProspect().setDirectPhone(directFlag);
					}

					StringBuffer sb= new StringBuffer();

					String emp ="";
					String mgmt="";
					String direct="";
					if(directFlag ){
						direct="1";
					}else{
						direct="2";
					}


					String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
					sb.append(p.getStatus());
					sb.append("|");
					if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number){
						Number n1 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount");
						sb.append(getBoundry(n1,"min"));
						sb.append("-");
						emp = getEmployeeWeight(n1);
					}else{
						String x = "0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString();
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"min"));
						sb.append("-");
						emp = getEmployeeWeight(a);
					}

					if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number){
						Number n2 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount");
						sb.append(getBoundry(n2,"max"));
					}else{

						String x ="0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")!=null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
						if(x == null || x.isEmpty()){
							x = "0";
						}
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"max"));
					}

					sb.append("|");
					sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
					mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());

					int qbSort=0;
					if(!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty() ){
						String sortString = direct+tz+mgmt+emp;
						qbSort = Integer.parseInt(sortString);

					}


					String slice = "";
					p.getProspectCall().setQualityBucket(sb.toString());
					p.getProspectCall().setQualityBucketSort(qbSort);
					p.getProspectCall().getProspect().setDirectPhone(directFlag);
					Long employee = new Long(emp);
					Long management = new Long(mgmt);
					if(directFlag && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")){
						slice = "slice1";
					}else if(directFlag /*&& employee<=8 && management<=3*/){
						slice = "slice1";
					}

					if(!slice.isEmpty() && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")) {
						if(directFlag){


							if(management>=4 || employee >= 12){
								p.setDataSlice("slice2");
								p.getProspectCall().setDataSlice("slice2");
							}else{
								p.setDataSlice("slice1");
								p.getProspectCall().setDataSlice("slice1");
							}

						}else{
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");

						}
					}

					if(!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty() || p.getDataSlice()==null || p.getDataSlice().isEmpty() ) {
						//logger.info(p.getProspectCall().getProspectCallId()+"--->"+p.getDataSlice());
						if(p.getDataSlice()==null || !p.getDataSlice().isEmpty() ||  !p.getDataSlice().equalsIgnoreCase("slice10")){
							//p = prospectCallLogRepository.findOneById(p.getId());
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");

						}
					}//p.getProspectCall().getProspect().setTimeZone(timezone);
					//p.getProspectCall().getProspect().setTimeZone(timezone);

					if(p.getProspectCall().getDataSlice()==null && p.getDataSlice()!=null){
						p.getProspectCall().setDataSlice(p.getDataSlice());
						//prospectSalutaryRepository.save(p);

					}
					//	p.setEnrichedStatus("true");
					prospectSalutaryRepository.save(p);
					//System.out.println("Slic updated");
					//	updateProspectList.add(p);
				}
			}

			//prospectSalutaryRepository.bulkinsert(updateProspectList);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}




	public void moveDataTempToLiveCollection(int i){
		boolean found = true;
		while (found) {
			try{
				int batchSize = propertyService.getIntApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.BULK_MOVE_SALUATARY_BATCHSIZE.name(), 2000);
				Pageable pageable = PageRequest.of(i, batchSize);
				logger.info("Record processed till page [{}] and Batch size [{}] ", i, batchSize);
				List<ProspectSalutaryTemp> psList = prospectSalutaryRepositoryTemp.findSalutaryProspectsPage(pageable);
				if (psList != null && !psList.isEmpty() && psList.size() > 0) {
					List<ProspectSalutaryNew> psNewList = new ArrayList<ProspectSalutaryNew>();
					for (ProspectSalutaryTemp ps : psList) {
						try{
							createProspectSalutary(ps, psNewList);

						}catch (Exception e){
							logger.error("Error while creating ProspectSalutary object. Exception : [{}] ",e);
						}
					}
					i++;
					prospectSalutaryNewRepository.bulkInsert(psNewList);
				}else {
					found = false;
					logger.info("Data moved successfully to live collcetion : prospectsalutary_new");
				}
			}catch (Exception e){
				found = false;
				logger.info("Exception came at page No [{}] while data movine to live collcetion : prospectsalutary_new till page [{}] ", i, i);
				e.printStackTrace();
			}

		}

	}



	public void createProspectSalutary(ProspectSalutaryTemp dbPS,List<ProspectSalutaryNew> psNewList){
		ProspectSalutaryNew prospectSalutary = new ProspectSalutaryNew();

		ProspectCall dbProspectCall = dbPS.getProspectCall();
		prospectSalutary.setProspectCall(dbProspectCall);
		prospectSalutary.setStatus(prospectSalutary.getStatus().MAX_RETRY_LIMIT_REACHED);
		prospectSalutary.setDataSlice(dbPS.getDataSlice());
		prospectSalutary.setCallbackDate(dbPS.getCallbackDate());
		prospectSalutary.setTagDirectIndirect(dbPS.isTagDirectIndirect());
		prospectSalutary.setCreatedDate(dbPS.getCreatedDate());
		prospectSalutary.setCreatedBy(dbPS.getCreatedBy());
		prospectSalutary.setUpdatedDate(dbPS.getUpdatedDate());
		prospectSalutary.setUpdatedBy(dbPS.getUpdatedBy());
		prospectSalutary.setVersion(dbPS.getVersion());
		psNewList.add(prospectSalutary);
	}


	private void insertJsonToProspectCallOneClick() {
		String dataPath = "D:\\XTAAS_SALUTARY_DATA\\Salutary New Q2.json";
		long counter = 0;
		try {
			JsonParser parser = Json.createParser(new FileReader(dataPath));
			List<String> row = new ArrayList<>();

			while (parser.hasNext()) {
				JsonParser.Event event = parser.next();
				switch (event) {
					case START_ARRAY:
						continue;
					case VALUE_STRING:
						row.add(parser.getString());
						System.out.println(parser.getString());

						break;
					case START_OBJECT:
						JsonObject obj = parser.getObject();
						System.out.println(obj.toString());

						Salatuary salat = new Salatuary();
						try {
							salat.setEmailDomain(obj.getString("emailDomain"));
						} catch (Exception ee) {

						}
						try {
							salat.setEmailAddres(obj.getString("emailAddress"));
						} catch (Exception ee) {

						}
						try {
							salat.setFirstName(obj.getString("firstName"));
						} catch (Exception ee) {

						}
						try {
							salat.setLastName(obj.getString("lastName"));
						} catch (Exception ee) {

						}
						try {
							salat.setJobTitle(obj.getString("jobTitle"));
						} catch (Exception ee) {

						}
						try {
							salat.setJobLevel(obj.getString("jobLevel"));
						} catch (Exception ee) {

						}
						try {
							salat.setJobFunction(obj.getString("jobFunction"));
						} catch (Exception ee) {

						}
						try {
							salat.setPhone1(obj.getString("phone1"));
						} catch (Exception ee) {

						}
						try {
							salat.setPhone2(obj.getString("phone2"));
						} catch (Exception ee) {

						}
						try {
							salat.setOrgName(obj.getString("orgName"));
						} catch (Exception ee) {

						}
						try {
							salat.setOrgDomain(obj.getString("orgDomain"));
						} catch (Exception ee) {

						}
						try {
							salat.setRevenueRange(obj.getString("revenueRange"));
						} catch (Exception ee) {

						}
						try {
							salat.setEmployeeCountRange(obj.getString("employeeCountRange"));
						} catch (Exception ee) {

						}
						try {
							salat.setSicCode(obj.getJsonNumber("sicCode").toString());
						} catch (Exception ee) {

						}
						try {
							salat.setSicDescription(obj.getString("sicDescription"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress1_line1(obj.getString("address1_line1"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress1_city(obj.getString("address1_city"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress1_state(obj.getString("address1_state"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress1_postal(obj.getJsonNumber("address1_postal").toString());
						} catch (Exception ee) {

						}
						try {
							salat.setAddress2_line1(obj.getString("address2_line1"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress2_city(obj.getString("address2_city"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress1_state(obj.getString("address2_state"));
						} catch (Exception ee) {

						}
						try {
							salat.setAddress2_postal(obj.getJsonNumber("address2_postal").toString());
						} catch (Exception ee) {

						}
						try {
							salat.setLinkedIn_url(obj.getString("LinkedIn_url"));
						} catch (Exception ee) {

						}
						try {
							salat.setNAICS(obj.getJsonNumber("NAICS").toString());
						} catch (Exception ee) {

						}
						try {
							salat.setNAICS_description(obj.getString("NAICS_description"));
						} catch (Exception ee) {

						}
						try {
							salat.setPhoneC(obj.getString("phoneC"));
						} catch (Exception ee) {

						}
						try {
							salat.setPhoneC_dnc(obj.getString("phoneC_dnc"));
						} catch (Exception ee) {

						}
						try {
							if ((salat.getJobFunction() == null || salat.getJobFunction().isEmpty() || salat.getJobLevel() == null || salat.getJobLevel().isEmpty())
									&& (salat.getJobTitle() != null && !salat.getJobTitle().isEmpty())) {
								String mgmt = null;
								String dep = null;
								if (salat.getJobFunction() == null || salat.getJobFunction().isEmpty()) {
									dep = getValidValue(salat.getJobTitle(), "Department");
									salat.setJobFunction(dep);
								} else {
									dep = salat.getJobFunction();
									salat.setJobFunction(dep);
								}


								if (salat.getJobLevel() == null || salat.getJobLevel().isEmpty()) {
									mgmt = getValidValue(salat.getJobTitle(), "Management");
									salat.setJobLevel(mgmt);
								} else {
									mgmt = salat.getJobLevel();
									salat.setJobLevel(mgmt);
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						createProspectCall(salat);

						//  salatuaryRepository.save(salat);
						System.out.println(counter++);
						break;
					case END_ARRAY:
						if (!row.isEmpty()) {
							//Do something with the current row of data
							System.out.println(row);

							//Reset it (prepare for the new row)
							row.clear();
						}
						break;
					default:
						throw new IllegalStateException("Unexpected JSON event: " + event);
				}
			}

			//insert into salutory_everstring
			System.out.println("Data is populates in new collection with prospect call log. Step-1 Done");

			domainEnrichMent();
			System.out.println("Populates data in salutory_everstring and companymaster collection. Step-2 Done");

			updateProspectSalutaryCompanyDetailsForSalOneClick() ;
			System.out.println("Replenish company data and mark all data in our/zoom format - empCount,Revenue,Industry. Step-3 Done");

			markSliceAndQualityScore();
			System.out.println("Data Slice and quality bucket done Step-4 Done");

		} catch (Exception e) {

		}
	}

	public void domainEnrichMent(){
		try {
			boolean found = true;
			int i = 0;
			Map<String, String> searchDomainList = new HashMap<String, String>();
			while (found) {
				List<ProspectSalutaryQ3> updatedList = new ArrayList<>();
				Pageable pageable = PageRequest.of(i, 10000);
				List<ProspectSalutaryQ3> psList = prospectSalutaryRepositoryQ3.findSalutaryProspectsDomainInMasterCompany(pageable);
				Set<String> set = new HashSet<>();
				if (psList != null && !psList.isEmpty() && psList.size() > 0) {
					for (ProspectSalutaryQ3 ps : psList) {
						try {
							if (ps.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
									&& ps.getProspectCall().getProspect().getCustomAttributeValue("domain")
									.toString() != null) {
								String psDomain = getHostName(ps.getProspectCall().getProspect()
										.getCustomAttributeValue("domain").toString());

								if(set.add(psDomain)){
									addDataInMasterCompanyQ3(psDomain, ps,searchDomainList);
								}
								ps.setCompanyMaster("FOUND");
							}else{
								ps.setCompanyMaster("NOT_FOUND");
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						updatedList.add(ps);
					}
					prospectSalutaryRepositoryQ3.saveAll(updatedList);
					i++;

				} else {
					found = false;
				}

			}
			if (searchDomainList != null && !searchDomainList.isEmpty() && searchDomainList.size() > 0) {
				List<Map<String, String>> batchSearchDomain = noOfBatches(searchDomainList, 25);
				callEverStringMultipleCompanySearchAPI(batchSearchDomain, i);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void addDataInMasterCompany(String domain, ProspectSalutaryTemp ps ,Map<String, String> searchDomainList ){

		try{
			List<SalutoryEverstring> dbList = salutoryEverstringRepository.findDomainList(getHostName(domain));
			if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
				saveMasterCompanyInfo(dbList.get(0));
				logger.info("Find Everstring collection : " +domain);
			} else{
				searchDomainList.put(domain, ps.getProspectCall().getProspect()
						.getCustomAttributeValue("domain").toString());
			}
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("errow while saving/fetching records from Everstring collection : " + e);
		}


	}


	public  void createProspectCall(Salatuary s){

		try {
			ProspectSalutaryQ3 ps = new ProspectSalutaryQ3();
			Prospect p = new Prospect();
			p.setFirstName(s.getFirstName());
			p.setLastName(s.getLastName());

			p.setSource("SALUTORY");
			p.setDataSource("Xtaas Data Source");
			p.setSourceId(UUID.randomUUID().toString());
			p.setCompany(s.getOrgName());
			p.setTitle(s.getJobTitle());
			p.setDepartment(s.getJobFunction());
			String phn = "INVALID_PHONE";
			String phnReason = "INVALID_PHONE";

			if(s.getPhone1()!=null && !s.getPhone1().isEmpty()) {
				String ext = getExtension(s.getPhone1());
				p.setExtension(ext);
				phn = getUSSalutaryPhone(s.getPhone1());
				if(!phn.equalsIgnoreCase("INVALID_PHONE")) {
					p.setPhone(phn);
					phnReason  = "VALID";
				}else if(s.getPhone2()!=null && !s.getPhone2().isEmpty()) {
					phn = getUSSalutaryPhone(s.getPhone2());
					if(!phn.equalsIgnoreCase("INVALID_PHONE")) {
						p.setPhone(phn);
						phnReason  = "VALID";
					}
				}else {
					p.setPhone(s.getPhone1());
				}
			}else if(s.getPhone2()!=null && !s.getPhone2().isEmpty()) {
				String ext = getExtension(s.getPhone2());
				p.setExtension(ext);
				phn = getUSSalutaryPhone(s.getPhone2());
				if(!phn.equalsIgnoreCase("INVALID_PHONE")) {
					p.setPhone(phn);
				}else {
					p.setPhone(s.getPhone1());
				}
			}else {
				p.setPhone(s.getPhone1());
				phnReason =  "NO PHONE NUMBER";
			}


			if(s.getPhone2()!=null && !s.getPhone2().isEmpty()) {
				String cphn =  getUSSalutaryPhone(s.getPhone2());
				if(!cphn.equalsIgnoreCase("INVALID_PHONE")) {
					p.setCompanyPhone(cphn);
				}else {
					p.setCompanyPhone(s.getPhone2());
				}
			}

			if(s.getAddress1_state()!=null && !s.getAddress1_state().isEmpty()) {
				p.setStateCode(s.getAddress1_state());
				p.setCity(s.getAddress1_city());
				p.setAddressLine1(s.getAddress1_line1());
				p.setZipCode(s.getAddress1_postal());
				p.setTimeZone(getTimeZone(s.getAddress1_state()));
			}else if(s.getAddress2_state()!=null && !s.getAddress2_state().isEmpty()) {
				p.setStateCode(s.getAddress2_state());
				p.setCity(s.getAddress2_city());
				p.setAddressLine1(s.getAddress2_line1());
				p.setZipCode(s.getAddress2_postal());
				p.setTimeZone(getTimeZone(s.getAddress2_state()));
			}else {

				p.setCity(s.getAddress1_city());
				p.setAddressLine1(s.getAddress1_line1());
				p.setZipCode(s.getAddress1_postal());
				setSalutaryTimeZoneForOneClick(p);
			}


			Map<String, Object> customAttributes = new HashMap<String, Object>();
			if(s.getRevenueRange()!=null && !s.getRevenueRange().isEmpty()) {
				Map<String, String[]> revMap = getRevenueMap();
				String[] revArr = revMap.get(s.getRevenueRange());
				customAttributes.put("minRevenue", new Double(Integer.parseInt(revArr[0])));
				customAttributes.put("maxRevenue", new Double(revArr[1]));
			}
			if(s.getEmployeeCountRange()!=null && !s.getEmployeeCountRange().isEmpty()) {
				Map<String, String[]> empMap = getEmployeeMap();
				String[] empArr = empMap.get(s.getEmployeeCountRange());

				customAttributes.put("maxEmployeeCount", new Double(Integer.parseInt(empArr[1])));
				customAttributes.put("minEmployeeCount", new Double(Integer.parseInt(empArr[0])));
			}


			if(s.getOrgDomain()!=null && !s.getOrgDomain().isEmpty() && s.getOrgDomain().contains("."))
				customAttributes.put("domain", s.getOrgDomain());
			else
				customAttributes.put("domain", s.getEmailDomain());


			p.setCustomAttributes(customAttributes);
			p.setSourceCompanyId(s.getId());
			p.setManagementLevel(s.getJobLevel());

			p.setZoomPersonUrl(s.getLinkedIn_url());
			p.setLinkedInURL(s.getLinkedIn_url());
			p.setSourceType("INTERNAL");
			List<String> sicList = new ArrayList<String>(1);
			sicList.add(s.getSicCode());
			p.setCompanySIC(sicList);
			List<String> naicsList = new ArrayList<String>(1);
			naicsList.add(s.getNAICS());
			p.setCompanyNAICS(naicsList);


			if(s.getSicDescription()   !=null && !s.getSicDescription().isEmpty())
				p.setIndustry(s.getSicDescription());
			else if(s.getNAICS_description()   !=null && !s.getNAICS_description().isEmpty())
				p.setIndustry(s.getNAICS_description());
			p.setEmail(s.getEmailAddres());
			p.setCountry("United States");

			ProspectCall pc = new ProspectCall(p.getSourceId(), "SALUTARY_DATA", p);
			ps.setDataSlice("slice10");
			pc.setCampaignId("SALUTARY_DATA");
			pc.setDataSlice("slice10");
			pc.setCallRetryCount(786);
			pc.setCallDuration(0);
			pc.setOrganizationId("XTAAS CALL CENTER");

			ps.setStatus(com.xtaas.db.entity.ProspectSalutaryQ3.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			ps.setProspectCall(pc);
			if(phn.equalsIgnoreCase("INVALID_PHONE")) {
				ProspectSalutaryInvalid psi = new ProspectSalutaryInvalid();
				psi.setProspectCall(ps.getProspectCall());
				psi.getProspectCall().setProspect(ps.getProspectCall().getProspect());
				psi.setDataSlice(ps.getDataSlice());
				psi.setReason(phnReason);
				psi.setStatus(com.xtaas.db.entity.ProspectSalutaryInvalid.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
				salutaryInvalidRepository.save(psi);
			}else {
				try {
					prospectSalutaryRepositoryQ3.save(ps);
				}catch(Exception ex) {ProspectSalutaryInvalid psi = new ProspectSalutaryInvalid();
					psi.setProspectCall(ps.getProspectCall());
					psi.getProspectCall().setProspect(ps.getProspectCall().getProspect());
					psi.setDataSlice(ps.getDataSlice());
					psi.setReason("Duplicate");
					psi.setStatus(com.xtaas.db.entity.ProspectSalutaryInvalid.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					salutaryInvalidRepository.save(psi);
					//ex.printStackTrace();
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}


	public void setSalutaryTimeZoneForOneClick(Prospect p) {
		if(p.getPhone() != null && p.getPhone() != "" && !p.getPhone().isEmpty() &&  !p.getPhone().equalsIgnoreCase("INVALID_PHONE")) {
			try {
				FileReader reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
				properties.load(reader);
				String st = "";
				String timezzzz = properties
						.getProperty(findAreaCode(p.getPhone()));
				if (timezzzz != null && !timezzzz.isEmpty()) {
					if (timezzzz.equalsIgnoreCase("US/Pacific")) {
						st = "CA";
					} else if (timezzzz.equalsIgnoreCase("US/Eastern")) {
						st = "DE";
					} else if (timezzzz.equalsIgnoreCase("US/Mountain")) {
						st = "CO";
					} else if (timezzzz.equalsIgnoreCase("US/Central")) {
						st = "FL";
					}else{
						st = "CA";
					}
				} else {
					logger.info("salutary State Code NOT Found for Number: "
							+ p.getPhone());
				}

				if (!st.isEmpty()) {
					p.setStateCode(st);
					if (timezzzz != null && !timezzzz.isEmpty()) {
						p.setTimeZone(timezzzz);

					}
				}


			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	}

	private String getExtension(String phone) {
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			String phonepp = phone.substring(0, phone.indexOf("ext"));
			ext = phone.substring(phone.indexOf("ext"));
			ext = ext.replaceAll("[^[A-Za-z]*$]", "");
		}
		return ext;
	}


	public static String  getValidValue(String title, String level){
		String val = "";
		Map<String, List<String>> map = null;
		if(level.equals("Department")){
			map  =  createDepMap();
		}else{
			map  =  createManagementMap();
		}
		for (Map.Entry<String, List<String>> set : map.entrySet()) {
			String key = set.getKey();
			List<String> values = set.getValue();
			boolean isValid  =  isValidDept(values,title);
			if(isValid){
				return key;
			}
		}
		return val;
	}



	public static boolean isValidDept(List<String> personTitleList, String title) {
		boolean flag = false;
		for (String purchaseTitle : personTitleList) {
			String apurchaseTitle = purchaseTitle.toLowerCase();
			String bpurchaseTitle = title.toLowerCase();
			List<String> apurchaseTitleList = Arrays.asList(apurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));
			List<String> bpurchaseTitleList = Arrays.asList(bpurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));
			for(String actualTitle : bpurchaseTitleList){
				if(!actualTitle.equals("of") && !actualTitle.equals("and")
						&& apurchaseTitleList.contains(actualTitle)){
					flag = true;
					return flag;
				}
			}
			if (flag) {
				return flag;
			}
		}
		return flag;
	}




	public static Map<String, List<String>> createDepMap(){
		Map<String, List<String>> map = new HashMap<>();

		// Dept
		map.put("Information Technology", Arrays.asList("IT Administrator","IT Lead","IT Manager","IT Specialist","IT Support","IT Support Specialist","IT System Administrator","IT Systems Engineer","Junior System Administrator","Junior Systems Administrator","Manager Information Systems","Manager Information Technology","Information Systems Specialist","Information Systems Technician","Information Technology Administrator","Information Technology Director","Information Technology Specialist","Information Technology Support","Network Administrator","Network Analyst","Network Architect","Network Support Specialist","Network Systems Engineer","Network Technician","Senior IT Specialist","Senior IT Support Analyst","Information Technology","Senior Network Administrator","Senior Network Engineer","Senior System Analyst","Solution Architect","Information Tech","System Administrator","Telecommunications Analyst","Wireless Network Engineer","Chief Information Officer","Chief Technical Officer","Chief Technology Officer","Desktop Support","Information Systems Manager"));
		map.put("Marketing", Arrays.asList("digital","marketing","market","eCommerce","Advertising","Lead Generation","brand","Business Devlopment"));
		map.put("Human Resources", Arrays.asList("Human Resource","Recruit","On boarding","Onboarding","Client Relationship","Talent Acquisition","Benefits","Talent","Compensation"));
		map.put("Finance", Arrays.asList("Finance","Accounting","Purchasing","Financial","Payroll","Budget","Credit","Tax","Risk"));
		map.put("Operations", Arrays.asList("operations","operation","COO","Chief operating officer","Fleet","Dispatch","Transport","Logistics","General Management"));
		map.put("Sales", Arrays.asList("sales","sales","Business Development","store manager","client advisor","Telemarketer","chief revenue officer","salesperson"));
		map.put("Legal", Arrays.asList("legal","law","Litigation","Patent","Chief Compliance Officer","Chief IP Counsel","Head of Compliance","Head of Legal"));
		map.put("Publishing", Arrays.asList("Assistant Editor","Author","Biographer","Communication Specialist","Communications Director","Communications Manager","Content Engineer","Content Manager","Copy Editor","Copy Writer","Digital Media Specialist","Editor","Editorial Assistant","Ghost Writer","Managing Editor","Section Editor","Social Media Specialist","Technical Writer","Writer","Animator","Associate Producer","Audio and Video Equipment Technician","Camera Operator","Line Producer","Motion Picture Set Worker","Photo Editor","Photographer","Producer","Production Artist","Production Assistant","Production Manager","Project Coordinator","Project Manager","Proofreader","Radio Operator","Recording Engineer","Sound Mixer","Stage Hand","Technical Producer","Videographer"));

		return map;
	}

	public static Map<String, List<String>> createManagementMap() {
		Map<String, List<String>> map = new HashMap<>();

		//Management level
		map.put("C_EXECUTIVES", Arrays.asList("Chief Executive Officer", "Chairman", "Head Of", "General Manager", "Chief Operating Officer", "Chief Tech", "Chief Fin", "COO", "CTO", "CFO", "Founder"));
		map.put("VP_EXECUTIVES", Arrays.asList("vice president", "vp"));
		map.put("DIRECTOR", Arrays.asList("director", "dir"));
		map.put("MANAGER", Arrays.asList("manager", "mgr"));
		map.put("Staff", Arrays.asList("Business Development", "Business Analayst", "Billing", "Bank", "Supply Chain", "It", "Field", "producer", "Operations", "Office Manger", "Market", "Manufacturing", "Product Owner", "Team Lead", "Talent", "compensation", "Data", "payroll", "lease", "Learning", "Customer", "Human resource", "HR", "Investment", "Internet", "Inventory", "Information Technology", "Accounting", "tax", "audit", "Advisor", "Adviser", "Advertising", "analyst", "Technical", "database", "Information tech", "Analytics", "system", "admin", "sales", "marketing", "finance", "Fleet", "transport", "Supervisor", "Modeler", "Engineer", "Coordinator", "Specialist", "Product Development"));
		return map;
	}
	@Override
	public void fetchAndSaveRecordingUrl(String callSid, String emailId, boolean isSend) {
		StringBuffer buffer = new StringBuffer();
		CallLog callLog = callLogRepository.findOneById(callSid);
		if (callLog != null) {
			if (callLog.getCallLogMap().get("RecordingID") != null) {
				String recordingId = callLog.getCallLogMap().get("RecordingID");
				try {
					// don't initiate Plivo with main account auth id use existing sub-account
					// Plivo.init("MAYMZIYJC1YZMXZGE2N2", "MGQ1NmNhY2EzMTRmNWYzYzRkNzk3YTZiYWU1Y2Rm");
					Recording plivoResponse = Recording.getter(recordingId).get();
					ProspectCallLog prospectFromDB = prospectCallLogRepository.findByTwilioCallId(callSid);
					if (prospectFromDB != null && plivoResponse != null) {
						uploadRecordingUrlToAWS(plivoResponse.getRecordingId(), plivoResponse.getRecordingUrl());
						saveRecordingUrlInProspect(plivoResponse.getRecordingUrl(), prospectFromDB, buffer);
					} else {
						buffer.append("Prospect not found from twilioCallSid.");
					}
				} catch (Exception e) {
					buffer.append(e.getMessage());
					e.printStackTrace();
				}
			} else {
				buffer.append("Recording ID doesn't exists in the calllog collection.");
			}
		} else {
			buffer.append("CallLog doesn't exists in the system.");
		}
		if (isSend) {
			XtaasEmailUtils.sendEmail(emailId, "data@xtaascorp.com", "Status of stamping recording url in prospect.", buffer.toString());
		}
	}

	private void uploadRecordingUrlToAWS(String recordingId, String recordingUrl) throws MalformedURLException, IOException {
		InputStream inputStream = null;
		inputStream = new URL(recordingUrl).openStream();
		String fileName = recordingId;
		String fileFormat = recordingUrl.substring(recordingUrl.length() - 4, recordingUrl.length());
		fileName = fileName + fileFormat;
		downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream, true);
	}

	private void saveRecordingUrlInProspect(String recordingUrl, ProspectCallLog prospectFromDB, StringBuffer buffer) {
		if (recordingUrl != null && prospectFromDB != null) {
			int lastIndex = recordingUrl.lastIndexOf("/");
			String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
			String rfileName = fileName + ".wav";
			String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
			prospectFromDB.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
			prospectFromDB.getProspectCall().setRecordingUrl(recordingUrl);
			prospectCallLogRepository.save(prospectFromDB);
			buffer.append("Recording url saved successfully in ProspectCallLog collection with prospectcallid " + prospectFromDB.getProspectCall().getProspectCallId());
		}
	}


	@Override
	public String uploadSource(String source) {
		if(StringUtils.isNotBlank(source)){
			EnrichSourcePriority enrichSource = enrichSourcePriorityRepository.findBySource(source.toUpperCase());
			if(enrichSource != null){
				if(source.equalsIgnoreCase("Salutary")){
					enrichSalutaryData(enrichSource);
				}else if(source.equalsIgnoreCase("Pivotal")) {
					enrichPivotalData(enrichSource);
				}
			}
		}
		return null;
	}

	public void enrichSalutaryData(EnrichSourcePriority priority){
		if(priority.isUpload()) {
			Runnable runnable = () -> {
				insertJsonToProspectCallOneClick();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isDomainEnrich()){
			Runnable runnable = () -> {
				domainEnrichMent();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isCompanyEnrich()){
			Runnable runnable = () -> {
				updateProspectSalutaryCompanyDetailsForSalOneClick() ;
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isQualityBucket()){
			Runnable runnable = () -> {
				markSliceAndQualityScore();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
	}
	public void enrichPivotalData(EnrichSourcePriority priority){
		if(priority.isUpload()) {
			Runnable runnable = () -> {
				uploadPivotalData();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isDomainEnrich()){
			Runnable runnable = () -> {
				domainEnrichMent();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isCompanyEnrich()){
			Runnable runnable = () -> {
				updateProspectSalutaryCompanyDetailsForSalOneClick() ;
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
		if(priority.isQualityBucket()){
			Runnable runnable = () -> {
				markSliceAndQualityScore();
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
	}

	public String uploadPivotalData() {

		try {
				try {
					String dataPath = "D:\\XTAAS_pivotal_DATA\\pivotal.xlsx";
					File f = new File(dataPath);
					book = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(f);
					sheet = book.getSheetAt(0);
					readPivotalExcelFile();

				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Error while reading xlsx file : ", e);
				}

			//insert into salutory_everstring
			System.out.println("Data is populates in new collection with prospect call log. Step-1 Done");

			markSliceAndQualityScore();
			System.out.println("Data Slice and quality bucket done Step-4 Done");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Success";
	}

	private void readPivotalExcelFile() throws IOException, InterruptedException {
		try {
			HashMap<Integer, String> header = new HashMap<>();
			Iterator<Row> rowIterator = sheet.rowIterator();
			if (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				int colCounts = row.getPhysicalNumberOfCells();
				for (int j = 0; j < colCounts; j++) {
					Cell cell = row.getCell(j);
					header.put(j, cell.getStringCellValue().replace("*", ""));
				}
			}

					for (Row row : sheet) {
						try{
							List<String> contactIndustries = new ArrayList<>();
							Map<String, Object> customAttributes = new HashMap<String, Object>();
							Prospect prospect = new Prospect();
							ProspectCall prospectCall = new ProspectCall();
							ProspectSalutaryTemp prospectCallLog = new ProspectSalutaryTemp();
							for (int j = 0; j < header.size(); j++) {
								try{
									Cell cell = row.getCell(j);
									String columnName = header.get(j).replace("*", "").trim();
									if(cell == null){
										continue;
									}
									prospect.setSource("PIVOTAL");
									prospect.setDataSource("Xtaas Data Source");
									if (columnName.equalsIgnoreCase("Prefix")) {
										String prefix = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										prefix = (prefix == null || prefix.length() < 250) ? prefix : prefix.substring(0, 250);
										prospect.setPrefix(prefix);
									} else if (columnName.equalsIgnoreCase("First Name")
											|| columnName.equalsIgnoreCase("First Name*")) {
										String firstname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										firstname = (firstname == null || firstname.length() < 250) ? firstname
												: firstname.substring(0, 250);
										prospect.setFirstName(firstname);
									} else if (columnName.equalsIgnoreCase("Last Name") || columnName.equalsIgnoreCase("Last Name*")) {
										String lastname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										lastname = (lastname == null || lastname.length() < 250) ? lastname
												: lastname.substring(0, 250);
										prospect.setLastName(lastname);
									} else if (columnName.equalsIgnoreCase("Department")
											|| columnName.equalsIgnoreCase("Department*")) {
										String department = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										department = (department == null || department.length() < 250) ? department
												: department.substring(0, 250);
										prospect.setDepartment(department);
									} else if (columnName.equalsIgnoreCase("Suffix")) {
										String suffix = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										suffix = (suffix == null || suffix.length() < 250) ? suffix : suffix.substring(0, 250);
										prospect.setSuffix(suffix);
									} else if (columnName.equalsIgnoreCase("Organization Name")
											|| columnName.equalsIgnoreCase("Organization Name*")) {
										String orgname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										orgname = (orgname != null && orgname.length() < 250) ? orgname : orgname.substring(0, 250);
										prospectCall.setOrganizationId(orgname);
									} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("Email*")) {
										String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										email = (email == null || email.length() < 250) ? email : email.substring(0, 250);
										prospect.setEmail(email);
									} else if (columnName.equalsIgnoreCase("Phone") || columnName.equalsIgnoreCase("Phone*")) {
										try {
											DataFormatter formatter = new DataFormatter();
											String phoneNumber = formatter.formatCellValue(cell);
											String phone = XtaasUtils
													.removeNonAsciiChars(phoneNumber).trim();
											phone = (phone == null || phone.length() < 250) ? phone : phone.substring(0, 250);
											if (!phone.startsWith("+")) {
												phone = "+" + phone;
											}
											prospect.setPhone(phone);
										} catch (Exception e) {

										}
									} else if (columnName.equalsIgnoreCase("CompanyPhone")
											|| columnName.equalsIgnoreCase("CompanyPhone*")) {
										try {
											String cphone = XtaasUtils
													.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "CompanyPhone")).trim();
											cphone = (cphone == null || cphone.length() < 250) ? cphone : cphone.substring(0, 250);
											prospect.setCompanyPhone(cphone);
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("State Code")
											|| columnName.equalsIgnoreCase("State Code*")) {
										String stateCode = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										stateCode = (stateCode == null || stateCode.length() < 250) ? stateCode
												: stateCode.substring(0, 250);
										StateCallConfig callConfig = stateCallConfigRepository.findByStateCode(stateCode);
										if (callConfig != null) {
											prospect.setStateCode(stateCode);
										}
									} else if (columnName.equalsIgnoreCase("Country") || columnName.equalsIgnoreCase("Country*")) {
										String country = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										country = (country == null || country.length() < 250) ? country : country.substring(0, 250);
										prospect.setCountry(country);
									} else if (columnName.equalsIgnoreCase("Title")) {
										String title = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										title = (title == null || title.length() < 250) ? title : title.substring(0, 250);
										prospect.setTitle(title);
									} else if (columnName.equalsIgnoreCase("Functions")) {
										String functionsList[] = row.getCell(j).getStringCellValue().split("\\|");
										List<ContactFunction> functions = new ArrayList<ContactFunction>();
										for (String function : functionsList) {
											ContactFunction contactFunction = new ContactFunction();
											contactFunction.setName(XtaasUtils.removeNonAsciiChars(function));
											functions.add(contactFunction);
										}
									} else if (columnName.equalsIgnoreCase("Domain")) {
										String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										domain = (domain == null || domain.length() < 250) ? domain : domain.substring(0, 250);
										customAttributes.put("domain",domain);
									} else if (columnName.equalsIgnoreCase("Industries")) {
										String industryList[] = cell.getStringCellValue().split("\\|");
										List<String> industries = new ArrayList<>();
										for (String industry : industryList) {
											industry = (industry == null || industry.length() < 250) ? industry
													: industry.substring(0, 250);
											industries.add(industry);
										}
										prospect.setIndustryList(industries);
									} else if (columnName.equalsIgnoreCase("Industry 1") || columnName.equalsIgnoreCase("Industry 2")
											|| columnName.equalsIgnoreCase("Industry 3") || columnName.equalsIgnoreCase("Industry 4")) {
										String industry = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
										industry = (industry == null || industry.length() < 250) ? industry
												: industry.substring(0, 250);
										contactIndustries.add(industry);
									} else if (columnName.equalsIgnoreCase("Minimum Revenue")) {
										try {
											customAttributes.put("minRevenue",validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Minimum Revenue"));
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("Maximum Revenue")) {
										try {
											customAttributes.put("maxRevenue",validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Maximum Revenue"));
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("Minimum Employee Count") || columnName.equalsIgnoreCase("Minimum Employee Count*")) {
										try {
											customAttributes.put("minEmployeeCount",validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Minimum Employee Count"));
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("Maximum Employee Count") || columnName.equalsIgnoreCase("Maximum Employee Count*")) {
										try {
											customAttributes.put("maxEmployeeCount",validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Maximum Employee Count"));
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("AddressLine1")) {
										String addressLine1 = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										addressLine1 = (addressLine1 == null || addressLine1.length() < 250) ? addressLine1
												: addressLine1.substring(0, 250);
										prospect.setAddressLine1(addressLine1);
									} else if (columnName.equalsIgnoreCase("AddressLine2")) {
										String addressLine2 = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										addressLine2 = (addressLine2 == null || addressLine2.length() < 250) ? addressLine2
												: addressLine2.substring(0, 250);
										prospect.setAddressLine2(addressLine2);
									} else if (columnName.equalsIgnoreCase("City")) {
										String city = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
										city = (city == null || city.length() < 250) ? city : city.substring(0, 250);
										prospect.setCity(city);
									} else if (columnName.equalsIgnoreCase("Source")) {
										try {
											prospect.setSource("PIVOTAL");
											prospect.setDataSource("Xtaas Data Source");
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("SourceId")) {
										String sourceId = XtaasUtils
												.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "SourceId")).trim();
										prospect.setSourceId(sourceId);
									} else if (columnName.equalsIgnoreCase("Postal Code")) {
										try {
											prospect.setZipCode(validateMultiTypeFieldValue(cell, row, "Postal Code"));
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("Extension")) {
										try {
											String ext = XtaasUtils
													.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "Extension"));
											prospect.setExtension(ext);
										} catch (IllegalArgumentException e) {

										}
									} else if (columnName.equalsIgnoreCase("OrganizationId")) {
										String sourceCompanyId = XtaasUtils
												.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "OrganizationId"));
										prospect.setSourceCompanyId(sourceCompanyId);
										;
									} else if (columnName.equalsIgnoreCase("ManagementLevel")) {
										String managementLevel = XtaasUtils
												.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "ManagementLevel")).trim();
										managementLevel = (managementLevel == null || managementLevel.length() < 250) ? managementLevel
												: managementLevel.substring(0, 250);
										prospect.setManagementLevel(managementLevel);
									} else if (columnName.equalsIgnoreCase("Company Validation Link")) {
										if (cell != null) {
											String zoomCompanyUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
											prospect.setZoomCompanyUrl(zoomCompanyUrl);
										}
									} else if (columnName.equalsIgnoreCase("Title Validation Link")) {
										if (cell != null) {
											String zoomPersonUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
											prospect.setZoomPersonUrl(zoomPersonUrl);
										}
									} else if (columnName.equalsIgnoreCase("Direct Phone")
											|| columnName.equalsIgnoreCase("Direct Phone*")) {
										if (cell != null) {
											if (cell.getCellType() == CellType.STRING) {
												String isDirectPhone = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
												if (isDirectPhone.equalsIgnoreCase("yes")) {
													prospect.setDirectPhone(true);
												} else if (isDirectPhone.equalsIgnoreCase("no")) {
													prospect.setDirectPhone(false);
												}
											}
										} else {
											prospect.setDirectPhone(false);
										}
									} else if (columnName.equalsIgnoreCase("isEuropean")
											|| columnName.equalsIgnoreCase("isEuropean*")) {
										if (cell != null) {
											if (cell.getCellType() == CellType.STRING) {
												String isEu = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
												if (isEu.equalsIgnoreCase("yes")) {
													prospect.setEu(true);
												} else if (isEu.equalsIgnoreCase("no")) {
													prospect.setEu(false);
												}
											}
										} else {
											prospect.setEu(false);
										}
									}
								}catch (Exception e){
									e.printStackTrace();
									logger.error("Error while reading cell. Exception : [{}]", e);
								}

							}
							if(contactIndustries.size() > 0){
								prospect.setIndustryList(contactIndustries);
							}
							if(customAttributes.size() > 0){
								prospect.setCustomAttributes(customAttributes);
							}
							prospectCall.setProspect(prospect);
							prospectCallLog.setProspectCall(prospectCall);
							if(prospect.getPhone() != null){
								prospectSalutaryRepositoryTemp.save(prospectCallLog);
							}
						}catch (Exception e){
							e.printStackTrace();
							logger.error("Error while reading Row. Exception : [{}]", e);
						}

					}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String validateMultiTypeFieldValue(Cell cell, Row row, String fieldName) {
		String value;
		// DATE:20/02/2018 REASON:Added condition for postal code (connvert 12345.0 to
		// 12345)
		if (fieldName.equals("Postal Code")) {
			DataFormatter dataFormatter = new DataFormatter();
			value = dataFormatter.formatCellValue(cell);
		} else if (cell.getCellType() == CellType.STRING) {
			value = cell.getStringCellValue();
		} else if (cell.getCellType() == CellType.NUMERIC) {
			value = String.valueOf(cell.getNumericCellValue());
		} else {
			logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1) + ":- "
					+ fieldName + "  must contain only Number or String values.");
			throw new IllegalArgumentException("Row " + (row.getRowNum() + 1) + ":- " + fieldName
					+ "  must contain only Number or String values.");
		}
		return value;
	}

	private double validateDoubleTypeFieldValue(double inputValue, String fieldName) {
		try {
			if (inputValue < 0) {
				//throw new IllegalArgumentException("'" + fieldName + "' must be greater than 'zero'.");
			} else {
				return inputValue;
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("'" + inputValue + "' is not in correct format. ");
		}
		return 0;
	}

	@Override
	public String apiDataBuy(CustomBuyDTO customBuyDTO) {
		if(customBuyDTO != null && customBuyDTO.getDataBuyRequest() <= 0){
			return "Please Enter Data Buy Request Count";
		}
		if(customBuyDTO != null && StringUtils.isEmpty(customBuyDTO.getSource())){
			return "Please Enter Data Buy Source";
		}
		int increaseRequestCount = increaseRequestCount(customBuyDTO.getDataBuyRequest());
		customBuyDTO.setDataBuyRequest(increaseRequestCount);
		if(StringUtils.isNotBlank(customBuyDTO.getCampaignId()) && customBuyDTO.getDataBuyRequest() > 0){
			Campaign campaign = campaignRepository.findOneById(customBuyDTO.getCampaignId());
			if(campaign != null){
				DataBuyQueue dataBuyQueue = new DataBuyQueue();
				dataBuyQueue.setJobRequestTime(new Date());
				dataBuyQueue.setCampaignId(campaign.getId());
				dataBuyQueue.setRestFullAPI(true);
				CampaignCriteria criteria = new CampaignCriteria();
				criteria.setRestFullAPI(true);
				if(StringUtils.isNotBlank(customBuyDTO.getCampaignId())){
					criteria.setCampaignId(customBuyDTO.getCampaignId());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getMaxRev())){
					criteria.setMaxRevenue(customBuyDTO.getMaxRev());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getMinRev())){
					criteria.setMinRevenue(customBuyDTO.getMinRev());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getMinEmp())){
					criteria.setMinEmployeeCount(customBuyDTO.getMinEmp());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getMaxEmp())){
					criteria.setMaxEmployeeCount(customBuyDTO.getMaxEmp());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getCountry())){
					criteria.setCountry(customBuyDTO.getCountry());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getDepartment())){
					criteria.setDepartment(customBuyDTO.getDepartment());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getIndustry())){
					criteria.setIndustry(customBuyDTO.getIndustry());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getManagementLevel())){
					criteria.setManagementLevel(customBuyDTO.getManagementLevel());
				}
				if(StringUtils.isNotBlank(customBuyDTO.getTitle())){
					criteria.setTitleKeyWord(customBuyDTO.getTitle());
				}
				if(customBuyDTO.getDataBuyRequest() > 0){
					criteria.setTotalPurchaseReq(Long.valueOf(customBuyDTO.getDataBuyRequest()));
				}
				if(StringUtils.isNotBlank(customBuyDTO.getPhoneType())){
					criteria.setPhoneType(customBuyDTO.getPhoneType());
				}
				dataBuyQueue.setCampaignCriteria(criteria);
				dataBuyQueue.setStatus(DataBuyQueueStatus.INPROCESS.toString());
				dataBuyQueue.setRequestCount(Long.valueOf(customBuyDTO.getDataBuyRequest()));
				dataBuyQueueRepository.save(dataBuyQueue);
				plivoOutboundNumberService.cacheAllPlivoOutboundNumberForDatabuy();
				Runnable runnable = () -> {
					try {
						if(customBuyDTO.getSource().equalsIgnoreCase("InsideView")){
							insideViewServiceImpl.getData(campaign,dataBuyQueue,Long.valueOf(customBuyDTO.getDataBuyRequest()));
						}else if(customBuyDTO.getSource().equalsIgnoreCase("LeadIQ")) {
							leadIQSearchServiceImpl.getData(campaign,dataBuyQueue,Long.valueOf(customBuyDTO.getDataBuyRequest()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				};
				Thread t = new Thread(runnable);
				t.start();
			}else {
				return "Campaign Not Found";
			}
		}
		return "Success";
	}


	public int increaseRequestCount(int requestCount){
		int updatedRequestCount = 0;
		try{
			updatedRequestCount = (requestCount * 50) /100;
			updatedRequestCount  = updatedRequestCount + requestCount;
		}catch (Exception e){
			return requestCount;
		}
		return updatedRequestCount;
	}

	@Override
	public String updateZoomToken(ZoomTokenDTO zoomTokenDTO) {
		String status = "Success";
		try {
			if(zoomTokenDTO != null){
				Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
				List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
				if(CollectionUtils.isNotEmpty(zoomTokenList)){
					ZoomToken ezt = zoomTokenList.get(0);
					ezt.setStatus("INACTIVE");
					zoomTokenRepository.save(ezt);
					ZoomToken newZoomToken = new ZoomToken();
					newZoomToken.setOauthToken(zoomTokenDTO.getOauthToken());
					newZoomToken.setStatus("ACTIVE");
					zoomTokenRepository.save(newZoomToken);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			status = "Failed";
		}
		return status;
	}

	@Override
	public String readSalutaryData(int startIndex) {
		Runnable runnable = () -> {
			try {
				readDataFromS3(startIndex);
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		return "Success";
	}

	public void readDataFromS3(int startIndex){
		try {
			AmazonS3 s3client  = downloadRecordingsToAws.getFileBucketConnectionIMUser();

			ObjectListing objectListing = s3client.listObjects("salutary-data-xtaas");
			for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
				if (os != null && os.getKey() != null && (os.getKey().contains(".csv") || os.getKey().contains(".CSV"))) {
					System.out.println(os.getKey());
					S3Object s3object = s3client.getObject(new GetObjectRequest(os.getBucketName(), os.getKey()));
					if (s3object != null) {
						BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
						List<String> csvHeader = getCsvFileheader(reader);
						int counter = 1;
						if(CollectionUtils.isNotEmpty(csvHeader)){
							String line = reader.readLine();
							if(line != null) {
								while(line != null) {
									if(counter > startIndex) {
										try {
											String[] arr = line.split(XtaasConstants.COMMA);
											List<String> dataList = convertStringToList(arr);
											// add "" value if cell value not available(to compare header and cell value)
											if (dataList.size() < csvHeader.size()) {
												for (int i = dataList.size(); i < csvHeader.size(); i++) {
													dataList.add("");
												}
											}
											if(CollectionUtils.isNotEmpty(dataList)){
												Salatuary salatuary = new Salatuary();
												for(int i = 0; i < dataList.size(); i++){
													try {
														String colName = csvHeader.get(i);
														String colData = dataList.get(i);
														if(StringUtils.isNotBlank(colName) && StringUtils.isNotBlank(colData)){
															createSalatuaryObject(salatuary,colName,colData);
														}
													}catch (Exception e){
														e.printStackTrace();
														logger.error("Error while Salatuary movement data [{}] ", e);
													}
												}
												try {
													if ((salatuary.getJobFunction() == null || salatuary.getJobFunction().isEmpty() || salatuary.getJobLevel() == null || salatuary.getJobLevel().isEmpty())
															&& (salatuary.getJobTitle() != null && !salatuary.getJobTitle().isEmpty())) {
														String mgmt = null;
														String dep = null;
														if (salatuary.getJobFunction() == null || salatuary.getJobFunction().isEmpty()) {
															dep = getValidValue(salatuary.getJobTitle(), "Department");
															salatuary.setJobFunction(dep);
														} else {
															dep = salatuary.getJobFunction();
															salatuary.setJobFunction(dep);
														}


														if (salatuary.getJobLevel() == null || salatuary.getJobLevel().isEmpty()) {
															mgmt = getValidValue(salatuary.getJobTitle(), "Management");
															salatuary.setJobLevel(mgmt);
														} else {
															mgmt = salatuary.getJobLevel();
															salatuary.setJobLevel(mgmt);
														}

													}
												} catch (Exception e) {
													e.printStackTrace();
												}
												createProspectCall(salatuary);
											}
										}catch (Exception e){
											e.printStackTrace();
										}
									}
									line = reader.readLine();
									counter++;

									logger.info("Salutary row processed till [{}] ", counter);
								}
							}
						} else {
							logger.error("No header found for salutary file");
						}
					}
				}
			}


		}catch (Exception e){
			e.printStackTrace();
		}

		//insert into salutory_everstring
		System.out.println("Data is populates in new collection with prospect call log. Step-1 Done");

		domainEnrichMent();
		System.out.println("Populates data in salutory_everstring and companymaster collection. Step-2 Done");

		updateProspectSalutaryCompanyDetailsForSalOneClick() ;
		System.out.println("Replenish company data and mark all data in our/zoom format - empCount,Revenue,Industry. Step-3 Done");

		markSliceAndQualityScore();
		System.out.println("Data Slice and quality bucket done Step-4 Done");
	}

	private void createSalatuaryObject(Salatuary salatuary, String colName, String colData) {
		try {
			if(colName.equalsIgnoreCase("emailDomain")){
				salatuary.setEmailDomain(colData);
			}
			if(colName.equalsIgnoreCase("email")){
				salatuary.setEmailAddres(colData);
			}
			if(colName.equalsIgnoreCase("firstName")){
				salatuary.setFirstName(colData);
			}
			if(colName.equalsIgnoreCase("lastName")){
				salatuary.setLastName(colData);
			}
			if(colName.equalsIgnoreCase("jobTitle")){
				salatuary.setJobTitle(colData);
			}
			if(colName.equalsIgnoreCase("jobLevel")){
				salatuary.setJobLevel(colData);
			}
			if(colName.equalsIgnoreCase("jobFunction")){
				salatuary.setJobFunction(colData);
			}
			if(colName.equalsIgnoreCase("phone1")){
				salatuary.setPhone1(colData);
			}
			if(colName.equalsIgnoreCase("phone2")){
				salatuary.setPhone2(colData);
			}
			if(colName.equalsIgnoreCase("orgName")){
				salatuary.setOrgName(colData);
			}
			if(colName.equalsIgnoreCase("orgDomain")){
				salatuary.setOrgDomain(colData);
			}
			if(colName.equalsIgnoreCase("revenueRange")){
				salatuary.setRevenueRange(colData);
			}
			if(colName.equalsIgnoreCase("employeeCountRange")){
				salatuary.setEmployeeCountRange(colData);
			}
			if(colName.equalsIgnoreCase("sicCode")){
				salatuary.setSicCode(colData);
			}
			if(colName.equalsIgnoreCase("sicDescription")){
				salatuary.setSicDescription(colData);
			}
			if(colName.equalsIgnoreCase("address1_line1")){
				salatuary.setAddress1_line1(colData);
			}
			if(colName.equalsIgnoreCase("address1_city")){
				salatuary.setAddress1_city(colData);
			}
			if(colName.equalsIgnoreCase("address1_state")){
				salatuary.setAddress1_state(colData);
			}
			if(colName.equalsIgnoreCase("address1_postal")){
				salatuary.setAddress1_postal(colData);
			}
			if(colName.equalsIgnoreCase("address2_line1")){
				salatuary.setAddress2_line1(colData);
			}
			if(colName.equalsIgnoreCase("address2_city")){
				salatuary.setAddress2_city(colData);
			}
			if(colName.equalsIgnoreCase("address2_state")){
				salatuary.setAddress2_state(colData);
			}
			if(colName.equalsIgnoreCase("address2_postal")){
				salatuary.setAddress2_postal(colData);
			}
			if(colName.equalsIgnoreCase("LinkedIn_url")){
				salatuary.setLinkedIn_url(colData);
			}
			if(colName.equalsIgnoreCase("NAICS")){
				salatuary.setNAICS(colData);
			}
			if(colName.equalsIgnoreCase("NAICS_description")){
				salatuary.setNAICS_description(colData);
			}
			if(colName.equalsIgnoreCase("phoneC")){
				salatuary.setPhoneC(colData);
			}
			if(colName.equalsIgnoreCase("emailDisposition")){
				salatuary.setEmailDisposition(colData);
			}
		}catch (Exception e){
			e.printStackTrace();
			logger.error("Error while creating Salatuary object, while Salatuary movement data");
		}
	}
	private List<String> getCsvFileheader(BufferedReader reader)
			throws IOException {
		List<String> headerList = new ArrayList<>();
		String header = reader.readLine();
		if (header != null && !header.isEmpty()) {
			String[] arr = header.split(XtaasConstants.COMMA);
			if (arr != null && arr.length > 0) {
				for(String s : arr){
					if(org.apache.commons.lang.StringUtils.isNotBlank(s)) {
						headerList.add(s.trim());
					}
				}
			}
			if(CollectionUtils.isNotEmpty(headerList)){
				return headerList;
			}
		} else {
			return null;
		}
		return null;
	}

	public List<String> getHeaderList(){
		List<String> headerList = new ArrayList<>();
		headerList.add("emailDomain");
		headerList.add("email");
		headerList.add("firstName");
		headerList.add("lastName");
		headerList.add("jobTitle");
		headerList.add("jobLevel");
		headerList.add("jobFunction");
		headerList.add("phone1");
		headerList.add("phone2");
		headerList.add("orgName");
		headerList.add("orgDomain");
		headerList.add("revenueRange");
		headerList.add("employeeCountRange");
		headerList.add("sicCode");
		headerList.add("sicDescription");
		headerList.add("address1_line1");
		headerList.add("address1_city");
		headerList.add("address1_state");
		headerList.add("address1_postal");
		headerList.add("address2_line1");
		headerList.add("address2_city");
		headerList.add("address2_state");
		headerList.add("address2_postal");
		headerList.add("LinkedIn_url");
		headerList.add("NAICS");
		headerList.add("NAICS_description");
		headerList.add("phoneC");
		headerList.add("emailDisposition");
		return headerList;
	}

	private List<String> convertStringToList(String[] arr){

		List<String> dList = new ArrayList<String>();
		String strConcat = null;
		boolean firstIndex = false;
		for(int i = 0; i < arr.length; i++) {
			if (arr[i].indexOf('"') == 0 && arr[i].lastIndexOf('"') == arr[i].length() - 1) {
				if(arr[i].replaceAll("\"", "").length() == 0) {

				}else {
					strConcat = arr[i].replaceAll("\"", "");
				}
				dList.add(strConcat);
				strConcat = null;
			} else if (arr[i].indexOf('"') == 0 && !firstIndex) {
				strConcat = arr[i].replaceAll("\"", "");
				firstIndex = true;
			} else if (arr[i].lastIndexOf('"') == arr[i].length() - 1) {
				if (firstIndex) {
					strConcat =  strConcat + ", " + arr[i].replaceAll("\"", "");
					firstIndex = false;
					dList.add(strConcat);
					strConcat = null;
				} else {
					dList.add(arr[i]);
				}
			} else {
				if (firstIndex) {
					strConcat = strConcat + ", " + arr[i].replaceAll("\"", "");
				} else {
					dList.add(arr[i]);
				}
			}
		}
		return dList;
	}

	public void tagSliceAndQualityScoreForQ3(List<ProspectSalutaryQ3> pList) {
		try {
			Map<String, List<ProspectSalutaryQ3>> pMap = new HashMap<String,List<ProspectSalutaryQ3>>();

			for (ProspectSalutaryQ3 p : pList) {
				List<ProspectSalutaryQ3> prospectList = pMap.get(p.getProspectCall().getProspect().getPhone());
				if(prospectList!=null && prospectList.size()>0){
					prospectList.add(p);
				}else{
					prospectList = new ArrayList<ProspectSalutaryQ3>();
					prospectList.add(p);
				}
				pMap.put(p.getProspectCall().getProspect().getPhone(), prospectList);
			}
			List<ProspectSalutaryQ3> updateProspectList = new ArrayList<ProspectSalutaryQ3>();
			for (Map.Entry<String, List<ProspectSalutaryQ3>> entry : pMap.entrySet()) {
				List<ProspectSalutaryQ3> prospectList = entry.getValue();
				boolean directFlag= true;
				if(prospectList.size()>1)
					directFlag = false;
				for (ProspectSalutaryQ3 p : prospectList) {
					if(p.getProspectCall().getProspect().getExtension()!=null && !p.getProspectCall().getProspect().getExtension().isEmpty()) {
						p.getProspectCall().getProspect().setDirectPhone(true);
					}
					if(prospectList.size()==1 && !p.getProspectCall().getProspect().isDirectPhone()){
						//DO nothing
					}else if(p.getProspectCall().getProspect().getExtension()!=null && !p.getProspectCall().getProspect().getExtension().isEmpty()){
						p.getProspectCall().getProspect().setDirectPhone(directFlag);
					}else{
						p.getProspectCall().getProspect().setDirectPhone(directFlag);
					}

					StringBuffer sb= new StringBuffer();

					String emp ="";
					String mgmt="";
					String direct="";
					if(directFlag ){
						direct="1";
					}else{
						direct="2";
					}


					String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
					sb.append(p.getStatus());
					sb.append("|");
					if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number){
						Number n1 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount");
						sb.append(getBoundry(n1,"min"));
						sb.append("-");
						emp = getEmployeeWeight(n1);
					}else{
						String x = "0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString();
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"min"));
						sb.append("-");
						emp = getEmployeeWeight(a);
					}

					if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number){
						Number n2 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount");
						sb.append(getBoundry(n2,"max"));
					}else{

						String x ="0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")!=null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
						if(x == null || x.isEmpty()){
							x = "0";
						}
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"max"));
					}

					sb.append("|");
					sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
					mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());

					int qbSort=0;
					if(!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty() ){
						String sortString = direct+tz+mgmt+emp;
						qbSort = Integer.parseInt(sortString);

					}


					String slice = "";
					p.getProspectCall().setQualityBucket(sb.toString());
					p.getProspectCall().setQualityBucketSort(qbSort);
					p.getProspectCall().getProspect().setDirectPhone(directFlag);
					Long employee = new Long(emp);
					Long management = new Long(mgmt);
					if(directFlag && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")){
						slice = "slice1";
					}else if(directFlag /*&& employee<=8 && management<=3*/){
						slice = "slice1";
					}

					if(!slice.isEmpty() && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")) {
						if(directFlag){


							if(management>=4 || employee >= 12){
								p.setDataSlice("slice2");
								p.getProspectCall().setDataSlice("slice2");
							}else{
								p.setDataSlice("slice1");
								p.getProspectCall().setDataSlice("slice1");
							}

						}else{
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");

						}
					}

					if(!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty() || p.getDataSlice()==null || p.getDataSlice().isEmpty() ) {
						//logger.info(p.getProspectCall().getProspectCallId()+"--->"+p.getDataSlice());
						if(p.getDataSlice()==null || !p.getDataSlice().isEmpty() ||  !p.getDataSlice().equalsIgnoreCase("slice10")){
							//p = prospectCallLogRepository.findOneById(p.getId());
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");

						}
					}//p.getProspectCall().getProspect().setTimeZone(timezone);
					//p.getProspectCall().getProspect().setTimeZone(timezone);

					if(p.getProspectCall().getDataSlice()==null && p.getDataSlice()!=null){
						p.getProspectCall().setDataSlice(p.getDataSlice());
						//prospectSalutaryRepository.save(p);

					}
					//	p.setEnrichedStatus("true");
					if(StringUtils.isBlank(p.getDataSlice())){
						p.setDataSlice("slice10");
					}
					if(StringUtils.isBlank(p.getProspectCall().getDataSlice())){
						p.getProspectCall().setDataSlice("slice10");
					}
					prospectSalutaryRepositoryQ3.save(p);
					//System.out.println("Slic updated");
					//	updateProspectList.add(p);
				}
			}

			//prospectSalutaryRepository.bulkinsert(updateProspectList);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}



	public void tagSliceAndQualityScoreQ3(List<ProspectSalutaryQ3> pList) {
		try {
			Map<String, List<ProspectSalutaryQ3>> pMap = new HashMap<String,List<ProspectSalutaryQ3>>();

			for (ProspectSalutaryQ3 p : pList) {
				List<ProspectSalutaryQ3> prospectList = pMap.get(p.getProspectCall().getProspect().getPhone());
				if(prospectList!=null && prospectList.size()>0){
					prospectList.add(p);
				}else{
					prospectList = new ArrayList<ProspectSalutaryQ3>();
					prospectList.add(p);
				}
				pMap.put(p.getProspectCall().getProspect().getPhone(), prospectList);
			}
			List<ProspectSalutaryQ3> updateProspectList = new ArrayList<ProspectSalutaryQ3>();
			for (Map.Entry<String, List<ProspectSalutaryQ3>> entry : pMap.entrySet()) {
				List<ProspectSalutaryQ3> prospectList = entry.getValue();
				boolean directFlag= true;
				if(prospectList.size()>1)
					directFlag = false;
				for (ProspectSalutaryQ3 p : prospectList) {
					if(prospectList.size()==1 && !p.getProspectCall().getProspect().isDirectPhone()){
						//DO nothing
					}else{
						p.getProspectCall().getProspect().setDirectPhone(directFlag);
					}
					//p.getProspectCall().getProspect().setDirectPhone(directFlag);
					StringBuffer sb= new StringBuffer();
		 			   /*sb.append(p.getProspectCall().getProspect().getSource());
					sb.append("|");*/

					String emp ="";
					String mgmt="";
					String direct="";
					if(directFlag ){
						direct="1";
					}else{
						direct="2";
					}


					String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
					sb.append(p.getStatus());
					sb.append("|");
					if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number){
						Number n1 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount");
						sb.append(getBoundry(n1,"min"));
						sb.append("-");
						emp = getEmployeeWeight(n1);
					}else{
						String x = "0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString();
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"min"));
						sb.append("-");
						emp = getEmployeeWeight(a);
					}

					if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number){
						Number n2 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount");
						sb.append(getBoundry(n2,"max"));
					}else{

						String x ="0";
						if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")!=null)
							x = p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
						if(x == null || x.isEmpty()){
							x = "0";
						}
						Double a = Double.parseDouble(x);
						sb.append(getBoundry(a,"max"));
					    	/* int a = Integer.parseInt(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString());
					    	 sb.append(getBoundry(a,"max"));*/
						//  sb.append("-");
						//  emp = getEmployeeWeight(a);
					}
					/*sb.append(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount"));
					sb.append("-");
					sb.append(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount"));*/
					sb.append("|");
					sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
					mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());

					int qbSort=0;
					if(!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty() ){
						String sortString = direct+tz+mgmt+emp;
						qbSort = Integer.parseInt(sortString);

					}


					String slice = "slice10";
					p.getProspectCall().setQualityBucket(sb.toString());
					p.getProspectCall().setQualityBucketSort(qbSort);
					p.getProspectCall().getProspect().setDirectPhone(directFlag);
					Long employee = new Long(emp);
					Long management = new Long(mgmt);
					if(directFlag && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")){
						slice = "slice1";
					}else if(directFlag /*&& employee<=8 && management<=3*/){
						slice = "slice1";
					}
					p.setDataSlice(slice);
					p.getProspectCall().setDataSlice(slice);

					if(!slice.isEmpty() && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")) {
						if(directFlag){


							if(management>=4 || employee >= 12){
								p.setDataSlice("slice2");
								p.getProspectCall().setDataSlice("slice2");
							}else{
								p.setDataSlice("slice1");
								p.getProspectCall().setDataSlice("slice1");
							}
							p = prospectSalutaryRepositoryQ3.save(p);
						}else{
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");
							p = prospectSalutaryRepositoryQ3.save(p);
						}
					}

					if(!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty() || p.getDataSlice()==null || p.getDataSlice().isEmpty() ) {
						//logger.info(p.getProspectCall().getProspectCallId()+"--->"+p.getDataSlice());
						if(p.getDataSlice()==null || !p.getDataSlice().isEmpty() ||  !p.getDataSlice().equalsIgnoreCase("slice10")){
							//p = prospectCallLogRepository.findOneById(p.getId());
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");
							//p.getProspectCall().setCallRetryCount(696969);
							//p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							p = prospectSalutaryRepositoryQ3.save(p);
						}
					}//p.getProspectCall().getProspect().setTimeZone(timezone);
					//p.getProspectCall().getProspect().setTimeZone(timezone);

					if(p.getProspectCall().getDataSlice()==null && p.getDataSlice()!=null){
						p.getProspectCall().setDataSlice(p.getDataSlice());
						p = prospectSalutaryRepositoryQ3.save(p);
					}
					if(p.getProspectCall().getDataSlice() == null){
						p.getProspectCall().setDataSlice("slice10");
						p = prospectSalutaryRepositoryQ3.save(p);
					}
					if(p.getDataSlice() == null){
						p.setDataSlice("slice10");
						p = prospectSalutaryRepositoryQ3.save(p);
					}
					//updateProspectList.add(p);
				}
			}
			//prospectCallLogRepository.bulkinsert(updateProspectList);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void addDataInMasterCompanyQ3(String domain, ProspectSalutaryQ3 ps ,Map<String, String> searchDomainList ){

		try{
			List<SalutoryEverstring> dbList = salutoryEverstringRepository.findDomainList(getHostName(domain));
			if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
				saveMasterCompanyInfo(dbList.get(0));
				logger.info("Find Everstring collection : " +domain);
			} else{
				searchDomainList.put(domain, ps.getProspectCall().getProspect()
						.getCustomAttributeValue("domain").toString());
			}
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("errow while saving/fetching records from Everstring collection : " + e);
		}


	}
	
}
