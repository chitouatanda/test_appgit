package com.xtaas.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentOnlineTime;
import com.xtaas.db.entity.AgentOnlineTimeMapping;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.AgentOnlineTimeMappingRepository;
import com.xtaas.db.repository.AgentOnlineTimeRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.utils.XtaasDateUtils;

@Service
public class AgentOnlineJobReportServiceImpl {

	@Autowired
	private AgentService agentService;
	
	@Autowired
	private AgentOnlineTimeRepository agentOnlineTimeRepository;


	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private AgentOnlineTimeMappingRepository agentOnlineTimeMappingRepository;
	
	public void onlineAgentReport() {
		List<AgentOnlineTimeMapping> aotmList = agentOnlineTimeMappingRepository.findMapping();
		try {
		for(AgentOnlineTimeMapping aotm : aotmList) {
			if(aotm.getSupervisorId()!=null && aotm.getTeamId()!=null) {
				List<Team> teamSuperList = teamRepository.findBySupervisorId(aotm.getSupervisorId()); 
				for (Team ts : teamSuperList) {
					if (ts.getName().equalsIgnoreCase(aotm.getTeamId())) {
						List<Team> teamSupervisorList = new ArrayList<Team>(); 						
						teamSupervisorList.add(ts);
						getOnlineAgentReport(teamSupervisorList);			
					}
				}
			}else if(aotm.getPartnerId()!=null) {
				List<Team> teamParternerList = new ArrayList<Team>(); 

				 teamParternerList = teamRepository.findAllByPartnerId(aotm.getPartnerId());
					getOnlineAgentReport(teamParternerList);			


			}
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void getOnlineAgentReport(List<Team> teams) {
		try {
			Calendar cal = Calendar.getInstance();
	    	cal.add(GregorianCalendar.DAY_OF_MONTH, -1);
			String fromDate = XtaasDateUtils.convertDateToString(XtaasDateUtils.getDateAtTime(cal.getTime(), 0, 0, 0, 0), "MM/dd/yyyy HH:mm:ss");  // "12/01/2019 00:00:00";// UTC date MM/DD//YYYY
			String toDate = XtaasDateUtils.convertDateToString(XtaasDateUtils.getDateAtTime(cal.getTime(), 23, 59, 59, 0), "MM/dd/yyyy HH:mm:ss");  // "12/01/2019 00:00:00";// UTC date MM/DD//YYYY
	
			
			Map<String, List<AgentActivityLog>> activityMap = new HashMap<String, List<AgentActivityLog>>();
			Map<String, List<Date>> onlineMap = new HashMap<String, List<Date>>();
			Map<String, List<Date>> breakMap = new HashMap<String, List<Date>>();
			Map<String, List<Date>> offlineMap = new HashMap<String, List<Date>>();
			Map<String, List<Date>> otherMap = new HashMap<String, List<Date>>();
			Map<String, List<Date>> trainingmap = new HashMap<String, List<Date>>();
			Map<String, List<Date>> meetingmap = new HashMap<String, List<Date>>();
			Map<String, String> agentPartnerMap = new HashMap<String, String>();
		
			Date startDate = XtaasDateUtils.getDate(fromDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			System.out.println(startDateInUTC);
			Date endDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
			System.out.println(endDateInUTC);
			

			List<String> agentIds = new ArrayList<String>();
			StringBuffer sb = new StringBuffer();
			
			
			/*
			 * case ONLINE: case REQUEST_BREAK: case REQUEST_MEETING: case
			 * REQUEST_OFFLINE: case REQUEST_OTHER: case REQUEST_TRAINING:
			 */
			
			
			for (Team t : teams) {
				//if (t.getName().equalsIgnoreCase("XTaaS-Team1")) { // change
																	// team ID
																	// to
																	// Demandshore
					List<TeamResource> teamResources = t.getAgents();
					for (TeamResource tr : teamResources) {
						agentIds.add(tr.getResourceId());
						List<AgentActivityLog> activityList = agentActivityLogRepository.getStatusesForReporting(
								tr.getResourceId(), startDateInUTC, endDateInUTC,
								Sort.by(Direction.ASC, "createdDate"));

						System.out.println(tr.getResourceId());
						if (activityList != null && activityList.size() > 0) {
							Agent agent = agentService.getAgent(tr.getResourceId());
							if (agent != null) {
								agentPartnerMap.put(tr.getResourceId(), agent.getPartnerId());
							}
							//if(tr.getResourceId().equalsIgnoreCase("piv_eval_mustafafaiz_0717"))
							activityMap.put(tr.getResourceId(), activityList);
						}
					}

				//}
			}

			Date lastTime = null;
			

			for (Map.Entry<String, List<AgentActivityLog>> entry : activityMap.entrySet()) {
				String lastStatus = "";

				List<AgentActivityLog> alog = entry.getValue();
				if (alog != null && alog.size() > 0) {
					for (AgentActivityLog aalog : alog) {
						String agentStatus = aalog.getStatus();
						if (agentStatus.equalsIgnoreCase("ONLINE")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
								if (onlineDates == null) {
									onlineDates = new ArrayList<Date>();
								}
								onlineDates.add(aalog.getCreatedDate());
								onlineMap.put(aalog.getAgentId(), onlineDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("ONLINE"))
									continue;
								List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
								if (onlineDates == null) {
									onlineDates = new ArrayList<Date>();
								}
								onlineDates.add(aalog.getCreatedDate());
								onlineMap.put(aalog.getAgentId(), onlineDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}
						}
						if (agentStatus.equalsIgnoreCase("ONBREAK")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								//lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> breakDates = breakMap.get(aalog.getAgentId());
								if (breakDates == null) {
									breakDates = new ArrayList<Date>();
								}
								breakDates.add(aalog.getCreatedDate());
								breakMap.put(aalog.getAgentId(), breakDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("ONBREAK"))
									continue;
								List<Date> breakDates = breakMap.get(aalog.getAgentId());
								if (breakDates == null) {
									breakDates = new ArrayList<Date>();
								}
								breakDates.add(aalog.getCreatedDate());
								breakMap.put(aalog.getAgentId(), breakDates);

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("MEETING")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								//lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> meetingDates = meetingmap.get(aalog.getAgentId());
								if (meetingDates == null) {
									meetingDates = new ArrayList<Date>();
								}
								meetingDates.add(aalog.getCreatedDate());
								meetingmap.put(aalog.getAgentId(), meetingDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("MEETING"))
									continue;
								List<Date> meetingDates = meetingmap.get(aalog.getAgentId());
								if (meetingDates == null) {
									meetingDates = new ArrayList<Date>();
								}
								meetingDates.add(aalog.getCreatedDate());
								meetingmap.put(aalog.getAgentId(), meetingDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("TRAINING")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastTime = aalog.getCreatedDate();
								List<Date> trainingDates = trainingmap.get(aalog.getAgentId());
								if (trainingDates == null) {
									trainingDates = new ArrayList<Date>();
								}
								trainingDates.add(aalog.getCreatedDate());
								trainingmap.put(aalog.getAgentId(), trainingDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("TRAINING"))
									continue;
								List<Date> trainingDates = trainingmap.get(aalog.getAgentId());
								if (trainingDates == null) {
									trainingDates = new ArrayList<Date>();
								}
								trainingDates.add(aalog.getCreatedDate());
								trainingmap.put(aalog.getAgentId(), trainingDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("OFFLINE")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								//lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> offlineDates = offlineMap.get(aalog.getAgentId());
								if (offlineDates == null) {
									offlineDates = new ArrayList<Date>();
								}
								offlineDates.add(aalog.getCreatedDate());
								offlineMap.put(aalog.getAgentId(), offlineDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("OFFLINE"))
									continue;
								List<Date> offlineDates = offlineMap.get(aalog.getAgentId());
								if (offlineDates == null) {
									offlineDates = new ArrayList<Date>();
								}
								offlineDates.add(aalog.getCreatedDate());
								offlineMap.put(aalog.getAgentId(), offlineDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("OTHER")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								//slastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> otherDates = otherMap.get(aalog.getAgentId());
								if (otherDates == null) {
									otherDates = new ArrayList<Date>();
								}
								otherDates.add(aalog.getCreatedDate());
								otherMap.put(aalog.getAgentId(), otherDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("OTHER"))
									continue;
								List<Date> otherDates = otherMap.get(aalog.getAgentId());
								if (otherDates == null) {
									otherDates = new ArrayList<Date>();
								}
								otherDates.add(aalog.getCreatedDate());
								otherMap.put(aalog.getAgentId(), otherDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
							}

						}

						//System.out.println(aalog.getStatus());
					}

				}
				//System.out.println("out");
			}

			
			
			
			
			for (Map.Entry<String, List<Date>> entry : onlineMap.entrySet()) {
				List<Date> dates = entry.getValue();
				String partnerId = agentPartnerMap.get(entry.getKey());
				
				sb.append(entry.getKey() + "|" + partnerId + "|");
				int mod = dates.size() % 2;
				int size = dates.size();
				if (mod > 0) {
					size = dates.size() - mod;
				}
				float online = 0;
				long breakvalue = 0;
				long meeting = 0;
				long offline = 0;
				long other = 0;
				long training = 0;
				int i = 0;
				while (i < size) {
					long temp = 0;
					Date d1 = dates.get(i);
					i = i + 1;
					Date d2 = dates.get(i);
					i++;
					temp = (d2.getTime() - d1.getTime()) / 1000;
					online = online + temp;
				}
				online = online / 3600;
				sb.append(online + " hrs\n");
				
				AgentOnlineTime  aot = new AgentOnlineTime();
				aot.setAgentId(entry.getKey());
				aot.setPartnerId(partnerId);
				aot.setStateDuration(online);
				aot.setJobDate(startDateInUTC);
				aot.setStatus("ONLINE");
				agentOnlineTimeRepository.save(aot);
				
			}
			System.out.println(sb);
			//fw = new FileWriter(FILENAME);
			//bw = new BufferedWriter(fw);
		//	bw.write(sb.toString());

			System.out.println("Done");

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
		
	
}
