package com.xtaas.service;

import com.xtaas.web.dto.PartnerFeedbackMappingDTO;

/**
 * Generate and store data feedback report on AWS S3 storage
 * 
 * @author pranay
 */
public interface DataFeedbackReportService {

	public String autoDataFeedbackReport(String jobId, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO);

	public String manualDataFeedbackReport(String name) throws Exception;

}
