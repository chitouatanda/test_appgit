package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.CampaignDTO;

@Service
public class DeadCallsReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DeadCallsReportServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	private static final String PRIMARY_CONTACT = "Primary Contact";

	public void sendDeadCallsReport() {
		sendReportForInternal();
		sendReportForClient();
	}

	@Async
	public void sendReportForInternal() {
		logger.debug("sendReportForInternal() : start");
		List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
		List<String> campaignIds = new ArrayList<>();
		Map<String, String> campaignsMap = new HashMap<String, String>();
		Calendar startCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();
		startCalendar.set(Calendar.HOUR_OF_DAY, 0);
		startCalendar.set(Calendar.MINUTE, 0);
		startCalendar.set(Calendar.SECOND, 0);
		startCalendar.set(Calendar.MILLISECOND, 0);
		Date fromDateStr = startCalendar.getTime();

		endCalendar.set(Calendar.HOUR_OF_DAY, 23);
		endCalendar.set(Calendar.MINUTE, 59);
		endCalendar.set(Calendar.SECOND, 59);
		startCalendar.set(Calendar.MILLISECOND, 0);
		Date endDateStr = endCalendar.getTime();
		logger.debug("sendReportForInternal() : fromDate : " + fromDateStr + " toDate : " + endDateStr);
		prospectCallLogs = prospectCallLogRepository.getProspectsByPartnerIdAndDispositionStatus("XTAAS CALL CENTER", "DIALERCODE", "DEADAIR", fromDateStr, endDateStr);
		if (prospectCallLogs != null && prospectCallLogs.size() > 0) {
			campaignIds = prospectCallLogs.stream()
					.map(prospectCallLog -> prospectCallLog.getProspectCall().getCampaignId()).distinct()
					.collect(Collectors.toList());
			List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(campaignIds, Arrays.asList());
			if (campaigns != null && campaigns.size() > 0) {
				for (CampaignDTO campaign : campaigns) {
					campaignsMap.put(campaign.getId(), campaign.getName());
				}
			}
			try {
				createExcelFileForInternal(prospectCallLogs, campaignsMap);
			} catch (Exception e) {
				logger.error("Error in sendReportForInternal() while craeting excel file !!!");
				e.printStackTrace();
			}
		} else {
			logger.debug("Prospects not found for internal for day : " + new Date());
		}
	}

	private void createExcelFileForInternal(List<ProspectCallLog> prospectCallLogs, Map<String, String> campaignsMap)
			throws IOException, ParseException {
		logger.debug("createExcelFileForInternal() : creating excel file for internal.");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeadersForInternalReport(headerRow);
		if (prospectCallLogs.size() > 0) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				Row row = sheet.createRow(++rowCount);
				writeToExcelFileForInternalReport(prospectCallLog, row, workbook, campaignsMap);
			}
			int totalRows = sheet.getPhysicalNumberOfRows();
			Row row = sheet.createRow(totalRows + 1);
			String fileName = "DeadAirCallsReport_" + dateStr + ".xlsx";
			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			workbook.close();
			String subject = "Dead Air Calls Report_" + dateStr;
			String message = " Please find the attached Dead calls report file.";
			Organization organization = organizationRepository.findOneById("XTAAS CALL CENTER");
			if (organization.getContacts() != null && organization.getContacts().size() > 0) {
				List<Contact> contacts = organization.getContacts();
				for (Contact contact : contacts) {
					if (contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) && contact.getEmail() != null
							&& !contact.getEmail().isEmpty()) {
						XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com", subject, message,
								fileNames);
						logger.debug("Dead Air calls report sent successfully to : " + contact.getEmail());
					}
				}
			}
		}

	}

	private void writeHeadersForInternalReport(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("AgentId");
		cell = headerRow.createCell(1);
		cell.setCellValue("Campaign Name");
		cell = headerRow.createCell(2);
		cell.setCellValue("Recording URL");
		cell = headerRow.createCell(3);
		cell.setCellValue("Call Duration");
		cell = headerRow.createCell(4);
		cell.setCellValue("Start Time");
	}

	private void writeToExcelFileForInternalReport(ProspectCallLog prospectCallLog, Row row, Workbook workbook,
			Map<String, String> campaignsMap) throws ParseException {
		Cell cell = row.createCell(0);
		cell.setCellValue(prospectCallLog.getProspectCall().getAgentId());
		cell = row.createCell(1);
		cell.setCellValue(campaignsMap.get(prospectCallLog.getProspectCall().getCampaignId()));
		cell = row.createCell(2);
		if (prospectCallLog.getProspectCall().getRecordingUrlAws() == null) {
			cell.setCellValue(prospectCallLog.getProspectCall().getRecordingUrl());
		} else {
			cell.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
		}
		cell = row.createCell(3);
		cell.setCellValue(prospectCallLog.getProspectCall().getCallDuration());
		cell = row.createCell(4);
		DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
		Date pstDate = pstFormat.parse(pstStr);
		cell.setCellValue(pstFormat.format(pstDate));
	}

	@Async
	private void sendReportForClient() {
		logger.debug("sendReportForClient() : start");
		List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
		List<String> campaignIds = new ArrayList<>();
		Map<String, String> campaignNamesMap = new HashMap<String, String>();
		Calendar startCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();
		startCalendar.set(Calendar.HOUR_OF_DAY, 00);
		startCalendar.set(Calendar.MINUTE, 00);
		startCalendar.set(Calendar.SECOND, 00);
		startCalendar.set(Calendar.MILLISECOND, 00);
		Date fromDateStr = startCalendar.getTime();

		endCalendar.set(Calendar.HOUR_OF_DAY, 23);
		endCalendar.set(Calendar.MINUTE, 59);
		endCalendar.set(Calendar.SECOND, 59);
		startCalendar.set(Calendar.MILLISECOND, 0);
		Date endDateStr = endCalendar.getTime();
		logger.debug("sendReportForClient() : fromDate : " + fromDateStr + " toDate : " + endDateStr);
		prospectCallLogs = prospectCallLogRepository.getProspectsByPartnerIdAndDispositionStatus("DEMANDSHORE", "DIALERCODE", "DEADAIR", fromDateStr, endDateStr);
		if (prospectCallLogs != null && prospectCallLogs.size() > 0) {
			campaignIds = prospectCallLogs.stream()
					.map(prospectCallLog -> prospectCallLog.getProspectCall().getCampaignId()).distinct()
					.collect(Collectors.toList());
			List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(campaignIds, Arrays.asList());
			if (campaigns != null && campaigns.size() > 0) {
				for (CampaignDTO campaign : campaigns) {
					campaignNamesMap.put(campaign.getId(), campaign.getName());
				}
			}
			try {
				createExcelFileForClient(prospectCallLogs, campaignNamesMap);
			} catch (Exception e) {
				logger.error("Error in sendReportForClient() while craeting excel file !!!");
				e.printStackTrace();
			}
		} else {
			logger.debug("Prospects not found for client for day : " + new Date());
		}
	}

	private void createExcelFileForClient(List<ProspectCallLog> prospectCallLogs, Map<String, String> campaignNamesMap)
			throws IOException, ParseException {
		logger.debug("createExcelFileForClient() : creating excel file for client.");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeadersForClientReport(headerRow);
		if (prospectCallLogs.size() > 0) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				Row row = sheet.createRow(++rowCount);
				writeToExcelFileForClientReport(prospectCallLog, row, workbook, campaignNamesMap);
			}
			int totalRows = sheet.getPhysicalNumberOfRows();
			Row row = sheet.createRow(totalRows + 1);
			String fileName = "DeadAirCallsReport_" + dateStr + ".xlsx";
			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			workbook.close();
			String subject = "Dead Air Call Report_" + dateStr;
			String message = " Please find the attached Dead calls report file.";
			Organization organization = organizationRepository.findOneById("DEMANDSHORE");
			if (organization.getContacts() != null && organization.getContacts().size() > 0) {
				List<Contact> contacts = organization.getContacts();
				for (Contact contact : contacts) {
					if (contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) && contact.getEmail() != null
							&& !contact.getEmail().isEmpty()) {
						XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com", subject, message,
								fileNames);
						logger.debug("Dead Air calls report sent successfully to : " + contact.getEmail());
					}
				}
			}
		}

	}

	private void writeHeadersForClientReport(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("AgentId");
		cell = headerRow.createCell(1);
		cell.setCellValue("Campaign Name");
		cell = headerRow.createCell(2);
		cell.setCellValue("Recording URL");
		cell = headerRow.createCell(3);
		cell.setCellValue("Call Duration");
		cell = headerRow.createCell(4);
		cell.setCellValue("Start Time");
	}

	private void writeToExcelFileForClientReport(ProspectCallLog prospectCallLog, Row row, Workbook workbook,
			Map<String, String> campaignNamesMap) throws ParseException {
		Cell cell = row.createCell(0);
		cell.setCellValue(prospectCallLog.getProspectCall().getAgentId());
		cell = row.createCell(1);
		cell.setCellValue(campaignNamesMap.get(prospectCallLog.getProspectCall().getCampaignId()));
		cell = row.createCell(2);
		if (prospectCallLog.getProspectCall().getRecordingUrlAws() == null) {
			cell.setCellValue(prospectCallLog.getProspectCall().getRecordingUrl());
		} else {
			cell.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
		}
		cell = row.createCell(3);
		cell.setCellValue(prospectCallLog.getProspectCall().getCallDuration());
		cell = row.createCell(4);
		DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
		Date pstDate = pstFormat.parse(pstStr);
		cell.setCellValue(pstFormat.format(pstDate));
	}
}
