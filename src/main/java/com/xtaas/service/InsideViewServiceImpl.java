package com.xtaas.service;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.infra.zoominfo.service.DataSlice;
import com.xtaas.logging.SplunkLoggingUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.db.entity.CompanyMaster;
import com.xtaas.db.entity.DataBuyMapping;
import com.xtaas.db.entity.InsideviewDataSource;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
import com.xtaas.insideview.Companies;
import com.xtaas.insideview.CompanyResponse;
import com.xtaas.insideview.Contacts;
import com.xtaas.insideview.DetailedContact;
import com.xtaas.insideview.DetailedContactJobResultInsideView;
import com.xtaas.insideview.Email;
import com.xtaas.insideview.PersonResponse;
import com.xtaas.insideview.Tickers;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;

@Service
public class InsideViewServiceImpl implements InsideviewService {

	@Autowired
	private DataBuyMappingRepository dataBuyMappingRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private DataBuyCleanup dataBuyCleanup;

	@Autowired
	private InsideviewDataSourceRepository insideviewDataSourceRepository;

	@Autowired
	DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private AbmListServiceImpl abmListServiceImpl;
	
	@Autowired
	StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private DataSlice dataSlice;
	
	
	@Autowired
	CompanyMasterRepository companyMasterRepository;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private PhoneDncService phoneDncService;

	@Autowired
	private AbmListNewRepository abmListRepositoryNew;

	@Autowired
	private LeadIQSearchServiceImpl leadIQSearchService;

	static Properties properties = new Properties();

	private static final Logger logger = LoggerFactory.getLogger(InsideViewServiceImpl.class);
	final private String insideViewToken;
	final private String insideView = "INSIDEVIEW";
	final private String NON_CALLABLE = "_NON_CALLABLE";
	// person urls
	private static final String manualPersonSearchUrl = "https://api.insideview.com/api/v1/contacts";//GET
	private static final String manualPersonDetailUrl = "https://api.insideview.com/api/v1/contact/{contactId}";//GET
	private static final String personSearchUrl = "https://api.insideview.com/api/v1/target/contacts";// POST
	private static final String personDetailUrl = "https://api.insideview.com/api/v1/target/contact/{newContactId}";// GET
	private static final String personDetailJobUrl = "https://api.insideview.com/api/v1/target/contact/job";// POST
	private static final String personDetailJobResult = "https://api.insideview.com/api/v1/target/contact/job/{jobId}/results";// GET
	private static final String personCountUrl = "https://api.insideview.com/api/v1/target/contacts/count";// POST
	private static final String personCompanyUrl = "https://api.insideview.com/api/v1/target/company/{newCompanyId}/contacts";// GET
	private static final String personWrongFeedback = "https://api.insideview.com/api/v1/target/contact/{newContactId}/wrongInfo";// POST
	// Company urls
	private static final String companySearchUrl = "https://api.insideview.com/api/v1/companies";//GET
	private static final String manualCompanyUrl = "https://api.insideview.com/api/v1/company/{companyId}";
	private static final String companyLookupUrl = "https://api.insideview.com/api/v1/target/company/lookup";
	private static final String companyDetailUrl = "https://api.insideview.com/api/v1/target/companies/{newCompanyId}";// GET
	private static final String companyCountUrl = "https://api.insideview.com/api/v1/target/companies/count";// POST
	private static final String companyTechProfileUrl = "https://api.insideview.com/api/v1/target/companies/{newCompanyId}/techProfile";// GET
	private static final String companyWrongFeedback = "https://api.insideview.com/api/v1/target/companies/{newCompanyId}/wrongInfo";// POST
	private int maxRecordsIndirect = 12;
	private int maxDirectNumbers = 20;

	/*
	 * =============================================================================
	 * ========================================================= ALL api end points
	 * are at below location ------->
	 * https://kb.insideview.com/hc/en-us/sections/200542326-Target-API-Endpoints
	 * =============================================================================
	 * =========================================================
	 */
/*
 * {"accessTokenDetails":{"accessToken":"f0VbD34ZWUJkB7DuzKfiKE8v+3lniKgTcC9lOkPm1R/u0PNIAT1BD2AVC08ZFdWIu+pXWAB361zZWxQ3Dv5/81wTqDchuL5SyAHKprPeLztLqMvI6AvofZUggIDldY2fUFq+o+lgLnJajEfhxSsa8iNmjeJQAh6s4tI4BNWBMbs=.eyJmZWF0dXJlcyI6InsgfSIsImNsaWVudElkIjoibnZhODZpbGI0YzFtdm83Mmdva2wiLCJncmFudFR5cGUiOiJjcmVkIiwidHRsIjoiMzE1MzYwMDAiLCJpYXQiOiIxNjE3MTM2MDU1IiwidmVyc2lvbiI6InYxIiwianRpIjoiYTgxNGFhYWQtM2Y5ZS00YzczLThlMDktMTQ1MzRhYzMwMzgzIn0=",
 * "tokenType":"bearer",
 * "expirationTime":"Wed, Mar 30, 2022 08:27:35 PM GMT",
 * "userInfo":{}}}

 */
	public InsideViewServiceImpl(@Value("${insideViewToken}") String insideViewToken) {
		// this.insideViewToken = insideViewToken;
		this.insideViewToken = "f0VbD34ZWUJkB7DuzKfiKE8v+3lniKgTcC9lOkPm1R/u0PNIAT1BD2AVC08ZFdWIu+pXWAB361zZWxQ3Dv5/81wTqDchuL5SyAHKprPeLztLqMvI6AvofZUggIDldY2fUFq+o+lgLnJajEfhxSsa8iNmjeJQAh6s4tI4BNWBMbs=.eyJmZWF0dXJlcyI6InsgfSIsImNsaWVudElkIjoibnZhODZpbGI0YzFtdm83Mmdva2wiLCJncmFudFR5cGUiOiJjcmVkIiwidHRsIjoiMzE1MzYwMDAiLCJpYXQiOiIxNjE3MTM2MDU1IiwidmVyc2lvbiI6InYxIiwianRpIjoiYTgxNGFhYWQtM2Y5ZS00YzczLThlMDktMTQ1MzRhYzMwMzgzIn0=";
	}

	/////////// Company Apis
	public void getCompaniesByCriteria() {
		StringBuilder params = new StringBuilder();
		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getResponseCriteria(companyLookupUrl, params.toString());
			CompanyResponse companies = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);
				System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	// This method to be used for ABM campaigns
	public void getCompaniesByDomain() {
		StringBuilder params = new StringBuilder();
		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getIVResponse(companySearchUrl, params.toString());
			CompanyResponse companies = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);
				System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void getCompanyDetail(String params) {
		//StringBuilder params = new StringBuilder();
		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getIVResponse(companyDetailUrl, params);
			CompanyResponse companies = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);
				System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void getCompaniesCount() {
		StringBuilder params = new StringBuilder();
		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getResponseCriteria(companyCountUrl, params.toString());
			CompanyResponse companies = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);
				System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void getCompanyTechProfile() {

		StringBuilder params = new StringBuilder();
		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getResponseCriteria(companyDetailUrl, params.toString());
			CompanyResponse companies = null;
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);
				System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	public void giveCompanyWrongFeedback() {
		// TODO
	}

	///////////// Person Apis
	public PersonResponse getPersonsByCriteria(String url, String urlParameters) {
		PersonResponse personResponse = null;
		try {
			// StringBuilder params = new StringBuilder();
			StringBuilder response = getResponseCriteria(url, urlParameters);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				personResponse = mapper.readValue(response.toString(), PersonResponse.class);
				System.out.println(personResponse);
			}
		} catch (Exception e) {
			logger.error("Error in buildivParam string : " + urlParameters);
			logger.error(e.getMessage());
		}
		return personResponse;
	}

	public Prospect getPersonDetail(String url, String urlParameters) {
		// {"contactId":157117130,"firstName":"Steve","lastName":"T","companyId":3645001,"companyName":"1
		// EDI Source, Inc.","phone":"+1 440 519
		// 7800","linkedinHandle":"https://www.linkedin.com/in/stevectuttle","active":true,"jobLevels":["Manager"],"jobFunctions":["IT
		// &
		// IS"],"peopleId":"72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-","fullName":"Steve
		// T","confidenceScore":45.0,"corporatePhone":"+1 440 519 7800","sources":["Web
		// References"],"titles":["IT Manager"]}
		Prospect prospectInfo = null;
		try {
			// StringBuilder params = new StringBuilder();
			StringBuilder response = getIVResponse(url, urlParameters);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				DetailedContact detailedContact = mapper.readValue(response.toString(), DetailedContact.class);
				prospectInfo = mapToProspectObject(detailedContact);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return prospectInfo;
	}

	private Prospect mapToProspectObject(DetailedContact detailedContact) {
		Prospect prospectInfo = new Prospect();
		prospectInfo.setSource("INSIDEVIEW");
		prospectInfo.setSourceType("INTERNAL");
		prospectInfo.setDataSource("Xtaas Data Source");
		prospectInfo.setSourceId(detailedContact.getContactId().toString());
		prospectInfo.setFirstName(detailedContact.getFirstName());
		prospectInfo.setLastName(detailedContact.getLastName());
		prospectInfo.setSourceCompanyId(Long.toString(detailedContact.getCompanyId()));
		prospectInfo.setCompany(detailedContact.getCompanyName());
		prospectInfo.setPhone(getFormattedPhone(detailedContact.getPhone()));
		prospectInfo.setCustomAttribute("linkedinUrl", detailedContact.getLinkedinHandle());
		prospectInfo.setZoomPersonUrl(detailedContact.getLinkedinHandle());
		prospectInfo.setCompanyPhone(getFormattedPhone(detailedContact.getCorporatePhone()));
		prospectInfo.setTitle(detailedContact.getTitles().get(0));
		prospectInfo.setConfidenceScore(detailedContact.getConfidenceScore().toString());
		if (detailedContact.getJobFunctions() != null && detailedContact.getJobFunctions().size() > 0) {
			prospectInfo.setDepartment(detailedContact.getJobFunctions().get(0));
		}
		if (detailedContact.getJobLevels() != null && detailedContact.getJobLevels().size() > 0) {
			prospectInfo.setManagementLevel(getStdMgmtLevl(detailedContact.getJobLevels().get(0)));
		}
		if (detailedContact.getCompanyDetails() != null) {
			prospectInfo.setCustomAttribute("maxRevenue", detailedContact.getCompanyDetails().getRevenue());
			prospectInfo.setCustomAttribute("maxEmployeeCount", detailedContact.getCompanyDetails().getEmployees());
			if (detailedContact.getCompanyDetails().getWebsites().size() > 0) {
				prospectInfo.setCustomAttribute("domain", detailedContact.getCompanyDetails().getWebsites().get(0));
			}
		}
		if (detailedContact.getEmail() != null && detailedContact.getEmailValidationStatus() != null
				&& detailedContact.getEmailValidationStatus().equalsIgnoreCase("ValidEmail")) {
			prospectInfo.setEmail(detailedContact.getEmail());
		} else if (detailedContact.getEmails() != null && detailedContact.getEmails().size() > 0) {
			for (Email emailObj : detailedContact.getEmails()) {
				if (emailObj.getValidationStatus().equalsIgnoreCase("ValidEmail")) {
					prospectInfo.setEmail(emailObj.getEmail());
					break;
				}
			}
			if (prospectInfo.getEmail() == null || prospectInfo.getEmail().isEmpty()) {
				prospectInfo.setEmail(detailedContact.getEmails().get(0).getEmail());
			}
		}
		return prospectInfo;
	}

	private Prospect mapDetailedJobResultToProspectObject(DetailedContactJobResultInsideView detailedContact) {
		Prospect prospectInfo = new Prospect();
		prospectInfo.setSource("INSIDEVIEW");
		prospectInfo.setSourceType("INTERNAL");
		prospectInfo.setDataSource("Xtaas Data Source");
		prospectInfo.setSourceId(detailedContact.getPeopleId());
		if(StringUtils.isNotBlank(detailedContact.getFirstName())){
			prospectInfo.setFirstName(detailedContact.getFirstName());
		}else{
			prospectInfo.setFirstName("");
		}
		if(StringUtils.isNotBlank(detailedContact.getLastName())){
			prospectInfo.setLastName(detailedContact.getLastName());
		}else{
			prospectInfo.setLastName("");
		}
		if(StringUtils.isNotBlank(detailedContact.getPhone())){
			prospectInfo.setPhone(getFormattedPhone(detailedContact.getPhone()));
		}else{
			prospectInfo.setPhone("");
		}

		prospectInfo.setSourceCompanyId(detailedContact.getCompanyId());
		prospectInfo.setCompany(detailedContact.getCompanyName());
		prospectInfo.setCustomAttribute("linkedinUrl", detailedContact.getLinkedinHandle());
		prospectInfo.setZoomPersonUrl(detailedContact.getLinkedinHandle());
		prospectInfo.setCompanyPhone(getFormattedPhone(detailedContact.getCorporatePhone()));
		prospectInfo.setTitle(detailedContact.getTitles());
		prospectInfo.setConfidenceScore(detailedContact.getConfidenceScore());
		prospectInfo.setDepartment(detailedContact.getJobFunctions());
		List<String> mgtListArr = Arrays.asList(detailedContact.getJobLevels().split("\\|"));
		prospectInfo.setManagementLevel(getStdMgmtLevl(mgtListArr.get(0)));
		/*if(detailedContact.getCOMPANY_revenue()!=null && !detailedContact.getCOMPANY_revenue().isEmpty()) {
			String re = detailedContact.getCOMPANY_revenue();
			if(detailedContact.getCOMPANY_revenue().contains(".")) {
				re = re.substring(0,re.indexOf("."));
			}
			prospectInfo.setCustomAttribute("maxRevenue", re+"000000");
		}else 
			prospectInfo.setCustomAttribute("maxRevenue", "");*/

		if(detailedContact.getCOMPANY_revenue()!=null && !detailedContact.getCOMPANY_revenue().isEmpty()) {
			String re = detailedContact.getCOMPANY_revenue();
			if(detailedContact.getCOMPANY_revenue().contains(".")) {
				re = re.substring(0,re.indexOf("."));
			}
			Map<String, String> empRangeMap = getMinAndMaxRevenueCount(re+"000000");
			if (empRangeMap != null) {
				prospectInfo.setCustomAttribute("maxRevenue", empRangeMap.get("maxRevenue"));
				prospectInfo.setCustomAttribute("minRevenue", empRangeMap.get("minRevenue"));
			}
		}
		
		Map<String, String> empRangeMap = getMinAndMaxEmployeeCount(detailedContact.getCOMPANY_employees());
		if (empRangeMap != null) {
			prospectInfo.setCustomAttribute("maxEmployeeCount", empRangeMap.get("maxEmp"));
			prospectInfo.setCustomAttribute("minEmployeeCount", empRangeMap.get("minEmp"));
		}
		prospectInfo.setCustomAttribute("domain", detailedContact.getCOMPANY_website());

		if(StringUtils.isNotBlank(detailedContact.getCOMPANY_subIndustry())){
			prospectInfo.setIndustry(detailedContact.getCOMPANY_subIndustry());
		}else{
			prospectInfo.setIndustry(detailedContact.getCOMPANY_industry());
		}

		if(StringUtils.isNotBlank(detailedContact.getCOMPANY_industry())){
			prospectInfo.setIndustryList(Arrays.asList(detailedContact.getCOMPANY_industry()));
		}

		prospectInfo.setCountry(detailedContact.getCOMPANY_country());

		setStateAndTimezone(prospectInfo,detailedContact.getCOMPANY_state());
		if (detailedContact.getEmail() != null && detailedContact.getEmailValidationStatus() != null
				&& detailedContact.getEmailValidationStatus().equalsIgnoreCase("ValidEmail")) {
			prospectInfo.setEmail(detailedContact.getEmail());
		}else {
			prospectInfo.setEmail("");
		}
		return prospectInfo;
	}

	public void setStateAndTimezone(Prospect prospectInfo,  String state){
		if(StringUtils.isNotBlank(prospectInfo.getCountry()) && StringUtils.isNotBlank(prospectInfo.getPhone())){
			String insideviewCountry = prospectInfo.getCountry().toLowerCase();
			if(insideviewCountry.equalsIgnoreCase("usa") ||
					insideviewCountry.equalsIgnoreCase("united states")){
				String st = "";
				String timezone = "";
				timezone = properties
						.getProperty(leadIQSearchService.findAreaCode(prospectInfo.getPhone()));
				if (timezone != null && !timezone.isEmpty()) {
					if (timezone.equalsIgnoreCase("US/Pacific")) {
						st = "CA";
					} else if (timezone.equalsIgnoreCase("US/Eastern")) {
						st = "DE";
					} else if (timezone.equalsIgnoreCase("US/Mountain")) {
						st = "CO";
					} else if (timezone.equalsIgnoreCase("US/Central")) {
						st = "FL";
					}
				}
				prospectInfo.setTimeZone(timezone);
				prospectInfo.setStateCode(st);

				if(StringUtils.isBlank(prospectInfo.getTimeZone())){
					prospectInfo.setTimeZone("US/Pacific");
				}
				if(StringUtils.isBlank(prospectInfo.getStateCode())){
					prospectInfo.setStateCode("CA");
				}

			}else {
				// get state code and timezone except usa
				StateCallConfig scc = leadIQSearchService.getStateCodeAndTimeZone(state, prospectInfo.getCountry());
				if (scc != null) {
					prospectInfo.setTimeZone(scc.getTimezone());
					prospectInfo.setStateCode(scc.getStateCode());
				}
			}
		}
	}

	
	private Map<String, String> getMinAndMaxEmployeeCount(String emplyeeCount) {
		if(emplyeeCount!=null && !emplyeeCount.isEmpty()) {
			int empCountInt = Integer.parseInt(emplyeeCount);
			Map<String, String> empRangeMain = new HashMap<>();
			List<Map<String, Integer>> empRangeList = XtaasUtils.getEmployeeRangesList();
			for (Map<String, Integer> employeeMap : empRangeList) {
				int minEmp = employeeMap.get("minEmp");
				if (empCountInt >= minEmp) {
					if (employeeMap.containsKey("maxEmp")) {
						int maxEmp = employeeMap.get("maxEmp");
						if (empCountInt < maxEmp) {
							empRangeMain.put("minEmp", String.valueOf(minEmp));
							empRangeMain.put("maxEmp", String.valueOf(maxEmp));
							return empRangeMain;
						}
					} else {
						empRangeMain.put("minEmp", String.valueOf(minEmp));
						empRangeMain.put("maxEmp", "");
						return empRangeMain;
					}
				}
			}
		}
		return null;
	}
	
	 public String getTimeZone(String stateCode) {
		 // List<OutboundNumber> outboundNumberList;
		 // int areaCode =  Integer.parseInt(findAreaCode(prospect.getPhone())); 
		 if(stateCode!=null && stateCode!=null){
			
			 StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(stateCode);
			 return stateCallConfig.getTimezone(); 
		 }
		// String s = prospect.getFirstName()+""+prospect.getLastName();
		// logger.debug("prospect State is null : "+s);
		  //if(stateCallConfig==null){
		   return null;
		 // }
		  
		  //find a random number from the list
		  
		 
	}

	private String getFormattedPhone(String phone) {
		phone = phone.replaceAll("[^\\d]", "");
		phone = "+" + phone.trim();
		return phone;
	}

	public void getPersonsCount() {

	}

	public void getPersonsByCompany() {

	}

	public void givePersonWrongFeedback() {

	}

	public StringBuilder getIVResponse(String url, String params) {
		StringBuilder response = new StringBuilder();

		try {
			if (url.contains("{newContactId}")) {
				url = url.replace("{newContactId}", params);
				url = url + "?retrieveCompanyDetails=true";
			} else if (url.contains("{newCompanyId}")) {
				url = url.replace("{newCompanyId}", params);
			} else if (url.contains("{companyId}")) {
				url = url.replace("{companyId}", params);
			}else if (url.contains("{contactId}")) {
				url = url.replace("{contactId}", params);
			}else {
				url = url + "?" + params;
			}
			HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();
			// add request header
			httpClient.setRequestMethod("GET");
			httpClient.setRequestProperty("accessToken", insideViewToken);
			httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
			httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			httpClient.setRequestProperty("Accept", "application/json");
			int responseCode = httpClient.getResponseCode();
			logger.debug("\nSending 'GET' request to URL : " + url);
			logger.debug("Response Code : " + responseCode);
			try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
				String line;

				while ((line = in.readLine()) != null) {
					response.append(line);
				}
				logger.debug(response.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public StringBuilder getResponseCriteria(String url, String urlParameters) {
		StringBuilder response = new StringBuilder();
		try {
			if (url.contains("{newContactId}")) {
				url = url.replace("{newContactId}", urlParameters);
			} else if (url.contains("{newCompanyId}")) {
				url = url.replace("{newCompanyId}", urlParameters);
			} else {
				urlParameters = getInsideViewParams(urlParameters);
			}
			HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();
			httpClient.setRequestMethod("POST");
			httpClient.setRequestProperty("accessToken", insideViewToken);
			httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
			httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			httpClient.setRequestProperty("Accept", "application/json");

			if(urlParameters.contains(" "))
				urlParameters = urlParameters.replace(" ", "%20");
			// Send post request
			httpClient.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(httpClient.getOutputStream())) {
				wr.writeBytes(urlParameters);
				wr.flush();
			}
			int responseCode = httpClient.getResponseCode();
			try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
				}
				logger.debug(response.toString());
			}
		} catch (Exception e) {
			logger.error("Error in buildivParam string : " + urlParameters);
			e.printStackTrace();
			return null;
		}
		return response;
	}

	public String createContactDetailJob(String url, List<String> contactIdList, String campaignId) {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-M-yyyy");
		String formattedDate = myDateObj.format(myFormatObj);
		String fileName = campaignId + "_" + formattedDate + ".csv";
		File file = new File(fileName);
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			url = url + "?retrieveCompanyDetails=true";
			if (serverUrl.contains("localhost")) {
				url += "&webhook=" + "https://5534-122-161-89-180.ngrok.io" + "/spr/insideview/jobstatus";
			} else {
				url += "&webhook=" + serverUrl + "/spr/insideview/jobstatus";
			}
			FileWriter writer = new FileWriter(file);
			String collect = contactIdList.stream().collect(Collectors.joining("\n"));

			writer.write(collect);
			writer.close();
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("accessToken", insideViewToken);
			httppost.addHeader("Accept", "application/json");
			httppost.addHeader("Content-Type", "application/octet-stream");
			httppost.setHeader("Connection", "keep-alive");

			byte[] bFile = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(bFile);
			fis.close();
			// add the binary data to the query
			httppost.setEntity(new ByteArrayEntity(bFile));

			// Send post request
			HttpResponse httpresponse = httpclient.execute(httppost);
			int responseCode = httpresponse.getStatusLine().getStatusCode();
			if (responseCode == 200) {
				String line;
				StringBuilder response = new StringBuilder();
				try (BufferedReader in = new BufferedReader(new InputStreamReader(httpresponse.getEntity().getContent()))) {
					while ((line = in.readLine()) != null) {
						response.append(line);
					}
					String responseString = response.toString();
					logger.info("Insideview response while creating the job , Response : [{}] ",responseString);
				}
				// {"jobId":"sh7dae35eejeiv1js8hg","status":"accepted","type":"targetContactDetails"}
				if (response != null) {
					ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
					mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
					Map<String, String> responseValues = mapper.readValue(response.toString(), Map.class);
					if (responseValues.containsKey("jobId")) {
						String jobId = responseValues.get("jobId");
						return jobId;
					}
				}
			}
		} catch (Exception e) {
			//TODO Save file if exception occurs while sending csv file
			logger.error("Error in insideview Job creation. Exception  [{}] ", e);
			e.printStackTrace();
			return "Error";
		} finally {
			try {
				Files.deleteIfExists(file.toPath());
			} catch (IOException e) {
				logger.error("Error in insideview File deletion");
			}
		}
		return "Error";
	}

	public String buildIVParams(Campaign campaign, CampaignCriteria cc, int page, DataBuyQueue dbq) {
		StringBuilder sb = new StringBuilder();
		if (cc.getCountry() != null && !cc.getCountry().isEmpty()) {
			Map<String, String> dbmCountryMap = new HashMap<String, String>();
			List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Country");
			for (DataBuyMapping dbm : dbmList) {
				dbmCountryMap.put(dbm.getSearchValue(), dbm.getRootTaxonamyId());
			}
			StringBuilder sbCountry = new StringBuilder();
			List<String> countryList = Arrays.asList(cc.getCountry().split(","));
			for (String country : countryList) {
				String countryCode = dbmCountryMap.get(country);
				if (sbCountry.toString().isEmpty()) {
					sbCountry.append(countryCode);
				} else {
					sbCountry.append(",");
					sbCountry.append(countryCode);
				}
			}
			cc.setCountryList(countryList);
			sb.append("countries=");
			sb.append(sbCountry.toString());
		}
		if (cc.getState() != null && !cc.getState().isEmpty()) {
			sb.append("&state=");
			sb.append(cc.getState());
		}
		if (cc.getTitleKeyWord() != null && !cc.getTitleKeyWord().isEmpty()) {
			sb.append("&titles=");
			sb.append(cc.getTitleKeyWord());
		}
		if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()) {
			Map<String, List<DataBuyMapping>> dbmIndustryMap = new HashMap<String, List<DataBuyMapping>>();
			List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Industry");
			for (DataBuyMapping dbm : dbmList) {
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(dbm.getSearchString());
				if (dbmSubList == null) {
					dbmSubList = new ArrayList<DataBuyMapping>();
				}
				dbmSubList.add(dbm);
				dbmIndustryMap.put(dbm.getSearchString(), dbmSubList);
			}
			StringBuilder sbIndustry = new StringBuilder();
			List<String> industryList = Arrays.asList(cc.getIndustry().split(","));
			for (String ind : industryList) {
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(ind);
				if (dbmSubList != null) {
					for (DataBuyMapping sdbm : dbmSubList) {
						if (sbIndustry.toString().isEmpty()) {
							sbIndustry.append(sdbm.getChildTaxonamyId());
						} else {
							sbIndustry.append(",");
							sbIndustry.append(sdbm.getChildTaxonamyId());
						}
					}
				}
			}
			cc.setSubIndustries(sbIndustry.toString());

			// sb.append("&industries=");
			// sb.append(cc.getIndustry());
			if (StringUtils.isBlank(cc.getSubIndustries())) {
				logger.error("No industries found in insideview for zoominfo industries [{}] ", cc.getIndustry());
				return null;
			}
		}else{
			//if no industry selected
			Map<String, List<DataBuyMapping>> dbmIndustryMap = new HashMap<String, List<DataBuyMapping>>();
			List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Industry");
			for (DataBuyMapping dbm : dbmList) {
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(dbm.getSearchValue());
				if (dbmSubList == null) {
					dbmSubList = new ArrayList<DataBuyMapping>();
				}
				dbmSubList.add(dbm);
				dbmIndustryMap.put(dbm.getSearchString(), dbmSubList);
			}
			List<String> removeIndustries = removeDefaultIndustry();
			for(String ind : removeIndustries){
				if(dbmIndustryMap.containsKey(ind)){
					dbmIndustryMap.remove(ind);
				}
			}
			StringBuilder sbIndustry = new StringBuilder();
			Set<String> keysSet  = dbmIndustryMap.keySet();
			for(String key :  keysSet){
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(key);
				if (dbmSubList != null) {
					for (DataBuyMapping sdbm : dbmSubList) {
						if (sbIndustry.toString().isEmpty()) {
							sbIndustry.append(sdbm.getChildTaxonamyId());
						} else {
							sbIndustry.append(",");
							sbIndustry.append(sdbm.getChildTaxonamyId());
						}
					}
				}
			}

			cc.setSubIndustries(sbIndustry.toString());

		}
		if (cc.getSubIndustries() != null && !cc.getSubIndustries().isEmpty()) {
			sb.append("&subIndustries=");
			sb.append(cc.getSubIndustries());
		}
		// if(cc.getState()!=null && !cc.getState().isEmpty()) {
		sb.append("&active=true");// If present returns only active / inactive contacts. //Defaulted to true
		// sb.append(cc.getState());
		// }
		if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign)) {// TODO this is being used as email campaign,once
																																			// we introduce email campaign change this to
																																			// email campaign.
			sb.append("&isEmailRequired=true");
		}
		// Defaulted to true, prospects without phone is not needed.
		// sb.append("&isPhoneRequired=true");// If set to true, only contacts with a
		// phone number will be counted.

		if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()
				&& !cc.getMinEmployeeCount().equalsIgnoreCase("0")) {
			sb.append("&minEmployees=");
			sb.append(Integer.parseInt(cc.getMinEmployeeCount()));
		}
		if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()
				&& !cc.getMaxEmployeeCount().equalsIgnoreCase("0")) {
			sb.append("&maxEmployees=");
			sb.append(Integer.parseInt(cc.getMaxEmployeeCount()));
		}
		if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty() && !cc.getMinRevenue().equalsIgnoreCase("0")) {
			sb.append("&minRevenue=");
			Integer multiple = 1000000;
			Long value = Long.parseLong(cc.getMinRevenue());
			Long minvalue = new Double(value / multiple).longValue();
			sb.append(minvalue);
		}
		if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty() && !cc.getMaxRevenue().equalsIgnoreCase("0")) {
			sb.append("&maxRevenue=");
			Integer multiple = 1000000;
			Long value =  Long.parseLong(cc.getMaxRevenue());
			Long maxvalue = new Double(value / multiple).longValue();
			sb.append(maxvalue);
		}
		sb.append("&primaryIndustryOnly=true");
		if (cc.getNaicsCode() != null && !cc.getNaicsCode().isEmpty()) {
			sb.append("&naics=");
			sb.append(cc.getNaicsCode());
			sb.append("&primaryNAICSOnly=true");
		}
		if (cc.getSicCode() != null && !cc.getSicCode().isEmpty()) {
			sb.append("&sic=");
			sb.append(cc.getSicCode());
			sb.append("&primarySICOnly=true");
		}
		// ##############################################################
		// TODO check why below param is not working
		// sb.append("&isPhoneRequired=true");//If set to true, only contacts with a
		// phone number will be counted.
		// ##############################################################

		// sb.append("&industries=8,9")
		// sb.append("&subIndustries=8_1,8_10,8_11,8_12,8_13,8_14,8_15,8_16,8_17,8_18,8_19,8_2,8_20");//https://kb.insideview.com/hc/en-us/articles/203433458-API-Reference-Data
		// sb.append("&active=true");//If present returns only active / inactive
		// contacts
		// sb.append("&isEmailRequired=false");//If set to true, only contacts with an
		// email address will be counted.
		// sb.append("&isPhoneRequired=true");//If set to true, only contacts with a
		// phone number will be counted.

		if(cc.isRestFullAPI() && StringUtils.isNotBlank(cc.getPhoneType())){
			sb.append("&phoneType=");
			sb.append(cc.getPhoneType());
			// ***Accepts DIRECT, CORP, and ANY to find the
		}else{
			if(!campaign.isIndirectBuyStarted()){
				sb.append("&phoneType=DIRECT");// ***Accepts DIRECT, CORP, and ANY to find the
			}
		}

		// corresponding contact's phone numbers.
		Map<String, Integer> jobFunctionMap = getDepartment();
		if (cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
			sb.append("&jobFunctionsV2=");
			List<String> departmentList = Arrays.asList(cc.getDepartment().split(","));
			boolean first = true;
			for (String deptStr : departmentList) {
				if (jobFunctionMap.get(deptStr) != null) {
					if (first) {
						first = false;
					} else {
						sb.append(",");
					}
					sb.append(jobFunctionMap.get(deptStr).toString());	
				}
			}
		}

		Map<String, Integer> managementMap = geManagementLevel();
		if (cc.getManagementLevel() != null && !cc.getManagementLevel().isEmpty()) {
			sb.append("&jobLevelsV2=");
			List<String> managementList = Arrays.asList(cc.getManagementLevel().split(","));
			boolean first = true;
			for (String deptStr : managementList) {
				// managementMap.get(deptStr);
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
				if (deptStr.equalsIgnoreCase("Mid Management")) {
					sb.append(managementMap.get("Executives").toString());
				} else {
					sb.append(managementMap.get(deptStr).toString());
				}
			}
		}


		// sb.append("&jobFunctions=7,8");////
		// https://kb.insideview.com/hc/en-us/articles/203433458-API-Reference-Data
		// sb.append("&jobLevels=5,4");
		/*
		 * sb.append("&minEmployees=10"); sb.append("&maxEmployees=100");
		 * sb.append("&minRevenue=500"); sb.append("&maxRevenue=1000000");
		 */
		// sb.append("&naics=511");
		// sb.append("&sic=511")
		// sb.append("&categoryIds=1000000");//??????
		// sb.append("&subCategoryIds=1000000");//??????
		// sb.append("&productIds")//????
		// sb.append("&primaryIndustryOnly=true");
		// sb.append("&primaryNAICSOnly=true");
		// sb.append("&primarySICOnly=true");

		if(CollectionUtils.isNotEmpty(cc.getAbmCompanyIds())){
			List<String> list = cc.getAbmCompanyIds();
			String companyName = getCompanyName(list.get(page)); // call company search api for getting company name

			sb.append("&companyName=");
			sb.append(companyName);
			sb.append("&resultsPerPage=500");
			sb.append("&page=" + 1);// Default: 1
		}else {
			sb.append("&resultsPerPage=500");
			sb.append("&page=" + page);// Default: 1
		}
		sb.append("&sortBy=companyName");// Attributes used to sort the list of results. Valid values are: active, title,
																			// popularity, companyName
		sb.append("&sortOrder=asc");

		return sb.toString();
	}


	public List<String> removeDefaultIndustry(){
		List<String> newRemoveInd = new ArrayList<>();
		newRemoveInd.add("1802");
		newRemoveInd.add("263946");
		newRemoveInd.add("132874");
		newRemoveInd.add("198410");
		newRemoveInd.add("67338");
		newRemoveInd.add("2570");
		newRemoveInd.add("201482");
		newRemoveInd.add("4874");
		newRemoveInd.add("135946");
		newRemoveInd.add("70410");
		newRemoveInd.add("267018");
		newRemoveInd.add("4618");
		newRemoveInd.add("135690");
		newRemoveInd.add("70154");
		return newRemoveInd;
	}

	private Map<String, Integer> getDepartment() {
		// Map<String,Integer> mgmtMap = new HashMap<String, Integer>();
		/*
		 * Sales 1 Marketing 2 Finance 3 Human Resources 4 Engineering & Research 5
		 * Operations & Administration 6 IT 7 Other 8
		 */
		Map<String, Integer> titleMap = new HashMap<String, Integer>();

		titleMap.put("4980839", 6);
		titleMap.put("4784231", 9);
		titleMap.put("2293762", 19);
		titleMap.put("2228226", 15);
		titleMap.put("3211266", 19);
		titleMap.put("3801090", 5);

		titleMap.put("4653058", 9);
		titleMap.put("4849666", 13);
		titleMap.put("1376258", 13);
		titleMap.put("5570562", 13);
		titleMap.put("5505026", 9);
		titleMap.put("6029314", 18);
		titleMap.put("4718594", 9);
		titleMap.put("1114114", 9);
		titleMap.put("5308418", 9);
		titleMap.put("4587623", 2);
		titleMap.put("262146", 2);
		titleMap.put("1769474", 2);
		titleMap.put("327682", 2);
		titleMap.put("2621442", 10);
		titleMap.put("1441794", 2);
		titleMap.put("4718695", 8);
		titleMap.put("2162690", 8);
		titleMap.put("4915202", 8);
		titleMap.put("3473410", 8);
		titleMap.put("5177346", 8);
		titleMap.put("5242882", 8);
		titleMap.put("5832807",11);
		titleMap.put("1572866", 11);
		titleMap.put("1507330", 11);
		titleMap.put("4456551", 14);
		titleMap.put("786434", 14);
		titleMap.put("2555906", 14);
		titleMap.put("3932162", 14);
		titleMap.put("393216", 14);
		titleMap.put("3276802", 14);
		titleMap.put("4325378", 14);
		titleMap.put("5701735", 15);
		titleMap.put("4784130", 15);
		titleMap.put("3670018", 15);
		titleMap.put("589826", 15);
		titleMap.put("3145730", 15);
		titleMap.put("4653159", 16);
		titleMap.put("3014658", 16);
		titleMap.put("4063234", 16);
		titleMap.put("4390914", 16);
		titleMap.put("2752514", 16);
		titleMap.put("5373954", 16);
		titleMap.put("5701634", 12);
		titleMap.put("2686978", 16);
		titleMap.put("6094850", 16);
		titleMap.put("4522087", 21);
		titleMap.put("393218", 4);
		titleMap.put("196610", 21);
		titleMap.put("4849767", 19);
		titleMap.put("4259942", 3);



		return titleMap;
	}
	
	private String getStdMgmtLevl(String mgmtLevel) {
		String ivmgmtLevel = "C-Level";
		if(mgmtLevel.equalsIgnoreCase("C-Level") ||mgmtLevel.equalsIgnoreCase("C Level")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Senior Executive")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("VP") || mgmtLevel.equalsIgnoreCase("Vice President")) {
			ivmgmtLevel = "VP-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Director")) {
			ivmgmtLevel = "DIRECTOR";
		}else if(mgmtLevel.equalsIgnoreCase("Manager")) {
			ivmgmtLevel = "MANAGER";
		}else if(mgmtLevel.equalsIgnoreCase("Board Member")) {
			ivmgmtLevel = "Board Members";
		}else if(mgmtLevel.equalsIgnoreCase("Other")) {
			ivmgmtLevel = "Non Management";
		}
		return ivmgmtLevel;
	}

	private Map<String, Integer> geManagementLevel() {
		/*
		 * C-Level 1 Senior Executive 2 VP 3 Director 4 Manager 5 Board Member 6 Other 7
		 */
		Map<String, Integer> mgmtMap = new HashMap<String, Integer>();
		mgmtMap.put("Board Members", 0);
		mgmtMap.put("C_EXECUTIVES", 1);
		mgmtMap.put("Executives", 2);
		mgmtMap.put("VP_EXECUTIVES", 3);
		mgmtMap.put("DIRECTOR", 4);
		mgmtMap.put("MANAGER", 5);
		mgmtMap.put("Non Management", 6);
		return mgmtMap;
	}

	public void getData(Campaign campaign, DataBuyQueue dbq, Long ivRequestBuy) {
		CampaignCriteria campaignCriteria = new CampaignCriteria();
		if(dbq.isRestFullAPI()){
			campaignCriteria = dbq.getCampaignCriteria();
		}else {
			List<Expression> expressions = dbq.getCampaignCriteriaDTO();
			campaignCriteria.setExpressions(expressions);
			campaignCriteria.setAborted(false);
			// campaignCriteria.setIgnoreDefaultIndustries();
			// campaignCriteria.setIgnoreDefaultTitles(zoomBuySearchDTO.isIgnoreDefaultTitles());
			// campaignCriteria.setParallelBuy(zoomBuySearchDTO.isParallelBuy());
			campaignCriteria.setRequestJobDate(dbq.getJobRequestTime());
			campaignCriteria.setOrganizationId(campaign.getOrganizationId());
			campaignCriteria.setCampaignId(campaign.getId());
			campaignCriteria.setTotalPurchaseReq(dbq.getInsideViewRequestCount());
			if (campaign.getLeadsPerCompany() > 0) {
				campaignCriteria.setLeadPerCompany(campaign.getLeadsPerCompany());
			}
			if (campaign.getDuplicateCompanyDuration() > 0) {
				campaignCriteria.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration() + " Days");
			}
			if (campaign.getDuplicateLeadsDuration() > 0) {
				campaignCriteria.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration() + " Days");
			}


			/*
			 * if(zoomBuySearchDTO.isIgnoreANDTitles()){
			 * campaignCriteria.setAndTitleIgnored(true); }else{
			 * campaignCriteria.setAndTitleIgnored(false); }
			 * if(zoomBuySearchDTO.isIgnorePrimaryIndustries()){
			 * campaignCriteria.setPrimaryIndustriesOnly(true); }else{
			 * campaignCriteria.setPrimaryIndustriesOnly(false); }
			 */

			// if (ivRequestBuy > 500) {
			// requestBuyPerPage = 500;
			// } else {
			// requestBuyPerPage = ivRequestBuy;
			// }
			campaignCriteria = setCampaignCriteria(campaignCriteria, expressions);
			// List<Contacts> searchDetailsContactListMain = new ArrayList<Contacts>();

		}
		if(campaign.isABM()){
			List<String> listOfCompanyNames = getABMCompanyName(campaign);
			if(CollectionUtils.isNotEmpty(listOfCompanyNames)){
				campaignCriteria.setAbmCompanyIds(listOfCompanyNames);
			}
		}
		int page = 1;
		List<String> contactIdListMain = new ArrayList<String>();
		long totalPurchasedData = 0L;
		long internalDataBought = 0L;
		List<ProspectCallLog> callableProspects = getCallableList(campaign);
		HashMap<String, HashMap<String, Integer>> directIndirectCallableCompanyCountMap = XtaasUtils.getCallableDirectIndirectCompanyCountMap(callableProspects);
		HashMap<String, Integer> directPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
				.get("directCompanyMap");
		HashMap<String, Integer> indirectPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
				.get("indirectCompanyMap");
		while (totalPurchasedData < ivRequestBuy) {
			DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueue.DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
			if (dbq1 != null) {
				break;
			}
			String ivParams = null;
			if(campaignCriteria.isRestFullAPI()){
				ivParams = buildIVParamsForRestAPI(campaign, campaignCriteria, page);
			}else {
				//TODO add conditions for state code and zip
				ivParams = buildIVParams(campaign, campaignCriteria, page,dbq);
			}
			if(ivParams == null){
				logger.info("Criteria not available in InsideView for CamapaignId : " + dbq.getCampaignId() + " , dbq id : "
						+ dbq.getId() + " , insideview purchased data now : " + totalPurchasedData);
				break;
			}
			PersonResponse personSearchList = getPersonsByCriteria(personSearchUrl, ivParams);
			if(campaign.isABM()){
				if (personSearchList == null || personSearchList.getContacts() == null
						|| personSearchList.getContacts().isEmpty()) {
					page++;
					continue;

				}
				if(CollectionUtils.isNotEmpty(campaignCriteria.getAbmCompanyIds())){
					if(campaignCriteria.getAbmCompanyIds().size() == page){
						break;
					}
				}else {
					logger.error("Campaign is ABM, but there is no company present. Please provice the company details for this campaign [{}] ",campaign.getId());
					break;
				}
			}else {
				if (personSearchList == null || personSearchList.getContacts() == null
						|| personSearchList.getContacts().isEmpty()) {
					logger.info("No data available to Buy from InsideView for CamapaignId : " + dbq.getCampaignId() + " , dbq id : "
							+ dbq.getId() + " , insideview purchased data now : " + totalPurchasedData + "Criteria : [{}]" , ivParams);
					break;
				}
			}

			logger.info("Total records found [{}] in insideview for campaign [{}] . Criteria : [{}]", personSearchList.getTotalResults(), campaign.getId(), ivParams);
			HashMap<String, Object> insertedCountAndContactIdListMap = new HashMap<String, Object>();
			insertedCountAndContactIdListMap = getPersonRecordList(personSearchList, campaign,
					ivRequestBuy - totalPurchasedData, campaignCriteria, directPhoneCallableCompanyMap);
			if (insertedCountAndContactIdListMap.containsKey("insertedProspects")) {
				internalDataBought = (long) insertedCountAndContactIdListMap.get("insertedProspects");
			}
			if (insertedCountAndContactIdListMap.containsKey("contactList")) {
				contactIdListMain.addAll((Collection<? extends String>) insertedCountAndContactIdListMap.get("contactList"));
			}
			totalPurchasedData += (internalDataBought + Long.valueOf(contactIdListMain.size()));
			// searchDetailsContactListMain.addAll(searchDetailsContactList);
			page++;
			try{
				if(campaign.isUseInsideviewSmartCache()){
					dbq.setDataActivate(true);
				}
				dbq.setInsideViewPurchasedCount(totalPurchasedData);
				dbq = dataBuyQueueRepository.save(dbq);
			}catch (Exception e){
				logger.error("Error while saving DBQ in insideview for campaign [{}]. Exception [{}] ", campaign.getId(), e);
			}

		}
		// List<ProspectCallLog> prospectCallLogList = new ArrayList<>();
		// for (Contacts contacts : searchDetailsContactListMain) {
		// Prospect prospectInfo = getPersonDetail(personDetailUrl, contacts.getId());
		// if (prospectInfo != null) {
		// // ProspectCall prospectCall = new ProspectCall(getUniqueProspect(),
		// // campaign.getId(), prospectInfo);
		// ProspectCall prospectCall = new ProspectCall(getUniqueProspect(),
		// campaign.getId(), prospectInfo);
		// ProspectCallLog prospectCallLogObj = new ProspectCallLog();
		// prospectCallLogObj.setProspectCall(prospectCall);
		// prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		// if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria) ||
		// isEmailSuppressed(prospectCallLogObj, campaignCriteria)) {
		// prospectCall.setCallRetryCount(144);
		// } else {
		// prospectCall.setCallRetryCount(786);
		// }
		// prospectCallLogList.add(prospectCallLogObj);
		// insertProspectCallLog(prospectCallLogObj);
		// }
		// }
		if (contactIdListMain.size() > 0) {
			String jobId = createContactDetailJob(personDetailJobUrl, contactIdListMain, campaign.getId());
			if (!jobId.equalsIgnoreCase("Error")) {
				dbq.setInsideViewJobId(jobId);
				dbq.setInsideViewJobStatus("ACCEPTED");
				logger.info("Prospects Job initiated for InsideView for total records:  [{}] ", contactIdListMain.size());
			} else {
				dbq.setInsideViewJobStatus("ERROR");
				logger.error("Prospects Job initiation error for Insideview with DatabuyQueued Id : " + dbq.getId());
			}
		} else {
			dbq.setInsideViewJobStatus("NO_JOB");
			if(!campaign.isUseInsideviewSmartCache()){
				logger.error("Prospects Job not initiated for Insideview with DatabuyQueued Id : " + dbq.getId());
			}
		}
		
		dbq.setInsideViewPurchasedCount(totalPurchasedData);
		if(campaign.isUseInsideviewSmartCache()){
			dbq.setDataActivate(true);
		}
		dbq = dataBuyQueueRepository.save(dbq);
		boolean isInsideviewDataBuyComplete = false;
		while (!isInsideviewDataBuyComplete) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (dbq.getInsideViewJobStatus() == null || dbq.getInsideViewJobStatus().equalsIgnoreCase("ERROR") || dbq.getInsideViewJobStatus().equalsIgnoreCase("NO_JOB")) {
				isInsideviewDataBuyComplete = true;
				break;
			}
			if (dbq.getInsideViewJobId() != null) {
				DataBuyQueue dbqnew = dataBuyQueueRepository.findByInsideViewJobId(dbq.getInsideViewJobId());
				if (dbqnew != null && dbqnew.getInsideViewJobStatus() != null && dbqnew.getInsideViewJobStatus().equalsIgnoreCase("FINISHED")) {
					isInsideviewDataBuyComplete = true;
				}
			} else {
				isInsideviewDataBuyComplete = true;
			}
		}
		logger.info("Insideview databuy process complete for campaign [{}] . Total records bought [{}] ", campaign.getId(), totalPurchasedData);
		// dataBuyCleanup.dataCleanupAfterBuy(dbq, campaign, campaignCriteria,
		// totalPurchasedData);
	}

	private Boolean getDetailedInfoAndInsertProspect(Contacts contacts, Campaign campaign,
			CampaignCriteria campaignCriteria) {
		Prospect prospectInfo = getPersonDetail(personDetailUrl, contacts.getId());
		boolean isDataInserted = false;
		if (prospectInfo != null) {
			// ProspectCall prospectCall = new ProspectCall(getUniqueProspect(),
			// campaign.getId(), prospectInfo);
			ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospectInfo);
			ProspectCallLog prospectCallLogObj = new ProspectCallLog();
			prospectCallLogObj.setProspectCall(prospectCall);
			prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria)
					|| isEmailSuppressed(prospectCallLogObj, campaignCriteria,campaign)) {
				prospectCall.setCallRetryCount(144);
			} else {
				prospectCall.setCallRetryCount(786);
			}
			// prospectCallLogList.add(prospectCallLogObj);
			try {
				insertProspectCallLog(prospectCallLogObj);
				isDataInserted = true;
			} catch (Exception e) {
				logger.error(e.getStackTrace().toString());
			}
		}
		return isDataInserted;
	}

	private boolean isCompanySuppressedDetail(ProspectCallLog domain, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		if (domain.getProspectCall() != null && domain.getProspectCall().getProspect() != null
				&& domain.getProspectCall().getProspect().getCustomAttributes() != null
				&& domain.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
			String pdomain = domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
			if (pdomain != null && !pdomain.isEmpty()) {
				pdomain = getSuppressedHostName(pdomain);
			}
			if (domain.getProspectCall().getProspect().getEmail() != null
					&& !domain.getProspectCall().getProspect().getEmail().isEmpty()) {
				String pEmail = domain.getProspectCall().getProspect().getEmail();
				String domEmail = pEmail.substring(pEmail.indexOf("@") + 1, pEmail.length());

				long compSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), domEmail);
				if (compSupCount > 0) {
					suppressed = true;
				}
			}

			if (pdomain != null && !pdomain.isEmpty()) {
				long domainSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), pdomain);
				if (domainSupCount > 0) {
					suppressed = true;
				}
			}
		}
		return suppressed;
	}

	private boolean isEmailSuppressed(ProspectCallLog personRecord, 
			CampaignCriteria campaignCriteria, Campaign campaign) {
		boolean suppressed = suppressionListService.isEmailSuppressed(
				personRecord.getProspectCall().getProspect().getEmail(), campaignCriteria.getCampaignId(),
				campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck());

		if (!suppressed && campaignCriteria.getRemoveIndustries() != null
				&& !campaignCriteria.getRemoveIndustries().isEmpty()
				&& personRecord.getProspectCall().getProspect().getEmail() != null
				&& !personRecord.getProspectCall().getProspect().getEmail().isEmpty()) {

			String[] indValsArr = campaignCriteria.getRemoveIndustries().split(",");
			List<String> inIndValsList = Arrays.asList(indValsArr);
			// Educatin
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".edu")) {
				if (inIndValsList.contains("1802") || inIndValsList.contains("263946") || inIndValsList.contains("132874")
						|| inIndValsList.contains("198410") || inIndValsList.contains("67338")) {
					suppressed = true;
				}
			}
			// Government
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".gov")) {
				if (inIndValsList.contains("2570")) {
					suppressed = true;
				}
			}
			// religious organization
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".org")) {
				if (inIndValsList.contains("201482") || inIndValsList.contains("4874") || inIndValsList.contains("135946")
						|| inIndValsList.contains("70410") || inIndValsList.contains("267018")) {
					suppressed = true;
				}
			}
			// municipal
			// if(inIndValsList.contains("4618") || inIndValsList.contains("135690") ||
			// inIndValsList.contains("70154")) {
			// do nothing
			// }
		}
		return suppressed;
	}

	private String getSuppressedHostName(String domain) {
		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}

	private ProspectCallLog insertProspectCallLog(ProspectCallLog prospectCallLogObj) {
		return prospectCallLogRepository.save(prospectCallLogObj);
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d = new Date();
		// System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid + Long.toString(d.getTime());
		return pcidv;
	}

	private CampaignCriteria setCampaignCriteria(CampaignCriteria cc, List<Expression> expressions) {

		Map<String, String[]> revMap = XtaasUtils.getInsideviewRevenueMap();
		Map<String, String[]> empMap = XtaasUtils.getEmployeeMap();
		List<Long> minRevList = new ArrayList<Long>();
		List<Long> maxRevList = new ArrayList<Long>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		boolean isEmployeeInCriteria = false;

		StringBuffer sb = new StringBuffer();
		if(CollectionUtils.isNotEmpty(expressions)){
			for (Expression exp : expressions) {
				String attribute = exp.getAttribute();
				String value = exp.getValue();
				String operator = exp.getOperator();
				if (value.equalsIgnoreCase("All")) {
					if (attribute.equalsIgnoreCase("INDUSTRY")) {
						String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570,1802,263946,132874,198410,67338";
						cc.setRemoveIndustries(removeIndusCodes);
					}
				} else {
					if (attribute.equalsIgnoreCase("INDUSTRY")) {
						if (operator.equalsIgnoreCase("NOT IN")) {
							cc.setRemoveIndustries(value);
							List<String> indusList = Arrays.asList(value.split(","));
							List<String> dbmIndustryList = new ArrayList<String>();
							List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Industry");
							for (DataBuyMapping dbm : dbmList) {
								if (!indusList.contains(dbm.getSearchValue())) {
									dbmIndustryList.add(dbm.getSearchValue());
								}
							}
							cc.setIndustry(String.join(",", dbmIndustryList));
						} else {
							cc.setIndustry(value);
						}
					}
					if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
						cc.setManagementLevel(value);
						List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
						cc.setSortedMgmtSearchList(mgmtSortedList);
					}
					if (attribute.equalsIgnoreCase("DEPARTMENT")) {
						cc.setDepartment(value);
					}
					if (attribute.equalsIgnoreCase("REVENUE")) {
						String[] revenueArr = value.split(",");
						cc.setSortedRevenuetSearchList(XtaasUtils.getRevenuSortedeMap(revenueArr));
						for (int i = 0; i < revenueArr.length; i++) {
							String revVal = revenueArr[i];
							String[] revArr = revMap.get(revVal);

							minRevList.add(Long.parseLong(revArr[0]));
							maxRevList.add(Long.parseLong(revArr[1]));
						}

						Collections.sort(minRevList);
						Collections.sort(maxRevList, Collections.reverseOrder());
						if (minRevList.contains(new Integer(0))) {

						} else if (minRevList.get(0) > 0) {
							cc.setMinRevenue(minRevList.get(0).toString());
						}
						if (maxRevList.contains(new Integer(0))) {

						} else if (maxRevList.get(0) > 0) {
							cc.setMaxRevenue(maxRevList.get(0).toString());
						}
					}

					if (attribute.equalsIgnoreCase("EMPLOYEE")) {
						isEmployeeInCriteria=true;
						String[] employeeArr = value.split(",");
						cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
						for (int i = 0; i < employeeArr.length; i++) {
							String empVal = employeeArr[i];
							String[] empArr = empMap.get(empVal);

							minEmpList.add(Integer.parseInt(empArr[0]));
							maxEmpList.add(Integer.parseInt(empArr[1]));
						}

						Collections.sort(minEmpList);
						Collections.sort(maxEmpList, Collections.reverseOrder());
						if (minEmpList.contains(new Integer(0))) {

						} else if (minEmpList.get(0) > 0) {
							cc.setMinEmployeeCount(minEmpList.get(0).toString());
						}
						if (maxEmpList.contains(new Integer(0))) {

						} else if (maxEmpList.get(0) > 0) {
							cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
						}
					}

					if (attribute.equalsIgnoreCase("SIC_CODE")) {
						cc.setSicCode(value);
					}
					if (attribute.equalsIgnoreCase("NAICS_CODE")) {
						cc.setNaicsCode(value);
					}
					if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
						List<String> countryList = Arrays.asList(value.split(","));
						cc.setCountryList(countryList);
						cc.setCountry(value);
					}
					if (attribute.equalsIgnoreCase("VALID_STATE")) {
						cc.setState(value);
					}
					if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
						cc.setTitleKeyWord(value);
					}
					if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
						cc.setNegativeKeyWord(value);
					}
					if (attribute.equalsIgnoreCase("STATE (US ONLY)")) {
						cc.setState(value);
					}

					if (attribute.equalsIgnoreCase("ZIP_CODE")) {
						cc.setZipcodes(value);
					}

				}

			}
		}

		
		if(!isEmployeeInCriteria) {
			String defaultEmp = "1-5,5-10,10-20,20-50,50-100,100-250,250-500,500-1000,1000-5000,5000-10000";
			String[] employeeArr = defaultEmp.split(",");
			cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
			for (int i = 0; i < employeeArr.length; i++) {
				String empVal = employeeArr[i];
				String[] empArr = empMap.get(empVal);

				minEmpList.add(Integer.parseInt(empArr[0]));
				maxEmpList.add(Integer.parseInt(empArr[1]));
			}

			Collections.sort(minEmpList);
			Collections.sort(maxEmpList, Collections.reverseOrder());
			if (minEmpList.contains(new Integer(0))) {

			} else if (minEmpList.get(0) > 0) {
				cc.setMinEmployeeCount(minEmpList.get(0).toString());
			}
			if (maxEmpList.contains(new Integer(0))) {

			} else if (maxEmpList.get(0) > 0) {
				cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
			}
		}

		return cc;
	}

	private String getInsideViewParams(/* databuyqueue.getCampaignCriteriaDTO() */String params) {
		// StringBuffer sb = new StringBuffer();
		// TODO
		// logic here
		return params;
	}

	private Long getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long y = new Double(value * million).longValue();
			return new Double(value * million).longValue();

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long x = new Double(value * billion).longValue();
			return new Double(value * billion).longValue();
		}
		return new Long(0);
	}

	private HashMap<String, Object> getPersonRecordList(PersonResponse personResponse, Campaign campaign, Long buyLimit,
			CampaignCriteria campaignCriteria, HashMap<String, Integer> directPhoneCallableCompanyMap) {
		HashMap<String, Object> finalMap = new HashMap<String, Object>();
		// List<ProspectCallLog> personRecordList = new ArrayList<ProspectCallLog>();
		List<String> contactIdList = new ArrayList<String>();
		long insertedProspects = 0L;
		if (personResponse.getTotalResults() > 0 && personResponse.getContacts() != null) {
			List<Contacts> list = personResponse.getContacts();
			for (int i = 0; i < list.size(); i++) {
				// if(personRecordList.size() < campaignCriteria.getTotalPurchaseReq()){
				Contacts personRecord = list.get(i);
				if (personRecord != null) {
					boolean isValidSICCode = false;
					try {
						// if (!isPersonIdAlreadyPurchased(personRecord, campaign.getId())) { // check
						// if the record is purchased in
						// // this job only
						// String reason = "PersonId already purchased " + personRecord.getPeopleId();
						// personRecord = null;
						// continue;
						// } else if (isPersonRecordAlreadyPurchasedPCL(personRecord, campaign.getId()))
						// { // check if person is
						// // already bought by name
						// // and company
						// // else if (isPersonRecordAlreadyPurchased(personRecord)) { //check if person
						// is
						// // already bought by name and company
						// String reason = personRecord.getFirstName() + "_" +
						// personRecord.getLastName() + ":"
						// + "First and Last name is already purchased";
						// personRecord = null;
						// continue;
						// } else {

						/*
						 * long c=1;
						 * recordsBoughtPerOrg.put(personRecord.getCurrentEmployment().getCompany().
						 * getCompanyID(), c);
						 */

						// boolean isDataInserted = getDetailedInfoAndInsertProspect(personRecord,
						// campaign, campaignCriteria);
						// if (isDataInserted) {
						// insertedProspects++;
						// }
						// if (insertedProspects >= buyLimit.intValue()) {
						// break;
						// }
						
						if (isCompanyLimitReached(personRecord, directPhoneCallableCompanyMap)) {
              continue;
            }
						if(!isValidProspect(campaignCriteria,campaign,personRecord)){
							continue;
						}

						// Check if data already present in insideviewdatasource collection
						/*InsideviewDataSource insideviewDataSource = insideviewDataSourceRepository
								.findByPeopleId(personRecord.getPeopleId());*/
						InsideviewDataSource insideviewDataSource = null;
						if (insideviewDataSource != null) {
							ProspectCallLog prospectCallLoginfo = prospectCallLogRepository
									.findByCampaignIdSourceIdAndSource(campaign.getId(), personRecord.getPeopleId(), "INSIDEVIEW");
							if (prospectCallLoginfo == null) {
								Prospect prospectInfo = null;
								try {
									prospectInfo = mapDetailedJobResultToProspectObject(insideviewDataSource.getDetailedContact());
								} catch (Exception e) {
									logger.error("Insideview=======>Error in mapping InsideviewDatasource collection data with Exception : " + e.getStackTrace());
								}
								if (prospectInfo != null && prospectInfo.getPhone() != null) {
									String phone = prospectInfo.getPhone();
									//TODO The number was trailing with extension without any separator so implemented below. Need to implement in more generic way
									if (phone.contains("+1") && phone.length() > 12) {
										String newPhone = phone.substring(0, 12);
										String ext = phone.substring(12);
										prospectInfo.setPhone(newPhone);
										prospectInfo.setExtension(ext);
									}
									ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospectInfo);
									ProspectCallLog prospectCallLogObj = new ProspectCallLog();
									if(prospectInfo.isDirectPhone()) {
										prospectCall.setDataSlice("slice1");
										prospectCallLogObj.setDataSlice("slice1");
									}else {
										prospectCall.setDataSlice("slice10");
										prospectCallLogObj.setDataSlice("slice10");
									}
									prospectCallLogObj.setProspectCall(prospectCall);
									prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
									if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria)
											|| isEmailSuppressed(prospectCallLogObj, campaignCriteria,campaign)) {
										prospectCall.setCallRetryCount(144);
									} else {
										prospectCall.setCallRetryCount(786);
									}
									// prospectCallLogList.add(prospectCallLogObj);
									try {
										insertProspectCallLog(prospectCallLogObj);
										insertedProspects++;
									} catch (Exception e) {
										logger.error(
												"InsideviewServiceImpl=========>Error inserting ProspectCallLog, Error : " + e.getStackTrace());
										continue;
									}
								}
							}
						} else {
							if(campaign.isUseInsideviewSmartCache()){
								try{
									ProspectCallLog prospectCallLoginfo = prospectCallLogRepository
											.findByCampaignIdSourceIdAndSource(campaign.getId(), personRecord.getPeopleId(), "INSIDEVIEW");
									if(prospectCallLoginfo == null){
										Prospect prospect  = mapSearchJobResultToProspectObject(personRecord,campaignCriteria,campaign);
									boolean isProspectInserted=	saveSearchCriteriaInPropectCallLog(prospect, campaign,campaignCriteria);
									if(isProspectInserted){
										insertedProspects++;
									}
									}

								}catch (Exception e){
									logger.error("Error while inserting search data in insideview for campaign [{}] .Exception [{}] Skip the record ", campaign.getId(), e.getMessage());
									continue;
								}
							}else {
								contactIdList.add(personRecord.getId());
							}
						}
						if ((insertedProspects + contactIdList.size()) >= buyLimit.intValue()) {
							break;
						}
						// }
					} catch (Exception e) {
						logger.error("Error while saving search record in insideview for campaign [{}] . Exception  [{}] ", campaign.getId(), e);
						e.printStackTrace();
					}
				}
			}
		}
		finalMap.put("insertedProspects", insertedProspects);
		finalMap.put("contactList", contactIdList);
		return finalMap;
	}
	
	private boolean isCompanyLimitReached(Contacts personRecord, HashMap<String, Integer> directPhoneCallableCompanyMap) {
    boolean suppressed = false;
    if (personRecord.getCompanyName() != null) {
      String companyName = personRecord.getCompanyName();
			Integer dCallableCount = directPhoneCallableCompanyMap.get(companyName);
			if (dCallableCount != null && dCallableCount >= maxDirectNumbers) {
				suppressed = true;
				return suppressed;
			}
			dCallableCount = dCallableCount != null ? dCallableCount : 0;
			directPhoneCallableCompanyMap.put(companyName, dCallableCount + 1);
    }
    return suppressed;
  }

	private boolean isPersonIdAlreadyPurchased(Contacts personRecord, String campaignId) {
		List<ProspectCallLog> pclist = prospectCallLogRepository.getProspectByPersonId(campaignId,
				personRecord.getPeopleId());
		if (pclist != null && pclist.size() > 0) {
			return false;
		}
		return true;
	}

	private boolean isPersonRecordAlreadyPurchasedPCL(Contacts personRecord, String campaignId) {
		String firstName = personRecord.getFirstName();
		String lastName = personRecord.getLastName();
		String company = personRecord.getCompanyName();
		String title = personRecord.getTitles().get(0);
		// List<ProspectCallLog> uniqueRecord =
		// prospectCallLogRepository.findUinqueRecord(campaignId,firstName, lastName,
		// title, company);
		List<String> campaignIds = new ArrayList<String>();
		Campaign campaign = campaignService.getCampaign(campaignId);

		campaignIds.add(campaignId);
		campaignIds.add(campaignId + NON_CALLABLE);
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			for (String cid : campaign.getCampaignGroupIds()) {
				campaignIds.add(cid);
			}
		} else {
			campaignIds.add(campaignId);
		}
		// TODO remove after buying ringcentral mm
		// campaignIds.add("5cedb5fee4b0023f192dd97a");
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName, title,
				company);
		// if (uniqueRecord.size() == 0) {
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean isCompanySuppressed(Contacts personRecord, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		/*
		 * String pdomain = personRecord.; if(pdomain!=null && !pdomain.isEmpty() ){
		 * pdomain = getSuppressedHostName(pdomain); } if(pdomain!=null &&
		 * !pdomain.isEmpty()) { List<SuppressionListNew> domainSup =
		 * suppressionListNewRepository.findSupressionListByCampaignDomain(
		 * campaignCriteria.getCampaignId(), pdomain); if(domainSup!=null &&
		 * domainSup.size()>0) { suppressed = true; } if(!suppressed &&
		 * personRecord.getCurrentEmployment().getCompany().getCompanyName()!=null &&
		 * !personRecord.getCurrentEmployment().getCompany().getCompanyName().isEmpty()
		 * ) { List<SuppressionListNew> compSup =
		 * suppressionListNewRepository.findSupressionListByCampaignDomain(
		 * campaignCriteria.getCampaignId(),
		 * personRecord.getCurrentEmployment().getCompany().getCompanyName());
		 * if(compSup!=null && compSup.size()>0) { suppressed = true; } } }
		 */
		return suppressed;
	}

	private void convertIVtoPCL(StringBuilder response) {
		/*
		 * 
		 * 
		 * =============================================================================
		 * ================================================= 1){"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJlscbecNBSzgDhTS63P6oHoyVbQg5Ib0Dn9fHD3-gprk",
		 * "companyId":"2073410","companyName":"#1 Network, Inc.","titles":["Webmaster"]
		 * ,"city":"Effingham","state":"IL","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":false,"firstName":"Barb","lastName":"Gould",
		 * "fullName":"Barb Gould","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3SpqDr6m5GrDvPW5IuhXerE0DsqIgt9Sn_8DhzoFuOxK",
		 * "confidenceScore":25.0,"phoneType":"CORP"},{"id":
		 * "mURVmNFEvZJ4OiLUE8I5u72jCRZuMDC79OS5CnzOSh2FjbTqSSNNxfV6zJTOuhm-",
		 * "companyId":"2073410","companyName":"#1 Network, Inc.","titles":["Controller"
		 * ],"city":"Effingham","state":"IL","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":false,"firstName":"Greg","lastName":"Koester",
		 * "fullName":"Greg Koester","peopleId":
		 * "Mww_J9VN2LspQxtKfMIMbOlYkNdwU3FkM_epG1YLdi1ivAQwqkoCwqLZjiSFWBR5",
		 * "confidenceScore":38.0,"phoneType":"CORP"}],"totalResults":"1851207","page":
		 * "1","resultsPerPage":"2"} 2) {"contacts":[{"id":
		 * "GJdc-td6UmgMwKlu1bvnKcDzpj6DcDq3ssrouM-vhcvXC5rTPqk6r1YQS8NuBKmR",
		 * "companyId":"735859","companyName":"Salesforce.com, Inc.","titles":
		 * ["Chair, Chief Executive Officer and Founder"],"city":"San Francisco","state"
		 * :"CA","country":"United States","active":true,"hasPhone":true,"hasEmail":
		 * false,"firstName":"Marc","lastName":"Benioff","fullName":"Marc  Benioff"
		 * ,"imageUrl":"https://pbs.twimg.com/profile_images/1213916610/marc.jpg",
		 * "peopleId":"Itf5drq3gpqU15QJEXL3TOI0gRfjoFjpD1V7aX5k74ua0GJtJ-Zw8F8trpoo24q_"
		 * ,"confidenceScore":70.0,"phoneType":"CORP"},{"id":
		 * "xS9uxDa6t7vkfOvbJWr7DegfSkofddXp_8WVf65_ztXYQSNUXKckzuiHama4R1Nq",
		 * "companyId":"50092300","companyName":"Birdeye Inc.","titles":
		 * ["Board of Director"],"emailMd5Hash":"6ce4604ac1cc4dde6154afcfd3c9d03e",
		 * "city":"Palo Alto","state":"CA","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":true,"firstName":"Marc","lastName":"Benioff",
		 * "fullName":"Marc  Benioff","imageUrl":
		 * "https://pbs.twimg.com/profile_images/1213916610/marc.jpg","peopleId":
		 * "Itf5drq3gpqU15QJEXL3TOI0gRfjoFjpD1V7aX5k74ua0GJtJ-Zw8F8trpoo24q_",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "6ce4604ac1cc4dde6154afcfd3c9d03e","validationStatus":"ValidDomain"}],
		 * "confidenceScore":45.5,"phoneType":"CORP"},{"id":
		 * "7xGxrl4wcY037sjuUlgPUN9zWZdJbuTw7I81Bt0Xz2mRmnZbvpG8Qts7Ol8HX5Pw",
		 * "companyId":"15260155","companyName":"YourPeople, Inc.","titles":
		 * ["Board Member"],"emailMd5Hash":"1d62e401d1e2cea0772c4afaf59bfe85",
		 * "city":"San Francisco","state":"CA","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":true,"firstName":"Lars","lastName":"Dalgaard",
		 * "fullName":"Lars Dalgaard","imageUrl":
		 * "https://pbs.twimg.com/profile_images/1671788146/photo.jpg","peopleId":
		 * "FLE7hSena8P2xG4eYVXIu8oFoW2C57KUTTF-P86-X6GA3FpAtfreACgFhXEXzXOJ",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "1d62e401d1e2cea0772c4afaf59bfe85","validationStatus":"ValidDomain"}],
		 * "confidenceScore":70.5,"phoneType":"CORP"},{"id":
		 * "bA3KYTYmAj0QT_hN9eGM3brIHTtC5cPmVogh3UDCGwTHUcBGyp3D-EpdDfXp3kll",
		 * "companyId":"17068857","companyName":"Gigster Inc.","titles":
		 * ["Member of the Board of Directors"],"city":"San Francisco","state":"CA",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":false,
		 * "firstName":"Lars","lastName":"Dalgaard","fullName":"Lars Dalgaard"
		 * ,"imageUrl":"https://pbs.twimg.com/profile_images/1671788146/photo.jpg",
		 * "peopleId":"FLE7hSena8P2xG4eYVXIu8oFoW2C57KUTTF-P86-X6GA3FpAtfreACgFhXEXzXOJ"
		 * ,"confidenceScore":33.0,"phoneType":"CORP"},{"id":
		 * "FLE7hSena8P2xG4eYVXIuyDF4rrnUcn0gDaRY3Os61pV1B3e-ymaxOaRUk2bg9WT",
		 * "companyId":"16973826","companyName":"Samsara Networks Inc.","titles":
		 * ["Founder, CEO, President & Director"],"emailMd5Hash":
		 * "53267baeed87e852ac51d5e5c20e20b3","city":"San Francisco","state":"CA",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"Sanjit","lastName":"Biswas","fullName":"Sanjit Biswas"
		 * ,"peopleId":
		 * "Th_rjWpNjNOxnG3XtsVLsqEC_m6pYz1bo1Fj5YWdaBua8Cptgrx82EjbCIOupE1U",
		 * "emailValidationStatus":"ValidEmail","emails":[{"emailMd5Hash":
		 * "53267baeed87e852ac51d5e5c20e20b3","validationStatus":"ValidEmail"}],
		 * "confidenceScore":78.0,"phoneType":"DIRECT"},{"id":
		 * "lj23wCdcMN-Yr28zZdnSXemvv49eCbDuwBYp0_ACpda6a9KqWhfCUPEWON5Poc0r",
		 * "companyId":"3771087","companyName":"ThousandEyes, Inc.","titles":
		 * ["Member of the Board"],"emailMd5Hash":"ff3144c4c30322fff9d48ae456ffdd54",
		 * "city":"San Francisco","state":"CA","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":true,"firstName":"Sanjit","lastName":"Biswas",
		 * "fullName":"Sanjit Biswas","peopleId":
		 * "Th_rjWpNjNOxnG3XtsVLsqEC_m6pYz1bo1Fj5YWdaBua8Cptgrx82EjbCIOupE1U",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "ff3144c4c30322fff9d48ae456ffdd54","validationStatus":"ValidDomain"}],
		 * "confidenceScore":37.5,"phoneType":"CORP"},{"id":
		 * "Nmv149lLKg4bvr-QBuuewxTUZUx-1P7HvlhBiTpDPrnn_cRPYs5zb9dF9dYciszJ",
		 * "companyId":"50164938","companyName":"SecureWorks, Inc.","titles":["Director"
		 * ],"emailMd5Hash":"c6fc8da41636a128866669ffb78df6c1","city":"Atlanta","state":
		 * "GA","country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"Michael","lastName":"Dell","fullName":"Michael Dell","imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":"cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa"
		 * ,"emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "c6fc8da41636a128866669ffb78df6c1","validationStatus":"ValidDomain"}],
		 * "confidenceScore":77.5,"phoneType":"CORP"},{"id":
		 * "8X6X9GdyvOIIjC4QCdh65EQP2XOcAfObmrHr6xOjeMpi99cDwQEhBVnT-lLanjFz",
		 * "companyId":"738862","companyName":"VMware, Inc.","titles":
		 * ["Chairman of the Board"],"emailMd5Hash":"1a97b66f85c2dad69939b76960436d41",
		 * "city":"Palo Alto","state":"CA","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":true,"firstName":"Michael","lastName":"Dell",
		 * "fullName":"Michael Dell","imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":"cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa"
		 * ,"emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "1a97b66f85c2dad69939b76960436d41","validationStatus":"ValidDomain"}],
		 * "confidenceScore":70.5,"phoneType":"CORP"},{"id":
		 * "8X6X9GdyvOIIjC4QCdh65AEueOszfLWOQWkuP9W2e_aL8h6KAU5rjzHGEBR3YNmY",
		 * "companyId":"3406023","companyName":"Compellent Technologies, Inc.","titles":
		 * ["Executive Officer"],"emailMd5Hash":"2b19e917571dc3678d183d38523638cd",
		 * "city":"Eden Prairie","state":"MN","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":true,"firstName":"Michael","lastName":"Dell",
		 * "fullName":"Michael Dell","imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":"cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa"
		 * ,"emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "2b19e917571dc3678d183d38523638cd","validationStatus":"ValidDomain"}],
		 * "confidenceScore":77.5,"phoneType":"CORP"},{"id":
		 * "qJIDv-E0hrBPl9jbEu-hQHN2gCG58MuMH1z8KSbZYX0GSHaaXtDFAGzF8cVs2-di",
		 * "companyId":"56592","companyName":"ON24, Inc.","titles":
		 * ["Vice President, Strategy and Operations"],"emailMd5Hash":
		 * "5962b471bd3464eae41be385a48033ad","city":"San Francisco","state":"CA",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"Alex","lastName":"Saleh","fullName":"Alex Saleh","imageUrl":
		 * "https://pbs.twimg.com/profile_images/1674829545/Daddy_Berkeley.jpg",
		 * "peopleId":"UJ1JsmsU0Wyj3M5pOT3CMxBZl_SgvoJuynSoIuMLb3SKFoHR9NToiLfJq0UF9Mi4"
		 * ,"emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "5962b471bd3464eae41be385a48033ad","validationStatus":"ValidDomain"}],
		 * "confidenceScore":70.5,"phoneType":"DIRECT"}],"totalResults":"1851207","page"
		 * :"1","resultsPerPage":"10"} 3) {"contacts":[{"id":
		 * "mURVmNFEvZJ4OiLUE8I5u72jCRZuMDC79OS5CnzOSh2FjbTqSSNNxfV6zJTOuhm-",
		 * "companyId":"2073410","companyName":"#1 Network, Inc.","titles":["Controller"
		 * ],"city":"Effingham","state":"IL","country":"United States","active":true,
		 * "hasPhone":true,"hasEmail":false,"firstName":"Greg","lastName":"Koester",
		 * "fullName":"Greg Koester","peopleId":
		 * "Mww_J9VN2LspQxtKfMIMbOlYkNdwU3FkM_epG1YLdi1ivAQwqkoCwqLZjiSFWBR5",
		 * "confidenceScore":38.0,"phoneType":"CORP"}],"totalResults":"1851207","page":
		 * "2","resultsPerPage":"1"} 4) {"contacts":[{"id":
		 * "-jOhxznzonyOHOGI7YreKPU_xOKW97A7x3R6NlabxHnbEVT4__TKKja5mOeaUA0M",
		 * "companyId":"2073410","companyName":"#1 Network, Inc.","titles":["President"]
		 * ,"emailMd5Hash":"917df4f69ee7879c6dc574540924e323","city":"Effingham","state"
		 * :"IL","country":"United States","active":true,"hasPhone":true,"hasEmail":true
		 * ,"firstName":"Roger","lastName":"Gould","fullName":"Roger Gould","peopleId":
		 * "Nmv149lLKg4bvr-QBuuewzEOiWcoVJ6ghQhdMAsq_t5Hlw3zcOWbP8C7Oi795ki8",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "917df4f69ee7879c6dc574540924e323","validationStatus":"ValidDomain"}],
		 * "confidenceScore":25.5,"phoneType":"CORP"}],"totalResults":"1799873","page":
		 * "3","resultsPerPage":"1"} 5) {"contacts":[{"id":
		 * "mWKOl8HPyKz795KGb1p0Ar_2yvyCdwlFf3BdoxshsH-IFgVb73eY_38n80LxjI7C",
		 * "companyId":"10804726","companyName":"(m)PHASIZE, LLC","titles":
		 * ["Director, Client Solutions and Analytics"],"emailMd5Hash":
		 * "6eb48339b7b43c82f73bbcbde831a351","city":"Westport","state":"CT",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"George","lastName":"Wu","fullName":"George Wu","peopleId":
		 * "7i5-DyowVl0JMeJzM1SavGPqqynZF3Y6dJ62YDnRv7Kkjb12wGdir6IllTYAylo3",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "6eb48339b7b43c82f73bbcbde831a351","validationStatus":"ValidDomain"}],
		 * "confidenceScore":77.5,"phoneType":"CORP"}],"totalResults":"526532","page":
		 * "3","resultsPerPage":"1"} 6) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ",
		 * "companyId":"3645001","companyName":"1 EDI Source, Inc.","titles":
		 * ["IT Manager"],"city":"Solon","state":"OH","country":"United States","active"
		 * :true,"hasPhone":true,"hasEmail":false,"firstName":"Steve","lastName":"T",
		 * "fullName":"Steve T","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"3297","page":"1",
		 * "resultsPerPage":"1"} 7) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ",
		 * "companyId":"3645001","companyName":"1 EDI Source, Inc.","titles":
		 * ["IT Manager"],"city":"Solon","state":"OH","country":"United States","active"
		 * :true,"hasPhone":true,"hasEmail":false,"firstName":"Steve","lastName":"T",
		 * "fullName":"Steve T","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"3297","page":"1",
		 * "resultsPerPage":"1"} 8){"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ",
		 * "companyId":"3645001","companyName":"1 EDI Source, Inc.","titles":
		 * ["IT Manager"],"city":"Solon","state":"OH","country":"United States","active"
		 * :true,"hasPhone":true,"hasEmail":false,"firstName":"Steve","lastName":"T",
		 * "fullName":"Steve T","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"5212","page":"1",
		 * "resultsPerPage":"1"} 9){"contacts":[{"id":
		 * "7xGxrl4wcY037sjuUlgPUFnkOduZwbowMl84dwfzDAhMWHGrdO2mupqvWAuYf4xG",
		 * "companyId":"25553",
		 * "companyName":" Klein, DeNatale, Goldner, Attorneys at Law","titles":
		 * ["Management Information Systems Manager"],"emailMd5Hash":
		 * "1499cf5dee43b95273231e8f2c68d26d","city":"Bakersfield","state":"CA",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"Michael","lastName":"Ortega","fullName":"Michael Ortega"
		 * ,"peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3WzsEszB41qoGiLGg_NMitf1Uv6tQl0g_KypZaqqpxgU",
		 * "emailValidationStatus":"ValidEmail","emails":[{"emailMd5Hash":
		 * "1499cf5dee43b95273231e8f2c68d26d","validationStatus":"ValidEmail"}],
		 * "confidenceScore":25.0,"phoneType":"DIRECT"}],"totalResults":"38264","page":
		 * "1","resultsPerPage":"1"} 10) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ",
		 * "companyId":"3645001","companyName":"1 EDI Source, Inc.","titles":
		 * ["IT Manager"],"city":"Solon","state":"OH","country":"United States","active"
		 * :true,"hasPhone":true,"hasEmail":false,"firstName":"Steve","lastName":"T",
		 * "fullName":"Steve T","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"5878","page":"1",
		 * "resultsPerPage":"1"} 11){"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ",
		 * "companyId":"3645001","companyName":"1 EDI Source, Inc.","titles":
		 * ["IT Manager"],"city":"Solon","state":"OH","country":"United States","active"
		 * :true,"hasPhone":true,"hasEmail":false,"firstName":"Steve","lastName":"T",
		 * "fullName":"Steve T","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"51061","page":"1"
		 * ,"resultsPerPage":"1"} 12){"contacts":[{"id":
		 * "7xGxrl4wcY037sjuUlgPUIZsoYJhdojyvZA5HHOJDxzZJaazorZuuGSKr_Si9Lji",
		 * "companyId":"647219","companyName":"ABC Supply Co., Inc.","titles":
		 * ["Branch Manager"],"city":"Beloit","state":"WI","country":"United States"
		 * ,"active":true,"hasPhone":true,"hasEmail":false,"firstName":"Mike","lastName"
		 * :"Roadt","fullName":"Mike Roadt","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3X7yTdYs64gflYjt8y9_brpqBH6hIwF_zoifo8yq-d9o",
		 * "confidenceScore":15.0,"phoneType":"CORP"}],"totalResults":"96433","page":"1"
		 * ,"resultsPerPage":"1"} 13) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJuTa58M_foRLQA0ZXZ8kzLIuELJkPxH4CVdowsQwfLsK",
		 * "companyId":"1952105","companyName":"Aspire Defence Holdings Limited"
		 * ,"titles":["Building Services Manager (Pan Garrison)"],"city":"Tidworth",
		 * "state":"England","country":"United Kingdom","active":true,"hasPhone":true,
		 * "hasEmail":false,"firstName":"Alistair","lastName":"Egan",
		 * "fullName":"Alistair Egan","peopleId":
		 * "rKN9R2VO5Aaz3fYjwX1EzlBYQTDzJlv-NsQjoWXCQLAJK3Qa68II2FOH7Ry6qG35",
		 * "confidenceScore":40.0,"phoneType":"CORP"}],"totalResults":"969","page":"1",
		 * "resultsPerPage":"1"} 14) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJsBRafNM0BGBNxM2TIcD04j4t7kVzzj8j7V_5dRMQNX9",
		 * "companyId":"1150230","companyName":"Centennial Media Corp.","titles":
		 * ["Art Director"],"city":"Englewood","state":"CO","country":"United States"
		 * ,"active":true,"hasPhone":true,"hasEmail":false,"firstName":"Natali",
		 * "lastName":"Suasnavas","fullName":"Natali Suasnavas","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3eemeFoV9zp2fC28_1tGXt4YcZ-RElDPzxyfR_1ivSg5",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"32","page":"1",
		 * "resultsPerPage":"1"} 15){"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJsBRafNM0BGBNxM2TIcD04j4t7kVzzj8j7V_5dRMQNX9",
		 * "companyId":"1150230","companyName":"Centennial Media Corp.","titles":
		 * ["Art Director"],"city":"Englewood","state":"CO","country":"United States"
		 * ,"active":true,"hasPhone":true,"hasEmail":false,"firstName":"Natali",
		 * "lastName":"Suasnavas","fullName":"Natali Suasnavas","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3eemeFoV9zp2fC28_1tGXt4YcZ-RElDPzxyfR_1ivSg5",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"32","page":"1",
		 * "resultsPerPage":"1"} 16) {"contacts":[{"id":
		 * "OD7VdWUCGGSre-puvpKoJsBRafNM0BGBNxM2TIcD04j4t7kVzzj8j7V_5dRMQNX9",
		 * "companyId":"1150230","companyName":"Centennial Media Corp.","titles":
		 * ["Art Director"],"city":"Englewood","state":"CO","country":"United States"
		 * ,"active":true,"hasPhone":true,"hasEmail":false,"firstName":"Natali",
		 * "lastName":"Suasnavas","fullName":"Natali Suasnavas","peopleId":
		 * "72rIihOXZC-Z8Mz3BIQo3eemeFoV9zp2fC28_1tGXt4YcZ-RElDPzxyfR_1ivSg5",
		 * "confidenceScore":45.0,"phoneType":"CORP"}],"totalResults":"32","page":"1",
		 * "resultsPerPage":"1"} 17) {"contacts":[{"id":
		 * "DxZumUrumT0MfdWdeVX-opWnAfkY3kUXGvxRt96lcPCHMqWtATBFb0dSxwTfXGnA",
		 * "companyId":"15874403","companyName":" i-wireless LLC","titles":
		 * ["Regional Manager"],"city":"Newport","state":"KY","country":"United States"
		 * ,"active":true,"hasPhone":true,"hasEmail":false,"firstName":"Ben","lastName":
		 * "Emanuel","fullName":"Ben Emanuel","peopleId":
		 * "ygSw4dMPR1rW2cRTY60eftw6rv2JhGL2mzz7K8qzapukFaAbG5Hm9KbxN1u9BAIM",
		 * "confidenceScore":48.0,"phoneType":"CORP"}],"totalResults":"5779356","page":
		 * "1","resultsPerPage":"1"} 18) {"contacts":[{"id":
		 * "rKN9R2VO5Aaz3fYjwX1EzprUjKsXWXWh7CnGICkPZnP5F7NEGNCpupLZtGgExKG-",
		 * "companyId":"35393","companyName":"123Greetings.com, Inc.","titles":
		 * ["Digital Media Sales Manager"],"emailMd5Hash":
		 * "627330b8840c3184a9c2deca7fe4ecb4","city":"New York","state":"NY",
		 * "country":"United States","active":true,"hasPhone":true,"hasEmail":true,
		 * "firstName":"Rajat","lastName":"Beri","fullName":"Rajat Beri","peopleId":
		 * "kgUd4lAW7BcVi1FiDYKtLmr6Nb_Qo2GFh0vPOrtmcnWDFCdzeaP9BO2PNnZr0agp",
		 * "emailValidationStatus":"ValidDomain","emails":[{"emailMd5Hash":
		 * "627330b8840c3184a9c2deca7fe4ecb4","validationStatus":"ValidDomain"}],
		 * "confidenceScore":67.5,"phoneType":"CORP"}],"totalResults":"30648","page":"1"
		 * ,"resultsPerPage":"1"}
		 * 
		 * 
		 * 
		 * { "contacts":[ {
		 * "id":"GJdc-td6UmgMwKlu1bvnKcDzpj6DcDq3ssrouM-vhcvXC5rTPqk6r1YQS8NuBKmR",
		 * "companyId":"735859", "companyName":"Salesforce.com, Inc.", "titles":[
		 * "Chair, Chief Executive Officer and Founder" ], "city":"San Francisco",
		 * "state":"CA", "country":"United States", "active":true, "hasPhone":true,
		 * "hasEmail":false, "firstName":"Marc", "lastName":"Benioff",
		 * "fullName":"Marc  Benioff",
		 * "imageUrl":"https://pbs.twimg.com/profile_images/1213916610/marc.jpg",
		 * "peopleId":
		 * "Itf5drq3gpqU15QJEXL3TOI0gRfjoFjpD1V7aX5k74ua0GJtJ-Zw8F8trpoo24q_",
		 * "confidenceScore":70.0, "phoneType":"CORP" }, {
		 * "id":"xS9uxDa6t7vkfOvbJWr7DegfSkofddXp_8WVf65_ztXYQSNUXKckzuiHama4R1Nq",
		 * "companyId":"50092300", "companyName":"Birdeye Inc.", "titles":[
		 * "Board of Director" ], "emailMd5Hash":"6ce4604ac1cc4dde6154afcfd3c9d03e",
		 * "city":"Palo Alto", "state":"CA", "country":"United States", "active":true,
		 * "hasPhone":true, "hasEmail":true, "firstName":"Marc", "lastName":"Benioff",
		 * "fullName":"Marc  Benioff",
		 * "imageUrl":"https://pbs.twimg.com/profile_images/1213916610/marc.jpg",
		 * "peopleId":
		 * "Itf5drq3gpqU15QJEXL3TOI0gRfjoFjpD1V7aX5k74ua0GJtJ-Zw8F8trpoo24q_",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"6ce4604ac1cc4dde6154afcfd3c9d03e",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":45.5,
		 * "phoneType":"CORP" }, {
		 * "id":"7xGxrl4wcY037sjuUlgPUN9zWZdJbuTw7I81Bt0Xz2mRmnZbvpG8Qts7Ol8HX5Pw",
		 * "companyId":"15260155", "companyName":"YourPeople, Inc.", "titles":[
		 * "Board Member" ], "emailMd5Hash":"1d62e401d1e2cea0772c4afaf59bfe85",
		 * "city":"San Francisco", "state":"CA", "country":"United States",
		 * "active":true, "hasPhone":true, "hasEmail":true, "firstName":"Lars",
		 * "lastName":"Dalgaard", "fullName":"Lars Dalgaard",
		 * "imageUrl":"https://pbs.twimg.com/profile_images/1671788146/photo.jpg",
		 * "peopleId":
		 * "FLE7hSena8P2xG4eYVXIu8oFoW2C57KUTTF-P86-X6GA3FpAtfreACgFhXEXzXOJ",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"1d62e401d1e2cea0772c4afaf59bfe85",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":70.5,
		 * "phoneType":"CORP" }, {
		 * "id":"bA3KYTYmAj0QT_hN9eGM3brIHTtC5cPmVogh3UDCGwTHUcBGyp3D-EpdDfXp3kll",
		 * "companyId":"17068857", "companyName":"Gigster Inc.", "titles":[
		 * "Member of the Board of Directors" ], "city":"San Francisco", "state":"CA",
		 * "country":"United States", "active":true, "hasPhone":true, "hasEmail":false,
		 * "firstName":"Lars", "lastName":"Dalgaard", "fullName":"Lars Dalgaard",
		 * "imageUrl":"https://pbs.twimg.com/profile_images/1671788146/photo.jpg",
		 * "peopleId":
		 * "FLE7hSena8P2xG4eYVXIu8oFoW2C57KUTTF-P86-X6GA3FpAtfreACgFhXEXzXOJ",
		 * "confidenceScore":33.0, "phoneType":"CORP" }, {
		 * "id":"FLE7hSena8P2xG4eYVXIuyDF4rrnUcn0gDaRY3Os61pV1B3e-ymaxOaRUk2bg9WT",
		 * "companyId":"16973826", "companyName":"Samsara Networks Inc.", "titles":[
		 * "Founder, CEO, President & Director" ],
		 * "emailMd5Hash":"53267baeed87e852ac51d5e5c20e20b3", "city":"San Francisco",
		 * "state":"CA", "country":"United States", "active":true, "hasPhone":true,
		 * "hasEmail":true, "firstName":"Sanjit", "lastName":"Biswas",
		 * "fullName":"Sanjit Biswas", "peopleId":
		 * "Th_rjWpNjNOxnG3XtsVLsqEC_m6pYz1bo1Fj5YWdaBua8Cptgrx82EjbCIOupE1U",
		 * "emailValidationStatus":"ValidEmail", "emails":[ {
		 * "emailMd5Hash":"53267baeed87e852ac51d5e5c20e20b3",
		 * "validationStatus":"ValidEmail" } ], "confidenceScore":78.0,
		 * "phoneType":"DIRECT" }, {
		 * "id":"lj23wCdcMN-Yr28zZdnSXemvv49eCbDuwBYp0_ACpda6a9KqWhfCUPEWON5Poc0r",
		 * "companyId":"3771087", "companyName":"ThousandEyes, Inc.", "titles":[
		 * "Member of the Board" ], "emailMd5Hash":"ff3144c4c30322fff9d48ae456ffdd54",
		 * "city":"San Francisco", "state":"CA", "country":"United States",
		 * "active":true, "hasPhone":true, "hasEmail":true, "firstName":"Sanjit",
		 * "lastName":"Biswas", "fullName":"Sanjit Biswas", "peopleId":
		 * "Th_rjWpNjNOxnG3XtsVLsqEC_m6pYz1bo1Fj5YWdaBua8Cptgrx82EjbCIOupE1U",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"ff3144c4c30322fff9d48ae456ffdd54",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":37.5,
		 * "phoneType":"CORP" }, {
		 * "id":"Nmv149lLKg4bvr-QBuuewxTUZUx-1P7HvlhBiTpDPrnn_cRPYs5zb9dF9dYciszJ",
		 * "companyId":"50164938", "companyName":"SecureWorks, Inc.", "titles":[
		 * "Director" ], "emailMd5Hash":"c6fc8da41636a128866669ffb78df6c1",
		 * "city":"Atlanta", "state":"GA", "country":"United States", "active":true,
		 * "hasPhone":true, "hasEmail":true, "firstName":"Michael", "lastName":"Dell",
		 * "fullName":"Michael Dell", "imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":
		 * "cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"c6fc8da41636a128866669ffb78df6c1",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":77.5,
		 * "phoneType":"CORP" }, {
		 * "id":"8X6X9GdyvOIIjC4QCdh65EQP2XOcAfObmrHr6xOjeMpi99cDwQEhBVnT-lLanjFz",
		 * "companyId":"738862", "companyName":"VMware, Inc.", "titles":[
		 * "Chairman of the Board" ], "emailMd5Hash":"1a97b66f85c2dad69939b76960436d41",
		 * "city":"Palo Alto", "state":"CA", "country":"United States", "active":true,
		 * "hasPhone":true, "hasEmail":true, "firstName":"Michael", "lastName":"Dell",
		 * "fullName":"Michael Dell", "imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":
		 * "cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"1a97b66f85c2dad69939b76960436d41",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":70.5,
		 * "phoneType":"CORP" }, {
		 * "id":"8X6X9GdyvOIIjC4QCdh65AEueOszfLWOQWkuP9W2e_aL8h6KAU5rjzHGEBR3YNmY",
		 * "companyId":"3406023", "companyName":"Compellent Technologies, Inc.",
		 * "titles":[ "Executive Officer" ],
		 * "emailMd5Hash":"2b19e917571dc3678d183d38523638cd", "city":"Eden Prairie",
		 * "state":"MN", "country":"United States", "active":true, "hasPhone":true,
		 * "hasEmail":true, "firstName":"Michael", "lastName":"Dell",
		 * "fullName":"Michael Dell", "imageUrl":
		 * "https://pbs.twimg.com/profile_images/817627299570798592/XxdAd5R2.jpg",
		 * "peopleId":
		 * "cMxY4eNS0Dsm06qpbwIFnaf3IlwIrKIXxpv98IXYBN7XhRrEEO0EImstBKmn2oJa",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"2b19e917571dc3678d183d38523638cd",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":77.5,
		 * "phoneType":"CORP" }, {
		 * "id":"qJIDv-E0hrBPl9jbEu-hQHN2gCG58MuMH1z8KSbZYX0GSHaaXtDFAGzF8cVs2-di",
		 * "companyId":"56592", "companyName":"ON24, Inc.", "titles":[
		 * "Vice President, Strategy and Operations" ],
		 * "emailMd5Hash":"5962b471bd3464eae41be385a48033ad", "city":"San Francisco",
		 * "state":"CA", "country":"United States", "active":true, "hasPhone":true,
		 * "hasEmail":true, "firstName":"Alex", "lastName":"Saleh",
		 * "fullName":"Alex Saleh", "imageUrl":
		 * "https://pbs.twimg.com/profile_images/1674829545/Daddy_Berkeley.jpg",
		 * "peopleId":
		 * "UJ1JsmsU0Wyj3M5pOT3CMxBZl_SgvoJuynSoIuMLb3SKFoHR9NToiLfJq0UF9Mi4",
		 * "emailValidationStatus":"ValidDomain", "emails":[ {
		 * "emailMd5Hash":"5962b471bd3464eae41be385a48033ad",
		 * "validationStatus":"ValidDomain" } ], "confidenceScore":70.5,
		 * "phoneType":"DIRECT" } ], "totalResults":"1851207", "page":"1",
		 * "resultsPerPage":"10" }
		 * 
		 * 
		 * =============================================================================
		 * ===============================================
		 * 
		 * 
		 * 
		 */
	}
	public Companies getManualCompanyDetail(String params) {
		//StringBuilder params = new StringBuilder();
		Companies companies = null;

		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getIVResponse(manualCompanyUrl, params);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), Companies.class);
				//System.out.println(companies);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return companies;
	}
	public Prospect getManualPersonsByCriteria(String url, String urlParameters) {
		Prospect prospectInfo = null;
		try {
			// StringBuilder params = new StringBuilder();
			StringBuilder response = getIVResponse(url, urlParameters);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				PersonResponse personResponse = mapper.readValue(response.toString(), PersonResponse.class);
				if (personResponse == null || personResponse.getContacts() == null
						|| personResponse.getContacts().isEmpty()) {
					prospectInfo=null;
				}else {
					prospectInfo = getManualDetail(personResponse);
				}
			}
		} catch (Exception e) {
			logger.error("Error in buildivParam string : " + urlParameters);
			logger.error(e.getMessage());
		}
		
		return prospectInfo;
	}
	
	private Prospect getManualDetail(PersonResponse personResponse) {
		Prospect prospectInfo = null;
		if (personResponse!=null && personResponse.getTotalResults() > 0 && personResponse.getContacts() != null) {
			List<Contacts> list = personResponse.getContacts();
			Contacts personRecord = list.get(0);
			if (personRecord != null) {
				String contactId =  personRecord.getContactId();
				 prospectInfo = getPersonDetail(manualPersonDetailUrl, contactId);
				 Companies cresponse = getManualCompanyDetail(personRecord.getCompanyId());
				 if (cresponse!=null) {
					 List<CompanyMaster> cmList = companyMasterRepository.findCompanyMasterListByCompanyId(personRecord.getCompanyId());
					 if(cmList ==null || cmList.size()==0) {
						 CompanyMaster cm = new CompanyMaster();
						 CompanyAddress ca = new CompanyAddress();
						 ca.setCity(cresponse.getCity());
						 ca.setCountry(cresponse.getCountry());
						 ca.setState(cresponse.getState());
						 ca.setStreet(cresponse.getStreet());
						 ca.setZip(cresponse.getZip());
						 cm.setCompanyAddress(ca);
						 cm.setCompanyDescription(cresponse.getCompanyBlogProfile());
						 cm.setCompanyDetailXmlUrl(cresponse.getCompanyFacebookProfile());
						// cm.setCompanyEmployeeCount(cresponse.getEmployees());
						 cm.setCompanyEmployeeCountRange(cresponse.getEmployees());
						 cm.setCompanyId(cresponse.getCompanyId());
						 List<String> indus = new ArrayList<String>(1);
						 indus.add(cresponse.getIndustry());
						 cm.setCompanyIndustry(indus);
						 if(cresponse.getNaics()!=null) {
							 cm.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
						 }
						 cm.setCompanyName(cresponse.getName());
						 //cm.setCompanyProductsAndServices(cresponse.get);
						// cm.setCompanyRanking(cresponse.getFortuneRanking());
						 //cm.setCompanyRevenueIn000s(cresponse.getRevenueCurrency());
						 cm.setCompanyRevenueRange(cresponse.getRevenue());
						 if(cresponse.getSic()!=null) {
							 cm.setCompanySIC(Arrays.asList(cresponse.getSic().split(",")));
						 }	
						 if(cresponse.getTickers()!=null) {
							 StringBuilder tickersb = new StringBuilder();
							 
							 for(Tickers ticker : cresponse.getTickers()) {
								tickersb.append(ticker.getTickerName());
							 }
							 cm.setCompanyTicker(tickersb.toString());
						 }
						// cm.setCompanyTopLevelIndustry(companyTopLevelIndustry);
						 cm.setCompanyType(cresponse.getCompanyType());
						 if(cresponse.getWebsites()!=null) {
							 cm.setCompanyWebsite(cresponse.getWebsites().get(0));

						 }
						 //cm.setDefunct(cresponse);
						 cm.setEnrichSource("INSIDEVIEW");
						// cm.setHashtags(hashtags);
						 //cm.setNumOfCompany(cresponse.get);
						 cm.setPhone(cresponse.getPhone());
						 //cm.setSimilarCompanies(similarCompanies);
						 cm.setZoomCompanyUrl(cresponse.getCompanyTwitterProfile());
						 companyMasterRepository.save(cm);
					 }
					 	prospectInfo.setCity(cresponse.getCity());
					 	prospectInfo.setCountry(cresponse.getCountry());
					 	prospectInfo.setStateCode(cresponse.getState());
					 	prospectInfo.setAddressLine1(cresponse.getStreet());
					 	prospectInfo.setZipCode(cresponse.getZip());
					 	/*StateCallConfig scc = getStateCodeAndTimeZone(cresponse.getState(),cresponse.getCountry());
						if (scc != null) {
							prospectInfo.setTimeZone(scc.getTimezone());
							prospectInfo.setStateCode(scc.getStateCode());
						}*/						 	
					 	prospectInfo.setIndustry(cresponse.getIndustry());	
					 	prospectInfo.setSource("INSIDEVIEW");
						prospectInfo.setDataSource("Xtaas Data Source");
						prospectInfo.setSourceType("INTERNAL");					 	
						prospectInfo.setCustomAttribute("maxRevenue", cresponse.getRevenue());
						prospectInfo.setCustomAttribute("maxEmployeeCount", cresponse.getEmployees());
						if (cresponse.getWebsites().size() > 0) {
							prospectInfo.setCustomAttribute("domain", cresponse.getWebsites().get(0));
						}
						if(cresponse.getNaics()!=null) {
							prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
						}
						if(cresponse.getSic()!=null) {
							prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getSic().split(",")));
						}
						
					}
			}
		}
		return prospectInfo;
	}
	
	private StateCallConfig getStateCodeAndTimeZone(String stateName,String countryName) {
		StateCallConfig stateCallConfigFromDB = null;

		if (stateName != null && !stateName.isEmpty()) {
			List<StateCallConfig> stateCallConfigFromDBList = stateCallConfigRepository.findByStateName(stateName);
			if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
				stateCallConfigFromDB = stateCallConfigFromDBList.get(0);	
			} else {
				stateCallConfigFromDBList = stateCallConfigRepository.findByCountryName(countryName);
				if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
					stateCallConfigFromDB = stateCallConfigFromDBList.get(0);	
				}	
			}
		}
		return stateCallConfigFromDB;
	}
	
	private Prospect checkSearchType(Prospect prospect, Prospect ivProspect,Campaign campaign) {
		if(prospect !=null && prospect.getFirstName()!=null && prospect.getLastName()!=null 
				&& ivProspect !=null && ivProspect.getFirstName()!=null &&  ivProspect.getLastName()!=null
				&& !campaign.isManaulBroadSearch()) {
			if(prospect.getFirstName().equalsIgnoreCase(ivProspect.getFirstName())  
					&& prospect.getLastName().equalsIgnoreCase(ivProspect.getLastName())  ) {
				prospect = ivProspect;
			}else {
				prospect =null;
			}
		}else {
			prospect = ivProspect;
		}
		
		return prospect;
	}
	
	@Override
	public Prospect getManualProspect(Prospect prospect) {
		try {
			Campaign campaign =campaignService.getCampaign(prospect.getCampaignContactId());

		if (prospect.getFirstName()!=null && !prospect.getFirstName().isEmpty() && prospect.getName() != null && prospect.getCompany() != null
				&& !prospect.getName().equalsIgnoreCase(" ") && !prospect.getCompany().isEmpty()) {
			// Search by Name and Company
			String fullName = prospect.getName();
			String companyName = prospect.getCompany();

			String urlParameters = "firstName="+URLEncoder.encode(prospect.getFirstName(), XtaasConstants.UTF8)+
					"&lastName="+URLEncoder.encode(prospect.getLastName(), XtaasConstants.UTF8)+
					"&companyName="+URLEncoder.encode(companyName, XtaasConstants.UTF8)+
					"&active=true&phoneType=DIRECT&page=1&resultsPerPage=1";
			Prospect pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);
			// if  no result from Name and Company, search by Name and Domain
			if (pro == null ) {
				urlParameters = "firstName="+URLEncoder.encode(prospect.getFirstName(), XtaasConstants.UTF8)+
						"&lastName="+URLEncoder.encode(prospect.getLastName(), XtaasConstants.UTF8)+
						"&companyName="+URLEncoder.encode(companyName, XtaasConstants.UTF8)+
						"&active=true&phoneType=ANY&page=1&resultsPerPage=1";
				pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);	
			}
			prospect = checkSearchType(prospect,pro,campaign);
		} else if ( prospect.getFirstName()!=null && !prospect.getFirstName().isEmpty() && prospect.getName() != null && prospect.getCustomAttributes().get("domain") != null
				&& !prospect.getName().equalsIgnoreCase(" ")
				&& !((String) prospect.getCustomAttributes().get("domain")).isEmpty()) {
			String fullName = prospect.getName();
			String domain = (String) prospect.getCustomAttributes().get("domain");

			String urlParameters ="firstName="+URLEncoder.encode(prospect.getFirstName(), XtaasConstants.UTF8)+
					"&lastName="+URLEncoder.encode(prospect.getLastName(), XtaasConstants.UTF8)+
					"&companyWebsite="+URLEncoder.encode(domain, XtaasConstants.UTF8)+"&active=true&phoneType=DIRECT&page=1&resultsPerPage=1";
			Prospect pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);	
			
			if (pro == null ) {
				urlParameters = "firstName="+URLEncoder.encode(prospect.getFirstName(), XtaasConstants.UTF8)+
						"&lastName="+URLEncoder.encode(prospect.getLastName(), XtaasConstants.UTF8)+
						"&companyWebsite="+URLEncoder.encode(domain, XtaasConstants.UTF8)+"&active=true&phoneType=ANY&page=1&resultsPerPage=1";
				pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);	
			}//else {
			prospect = checkSearchType(prospect,pro,campaign);
			//}
		} else if (prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
			
			String email= prospect.getEmail();
			String urlParameters = "email="+URLEncoder.encode(email, XtaasConstants.UTF8)+"&active=true&phoneType=DIRECT&page=1&resultsPerPage=1";
			Prospect pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);	
			
			if (pro == null ) {
				urlParameters = "email="+URLEncoder.encode(email, XtaasConstants.UTF8)+"&active=true&phoneType=ANY&page=1&resultsPerPage=1";
				pro = getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters);	
			}
			prospect = checkSearchType(prospect,pro,campaign);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return prospect;
	}

	@Override
	public void getJobResults(String jobId) {

		List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
		List<ProspectCallLog> prospectReviwedList = null;
		DataBuyQueue dbq = null;
		if (jobId != null && !jobId.isEmpty()) {
			dbq = dataBuyQueueRepository.findByInsideViewJobId(jobId);
		}
		if (dbq != null && dbq.getInsideViewJobStatus() != null &&  dbq.getInsideViewJobStatus().equalsIgnoreCase("ACCEPTED")) {
			long dataPurchased = 0L;
			dbq.setInsideViewJobStatus("DOWNLOADING");
			dataBuyQueueRepository.save(dbq);
			String campaignId = dbq.getCampaignId();
			Campaign campaign = campaignService.getCampaign(campaignId);
			FileReader reader = null;
			try {
				reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
				properties.load(reader);
			} catch (Exception e) {
				logger.error("Error while loading timezonareacodemapping.properties for campaign [{}]. Exception [{}] ", campaign.getId(),e);
				return;
			}
			if(campaign.isUseInsideviewSmartCache()){
				prospectReviwedList= prospectCallLogRepository
						.findProspectsByCampaignIdAndInsideViewBuysAndSource(campaign.getId(), "No","INSIDEVIEW");
			}
			CampaignCriteria campaignCriteria = new CampaignCriteria();
			if (campaign != null) {
				if(dbq.getCampaignCriteria() != null && dbq.getCampaignCriteria().isRestFullAPI()){
					campaignCriteria = dbq.getCampaignCriteria();
				}else {
					List<Expression> expressions = dbq.getCampaignCriteriaDTO();
					campaignCriteria.setAborted(false);
					// campaignCriteria.setIgnoreDefaultIndustries();
					// campaignCriteria.setIgnoreDefaultTitles(zoomBuySearchDTO.isIgnoreDefaultTitles());
					// campaignCriteria.setParallelBuy(zoomBuySearchDTO.isParallelBuy());
					campaignCriteria.setRequestJobDate(dbq.getJobRequestTime());
					campaignCriteria.setOrganizationId(campaign.getOrganizationId());
					campaignCriteria.setCampaignId(campaign.getId());
					campaignCriteria.setTotalPurchaseReq(dbq.getInsideViewRequestCount());
					if (campaign.getLeadsPerCompany() > 0) {
						campaignCriteria.setLeadPerCompany(campaign.getLeadsPerCompany());
					}
					if (campaign.getDuplicateCompanyDuration() > 0) {
						campaignCriteria.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration() + " Days");
					}
					if (campaign.getDuplicateLeadsDuration() > 0) {
						campaignCriteria.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration() + " Days");
					}
					campaignCriteria = setCampaignCriteria(campaignCriteria, expressions);

					if(StringUtils.isNotBlank(dbq.getSearchManagementlevel())){
						List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(dbq.getSearchManagementlevel());
						campaignCriteria.setSortedMgmtSearchList(mgmtSortedList);
					}
					if(StringUtils.isNotBlank(dbq.getSearchDepartment())){
						campaignCriteria.setDepartment(dbq.getSearchDepartment());
					}
					if(StringUtils.isNotBlank(dbq.getSearchRevenueMin()) && StringUtils.isNotBlank(dbq.getSearchRevenueMax())) {
						campaignCriteria.setMinRevenue(dbq.getSearchRevenueMin());
						campaignCriteria.setMaxRevenue(dbq.getSearchRevenueMax());
					}
					if(StringUtils.isNotBlank(dbq.getSearchIndustry())){
						campaignCriteria.setIndustry(dbq.getSearchIndustry());
					}

				}
				if(campaign.isABM()){
					List<String> listOfCompanyNames = getABMCompanyName(campaign);
					if(CollectionUtils.isNotEmpty(listOfCompanyNames)){
						campaignCriteria.setAbmCompanyIds(listOfCompanyNames);
					}
				}

				logger.debug("InsideviewServiceImpl=========>Get Job Result webhook response for JobId : " + jobId);
				StringBuilder response = new StringBuilder();
				String url = personDetailJobResult.replace("{jobId}", jobId);
				try {
					HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();
					// add request header
					httpClient.setRequestMethod("GET");
					httpClient.setRequestProperty("accessToken", insideViewToken);
					httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
					httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
					httpClient.setRequestProperty("Accept", "application/json");
					int responseCode = httpClient.getResponseCode();
					logger.debug("InsideviewServiceImpl=========>Sending 'GET' request to URL : " + url);
					logger.debug("InsideviewServiceImpl=========>Response Code : " + responseCode);
					BufferedReader csvReader = null;
					char sep = ',';
					List<List<String>> csvList = new ArrayList<List<String>>();
					String csvRecord = null;
					csvReader = new BufferedReader(new InputStreamReader(httpClient.getInputStream(), "UTF-8"));
					while ((csvRecord = csvReader.readLine()) != null) {
						csvList.add(parseCsvRecord(csvRecord, sep));
					}
					if (csvList.size() > 1) {
						List<List<String>> lines = csvList.subList(1, csvList.size());
						List<String> fieldnames = csvList.get(0);
						List<Map<String, String>> recordsList = new ArrayList<>();
						for (List<String> fieldvalues : lines) {
							Map<String, String> obj = new LinkedHashMap<>();
							for (int i = 0; i < fieldvalues.size(); i++) {
								obj.put(fieldnames.get(i), fieldvalues.get(i));
							}
							recordsList.add(obj);
						}

						for (Map<String, String> map : recordsList) {
							ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.ALL, Visibility.ANY);
							mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
							mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
							String jsonStringResult = mapper.writeValueAsString(map);
							DetailedContactJobResultInsideView detailedContact = mapper.readValue(jsonStringResult,
									DetailedContactJobResultInsideView.class);
							Prospect prospectInfo = null;
							try {
								prospectInfo = mapDetailedJobResultToProspectObject(detailedContact);	
							} catch (Exception e) {
								logger.error(new SplunkLoggingUtils("Insideview details buy", "")
										.eventDescription("Error in mapping new object from job result.")
										.campaignId(campaignId)
										.addThrowableWithStacktrace(e).build());
							}							
							if (prospectInfo != null) {
								String phone = prospectInfo.getPhone();
								//TODO The number was trailing with extension without any separator so implemented below. Need to implement in more generic way
								if (phone.contains("+1") && phone.length() > 12) {
									String newPhone = phone.substring(0, 12);
									String ext = phone.substring(12);
									prospectInfo.setPhone(newPhone);
									prospectInfo.setExtension(ext);
								}


								if(campaign.isUseInsideviewSmartCache()){
									ProspectCallLog dbProspectCallLog = getProspectCallLog(detailedContact.getPeopleId(), prospectReviwedList);
									if(dbProspectCallLog != null){
										ProspectCall prospectCall = dbProspectCallLog.getProspectCall();
										prospectCall.setProspect(prospectInfo);
										dbProspectCallLog.setProspectCall(prospectCall);
										// will not buy those record whose empsize greater then 10k
										if (prospectCall != null && prospectCall.getProspect() != null
												&& prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") != null
												&& !prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty()
												&& !campaign.isABM() && !campaign.isMaxEmployeeAllow()
												&& (Double.valueOf(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) >= 10000
												|| Double.valueOf(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) == 0)) {
											updateProspectCallLogRejectStatus(dbProspectCallLog);
											logger.error("Validation failed for campaing [{}], because emp count is equal or greater then 10000. Insideview emp count [{}] ", campaign.getId(), prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString());
											continue;
										}
										if (isCompanySuppressedDetail(dbProspectCallLog, campaignCriteria)
												|| isEmailSuppressed(dbProspectCallLog, campaignCriteria, campaign)) {
											prospectCall.setCallRetryCount(144);
											updateProspectCallLogRejectStatus(dbProspectCallLog);
											logger.error("Validation failed for campaing [{}], because of CompanySuppressed/EmailSuppressed  ", campaign.getId());
											continue;
										} else {
											prospectCall.setCallRetryCount(0);
										}
										dbProspectCallLog.setStatus(ProspectCallStatus.QUEUED);
										dbProspectCallLog.getProspectCall().setSubStatus("BUSY");
										dbProspectCallLog.setInsideViewBuy("Yes");

										// validate prospect, if it is valid or not
										if (!isValidProspectAfterDetailBuy(campaignCriteria, prospectInfo, campaign)) {
											updateProspectCallLogRejectStatus(dbProspectCallLog);
											continue;
										}
										try{
											dbProspectCallLog = insertProspectCallLog(dbProspectCallLog);
											prospectCallLogs.add(dbProspectCallLog);
										}catch (Exception e){

										}
									}
								}else {
									ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospectInfo);

									// will not buy those record whose empsize greater then 10k
									if (prospectCall != null && prospectCall.getProspect() != null
											&& prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") != null
											&& !prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty()
											&& !campaign.isABM() && !campaign.isMaxEmployeeAllow()
											&& (Double.valueOf(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) >= 10000
											|| Double.valueOf(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) == 0)) {
										continue;
									}
									ProspectCallLog prospectCallLogObj = new ProspectCallLog();
									prospectCallLogObj.setProspectCall(prospectCall);
									if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria)
											|| isEmailSuppressed(prospectCallLogObj, campaignCriteria,campaign)) {
										prospectCall.setCallRetryCount(144);
										updateProspectCallLogRejectStatus(prospectCallLogObj);
										continue;
									} else {
										prospectCall.setCallRetryCount(786);

									}
									prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
									prospectCallLogObj.getProspectCall().setSubStatus("BUSY");
									prospectCallLogObj.setInsideViewBuy("Yes");

									// validate prospect, if it is valid or not
									if (!isValidProspectAfterDetailBuyForRestAPI(campaignCriteria, prospectInfo, campaign)) {
										continue;
									}
									try {
										prospectCallLogObj = insertProspectCallLog(prospectCallLogObj);
										prospectCallLogs.add(prospectCallLogObj);
										dataPurchased++;
									}catch (Exception e){

									}
								}


								// prospectCallLogList.add(prospectCallLogObj);
								try {
									InsideviewDataSource insideviewDataSource = insideviewDataSourceRepository
											.findByPeopleId(detailedContact.getPeopleId());
									//insertProspectCallLog(prospectCallLogObj);
									if (insideviewDataSource == null) {
										InsideviewDataSource insideviewDataSourceNew = new InsideviewDataSource();
										insideviewDataSourceNew.setPeopleId(detailedContact.getPeopleId());
										insideviewDataSourceNew.setDetailedContact(detailedContact);
										insideviewDataSourceNew.setProspect(prospectInfo);
										insideviewDataSourceRepository.save(insideviewDataSourceNew);
									}

								} catch (Exception e) {
									logger.error(e.getStackTrace().toString());
								}
							}
						}
					} else {
						logger.error("InsideviewServiceImpl=========>Empty Job result file for JobId : " + jobId);
					}
					// ObjectMapper mapper = new ObjectMapper();
					// mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
					// String jsonResponse = mapper.writeValueAsString(recordsList);
					// System.out.println(jsonResponse);
				} catch (Exception e) {
					logger.error("InsideviewServiceImpl=========>Error in Fetch Job Result for JobId : " + jobId);
					e.printStackTrace();
				}
			} else {
				logger.error("InsideviewServiceImpl=========>Campaign not found for campaignId : " + campaignId);
			}
			if (dbq.getInsideViewPurchasedCount() != null) {
				dbq.setInsideViewPurchasedCount(dbq.getInsideViewPurchasedCount() + dataPurchased);
			} else {
				dbq.setInsideViewPurchasedCount(dataPurchased);
			}
			dbq.setInsideViewJobStatus("FINISHED");
			dbq = dataBuyQueueRepository.save(dbq);

			// for insideview smart buy
			if(prospectCallLogs != null && !prospectCallLogs.isEmpty() && prospectCallLogs.size() > 0){
				dataSlice.tagSliceAndQualityScore(prospectCallLogs,campaign);
				dbq.setInsideViewPurchasedCount(Long.valueOf(prospectCallLogs.size()));
				dbq= 	dataBuyQueueRepository.save(dbq);
			}
			if(dbq.isRestFullAPI()){
				dbq.setStatus(DataBuyQueue.DataBuyQueueStatus.COMPLETED.toString());
				dbq.setInsideViewPurchasedCount(dataPurchased);
				dbq = dataBuyQueueRepository.save(dbq);
			}
			//TODO remove when cleanup implemented for all data source buys
			/*if (campaign != null) {
				dataBuyCleanup.dataCleanupAfterBuy(dbq, campaign, campaignCriteria, dbq.getInsideViewPurchasedCount());
			}*/
		} else {
			if (dbq == null) {
				logger.error("Insideview========>No DatabuyQueue object found for Jobid : " + jobId);
			} else if (dbq.getInsideViewJobStatus() == null || (dbq.getInsideViewJobStatus() != null && dbq.getInsideViewJobStatus().equalsIgnoreCase("ACCEPTED"))) {
				logger.error("Insideview========>Job status not valid to process for Jobid : " + jobId);
			}
		}
	}

	private List<String> parseCsvRecord(String record, char csvSeparator) {

		// Prepare.
		boolean quoted = false;
		StringBuilder fieldBuilder = new StringBuilder();
		List<String> fields = new ArrayList<String>();

		// Process fields.
		for (int i = 0; i < record.length(); i++) {
			char c = record.charAt(i);
			fieldBuilder.append(c);

			if (c == '"') {
				quoted = !quoted; // Detect nested quotes.
			}

			if ((!quoted && c == csvSeparator) // The separator ..
					|| i + 1 == record.length()) // .. or, the end of record.
			{
				String field = fieldBuilder.toString() // Obtain the field, ..
						.replaceAll(csvSeparator + "$", "") // .. trim ending separator, ..
						.replaceAll("^\"|\"$", "") // .. trim surrounding quotes, ..
						.replace("\"\"", "\""); // .. and un-escape quotes.
				fields.add(field.trim()); // Add field to List.
				fieldBuilder = new StringBuilder(); // Reset.
			}
		}
		return fields;
	}
	
	private List<ProspectCallLog> getCallableList(Campaign campaign) {
    // Fetching the callables below

    List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
    List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList0);
    List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList1);
    List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList2);
    List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList3);
    List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList4);
    List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList5);
    List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList6);
    List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList7);
    List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList8);
    List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList9);
    List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList10);

    return prospectCallLogList;
  }

	private ProspectCall preparedProspectCall(Prospect prospect, Campaign campaign){
		ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospect);
		return prospectCall;
	}

	private boolean saveSearchCriteriaInPropectCallLog(Prospect prospect, Campaign campaign, CampaignCriteria campaignCriteria){
		boolean val =  true;
		try{
			ProspectCall prospectCall  = preparedProspectCall(prospect,campaign);
			prospectCall.setSubStatus("BUSY");

			ProspectCallLog prospectCallLogObj = new ProspectCallLog();
			prospectCallLogObj.setProspectCall(prospectCall);
			prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			prospectCallLogObj.setInsideViewBuy("No");
			if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria)
					|| isEmailSuppressed(prospectCallLogObj, campaignCriteria,campaign)) {
				prospectCall.setCallRetryCount(144);
				val = false;

			} else {
				prospectCall.setCallRetryCount(786);
				insertProspectCallLog(prospectCallLogObj);
				val = true;
			}
		}catch (Exception e){
			e.printStackTrace();
			val = false;
		}

		return val;
	}

	public Prospect mapSearchJobResultToProspectObject(Contacts personRecord, CampaignCriteria campaignCriteria,Campaign campaign) {
		Prospect prospectInfo = new Prospect();
		prospectInfo.setSource("INSIDEVIEW");
		prospectInfo.setSourceType("INTERNAL");
		prospectInfo.setDataSource("Xtaas Data Source");
		prospectInfo.setSourceId(personRecord.getPeopleId());
		prospectInfo.setInsideViewPurchaseDetailsId(personRecord.getId());
		prospectInfo.setFirstName(personRecord.getFirstName());
		prospectInfo.setLastName(personRecord.getLastName());
		prospectInfo.setSourceCompanyId(personRecord.getCompanyId());
		prospectInfo.setCompany(personRecord.getCompanyName());
		prospectInfo.setCountry(personRecord.getCountry());
		prospectInfo.setStatus(personRecord.getState());
		prospectInfo.setCity(personRecord.getCity());
		prospectInfo.setConfidenceScore(personRecord.getConfidenceScore());
		if(!CollectionUtils.isEmpty(personRecord.getTitles())){
			prospectInfo.setTitle(personRecord.getTitles().get(0));
		}
		if(StringUtils.isNotBlank(campaignCriteria.getDepartment())){
			prospectInfo.setDepartment(getCriteria(campaignCriteria,"DEPARTMENT",Arrays.asList(campaignCriteria.getDepartment().split(","))));
		}
		if(StringUtils.isNotBlank(campaignCriteria.getIndustry())){
			prospectInfo.setIndustry(getCriteria(campaignCriteria,"INDUSTRY",Arrays.asList(campaignCriteria.getIndustry().split(","))));
		}
		if(StringUtils.isNotBlank(campaignCriteria.getManagementLevel())) {
			prospectInfo.setManagementLevel(getRandomItem(campaignCriteria.getManagementLevel()));
		}
		Map<String, Object> customAttributes = getRevenueAndEmployee(campaignCriteria);
		if(customAttributes != null && customAttributes.size() > 0){
			prospectInfo.setCustomAttributes(customAttributes);
		}

		return prospectInfo;
	}

	private String getCriteria(CampaignCriteria campaignCriteria,String type,List<String> valueList){
		for (Expression exp : campaignCriteria.getExpressions()) {
			String attribute = exp.getAttribute();
			if (attribute.equalsIgnoreCase(type)) {
				List<String> iLIst = new ArrayList<>();
				for (PickListItem item : exp.getPickList()) {
					if(valueList.contains(item.getValue())) {
						iLIst.add(item.getLabel());
					}
				}
				if(iLIst != null && iLIst.size() > 0) {
					Random rand = new Random();
					return iLIst.get(rand.nextInt(iLIst.size()));
				}else{
					return null;
				}
			}
		}
		return null;
	}
	private String getRandomItem(String str){
		List<String> list = Arrays.asList(str.split(","));
		Random rand = new Random();
		return list.get(rand.nextInt(list.size()));
	}

	private ProspectCallLog getProspectCallLog(String peopleId, List<ProspectCallLog> prospectCallLogList){
		for(ProspectCallLog prospectCallLog : prospectCallLogList){
			String dbPerpleId = prospectCallLog.getProspectCall().getProspect().getSourceId();
			if(peopleId.equals(dbPerpleId)){
				return prospectCallLog;
			}
		}
		return null;
	}

	private Map<String, String> getMinAndMaxRevenueCount(String revenueCount) {
		if(revenueCount!=null && !revenueCount.isEmpty()) {
			Long revCountInt = Long.parseLong(revenueCount);
			Map<String, String> revRangeMain = new HashMap<>();
			List<Map<String, Long>> revRangeList = XtaasUtils.getInsideviewRevenueListMap();
			for (Map<String, Long> revenueMap : revRangeList) {
				Long minRev = revenueMap.get("minRevenue");
				if (revCountInt >= minRev) {
					if (revenueMap.containsKey("maxRevenue")) {
						Long maxEmp = revenueMap.get("maxRevenue");
						if (revCountInt < maxEmp) {
							revRangeMain.put("minRevenue", String.valueOf(minRev));
							revRangeMain.put("maxRevenue", String.valueOf(maxEmp));
							return revRangeMain;
						}
					} else {
						revRangeMain.put("minRevenue", String.valueOf(minRev));
						revRangeMain.put("maxRevenue", "");
						return revRangeMain;
					}
				}
			}
		}
		return null;
	}


	private Map<String, Object> getRevenueAndEmployee(CampaignCriteria campaignCriteria){
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		Map<String, String[]> revMap = XtaasUtils.getInsideviewRevenueMap();
		Map<String, String[]> empMap = XtaasUtils.getEmployeeMap();
		List<Long> minRevList = new ArrayList<Long>();
		List<Long> maxRevList = new ArrayList<Long>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		for (Expression exp : campaignCriteria.getExpressions()) {
			String attribute = exp.getAttribute();
			if (attribute.equalsIgnoreCase("REVENUE")) {
				String[] revenueArr = exp.getValue().split(",");
				for (int i = 0; i < revenueArr.length; i++) {
					String revVal = revenueArr[i];
					String[] revArr = revMap.get(revVal);

					minRevList.add(Long.parseLong(revArr[0]));
					maxRevList.add(Long.parseLong(revArr[1]));
				}
				Collections.sort(minRevList);
				Collections.sort(maxRevList, Collections.reverseOrder());
				if (minRevList.get(0) >= 0) {
					customAttributes.put("minRevenue",minRevList.get(0).toString());
				}
				if (maxRevList.get(0) > 0) {
					customAttributes.put("maxRevenue",maxRevList.get(0).toString());
				}
			}

			if (attribute.equalsIgnoreCase("EMPLOYEE")) {
				String[] employeeArr = exp.getValue().split(",");
				for (int i = 0; i < employeeArr.length; i++) {
					String empVal = employeeArr[i];
					String[] empArr = empMap.get(empVal);

					minEmpList.add(Integer.parseInt(empArr[0]));
					maxEmpList.add(Integer.parseInt(empArr[1]) +1);
				}
				Collections.sort(minEmpList);
				Collections.sort(maxEmpList, Collections.reverseOrder());
				if (minEmpList.get(0) >= 0) {
					customAttributes.put("minEmployeeCount",Double.parseDouble(minEmpList.get(0).toString()));
				}
				if (maxEmpList.get(0) > 0) {
					customAttributes.put("maxEmployeeCount",Double.parseDouble(maxEmpList.get(0).toString()));
				}
			}
		}
		return customAttributes;
	}

	/*
	 * Check phone DNC in local DNC list if not present then check with third-party
	 */
	private boolean checkDNC(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		boolean isLocalDNC = internalDNCListService.checkLocalDNCList(prospect.getPhone(),
				prospect.getFirstName(), prospect.getLastName());
		String pospectCountry = !StringUtils.isEmpty(prospect.getCountry())
				? prospect.getCountry().toUpperCase()
				: "US";
		if (isLocalDNC) {
			isDNC = isLocalDNC;
		} else if (campaign.isUsPhoneDncCheck() && XtaasConstants.US_COUNTRY.contains(pospectCountry)
				&& phoneDncService.usPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		} else if (campaign.isUkPhoneDncCheck() && XtaasConstants.UK_COUNTRY.contains(pospectCountry)
				&& phoneDncService.ukPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		}
		return isDNC;
	}


	public List<String> getABMCompanyName(Campaign campaign){
		int count = Integer.parseInt(abmListServiceImpl.getABMListCountOfCampaign(campaign.getId()));
		List<String> companyIds = new ArrayList<String>();

		if(count >0) {
			int  batchsize = 0;
			if(count>0) {
				batchsize = count /1000;
				long modSize = count % 1000;
				if(modSize > 0){
					batchsize = batchsize +1;
				}
			}
			for(int i=0;i<batchsize;i++) {
				Pageable pageable =  PageRequest.of(i, 1000);
				List<AbmListNew> abmList = abmListRepositoryNew.findABMListNewByCampaignIdAndPage(campaign.getId(), pageable);
				if(abmList!=null && abmList.size()>0){
					// if(abmList.getCompanyList()!=null && abmList.getCompanyList().size()>0){
					for(AbmListNew abm : abmList){
							companyIds.add(abm.getCompanyName());
					}
				}
			}
		}
		return companyIds;
	}


	public boolean isValidProspect(CampaignCriteria cc, Campaign campaign, Contacts personRecord){
		boolean isValid = true;
		try{
			/*if(!isValidCompany(cc, campaign, personRecord)){
				logger.error("Validation failed in company for insideview while searching records. CampaignCriteria companies : [{}] and insideview company : [{}] " , cc.getAbmCompanyIds(), personRecord.getCompanyName());
				return false;
			}*/
			if(isNegativeTitle(cc, campaign, personRecord)){
				return false;
			}
		}catch (Exception e){
			logger.error("Error while validating prospect in insideview. Exception [{}]: ", e);
			return false;
		}
		return isValid;
	}

	public boolean isNegativeTitle(CampaignCriteria cc, Campaign campaign, Contacts personRecord){
		boolean isValid = false;
		List<String> negativeTitles = getNegativeTitles(cc);
		if(!CollectionUtils.isEmpty(personRecord.getTitles())){
			String lowerTitle = personRecord.getTitles().get(0).toLowerCase();
			for (String neg : negativeTitles) {
				if (lowerTitle.contains(neg)) {
					return true;
				}
			}
		}
		return isValid;
	}

	public boolean isValidCompany(CampaignCriteria cc,Prospect prospect){
		boolean isValid = false;
		if(CollectionUtils.isEmpty(cc.getAbmCompanyIds())){
			return true;
		}
		if(prospect != null && prospect.getCustomAttributes() != null && prospect.getCustomAttributes().get("domain") != null
				&& prospect.getCustomAttributes().get("domain").toString() != null){
			String domain = prospect.getCustomAttributes().get("domain").toString().toLowerCase();
			List<String> companyNames = cc.getAbmCompanyIds();
			for(String name : companyNames){
				String cName = name.toLowerCase();
				boolean isValidDomain = XtaasUtils.isValidDomain(cName);
				String newCName = null;
				if(isValidDomain){
					newCName = XtaasUtils.getHostName(cName);
					domain = XtaasUtils.getHostName(domain);
					if(StringUtils.isNotBlank(domain) && StringUtils.isNotBlank(newCName) && domain.equalsIgnoreCase(newCName)){
						return true;
					}
				}else{
					newCName = cName;
					if(StringUtils.isNotBlank(domain) && StringUtils.isNotBlank(newCName) && domain.contains(newCName)){
						return true;
					}
				}
			}
		}else{
			return isValid;
		}
		return isValid;
	}


	private List<String> getNegativeTitles(CampaignCriteria campaignCriteria) {
		List<String> negativeTitles = new ArrayList<String>();
		List<String> cNegativeTitles = new ArrayList<String>();
		List<String> retNegativeTitles = new ArrayList<String>();
		if (campaignCriteria.getNegativeKeyWord() != null) {
			negativeTitles = Arrays.asList(campaignCriteria.getNegativeKeyWord().split(","));
			for (String negtit : negativeTitles) {
				cNegativeTitles.add(negtit.toLowerCase());
			}
		}
			cNegativeTitles.add("coordinator");
			cNegativeTitles.add("interim");
			cNegativeTitles.add("program");
			cNegativeTitles.add("special");
			cNegativeTitles.add("assistant");
			cNegativeTitles.add("associate");
			cNegativeTitles.add("part time");
			cNegativeTitles.add("supervisor");
			cNegativeTitles.add("program");
			cNegativeTitles.add("lead");
			cNegativeTitles.add("head");
			cNegativeTitles.add("intern");
			cNegativeTitles.add("consulting");
			cNegativeTitles.add("consultant");
			cNegativeTitles.add("temp");
			cNegativeTitles.add("sale");


		for (String negitive : cNegativeTitles) {
			retNegativeTitles.add(negitive.toLowerCase());
		}

		return retNegativeTitles;

	}


	public boolean isValidProspectAfterDetailBuy(CampaignCriteria campaignCriteria, Prospect prospectInfo,Campaign campaign){
		boolean isValid = true;
		try{
			if(!isValidManagementLevelAfterDetailBuy(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in managment level for insideview after details buy. CampaignCriteria management list : [{}] and insideview management level : [{}] " , campaignCriteria.getSortedMgmtSearchList(), prospectInfo.getManagementLevel());
				return false;
			}
			if (!isValidDepartmentAfterDetailBuy(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in department for insideview after details buy. CampaignCriteria department list : [{}] and insideview department: [{}] " , campaignCriteria.getDepartment(), prospectInfo.getDepartment());
				return false;
			}
			if(!isValidRevenue(prospectInfo,campaignCriteria)){
				logger.error("Validation failed in revenue for insideview after details buy. CampaignCriteria max revenue  : [{}] and insideview max revenue: [{}] " , campaignCriteria.getMaxRevenue(), prospectInfo.getCustomAttributeValue("maxRevenue").toString());
				return false;
			}
			if (!isValidIndustryAfterDetailBuy(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in industry for insideview after details buy. CampaignCriteria industry list : [{}] and insideview indutry: [{}] " , campaignCriteria.getIndustry(), prospectInfo.getIndustry());
				return false;
			}
			if(!isValidCompany(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in company for insideview wafter details buy. insideview company : [{}] " , prospectInfo.getCustomAttributes().get("domain").toString());
				return false;
			}
			if (checkDNC(prospectInfo, campaign)){
				return false;
			}
		}catch (Exception e){
			logger.error("Error while print logs for insideview", e);
			return false;
		}
		return isValid;
	}

	private boolean isValidRevenue(Prospect prospect,CampaignCriteria campaignCriteria){
		boolean found = false;
		if (prospect == null)
			return false;
		String maxRev = campaignCriteria.getMaxRevenue();
		if(StringUtils.isBlank(maxRev)){
			return true;
		}

		if(StringUtils.isNotEmpty(maxRev)  && prospect.getCustomAttributeValue("maxRevenue") != null
				&& !prospect.getCustomAttributeValue("maxRevenue").toString().isEmpty() &&
				Double.valueOf(prospect.getCustomAttributeValue("maxRevenue").toString()) <= Double.valueOf(maxRev)){
			found = true;
		}
		return found;
	}

	public boolean isValidManagementLevelAfterDetailBuy(CampaignCriteria campaignCriteria, Prospect prospectInfo){
		boolean isValid = false;
		if(CollectionUtils.isNotEmpty(campaignCriteria.getSortedMgmtSearchList()) && StringUtils.isNotBlank(prospectInfo.getManagementLevel())){
			boolean containsSearchStr = campaignCriteria.getSortedMgmtSearchList().stream().anyMatch(prospectInfo.getManagementLevel() ::equalsIgnoreCase);
			if(containsSearchStr){
				isValid = true;
			}
		}
		return isValid;
	}

	public void updateProspectCallLogRejectStatus(ProspectCallLog prospectCallLog){
		try{
			prospectCallLog.getProspectCall().setCallRetryCount(1000);
			prospectCallLog.setStatus(ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			prospectCallLog.getProspectCall().setSubStatus("Failed");
			prospectCallLog.setInsideViewBuy("Failed");
			prospectCallLogRepository.save(prospectCallLog);
		}catch (Exception e){

		}

	}

	public String getInsideViewDepartment(List<String> department){
		List<String> departmentList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(department)){
			Map<String, Integer> zoomInsideViewMap = getDepartment();
			Map<Integer,String> insideViewMap = getJobFunctions();
			for(String s : department){
				if(zoomInsideViewMap.containsKey(s)){
					Integer value = zoomInsideViewMap.get(s);
					String dep = insideViewMap.get(value);
					if(StringUtils.isNotBlank(dep)){
						departmentList.add(dep);
					}
				}
			}
			if(departmentList.contains("Accounting")){
				departmentList.add("Finance");
			}
			return String.join(",",departmentList);
		}
		return null;
	}
	private Map<Integer,String> getJobFunctions(){
		Map<Integer,String> jobFunctionMap= new HashMap<>();
		jobFunctionMap.put(1,"Academics");
		jobFunctionMap.put(2,"Accounting");
		jobFunctionMap.put(3,"Administration");
		jobFunctionMap.put(4,"Business Development");
		jobFunctionMap.put(5,"Construction Engineering");
		jobFunctionMap.put(6,"Consulting");
		jobFunctionMap.put(7,"Customer Support");
		jobFunctionMap.put(8,"Human Resources");
		jobFunctionMap.put(9,"Information Technology");
		jobFunctionMap.put(10,"Investment Management");
		jobFunctionMap.put(11,"Legal");
		jobFunctionMap.put(12,"Logistics");
		jobFunctionMap.put(13,"Manufacturing");
		jobFunctionMap.put(14,"Marketing");
		jobFunctionMap.put(15,"Medical");
		jobFunctionMap.put(16,"Operations");
		jobFunctionMap.put(17,"Purchasing and Procurement");
		jobFunctionMap.put(18,"Quality Assurance");
		jobFunctionMap.put(19,"Research and Development");
		jobFunctionMap.put(20,"Safety and Security");
		jobFunctionMap.put(21,"Sales");
		jobFunctionMap.put(22,"Others");
		return jobFunctionMap;
	}

	public boolean isValidDepartmentAfterDetailBuy(CampaignCriteria campaignCriteria, Prospect prospectInfo){
		boolean isValid = false;
		/*if(StringUtils.isNotBlank(campaignCriteria.getDepartment()) && StringUtils.isNotBlank(prospectInfo.getDepartment())){
			List<String> departmentList = Arrays.asList(campaignCriteria.getDepartment().split(","));
			boolean containsSearchStr = departmentList.stream().anyMatch(prospectInfo.getDepartment() ::equalsIgnoreCase);
			if(containsSearchStr){
				isValid = true;
			}
		}*/


		if(StringUtils.isNotBlank(campaignCriteria.getDepartment()) && StringUtils.isNotBlank(prospectInfo.getDepartment())){
			List<String> departmentList = new ArrayList<>(Arrays.asList(campaignCriteria.getDepartment().split(",")));
			if(departmentList.contains("Information Technology")){
				departmentList.add("IT & IS");
				departmentList.add("IT");
				departmentList.add("IS");
			}
			String[] value_split = prospectInfo.getDepartment().split("\\|");
			if(ArrayUtils.isNotEmpty(value_split)){
				for(int i = 0 ; i <value_split.length; i++){
					boolean containsSearchStr = departmentList.stream().anyMatch(value_split[i] ::equalsIgnoreCase);
					if(containsSearchStr){
						return true;
					}
				}
			}
		}

		return isValid;
	}

	public boolean isValidIndustryAfterDetailBuy(CampaignCriteria campaignCriteria, Prospect prospectInfo){
		boolean isValid = false;
		if(StringUtils.isBlank(campaignCriteria.getIndustry())){
			return true;
		}
		if(StringUtils.isNotBlank(campaignCriteria.getIndustry()) && StringUtils.isNotBlank(prospectInfo.getIndustry())){
			List<String> industryList = Arrays.asList(campaignCriteria.getIndustry().split(","));
			boolean containsSearchStr = industryList.stream().anyMatch(prospectInfo.getIndustry() ::equalsIgnoreCase);
			if(containsSearchStr){
				isValid = true;
			}
		}
		return isValid;
	}

	public String getInsideViewIndustry(List<String> industry){
		List<String> industryList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(industry)){
			Map<String, String> insideViewMap = getInsideViewIndustryMap();

			for (Map.Entry<String, String> entry : insideViewMap.entrySet()) {
				for(String in : industry){
					if(in.equalsIgnoreCase(entry.getValue())){
						industryList.add(entry.getKey());
					}
				}
			}
			return String.join(",",industryList);
		}
		return null;
	}


	public List<String> getABMCompanyId(Campaign campaign){
		int count = Integer.parseInt(abmListServiceImpl.getABMListCountOfCampaign(campaign.getId()));
		List<String> insideviewCompanyId = new ArrayList<String>();
		List<String> companyIds = new ArrayList<String>();
		if(count >0) {
			int  batchsize = 0;
			if(count>0) {
				batchsize = count /1000;
				long modSize = count % 1000;
				if(modSize > 0){
					batchsize = batchsize +1;
				}
			}
			for(int i=0;i<batchsize;i++) {
				Pageable pageable =  PageRequest.of(i, 1000);
				List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
				if(abmList!=null && abmList.size()>0){
					// if(abmList.getCompanyList()!=null && abmList.getCompanyList().size()>0){
					for(AbmListNew abm : abmList){
						if(abm.getCompanyId() !=null &&  !abm.getCompanyId().isEmpty()
								&& !abm.getCompanyId().equalsIgnoreCase("NOT_FOUND")
								&& !abm.getCompanyId().equalsIgnoreCase("GET_COMPANY_ID")) {
							if(abm.getCompanyName().contains(".")){
								String cName = abm.getCompanyName();
								cName = cName.substring(0,cName.indexOf("."));
								companyIds.add(cName);
							}else{
								companyIds.add(abm.getCompanyName());
							}

						}
					}
				}
			}
		}
		return companyIds;
	}
	public String buildIVParamsForRestAPI(Campaign campaign, CampaignCriteria cc, int page) {
		StringBuilder sb = new StringBuilder();
		if (cc.getCountry() != null && !cc.getCountry().isEmpty()) {
			Map<String, String> dbmCountryMap = new HashMap<String, String>();
			List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Country");
			for (DataBuyMapping dbm : dbmList) {
				dbmCountryMap.put(dbm.getSearchValue(), dbm.getRootTaxonamyId());
			}
			StringBuilder sbCountry = new StringBuilder();
			List<String> countryList = Arrays.asList(cc.getCountry().split(","));
			for (String country : countryList) {
				String countryCode = dbmCountryMap.get(country);
				if (sbCountry.toString().isEmpty()) {
					sbCountry.append(countryCode);
				} else {
					sbCountry.append(",");
					sbCountry.append(countryCode);
				}
			}
			cc.setCountryList(countryList);
			sb.append("countries=");
			sb.append(sbCountry.toString());
		}
		if (cc.getState() != null && !cc.getState().isEmpty()) {
			sb.append("&state=");
			sb.append(cc.getState());
		}
		if (cc.getTitleKeyWord() != null && !cc.getTitleKeyWord().isEmpty()) {
			sb.append("&titles=");
			sb.append(cc.getTitleKeyWord());
		}
		if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()) {

			StringBuilder sbIndustry = new StringBuilder();
			Map<String,String> industryMap = getInsideViewIndustryMap();
			List<String> industryList = Arrays.asList(cc.getIndustry().split(","));
			boolean first = true;
			for (String ind : industryList) {
				if (industryMap.get(ind) != null) {
					if (first) {
						first = false;
					} else {
						sbIndustry.append(",");
					}
					sbIndustry.append(industryMap.get(ind).toString());
				}
			}

			cc.setSubIndustries(sbIndustry.toString());
			// sb.append("&industries=");
			// sb.append(cc.getIndustry());
//			if (StringUtils.isBlank(cc.getSubIndustries())) {
//				logger.error("No industries found in insideview for zoominfo industries [{}] ", cc.getIndustry());
//				return null;
//			}
		}else{
			//if no industry selected
			Map<String, List<DataBuyMapping>> dbmIndustryMap = new HashMap<String, List<DataBuyMapping>>();
			List<DataBuyMapping> dbmList = dataBuyMappingRepository.findBySourceAndInputType(insideView, "Industry");
			for (DataBuyMapping dbm : dbmList) {
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(dbm.getSearchValue());
				if (dbmSubList == null) {
					dbmSubList = new ArrayList<DataBuyMapping>();
				}
				dbmSubList.add(dbm);
				dbmIndustryMap.put(dbm.getSearchValue(), dbmSubList);
			}
			List<String> removeIndustries = removeDefaultIndustry();
			for(String ind : removeIndustries){
				if(dbmIndustryMap.containsKey(ind)){
					dbmIndustryMap.remove(ind);
				}
			}
			StringBuilder sbIndustry = new StringBuilder();
			Set<String> keysSet  = dbmIndustryMap.keySet();
			for(String key :  keysSet){
				List<DataBuyMapping> dbmSubList = dbmIndustryMap.get(key);
				if (dbmSubList != null) {
					for (DataBuyMapping sdbm : dbmSubList) {
						if (sbIndustry.toString().isEmpty()) {
							sbIndustry.append(sdbm.getChildTaxonamyId());
						} else {
							sbIndustry.append(",");
							sbIndustry.append(sdbm.getChildTaxonamyId());
						}
					}
				}
			}

			cc.setSubIndustries(sbIndustry.toString());

		}
		if (cc.getSubIndustries() != null && !cc.getSubIndustries().isEmpty()) {
			sb.append("&subIndustries=");
			sb.append(cc.getSubIndustries());
		}
		sb.append("&active=true");// If present returns only active / inactive contacts. //Defaulted to true

		if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign)) {// TODO this is being used as email campaign,once
			// we introduce email campaign change this to
			// email campaign.
			sb.append("&isEmailRequired=true");
		}
		if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()
				&& !cc.getMinEmployeeCount().equalsIgnoreCase("0")) {
			sb.append("&minEmployees=");
			sb.append(Integer.parseInt(cc.getMinEmployeeCount()));
		}
		if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()
				&& !cc.getMaxEmployeeCount().equalsIgnoreCase("0")) {
			sb.append("&maxEmployees=");
			sb.append(Integer.parseInt(cc.getMaxEmployeeCount()));
		}
		if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty() && !cc.getMinRevenue().equalsIgnoreCase("0")) {
			sb.append("&minRevenue=");
			//Long value = Long.parseLong(cc.getMinRevenue());
			sb.append(cc.getMinRevenue());
		}
		if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty() && !cc.getMaxRevenue().equalsIgnoreCase("0")) {
			sb.append("&maxRevenue=");
			//Long value =  Long.parseLong(cc.getMaxRevenue());
			sb.append(cc.getMaxRevenue());
		}
		sb.append("&primaryIndustryOnly=true");
		if (cc.getNaicsCode() != null && !cc.getNaicsCode().isEmpty()) {
			sb.append("&naics=");
			sb.append(cc.getNaicsCode());
			sb.append("&primaryNAICSOnly=true");
		}
		if (cc.getSicCode() != null && !cc.getSicCode().isEmpty()) {
			sb.append("&sic=");
			sb.append(cc.getSicCode());
			sb.append("&primarySICOnly=true");
		}

		if(cc.isRestFullAPI() && StringUtils.isNotBlank(cc.getPhoneType())){
			sb.append("&phoneType=");
			sb.append(cc.getPhoneType().toUpperCase());
			// ***Accepts DIRECT, CORP, and ANY to find the
		}
		// corresponding contact's phone numbers.
		Map<String, Integer> jobFunctionMap = getInsideViewDepartment();
		if (cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
			sb.append("&jobFunctionsV2=");
			List<String> departmentList = Arrays.asList(cc.getDepartment().split(","));
			boolean first = true;
			for (String deptStr : departmentList) {
				if (jobFunctionMap.get(deptStr) != null) {
					if (first) {
						first = false;
					} else {
						sb.append(",");
					}
					sb.append(jobFunctionMap.get(deptStr).toString());
				}
			}
		}

		Map<String, Integer> managementMap = getInsideViewManagementLevel();
		if (cc.getManagementLevel() != null && !cc.getManagementLevel().isEmpty()) {
			sb.append("&jobLevelsV2=");
			List<String> managementList = Arrays.asList(cc.getManagementLevel().split(","));
			boolean first = true;
			for (String deptStr : managementList) {
				// managementMap.get(deptStr);
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
				if (deptStr.equalsIgnoreCase("Mid Management")) {
					sb.append(managementMap.get("Executives").toString());
				} else {
					if(managementMap.get(deptStr) != null){
						sb.append(managementMap.get(deptStr).toString());
					}

				}
			}
		}


		if(CollectionUtils.isNotEmpty(cc.getAbmCompanyIds())){
			List<String> list = cc.getAbmCompanyIds();
			String companyName = getCompanyName(list.get(page)); // call company search api for getting company name

			sb.append("&companyName=");
			sb.append(companyName);
			sb.append("&resultsPerPage=100");
		}else {
			sb.append("&resultsPerPage=500");
		}

		// Default: 10, maximum value: 500
		sb.append("&page=" + page);// Default: 1
		sb.append("&sortBy=companyName");// Attributes used to sort the list of results. Valid values are: active, title,
		// popularity, companyName
		sb.append("&sortOrder=asc");

		return sb.toString();
	}

	private Map<String, Integer> getInsideViewDepartment() {
		/*
		 * Sales 1 Marketing 2 Finance 3 Human Resources 4 Engineering & Research 5
		 * Operations & Administration 6 IT 7 Other 8
		 */
		Map<String, Integer> deptMap = new HashMap<String, Integer>();

		deptMap.put("Academics", 1);
		deptMap.put("Accounting", 2);
		deptMap.put("Administration", 3);
		deptMap.put("Business Development", 4);
		deptMap.put("Construction Engineering", 5);
		deptMap.put("Consulting", 6);
		deptMap.put("Customer Support", 7);
		deptMap.put("Human Resources", 8);
		deptMap.put("Information Technology", 9);
		deptMap.put("Investment Management", 10);
		deptMap.put("Legal", 11);
		deptMap.put("Logistics", 12);
		deptMap.put("Manufacturing", 13);
		deptMap.put("Marketing", 14);
		deptMap.put("Medical", 15);
		deptMap.put("Operations", 16);
		deptMap.put("Purchasing and Procurement", 17);
		deptMap.put("Quality Assurance", 18);
		deptMap.put("Research and Development", 19);
		deptMap.put("Safety and Security", 20);
		deptMap.put("Sales", 21);
		deptMap.put("Others", 22);
		return deptMap;
	}

	private Map<String, Integer> getInsideViewManagementLevel() {
		/*
		 * C-Level 1 Senior Executive 2 VP 3 Director 4 Manager 5 Board Member 6 Other 7
		 */
		Map<String, Integer> mgmtMap = new HashMap<String, Integer>();
		mgmtMap.put("Board Member", 0);
		mgmtMap.put("C Level", 1);
		mgmtMap.put("Senior Executive", 2);
		mgmtMap.put("Vice President", 3);
		mgmtMap.put("Director", 4);
		mgmtMap.put("Manager", 5);
		mgmtMap.put("Others", 6);
		return mgmtMap;
	}

	public boolean isValidProspectAfterDetailBuyForRestAPI(CampaignCriteria campaignCriteria, Prospect prospectInfo,Campaign campaign){
		boolean isValid = true;
		try{
			if( StringUtils.isBlank(prospectInfo.getPhone())){
				logger.error("Validation failed phone is not present for insideview in campaign [{}] ", campaign.getId());
				return false;
			}
			if(!isValidManagementLevelAfterDetailBuyForRestAPI(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in managment level for insideview after details buy. CampaignCriteria management list : [{}] and insideview management level : [{}] " , campaignCriteria.getSortedMgmtSearchList(), prospectInfo.getManagementLevel());
				return false;
			}
			if(!isValidRevenueForRestAPI(prospectInfo,campaignCriteria)){
				logger.error("Validation failed in revenue for insideview after details buy. CampaignCriteria max revenue  : [{}] and insideview max revenue: [{}] " , campaignCriteria.getMaxRevenue(), prospectInfo.getCustomAttributeValue("maxRevenue").toString());
				return false;
			}
			if (checkDNC(prospectInfo, campaign)){
				return false;
			}
			if (!isValidDepartmentAfterDetailBuy(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in department for insideview after details buy. CampaignCriteria department list : [{}] and insideview department: [{}] " , campaignCriteria.getDepartment(), prospectInfo.getDepartment());
				return false;
			}
			if (!isValidIndustryAfterDetailBuy(campaignCriteria, prospectInfo)){
				logger.error("Validation failed in industry for insideview after details buy. CampaignCriteria industry list : [{}] and insideview indutry: [{}] " , campaignCriteria.getIndustry(), prospectInfo.getIndustry());
				return false;
			}
		}catch (Exception e){
			logger.error("Error while print logs for insideview", e);
			return false;
		}
		return isValid;
	}

	private boolean isValidRevenueForRestAPI(Prospect prospect,CampaignCriteria campaignCriteria){
		boolean found = false;
		if (prospect == null)
			return false;
		String maxRev = campaignCriteria.getMaxRevenue();
		if(StringUtils.isBlank(maxRev)){
			return true;
		}

		if(StringUtils.isNotEmpty(maxRev)  && prospect.getCustomAttributeValue("maxRevenue") != null
				&& !prospect.getCustomAttributeValue("maxRevenue").toString().isEmpty() &&
				Double.valueOf(prospect.getCustomAttributeValue("maxRevenue").toString()) <= Double.valueOf(maxRev+"000000")){
			found = true;
		}
		return found;
	}

	public boolean isValidManagementLevelAfterDetailBuyForRestAPI(CampaignCriteria campaignCriteria, Prospect prospectInfo){
		boolean isValid = false;
		if(StringUtils.isNotBlank(campaignCriteria.getManagementLevel())){
			List<String> mList = Arrays.asList(campaignCriteria.getManagementLevel().split(","));
			for(String s : mList){
				if(getStdMgmtLevl(s).equalsIgnoreCase(prospectInfo.getManagementLevel())){
					isValid = true;
					break;
				}
			}
		}
		return isValid;
	}

	public String getCompanyName(String name){
		String companyName = name;
		try{
			boolean isValidDomain = XtaasUtils.isValidDomain(name);
			String ivrParam = buildCompanyParam(isValidDomain, name);
			CompanyResponse response = getCompanyResponse(ivrParam);

			if(response != null){
				List<Companies>  companies =  response.getCompanies();
				if(CollectionUtils.isNotEmpty(companies)){
					if(isValidDomain){
						companyName = companies.get(0).getName();
						return companyName;
					}else {
						for(Companies cName : companies){
							if(StringUtils.isNotBlank(cName.getName()) && cName.getName().contains(name)){
								return cName.getName();
							}
						}
					}
				}
			}
		}catch (Exception e){
			logger.error("Error while getting company name for company [{}] . Exception [{}] ",name,  e);
			return companyName;
		}
		return companyName;
	}

	private CompanyResponse getCompanyResponse(String ivrParam){
		CompanyResponse companies = null;
		StringBuilder response = null;
		try{
			response = getIVResponse(companySearchUrl, ivrParam);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), CompanyResponse.class);

			}
		}catch (Exception e){
			logger.error("Error while fetching company details from insideview. Insideview response : [{}] Exception [{}] ", response,e);
		}

		return companies;
	}


	public String buildCompanyParam(boolean isValidDomain, String name){
		StringBuilder params = new StringBuilder();
		if(isValidDomain){
			params.append("website=").append(name);
		}else {
			params.append("name=").append(name);
		}
		  params.append("&page=");
		  params.append(1);
		  params.append("&resultsPerPage=");
		  params.append(45);
		  return params.toString();
	}

	public Map<String,String> getInsideViewIndustryMap(){
		Map<String,String> industryMap = new HashMap<>();
		industryMap.put("Sub-Industry","ID");
		industryMap.put("Aerospace Products and Parts","1_1");
		industryMap.put("Aerospace Research and Development","1_2");
		industryMap.put("Aircraft Manufacturing","1_3");
		industryMap.put("Military Vehicles Manufacturing","1_4");
		industryMap.put("Ordnance, Missiles, Weaponry and Related","1_5");
		industryMap.put("Space Vehicles, Satellites and Related","1_6");
		industryMap.put("Agricultural Machinery and Equipment","2_1");
		industryMap.put("Grains Farming","2_10");
		industryMap.put("Horticulture","2_11");
		industryMap.put("Irrigation and Drainage Districts","2_12");
		industryMap.put("Livestock and Husbandry","2_13");
		industryMap.put("Oilseeds Farming","2_14");
		industryMap.put("Pulses and Legume Farming","2_15");
		industryMap.put("Sericulture and Beekeeping","2_16");
		industryMap.put("Agricultural Product Distribution","2_2");
		industryMap.put("Agriculture Services","2_3");
		industryMap.put("Coffee, Tea and Cocoa Farming","2_4");
		industryMap.put("Cotton and Textile Farming","2_5");
		industryMap.put("Farming Materials and Supplies","2_6");
		industryMap.put("Fishing and Aquaculture","2_7");
		industryMap.put("Forestry and Logging","2_8");
		industryMap.put("Fruits and Vegetables Farming","2_9");
		industryMap.put("Automobile Parts Manufacturing","3_1");
		industryMap.put("Motor Vehicle Manufacturing","3_2");
		industryMap.put("Motor Vehicle Parts Suppliers","3_3");
		industryMap.put("Motor Vehicle Repair and Servicing","3_4");
		industryMap.put("Truck and Bus Parts Manufacturing","3_5");
		industryMap.put("Truck, Bus and Big Rig Manufacturing","3_6");
		industryMap.put("Automated Teller Machine Operators","4_1");
		industryMap.put("Offshore Banks","4_10");
		industryMap.put("Regional Banks","4_11");
		industryMap.put("Savings Institutions","4_12");
		industryMap.put("Short-Term Business Loans and Credit","4_13");
		industryMap.put("Trust, Fiduciary and Custody Activities","4_14");
		industryMap.put("Banking Transaction Processing","4_2");
		industryMap.put("Central Banks","4_3");
		industryMap.put("Commercial Banking","4_4");
		industryMap.put("Credit Agencies","4_5");
		industryMap.put("Credit Unions","4_6");
		industryMap.put("Islamic Banks","4_7");
		industryMap.put("Landesbanken","4_8");
		industryMap.put("Mortgage Banking","4_9");
		industryMap.put("Agricultural Chemicals","5_1");
		industryMap.put("Polymers and Films","5_10");
		industryMap.put("Sealants and Adhesives","5_11");
		industryMap.put("Specialty Chemicals","5_12");
		industryMap.put("Commodity Chemicals","5_2");
		industryMap.put("Diversified Chemicals","5_3");
		industryMap.put("Explosives and Petrochemicals","5_4");
		industryMap.put("Fine Chemicals","5_5");
		industryMap.put("Industrial Fluid Manufacturing","5_6");
		industryMap.put("Medicinal Chemicals and Botanicals","5_7");
		industryMap.put("Paints, Dyes, Varnishes and Lacquers","5_8");
		industryMap.put("Plastic and Fiber Manufacturing","5_9");
		industryMap.put("Business Associations","6_1");
		industryMap.put("Social Services Institutions","6_10");
		industryMap.put("Charities","6_2");
		industryMap.put("Environmental and Wildlife Organizations","6_3");
		industryMap.put("Foundations","6_4");
		industryMap.put("Humanitarian and Emergency Relief","6_5");
		industryMap.put("Labor Unions","6_6");
		industryMap.put("Political Organizations","6_7");
		industryMap.put("Public Policy Research and Advocacy","6_8");
		industryMap.put("Religious Organizations","6_9");
		industryMap.put("ATMs and Self-Service Kiosks","7_1");
		industryMap.put("Network Security Devices","7_10");
		industryMap.put("POS and Electronic Retail Equipment","7_11");
		industryMap.put("Printing and Imaging Equipment","7_12");
		industryMap.put("Routing and Switching Devices","7_13");
		industryMap.put("Semiconductors","7_14");
		industryMap.put("Servers and Mainframes","7_15");
		industryMap.put("Wireless Networking Equipment","7_16");
		industryMap.put("CD and Other Optical Discs Production","7_2");
		industryMap.put("Computer Monitors and Projectors","7_3");
		industryMap.put("Computer Networking Equipment","7_4");
		industryMap.put("Computer Peripherals and Accessories","7_5");
		industryMap.put("Computer Storage Devices","7_6");
		industryMap.put("Desktop and Laptop Computers","7_7");
		industryMap.put("Handheld Devices and Smartphones","7_8");
		industryMap.put("Magnetic and Optical Recording","7_9");
		industryMap.put("Accounting and Tax Software","8_1");
		industryMap.put("Business Intelligence Software","8_10");
		industryMap.put("Casino Management Software","8_11");
		industryMap.put("Catalog Management Software","8_12");
		industryMap.put("Channel Partner Management Software","8_13");
		industryMap.put("Collaborative Software","8_14");
		industryMap.put("Compliance and Governance Software","8_15");
		industryMap.put("Computer-Aided Manufacturing Software","8_16");
		industryMap.put("Construction and Architecture Software","8_17");
		industryMap.put("Content Management Software","8_18");
		industryMap.put("Customer Relationship Management","8_19");
		industryMap.put("Advertising Industry Software","8_2");
		industryMap.put("Data Warehousing","8_20");
		industryMap.put("Database Management Software","8_21");
		industryMap.put("Defense and Military Software","8_22");
		industryMap.put("Desktop Publishing Software","8_23");
		industryMap.put("Development Tools and Utilities Software","8_24");
		industryMap.put("Distribution Software","8_25");
		industryMap.put("Document Management Software","8_26");
		industryMap.put("E-commerce Software","8_27");
		industryMap.put("Education and Training Software","8_28");
		industryMap.put("Electronics Industry Software","8_29");
		industryMap.put("Agriculture Industry Software","8_3");
		industryMap.put("Engineering, Scientific and CAD Software","8_30");
		industryMap.put("Enterprise Application Integration (EAI);","8_31");
		industryMap.put("Enterprise Resource Planning Software","8_32");
		industryMap.put("Event Planning Industry Software","8_33");
		industryMap.put("File Management Software","8_34");
		industryMap.put("Financial Services Software","8_35");
		industryMap.put("Food and Beverage Industry Software","8_36");
		industryMap.put("Health Care Management Software","8_37");
		industryMap.put("Hotel Management Industry Software","8_38");
		industryMap.put("Human Resources Software","8_39");
		industryMap.put("Analytics and Reporting Software","8_4");
		industryMap.put("Insurance Industry Software","8_40");
		industryMap.put("Law Enforcement Industry Software","8_41");
		industryMap.put("Legal Industry Software","8_42");
		industryMap.put("Management Consulting Software","8_43");
		industryMap.put("Manufacturing and Industrial Software","8_44");
		industryMap.put("Marketing Automation Software","8_45");
		industryMap.put("Message, Conference and Communications","8_46");
		industryMap.put("Mobile Application Software","8_47");
		industryMap.put("Multimedia and Graphics Software","8_48");
		industryMap.put("Networking and Connectivity Software","8_49");
		industryMap.put("Application Service Providers (ASPs);","8_5");
		industryMap.put("Operating System Software","8_50");
		industryMap.put("Order Management Software","8_51");
		industryMap.put("Pharma and Biotech Software","8_52");
		industryMap.put("Procurement Software","8_53");
		industryMap.put("Project Management Software","8_54");
		industryMap.put("Purchasing Software","8_55");
		industryMap.put("Quality Assurance Software","8_56");
		industryMap.put("Real Estate Industry Software","8_57");
		industryMap.put("Restaurant Industry Software","8_58");
		industryMap.put("Retail Management Software","8_59");
		industryMap.put("Automotive Industry Software","8_6");
		industryMap.put("Sales Force Automation Software","8_60");
		industryMap.put("Sales Intelligence Software","8_61");
		industryMap.put("Security Software","8_62");
		industryMap.put("Service Industry Software","8_63");
		industryMap.put("Smart Home Software","8_64");
		industryMap.put("Storage and Systems Management Software","8_65");
		industryMap.put("Supply Chain and Logistics Software","8_66");
		industryMap.put("Telecommunication Software","8_67");
		industryMap.put("Textiles Industry Software","8_68");
		industryMap.put("Tourism Industry Software","8_69");
		industryMap.put("Banking Software","8_7");
		industryMap.put("Trading and Order Management Software","8_70");
		industryMap.put("Transportation Industry Software","8_71");
		industryMap.put("Warehousing Software","8_72");
		industryMap.put("Wireless Communication Software","8_73");
		industryMap.put("Billing and Service Provision Software","8_8");
		industryMap.put("Budgeting and Forecasting Software","8_9");
		industryMap.put("Aggregates, Concrete and Cement","9_1");
		industryMap.put("Electric Lighting and Wiring","9_10");
		industryMap.put("Electrical Contractors","9_11");
		industryMap.put("Engineering Services","9_12");
		industryMap.put("Hardware Wholesalers","9_13");
		industryMap.put("Heavy Construction","9_14");
		industryMap.put("Infrastructure Construction","9_15");
		industryMap.put("Non-Residential General Contractors","9_16");
		industryMap.put("Oil and Gas Pipeline Construction","9_17");
		industryMap.put("Plumbing and HVAC Equipment","9_18");
		industryMap.put("Plywood, Veneer and Particle Board","9_19");
		industryMap.put("Apartment and Condominium Construction","9_2");
		industryMap.put("Prefabricated Buildings","9_20");
		industryMap.put("Residential General Contractors","9_21");
		industryMap.put("Sawmills and Other Mill Operations","9_22");
		industryMap.put("Sheet Metal","9_23");
		industryMap.put("Single-Family Housing Builders","9_24");
		industryMap.put("Specialty Construction","9_25");
		industryMap.put("Specialty Trade Contractors","9_26");
		industryMap.put("Stone Products","9_27");
		industryMap.put("Water, Sewer and Power Line","9_28");
		industryMap.put("Window and Door Manufacturing","9_29");
		industryMap.put("Architecture Services","9_3");
		industryMap.put("Wood Products Manufacturing","9_30");
		industryMap.put("Asphalt and Roofing Materials","9_4");
		industryMap.put("Carpentry and Floor Work","9_5");
		industryMap.put("Ceramic, Tile, Roofing and Clay Products","9_6");
		industryMap.put("Construction Equipment Manufacturing","9_7");
		industryMap.put("Construction Equipment Sales","9_8");
		industryMap.put("Construction Materials","9_9");
		industryMap.put("Apparel","10_1");
		industryMap.put("Dietary Supplements","10_10");
		industryMap.put("Dinnerware, Cookware and Cutlery","10_11");
		industryMap.put("Electronic Gaming Products","10_12");
		industryMap.put("Fashion Accessories","10_13");
		industryMap.put("Footwear","10_14");
		industryMap.put("Games and Toys","10_15");
		industryMap.put("Garden Equipment and Mowers","10_16");
		industryMap.put("Guns and Ammunition","10_17");
		industryMap.put("Hand and Power Tools","10_18");
		industryMap.put("Home Furnishings","10_19");
		industryMap.put("Art Supplies","10_2");
		industryMap.put("Home Storage Products","10_20");
		industryMap.put("Household Furniture","10_21");
		industryMap.put("Household Products","10_22");
		industryMap.put("Jewelry and Gemstones","10_23");
		industryMap.put("Leather Products","10_24");
		industryMap.put("Linens","10_25");
		industryMap.put("Major Appliances","10_26");
		industryMap.put("Mattresses and Bed Manufacturers","10_27");
		industryMap.put("Men's Clothing","10_28");
		industryMap.put("Musical Instruments","10_29");
		industryMap.put("Baby Supplies and Accessories","10_3");
		industryMap.put("Office Furniture and Fixtures","10_30");
		industryMap.put("Outdoor Furniture and Storage Products","10_31");
		industryMap.put("Paper Products","10_32");
		industryMap.put("Perfumes, Cosmetics and Toiletries","10_33");
		industryMap.put("Personal Products","10_34");
		industryMap.put("Pet Food Products","10_35");
		industryMap.put("Pet Products","10_36");
		industryMap.put("Photographic Equipment and Supplies","10_37");
		industryMap.put("Pottery","10_38");
		industryMap.put("Security and Alarm Systems","10_39");
		industryMap.put("Bicycles and Accessories","10_4");
		industryMap.put("Soaps and Detergents","10_40");
		industryMap.put("Specialty Cleaning Products","10_41");
		industryMap.put("Sporting Goods, Outdoor Gear and Apparel","10_42");
		industryMap.put("Sports Equipment","10_43");
		industryMap.put("Stationery and Related Products","10_44");
		industryMap.put("Textile Products","10_45");
		industryMap.put("Tobacco Products and Distributors","10_46");
		industryMap.put("Watches and Clocks","10_47");
		industryMap.put("Window Coverings and Wall Coverings","10_48");
		industryMap.put("Women's Clothing","10_49");
		industryMap.put("Carpets, Rugs and Floor Coverings","10_5");
		industryMap.put("Children's Clothing","10_6");
		industryMap.put("Collectibles and Giftware","10_7");
		industryMap.put("Consumer Electronics","10_8");
		industryMap.put("Costume Makers","10_9");
		industryMap.put("Auctions and Internet Auctions","11_1");
		industryMap.put("Gym, Spa and Fitness","11_10");
		industryMap.put("Landscaping and Gardening Services","11_11");
		industryMap.put("Laundry and Dry Cleaning Services","11_12");
		industryMap.put("Motor Vehicle Parking and Garages","11_13");
		industryMap.put("Motor Vehicle Rental and Leasing","11_14");
		industryMap.put("Moving Services","11_15");
		industryMap.put("Personal Services","11_16");
		industryMap.put("Photographic Services","11_17");
		industryMap.put("Plumbing Services","11_18");
		industryMap.put("Taxi and Limousine Services","11_19");
		industryMap.put("Beauty Salons","11_2");
		industryMap.put("Travel and Tourism","11_20");
		industryMap.put("Veterinary Care","11_21");
		industryMap.put("Wedding Planners","11_22");
		industryMap.put("Weight and Health Management","11_23");
		industryMap.put("Carpenters","11_3");
		industryMap.put("Catering Services","11_4");
		industryMap.put("Child Care Services","11_5");
		industryMap.put("Consumer Electronics Repair Services","11_6");
		industryMap.put("Courier, Messenger and Delivery Services","11_7");
		industryMap.put("Death Care Products and Services","11_8");
		industryMap.put("Electricians","11_9");
		industryMap.put("Advertising Agencies","12_1");
		industryMap.put("Data and Analytics Services","12_10");
		industryMap.put("Detective and Security Services","12_11");
		industryMap.put("Direct Marketing","12_12");
		industryMap.put("Equipment Rental and Leasing","12_13");
		industryMap.put("Executive Search","12_14");
		industryMap.put("Human Resources and Staffing","12_15");
		industryMap.put("Integrated Computer Systems Design","12_16");
		industryMap.put("IT Services and Consulting","12_17");
		industryMap.put("Legal Services","12_18");
		industryMap.put("Management Consulting","12_19");
		industryMap.put("Billboards and Outdoor Advertising","12_2");
		industryMap.put("Market Research Services","12_20");
		industryMap.put("Marketing and Advertising","12_21");
		industryMap.put("Mediation and Arbitration","12_22");
		industryMap.put("Mobile Application Developers","12_23");
		industryMap.put("Online Staffing and Recruitment Services","12_24");
		industryMap.put("Outsourced Human Resources Services","12_25");
		industryMap.put("Programming and Data Processing Services","12_26");
		industryMap.put("Public Relations","12_27");
		industryMap.put("Records Management Services","12_28");
		industryMap.put("Repair and Maintenance Services","12_29");
		industryMap.put("Business Services","12_3");
		industryMap.put("Sales Promotion","12_30");
		industryMap.put("Services for the Printing Trade","12_31");
		industryMap.put("Talent and Modeling Agencies","12_32");
		industryMap.put("Testing Lab and Scientific Research","12_33");
		industryMap.put("Trade Show, Event Planning and Support","12_34");
		industryMap.put("Transcription Services","12_35");
		industryMap.put("Translation Services","12_36");
		industryMap.put("Uniform Supplies","12_37");
		industryMap.put("Call Centers","12_4");
		industryMap.put("Cleaning and Facilities Management","12_5");
		industryMap.put("Commercial Design Services","12_6");
		industryMap.put("Commercial Printing Services","12_7");
		industryMap.put("Computer Facilities","12_8");
		industryMap.put("Computer Related Services","12_9");
		industryMap.put("Electric Testing Equipment","13_1");
		industryMap.put("Vending Machines","13_10");
		industryMap.put("Electronic Coils and Transformers","13_2");
		industryMap.put("Electronic Components and Accessories","13_3");
		industryMap.put("Electronic Connectors","13_4");
		industryMap.put("Heavy Electrical Equipment","13_5");
		industryMap.put("Miscellaneous Electrical Equipment","13_6");
		industryMap.put("Motors and Generators","13_7");
		industryMap.put("Printed Circuit Boards","13_8");
		industryMap.put("Sound and Lighting Equipment","13_9");
		industryMap.put("Alternative Energy Sources","14_1");
		industryMap.put("Energy Trading and Marketing","14_10");
		industryMap.put("Environmental Services","14_11");
		industryMap.put("Hazardous Waste Management","14_12");
		industryMap.put("Hydroelectric Power Generation","14_13");
		industryMap.put("Liquefied Petroleum Gas Dealers","14_14");
		industryMap.put("Multiline Utilities","14_15");
		industryMap.put("Natural Gas Pipelines","14_16");
		industryMap.put("Natural Gas Transmission","14_17");
		industryMap.put("Natural Gas Utilities","14_18");
		industryMap.put("Nuclear Power Generation","14_19");
		industryMap.put("Coal Energy Generation","14_2");
		industryMap.put("Oil and Gas Equipment","14_20");
		industryMap.put("Oil and Gas Field Services","14_21");
		industryMap.put("Oil and Gas Production and Exploration","14_22");
		industryMap.put("Oil and Gas Refining","14_23");
		industryMap.put("Oil and Gas Transport and Storage","14_24");
		industryMap.put("Petroleum Pipelines","14_25");
		industryMap.put("Recycling Services","14_26");
		industryMap.put("Remediation and Environmental Cleanup","14_27");
		industryMap.put("Sanitary and Sewage Districts","14_28");
		industryMap.put("Sanitation Services","14_29");
		industryMap.put("Cogeneration and Small Power Producers","14_3");
		industryMap.put("Solar Power Generation","14_30");
		industryMap.put("Solid Waste and Refuse Systems","14_31");
		industryMap.put("Waste Management Districts","14_32");
		industryMap.put("Wastewater Treatment","14_33");
		industryMap.put("Water Supply and Utilities","14_34");
		industryMap.put("Wholesale Petroleum and Related Products","14_35");
		industryMap.put("Wind Power Generation","14_36");
		industryMap.put("Conservation Districts","14_4");
		industryMap.put("Crude Petroleum Production","14_5");
		industryMap.put("Electric Utilities","14_6");
		industryMap.put("Electricity Distribution","14_7");
		industryMap.put("Electricity Transmission","14_8");
		industryMap.put("Energy Equipment","14_9");
		industryMap.put("Accounting, Tax, Bookkeeping and Payroll","15_1");
		industryMap.put("Currency and Forex Brokers","15_10");
		industryMap.put("Currency, Commodity & Futures Trading","15_11");
		industryMap.put("Diversified Financial Services","15_12");
		industryMap.put("Diversified Lending","15_13");
		industryMap.put("Electronic Funds Transfer","15_14");
		industryMap.put("Electronic Payment Systems","15_15");
		industryMap.put("Endowment Funds","15_16");
		industryMap.put("Energy Exchanges","15_17");
		industryMap.put("Finance Authorities","15_18");
		industryMap.put("Financial Leasing Companies","15_19");
		industryMap.put("Asset Management","15_2");
		industryMap.put("Financial Transaction Settlement","15_20");
		industryMap.put("Forfeiting and Factoring","15_21");
		industryMap.put("Hedge Funds","15_22");
		industryMap.put("Investment Banking","15_23");
		industryMap.put("Investment Management and Fund Operators","15_24");
		industryMap.put("Investment Services and Advice","15_25");
		industryMap.put("Investment Trusts","15_26");
		industryMap.put("Leveraged Finance","15_27");
		industryMap.put("Market Makers and Trade Clearing","15_28");
		industryMap.put("Miscellaneous Investment Firms","15_29");
		industryMap.put("Blank Check Companies","15_3");
		industryMap.put("Mobile Payment Systems","15_30");
		industryMap.put("Mortgage Brokers","15_31");
		industryMap.put("Patent Owners and Lessors","15_32");
		industryMap.put("Pension and Retirement Funds","15_33");
		industryMap.put("Private Equity","15_34");
		industryMap.put("Royalty Trusts","15_35");
		industryMap.put("Securities Brokers and Traders","15_36");
		industryMap.put("Specialty Financial Services","15_37");
		industryMap.put("Stock Exchanges","15_38");
		industryMap.put("Trade Facilitation","15_39");
		industryMap.put("Consumer Credit Reporting","15_4");
		industryMap.put("Venture Capital","15_40");
		industryMap.put("Credit and Collection Services","15_5");
		industryMap.put("Credit Cards","15_6");
		industryMap.put("Credit Intermediation","15_7");
		industryMap.put("Credit Rating Agency","15_8");
		industryMap.put("Crowdfunding","15_9");
		industryMap.put("Alcoholic Beverage Distribution","16_1");
		industryMap.put("Flavorings, Spices and Other Ingredients","16_10");
		industryMap.put("Flours, Sugar and Mixes","16_11");
		industryMap.put("Food and Beverage Processing Machinery","16_12");
		industryMap.put("Food Oils","16_13");
		industryMap.put("Food Products","16_14");
		industryMap.put("Food Wholesale Distributors","16_15");
		industryMap.put("Fresh and Frozen Seafood","16_16");
		industryMap.put("Fruits and Nuts","16_17");
		industryMap.put("Ice creams and Frozen Desserts","16_18");
		industryMap.put("Infant food","16_19");
		industryMap.put("Baked Goods","16_2");
		industryMap.put("Meat Packing and Meat Products","16_20");
		industryMap.put("Non-Alcoholic Beverages","16_21");
		industryMap.put("Pastas and Cereals","16_22");
		industryMap.put("Poultry","16_23");
		industryMap.put("Processed Food Products","16_24");
		industryMap.put("Wineries","16_25");
		industryMap.put("Beverage Bottling","16_3");
		industryMap.put("Beverage Distillers","16_4");
		industryMap.put("Breweries","16_5");
		industryMap.put("Candy and Confections","16_6");
		industryMap.put("Canned and Frozen Fruits and Vegetables","16_7");
		industryMap.put("Coffee and Tea Manufacturers","16_8");
		industryMap.put("Dairy Products","16_9");
		industryMap.put("Cities, Towns and Municipalities","17_1");
		industryMap.put("Legislatures","17_10");
		industryMap.put("National Security","17_11");
		industryMap.put("Police Protection","17_12");
		industryMap.put("Regional Promotion Agencies","17_13");
		industryMap.put("Special Districts","17_14");
		industryMap.put("State, Provincial or Regional Government","17_15");
		industryMap.put("Villages and Small Municipalities","17_16");
		industryMap.put("Correctional Facilities","17_2");
		industryMap.put("County Governments","17_3");
		industryMap.put("Courts of Law","17_4");
		industryMap.put("Executive Government Offices","17_5");
		industryMap.put("Federal Government Agencies","17_6");
		industryMap.put("Fire Protection","17_7");
		industryMap.put("International Government Agencies","17_8");
		industryMap.put("Legal Counsel and Prosecution","17_9");
		industryMap.put("Industrial Conglomerates","18_1");
		industryMap.put("Ambulance Services","19_1");
		industryMap.put("Dentists","19_10");
		industryMap.put("Dermatologists","19_11");
		industryMap.put("Diagnostic Imaging Centers","19_12");
		industryMap.put("Electromedical and Therapeutic Equipment","19_13");
		industryMap.put("Emergency Medical Services","19_14");
		industryMap.put("Family Planning Clinics","19_15");
		industryMap.put("Fertility Clinics","19_16");
		industryMap.put("General Healthcare Equipment","19_17");
		industryMap.put("General Medical and Surgical Hospitals","19_18");
		industryMap.put("General Physicians and Clinics","19_19");
		industryMap.put("Anesthesiologists","19_2");
		industryMap.put("Healthcare Districts","19_20");
		industryMap.put("Home Healthcare","19_21");
		industryMap.put("Hospice Services","19_22");
		industryMap.put("Integrated Healthcare Networks","19_23");
		industryMap.put("Kidney Dialysis Centers","19_24");
		industryMap.put("Medical and Dental Laboratories","19_25");
		industryMap.put("Medical Practice Management and Services","19_26");
		industryMap.put("Mental Health Practitioners","19_27");
		industryMap.put("Neurologists","19_28");
		industryMap.put("Nursing Homes and Extended Care","19_29");
		industryMap.put("Assisted Living Facilities","19_3");
		industryMap.put("Obstetricians and Gynecologists","19_30");
		industryMap.put("Oncology Services","19_31");
		industryMap.put("Ophthalmic Equipment","19_32");
		industryMap.put("Ophthalmologists and Optometrists","19_33");
		industryMap.put("Orthopedic and Prosthetic Equipment","19_34");
		industryMap.put("Orthopedic Services","19_35");
		industryMap.put("Other Specialty Hospitals","19_36");
		industryMap.put("Pediatricians","19_37");
		industryMap.put("Physical Therapy Facilities","19_38");
		industryMap.put("Podiatrists","19_39");
		industryMap.put("Blood and Organ Banks","19_4");
		industryMap.put("Psychiatric Hospitals","19_40");
		industryMap.put("Radiology Services","19_41");
		industryMap.put("Specialty Surgical Hospitals","19_42");
		industryMap.put("Substance Abuse Rehabilitation Centers","19_43");
		industryMap.put("Surgical and Medical Devices","19_44");
		industryMap.put("Urgent Care Centers","19_45");
		industryMap.put("X-ray Equipment","19_46");
		industryMap.put("Cardiologists","19_5");
		industryMap.put("Children's Hospitals","19_6");
		industryMap.put("Chiropractors","19_7");
		industryMap.put("Cosmetic Surgery","19_8");
		industryMap.put("Dental Equipment and Supplies","19_9");
		industryMap.put("Air and Gas Compressors","20_1");
		industryMap.put("Fabricated Structural Metal","20_10");
		industryMap.put("Fabricated Wire Products","20_11");
		industryMap.put("Flat Glass","20_12");
		industryMap.put("Gaskets and Sealing Devices","20_13");
		industryMap.put("Glass Products","20_14");
		industryMap.put("Heating Equipment","20_15");
		industryMap.put("Industrial Contractors","20_16");
		industryMap.put("Industrial Equipment and Machinery","20_17");
		industryMap.put("Industrial Fans","20_18");
		industryMap.put("Industrial Furnaces and Ovens","20_19");
		industryMap.put("Ball Bearings and Roller Bearing","20_2");
		industryMap.put("Industrial Machinery Distribution","20_20");
		industryMap.put("Industrial Measurement Devices","20_21");
		industryMap.put("Injection Molding and Die Casting","20_22");
		industryMap.put("Laboratory Equipment","20_23");
		industryMap.put("Machine Tools and Metal Equipment","20_24");
		industryMap.put("Measuring Devices and Controllers","20_25");
		industryMap.put("Metal Cans","20_26");
		industryMap.put("Metal Forgings","20_27");
		industryMap.put("Metalworking Machinery","20_28");
		industryMap.put("Paper Board and Paper Products","20_29");
		industryMap.put("Bolts, Nuts, Screws, Rivets and Washers","20_3");
		industryMap.put("Paper Containers and Packaging","20_30");
		industryMap.put("Petroleum Products","20_31");
		industryMap.put("Plastic Materials and Synthetic Resins","20_32");
		industryMap.put("Plastics, Foil and Coated Paper Bags","20_33");
		industryMap.put("Power Distribution and Transformers","20_34");
		industryMap.put("Printing Press Machinery","20_35");
		industryMap.put("Pulp, Paper and Paperboard Mills","20_36");
		industryMap.put("Pumping Equipment","20_37");
		industryMap.put("Recycled and Converted Paper Products","20_38");
		industryMap.put("Relays and Industrial Controls","20_39");
		industryMap.put("Building Climate Control and HVAC","20_4");
		industryMap.put("Screw Machines","20_40");
		industryMap.put("Shipping Barrels, Drums, Kegs and Pails","20_41");
		industryMap.put("Specialty Industrial Machinery","20_42");
		industryMap.put("Steel Pipes and Tubes","20_43");
		industryMap.put("Steel Wire Drawing, Nails and Spikes","20_44");
		industryMap.put("Switching and Switchboard Equipment","20_45");
		industryMap.put("Tires and Inner Tubes","20_46");
		industryMap.put("Totalizing Fluid Meters","20_47");
		industryMap.put("Coating and Engravings","20_5");
		industryMap.put("Commercial Equipment and Supplies","20_6");
		industryMap.put("Cotton, Fiber and Silk Mills","20_7");
		industryMap.put("Engines and Turbines","20_8");
		industryMap.put("Fabricated Rubber Products","20_9");
		industryMap.put("Automobile Insurance","21_1");
		industryMap.put("Insurance Financing","21_10");
		industryMap.put("Liability Insurance","21_11");
		industryMap.put("Life Insurance","21_12");
		industryMap.put("Mortgage Insurance","21_13");
		industryMap.put("Multiline Insurance","21_14");
		industryMap.put("Property and Casualty Insurance","21_15");
		industryMap.put("Reinsurance","21_16");
		industryMap.put("Risk Management","21_17");
		industryMap.put("Specialty Insurance","21_18");
		industryMap.put("Surety Insurance","21_19");
		industryMap.put("Claims Administration and Processing","21_2");
		industryMap.put("Travel Insurance","21_20");
		industryMap.put("Workers' Compensation","21_21");
		industryMap.put("Commercial Insurance","21_3");
		industryMap.put("Credit Insurance","21_4");
		industryMap.put("Disability Insurance","21_5");
		industryMap.put("Fire and Marine Insurance","21_6");
		industryMap.put("Health Insurance","21_7");
		industryMap.put("Homeowners and Title Insurance","21_8");
		industryMap.put("Insurance Agents and Brokers","21_9");
		industryMap.put("Accommodation","22_1");
		industryMap.put("Internet Gaming Arcades","22_10");
		industryMap.put("Motion Picture Theaters","22_11");
		industryMap.put("Museums and Art Galleries","22_12");
		industryMap.put("Park and Recreation Districts","22_13");
		industryMap.put("Performing Arts","22_14");
		industryMap.put("Professional Sports Teams","22_15");
		industryMap.put("Racetracks","22_16");
		industryMap.put("Restaurants, Bars and Eateries","22_17");
		industryMap.put("Ski Resorts","22_18");
		industryMap.put("Sports and Recreations Clubs","22_19");
		industryMap.put("Amusement and Recreation","22_2");
		industryMap.put("Zoos and Parks","22_20");
		industryMap.put("Amusement Parks and Theme Parks","22_3");
		industryMap.put("Athletic Facilities","22_4");
		industryMap.put("Casinos and Gambling","22_5");
		industryMap.put("Convention Centers, Arenas, and Stadiums","22_6");
		industryMap.put("Cruise Lines","22_7");
		industryMap.put("Golf Courses and Country Clubs","22_8");
		industryMap.put("Hotels and Motels","22_9");
		industryMap.put("Book Publishers","23_1");
		industryMap.put("Motion Picture Post-Production Services","23_10");
		industryMap.put("Movie and TV Broadcasting Equipment","23_11");
		industryMap.put("Movie Production and Distribution","23_12");
		industryMap.put("Music Production and Distribution","23_13");
		industryMap.put("Newspapers and Online News Organizations","23_14");
		industryMap.put("Radio and Online Music Broadcasting","23_15");
		industryMap.put("Social Media","23_16");
		industryMap.put("Television Broadcasting","23_17");
		industryMap.put("Trading Cards and Comic Books","23_18");
		industryMap.put("Video Game Production","23_19");
		industryMap.put("Cable Television Networks","23_2");
		industryMap.put("Directories and Yellow Pages Publishers","23_3");
		industryMap.put("Diversified Media","23_4");
		industryMap.put("Greeting Cards","23_5");
		industryMap.put("Internet Content Providers","23_6");
		industryMap.put("Internet Information Services","23_7");
		industryMap.put("Internet Search and Navigation Services","23_8");
		industryMap.put("Magazine Publishers","23_9");
		industryMap.put("Aluminum Mining and Foundries","24_1");
		industryMap.put("Metal Rolling and Extruding","24_10");
		industryMap.put("Metal Smelting and Refining","24_11");
		industryMap.put("Metals and Minerals Distribution","24_12");
		industryMap.put("Mining and Quarrying Machinery","24_13");
		industryMap.put("Miscellaneous Metal Products","24_14");
		industryMap.put("Non-Ferrous Foundries","24_15");
		industryMap.put("Non-Metallic Mining and Quarrying","24_16");
		industryMap.put("Precious Metals and Minerals","24_17");
		industryMap.put("Sand And Gravel","24_18");
		industryMap.put("Specialty Mining and Metals","24_19");
		industryMap.put("Coal Mining","24_2");
		industryMap.put("Steel Works, Furnaces and Coke Ovens","24_20");
		industryMap.put("Concrete and Gypsum","24_3");
		industryMap.put("Diamond and Other Precious Stone Mining","24_4");
		industryMap.put("Diversified Metals and Mining","24_5");
		industryMap.put("Gold and Silver Mining","24_6");
		industryMap.put("Iron and Steel Foundries","24_7");
		industryMap.put("Limestone, Granite and Stone","24_8");
		industryMap.put("Metal Ore Mining","24_9");
		industryMap.put("Biological Products","25_1");
		industryMap.put("Pharmaceuticals Wholesale","25_10");
		industryMap.put("Scientific Research Services","25_11");
		industryMap.put("Specialty Pharmaceuticals","25_12");
		industryMap.put("Veterinary Drugs","25_13");
		industryMap.put("Biopharmaceuticals and Biotherapeutics","25_2");
		industryMap.put("Biotechnology Research","25_3");
		industryMap.put("Biotechnology Research Equipment","25_4");
		industryMap.put("Diagnostic Substances","25_5");
		industryMap.put("Diversified Pharmaceuticals","25_6");
		industryMap.put("Drug Delivery Systems","25_7");
		industryMap.put("Generic Pharmaceuticals","25_8");
		industryMap.put("Life Sciences Research and Development","25_9");
		industryMap.put("Cemetery Developers and Operators","26_1");
		industryMap.put("Mortgage REITs","26_10");
		industryMap.put("Office REITs","26_11");
		industryMap.put("Other Real Estate Investors","26_12");
		industryMap.put("Planning and Development Authorities","26_13");
		industryMap.put("Real Estate Appraisers","26_14");
		industryMap.put("Real Estate Brokers","26_15");
		industryMap.put("Real Estate Development","26_16");
		industryMap.put("Real Estate Investment Trusts (REITs);","26_17");
		industryMap.put("Real Estate Leasing","26_18");
		industryMap.put("Real Estate Operating Company (REOC);","26_19");
		industryMap.put("Health Care REITs","26_2");
		industryMap.put("Real Estate Property Management","26_20");
		industryMap.put("Residential REITs","26_21");
		industryMap.put("Retail REITs","26_22");
		industryMap.put("School and Public Building Authorities","26_23");
		industryMap.put("Title Abstract Offices","26_24");
		industryMap.put("Hotel and Motel REITs","26_3");
		industryMap.put("Housing Authorities","26_4");
		industryMap.put("Industrial Development Authorities","26_5");
		industryMap.put("Industrial REITs","26_6");
		industryMap.put("Land REITs","26_7");
		industryMap.put("Leisure and Entertainment REITs","26_8");
		industryMap.put("Mobile Home Parks","26_9");
		industryMap.put("Apparel Retail","27_1");
		industryMap.put("Computer and Software Retail","27_10");
		industryMap.put("Convenience Stores and Gas Stations","27_11");
		industryMap.put("Department Stores","27_12");
		industryMap.put("Dinnerware, Cookware and Cutlery Retail","27_13");
		industryMap.put("Discount and Variety Stores","27_14");
		industryMap.put("Electronics Retail","27_15");
		industryMap.put("Fashion Accessories Retail","27_16");
		industryMap.put("Florists and Nurseries","27_17");
		industryMap.put("Footwear Retail","27_18");
		industryMap.put("Furniture Retail","27_19");
		industryMap.put("Appliances Retail","27_2");
		industryMap.put("Gardening Supplies","27_20");
		industryMap.put("General Retailers","27_21");
		industryMap.put("Hobby, Toy and Game Stores","27_22");
		industryMap.put("Home Furnishing Retail","27_23");
		industryMap.put("Home Improvement and Hardware Stores","27_24");
		industryMap.put("Hyper and Supermarkets","27_25");
		industryMap.put("Internet Retail","27_26");
		industryMap.put("Jewelry and Gemstone Retail","27_27");
		industryMap.put("Luggage and Leather Goods Stores","27_28");
		industryMap.put("Lumber Retail","27_29");
		industryMap.put("Arts, Gifts and Novelties Retail","27_3");
		industryMap.put("Mail and Catalog Order Retail","27_30");
		industryMap.put("Maternity and Baby Supplies Retail","27_31");
		industryMap.put("Men's Clothing Retail","27_32");
		industryMap.put("Mini Markets","27_33");
		industryMap.put("Mobile Home Dealers","27_34");
		industryMap.put("Motor Vehicle Dealers","27_35");
		industryMap.put("Music, Video and Book Retail","27_36");
		industryMap.put("Musical Equipment Retail","27_37");
		industryMap.put("Office Supplies and Stationery Retail","27_38");
		industryMap.put("Party and Supply Stores","27_39");
		industryMap.put("Audiovisual Equipment Sales and Services","27_4");
		industryMap.put("Perfume, Cosmetics and Toiletries Retail","27_40");
		industryMap.put("Pet Shops","27_41");
		industryMap.put("Pharmacies and Drug Stores","27_42");
		industryMap.put("Specialty Retailers","27_43");
		industryMap.put("Sports and Recreational Equipment Retail","27_44");
		industryMap.put("Tobacco and Pipe Stores","27_45");
		industryMap.put("Vending Machines and Automated Kiosks","27_46");
		industryMap.put("Warehouse Clubs and Superstores","27_47");
		industryMap.put("Women's Clothing Retail","27_48");
		industryMap.put("Beer, Wine, and Liquor Retail","27_5");
		industryMap.put("Boat Retail","27_6");
		industryMap.put("Building Materials Retail","27_7");
		industryMap.put("Children's Clothing Retail","27_8");
		industryMap.put("Clock and Watch Retail","27_9");
		industryMap.put("Colleges and Universities","28_1");
		industryMap.put("School Districts","28_10");
		industryMap.put("Training Institutions and Services","28_11");
		industryMap.put("Vocational and Technical Schools","28_12");
		industryMap.put("Community Colleges","28_2");
		industryMap.put("Educational Services","28_3");
		industryMap.put("Elementary and Secondary Schools","28_4");
		industryMap.put("Graduate and Professional Schools","28_5");
		industryMap.put("Internet Educational Services","28_6");
		industryMap.put("Law Schools","28_7");
		industryMap.put("Libraries","28_8");
		industryMap.put("Medical Schools","28_9");
		industryMap.put("Cable and Satellite Services","29_1");
		industryMap.put("Telecommunications Equipment","29_10");
		industryMap.put("Telecommunications Resellers","29_11");
		industryMap.put("Telecommunications Services","29_12");
		industryMap.put("Teleconferencing Services Providers","29_13");
		industryMap.put("Telemetry and Telematics Services","29_14");
		industryMap.put("Videoconferencing Equipment","29_15");
		industryMap.put("Web Hosting Services","29_16");
		industryMap.put("Wired Telecommunications Carriers","29_17");
		industryMap.put("Wireless Network Operators","29_18");
		industryMap.put("Electronic Communications Networks","29_2");
		industryMap.put("Internet and Online Services Providers","29_3");
		industryMap.put("Local Exchange Carriers","29_4");
		industryMap.put("Long-Distance Carriers","29_5");
		industryMap.put("Managed Network Services","29_6");
		industryMap.put("Messaging Services Providers","29_7");
		industryMap.put("Satellite and Broadcasting Equipment","29_8");
		industryMap.put("Telecom Switching and Transmission","29_9");
		industryMap.put("Air Freight Transportation","30_1");
		industryMap.put("Highways and Toll Road Management","30_10");
		industryMap.put("Locomotive and Rail Car Manufacturing","30_11");
		industryMap.put("Marine Shipping","30_12");
		industryMap.put("Marine Transportation Support","30_13");
		industryMap.put("Non-Scheduled and Charter Air Transport","30_14");
		industryMap.put("Passenger Railroads","30_15");
		industryMap.put("Ports, Harbors and Marinas","30_16");
		industryMap.put("Postal Services","30_17");
		industryMap.put("Railroad Equipment Manufacturing","30_18");
		industryMap.put("Railroad Terminal Management","30_19");
		industryMap.put("Aircraft Leasing","30_2");
		industryMap.put("Railroads and Rail Tracks Maintenance","30_20");
		industryMap.put("Ship and Boat Manufacturing","30_21");
		industryMap.put("Ship and Boat Parts Manufacturing","30_22");
		industryMap.put("Ship and Boat Repair","30_23");
		industryMap.put("Shipping Equipment","30_24");
		industryMap.put("Storage and Warehousing","30_25");
		industryMap.put("Terminal Facilities For Motor Vehicles","30_26");
		industryMap.put("Transportation Authorities","30_27");
		industryMap.put("Truck Rental and Leasing","30_28");
		industryMap.put("Truck Transportation and Services","30_29");
		industryMap.put("Airlines and Scheduled Air Transport","30_3");
		industryMap.put("Airport and Terminal Management","30_4");
		industryMap.put("Bus Services","30_5");
		industryMap.put("Container Leasing","30_6");
		industryMap.put("Ferries and Water Transport","30_7");
		industryMap.put("Freight Railroads","30_8");
		industryMap.put("Helicopter Services","30_9");

		return industryMap;
	}

}