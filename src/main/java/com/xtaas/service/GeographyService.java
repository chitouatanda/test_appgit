package com.xtaas.service;

import java.util.List;

import com.xtaas.domain.valueobject.Location;

public interface GeographyService {
	public List<Location> getAreas();
	public List<Location> getCountries(); 
	public List<Location> getStates();
	public List<Location> getCities();
	public List<String> getIncludedCountries(List<String> countries, boolean excludeCountries);
	public List<String> getIncludedStates(List<String> countries, List<String> states, boolean excludeStates);
	public List<String> getIncludedCities(List<String> countries, List<String> states, List<String> cities,
			boolean excludeCities);
	
}