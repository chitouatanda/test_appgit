/**
 * 
 */
package com.xtaas.service;

import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.infra.security.AppManager;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.infra.security.Roles;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.UserPrincipal;
import com.xtaas.web.dto.LoginDTO;
import com.xtaas.web.dto.PasswordDTO;
import com.xtaas.web.dto.PasswordPolicyDTO;
import com.xtaas.web.dto.SupervisorDTO;
import com.xtaas.web.dto.UserImpersonateDTO;
import com.xtaas.web.dto.UserImpersonateResultDTO;
import com.xtaas.web.dto.UserImpersonateSearchDTO;

import de.taimos.totp.TOTP;

/**
 * @author djain
 *
 */
@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	@Qualifier("xtaasUserDetailsServiceImpl") 
	private UserDetailsService userDetailsService;
	
	@Value("${SERVER_BASE_URL}")
	private String serverBaseUrl;
	
	@Value("${server.access.url}")
	private String serverAccessUrl;
	
	@Autowired
	private AgentRepository agentRepository;
	
	ConcurrentHashMap<String, Login> userByUsernameCache = new ConcurrentHashMap<String, Login>();
	
	@Override
	public Login getUser(String userId) {
		String username = userId.toLowerCase();
		Login user = userByUsernameCache.get(username);
		if (user == null) {
			user = loginRepository.findOneById(username);
			userByUsernameCache.put(username, user);
		}
		return user;
	}
	
	@Override
	public String removeUserFromCache(String userId) {
		String username = userId.toLowerCase();
		Login user = userByUsernameCache.get(username);
		if (user != null) {
			userByUsernameCache.remove(username);
		}
		return "Remove "+username+" From Cache Successfully";
	}
	
	@Override
	public void updateSalesforceOAuthTokens(String userId, String refreshToken, String accessToken, String instanceUrl) {
		/*Login userFromDB = loginRepository.findOneById(userId);
		if ((refreshToken != null && !refreshToken.equals(userFromDB.getSalesforceRefreshToken()))
				|| (accessToken != null && !accessToken.equals(userFromDB.getSalesforceAccessToken()))) {
			userFromDB.setSalesforceRefreshToken(refreshToken);
			userFromDB.setSalesforceAccessToken(accessToken);
			userFromDB.setInstanceUrl(instanceUrl);
			Login savedUser = loginRepository.save(userFromDB);
			userByUsernameCache.put(userId, savedUser);
			logger.debug("updateSalesforceOAuthTokens() : Updated Salesforce Refresh and Access Tokens for userId : " + userId);
		}*/
	}

	@Override
	public boolean updatePassword(String userId, PasswordDTO passwordDTO) {
		Login userFromDB = loginRepository.findOneById(userId.toLowerCase());
		if (userFromDB == null) {
			logger.error("updatePassword() : User [{}] not found", userId);
			return false;
		}
		if (!userFromDB.getPasswordHash(passwordDTO.getCurrentPassword()).equals(userFromDB.getPassword())) {
			throw new IllegalArgumentException("Current password is incorrect for userId "+ userId);
		}
		Organization organization = organizationRepository.findOneById(userFromDB.getOrganization());
		String hashPassword = userFromDB.getPasswordHash(passwordDTO.getNewPassword());
		if(organization.isPasswordPolicy()) {
			calculatePasswordStrength(passwordDTO.getNewPassword(),userFromDB);
			Set<String> recentPasswordsFromDB = userFromDB.getRecentPasswords();
			if(recentPasswordsFromDB != null && !recentPasswordsFromDB.isEmpty()) {
				if (recentPasswordsFromDB.contains(hashPassword)) {
					throw new IllegalArgumentException("Password must be different from last four passwords.");
				}else {
					recentPasswordsFromDB.add(hashPassword);
					if(recentPasswordsFromDB.size() > 4) {
						recentPasswordsFromDB.remove(recentPasswordsFromDB.iterator().next());
					}	
				}
			}else {
				recentPasswordsFromDB = new LinkedHashSet<>();
				recentPasswordsFromDB.add(hashPassword);		
			}
			userFromDB.setLastPasswordUpdated(new Date());
			userFromDB.setActivateUser(true);
			userFromDB.setRecentPasswords(recentPasswordsFromDB);
		}
		userFromDB.setPassword(hashPassword);
		userFromDB = loginRepository.save(userFromDB);
		logger.debug("updatePassword() : Updated Password for userId [{}] ", userId);
		return true;
	}

	@Override
	public boolean createUser(Login user) throws DuplicateKeyException{
		if (null == user) {
			return false;
		}
		user.setUsername(user.getUsername().toLowerCase());
		user.setPassword(user.getPasswordHash(user.getPassword()));
		//converting all roles to UPPER CASE 
		ListIterator<String> iterator = user.getRoles().listIterator();
	    while (iterator.hasNext()) {
	        iterator.set(iterator.next().toUpperCase());
	    }
	    user.setOrganization(user.getOrganization());
	   Login login = loginRepository.save(user);
		logger.debug(user.getUsername()+" user created successfully");
		return true;
	}

	/**
     * Send reset password link to emailId of user
     * @return
     */
	@Override
	public boolean sendResetPasswordLink(String username) {
		Login user = loginRepository.findOneById(username.toLowerCase());
		if (user == null) {
			throw new IllegalArgumentException(username + " not found ");
		}
		if(user.getRoles().contains(Roles.ROLE_AGENT.toString())) {
			Agent agent = agentRepository.findByIdAndPartnerId(user.getId(),user.getOrganization());
			if(agent != null && agent.getStatus().equals(AgentStatus.INACTIVE)) {
				throw new IllegalArgumentException("Your account is Inactive. Please contact your supervisor.");
			}
		}
		Organization organization = organizationRepository.findOneById(user.getOrganization());
		if(organization.isPasswordPolicy()) {
			if(user.isUserLock()) {
				throw new IllegalArgumentException("Your account is Locked. Please contact support team.");
			}
		}
		String adminEmail = organization.getOrganizationEmail();
		//Authenticate the user
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		  authorities.add(new SimpleGrantedAuthority(StringUtils.join(user.getRoles(),",")));
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username,user.getPassword(), authorities));
		
		String token = generateToken(user.getUsername());
		String subject = "Reset password";
		String link = serverAccessUrl + "/spr/rest/user/changeforgotpassword?token="+token;
		String linkref = "<a href=\" "+link+" \"><button style=\"background-color:#4CAF50;border: none;color: white;text-align: center;text-decoration: none;display: inline-block;font-size: 16px; margin: 4px 2px;cursor: pointer;padding: 14px 40px;\">Reset Password</button></a>";
		String msgBody = "<p style=\"font-size: 30px;\">Hello " + user.getFirstName() + " " + user.getLastName()+"<p>"
				+ "<p><b>A request has been received to change the password for your account.<b></p>"
				+ linkref+"<br><br>if you did not initiate this request, please contact administrator."
				+"<br><br>Thank you.";
		
		XtaasEmailUtils.sendEmail(user.getEmail(), "data@xtaascorp.com", subject, msgBody);
		if (adminEmail != null) {
			XtaasEmailUtils.sendEmail(adminEmail, "data@xtaascorp.com", "Reset Password link for user : "+username, msgBody);
		} else {
			logger.debug("Admin Email not found");
		}
		
		
		SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
		return true;
	}
	
	/**
     * Generate a token to integrate in a change password link
     * @return
	 * @throws UnsupportedEncodingException 
     */
	private String generateToken(String userName) {
		//generate authdata in format username|time in MS
		StringBuilder authDataBuilder = new StringBuilder(); 
		authDataBuilder.append(userName).append("|"); //user
		authDataBuilder.append(System.currentTimeMillis()); //time in MS when token was generated
		String authData = authDataBuilder.toString();
		
		//generate token in the format data:hash
		String appSecurityKey = AppManager.getSecurityKey(App.BACKEND_SUPPORT_SYSTEM);
		String token = EncryptionUtils.encrypt(authData,  appSecurityKey) + ":" + EncryptionUtils.generateHash(authData, appSecurityKey);
		String encryptedToken = null;
		try {
			encryptedToken = URLEncoder.encode(token,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("encoding is unsupported", e);
		}
		return encryptedToken;
	}
	
	/**
     * Generate email for sending a change password page link
     * @return
     */
	private EmailMessage generateEmail(Login user, String appUrl, String token) {
		String email = "dharmesh.vekariya@invimatic.in";
		List<String> to = Arrays.asList(email);
		String subject = "Reset password";
		String msgBody = "Click on link to reset your password "+appUrl+ "/spr/rest/user/changeforgotpassword?token="+token;
		return new EmailMessage(to, subject, msgBody);
	}

	/**
     * Validate a token to redirect a user to change password page
     * @return
     */
	@Override
	public boolean validateResetPasswordLink(String encryptedToken) {
		if(encryptedToken==null) return false;

	        /* Token is in format data:hash where,
	         * 		data is two-way encrypted data using the app's secret key. After decryption data will be in format username|time in MS 
	         * 		hash is one-way encryption of data using the app's secret key
	         */
		String token = null;
		try {
			token = URLDecoder.decode(encryptedToken,"UTF-8").replaceAll(" ", "+");
		} catch (UnsupportedEncodingException e) {
			logger.error("encoding is unsupported", e);
		}
	        
        //get the data and hash from token
        String[] splittedTokens = StringUtils.split(token, ":");
        String data = splittedTokens[0];
        String hash = splittedTokens[1];
        
        //get the app's secret key
        String appSecurityKey = AppManager.getSecurityKey(App.BACKEND_SUPPORT_SYSTEM);
        
        //decrypt the data using the secret key
        String decryptedData = EncryptionUtils.decrypt(data, appSecurityKey);
        
        //check if data has not been compromised in transit
        String generatedHash = EncryptionUtils.generateHash(decryptedData, appSecurityKey);
        if (!hash.equals(generatedHash)) return false;
        String[] splittedData = StringUtils.split(decryptedData, "|");
        String username = splittedData[0];
        Long tokenGenTimeInMillis = Long.valueOf(splittedData[1]);
        
        //token must not be older than one day (86400000) Millis
        if ((System.currentTimeMillis() - tokenGenTimeInMillis) > XtaasConstants.ONE_DAY_IN_MILLIS) {
        	throw new IllegalArgumentException("Token is Expired ");
        }
        
        //Authenticate the user
        UserDetails userPrincipal = userDetailsService.loadUserByUsername(username);
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userPrincipal, userPrincipal.getPassword(), userPrincipal.getAuthorities()));
	    return true;
	}

	@Override
	public boolean updateForgotPassword(String newPassword) {
		//get currently logged in user from security context
		UserPrincipal userPrinciple = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (userPrinciple == null) {
			throw new IllegalArgumentException("Logged in user not found ");
		}
		Login user = userPrinciple.getLogin();
		Organization organization = organizationRepository.findOneById(user.getOrganization());
		String hashPassword = user.getPasswordHash(newPassword);
		if(organization.isPasswordPolicy()) {
			calculatePasswordStrength(newPassword,user);
			Set<String> recentPasswordsFromDB = user.getRecentPasswords();
			if(recentPasswordsFromDB != null && !recentPasswordsFromDB.isEmpty()) {
				if (recentPasswordsFromDB.contains(hashPassword)) {
					throw new IllegalArgumentException("Password must be different from last four passwords.");
				}else {
					recentPasswordsFromDB.add(hashPassword);
					if(recentPasswordsFromDB.size() > 4) {
						recentPasswordsFromDB.remove(recentPasswordsFromDB.iterator().next());
					}	
				}
			}else {
				recentPasswordsFromDB = new LinkedHashSet<>();
				recentPasswordsFromDB.add(hashPassword);		
			}
			user.setLastPasswordUpdated(new Date());
			user.setActivateUser(true);
			user.setRecentPasswords(recentPasswordsFromDB);
		}
		user.setPassword(hashPassword);
		try {
			user = loginRepository.save(user);
		} catch (OptimisticLockingFailureException e) {
			// TODO: handle exception
			user.setVersion(user.getVersion());
			user = loginRepository.save(user);
		}
		logger.debug("updatePassword() : Updated Password for userId [{}] "+user.getId());
		return true;
	}
	
	private void calculatePasswordStrength(String password,Login user) {
		StringBuffer sBuffer = new StringBuffer();
		if (password.length() < 12)
			sBuffer.append("Password must contain minimum 12 characters.<br>");
		else if (password.length() > 22)
			sBuffer.append("Password must not exceed 22 characters.<br>");
		
		if (!password.matches("(?=.*[0-9]).*"))
			sBuffer.append("Password must contain at least one digit.<br>");
		
		if (!password.matches("(?=.*[a-z]).*"))
			sBuffer.append("Password must contain at least one lower case alphabet.<br>");

		if (!password.matches("(?=.*[A-Z]).*"))
			sBuffer.append("Password must contain at least one upper case alphabet.<br>");

		if (!password.matches("(?=.*[!@#$]).*"))
			sBuffer.append("Password must contain at least one special character which includes ! @ # $.<br>");
		
		if (password.contains(" "))
			sBuffer.append("Password must not contain any white space.<br>");
		
		if (password.contains(user.getFirstName()) || password.contains(user.getLastName()) || password.contains(user.getEmail().split("@")[0]))
			sBuffer.append("Password must not contain any personal information.<br>");
		
		if(sBuffer != null && sBuffer.length() > 0) {
			throw new IllegalArgumentException(sBuffer.toString());
		}
	}
	
	@Override
	public List<SupervisorDTO> getsupervisorList(String organizationId) {
		Login userLogin = getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		List<SupervisorDTO> supervisorList = new ArrayList<SupervisorDTO>();
		if (userLogin != null && userLogin.getOrganization() != null && organizationId != null
				&& userLogin.getOrganization().equalsIgnoreCase(organizationId)
				&& userLogin.getRoles().contains(Roles.ROLE_CAMPAIGN_MANAGER.toString())) {
			List<LoginDTO> list = loginRepository.findSupervisorByOrganization(organizationId);
			list = list.stream()
					.filter(login -> login.getAdminRole() != null
							&& login.getAdminRole().equalsIgnoreCase(XtaasConstants.ADMIN_SUPERVISOR))
					.collect(Collectors.toList());
			supervisorList = list.stream().map(supervisorDTO -> new SupervisorDTO(supervisorDTO))
					.collect(Collectors.toList());
			return supervisorList;
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
	}
	
	@Override
	public Login getAgentNetworkStatus(String username) {
		Login login = loginRepository.findOneById(username);
		return login;
	}

	@Override
	public Login postAgentNetworkStatus(String username,boolean networkstatus) {
		Login login = loginRepository.findOneById(username);
		if (login != null) {
			login.setVersion(login.getVersion());
			login.setNetworkStatus(networkstatus);
			loginRepository.save(login);
		}
		return login;
	}

	@Override
	public void resetUserAfterFiveAttempts(String username) {
		String logedinUser = XtaasUserUtils.getCurrentLoggedInUsername();
		if(logedinUser.equals(username)) {
			removeUserFromCache(username);
			Login login = loginRepository.findOneById(username.toLowerCase());
			if (login != null) {
				login.setNoOfAttempt(0);
				login.setUserLock(false);
				loginRepository.save(login);
			}
		}else {
			throw new IllegalArgumentException("You are not authorized.");
		}
	}

	@Override
	public void lockUserAfterFiveAttempts(String username) {
		int defaultAttempts = 5;
		byte[] decodedBytes = Base64.getDecoder().decode(username);
		String decryptUserName = new String(decodedBytes);
		Login login = loginRepository.findOneById(decryptUserName.toLowerCase());
		if (login != null) {
			Organization organization = organizationRepository.findOneById(login.getOrganization());
			if(organization.getLockPasswordAttempts() > 0) {
				defaultAttempts = organization.getLockPasswordAttempts();
			}
			login.setNoOfAttempt(login.getNoOfAttempt() + 1);
			if(login.getNoOfAttempt() >= defaultAttempts) {
				login.setUserLock(true);	
			}
			loginRepository.save(login);
		}
	}

	@Override
	public PasswordPolicyDTO getUserFromDB(String username) {
		byte[] decodedBytes = Base64.getDecoder().decode(username);
		String decryptUserName = new String(decodedBytes);
		Login login = loginRepository.findOneById(decryptUserName.toLowerCase());
		if (login != null) {
			Organization organization = organizationRepository.findOneById(login.getOrganization());
			PasswordPolicyDTO dto = new PasswordPolicyDTO();
			dto.setActivateUser(login.isActivateUser());
			dto.setLastPasswordUpdated(login.getLastPasswordUpdated());
			dto.setNoOfAttempt(login.getNoOfAttempt());
			dto.setUserLock(login.isUserLock());
			dto.setPasswordPolicy(organization.isPasswordPolicy());
			dto.setMultiFactorRequired(organization.isMultiFactorRequired());
			dto.setActivateMFAUser(login.isActivateMFAUser());
			dto.setEnforeMFA(organization.isEnforeMFA());
			if (organization.getAllowMFAUsers() != null && !organization.getAllowMFAUsers().isEmpty()) {
				if (organization.getAllowMFAUsers().contains(login.getUsername())) {
					dto.setAllowMFAUser(true);
				} else {
					dto.setAllowMFAUser(false);
				}
			} else {
				dto.setAllowMFAUser(true);
			}
			return dto;
		}
		return null;
	}
	
	@Override
	public String createQRCodeMFA(String username) throws WriterException, IOException {
		if(!XtaasUserUtils.getCurrentLoggedInUsername().equalsIgnoreCase(username)) {
			throw new IllegalArgumentException("You dont have permission to access");
		}
		Login login = loginRepository.findOneById(username.toLowerCase());
		if (login != null) {
			if(!login.isActivateMFAUser()) {
				String secretKey = generateSecretKey();
				login.setMultiFactorKey(secretKey);
				login = loginRepository.save(login);
			}
			String barCodeData = getGoogleAuthenticatorBarCode(login);
			BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE, 120, 120);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(matrix, "png", outputStream);
			String base64 = new String(Base64.getEncoder().encode(outputStream.toByteArray()));
			return "data:image/png;base64," + base64;
		}
		return null;
	}
	
	/*
     * to validate the  totp which is generated in google authenticaotr, if valid should redirt to the app.
     * we need to call this method when user enters the totp code to compare and if same redirect
     * this code and the code enterd by user are validated on ui level if same validate
     */
	@Override
	public boolean getTOTPCode(String username, int topt) {
		if(!XtaasUserUtils.getCurrentLoggedInUsername().equalsIgnoreCase(username)) {
			throw new IllegalArgumentException("You dont have permission to access");
		}
		int defaultAttempts = 5;
		Login login = loginRepository.findOneById(username.toLowerCase());
		if (login != null) {
			Organization organization = organizationRepository.findOneById(login.getOrganization());
			if(organization.getLockPasswordAttempts() > 0) {
				defaultAttempts = organization.getLockPasswordAttempts();
			}
			Base32 base32 = new Base32();
			byte[] bytes = base32.decode(login.getMultiFactorKey());
			String hexKey = Hex.encodeHexString(bytes);
			if(TOTP.getOTP(hexKey).equals(String.valueOf(topt))) {
				if(!login.isActivateMFAUser()) {
					login.setActivateMFAUser(true);
					loginRepository.save(login);
				}
				if(login.getNoOfAttempt() > 0) {
					login.setNoOfAttempt(0);
					loginRepository.save(login);
				}
				return true;
			}else {
				if(login.getNoOfAttempt() >= defaultAttempts) {
					//login.setActivateMFAUser(false);
					login.setUserLock(true);
					loginRepository.save(login);
					throw new IllegalArgumentException("Your account is Locked. Please contact support team.");
				}
				login.setNoOfAttempt(login.getNoOfAttempt() + 1);
				loginRepository.save(login);
				return false;
			}
		}
		return false;
	}  
	
	/*
	 * following method is used to generate a secrete key for TOTP authentication
	 * if this key is in login collection, we should not generate a new one if opted for 2FA 
	 * else we need to generate a new one.
	 */
	private static String generateSecretKey() {
		SecureRandom random = new SecureRandom();
		byte[] bytes = new byte[20];
		random.nextBytes(bytes);
		Base32 base32 = new Base32();
		return base32.encodeToString(bytes);
	}
	
	/*
	 * to generate a google authenticator bar code which needs to be sent to the
	 * user when he opts for 2FA
	 */
	private String getGoogleAuthenticatorBarCode(Login login) {
		try {
			return "otpauth://totp/"
					+ URLEncoder.encode(XtaasConstants.AUTH_MFA_ISSUER + ":" + login.getId(), "UTF-8").replace("+",
							"%20")
					+ "?secret=" + URLEncoder.encode(login.getMultiFactorKey(), "UTF-8").replace("+", "%20")
					+ "&issuer=" + URLEncoder.encode(XtaasConstants.AUTH_MFA_ISSUER, "UTF-8").replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
	}
	  
	public void firstLoginSuccess(String userName) {
		try {
			Login login = loginRepository.findOneById(userName.toLowerCase());
			if (!XtaasUserUtils.getCurrentLoggedInUsername().equalsIgnoreCase(userName)) {
				throw new IllegalArgumentException("You dont have permission to access");
			}
			login.setActivateMFAUser(true);
			login = loginRepository.save(login);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}  
		                  
	    
	/*
     * to reset the 2MFA function
     */
    public void resetTOTPCode(String userName) {
		Login login = loginRepository.findOneById(userName.toLowerCase());
		if(!XtaasUserUtils.getCurrentLoggedInUsername().equalsIgnoreCase("userName")) {
			throw new IllegalArgumentException("You dont have permission to access");
		}
		//login.setMultiFactorRequired(false);
		login.setMultiFactorKey(null);
		loginRepository.save(login);
    }

	@Override
	public UserImpersonateResultDTO getUserList(UserImpersonateSearchDTO searchDTO) {
		List<LoginDTO> loginDTOs = new ArrayList<LoginDTO>();
		long totalUsers = 0;
		Pageable pageable = PageRequest.of(searchDTO.getPage(), searchDTO.getSize(), searchDTO.getDirection(),
				searchDTO.getSort());
		List<Agent> inactiveAgents = agentRepository.findInactive();
		List<String> inactiveAgentIds = new ArrayList<String>();
		if (!CollectionUtils.isEmpty(inactiveAgents)) {
			inactiveAgentIds = inactiveAgents.stream().map(agent -> agent.getId()).collect(Collectors.toList());
		}
		if (StringUtils.isEmpty(searchDTO.getSearchText()) || StringUtils.isEmpty(searchDTO.getSearchType())) {
			loginDTOs = loginRepository.findByIdsAndRoles(inactiveAgentIds, pageable);
			totalUsers = loginRepository.countByIdsAndRoles(inactiveAgentIds);
		} else {
			if (searchDTO.getSearchType().equalsIgnoreCase("username")) {
				loginDTOs = loginRepository.findByUsernameAndRoles(searchDTO.getSearchText(), inactiveAgentIds, pageable);
				totalUsers = loginRepository.countByUsernameAndRoles(searchDTO.getSearchText(), inactiveAgentIds);
			} else if (searchDTO.getSearchType().equalsIgnoreCase("organization")) {
				loginDTOs = loginRepository.findByOrganizationAndIdsAndRoles(searchDTO.getSearchText(), inactiveAgentIds, pageable);
				totalUsers = loginRepository.countByOrganizationAndIdsAndRoles(searchDTO.getSearchText(), inactiveAgentIds);
			}
		}
		List<UserImpersonateDTO> userImpersonateDTOs = loginDTOs.stream()
				.map(loginDTO -> new UserImpersonateDTO(loginDTO)).collect(Collectors.toList());
		return new UserImpersonateResultDTO(totalUsers, userImpersonateDTOs);
	}

}
