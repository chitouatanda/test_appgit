/**
 * 
 */
package com.xtaas.service;

import java.util.HashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * A wrapper over Jersey Rest client to provide ease of use of Rest Client Service
 * 
 * @author djain
 */
@Component
public class RestClientService {

	private static final Logger logger = LoggerFactory.getLogger(RestClientService.class);
	
	/**
	 * Jersey client is thread safe. We can create many WebTarget from one Client instance 
	 * and invoke many requests on these WebTargets and even more requests on one WebTarget instance in the same time.
	 * 
	 * From Jersey Documentation :
	 * The creation of a Client instance is an expensive operation and the instance may make use of and retain many resources. 
	 * It is therefore recommended that a Client instance is reused for the creation of WebResource instances that require the 
	 * same configuration settings.
	 */
	private Client client = ClientBuilder.newClient(); //Single Client instance
	
	/**
	 * Make a Post request to specified url with Content-Type as Application/Json and accepting response in JSON/Plain Text.
	 * @param url - Url to which request needs to be sent
	 * @param postMessage - Message which needs to be submitted
	 * @return Response received from Server
	 */
	public String makePostRequest(String url, HashMap<String, String> headers, String postMessage) {
		//logger.info("makePostRequest() : Making Request to URL: {}, POSTDATA : {}" , url, postMessage);
		try {
			WebTarget webTarget = client.target(url);
			
			Builder requestBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE, MediaType.TEXT_PLAIN_TYPE);
			
			Response response = setHeaders(requestBuilder, headers).post(Entity.json(postMessage));
			
			String respBody = response.readEntity(String.class);
			
			//logger.info("makePostRequest(): Response Status: {}, Response Body: {} " + response.getStatus(), respBody);
			return respBody;
		} catch (Exception e) {
			logger.error("makePostRequest(): Exception occurred." , e);
		}
		return null;
	}
	
	/**
	 * Make a Get Rest request to specified Url  and returns the response received.
	 * @param url - All get params emebedded to incoming url
	 * @return response received from rest call.
	 */
	public String makeGetRequest(String url, HashMap<String, String> headers) {
		//logger.info("makeGetRequest() : Making Request to URL: " + url);
		try {
			WebTarget webTarget = client.target(url);
			
			Builder requestBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE, MediaType.TEXT_PLAIN_TYPE);
			Response response =	setHeaders(requestBuilder, headers).get();
			String respBody = response.readEntity(String.class);
			//logger.info("makeGetRequest(): Response Status: {}, Response Body: {} " + response.getStatus(), respBody);
			return respBody;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("makeGetRequest(): Exception occurred." , e);
		}
		return null;
	}
	
	/**
	 * Make a Delete Rest request to specified Url  and returns the response received.
	 * @param url 
	 * @return response received from rest call.
	 */
	public String makeDeleteRequest(String url, HashMap<String, String> headers) {
		//logger.info("makeDeleteRequest() : Making Request to URL: " + url);
		try {
			WebTarget webTarget = client.target(url);
			
			Builder requestBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE, MediaType.TEXT_PLAIN_TYPE);
					
			Response response =	setHeaders(requestBuilder, headers).delete();
			
			String respBody = response.readEntity(String.class);
			//logger.info("makeDeleteRequest(): Response Status: {}, Response Body: {} " + response.getStatus(), respBody);
			return respBody;
		} catch (Exception e) {
			logger.error("makeDeleteRequest(): Exception occurred." , e);
		}
		return null;
	}
	
	private Builder setHeaders(Builder requestBuilder, HashMap<String, String> headers) {
		if (headers != null) {
			for (String header : headers.keySet()) {
				requestBuilder.header(header, headers.get(header));
			}
		}
		return requestBuilder;
	}
}
