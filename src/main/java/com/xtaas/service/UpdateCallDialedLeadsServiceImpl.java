package com.xtaas.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.CampaignDTO;

@Service
public class UpdateCallDialedLeadsServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(UpdateCallDialedLeadsServiceImpl.class);

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private ApiTestingService apiTestingService;

	public void moveLeadsToAuto() {
		Date jobExecutionTime = new Date();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		List<CampaignDTO> campaigns = campaignService.findActiveWithProjectedFields(Arrays.asList());
		if (campaigns != null && campaigns.size() > 0) {
			for (CampaignDTO activeCampaign : campaigns) {
				Runnable runnable = () -> {
					processCallDialedProspects(activeCampaign.getId(), jobExecutionTime);
				};
				executorService.submit(runnable);
			}
		}
	}

	private void processCallDialedProspects(String activeCampaignId, Date jobExecutionTime) {
		Date startDate = XtaasDateUtils.getDateAtTime(new Date(), 0, 0, 0, 0);
		Date endDate = XtaasDateUtils.getTimeBeforeGivenMinutes(jobExecutionTime, 15);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDate);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		if (hours == 23 || hours == 00) {
			startDate = XtaasDateUtils.getTimeBeforeGivenHours(endDate, 24);
		}
		List<ProspectCallLog> prospectCallLogsFromDB = prospectCallLogRepository
				.findAllCallDialedProspectsOfCampaign(activeCampaignId, startDate, endDate);
		if (prospectCallLogsFromDB != null && prospectCallLogsFromDB.size() > 0) {
			logger.info("Found [{}] CALL_DIALED Prospects having campaignId :- ", prospectCallLogsFromDB.size(),
					activeCampaignId);
			List<ProspectCallLog> autoProspects = createProspectList(prospectCallLogsFromDB);
			if (autoProspects != null && autoProspects.size() > 0) {
				logger.info("Updating prospects to disposition status NOT_DISPOSED size [{}] in PROSPECTCALLLOG",
						autoProspects.size());
				prospectCallLogRepository.saveAll(autoProspects);
				updateInteractions(autoProspects);
			}
		}
	}

	private void updateInteractions(List<ProspectCallLog> autoProspects) {
		List<String> prospectCallIds = autoProspects.stream()
				.map(prospect -> prospect.getProspectCall().getProspectCallId()).collect(Collectors.toList());
		List<ProspectCallInteraction> interactionsFromDB = prospectCallInteractionRepository
				.findByProspectCallIds(prospectCallIds);
		if (interactionsFromDB != null && interactionsFromDB.size() > 0) {
			List<ProspectCallInteraction> interactions = createInteractionList(interactionsFromDB);
			logger.info("Updating prospects to disposition status NOT_DISPOSED size [{}] in PROSPECTCALLINTERACTION",
					autoProspects.size());
			prospectCallInteractionRepository.saveAll(interactions);
		}
	}

	private List<ProspectCallLog> createProspectList(List<ProspectCallLog> prospects) {
		List<ProspectCallLog> autoUpdatedProspects = new ArrayList<ProspectCallLog>();
		if (prospects != null && prospects.size() > 0) {
			for (ProspectCallLog prospectCallLog : prospects) {
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
					if (prospectCallLog.getProspectCall().getTwilioCallSid() != null) {
						apiTestingService.fetchAndSaveRecordingUrl(prospectCallLog.getProspectCall().getTwilioCallSid(),
								"supportteam@xtaascorp.com", false);
					}
					prospectCallLog.getProspectCall().setDispositionStatus("NOT_DISPOSED");
					prospectCallLog.getProspectCall().setSubStatus("NOT_DISPOSED");
					prospectCallLog.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
					autoUpdatedProspects.add(prospectCallLog);
				}
			}
		}
		return autoUpdatedProspects;
	}

	private List<ProspectCallInteraction> createInteractionList(List<ProspectCallInteraction> prospects) {
		List<ProspectCallInteraction> autoUpdatedInteractions = new ArrayList<ProspectCallInteraction>();
		if (prospects != null && prospects.size() > 0) {
			for (ProspectCallInteraction prospectCallLog : prospects) {
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
					prospectCallLog.getProspectCall().setDispositionStatus("NOT_DISPOSED");
					prospectCallLog.getProspectCall().setSubStatus("NOT_DISPOSED");
					prospectCallLog.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
					autoUpdatedInteractions.add(prospectCallLog);
				}
			}
		}
		return autoUpdatedInteractions;
	}
}