package com.xtaas.service;

import com.xtaas.domain.valueobject.AgentStatus;

public interface AutoApproveAgentRequestService {

	public void insertAgentRequest(String agentId, String campaignId, String agentType, AgentStatus agentStatus);

	public void removeApprovedRequest(String agentid);

	public void autoApproveAgentRequest();

}
