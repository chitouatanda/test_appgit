package com.xtaas.service;

import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.db.entity.RealTimeDeliveryIndustryMapping;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.RealTimeDeliveryRepository;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.RealTimePost;
import com.xtaas.utils.XMLConverterUtil;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.Attribute;
import com.xtaas.web.dto.LeadformAttributes;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RealTimeDeliveryServiceImpl implements RealTimeDeliveryService {

	private static final Logger logger = LoggerFactory.getLogger(RealTimeDeliveryServiceImpl.class);

	@Autowired
	private RealTimeDeliveryRepository realTimeDeliveryRepository;

	@Autowired
	private ClientMappingRepository clientMappingRepository;

	@Autowired
	private RealTimeDeliveryIndustryMappingService realTimeDeliveryIndustryMappingService;
	
	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Override
	public void sendLeadToClient(ProspectCallLog prospectCallLogFromDB, RealTimePost realTimePost) {
		String prospectCampaignID = prospectCallLogFromDB.getProspectCall().getCampaignId();
		RealTimeDelivery realTimeDelivery = getRealTimeDelivery(prospectCampaignID);

		if (realTimeDelivery != null && realTimeDelivery.getRealTimePost().equals(realTimePost)) {
			try {
				if (realTimeDelivery.getClientName().equalsIgnoreCase(XtaasConstants.WHEELHOUSE_CLIENT)) {
					logger.debug("Sending real time lead for [{}] client for prospectCallId [{}].",
							realTimeDelivery.getClientName(),
							prospectCallLogFromDB.getProspectCall().getProspectCallId());
					prepareRequestBodyWheelhouse(prospectCallLogFromDB, realTimeDelivery);
				} else if (realTimeDelivery.getClientName().equalsIgnoreCase(XtaasConstants.INSIDEUP_CLIENT)) {
					logger.debug("Sending real time lead for [{}] client for prospectCallId [{}].",
							realTimeDelivery.getClientName(),
							prospectCallLogFromDB.getProspectCall().getProspectCallId());
					prepareRequestBodyInsideUp(prospectCallLogFromDB, realTimeDelivery);
				}
			} catch (Exception e) {
				logger.error(
						"Error while sending real time lead delivery for prospectCallId - [{}] and exception is [{}]",
						prospectCallLogFromDB.getProspectCall().getProspectCallId(), e);
			}
		}
	}

	private RealTimeDelivery getRealTimeDelivery(String xtaasCampaignId) {
		return realTimeDeliveryRepository.findByXtaasCampaignId(xtaasCampaignId);
	}

	@SuppressWarnings("deprecation")
	private void prepareRequestBodyWheelhouse(ProspectCallLog prospectCallLogFromDB, RealTimeDelivery realTimeDelivery)
			throws Exception {
		String prospectCampaignID = prospectCallLogFromDB.getProspectCall().getCampaignId();

		logger.debug("Sending real time lead [{}] ", realTimeDelivery.toString());
		String postUrl = realTimeDelivery.getPostUrl();

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair(realTimeDelivery.getAuthCodeKey(), realTimeDelivery.getAuthCodeValue()));
		nameValuePairs.add(new BasicNameValuePair("source", realTimeDelivery.getSource()));
		nameValuePairs.add(new BasicNameValuePair("campaign_code", realTimeDelivery.getCampaignCode()));
		nameValuePairs.add(new BasicNameValuePair("asset_name", realTimeDelivery.getAssetName()));
		nameValuePairs.add(new BasicNameValuePair("category_name", realTimeDelivery.getCategoryName()));

		nameValuePairs.add(new BasicNameValuePair("success_email", realTimeDelivery.getSuccessMailId()));
		nameValuePairs.add(new BasicNameValuePair("error_email", realTimeDelivery.getErrorMailId()));
		nameValuePairs
				.add(new BasicNameValuePair("lead_id", prospectCallLogFromDB.getProspectCall().getProspectCallId()));

		if (realTimeDelivery.getTestMode() != null && !realTimeDelivery.getTestMode().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("is_test_mode", realTimeDelivery.getTestMode()));
		}

		String first_name = prospectCallLogFromDB.getProspectCall().getProspect().getFirstName();
		if (first_name != null && !first_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("first_name", first_name));
		}

		String last_name = prospectCallLogFromDB.getProspectCall().getProspect().getLastName();
		if (last_name != null && !last_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("last_name", last_name));
		}

		String phone = prospectCallLogFromDB.getProspectCall().getProspect().getPhone();
		if (phone != null && !phone.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("phone", phone));
		}

		String email = prospectCallLogFromDB.getProspectCall().getProspect().getEmail();
		if (email != null && !email.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("email", email));
		}

		String company_name = prospectCallLogFromDB.getProspectCall().getProspect().getCompany();
		if (company_name != null && !company_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("company_name", company_name));
		}

		String job_title = prospectCallLogFromDB.getProspectCall().getProspect().getTitle();
		if (job_title != null && !job_title.equals("")) {
			int maxJobTitleLength = (job_title.length() < 100) ? job_title.length() : 100;
			job_title = job_title.substring(0, maxJobTitleLength);
			nameValuePairs.add(new BasicNameValuePair("job_title", job_title));
		}

		String industry = mapXtaasRealTimeDeliveryIndustry(
				prospectCallLogFromDB.getProspectCall().getProspect().getIndustry());
		nameValuePairs.add(new BasicNameValuePair("industry", industry));

		String company_size = findCompanySize(prospectCallLogFromDB.getProspectCall().getProspect());
		nameValuePairs.add(new BasicNameValuePair("company_size", company_size));

		String addressLine1 = prospectCallLogFromDB.getProspectCall().getProspect().getAddressLine1();
		String addressLine2 = prospectCallLogFromDB.getProspectCall().getProspect().getAddressLine2();
		String address1 = "";
		if (addressLine1 != null && !addressLine1.equals("")) {
			address1 = address1 + addressLine1;
		} else if (addressLine2 != null && !addressLine2.equals("")) {
			address1 = address1 + addressLine2;
		}
		if (!address1.equals("")) {
			int maxAddressLength = (address1.length() < 80) ? address1.length() : 80;
			address1 = address1.substring(0, maxAddressLength);
			nameValuePairs.add(new BasicNameValuePair("address1", address1));
		}

		String city = prospectCallLogFromDB.getProspectCall().getProspect().getCity();
		if (city != null && !city.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("city", city));
		}

		String state = prospectCallLogFromDB.getProspectCall().getProspect().getStateCode();
		if (state != null && !state.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("state", state));
		}

		String postal_code = prospectCallLogFromDB.getProspectCall().getProspect().getZipCode();
		if (postal_code != null && !postal_code.equals("")) {
			if (postal_code.length() > 5) {
				postal_code = postal_code.substring(0, 5);
				nameValuePairs.add(new BasicNameValuePair("postal_code", postal_code));
			} else if (postal_code.length() == 5) {
				nameValuePairs.add(new BasicNameValuePair("postal_code", postal_code));
			}
		}

		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(postUrl);
			logger.debug("Real time post lead URL is: [{}] and Form Data Enitty is: {}", postUrl,
					nameValuePairs.toString());
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpClient.execute(httpPost);
			logger.debug("Real time post lead Response is: {} and Enitty is: {}", response,
					response.getEntity().toString());
			ProspectCallLog callLog = prospectCallLogRepository
					.findByProspectCallId(prospectCallLogFromDB.getProspectCall().getProspectCallId());
			callLog.getProspectCall().setLeadDelivered(true);
			prospectCallLogService.save(callLog, false);
			httpClient.close();
		} catch (Exception e) {
			logger.error("Error while sending Real time lead for campaign Id: [{}] and exception is {}",
					prospectCampaignID, e);
			e.printStackTrace();
		}
	}

	private String findCompanySize(Prospect prospect) {
		String companySize = "Unknown";
		Object minEmployeeCount = prospect.getCustomAttributeValue("minEmployeeCount");
		int minEmployeeCountInt = (Integer) minEmployeeCount;
		Object maxEmployeeCount = prospect.getCustomAttributeValue("maxEmployeeCount");
		int maxEmployeeCountInt = (Integer) maxEmployeeCount;

		/*
		 * if (minEmployeeCountInt != 0) { companySize =
		 * compareMinEmployeeCount(minEmployeeCountInt); } else
		 */if (maxEmployeeCountInt != 0) {
			companySize = compareMaxEmployeeCount(maxEmployeeCountInt);
		}
		return companySize;
	}

	private String compareMaxEmployeeCount(int maxEmployeeCountInt) {
		String companySize = "Unknown";
		if (maxEmployeeCountInt <= 3) {
			companySize = "1 to 3";
		} else if (maxEmployeeCountInt <= 10) {
			companySize = "4 to 10";
		} else if (maxEmployeeCountInt <= 25) {
			companySize = "11 to 25";
		} else if (maxEmployeeCountInt <= 50) {
			companySize = "26 to 50";
		} else if (maxEmployeeCountInt <= 100) {
			companySize = "51 to 100";
		} else if (maxEmployeeCountInt <= 200) {
			companySize = "101 to 200";
		} else if (maxEmployeeCountInt <= 500) {
			companySize = "201 to 500";
		} else if (maxEmployeeCountInt <= 1000) {
			companySize = "501 to 1000";
		} else {
			companySize = "1001 or more";
		}
		return companySize;
	}

	private String compareMinEmployeeCount(int minEmployeeCountInt) {
		String companySize = "Unknown";
		if (minEmployeeCountInt < 4) {
			companySize = "1 to 3";
		} else if (minEmployeeCountInt < 11) {
			companySize = "4 to 10";
		} else if (minEmployeeCountInt < 26) {
			companySize = "11 to 25";
		} else if (minEmployeeCountInt < 51) {
			companySize = "26 to 50";
		} else if (minEmployeeCountInt < 101) {
			companySize = "51 to 100";
		} else if (minEmployeeCountInt < 201) {
			companySize = "101 to 200";
		} else if (minEmployeeCountInt < 501) {
			companySize = "201 to 500";
		} else if (minEmployeeCountInt < 1001) {
			companySize = "501 to 1000";
		} else {
			companySize = "1001 or more";
		}
		return companySize;
	}

	private String mapXtaasRealTimeDeliveryIndustry(String xtaasIndustry) {
		String realTimeDeliveryIndustry = "Unknown";
		RealTimeDeliveryIndustryMapping realTimeDeliveryIndustryMapping = realTimeDeliveryIndustryMappingService
				.getRealTimeDeliveryIndustryMapping(xtaasIndustry);
		if (realTimeDeliveryIndustryMapping != null) {
			realTimeDeliveryIndustry = realTimeDeliveryIndustryMapping.getRealTimeDeliveryIndustry();
		}
		return realTimeDeliveryIndustry;
	}

	@SuppressWarnings("deprecation")
	private void prepareRequestBodyInsideUp(ProspectCallLog prospectCallLog, RealTimeDelivery realTimeDelivery)
			throws Exception {
		String prospectCampaignID = prospectCallLog.getProspectCall().getCampaignId();
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		Prospect prospect = prospectCallLog.getProspectCall().getProspect();

		String postUrl = realTimeDelivery.getPostUrl();

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("catId", realTimeDelivery.getCategoryId()));
		nameValuePairs.add(new BasicNameValuePair("iusrc", realTimeDelivery.getIusrc()));

		String first_name = prospectCallLog.getProspectCall().getProspect().getFirstName();
		if (first_name != null && !first_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("firstName", first_name));
		}

		String last_name = prospectCallLog.getProspectCall().getProspect().getLastName();
		if (last_name != null && !last_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("lastName", last_name));
		}

		String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
		if (phone != null && !phone.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("phoneNumber", phone));
		}

		String addressLine1 = prospectCallLog.getProspectCall().getProspect().getAddressLine1();
		String addressLine2 = prospectCallLog.getProspectCall().getProspect().getAddressLine2();
		String address = "";
		if (addressLine1 != null && !addressLine1.equals("")) {
			address = address + addressLine1 + " ";
		}
		if (addressLine2 != null && !addressLine2.equals("")) {
			address = address + addressLine2 + " ";
		}
		String city = prospectCallLog.getProspectCall().getProspect().getCity();
		if (city != null && !city.isEmpty()) {
			address = address + city + " ";
		}
		String state = prospectCallLog.getProspectCall().getProspect().getStateCode();
		if (state != null && !state.isEmpty()) {
			address = address + state;
		}
		nameValuePairs.add(new BasicNameValuePair("address", address));

		String email = prospectCallLog.getProspectCall().getProspect().getEmail();
		if (email != null && !email.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("email", email));
		}

		String postal_code = prospectCallLog.getProspectCall().getProspect().getZipCode();
		if (postal_code != null && !postal_code.equals("")) {
			if (postal_code.length() > 5) {
				postal_code = postal_code.substring(0, 5);
				nameValuePairs.add(new BasicNameValuePair("zip", postal_code));
			} else if (postal_code.length() == 5) {
				nameValuePairs.add(new BasicNameValuePair("zip", postal_code));
			}
		}

		String company_name = prospectCallLog.getProspectCall().getProspect().getCompany();
		if (company_name != null && !company_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("company", company_name));
		}

		String title = prospectCallLog.getProspectCall().getProspect().getTitle();
		ClientMapping clientTitleMapping = clientMappingRepository
				.findByClientNameAndByKey(XtaasConstants.INSIDEUP_CLIENT, title);
		if (clientTitleMapping != null) {
			nameValuePairs.add(new BasicNameValuePair("title", clientTitleMapping.getValue()));
		} else {
			ClientMapping clientDefaultTitleMapping = clientMappingRepository
					.findByCampaignIdAndByKey(prospectCampaignID, "defaultTitle");
			if (clientDefaultTitleMapping != null) {
				nameValuePairs.add(new BasicNameValuePair("title", clientDefaultTitleMapping.getValue()));
			}
		}
		// TODO Sending recording file name which is sending manually
		String fileName = prospectCallLog.getProspectCall().getProspect().getFirstName() + "_"
				+ prospectCallLog.getProspectCall().getProspect().getLastName() + "_"
				+ prospectCallLog.getProspectCall().getProspect().getSourceId() + ".wav";

		ClientMapping clientDefaultRecordingMapping = clientMappingRepository
				.findByCampaignIdAndByKey(prospectCampaignID, "recordingName");
		if (clientDefaultRecordingMapping != null ) {
			nameValuePairs.add(new BasicNameValuePair("recording_name", fileName));
		}

		String industry = prospectCallLog.getProspectCall().getProspect().getIndustry();
		ClientMapping clientIndustryMapping = clientMappingRepository
				.findByClientNameAndByKey(XtaasConstants.INSIDEUP_CLIENT, industry.trim());
		if (clientIndustryMapping != null) {
			nameValuePairs.add(new BasicNameValuePair("industry", clientIndustryMapping.getValue()));
		} else {
			ClientMapping clientDefaultIndustryMapping = clientMappingRepository
					.findByCampaignIdAndByKey(prospectCampaignID, "defaultIndustry");
			if (clientDefaultIndustryMapping != null) {
				nameValuePairs.add(new BasicNameValuePair("industry", clientDefaultIndustryMapping.getValue()));
			}
		}

		Object maxEmployeeCount = prospect.getCustomAttributeValue("maxEmployeeCount");
		int maxEmployeeCountInt = (Integer) maxEmployeeCount;
		String employeeSize = InsideUpEmployeeMapping(maxEmployeeCountInt);
		nameValuePairs.add(new BasicNameValuePair("employee", employeeSize));

		try {
			LeadformAttributes leadformAttributes = getInsideUpCategoryAttributes(realTimeDelivery.getCategoryId());
			logger.debug("LeadformAttributes : " + leadformAttributes.toString());

			if (leadformAttributes.getAttributes() != null && leadformAttributes.getAttributes().size() > 0) {
				for (int i = 0; i < leadformAttributes.getAttributes().size(); i++) {
					Attribute attribute = leadformAttributes.getAttributes().get(i);
					ClientMapping clientQuestionMapping = clientMappingRepository
							.findByCampaignIdAndByValue(prospectCampaignID, attribute.getAttributeName().trim());
					String questionSequenceNumber = "Q";
					int queNumber = i + 1;
					questionSequenceNumber = questionSequenceNumber + queNumber;
					String answerId = "";
					if (clientQuestionMapping != null && prospectCall.getAnswers() != null
							&& prospectCall.getAnswers().size() > 0) {
						for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
							if (agentQaAnswer.getQuestion().equalsIgnoreCase(clientQuestionMapping.getKey())) {
								ClientMapping clientAnswerMapping = clientMappingRepository
										.findByCampaignIdAndByKey(prospectCampaignID, agentQaAnswer.getAnswer().trim());
								if (clientAnswerMapping != null) {
									answerId = clientAnswerMapping.getValue();
									nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber, answerId));
								}
							}
						}
					} else {
						ClientMapping clientQuestionDefaultMapping = clientMappingRepository
								.findByCampaignIdAndByKey(prospectCampaignID, attribute.getAttributeName().trim());
						if (clientQuestionDefaultMapping != null) {
							nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber,
									clientQuestionDefaultMapping.getValue()));
						} else {
							nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber, "2040"));
						}
					}
				}
			}

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(postUrl);
			logger.debug("Real time post lead URL - [{}] with reuquest data - [{}]", postUrl, nameValuePairs.toString());
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			if (responseString.contains("Following error has occured")) {
				String message = "Lead delivery failed for prospectCallId [" + prospectCall.getProspectCallId()
						+ "]. \nData sent [" + nameValuePairs.toString() + "]. \n" + responseString;
				XtaasEmailUtils.sendEmail(realTimeDelivery.getErrorMailId(), realTimeDelivery.getSuccessMailId(),
						"Lead delivery to INSIDEUP failed", message);
				logger.debug("Real time post failed for proapectCallId [{}] due to - [{}]",
						prospectCall.getProspectCallId(), responseString);
			} else {
				String message = "prospectCallId [" + prospectCall.getProspectCallId()
						+ "] sent to INSIDEUP. \nData sent [" + nameValuePairs.toString() + "]";
				XtaasEmailUtils.sendEmail(realTimeDelivery.getSuccessMailId(), realTimeDelivery.getSuccessMailId(),
						"Sent lead to INSIDEUP", message);
				logger.debug("Lead with prospectCallId [{}] sent to [{}] client.", prospectCall.getProspectCallId(),
						XtaasConstants.INSIDEUP_CLIENT);
				ProspectCallLog callLog = prospectCallLogRepository
						.findByProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
				callLog.getProspectCall().setLeadDelivered(true);
				prospectCallLogService.save(callLog, false);
			}
			httpClient.close();
		} catch (Exception e) {
			String message = "Lead delivery failed for prospectCallId [" + prospectCall.getProspectCallId()
					+ "]. \nData sent [" + nameValuePairs.toString() + "].";
			XtaasEmailUtils.sendEmail(realTimeDelivery.getErrorMailId(), realTimeDelivery.getSuccessMailId(),
					"Lead delivery to INSIDEUP failed", message);
			logger.debug("Real time post failed for proapectCallId [{}].", prospectCall.getProspectCallId());
			logger.error("Error while sending Real time lead for campaign Id: [{}] and exception is {}",
					prospectCampaignID, e);
		}
	}

	private String InsideUpEmployeeMapping(int maxEmployeeCount) {
		String employeeCountCode = "2009";
		if (maxEmployeeCount <= 9) {
			employeeCountCode = "2009";
		} else if (maxEmployeeCount <= 19) {
			employeeCountCode = "2010";
		} else if (maxEmployeeCount <= 49) {
			employeeCountCode = "2011";
		} else if (maxEmployeeCount <= 99) {
			employeeCountCode = "2012";
		} else if (maxEmployeeCount <= 499) {
			employeeCountCode = "2013";
		} else if (maxEmployeeCount <= 999) {
			employeeCountCode = "2014";
		} else {
			employeeCountCode = "2015";
		}
		return employeeCountCode;
	}

	private LeadformAttributes getInsideUpCategoryAttributes(String categoryId) {
		String xmlResponseString = "";
		LeadformAttributes leadformAttributes = new LeadformAttributes();
		String getCategoryAttributesUrl = "http://www.insideup.com/categoryattributes.html?catId=" + categoryId;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(getCategoryAttributesUrl);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			xmlResponseString = EntityUtils.toString(entity, "UTF-8");
			leadformAttributes = XMLConverterUtil.convertXMLToLeadformAttributes(xmlResponseString);
		} catch (Exception e) {
			logger.error("Error fetching category attributes from InsideUp. Exception is : [{}]", e);
		}
		httpClient.close();
		return leadformAttributes;
	}

}
