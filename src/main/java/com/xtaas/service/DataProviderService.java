package com.xtaas.service;

import java.util.List;
import java.util.Map;

public interface DataProviderService {

	List<String> getXtaasDataProvider();
	
	Map<String,String> getXtaasDataProviderByIndex();
}
