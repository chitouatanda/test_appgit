package com.xtaas.service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.valueobjects.UserPrincipal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/10/14
 * Time: 1:19 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value="xtaasUserDetailsServiceImpl")
public class XtaasUserDetailsServiceImpl implements UserDetailsService {
	private static final Logger log = LoggerFactory.getLogger(XtaasUserDetailsServiceImpl.class);
	
	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Login login = loginRepository.findOneById(username.toLowerCase());
		log.debug(username+" Login:"+login);
		if(login == null) throw new UsernameNotFoundException(username+" not found");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(login.getRoles()==null? 0 : login.getRoles().size());
		if(login.getRoles() != null) for(String role : login.getRoles()) authorities.add(new SimpleGrantedAuthority("ROLE_"+role.toUpperCase()));
		
		Organization org = organizationRepository.findOneById(login.getOrganization());
		//return new User(username, login.getPassword(), authorities);
		// HACK - Added because Spring MVC 5 doesn't support MD5 PasswordEncoding
		login.setPassword(login.getPassword() + " | " + login.getUsername());
		return new UserPrincipal(login, org, authorities);
	}
}
