package com.xtaas.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.application.service.AgentConferenceService;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.application.service.SignalwireOutboundNumberService;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.AgentConference;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.entity.SignalwireOutboundNumber;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.pusher.Pusher;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.CountryWiseDetailsDTO;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;
import com.xtaas.web.dto.SignalwireEndpointRequestDTO;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class SignalwireServiceImpl implements SignalwireService {

	private final Logger logger = LoggerFactory.getLogger(SignalwireServiceImpl.class);
	private final static String endpointUrl = "https://xtaascorp.signalwire.com/api/relay/rest/endpoints/sip";
	private final static String signalWireBaseUrl = "https://xtaascorp.signalwire.com/api/laml/2010-04-01/Accounts/c49a511a-2d7a-4816-b139-302ae7d7c74d/";

	@Autowired
	HttpService httpService;

	@Autowired
	AgentConferenceService agentConferenceService;

	private ConcurrentHashMap<String, String> signalwireCallSidAgentIdNotKickedMap = new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, String> signalwireCallSidExtensionMap = new ConcurrentHashMap<>();

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private DialerService dialerService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private CallLogService callLogService;

	@Autowired
	private ProspectCallLogService prospectLogService;

	@Autowired
	private SignalwireOutboundNumberService signalwireOutboundNumberService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Autowired
	private TranscriptionInterpretationService interpretationService;

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "*/*");
		headers.add("content-type", "application/json");
		// headers.add("Authorization",
		// "Basic
		// YzQ5YTUxMWEtMmQ3YS00ODE2LWIxMzktMzAyYWU3ZDdjNzRkOlBUMTc2ZGE5Y2ZiMGRlYTk0ZGRlYTU0Njg1OWYyZjJlYjVlY2U4ZjYwN2Q3OTNiMThm");
		headers.add("Authorization", "Basic " + getBase64EncodedAuthorizationString());
		return headers;
	}

	private String getBase64EncodedAuthorizationString() {
		String signalwireProjectId = ApplicationEnvironmentPropertyUtils.getSignalwireProjectId();
		String signalwireAuthToken = ApplicationEnvironmentPropertyUtils.getSignalwireAuthToken();
		String encodedString = Base64.getEncoder()
				.encodeToString((signalwireProjectId + ":" + signalwireAuthToken).getBytes());
		return encodedString;
	}

	@Override
	public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("SignalwireServiceImpl request to handle voice call.");
		Map<String, String> requestMap = new HashMap<String, String>();

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String[] paramValues = request.getParameterValues(paramName);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < paramValues.length; i++) {
				sb.append(paramValues[i]);
			}
			String str = sb.toString();
			requestMap.put(paramName, str);
		}

		logger.info("Signalwire_POC requestMap : " + requestMap.toString());
		String xmlString = "";
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String dialerUrl = ApplicationEnvironmentPropertyUtils.getDialerUrl();
		if (requestMap.containsKey("CallStatus") && requestMap.get("CallStatus").equalsIgnoreCase("failed")) {
			return xmlString;
		} else if (requestMap.containsKey("transcription")) {
			String transcription = requestMap.get("transcription");
			if (transcription != null && !transcription.isEmpty()) {
				// transcription = "Please dial zero to talk to the operator";
				String dtmfValue = interpretationService.getInterpretationCommand(transcription);
				// xmlString = "<Response><Stop><Stream name=\"mystream\"/></Stop>" + "<Play digits=\"0\"></Play>"
				// 		+ "<Dial><Conference startConferenceOnEnter=\"false\" endConferenceOnExit=\"false\" beep=\"onExit\">agent1</Conference></Dial>"
				// 		+ "</Response>";
				String callSid = requestMap.get("CallSid");
				if (dtmfValue == null || (dtmfValue != null && dtmfValue.isEmpty())) {
					logger.info("SignalwireServiceImpl ===> dtmf value is null or empty");
					//Hangup call
					xmlString = getHangupString(callSid, true);
				} else if (dtmfValue != null && dtmfValue.equalsIgnoreCase("extension")) {
					String extension = getSignalwireExtensionFromCallSid(callSid);
					if (extension != null && !extension.isEmpty()) {
						xmlString = addProspectToConference(requestMap, extension, true);
					} else {
						xmlString = getHangupString(callSid, true);
					}
				} else if (dtmfValue != null && dtmfValue.equalsIgnoreCase("hold")) {
					xmlString = addProspectToConference(requestMap, "", true);
				} else if (dtmfValue != null && !dtmfValue.isEmpty()) {
					xmlString = addProspectToConference(requestMap, dtmfValue, true);
				} else {
					xmlString = getHangupString(callSid, true);
				}

				// xmlString = "<Response>"
				// + "<Dial><Conference startConferenceOnEnter=\"false\"
				// endConferenceOnExit=\"false\" beep=\"onExit\">agent1</Conference></Dial>"
				// + "</Response>";
				logger.info("Signalwire_POC : xmlString for Stream transcription => " + xmlString);
			}
		} else if (requestMap.containsKey("SpeechResult")) {
			String speechText = requestMap.get("SpeechResult");
			if (speechText.contains("0") || speechText.contains("zero")) {
				xmlString = "<Response>" + "<Play digits=\"0\"></Play>"
						+ "<Dial><Conference startConferenceOnEnter=\"false\" endConferenceOnExit=\"false\" beep=\"onExit\">agent1</Conference></Dial>"
						+ "</Response>";
				// xmlString = "<Response>"
				// + "<Dial><Conference startConferenceOnEnter=\"false\"
				// endConferenceOnExit=\"false\" beep=\"onExit\">agent1</Conference></Dial>"
				// + "</Response>";
				logger.info("Signalwire_POC : xmlString for SpeechResult => " + xmlString);
			}
		} else if (requestMap.containsKey("StatusCallbackEvent")
				&& requestMap.get("StatusCallbackEvent").equalsIgnoreCase("participant-join")
				&& requestMap.get("EndConferenceOnExit").equalsIgnoreCase("true")) {
			String agentId = requestMap.get("FriendlyName");
			String conferenceId = requestMap.get("ConferenceSid");
			String campaignId = requestMap.get("campaignId");
			if (campaignId != null && !campaignId.isEmpty()) {
				logger.info("Signalwire conference statuscallbackevent CampaignId present: " + campaignId);
			} else {
				logger.info("Signalwire conference statuscallbackevent CampaignId absent ");
			}
			registerAgent(campaignId, agentId);
			saveAgentConferenceDetails(agentId, conferenceId, "", "");
		} else if (requestMap.containsKey("StatusCallbackEvent")
				&& requestMap.get("StatusCallbackEvent").equalsIgnoreCase("participant-join")) {
			String agentId = requestMap.get("FriendlyName");
			String conferenceId = requestMap.get("ConferenceSid");
			// saveAgentConferenceDetails(agentId, conferenceId, "", "");
			logger.info("Signalwire_POC : Conference ended => Agent: " + agentId + " ConferenceId: " + conferenceId);
		} else if (requestMap.containsKey("CallStatus") && requestMap.get("CallStatus").equalsIgnoreCase("answered")) {
			// xmlString = "<Response>" + "<Pause length=\"1\"/>"
			// + "</Response>";

			xmlString = "<Response>"
					+ "<Dial><Conference startConferenceOnEnter=\"false\" endConferenceOnExit=\"false\" beep=\"onExit\">agent1</Conference></Dial>"
					+ "</Response>";
		} else if (requestMap.containsKey("CallStatus") && requestMap.get("CallStatus").equalsIgnoreCase("initiated")) {
			// Gather command
			// xmlString = "<Response>" + "<Gather input=\"speech\" speechTimeout=\"5\"
			// action=\"" + serverUrl
			// + "/spr/signalwire/voice\" />" + "</Response>";
			// xmlString = "<Response>" + "<Gather input=\"speech\" speechTimeout=\"10\"
			// action=\"" + "https://31201ba7b0e1.ngrok.io"
			// + "/spr/signalwire/voice\" />" + "</Response>";

			// xmlString = "<Response>" + "<Start><Stream url=\"" + dialerUrl +
			// "/app/audiostream\" /></Start>" + "</Response>";

			// Stream command
			logger.info("Signalwire_POC : Stream started");
			String callSid = requestMap.get("CallSid");
			if (requestMap.containsKey("autoIVR")) {
				xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response>" + "<Start><Stream name=\"" + callSid + "\" url=\""
				+ dialerUrl.replaceFirst("https", "wss") + "/audiostream\" /></Start><Pause length=\"20\"/>"
				+ "</Response>";
				if (requestMap.containsKey("defaultExtension")) {
					String extensionAuto = requestMap.get("defaultExtension");
					if (extensionAuto != null && !extensionAuto.isEmpty()) {
						putSignalwireExtensionInMap(callSid, extensionAuto);
					}
				}
			} else {
				//join conference
				xmlString = addProspectToConference(requestMap, "", false);
			}			
		} else if (requestMap.containsKey("SipUser")) {
			String sipUser = requestMap.get("SipUser");
			if (sipUser.contains("--")) {
				// Note: Please check sequence with agentcontroller.js ===> agentId--campaignId
				String[] arrSplit = sipUser.split("--");
				String agentId = arrSplit[0];
				String campaignId = arrSplit[1];
				xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Dial><Conference startConferenceOnEnter=\"true\" endConferenceOnExit=\"true\" waitUrl=\"https://xtaascorp.signalwire.com/laml-bins/141465c9-0a96-4726-9899-06c95b1852fd\" statusCallbackEvent=\"start join end\" statusCallback=\""
						+ serverUrl + "/spr/signalwire/voice?campaignId=" + campaignId + "\" >" + agentId
						+ "</Conference></Dial></Response>";
				logger.info("Signalwire_POC : xmlString for Initiated agentcall => " + xmlString);
			} else if (sipUser.contains("==")) {
				// Note: Please check sequence with agentcontroller.js ===> agentId==campaignId==pcid==toNumber==fromNumber
				String[] arrSplit = sipUser.split("==");
				String agentId = arrSplit[0];
				String campaignId = arrSplit[1];
				String pcid = arrSplit[2];
				String toNumber = "+" + arrSplit[3];
				String fromNumber = "+" + arrSplit[4];
				Campaign campaign = campaignService.getCampaign(campaignId);
				
				xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Dial callerId=\"" + fromNumber
						+ "\" timeout=\"21\" record=\"record-from-answer\" recordingStatusCallbackEvent=\"completed\" recordingStatusCallback=\""
						+ serverUrl + "/spr/signalwire/recordingstatus?pcid=" + pcid + "\">";
				if (campaign != null && campaign.getSipProvider().equalsIgnoreCase("Telnyx")) {
					String telnyxDomainUrl = ApplicationEnvironmentPropertyUtils.getTelnyxDomainUrl();
					String signalwireTelnyxToken = ApplicationEnvironmentPropertyUtils.getSignalwireTelnyxToken();
					StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@")
							.append(telnyxDomainUrl).append("?X-Telnyx-Token=").append(signalwireTelnyxToken);
					xmlString += "<Sip statusCallbackEvent=\"completed\" statusCallback=\"" + serverUrl
							+ "/spr/signalwire/voicestatus?dialmode=manual&amp;agentId=" + agentId + "\" >" + telnyxNumber.toString() + "</Sip>";
				} else if (campaign != null && campaign.getSipProvider().equalsIgnoreCase("Plivo")) {
					String plivoSIPUsername = ApplicationEnvironmentPropertyUtils.getPlivoSIPUsername();
					String plivoSIPPassword = ApplicationEnvironmentPropertyUtils.getPlivoSIPPassword();
					String plivoSIPDomain = ApplicationEnvironmentPropertyUtils.getPlivoSIPDomain();
					StringBuilder plivoSIPNumber = new StringBuilder("sip:").append(toNumber).append("@").append(plivoSIPDomain);
					xmlString += "<Sip username=\"" + plivoSIPUsername +"\" password=\"" + plivoSIPPassword + "\" statusCallbackEvent=\"completed\" statusCallback=\"" + serverUrl
					+ "/spr/signalwire/voicestatus?dialmode=manual&amp;agentId=" + agentId + "\" >" + plivoSIPNumber.toString() + "</Sip>";
				} else if (campaign != null && campaign.getSipProvider().equalsIgnoreCase("Viva")) {
					String vivaSIPAccountId = ApplicationEnvironmentPropertyUtils.getSignalwireVivaSIPAccountId();
					String vivaSIPPassword = ApplicationEnvironmentPropertyUtils.getSignalwireVivaSIPPassword();
					String vivaSIPDomain = ApplicationEnvironmentPropertyUtils.getSignalwireVivaSIPDomain();
					StringBuilder vivaSIPNumber = new StringBuilder("sip:").append(toNumber).append("@").append(vivaSIPDomain).append(";transport=udp");
					xmlString += "<Sip username=\"" + vivaSIPAccountId +"\" password=\"" + vivaSIPPassword + "\" statusCallbackEvent=\"completed\" statusCallback=\"" + serverUrl
					+ "/spr/signalwire/voicestatus?dialmode=manual&amp;agentId=" + agentId + "\" >" + vivaSIPNumber.toString() + "</Sip>";
				} else if (campaign != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
					String thinQId = ApplicationEnvironmentPropertyUtils.getThinQId();
					String thinQToken = ApplicationEnvironmentPropertyUtils.getSignalwireThinQSipToken();
					String thinQSIPDomain = ApplicationEnvironmentPropertyUtils.getThinQDomainUrl();
					StringBuilder thinQSIPNumber = new StringBuilder("sip:").append(toNumber).append("@").append(thinQSIPDomain).append(";transport=udp").append("?X-account-id=").append(thinQId).append("&amp;X-account-token=").append(thinQToken);
					xmlString += "<Sip username=\"" + "user" +"\" password=\"" + "pass" + "\" statusCallbackEvent=\"completed\" statusCallback=\"" + serverUrl
					+ "/spr/signalwire/voicestatus?dialmode=manual&amp;agentId=" + agentId + "\" >" + thinQSIPNumber.toString() + "</Sip>";
				} else {
					xmlString += "<Number statusCallbackEvent=\"completed\" statusCallback=\"" + serverUrl
							+ "/spr/signalwire/voicestatus?dialmode=manual&amp;agentId=" + agentId + "\" >" + toNumber + "</Number>";
				}
				xmlString += "</Dial></Response>";
				logger.info("Signalwire_POC : xmlString for Initiated manual call => " + xmlString + " \n callsid :"
						+ requestMap.get("CallSid"));
				pushMessageToAgent(agentId, XtaasConstants.SIGNALWIRE_MANUAL_CALLSID,
				requestMap.get("CallSid"));
			}
		}
		return xmlString;
	}

	@Override
	public void handleVoiceStatusCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("SignalwireServiceImpl request to handle voice statuscallback.");
		Map<String, String> requestMap = new HashMap<String, String>();

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String[] paramValues = request.getParameterValues(paramName);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < paramValues.length; i++) {
				sb.append(paramValues[i]);
			}
			String str = sb.toString();
			requestMap.put(paramName, str);
		}
		String callSid = requestMap.get("CallSid");
		logger.info("Signalwire_POC SignalwireServiceImpl=>handleVoiceStatusCallback() requestMap for callstatus: " + requestMap.toString());
		if (callSid != null && !callSid.isEmpty()) {
			dialerService.removeSignalwireAnsweredCallSidFromDialer(callSid);
			removeSignalwireCallSidFromExtensionMap(callSid);
		}
		if (requestMap.containsKey("CallStatus") && (requestMap.get("CallStatus").equalsIgnoreCase("busy")
				|| requestMap.get("CallStatus").equalsIgnoreCase("canceled")
				|| requestMap.get("CallStatus").equalsIgnoreCase("failed")
				|| requestMap.get("CallStatus").equalsIgnoreCase("no-answer"))) {
			// dialerService.removeSignalwireAnsweredCallSidFromDialer(callSid);
			if (requestMap.get("CallStatus").equalsIgnoreCase("busy")) {
				prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_BUSY, callSid, 0, null);
				if (requestMap.containsKey("dialmode") && requestMap.get("dialmode").equalsIgnoreCase("manual") && requestMap.containsKey("agentId")) {
					pushMessageToAgent(requestMap.get("agentId"), XtaasConstants.CALL_DISCONNECCT_EVENT, "no-answer");
				}
			} else if (requestMap.get("CallStatus").equalsIgnoreCase("canceled")) {
				prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_CANCELED, callSid, 0, null);
			} else if (requestMap.get("CallStatus").equalsIgnoreCase("no-answer")) {
				prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_NO_ANSWER, callSid, 0, null);
				if (requestMap.containsKey("dialmode") && requestMap.get("dialmode").equalsIgnoreCase("manual") && requestMap.containsKey("agentId")) {
					pushMessageToAgent(requestMap.get("agentId"), XtaasConstants.CALL_DISCONNECCT_EVENT, "no-answer");	
				}
			} else {
				prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_FAILED, callSid, 0, null);
				if (requestMap.containsKey("dialmode") && requestMap.get("dialmode").equalsIgnoreCase("manual") && requestMap.containsKey("agentId")) {
					pushMessageToAgent(requestMap.get("agentId"), XtaasConstants.CALL_DISCONNECCT_EVENT, "Call failed.");
				}
			}
		}

		if (requestMap.containsKey("CallStatus") && requestMap.get("CallStatus").equalsIgnoreCase("completed")) {
			// check if call hanged up by Prospect and disconnect
			// dialerService.removeSignalwireAnsweredCallSidFromDialer(callSid);
			if (requestMap.containsKey("dialmode") && requestMap.get("dialmode").equalsIgnoreCase("manual") && requestMap.containsKey("agentId")) {
				pushMessageToAgent(requestMap.get("agentId"), XtaasConstants.CALL_DISCONNECCT_EVENT, "");	
			} else {
				checkAndDisconnectCallFromAgentScreen(callSid);
			}
		}
	}

	@Override
	public Map<String, String> createEndpointForAgent(String username) {
		Map<String, String> signalwireEndpointCredentials = new HashMap<>();
		String randomPassword = RandomStringUtils.random(10, true, true);
		try {
			String signalwireDomainApp = ApplicationEnvironmentPropertyUtils.getSignalwireDomainApp();
			SignalwireEndpointRequestDTO data = new SignalwireEndpointRequestDTO();
			data.setUsername(username + randomPassword);
			data.setPassword(randomPassword);
			// data.setOutbound(outbound);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> response = httpService.post(endpointUrl, headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, mapper.writeValueAsString(data));
			if (response.containsKey("username")) {
				// Map<String, Object> connectiondetails = (HashMap<String, Object>)
				// response.get("data");
				signalwireEndpointCredentials.put(XtaasConstants.SIGNALWIRE_ENDPOINT_USERNAME,
						response.get("username").toString());
				signalwireEndpointCredentials.put(XtaasConstants.SIGNALWIRE_ENDPOINT_PASSWORD, randomPassword);
				signalwireEndpointCredentials.put(XtaasConstants.SIGNALWIRE_ENDPOINT_ID, response.get("id").toString());
				signalwireEndpointCredentials.put(XtaasConstants.SIGNALWIRE_DOMAINAPP, signalwireDomainApp);
				logger.debug("Endpoint [{}] created for [{}] agent.", response.get("id"), response.get("username"));
			}

		} catch (IOException e) {
			logger.error("==========> SignalwireController - Error occurred in createConnectionForAgent <==========");
			logger.error("Expection : {} ", e.getMessage());
			throw new IllegalArgumentException("An error occurred while creating Signalwire Endpoint.");
		}
		return signalwireEndpointCredentials;
	}

	@Override
	public void deleteEndpoint(String endpointId) {
		Map<String, Object> response = httpService.delete(endpointUrl + "/" + endpointId, headers(),
				new ParameterizedTypeReference<Map<String, Object>>() {
				});
	}

	private String addProspectToConference(Map<String, String> requestMap, String dtmfString, boolean stopStream) {
		// Join conference
		String xmlString = "";
		AgentCall allocatedAgent = null;
		String prospectCallAsJson = null;
		ProspectCall prospectCall = null;
		String callSid = requestMap.get("CallSid");
		String prospectCallId = requestMap.get("pcid");
		String callerId = requestMap.get("From");

		logger.debug(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Finding available agent")
				.prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId).build());
		allocatedAgent = agentRegistrationService.allocateAgent(callSid);
		if (allocatedAgent != null) {
			prospectCall = allocatedAgent.getProspectCall();
			String campaignInfoId = prospectCall.getCampaignId();
			Campaign campInfo = campaignService.getCampaign(campaignInfoId);
			try {
				// hideEmailAndPhone(prospectCall, campInfo);
				prospectCallAsJson = JSONUtils.toJson(prospectCall);
				pushMessageToAgent(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
				prospectCallAsJson);
			} catch (IOException e) {
				logger.error(new SplunkLoggingUtils("AutoCall", callSid)
						.eventDescription("Error occurred in addProspectToConference")
						.methodName("addProspectToConference").processName("AutoCall").prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
			}
			logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Allocated Agent to Prospect")
					.methodName("handleDialerProspectCall").processName("AutoCall")
					.agentId(allocatedAgent.getAgentId()).prospectCallId(prospectCallId)
					.callSid(callSid).outboundNumber(callerId).build());

			xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response>";
			if (stopStream) {
				xmlString += "<Stop><Stream name=\"" + callSid + "\"/></Stop>";
			}
			boolean first = true;
			if (dtmfString != null && !dtmfString.isEmpty()) {
				String finalDtmfString = "";
				for ( char i : dtmfString.toCharArray()) {
					if (String.valueOf(i).equalsIgnoreCase(".")) {
						break;
					}
					if (first) {
						first = false;
					} else {
						finalDtmfString += "w";
					}
					finalDtmfString += i;
				}
				xmlString += "<Play digits=\"" + finalDtmfString + "\"></Play>";
			}
			xmlString += "<Dial><Conference startConferenceOnEnter=\"false\" endConferenceOnExit=\"false\" beep=\"onExit\">"
					+ allocatedAgent.getAgentId() + "</Conference></Dial>" + "</Response>";
			signalwireCallSidAgentIdNotKickedMap.put(callSid, allocatedAgent.getAgentId());
			logger.info("signalwireCallSidAgentIdNotKickedMap added callSid : " + callSid + " and AgentId: "
					+ allocatedAgent.getAgentId());
			System.out.println("Signalwire_POC : xmlString for Initiated prospectcall=> " + xmlString);
		} else {
			try {
				// wait for a second and attempt again to find an agent
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error(new SplunkLoggingUtils("AutoCall", callSid)
						.eventDescription("Error occurred in addProspectToConference")
						.methodName("addProspectToConference").processName("AutoCall").prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
			}
			allocatedAgent = agentRegistrationService.allocateAgent(callSid);
			if (allocatedAgent != null) {
				prospectCall = allocatedAgent.getProspectCall();
				String campaignInfoId = prospectCall.getCampaignId();
				Campaign campInfo = campaignService.getCampaign(campaignInfoId);
				try {
					// hideEmailAndPhone(prospectCall, campInfo);
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					pushMessageToAgent(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
							prospectCallAsJson);
				} catch (IOException e) {
					logger.error(new SplunkLoggingUtils("AutoCall", callSid)
							.eventDescription("Error occurred in addProspectToConference")
							.methodName("addProspectToConference").processName("AutoCall").prospectCallId(prospectCallId)
							.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
				}
				logger.info("==========> Allocated Agent [{}] to ProspectCallId [{}]. <==========",
						allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
				
				xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response>";
				if (stopStream) {
					xmlString += "<Stop><Stream name=\"" + callSid + "\"/></Stop>";
				}
				if (dtmfString != null && !dtmfString.isEmpty()) {
					xmlString += "<Play digits=\"" + dtmfString + "\"></Play>";
				}
				logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("Allocated Agent to Prospect")
						.methodName("handleDialerProspectCall").processName("AutoCall")
						.agentId(allocatedAgent.getAgentId()).prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).build());

				xmlString += "<Dial><Conference startConferenceOnEnter=\"false\" endConferenceOnExit=\"false\" beep=\"onExit\">"
						+ allocatedAgent.getAgentId() + "</Conference></Dial>" + "</Response>";
				signalwireCallSidAgentIdNotKickedMap.put(callSid, allocatedAgent.getAgentId());
				logger.info("signalwireCallSidAgentIdNotKickedMap added callSid : " + callSid + " and AgentId: "
						+ allocatedAgent.getAgentId());
				System.out.println("Signalwire_POC : xmlString for Initiated prospectcall=> " + xmlString);
			} else {
				prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_ABANDONED, callSid, null, null);
				logger.info(new SplunkLoggingUtils("AutoCall", callSid).eventDescription("No Agent available")
						.methodName("handleDialerProspectCall").processName("AutoCall")
						.prospectCallId(prospectCallId).callSid(callSid).outboundNumber(callerId).build());
			}

			try {
				dialerService.removeSignalwireAnsweredCallSidFromDialer(callSid);
			} catch (Exception e) {
				logger.error(new SplunkLoggingUtils("AutoCall", callSid)
						.eventDescription("Error occurred in addProspectToConference")
						.methodName("addProspectToConference").processName("AutoCall").prospectCallId(prospectCallId)
						.callSid(callSid).outboundNumber(callerId).addThrowableWithStacktrace(e).build());
			}
		}
		return xmlString;
	}

	@Override
	public void kickParticipantFromConference(String agentId, String callSid, boolean isConferenceCall) {
		logger.info("***** Conference name ***** [{}]", agentId);
		logger.info("***** Is Conference call ***** [{}]", isConferenceCall);

		AgentConference agentConference = agentConferenceService.findConferenceIdByAgentId(agentId);
		String conferenceSid = agentConference.getConferenceId();

		String kickParticipantUrl = signalWireBaseUrl + "Conferences/" + conferenceSid + "/Participants/" + callSid;

		Map<String, Object> kickParticipantResponse = null;

		kickParticipantResponse = (Map<String, Object>) httpService.delete(kickParticipantUrl, headers(),
				new ParameterizedTypeReference<Map<String, Object>>() {
				});
		logger.info("Signalwire Kick participant [{}] from conference [{}].", callSid, conferenceSid);
		signalwireCallSidAgentIdNotKickedMap.remove(callSid);
	}

	private void saveAgentConferenceDetails(String agentId, String conferenceId, String callControlId,
			String callLegId) {
		AgentConference agentConference = new AgentConference(agentId, conferenceId, callControlId, callLegId);
		agentConferenceService.saveAgentConference(agentConference);
	}

	private void checkAndDisconnectCallFromAgentScreen(String callSid) {
		if (signalwireCallSidAgentIdNotKickedMap.containsKey(callSid)) {
			String agentId = signalwireCallSidAgentIdNotKickedMap.get(callSid);
			pushMessageToAgent(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
			logger.info(
					"signalwireCallSidAgentIdNotKickedMap contains callSid : " + callSid + " and AgentId: " + agentId);
			signalwireCallSidAgentIdNotKickedMap.remove(callSid);
		} else {
			logger.info("signalwireCallSidAgentIdNotKickedMap doesnot contain callSid : " + callSid);
		}
	}

	private void pushMessageToAgent(String agentId, String event, String message) {
		logger.debug("pushMessageToAgent() : Event = [{}], Message = [{}], Agent = " + agentId, event, message);
		try {
			Pusher.triggerPush(agentId, event, message);
		} catch (IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing message to agent: " + agentId + " Msg:"
					+ message, e);
		}
	}

	private void registerAgent(String campaignId, String agentId) {
		AgentCampaignDTO agentCampaign = agentService.getAgentCampaign(campaignId);
		agentRegistrationService.manageAgent(agentId, "AUTO", agentCampaign.getDialerMode(), AgentStatus.ONLINE,
				agentCampaign);
	}

	@Override
	public void handleRecordingCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.info("Signalwire request to handle recording statuscallback.");
		HashMap<String, String> requestMap = new HashMap<String, String>();

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String[] paramValues = request.getParameterValues(paramName);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < paramValues.length; i++) {
				sb.append(paramValues[i]);
			}
			String str = sb.toString();
			requestMap.put(paramName, str);
		}
		logger.info("Signalwire_POC requestMap for Recording status: " + requestMap.toString());
		saveCallLog(requestMap);
		String callSid = requestMap.get("CallSid");
		String recordingUrl = requestMap.get("RecordingUrl");
		String recordingStatus = requestMap.get("RecordingStatus");
		String recordingId = requestMap.get("RecordingSid");
		if (recordingStatus.equalsIgnoreCase("completed")) {
			try {
				if (recordingUrl != null && recordingId != null) {
					InputStream inputStream = new URL(recordingUrl).openStream();
					int lastIndex = recordingUrl.lastIndexOf("/");
					String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
					String rfileName = fileName + ".wav";
					downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, rfileName,
							inputStream, true);
				}
			} catch (Exception e) {
				logger.error(
						"==========> SignalwireServiceImpl - Error occurred in handleRecordingCallback <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
		}
	}

	private void saveCallLog(HashMap<String, String> requestMap) {
		CallLog callLog = callLogService.findCallLog(requestMap.get("CallSid"));
		if (callLog == null) {
			callLog = new CallLog();
			callLog.setCallLogMap(requestMap);
		} else {
			HashMap<String, String> callLogMap = callLog.getCallLogMap();
			requestMap.forEach((key, value) -> {
				if (callLogMap.get(key) == null) {
					callLogMap.put(key, value);
				}
			});
			callLog.setCallLogMap(callLogMap);
		}
		callLogService.saveCallLog(callLog);
	}

	private String getHangupString(String callSid, boolean stopStream) {
		if (stopStream) {
			return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Stop><Stream name=\"" + callSid + "\"/></Stop><Hangup/></Response>";
		}
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Hangup/></Response>";
	}
	
	private List<CountryWiseDetailsDTO> getUniqueCountryDetails() {
		List<CountryWiseDetailsDTO> countryWiseList = new ArrayList<CountryWiseDetailsDTO>();
		List<PlivoOutboundNumber> plivoNumbers = plivoOutboundNumberService.findAll();
		if (plivoNumbers != null && plivoNumbers.size() > 0) {
			for (PlivoOutboundNumber plivoNumber : plivoNumbers) {
				if (!plivoNumber.getCountryName().equalsIgnoreCase("United States")) {
					List<CountryWiseDetailsDTO> filterList = new ArrayList<CountryWiseDetailsDTO>();
					if (countryWiseList != null && countryWiseList.size() > 0) {
						filterList = countryWiseList.stream().filter(
								pNumber -> pNumber.getCountryName().equalsIgnoreCase(plivoNumber.getCountryName()))
								.collect(Collectors.toList());
					}
					if (filterList.size() == 0) {
						CountryWiseDetailsDTO countryDTO = new CountryWiseDetailsDTO();
						countryDTO.setCountryCode(plivoNumber.getCountryCode());
						countryDTO.setCountryName(plivoNumber.getCountryName());
						countryDTO.setTimeZone(plivoNumber.getTimeZone());
						countryDTO.setIsdCode(plivoNumber.getIsdCode());
						countryWiseList.add(countryDTO);
					}
				}
			}
		}
		return countryWiseList;
	}

	private void addSignalwireNumbersInCountries(List<SignalwireOutboundNumber> signalwireNumbers,
			PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
		for (SignalwireOutboundNumber signalwireNumber : signalwireNumbers) {
			for (CountryWiseDetailsDTO countryWiseDetailsDTO : countryDetails) {
				if (!countryWiseDetailsDTO.getCountryCode().equalsIgnoreCase("US")) {
					SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(
							signalwireNumber.getDisplayPhoneNumber(), signalwireNumber.getDisplayPhoneNumber(),
							signalwireNumber.getAreaCode(), countryWiseDetailsDTO.getIsdCode(), "SIGNALWIRE",
							signalwireNumber.getPartnerId(), signalwireNumber.getPartnerId(),
							signalwireNumber.getVoiceMessage(), signalwireNumber.getPool(),
							countryWiseDetailsDTO.getTimeZone(), countryWiseDetailsDTO.getCountryCode(),
							countryWiseDetailsDTO.getCountryName(), signalwireNumber.getSignalwireId());
					signalwireOutboundNumberService.save(signalwireOutboundNumber);
				}
			}
		}
		logger.info("==========> Successfully added signalwire number in all countries. <==========");
	}

	@Override
	public String purchasePhoneNumbers(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<String> signalwireOutboundNumbers = new ArrayList<>();
		if (phoneDTO != null && phoneDTO.getStates() != null) {
			for (String state : phoneDTO.getStates()) {
				try {
					// Map<String, String> data = new HashMap<>();
					// data.put("max_results", "1");
					// data.put("region", state);
					// ObjectMapper mapper = new ObjectMapper();
					// String data = "{\"max_results\": \"1\", \"region\": \"" + state +"\"}";
					String searchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1&region="
							+ state;
					Map<String, Object> response = httpService.get(searchUrl, headers(),
							new ParameterizedTypeReference<Map<String, Object>>() {
							});
					Map<String, String> phoneNumber = new HashMap<>();
					if (response != null && response.get("data") != null
							&& ((List<Map<String, String>>) response.get("data")).size() > 0) {
						phoneNumber = ((List<Map<String, String>>) response.get("data")).get(0);
					} else {
						String defaultSearchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1";
						Map<String, Object> searchResponse = httpService.get(defaultSearchUrl, headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								});
						if (searchResponse != null && searchResponse.get("data") != null
								&& ((List<Map<String, String>>) searchResponse.get("data")).size() > 0) {
							phoneNumber = ((List<Map<String, String>>) searchResponse.get("data")).get(0);
						}
					}
					if (phoneNumber != null && !phoneNumber.isEmpty()) {
						String urlForPurchase = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers";
						Map<String, Object> purchaseResponse = httpService.post(urlForPurchase, headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								}, "{\"number\": \"" + phoneNumber.get("e164") + "\"}");
						if (purchaseResponse != null && purchaseResponse.get("number") != null) {
							String number = (String) purchaseResponse.get("number");
							String id = (String) purchaseResponse.get("id");
							String partnerId = phoneDTO.getPartnerId();
							String region = phoneDTO.getRegion();
							int areaCode = Integer.valueOf(number.substring(1, 4));
							String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
							SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(number,
									number, areaCode, phoneDTO.getIsdCode(), "SIGNALWIRE", partnerId, partnerId,
									voiceMessage, phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(),
									region, id);
							signalwireOutboundNumber = signalwireOutboundNumberService.save(signalwireOutboundNumber);
							signalwireOutboundNumbers.add(signalwireOutboundNumber.getPhoneNumber());
							// String urlForUpdate =
							// "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/" + id;
							// Map<String, Object> updateResponse = httpService.put(urlForUpdate, headers(),
							// new ParameterizedTypeReference<Map<String, Object>>() {
							// }, "{\"name\": \"" + number + "\", \"call_handler\": \"laml_application\",
							// \"call_laml_application_id\": \""
							// + phoneDTO.getAppId() + "\"}");
						} else {
							logger.info("==========> Signalwire Phone Number not found for [{}] <==========", state);
						}
					} else {
						logger.info("==========> Signalwire Phone Number not found for [{}] <==========", state);
					}
				} catch (Exception e) {
					logger.error(
							"==========> SignalwireServiceImpl - Error occurred in purchasePhoneNumbers <==========");
					logger.error("Expection : {} ", e);
				}
			}
		}
		return signalwireOutboundNumbers.toString();
	}

	@Override
	public String purchaseUSPhoneNumbersForOtherCountry(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<String> signalwireOutboundNumbers = new ArrayList<>();
		if (phoneDTO != null && phoneDTO.getPattern() != null) {
			// NumberType numberType = NumberType.valueOf(phoneDTO.getType());
			for (String pattern : phoneDTO.getPattern()) {
				try {
					// Map<String, String> data = new HashMap<>();
					// data.put("max_results", "1");
					// data.put("contains", pattern);
					// ObjectMapper mapper = new ObjectMapper();
					// String data = "{\"max_results\": \"1\", \"contains\": \"" + pattern +"\"}";
					String searchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1&contains="
							+ pattern;
					Map<String, Object> response = httpService.get(searchUrl, headers(),
							new ParameterizedTypeReference<Map<String, Object>>() {
							});
					Map<String, String> phoneNumber = new HashMap<>();
					if (response != null && response.get("data") != null
							&& ((List<Map<String, String>>) response.get("data")).size() > 0) {
						phoneNumber = ((List<Map<String, String>>) response.get("data")).get(0);
					} else {
						String defaultSearchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1";
						Map<String, Object> searchResponse = httpService.get(defaultSearchUrl, headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								});
						if (searchResponse != null && searchResponse.get("data") != null
								&& ((List<Map<String, String>>) searchResponse.get("data")).size() > 0) {
							phoneNumber = ((List<Map<String, String>>) searchResponse.get("data")).get(0);
						}
					}
					if (phoneNumber != null && !phoneNumber.isEmpty()) {
						String urlForPurchase = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers";
						Map<String, Object> purchaseResponse = httpService.post(urlForPurchase, headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								}, "{\"number\": \"" + phoneNumber.get("e164") + "\"}");
						if (purchaseResponse != null && purchaseResponse.get("number") != null) {
							String number = (String) purchaseResponse.get("number");
							String id = (String) purchaseResponse.get("id");
							String partnerId = phoneDTO.getPartnerId();
							String region = phoneDTO.getRegion();
							int areaCode = Integer.valueOf(number.substring(1, 4));
							String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
							SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(number,
									number, areaCode, phoneDTO.getIsdCode(), "SIGNALWIRE", partnerId, partnerId,
									voiceMessage, phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(),
									region, id);
							signalwireOutboundNumber = signalwireOutboundNumberService.save(signalwireOutboundNumber);
							signalwireOutboundNumbers.add(signalwireOutboundNumber.getPhoneNumber());
							// String urlForUpdate =
							// "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/" + id;
							// Map<String, Object> updateResponse = httpService.put(urlForUpdate, headers(),
							// new ParameterizedTypeReference<Map<String, Object>>() {
							// }, "{\"name\": \"" + number + "\", \"call_handler\": \"laml_application\",
							// \"call_laml_application_id\": \""
							// + phoneDTO.getAppId() + "\"}");
						} else {
							logger.info("==========> Signalwire Phone Number not found for [{}] <==========", pattern);
						}
					} else {
						logger.info("==========> Signalwire Phone Number not found for [{}] <==========", pattern);
					}
				} catch (Exception e) {
					logger.error(
							"==========> SignalwireServiceImpl - Error occurred in purchasePhoneNumbers <==========");
					logger.error("Expection : {} ", e);
				}
			}
		}
		return signalwireOutboundNumbers.toString();
	}

	@Override
	public String purchaseUSPhoneNumbersForOtherCountryUsingLimit(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<SignalwireOutboundNumber> signalwireOutboundNumbers = new ArrayList<>();
		if (phoneDTO != null && phoneDTO.getLimit() > 0) {
			try {
				String searchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results="
						+ phoneDTO.getLimit();
				Map<String, Object> response = httpService.get(searchUrl, headers(),
						new ParameterizedTypeReference<Map<String, Object>>() {
						});
				if (response != null && response.get("data") != null
						&& ((List<Map<String, String>>) response.get("data")).size() > 0) {
					List<Map<String, String>> phoneNumbers = ((List<Map<String, String>>) response.get("data"));

					for (Map<String, String> phoneNumber : phoneNumbers) {
						String urlForPurchase = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers";
						Map<String, Object> purchaseResponse = httpService.post(urlForPurchase, headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								}, "{\"number\": \"" + phoneNumber.get("e164") + "\"}");
						if (purchaseResponse != null && purchaseResponse.get("number") != null) {
							String number = (String) purchaseResponse.get("number");
							String id = (String) purchaseResponse.get("id");
							String partnerId = phoneDTO.getPartnerId();
							String region = phoneDTO.getRegion();
							int areaCode = Integer.valueOf(number.substring(1, 4));
							String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
							SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(number,
									number, areaCode, phoneDTO.getIsdCode(), "SIGNALWIRE", partnerId, partnerId,
									voiceMessage, phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(),
									region, id);
							// signalwireOutboundNumber =
							// signalwireOutboundNumberService.save(signalwireOutboundNumber);
							signalwireOutboundNumbers.add(signalwireOutboundNumber);
						}
					}
				} else {
					logger.info("==========> Signalwire Phone Number not found for [{}] <==========");
				}
			} catch (Exception e) {
				logger.error("==========> SignalwireServiceImpl - Error occurred in purchasePhoneNumbers <==========");
				logger.error("Expection : {} ", e);
			}
		}
		addSignalwireNumbersInCountries(signalwireOutboundNumbers, phoneDTO);
		return signalwireOutboundNumbers.toString();
	}

	private String getSignalwireExtensionFromCallSid(String callSid) {
		return this.signalwireCallSidExtensionMap.get(callSid);
	}

	private void putSignalwireExtensionInMap(String callSid, String extension) {
		this.signalwireCallSidExtensionMap.put(callSid, extension);
	}

	private void removeSignalwireCallSidFromExtensionMap(String callSid) {
		this.signalwireCallSidExtensionMap.remove(callSid);
	}
	
	@Override
	public Map<String, String> getSignalwireOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO) {
		Map<String, String> outboundNumberMap = new HashMap<String, String>();
		String outboundNumber = null;
		if (redialNumberDTO != null && redialNumberDTO.getPhone() != null && redialNumberDTO.getCountry() != null
				&& redialNumberDTO.getOrganizationId() != null) {
			outboundNumber = signalwireOutboundNumberService.getOutboundNumber(redialNumberDTO.getPhone(),
					redialNumberDTO.getCallRetryCount(), redialNumberDTO.getCountry(),
					redialNumberDTO.getOrganizationId());
		}
		outboundNumberMap.put("redialoutboundnumber", outboundNumber);
		return outboundNumberMap;
	}

}
