package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface ClientMappingUploadService {
	
	public void downloadTemplate(String campaignId, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException;

	public String createClientMappingEntries(String campaignId, MultipartFile file) throws IOException;

}
