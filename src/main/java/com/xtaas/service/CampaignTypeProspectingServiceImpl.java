package com.xtaas.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.TeamService;
import com.xtaas.db.entity.Country;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.CountryRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.db.repository.SuccessCallLogRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.ProspectCallDTO;

@Service
public class CampaignTypeProspectingServiceImpl implements CampaignTypeProspectingService {

	@Autowired
	TeamService teamService;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	CampaignService campaignService;

	@Autowired
	GlobalContactService globalContactService;

	@Autowired
	CountryRepository countryRepository;
	
	@Autowired
	AgentRepository agentRepository;

	@Autowired
	ProspectCallLogServiceImpl prospectCallLogServiceImpl;
	
	@Autowired
	StateCallConfigRepository stateCallConfigRepository;
	
	@Autowired
	SuccessCallLogRepository successCallLogRepository;

	@Override
	public ProspectCallDTO createProspectingLead(ProspectCallDTO prospectCallDTO) {
		ProspectCallLog prospectCallLog = null;
		String randomUUID = getUniqueProspect();
		String randomeUUIDWithoutHyphon = StringUtils.replace(randomUUID.toString(), "-", "");
		Campaign campaign = campaignService.getCampaign(prospectCallDTO.getCampaignId());
		if (!campaign.getStatus().equals(CampaignStatus.RUNNING)) {
			throw new IllegalArgumentException("Campaign is not in running state. Please check with your supervisor.");
		}
		if (prospectCallDTO.getProspect().getSource() == null
				|| (!prospectCallDTO.getProspect().getSource().equals("INSIDEVIEW")
						&& !prospectCallDTO.getProspect().getSource().equals("LeadIQ")
						&& !prospectCallDTO.getProspect().getSource().equals("ZOOINFO"))) {
			prospectCallDTO.getProspect().setSource("RESEARCH");
		}
		if (campaign.getOrganizationId().equalsIgnoreCase("XTAAS CALL CENTER")) {
			prospectCallDTO.getProspect().setSourceType("INTERNAL");
		} else {
			prospectCallDTO.getProspect().setSourceType("CLIENT");
		}
		prospectCallDTO.getProspect().setSourceType(prospectCallDTO.getPartnerId());
		prospectCallDTO.getProspect().setSourceId(randomeUUIDWithoutHyphon);
		prospectCallDTO.getProspect().setSourceCompanyId(randomeUUIDWithoutHyphon);
		String domainEmail = prospectCallDTO.getProspect().getEmail();

		if (domainEmail != null && !domainEmail.isEmpty()) {
			domainEmail = domainEmail.substring(domainEmail.indexOf("@") + 1, domainEmail.length());
		}
		setStateCodeAndTimezone(prospectCallDTO);
		Country country = countryRepository.findByCountry(prospectCallDTO.getProspect().getCountry());
		if (country != null && country.getContinent() != null && country.getContinent().equalsIgnoreCase("Europe")) {
			prospectCallDTO.getProspect().setEu(true);
		} else {
			prospectCallDTO.getProspect().setEu(false);
		}
		if (prospectCallDTO.getProspectCallId() == null) {
			prospectCallLog = new ProspectCallLog();
			ProspectCall prospectCallFromAgent = prospectCallDTO.toNewProspectCall(prospectCallDTO.getProspect());
			prospectCallFromAgent.setProspectCallId(randomUUID);
			if (prospectCallFromAgent.getProspectInteractionSessionId() == null) {
				prospectCallFromAgent.setProspectInteractionSessionId("IS" + randomeUUIDWithoutHyphon);
			}
			prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
			prospectCallLog.setProspectCall(prospectCallFromAgent);
		} else {
			ProspectCall prospectCallFromAgent = prospectCallDTO.toProspectCall();
			prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallDTO.getProspectCallId());
			prospectCallLog.setProspectCall(prospectCallFromAgent);
		}
		XtaasUtils.getQualityBucket(prospectCallLog);
		ProspectCallLog prospectCallLogFromDB = prospectCallLog;
		if (campaign != null && campaign.isTagSuccess()) {
			Agent agent = agentRepository.findOneById(prospectCallDTO.getAgentId());
			prospectCallLog.getProspectCall().setDispositionStatus("SUCCESS");
			prospectCallLog.getProspectCall().setSubStatus("SUBMITLEAD");
			prospectCallLog.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
			prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
			prospectCallLog.getProspectCall().setCallStartTime(new Date());
			prospectCallLog.getProspectCall().setAgentId(prospectCallDTO.getAgentId());
			prospectCallLog.getProspectCall().setPartnerId(prospectCallDTO.getPartnerId());
			SuccessCallLog successCallLog = new SuccessCallLog();
			successCallLog = successCallLog.toSuccessCallLog(prospectCallLogFromDB);
			successCallLogRepository.save(successCallLog);
			if (agent != null) {
				prospectCallLog.getProspectCall().setSupervisorId(agent.getSupervisorId());
			}
		}
		prospectCallLog.setProspectingData(true);
		prospectCallLogFromDB = prospectCallLogServiceImpl.save(prospectCallLog, true);
		createEntryInGlobalContact(prospectCallLogFromDB);
		prospectCallDTO = new ProspectCallDTO(prospectCallLogFromDB.getProspectCall());
		return prospectCallDTO;
	}
	
	private void setStateCodeAndTimezone(ProspectCallDTO prospectCallDTO) {
		List<StateCallConfig> stateCallConfigList = new ArrayList<>();
		stateCallConfigList = stateCallConfigRepository.findByCountryName(prospectCallDTO.getProspect().getCountry());
		if (stateCallConfigList != null && stateCallConfigList.size() > 0) {
			prospectCallDTO.getProspect().setStateCode(prospectCallDTO.getProspect().getStateCode());
			prospectCallDTO.getProspect().setTimeZone(stateCallConfigList.get(0).getTimezone());
		} else {
			prospectCallDTO.getProspect().setStateCode("ZZ");
			prospectCallDTO.getProspect().setTimeZone("US/Pacific");
		}

	}

	private void createEntryInGlobalContact(ProspectCallLog prospectCallLog) {
		GlobalContact globalContact = new GlobalContact();
		globalContact = globalContact.toGlobalContact(prospectCallLog.getProspectCall());
		globalContactService.saveGlobalContactAndInteraction(globalContact, true);
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d = new Date();
		String pcidv = uniquepcid + Long.toString(d.getTime());
		return pcidv;
	}
}