package com.xtaas.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.FileDTO;
import com.xtaas.web.dto.PlivoCallLogFilterDTO;

@Service
public class PlivoCallLogService {

	private static final Logger logger = LoggerFactory.getLogger(PlivoCallLogService.class);
	private static final String DM_PROFILE = "DEMANDSHORE";
	private static final String XTAAS_PROFILE = "XTAAS CALL CENTER";

	@Autowired
	private VoiceMailServiceImpl voiceMailServiceImpl;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Async
	public Boolean sendIncomingVoiceMails(PlivoCallLogFilterDTO filterDTO) {
		Boolean isSent = false;
		List<VoiceMail> voiceMails = voiceMailServiceImpl.findByProviderAndUpdatedDate(XtaasConstants.PLIVO,
				filterDTO.getProfile(), filterDTO.getStartDate(), filterDTO.getEndDate());
		if (voiceMails != null && voiceMails.size() > 0) {
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet();
			int rowCount = 0;
			Row headerRow = sheet.createRow(rowCount);
			writeHeaders(headerRow);
			for (VoiceMail voiceMail : voiceMails) {
				Row row = sheet.createRow(++rowCount);
				writeToExcel(voiceMail.getCallParams(), row, workbook);
			}
			try {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				workbook.write(outputStream);
				outputStream.toByteArray();
				List<FileDTO> files = new ArrayList<FileDTO>();
				files.add(new FileDTO("Incoming_VoiceMails.xlsx",
						Base64.getEncoder().encodeToString(outputStream.toByteArray()),
						XtaasConstants.FILE_TYPE_EXCEL));
				String subject = "Incoming VM ";
				if (filterDTO.getProfile().equalsIgnoreCase(XTAAS_PROFILE)) {
					subject = "XTaaS Incoming VM ";
				} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
					subject = "Demandshore Incoming VM ";
				}
				String message = "Please find attachment for incoming voicemails.";
				sendCallLogEmail(filterDTO, subject, message, files);
				isSent = true;
			} catch (Exception e) {
				logger.error("==========> PlivoCallLogService - Error occurred in sendIncomingVoiceMail <==========");
				logger.error("Expection : {} ", e);
			}
		}
		return isSent;
	}

	private void writeHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("CallSid");
		cell = headerRow.createCell(1);
		cell.setCellValue("From");
		cell = headerRow.createCell(2);
		cell.setCellValue("To");
		cell = headerRow.createCell(3);
		cell.setCellValue("Duration");
		cell = headerRow.createCell(4);
		cell.setCellValue("Direction");
		cell = headerRow.createCell(5);
		cell.setCellValue("Start Time");
		cell = headerRow.createCell(6);
		cell.setCellValue("End Time");
		cell = headerRow.createCell(7);
		cell.setCellValue("Status");
		cell = headerRow.createCell(8);
		cell.setCellValue("Recording URL");
		cell = headerRow.createCell(9);
		cell.setCellValue("Price");
	}

	private void writeToExcel(HashMap<String, String> callParams, Row row, Workbook workbook) {
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
		Date startDate = new Date(new Long(callParams.get("RecordingStartMs")));
		Date endDate = new Date(new Long(callParams.get("RecordingEndMs")));

		Cell cell = row.createCell(0);
		cell.setCellValue(callParams.get("CallUUID"));
		cell = row.createCell(1);
		cell.setCellValue(callParams.get("From"));
		cell = row.createCell(2);
		cell.setCellValue(callParams.get("To"));
		cell = row.createCell(3);
		cell.setCellValue(callParams.get("RecordingDuration"));
		cell = row.createCell(4);
		cell.setCellValue(callParams.get("Direction"));
		cell = row.createCell(5);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(startDate);
		cell = row.createCell(6);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(endDate);
		cell = row.createCell(7);
		cell.setCellValue(callParams.get("CallStatus"));
		cell = row.createCell(8);
		/* Set Plivo url */
		// cell.setCellValue(callParams.get("RecordUrl"));
		/* Set AWS S3 bucket recording url */

		String recordingUrl = callParams.get("RecordUrl");
		InputStream inputStream = null;
		try {
			inputStream = new URL(recordingUrl).openStream();
			String fileName = recordingUrl.substring(recordingUrl.length() - 34, recordingUrl.length());
			fileName = fileName + ".wav";
			downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream, true);
			cell.setCellValue(createAwsUrl(recordingUrl));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		cell = row.createCell(9);
		cell.setCellValue(callParams.get("BillRate"));
	}

	private String createAwsUrl(String recordingUrl) {
		int lastIndex = recordingUrl.lastIndexOf("/");
		String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
		String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + fileName;
		return awsRecordingUrl;
	}

	private void sendCallLogEmail(PlivoCallLogFilterDTO filterDTO, String subject, String message,
			List<FileDTO> files) {
		if (filterDTO != null && filterDTO.getToEmails() != null && filterDTO.getToEmails().size() > 0
				&& filterDTO.getFromEmail() != null && !filterDTO.getFromEmail().isEmpty()) {
			logger.info("Sending Plivo call Log : From-[{}], To-{}", filterDTO.getFromEmail(), filterDTO.getToEmails());
			String fromEmail = filterDTO.getFromEmail();
			for (String toEmail : filterDTO.getToEmails()) {
				XtaasEmailUtils.sendEmailWithAttachment(toEmail, fromEmail, subject, message, files);
			}
		} else {
			logger.info(
					"==========> PlivoCallLogService - sendCallLogEmail - From or To emails are empty. <==========");
		}
	}

}
