package com.xtaas.service;

import com.monitorjbl.xlsx.StreamingReader;
import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.application.service.PlivoOutboundNumberServiceImpl;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.*;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanyRecord;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CurrentEmployment;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.utils.*;
import com.xtaas.web.dto.ManualProspectDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ZoomManualSearchProspectServiceImpl implements Runnable {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ZoomManualSearchProspectServiceImpl.class);

	private String partnerKey = "Xtaascorp.partner";

	private ZoomTokenRepository zoomTokenRepository;

	private StateCallConfigRepository stateCallConfigRepository;

	private EntityMappingRepository entityMappingRepository;

	String mTokenKey = "";

	Date mCachedDate;

	CampaignRepository campaignRepository;

	private ProspectCallLogRepository prospectCallLogRepository;

	private ManualProspectSearchServiceImpl manualProspectSearchServiceImpl;

	private Prospect prospect;

	private String agentId;

	private CountDownLatch latch;

	private Campaign campaign;

	private GlobalContactRepository globalContactRepository;

	private boolean manualSearch;

	AbmListServiceImpl abmListServiceImpl;

	private AbmListDetailRepository abmListDetailRepository;

	private PlivoOutboundNumberService plivoOutboundNumberService;

	private ZoomInfoDataBuy zoomInfoDataBuy;

	private Map<String, Boolean> validationMap = new HashMap<>();

	private List<DomainCompanyCountPair> successCompanyData;

	private PropertyService propertyService;


	public ZoomManualSearchProspectServiceImpl(Prospect prospect,Campaign campaign,String agentId,CountDownLatch latch,
											   boolean manualSearch,List<DomainCompanyCountPair> successCompanyData) {
		this.prospect = prospect;
		this.successCompanyData = successCompanyData;
		this.agentId = agentId;
		this.latch = latch;
		this.campaign = campaign;
		this.manualSearch = manualSearch;
		campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		entityMappingRepository = BeanLocator.getBean("entityMappingRepository", EntityMappingRepository.class);
		manualProspectSearchServiceImpl = BeanLocator.getBean("manualProspectSearchServiceImpl", ManualProspectSearchServiceImpl.class);
		prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
		stateCallConfigRepository = BeanLocator.getBean("stateCallConfigRepository", StateCallConfigRepository.class);
		zoomTokenRepository = BeanLocator.getBean("zoomTokenRepository", ZoomTokenRepository.class);
		globalContactRepository = BeanLocator.getBean("globalContactRepository", GlobalContactRepository.class);
		abmListServiceImpl = BeanLocator.getBean("abmListServiceImpl", AbmListServiceImpl.class);
		plivoOutboundNumberService = BeanLocator.getBean("plivoOutboundNumberServiceImpl", PlivoOutboundNumberService.class);
		abmListDetailRepository = BeanLocator.getBean("abmListDetailRepository", AbmListDetailRepository.class);
		zoomInfoDataBuy = BeanLocator.getBean("zoomInfoDataBuy", ZoomInfoDataBuy.class);
		propertyService = BeanLocator.getBean("propertyServiceImpl", PropertyService.class);


	}

	@Override
	public void run() {
			try {
				getPurchaseResponse(prospect);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.latch.countDown();
	}

	private void getPurchaseResponse(Prospect prospect) {
		int maxSearchTime = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_SEARCH_TIME.name(), 10);

		boolean maxLimitCheck = true;
		int size = 25;
		boolean isManualProspect = false;
		if(!manualSearch){
			isManualProspect = true;
		}
		try {
			setCompanyId(prospect);
			int offset = 1;

			//CampaignCriteria cc = getCampaignCriteria(campaign);
			CampaignCriteria cc = fetchZoomCampaignCriteria(campaign);
			boolean isDirectSearch = true;
			int tempPageCount = 1;
			boolean isPageDataFetch = true;
			Date date = new Date();
			for (int pageCount = 1; pageCount <= offset; pageCount++) {
				long diff = new Date().getTime() - date. getTime();
				long seconds = TimeUnit. MILLISECONDS. toSeconds(diff);
				if(seconds> maxSearchTime) {
					logger.info("Timeout stopping the thread for campaing [{}] ", campaign.getId());
					return ;
				}
				tempPageCount = pageCount;
				// first search for direct
				ZoomInfoResponse personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size, isManualProspect,maxLimitCheck);
				// if you dont get direct then search for indirect number;
				if (personSearchZoomInfoResponse == null || personSearchZoomInfoResponse.getPersonSearchResponse() == null
						|| personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
					isDirectSearch = false;
					if(isPageDataFetch){
						tempPageCount = 1;
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size,isManualProspect,maxLimitCheck);
						isPageDataFetch = false;
					}else {
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,pageCount,size,isManualProspect,maxLimitCheck);
					}
				}
				if (personSearchZoomInfoResponse == null || personSearchZoomInfoResponse.getPersonSearchResponse() == null
						|| personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
					isDirectSearch = true;
					isPageDataFetch = true;
					manualSearch = false;
					isManualProspect = true;
					if(isPageDataFetch){
						tempPageCount = 1;
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size,isManualProspect,maxLimitCheck);
						isPageDataFetch = false;
					}else {
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,pageCount,size,isManualProspect,maxLimitCheck);
					}
				}
				if (personSearchZoomInfoResponse == null || personSearchZoomInfoResponse.getPersonSearchResponse() == null
						|| personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
					isDirectSearch = false;
					isPageDataFetch = true;
					manualSearch = false;
					isManualProspect = true;
					if(isPageDataFetch){
						tempPageCount = 1;
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size,isManualProspect,maxLimitCheck);
						isPageDataFetch = false;
					}else {
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,pageCount,size,isManualProspect,maxLimitCheck);
					}
				}
				if (personSearchZoomInfoResponse == null || personSearchZoomInfoResponse.getPersonSearchResponse() == null
						|| personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
					isDirectSearch = true;
					isPageDataFetch = true;
					manualSearch = false;
					isManualProspect = false;
					if(isPageDataFetch){
						tempPageCount = 1;
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size,isManualProspect,maxLimitCheck);
						isPageDataFetch = false;
					}else {
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,pageCount,size,isManualProspect,maxLimitCheck);
					}
				}
				if (personSearchZoomInfoResponse == null || personSearchZoomInfoResponse.getPersonSearchResponse() == null
						|| personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
					isDirectSearch = false;
					manualSearch = false;
					isPageDataFetch = true;
					isManualProspect = false;
					if(isPageDataFetch){
						tempPageCount = 1;
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,tempPageCount,size,isManualProspect,maxLimitCheck);
						isPageDataFetch = false;
					}else {
						personSearchZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc,pageCount,size,isManualProspect,maxLimitCheck);
					}
				}


				if (personSearchZoomInfoResponse != null && personSearchZoomInfoResponse.getPersonSearchResponse() != null) {
					int totalResult = personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults();

					if (totalResult > 0 && personSearchZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
							.getPersonRecord() != null) {
						List<PersonRecord> list = personSearchZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
								.getPersonRecord();
						boolean isFull = true;
						if (list != null && list.size() > 0) {
							for(PersonRecord pr : list){
								if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null &&
										manualProspectSearchServiceImpl.manualProspectMap.get(agentId).size() >= manualProspectSearchServiceImpl.SIMILAR_PROSPECT_SIZE) {
									isFull = false;
									return;
								}
								ManualProspectDTO dto = new ManualProspectDTO();
								int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordBySourceId(campaign.getId(),pr.getPersonId());
								boolean isValidCountry = zoomInfoDataBuy.isValidCountry(cc, pr);
								boolean isValidProspect = zoomInfoDataBuy.validateSearchData(pr,cc, campaign,validationMap,prospect);
								if(isValidProspect && uniqueRecord == 0 && isValidCountry) {
									Prospect personDetailsResponse = getProspectInfo(pr,isDirectSearch,prospect);
									boolean isValidDomain = zoomInfoDataBuy.isValidDomain(personDetailsResponse,cc, campaign);
									boolean isValidCountryPhone = zoomInfoDataBuy.isValidCountryPhone(personDetailsResponse);
									if(personDetailsResponse != null && personDetailsResponse.getPhone() != null && isValidCountryPhone
											&& isValidDomain){
										List<Prospect> listDto = manualProspectSearchServiceImpl.manualProspectMap.get(agentId);
										if(CollectionUtils.isNotEmpty(listDto)){
											List<Prospect> plist = manualProspectSearchServiceImpl.manualProspectMap.get(agentId);
											boolean itemExists = plist.stream().anyMatch(c -> c.getPhone().equals(personDetailsResponse.getPhone()));
											if(!itemExists){
												manualProspectSearchServiceImpl.manualProspectMap.get(agentId).add(personDetailsResponse);
											}
										}else {
											List<Prospect> emptyListDto = new ArrayList<>();
											emptyListDto.add(personDetailsResponse);
											manualProspectSearchServiceImpl.manualProspectMap.put(agentId,emptyListDto);
										}
										saveGlobalContact(personDetailsResponse);
									}
									if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null &&
											manualProspectSearchServiceImpl.manualProspectMap.get(agentId).size() >= manualProspectSearchServiceImpl.SIMILAR_PROSPECT_SIZE) {
										isFull = false;
										return;
									}
								}
							}
							if(!isFull) {
								return;
							}
						}
					}
					offset++;
				}

//				int totalResult = personSearchZoomInfoResponse.getPersonSearchResponse().getTotalResults();
//				int totalPageCount = (int) Math.ceil((double) totalResult / size);
//				offset = totalPageCount;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void saveGlobalContact(Prospect prospect){
		zoomInfoDataBuy.saveGlobalContact(prospect);
	}
	private Prospect getProspectInfo(PersonRecord personRecord,boolean isDirectSearch,Prospect prospect) {
		try {
			String personDetailRequestUrl = buildUrl(null, 0, 0, personRecord.getPersonId());
			URL detailUrl = new URL(personDetailRequestUrl); // purchase request
			ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(detailUrl);
			if (zoomInfoResponseDetail != null && zoomInfoResponseDetail.getPersonDetailResponse() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getFirstName() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getLastName() != null
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getFirstName())
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getLastName())
					&& (zoomInfoResponseDetail.getPersonDetailResponse().getDirectPhone() != null
					|| zoomInfoResponseDetail.getPersonDetailResponse()
					.getCompanyPhone() != null)) {

				PersonDetailResponse personDetailResponse = zoomInfoResponseDetail
						.getPersonDetailResponse();
				List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
				int industryListSize = 0;
				if (industryList != null) {
					industryListSize = industryList.size();
				}
				List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
				List<String> topLevelIndustries = new ArrayList<String>();
				String zoomCountry = "United States";
				Prospect prospectInfo = new Prospect();
				if (personDetailResponse.getLocalAddress() != null
						&& personDetailResponse.getLocalAddress().getCountry() != null) {
					if (personDetailResponse.getLocalAddress().getStreet() != null)
						prospectInfo.setAddressLine1(personDetailResponse.getLocalAddress().getStreet());
					else
						prospectInfo.setAddressLine1("");
					if (personDetailResponse.getLocalAddress().getZip() != null)
						prospectInfo.setZipCode(personDetailResponse.getLocalAddress().getZip());
					else
						prospectInfo.setZipCode("");
					if (personDetailResponse.getLocalAddress().getCity() != null)
						prospectInfo.setCity(personDetailResponse.getLocalAddress().getCity());
					else
						prospectInfo.setCity("");
					// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
					// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
				} else if (personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCountry() != null) {
					prospectInfo.setAddressLine1(personDetailResponse.getCurrentEmployment().get(0)
							.getCompany().getCompanyAddress().getStreet());
					prospectInfo.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getZip());
					prospectInfo.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCity());
				}
				if (personDetailResponse.getLastUpdatedDate() != null
						&& !personDetailResponse.getLastUpdatedDate().isEmpty()) {
					String ld = personDetailResponse.getLastUpdatedDate();
					if (ld.contains("-"))
						ld = ld.replace("-", "/");
					Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);

					prospectInfo.setLastUpdatedDate(date1);
				}
				if (personDetailResponse.getLocalAddress() != null) {
					prospectInfo.setCountry(personDetailResponse.getLocalAddress().getCountry());
					zoomCountry = personDetailResponse.getLocalAddress().getCountry();
				} else if (personDetailResponse.getCurrentEmployment() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCountry() != null) {
					prospectInfo.setCountry(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCountry());
					zoomCountry = personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCountry();
				}

				if (prospectInfo.getCountry().equalsIgnoreCase("USA")
						|| prospectInfo.getCountry().equalsIgnoreCase("United States")
						|| prospectInfo.getCountry().equalsIgnoreCase("US")
						|| prospectInfo.getCountry().equalsIgnoreCase("Canada")) {
					prospectInfo.setPhone("+1"+getUSPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getUSCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getStateCode(personDetailResponse));
				} else if (prospectInfo.getCountry().equalsIgnoreCase("United Kingdom")
						|| prospectInfo.getCountry().equalsIgnoreCase("UK")) {
					prospectInfo.setPhone(getUKPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getUKCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getUKStateCode(personDetailResponse));
				} else {
					prospectInfo.setPhone(getPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getStateCode(personDetailResponse));
				}

				if (industryListSize > 0) {
					prospectInfo.setIndustry(industryList.get(0));
					prospectInfo.setIndustryList(industryList);
				}

				prospectInfo.setEu(personDetailResponse.isEU());

				String title = personRecord.getCurrentEmployment().getJobTitle();
				String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
				String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
				String empRange = personRecord.getCurrentEmployment().getCompany()
						.getCompanyEmployeeCountRange();
				long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();
				long revCountInZeros = personRecord.getCurrentEmployment().getCompany()
						.getCompanyRevenueIn000s();
				String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
				// Hitting the company details api
				ZoomInfoResponse companyResponse = getCompanyResponse(null,
						personRecord.getCurrentEmployment().getCompany().getCompanyId());

				Map<String, Object> customAttributes = new HashMap<String, Object>();
				Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);

				customAttributes.put("minRevenue", revenueEmployeeList.get("minRevenue"));
				customAttributes.put("maxRevenue", revenueEmployeeList.get("maxRevenue"));

				customAttributes.put("maxEmployeeCount", revenueEmployeeList.get("maxEmployeeCount"));
				customAttributes.put("minEmployeeCount", revenueEmployeeList.get("minEmployeeCount"));

				String detailDomain = personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyWebsite();
				if (detailDomain != null && !detailDomain.isEmpty()) {
					customAttributes.put("domain", detailDomain);
				} else {
					customAttributes.put("domain", domain);
				}
				customAttributes.put("revCount", revCount);
				customAttributes.put("revCountIn000s", new Long(revCountInZeros));
				customAttributes.put("empCount", new Long(empCount));
				prospectInfo.setCustomAttributes(customAttributes);

				prospectInfo.setCompany(getOrganizationName(zoomInfoResponseDetail));
				if (personDetailResponse.getCurrentEmployment().get(0).getManagementLevel() != null)
					prospectInfo.setManagementLevel(
							personDetailResponse.getCurrentEmployment().get(0).getManagementLevel().get(0));

				String timez = getTimeZone(prospectInfo);
				prospectInfo.setTimeZone(timez);

				// prospectInfo.setCompanyAddress(personDetailResponse.get);

				prospectInfo.setDepartment(getJobFunction(zoomInfoResponseDetail));

				prospectInfo.setDirectPhone(isDirectSearch);
				if(personDetailResponse.getEmailAddress() != null && !personDetailResponse.getEmailAddress().isEmpty()) {
					prospectInfo.setEmail(personDetailResponse.getEmailAddress());
				}else {
					if(manualSearch){
						if(prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
							prospectInfo.setEmail(prospect.getEmail());
						}
					}
				}

				// prospectInfo.setEu(personDetailResponse.get);
				prospectInfo.setFirstName(personDetailResponse.getFirstName());

				prospectInfo.setLastName(personDetailResponse.getLastName());
				// prospectInfo.setPrefix(prefix);
				// prospectInfo.setProspectType(prospectType);
				prospectInfo.setSourceType("INTERNAL");
				prospectInfo.setSourceCompanyId(getOrganizationId(zoomInfoResponseDetail));
				prospectInfo.setSourceId(personDetailResponse.getPersonId());
				prospectInfo.setSource("ZOOINFO");
				// prospectInfo.setStatus(status);
				prospectInfo.setTitle(title);
				prospectInfo.setTopLevelIndustries(topLevelIndustries);
				prospectInfo.setZoomPersonUrl(personDetailResponse.getZoomPersonUrl());

				if (companyResponse.getCompanySearchResponse() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0) != null) {

					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getZoomCompanyUrl() != null) {
						prospectInfo.setZoomCompanyUrl(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getZoomCompanyUrl());
					}

					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyNAICS() != null) {
						prospectInfo.setCompanyNAICS(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS());
					}
					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanySIC() != null) {
						prospectInfo.setCompanySIC(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC());
					}

				}
				return prospectInfo;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private ZoomInfoResponse preparePersonSearch(Prospect prospect, boolean isDirectSearch,CampaignCriteria cc,
												 int pageNumber,int size, boolean isManualProspect, boolean maxLimitCheck) {
		URL url;
		try {
			String queryString = buildQueryString(prospect, isDirectSearch,cc,isManualProspect);
			String previewUrl = buildUrl(queryString, pageNumber, size, null);
			logger.info("\n similar search previewUrl1: " + previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				if(inputline.contains("Authentication Failed") || inputline.contains("You have exceeded your API query rate limit")) {
					logger.error("Zoom Authentication Failed, You have exceeded your API query rate limit");
					if(maxLimitCheck){
						maxLimitCheck = false;
						preparePersonSearch(prospect, isDirectSearch,cc, pageNumber,size,isManualProspect,maxLimitCheck);
					}
				}
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
			if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
				int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();
				logger.info("\n Total record fetched from zoom similar prospect: [{}]" ,totalResult);
			}else{
				logger.info("\n No similar prospect fetched of previewUrl1: [{}] " , previewUrl);
			}
			return personZoomInfoResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String buildUrl(String queryString, int offset, int size, String personId) throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		//logger.debug("Key : " + key);
		if (personId != null) {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&PersonID=" + personId
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key=" + key;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,companyAddress,companyRevenueNumeric,companyRevenueRange,companyEmployeeRange,companyEmployeeCount&key="
					+ key + "&rpp=" + size + "&page=" + offset;
			return url;
		}
	}

	private ZoomInfoResponse getZoomInfoResponse(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent",
				"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
		// int responseCode= connection.getResponseCode();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputline;
		StringBuffer response = new StringBuffer();
		while ((inputline = bufferedReader.readLine()) != null) {
			response.append(inputline);
		}
		bufferedReader.close();
		ZoomInfoResponse zoomInfoResponse = getResponse(response, false);
		return zoomInfoResponse;
	}

	private ZoomInfoResponse getCompanyResponse(String companyName, String companyId) {
		ZoomInfoResponse companyZoomInfoResponse = new ZoomInfoResponse();

		try {
			StringBuilder queryString = createCompanyCriteria(companyName, companyId, companyId);

			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			//logger.debug("\n previewUrl3 : " + previewUrl);
			URL url = new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			companyZoomInfoResponse = getResponse(response, true);
			return companyZoomInfoResponse;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return companyZoomInfoResponse;

	}

	private ZoomInfoResponse getResponse(StringBuffer response, boolean isCompanySearch) throws IOException {
		int countCA = 0;
		if (!response.toString().contains("personId") && !isCompanySearch) {
			return null;
		}
		String ignore = "\"companyAddress\":\"\"";
		if (response.toString().contains(ignore)) {
			countCA = StringUtils.countMatches(response.toString(), ignore);
		}

		String rep = "\"companyAddress\":{}";
		// String ignorewith = "CurrentEmployment\":[";
		for (int i = 0; i < countCA; i++) {

			int xindex = response.indexOf(ignore);
			String sub = response.substring(xindex);
			String sub1 = sub.substring(0, 19);
			int startindex = response.indexOf(sub1);
			int endindex = startindex + sub1.length();
			response.replace(xindex, endindex, rep);
			// logger.debug(response.toString());
		}
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null) {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
			return zoomInfoResponse;
		} else {
			return null;
		}
	}

	private String getOrganizationId(ZoomInfoResponse zoomInfoResponseDetail) {
		String companyId = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					companyId = currentEmployment.getCompany().getCompanyId();
					break;
				}
			}
		}

		return companyId;
	}

	public String getTimeZone(Prospect prospect) {
		// List<OutboundNumber> outboundNumberList;
		// int areaCode = Integer.parseInt(findAreaCode(prospect.getPhone()));
		if (prospect != null && prospect.getStateCode() != null) {

			StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(prospect.getStateCode());
			return stateCallConfig.getTimezone();
		}
		String s = prospect.getFirstName() + "" + prospect.getLastName();
		//logger.debug("prospect State is null : " + s);
		// if(stateCallConfig==null){
		return null;
		// }

		// find a random number from the list

	}

	private String getOrganizationName(ZoomInfoResponse zoomInfoResponseDetail) {
		String orgName = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					orgName = currentEmployment.getCompany().getCompanyName();
					break;
				}
			}
		}

		return orgName;
	}

	private String getJobFunction(ZoomInfoResponse zoomInfoResponseDetail) {
		String jobFunction = null;
		try {
			if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
				for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
						.getCurrentEmployment()) {
					if (currentEmployment.getJobFunction() != null) {
						jobFunction = currentEmployment.getJobFunction();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			//logger.error("Department null :" + e);
			jobFunction = "_";
		}
		return jobFunction;
	}

	private String buildQueryString(Prospect prospect, boolean isDirectSearch,CampaignCriteria cc,boolean isManualProspect) {
		StringBuilder sb = new StringBuilder();
		validationMap.clear();
		try {
			if (cc != null && cc.getCountry() != null && !cc.getCountry().isEmpty()) {
				sb.append("Country=" + URLEncoder.encode(cc.getCountry(), XtaasConstants.UTF8));
			}
			if(cc.getAbmCompanyIds() != null && !cc.getAbmCompanyIds().isEmpty() && cc.getAbmCompanyIds().size() > 0){
				if (isManualProspect) {
					validationMap.clear();
					if (cc != null && cc.getManagementLevel() != null) {
						sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
						validationMap.put("management", true);
					}
					if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
						sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
						validationMap.put("department", true);
					}
					sb.append("&companyIds=" + URLEncoder.encode(StringUtils.join(cc.getAbmCompanyIds(), ",")));
				}else{
					if(manualSearch){
						validationMap.clear();
						if (cc != null && StringUtils.isNotBlank(prospect.getTitle())) {
							sb.append("&personTitle=" + URLEncoder.encode(prospect.getTitle(), XtaasConstants.UTF8));
							validationMap.put("title", true);
						}
						if (prospect != null && prospect.getCompany() != null && !prospect.getCompany().isEmpty()) {
							validationMap.put("company", true);
							if(StringUtils.isNotBlank(prospect.getCompanyId()))
								sb.append("&companyIds=" + URLEncoder.encode(prospect.getCompanyId(), XtaasConstants.UTF8));
							else
								sb.append("&companyName=" + URLEncoder.encode(prospect.getCompany(), XtaasConstants.UTF8));
						}
					}else {
						validationMap.clear();
						if (cc != null && cc.getManagementLevel() != null) {
							sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
							validationMap.put("management", true);
						}
						if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
							sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
							validationMap.put("department", true);
						}
						if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()
								&& !cc.getMinEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(cc.getMinEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()
								&& !cc.getMaxEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(cc.getMaxEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty()
								&& !cc.getMinRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMin=" + URLEncoder.encode(cc.getMinRevenue(), XtaasConstants.UTF8));

						if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty()
								&& !cc.getMaxRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMax=" + URLEncoder.encode(cc.getMaxRevenue(), XtaasConstants.UTF8));

						if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()){
							sb.append("&IndustryClassification=" + URLEncoder.encode(cc.getIndustry(), XtaasConstants.UTF8));
							validationMap.put("industry", true);
						}
					}
				}

			}else{
				if(isManualProspect){
					validationMap.clear();
					if (cc != null && cc.getManagementLevel() != null && prospect != null && prospect.getCompany() != null &&
							!prospect.getCompany().isEmpty()) {

						if (cc != null && cc.getManagementLevel() != null) {
							sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
							validationMap.put("management", true);
						}
						if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
							sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
							validationMap.put("department", true);
						}
						if(StringUtils.isNotBlank(prospect.getCompanyId()))
							sb.append("&companyIds=" + URLEncoder.encode(prospect.getCompanyId(), XtaasConstants.UTF8));
						else
							sb.append("&companyName=" + URLEncoder.encode(prospect.getCompany(), XtaasConstants.UTF8));
						validationMap.put("company", true);
					}else {
						validationMap.clear();
						if (cc != null && cc.getManagementLevel() != null) {
							sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
							validationMap.put("management", true);
						}
						if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
							sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
							validationMap.put("department", true);
						}
						if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()
								&& !cc.getMinEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(cc.getMinEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()
								&& !cc.getMaxEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(cc.getMaxEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty()
								&& !cc.getMinRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMin=" + URLEncoder.encode(cc.getMinRevenue(), XtaasConstants.UTF8));

						if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty()
								&& !cc.getMaxRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMax=" + URLEncoder.encode(cc.getMaxRevenue(), XtaasConstants.UTF8));

						if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()){
							sb.append("&IndustryClassification=" + URLEncoder.encode(cc.getIndustry(), XtaasConstants.UTF8));
							validationMap.put("industry", true);
						}

					}

				}else {
					if(manualSearch){
						validationMap.clear();

						if (cc != null && StringUtils.isNotBlank(prospect.getTitle())) {
							sb.append("&personTitle=" + URLEncoder.encode(prospect.getTitle(), XtaasConstants.UTF8));
							validationMap.put("title", true);
						}
						if (prospect != null && prospect.getCompany() != null && !prospect.getCompany().isEmpty()) {
							if(StringUtils.isNotBlank(prospect.getCompanyId()))
								sb.append("&companyIds=" + URLEncoder.encode(prospect.getCompanyId(), XtaasConstants.UTF8));
							else
								sb.append("&companyName=" + URLEncoder.encode(prospect.getCompany(), XtaasConstants.UTF8));
						}
						validationMap.put("company", true);
					}else {
						validationMap.clear();
						if (cc != null && cc.getManagementLevel() != null) {
							sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
							validationMap.put("management", true);
						}
						if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
							sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
							validationMap.put("department", true);
						}
						if (cc.getMinEmployeeCount() != null && !cc.getMinEmployeeCount().isEmpty()
								&& !cc.getMinEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(cc.getMinEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMaxEmployeeCount() != null && !cc.getMaxEmployeeCount().isEmpty()
								&& !cc.getMaxEmployeeCount().equalsIgnoreCase("0"))
							sb.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(cc.getMaxEmployeeCount(), XtaasConstants.UTF8));

						if (cc.getMinRevenue() != null && !cc.getMinRevenue().isEmpty()
								&& !cc.getMinRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMin=" + URLEncoder.encode(cc.getMinRevenue(), XtaasConstants.UTF8));

						if (cc.getMaxRevenue() != null && !cc.getMaxRevenue().isEmpty()
								&& !cc.getMaxRevenue().equalsIgnoreCase("0"))
							sb.append("&RevenueClassificationMax=" + URLEncoder.encode(cc.getMaxRevenue(), XtaasConstants.UTF8));

						if (cc.getIndustry() != null && !cc.getIndustry().isEmpty()){
							sb.append("&IndustryClassification=" + URLEncoder.encode(cc.getIndustry(), XtaasConstants.UTF8));
							validationMap.put("industry", true);
						}

					}
				}

			}

			if (isDirectSearch)
				sb.append("&ContactRequirements=" + URLEncoder.encode("5", XtaasConstants.UTF8));// require phone,
			else
				sb.append("&ContactRequirements=" + URLEncoder.encode("2", XtaasConstants.UTF8));// require phone,

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	private String getPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");

		} else {
			phone = getCompanyPhone(personDetailResponse);
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");
		}
	}

	private String getCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		return phone;
	}

	private String getExtension(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			String phonepp = phone.substring(0, phone.indexOf("ext"));
			ext = phone.substring(phone.indexOf("ext"));
			ext = ext.replaceAll("[^[A-Za-z]*$]", "");

			// System.out.println(ext);
		}
		return ext;
	}

	private String getUSPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		if (phone != null) {

			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			phone = getUSCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUSCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private String getUKPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;

		} else {
			phone = getUKCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUKCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;
		} else {
			return phone;
		}
	}

	private String getStateCode(PersonDetailResponse personDetailResponse) {
		if (personDetailResponse.getLocalAddress() != null) {
			String state = personDetailResponse.getLocalAddress().getState();
			String stateCode = getState(state);
			if (personDetailResponse.getLocalAddress().getCountry() != null
					&& !personDetailResponse.getLocalAddress().getCountry().isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository
						.findByCountryName(personDetailResponse.getLocalAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		} else if (personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
				.getCountry() != null) {
			String state = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
					.getState();
			String stateCode = getState(state);
			if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry() != null
					&& !personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry()
					.isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository.findByCountryName(personDetailResponse
						.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		}
		return null;
	}

	private String getUKStateCode(PersonDetailResponse personDetailResponse) {

		return "GL";
	}

	private String getState(String state) {
		EntityMapping toMapping = getEntityMapping();
		AttributeMapping attributeMapping = getReverseAttributeMapping("state", toMapping);
		List<ValueMapping> stateCode = attributeMapping.getValues();
		Iterator<ValueMapping> iterator = stateCode.iterator();
		while (iterator.hasNext()) {
			ValueMapping valueMapping = (ValueMapping) iterator.next();
			String to = valueMapping.getTo().replaceAll("%20", " ");
			if (to.equals(state)) {
				return valueMapping.getFrom();
			}
		}
		return null;

	}

	private EntityMapping getEntityMapping() {
		List<EntityMapping> toMappings = entityMappingRepository.findByFromSystemToSystem("xtaas",
				ContactListProviderType.ZOOMINFO.name());
		if (toMappings.size() > 0) {
			return toMappings.get(0);
		}
		return null;
	}

	private AttributeMapping getReverseAttributeMapping(String netProspexAttribute, EntityMapping toMapping) {
		for (AttributeMapping attributeMapping : toMapping.getAttributes()) {
			if (attributeMapping.getTo().equals(netProspexAttribute)) {
				return attributeMapping;
			}
		}
		return null;
	}

	private String getZoomOAuth() {
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (mCachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}

		Date currDate = new Date();
		long duration = currDate.getTime() - mCachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}
		return mTokenKey;
	}

	private String buildCompanyUrl(String queryString, /* int offset, int size, */String personId)
			throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		//logger.debug("Key : " + key);
		if (personId != null) {
			/*
			 * String
			 * baseUrl="https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			 * String url = baseUrl + "pc="+partnerKey +"&CompanyID="+personId +
			 * "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key="
			 * +key; return url;
			 */
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companysic,companyType&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		}
	}

	private StringBuilder createCompanyCriteria(String companyName, String companyId, String campaignId) {
		StringBuilder queryString = new StringBuilder();
		try {
			if (companyId != null) {
				queryString.append("companyIds=" + URLEncoder.encode(companyId, XtaasConstants.UTF8));
			} else if (companyName != null) {
				queryString.append("companyName=" + URLEncoder.encode(companyName, XtaasConstants.UTF8));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryString;

	}

	private Map<String, Long> findCompanyData(String revRange, String empRange) {
		List<Long> revenueEmployesData = new ArrayList<Long>();
		Map<String, Long> companyData = new HashMap<String, Long>();
		Long minRevenue = new Long(0);
		Long maxRevenue = new Long(0);
		Long minEmployeeCount = new Long(0);
		Long maxEmployeeCount = new Long(0);
		if (revRange != null) {
			String[] revenueParts = revRange.split(" - ");

			if (revenueParts.length == 2) { // Ex response from Zoominfo:
				minRevenue = getRevenue(revenueParts[0]);
				maxRevenue = getRevenue(revenueParts[1]);

			} else if (revRange.contains("Over")) {
				String revenuePart = revRange;
				minRevenue = getRevenue(revenuePart);

			} else if (revRange.contains("Under")) {
				String revenuePart = revRange;
				maxRevenue = getRevenue(revenuePart);
			}
		} else {
			minRevenue = new Long(0);
			maxRevenue = new Long(0);
		}
		if (empRange != null) {
			String[] employeeCountParts = empRange.split(" - ");
			if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
				minEmployeeCount = Long.parseLong(employeeCountParts[0].trim().replace(",", ""));
				maxEmployeeCount = Long.parseLong(employeeCountParts[1].trim().replace(",", ""));

			} else if (empRange.contains("Over")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				minEmployeeCount = Long.parseLong(employeeCountPart);

			} else if (empRange.contains("Under")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				maxEmployeeCount = Long.parseLong(employeeCountPart);
			}
		} else {
			minEmployeeCount = new Long(0);
			maxEmployeeCount = new Long(0);
		}
		revenueEmployesData.add(minRevenue);
		revenueEmployesData.add(maxRevenue);
		revenueEmployesData.add(minEmployeeCount);
		revenueEmployesData.add(maxEmployeeCount);

		if (minRevenue > 0 && maxRevenue > 0) {
			if (minRevenue > maxRevenue) {
				companyData.put("minRevenue", maxRevenue);
				companyData.put("maxRevenue", minRevenue);
			} else {
				companyData.put("minRevenue", minRevenue);
				companyData.put("maxRevenue", maxRevenue);
			}
		} else {
			companyData.put("minRevenue", minRevenue);
			companyData.put("maxRevenue", maxRevenue);
		}

		companyData.put("minRevenue", minRevenue);
		companyData.put("maxRevenue", maxRevenue);
		companyData.put("minEmployeeCount", minEmployeeCount);
		companyData.put("maxEmployeeCount", maxEmployeeCount);
		return companyData;
	}

	private Long getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long y = new Double(value * million).longValue();
			return new Double(value * million).longValue();

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long x = new Double(value * billion).longValue();
			return new Double(value * billion).longValue();
		}
		return new Long(0);
	}


	private CampaignCriteria fetchZoomCampaignCriteria(Campaign campaign){
		CampaignCriteria cc = new CampaignCriteria();
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		cc = zoomInfoDataBuy.setCampaignCriteria(cc, expressions,campaign);
		String industry = cc.getIndustry();
		if(StringUtils.isNotBlank(industry)){
			List<String> l1 = new ArrayList<>(Arrays.asList(industry.split(",")));
			l1.remove("All");
			String newVal =   l1.stream().map(String::valueOf).collect(Collectors.joining(","));
			cc.setIndustry(newVal);
		}
		cc.setContactRequirements("5");
		if (campaign.isIgnorePrimaryIndustries()) {
			cc.setPrimaryIndustriesOnly(true);
		} else {
			cc.setPrimaryIndustriesOnly(false);
		}
		if (cc.getManagementLevel() == null || cc.getManagementLevel().isEmpty())
			cc.setManagementLevel("Non Management,MANAGER,DIRECTOR,VP_EXECUTIVES,C_EXECUTIVES,Board Members");

		if ((cc.getMinEmployeeCount() == null || cc.getMinEmployeeCount().isEmpty())
				&& (cc.getMaxEmployeeCount() == null || cc.getMaxEmployeeCount().isEmpty())) {
			cc.setMinEmployeeCount("0");
			cc.setMaxEmployeeCount("0");
		}
		if(campaign.isABM()){
			Pageable pageable = PageRequest.of(0, 9000);
			List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
			if(CollectionUtils.isNotEmpty(abmList)){
				List<String> abmCmpIds = abmList.stream().map(AbmListNew ::getCompanyId).collect(Collectors.toList());
				if(CollectionUtils.isNotEmpty(abmCmpIds)){
					abmCmpIds.removeIf(item -> item.equals("NOT_FOUND") || "GET_COMPANY_ID".equals(item));
				}
				//String abmDomainsList  = StringUtils.join(abmDomains, ",") ;
				cc.setAbmCompanyIds(abmCmpIds);
				List<String> abmDomains = abmList.stream().map(AbmListNew ::getDomain).collect(Collectors.toList());
				if(CollectionUtils.isNotEmpty(abmDomains)){
					abmDomains.removeIf(item -> item == null || "".equals(item));
				}
				List<String> abmCompanys = abmList.stream().map(AbmListNew ::getCompanyName).collect(Collectors.toList());
				if(CollectionUtils.isNotEmpty(abmCompanys)){
					abmCompanys.removeIf(item -> item == null || "".equals(item));
				}
				cc.setAbmCompanys(abmCompanys);
			}
		}
		return cc;
	}

	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		Map<String, String[]> revMap = XtaasUtils.getRevenueMap();
		Map<String, String[]> empMap = XtaasUtils.getEmployeeMap();
		List<Integer> minRevList = new ArrayList<Integer>();
		List<Integer> maxRevList = new ArrayList<Integer>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();
			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
			if (attribute.equalsIgnoreCase("INDUSTRY")) {
				List<String> l1 = new ArrayList<>(Arrays.asList(value.split(",")));
				l1.remove("All");
				String newVal =   l1.stream().map(String::valueOf).collect(Collectors.joining(","));
				cc.setIndustry(newVal);
			}
			if (attribute.equalsIgnoreCase("SIC_CODE")) {
				if (cc.getSicCode() != null && !cc.getSicCode().isEmpty()) {
					String nas = cc.getSicCode() + "," + value;
					cc.setSicCode(nas);
				} else {
					cc.setSicCode(value);
				}
			}

			if (attribute.equalsIgnoreCase("VALID_STATE")) {
				cc.setState(value);
			}
			if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
				cc.setTitleKeyWord(value);
			}
			if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
				cc.setNegativeKeyWord(value);
			}
			if (attribute.equalsIgnoreCase("STATE (US ONLY)")) {
				cc.setState(value);
			}

			if (attribute.equalsIgnoreCase("REVENUE")) {
				String[] revenueArr = value.split(",");
				cc.setSortedRevenuetSearchList(XtaasUtils.getRevenuSortedeMap(revenueArr));
				for (int i = 0; i < revenueArr.length; i++) {
					String revVal = revenueArr[i];
					String[] revArr = revMap.get(revVal);

					minRevList.add(Integer.parseInt(revArr[0]));
					maxRevList.add(Integer.parseInt(revArr[1]));
				}

				Collections.sort(minRevList);
				Collections.sort(maxRevList, Collections.reverseOrder());
				if (minRevList.contains(new Integer(0))) {

				} else if (minRevList.get(0) > 0) {
					cc.setMinRevenue(minRevList.get(0).toString());
				}
				if (maxRevList.contains(new Integer(0))) {

				} else if (maxRevList.get(0) > 0) {
					cc.setMaxRevenue(maxRevList.get(0).toString());
				}
			}

			if (attribute.equalsIgnoreCase("EMPLOYEE")) {
				String[] employeeArr = value.split(",");
				cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
				for (int i = 0; i < employeeArr.length; i++) {
					String empVal = employeeArr[i];
					String[] empArr = empMap.get(empVal);

					minEmpList.add(Integer.parseInt(empArr[0]));
					maxEmpList.add(Integer.parseInt(empArr[1]));
				}

				Collections.sort(minEmpList);
				Collections.sort(maxEmpList, Collections.reverseOrder());
				if (minEmpList.contains(new Integer(0))) {

				} else if (minEmpList.get(0) > 0) {
					cc.setMinEmployeeCount(minEmpList.get(0).toString());
				}
				if (maxEmpList.contains(new Integer(0))) {

				} else if (maxEmpList.get(0) > 0) {
					cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
				}
			}

		}
		if(campaign.isABM()){
			Pageable pageable = PageRequest.of(0, 9000);
			List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
			if(CollectionUtils.isNotEmpty(abmList)){
				List<String> abmCompany = abmList.stream().map(AbmListNew ::getCompanyId).collect(Collectors.toList());
				//String abmDomainsList  = StringUtils.join(abmDomains, ",") ;
				cc.setAbmCompanyIds(abmCompany);
				List<String> abmDomains = abmList.stream().map(AbmListNew ::getDomain).collect(Collectors.toList());
				cc.setAbmDomains(abmDomains);
			}
		}

		return cc;


	}

	private ZoomInfoResponse getCompanyResponse(String companyName){
		ZoomInfoResponse companyZoomInfoResponse = new ZoomInfoResponse();

		try{
			StringBuilder queryString= createCompanyCriteria(companyName,null,null);

			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			logger.info("\n Company previewUrl3 : "+previewUrl);
			URL url=new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; "
					+ "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response= new StringBuffer();
			while((inputline=bufferedReader.readLine())!=null)
			{
				response.append(inputline);
			}
			bufferedReader.close();
			companyZoomInfoResponse = getResponse(response,true);
			return companyZoomInfoResponse;
		}catch(Exception e){
			logger.error("Zoom response error : ", e);
			e.printStackTrace();
		}
		return companyZoomInfoResponse;

	}


	private String getCompanyId(String companyName){
		String zoomCompanyId = null;
		try{
			ZoomInfoResponse companyResponse = getCompanyResponse(companyName);
			if(companyResponse!=null && companyResponse.getCompanySearchResponse().getMaxResults()>0) {
				List<CompanyRecord> companyRecordsList = companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord();
				if(CollectionUtils.isNotEmpty(companyRecordsList) && companyRecordsList.size() > 0){
					zoomCompanyId = companyResponse.getCompanySearchResponse()
							.getCompanySearchResults().getCompanyRecord().get(0).getCompanyId();
				}
			}
		}catch (Exception e){
			logger.error("Error while fetching company id from zoom, getting exception ", e);
			e.printStackTrace();
		}
		return zoomCompanyId;
	}


	private void setCompanyId(Prospect prospect){
		if(StringUtils.isNotBlank(prospect.getCompany())){
			String companyId=	getCompanyId(prospect.getCompany());
			if(StringUtils.isNotBlank(companyId)){
				prospect.setCompanyId(companyId);
			}
		}
	}

}
