package com.xtaas.service;

import java.util.List;

import com.xtaas.web.dto.WebHookEventDTO;

public interface SendGridService {
	
	public void handleEmailNotificationEvent(List<WebHookEventDTO> webHookEvents);
	
	public void sendSendGridEmail();
	
}
