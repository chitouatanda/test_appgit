package com.xtaas.service;

import java.util.List;

public interface CountryNewService {

	public List<String> getStatesOfCountry(String countryName);

}
