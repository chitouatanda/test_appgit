package com.xtaas.service;

import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.domain.valueobject.RealTimePost;

public interface RealTimeDeliveryService {

	public void sendLeadToClient(ProspectCallLog prospectCallLogFromDB, RealTimePost realTimePost);

}
