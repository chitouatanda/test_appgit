package com.xtaas.service;

import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.service.TeamNoticeServiceImpl;
import com.xtaas.infra.zoominfo.service.DataSlice;
import com.xtaas.infra.zoominfo.service.RemoveSuccessDuplicates;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.OneOffDTO;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DataBuyCleanup implements Runnable{

  private static final Logger logger = LoggerFactory.getLogger(DataBuyCleanup.class);
  
  RemoveSuccessDuplicates removeSuccessDuplicates;
  
  DataSlice dataSlice;
  
  ProspectCallLogRepository prospectCallLogRepository;
  
  AbmListServiceImpl abmListServiceImpl;
  
  DataBuyQueueRepository dataBuyQueueRepository;
  private TeamNoticeServiceImpl teamNoticeServiceImpl;


    private DataBuyQueue dbq;

    private Campaign campaign;

    private Long buyRowCount;

    public DataBuyCleanup() {

    }


    public DataBuyCleanup(DataBuyQueue dbq, Campaign campaign, Long buyRowCount) {
        this.dbq = dbq;
        this.campaign = campaign;
        this.buyRowCount = buyRowCount;
        removeSuccessDuplicates = BeanLocator.getBean("removeSuccessDuplicates", RemoveSuccessDuplicates.class);
        dataSlice = BeanLocator.getBean("dataSlice", DataSlice.class);
        prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
        abmListServiceImpl = BeanLocator.getBean("abmListServiceImpl", AbmListServiceImpl.class);
        dataBuyQueueRepository = BeanLocator.getBean("dataBuyQueueRepository", DataBuyQueueRepository.class);
        teamNoticeServiceImpl = BeanLocator.getBean("teamNoticeServiceImpl", TeamNoticeServiceImpl.class);

    }


  public void dataCleanupAfterBuy(DataBuyQueue dbq, Campaign campaign,  Long buyRowCount) {
    //TODO : check for below values in parameter
   // List<ProspectCallLog> callableList = getCallableList(campaign);

    try {
      Long startTime = System.currentTimeMillis();
      logger.info("CleanUpProcess Start for CampaignId : [{}] StartTime : [{}]",campaign.getId(),getTime(startTime));
      logger.debug("Total no of Record Purchased for this Job : " + buyRowCount);
      Long successRemoved = removeSuccessDuplicates.removeSuccessLeads(campaign);
      buyRowCount = buyRowCount - successRemoved;
      List<String> campaignIds = new ArrayList<String>();
      campaignIds.add(campaign.getId());

     
      dataSlice.getQualityBucket(campaignIds);  // directly call api testing service method

      // Long tenCount =
      // tenCompanies.getTenProspectsForACompany(campaignCriteria.getCampaignId());
      // buyRowCount = buyRowCount - tenCount;
      /*if (campaignCriteria.getMultiprospectMap() != null && campaignCriteria.getMultiprospectMap().size() > 0) {
        Map<String, List<ProspectCallLog>> multiMap = new HashMap<String, List<ProspectCallLog>>();
        for (ProspectCallLog pc : callableList) {
          List<ProspectCallLog> pclList = multiMap.get(pc.getProspectCall().getProspect().getPhone());
          if (pclList == null) {
            pclList = new ArrayList<ProspectCallLog>();
          }
          pclList.add(pc);
          multiMap.put(pc.getProspectCall().getProspect().getPhone(), pclList);
        }
        for (Map.Entry<String, List<ProspectCallLog>> entry : multiMap.entrySet()) {
          List<ProspectCallLog> mpcList = entry.getValue();
          if (mpcList.size() == 5) {
            for (ProspectCallLog p : mpcList) {
              if (p.getProspectCall().getProspect().isDirectPhone()) {
                p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
                p.getProspectCall().getProspect().setDirectPhone(false);
                int qSort = p.getProspectCall().getQualityBucketSort();
                int firstTwo = qSort / 1000;
                int secondTemp = firstTwo % 10;
                int second = secondTemp * 1000;
                qSort = qSort - second;
                qSort = qSort + 2000;
                p.getProspectCall().setQualityBucketSort(qSort);
                prospectCallLogRepository.save(p);
              }
            }
          }
          if (mpcList.size() == 4) {
            for (ProspectCallLog p : mpcList) {
              if (p.getProspectCall().getProspect().isDirectPhone()) {
                p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
                p.getProspectCall().getProspect().setDirectPhone(false);
                int qSort = p.getProspectCall().getQualityBucketSort();
                int firstTwo = qSort / 1000;
                int secondTemp = firstTwo % 10;
                int second = secondTemp * 1000;
                qSort = qSort - second;
                qSort = qSort + 3000;
                p.getProspectCall().setQualityBucketSort(qSort);
                prospectCallLogRepository.save(p);
              }
            }

          }
          if (mpcList.size() == 3) {
            for (ProspectCallLog p : mpcList) {
              if (p.getProspectCall().getProspect().isDirectPhone()) {
                p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
                p.getProspectCall().getProspect().setDirectPhone(false);
                int qSort = p.getProspectCall().getQualityBucketSort();
                int firstTwo = qSort / 1000;
                int secondTemp = firstTwo % 10;
                int second = secondTemp * 1000;
                qSort = qSort - second;
                qSort = qSort + 4000;
                p.getProspectCall().setQualityBucketSort(qSort);
                prospectCallLogRepository.save(p);
              }
            }
          }
          if (mpcList.size() == 2) {
            for (ProspectCallLog p : mpcList) {
              if (p.getProspectCall().getProspect().isDirectPhone()) {
                p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
                p.getProspectCall().getProspect().setDirectPhone(false);
                int qSort = p.getProspectCall().getQualityBucketSort();
                int firstTwo = qSort / 1000;
                int secondTemp = firstTwo % 10;
                int second = secondTemp * 1000;
                qSort = qSort - second;
                qSort = qSort + 5000;
                p.getProspectCall().setQualityBucketSort(qSort);
                prospectCallLogRepository.save(p);
              }
            }
          }
          if (mpcList.size() == 1) {
            for (ProspectCallLog p : mpcList) {

              if (!p.getProspectCall().getProspect().isDirectPhone()) {
                p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
                int qSort = p.getProspectCall().getQualityBucketSort();
                int firstTwo = qSort / 1000;
                int secondTemp = firstTwo % 10;
                int second = secondTemp * 1000;
                qSort = qSort - second;
                qSort = qSort + 6000;
                p.getProspectCall().setQualityBucketSort(qSort);
                prospectCallLogRepository.save(p);
              }
            }
          }

        }

      }*/
      List<String> toemails = new ArrayList<String>();
      toemails.add("data@xtaascorp.com");
      if (dbq.getRequestorId() != null && !dbq.getRequestorId().isEmpty()) {
        Login l = abmListServiceImpl.getLogin(dbq.getRequestorId());
        if (l.getEmail() != null && !l.getEmail().isEmpty())
          toemails.add(l.getEmail());
      }

      DataBuyQueue dbqc = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
          DataBuyQueueStatus.CLEANUP_INPROCESS.toString(), dbq.getJobRequestTime());
      if (dbqc.getPurchasedCount() != null) {
        //dbqc.setPurchasedCount(dbqc.getPurchasedCount());
        dbqc.setZoomInfoPurchasedCount(buyRowCount);
      } else {
        dbqc.setPurchasedCount(buyRowCount);
//        dbqc.setZoomInfoPurchasedCount(buyRowCount);
      }
      //TODO commented for latch process marking it COMPLETED
      // dbqc.setStatus(DataBuyQueueStatus.COMPLETED.toString());
      dbqc.setJobEndTime(new Date());
      dbqc.setStatus(DataBuyQueueStatus.COMPLETED.toString());
      dbqc =  dataBuyQueueRepository.save(dbqc);
      String notice = "Data Buy for campaign "+  campaign.getName() + " was successfully completed with Data Buy Count: "+buyRowCount+" records";
      XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com", campaign.getName() + " Data purchase job completed",
              notice);

      if(campaign != null && CollectionUtils.isNotEmpty(campaign.getAdminSupervisorIds())){
        for(String supervisorId : campaign.getAdminSupervisorIds()){
          PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
          //String notice = campaign.getName()+" : Data Buy for campaign "+  campaign.getName() + " was successfully completed with Data Buy Count "+dbqc.getTotalBought();
          teamNoticeServiceImpl.saveUserNotices(supervisorId, notice);
        }
      }

      Long endTime = System.currentTimeMillis();
      logger.info("CleanUpProcess End for CampaignId : [{}] StartTime : [{}] EndTime : [{}] CleanUpProcessTime  : [{}] Sec",
              campaign.getId(),getTime(startTime),getTime(endTime),(endTime - startTime)/1000);

    } catch (Exception e) {
      DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
          DataBuyQueueStatus.CLEANUP_INPROCESS.toString(), dbq.getJobRequestTime());
      dbqe.setPurchasedCount(buyRowCount);
      StringWriter sw = new StringWriter();
      e.printStackTrace(new PrintWriter(sw));
      String exceptionAsString = sw.toString();
      // logger.debug(exceptionAsString);
      dbqe.setErrorMsg(exceptionAsString);
      if (dbqe.getJobretry() >= 3) {
        dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
        dbqe.setJobEndTime(new Date());
      } else {
        dbqe.setJobretry(dbqe.getJobretry() + 1);
        dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
        dbqe.setInterupted(true);
      }
      dataBuyQueueRepository.save(dbqe);
      e.printStackTrace();
    }
  }

  private List<ProspectCallLog> getCallableList(Campaign campaign ){

		//buyTotalData(campaign,runningCampaignIds,campaignCriteria);// bought the data with 2
		//Fetching the callables below
      List<ProspectCallLog> pList = prospectCallLogRepository.findCallableContacts(campaign.getId());
//		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
//		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList0);
//		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList1);
//		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList2);
//		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList3);
//		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList4);
//		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList5);
//		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList6);
//		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList7);
//		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList8);
//		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList9);
//		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,campaign.getId());
//		prospectCallLogList.addAll(prospectCallLogList10);

		return pList;
  }


    @Override
    public void run() {
        dataCleanupAfterBuy( this.dbq,  this.campaign,   this.buyRowCount);
    }

  private String getTime(Long time){
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    Date date = new Date(time);
    return simpleDateFormat.format(date);
  }
}
