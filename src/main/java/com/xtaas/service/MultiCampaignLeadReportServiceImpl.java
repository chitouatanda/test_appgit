package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Service
public class MultiCampaignLeadReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(MultiCampaignLeadReportServiceImpl.class);

	private static final String PRIMARY_CONTACT = "Primary Contact";

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	TeamRepository teamRepository;

	public void downloadMultiCampaignLeadReport(ProspectCallSearchDTO prospectCallSearchDTO, Login login)
			throws ParseException, Exception {
		try {
			logger.debug(
					"downloadMultiCampaignLeadReport() : Request to download multi campaign lead report for internal.");
			Organization organization = organizationRepository.findOneById(login.getOrganization());
			List<Campaign> campaigns = campaignService.getCampaignByIds(prospectCallSearchDTO.getCampaignIds());
			if (prospectCallSearchDTO.getCampaignIds().size() == 0) {
				throw new IllegalArgumentException("Campaign is mandatory for the lead report.");
			}
			ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
			ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
			List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
			if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
				LocalDate todaydate = LocalDate.now();
				LocalDate fromDate = todaydate.minusDays(7);
				prospectCallSearchDTO.setFromDate(fromDate.toString());
				prospectCallSearchDTO.setToDate(todaydate.toString());
				prospectCallSearchDTO.setStartMinute(0);
				prospectCallSearchDTO.setEndMinute(0);
			}

			if (prospectCallSearchDTO.getLeadStatus() != null && prospectCallSearchDTO.getLeadStatus().size() > 0) {
				List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
				List<String> successLeadStatus = new ArrayList<String>();
				List<String> failureLeadStatus = new ArrayList<String>();
				for (String lStatus : leadStatusList) {
					if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString())
							|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())) {
						successLeadStatus.add(lStatus);
					} else {
						failureLeadStatus.add(lStatus);
					}
				}
				if (successLeadStatus.size() > 0) {
					List<String> dispositionList = new ArrayList<String>(1);
					dispositionList.add("SUCCESS");
					successPCSDTO = prospectCallSearchDTO;
					successPCSDTO.setLeadStatus(successLeadStatus);
					successPCSDTO.setDisposition(dispositionList);
					successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
					List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
							.searchProspectCallByReport(successPCSDTO);
					prospectCallLogList.addAll(prospectCallLogSuccess);
				}
				if (failureLeadStatus.size() > 0) {
					List<String> dispositionList = new ArrayList<String>(1);
					dispositionList.add("FAILURE");
					failurePCSDTO = prospectCallSearchDTO;
					failurePCSDTO.setLeadStatus(null);
					failurePCSDTO.setDisposition(dispositionList);
					failurePCSDTO.setSubStatus(failureLeadStatus);
					successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
					List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl
							.searchProspectCallByReport(failurePCSDTO);
					prospectCallLogList.addAll(prospectCallLogFailure);
				}
			} else {
				successPCSDTO = prospectCallSearchDTO;
				successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(successPCSDTO);
				prospectCallLogList.addAll(prospectCallLogSuccess);
			}

			if (prospectCallLogList.size() <= 0) {
				logger.debug("No records found for given campaigns.");
			}
			Workbook workbook = new XSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("Leads");
			Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));

			String[] columns = createExcelHeaders();
			writeLeadsToExcelFile(columns, workbook, createHelper, sheet, campaigns, campaign, prospectCallLogList);

			String fileName = null;
			if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() == 1
					&& prospectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
				fileName = new Date().getTime() + "_DNCreport.xlsx";
			} else if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 1
					&& prospectCallSearchDTO.getSubStatus().contains("DNCL")) {
				fileName = new Date().getTime() + " _failurereport.xlsx";
			} else {
				fileName = new Date().getTime() + "_leadreport.xlsx";
			}

			File file = new File(fileName);
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			List<File> fileNames = new ArrayList<File>();
			fileNames.add(file);
			String message = "Please find the attached lead file.";
			workbook.close();
			boolean leadReportSent = false;

			String subject;
			if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() == 1
					&& prospectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
				subject = " Multi campaign DNC report file attached.";
			} else if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 1
					&& prospectCallSearchDTO.getSubStatus().contains("DNCL")) {
				subject = " Multi campaign failure report file attached.";
			} else {
				subject = " Multi campaign lead report file attached.";
			}
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

		
			if (loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
				List<String> emails = loginUser.getReportEmails();
				if (emails != null && emails.size() > 0) {
					for (String email : emails) {
						if (email != null && !email.isEmpty()) {
							XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, message, fileNames);
							logger.debug("Lead report sent successfully to : " + email);
							leadReportSent = true;
						}
					}
				}
			}else {
				XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", subject, message, fileNames);
			}
			if (!leadReportSent) {
				XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com", fileName + subject,
						message, fileNames);
				logger.debug("Lead report sent successfully to leadreports@xtaascorp.com");
			}
			logger.debug("Lead report file sent as attachment in the email");
		} catch (Exception e) {
			logger.debug("downloadDemandshoreLeadReport() : Error in CsvFileWriter!!!");
			e.printStackTrace();
		}
	}

	private String[] createExcelHeaders() {
		Map<String, String> colNames = new HashMap<String, String>();
		int i = 0;
		String[] columns = new String[1000];
		colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
		columns[i] = "ProspectCallId (for internal use only)";
		i++;
		colNames.put("Date", "Date");
		columns[i] = "Date";
		i++;
		colNames.put("Agent Name", "Agent Name");
		columns[i] = "Agent Name";
		i++;
		colNames.put("Partner Name", "Partner Name");
		columns[i] = "Partner Name";
		i++;
		colNames.put("Campaign Name", "Campaign Name");
		columns[i] = "Campaign Name";
		i++;
		colNames.put("Email ID", "Email ID");
		columns[i] = "Email ID";
		i++;
		colNames.put("Email Status", "Email Status");
		columns[i] = "Email Status";
		i++;
		colNames.put("Recording Url", "Recording Url");
		columns[i] = "Recording Url";
		i++;
		colNames.put("Lead Status", "Lead Status");
		columns[i] = "Lead Status";
		i++;
		colNames.put("C1.Reason", "C1.Reason");
		columns[i] = "C1.Reason";
		i++;
		colNames.put("C1.CallNotes", "C1.CallNotes");
		columns[i] = "C1.CallNotes";
		i++;
		colNames.put("Client Delivered", "Client Delivered");
		columns[i] = "Client Delivered";
		i++;
		colNames.put("Action", "Action");
		columns[i] = "Action";
		return columns;
	}

	private void writeLeadsToExcelFile(String[] columns, Workbook workbook, CreationHelper createHelper, Sheet sheet,
			List<Campaign> campaigns, Campaign campaign, List<ProspectCallLog> prospectCallLogList)
			throws ParseException {
		int recordsCounter = 0;
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		Row headerRow = sheet.createRow(0);

		for (int x = 0; x < columns.length; x++) {
			if (columns[x] != null && !columns[x].isEmpty()) {
				Cell cell = headerRow.createCell(x);
				cell.setCellValue(columns[x]);
				cell.setCellStyle(headerCellStyle);
			}
		}

		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		int rowNum = 1;
		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			Row row = sheet.createRow(rowNum++);
			ProspectCall prospectCall = prospectCallLog.getProspectCall();
			Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			Optional<Campaign> matchCampaign = campaigns.stream()
					.filter(filteredCampaign -> filteredCampaign.getId().equalsIgnoreCase(prospectCall.getCampaignId()))
					.findAny();

			for (int col = 0; col < columns.length; col++) {
				if (columns[col] != null && !columns[col].isEmpty()) {
					if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
						Cell cellx = row.createCell(col);
						if (prospectCall != null && prospectCall.getProspectCallId() != null) {
							cellx.setCellValue(prospectCall.getProspectCallId());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Date")) {
						Cell cellx = row.createCell(col);
						DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
						pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
						if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
							String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
							Date pstDate = pstFormat.parse(pstStr);
							cellx.setCellValue(pstFormat.format(pstDate));
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Agent Name")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getProspectCall().getAgentId() != null) {
							cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Partner Name")) {
						if (prospectCallLog.getProspectCall().getPartnerId() != null) {
							Cell cellx = row.createCell(col);
							if (prospectCallLog.getProspectCall().getPartnerId() != null) {
								cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
								cellx.setCellStyle(dateCellStyle);
							}
						}
					} else if (columns[col].contentEquals("Campaign Name")) {
						String campaignName = null;
						Cell cellx = row.createCell(col);
						campaignName = (matchCampaign.isPresent()) ? matchCampaign.get().getName() : "";
						cellx.setCellValue(campaignName);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Email ID")) {
						if (prospect.getEmail() != null) {
							Cell cellx = row.createCell(col);
							cellx.setCellValue(prospect.getEmail());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("Email Status")) {
						String isMailBounced = "";
						if (prospectCall.isEmailBounce()) {
							isMailBounced = "BOUNCED";
						} else {
							isMailBounced = "NOT-BOUNCED";
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(isMailBounced);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Recording Url")) {
						Cell cellx = row.createCell(col);
						if (prospectCall.getRecordingUrlAws() != null) {
							cellx.setCellValue(prospectCall.getRecordingUrlAws());
						} else {
							if (prospectCall.getRecordingUrlAws() == null
									|| prospectCall.getRecordingUrlAws().isEmpty()) {
								if (prospectCall.getRecordingUrl() == null
										|| prospectCall.getRecordingUrl().isEmpty()) {
									cellx.setCellValue("");
								} else {
									String rUrl = prospectCall.getRecordingUrl();
									int lastIndex = rUrl.lastIndexOf("/");
									String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
									String rdfile = xfileName + ".wav";
									prospectCall.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
									cellx.setCellValue(prospectCall.getRecordingUrlAws());
								}
							}
						}
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Lead Status")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getProspectCall().getLeadStatus() != null) {
							cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus().toString());
							cellx.setCellStyle(dateCellStyle);
						} else {
							cellx.setCellValue(prospectCallLog.getProspectCall().getSubStatus().toString());
							cellx.setCellStyle(dateCellStyle);
						}
					} else if (columns[col].contentEquals("C1.Reason")) {
						String qaReason = "";
						if (prospectCallLog.getQaFeedback() != null
								&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
							List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
									.getFeedbackResponseList();
							for (FeedbackSectionResponse fsr : fsrList) {
								if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
									List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
									for (FeedbackResponseAttribute fra : fraList) {
										if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
											if (fra.getRejectionReason() != null) {
												qaReason = qaReason + fra.getRejectionReason();
											}
										}
									}
								}
							}
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(qaReason);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("C1.CallNotes")) {
						String callnotes = "";
						if (prospectCallLog.getQaFeedback() != null
								&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
							List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
									.getFeedbackResponseList();
							for (FeedbackSectionResponse fsr : fsrList) {
								if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
									List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
									for (FeedbackResponseAttribute fra : fraList) {
										if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
											if (fra.getAttributeComment() != null) {
												callnotes = fra.getAttributeComment();
											}
										}
									}
								}
							}
						}
						Cell cellx = row.createCell(col);
						cellx.setCellValue(callnotes);
						cellx.setCellStyle(dateCellStyle);
					} else if (columns[col].contentEquals("Client Delivered")) {
						Cell cellx = row.createCell(col);
						if (prospectCallLog.getProspectCall().getClientDelivered() != null
								&& !prospectCallLog.getProspectCall().getClientDelivered().isEmpty()) {
							cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
						} else {
							cellx.setCellValue("");
							cellx.setCellStyle(dateCellStyle);
						}
					}
				}
			}
			recordsCounter++;
		}
	}

}
