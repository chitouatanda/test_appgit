package com.xtaas.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
// import com.xtaas.application.service.SpeechToTextTranscribe;
import com.xtaas.db.repository.VoiceMailRepository;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.TranscribeCriteriaDTO;

@Service
public class TranscriptionServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(TranscriptionServiceImpl.class);

	@Autowired
	private VoiceMailRepository voiceMailRepository;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private CallClassificationJobQueueRepository callClassificationJobQueueRepository;

	// @Autowired
	// SpeechToTextTranscribe speechToTextTranscribe;

	public void transcriptVoiceMails() {
		Date startDate = XtaasDateUtils.getDateAtTime(new Date(), 0, 0, 0, 0);
		Date endDate = XtaasDateUtils.getDateAtTime(new Date(), new Date().getHours(), 0, 0, 0);
		transcribeVoiceMail(getVoiceMails(startDate, endDate));
	}

	private String createAwsUrl(String recordingUrl) {
		int lastIndex = recordingUrl.lastIndexOf("/");
		String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
		String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + fileName;
		return awsRecordingUrl;
	}

	private List<VoiceMail> getVoiceMails(Date startDate, Date endDate) {
		List<VoiceMail> voiceMailsFewHoursAgo = voiceMailRepository.getVoiceMailsOfGivenHours("XTAAS CALL CENTER",
				false, startDate, endDate);
		logger.debug("getVoiceMails() :: Found " + voiceMailsFewHoursAgo.size()
				+ " voicemails for this hour to transcribe.");
		return voiceMailsFewHoursAgo;
	}

	private void transcribeVoiceMail(List<VoiceMail> voiceMails) {
		if (voiceMails.size() > 0) {
			for (VoiceMail voiceMail : voiceMails) {
				try {
//					speechToTextTranscribe.convertSpeechToText(createAwsUrl(voiceMail.getCallParams().get("RecordUrl")),
//							voiceMail);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			logger.debug("No voicemails found for this hour.");
		}

	}
	
	public String transcribeBasedOnCriteria(TranscribeCriteriaDTO transcribeCriteriaDTO) throws ParseException {
		try {
			long count = prospectCallLogRepository.getCountOfProspectsForTranscription(transcribeCriteriaDTO);
			if (count == 0) {
				return "No records found from db for transcription.";
			}
			Runnable runnable = () -> {
				List<ProspectCallLog> prospectsFromDB = null;
				try {
					prospectsFromDB = prospectCallLogRepository.getProspectsForTranscription(transcribeCriteriaDTO);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				logger.info(new SplunkLoggingUtils("Criteria based transcription API for campaign", "")
						.eventDescription("Transcription of prospects in campaign")
						.methodName("transcribeBasedOnCriteria")
						.processName("Criteria based transcription API for campaign")
						.addField("transcribeCriteria", transcribeCriteriaDTO.toString()).build());

				if (prospectsFromDB != null && prospectsFromDB.size() > 0) {
					for (ProspectCallLog singleProspect : prospectsFromDB) {
						if (singleProspect != null && singleProspect.getProspectCall() != null
								&& singleProspect.getProspectCall().getRecordingUrlAws() != null) {
							String phone = singleProspect.getProspectCall().getProspect().getPhone();
							callClassificationJobQueueRepository.save(new CallClassificationJobQueue()
									.withStatus(CallClassificationJobQueue.Status.TRANSCRIBE_QUEUED)
									.withMessageLifecycleId(UUID.randomUUID().toString()).withTtl(3)
									.withProducer("TranscriptionServiceImpl")
									.withProducerServerId(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
									.withNamespace(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
									.withJobCreationTime(new Date())
									.withCampaignId(singleProspect.getProspectCall().getCampaignId())
									.withProspectCallLogId(singleProspect.getId())
									.withProspectCallId(singleProspect.getProspectCall().getProspectCallId())
									.withRecordingUrl(singleProspect.getProspectCall().getRecordingUrlAws())
									.withCallDuration(singleProspect.getProspectCall().getCallDuration())
									.withPhone(phone));
						}
					}
				}
			};
			Thread thread = new Thread(runnable);
			thread.start();
		} catch (Exception e) {
			logger.error(new SplunkLoggingUtils("Criteria based transcription API for campaign", "")
					.eventDescription("Error occurred in transcribeBasedOnCriteria")
					.methodName("transcribeBasedOnCriteria")
					.processName("Criteria based transcription API for campaign").addThrowableWithStacktrace(e)
					.build());
		}
		return "Transcription started successfully.";
	}
}