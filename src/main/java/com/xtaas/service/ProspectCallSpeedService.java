package com.xtaas.service;

import com.xtaas.db.entity.ProspectCallSpeedLog;

public interface ProspectCallSpeedService {

	public ProspectCallSpeedLog save(ProspectCallSpeedLog prospectCallSpeedLog);
	
}
