/**
 *
 */
package com.xtaas.service;

import com.xtaas.BeanLocator;

/**
 * This is a wrapper class over ApplicationEnvironmentPropertyService for ease
 * of use, encapsulating instantiation for
 * ApplicationEnvironmentPropertyService.
 *
 */
public class ApplicationEnvironmentPropertyUtils {

	private static ApplicationEnvironmentPropertyService envPropertyService;

	private static ApplicationEnvironmentPropertyService getApplicationEnvironmentPropertyService() {
		if (envPropertyService == null) {
			envPropertyService = BeanLocator.getBean("applicationEnvironmentPropertyService",
					ApplicationEnvironmentPropertyService.class);
		}
		return envPropertyService;
	}

	public static String getTwimlDialAppSID() {
		return getApplicationEnvironmentPropertyService().getTwimlDialAppSID();
	}

	public static String getTwimlWhisperAppSID() {
		return getApplicationEnvironmentPropertyService().getTwimlWhisperAppSID();
	}

	public static String getServerBaseUrl() {
		return getApplicationEnvironmentPropertyService().getServerBaseUrl();
	}

	public static String getDialerUrl() {
		return getApplicationEnvironmentPropertyService().getDialerUrl();
	}

	public static String getAMQPUrl() {
		return getApplicationEnvironmentPropertyService().getAMQPUrl();
	}

	public static String getPusherApplicationId() {
		return getApplicationEnvironmentPropertyService().getPusherApplicationId();
	}

	public static String getPusherApplicationKey() {
		return getApplicationEnvironmentPropertyService().getPusherApplicationKey();
	}

	public static String getPusherApplicationSecret() {
		return getApplicationEnvironmentPropertyService().getPusherApplicationSecret();
	}

	public static String getSalesforceAppConsumerKey() {
		return getApplicationEnvironmentPropertyService().getSalesforceAppConsumerKey();
	}

	public static String getSalesforceAppConsumerSecret() {
		return getApplicationEnvironmentPropertyService().getSalesforceAppConsumerSecret();
	}

	public static String getTwimlDialXmlUrl() {
		return getApplicationEnvironmentPropertyService().getTwimlDialXmlUrl();
	}

	public static String getThinQDomainUrl() {
		return getApplicationEnvironmentPropertyService().getThinQDomainUrl();
	}

	public static String getThinQId() {
		return getApplicationEnvironmentPropertyService().getThinQId();
	}

	public static String getThinQToken() {
		return getApplicationEnvironmentPropertyService().getThinQToken();
	}

	public static String getTelnyxDomainUrl() {
		return getApplicationEnvironmentPropertyService().getTelnyxDomainUrl();
	}

	public static String getTelnyxToken() {
		return getApplicationEnvironmentPropertyService().getTelnyxToken();
	}

	public static String getBssServerBaseUrl() {
		return getApplicationEnvironmentPropertyService().getBssServerBaseUrl();
	}

	public static String getTwilioAccountSid() {
		return getApplicationEnvironmentPropertyService().getTwilioAccountSid();
	}

	public static String getTwilioAuthToken() {
		return getApplicationEnvironmentPropertyService().getTwilioAuthToken();
	}

	public static String getPlivoAuthId() {
		return getApplicationEnvironmentPropertyService().getPlivoAuthId();
	}

	public static String getPlivoAuthToken() {
		return getApplicationEnvironmentPropertyService().getPlivoAuthToken();
	}

	public static String getPlivoAppId() {
		return getApplicationEnvironmentPropertyService().getPlivoAppId();
	}

	public static String getGeoCodeApiUrl() {
		return getApplicationEnvironmentPropertyService().getGeoCodeApiUrl();
	}

	public static String getAwsAccessKeyId() {
		return getApplicationEnvironmentPropertyService().getAwsAccessKeyId();
	}

	public static String getAwsSecretKey() {
		return getApplicationEnvironmentPropertyService().getAwsSecretKey();
	}

	public static String getVivaAccountId() {
		return getApplicationEnvironmentPropertyService().getVivaAccountId();
	}

	public static String getVivaPasscode() {
		return getApplicationEnvironmentPropertyService().getVivaPasscode();
	}

	public static String getVivaDomain() {
		return getApplicationEnvironmentPropertyService().getVivaDomain();
	}

	public static String getTelnyxConnectionId() {
		return getApplicationEnvironmentPropertyService().getTelnyxConnectionId();
	}
	
	public static String getLeadIQAPIKey() {
		return getApplicationEnvironmentPropertyService().getLeadIQAPIKey();
	}
	
	public static String getLeadIQUrl() {
		return getApplicationEnvironmentPropertyService().getLeadIQUrl();
	}
	
	public static String getVivaTelnyxAccountId() {
		return getApplicationEnvironmentPropertyService().getVivaTelnyxAccountId();
	}

	public static String getVivaTelnyxPasscode() {
		return getApplicationEnvironmentPropertyService().getVivaTelnyxPasscode();
	}

	public static String getVivaTelnyxDomain() {
		return getApplicationEnvironmentPropertyService().getVivaTelnyxDomain();
	}
	
	public static String getSamespaceUsername() {
		return getApplicationEnvironmentPropertyService().getSamespaceUsername();
	}
	
	public static String getSamespacePassword() {
		return getApplicationEnvironmentPropertyService().getSamespacePassword();
	}
	
	public static String getSamespaceDomain() {
		return getApplicationEnvironmentPropertyService().getSamespaceDomain();
	}
	public static String getSignalwireDomainApp() {
		return getApplicationEnvironmentPropertyService().getSignalwireDomainApp();
	}

	public static String getSignalwireProjectId() {
		return getApplicationEnvironmentPropertyService().getSignalwireProjectId();
	}

	public static String getSignalwireAuthToken() {
		return getApplicationEnvironmentPropertyService().getSignalwireAuthToken();
	}

	public static String getSignalwireTelnyxToken() {
		return getApplicationEnvironmentPropertyService().getSignalwireTelnyxToken();
	}

	public static String getPlivoSIPDomain() {
		return getApplicationEnvironmentPropertyService().getPlivoSIPDomain();
	}

	public static String getPlivoSIPUsername() {
		return getApplicationEnvironmentPropertyService().getPlivoSIPUsername();
	}

	public static String getPlivoSIPPassword() {
		return getApplicationEnvironmentPropertyService().getPlivoSIPPassword();
	}

	public static String getSignalwireVivaSIPDomain() {
		return getApplicationEnvironmentPropertyService().getSignalwireVivaSIPDomain();
	}

	public static String getSignalwireVivaSIPAccountId() {
		return getApplicationEnvironmentPropertyService().getSignalwireVivaSIPAccountId();
	}

	public static String getSignalwireVivaSIPPassword() {
		return getApplicationEnvironmentPropertyService().getSignalwireVivaSIPPassword();
	}
	
	public static String getSignalwireThinQSipToken() {
		return getApplicationEnvironmentPropertyService().getSignalwireThinQSipToken();
	}

}
