package com.xtaas.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.*;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.utils.XtaasUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.EntityMappingRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.db.repository.ZoomTokenRepository;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CurrentEmployment;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.utils.XtaasConstants;

@Service
public class ZoomInfoSearchServiceImpl implements ZoomInfoSearchService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ZoomInfoSearchServiceImpl.class);

	private String partnerKey = "Xtaascorp.partner";

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private EntityMappingRepository entityMappingRepository;

	@Autowired
	private ZoomInfoDataBuy zoomInfoDataBuy;

	@Autowired
	AbmListNewRepository abmListNewRepository;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
    private PhoneDncService phoneDncService;

	String mTokenKey = "";

	Date mCachedDate;

	@Override
	public Prospect getZoomProspect(Prospect prospect) {
		prospect = getPurchaseResponse(prospect);
		return prospect;
	}

	@Override
	public Prospect getCampaignZoomProspect(Prospect prospect, Campaign campaign) {
		prospect = getPurchaseCampaignResponse(prospect, campaign);
		return prospect;
	}


	private String getZoomOAuth() {
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (mCachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}

		Date currDate = new Date();
		long duration = currDate.getTime() - mCachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}
		return mTokenKey;
	}

	private ZoomInfoResponse getZoomInfoResponse(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent",
				"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
		// int responseCode= connection.getResponseCode();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputline;
		StringBuffer response = new StringBuffer();
		while ((inputline = bufferedReader.readLine()) != null) {
		/*	if(inputline.contains("Authentication Failed") || inputline.contains("You have exceeded your API query rate limit")) {
				try {
					Thread.sleep(2000);
					getZoomInfoResponse(url);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}*/

			response.append(inputline);
		}
		bufferedReader.close();
		ZoomInfoResponse zoomInfoResponse = getResponse(response, false);
		return zoomInfoResponse;
	}

	private ZoomInfoResponse getResponse(StringBuffer response, boolean isCompanySearch) throws IOException {
		int countCA = 0;
		if (!response.toString().contains("personId") && !isCompanySearch) {
			return null;
		}
		String ignore = "\"companyAddress\":\"\"";
		if (response.toString().contains(ignore)) {
			countCA = StringUtils.countMatches(response.toString(), ignore);
		}

		String rep = "\"companyAddress\":{}";
		// String ignorewith = "CurrentEmployment\":[";
		for (int i = 0; i < countCA; i++) {

			int xindex = response.indexOf(ignore);
			String sub = response.substring(xindex);
			String sub1 = sub.substring(0, 19);
			int startindex = response.indexOf(sub1);
			int endindex = startindex + sub1.length();
			response.replace(xindex, endindex, rep);
			// logger.debug(response.toString());
		}
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null) {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
			return zoomInfoResponse;
		} else {
			return null;
		}
	}

	private String buildUrl(String queryString, int offset, int size, String personId) throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		logger.debug("Key : " + key);
		if (personId != null) {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&PersonID=" + personId
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key=" + key;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,companyAddress,companyRevenueNumeric,companyRevenueRange,companyEmployeeRange,companyEmployeeCount&key="
					+ key + "&rpp=" + size + "&page=" + offset;
			return url;
		}
	}

	private ZoomInfoResponse preparePersonSearch(Prospect prospect, boolean isDirectSearch) {
		int pageNumber = 1;
		URL url;
		int size = 25;
		try {
			String queryString = buildQueryString(prospect, isDirectSearch);
			String previewUrl = buildUrl(queryString, pageNumber, size, null);
			logger.info("\n Primary prospect previewUrl1: " + previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
			return personZoomInfoResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Prospect getPurchaseResponse(Prospect prospect) {

		try {
			boolean isDirectSearch = true;
			// first search for direct
			ZoomInfoResponse personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch);
			// if you dont get direct then search for indirect number;
			if (personZoomInfoResponse == null || personZoomInfoResponse.getPersonSearchResponse() == null
					|| personZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
				isDirectSearch = false;
				personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch);
			}

			if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
				int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();

				if (totalResult > 0 && personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
						.getPersonRecord() != null) {
					List<PersonRecord> list = personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
							.getPersonRecord();
					if (list != null && list.size() > 0) {
						PersonRecord personRecord = list.get(0);
						String personDetailRequestUrl = buildUrl(null, 0, 0, personRecord.getPersonId());
						URL detailUrl = new URL(personDetailRequestUrl); // purchase request
						ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(detailUrl);
						if (zoomInfoResponseDetail.getPersonDetailResponse() != null
								&& zoomInfoResponseDetail.getPersonDetailResponse().getFirstName() != null
								&& zoomInfoResponseDetail.getPersonDetailResponse().getLastName() != null
								&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getFirstName())
								&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getLastName())
								&& (zoomInfoResponseDetail.getPersonDetailResponse().getDirectPhone() != null
										|| zoomInfoResponseDetail.getPersonDetailResponse()
												.getCompanyPhone() != null)) {

							PersonDetailResponse personDetailResponse = zoomInfoResponseDetail
									.getPersonDetailResponse();
							List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
							int industryListSize = 0;
							if (industryList != null) {
								industryListSize = industryList.size();
							}
							List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
							List<String> topLevelIndustries = new ArrayList<String>();
							String zoomCountry = "United States";
							Prospect prospectInfo = new Prospect();
							if (personDetailResponse.getLocalAddress() != null
									&& personDetailResponse.getLocalAddress().getCountry() != null) {
								if (personDetailResponse.getLocalAddress().getStreet() != null)
									prospectInfo.setAddressLine1(personDetailResponse.getLocalAddress().getStreet());
								else
									prospectInfo.setAddressLine1("");
								if (personDetailResponse.getLocalAddress().getZip() != null)
									prospectInfo.setZipCode(personDetailResponse.getLocalAddress().getZip());
								else
									prospectInfo.setZipCode("");
								if (personDetailResponse.getLocalAddress().getCity() != null)
									prospectInfo.setCity(personDetailResponse.getLocalAddress().getCity());
								else
									prospectInfo.setCity("");
								// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
								// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
							} else if (personDetailResponse.getCurrentEmployment().get(0).getCompany()
									.getCompanyAddress() != null
									&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress().getCountry() != null) {
								prospectInfo.setAddressLine1(personDetailResponse.getCurrentEmployment().get(0)
										.getCompany().getCompanyAddress().getStreet());
								prospectInfo.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany()
										.getCompanyAddress().getZip());
								prospectInfo.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany()
										.getCompanyAddress().getCity());
							}
							if (personDetailResponse.getLastUpdatedDate() != null
									&& !personDetailResponse.getLastUpdatedDate().isEmpty()) {
								String ld = personDetailResponse.getLastUpdatedDate();
								if (ld.contains("-"))
									ld = ld.replace("-", "/");
								Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);

								prospectInfo.setLastUpdatedDate(date1);
							}
							if (personDetailResponse.getLocalAddress() != null) {
								prospectInfo.setCountry(personDetailResponse.getLocalAddress().getCountry());
								zoomCountry = personDetailResponse.getLocalAddress().getCountry();
							} else if (personDetailResponse.getCurrentEmployment() != null
									&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
									&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress() != null
									&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress().getCountry() != null) {
								prospectInfo.setCountry(personDetailResponse.getCurrentEmployment().get(0).getCompany()
										.getCompanyAddress().getCountry());
								zoomCountry = personDetailResponse.getCurrentEmployment().get(0).getCompany()
										.getCompanyAddress().getCountry();
							}

							if (prospectInfo.getCountry().equalsIgnoreCase("USA")
									|| prospectInfo.getCountry().equalsIgnoreCase("United States")
									|| prospectInfo.getCountry().equalsIgnoreCase("US")
									|| prospectInfo.getCountry().equalsIgnoreCase("Canada")) {
								prospectInfo.setPhone("+1"+getUSPhone(personDetailResponse));
								prospectInfo.setExtension(getExtension(personDetailResponse));
								// String phoneType =
								// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
								// prospect.setPhoneType(phoneType);
								prospectInfo.setCompanyPhone(getUSCompanyPhone(personDetailResponse));
								prospectInfo.setStateCode(getStateCode(personDetailResponse));
							} else if (prospectInfo.getCountry().equalsIgnoreCase("United Kingdom")
									|| prospectInfo.getCountry().equalsIgnoreCase("UK")) {
								prospectInfo.setPhone(getUKPhone(personDetailResponse));
								prospectInfo.setExtension(getExtension(personDetailResponse));
								// String phoneType =
								// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
								// prospect.setPhoneType(phoneType);
								prospectInfo.setCompanyPhone(getUKCompanyPhone(personDetailResponse));
								prospectInfo.setStateCode(getUKStateCode(personDetailResponse));
							} else {
								prospectInfo.setPhone(getPhone(personDetailResponse));
								prospectInfo.setExtension(getExtension(personDetailResponse));
								// String phoneType =
								// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
								// prospect.setPhoneType(phoneType);
								prospectInfo.setCompanyPhone(getCompanyPhone(personDetailResponse));
								prospectInfo.setStateCode(getStateCode(personDetailResponse));
							}

							if (industryListSize > 0) {
								prospectInfo.setIndustry(industryList.get(0));
								prospectInfo.setIndustryList(industryList);
							}
							
							prospectInfo.setEu(personDetailResponse.isEU());

							String title = personRecord.getCurrentEmployment().getJobTitle();
							String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
							String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
							String empRange = personRecord.getCurrentEmployment().getCompany()
									.getCompanyEmployeeCountRange();
							long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();
							long revCountInZeros = personRecord.getCurrentEmployment().getCompany()
									.getCompanyRevenueIn000s();
							String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
							ZoomInfoResponse companyResponse = getCompanyResponse(null,
									personRecord.getCurrentEmployment().getCompany().getCompanyId());

							Map<String, Object> customAttributes = new HashMap<String, Object>();
							Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);

							customAttributes.put("minRevenue", revenueEmployeeList.get("minRevenue"));
							customAttributes.put("maxRevenue", revenueEmployeeList.get("maxRevenue"));

							customAttributes.put("maxEmployeeCount", revenueEmployeeList.get("maxEmployeeCount"));
							customAttributes.put("minEmployeeCount", revenueEmployeeList.get("minEmployeeCount"));

							String detailDomain = personDetailResponse.getCurrentEmployment().get(0).getCompany()
									.getCompanyWebsite();
							if (detailDomain != null && !detailDomain.isEmpty()) {
								customAttributes.put("domain", detailDomain);
							} else {
								customAttributes.put("domain", domain);
							}
							customAttributes.put("revCount", revCount);
							customAttributes.put("revCountIn000s", new Long(revCountInZeros));
							customAttributes.put("empCount", new Long(empCount));
							prospectInfo.setCustomAttributes(customAttributes);

							prospectInfo.setCompany(getOrganizationName(zoomInfoResponseDetail));
							if (personDetailResponse.getCurrentEmployment().get(0).getManagementLevel() != null)
								prospectInfo.setManagementLevel(
										personDetailResponse.getCurrentEmployment().get(0).getManagementLevel().get(0));

							String timez = getTimeZone(prospect);
							prospectInfo.setTimeZone(timez);
							// prospectInfo.setCompanyAddress(personDetailResponse.get);

							prospectInfo.setDepartment(getJobFunction(zoomInfoResponseDetail));

							prospectInfo.setDirectPhone(isDirectSearch);
							if(personDetailResponse.getEmailAddress() != null && !personDetailResponse.getEmailAddress().isEmpty()) {
								prospectInfo.setEmail(personDetailResponse.getEmailAddress());
							}else {
								if(prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
									prospectInfo.setEmail(prospect.getEmail());
								}
							}

							// prospectInfo.setEu(personDetailResponse.get);
							prospectInfo.setFirstName(personDetailResponse.getFirstName());

							prospectInfo.setLastName(personDetailResponse.getLastName());
							// prospectInfo.setPrefix(prefix);
							// prospectInfo.setProspectType(prospectType);
							prospectInfo.setSourceType("INTERNAL");
							prospectInfo.setSourceCompanyId(getOrganizationId(zoomInfoResponseDetail));
							prospectInfo.setSourceId(personDetailResponse.getPersonId());
							prospectInfo.setSource("ZOOINFO");
							// prospectInfo.setStatus(status);
							prospectInfo.setTitle(title);
							prospectInfo.setTopLevelIndustries(topLevelIndustries);
							prospectInfo.setZoomPersonUrl(personDetailResponse.getZoomPersonUrl());

							if (companyResponse.getCompanySearchResponse() != null
									&& companyResponse.getCompanySearchResponse().getCompanySearchResults() != null
									&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
											.getCompanyRecord() != null
									&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
											.getCompanyRecord().get(0) != null) {

								if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getZoomCompanyUrl() != null) {
									prospectInfo.setZoomCompanyUrl(companyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getZoomCompanyUrl());
								}

								if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanyNAICS() != null) {
									prospectInfo.setCompanyNAICS(companyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS());
								}
								if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanySIC() != null) {
									prospectInfo.setCompanySIC(companyResponse.getCompanySearchResponse()
											.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC());
								}

							}
							return prospectInfo;
						}

					}
				}

			} else {
				logger.error("no data found from zoom for manual search for search parameters inzoominfo : ");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private ZoomInfoResponse getCompanyResponse(String companyName, String companyId) {
		ZoomInfoResponse companyZoomInfoResponse = new ZoomInfoResponse();

		try {
			StringBuilder queryString = createCompanyCriteria(companyName, companyId, companyId);

			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			logger.debug("\n previewUrl3 : " + previewUrl);
			URL url = new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				/*if(inputline.contains("Authentication Failed") || inputline.contains("You have exceeded your API query rate limit")) {
					try {
						Thread.sleep(2000);
						getCompanyResponse( companyName, companyId);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}*/
				response.append(inputline);
			}
			bufferedReader.close();
			companyZoomInfoResponse = getResponse(response, true);
			return companyZoomInfoResponse;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return companyZoomInfoResponse;

	}

	private String getOrganizationId(ZoomInfoResponse zoomInfoResponseDetail) {
		String companyId = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					companyId = currentEmployment.getCompany().getCompanyId();
					break;
				}
			}
		}

		return companyId;
	}

	public String getTimeZone(Prospect prospect) {
		// List<OutboundNumber> outboundNumberList;
		// int areaCode = Integer.parseInt(findAreaCode(prospect.getPhone()));
		if (prospect != null && prospect.getStateCode() != null  && !prospect.getStateCode().isEmpty() ) {

			StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(prospect.getStateCode());
			return stateCallConfig.getTimezone();
		}
		String s = prospect.getFirstName() + "" + prospect.getLastName();
		logger.debug("prospect State is null : " + s);
		// if(stateCallConfig==null){
		return null;
		// }

		// find a random number from the list

	}

	private String getOrganizationName(ZoomInfoResponse zoomInfoResponseDetail) {
		String orgName = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					orgName = currentEmployment.getCompany().getCompanyName();
					break;
				}
			}
		}

		return orgName;
	}

	private String getJobFunction(ZoomInfoResponse zoomInfoResponseDetail) {
		String jobFunction = null;
		try {
			if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
				for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
						.getCurrentEmployment()) {
					if (currentEmployment.getJobFunction() != null) {
						jobFunction = currentEmployment.getJobFunction();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			logger.error("Department null :" + e);
			jobFunction = "_";
		}
		return jobFunction;
	}

	private String buildQueryString(Prospect prospect, boolean isDirectSearch) {
		StringBuilder sb = new StringBuilder();
		try {
			if (prospect != null && prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
				sb.append("companyPastOrPresent=1");
				sb.append("&ValidDateMonthDist=" + URLEncoder.encode("24", XtaasConstants.UTF8));// checking for 2 years
				sb.append("&ContactRequirements=" + URLEncoder.encode("2", XtaasConstants.UTF8));// require phone,
				sb.append("&EmailAddress=" + URLEncoder.encode(prospect.getEmail(), XtaasConstants.UTF8));
			} else {
				if (prospect != null && prospect.getFirstName() != null && !prospect.getFirstName().isEmpty()
						&& prospect.getLastName() != null && !prospect.getLastName().isEmpty()) {
					String fullName = prospect.getFirstName() + " " + prospect.getLastName();
					sb.append("fullName=" + URLEncoder.encode(fullName, XtaasConstants.UTF8));
				} else if (prospect != null && prospect.getFirstName() != null && !prospect.getFirstName().isEmpty()) {
					sb.append("firstName=" + URLEncoder.encode(prospect.getFirstName(), XtaasConstants.UTF8));
				} else if (prospect != null && prospect.getLastName() != null && !prospect.getLastName().isEmpty()) {
					sb.append("lastName=" + URLEncoder.encode(prospect.getLastName(), XtaasConstants.UTF8));
				}

				if (prospect != null && prospect.getTitle() != null && !prospect.getTitle().isEmpty()) {
					sb.append("&personTitle=" + URLEncoder.encode(prospect.getTitle(), XtaasConstants.UTF8));
				}
				if (prospect != null && prospect.getCompanyId() != null && !prospect.getCompanyId().isEmpty()) {
					sb.append("&companyIds=" + URLEncoder.encode(prospect.getCompanyId(), XtaasConstants.UTF8));
				} else if (prospect != null && prospect.getCompany() != null && !prospect.getCompany().isEmpty()) {
					sb.append("&companyName=" + URLEncoder.encode(prospect.getCompany(), XtaasConstants.UTF8));
				} else if (prospect != null && prospect.getCustomAttributes().get("domain") != null
						&& !prospect.getCustomAttributes().get("domain").toString().isEmpty()) {
					sb.append("&companyDomainName=" + URLEncoder
							.encode(prospect.getCustomAttributes().get("domain").toString(), XtaasConstants.UTF8));
				} 

				if (prospect != null && prospect.getCountry() != null && !prospect.getCountry().isEmpty()) {
					sb.append("&Country=" + URLEncoder.encode(prospect.getCountry(), XtaasConstants.UTF8));
				}
				
				
				
				sb.append("&companyPastOrPresent=1");
				sb.append("&ValidDateMonthDist=" + URLEncoder.encode("24", XtaasConstants.UTF8));// checking for 2 years
				if (isDirectSearch)
					sb.append("&ContactRequirements=" + URLEncoder.encode("5", XtaasConstants.UTF8));// require phone,
				else
					sb.append("&ContactRequirements=" + URLEncoder.encode("2", XtaasConstants.UTF8));// require phone,
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	private String getPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");

		} else {
			phone = getCompanyPhone(personDetailResponse);
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");
		}
	}

	private String getCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		return phone;
	}

	private String getExtension(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			String phonepp = phone.substring(0, phone.indexOf("ext"));
			ext = phone.substring(phone.indexOf("ext"));
			ext = ext.replaceAll("[^[A-Za-z]*$]", "");

			// System.out.println(ext);
		}
		return ext;
	}

	private String getUSPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		if (phone != null) {
			// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			phone = getUSCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUSCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private String getUKPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;

		} else {
			phone = getUKCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUKCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;
		} else {
			return phone;
		}
	}

	private String getStateCode(PersonDetailResponse personDetailResponse) {
		if (personDetailResponse.getLocalAddress() != null) {
			String state = personDetailResponse.getLocalAddress().getState();
			String stateCode = getState(state);
			if (personDetailResponse.getLocalAddress().getCountry() != null
					&& !personDetailResponse.getLocalAddress().getCountry().isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository
						.findByCountryName(personDetailResponse.getLocalAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		} else if (personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
						.getCountry() != null) {
			String state = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
					.getState();
			String stateCode = getState(state);
			if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry() != null
					&& !personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry()
							.isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository.findByCountryName(personDetailResponse
						.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		}
		return null;
	}

	private String getUKStateCode(PersonDetailResponse personDetailResponse) {

		return "GL";
	}

	private String getState(String state) {
		EntityMapping toMapping = getEntityMapping();
		AttributeMapping attributeMapping = getReverseAttributeMapping("state", toMapping);
		List<ValueMapping> stateCode = attributeMapping.getValues();
		Iterator<ValueMapping> iterator = stateCode.iterator();
		while (iterator.hasNext()) {
			ValueMapping valueMapping = (ValueMapping) iterator.next();
			String to = valueMapping.getTo().replaceAll("%20", " ");
			if (to.equals(state)) {
				return valueMapping.getFrom();
			}
		}
		return null;

	}

	private EntityMapping getEntityMapping() {
		List<EntityMapping> toMappings = entityMappingRepository.findByFromSystemToSystem("xtaas",
				ContactListProviderType.ZOOMINFO.name());
		if (toMappings.size() > 0) {
			return toMappings.get(0);
		}
		return null;
	}

	private AttributeMapping getReverseAttributeMapping(String netProspexAttribute, EntityMapping toMapping) {
		for (AttributeMapping attributeMapping : toMapping.getAttributes()) {
			if (attributeMapping.getTo().equals(netProspexAttribute)) {
				return attributeMapping;
			}
		}
		return null;
	}

	private String buildCompanyUrl(String queryString, /* int offset, int size, */String personId)
			throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		logger.debug("Key : " + key);
		if (personId != null) {
			/*
			 * String
			 * baseUrl="https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			 * String url = baseUrl + "pc="+partnerKey +"&CompanyID="+personId +
			 * "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key="
			 * +key; return url;
			 */
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companysic,companyType&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		}
	}

	private StringBuilder createCompanyCriteria(String companyName, String companyId, String campaignId) {

		StringBuilder queryString = new StringBuilder();

		try {

			if (companyId != null) {
				queryString.append("companyIds=" + URLEncoder.encode(companyId, XtaasConstants.UTF8));
			} else if (companyName != null) {
				queryString.append("companyName=" + URLEncoder.encode(companyName, XtaasConstants.UTF8));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryString;

	}

	private Map<String, Long> findCompanyData(String revRange, String empRange) {
		List<Long> revenueEmployesData = new ArrayList<Long>();
		Map<String, Long> companyData = new HashMap<String, Long>();
		Long minRevenue = new Long(0);
		Long maxRevenue = new Long(0);
		Long minEmployeeCount = new Long(0);
		Long maxEmployeeCount = new Long(0);
		if (revRange != null) {
			String[] revenueParts = revRange.split(" - ");

			if (revenueParts.length == 2) { // Ex response from Zoominfo:
				minRevenue = getRevenue(revenueParts[0]);
				maxRevenue = getRevenue(revenueParts[1]);

			} else if (revRange.contains("Over")) {
				String revenuePart = revRange;
				minRevenue = getRevenue(revenuePart);

			} else if (revRange.contains("Under")) {
				String revenuePart = revRange;
				maxRevenue = getRevenue(revenuePart);
			}
		} else {
			minRevenue = new Long(0);
			maxRevenue = new Long(0);
		}
		if (empRange != null) {
			String[] employeeCountParts = empRange.split(" - ");
			if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
				minEmployeeCount = Long.parseLong(employeeCountParts[0].trim().replace(",", ""));
				maxEmployeeCount = Long.parseLong(employeeCountParts[1].trim().replace(",", ""));

			} else if (empRange.contains("Over")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				minEmployeeCount = Long.parseLong(employeeCountPart);

			} else if (empRange.contains("Under")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				maxEmployeeCount = Long.parseLong(employeeCountPart);
			}
		} else {
			minEmployeeCount = new Long(0);
			maxEmployeeCount = new Long(0);
		}
		revenueEmployesData.add(minRevenue);
		revenueEmployesData.add(maxRevenue);
		revenueEmployesData.add(minEmployeeCount);
		revenueEmployesData.add(maxEmployeeCount);

		if (minRevenue > 0 && maxRevenue > 0) {
			if (minRevenue > maxRevenue) {
				companyData.put("minRevenue", maxRevenue);
				companyData.put("maxRevenue", minRevenue);
			} else {
				companyData.put("minRevenue", minRevenue);
				companyData.put("maxRevenue", maxRevenue);
			}
		} else {
			companyData.put("minRevenue", minRevenue);
			companyData.put("maxRevenue", maxRevenue);
		}

		companyData.put("minRevenue", minRevenue);
		companyData.put("maxRevenue", maxRevenue);
		companyData.put("minEmployeeCount", minEmployeeCount);
		companyData.put("maxEmployeeCount", maxEmployeeCount);
		return companyData;
	}

	private Long getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long y = new Double(value * million).longValue();
			return new Double(value * million).longValue();

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long x = new Double(value * billion).longValue();
			return new Double(value * billion).longValue();
		}
		return new Long(0);
	}


	private Prospect getPurchaseCampaignResponse(Prospect prospect, Campaign campaign) {

		try {
			CampaignCriteria cc = getCampaignCriteria(campaign);
			setCountryInPrimaryProspect(cc,prospect);
			boolean isDirectSearch = true;
			if(campaign.isABM()){
				setCompanyIdToProspect(prospect,campaign);
			}
			setCountryInPrimaryProspect(cc, prospect);

			// first search for direct
			ZoomInfoResponse personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch);
			// if you dont get direct then search for indirect number;
			if (personZoomInfoResponse == null || personZoomInfoResponse.getPersonSearchResponse() == null
					|| personZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
				isDirectSearch = false;
				personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch);
			}

			if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
				int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();

				if (totalResult > 0 && personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
						.getPersonRecord() != null) {
					List<PersonRecord> list = personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
							.getPersonRecord();
					if (list != null && list.size() > 0) {
						for(int i=0 ; i <list.size(); i++){
							PersonRecord personRecord = list.get(i);
							boolean isValidCountry = zoomInfoDataBuy.isValidCountry(cc, personRecord);
							if(isValidCountry){
								String personDetailRequestUrl = buildUrl(null, 0, 0, personRecord.getPersonId());
								URL detailUrl = new URL(personDetailRequestUrl); // purchase request
								ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(detailUrl);
								if (zoomInfoResponseDetail.getPersonDetailResponse() != null
										&& zoomInfoResponseDetail.getPersonDetailResponse().getFirstName() != null
										&& zoomInfoResponseDetail.getPersonDetailResponse().getLastName() != null
										&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getFirstName())
										&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getLastName())
										&& (zoomInfoResponseDetail.getPersonDetailResponse().getDirectPhone() != null
										|| zoomInfoResponseDetail.getPersonDetailResponse()
										.getCompanyPhone() != null)) {

									PersonDetailResponse personDetailResponse = zoomInfoResponseDetail
											.getPersonDetailResponse();
									List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
									int industryListSize = 0;
									if (industryList != null) {
										industryListSize = industryList.size();
									}
									List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
									List<String> topLevelIndustries = new ArrayList<String>();
									String zoomCountry = "United States";
									Prospect prospectInfo = new Prospect();
									if (personDetailResponse.getLocalAddress() != null
											&& personDetailResponse.getLocalAddress().getCountry() != null) {
										if (personDetailResponse.getLocalAddress().getStreet() != null)
											prospectInfo.setAddressLine1(personDetailResponse.getLocalAddress().getStreet());
										else
											prospectInfo.setAddressLine1("");
										if (personDetailResponse.getLocalAddress().getZip() != null)
											prospectInfo.setZipCode(personDetailResponse.getLocalAddress().getZip());
										else
											prospectInfo.setZipCode("");
										if (personDetailResponse.getLocalAddress().getCity() != null)
											prospectInfo.setCity(personDetailResponse.getLocalAddress().getCity());
										else
											prospectInfo.setCity("");
										// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
										// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
									} else if (personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress() != null
											&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress().getCountry() != null) {
										prospectInfo.setAddressLine1(personDetailResponse.getCurrentEmployment().get(0)
												.getCompany().getCompanyAddress().getStreet());
										prospectInfo.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany()
												.getCompanyAddress().getZip());
										prospectInfo.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany()
												.getCompanyAddress().getCity());
									}
									if (personDetailResponse.getLastUpdatedDate() != null
											&& !personDetailResponse.getLastUpdatedDate().isEmpty()) {
										String ld = personDetailResponse.getLastUpdatedDate();
										if (ld.contains("-"))
											ld = ld.replace("-", "/");
										Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);

										prospectInfo.setLastUpdatedDate(date1);
									}
									if (personDetailResponse.getLocalAddress() != null) {
										prospectInfo.setCountry(personDetailResponse.getLocalAddress().getCountry());
										zoomCountry = personDetailResponse.getLocalAddress().getCountry();
									} else if (personDetailResponse.getCurrentEmployment() != null
											&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
											&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress() != null
											&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyAddress().getCountry() != null) {
										prospectInfo.setCountry(personDetailResponse.getCurrentEmployment().get(0).getCompany()
												.getCompanyAddress().getCountry());
										zoomCountry = personDetailResponse.getCurrentEmployment().get(0).getCompany()
												.getCompanyAddress().getCountry();
									}

									if (prospectInfo.getCountry().equalsIgnoreCase("USA")
											|| prospectInfo.getCountry().equalsIgnoreCase("United States")
											|| prospectInfo.getCountry().equalsIgnoreCase("US")
											|| prospectInfo.getCountry().equalsIgnoreCase("Canada")) {
										prospectInfo.setPhone("+1"+getUSPhone(personDetailResponse));
										prospectInfo.setExtension(getExtension(personDetailResponse));
										// String phoneType =
										// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
										// prospect.setPhoneType(phoneType);
										prospectInfo.setCompanyPhone(getUSCompanyPhone(personDetailResponse));
										prospectInfo.setStateCode(getStateCode(personDetailResponse));
									} else if (prospectInfo.getCountry().equalsIgnoreCase("United Kingdom")
											|| prospectInfo.getCountry().equalsIgnoreCase("UK")) {
										prospectInfo.setPhone(getUKPhone(personDetailResponse));
										prospectInfo.setExtension(getExtension(personDetailResponse));
										// String phoneType =
										// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
										// prospect.setPhoneType(phoneType);
										prospectInfo.setCompanyPhone(getUKCompanyPhone(personDetailResponse));
										prospectInfo.setStateCode(getUKStateCode(personDetailResponse));
									} else {
										prospectInfo.setPhone(getPhone(personDetailResponse));
										prospectInfo.setExtension(getExtension(personDetailResponse));
										// String phoneType =
										// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
										// prospect.setPhoneType(phoneType);
										prospectInfo.setCompanyPhone(getCompanyPhone(personDetailResponse));
										prospectInfo.setStateCode(getStateCode(personDetailResponse));
									}
									String title = personRecord.getCurrentEmployment().getJobTitle();
									if(StringUtils.isBlank(title)){
										continue;
									}

									if (industryListSize > 0) {
										prospectInfo.setIndustry(industryList.get(0));
										prospectInfo.setIndustryList(industryList);
									}
									boolean isValidCountryPhone = zoomInfoDataBuy.isValidCountryPhone(prospectInfo);
									if(!isValidCountryPhone){
										continue;
									}

									if (checkDNC(prospectInfo, campaign)) {
										continue;
									}

									prospectInfo.setEu(personDetailResponse.isEU());


									String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
									String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
									String empRange = personRecord.getCurrentEmployment().getCompany()
											.getCompanyEmployeeCountRange();
									long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();
									long revCountInZeros = personRecord.getCurrentEmployment().getCompany()
											.getCompanyRevenueIn000s();
									String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
									ZoomInfoResponse companyResponse = getCompanyResponse(null,
											personRecord.getCurrentEmployment().getCompany().getCompanyId());

									Map<String, Object> customAttributes = new HashMap<String, Object>();
									Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);

									customAttributes.put("minRevenue", revenueEmployeeList.get("minRevenue"));
									customAttributes.put("maxRevenue", revenueEmployeeList.get("maxRevenue"));

									customAttributes.put("maxEmployeeCount", revenueEmployeeList.get("maxEmployeeCount"));
									customAttributes.put("minEmployeeCount", revenueEmployeeList.get("minEmployeeCount"));

									String detailDomain = personDetailResponse.getCurrentEmployment().get(0).getCompany()
											.getCompanyWebsite();
									if (detailDomain != null && !detailDomain.isEmpty()) {
										customAttributes.put("domain", detailDomain);
									} else {
										customAttributes.put("domain", domain);
									}
									customAttributes.put("revCount", revCount);
									customAttributes.put("revCountIn000s", new Long(revCountInZeros));
									customAttributes.put("empCount", new Long(empCount));
									prospectInfo.setCustomAttributes(customAttributes);

									prospectInfo.setCompany(getOrganizationName(zoomInfoResponseDetail));
									if (personDetailResponse.getCurrentEmployment().get(0).getManagementLevel() != null)
										prospectInfo.setManagementLevel(
												personDetailResponse.getCurrentEmployment().get(0).getManagementLevel().get(0));

									String timez = getTimeZone(prospect);
									prospectInfo.setTimeZone(timez);
									// prospectInfo.setCompanyAddress(personDetailResponse.get);

									prospectInfo.setDepartment(getJobFunction(zoomInfoResponseDetail));

									prospectInfo.setDirectPhone(isDirectSearch);
									if(personDetailResponse.getEmailAddress() != null && !personDetailResponse.getEmailAddress().isEmpty()) {
										prospectInfo.setEmail(personDetailResponse.getEmailAddress());
									}else {
										if(prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
											prospectInfo.setEmail(prospect.getEmail());
										}
									}

									// prospectInfo.setEu(personDetailResponse.get);
									prospectInfo.setFirstName(personDetailResponse.getFirstName());

									prospectInfo.setLastName(personDetailResponse.getLastName());
									// prospectInfo.setPrefix(prefix);
									// prospectInfo.setProspectType(prospectType);
									prospectInfo.setSourceType("INTERNAL");
									prospectInfo.setSourceCompanyId(getOrganizationId(zoomInfoResponseDetail));
									prospectInfo.setSourceId(personDetailResponse.getPersonId());
									prospectInfo.setSource("ZOOINFO");
									// prospectInfo.setStatus(status);
									prospectInfo.setTitle(title);
									prospectInfo.setTopLevelIndustries(topLevelIndustries);
									prospectInfo.setZoomPersonUrl(personDetailResponse.getZoomPersonUrl());

									if (companyResponse.getCompanySearchResponse() != null
											&& companyResponse.getCompanySearchResponse().getCompanySearchResults() != null
											&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
											.getCompanyRecord() != null
											&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
											.getCompanyRecord().get(0) != null) {

										if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
												.getCompanyRecord().get(0).getZoomCompanyUrl() != null) {
											prospectInfo.setZoomCompanyUrl(companyResponse.getCompanySearchResponse()
													.getCompanySearchResults().getCompanyRecord().get(0).getZoomCompanyUrl());
										}

										if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
												.getCompanyRecord().get(0).getCompanyNAICS() != null) {
											prospectInfo.setCompanyNAICS(companyResponse.getCompanySearchResponse()
													.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS());
										}
										if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
												.getCompanyRecord().get(0).getCompanySIC() != null) {
											prospectInfo.setCompanySIC(companyResponse.getCompanySearchResponse()
													.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC());
										}

									}
									return prospectInfo;
								}
							}

						}

					}
				}

			} else {
				logger.error("no data found from zoom for manual search for search parameters inzoominfo : ");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private void setCountryInPrimaryProspect(CampaignCriteria cc, Prospect prospect) {
		if(cc != null && prospect != null && StringUtils.isNotBlank(cc.getCountry())){
			prospect.setCountry(cc.getCountry());
		}
	}

	private void setCompanyIdToProspect(Prospect prospect, Campaign campaign) {
		try{
			if(prospect != null && StringUtils.isNotBlank(prospect.getCompany())){
				AbmListNew abmListNew = abmListNewRepository.findOneByCampaignIdCompanyName(campaign.getId(), prospect.getCompany());
				if(abmListNew != null) {
					String dbCompanyId = abmListNew.getCompanyId();
					if(StringUtils.isBlank(dbCompanyId) || dbCompanyId.equals("NOT_FOUND") || dbCompanyId.equals("GET_COMPANY_ID")){
						return;
					} else {
						prospect.setCompanyId(dbCompanyId);
					}
				} else {
					return;
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();

			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
				return cc;
			}
		}
		return cc;
	}

	/*
	 * Check phone DNC in local DNC list if not present then check with third-party
	 */
	private boolean checkDNC(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		boolean isLocalDNC = internalDNCListService.checkLocalDNCList(prospect.getPhone(),
				prospect.getFirstName(), prospect.getLastName());
		String pospectCountry = !StringUtils.isEmpty(prospect.getCountry())
				? prospect.getCountry().toUpperCase()
				: "US";
		if (isLocalDNC) {
			isDNC = isLocalDNC;
		} else if (campaign.isUsPhoneDncCheck() && XtaasConstants.US_COUNTRY.contains(pospectCountry)
				&& phoneDncService.usPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		} else if (campaign.isUkPhoneDncCheck() && XtaasConstants.UK_COUNTRY.contains(pospectCountry)
				&& phoneDncService.ukPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		}
		return isDNC;
	}

}
