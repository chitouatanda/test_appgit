package com.xtaas.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream.GetField;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.util.concurrent.RateLimiter;
import com.mongodb.BasicDBObject;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.monitorjbl.xlsx.StreamingReader;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.EverStringCompany;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.db.repository.ZoomTokenRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.EverStringRealTimeCompanyDTO;
import com.xtaas.web.dto.EverStringRealTimeRequestDTO;
import com.xtaas.web.dto.EverStringRealTimeResponseDTO;
import com.xtaas.web.dto.LeadIQSearchResponse;

@Service
public class CampaignSuppressionServiceImpl implements CampaignSuppressionService,TrafficManagerService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignSuppressionServiceImpl.class);

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	CampaignRepository campaignRepository;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	SuppressCallableServiceImpl suppressCallableServiceImpl;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	@Autowired
	private ZoomTokenRepository zoomTokenRepository;
	
	@Autowired
	HttpService httpService;
	
	private RateLimiter rateLimiter = RateLimiter.create(0.33);
	
	private SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();

	
	Date cachedDate;
	
	String tokenKey = "";
	
	private String partnerKey = "Xtaascorp.partner";
	
	private ThreadLocal<HashMap<Integer, String>> headerIndex = new ThreadLocal<>();


	//private String fileName;
	private ThreadLocal<String> fileName = new ThreadLocal<>();


	private ThreadLocal<Sheet> sheet  = new ThreadLocal<>();
	
	private ThreadLocal<HashMap<Integer, List<String>>> errorMap = new ThreadLocal<>();

	private ThreadLocal<Workbook>  book = new ThreadLocal<>();
	
	private ThreadLocal<Workbook> bookCopy  = new ThreadLocal<>();
	
	//String xlsFileName;
	
	//private File myFile;
	
	//private String campId;
	
	private String bucketName = "xtaasreports";
	
	@Override
	public void downloadSuppressionListTemplate(HttpServletRequest request, HttpServletResponse response,
			String campaignId, String suppressionType) throws IOException {
		//List<String> cids = new ArrayList<String>();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		this.sheet.set(sheet); 
		//int rowCount = 0;
		String fileName = null;
		String fileSuppressiontype = null;
		String newCampaignFileName = null;
		Row headerRow = this.sheet.get().createRow(0);
		writeHeader(headerRow, suppressionType);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		/*cids.add(campaignId);
		int supCount = suppressionListNewRepository
				.countSupressionListByCampaignIds(cids);
		
		
		if (supCount>0 ) {*/
		if (suppressionType.equalsIgnoreCase("contactSuppression")) {
			int rowCount = 1;
			int emailCount = suppressionListNewRepository
						.countByCampaignEmail(campaignId);
			
			int  batchsize = 0;
			if(emailCount>0) {
				batchsize = emailCount /10000;
				long modSize = emailCount % 10000;
				if(modSize > 0){
					batchsize = batchsize +1;
				}
			}
				
			if(batchsize > 0) {
				for(int i=0;i<batchsize;i++) {
					Pageable pageable = PageRequest.of(i, 10000);
					List<SuppressionListNew> newSupList =suppressionListNewRepository.pageByCampaignEmail(campaignId,pageable);
					for (SuppressionListNew data : newSupList) {
						Row dataRow = this.sheet.get().createRow(rowCount);
						Cell cell = dataRow.createCell(0);
						cell.setCellValue(data.getEmailId());
						rowCount++;
					}
				}
			} 
		}else {
			int rowCount = 1;
			int emailCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignId);
			
			int  batchsize = 0;
			if(emailCount>0) {
				batchsize = emailCount /10000;
				long modSize = emailCount % 10000;
				if(modSize > 0){
					batchsize = batchsize +1;
				}
			}
				
			if(batchsize > 0) {
				for(int i=0;i<batchsize;i++) {
					Pageable pageable = PageRequest.of(i, 10000);
					List<SuppressionListNew> newSupList =suppressionListNewRepository.pageSupressionListByCampaignDomain(campaignId,pageable);
					for (SuppressionListNew data : newSupList) {
						Row dataRow = this.sheet.get().createRow(rowCount);
						Cell cell = dataRow.createCell(0);
						cell.setCellValue(data.getDomains());
						rowCount++;
					}
				}
			} 
			
		}
		fileSuppressiontype = suppressionType.equalsIgnoreCase("contactSuppression") ? "_contact_suppression_list.xlsx"
				: "_company_suppression_list.xlsx";
		newCampaignFileName = suppressionType.equalsIgnoreCase("contactSuppression") ? "new_contact_" : "new_company_";
		fileName = (campaign != null) ? campaign.getName() + fileSuppressiontype
				: newCampaignFileName + "suppression_list.xlsx";
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	private void writeHeader(Row headerRow, String suppressionType) {
		if (suppressionType.equalsIgnoreCase("contactSuppression")) {
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Email");
			Cell action = headerRow.createCell(1);
			action.setCellValue("Action");
			Cell date = headerRow.createCell(2);
			date.setCellValue("Created Date");
		} else {
			Cell domainCell = headerRow.createCell(0);
			domainCell.setCellValue("Domain");
			Cell companyNameCell = headerRow.createCell(1);
			companyNameCell.setCellValue("Company Name");
			Cell action = headerRow.createCell(2);
			action.setCellValue("Action");
			Cell date = headerRow.createCell(3);
			date.setCellValue("Created Date");
		}
	}
	@Override
	public String multiDownloadSupressionlist(String campaignId, String suppressionType, String loginId) throws IOException {
		
		SuppressionDownload suppressionDownload = new SuppressionDownload();
		suppressionDownload.setCampaignId(campaignId);
		suppressionDownload.setSuppressionType(suppressionType);
		suppressionDownload.setSheet(sheet.get());
		suppressionDownload.setLoginId(loginId);
		//SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
		delegateExecutor.setConcurrencyLimit(2);
		DelegatingSecurityContextExecutor suppressionDownloadThreadExecutor = new DelegatingSecurityContextExecutor(
				delegateExecutor, SecurityContextHolder.getContext());
		suppressionDownloadThreadExecutor.execute(suppressionDownload);
		logger.debug("Suppression File - DOWNLOADING-THREAD is started...");
		
		
		return null;
	}

	
	@Override
	public String createCampaignSuppressionList(String campaignId, String suppressionType, MultipartFile file)
			throws IOException {
		//this.suppressionType = suppressionType;
		//xlsFileName = file.getOriginalFilename();
		//System.out.println(xlsFileName);
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				//fileInputStream = file.getInputStream();
				SuppressionFileUploader suppressionFileUploader = new SuppressionFileUploader();
				suppressionFileUploader.setCampaignId(campaignId);
				suppressionFileUploader.setMyFile(convertMultiPartToFile(file));
				suppressionFileUploader.setSuppressionType(suppressionType);
				suppressionFileUploader.setXlsFileName(file.getOriginalFilename());
				//myFile = convertMultiPartToFile(file);
				//campId = campaignId;
				//SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
				delegateExecutor.setConcurrencyLimit(2);
				DelegatingSecurityContextExecutor suppressionFileUploadThreadExecutor = new DelegatingSecurityContextExecutor(
						delegateExecutor, SecurityContextHolder.getContext());
				suppressionFileUploadThreadExecutor.execute(suppressionFileUploader);
				logger.debug("Suppression File - UPLOADING-THREAD is started...");
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	class SuppressionFileUploader implements Runnable {
		private String suppressionType;
		private String xlsFileName;
		private File myFile;
		private String campaignId;
		
		
		public String getSuppressionType() {
			return suppressionType;
		}


		public void setSuppressionType(String suppressionType) {
			this.suppressionType = suppressionType;
		}


		public String getXlsFileName() {
			return xlsFileName;
		}


		public void setXlsFileName(String xlsFileName) {
			this.xlsFileName = xlsFileName;
		}


		public File getMyFile() {
			return myFile;
		}


		public void setMyFile(File myFile) {
			this.myFile = myFile;
		}


		public String getCampaignId() {
			return campaignId;
		}


		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}


		@Override
		public void run() {
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
			Campaign campaign = campaignRepository.findOneById(campaignId);
			fileName.set(getXlsFileName());
			try {
				headerIndex.set(new HashMap<Integer, String>());
				errorMap.set(new HashMap<Integer, List<String>>());
				bookCopy.set(WorkbookFactory.create(myFile)); 
				book.set(StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(myFile)); 
				sheet.set(book.get().getSheetAt(0));
				//int totalRowsCount = sheet.getPhysicalNumberOfRows();
				int totalRowsCount = sheet.get().getLastRowNum() + 1;
				if (totalRowsCount < 1) {// check if nothing found in file
					book.get().close();
					logger.error(fileName.get() + " file is empty");
					//throw new IllegalArgumentException("File cannot be empty.");
					sendEmail(loginUser, campaignId,suppressionType,xlsFileName + " File is Empty",organization);
				}else if (totalRowsCount <= 1) {// check if nothing found in file
					book.get().close();
					logger.error("No records found in file " + fileName.get());
					//throw new IllegalArgumentException("No records found in file.");
					sendEmail(loginUser, campaignId,suppressionType,"No records found in file " + xlsFileName,organization);
				}
				if (!validateExcelFileheader(loginUser,organization,campaignId,suppressionType))
					return;
				validateExcelFileRecords(loginUser, campaignId,organization,suppressionType,xlsFileName,campaign);
				book.get().close();
			} catch (IOException e) {
				logger.error("Problem in reading the file " + fileName.get());
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
				return;
			} catch (Exception e) {
				e.printStackTrace();
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
			}
			
		}
		
	}
	private boolean validateExcelFileheader(Login loginUser,Organization organization,String campaignId,String suppressionType) {
		Set<String> fileColumns = new HashSet<String>();
		//final Row row = sheet.getRow(0);// get first sheet in file
		Iterator<Row> rowIterator = sheet.get().rowIterator();
		if (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			int colCounts = row.getPhysicalNumberOfCells();

			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell == null || cell.getCellType() == CellType.BLANK) {
					logger.error(
							"Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
					return false;
				}
				if (cell.getCellType() != CellType.STRING) {
					logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
							+ " Header must contain only String values");
					return false;
				} else {
					headerIndex.get().put(j, cell.getStringCellValue().replace("*", ""));
					// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
					fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
					if (cell.getStringCellValue().replace("*", "").trim().equalsIgnoreCase("Email") 
							|| cell.getStringCellValue().replace("*", "").trim().equalsIgnoreCase("Email*")
							|| cell.getStringCellValue().replace("*", "").trim().equalsIgnoreCase("Email address")) {
						if (!suppressionType.equalsIgnoreCase("contactSuppression")) {
							sendEmail(loginUser, campaignId, suppressionType, "Please upload company suppression excel file. The column header should be Domain and Company Name",organization);
							return false;
						}
					}else if (cell.getStringCellValue().replace("*", "").trim().equalsIgnoreCase("Domain") 
							|| cell.getStringCellValue().replace("*", "").trim().equalsIgnoreCase("Domain*")) {
						Cell cellType = row.getCell(1);
						if (!suppressionType.equalsIgnoreCase("companySuppression")) {
							sendEmail(loginUser, campaignId, suppressionType, "Please upload Contact suppression excel file. The column header should be Email",organization);
							return false;
						} else if (cellType != null && cellType.getCellType() != CellType.BLANK) {
							if(cellType.getStringCellValue().equalsIgnoreCase("Action")
									&& suppressionType.equalsIgnoreCase("companySuppression")) {
								sendEmail(loginUser, campaignId, suppressionType,
										"Please upload company suppression excel file. The column header should be Domain and Company Name",
										organization);
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	private void validateExcelFileRecords(Login loginUser,String campaignId,Organization organization,String suppressionType,String xlsFileName,Campaign campaign) {
		//int rowsCount = sheet.getPhysicalNumberOfRows();
		int rowsCount = sheet.get().getLastRowNum() + 1;
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName.get());
			//errorList.add("No records found in file " + fileName);
			return;
		}
		//final Row tempRow = sheet.getRow(0);
		// getting first row for counting number of header columns
		int colCounts = 0;
//		Iterator<Row> rowIterator = sheet.rowIterator();
//		if (rowIterator.hasNext()) {
//			Row headerRow = rowIterator.next();
//			colCounts = headerRow.getPhysicalNumberOfCells();
//		}
		//int colCounts = tempRow.getPhysicalNumberOfCells();
		// makeBatchProcessingCall(rowsCount, 0, colCounts, campaignId);
		try {
			processRecords(loginUser,0, rowsCount, colCounts, campaignId,organization,suppressionType,xlsFileName);
		} catch (Exception e) {
			e.printStackTrace();	
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}
		
	}

	public void processRecords(Login loginUser,int startPos, int EndPos, int colCounts, String campaignId,Organization organization,String suppressionType,String xlsFileName) {
		int  batchsize = 0;
		if(EndPos>0) {
			batchsize = EndPos /1000;
			long modSize = EndPos % 1000;
			if(modSize > 0){
				batchsize = batchsize +1;
			}
		}
	
		int countSuccess = 0;
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<String> errorList = null;
		List<SuppressionListNew> emailIds = new ArrayList<SuppressionListNew>();
		List<SuppressionListNew> domains = new ArrayList<SuppressionListNew>();
		boolean invalidfile = false;
		//for (int i = startPos; i < EndPos; i++) {
		int rowCount = 0;
		int errorMapCount = 0;
		for (Row row : sheet.get()) {
			errorList = new ArrayList<String>();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + rowCount);
			if (rowCount == 0) {
				errorMap.get().put(0, null);
				errorMapCount++;
				//continue;
			}
			//Row row = sheet.getRow(i);
//			if (row == null || i == 0) {
//				continue;
//			}
			boolean emptyCell = true;
			int maxCol = 2;
			for (int j = 0; j < maxCol; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				continue;
			}
			
			for (int j = 0; j < maxCol; j++) {
				String columnName = headerIndex.get().get(j).replace("*", "").trim();
				// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
				Cell cell = row.getCell(j);
				if (cell == null || cell.getCellType() == CellType.BLANK) {
					logger.debug("Cell is empty");
				} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("Email*")
						|| columnName.equalsIgnoreCase("Email address")) {
					if (suppressionType.equalsIgnoreCase("contactSuppression")) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
//						if (email.contains(" ")) {
//							errorList.add("Please Remove Space from Email");
//						}else {
							//EmailValidator emailValidator = EmailValidator.getInstance();
							if (email != null) {
								Cell cellAction = row.getCell(1);
								if (cellAction != null && cellAction.getCellType() != CellType.BLANK) {
									String action = cellAction.getStringCellValue().trim();
									if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("INSERT")) {
										SuppressionListNew supemail = new SuppressionListNew();
										supemail.setCampaignId(campaignId);
										if(!campaign.isMdFiveSuppressionCheck() && 
												!XtaasUtils.validateEmailId(email.toLowerCase())) {
											campaign.setMdFiveSuppressionCheck(true);
											campaign = campaignRepository.save(campaign);
											//campaign = campaignRepository.findOneById(campaignId);
										}
										supemail.setEmailId(email.toLowerCase());
										supemail.setOrganizationId(campaign.getOrganizationId());
										supemail.setStatus("ACTIVE");
										emailIds.add(supemail);
										countSuccess++;
										//suppressionListNewRepository.save(supemail);
									}else if (action.equalsIgnoreCase("REMOVE") || action.equalsIgnoreCase("DELETE")) {
										List<SuppressionListNew> removeList = suppressionListNewRepository
												.findByCampaignEmail(campaignId, email.toLowerCase());
										removeList.forEach(obj -> obj.setStatus("INACTIVE"));
										suppressionListNewRepository.saveAll(removeList);
									}
								}
								else {
									logger.error("cell is empty");
								}
							}else {
								errorList.add("Invalid Email");
							}
						//}
					}
				} else if (columnName.equalsIgnoreCase("Domain")) {
					if (suppressionType.equalsIgnoreCase("companySuppression")) {
						String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						domain = getSuppressedHostName(domain, loginUser, campaignId, suppressionType, campaign);
						if (domain != null) {
							Cell cellAction = row.getCell(2);
							if (cellAction != null && cellAction.getCellType() != CellType.BLANK) {
								String action = cellAction.getStringCellValue().trim();
								if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("INSERT")) {
									SuppressionListNew supdomain = new SuppressionListNew();
									supdomain.setCampaignId(campaignId);
									supdomain.setOrganizationId(campaign.getOrganizationId());
									supdomain.setStatus("ACTIVE");
									supdomain.setDomains(domain.toLowerCase());
									domains.add(supdomain);
									countSuccess++;
									// suppressionListNewRepository.save(supdomain);
								} else if (action.equalsIgnoreCase("REMOVE") || action.equalsIgnoreCase("DELETE")) {
									List<SuppressionListNew> removeList = suppressionListNewRepository
											.findSupressionListByCampaignDomain(campaignId, domain.toLowerCase());
									removeList.forEach(obj -> obj.setStatus("INACTIVE"));
									suppressionListNewRepository.saveAll(removeList);
								}
							} else {
								logger.error("cell is empty");
							}

						} else {
							errorList.add("Invalid Domain");
						}
					}
				}else if (columnName.equalsIgnoreCase("Company Name")) {
					Cell celldomain = row.getCell(0);// == null && row.getCell(0).getCellType() == CellType.BLANK
					if (celldomain == null || celldomain.getCellType() == CellType.BLANK) {
						if (suppressionType.equalsIgnoreCase("companySuppression")) {
							String companyName = null;
							String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
							companyName = domain;
							try {
								if (domain != null) {
									domain = getCompanyDomainFromZoomInfo(companyName.toLowerCase());
									if (domain == null) {
										domain = getCompanyDomainFromLeadIQ(companyName.toLowerCase());
										if (domain == null) {
											domain = getCompanyDomainFromEverString(companyName.toLowerCase());
											if(domain == null) {
												domain = "";
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							domain = getSuppressedHostName(domain, loginUser, campaignId, suppressionType, campaign);
							if (domain != null && !domain.isEmpty()) {
								Cell cellAction = row.getCell(2);
								if (cellAction != null && cellAction.getCellType() != CellType.BLANK) {
									String action = cellAction.getStringCellValue().trim();
									if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("INSERT")) {
										SuppressionListNew supdomain = new SuppressionListNew();
										supdomain.setCampaignId(campaignId);
										supdomain.setOrganizationId(campaign.getOrganizationId());
										supdomain.setStatus("ACTIVE");
										supdomain.setDomains(domain.toLowerCase());
										supdomain.setCompanyName(companyName);
										domains.add(supdomain);
										countSuccess++;
										// suppressionListNewRepository.save(supdomain);
									} else if (action.equalsIgnoreCase("REMOVE") || action.equalsIgnoreCase("DELETE")) {
										List<SuppressionListNew> removeList = suppressionListNewRepository
												.findSupressionListByCampaignDomain(campaignId, domain.toLowerCase());
										removeList.forEach(obj -> obj.setStatus("INACTIVE"));
										suppressionListNewRepository.saveAll(removeList);
									}
								} else {
									logger.error("cell is empty");
								}

							} else {
								errorList.add("Invalid Domain");
							}
						}
					}
				}
			}
			
			if(errorList != null  && errorList.size() > 0) {
				invalidfile = true;
			}
			errorMap.get().put(errorMapCount, errorList);
			rowCount++;
			errorMapCount++;
			boolean save =  false;
			
			if(rowCount == EndPos-1 && batchsize==1) {
				save  = true;
			}else if((rowCount % 1000)==  0) {
				save = true;
				batchsize--;
		    }
			if (save  && suppressionType.equalsIgnoreCase("contactSuppression")) {
				suppressionListNewRepository.bulkinsert(emailIds);
				emailIds = new ArrayList<SuppressionListNew>();

			}else if (save && suppressionType.equalsIgnoreCase("companySuppression")) {
				suppressionListNewRepository.bulkinsert(domains);
				domains  = new ArrayList<SuppressionListNew>();
			}

		}
		if (suppressionType.equalsIgnoreCase("contactSuppression")) {
			campaign.setEmailSuppressionCount(countSuccess);
			if(countSuccess > 0 && !campaign.isEmailSuppressed()) {
				campaign.setEmailSuppressed(true);
			}
			campaignRepository.save(campaign);
		}else if (suppressionType.equalsIgnoreCase("companySuppression")){
			campaign.setDomainSuppressionCount(countSuccess);
			campaignRepository.save(campaign);
		}
		List<String> reportingEmail = null;
		if ( loginUser.getReportEmails() != null &&  loginUser.getReportEmails().size() > 0) {
			reportingEmail = loginUser.getReportEmails();
		}
		if(invalidfile)
		{
			ErrorHandlindutils.excelFileChecking(bookCopy.get(), errorMap.get());
			try {
				if (suppressionType.equalsIgnoreCase("contactSuppression")) {
					ErrorHandlindutils.sendExcelwithMsg(loginUser,bookCopy.get(),xlsFileName,"Contact Supression",campaign.getName(),reportingEmail);
				}else {
					ErrorHandlindutils.sendExcelwithMsg(loginUser,bookCopy.get(),xlsFileName,"Company Supression",campaign.getName(),reportingEmail);
				}
				
			} catch (IOException e) {
				e.printStackTrace();	
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
			}
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError("Excel file uploaded with errors");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
			
		}else {
			
			sendEmail(loginUser,  campaignId, suppressionType,xlsFileName + " File Uploaded Successfully with " + countSuccess + " Records",organization);
		}
		
		
	

	}

	@Override
	public Map<String,Integer> getSuppressionListByCampaignId(String campaignId) {
		int domainCount = suppressionListNewRepository.countSupressionListByCampaignDomain(campaignId);
		int  emailCount = suppressionListNewRepository.countByCampaignEmail(campaignId);
		Map<String,Integer> countMap = new HashMap<String,Integer>();
		countMap.put("emailCount", emailCount);
		countMap.put("domainCount", domainCount);
		return countMap;
	}
	
	private File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File( file.getOriginalFilename() );
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }
	
	private void sendEmail(Login loginUser, String campaignId, String suppressionType, String body,Organization organization) {
		Campaign campaign = null;
		try {
			String subject = null;
			campaign = campaignRepository.findOneById(campaignId);
			if (suppressionType.equalsIgnoreCase("contactSuppression")) {
				subject = "Contact Supression Upload Status For " + campaign.getName();
			} else {
				subject = "Company Supression Upload Status For " + campaign.getName();
			}
			//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, body);

//			List<String> listOfEmail = loginUser.getReportEmails();
//			if (listOfEmail != null && listOfEmail.size() > 0) {
//				XtaasEmailUtils.sendEmail(listOfEmail, "data@xtaascorp.com", subject, body);
//			}
			if (loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
				for(String email : loginUser.getReportEmails()) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, body);	
				}
			}else {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, body);
			}
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("COMPLETED");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : "+body;
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		} catch (Exception e) {
			e.printStackTrace();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}

	}

	
	private String getSuppressedHostName(String domain,Login loginUser,String campaignId,String suppressionType,Campaign campaign) {

		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,suppressionType, "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : Suppression list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}
		return domain;
	}
	
	class SuppressionDownload implements Runnable {
		
		private String campaignId;
		private String suppressionType;
		private Sheet sheet;
		private String loginId;

		public String getCampaignId() {
			return campaignId;
		}



		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}



		public String getSuppressionType() {
			return suppressionType;
		}



		public void setSuppressionType(String suppressionType) {
			this.suppressionType = suppressionType;
		}



		public Sheet getSheet() {
			return sheet;
		}



		public void setSheet(Sheet sheet) {
			this.sheet = sheet;
		}

		


		public String getLoginId() {
			return loginId;
		}



		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}



		@Override
		public void run() {
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			Campaign campaign = campaignRepository.findOneById(getCampaignId());
			try {
				XSSFWorkbook workbook = new XSSFWorkbook();
				XSSFSheet sheet = workbook.createSheet();
				this.sheet = sheet;

				// int rowCount = 0;
				String fileName = null;
				String fileSuppressiontype = null;
				String newCampaignFileName = null;
				Row headerRow = this.sheet.createRow(0);
				writeHeader(headerRow, getSuppressionType());
				Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
				/*
				 * cids.add(campaignId); int supCount = suppressionListNewRepository
				 * .countSupressionListByCampaignIds(cids);
				 * 
				 * if (supCount>0 ) {
				 */
				if (getSuppressionType().equalsIgnoreCase("contactSuppression")) {
					int rowCount = 1;
					int emailCount = suppressionListNewRepository.countByCampaignEmail(getCampaignId());

					int batchsize = 0;
					if (emailCount > 0) {
						batchsize = emailCount / 5000;
						long modSize = emailCount % 5000;
						if (modSize > 0) {
							batchsize = batchsize + 1;
						}
					}

					if (batchsize > 0) {
						for (int i = 0; i < batchsize; i++) {
							Pageable pageable = PageRequest.of(i, 5000);
							List<SuppressionListNew> newSupList = suppressionListNewRepository
									.pageByCampaignEmail(getCampaignId(), pageable);
							for (SuppressionListNew data : newSupList) {
								Row dataRow = this.sheet.createRow(rowCount);
								Cell cell = dataRow.createCell(0);
								cell.setCellValue(data.getEmailId());
								Cell cell2 = dataRow.createCell(2);
								cell2.setCellValue(dateToString(data.getCreatedDate()));
								rowCount++;
							}
						}
					}
				} else {
					int rowCount = 1;
					int emailCount = suppressionListNewRepository.countSupressionListByCampaignDomain(getCampaignId());

					int batchsize = 0;
					if (emailCount > 0) {
						batchsize = emailCount / 5000;
						long modSize = emailCount % 5000;
						if (modSize > 0) {
							batchsize = batchsize + 1;
						}
					}

					if (batchsize > 0) {
						for (int i = 0; i < batchsize; i++) {
							Pageable pageable = PageRequest.of(i, 5000);
							List<SuppressionListNew> newSupList = suppressionListNewRepository
									.pageSupressionListByCampaignDomain(getCampaignId(), pageable);
							for (SuppressionListNew data : newSupList) {
								Row dataRow = this.sheet.createRow(rowCount);
								Cell cell = dataRow.createCell(0);
								cell.setCellValue(data.getDomains());
								Cell cellCompanyName = dataRow.createCell(1);
								cellCompanyName.setCellValue(data.getCompanyName());
								Cell cell2 = dataRow.createCell(3);
								cell2.setCellValue(dateToString(data.getCreatedDate()));
								rowCount++;
							}
						}
					}

				}
				LocalDateTime myDateObj = LocalDateTime.now();
				DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-M-yyyy");
				String formattedDate = myDateObj.format(myFormatObj);
				fileSuppressiontype = getSuppressionType().equalsIgnoreCase("contactSuppression")
						? "_contact_suppression_list_" + formattedDate + ".xlsx"
						: "_company_suppression_list_" + formattedDate + ".xlsx";
				newCampaignFileName = getSuppressionType().equalsIgnoreCase("contactSuppression") ? "new_contact_"
						: "new_company_";
				fileName = (campaign != null) ? campaign.getName() + fileSuppressiontype
						: newCampaignFileName + "suppression_list_" + formattedDate + ".xlsx";

				FileOutputStream out;
				try {
					File file = new File(fileName);
					file.createNewFile();
					out = new FileOutputStream(file);
					workbook.write(out);
					workbook.close();
					String link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketName, fileName, file);
					logger.debug("Suppression File :" + link);
					String subject = null;
					String linkrefNotice = "<" + link + ">";
					String linkref = "<a href=\" " + link + " \">Download here</a>";
					String body = null;
					if (suppressionType.equalsIgnoreCase("contactSuppression")) {
						subject = "Contact Suppression Download Status For " + campaign.getName();
						body = "Contact Suppression Download Link :  " + linkref;
					} else {
						subject = "Company Suppression Download Status For " + campaign.getName();
						body = "Company Suppression Download Link :  " + linkref;
					}
					if (loginUser.getReportEmails() != null
							&& loginUser.getReportEmails().size() > 0) {
						for (String email : loginUser.getReportEmails()) {
							XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, body);
						}
					} else {
						XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, body);
					}
					SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(
							getLoginId(), getCampaignId(), getSuppressionType(), "INPROCESS");
					if (supervisorStats != null) {
						supervisorStats.setStatus("COMPLETED");
						supervisorStatsRepository.save(supervisorStats);
						String msg = null;
						if (suppressionType.equalsIgnoreCase("contactSuppression")) {
							msg = "Contact Suppression Download Link : Download here" + linkrefNotice;
						} else {
							msg = "Company Suppression Download Link : Download here" + linkrefNotice;
						}
						PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						String notice = campaign.getName()+" : "+msg;
						teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(
							loginUser.getId(), campaignId, suppressionType, "INPROCESS");
					if (supervisorStats != null) {
						supervisorStats.setStatus("ERROR");
						supervisorStats.setError(ExceptionUtils.getStackTrace(e));
						supervisorStatsRepository.save(supervisorStats);
						PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						String notice = campaign.getName()+" : Suppression list download failed, please contact support";
						teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(
						loginUser.getId(), campaignId, suppressionType, "INPROCESS");
				if (supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName()+" : Suppression list download failed, please contact support";
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
			}

		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String removeDublicateRecords() {
		Calendar startDateCalendar = XtaasDateUtils.getPreviousDate(Calendar.getInstance(), 1);
		Date startDate = startDateCalendar.getTime();
		Date endDate = Calendar.getInstance().getTime();
		BasicDBObject elemMatch = new BasicDBObject();
		elemMatch.put("createdDate", new BasicDBObject("$gte", XtaasDateUtils.getDateAtTime(startDate, 0, 0, 0, 0)));
		elemMatch.put("createdDate", new BasicDBObject("$lte", XtaasDateUtils.getDateAtTime(endDate, 23, 59, 59, 0)));
		DistinctIterable<String> distinctCampaignId = mongoTemplate.getCollection("suppressionlistnew").distinct("campaignId", elemMatch, String.class);
		List<String> distinctIds = new ArrayList<>();
		MongoCursor<String> cursor = distinctCampaignId.iterator();
		while (cursor.hasNext()) {
			distinctIds.add(cursor.next());
		}
		if(distinctIds != null && distinctIds.size() > 0) {
			for(String campaignId : distinctCampaignId) {
				List<SuppressionListNew> domainList = suppressionListNewRepository.findSupressionListByCampaignDomain(campaignId);
				if(domainList != null && domainList.size() > 0) {
					//List<SuppressionListNew> distinctDomain = new ArrayList<SuppressionListNew>();
					Set<String> domainSet = new HashSet<String>();
					//int count = 0;
					for( SuppressionListNew suppressionListNew : domainList ) {
					    if(domainSet.add(suppressionListNew.getDomains())) {
					    	//distinctDomain.add(suppressionListNew);
					    }else {
					    	//count++;
					    	suppressionListNewRepository.delete(suppressionListNew);
					    }
					}
				}

				List<SuppressionListNew> emailList = suppressionListNewRepository.findByCampaignEmail(campaignId);
				if(emailList != null && emailList.size() > 0) {
					//List<SuppressionListNew> distinctEmail = new ArrayList<SuppressionListNew>();
					Set<String> emailSet = new HashSet<String>();
					//int count = 0;
					for( SuppressionListNew suppressionListNew : emailList ) {
					    if(emailSet.add(suppressionListNew.getEmailId())) {
					    	//distinctEmail.add(suppressionListNew);
					    }else {
					    	//count++;
					    	suppressionListNewRepository.delete(suppressionListNew);
					    }
					}
				}

				List<SuppressionListNew> inActiveList = suppressionListNewRepository.removeSupressionListByCampaignId(campaignId);
				if(inActiveList != null && inActiveList.size() > 0) {
					suppressionListNewRepository.deleteAll(inActiveList);
				}
			}		
		}
		return "Successfully Remove";
	}
	
	public String dateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String sDate = sdf.format(date); 
		return sDate;
	}
	
	private String getCompanyDomainFromLeadIQ(String companyName) {
		String domain = null;
		try {
			StringBuilder payload = new StringBuilder();
			payload.append(
					"{   \"query\": \"query SearchCompany($input: SearchCompanyInput!) { searchCompany(input: $input) { results { name alternativeNames domain linkedinId locationInfo { city areaLevel1 country postalCode } numberOfEmployees industry } } }  \",   \"variables\": {  \"input\": {  \"name\":\"");
			payload.append(companyName);
			payload.append("\"}}}");
			String apiKey = ApplicationEnvironmentPropertyUtils.getLeadIQAPIKey();
			String leadIQAPIURL = ApplicationEnvironmentPropertyUtils.getLeadIQUrl();
			String apiKeyEncoded = Base64.getEncoder().encodeToString(apiKey.getBytes());
			Client client = ClientBuilder.newClient();
			Entity payloadEntity = Entity.json(payload.toString());
			Response response = client.target(leadIQAPIURL).request(MediaType.APPLICATION_JSON_TYPE)
					.header("Authorization", "Basic " + apiKeyEncoded).post(payloadEntity);
			String responseJsonString = response.readEntity(String.class);
			JSONObject responseJson = new JSONObject(responseJsonString);
			if(responseJson != null) {
				JSONObject data = responseJson.getJSONObject("data");
				if(data != null) {
					JSONObject searchCompany = data.getJSONObject("searchCompany");
					if(searchCompany != null) {
						JSONArray results = (JSONArray) searchCompany.get("results");
						if(results!=null && results.length()>0)
							domain = results.getJSONObject(0).getString("domain").toString();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return domain;
		}
		return domain;
	}
	
	private String getCompanyDomainFromEverString(String companyName) {
		acquire();
		String domain= null;
		EverStringRealTimeRequestDTO dto = new EverStringRealTimeRequestDTO();
		dto.setName(companyName);
		try {
			EverStringRealTimeResponseDTO responseDTO = httpService.post(XtaasConstants.REALTIMEURL, headers(),
				new ParameterizedTypeReference<EverStringRealTimeResponseDTO>() {
			}, new ObjectMapper().writeValueAsString(dto));
			if(responseDTO != null && responseDTO.getData() != null) {
				domain = responseDTO.getData().get(0).getES_PrimaryWebsite();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return domain;
		}
		return domain;
	}

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");
		headers.add("Authorization", XtaasConstants.TOKEN);
		return headers;
	}
	
	private String  getCompanyDomainFromZoomInfo(String companyName) throws IOException {
		String website = null;
		try {
			URL url;
			String previewUrl = buildCompanyUrl(companyName);
			logger.debug(previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse zoomInfoResponse = getResponse(response);
			if (zoomInfoResponse != null && zoomInfoResponse.getCompanySearchResponse() != null
					&& zoomInfoResponse.getCompanySearchResponse().getCompanySearchResults() != null) {
				website = zoomInfoResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyWebsite();
			}
			return website;
		} catch (Exception e) {
			e.printStackTrace();
			return website;
		}

	}
	
	private ZoomInfoResponse getResponse(StringBuffer response) throws IOException {
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null && !response.toString().contains("<errorResponse><errorCode>")) {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
			return zoomInfoResponse;
		} else {
			return null;
		}
	}
	
	private String buildCompanyUrl(String companyName) {
		StringBuilder queryString = new StringBuilder();
		
		if (companyName != null)
			queryString.append("companyName=" + companyName.trim());
		
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;

		String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
		 String url = baseUrl
					+ "pc="+partnerKey
					+"&"+queryString//personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key="+key;
		return url;
	}
	
	public String getZoomOAuth() {
		String oAuth = "";

		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (cachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		
		Date currDate = new Date();

		long duration = currDate.getTime() - cachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		return tokenKey;
	}

	@Override
	public void acquire() {
		rateLimiter.acquire();
	}
}