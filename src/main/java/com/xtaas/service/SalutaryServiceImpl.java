package com.xtaas.service;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.AbmListDetail;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.ProspectSalutary;
import com.xtaas.db.entity.SalutoryEverstring;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.AbmListDetailRepository;
//import com.xtaas.db.repository.AbmListDetailRepositoryImpl;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectSalutaryRepository;
import com.xtaas.db.repository.SalatuaryRepositoryImpl;
import com.xtaas.db.repository.SalutoryEverstringRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.SendGirdEmailEvents;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.utils.DataBuyUtils;
import com.xtaas.utils.RangeUtils;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.HealthChecksCriteriaDTO;
import com.xtaas.web.dto.HealthChecksDTO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class SalutaryServiceImpl {

  private static final Logger logger = LoggerFactory.getLogger(SalutaryServiceImpl.class);

  @Autowired
  private SalatuaryRepositoryImpl salatuaryRepositoryImpl;
  


  @Autowired
  private ProspectCallLogRepository prospectCallLogRepository;

  @Autowired
  private AbmListService abmListService;

  @Autowired
  private CampaignRepository campaignRepository;
  
  @Autowired
  private DataBuyQueueRepository dataBuyQueueRepository;

  @Autowired
  private SuppressionListNewRepository suppressionListNewRepository;
  
  @Autowired
  private SalutoryEverstringRepository salutoryEverstringRepository;
  
  @Autowired
  private ProspectSalutaryRepository prospectSalutaryRepository; 

  @Autowired
  private AbmListDetailRepository abmListDetailRepository; 
  
  private int maxRecordsIndirect = 12;

  private int maxDirectNumbers = 20;

  private Long abmDataBuySalutary(List<Expression> campaignCriteriaDTO, Long requestCount, Campaign campaign,
      Long purchasedCount,boolean isDirectPurchase) {
  	  DataBuyQueue datapro  = dataBuyQueueRepository.findByCampaignIdAndStatus(campaign.getId(), "INPROCESS");

    boolean isAbmExists = true;
    boolean limitReached = false;
    int pageSize = 0;
    // int eachpageCount = 2;
    List<String> titleNOTINList = null;
    List<String> titleINList = null;
    for (Expression criteria : campaignCriteriaDTO) {
      String attribute = criteria.getAttribute();
      String value = criteria.getValue();
      String operator = criteria.getOperator();
      if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
        titleINList = new ArrayList<>();
        titleINList = Arrays.asList(value.split(","));
      } else if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
        titleNOTINList = new ArrayList<>();
        titleNOTINList = Arrays.asList(value.split(","));
      }
    }
    
    List<ProspectCallLog> callableProspects = getCallableList(campaign);
      HashMap<String, HashMap<String, Integer>> directIndirectCallableCompanyCountMap = XtaasUtils.getCallableDirectIndirectCompanyCountMap(callableProspects);  
      HashMap<String, Integer> directPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
          .get("directCompanyMap");
      HashMap<String, Integer> indirectPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
          .get("indirectCompanyMap");
    while (isAbmExists && !limitReached) {
  	  DataBuyQueue databuyq  = dataBuyQueueRepository.findOneById(datapro.getId());
    	if(databuyq!=null&& databuyq.getStatus().equalsIgnoreCase("KILL")) {
  		  limitReached = true;
  		  break;
  	  }
      Pageable pageable = PageRequest.of(pageSize, 50, Direction.ASC, "createdDate");
      List<AbmListNew> abmList = abmListService.getABMListByCampaignIdByPage(campaign.getId(), pageable);

      if (abmList != null && abmList.size() > 0) {
        List<String> domains = new ArrayList<String>();
        for (AbmListNew abm : abmList) {
          if (abm.getDomain() != null && !abm.getDomain().isEmpty())
            domains.add(abm.getDomain());
        }
        if(CollectionUtils.isEmpty(domains)){
            continue;
        }
        Query searchQuery = fetchProspectSalutaryListQuery(campaignCriteriaDTO, domains, null,null,isDirectPurchase);

        int eachpageCount = 10000;
        // while (!limitReached) {
        Pageable pageableSalutary = PageRequest.of(0, eachpageCount);
        List<ProspectSalutary> salutaryProspects = salatuaryRepositoryImpl.searchProspectSalutaryContacts(searchQuery,
            pageableSalutary);
        if (salutaryProspects != null && salutaryProspects.size() > 0) {
          for (ProspectSalutary prospectSalutary : salutaryProspects) {
            if (purchasedCount >= requestCount) {
              limitReached = true;
              break;
            }
            if (prospectSalutary.getProspectCall() != null && prospectSalutary.getProspectCall().getProspect() != null
                    && prospectSalutary.getProspectCall().getProspect().getTitle() != null) {
                  String title = prospectSalutary.getProspectCall().getProspect().getTitle();
                  if (titleINList != null && titleINList.size() > 0) {
                    boolean containsKeyword = false;
                    for (String keywordTitle : titleINList) {
                      if (title.contains(keywordTitle)) {
                        containsKeyword = true;
                        break;
                      }
                    }
                    if (!containsKeyword) {
                      continue;
                    }
                  }
                  if (titleNOTINList != null && titleNOTINList.size() > 0) {
                    boolean containsKeyword = false;
                    for (String keywordTitle : titleNOTINList) {
                      if (title.contains(keywordTitle)) {
                        containsKeyword = true;
                        break;
                      }
                    }
                    if (containsKeyword) {
                      continue;
                    }
                  }
                }
            if (!isPersonIdAlreadyPurchased(prospectSalutary, campaign.getId())) {
              if (directPhoneCallableCompanyMap != null && indirectPhoneCallableCompanyMap != null && isCompanyLimitReached(prospectSalutary, directPhoneCallableCompanyMap,
                  indirectPhoneCallableCompanyMap)) {
                continue;
              }
              
              ProspectCallLog prospectCallLogObj = new ProspectCallLog();
              prospectCallLogObj.setProspectCall(prospectSalutary.getProspectCall());
              prospectCallLogObj.getProspectCall().setProspectCallId(getUniqueProspect());
              if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign)) {// TODO change this once email type is
                                                                               // implemented
                prospectCallLogObj.getProspectCall().setSendGirdEmailEvents(new SendGirdEmailEvents());
              }
              prospectCallLogObj.getProspectCall().setCampaignId(campaign.getId());
              prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
              prospectCallLogObj.setDataSlice(prospectSalutary.getDataSlice());

              insertProspectCallLog(prospectCallLogObj);
              purchasedCount++;
            }
          }
        } else {
          limitReached = true;
          return purchasedCount;
        }
        // }
        pageSize++;
      } else {

        return purchasedCount;
      }
    }
    return purchasedCount;
  }

  private String getUniqueProspect() {
    String uniquepcid = UUID.randomUUID().toString();
    Date d = new Date();
    // System.out.println(uniquepcid+Long.toString(d.getTime()));
    String pcidv = uniquepcid + Long.toString(d.getTime());
    return pcidv;
  }

  public Long databuySaltary(List<Expression> campaignCriteriaDTO, Long requestCount, String campaignId, boolean isDirectPurchase) {
    Long purchasedCount = Long.parseLong("0");
    boolean isRevenue = false;

    Campaign campaign = campaignRepository.findOneById(campaignId);
    if (campaign.isABM()) {
      purchasedCount = abmDataBuySalutary(campaignCriteriaDTO, requestCount, campaign, purchasedCount,isDirectPurchase);
      return purchasedCount;
    }
    Map<Integer, String> mgmtSortMap = new TreeMap<Integer, String>(salatuaryRepositoryImpl.getMgmtWeighMap());
    String mgmtLevel = "";
    String department = "";
   
    ArrayList<PickListItem> pList = new ArrayList<PickListItem>();
    Expression mgmtExp = new Expression("mgmt", "mgmt", "mgmt", false, "MGMT", pList, "");
    for (Expression criteria : campaignCriteriaDTO) {
      String attribute = criteria.getAttribute();
      String value = criteria.getValue();
      String operator = criteria.getOperator();
      mgmtExp = criteria;
      if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
        mgmtLevel = value;
      }
      if (attribute.equalsIgnoreCase("DEPARTMENT")) {
          department = value;
      }
      // TODO this condition is a hack, once revenue data is fixed remove this condition.
      if (attribute.equalsIgnoreCase("REVENUE")) {
    	  isRevenue = true;
      }
    }
    
   /* if(isRevenue)
    	return purchasedCount;*/
 
  
    
    List<String> depList = new ArrayList<String>();
    if(department!=null && !department.isEmpty()) {
        depList = Arrays.stream(department.split(",")).collect(Collectors.toList());

    }
    
    if (mgmtLevel != null && !mgmtLevel.isEmpty()
    		&& department != null && !department.isEmpty()) {
      List<String> mgList = Arrays.stream(mgmtLevel.split(",")).collect(Collectors.toList());
      if (mgList.size() == 1 && mgList.get(0).equalsIgnoreCase("ALL")) {
    	  for (Map.Entry<Integer, String> entry : mgmtSortMap.entrySet()) {
    		  mgList.add(entry.getValue());
    	  }
      }
      
     /* List<String> empList = Arrays.stream(empLevel.split(",")).collect(Collectors.toList());
      if (empList.size() == 1 && empList.get(0).equalsIgnoreCase("ALL")) {
    	  for (Map.Entry<Integer, String> entry1 : empSortMap.entrySet()) {
    		  empList.add(entry1.getValue());
    	  }
      }else {
    	  empList = salatuaryRepositoryImpl.getSalutaryZoomMap(empList);

		  
    	 // for(String empstr : empList) {
    	     // List<String> emparr = Arrays.stream(empstr.split("-")).collect(Collectors.toList());
    	     // int result = RangeUtils.checkRange(Long.parseLong(emparr.get(0)), Long.parseLong(emparr.get(1)));
    	 // }
    	/*  Integer[] numbers = new Integer[] { 1, 2, 1, 3, 4, 4 };
    	  Set<Integer> allItems = new HashSet<>();
    	  Set<Integer> duplicates = Arrays.stream(numbers)
    	          .filter(n -> !allItems.add(n)) //Set.add() returns false if the item was already in the set.
    	          .collect(Collectors.toSet());
    	  System.out.println(duplicates); // [1, 4]*/
     // }
     
      
      for (Map.Entry<Integer, String> entry : mgmtSortMap.entrySet()) {
      	  DataBuyQueue datapro  = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId, "INPROCESS");
        if (mgList.contains(entry.getValue()) && datapro!=null) {
        	//for (Map.Entry<Integer, String> entry1 : empSortMap.entrySet()) {
        	  	 // DataBuyQueue datapro1  = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId, "INPROCESS");
               // if (empList.contains(entry1.getValue()) && datapro1!=null) {
                	for (String dep : depList) {
              	  	  DataBuyQueue datapro2  = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId, "INPROCESS");
                      if (datapro2!=null) {
                    	  purchasedCount = databuySaltary(campaignCriteriaDTO, requestCount, campaignId, entry.getValue(),
                    			  dep,purchasedCount,isDirectPurchase);
                      }
                	}
               // }
        	//}
//          purchasedCount = purchasedCount + mgcount;
        }
      }

    } else {
      purchasedCount = databuySaltary(campaignCriteriaDTO, requestCount, campaignId, null,null, purchasedCount,isDirectPurchase);
    }
    return purchasedCount;
  }

  private Long databuySaltary(List<Expression> campaignCriteriaDTO, Long requestCount, String campaignId,
      String mgmtLevel, String department, Long purchasedCount,boolean isDirectPurchase) {
  	  DataBuyQueue datapro  = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId, "INPROCESS");


    Campaign campaign = campaignRepository.findOneById(campaignId);

    Query searchQuery = fetchProspectSalutaryListQuery(campaignCriteriaDTO, null, mgmtLevel, department,isDirectPurchase);
    if (searchQuery != null) {
      int page = 0;
      boolean limitReached = false;
      int eachpageCount = 10000;
      // int eachpageCount = 2;
      List<String> titleNOTINList = null;
      List<String> titleINList = null;
      for (Expression criteria : campaignCriteriaDTO) {
        String attribute = criteria.getAttribute();
        String value = criteria.getValue();
        String operator = criteria.getOperator();
        if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
          titleINList = new ArrayList<>();
          titleINList = Arrays.asList(value.split(","));
        } else if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
          titleNOTINList = new ArrayList<>();
          titleNOTINList = Arrays.asList(value.split(","));
        }
      }
      List<ProspectCallLog> callableProspects = getCallableList(campaign);
      HashMap<String, HashMap<String, Integer>> directIndirectCallableCompanyCountMap = XtaasUtils.getCallableDirectIndirectCompanyCountMap(callableProspects);  
      HashMap<String, Integer> directPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
          .get("directCompanyMap");
      HashMap<String, Integer> indirectPhoneCallableCompanyMap = directIndirectCallableCompanyCountMap
          .get("indirectCompanyMap");
      int counterdebug = 1;
      while (!limitReached) {
    	  DataBuyQueue databuyq  = dataBuyQueueRepository.findOneById(datapro.getId());
      	if(databuyq!=null&& databuyq.getStatus().equalsIgnoreCase("KILL")) {
    		  limitReached = true;
    		  break;
    	  }
      	long queryStartTime = XtaasDateUtils
				  .getTimeInSeconds(new Date());
      	Pageable pageable = PageRequest.of(page, eachpageCount);
        List<ProspectSalutary> salutaryProspects = salatuaryRepositoryImpl.searchProspectSalutaryContacts(searchQuery,
            pageable);
        
		  long queryEndTime = XtaasDateUtils.getTimeInSeconds(new Date());
		  long queryTime = queryStartTime - queryEndTime;
		  
      	logger.info("time taken to fetch "+eachpageCount+" Records for campaign : "+"("+counterdebug+")"+campaignId+"---->"+queryTime+" Seconds");
      	counterdebug++;
      	if (salutaryProspects != null && salutaryProspects.size() > 0) {
        	long processingStartTime = XtaasDateUtils
  				  .getTimeInSeconds(new Date()); 
        
          for (ProspectSalutary prospectSalutary : salutaryProspects) {
              if (prospectSalutary != null && prospectSalutary.getProspectCall() != null && prospectSalutary.getProspectCall().getProspect() != null
                      && StringUtils.isNotBlank(prospectSalutary.getProspectCall().getProspect().getFirstName())
                      && StringUtils.isNotBlank(prospectSalutary.getProspectCall().getProspect().getLastName())
                      && StringUtils.isNotBlank(prospectSalutary.getProspectCall().getProspect().getEmail())
                      && StringUtils.isNotBlank(prospectSalutary.getProspectCall().getProspect().getPhone())) {

                  if (prospectSalutary.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") != null
                          && !prospectSalutary.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty()
                          && !campaign.isABM() && !campaign.isMaxEmployeeAllow()
                          && (Double.valueOf(prospectSalutary.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) >= 10000
                          || Double.valueOf(prospectSalutary.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString()) == 0)) {
                            continue;
                  }
                  // log processing time
                  if (purchasedCount >= requestCount) {
                      limitReached = true;
                      break;
                  }
                  if (prospectSalutary.getProspectCall() != null && prospectSalutary.getProspectCall().getProspect() != null
                          && prospectSalutary.getProspectCall().getProspect().getTitle() != null) {
                      String title = prospectSalutary.getProspectCall().getProspect().getTitle();
                      if (titleINList != null && titleINList.size() > 0) {
                          boolean containsKeyword = false;
                          // log for title search for each prospect
                          for (String keywordTitle : titleINList) {
                              if (title.contains(keywordTitle)) {
                                  containsKeyword = true;
                                  break;
                              }
                          }
                          if (!containsKeyword) {
                              continue;
                          }
                      }
                      if (titleNOTINList != null && titleNOTINList.size() > 0) {
                          boolean containsKeyword = false;
                          for (String keywordTitle : titleNOTINList) {
                              if (title.contains(keywordTitle)) {
                                  containsKeyword = true;
                                  break;
                              }
                          }
                          if (containsKeyword) {
                              continue;
                          }
                      }
                  }
                  if (!isPersonIdAlreadyPurchased(prospectSalutary, campaignId)) {
                      if (directPhoneCallableCompanyMap != null && indirectPhoneCallableCompanyMap != null && isCompanyLimitReached(prospectSalutary, directPhoneCallableCompanyMap,
                              indirectPhoneCallableCompanyMap)) {
                          continue;
                      }

                      ProspectCallLog prospectCallLogObj = new ProspectCallLog();
                      prospectCallLogObj.setProspectCall(prospectSalutary.getProspectCall());
                      prospectCallLogObj.getProspectCall().setProspectCallId(getUniqueProspect());
                      if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign)) {// TODO change this once email type is
                          // implemented
                          prospectCallLogObj.getProspectCall().setSendGirdEmailEvents(new SendGirdEmailEvents());
                      }
                      prospectCallLogObj.setDataSlice(prospectSalutary.getDataSlice());
                      prospectCallLogObj.getProspectCall().setCampaignId(campaignId);
                      prospectCallLogObj.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
                      Long totalBought = datapro.getTotalBought();
                      // update the Salutary data buy count
                      if (totalBought != null) {
                          totalBought = totalBought++;
                          datapro.setTotalBought(totalBought);
                          dataBuyQueueRepository.save(datapro);
                      }
                      try {
                          insertProspectCallLog(prospectCallLogObj);
                      } catch (Exception e) {
                          logger.error("SalutaryServiceImpl======>Error while inserting ProspectCallLog with exception : "
                                  + e.getStackTrace());
                          continue;
                      }
                      purchasedCount++;
                  }
              }
          }
          page++;
      	long processingEndTime = XtaasDateUtils.getTimeInSeconds(new Date());
 		  long processingTime = processingStartTime - processingEndTime;
        	logger.info("time taken to PROCESS "+eachpageCount+ " Records for campaign : "+"("+counterdebug+")"+campaignId+"---->"+processingTime+" Seconds");

        } else {
          limitReached = true;
        }
      }
    }
    return purchasedCount;
  }

	private ProspectSalutary updateProspectDetailsUsingEverStringAndZoominfo(ProspectSalutary pc) {
		try {
			if (pc.getProspectCall().getUpdatedSource() == null && pc.getProspectCall().getUpdatedDate() == null) {
				if (pc.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
						&& !pc.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
					String searchDomain = pc.getProspectCall().getProspect().getCustomAttributeValue("domain")
							.toString();
					List<SalutoryEverstring> dbList = salutoryEverstringRepository.findDomainList(getHostName(searchDomain));
					if (dbList != null && !dbList.isEmpty() && dbList.size() > 0) {
						SalutoryEverstring seObj = dbList.get(0);
						return updateDetailsUsingEverString(seObj, pc);
					} else {
						AbmListDetail abmObj = abmListDetailRepository
								//.getCompanyByDomain("www."+getHostName(searchDomain));
								.findOneByDomain("www."+getHostName(searchDomain));
						
						if (abmObj != null) {
							return updateDetailsUsingZoom(abmObj, pc);
						}
					}
				}
			}else {
				return pc;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public String getHostName(String url) {
		if (url != null && !url.isEmpty()) {
			url = url.toLowerCase();
			if (url.contains("http://") || url.contains("https://") 
					|| url.contains("HTTP://")
					|| url.contains("HTTPS://")) {

			} else {
				url = "http://" + url;
			}
			try {
				URI uri = new URI(url);
				String hostname = uri.getHost();
				if (hostname != null && !hostname.isEmpty()) {
					if (hostname.contains("www.") || hostname.contains("WWW.")) {
						return hostname = hostname.substring(4, hostname.length());
					}else {
						return hostname;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private ProspectSalutary updateDetailsUsingZoom(AbmListDetail abmObj, ProspectSalutary pc) {
		Map<String, Object> customAttributes = pc.getProspectCall().getProspect().getCustomAttributes();
		if(abmObj.getCompanyEmployeeCountRange() != null && !abmObj.getCompanyEmployeeCountRange().isEmpty()) {
			customAttributes.put("employeeBand", abmObj.getCompanyEmployeeCountRange());
		}
		if(abmObj.getCompanyRevenueRange() != null && !abmObj.getCompanyRevenueRange().isEmpty()) {
			customAttributes.put("revenueBand", abmObj.getCompanyRevenueRange());
		}
		if(abmObj.getCompanyWebsite() != null && !abmObj.getCompanyWebsite().isEmpty()) {
			customAttributes.put("zoomDomain",abmObj.getCompanyWebsite());
		}
		if(abmObj.getCompanyRevenueIn000s() > 0) {
			customAttributes.put("exactRevenue", abmObj.getCompanyRevenueIn000s());
		}
		if(abmObj.getCompanyEmployeeCount() > 0) {
			customAttributes.put("exactEmployee", abmObj.getCompanyEmployeeCount());
		}
		if(abmObj.getCompanyEmployeeCountRange() != null && !abmObj.getCompanyEmployeeCountRange().isEmpty()) {
			customAttributes.put("minEmployeeCount",Double.valueOf(abmObj.getCompanyEmployeeCountRange().split("-")[0].trim()));
			customAttributes.put("maxEmployeeCount",Double.valueOf(abmObj.getCompanyEmployeeCountRange().split("-")[1].trim()));
		}
		if(abmObj.getCompanyRevenueRange() != null && !abmObj.getCompanyRevenueRange().isEmpty()) {
			Map<String, String[]> revenuMap = getZoomRevenueMap();
			String[] revArr = revenuMap.get(abmObj.getCompanyRevenueRange());
			customAttributes.put("minRevenue",Double.valueOf(revArr[0]));
			customAttributes.put("maxRevenue", Double.valueOf(revArr[1]));
		}

		pc.getProspectCall().getProspect().setCustomAttributes(customAttributes);
		pc.getProspectCall().getProspect().setSourceCompanyId(abmObj.getCompanyId());

		List<String> companySIC = pc.getProspectCall().getProspect().getCompanySIC();
		if (abmObj.getCompanySIC() != null && !abmObj.getCompanySIC().isEmpty() && abmObj.getCompanySIC().size() > 0) {
			companySIC.addAll(abmObj.getCompanySIC());
			pc.getProspectCall().getProspect().setCompanySIC(companySIC);
		}

		List<String> companyNAICS = pc.getProspectCall().getProspect().getCompanyNAICS();
		if (abmObj.getCompanyNAICS() != null && !abmObj.getCompanyNAICS().isEmpty() && abmObj.getCompanyNAICS().size() > 0) {
			companyNAICS.addAll(abmObj.getCompanyNAICS());
			pc.getProspectCall().getProspect().setCompanyNAICS(companyNAICS);
		}
		pc.getProspectCall().setUpdatedDate(new Date());
		pc.getProspectCall().setUpdatedSource("ZOOMINFO");
		return prospectSalutaryRepository.save(pc);
		
	}

	private ProspectSalutary updateDetailsUsingEverString(SalutoryEverstring seObj, ProspectSalutary pc) {
		Map<String, Object> customAttributes = pc.getProspectCall().getProspect().getCustomAttributes();
		if(seObj.getES_EmployeeBand() != null && !seObj.getES_EmployeeBand().isEmpty()) {
			customAttributes.put("employeeBand", seObj.getES_EmployeeBand());
		}
		if(seObj.getES_RevenueBand() != null && !seObj.getES_RevenueBand().isEmpty()) {
			customAttributes.put("revenueBand", seObj.getES_RevenueBand());
		}
		if(seObj.getES_PrimaryWebsite() != null && !seObj.getES_PrimaryWebsite().isEmpty()) {
			customAttributes.put("everstringDomain",seObj.getES_PrimaryWebsite());
		}
		if(seObj.getES_NumLocations() > 0) {
			customAttributes.put("numLocations",seObj.getES_NumLocations());
		}
		if(seObj.getES_Revenue() > 0) {
			customAttributes.put("exactRevenue", seObj.getES_Revenue());
		}
		if(seObj.getES_Employee() > 0) {
			customAttributes.put("exactEmployee", seObj.getES_Employee());
		}
		if(seObj.getES_EmployeeBand() != null && !seObj.getES_EmployeeBand().isEmpty()) {
			String employeeBand = "";
			if(seObj.getES_EmployeeBand().equals("10,000+")) {
				employeeBand = "10,000-1,00,000";
			}else {
				employeeBand = seObj.getES_EmployeeBand();
			}
			customAttributes.put("minEmployeeCount",Double.valueOf(employeeBand.split("-")[0]));
			customAttributes.put("maxEmployeeCount",Double.valueOf(employeeBand.split("-")[1]));
		}
		if(seObj.getES_RevenueBand() != null && !seObj.getES_RevenueBand().isEmpty()) {
			Map<String, String[]> revenuMap = getEverStringRevenueMap();
			String[] revArr = revenuMap.get(seObj.getES_RevenueBand());
			customAttributes.put("minRevenue",Double.valueOf(revArr[0]));
			customAttributes.put("maxRevenue", Double.valueOf(revArr[1]));
		}

		pc.getProspectCall().getProspect().setCustomAttributes(customAttributes);
		pc.getProspectCall().getProspect().setSourceCompanyId(String.valueOf(seObj.getES_ECID()));

		List<String> companySIC = pc.getProspectCall().getProspect().getCompanySIC();
		if (seObj.getES_SIC2() != null && seObj.getES_SIC3() != null && seObj.getES_SIC4() != null) {
			companySIC.add(seObj.getES_SIC2());
			companySIC.add(seObj.getES_SIC3());
			companySIC.add(seObj.getES_SIC4());
			pc.getProspectCall().getProspect().setCompanySIC(companySIC);
		}

		List<String> companyNAICS = pc.getProspectCall().getProspect().getCompanyNAICS();
		if (seObj.getES_NAICS2() != null && seObj.getES_NAICS4() != null && seObj.getES_NAICS6() != null) {
			companyNAICS.add(seObj.getES_NAICS2());
			companyNAICS.add(seObj.getES_NAICS4());
			companyNAICS.add(seObj.getES_NAICS6());
			pc.getProspectCall().getProspect().setCompanyNAICS(companyNAICS);
		}
		pc.getProspectCall().setUpdatedDate(new Date());
		pc.getProspectCall().setUpdatedSource("EVERSTRING");
		return prospectSalutaryRepository.save(pc);
	}

	private Map<String, String[]> getEverStringRevenueMap() {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		// $0M-$1M
		String[] empArray1 = new String[2];
		empArray1[0] = "1000000";
		empArray1[1] = "1000000";
		empMap.put("$0M-$1M", empArray1);
		// $1M-$5M
		String[] empArray2 = new String[2];
		empArray2[0] = "1000000";
		empArray2[1] = "5000000";
		empMap.put("$1M-$5M", empArray2);
		// $10M-$25M
		String[] empArray3 = new String[2];
		empArray3[0] = "10000000";
		empArray3[1] = "25000000";
		empMap.put("$10M-$25M", empArray3);
		// $25M-$50M
		String[] empArray4 = new String[2];
		empArray4[0] = "25000000";
		empArray4[1] = "50000000";
		empMap.put("$25M-$50M", empArray4);
		// $50M-$100M
		String[] empArray5 = new String[2];
		empArray5[0] = "50000000";
		empArray5[1] = "100000000";
		empMap.put("$50M-$100M", empArray5);
		// $100M-$250M
		String[] empArray6 = new String[2];
		empArray6[0] = "100000000";
		empArray6[1] = "250000000";
		empMap.put("$100M-$250M", empArray6);
		// $250M-$500M
		String[] empArray7 = new String[2];
		empArray7[0] = "250000000";
		empArray7[1] = "500000000";
		empMap.put("$250M-$500M", empArray7);
		// $500M-$1B
		String[] empArray8 = new String[2];
		empArray8[0] = "500000000";
		empArray8[1] = "1000000000";
		empMap.put("$500M-$1B", empArray8);
		// $1B-$5B
		String[] empArray9 = new String[2];
		empArray9[0] = "1000000000";
		empArray9[1] = "5000000000";
		empMap.put("$1B-$5B", empArray9);
		// $5B+
		String[] empArray10 = new String[2];
		empArray10[0] = "5000000000";
		empArray10[1] = "10000000000";
		empMap.put("$5B+", empArray10);

		return empMap;
  }
	
	private Map<String, String[]> getZoomRevenueMap() {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		// $1 mil. - $5 mil.
		String[] empArray1 = new String[2];
		empArray1[0] = "1000000";
		empArray1[1] = "5000000";
		empMap.put("$1 mil. - $5 mil.", empArray1);
		// $5 mil. - $10 mil.
		String[] empArray2 = new String[2];
		empArray2[0] = "5000000";
		empArray2[1] = "10000000";
		empMap.put("$5 mil. - $10 mil.", empArray2);
		// $10 mil. - $25 mil.
		String[] empArray3 = new String[2];
		empArray3[0] = "10000000";
		empArray3[1] = "25000000";
		empMap.put("$10 mil. - $25 mil.", empArray3);
		// $25 mil. - $50 mil.
		String[] empArray4 = new String[2];
		empArray4[0] = "25000000";
		empArray4[1] = "50000000";
		empMap.put("$25 mil. - $50 mil.", empArray4);
		// $100 mil. - $250 mil.
		String[] empArray6 = new String[2];
		empArray6[0] = "100000000";
		empArray6[1] = "250000000";
		empMap.put("$100 mil. - $250 mil.", empArray6);
		// $500 mil. - $1 bil.
		String[] empArray8 = new String[2];
		empArray8[0] = "500000000";
		empArray8[1] = "1000000000";
		empMap.put("$500 mil. - $1 bil.", empArray8);
		// "$500,000 - $1 mil.",
		String[] empArray9 = new String[2];
		empArray9[0] = "500000";
		empArray9[1] = "1000000";
		empMap.put("$500,000 - $1 mil.", empArray9);
		// "Under $500,000"
		String[] empArray10 = new String[2];
		empArray10[0] = "500000";
		empArray10[1] = "1000000";
		empMap.put("Under $500,000", empArray10);

		return empMap;
  }
  
  private boolean isCompanyLimitReached(ProspectSalutary prospectSalutary,
      HashMap<String, Integer> directPhoneCallableCompanyMap,
      HashMap<String, Integer> indirectPhoneCallableCompanyMap) {
    boolean suppressed = false;
    if (prospectSalutary.getProspectCall() != null && prospectSalutary.getProspectCall().getProspect() != null
        && prospectSalutary.getProspectCall().getProspect().getCompany() != null) {
      String companyName = prospectSalutary.getProspectCall().getProspect().getCompany();
      if (prospectSalutary.getProspectCall().getProspect().isDirectPhone()) {
    	  Integer dCallableCount = directPhoneCallableCompanyMap.get(companyName);
        if (dCallableCount != null && dCallableCount >= maxDirectNumbers) {
          suppressed = true;
          return suppressed;
        }
        dCallableCount = dCallableCount != null ? dCallableCount : 0;
        directPhoneCallableCompanyMap.put(companyName, dCallableCount + 1);
      } else {
        Integer indCallableCount =  indirectPhoneCallableCompanyMap.get(companyName);
        if (indCallableCount != null && indCallableCount >= maxRecordsIndirect) {
          suppressed = true;
          return suppressed;
        }
        indCallableCount = indCallableCount != null ? indCallableCount : 0;
        indirectPhoneCallableCompanyMap.put(companyName, indCallableCount + 1);
      }
    }
    return suppressed;
  }

  private List<ProspectCallLog> getCallableList(Campaign campaign) {
    // Fetching the callables below

    List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
    List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList0);
    List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList1);
    List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList2);
    List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList3);
    List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList4);
    List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList5);
    List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList6);
    List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList7);
    List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList8);
    List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList9);
    List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,
        campaign.getId());
    prospectCallLogList.addAll(prospectCallLogList10);

    return prospectCallLogList;
  }
  
  private String getMgmtEmp() {
	  String value = "";
	  
	  return value;
	  
  }

  private Query fetchProspectSalutaryListQuery(List<Expression> campaignCriteriaDTO, List<String> abmDomains,
      String mgmtLevel, String departmentLevel, boolean  isDirectPurchase) {
    // create new map for List of attributes
    HashMap<String, Map<String, Object>> finalCriteriaMap = new HashMap<>();
    
    boolean maximumRev = false;
    boolean maximumEmp = false;
    boolean isEmployeeInCriteria = false;
    // for loop healthcheck criteria list
    for (Expression criteria : campaignCriteriaDTO) {
      String attribute = criteria.getAttribute();
      String value = criteria.getValue();
      String operator = criteria.getOperator();
      String collectionAttributeName = null;
      List<String> selectedValues = Arrays.stream(value.split(",")).collect(Collectors.toList());
      List<String> mappedSelectedValues = new ArrayList<>();
      // get mapping and fetch value for salutary
      if (attribute.equalsIgnoreCase("INDUSTRY")) {
        collectionAttributeName = "prospectCall.prospect.industry";
        Map<String, List<String>> industryMap = salatuaryRepositoryImpl.getSalutaryIndustry();
        if (industryMap != null) {
          for (String selectedValue : selectedValues) {
            if (industryMap.containsKey(selectedValue)) {
              mappedSelectedValues.addAll(industryMap.get(selectedValue));
            }
          }
        }
      } else if (attribute.equalsIgnoreCase("DEPARTMENT")) {
        collectionAttributeName = "prospectCall.prospect.department";
        Map<String, String> departmentMap = salatuaryRepositoryImpl.getSalutaryDepartment();
        if(departmentLevel!=null && !departmentLevel.isEmpty()) {
        	List<String> depValues = new ArrayList<String>();
        	depValues.add(departmentLevel);
        	selectedValues = depValues;
        }
        for (String selectedValue : selectedValues) {
          if (departmentMap.containsKey(selectedValue)) {
            mappedSelectedValues.add(departmentMap.get(selectedValue));
          }
        }
      } else if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
        collectionAttributeName = "prospectCall.prospect.managementLevel";
        Map<String, List<String>> managementLevelMap = salatuaryRepositoryImpl.getSalutaryMgmtLevel();
        if (mgmtLevel != null && !mgmtLevel.isEmpty()) {
          mappedSelectedValues.addAll(managementLevelMap.get(mgmtLevel));
        } else {
          for (String selectedValue : selectedValues) {
            if (managementLevelMap.containsKey(selectedValue)) {
              mappedSelectedValues.addAll(managementLevelMap.get(selectedValue));
            }
          }
        }
      } else if (attribute.equalsIgnoreCase("REVENUE")) {
    	  List<Integer> minRevList = new ArrayList<Integer>();
    	  List<Integer> maxRevList = new ArrayList<Integer>();
    	  Map<String,String[]> revMap = DataBuyUtils.getRevenueMap();

    	  String[] revenueArr = value.split(",");
    	  for(int i=0;i<revenueArr.length;i++){
    		  String revVal = revenueArr[i];
    		  String[] revArr = revMap.get(revVal);

    		  minRevList.add(Integer.parseInt(revArr[0]));
    		  maxRevList.add(Integer.parseInt(revArr[1]));
    	  }

    	  Collections.sort(minRevList);
    	  Collections.sort(maxRevList,Collections.reverseOrder());
    	  Long minRevenue = 0L;
    	  Long maxRevenue = 0L;

    	  if(minRevList.contains(new Integer(0))){
    	  }else if(minRevList.get(0)>0){
    		  minRevenue =   new Double(minRevList.get(0) * 1000).longValue();

    		 // minRevenue = minRevList.get(0).longValue()*1000;
    	  }
    	  if(maxRevList.contains(new Integer(0))){
    		  maxRevenue =  new Double(Long.valueOf(maxRevList.get(0)) * 1000).longValue() + 1;
    		  maximumRev = true;
    	  }else if(maxRevList.get(0)>0){
    		  maxRevenue =  new Double(Long.valueOf(maxRevList.get(0)) * 1000).longValue() + 1;
    		  //maxRevenue = maxRevList.get(0).longValue();
    	  }


    	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.minRevenue")
    			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.minRevenue")
    			  .containsKey(operator)) {
    		  if (minRevenue < (Long) finalCriteriaMap.get("prospectCall.prospect.customAttributes.minRevenue")
    				  .get(operator)) {
    			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.minRevenue").put(operator,
    					  minRevenue);
    		  }
    	  } else {
    		  Map<String, Object> operatorCriteriaRevMap = new HashMap<>();
    		  operatorCriteriaRevMap.put(operator, minRevenue);
    		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.minRevenue", operatorCriteriaRevMap);
    	  }
    	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.maxRevenue")
    			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxRevenue")
    			  .containsKey(operator)) {
    		  if (maxRevenue > (Long) finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxRevenue")
    				  .get(operator)) {
    			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxRevenue").put(operator,
    					  maxRevenue);
    		  }
    	  } else {
    		  Map<String, Object> operatorCriteriaRevMap = new HashMap<>();
    		  operatorCriteriaRevMap.put(operator, maxRevenue);
    		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.maxRevenue", operatorCriteriaRevMap);
    	  }

      } else if (attribute.equalsIgnoreCase("EMPLOYEE")) {
    	  isEmployeeInCriteria = true;
    	  List<Integer> minEmpList = new ArrayList<Integer>();
    	  List<Integer> maxEmpList = new ArrayList<Integer>();
    	  Map<String,String[]> empMapa = DataBuyUtils.getEmployeeMap();
    	  String[] employeeArr = value.split(",");
    	  for(int i=0;i<employeeArr.length;i++){
    		  String empVal = employeeArr[i];
    		  String[] empArr = empMapa.get(empVal);

    		  minEmpList.add(Integer.parseInt(empArr[0]));
    		  maxEmpList.add(Integer.parseInt(empArr[1]));
    	  }

    	  Collections.sort(minEmpList);
    	  Collections.sort(maxEmpList,Collections.reverseOrder());

    	  int minEmpCount = 0;
    	  int maxEmpCount = 0;
    	  
    	  if(minEmpList.contains(new Integer(0))){
    		  
    	  }else if(minEmpList.get(0)>0){
    		  minEmpCount = minEmpList.get(0).intValue();
    	  }
    	  if(maxEmpList.contains(new Integer(0))){
    		  maxEmpCount = maxEmpList.get(0).intValue() + 1;
    		  maximumEmp= true;
    	  }else if(maxEmpList.get(0)>0){
    		  maxEmpCount = maxEmpList.get(0).intValue() + 1;
    	  }
    	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.minEmployeeCount")
    			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.minEmployeeCount")
    			  .containsKey(operator)) {
    		  if (minEmpCount < (Integer) finalCriteriaMap
    				  .get("prospectCall.prospect.customAttributes.minEmployeeCount").get(operator)) {
    			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.minEmployeeCount").put(operator,
    					  minEmpCount);
    		  }
    	  } else {
    		  Map<String, Object> operatorCriteriaEmpMap = new HashMap<>();
    		  operatorCriteriaEmpMap.put(operator, minEmpCount);
    		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.minEmployeeCount",
    				  operatorCriteriaEmpMap);
    	  }
    	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.maxEmployeeCount")
    			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxEmployeeCount")
    			  .containsKey(operator)) {
    		  if (maxEmpCount > (Integer) finalCriteriaMap
    				  .get("prospectCall.prospect.customAttributes.maxEmployeeCount").get(operator)) {
    			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxEmployeeCount").put(operator,
    					  maxEmpCount);
    		  }
    	  } else {
    		  Map<String, Object> operatorCriteriaEmpMap = new HashMap<>();
    		  operatorCriteriaEmpMap.put(operator, maxEmpCount);
    		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.maxEmployeeCount",
    				  operatorCriteriaEmpMap);
    	  }

      } else if (attribute.equalsIgnoreCase("SIC_CODE") || attribute.equalsIgnoreCase("NAICS_CODE")) {
        if (attribute.equalsIgnoreCase("SIC_CODE")) {
          collectionAttributeName = "prospectCall.prospect.companySIC";
          String newValue = value.replace("SIC", "");
          selectedValues = Arrays.stream(newValue.split(",")).collect(Collectors.toList());
        } else if (attribute.equalsIgnoreCase("NAICS_CODE")) {
          collectionAttributeName = "prospectCall.prospect.companyNAICS";
          String newValue = value.replace("NAICS", "");
          selectedValues = Arrays.stream(newValue.split(",")).collect(Collectors.toList());
        }
        mappedSelectedValues = selectedValues;
      } else {
        if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
          collectionAttributeName = "prospectCall.prospect.country";
          if (selectedValues.contains("USA") || selectedValues.contains("usa")) {
            selectedValues.add("United States");
          }
        } else if (attribute.equalsIgnoreCase("VALID_STATE") || attribute.equalsIgnoreCase("STATE (US ONLY)")) {
          collectionAttributeName = "prospectCall.prospect.stateCode";
        } /*else if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
          // collectionAttributeName = "prospectCall.prospect.title";
          continue;
        } else if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
          // collectionAttributeName = "prospectCall.prospect.title";
          // operator = "NOT IN";
          continue;
        }*/ else if (attribute.equalsIgnoreCase("ZIP_CODE")) {
          collectionAttributeName = "prospectCall.prospect.zipCode";
        }
        mappedSelectedValues = selectedValues;
      }
      // add value to Map
      if (!attribute.equalsIgnoreCase("REVENUE") && !attribute.equalsIgnoreCase("EMPLOYEE")
          && !mappedSelectedValues.isEmpty() && collectionAttributeName != null && !collectionAttributeName.isEmpty()
          && !operator.isEmpty()) {
        Map<String, Object> operatorCriteriaMap = new HashMap<>();
        if (finalCriteriaMap.containsKey(collectionAttributeName)
            && finalCriteriaMap.get(collectionAttributeName).containsKey(operator)) {
          List<String> prevList = (List<String>) finalCriteriaMap.get(collectionAttributeName).get(operator);
          prevList.addAll(mappedSelectedValues);
          finalCriteriaMap.get(collectionAttributeName).put(operator, prevList);
        } else {
          operatorCriteriaMap.put(operator, mappedSelectedValues);
          finalCriteriaMap.put(collectionAttributeName, operatorCriteriaMap);
        }
      }
    }
    
    if(!isEmployeeInCriteria) {
		String defaultEmp = "1-5,5-10,10-20,20-50,50-100,100-250,250-500,500-1000,1000-5000,5000-10000";
		String[] employeeArr = defaultEmp.split(",");


  	  isEmployeeInCriteria = true;
  	  List<Integer> minEmpList = new ArrayList<Integer>();
  	  List<Integer> maxEmpList = new ArrayList<Integer>();
  	  Map<String,String[]> empMapa = DataBuyUtils.getEmployeeMap();
  	  
  	  for(int i=0;i<employeeArr.length;i++){
  		  String empVal = employeeArr[i];
  		  String[] empArr = empMapa.get(empVal);

  		  minEmpList.add(Integer.parseInt(empArr[0]));
  		  maxEmpList.add(Integer.parseInt(empArr[1]));
  	  }

  	  Collections.sort(minEmpList);
  	  Collections.sort(maxEmpList,Collections.reverseOrder());

  	  int minEmpCount = 0;
  	  int maxEmpCount = 0;
  	  
  	  if(minEmpList.contains(new Integer(0))){
  		  
  	  }else if(minEmpList.get(0)>0){
  		  minEmpCount = minEmpList.get(0).intValue();
  	  }
  	  if(maxEmpList.contains(new Integer(0))){
  		  maxEmpCount = maxEmpList.get(0).intValue() + 1;
  		  maximumEmp= true;
  	  }else if(maxEmpList.get(0)>0){
  		  maxEmpCount = maxEmpList.get(0).intValue() + 1;
  	  }
  	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.minEmployeeCount")
  			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.minEmployeeCount")
  			  .containsKey("IN")) {
  		  if (minEmpCount < (Integer) finalCriteriaMap
  				  .get("prospectCall.prospect.customAttributes.minEmployeeCount").get("IN")) {
  			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.minEmployeeCount").put("IN",
  					  minEmpCount);
  		  }
  	  } else {
  		  Map<String, Object> operatorCriteriaEmpMap = new HashMap<>();
  		  operatorCriteriaEmpMap.put("IN", minEmpCount);
  		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.minEmployeeCount",
  				  operatorCriteriaEmpMap);
  	  }
  	  if (finalCriteriaMap.containsKey("prospectCall.prospect.customAttributes.maxEmployeeCount")
  			  && finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxEmployeeCount")
  			  .containsKey("IN")) {
  		  if (maxEmpCount > (Integer) finalCriteriaMap
  				  .get("prospectCall.prospect.customAttributes.maxEmployeeCount").get("IN")) {
  			  finalCriteriaMap.get("prospectCall.prospect.customAttributes.maxEmployeeCount").put("IN",
  					  maxEmpCount);
  		  }
  	  } else {
  		  Map<String, Object> operatorCriteriaEmpMap = new HashMap<>();
  		  operatorCriteriaEmpMap.put("IN", maxEmpCount);
  		  finalCriteriaMap.put("prospectCall.prospect.customAttributes.maxEmployeeCount",
  				  operatorCriteriaEmpMap);
  	  }

    
	}
    
    // send ExpressionList to repo to get query
    if (!finalCriteriaMap.isEmpty()) {
      if (abmDomains != null && abmDomains.size() > 0) {
        Map<String, Object> abmOperatorCriteriaMap = new HashMap<>();
        abmOperatorCriteriaMap.put("IN", abmDomains);
        finalCriteriaMap.put("prospectCall.prospect.customAttributes.domain", abmOperatorCriteriaMap);
      }
      Query searchQuery = salatuaryRepositoryImpl.getSearchProspectSalutaryContactsQuery(finalCriteriaMap, abmDomains, maximumRev, 
    		  maximumEmp,isDirectPurchase);
      return searchQuery;
    } else {
      return null;
    }
  }

  private Long getRevenue(String revenue) {

    double million = 1000000;
    double billion = 1000000000;
    revenue = revenue.substring(1, revenue.length() - 1);
    if (revenue.contains("mil")) {
      double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
      // Long y = new Double(value * million).longValue();
      return new Double(value * million).longValue();

    } else if (revenue.contains(" bil")) {
      double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
      // Long x = new Double(value * billion).longValue();
      return new Double(value * billion).longValue();
    }
    return new Long(0);
  }

  private boolean isPersonIdAlreadyPurchased(ProspectSalutary personRecord, String campaignId) {
    if (personRecord != null && personRecord.getProspectCall() != null
        && personRecord.getProspectCall().getProspect() != null) {
      List<String> campaignList = new ArrayList<>();
      campaignList.add(campaignId);
      Prospect prospectDetails = personRecord.getProspectCall().getProspect();
      String firstName = prospectDetails.getFirstName();
      String lastName = prospectDetails.getLastName();
      String title = prospectDetails.getTitle();
      String company = prospectDetails.getCompany();
      int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordGroup(campaignList, firstName, lastName,
          title, company);
      if (uniqueRecord == 0) {
        return false;
      } else {
        logger.debug(firstName + " " + lastName + " " + company + " | Prospect #### : Already purchased in campaign : "
            + campaignId);
        return true;
      }
    }
    return true;
  }

  private void insertProspectCallLog(ProspectCallLog prospectCallLogPurchased) {
      if(StringUtils.isBlank(prospectCallLogPurchased.getProspectCall().getDataSlice())){
          prospectCallLogPurchased.getProspectCall().setDataSlice("slice10");
      }
      if(StringUtils.isBlank(prospectCallLogPurchased.getDataSlice())){
          prospectCallLogPurchased.setDataSlice("slice10");
      }
    prospectCallLogRepository.save(prospectCallLogPurchased);
  }

  private boolean isCompanySuppressedDetail(ProspectCallLog domain, CampaignCriteria campaignCriteria) {
    boolean suppressed = false;
    if (domain.getProspectCall() != null && domain.getProspectCall().getProspect() != null
        && domain.getProspectCall().getProspect().getCustomAttributes() != null
        && domain.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
        && !domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
      String pdomain = domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
      if (pdomain != null && !pdomain.isEmpty()) {
        pdomain = getSuppressedHostName(pdomain);
      }
      if (domain.getProspectCall().getProspect().getEmail() != null
          && !domain.getProspectCall().getProspect().getEmail().isEmpty()) {
        String pEmail = domain.getProspectCall().getProspect().getEmail();
        String domEmail = pEmail.substring(pEmail.indexOf("@") + 1, pEmail.length());

        long compSupCount = suppressionListNewRepository
            .countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), domEmail);
        if (compSupCount > 0) {
          suppressed = true;
        }
      }

      if (pdomain != null && !pdomain.isEmpty()) {
        long domainSupCount = suppressionListNewRepository
            .countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), pdomain);
        if (domainSupCount > 0) {
          suppressed = true;
        }
      }
    }
    return suppressed;
  }

  private String getSuppressedHostName(String domain) {
    try {
      if (domain != null) {
        domain = domain.toLowerCase();
        if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
            || domain.contains("HTTPS://")) {

        } else {
          domain = "http://" + domain;
        }
      }

      URL aURL = new URL(domain);
      domain = aURL.getHost();
      if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
        if ((domain.contains("www.") || domain.contains("WWW."))) {
          domain = domain.substring(4, domain.length());
          /*
           * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
           */
          return domain;
          // }
        } else {
          /*
           * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
           * return domain; }else{
           */
          return domain;
          // }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return domain;
  }
}