package com.xtaas.service;

import com.xtaas.db.entity.CampaignMetaData;

public interface CampaignMetaDataService {

	public CampaignMetaData getCampaignMetaData();

	public void resetCampaignMetaData();
}
