package com.xtaas.service;

import org.springframework.dao.DuplicateKeyException;

import com.xtaas.db.entity.Organization;
import com.xtaas.mvc.controller.OrganizationController;
import com.xtaas.web.dto.OrganizationDTO;

public interface OrganizationService {
	Organization findOne(String organizationId);
	void updateOrganization(Organization organization);
	
	public Organization createOrganization(OrganizationDTO  organizationDTO) throws DuplicateKeyException;
	
	public void storeOrganizationsInMemory();
	
	public Boolean clearOrganizationsInMemory();
	
	public Organization getOrganizationFromMemory(String organizationId);
}
