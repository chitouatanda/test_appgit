package com.xtaas.service;

import java.util.HashMap;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.mvc.model.Agent;

/**
 * @author djain
 *
 */
public interface ProspectService {

	void processAgentSubmission(Agent agentResponse, HashMap<String, String> attributeResponseMap, HashMap<String, CRMAttribute> prospectAttributeMap);
	
}
