package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xtaas.db.entity.TitleCategoryMapping;
import com.xtaas.db.repository.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.IllegalAddException;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.monitorjbl.xlsx.StreamingReader;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Organization.OrganizationLevel;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.Pivot;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.SupervisorPivotDTO;

@Service
public class SupervisorPivotServiceImpl implements SupervisorPivotService {

	private static final Logger logger = LoggerFactory.getLogger(SupervisorPivotServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	ProspectCallLogServiceImpl prospectCallLogServiceImpl;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private CacheRefreshService cacheRefreshService;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	@Autowired
	private DataProviderService dataProviderService;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private TitleCategoryMappingRepository titleCategoryMappingRepository;

	private InputStream fileInputStream;
	private Sheet sheet;
	private String fileName;
	private String bucketNameReports = "xtaasreports";

	@Override
	public SupervisorPivotDTO getSupervisorPivots(String campaignId, boolean isLimit) {
		SupervisorPivotDTO supervisorPivotDTO = new SupervisorPivotDTO();
		logger.debug("getSupervisorPivots() : Getting supervisor pivots for the campaignId: " + campaignId);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		supervisorPivotDTO.totalRecords = prospectCallLogRepository.findAllCallableRecordCount(campaign);
		List<Pivot> industryResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "industry",
				isLimit);
		if (!industryResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, industryResult, "industry", null);
		}
		List<Pivot> managementResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign,
				"managementLevel", isLimit);
		if (!managementResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, managementResult, "managementLevel", null);
		}
		List<Pivot> departmentResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "department",
				isLimit);
		if (!departmentResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, departmentResult, "department", null);
		}
		List<Pivot> employeeCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign,
				"employeeCount", isLimit);
		if (!employeeCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, employeeCountResult, "employeeCount", null);
		}
		List<Pivot> revenueCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "revenue",
				isLimit);
		if (!revenueCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, revenueCountResult, "revenue", null);
		}
		List<Pivot> titleCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "title",
				isLimit);
		if (!titleCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, titleCountResult, "title", null);
		}
		List<Pivot> countryCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "country",
				isLimit);
		if (!countryCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, countryCountResult, "country", null);
		}
		List<Pivot> stateCodeCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "stateCode",
				isLimit);
		if (!stateCodeCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, stateCodeCountResult, "stateCode", null);
		}
		List<Pivot> companyCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "company",
				isLimit);
		if (!companyCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, companyCountResult, "company", null);
		}

		List<Pivot> sourceCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "source",
				isLimit);
		if (!sourceCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, sourceCountResult, "source", null);
		}
		List<Pivot> isEuCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "isEu",
				isLimit);
		if (!isEuCountResult.isEmpty()) {
			setToSupervisorPivotDTO(supervisorPivotDTO, isEuCountResult, "isEu", null);
		}
		setToSupervisorPivotDTO(supervisorPivotDTO, null, "industryList", getPivotsList(campaign, null,"industryList"));
		setToSupervisorPivotDTO(supervisorPivotDTO, null, "sicList", getPivotsList(campaign, null,"sicList"));
		setToSupervisorPivotDTO(supervisorPivotDTO, null, "naicsList", getPivotsList(campaign, null,"naicsList"));
		return supervisorPivotDTO;
	}

	private void setToSupervisorPivotDTO(SupervisorPivotDTO supervisorPivotDTO, List<Pivot> resultFromDB, String field,
			Map<String, Integer> pivotTypeMap) {
		List<Pivot> pivots = new ArrayList<>();
		double percentage = 0.0;
		if (resultFromDB != null && resultFromDB.size() > 0) {
			for (Pivot object : resultFromDB) {
				Pivot pivot = new Pivot();
				pivot.setName(object.getId());
				pivot.setCount(object.getCount());
				if (supervisorPivotDTO.totalRecords != 0) {
					percentage = object.getCount() / Double.valueOf(supervisorPivotDTO.totalRecords);
					percentage *= 100;
					pivot.setPercentage(percentage);
				}
				pivots.add(pivot);
			}
		}

		// sort pivots in descending order by percentage
//		pivots.sort((p1, p2) -> Double.compare(p2.getPercentage(), p1.getPercentage()));

		if (field.equalsIgnoreCase("department")) {
			supervisorPivotDTO.setDepartment(pivots);
		}
		if (field.equalsIgnoreCase("industry")) {
			supervisorPivotDTO.setIndustry(pivots);
		}
		if (field.equalsIgnoreCase("managementLevel")) {
			supervisorPivotDTO.setManagementLevel(pivots);
		}
		if (field.equalsIgnoreCase("employeeCount")) {
			for(Pivot pivot : pivots) {
				if (pivot != null && pivot.getName() != null && pivot.getName().startsWith("0")) {
					pivot.setName("10K+");
				}
			}
			supervisorPivotDTO.setEmployeeCount(pivots);
		}
		if (field.equalsIgnoreCase("revenue")) { 
			System.out.println("3 Pivot revenue"+pivots);
			for(Pivot pivot : pivots) {
				if (pivot != null && pivot.getName() != null && pivot.getName().startsWith("0")) {
					pivot.setName("5B+");
				}
			}
			supervisorPivotDTO.setRevenue(pivots);
		}
		if (field.equalsIgnoreCase("title")) {
			supervisorPivotDTO.setTitle(pivots);
		}
		if (field.equalsIgnoreCase("country")) {
			supervisorPivotDTO.setCountry(pivots);
		}
		if (field.equalsIgnoreCase("stateCode")) {
			supervisorPivotDTO.setStateCode(pivots);
		}
		if (field.equalsIgnoreCase("company")) {
			supervisorPivotDTO.setCompany(pivots);
		}
		if (field.equalsIgnoreCase("source")) {
			Map<String, String> dataSourceList = dataProviderService.getXtaasDataProviderByIndex();
			for (Pivot pivot : pivots) {
				if (dataSourceList.containsKey(pivot.getName())) {
					for(Map.Entry<String,String> entry : dataSourceList.entrySet()) {
						if(entry.getKey().equals(pivot.getName())) {
							pivot.setName(entry.getValue());
						}
					}
				}
			}
			supervisorPivotDTO.setSource(pivots);
		}
		if (field.equalsIgnoreCase("isEu")) {
			for(Pivot pivot : pivots) {
				if(pivot.getName().equals("true")) {
					pivot.setName("Europe Data");
				}else {
					pivot.setName("Non Europe Data");
				}
			}
			supervisorPivotDTO.setIsEu(pivots);
		}

		if ((field.equalsIgnoreCase("industryList") || field.equalsIgnoreCase("sicList")
				|| field.equalsIgnoreCase("naicsList")) && pivotTypeMap.size() > 0) {
			for (Map.Entry<String, Integer> industry : pivotTypeMap.entrySet()) {
				Pivot pivot = new Pivot();
				pivot.setName(industry.getKey());
				pivot.setCount(industry.getValue());
				if (supervisorPivotDTO.totalRecords != 0) {
					percentage = industry.getValue() / Double.valueOf(supervisorPivotDTO.totalRecords);
					percentage *= 100;
					pivot.setPercentage(percentage);
				}
				pivots.add(pivot);
			}
			if (field.equalsIgnoreCase("industryList")) {
				supervisorPivotDTO.setIndustryList(pivots);
			} else if (field.equalsIgnoreCase("sicList")) {
				supervisorPivotDTO.setSicList(pivots);
			} else if (field.equalsIgnoreCase("naicsList")) {
				supervisorPivotDTO.setNaicsList(pivots);
			}
			//System.out.println("Pivot : "+pivots);
		}
	}

	@Override
	public void downloadPivots(String campaignId, HttpServletRequest request, HttpServletResponse response,
			Boolean downloadFlag) throws IOException {
		boolean isPartner = XtaasUserUtils.getCurrentLoggedInUsersOrganization().getOrganizationLevel()
				.equals(OrganizationLevel.PARTNER);
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		try {
			Runnable r = () -> {
				try {
					downloadOrSendPivot(campaignId, request, response, downloadFlag, false, isPartner, login);
				} catch (Exception e) {
					e.printStackTrace();
				}
			};
			Thread t = new Thread(r);
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			Campaign campaign = campaignRepository.findOneById(campaignId);
			SupervisorStats supervisorStats = supervisorStatsRepository
					.findBycampaignManagerIdAndcampaignId(login.getId(), campaign.getId(), "PIVOT_DOWNLOAD", "INPROCESS");
			if (supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : Pivot file download failed, please contact support";
				teamNoticeSerive.saveUserNotices(login.getId(), notice);
			}
		}
	}

	private void downloadOrSendPivot(String campaignId, HttpServletRequest request, HttpServletResponse response,
			boolean isLimit, boolean sendEmail, boolean isPartner, Login login) throws IOException {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		try {
			String campaignName = campaignId;
			if (campaign != null && campaign.getName() != null && !campaign.getName().isEmpty()) {
				campaignName = campaign.getName();
			}
			SupervisorPivotDTO supervisorPivotDTO = new SupervisorPivotDTO();
			supervisorPivotDTO = getSupervisorPivots(campaignId, isLimit);
			DecimalFormat decimalFormat = new DecimalFormat("#.##");

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet();
			sheet.setColumnWidth(0, 8000);
			sheet.setColumnWidth(1, 4000);
			sheet.setColumnWidth(2, 4000);
			XSSFFont headerCellFont = workbook.createFont();
			headerCellFont.setFontHeightInPoints((short) 12);
			headerCellFont.setFontName("IMPACT");
			XSSFFont labelCellFont = workbook.createFont();
			labelCellFont.setFontHeightInPoints((short) 12);
			labelCellFont.setFontName("ARIAL");
			XSSFCellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFillForegroundColor(HSSFColorPredefined.YELLOW.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerCellStyle.setFont(headerCellFont);
			XSSFCellStyle labelCellStyle = workbook.createCellStyle();
			labelCellStyle.setFillForegroundColor(HSSFColorPredefined.LIGHT_YELLOW.getIndex());
			labelCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			labelCellStyle.setFont(labelCellFont);

			int rowCount = 0;
			if (supervisorPivotDTO.getTotalRecords() >= 0) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("TOTAL RECORDS : ");
				cell.setCellStyle(headerCellStyle);
				Cell cell2 = row.createCell(1);
				cell2.setCellValue(supervisorPivotDTO.getTotalRecords());
				cell2.setCellStyle(headerCellStyle);
				rowCount++;
			}
			rowCount++;
			if (supervisorPivotDTO.getManagementLevel() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("MANAGEMENT LEVELS :");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getManagementLevel();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getDepartment() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("DEPARTMENT : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getDepartment();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getEmployeeCount() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("EMPLOYEE COUNT : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getEmployeeCount();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getRevenue() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("REVENUE : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getRevenue();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getIndustry() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("INDUSTRY : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getIndustry();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getTitle() != null) {
				List<Pivot> pivots = supervisorPivotDTO.getTitle();
				List<TitleCategoryMapping> titleCategoryMappings = titleCategoryMappingRepository.findAll();
				if(CollectionUtils.isNotEmpty(titleCategoryMappings)){
					for(TitleCategoryMapping mapping : titleCategoryMappings){
						if(mapping != null && CollectionUtils.isNotEmpty(mapping.getTitles())){
							for(String title : mapping.getTitles()) {
								for(Pivot p : pivots) {
									if(p.getName().equalsIgnoreCase(title) ) {
										p.setCategory(mapping.getCategoryName());
									}
									if(StringUtils.isBlank(p.getCategory())){
										String bpurchaseTitle = p.getName().toLowerCase();
										String dbPurchaseTitle = title.toLowerCase();

										List<String> spacebpurchaseTitle = Arrays.asList(bpurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));
										List<String> spacedbPurchaseTitle = Arrays.asList(dbPurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));
										if (spacedbPurchaseTitle.containsAll(spacebpurchaseTitle)) {
											p.setCategory(mapping.getCategoryName());
										}

									}
								}
							}
						}
					}

				}
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("TITLE : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
				supervisorPivotDTO.setTitleCategory(pivots);
			}
			rowCount++;
			if (supervisorPivotDTO.getCountry() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("COUNTRY : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getCountry();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getCompany() != null && !isPartner) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("COMPANY : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getCompany();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getSource() != null && !isPartner) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("SOURCE : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getSource();
				Iterator<Pivot> iterator = pivots.iterator();
				List<String> dataProviderList = dataProviderService.getXtaasDataProvider();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					Cell cellSource = newRow.createCell(0);
					if (dataProviderList.contains(pivot.getName())) {
						cellSource.setCellValue("Xtaas Data Source");
					} else {
						cellSource.setCellValue(pivot.getName());
					}
					cellSource = newRow.createCell(1);
					cellSource.setCellValue(pivot.getCount());
					cellSource = newRow.createCell(2);
					cellSource.setCellValue(decimalFormat.format(pivot.getPercentage()));
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getIndustryList() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("INDUSTRY LIST : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getIndustryList();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getStateCode() != null && !isPartner) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("STATE CODE : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getStateCode();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getIsEu() != null && !isPartner) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("EUROPE(FOR GDPR) : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getIsEu();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getSicList() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("SIC LIST : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getSicList();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;
			if (supervisorPivotDTO.getNaicsList() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("NAICS LIST : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getNaicsList();
				Iterator<Pivot> iterator = pivots.iterator();
				while (iterator.hasNext()) {
					Row newRow = sheet.createRow(rowCount);
					Pivot pivot = iterator.next();
					writeToExcel(pivot, decimalFormat, newRow, workbook);
					rowCount++;
				}
			}
			rowCount++;

			if (supervisorPivotDTO.getTitleCategory() != null) {
				Row row = sheet.createRow(rowCount);
				Cell cell = row.createCell(0);
				cell.setCellValue("TITLE CATEGORY : ");
				cell.setCellStyle(headerCellStyle);
				rowCount++;
				row = sheet.createRow(rowCount);
				writeHeaders(row, labelCellStyle);
				rowCount++;
				List<Pivot> pivots = supervisorPivotDTO.getTitleCategory();
				List<Pivot> newCategoryList = new ArrayList<>();
				List<Pivot> notEmptyCategoryList = pivots.stream().filter(obj -> obj.getCategory() != null).collect(Collectors.toList());
				if(CollectionUtils.isNotEmpty(notEmptyCategoryList)){
					List<String> categoryList = notEmptyCategoryList.stream().map(obj -> obj.getCategory()).collect(Collectors.toList());
					Map<String, Long> counts = categoryList.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
					for (Map.Entry<String, Long> val : counts.entrySet()) {
						Pivot p = new Pivot();
						p.setName(val.getKey());
						p.setCount(val.getValue().intValue());
						if (supervisorPivotDTO.totalRecords != 0) {
							double percentage = 0.0;
							percentage = p.getCount() / Double.valueOf(supervisorPivotDTO.totalRecords);
							percentage *= 100;
							p.setPercentage(percentage);
						}
						newCategoryList.add(p);
					}
				}
				if(CollectionUtils.isNotEmpty(newCategoryList)){
					Iterator<Pivot> iterator = newCategoryList.iterator();
					while (iterator.hasNext()) {
						Row newRow = sheet.createRow(rowCount);
						Pivot pivot = iterator.next();
						writeToExcel(pivot, decimalFormat, newRow, workbook);
						rowCount++;
					}
				}
			}
			String fileName = campaignName + " pivots.xlsx";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			if (sendEmail) {

				sendEmail(campaign, login, sendEmail);
			} else {
//				response.setContentType("application/ms-excel");
//				response.setContentType("application/vnd.ms-excel");
//				response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
//				workbook.write(response.getOutputStream());
				sendEmail(campaign, login, false);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(
					login.getId(), campaign.getId(), "PIVOT_DOWNLOAD", "INPROCESS");
			if (supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : Pivot file download failed, please contact support";
				teamNoticeSerive.saveUserNotices(login.getId(), notice);
			}
		}
	}

	private void writeHeaders(Row row, XSSFCellStyle labelCellStyle) {
		Cell cell = row.createCell(0);
		cell.setCellValue("Label");
		cell.setCellStyle(labelCellStyle);
		cell = row.createCell(1);
		cell.setCellValue("Category");
		cell.setCellStyle(labelCellStyle);
		cell = row.createCell(2);
		cell.setCellValue("Count");
		cell.setCellStyle(labelCellStyle);
		cell = row.createCell(3);
		cell.setCellValue("Percentage");
		cell.setCellStyle(labelCellStyle);
		cell = row.createCell(4);
		cell.setCellValue("Action");
		cell.setCellStyle(labelCellStyle);
	}

	private void writeToExcel(Pivot pivot, DecimalFormat decimalFormatm, Row row, Workbook workbook) {
		Cell cell = row.createCell(0);
		cell.setCellValue(pivot.getName());
		cell = row.createCell(1);
		cell.setCellValue(pivot.getCategory());
		cell = row.createCell(2);
		cell.setCellValue(pivot.getCount());
		cell = row.createCell(3);
		cell.setCellValue(decimalFormatm.format(pivot.getPercentage()));
	}

	@Override
	public String nukeCallableProspect(MultipartFile file, String campaignId) throws IOException {
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};

			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				// XSSFWorkbook book;
				Workbook book;
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(fileName + " file is empty");
						setPivotError(campaignId,null,fileName + " file is empty");
						return fileName + " file is empty";
					}

					validateExcelFileRecords(campaignId);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					setPivotError(campaignId,null,"Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					setPivotError(campaignId,e,null);
					e.printStackTrace();
				}
			} else {
				setPivotError(campaignId,null,"Invalid file format. Only .xls, .xlsx accepted.");
				throw new IllegalArgumentException("Invalid file format. Only .xls, .xlsx accepted.");
			}
		}
		return null;
	}

	public void setPivotError(String campaignId, Exception e, String errorMsg) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if(campaign != null && loginUser != null) {
			SupervisorStats supervisorStats = supervisorStatsRepository
					.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaign.getId(), "PIVOT_UPLOAD", "INPROCESS");
			if (supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				if (errorMsg != null) {
					supervisorStats.setError(errorMsg);
				}
				if (e != null) {
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				}
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : Pivot file upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}
		
	}
	
	private void validateExcelFileRecords(String campaignId) {
		int rowsCount = sheet.getLastRowNum();
		if (rowsCount < 1) {
			logger.error("No records found in file " + fileName);
			setPivotError(campaignId, null, "No records found in file " + fileName);
			return;
		}
		int colCounts = 4;
		processRecords(0, rowsCount, colCounts, campaignId);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		cacheRefreshService.clearCache(campaign);
	}

	private void processRecords(int startPos, int endPos, int colCount, String campaignId) {
		try {
			Map<String, String> managementMap = new HashMap<String, String>();
			Map<String, String> departmentMap = new HashMap<String, String>();
			Map<String, String> employeeCountMap = new HashMap<String, String>();
			Map<String, String> revenueMap = new HashMap<String, String>();
			Map<String, String> industryMap = new HashMap<String, String>();
			Map<String, String> titleMap = new HashMap<String, String>();
			Map<String, String> countryMap = new HashMap<String, String>();
			Map<String, String> sicCodeMap = new HashMap<String, String>();
			Map<String, String> stateMap = new HashMap<String, String>();
			Map<String, String> companyMap = new HashMap<String, String>();
			Map<String, String> sourceMap = new HashMap<String, String>();
			Map<String, String> industryListMap = new HashMap<String, String>();
			Map<String, String> stateCodeMap = new HashMap<String, String>();
			Map<String, String> isEuMap = new HashMap<String, String>();
			Map<String, String> sicListMap = new HashMap<String, String>();
			Map<String, String> naicsListMap = new HashMap<String, String>();
			
			List<String> managementList = new ArrayList<String>();
			List<String> departmentList = new ArrayList<String>();
			List<String> employeeCountList = new ArrayList<String>();
			List<String> revenueList = new ArrayList<String>();
			List<String> industryList = new ArrayList<String>();
			List<String> titleList = new ArrayList<String>();
			List<String> countryList = new ArrayList<String>();
			List<String> sicCodeList = new ArrayList<String>();
			List<String> stateList = new ArrayList<String>();
			List<String> companyList = new ArrayList<String>();
			List<String> sourceList = new ArrayList<String>();
			List<String> industriesList = new ArrayList<String>();
			List<String> stateCodeList = new ArrayList<String>();
			List<String> isEuList = new ArrayList<String>();
			List<String> sicList = new ArrayList<String>();
			List<String> naicsList = new ArrayList<String>();
			
			String managementAction = XtaasConstants.SOFT_DELETE;
			String departmentAction = XtaasConstants.SOFT_DELETE;
			String employeeAction = XtaasConstants.SOFT_DELETE;
			String revenueAction = XtaasConstants.SOFT_DELETE;
			String industryAction = XtaasConstants.SOFT_DELETE;
			String titleAction = XtaasConstants.SOFT_DELETE;
			String countryAction = XtaasConstants.SOFT_DELETE;
			String sicCodeAction = XtaasConstants.SOFT_DELETE;
			String stateAction = XtaasConstants.SOFT_DELETE;
			String companyAction = XtaasConstants.SOFT_DELETE;
			String sourceAction = XtaasConstants.SOFT_DELETE;
			String industryListAction = XtaasConstants.SOFT_DELETE;
			String stateCodeAction = XtaasConstants.SOFT_DELETE;
			
			Campaign campaign = campaignRepository.findOneById(campaignId);
			while (startPos < endPos) {
				Row row = sheet.getRow(startPos);
				if (row != null && row.getCell(0) != null) {
					if (row.getCell(0).getStringCellValue().trim()
							.equalsIgnoreCase(XtaasConstants.MANAGEMENT_LEVELS + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.MANAGEMENT_LEVELS, XtaasConstants.DEPARTMENT,
								managementList, managementMap);
					} else if (row.getCell(0).getStringCellValue().trim()
							.equalsIgnoreCase(XtaasConstants.DEPARTMENT + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.DEPARTMENT, XtaasConstants.EMPLOYEE_COUNT,
								departmentList, departmentMap);
					} else if (row.getCell(0).getStringCellValue().trim()
							.equalsIgnoreCase(XtaasConstants.EMPLOYEE_COUNT + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.EMPLOYEE_COUNT, XtaasConstants.REVENUE,
								employeeCountList, employeeCountMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.REVENUE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.REVENUE, XtaasConstants.INDUSTRY,
								revenueList, revenueMap);

					} else if (row.getCell(0).getStringCellValue().trim()
							.equalsIgnoreCase(XtaasConstants.INDUSTRY + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.INDUSTRY, XtaasConstants.TITLE, industryList,
								industryMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.TITLE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.TITLE, XtaasConstants.VALID_COUNTRY, titleList,
								titleMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.VALID_COUNTRY + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.VALID_COUNTRY, XtaasConstants.SIC_CODE, countryList,
								countryMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.SIC_CODE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.SIC_CODE, XtaasConstants.VALID_STATE, sicCodeList,
								sicCodeMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.VALID_STATE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.VALID_STATE, XtaasConstants.COMPANY, stateList,
								stateMap);

					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.COMPANY + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.COMPANY, XtaasConstants.SOURCE, companyList,
								companyMap);
					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.SOURCE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.SOURCE, XtaasConstants.INDUSTRY_LIST,
								sourceList, sourceMap);
						/*
						 * if (row.getCell(3) != null &&
						 * row.getCell(3).getStringCellValue().equalsIgnoreCase(XtaasConstants.
						 * HARD_DELETE)) sourceAction = XtaasConstants.HARD_DELETE;
						 */
					} else if (row.getCell(0).getStringCellValue().trim()
							.equalsIgnoreCase(XtaasConstants.INDUSTRY_LIST + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.INDUSTRY_LIST, XtaasConstants.STATE_CODE, industriesList,
								industryListMap);
					} else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.STATE_CODE + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.STATE_CODE, XtaasConstants.ISEU, stateCodeList,
								stateCodeMap);
					}else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.ISEU + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.ISEU, XtaasConstants.SIC, isEuList,
								isEuMap);
					}else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.SIC + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.SIC, XtaasConstants.NAICS, sicList,
								sicListMap);
					}else if (row.getCell(0).getStringCellValue().trim().equalsIgnoreCase(XtaasConstants.NAICS + " :")) {
						startPos = readValues(startPos, endPos, XtaasConstants.NAICS, "", naicsList,
								naicsListMap);
					}else {
						startPos = startPos + 1;
					}
				} else {
					startPos = startPos + 1;
				}
			}
			Map<String, Integer> nukeCountMap = new HashMap<String, Integer>();
			int managementCount = nukeCallableProspect(campaign, managementList, XtaasConstants.MANAGEMENT_LEVELS, managementMap);
			nukeCountMap.put(XtaasConstants.MANAGEMENT_LEVELS, new Integer(managementCount));
			int deptCount = nukeCallableProspect(campaign, departmentList, XtaasConstants.DEPARTMENT, departmentMap);
			nukeCountMap.put(XtaasConstants.DEPARTMENT, new Integer(deptCount));
			int empCount = nukeCallableProspect(campaign, employeeCountList, XtaasConstants.EMPLOYEE_COUNT, employeeCountMap);
			nukeCountMap.put(XtaasConstants.EMPLOYEE_COUNT, new Integer(empCount));
			int revenueCount = nukeCallableProspect(campaign, revenueList, XtaasConstants.REVENUE, revenueMap);
			nukeCountMap.put(XtaasConstants.REVENUE, new Integer(revenueCount));
			int industryCount = nukeCallableProspect(campaign, industryList, XtaasConstants.INDUSTRY, industryMap);
			nukeCountMap.put(XtaasConstants.INDUSTRY, new Integer(industryCount));
			int titleCount = nukeCallableProspect(campaign, titleList, XtaasConstants.TITLE, titleMap);
			nukeCountMap.put(XtaasConstants.TITLE, new Integer(titleCount));
			int stateCodeCount = nukeCallableProspect(campaign, stateCodeList, XtaasConstants.STATE_CODE, stateCodeMap);
			nukeCountMap.put(XtaasConstants.STATE_CODE, new Integer(stateCodeCount));
			int countryCount = nukeCallableProspect(campaign, countryList, XtaasConstants.VALID_COUNTRY, countryMap);
			nukeCountMap.put(XtaasConstants.VALID_COUNTRY, new Integer(countryCount));
			int sicCodeCount = nukeCallableProspect(campaign, sicCodeList, XtaasConstants.SIC_CODE, sicCodeMap);
			nukeCountMap.put(XtaasConstants.SIC_CODE, new Integer(sicCodeCount));
			int stateCount = nukeCallableProspect(campaign, stateList, XtaasConstants.VALID_STATE, stateMap);
			nukeCountMap.put(XtaasConstants.VALID_STATE, new Integer(stateCount));
			int companyCount = nukeCallableProspect(campaign, companyList, XtaasConstants.COMPANY, companyMap);
			nukeCountMap.put(XtaasConstants.COMPANY, new Integer(companyCount));
			int sourceCount = nukeCallableProspect(campaign, sourceList, XtaasConstants.SOURCE, sourceMap);
			nukeCountMap.put(XtaasConstants.SOURCE, new Integer(sourceCount));
			int industryListCount = nukeCallableProspect(campaign, industriesList, XtaasConstants.INDUSTRY_LIST, industryListMap);
			nukeCountMap.put(XtaasConstants.INDUSTRY_LIST, new Integer(industryListCount));
			int isEuCount = nukeCallableProspect(campaign, isEuList, XtaasConstants.ISEU, isEuMap);
			nukeCountMap.put(XtaasConstants.ISEU, new Integer(isEuCount));
			int sicCount = nukeCallableProspect(campaign, sicList, XtaasConstants.SIC, sicListMap);
			nukeCountMap.put(XtaasConstants.SIC, new Integer(sicCount));
			int naicsCount = nukeCallableProspect(campaign, naicsList, XtaasConstants.NAICS, naicsListMap);
			nukeCountMap.put(XtaasConstants.NAICS, new Integer(naicsCount));
			
			// clear prospect cache after data nuke
			prospectCacheServiceImpl.clearCache(campaignId);
			Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
			String callableCount = Integer.toString(prospectCallLogRepository.findAllCallableRecordCount(campaign));
			XtaasEmailUtils.sendNukeAndCallableCountEmail(nukeCountMap, login, callableCount,campaign);
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"PIVOT_UPLOAD", "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("COMPLETED");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() +" : Nuke pivots count email sent. TOTAL CALLABLES COUNT : " + callableCount;
				teamNoticeSerive.saveUserNotices(login.getId(), notice);
			}
			Organization organization = organizationRepository.findOneById(login.getOrganization());
			if(organization != null && organization.isLiteMode()) {
				if(Integer.parseInt(callableCount) < campaign.getLowerThreshold()) {
					campaign.setAutoGoldenPass(false);
					campaignRepository.save(campaign);
				}
			}
		} catch (Exception e) {
			setPivotError(campaignId, e, null);
		}
	}


	private int readValues(int startPos, int maxRowCount, String checkpoint, String endCheckpoint, List<String> list,
			Map<String, String> map) {
		int tempStartPos = startPos;
		Row row1 = sheet.getRow(tempStartPos);
		while (row1 != null && (row1.getCell(0) != null || row1.getCell(1) != null || row1.getCell(2) != null)) {
			if (row1.getCell(3) != null
					&& (row1.getCell(3).getStringCellValue().equalsIgnoreCase(XtaasConstants.SOFT_DELETE)
							|| row1.getCell(3).getStringCellValue().equalsIgnoreCase(XtaasConstants.HARD_DELETE))) {
				String delVal = row1.getCell(3).getStringCellValue();
				if (checkpoint.equalsIgnoreCase(XtaasConstants.MANAGEMENT_LEVELS)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.DEPARTMENT)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.EMPLOYEE_COUNT)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.REVENUE)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.INDUSTRY)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.TITLE)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.STATE_CODE)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.VALID_COUNTRY)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.SIC_CODE)) {
					createListForDataNuke(list, row1, map, delVal);
				}else if (checkpoint.equalsIgnoreCase(XtaasConstants.VALID_STATE)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.COMPANY)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.SOURCE)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.INDUSTRY_LIST)) {
					createListForDataNuke(list, row1, map, delVal);
				} else if (checkpoint.equalsIgnoreCase(XtaasConstants.ISEU)) {
					createListForDataNuke(list, row1, map, delVal);
				}else if (checkpoint.equalsIgnoreCase(XtaasConstants.SIC)) {
					createListForDataNuke(list, row1, map, delVal);
				}else if (checkpoint.equalsIgnoreCase(XtaasConstants.NAICS)) {
					createListForDataNuke(list, row1, map, delVal);
				}
			}
			tempStartPos = tempStartPos + 1;
			row1 = sheet.getRow(tempStartPos);
			if (row1 == null) {
				row1 = sheet.getRow(tempStartPos + 1);
				if (row1 != null && (row1.getCell(0).getStringCellValue().contains(XtaasConstants.DEPARTMENT)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.EMPLOYEE_COUNT)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.REVENUE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.INDUSTRY)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.TITLE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.STATE_CODE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.VALID_COUNTRY)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.SIC_CODE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.VALID_STATE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.COMPANY)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.SOURCE)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.INDUSTRY_LIST)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.ISEU)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.SIC)
						|| row1.getCell(0).getStringCellValue().contains(XtaasConstants.NAICS))) {
					break;
				} else {
					break;
				}
			}
		}
		return tempStartPos;
	}

	private int nukeCallableProspect(Campaign campaign, List<String> list, String checkPoint,
			Map<String, String> map) {
		int nukeCount = 0;
		if (list != null && list.size() > 0) {
			List<String> softDelete = new ArrayList<String>();
			List<String> hardDelete = new ArrayList<String>();
			for (String value : list) {
				if(value != null && !value.isEmpty()) {
					String action = map.get(value);
					if (action != null && !action.isEmpty()) {
						if (action.equalsIgnoreCase(XtaasConstants.SOFT_DELETE) || action.equalsIgnoreCase(XtaasConstants.HARD_DELETE)) {
							softDelete.add(value);
						}
					}
				}else {
					softDelete.add(null);
				}
			}	
			if (softDelete != null && softDelete.size() > 0)
				nukeCount = prospectCallLogRepository.nukeCallableProspectByPivots(campaign, softDelete, checkPoint,
						XtaasConstants.SOFT_DELETE);
		}
		return nukeCount;
	}

	private void createListForDataNuke(List<String> list, Row row1, Map<String, String> map, String deleteValue) {
		if (row1.getCell(0) != null) {
			list.add(row1.getCell(0).getStringCellValue());
			map.put(row1.getCell(0).getStringCellValue(), deleteValue);
		} else {
			list.add(null);
		}
	}

	@Async
	@Override
	public void sendPivotStats(String campaignId, Login login) throws IOException {
		downloadOrSendPivot(campaignId, null, null, true, true, false, login);
	}

	private void sendEmail(Campaign campaign, Login login, boolean sendEmail) {
		String fileName = campaign.getName() + " pivots.xlsx";
		File file = new File(fileName);
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		String message = "Please find the attached pivot file.";
		String subject = "" + campaign.getName() + "Pivot";
		boolean pivotReportSent = false;
		String link = "";
		String linkref = "";
		String linkrefNotice = "";
		if (login != null) {
			try {
				link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketNameReports, fileName, file);
				linkref = "<a href=\" " + link + " \">Download here</a>";
				linkrefNotice = "<" + link + ">";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				SupervisorStats supervisorStats = supervisorStatsRepository
						.findBycampaignManagerIdAndcampaignId(login.getId(), campaign.getId(), "PIVOT_DOWNLOAD", "INPROCESS");
				if (supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName() + " : Pivot file download failed, please contact support";
					teamNoticeSerive.saveUserNotices(login.getId(), notice);
				}
			}
			if(sendEmail) {
				List<String> emails = login.getReportEmails();
				if (emails != null && emails.size() > 0) {
					for (String email : emails) {
						if (email != null && !email.isEmpty()) {
							// XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", campaign.getName() + subject, message, fileNames);
							XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", campaign.getName() + subject, campaign.getName() + " - Pivot file Download Link :  " + linkref);
							logger.debug("Pivots sent successfully to : " + email);
							pivotReportSent = true;
						}
					}
				}
			}
			SupervisorStats supervisorStats = supervisorStatsRepository
					.findBycampaignManagerIdAndcampaignId(login.getId(), campaign.getId(), "PIVOT_DOWNLOAD", "INPROCESS");
			if (supervisorStats != null) {
				supervisorStats.setStatus("COMPLETED");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + ": Pivot file Download Link : Download here " + linkrefNotice;
				teamNoticeSerive.saveUserNotices(login.getId(), notice);
			}
		}
		if (!pivotReportSent) {
			// XtaasEmailUtils.sendEmail("reports@xtaascorp.com", "data@xtaascorp.com", fileName + subject, message, fileNames);
			XtaasEmailUtils.sendEmail("reports@xtaascorp.com", "data@xtaascorp.com", fileName + subject, campaign.getName() + " - Pivot file Download Link :  " + linkref);
			logger.debug("Pivots sent successfully to reports@xtaascorp.com");
		}
	}

	@Override
	public SupervisorPivotDTO getSupervisorPivotsForUI(String campaignId, boolean isLimit, String pivotSelectedTab) {
		SupervisorPivotDTO supervisorPivotDTO = new SupervisorPivotDTO();
		logger.debug("getSupervisorPivots() : Getting supervisor pivots for the campaignId: " + campaignId);
		List<Pivot> industryResult = new ArrayList<>();
		List<Pivot> managementResult = new ArrayList<>();
		List<Pivot> departmentResult = new ArrayList<>();
		List<Pivot> employeeCountResult = new ArrayList<>();
		List<Pivot> revenueCountResult = new ArrayList<>();
		List<Pivot> titleCountResult = new ArrayList<>();
		List<Pivot> countryCountResult = new ArrayList<>();
		List<Pivot> companyCountResult = new ArrayList<>();
		List<Pivot> sourceCountResult = new ArrayList<>();
		List<Pivot> stateCodeCountResult = new ArrayList<>();
		List<Pivot> isEuCountResult = new ArrayList<>();
		List<Pivot> sicCountResult = new ArrayList<>();
		List<Pivot> naicsCountResult = new ArrayList<>();
		Campaign campaign = campaignRepository.findOneById(campaignId);
		supervisorPivotDTO.totalRecords = prospectCallLogRepository.findAllCallableRecordCount(campaign);

		if (pivotSelectedTab.equalsIgnoreCase("industry")) {
			industryResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "industry", isLimit);
			if (!industryResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, industryResult, "industry", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("managementLevel")) {
			managementResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "managementLevel",
					isLimit);
			if (!managementResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, managementResult, "managementLevel", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("department")) {
			departmentResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "department", isLimit);
			if (!departmentResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, departmentResult, "department", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("employeeCount")) {
			employeeCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "employeeCount",
					isLimit);
			if (!employeeCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, employeeCountResult, "employeeCount", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("revenue")) {
			revenueCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "revenue", isLimit);
			if (!revenueCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, revenueCountResult, "revenue", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("title")) {
			titleCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "title", isLimit);
			if (!titleCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, titleCountResult, "title", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("country")) {
			countryCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "country", isLimit);
			if (!countryCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, countryCountResult, "country", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("company")) {
			companyCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "company", isLimit);
			if (!companyCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, companyCountResult, "company", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("stateCode")) {
			stateCodeCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "stateCode", isLimit);
			if (!stateCodeCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, stateCodeCountResult, "stateCode", null);
			}
		}

		if (pivotSelectedTab.equalsIgnoreCase("source")) {
			sourceCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "source", isLimit);
			if (!sourceCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, sourceCountResult, "source", null);
			}
		}

		if (pivotSelectedTab.equalsIgnoreCase("industryList")) {
			setToSupervisorPivotDTO(supervisorPivotDTO, sourceCountResult, "industryList",
					getPivotsList(campaign, "toShowOnUI","industryList"));
		}
		
		if (pivotSelectedTab.equalsIgnoreCase("sicList")) {
			setToSupervisorPivotDTO(supervisorPivotDTO, sicCountResult, "sicList",
					getPivotsList(campaign, "toShowOnUI","sicList"));
		}
		
		if (pivotSelectedTab.equalsIgnoreCase("naicsList")) {
			setToSupervisorPivotDTO(supervisorPivotDTO, naicsCountResult, "naicsList",
					getPivotsList(campaign, "toShowOnUI","naicsList"));
		}
		
		if (pivotSelectedTab.equalsIgnoreCase("isEu")) {
			isEuCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "isEu", isLimit);
			if (!isEuCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, isEuCountResult, "isEu", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("isEu")) {
			isEuCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "isEu", isLimit);
			if (!isEuCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, isEuCountResult, "isEu", null);
			}
		}
		if (pivotSelectedTab.equalsIgnoreCase("isEu")) {
			isEuCountResult = prospectCallLogRepository.generatePivotAggregationQuery(campaign, "isEu", isLimit);
			if (!isEuCountResult.isEmpty()) {
				setToSupervisorPivotDTO(supervisorPivotDTO, isEuCountResult, "isEu", null);
			}
		}
		return supervisorPivotDTO;
	}

	private Map<String, Integer> getPivotsList(Campaign campaign, String flag, String pivotType) {
		List<ProspectCallLog> prospects = new ArrayList<ProspectCallLog>();
		/**
		 * DATE :- 06/01/20 Added flag in method to fetch the records with limit 20.
		 * flag == "toShowOnUI" then it will fetch only top 20 prospects to display on
		 * supervisor screen. otherwise it will fetch without limit.
		 */
		if (flag != null && flag.equalsIgnoreCase("toShowOnUI")) {
			prospects = prospectCallLogRepository.findAllCallableRecordsWithLimit(campaign);
		} else {
			prospects = prospectCallLogRepository.findAllCallableRecords(campaign);
		}
		
		
		Map<String, Integer> pivotTypeMap = new HashMap<String, Integer>();
		if (prospects != null && prospects.size() > 0) {
			for (int i = 0; i < prospects.size(); i++) {
				int count = 0;
				ProspectCallLog prospect = prospects.get(i);
				if (prospect != null && prospect.getProspectCall() != null
						&& prospect.getProspectCall().getProspect() != null) {
					List<String> pList = null;
					if(pivotType.equalsIgnoreCase("industryList")) {
						pList = prospect.getProspectCall().getProspect().getIndustryList();
					}
					if(pivotType.equalsIgnoreCase("sicList")) {
						pList = prospect.getProspectCall().getProspect().getCompanySIC();					
					}
					if(pivotType.equalsIgnoreCase("naicsList")) {
						pList = prospect.getProspectCall().getProspect().getCompanyNAICS();
					}
					if (pList != null && pList.size() > 0) {
						String jsonStr = JSONArray.toJSONString(pList);
						String industryList = String.join(",", pList);
						if (pivotTypeMap.containsKey(jsonStr)) {
							Integer in = pivotTypeMap.get(jsonStr);
							pivotTypeMap.put(jsonStr, ++in);
						} else {
							pivotTypeMap.put(jsonStr, ++count);
						}
					}
				}
			}
		}
		
		Map<String, Integer> sortedMap = pivotTypeMap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
						LinkedHashMap::new));
		return sortedMap;
	}

}
