package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.QaService;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.AbmListDetail;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.AbmListDetailRepository;
//import com.xtaas.db.repository.AbmListDetailRepositoryImpl;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.utils.PusherUtils;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.valueobjects.NewClientMappingComparator;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Service
public class DownloadClientMappingReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DownloadClientMappingReportServiceImpl.class);

	String clientRegion = "us-west-2";
	String bucketName = "insideup";
	String bucketNameReports = "xtaasreports";
	String recordingBucketName = "xtaasrecordings";
	String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
	String awsSecretKey = "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";
	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();
	String ext = ".zip";
	String fileNameWithoutExt = null;
	private static final String SUFFIX = "/";
	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;

	@Autowired
	private NewClientMappingRepository newClientMappingRepository;

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private AbmListNewRepository abmListNewRepository;

	@Autowired
	private AbmListDetailRepository abmListDetailRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	@Autowired
	QaService qaService;
	
	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	DataProviderService dataProviderService;
	
	public void downloadLeadReportWithClientMappings(ProspectCallSearchDTO prospectCallSearchDTO, Login login)
			throws ParseException, Exception {
		logger.debug("Client mapping lead report thread started");
		Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign is mandatory for client mappings.");
		}
		if (campaign.getClientName() == null) {
			throw new IllegalArgumentException("Client name is mandatory for client mappings.");
		}
		AmazonS3 s3client = createAWSConnection(clientRegion);
		try {
			downloadClientMappingReport(campaign, prospectCallSearchDTO, s3client, login);
		} catch (Exception e) {
			logger.error("Error occured while writing to excel file.");
			e.printStackTrace();
		}
	}

	public void downloadLeadReportWithNewClientMappings(ProspectCallSearchDTO prospectCallSearchDTO, Login login,
			List<NewClientMapping> clientMappings, String type) throws ParseException, Exception {
		logger.info("Client mapping lead report thread started");
		Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign is mandatory for client mappings.");
		}
		if (campaign.getClientName() == null) {
			campaign.setClientName(campaign.getOrganizationId());
			//throw new IllegalArgumentException("Client name is mandatory for client mappings.");
		}
		AmazonS3 s3client = createAWSConnection(clientRegion);
		try {
			downloadNewClientMappingReport(campaign, prospectCallSearchDTO, s3client, login, clientMappings,type);
		} catch (Exception e) {
			logger.error("Error occured while writing to excel file.");
			e.printStackTrace();
		}
	}

	private void downloadNewClientMappingReport(Campaign campaign, ProspectCallSearchDTO prospectCallSearchDTO,
			AmazonS3 s3client, Login login, List<NewClientMapping> clientMappings,String type) {
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		try {
			ProspectCallSearchDTO tempProspectCallSearchDTO = prospectCallSearchDTO;
			Map<String, Campaign> groupCampaigMap = new HashMap<String, Campaign>();
			List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
			int recordsCounter = 0;
			boolean multiCampaign = false;
			// List<NewClientMapping> clientMappings = newClientMappingRepository
			// .findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
			if (clientMappings == null || clientMappings.size() == 0) {
				throw new IllegalArgumentException("Client mappings not found for selected campaign.");
			}
			if (campaign.getClientName() != null && !campaign.getClientName().isEmpty()) {
				File dirFile = new File(campaign.getName());
				if (!dirFile.exists()) {
					if (dirFile.mkdirs()) {
						logger.info("Directory is created!");
					} else {
						logger.info("Failed to create directory!");
					}
				}
				String fileTime = getCurrentDate();
				boolean dncFlag = false;
				String folderName = campaign.getName();
				String campaignName = campaign.getName() + "_" + fileTime;

				if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
					if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
						campaignName = campaign.getName() + "_GROUP_" + fileTime;
						List<Campaign> campaigsList = campaignService.getCampaignByIds(campaign.getCampaignGroupIds());
						for (Campaign camp : campaigsList) {
							groupCampaigMap.put(camp.getId(), camp);
						}
						prospectCallSearchDTO.setCampaignIds(campaign.getCampaignGroupIds());
					} else {
						groupCampaigMap.put(campaign.getId(), campaign);
					}
				} else if (prospectCallSearchDTO.getCampaignIds() != null
						&& prospectCallSearchDTO.getCampaignIds().size() > 1) {
					List<Campaign> campaignsFromDB = campaignService.getCampaignByIds(prospectCallSearchDTO.getCampaignIds());
					Map<String, Campaign> groupInternalCampaigMap = new HashMap<String, Campaign>();
					campaignName = campaign.getOrganizationId() + "_" + fileTime;

					if (campaignsFromDB != null && campaignsFromDB.size() > 0) {
						for (Campaign camp : campaignsFromDB) {
							groupInternalCampaigMap.put(camp.getId(), camp);
						}
						if (groupInternalCampaigMap != null && groupInternalCampaigMap.size() > 0) {
							List<String> inCids = new ArrayList<String>();
							for (Map.Entry<String, Campaign> entry : groupInternalCampaigMap.entrySet()) {
								if (entry.getValue().getCampaignGroupIds() != null && entry.getValue().getCampaignGroupIds().size() > 0)
									inCids.addAll(entry.getValue().getCampaignGroupIds());
								else
									inCids.add(entry.getKey());
							}
							campaignsFromDB = campaignService.getCampaignByIds(inCids);
							for (Campaign camp : campaignsFromDB) {
								groupCampaigMap.put(camp.getId(), camp);
							}
							List<String> finalCids = new ArrayList<String>();
							for (Map.Entry<String, Campaign> entry : groupInternalCampaigMap.entrySet()) {
								finalCids.add(entry.getKey());
							}
							prospectCallSearchDTO.setCampaignIds(finalCids);
							multiCampaign = true;
						}
					}
				}
				prospectCallLogList = getMultiCampaignLeadsFromDB(prospectCallSearchDTO);
				Map<String, String> customFieldsAttribMap = new HashMap<String, String>();
				Map<String, String> customFieldsSkinMap = new HashMap<String, String>();

				if (campaign.isEnableCustomFields() && campaign.getQualificationCriteria().getExpressions() != null
						&& !campaign.getQualificationCriteria().getExpressions().isEmpty()) {
					for (Expression expression : campaign.getQualificationCriteria().getExpressions()) {
						if(expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
								|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10")) {								
							customFieldsAttribMap.put(expression.getAttribute(), expression.getQuestionSkin());
							customFieldsSkinMap.put(expression.getQuestionSkin(),expression.getAttribute());
						}
					}
				}
				//iterating to get qa answerd values
				Map<String,String> stdQaAnswerHeaderMap = new HashMap<String, String>();
				Map<String, Map<String, String>> customQuestionsMap = new HashMap<String, Map<String,String>>();
				for(ProspectCallLog  qaPlog : prospectCallLogList) {
					if(qaPlog !=null && qaPlog.getQaFeedback()!=null && qaPlog.getQaFeedback().getQaAnswers()!=null) {
						List<AgentQaAnswer> qaAgentAnswers = qaPlog.getQaFeedback().getQaAnswers();
						for (AgentQaAnswer aqa : qaAgentAnswers) {
							if(aqa.getQuestion() != null && !aqa.getQuestion().isEmpty() && !customFieldsSkinMap.containsKey(aqa.getQuestion())) {
								stdQaAnswerHeaderMap.put("QA-" + aqa.getQuestion(), "QA-" + aqa.getQuestion());
							} else if(aqa.getAttribute() != null && !aqa.getAttribute().isEmpty() && !customFieldsAttribMap.containsKey(aqa.getAttribute())) {
								stdQaAnswerHeaderMap.put("QA-" + aqa.getAttribute(), "QA-" + aqa.getAttribute());
							}
						}
					}
					
					//Iterating to fetch customquestions attributes if requried.
					if(qaPlog !=null && qaPlog.getProspectCall() !=null && qaPlog.getProspectCall().getAnswers()!=null) {
						List<AgentQaAnswer> agentAnswers = qaPlog.getProspectCall().getAnswers();
						for(AgentQaAnswer aqa : agentAnswers) {
							if(aqa.getQuestion()!=null && !aqa.getQuestion().isEmpty() && !customFieldsSkinMap.containsKey(aqa.getQuestion())) {
								Map<String, String> cqmap = customQuestionsMap.get(qaPlog.getProspectCall().getCampaignId());
								if(cqmap==null) {
									cqmap = new HashMap<String,String>();
									customQuestionsMap.put(qaPlog.getProspectCall().getCampaignId(), cqmap);
								}
								if(!cqmap.containsKey(aqa.getQuestion())) {
									String customQuestionHeader = "CUSTOM_QUESTION_"+ (cqmap.size()+1);
									cqmap.put(aqa.getQuestion(), customQuestionHeader);
									customQuestionsMap.put(qaPlog.getProspectCall().getCampaignId(), cqmap);
								}
							}
						}
					}
				}

				if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1
						&& tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
					campaignName = campaignName + "_DNC";
					dncFlag = true;
				} else if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1
						&& tempProspectCallSearchDTO.getSubStatus().contains("DNCL")) {
					campaignName = campaignName + "_Failure";
				} else {
					campaignName = campaignName + "_Lead";
				}
				String fileName = folderName + SUFFIX + campaignName + ".xlsx";
				File file = new File(fileName);
				Workbook workbook = new XSSFWorkbook();
				CreationHelper createHelper = workbook.getCreationHelper();
				Sheet sheet = workbook.createSheet("Leads");
				Map<String, String> colNames = new HashMap<String, String>();
				Map<String, String> singleClientValueMap = new HashMap<String, String>();
				Collections.sort(clientMappings, new NewClientMappingComparator());
				int i = 0;
				String[] columns = new String[200];
				colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
				columns[i] = "ProspectCallId (for internal use only)";
				i++;
				colNames.put("Client Name", "Client Name");
				columns[i] = "Client Name";
				i++;
				for (NewClientMapping mapping : clientMappings) {
					String colHeaderString = mapping.getColHeader();
					if(colHeaderString.equalsIgnoreCase("CUSTOMFIELD_01")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_02")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_03")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_04")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_05")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_06")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_07")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_08")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_09")
							|| colHeaderString.equalsIgnoreCase("CUSTOMFIELD_10")) {
						colHeaderString = customFieldsAttribMap.get(colHeaderString);
						if(colHeaderString==null || colHeaderString.isEmpty()) {
							continue;
						}
					}
					
					if (mapping.getClientValues() != null && mapping.getClientValues().size() == 1) {
						if (!singleClientValueMap.containsKey(colHeaderString)) {
							singleClientValueMap.put(colHeaderString, mapping.getClientValues().get(0));
						}
					}
					if (colNames.containsKey(colHeaderString)) {
						continue;
					} else {
						colNames.put(colHeaderString, mapping.getStdAttribute());
						columns[i] = colHeaderString;
						i++;
					}
				}
	        	 List<String> questionKeyList = new ArrayList<String>();
	        	 Map<String,String> customFieldMap =  new HashMap<String, String>();
				if(multiCampaign) {
	 		        for (Map.Entry<String,Campaign> campEntry : groupCampaigMap.entrySet())  {
		                 HashMap<String,ArrayList<PickListItem>> questionsList = campEntry.getValue().getQualificationCriteria().getQuestions();
		                 for (Map.Entry<String,ArrayList<PickListItem>> entry : questionsList.entrySet())  {
		                	 if(!questionKeyList.contains(entry.getKey())) {
		                 		questionKeyList.add(entry.getKey());
		                 		//colNames.put(entry.getKey(),entry.getKey());
		                 		//columns[i]= entry.getKey();
		                 		//i++;
		                	 }
		                 }
		                 //i++;
	 		        }					
				}

				Map<String, List<String>> clientMap = new HashMap<String, List<String>>();
				for (NewClientMapping mapping : clientMappings) {
					clientMap.put(mapping.getStdAttribute(), mapping.getClientValues());
				}
//				colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
//				columns[i] = "ProspectCallId (for internal use only)";
//				if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
//					i++;
//					colNames.put("campaignName (for internal use only)", "campaignName (for internal use only)");
//					columns[i] = "campaignName (for internal use only)";
//				}
//				i++;
//				colNames.put("DownloadDate", "DownloadDate");
//				columns[i] = "DownloadDate";
//				i++;
//				if (dncFlag) {
//					colNames.put("Call Start Time", "Call Start Time");
//					columns[i] = "Call Start Time";
//					i++;
//				}
//				colNames.put("AssetName", "AssetName");
//				columns[i] = "AssetName";
//				i++;
//				colNames.put("DeliveryDate", "DeliveryDate");
//				columns[i] = "DeliveryDate";
//				i++;
//				colNames.put("PartnerId", "PartnerId");
//				columns[i] = "PartnerId";
//				i++;
//				colNames.put("AgentId", "AgentId");
//				columns[i] = "AgentId";
//				i++;
//				colNames.put("RecordingUrl", "RecordingUrl");
//				columns[i] = "RecordingUrl";
//				i++;
//				colNames.put("LeadStatus", "LeadStatus");
//				columns[i] = "LeadStatus";
//				i++;
//				colNames.put("C1.Reason", "C1.Reason");
//				columns[i] = "C1.Reason";
//				i++;
//				colNames.put("C1.CallNotes", "C1.CallNotes");
//				columns[i] = "C1.CallNotes";
//				i++;
//				colNames.put("Client Delivered", "Client Delivered");
//				columns[i] = "Client Delivered";
//				i++;
//				colNames.put("CompanyZoomURL", "CompanyZoomURL");
//				columns[i] = "CompanyZoomURL";
//				i++;
//				colNames.put("PersonZoomURL", "PersonZoomURL");
//				columns[i] = "PersonZoomURL";
//				i++;
//				colNames.put("Agent Notes", "Agent Notes");
//				columns[i] = "Agent Notes";
				// QualificationCriteria qc = campaign.getQualificationCriteria();
				// HashMap<String, String> attribQuestionsMap = qc.getStdAttribAndSkins();
				// HashMap<String, ArrayList<PickListItem>> questionsList =
				// campaign.getQualificationCriteria()
				// .getStdQuestions();
				// List<String> qaQuestionKeyList = new ArrayList<String>();
				// HashMap<String, ArrayList<PickListItem>> qaQuestionsList =
				// campaign.getQualificationCriteria()
				// .getQuestions();
				// for (Map.Entry<String, ArrayList<PickListItem>> entry :
				// qaQuestionsList.entrySet()) {
				// String qaQuest = "QA-" + entry.getKey();
				// qaQuestionKeyList.add(qaQuest);
				// i++;
				// colNames.put(qaQuest, qaQuest);
				// columns[i] = qaQuest;
				// }
				//i++;
		        List<String> qaQuestionKeyList = new ArrayList<String>();

				// if(multiCampaign) {
				// for (Map.Entry<String,Campaign> campEntry : groupCampaigMap.entrySet()) {
				for (Map.Entry<String, String> campaignEntry : stdQaAnswerHeaderMap.entrySet()) {

			                	 String qaQuest = campaignEntry.getKey();
			                	 qaQuestionKeyList.add(qaQuest);
			                 System.out.println(qaQuest);
			                 System.out.print("----"+columns[i]);

				                colNames.put(qaQuest,qaQuest);
				                columns[i]= qaQuest;
			                 
			                 i++;
		                 }
	 		      //  }					
				//}
		            
				colNames.put("Action", "Action");
				columns[i] = "Action";
				Font headerFont = workbook.createFont();
				headerFont.setBold(true);
				headerFont.setFontHeightInPoints((short) 14);
				headerFont.setColor(IndexedColors.RED.getIndex());
				CellStyle headerCellStyle = workbook.createCellStyle();
				headerCellStyle.setFont(headerFont);
				Row headerRow = sheet.createRow(0);
				for (int x = 0; x < columns.length; x++) {
					if (columns[x] != null && !columns[x].isEmpty()) {
						Cell cell = headerRow.createCell(x);
						cell.setCellValue(columns[x]);
						cell.setCellStyle(headerCellStyle);
					}
				}
				CellStyle dateCellStyle = workbook.createCellStyle();
				dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
				if (clientMappings != null && clientMappings.size() > 0)
					for (NewClientMapping cm : clientMappings) {
						if (cm.getStdAttribute() != null) {
							if (cm.getStdAttribute().equalsIgnoreCase("RECORDING_URL_DOWNLOAD")) {
								try {
									List<ProspectCallLog> recordingCallLogList = prospectCallLogList;
									for (ProspectCallLog pcl : recordingCallLogList) {
										if (pcl.getProspectCall().getRecordingUrlAws() == null
												|| pcl.getProspectCall().getRecordingUrlAws().isEmpty()) {
											if (pcl.getProspectCall().getRecordingUrl() == null
													|| pcl.getProspectCall().getRecordingUrl().isEmpty()) {
												System.out.println(pcl.getProspectCall().getProspectCallId());
												pcl.getProspectCall().setRecordingUrlAws("");
											} else {
												String rUrl = pcl.getProspectCall().getRecordingUrl();
												int lastIndex = rUrl.lastIndexOf("/");
												String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
												String rdfile = xfileName + ".wav";
												pcl.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
											}
										}
										if (pcl.getProspectCall().getRecordingUrlAws() != null
												&& !pcl.getProspectCall().getRecordingUrlAws().isEmpty()) {
											String cName = "";
											if (campaign.getClientCampaignName() != null && !campaign.getClientCampaignName().isEmpty()) {
												cName = campaign.getClientCampaignName();
											} else {
												cName = campaign.getName();
											}
											String cleanCampaignName = cName.replaceAll("[^a-zA-Z0-9\\s]", "");
											String rfileName = pcl.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]",
													"") + "_"
													+ pcl.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "") + "_"
													+ cleanCampaignName + ".wav";
											File rfile = new File(rfileName);
											File rfileTemp = new File(folderName + SUFFIX + rfileName);
											String afileName = folderName + SUFFIX + rfile;
											boolean fileDownloaded = s3client.doesObjectExist(bucketName, afileName);
											if (!fileDownloaded) {
												String awsurl = pcl.getProspectCall().getRecordingUrlAws();
												String rFolder = "";
												String recordingFileName = "";
												if (awsurl != null) {
													String[] urlarr = awsurl.split(SUFFIX);
													rFolder = urlarr[4];
													recordingFileName = urlarr[5];
												}
												if (!"".equals(rFolder) && !"".equals(recordingFileName)) {

													boolean isFileExistsInS3 = s3client.doesObjectExist(recordingBucketName,
															"recordings" + SUFFIX + recordingFileName);
													if (isFileExistsInS3) {
														s3client.getObject(
																new GetObjectRequest(recordingBucketName, rFolder + SUFFIX + recordingFileName),
																rfileTemp);
														s3client.putObject(new PutObjectRequest(bucketName, afileName, rfileTemp)
																.withCannedAcl(CannedAccessControlList.PublicRead));
														rfileTemp.delete();
													}
												}
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				int rowNum = 1;

				for (ProspectCallLog prospectCallLog : prospectCallLogList) {
					Row row = sheet.createRow(rowNum++);
					ProspectCall prospectCall = prospectCallLog.getProspectCall();

					for (int col = 0; col < columns.length; col++) {
						if (columns[col] != null && !columns[col].isEmpty()) {
							if (columns[col].contentEquals("Status")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getProspectCallId() != null) {
									cellx.setCellValue(prospectCall.getProspectCallId());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("campaignName (for internal use only)")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getCampaignId() != null) {
									Campaign gCampaign = groupCampaigMap.get(prospectCall.getCampaignId());
									String gCampaignName = "";
									if (gCampaign != null) {
										gCampaignName = gCampaign.getName();
									}
									cellx.setCellValue(gCampaignName);
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("DownloadDate")) {
								Cell cellx = row.createCell(col);

								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
								pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
								String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
								Date pstDate = pstFormat.parse(pstStr);

								cellx.setCellValue(pstFormat.format(pstDate));
								cellx.setCellStyle(dateCellStyle);
							} else if (dncFlag && columns[col].contentEquals("Call Start Time")) {
								Cell cellx = row.createCell(col);
								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
								pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
								if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
									String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
									Date pstDate = pstFormat.parse(pstStr);
									cellx.setCellValue(pstFormat.format(pstDate));
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("AssetName")) {
								Cell cellx = row.createCell(col);
								String assetName = "";
								if (prospectCall.getDeliveredAssetId() != null && !prospectCall.getDeliveredAssetId().isEmpty()) {

									assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
								}
								cellx.setCellValue(assetName);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("DeliveryDate")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("PartnerId")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("AgentId")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("RecordingUrl")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getRecordingUrlAws() == null
										|| prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()) {
									if (prospectCallLog.getProspectCall().getRecordingUrl() == null
											|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()) {
										cellx.setCellValue("");
									} else {
										String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
										int lastIndex = rUrl.lastIndexOf("/");
										String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
										String rdfile = xfileName + ".wav";
										prospectCallLog.getProspectCall()
												.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
										cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
									}
								} else {
									cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
								}

								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("C1.Reason")) {
								String qaReason = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getRejectionReason() != null) {
														qaReason = qaReason + fra.getRejectionReason();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(qaReason);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("C1.CallNotes")) {
								String callnotes = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getAttributeComment() != null) {
														callnotes = fra.getAttributeComment();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(callnotes);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Client Delivered")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getClientDelivered() != null
										&& !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
									cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
								else
									cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("CompanyZoomURL")) {
								Cell cellx = row.createCell(col);
								String companyUrl = null;
								if (prospectCall != null && prospectCall.getProspect() != null) {
									companyUrl = prospectCall.getProspect().getZoomCompanyUrl();
								}
								cellx.setCellValue(companyUrl);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("PersonZoomURL")) {
								Cell cellx = row.createCell(col);
								String personUrl = null;
								if (prospectCall != null && prospectCall.getProspect() != null) {
									personUrl = prospectCall.getProspect().getZoomPersonUrl();
								}
								cellx.setCellValue(personUrl);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("LeadStatus")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Callback Date")) {
								Cell cellx = row.createCell(col);
								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
								if (prospectCallLog.getCallbackDate() != null) {
									String pstStr = pstFormat.format(prospectCallLog.getCallbackDate());
									Date pstDate = pstFormat.parse(pstStr);
									cellx.setCellValue(pstFormat.format(pstDate));
									cellx.setCellStyle(dateCellStyle);
								}	
							} else if (columns[col].contentEquals("Agent Notes")) {
								Cell cellx = row.createCell(col);
								StringBuffer sb = new StringBuffer();
								if (prospectCallLog.getProspectCall().getNotes() != null
										&& prospectCallLog.getProspectCall().getNotes().size() > 0) {
									List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
									for (Note note : notesArr) {
										sb.append(note.getText());
									}
								}
								cellx.setCellValue(sb.toString());
								cellx.setCellStyle(dateCellStyle);
							} else {
								String val = getColumnValue(campaign, prospectCallLog, columns[col], colNames,
										clientMap, singleClientValueMap,customQuestionsMap);
								if((val == null || val.isEmpty()) && (type.equalsIgnoreCase("ORG") || type.equalsIgnoreCase("DEFAULT") 
										|| type.equalsIgnoreCase("SINGLE"))) {
									for(String question :questionKeyList){
		                				String qaQuest = "QA-"+question;
			                			if(columns[col].contentEquals(question) && prospectCall.getAnswers()!=null){    		
			                		    	for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
			                		   			if (agentQaAnswer.getQuestion()!=null 
			                		   					&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())){
			                		   				val = agentQaAnswer.getAnswer();
								                    break;
			                		   			}
			                		       	 }
			                			}else if(columns[col].contentEquals(qaQuest) && prospectCallLog.getQaFeedback()!=null 
			                					&& prospectCallLog.getQaFeedback().getQaAnswers()!=null){
			                				for (AgentQaAnswer agentQaAnswer : prospectCallLog.getQaFeedback().getQaAnswers()) {
			                		   			if (agentQaAnswer.getQuestion()!=null 
			                		   					&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())){
			                		   				val = agentQaAnswer.getAnswerCertification();
								                    break;
			                		   			}
			                		       	 }
			                			}
			                		}
								}
								row.createCell(col).setCellValue(val);
							}

						}
					}
					recordsCounter++;
				}
				FileOutputStream fileOut = new FileOutputStream(fileName);
				workbook.write(fileOut);
				fileOut.close();
				workbook.close();
				logger.info("Successfully written [{}] records for campaign having name : [{}]", recordsCounter,
						campaign.getName());

				String stype = "single";
				if (multiCampaign) {
					stype = "multi";
				}
				sendEmailWithLeadFile(campaign.getName(), campaignName, tempProspectCallSearchDTO, file, s3client, login,
						organization, stype);
				deleteDir(dirFile);
			}

		} catch (Exception e) {
			logger.info("downloadClientMappingReport() : An Error occured while download lead report file.");
			sendErrorListEmail(organization, e,login);
			e.printStackTrace();
		}
	}
	
	private void sendErrorListEmail(Organization organization, Exception exception,Login loginUser) {
		List<String> emailList = new ArrayList<>();
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		
		emailList.add("data@xtaascorp.com");
		
//		if (loginUser.getReportEmails() != null && loginUser.getReportEmails() .size() > 0) {
//			emailList.addAll(loginUser.getReportEmails() );
//		} else {
//			emailList.add("leadreports@xtaascorp.com");
//		}
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Follwing error occured.");
		stringBuilder.append("<br />");
		stringBuilder.append("<br />");
		stringBuilder.append(exceptionAsString);
		stringBuilder.append("<br />");
		stringBuilder.append("<br />");
		stringBuilder.append("Please contact your administrator.");
		for (String email : emailList) {
			XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", "Exception occured during lead report download",
					stringBuilder.toString());
			logger.info("Download lead report error list email sent successfully to : " + email + ", data@xtaascorp.com");
		}
		PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Lead report download failed, please contact support.";
		teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
	}

	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
			s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			return s3client;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(
					"Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :"
							+ e);
			return s3client;
		}
	}

	private void downloadClientMappingReport(Campaign campaign, ProspectCallSearchDTO prospectCallSearchDTO,
			AmazonS3 s3client, Login login) {
		try {
			ProspectCallSearchDTO tempProspectCallSearchDTO = prospectCallSearchDTO;
			Map<String, Campaign> groupCampaigMap = new HashMap<String, Campaign>();
			int recordsCounter = 0;
			List<NewClientMapping> clientMappings = newClientMappingRepository.findByCampaignIdAndClientName(campaign.getId(),
					campaign.getClientName());
			Organization organization = organizationRepository.findOneById(campaign.getId());
			if (clientMappings == null || clientMappings.size() == 0) {
				throw new IllegalArgumentException("Client mappings not found for selected campaign.");
			}
			if (campaign.getClientName() != null && !campaign.getClientName().isEmpty()) {
				File dirFile = new File(campaign.getName());
				if (!dirFile.exists()) {
					if (dirFile.mkdirs()) {
						logger.debug("Directory is created!");
					} else {
						logger.debug("Failed to create directory!");
					}
				}
				List<ProspectCallLog> prospectCallLogList = getLeadsFromDB(prospectCallSearchDTO, campaign);
				boolean dncFlag = false;
				String folderName = campaign.getName();
				String campaignName = campaign.getName();
				if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
					campaignName = campaign.getName() + "_GROUP";
					List<Campaign> campaigsList = campaignService.getCampaignByIds(campaign.getCampaignGroupIds());
					for (Campaign camp : campaigsList) {
						groupCampaigMap.put(camp.getId(), camp);
					}
				}
				if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1
						&& tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
					campaignName = campaignName + "_DNC";
					dncFlag = true;
				} else if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1
						&& tempProspectCallSearchDTO.getSubStatus().contains("DNCL")) {
					campaignName = campaignName + "_Failure";
				} else {
					campaignName = campaignName + "_Lead";
				}
				String fileName = folderName + SUFFIX + campaignName + ".xlsx";
				File file = new File(fileName);
				Workbook workbook = new XSSFWorkbook();
				CreationHelper createHelper = workbook.getCreationHelper();
				Sheet sheet = workbook.createSheet("Leads");
				Map<String, String> colNames = new HashMap<String, String>();
				Map<String, String> singleClientValueMap = new HashMap<String, String>();
				Collections.sort(clientMappings, new NewClientMappingComparator());
				int i = 0;
				String[] columns = new String[200];
				for (NewClientMapping mapping : clientMappings) {
					if (mapping.getClientValues() != null && mapping.getClientValues().size() == 1) {
						if (!singleClientValueMap.containsKey(mapping.getColHeader())) {
							singleClientValueMap.put(mapping.getColHeader(), mapping.getClientValues().get(0));
						}
					}
					if (colNames.containsKey(mapping.getColHeader())) {
						continue;
					} else {
						colNames.put(mapping.getColHeader(), mapping.getStdAttribute());
						columns[i] = mapping.getColHeader();
						i++;
					}
				}

				Map<String, List<String>> clientMap = new HashMap<String, List<String>>();
				for (NewClientMapping mapping : clientMappings) {
					clientMap.put(mapping.getStdAttribute(), mapping.getClientValues());
				}
				colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
				columns[i] = "ProspectCallId (for internal use only)";
				if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
					i++;
					colNames.put("campaignName (for internal use only)", "campaignName (for internal use only)");
					columns[i] = "campaignName (for internal use only)";
				}
				i++;
				colNames.put("DownloadDate", "DownloadDate");
				columns[i] = "DownloadDate";
				i++;
				if (dncFlag) {
					colNames.put("Call Start Time", "Call Start Time");
					columns[i] = "Call Start Time";
					i++;
				}
				colNames.put("AssetName", "AssetName");
				columns[i] = "AssetName";
				i++;
				colNames.put("DeliveryDate", "DeliveryDate");
				columns[i] = "DeliveryDate";
				i++;
				colNames.put("PartnerId", "PartnerId");
				columns[i] = "PartnerId";
				i++;
				colNames.put("AgentId", "AgentId");
				columns[i] = "AgentId";
				i++;
				colNames.put("RecordingUrl", "RecordingUrl");
				columns[i] = "RecordingUrl";
				i++;
				colNames.put("LeadStatus", "LeadStatus");
				columns[i] = "LeadStatus";
				i++;
				colNames.put("C1.Reason", "C1.Reason");
				columns[i] = "C1.Reason";
				i++;
				colNames.put("C1.CallNotes", "C1.CallNotes");
				columns[i] = "C1.CallNotes";
				i++;
				colNames.put("Client Delivered", "Client Delivered");
				columns[i] = "Client Delivered";
				i++;
				colNames.put("CompanyZoomURL", "CompanyZoomURL");
				columns[i] = "CompanyZoomURL";
				i++;
				colNames.put("PersonZoomURL", "PersonZoomURL");
				columns[i] = "PersonZoomURL";
				i++;
				colNames.put("Agent Notes", "Agent Notes");
				columns[i] = "Agent Notes";
				// QualificationCriteria qc = campaign.getQualificationCriteria();
				// HashMap<String, String> attribQuestionsMap = qc.getStdAttribAndSkins();
				// HashMap<String, ArrayList<PickListItem>> questionsList =
				// campaign.getQualificationCriteria()
				// .getStdQuestions();
				// List<String> qaQuestionKeyList = new ArrayList<String>();
				// HashMap<String, ArrayList<PickListItem>> qaQuestionsList =
				// campaign.getQualificationCriteria()
				// .getQuestions();
				// for (Map.Entry<String, ArrayList<PickListItem>> entry :
				// qaQuestionsList.entrySet()) {
				// String qaQuest = "QA-" + entry.getKey();
				// qaQuestionKeyList.add(qaQuest);
				// i++;
				// colNames.put(qaQuest, qaQuest);
				// columns[i] = qaQuest;
				// }
				i++;
				for (NewClientMapping mapping : clientMappings) {
					String qacertifyUrl = "QA-" + mapping.getColHeader();
					if (mapping.getClientValues().size() > 1) {
						colNames.put(qacertifyUrl, "QA-" + mapping.getStdAttribute());
						columns[i] = qacertifyUrl;
						i++;
					}
				}
				colNames.put("Action", "Action");
				columns[i] = "Action";
				Font headerFont = workbook.createFont();
				headerFont.setBold(true);
				headerFont.setFontHeightInPoints((short) 14);
				headerFont.setColor(IndexedColors.RED.getIndex());
				CellStyle headerCellStyle = workbook.createCellStyle();
				headerCellStyle.setFont(headerFont);
				Row headerRow = sheet.createRow(0);
				for (int x = 0; x < columns.length; x++) {
					if (columns[x] != null && !columns[x].isEmpty()) {
						Cell cell = headerRow.createCell(x);
						cell.setCellValue(columns[x]);
						cell.setCellStyle(headerCellStyle);
					}
				}
				CellStyle dateCellStyle = workbook.createCellStyle();
				dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
				if (clientMappings != null && clientMappings.size() > 0)
					for (NewClientMapping cm : clientMappings) {
						if (cm.getStdAttribute() != null) {
							if (cm.getStdAttribute().equalsIgnoreCase("RECORDING_URL_DOWNLOAD")) {
								try {
									List<ProspectCallLog> recordingCallLogList = prospectCallLogList;
									for (ProspectCallLog pcl : recordingCallLogList) {
										if (pcl.getProspectCall().getRecordingUrlAws() == null
												|| pcl.getProspectCall().getRecordingUrlAws().isEmpty()) {
											if (pcl.getProspectCall().getRecordingUrl() == null
													|| pcl.getProspectCall().getRecordingUrl().isEmpty()) {
												// System.out.println(pcl.getProspectCall().getProspectCallId());
												pcl.getProspectCall().setRecordingUrlAws("");
											} else {
												String rUrl = pcl.getProspectCall().getRecordingUrl();
												int lastIndex = rUrl.lastIndexOf("/");
												String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
												String rdfile = xfileName + ".wav";
												pcl.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
											}
										}
										if (pcl.getProspectCall().getRecordingUrlAws() != null
												&& !pcl.getProspectCall().getRecordingUrlAws().isEmpty()) {
											String cName = "";
											if (campaign.getClientCampaignName() != null && !campaign.getClientCampaignName().isEmpty()) {
												cName = campaign.getClientCampaignName();
											} else {
												cName = campaign.getName();
											}
											String cleanCampaignName = cName.replaceAll("[^a-zA-Z0-9\\s]", "");
											String rfileName = pcl.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]",
													"") + "_"
													+ pcl.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "") + "_"
													+ cleanCampaignName + ".wav";
											File rfile = new File(rfileName);
											File rfileTemp = new File(folderName + SUFFIX + rfileName);
											String afileName = folderName + SUFFIX + rfile;
											boolean fileDownloaded = s3client.doesObjectExist(bucketName, afileName);
											if (!fileDownloaded) {
												String awsurl = pcl.getProspectCall().getRecordingUrlAws();
												String rFolder = "";
												String recordingFileName = "";
												if (awsurl != null) {
													String[] urlarr = awsurl.split(SUFFIX);
													rFolder = urlarr[4];
													recordingFileName = urlarr[5];
												}
												if (!"".equals(rFolder) && !"".equals(recordingFileName)) {

													boolean isFileExistsInS3 = s3client.doesObjectExist(recordingBucketName,
															"recordings" + SUFFIX + recordingFileName);
													if (isFileExistsInS3) {
														s3client.getObject(
																new GetObjectRequest(recordingBucketName, rFolder + SUFFIX + recordingFileName),
																rfileTemp);
														s3client.putObject(new PutObjectRequest(bucketName, afileName, rfileTemp)
																.withCannedAcl(CannedAccessControlList.PublicRead));
														rfileTemp.delete();
													}
												}
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				int rowNum = 1;

				for (ProspectCallLog prospectCallLog : prospectCallLogList) {
					Row row = sheet.createRow(rowNum++);
					ProspectCall prospectCall = prospectCallLog.getProspectCall();

					for (int col = 0; col < columns.length; col++) {
						if (columns[col] != null && !columns[col].isEmpty()) {
							if (columns[col].contentEquals("Status")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getProspectCallId() != null) {
									cellx.setCellValue(prospectCall.getProspectCallId());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("campaignName (for internal use only)")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getCampaignId() != null) {
									Campaign gCampaign = groupCampaigMap.get(prospectCall.getCampaignId());
									String gCampaignName = "";
									if (gCampaign != null) {
										gCampaignName = gCampaign.getName();
									}
									cellx.setCellValue(gCampaignName);
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("DownloadDate")) {
								Cell cellx = row.createCell(col);

								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
								pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
								String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
								Date pstDate = pstFormat.parse(pstStr);

								cellx.setCellValue(pstFormat.format(pstDate));
								cellx.setCellStyle(dateCellStyle);
							} else if (dncFlag && columns[col].contentEquals("Call Start Time")) {
								Cell cellx = row.createCell(col);
								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
								pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
								if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
									String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
									Date pstDate = pstFormat.parse(pstStr);
									cellx.setCellValue(pstFormat.format(pstDate));
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("AssetName")) {
								Cell cellx = row.createCell(col);
								String assetName = "";
								if (prospectCall.getDeliveredAssetId() != null && !prospectCall.getDeliveredAssetId().isEmpty()) {

									assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
								}
								cellx.setCellValue(assetName);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("DeliveryDate")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("PartnerId")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("AgentId")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("RecordingUrl")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getRecordingUrlAws() == null
										|| prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()) {
									if (prospectCallLog.getProspectCall().getRecordingUrl() == null
											|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()) {
										cellx.setCellValue("");
									} else {
										String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
										int lastIndex = rUrl.lastIndexOf("/");
										String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
										String rdfile = xfileName + ".wav";
										prospectCallLog.getProspectCall()
												.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
										cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
									}
								} else {
									cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
								}

								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("C1.Reason")) {
								String qaReason = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getRejectionReason() != null) {
														qaReason = qaReason + fra.getRejectionReason();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(qaReason);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("C1.CallNotes")) {
								String callnotes = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getAttributeComment() != null) {
														callnotes = fra.getAttributeComment();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(callnotes);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Client Delivered")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getClientDelivered() != null
										&& !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
									cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
								else
									cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("CompanyZoomURL")) {
								Cell cellx = row.createCell(col);
								String companyUrl = null;
								if (prospectCall != null && prospectCall.getProspect() != null) {
									companyUrl = prospectCall.getProspect().getZoomCompanyUrl();
								}
								cellx.setCellValue(companyUrl);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("PersonZoomURL")) {
								Cell cellx = row.createCell(col);
								String personUrl = null;
								if (prospectCall != null && prospectCall.getProspect() != null) {
									personUrl = prospectCall.getProspect().getZoomPersonUrl();
								}
								cellx.setCellValue(personUrl);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("LeadStatus")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Agent Notes")) {
								Cell cellx = row.createCell(col);
								StringBuffer sb = new StringBuffer();
								if (prospectCallLog.getProspectCall().getNotes() != null
										&& prospectCallLog.getProspectCall().getNotes().size() > 0) {
									List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
									for (Note note : notesArr) {
										sb.append(note.getText());
									}
								}
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else {
								String val = getColumnValue(campaign, prospectCallLog, columns[col], colNames,
										clientMap, singleClientValueMap,null);
								row.createCell(col).setCellValue(val);
							}
						}
					}
					recordsCounter++;
				}
				FileOutputStream fileOut = new FileOutputStream(fileName);
				workbook.write(fileOut);
				fileOut.close();
				workbook.close();
				logger.debug("Successfully written [{}] records for campaign having name : [{}]", recordsCounter,
						campaign.getName());
				sendEmailWithLeadFile(campaign, tempProspectCallSearchDTO, file, s3client, login, organization);
				deleteDir(dirFile);
			}

		} catch (Exception e) {
			logger.debug("downloadClientMappingReport() : An Error occured while download lead report file.");
			sendErrorListEmail(login, e);
			e.printStackTrace();
		}
	}

	private void sendErrorListEmail(Login login, Exception exception) {
		List<String> emailList = new ArrayList<>();
		if (login.getReportEmails() != null && login.getReportEmails().size() > 0) {
			emailList.addAll(login.getReportEmails());
		} else {
			emailList.add("leadreports@xtaascorp.com");
		}
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Follwing error occured.");
		stringBuilder.append("<br />");
		stringBuilder.append("<br />");
		stringBuilder.append(exceptionAsString);
		stringBuilder.append("<br />");
		stringBuilder.append("<br />");
		stringBuilder.append("Please contact your administrator.");
		for (String email : emailList) {
			XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", "Exception occured during lead report download",
					stringBuilder.toString());
			logger.debug("Download lead report error list email sent successfully to : " + email + ", data@xtaascorp.com");
		}
		PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Exception occured during lead report download. Error list email sent.";
		teamNoticeSerive.saveUserNotices(login.getId(), notice);
	}

	private void sendEmailWithLeadFile(Campaign campaign, ProspectCallSearchDTO prospectCallSearchDTO, File file,
			AmazonS3 s3client, Login login, Organization organization) {
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		String message = campaign.getName() + " - Attached lead report file of the previous day.";
		boolean leadReportSent = false;
		String subject;
		if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() == 1
				&& prospectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
			subject = " - DNC Report File Attached.";
		} else if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 1
				&& prospectCallSearchDTO.getSubStatus().contains("DNCL")) {
			subject = " - Failure Report File Attached.";
		} else {
			subject = " - Lead Report File Attached.";
		}
		
		String link = "";
		String linkref = "";
		String linkrefNotice = "";
		try {
			link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketNameReports, campaign.getName() + ".xlsx", file);
			linkref = "<a href=\" " + link + " \">Download here</a>";
			linkrefNotice = "<" + link + ">";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (login.getReportEmails() != null && login.getReportEmails().size() > 0) {
			List<String> emails = login.getReportEmails();
			if (emails != null && emails.size() > 0) {
				for (String email : emails) {
					if (email != null && !email.isEmpty()) {
						// XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", campaign.getName() + subject, message, fileNames);
						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", campaign.getName() + subject, campaign.getName() + " - Lead report of the previous day Download Link :  " + linkref);
						logger.debug("Client mapping lead report for the campaign [{}] sent  successfully to [{}] ",
								campaign.getName(), email);
						leadReportSent = true;
					}
				}
			}
		} else {
			// XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", campaign.getName() + subject, message, fileNames);
			XtaasEmailUtils.sendEmail(login.getEmail() , "data@xtaascorp.com", campaign.getName() + subject, campaign.getName() + " - Lead report of the previous day Download Link :  " + linkref);
		}
		try {
			String leadfileName = "LEADREPORTS" + SUFFIX + file;
			s3client.putObject(
					new PutObjectRequest(bucketName, leadfileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
			logger.debug("Recording File uploaded successfully to s3 storage.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (!leadReportSent) {
			// XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com", campaign.getName() + subject, message, fileNames);
			XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com" , "data@xtaascorp.com", campaign.getName() + subject, campaign.getName() + " - Lead report of the previous day Download Link :  " + linkref);
			logger.debug("Client mapping lead report for the campaign [{}] sent  successfully to leadreports@xtaascorp.com",
					campaign.getName());
		}
		PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Lead report of the previous day Download here " + linkrefNotice;
		teamNoticeSerive.saveUserNotices(login.getId(), notice);
	}

	private List<ProspectCallLog> getLeadsFromDB(ProspectCallSearchDTO prospectCallSearchDTO, Campaign campaign)
			throws ParseException {
		ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
		ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			prospectCallSearchDTO.setCampaignIds(campaign.getCampaignGroupIds());
		}
		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			LocalDate todaydate = LocalDate.now();
			prospectCallSearchDTO.setFromDate(todaydate.withDayOfMonth(1).toString());
			prospectCallSearchDTO.setToDate(todaydate.toString());
			prospectCallSearchDTO.setStartMinute(0);
			prospectCallSearchDTO.setEndMinute(0);
		}
		if (prospectCallSearchDTO.getLeadStatus() != null && prospectCallSearchDTO.getLeadStatus().size() > 0) {

			List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
			List<String> successLeadStatus = new ArrayList<String>();
			List<String> failureLeadStatus = new ArrayList<String>();
			for (String lStatus : leadStatusList) {
				if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())) {
					successLeadStatus.add(lStatus);
				} else {
					failureLeadStatus.add(lStatus);
				}
			}
			if (successLeadStatus.size() > 0) {
				List<String> dispositionList = new ArrayList<String>(1);
				dispositionList.add("SUCCESS");
				successPCSDTO = prospectCallSearchDTO;
				successPCSDTO.setLeadStatus(successLeadStatus);
				successPCSDTO.setDisposition(dispositionList);
				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(successPCSDTO);
				prospectCallLogList.addAll(prospectCallLogSuccess);
			}
			if (failureLeadStatus.size() > 0) {
				List<String> dispositionList = new ArrayList<String>(1);
				dispositionList.add("FAILURE");
				failurePCSDTO = prospectCallSearchDTO;
				failurePCSDTO.setLeadStatus(null);
				failurePCSDTO.setDisposition(dispositionList);
				failurePCSDTO.setSubStatus(failureLeadStatus);
				List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(failurePCSDTO);
				prospectCallLogList.addAll(prospectCallLogFailure);
			}
		} else {
			prospectCallLogList = prospectCallLogRepositoryImpl.searchProspectCallByReport(prospectCallSearchDTO);
		}
		logger.debug("Total " + prospectCallLogList.size() + " leads found for campaign : " + campaign.getName());
		return prospectCallLogList;
	}

	public String getColumnValue(Campaign campaign, ProspectCallLog pcl, String column, Map<String, String> colNames,
			Map<String, List<String>> clientMap, Map<String, String> singleClientValueMap,Map<String, Map<String, String>> customQuestionsMap) {
		ProspectCall pc = pcl.getProspectCall();

		if (singleClientValueMap.containsKey(column)) {
			return singleClientValueMap.get(column);
		}

		if (pcl.getQaFeedback() != null && pcl.getQaFeedback().getQaAnswers() != null) {
			for (AgentQaAnswer qaAnswer : pcl.getQaFeedback().getQaAnswers()) {
				String qaQuest = "";
				if(qaAnswer.getQuestion() != null && !qaAnswer.getQuestion().isEmpty()) {
					qaQuest = "QA-" + qaAnswer.getQuestion();
				} else if(qaAnswer.getAttribute() != null && !qaAnswer.getAttribute().isEmpty()) {
					qaQuest = "QA-" + qaAnswer.getAttribute();
				}
				if (column != null && colNames.get(column) != null && colNames.get(column).equalsIgnoreCase(qaQuest)) {
					String answerCertification = qaAnswer.getAnswerCertification();
					return answerCertification;

				}
			}
		}
		String customQuestionCommonAttribute = colNames.get(column);
		if (customQuestionCommonAttribute != null && customQuestionCommonAttribute.contains("CUSTOM_QUESTION_")) {
			if(customQuestionsMap != null) {
				Map<String, String> cmap = customQuestionsMap.get(pc.getCampaignId());
				if (cmap != null && cmap.size() > 0) {
					for(Map.Entry<String,String> entry : cmap.entrySet()) {
						String cquest = entry.getKey(); 
						String cqValue = entry.getValue();
						if (pc.getAnswers() != null && cqValue!=null && cqValue.equalsIgnoreCase(customQuestionCommonAttribute)) {
							for (AgentQaAnswer aAnswer : pc.getAnswers()) {
								if (cquest.equalsIgnoreCase(aAnswer.getQuestion())) {
									return aAnswer.getQuestion();
								}
							}
						}
					}
					
				}
			}
		}
		
		if (customQuestionCommonAttribute != null && customQuestionCommonAttribute.contains("CUSTOM_ANSWER_")) {
			if(customQuestionsMap != null) {
				int in = "CUSTOM_ANSWER_".length();
				String tempAnswerStr = customQuestionCommonAttribute.substring(in, customQuestionCommonAttribute.length());
				String customquest = "CUSTOM_QUESTION_" + tempAnswerStr;
				Map<String, String> cmap = customQuestionsMap.get(pc.getCampaignId());
				if (cmap != null && cmap.size() > 0) {
					for(Map.Entry<String,String> entry : cmap.entrySet()) {
						String cquest = entry.getKey(); 
						String cValue = entry.getValue();
						if (pc.getAnswers() != null && customquest.equalsIgnoreCase(cValue)) {
							for (AgentQaAnswer aAnswer : pc.getAnswers()) {
								if (cquest.equalsIgnoreCase(aAnswer.getQuestion())) {
									return aAnswer.getAnswer();
								}
							}
						}
					}
				}	
			}
		}
		
		if("CAMPAIGN_NAME".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getCampaignId() != null) {
				Campaign c = campaignService.getCampaign(pc.getCampaignId());
				if(c != null && c.getName() != null) {
					return c.getName().toString();
				}
			}
			return "";
		}
		if ("EMPLOYEE_MIN".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				return (pc.getProspect().getCustomAttributeValue("minEmployeeCount") != null)
						? pc.getProspect().getCustomAttributeValue("minEmployeeCount").toString()
						: "000";
			}
			return "";
		}
		
		if ("REVENUE_MIN".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				return (pc.getProspect().getCustomAttributeValue("minRevenue") != null)
						? pc.getProspect().getCustomAttributeValue("minRevenue").toString()
						: "000";
			}
			return "";
		}
		
		if ("LEAD_STATUS".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getLeadStatus() != null) {
				return pc.getLeadStatus().toString();
			}else if (pc != null && pc.getSubStatus() != null) {
				return pc.getSubStatus().toString();
			}
			return "";
		}
		
		if ("QA_ID".equalsIgnoreCase(colNames.get(column))) {
			if (pcl != null && pcl.getQaFeedback() != null && pcl.getQaFeedback().getSecQaId() != null) {
				return pcl.getQaFeedback().getSecQaId().toString();
			} else if (pcl != null && pcl.getQaFeedback() != null && pcl.getQaFeedback().getQaId() != null) {
				return pcl.getQaFeedback().getQaId().toString();
			}
			return "";
		}
		
		if ("QA_STATUS".equalsIgnoreCase(colNames.get(column))) {
			if (pcl != null && pcl.getStatus() != null) {
				return pcl.getStatus().toString();
			}
			return "";
		}
		
		if ("DATA_SOURCE".equalsIgnoreCase(colNames.get(column))) {
			if (pcl != null && pcl.getProspectCall().getProspect().getSource() != null) {
				String prospectDataSource = pcl.getProspectCall().getProspect().getSource().toString();
				Map<String, String> dataSourceList = dataProviderService.getXtaasDataProviderByIndex();
				if (dataSourceList.containsKey(prospectDataSource)) {
					for(Map.Entry<String,String> entry : dataSourceList.entrySet()) {
						if(entry.getKey().equals(prospectDataSource)) {
							return entry.getValue();
						}
					}
				}else {
					return prospectDataSource;
				}
			}
			return "";
		}
		
		if ("LEAD_VALID".equalsIgnoreCase(colNames.get(column))) {
			String valid = "";
			if (pcl != null && pcl.getQaFeedback() != null && pcl.getQaFeedback().getFeedbackResponseList() != null) {
				List<FeedbackSectionResponse> fsrList = pcl.getQaFeedback().getFeedbackResponseList();
				for (FeedbackSectionResponse fsr : fsrList) {
					if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
						for (FeedbackResponseAttribute fra : fraList) {
							if (fra != null && fra.getAttribute() != null && fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
								valid = fra.getFeedback();
							}
						}
					}
				}
			}
			return valid;
		}
		
		if ("REJECT_REASON".equalsIgnoreCase(colNames.get(column))) {
			String qaReason = "";
			if (pcl != null && pcl.getQaFeedback() != null && pcl.getQaFeedback().getFeedbackResponseList() != null) {
				List<FeedbackSectionResponse> fsrList = pcl.getQaFeedback().getFeedbackResponseList();
				for (FeedbackSectionResponse fsr : fsrList) {
					if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
						for (FeedbackResponseAttribute fra : fraList) {
							if (fra != null && fra.getAttribute() != null && fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
								if (fra.getRejectionReason() != null) {
									qaReason = qaReason + fra.getRejectionReason();
								}
							}
						}
					}
				}
			}
			return qaReason;
		}
		
		if ("NOTES".equalsIgnoreCase(colNames.get(column))) {
			String callnotes = "";
			if (pcl != null && pcl.getQaFeedback() != null && pcl.getQaFeedback().getFeedbackResponseList() != null) {
				List<FeedbackSectionResponse> fsrList = pcl.getQaFeedback().getFeedbackResponseList();
				for (FeedbackSectionResponse fsr : fsrList) {
					if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
						for (FeedbackResponseAttribute fra : fraList) {
							if (fra != null && fra.getAttribute() != null && fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
								if (fra.getAttributeComment() != null) {
									callnotes = fra.getAttributeComment();
								}
							}
						}
					}
				}
			}
			return callnotes;
		}
		
		if ("EMAIL_STATUS".equalsIgnoreCase(colNames.get(column))) {
			String isMailBounced = "";
			if (pc != null) {
				if (pc.isEmailBounce()) {
					isMailBounced = "BOUNCED";
				} else {
					isMailBounced = "NOT-BOUNCED";
				}
			}
			return isMailBounced;
		}
		
		if ("CLIENT_DELIVERED".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getClientDelivered() != null && !pc.getClientDelivered().isEmpty()) {
				return pc.getClientDelivered().toString();
			}
			return "";
		}
		
		if ("PARTNER_ID".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getPartnerId() != null) {
				return pc.getPartnerId().toString();
			}
			return "";
		}
		
		if ("SUPERVISOR_ID".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getSupervisorId() != null) {
				return pc.getSupervisorId().toString();
			}
			return "";
		}
		
		if ("AGENT_ID".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getAgentId() != null) {
				return pc.getAgentId().toString();
			}
			return "";
		}
		
		if ("AGENT_NAME".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getAgentId() != null) {
				Login login = loginRepository.findOneById(pc.getAgentId());
				if(login != null) {
					return login.getFirstName()+" "+login.getLastName();	
				}
			}
			return "";
		}
		
		if ("TEAM_LEAD".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getAgentId() != null) {
				List<Team> team = teamRepository.findAllTeamsByAgentId(pc.getAgentId());
				if (team != null && team.size() > 0) {
					if (team.get(0).getSupervisor() != null) {
						return team.get(0).getSupervisor().getResourceId().toString();
					}
				}
			}
			return "";
		}
		
		if ("CALL_START_TIME".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getCallStartTime() != null) {
				DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
				return pstFormat.format(pc.getCallStartTime());
			}
			return "";
		}
		
		if ("LEAD_SOURCE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getZoomPersonUrl() != null) {
				return pc.getZoomPersonUrl().toString();
			}
			return "";
		}
		
		if ("EMPLOYEE_SIZE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null &&pc.getProspect().getCustomAttributeValue("minEmployeeCount") != null
					&& pc.getProspect().getCustomAttributeValue("maxEmployeeCount") != null) {
				String employeeSize = pc.getProspect().getCustomAttributeValue("minEmployeeCount").toString() + "-"
						+ pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
				return employeeSize;
			}
			return "";
		}
		
		if ("REVENUE_SIZE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null && pc.getProspect().getCustomAttributeValue("minRevenue") != null
					&& pc.getProspect().getCustomAttributeValue("maxRevenue") != null) {
				String revenueSize = pc.getProspect().getCustomAttributeValue("minRevenue").toString() + "-"
						+ pc.getProspect().getCustomAttributeValue("maxRevenue").toString();
				return revenueSize;
			}
			return "";
		}
		
		if ("AGENT_NOTES".equalsIgnoreCase(colNames.get(column))) {
			StringBuffer stringBuffer = new StringBuffer();
			if (pc != null && pc.getNotes() != null && pc.getNotes().size() > 0) {
				List<Note> notesArr = pc.getNotes();
				for (Note note : notesArr) {
					if (note != null) {
						stringBuffer.append(note.getText());
					}
				}
				return stringBuffer.toString();
			}
			return "";
		}
		
		// As per new implementation.
		if ("COMPANY_DOMAIN".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null && pc.getProspect().getCustomAttributeValue("domain") != null
					&& !pc.getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null && pc.getProspect().getCustomAttributeValue("domain") != null)
							? pc.getProspect().getCustomAttributeValue("domain").toString()
							: "";
				}
			}
		}

		if ("EMPLOYEE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null && pc.getProspect().getCustomAttributeValue("maxEmployeeCount") != null
					&& !pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty()) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null
							&& pc.getProspect().getCustomAttributeValue("maxEmployeeCount") != null)
									? pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()
									: "";
				}
			}
		}

		// For naics & sic
		if ("NAICS".equalsIgnoreCase(colNames.get(column))) {
			if (pc.getProspect().getSourceCompanyId() != null && !pc.getProspect().getSourceCompanyId().isEmpty()) {
				AbmListDetail abmListDetail = abmListDetailRepository
						.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if (abmListDetail != null && abmListDetail.getCompanyNAICS() != null && abmListDetail.getCompanyNAICS().size() > 0) {
						String naics = String.join(",", abmListDetail.getCompanyNAICS());
						return naics;
				}
			}
			return "";
		}

		if ("SIC".equalsIgnoreCase(colNames.get(column))) {
			if (pc.getProspect().getSourceCompanyId() != null && !pc.getProspect().getSourceCompanyId().isEmpty()) {
				AbmListDetail abmListDetail = abmListDetailRepository
						.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if (abmListDetail != null && abmListDetail.getCompanySIC() != null && abmListDetail.getCompanySIC().size() > 0) {
						String sic = String.join(",", abmListDetail.getCompanySIC());
						return sic;
				}
			}
			return "";
		}

		if ("INPUT_ABM_VALUE".equalsIgnoreCase(colNames.get(column))) {
			// List<AbmListNew> abmList =
			// abmListNewRepository.findABMListNewByCampaignIdAndStatus(pc.getCampaignId());
			// Map<String, String> abmMap = new HashMap<String, String>();
			/*
			 * if (abmList != null && abmList.size() > 0) { for (AbmList abm : abmList) {
			 * List<KeyValuePair<String, String>> companies = abm.getCompanyList(); for
			 * (KeyValuePair<String, String> companykv : companies) { if
			 * (companykv.getValue() != null && !companykv.getValue().isEmpty() &&
			 * !companykv.getValue().equalsIgnoreCase("NOT_FOUND") &&
			 * !companykv.getValue().equalsIgnoreCase("GET_COMPANY_ID")) {
			 * 
			 * abmMap.put(companykv.getValue(), companykv.getKey()); } } }
			 */
			if (pc.getProspect().getSourceCompanyId() != null && !pc.getProspect().getSourceCompanyId().isEmpty()) {
				AbmListNew abmListNew = abmListNewRepository.findOneByCampaignIdCompanyId(pc.getCampaignId(),
						pc.getProspect().getSourceCompanyId());

				if (abmListNew != null) {
					return abmListNew.getCompanyName();
				}
			}
			// }
			return "";
		}

		// For exact employee count
		if ("EXACT_EMPLOYEE_COUNT".equalsIgnoreCase(colNames.get(column)) || "EMPLOYEE_MAX".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				AbmListDetail abmListDetail = abmListDetailRepository
						.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if (abmListDetail != null && abmListDetail.getCompanyEmployeeCount() != null) {
						return abmListDetail.getCompanyEmployeeCount().toString();
				} else {
					return (pc.getProspect().getCustomAttributeValue("maxEmployeeCount") != null)
							? pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString()
							: "";
				}
			}
			return "";
		}

		// For exact revenue count
		if ("EXACT_REVENUE_COUNT".equalsIgnoreCase(colNames.get(column)) || "REVENUE_MAX".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				AbmListDetail abmListDetail = abmListDetailRepository
						.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if (abmListDetail != null && abmListDetail.getCompanyRevenue() != null) {
						return abmListDetail.getCompanyRevenue();
				} else {
					return (pc.getProspect().getCustomAttributeValue("maxRevenue") != null)
							? pc.getProspect().getCustomAttributeValue("maxRevenue").toString()
							: "000";
				}
			}
		}

		if ("REVENUE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null && pc.getProspect().getCustomAttributeValue("maxRevenue") != null
					&& !pc.getProspect().getCustomAttributeValue("maxRevenue").toString().isEmpty()) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null
							&& pc.getProspect().getCustomAttributeValue("maxRevenue") != null)
									? pc.getProspect().getCustomAttributeValue("maxRevenue").toString()
									: "";
				}
			}
		}

		if ("VALID_TITLE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getManagementLevel() : "";
				}
			}
		}

		if ("DEPARTMENT".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getDepartment() : "";
				}
			}
		}

		if ("INDUSTRY".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getIndustry() : "";
				}
			}
		}

		if ("STATE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getStateCode() : "";
				}
			}
		}

		if ("SEARCH_TITLE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getTitle() : "";
				}
			}
		}

		// All below methods checks if single value(default value) present in
		// clientValues returns that client value otherwise returns prospect value.
		if ("ZOOM_COMPANY_URL".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				String companyUrl = null;
				if (pcl != null && pcl.getProspectCall() != null && pcl.getProspectCall().getProspect() != null && pcl.getProspectCall().getProspect().getZoomCompanyUrl() != null) {
					companyUrl = pcl.getProspectCall().getProspect().getZoomCompanyUrl();
					return companyUrl;
				}
				else {
					if (pcl != null && pcl.getProspectCall() != null
							&& pcl.getProspectCall().getZoomCompanyUrl() != null) {
						companyUrl = pcl.getProspectCall().getZoomCompanyUrl();
						return (companyUrl != null) ? companyUrl : "";
					}
				}
			}
		}

		if ("ZOOM_PERSON_URL".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				String personUrl = null;
				if (pcl != null && pcl.getProspectCall() != null && pcl.getProspectCall().getProspect() != null && pcl.getProspectCall().getProspect().getZoomPersonUrl() != null) {
					personUrl = pcl.getProspectCall().getProspect().getZoomPersonUrl();
					return personUrl;
				} else {
					if (pcl != null && pcl.getProspectCall() != null
							&& pcl.getProspectCall().getZoomPersonUrl() != null) {
						personUrl = pcl.getProspectCall().getZoomPersonUrl();
						return (personUrl != null) ? personUrl : "";
					}
				}
			}
		}

		if ("PROSPECT_CALL_ID".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null) ? pc.getProspectCallId() : "";
			}
		}

		if ("VALID_COUNTRY".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getCountry() : "";
			}
		}

		if ("CITY".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getCity() : "";
			}
		}

		if ("ADDRESS".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null && pc.getProspect().getAddressLine1() != null)
						? pc.getProspect().getAddressLine1().replaceAll("[^a-zA-Z0-9\\s]", "")
						: "";
			}
		}

		if ("FULL_ADDRESS".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getAddressLine1() + " "
						+ pc.getProspect().getCity() + " " + pc.getProspect().getStateCode().replaceAll("[^a-zA-Z0-9\\s]", "") : "";
			}
		}

		if ("ORGANIZATION".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null && pc.getProspect().getCompany() != null)
						? pc.getProspect().getCompany()
						: "";
			}
		}

		if ("EMAIL".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getDispositionStatus() != null
					&& !pc.getDispositionStatus().equalsIgnoreCase("SUCCESS") && campaign != null
					&& campaign.isHideNonSuccessPII()) {
				return "XXXXXXXXXX@XXX";
			} else {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return (pc != null && pc.getProspect() != null) ? pc.getProspect().getEmail() : "";
				}
			}
		}

		if ("PHONE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getDispositionStatus() != null
					&& !pc.getDispositionStatus().equalsIgnoreCase("SUCCESS") && campaign != null
					&& campaign.isHideNonSuccessPII()) {
				return "XXX-XXX-XXXX";
			} else {
				String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
				if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
					return clientValue;
				} else {
					return formatPhone(pc, false);
				}
			}
		}

		if ("COMPANY_PHONE".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return formatPhone(pc, true);
			}
		}

		if ("FULL_NAME".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? StringUtils.capitalize(pc.getProspect().getFirstName()) + " "
						+ StringUtils.capitalize(pc.getProspect().getLastName()) : "";
			}
		}

		if ("FIRST_NAME".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? StringUtils.capitalize(pc.getProspect().getFirstName()) : "";
			}
		}
		
		
		
		
		if(colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_01")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_02")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_03")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_04")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_05")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_06")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_07")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_08")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_09")
				|| colNames.get(column).equalsIgnoreCase("CUSTOMFIELD_10")) {
			if(pc.getProspect().getCustomFields()!=null &&  pc.getProspect().getCustomFields().size()>0) {
				for(AgentQaAnswer aqa : pc.getProspect().getCustomFields()) {
					if(aqa.getQuestion().equalsIgnoreCase(column)) {
						String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
						if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
							return clientValue;
						} else {
							return aqa.getAnswer();
						}
					}

				}
			}

		}
		
		
		/*if(pc.getProspect().getCustomFields()!=null &&  pc.getProspect().getCustomFields().size()>0) {
			for(AgentQaAnswer aqa : pc.getProspect().getCustomFields()) {
				
				if(aqa.getQuestion().equalsIgnoreCase(colNames.get(column))) {
					String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
					if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
						return clientValue;
					} else {
						return aqa.getAnswer();
					}
				}
			}
		}*/
		
		/*if(expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
				|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10")) {
			
		}*/

		if ("LAST_NAME".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? StringUtils.capitalize(pc.getProspect().getLastName()) : "";
			}
		}

		if ("PROSPECT_CREATED".equalsIgnoreCase(colNames.get(column))) {
			List<String> clientValues = clientMap.get("PROSPECT_CREATED");
			if (clientValues != null && clientValues.size() == 1) {
				return getDateAsPerFormat(clientValues.get(0), "MM/dd/yyyy", false);
			} else {
				DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
				pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
				return (pc != null && pc.getCallStartTime() != null) ? formatDate(pc.getCallStartTime()) : "";
			}
		}

		if ("CREATED_TIME".equalsIgnoreCase(colNames.get(column))) {
			List<String> clientValues = clientMap.get("CREATED_TIME");
			if (clientValues != null && clientValues.size() == 1) {
				return getDateAsPerFormat(clientValues.get(0), "yyyy/MM/dd HH:mm:ss", true);
			} else {
				DateFormat pstFormat = new SimpleDateFormat("");
				pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
				return (pc != null && pc.getCallStartTime() != null)
						? getDateAsPerFormat(pc.getCallStartTime().toString(), "yyyy/MM/dd HH:mm:ss", true)
						: "";
			}
		}

		if ("ASSET_NAME".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				String assetName = "";
				if (pc != null && (pc.getDeliveredAssetId() != null && !pc.getDeliveredAssetId().isEmpty())) {
					assetName = getAssetNameFromId(campaign, pc.getDeliveredAssetId());
				}
				return assetName;
			}
		}

		if ("ZIP".equalsIgnoreCase(colNames.get(column))) {
			List<String> clientValues = clientMap.get("ZIP");
			if (clientValues != null && clientValues.size() == 1) {
				return getZipCodeInProperFormat(clientValues.get(0));
			} else {
				return (pc != null && pc.getProspect() != null) ? getZipCodeInProperFormat(pc.getProspect().getZipCode()) : "";
			}
		}

		if ("RECORDING_URL_DOWNLOAD".equalsIgnoreCase(colNames.get(column))) {
			try {
				String campaignName = "";
				if (campaign != null
						&& (campaign.getClientCampaignName() != null && !campaign.getClientCampaignName().isEmpty())) {
					campaignName = campaign.getClientCampaignName();
				} else {
					campaignName = (campaign != null) ? campaign.getName() : "";
				}
				String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
				String fileName = pc.getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "") + "_"
						+ pc.getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "") + "_" + cleanCampaignName + ".wav";
				return fileName;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if ("RECORDING_URL".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && (pc.getRecordingUrlAws() == null || pc.getRecordingUrlAws().isEmpty())) {
				if (pc.getRecordingUrl() == null || pc.getRecordingUrl().isEmpty()) {
					return "";
				}
				String recordingUrl = pc.getRecordingUrl();
				int lastIndex = recordingUrl.lastIndexOf("/");
				String recordingFile = recordingUrl.substring(lastIndex + 1, recordingUrl.length()) + ".wav";
				pc.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + recordingFile);
			}
			return pc.getRecordingUrlAws();
		}
		// Below for actual industry
		if ("ACTUAL_INDUSTRY".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				if (pc.getProspect().getIndustryList() != null && pc.getProspect().getIndustryList().size() > 0) {
					StringBuilder sb = new StringBuilder();
					List<String> indList = pc.getProspect().getIndustryList();
					for (String ind : indList) {
						sb.append(ind);
						sb.append(",");
					}
					return sb.toString();
				} else if (pc.getProspect().getIndustry() != null && !pc.getProspect().getIndustry().isEmpty()) {
					return pc.getProspect().getIndustry();
				}
			} else {
				return "";
			}
		}
		// below for actual department
		if ("ACTUAL_DEPARTMENT".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getDepartment() : "";
			}

		}
		// Below for job Title
		if ("ACTUAL_SEARCH_TITLE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getTitle() : "";
			}
		}
		// Below for managementlevel
		if ("ACTUAL_VALID_TITLE".equalsIgnoreCase(colNames.get(column))) {
			if (pc != null && pc.getProspect() != null) {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getManagementLevel() : "";
			}
		}

		if ("INDUSTRY_GROUP".equalsIgnoreCase(colNames.get(column))) {
			String clientValue = getQaSelectedClientValue(pcl, colNames.get(column));
			if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("NA")) {
				return clientValue;
			} else {
				return (pc != null && pc.getProspect() != null) ? pc.getProspect().getIndustry() : "";
			}
		}

		// if (pc != null && colNames != null && column != null) {
		// String customQuestionStdAttribute = colNames.get(column);
		// String questionSkin = attribQuestionsMap.get(customQuestionStdAttribute);
		// if (pc.getAnswers() != null) {
		// for (AgentQaAnswer agentAnswer : pc.getAnswers()) {
		// if (agentAnswer != null && agentAnswer.getQuestion() != null
		// && agentAnswer.getQuestion().equalsIgnoreCase(questionSkin)) {
		// return agentAnswer.getAnswer();
		// }
		// }
		// }
		// }

		if (pcl != null && pcl.getQaFeedback() != null && colNames != null && column != null) {
			String customQuestionStdAttribute = colNames.get(column);
			if (pcl.getQaFeedback().getQaAnswers() != null) {
				for (AgentQaAnswer qaAnswer : pcl.getQaFeedback().getQaAnswers()) {
					if (qaAnswer != null && qaAnswer.getAttribute() != null
							&& qaAnswer.getAttribute().equalsIgnoreCase(customQuestionStdAttribute)) {

						if (qaAnswer.getClientValue() != null) {
							return qaAnswer.getClientValue();
						} else {
							return qaAnswer.getAnswer();
						}

					}
				}
			}
		}
		return "";
	}

	private String getZipCodeInProperFormat(String zipCode) {
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		if (zipCode != null) {
			Matcher m = p.matcher(zipCode);
			int count = 0;
			while (m.find()) {
				count = m.start();
			}
			if (count > 0) {
				String tempString = zipCode.substring(0, count);
				return tempString;
			}
		}
		return zipCode;
	}

	private String formatDate(Date callStartTime) {
		DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
		pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String pstStr = pstFormat.format(callStartTime);
		Date pstDate = null;
		try {
			pstDate = pstFormat.parse(pstStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pstFormat.format(pstDate);
	}

	private String getDateAsPerFormat(String date, String format, boolean withTime) {
		DateFormat pstFormat = new SimpleDateFormat(format);
		pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		try {
			Date time = (Date) formatter.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(time);
			if (withTime) {
				String formatedDateWithTime = calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/"
						+ calendar.get(Calendar.DATE) + " " + calendar.get(Calendar.HOUR_OF_DAY) + ":"
						+ calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
				return formatedDateWithTime;
			} else {
				String formatedDate = calendar.get(Calendar.MONTH) + "/" + (calendar.get(Calendar.DATE) + 1) + "/"
						+ calendar.get(Calendar.YEAR);
				return formatedDate;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return format;
	}

	private String getAssetNameFromId(Campaign campaign, String assetId) {
		String assetName = "";
		List<Asset> assets = campaign.getAssets();
		if (assets != null && assets.size() > 0) {
			for (Asset asset : assets) {
				if (asset.getAssetId() != null && asset.getAssetId().equalsIgnoreCase(assetId)) {
					assetName = asset.getName();
					break;
				}
			}
		}
		return assetName;
	}

	void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				deleteDir(f);
			}
		}
		file.delete();
	}

	private String getQaSelectedClientValue(ProspectCallLog prospectCallLog, String stdAttribute) {
		String clientValue = "NA";
		if (prospectCallLog != null && prospectCallLog.getQaFeedback() != null
				&& prospectCallLog.getQaFeedback().getQaAnswers() != null) {
			for (AgentQaAnswer qaAnswer : prospectCallLog.getQaFeedback().getQaAnswers()) {
				if (qaAnswer != null && qaAnswer.getAttribute() != null
						&& qaAnswer.getAttribute().equalsIgnoreCase(stdAttribute)) {
					clientValue = qaAnswer.getClientValue();
				}
			}
		}
		return clientValue;
	}

	private String formatPhone(ProspectCall pc, boolean isCompanyPhone) {
		String ph = "";
		String country = "";
		if (pc != null && pc.getProspect() != null) {
			if (isCompanyPhone) {
				if (pc.getProspect().getSource() != null && pc.getProspect().getSource().equalsIgnoreCase("INSIDEVIEW")) {
					ph = pc.getProspect().getCompanyPhone() != null ? pc.getProspect().getCompanyPhone() : pc.getProspect().getPhone();
				} else {
					ph = pc.getProspect().getCompanyPhone() != null ? pc.getProspect().getCompanyPhone() : "NA";
				}
			} else {
				ph = pc.getProspect().getPhone() != null ? pc.getProspect().getPhone() : "NA";
			}
			if (ph.equalsIgnoreCase("NA")) {
				return ph;
			}
			ph = ph.replaceAll("[^a-zA-Z0-9]", "");
			if (pc.getProspect().getCountry() != null) {
				country = pc.getProspect().getCountry();
			}
			if (!country.isEmpty() && country.equalsIgnoreCase("US") || country.equalsIgnoreCase("united states")
					|| country.equalsIgnoreCase("USA") || country.equalsIgnoreCase("CANADA") || country.equalsIgnoreCase("CA")) {

				if (ph.length() > 10) {
					int startIndex = ph.length() - 10;
					String tempStr = ph.substring(startIndex);
					ph = String.valueOf(tempStr).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
				}
				if (ph.length() <= 10) {
					ph = String.valueOf(ph).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
				}
			} else {
				ph = "+" + ph;
			}
		}

		return ph;
	}

	private String getCurrentDate() {
		String currentDate = new Date().toString();
		String removeSpaceStr = currentDate.replaceAll(" ", "_");
		String removeColunStr = removeSpaceStr.replaceAll(":", "_");
		return removeColunStr;
		}
	
		public void downloadConnectReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO, HttpServletRequest request,
				HttpServletResponse response, Login login) throws ParseException, Exception {
			AmazonS3 s3client = createAWSConnection(clientRegion);
			try {
				String type = "connectSingle";
				// NOT_SCORED, QA_ACCEPTED, QA_REJECTED, QA_RETURNED, ACCEPTED, REJECTED,
				// CLIENT_ACCEPTED, CLIENT_REJECTED
				Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
				Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
				String fileTime = getCurrentDate();
				String campaignName = campaign.getName() + "_" + fileTime;
				Map<String, Campaign> groupCampaigMap = new HashMap<String, Campaign>();
				boolean multiCampaign = false;
				// List<Campaign> campaignsFromDB =
				// campaignService.getCampaignByIds(campaign.getCampaignGroupIds());
				if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
					if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
						campaignName = campaign.getName() + "_GROUP_" + fileTime;
						List<Campaign> campaigsList = campaignService.getCampaignByIds(campaign.getCampaignGroupIds());
						for (Campaign camp : campaigsList) {
							groupCampaigMap.put(camp.getId(), camp);
						}
						prospectCallSearchDTO.setCampaignIds(campaign.getCampaignGroupIds());
					}else {
						groupCampaigMap.put(campaign.getId(), campaign);
					}
				} else if (prospectCallSearchDTO.getCampaignIds() != null
						&& prospectCallSearchDTO.getCampaignIds().size() > 1) {
					type = "connectMulti";
					List<Campaign> campaignsFromDB = campaignService.getCampaignByIds(prospectCallSearchDTO.getCampaignIds());
					Map<String, Campaign> groupInternalCampaigMap = new HashMap<String, Campaign>();
					campaignName = campaign.getOrganizationId() + "_" + fileTime;
					if (campaignsFromDB != null && campaignsFromDB.size() > 0) {
						for (Campaign camp : campaignsFromDB) {
							groupInternalCampaigMap.put(camp.getId(), camp);
						}
						if (groupInternalCampaigMap != null && groupInternalCampaigMap.size() > 0) {
							List<String> inCids = new ArrayList<String>();
							for (Map.Entry<String, Campaign> entry : groupInternalCampaigMap.entrySet()) {
								if (entry.getValue().getCampaignGroupIds() != null && entry.getValue().getCampaignGroupIds().size() > 0)
									inCids.addAll(entry.getValue().getCampaignGroupIds());
								else
									inCids.add(entry.getKey());
							}
							campaignsFromDB = campaignService.getCampaignByIds(inCids);
							for (Campaign camp : campaignsFromDB) {
								groupCampaigMap.put(camp.getId(), camp);
							}
							List<String> finalCids = new ArrayList<String>();
							for (Map.Entry<String, Campaign> entry : groupInternalCampaigMap.entrySet()) {
								finalCids.add(entry.getKey());
							}
							prospectCallSearchDTO.setCampaignIds(finalCids);
							multiCampaign = true;
						}
					}
				}
				List<ProspectCallLog> prospectCallLogList = getMultiCampaignLeadsFromDB(prospectCallSearchDTO);

				int recordsCounter = 0;
				logger.info("CampaignName :" + campaign.getName());
				File dirFile = new File(campaign.getName());
				if (!dirFile.exists()) {
					if (dirFile.mkdirs()) {
						logger.info("Directory is created!");
					} else {
						logger.info("Failed to create directory!");
					}
				}
				String folderName = campaign.getName();
				logger.info("Processing Data... PLEASE WAIT!!! :)");
				if (prospectCallLogList.size() <= 0) {
					return;
				}
				String fileName = folderName + SUFFIX + campaignName + ".xlsx";
				File file = new File(fileName);
				Workbook workbook = new XSSFWorkbook();
				CreationHelper createHelper = workbook.getCreationHelper();
				Sheet sheet = workbook.createSheet("Leads");
				Map<String, String> colNames = new HashMap<String, String>();
				int i = 0;
				String[] columns = new String[200];
				colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
				columns[i] = "ProspectCallId (for internal use only)";
				i++;
				colNames.put("Campaign Name", "Campaign Name");
				columns[i] = "Campaign Name";
				i++;
				colNames.put("First Name", "First Name");
				columns[i] = "First Name";
				i++;
				colNames.put("Last Name", "Last Name");
				columns[i] = "Last Name";
				i++;
				colNames.put("Job Title", "Job Title");
				columns[i] = "Job Title";
				i++;
				colNames.put("Email ID", "Email ID");
				columns[i] = "Email ID";
				i++;
				colNames.put("Phone No.", "Phone No.");
				columns[i] = "Phone No.";
				i++;
				colNames.put("Company Name", "Company Name");
				columns[i] = "Company Name";
				i++;
				colNames.put("Address Line 1", "Address Line 1");
				columns[i] = "Address Line 1";
				i++;
				colNames.put("Address Line 2", "Address Line 2");
				columns[i] = "Address Line 2";
				i++;
				colNames.put("City", "City");
				columns[i] = "City";
				i++;
				colNames.put("State", "State");
				columns[i] = "State";
				i++;
				colNames.put("Postal Code", "Postal Code");
				columns[i] = "Postal Code";
				i++;
				colNames.put("Country", "Country");
				columns[i] = "Country";
				i++;
				colNames.put("Company Employee Size Min", "Company Employee Size Min");
				columns[i] = "Company Employee Size Min";
				i++;
				colNames.put("Company Employee Size Max", "Company Employee Size Max");
				columns[i] = "Company Employee Size Max";
				i++;
				colNames.put("Company Revenue Min", "Company Revenue Min");
				columns[i] = "Company Revenue Min";
				i++;
				colNames.put("Company Revenue Max", "Company Revenue Max");
				columns[i] = "Company Revenue Max";
				i++;
				colNames.put("Industry Type", "Industry Type");
				columns[i] = "Industry Type";

				List<String> questionKeyList = new ArrayList<String>();
				for (Map.Entry<String, Campaign> campaignEntry : groupCampaigMap.entrySet()) {
					HashMap<String, ArrayList<PickListItem>> questionsList = campaignEntry.getValue().getQualificationCriteria()
							.getQuestions();
					for (Map.Entry<String, ArrayList<PickListItem>> entry : questionsList.entrySet()) {
						if (!questionKeyList.contains(entry.getKey())) {
							questionKeyList.add(entry.getKey());
							i++;
							colNames.put(entry.getKey(), entry.getKey());
							columns[i] = entry.getKey();
						}
					}
					i++;
				}
				i++;
				colNames.put("Asset title", "Asset title");
				columns[i] = "Asset title";
				i++;
				colNames.put("Download Date", "Download Date");
				columns[i] = "Download Date";
				i++;
				colNames.put("Lead Status", "Lead Status");
				columns[i] = "Lead Status";
				i++;
				colNames.put("QA ID", "QA ID");
				columns[i] = "QA ID";
				i++;
				colNames.put("QA Status", "QA Status");
				columns[i] = "QA Status";
				i++;
				colNames.put("C1. Lead Valid", "C1. Lead Valid");
				columns[i] = "C1. Lead Valid";
				i++;
				colNames.put("Reject Reason", "Reject Reason");
				columns[i] = "Reject Reason";
				i++;
				colNames.put("C1. Notes", "C1. Notes");
				columns[i] = "C1. Notes";
				i++;
				colNames.put("Email Status", "Email Status");
				columns[i] = "Email Status";
				i++;
				colNames.put("Em Line 1", "Address Line 1");
				columns[i] = "Address Line 1";
				i++;
				colNames.put("Client Delivered", "Client Delivered");
				columns[i] = "Client Delivered";
				i++;
				colNames.put("A1. Call Rcdg", "A1. Call Rcdg");
				columns[i] = "A1. Call Rcdg";
				i++;
				colNames.put("Partner ID", "Partner ID");
				columns[i] = "Partner ID";
				i++;
				colNames.put("Agent ID", "Agent ID");
				columns[i] = "Agent ID";
				//////////
				i++;
				colNames.put("A2. Branding", "A2. Branding");
				columns[i] = "A2. Branding";
				i++;
				colNames.put("A3. Pitch", "A3. Pitch");
				columns[i] = "A3. Pitch";
				i++;
				colNames.put("B1. Interest", "B1. Interest");
				columns[i] = "B1. Interest";
				i++;
				colNames.put("B2. Qual Ques", "B2. Qual Ques");
				columns[i] = "B2. Qual Ques";
				i++;
				colNames.put("B3. Contact Dtls", "B3. Contact Dtls");
				columns[i] = "B3. Contact Dtls";
				i++;
				colNames.put("B4. Direct Number", "B4. Direct Number");
				columns[i] = "B4. Direct Number";
				i++;
				colNames.put("B5. Email Addy", "B5. Email Addy");
				columns[i] = "B5. Email Addy";
				i++;
				colNames.put("B6. Consent", "B6. Consent");
				columns[i] = "B6. Consent";
				//////////
				i++;
				colNames.put("Zoom Company Link", "Zoom Company Link");
				columns[i] = "Zoom Company Link";
				i++;
				colNames.put("Zoom Employee Link", "Zoom Employee Link");
				columns[i] = "Zoom Employee Link";
				i++;
				colNames.put("Comments", "Comments");
				columns[i] = "Comments";
				i++;
				colNames.put("Action", "Action");
				columns[i] = "Action";

				colNames.put("Agent Notes", "Agent Notes");
				columns[i] = "Agent Notes";
				List<String> qaQuestionKeyList = new ArrayList<String>();
				for (Map.Entry<String, Campaign> campaignEntry : groupCampaigMap.entrySet()) {

					HashMap<String, ArrayList<PickListItem>> qaQuestionsList = campaignEntry.getValue().getQualificationCriteria()
							.getQuestions();
					for (Map.Entry<String, ArrayList<PickListItem>> entry : qaQuestionsList.entrySet()) {
						String qaQuest = "QA-" + entry.getKey();
						qaQuestionKeyList.add(qaQuest);
						i++;
						colNames.put(qaQuest, qaQuest);
						columns[i] = qaQuest;
					}
					i++;
				}
				colNames.put("Action", "Action");
				columns[i] = "Action";
				// Add a new line separator after the header
				// Create a Font for styling header cells
				Font headerFont = workbook.createFont();
				headerFont.setBold(true);
				headerFont.setFontHeightInPoints((short) 14);
				headerFont.setColor(IndexedColors.RED.getIndex());
				// Create a CellStyle with the font
				CellStyle headerCellStyle = workbook.createCellStyle();
				headerCellStyle.setFont(headerFont);
				// Create a Row
				Row headerRow = sheet.createRow(0);
				for (int x = 0; x < columns.length; x++) {
					if (columns[x] != null && !columns[x].isEmpty()) {
						Cell cell = headerRow.createCell(x);
						cell.setCellValue(columns[x]);
						cell.setCellStyle(headerCellStyle);
					}
				}
				// Create Cell Style for formatting Date
				CellStyle dateCellStyle = workbook.createCellStyle();
				dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
				int rowNum = 1;
				int j = 0;
				for (ProspectCallLog prospectCallLog : prospectCallLogList) {
					Row row = sheet.createRow(rowNum++);
					ProspectCall prospectCall = prospectCallLog.getProspectCall();
					Prospect prospect = prospectCallLog.getProspectCall().getProspect();
					// String cname = groupCampaigMap.get(prospectCall.getCampaignId()).getName();
					for (int col = 0; col < columns.length; col++) {
						if (columns[col] != null && !columns[col].isEmpty()) {
							if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getProspectCallId() != null) {
									cellx.setCellValue(prospectCall.getProspectCallId());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Client Name")) {
								Cell cellx = row.createCell(col);
								if (campaign != null && campaign.getClientName() != null) {
									cellx.setCellValue(campaign.getClientName());
									cellx.setCellStyle(dateCellStyle);
								} else {
									cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Campaign Name")) {
								Cell cellx = row.createCell(col);
								if (prospectCall != null && prospectCall.getCampaignId() != null) {
									Campaign gCampaign = groupCampaigMap.get(prospectCall.getCampaignId());
									String gCampaignName = "";
									if (gCampaign != null) {
										gCampaignName = gCampaign.getName();
									} else {
										gCampaignName = campaign.getName();
									}
									cellx.setCellValue(gCampaignName);
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("First Name")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getFirstName());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Last Name")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getLastName());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Job Title")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getTitle());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Email ID")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getEmail());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Phone No.")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getPhone());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Company Name")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getCompany());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Address Line 1")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getAddressLine1());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Address Line 2")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getAddressLine2());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("City")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getCity());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("State")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getStateCode());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Postal Code")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getZipCode());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Country")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getCountry());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Company Employee Size Min")) {
								Cell cellx = row.createCell(col);
								if (prospect.getCustomAttributeValue("minEmployeeCount") != null) {
									cellx.setCellValue(prospect.getCustomAttributeValue("minEmployeeCount").toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Company Employee Size Max")) {
								Cell cellx = row.createCell(col);
								if (prospect.getCustomAttributeValue("maxEmployeeCount") != null) {
									cellx.setCellValue(prospect.getCustomAttributeValue("maxEmployeeCount").toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Company Revenue Min")) {
								Cell cellx = row.createCell(col);
								if (prospect.getCustomAttributeValue("minRevenue") != null) {
									cellx.setCellValue(prospect.getCustomAttributeValue("minRevenue").toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Company Revenue Max")) {
								Cell cellx = row.createCell(col);
								if (prospect.getCustomAttributeValue("maxRevenue") != null) {
									cellx.setCellValue(prospect.getCustomAttributeValue("maxRevenue").toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("Industry Type")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospect.getIndustry());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Asset title")) {
								Cell cellx = row.createCell(col);
								String assetName = "";
								if (prospectCall.getDeliveredAssetId() != null && !prospectCall.getDeliveredAssetId().isEmpty()) {
									// if(campaign.getActiveAsset()!=null &&
									// campaign.getActiveAsset().getName()!=null &&
									// !campaign.getActiveAsset().getName().isEmpty()){
									assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
								}
								cellx.setCellValue(assetName);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Download Date")) {
								Cell cellx = row.createCell(col);
								DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
								pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
								String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
								Date pstDate = pstFormat.parse(pstStr);

								logger.info(pstFormat.format(pstDate));
								cellx.setCellValue(pstFormat.format(pstDate));
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Lead Status")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getLeadStatus() != null) {
									cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus().toString());
									cellx.setCellStyle(dateCellStyle);
								} else {
									cellx.setCellValue(prospectCallLog.getProspectCall().getSubStatus().toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("QA ID")) {
								if (prospectCallLog.getQaFeedback() != null && prospectCallLog.getQaFeedback().getQaId() != null) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue(prospectCallLog.getQaFeedback().getQaId().toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("QA Status")) {
								if (prospectCallLog.getStatus() != null) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue(prospectCallLog.getStatus().toString());
									cellx.setCellStyle(dateCellStyle);
								}
							} else if (columns[col].contentEquals("C1. Lead Valid")) {
								String valid = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													valid = fra.getFeedback();
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(valid);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Reject Reason")) {
								String qaReason = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getRejectionReason() != null) {
														qaReason = qaReason + fra.getRejectionReason();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(qaReason);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("C1. Notes")) {
								String callnotes = "";
								if (prospectCallLog.getQaFeedback() != null
										&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
									List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
									for (FeedbackSectionResponse fsr : fsrList) {
										if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
											List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
											for (FeedbackResponseAttribute fra : fraList) {
												if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
													if (fra.getAttributeComment() != null) {
														callnotes = fra.getAttributeComment();
													}
												}
											}
										}
									}
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(callnotes);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Email Status")) {
								String isMailBounced = "";
								if (prospectCall.isEmailBounce()) {
									isMailBounced = "BOUNCED";
								} else {
									isMailBounced = "NOT-BOUNCED";
								}
								Cell cellx = row.createCell(col);
								cellx.setCellValue(isMailBounced);
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Client Delivered")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getClientDelivered() != null
										&& !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
									cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
								else
									cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("A1. Call Rcdg")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog.getProspectCall().getRecordingUrlAws() == null
										|| prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()) {
									if (prospectCallLog.getProspectCall().getRecordingUrl() == null
											|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()) {
										cellx.setCellValue("");
									} else {
										String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
										int lastIndex = rUrl.lastIndexOf("/");
										String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
										String rdfile = xfileName + ".wav";
										prospectCallLog.getProspectCall()
												.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
										cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
									}
								} else {
									cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
								}
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Partner ID")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Agent ID")) {
								Cell cellx = row.createCell(col);
								cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Zoom Company link")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
										&& prospectCallLog.getProspectCall().getProspect() != null) {
									cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomCompanyUrl());
								} else {
									cellx.setCellValue("");
								}
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Zoom Employee Link")) {
								Cell cellx = row.createCell(col);
								if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
										&& prospectCallLog.getProspectCall().getProspect() != null) {
									cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomPersonUrl());
								} else {
									cellx.setCellValue("");
								}
								cellx.setCellStyle(dateCellStyle);
							} else if (columns[col].contentEquals("Agent Notes")) {
								Cell cellx = row.createCell(col);
								StringBuffer sb = new StringBuffer();
								if (prospectCallLog.getProspectCall().getNotes() != null
										&& prospectCallLog.getProspectCall().getNotes().size() > 0) {
									List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
									for (Note note : notesArr) {
										sb.append(note.getAgentId() + " : " + note.getText());
									}
								}
								cellx.setCellValue("");
								cellx.setCellStyle(dateCellStyle);
							} else {
								for (String question : questionKeyList) {
									String qaQuest = "QA-" + question;
									if (columns[col].contentEquals(question) && prospectCall.getAnswers() != null) {
										for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
											if (agentQaAnswer.getQuestion() != null
													&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())) {
												String agentAns = agentQaAnswer.getAnswer();
												Cell cellx = row.createCell(col);
												cellx.setCellValue(agentAns);
												cellx.setCellStyle(dateCellStyle);
												break;
											}
										}
									} else if (columns[col].contentEquals(qaQuest) && prospectCallLog.getQaFeedback() != null
											&& prospectCallLog.getQaFeedback().getQaAnswers() != null) {
										for (AgentQaAnswer agentQaAnswer : prospectCallLog.getQaFeedback().getQaAnswers()) {
											if (agentQaAnswer.getQuestion() != null
													&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())) {
												String answerCertification = agentQaAnswer.getAnswerCertification();
												Cell cellx = row.createCell(col);
												cellx.setCellValue(answerCertification);
												cellx.setCellStyle(dateCellStyle);
												break;
											}
										}
									}
								}
							}
						}
					}
					recordsCounter++;
				}
				FileOutputStream fileOut = new FileOutputStream(fileName);
				workbook.write(fileOut);
				fileOut.close();
				logger.info("CSV file for campaign : " + campaign.getName() + " was created successfully. Records Exported : "
						+ recordsCounter);
				sendEmailWithLeadFile(campaign.getName(), campaignName, prospectCallSearchDTO, file, s3client, login,
						organization, "connectSingle");
				deleteDir(dirFile);
			} catch (Exception e) {
				logger.info("Error in CsvFileWriter !!!");
				e.printStackTrace();
			}
		}
	

	private List<ProspectCallLog> getMultiCampaignLeadsFromDB(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
		ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
		ProspectCallSearchDTO callbackPCSDTO = new ProspectCallSearchDTO();
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			LocalDate todaydate = LocalDate.now();
			LocalDate fromDate = todaydate.minusDays(7);
			prospectCallSearchDTO.setFromDate(fromDate.toString());
			prospectCallSearchDTO.setToDate(todaydate.toString());
			prospectCallSearchDTO.setStartMinute(0);
			prospectCallSearchDTO.setEndMinute(0);
		}

		if (prospectCallSearchDTO.getLeadStatus() != null && prospectCallSearchDTO.getLeadStatus().size() > 0) {
			List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
			List<String> successLeadStatus = new ArrayList<String>();
			List<String> failureLeadStatus = new ArrayList<String>();
			List<String> callbackLeadStatus = new ArrayList<String>();
			for (String lStatus : leadStatusList) {
				if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())) {
					successLeadStatus.add(lStatus);
				} else if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CALLBACK.toString())) {
					callbackLeadStatus.add(lStatus);
				} else {
					failureLeadStatus.add(lStatus);
				}
			}
			if (successLeadStatus.size() > 0) {
				List<String> dispositionList = new ArrayList<String>(1);
				dispositionList.add("SUCCESS");
				successPCSDTO = prospectCallSearchDTO;
				successPCSDTO.setLeadStatus(successLeadStatus);
				successPCSDTO.setDisposition(dispositionList);
				successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(successPCSDTO);
				prospectCallLogList.addAll(prospectCallLogSuccess);
			}
			 if (callbackLeadStatus.size() > 0) {
		 			List<String> dispositionList = new ArrayList<String>();
	 				dispositionList.add("CALLBACK");
	 				callbackPCSDTO = prospectCallSearchDTO;
	 				callbackPCSDTO.setDisposition(dispositionList);
	 				callbackPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
	 				callbackPCSDTO.setSubStatus(callbackLeadStatus);
	 				List<ProspectCallLog> prospectCallLogCallback = prospectCallLogRepositoryImpl
							.searchProspectCallByReport(callbackPCSDTO);
					prospectCallLogList.addAll(prospectCallLogCallback);
		 		 }
			if (failureLeadStatus.size() > 0) {
				List<String> dispositionList = new ArrayList<String>(1);
				dispositionList.add("FAILURE");
				failurePCSDTO = prospectCallSearchDTO;
				failurePCSDTO.setLeadStatus(null);
				failurePCSDTO.setDisposition(dispositionList);
				failurePCSDTO.setSubStatus(failureLeadStatus);
				successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
				List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl
						.searchProspectCallByReport(failurePCSDTO);
				prospectCallLogList.addAll(prospectCallLogFailure);
			}
		} else {
			successPCSDTO = prospectCallSearchDTO;
			successPCSDTO.setCampaignIds(prospectCallSearchDTO.getCampaignIds());
			List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl
					.searchProspectCallByReport(successPCSDTO);
			prospectCallLogList.addAll(prospectCallLogSuccess);
		}

		return prospectCallLogList;
	}

	private void sendEmailWithLeadFile(String campaignName, String FileName, ProspectCallSearchDTO prospectCallSearchDTO,
			File file, AmazonS3 s3client, Login login, Organization organization, String type) {
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		String message = FileName + " - Attached lead report file of the previous day.";
		boolean leadReportSent = false;
		String subject;
		String notification = "";
		if (type != null && !type.isEmpty()) {
			if (type.equalsIgnoreCase("single")) {
				subject = "Campaign Lead  Report for campaign " + campaignName;
				notification = campaignName;
			} else if (type.equalsIgnoreCase("multi")) {
				subject = "Organization Lead Report for " + organization.getName() + ". ";
				notification = organization.getName();
			} else if (type.equalsIgnoreCase("connectMulti")) {
				subject = "Standard Lead Report for " + organization.getName() + ". ";
				notification = organization.getName();
			} else if (type.equalsIgnoreCase("connectSingle")) {
				subject = "Standard Lead Report for " + campaignName + ". ";
				notification = campaignName;
			}

		}
		if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() == 1
				&& prospectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
			subject = " - DNC Report File Attached.";
		} else if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 1
				&& prospectCallSearchDTO.getSubStatus().contains("DNCL")) {
			subject = " - Failure Report File Attached.";
		} else {
			subject = " - Lead Report File Attached.";
		}
		
		String link = "";
		String linkref = "";
		String linkrefNotice = "";
		try {
			link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketNameReports, FileName  + ".xlsx", file);
			linkref = "<a href=\" " + link + " \">Download here</a>";
			linkrefNotice = "<" + link + ">";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (login!=null && login.getReportEmails()!=null && !login.getReportEmails().isEmpty()) {
			List<String> emails = login.getReportEmails();
			if (emails != null && emails.size() > 0) {
				for (String email : emails) {
					if (email != null && !email.isEmpty()) {
						// XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", FileName + subject, message, fileNames);
						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", FileName + subject, FileName + " - Lead report of the previous day Download Link :  " + linkref);
						logger.info("Client mapping lead report for the campaign [{}] sent  successfully to [{}] ",
								FileName, email);
						leadReportSent = true;
					}
				}
			}
		} else {
			// XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", FileName + subject, message, fileNames);
			XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", FileName + subject, FileName + " - Lead report of the previous day Download Link :  " + linkref);
		}
		try {
			String leadfileName = "LEADREPORTS" + SUFFIX + file;
//			s3client.putObject(new PutObjectRequest(bucketName, leadfileName, file)
//					.withCannedAcl(CannedAccessControlList.PublicRead));
			logger.info("Recording File uploaded successfully to s3 storage.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (!leadReportSent) {
			// XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com", FileName + subject, message, fileNames);
			XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com", FileName + subject, FileName + " - Lead report of the previous day Download Link :  " + linkref);
			logger.info("Client mapping lead report for the campaign [{}] sent  successfully to leadreports@xtaascorp.com",
					FileName);
		}
		PusherUtils.pushMessageToUser(login.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = notification + " : Lead report of the previous day Download here " + linkrefNotice;
		teamNoticeSerive.saveUserNotices(login.getId(), notice);
	}
	
	public List<String> getAgentIds(Login userLogin) {
		List<Team> teamsByQAs = teamRepository.findTeamsByQaId(userLogin.getId());
		List<String> supervisorIds = new ArrayList<>(); 
		for (Team team : teamsByQAs) {
			supervisorIds.add(team.getSupervisor().getResourceId());
		}
		List<Team> teamsBySupervisorIds = teamRepository.findByAdminSupervisorAndSupervisorId(supervisorIds);
		List<String> agentIds = new ArrayList<String>();
		for (Team team : teamsBySupervisorIds) {
			List<TeamResource> teamResources = team.getAgents();
			for (TeamResource teamResource : teamResources) {
				if (!agentIds.contains(teamResource.getResourceId())) {
					agentIds.add(teamResource.getResourceId());
				}
			}
		}
		return agentIds;
	}
}