package com.xtaas.service;

public interface TranscriptionInterpretationService {

    public String getInterpretationCommandTest (String transcription);

    public String getInterpretationCommand (String transcription);
    
}
