package com.xtaas.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import com.sendgrid.Client;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.DNCList.DNCTrigger;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.DNCListRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.SuccessCallLogRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.WebHookEventDTO;

@Service
public class SendGridServiceImpl implements SendGridService {

	private static final Logger logger = LoggerFactory.getLogger(SendGridServiceImpl.class);
	
	private String sendGridAPIKEY = "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";
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private ProspectCallLogService prospectCallLogService;
	
	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private InternalDNCListService internalDNCListService;
	
	@Autowired
	private DNCListRepository dncListRepository;
	
	@Autowired
	private SuccessCallLogRepository successCallLogRepository;
	
	@Override
	public void handleEmailNotificationEvent(List<WebHookEventDTO> webHookEvents) {
		for (WebHookEventDTO webHookEvent : webHookEvents) {
			try {
				if (webHookEvent != null && webHookEvent.getProspectCallId() != null) {
					ProspectCallLog pc = prospectCallLogRepository
							.findByProspectCallId(webHookEvent.getProspectCallId());
					if (pc != null && pc.getProspectCall().getSendGirdEmailEvents() != null) {
						if (webHookEvent.getEvent().equalsIgnoreCase("open")) {
							if (webHookEvent.getCallableEvent().equalsIgnoreCase("open")) {
								pc.getProspectCall().getSendGirdEmailEvents().setOpen(true);
								if (webHookEvent.getCampaignType() != null && webHookEvent.getCampaignType()
										.equalsIgnoreCase(CampaignTypes.LeadQualification.name())) {
									pc.getProspectCall().setDispositionStatus("SUCCESS");
									pc.getProspectCall().setSubStatus("SUBMITLEAD");
									pc.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
									pc.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
									pc.getProspectCall().setCallStartTime(new Date());
									pc.getProspectCall().setAgentId(pc.getProspectCall().getOrganizationId()+"_EMAIL_AGENT");
									pc.getProspectCall().setPartnerId(pc.getProspectCall().getOrganizationId());
									pc.getProspectCall().setSupervisorId(webHookEvent.getSupervisorId());
									SuccessCallLog successCallLogDB = successCallLogRepository.findByProspectCallId(pc.getProspectCall().getProspectCallId(),pc.getProspectCall().getOrganizationId());
									if(successCallLogDB == null) {
										SuccessCallLog successCallLog = new SuccessCallLog();
										successCallLog = successCallLog.toSuccessCallLog(pc);
										successCallLogRepository.save(successCallLog);
									}
								} else {
									pc.setStatus(ProspectCallStatus.QUEUED);
									pc.getProspectCall().setSubStatus("BUSY");
									pc.getProspectCall().setCallRetryCount(0);
								}
							} else {
								pc.getProspectCall().getSendGirdEmailEvents().setOpen(true);
							}
						} else if (webHookEvent.getEvent().equalsIgnoreCase("click")) {
							if (webHookEvent.getCallableEvent().equalsIgnoreCase("click") && pc != null
									&& pc.getProspectCall() != null
									&& pc.getProspectCall().getSendGirdEmailEvents() != null
									&& pc.getProspectCall().getSendGirdEmailEvents().isOpen()) {
								if (webHookEvent.getUrl().equals(webHookEvent.getClickUrl())) {
									pc.getProspectCall().getSendGirdEmailEvents().setClick(true);
									if (webHookEvent.getCampaignType() != null && webHookEvent.getCampaignType()
											.equalsIgnoreCase(CampaignTypes.LeadQualification.name())) {
										pc.getProspectCall().setDispositionStatus("SUCCESS");
										pc.getProspectCall().setSubStatus("SUBMITLEAD");
										pc.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
										pc.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
										pc.getProspectCall().setCallStartTime(new Date());
										pc.getProspectCall().setAgentId(pc.getProspectCall().getOrganizationId()+"_EMAIL_AGENT");
										pc.getProspectCall().setPartnerId(pc.getProspectCall().getOrganizationId());
										pc.getProspectCall().setSupervisorId(webHookEvent.getSupervisorId());
										SuccessCallLog successCallLogDB = successCallLogRepository.findByProspectCallId(pc.getProspectCall().getProspectCallId(),pc.getProspectCall().getOrganizationId());
										if(successCallLogDB == null) {
											SuccessCallLog successCallLog = new SuccessCallLog();
											successCallLog = successCallLog.toSuccessCallLog(pc);
											successCallLogRepository.save(successCallLog);
										}
									} else {
										pc.setStatus(ProspectCallStatus.QUEUED);
										pc.getProspectCall().setSubStatus("BUSY");
										pc.getProspectCall().setCallRetryCount(0);
									}
								} else if (webHookEvent.getUrl().equals(webHookEvent.getUnSubscribeUrl())) {
									pc.getProspectCall().getSendGirdEmailEvents().setUnsubscribe(true);
									pc.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
									pc.getProspectCall().setSubStatus("DNCL");
									pc.getProspectCall().setLeadStatus(null);
									pc.getProspectCall().setCallRetryCount(786123);
									pc.getProspectCall().setDispositionStatus("FAILURE");
									addProspectToDNC(webHookEvent);
								}
							} else {
								pc.getProspectCall().getSendGirdEmailEvents().setClick(true);
							}
						} else if (webHookEvent.getEvent().equalsIgnoreCase("unsubscribe")) {
							pc.getProspectCall().getSendGirdEmailEvents().setUnsubscribe(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("bounce")) {
							pc.getProspectCall().getSendGirdEmailEvents().setBounce(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("spam_report")) {
							pc.getProspectCall().getSendGirdEmailEvents().setSpam_report(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("processed")) {
							pc.getProspectCall().getSendGirdEmailEvents().setProcessed(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("delivered")) {
							pc.getProspectCall().getSendGirdEmailEvents().setDelivered(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("deferred")) {
							pc.getProspectCall().getSendGirdEmailEvents().setDeferred(true);
						} else if (webHookEvent.getEvent().equalsIgnoreCase("dropped")) {
							pc.getProspectCall().getSendGirdEmailEvents().setDropped(true);
						}
						try {
							prospectCallLogRepository.save(pc);
						}catch (OptimisticLockingFailureException e) {
							ProspectCallLog newObj = prospectCallLogRepository
									.findByProspectCallId(pc.getProspectCall().getProspectCallId());
							newObj.setProspectCall(pc.getProspectCall());
							newObj.setStatus(pc.getStatus());
							prospectCallLogRepository.save(newObj);
						}
						
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void sendSendGridEmail() {
		StringBuffer sBuffer = new StringBuffer();
		List<SupervisorStats> listFromDB = supervisorStatsRepository.findByTypeAndStatus("EmailJob","QUEUED");
		if (listFromDB != null && listFromDB.size() > 0) {
			for (SupervisorStats stats : listFromDB) {
				try {
					Campaign campaign = campaignRepository.findOneById(stats.getCampaignId());
					stats.setStatus("INPROCESS");
					supervisorStatsRepository.save(stats);
					if (campaign != null && campaign.getOrganizationId() != null) {
						EmailMessage campaignTemplate = campaign.getAssetEmailTemplate();
						if (campaignTemplate != null) {
							List<ProspectCallLog> listOfProspectCallLogDB = prospectCallLogRepository
									.findContactsToEmailSend(Arrays.asList(campaign.getId()));
							if (listOfProspectCallLogDB != null && listOfProspectCallLogDB.size() > 0) {

								for (ProspectCallLog prospectCallLog : listOfProspectCallLogDB) {
									try {
										String toEmail = prospectCallLog.getProspectCall().getProspect().getEmail();
										if (toEmail != null && campaignTemplate.getSubject() != null
												&& campaignTemplate.getMessage() != null) {
											EmailMessage message = new EmailMessage(Arrays.asList(toEmail),
													campaignTemplate.getSubject(), campaignTemplate.getMessage());
											if (campaignTemplate.getFromEmail() != null) {
												message.setFromEmail(campaignTemplate.getFromEmail());
											} else {
												message.setFromEmail("support+asset@xtaascorp.com");
											}
											if (campaign.isMdFiveSuppressionCheck()) {
												String md5Hex = DigestUtils.md5Hex(toEmail);
												boolean isEmailSuppressed = suppressionListService.isEmailSuppressed(
														md5Hex, campaign.getId(), campaign.getOrganizationId());
												if (!isEmailSuppressed) {
													if (!prospectCallLog.getProspectCall().getProspect().getIsEu()) {
														if(!checkDNCByNameAndNumber(prospectCallLog)) {
															sendEmail(message, prospectCallLog, campaign);
														}
													}
												}
											} else {
												boolean isEmailSuppressed = suppressionListService.isEmailSuppressed(
														toEmail, campaign.getId(), campaign.getOrganizationId());
												if (!isEmailSuppressed) {
													if (!prospectCallLog.getProspectCall().getProspect().getIsEu()) {
														if(!checkDNCByNameAndNumber(prospectCallLog)) {
															sendEmail(message, prospectCallLog, campaign);
														}
													}
												}
											}
										}
									} catch (Exception e) {
										sBuffer.append(prospectCallLog.getProspectCall().getProspectCallId() + "=======>"+e.getMessage()+"\n");
										logger.error(
												"sendSendGridEmail() : Error occurred while Sending email for prospect. Exception is : {} ", e.getMessage());
									}
								}
							}
						}
					}
					SupervisorStats newStats = supervisorStatsRepository.findByCampaignIdTypeAndStatus(stats.getType(),
							stats.getCampaignId(), "INPROCESS");
					newStats.setStatus("COMPLETED");
					if(!sBuffer.toString().isEmpty()) {
						newStats.setError(sBuffer.toString());
					}
					supervisorStatsRepository.save(newStats);
				} catch (Exception e) {
					logger.error(
							"sendSendGridEmail() : Error occurred while Sending email through SendGrid. Exception is : {} ", e.getMessage());
					SupervisorStats newStats = supervisorStatsRepository.findByCampaignIdTypeAndStatus(stats.getType(),
							stats.getCampaignId(), "INPROCESS");
					newStats.setStatus("ERROR");
					newStats.setError(e.getMessage());
					supervisorStatsRepository.save(newStats);
				}
			}
		}
	}
	
	public void sendEmail(EmailMessage message,ProspectCallLog prospectCallLog,Campaign campaign) {
		String msgBody = null;
		Email from = new Email(message.getFromEmail());
		from.setName(message.getFromName());
		String subject = message.getSubject();
		msgBody = message.getMessage();
		msgBody = StringUtils.replace(msgBody, "[PROSPECT_NAME]", prospectCallLog.getProspectCall().getProspect().getFirstName());
		Content content = new Content("text/html",msgBody);

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Personalization personalization = new Personalization();
		for (int i = 0; i < message.getTo().size(); i++) {
			if (i == 0) {
				personalization.addTo(new Email(message.getTo().get(i)));
			} else {
				personalization.addCc(new Email(message.getTo().get(i)));
			}
		}
		personalization.addCustomArg("campaignId", prospectCallLog.getProspectCall().getCampaignId());
		personalization.addCustomArg("prospectCallId", prospectCallLog.getProspectCall().getProspectCallId());
		personalization.addCustomArg("callableEvent", campaign.getCallableEvent());
		personalization.addCustomArg("clickUrl", campaign.getAssetEmailTemplate().getClickUrl());
		personalization.addCustomArg("unSubscribeUrl", campaign.getAssetEmailTemplate().getUnSubscribeUrl());
		personalization.addCustomArg("campaignType", campaign.getType().name());
		personalization.addCustomArg("supervisorId", campaign.getTeam().getSupervisorId());
		
		Mail mail = new Mail();
		mail.setFrom(from);
		mail.addContent(content);
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
			prospectCallLog.getProspectCall().setCallRetryCount(786123);
			prospectCallLogRepository.save(prospectCallLog);
			
		} catch (Exception ex) {
			logger.error(
					"sendEmailSendGrid() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}

	private void addProspectToDNC(WebHookEventDTO webHookEvent) {
		if (webHookEvent.getCampaignId() != null && webHookEvent.getProspectCallId() != null) {
			Campaign campaign = campaignRepository.findOneById(webHookEvent.getCampaignId());
			ProspectCallLog prospectCallLog = prospectCallLogRepository
					.getProspectByCIDAndPCID(webHookEvent.getCampaignId(), webHookEvent.getProspectCallId());
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getEmail() != null && prospectCallLog
							.getProspectCall().getProspect().getEmail().equalsIgnoreCase(webHookEvent.getEmail())) {
				String phoneNumber = prospectCallLog.getProspectCall().getProspect().getPhone();
				String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
				DNCList dncNumber = new DNCList(normalizedPhoneNumber,
						prospectCallLog.getProspectCall().getProspect().getFirstName(),
						prospectCallLog.getProspectCall().getProspect().getLastName(), DNCTrigger.DIRECT);
				dncNumber.setNote("Email Unsubscribe");
				IDNCNumberDTO idncNumberDTO = new IDNCNumberDTO(dncNumber);
				idncNumberDTO.setPhoneNumber(normalizedPhoneNumber);
				idncNumberDTO.setNote("Email Unsubscribe");
				idncNumberDTO.setTrigger(DNCTrigger.DIRECT);
				idncNumberDTO.setPartnerId(campaign.getOrganizationId());
				idncNumberDTO.setCampaignId(prospectCallLog.getProspectCall().getCampaignId());
				idncNumberDTO.setProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
				idncNumberDTO.setRecordingUrl("");
				idncNumberDTO.setRecordingUrlAws("");
				idncNumberDTO.setFirstName(prospectCallLog.getProspectCall().getProspect().getFirstName());
				idncNumberDTO.setLastName(prospectCallLog.getProspectCall().getProspect().getLastName());
				idncNumberDTO.setDncLevel(XtaasConstants.DNC_PROSPECT);
				internalDNCListService.addNumber(idncNumberDTO);
			}
		}
	}
	
	private boolean checkDNCByNameAndNumber(ProspectCallLog pc) {
		boolean isDNC = false;
		List<DNCList> dncDBList = dncListRepository.findPhoneNumber(pc.getProspectCall().getProspect().getPhone(),
				pc.getProspectCall().getProspect().getFirstName(),pc.getProspectCall().getProspect().getLastName());
		if(dncDBList != null && dncDBList.size() > 0) {
			isDNC = true;
		}
		return isDNC;
	}
}
