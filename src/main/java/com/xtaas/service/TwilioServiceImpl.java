package com.xtaas.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Recording;
import com.twilio.sdk.verbs.Conference;
import com.twilio.sdk.verbs.Dial;
import com.twilio.sdk.verbs.Enqueue;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Number;
import com.twilio.sdk.verbs.Play;
import com.twilio.sdk.verbs.Redirect;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.Sip;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;
import com.twilio.sdk.verbs.Conference.Record;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.OutboundNumberService;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasConstants.LEAD_STATUS;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.CallStatusDTO.CallState;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.TwilioCallLogFilterRequestDTO;

@Service
public class TwilioServiceImpl implements TwilioService {

	private final Logger logger = LoggerFactory.getLogger(TwilioServiceImpl.class);
	private static final String COUNTRYDIALCODE_US = "+1";
	private static final String MASTER_ACCOUNT_SID = "ACab4d611aa1465a246cb58290ba930a5c";
	private static final String MASTER_AUTH_TOKEN = "8096913a4808bc8287d5a7b0b8332d22";
	private static final String PROD_ACCOUNT_SID = "AC2a76c665eb3c39046214a9a6a2bd3935";
	private static final String PROD_AUTH_TOKEN = "2fc001a00ad725a9616bf4a6d5132ffc";
	private static final String DMS_ACCOUNT_SID = "AC517a9586ed1afe81d4fda44f90778d53";
	private static final String DMS_AUTH_TOKEN = "e9eda3a5a24028435e472992be403dc1";
	private static final String QA_ACCOUNT_SID = "ACbf52b47c38d9b03c72360c748b90309e";
	private static final String QA_AUTH_TOKEN = "25bb8b444f7befe0dfc9effd9a6c28bd";
	private static final String DM_PROFILE = "DEMANDSHORE";
	private static final String MASTER_PROFILE = "MASTER";
	private static final String PROD_PROFILE = "PROD";
	private static final String QA_PROFILE = "QA";

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private UserService userService;

	@Autowired
	private OutboundNumberService outboundNumberService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private ProspectCallLogService prospectCallLogService;
	
	@Autowired
	private CampaignRepository campaignRepository;

	private final String RECORDING_CALLBACK_URL = "/spr/dialer/confendevent?pcid=";

	private final String RECORDING_CALLBACK_ACTION = "recordingStatusCallback";

	private final String RECORDING_CALLBACK_EVENT = "recordingStatusCallbackEvent";

	private final String RECORDING_CALLBACK_EVENT_TYPE = "completed";

	@Override
	public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response) {
		// This is a twilio callback
		String toNumber = request.getParameter("To");
		String fromNumber = request.getParameter("From");
		String requestFromDialer = request.getParameter("dialer");
		String dialerMode = request.getParameter("dialerMode");
		String agentType = request.getParameter("agentType");
		String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
		logger.debug("==========> TwilioServiceImpl - handleVoiceCall - From: [{}] and To: [{}] <==========",
				fromNumber, toNumber);
		String xmlString = "";
		if (dialerMode != null && (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode))
				|| DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode))
				|| DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode))
				|| (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode)))
						&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
			xmlString = handleManualAgentCall(request, response);
		} else if (fromNumber.startsWith("client:") && isSupervisorJoinRequest == null) {
			xmlString = handleAutoAgentCall(request, response);
		} else {
			String answeredBy = request.getParameter("AnsweredBy");
			String machineAnsweredDetection = request.getParameter("MachineAnsweredDetection");
			if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
				/*
				 * phone is agentId in case of supervisorJoinRequest. NOTE : this is not used so
				 * will be removed.
				 */
				String agentId = request.getParameter("pn");
				AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
				DialerMode agentsCampaignDialerMode = agentCall.getCurrentCampaign().getDialerMode();
				xmlString = addSupervisorInConference(request, response, agentsCampaignDialerMode);
			} else if (answeredBy != null && answeredBy.equalsIgnoreCase("machine_start")
					&& machineAnsweredDetection != null && machineAnsweredDetection.equalsIgnoreCase("true")) {
				xmlString = handleAnsweringMachine(request, response);
			} else if (requestFromDialer != null) {
				xmlString = handleDialerProspectCall(request, response);
			}
		}
		return xmlString;
	}

	/**
	 * This will be called when agent dial prospect's phone manually from agent
	 * screen.
	 */
	private String handleManualAgentCall(HttpServletRequest request, HttpServletResponse response) {
		try {
			// pn is prospect phone number
			String toNumber = request.getParameter("pn");
			String campaignId = request.getParameter("campaignId");
			String prospectCallId = request.getParameter("pcid");
			String agentId = request.getParameter("agentId"); // play ring tone until prospect answer call
			// callerId is twilio outbound number
			String fromNumber = request.getParameter("callerId");
//			String fname = request.getParameter("fname");
//			String lname = request.getParameter("lname");
//			String domain = request.getParameter("domain");
			Campaign campaign = campaignService.getCampaign(campaignId);
			if (prospectCallId == null) {
				logger.info("***** prospectCallId is NULL for campaignId [{}] & phone [{}] *****", campaignId,
						toNumber);
			}
			ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
			ProspectCall prospectCall = null;
			Prospect prospect = null;
			if (prospectCallLog != null) {
				prospectCall = prospectCallLog.getProspectCall();
				prospect = prospectCallLog.getProspectCall().getProspect();
			}
			if (fromNumber == null) {
				logger.info("***** fromNumber is NULL for campaignId [{}] & phone [{}] *****", campaignId, toNumber);
				campaign = campaignService.getCampaign(prospectCall.getCampaignId());
				fromNumber = outboundNumberService.getOutboundNumber(prospect.getPhone(),
						prospectCall.getCallRetryCount(), prospect.getCountry(), campaign.getOrganizationId());
			}
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + prospectCallId + "&amp;agentid=" + agentId
					+ "&amp;campaignId=" + campaignId; // + "&amp;pn=" + toNumber;
			String recordingCallbackURL = RECORDING_CALLBACK_URL + prospectCallId;
			StringBuilder statusCallbackUrl = new StringBuilder(
					ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/twilio/call/status");
			try {
				statusCallbackUrl.append("?agentId=").append(URLEncoder.encode(agentId, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				logger.error("==========> TwilioServiceImpl - Error occurred in handleManualAgentCall <==========");
				logger.error("Expection : ", e);
			}
			TwiMLResponse twiml = new TwiMLResponse();

			// check DNC while initiating manual call
//			if (!checkLocalDNCList(campaign, new ProspectCallDTO(prospectCall), toNumber, agentId, fname, lname,
//					domain)) {
//				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "prospect-dnc");
//				return twiml.toXML();
//			}

			Dial dial = new Dial();
			dial.setCallerId(fromNumber);
			dial.setRecord("record-from-answer");
			dial.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
			dial.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			dial.setAction(actionUrl);

			String countryName = "United States";
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getCountry() != null) {
				countryName = prospectCallLog.getProspectCall().getProspect().getCountry();
			}
			if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
				String formattedToNumber = createThinQNumber(toNumber, countryName);
				Sip sip = new Sip(formattedToNumber);
				sip.setStatusCallbackMethod("POST");
				sip.setStatusCallback(statusCallbackUrl.toString());
				dial.append(sip);
			} else if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Telnyx")) {
				String formattedToNumber = createTelnyxNumber(toNumber, countryName);
				Sip sip = new Sip(formattedToNumber);
				sip.setStatusCallbackMethod("POST");
				sip.setStatusCallback(statusCallbackUrl.toString());
				dial.append(sip);
			} else {
				Number number = new Number(toNumber);
				number.setStatusCallbackMethod("POST");
				number.setStatusCallback(statusCallbackUrl.toString());
				dial.append(number);
			}
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in handleManualAgentCall <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}
	
	private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getThinQDomainUrl());
		thinQNumber.append("?X-account-id=").append(ApplicationEnvironmentPropertyUtils.getThinQId())
				.append("&amp;X-account-token=").append(ApplicationEnvironmentPropertyUtils.getThinQToken());
		return thinQNumber.toString();
	}

	private String createTelnyxNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder telnyxNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getTelnyxDomainUrl());
		telnyxNumber.append("?X-Telnyx-Token=").append(ApplicationEnvironmentPropertyUtils.getTelnyxToken());
		return telnyxNumber.toString();
	}

	/*
	 * SIP forwarding to ThinQ from Twilio Rest Client seems to reject number w/o
	 * country code and other formatting. This method should strip all formatting
	 * and add country code. For now it will check whether the country is US and add
	 * 1 else return null. Assumption. given number is a valid US number. doesn't
	 * validate.
	 */
	private String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US + normalizedPhoneNumber;
		} else {
			formattedPhoneNumber = "+" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	@Async
	private String dialProspect(String fromNumber, String toNumber, String prospectCallId, String agentId,
			String campaignId) {
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		CallFactory callFactory = account.getCallFactory();
		Map<String, String> callParams = new HashMap<String, String>();
		Campaign campaign = campaignService.getCampaign(campaignId);
		ProspectCallLog prospectCallLog = prospectCallLogRepository.getProspectByCIDAndPCID(campaignId, prospectCallId);
		String countryName = "United States";
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
				&& prospectCallLog.getProspectCall().getProspect() != null
				&& prospectCallLog.getProspectCall().getProspect().getCountry() != null) {
			countryName = prospectCallLog.getProspectCall().getProspect().getCountry();
		}
		if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
			String formattedToNumber = createThinQNumber(toNumber, countryName);
			// To=sip:+14085052222@wap.thinq.com?X-account-id=<id>&X-account-token=<access-token>
			callParams.put("To", formattedToNumber);
		} else if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Telnyx")) {
			String formattedToNumber = createTelnyxNumber(toNumber, countryName);
			// To=sip:+14085052222@sip.telnyx.com?X-Telnyx-Token=<access-token>
			callParams.put("To", formattedToNumber);
		} else {
			callParams.put("To", toNumber); // lead's phone number
		}
		callParams.put("From", fromNumber); // Twilio number
		try {
			StringBuilder dialXMLUrl = new StringBuilder(
					ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/twilio/dial/prospect");
			dialXMLUrl.append("?agentId=").append(URLEncoder.encode(agentId, "UTF-8"));
			dialXMLUrl.append("&pcid=").append(prospectCallId);
			dialXMLUrl.append("&recordCall=").append("true");
			callParams.put("Url", dialXMLUrl.toString());

			StringBuilder statusCallbackUrl = new StringBuilder(
					ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/twilio/call/status");
			statusCallbackUrl.append("?agentId=").append(URLEncoder.encode(agentId, "UTF-8"));
			callParams.put("Method", "POST");
			callParams.put("StatusCallback", statusCallbackUrl.toString());
			callParams.put("StatusCallbackEvent", "completed");
			int maxCallRingingDurationInSecs = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_MANUALDIAL.name(), 30);
			callParams.put("Timeout", Integer.toString(maxCallRingingDurationInSecs));
		} catch (Exception e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in dialProspect <==========");
			logger.error("Expection : ", e);
		}
		// Make the call
		Call call;
		try {
			call = callFactory.create(callParams);
			return call.getSid();
		} catch (Exception e) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "invalid-phone");
			logger.error("==========> TwilioServiceImpl - Error occurred in dialProspect <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called in POWER or HYBRID (agentType - AUTO) mode when agent
	 * goes online.
	 */
	private String handleAutoAgentCall(HttpServletRequest request, HttpServletResponse response) {
		try {
			// To is agent id
			String toNumber = request.getParameter("To");
			String campaignId = request.getParameter("campaignId");
			String prospectCallId = request.getParameter("pcid");
			TwiMLResponse twiml = new TwiMLResponse();
			Dial dial = new Dial();
			dial.setCallerId(null);
			dial.setAction("/spr/dialer/savecallmetrics?pcid=" + prospectCallId + "&amp;agentid=" + toNumber
					+ "&amp;campaignId=" + campaignId);
			Conference conference = new Conference(toNumber);
			conference.set("region", "sg1");
			conference.setStartConferenceOnEnter(true);
			conference.setEndConferenceOnExit(true);
			conference.setBeep(true);
			conference.setMuted(false);
			conference.setWaitUrl("");
			conference.setRecord(Record.DO_NOT_RECORD);
			dial.append(conference);
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in handleAutoAgentCall <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when supervisor request to join call.
	 */
	private String addSupervisorInConference(HttpServletRequest request, HttpServletResponse response,
			DialerMode dialerMode) {
		try {
			String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
			String prospectInteractionSessionId = request.getParameter("pisid");
			String prospectCallId = request.getParameter("pcid");
			// pn is agentId
			String confName = request.getParameter("pn");
			TwiMLResponse twiml = new TwiMLResponse();
			String actionUrl = "/spr/dialer/savecallmetrics?supervisorjoin=" + isSupervisorJoinRequest + "&amp;pcid="
					+ prospectCallId + "&amp;pisid=" + prospectInteractionSessionId;

			Dial dial = new Dial();
			dial.setAction(actionUrl);
			Conference conference = new Conference(confName);
			conference.setEndConferenceOnExit(false);
			conference.setBeep(false);
			conference.setMuted(true);
			conference.setWaitUrl("");
			conference.setRecord(Record.RECORD_FROM_START);
			dial.append(conference);
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in addSupervisorInConference <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when answering machine feature is ON. It will hangup call
	 * then redirect to URL to handle call details.
	 */
	private String handleAnsweringMachine(HttpServletRequest request, HttpServletResponse response) {
		try {
			String prospectCallId = request.getParameter("pcid");
			String campaignId = request.getParameter("campaignId");
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + prospectCallId;
			if (campaignId != null) {
				actionUrl = actionUrl + "&amp;campaignId=" + campaignId;
			}
			TwiMLResponse twiml = new TwiMLResponse();
			Redirect redirect = new Redirect(actionUrl);
			redirect.setMethod("POST");
			twiml.append(redirect);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in handleAnsweringMachine <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when call is dialed from dialer application.
	 */
	private String handleDialerProspectCall(HttpServletRequest request, HttpServletResponse response) {
		/*
		 * This is twilio callback. 1)This will be called when prospects phone is dialed
		 * from dialer. 2) First it will search available agent, if found then add
		 * prospect in agent's conference otherwise play abandoned message.
		 */
		String callSid = request.getParameter("CallSid");
		String campaignId = request.getParameter("campaignId");
		String prospectCallId = request.getParameter("pcid");
		String callerId = request.getParameter("callerId");
		AgentCall allocatedAgent = null;
		String prospectCallAsJson = null;
		ProspectCall prospectCall = null;
		// find the available agent to connect with the prospect
		logger.info("==========> Finding available agent for CallSid [{}] <==========", callSid);
		allocatedAgent = agentRegistrationService.allocateAgent(callSid);
		if (allocatedAgent != null) {
			prospectCall = allocatedAgent.getProspectCall();
			try {
				prospectCallAsJson = JSONUtils.toJson(prospectCall);
				PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
						prospectCallAsJson);
				return addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId);
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("==========> Allocated Agent [{}] to ProspectCallId [{}]. <==========",
					allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
		} else {
			try {
				// wait for a second and attempt again to find an agent
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("==========> TwilioServiceImpl - Error occurred in handleDialerProspectCall <==========");
				logger.error("Expection : {} ", e);
			}
			// re-attempting to find the available agent
			allocatedAgent = agentRegistrationService.allocateAgent(callSid);
			if (allocatedAgent == null) {
				// still no agent available then play the abandon message
				logger.info("==========> No Agent available for CallSid  [{}] <==========", callSid);
				return playAbandonMessage(callSid, prospectCallId, campaignId, callerId);
			} else {
				prospectCall = allocatedAgent.getProspectCall();
				try {
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
							prospectCallAsJson);
					return addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId);
				} catch (IOException e) {
					e.printStackTrace();
				}
				logger.info("==========> Allocated Agent [{}] to ProspectCallId [{}] <==========",
						allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
			}
		}
		return "";
	}

	/**
	 * This will be add prospect into agent's conference.
	 */
	private String addProspectInConference(String agentId, String pcid, String callerId) {
		// This will add prospect in agent's conference.
		try {
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + pcid + "&amp;agentid=" + agentId;
			String recordingCallbackURL = RECORDING_CALLBACK_URL + pcid;
			TwiMLResponse twiml = new TwiMLResponse();
			Dial dial = new Dial();
			dial.setCallerId(callerId);
			dial.setAction(actionUrl);
			dial.setRecord("record-from-ringing");
			dial.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
			dial.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			Conference conference = new Conference(agentId);
			conference.setStartConferenceOnEnter(true);
			conference.setEndConferenceOnExit(false);
			conference.setBeep(true);
			conference.setMuted(false);
			conference.setWaitUrl("");
			// conference.setRecord(Record.RECORD_FROM_START);
			// conference.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
			// conference.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			dial.append(conference);
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in addProspectInConference <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * Play abandon message if agent is not available.
	 */
	private String playAbandonMessage(String callSid, String prospectCallId, String campaignId, String callerId) {
		// This will play abandoned message - No agent available.
		try {
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + prospectCallId;
			if (campaignId != null) {
				actionUrl = actionUrl + "&amp;campaignId=" + campaignId;
			}
			if (callSid != null) {
				actionUrl = actionUrl + "&amp;callSid=" + callSid;
			}
			actionUrl = actionUrl + "&amp;abandoned=1";
			TwiMLResponse twiml = new TwiMLResponse();
			Say say = new Say(XtaasConstants.TWILIO_ABANDON_OPTOUT_MSG);
			Gather gather = new Gather();
			gather.setTimeout(10);
			gather.setNumDigits(1);
			gather.setMethod("POST");
			gather.setAction(actionUrl);
			gather.append(say);
			Redirect redirect = new Redirect(actionUrl);
			twiml.append(gather);
			twiml.append(redirect);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in playAbandonMessage <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * Play wait music in the conference till prospect connect in the conference.
	 */
	@Override
	public String handleConferenceWait(HttpServletRequest request, HttpServletResponse response) {
		try {
			TwiMLResponse twiml = new TwiMLResponse();
			String ringingPhoneUrl = "/sounds/ringing_phone.mp3";
			Play play = new Play(ringingPhoneUrl);
			play.setLoop(0);
			twiml.append(play);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in conferenceWait <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	@Override
	public String handleProspectCallPreviewMode(HttpServletRequest request, HttpServletResponse response) {
		final HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			logger.debug(param + " = " + request.getParameter(param));
			paramMap.put(param, request.getParameter(param));
		}

		final String agentId = paramMap.get("agentId");
		final String prospectCallId = paramMap.get("pcid");
		try {
			CallStatusDTO callStatusDTO = new CallStatusDTO();
			callStatusDTO.setCallStatus(CallState.CONNECTED);
			callStatusDTO.setCallStartTime(new Date());
			agentService.changeCallStatus(agentId, callStatusDTO);
		} catch (Exception e) {
			logger.error(
					"==========> TwilioServiceImpl - Error occurred in handleProspectCallPreviewMode for ProspectCallId [{}] <==========",
					prospectCallId);
			logger.error("Expection : ", e);
		}

		try {
			TwiMLResponse twiml = new TwiMLResponse();
			String msg = request.getParameter("msg");
			if (msg != null) {
				twiml.append(new Say(msg));
			}
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + prospectCallId + "&amp;agentid=" + agentId;
			String recordingCallbackURL = RECORDING_CALLBACK_URL + prospectCallId;
			Dial dial = new Dial();
			dial.setAction(actionUrl);
			if (paramMap.get("recordCall") != null && paramMap.get("recordCall").equals("true")) {
				dial.setRecord("record-from-ringing");
				dial.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
				dial.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			}
			// dial.setRecord("record-from-ringing");
			// dial.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
			// dial.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			String conferenceName = agentId;
			Conference conference = new Conference(conferenceName);
			conference.setWaitUrl("");
			conference.setBeep(true);
			conference.setEndConferenceOnExit(false);
			// conference.setRecord(Record.RECORD_FROM_START);
			// conference.set(RECORDING_CALLBACK_ACTION, recordingCallbackURL);
			// conference.set(RECORDING_CALLBACK_EVENT, RECORDING_CALLBACK_EVENT_TYPE);
			dial.append(conference);
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in handleProspectCallPreviewMode <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when call is disconnected. It is used to send pusher
	 * message to agent to notify call status like failed, busy, no answer etc.
	 */
	@Override
	public String handleCallStatus(HttpServletRequest request, HttpServletResponse response) {
		try {
			String agentId = request.getParameter("agentId");
			String callStatus = request.getParameter("CallStatus");
			PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, callStatus);
			TwiMLResponse twiml = new TwiMLResponse();
			Say say = new Say("");
			twiml.append(say);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in handleProspectDisconnect <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when agent click on hold button to hold the call.
	 */
	@Override
	public String holdCall(HttpServletRequest request, HttpServletResponse response) {
		try {
			String queueId = request.getParameter("qid");
			Enqueue enqueue = new Enqueue(queueId);
			TwiMLResponse twiml = new TwiMLResponse();
			twiml.append(enqueue);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in holdCall <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	/**
	 * This will be called when agent click on unhold button to unhold the call.
	 */
	@Override
	public String unHoldCall(HttpServletRequest request, HttpServletResponse response) {
		try {
			String agentId = request.getParameter("agentId");
			String prospectCallId = request.getParameter("pcid");
			String conferenceName = agentId;
			String actionUrl = "/spr/dialer/savecallmetrics?pcid=" + prospectCallId + "&amp;agentid=" + agentId;
			Dial dial = new Dial();
			dial.setAction(actionUrl);
			Conference conference = new Conference(conferenceName);
			conference.setWaitUrl("");
			conference.setBeep(false);
			conference.setEndConferenceOnExit(false);
			dial.append(conference);
			TwiMLResponse twiml = new TwiMLResponse();
			twiml.append(dial);
			return twiml.toXML();
		} catch (TwiMLException e) {
			logger.error("==========> TwilioServiceImpl - Error occurred in unHoldCall <==========");
			logger.error("Expection : ", e);
		}
		return "";
	}

	@Override
	public String dialProspectPhone(String prospectCallId) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
		Campaign campaign = null;
		ProspectCall prospectCall = null;
		Prospect prospect = null;
		if (prospectCallLog != null) {
			prospectCall = prospectCallLog.getProspectCall();
			prospect = prospectCallLog.getProspectCall().getProspect();
			campaign = campaignService.getCampaign(prospectCall.getCampaignId());
			String fromNumber = outboundNumberService.getOutboundNumber(prospect.getPhone(),
					prospectCall.getCallRetryCount(), prospect.getCountry(), campaign.getOrganizationId());
			String callSid = dialProspect(fromNumber, prospect.getPhone(), prospectCall.getProspectCallId(),
					userLogin.getId(), campaign.getId());
			return callSid;
		} else {
			logger.error("Prospect not found.");
			return "";
		}
	}

	@Override
	public String tagLeadForNumber(TwilioCallLogFilterRequestDTO filterDTO) {
		logger.info("tagLeadForNumber(): " + filterDTO.getFirstName(), filterDTO.getLastName(), filterDTO.getPhoneNumber());
		boolean isRecordingFound = false;
		String response = null;
		TwilioRestClient client = null;
		Account account = null;
		ProspectCallLog prospectCallLog = null;
		// If ProspectCallId is defined in the TwilioCallLogFilterRequestDTO object.
		if (filterDTO.getProspectCallId() != null) {
			prospectCallLog = prospectCallLogRepository.findByProspectCallId(filterDTO.getProspectCallId());
			if (prospectCallLog == null) {
				throw new IllegalArgumentException("Propsect does not exist in the system.");
			}
			Campaign campaign = campaignRepository.findOneById(prospectCallLog.getProspectCall().getCampaignId());
			if (campaign == null) {
				throw new IllegalArgumentException("Campaign does not exist in the system.");
			}
		} else {
			Campaign campaign = campaignRepository.findByExactName(filterDTO.getCampaignName());
			if (campaign != null) {
				if (!filterDTO.getPhoneNumber().startsWith("+")) {
					filterDTO.setPhoneNumber("+" + filterDTO.getPhoneNumber());
				}
				prospectCallLog = prospectCallLogRepository.findProspectForLeadTag(campaign.getId(),filterDTO.getFirstName(), filterDTO.getLastName(), filterDTO.getPhoneNumber());
				if (prospectCallLog == null) {
					throw new IllegalArgumentException("Propsect does not exist in the system.");
				}
			} else {
				throw new IllegalArgumentException("Campaign does not exist in the system.");
			}
		}
		
		if (filterDTO.getProfile() != null && !filterDTO.getProfile().isEmpty()) {
			if (filterDTO.getProfile().equalsIgnoreCase(MASTER_PROFILE)) {
				client = new TwilioRestClient(MASTER_ACCOUNT_SID, MASTER_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(PROD_PROFILE)) {
				client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(DM_PROFILE)) {
				client = new TwilioRestClient(DMS_ACCOUNT_SID, DMS_AUTH_TOKEN);
				account = client.getAccount();
			} else if (filterDTO.getProfile().equalsIgnoreCase(QA_PROFILE)) {
				client = new TwilioRestClient(QA_ACCOUNT_SID, QA_AUTH_TOKEN);
				account = client.getAccount();
			}
		} else {
			client = new TwilioRestClient(PROD_ACCOUNT_SID, PROD_AUTH_TOKEN);
			account = client.getAccount();
		}
		
		if (prospectCallLog != null) {
			isRecordingFound = getCallDetails(account, prospectCallLog, isRecordingFound);	
		}
		
		if (isRecordingFound) {
			response = "Prospect updated sucessfully.";
			logger.info("tagLeadForNumber() : Prospect updated sucessfully." + prospectCallLog.getProspectCall().getProspectCallId());
		} else {
			response = "Prospect not updated.";
		}
		return response;
	}
	
	private boolean getCallDetails(Account account, ProspectCallLog prospectCallLog,
			boolean isRecordingFound) {
		logger.info("getCallDetails() : start");
		try {
			Call call = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
			if (call != null) {
				for (Recording recording : call.getRecordings()) {
					if (recording.getProperty("uri") != null && !recording.getProperty("uri").isEmpty()) {
						String recordingUrl = "https://api.twilio.com"
								+ StringUtils.replace(recording.getProperty("uri"), ".json", "");
						if (prospectCallLog.getProspectCall().getTwilioCallSid() != null && prospectCallLog
								.getProspectCall().getTwilioCallSid().equalsIgnoreCase(call.getSid())) {
							isRecordingFound = true;
							prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
							int lastIndex = recordingUrl.lastIndexOf("/");
							String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
							String rfileName = fileName + ".wav";
							String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
							prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
							prospectCallLog.getProspectCall().setCallStartTime(call.getStartTime());
							prospectCallLog.setStatus(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE);
							prospectCallLog.getProspectCall().setDispositionStatus(DispositionType.SUCCESS.name());
							prospectCallLog.getProspectCall().setSubStatus("SUBMITLEAD");
							prospectCallLog.getProspectCall().setLeadStatus(LEAD_STATUS.NOT_SCORED.name());
							prospectCallLogService.save(prospectCallLog, true);
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured while fetching twilio details." + e);
		}
		return isRecordingFound;
	}

	private boolean checkLocalDNCList(Campaign campaign, ProspectCallDTO prospectCall, String toNumber, String agentId,
			String fname, String lname, String domain) {
		boolean isUSorCanada = false;
		if (prospectCall.getProspect().getCountry() != null
				&& (prospectCall.getProspect().getCountry().equalsIgnoreCase("USA")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("UNITED STATES")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("US")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CANADA")
						|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CA"))) {
			isUSorCanada = true;
		}
		String phone = toNumber.replaceAll("[^\\d]", "");
		boolean localDNCListCheckPassed = prospectCallLogService.checkLocalDNCList(fname, lname, phone, domain,
				isUSorCanada, campaign.getOrganizationId(), prospectCall, null);
		return localDNCListCheckPassed;
	}

}
