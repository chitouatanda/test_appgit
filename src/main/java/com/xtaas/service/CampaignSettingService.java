package com.xtaas.service;

import javax.servlet.http.HttpServletRequest;

import com.xtaas.web.dto.CampaignSettingDTO;

public interface CampaignSettingService {

	public void updateCampaignSettings(String campaignId, CampaignSettingDTO campaignSettingDTO,
			HttpServletRequest httpServletRequest);

}