package com.xtaas.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;

@Service
public class DeactivateAgentServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DeactivateAgentServiceImpl.class);

	private static final String PRIMARY_CONTACT = "Primary Contact";

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	AgentActivityLogRepository agentActivityLogRepository;

	@Autowired
	private PropertyService propertyService;

	/**
	 * TODO :- Need to Refactor all the methods in this file. Variable names should
	 * be given proper.
	 */

	public void deActivateAgents() {
		Date startDate = null;
		Date endDate = null;
		long agentAbsentDays = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.AGENT_ABSENT_DAYS.name(), 2);
		List<String> orgNames = getPartnerAndInternalOrganizationNames();
		List<DeActivateAgentDTO> teamAgents = getOrganizationAgents(orgNames);
		List<DeActivateAgentDTO> activeAgentIds = getActiveAgents(teamAgents);
		Calendar startDateCalendar = XtaasDateUtils.getPreviousDate(Calendar.getInstance(), agentAbsentDays);
		startDate = startDateCalendar.getTime();
		Calendar endDateCalendar = XtaasDateUtils.getNextDateForGivenDays(startDateCalendar, agentAbsentDays);
		endDate = endDateCalendar.getTime();
		checkAgentActivityForLastDays(activeAgentIds, startDate, endDate);
	}

	private List<DeActivateAgentDTO> getActiveAgents(List<DeActivateAgentDTO> agents) {
		List<DeActivateAgentDTO> gh = new ArrayList<DeActivateAgentDTO>();
		List<String> agList = agents.stream().map(DeActivateAgentDTO::getAgentId).collect(Collectors.toList());
		List<AgentSearchResult> activeAgents = agentRepository.findByIdsAndByStatus(agList);
		for (AgentSearchResult agentSearchResult : activeAgents) {
			for (DeActivateAgentDTO agentSearch : agents) {
				if (agentSearch.getAgentId().equalsIgnoreCase(agentSearchResult.getId())) {
					if (!gh.contains(agentSearch)) {
						gh.add(agentSearch);
					}
				}
			}
		}
		return gh;
	}

	private List<String> getPartnerAndInternalOrganizationNames() {
		List<String> orgLevels = new ArrayList<String>();
		List<String> orgs = new ArrayList<String>();
		orgLevels.add("PARTNER");
//		orgLevels.add("INTERNAL");
		List<Organization> organizations = organizationRepository.findByOrganizationLevel(orgLevels);
		if (organizations != null && organizations.size() > 0) {
			orgs = organizations.stream().map(Organization::getId).collect(Collectors.toList());
		}
		return orgs;
	}

	private List<DeActivateAgentDTO> getOrganizationAgents(List<String> orgs) {
		List<DeActivateAgentDTO> agents = new ArrayList<>();
		if (orgs != null && orgs.size() > 0) {
			List<Team> teams = teamRepository.findInternalAndPartnerTeams(orgs);
			if (teams != null && teams.size() > 0) {
				for (Team team : teams) {
					for (TeamResource agent : team.getAgents()) {
						if (!agents.contains(agent.getResourceId())) {
							DeActivateAgentDTO deActivateAgent = new DeActivateAgentDTO();
							deActivateAgent.setAgentId(agent.getResourceId());
							deActivateAgent.setPartnerId(team.getPartnerId());
							deActivateAgent.setTeamName(team.getName());
							agents.add(deActivateAgent);
						}
					}
				}
			}
		}
		return agents;
	}

	private void checkAgentActivityForLastDays(List<DeActivateAgentDTO> agents, Date startDate, Date endDate) {
		List<DeActivateAgentDTO> offflineAgents = new ArrayList<DeActivateAgentDTO>();
		for (DeActivateAgentDTO agentId : agents) {
			List<AgentActivityLog> agentStatusesWithinRange = agentActivityLogRepository
					.getOnlineStatusesWithinDateRange(agentId.getAgentId(),
							XtaasDateUtils.getDateAtTime(startDate, 0, 0, 0, 0),
							XtaasDateUtils.getDateAtTime(endDate, 23, 59, 59, 0),
							Sort.by(Direction.DESC, "createdDate"));
			if (agentStatusesWithinRange == null || agentStatusesWithinRange.size() == 0) {
				Agent agent = agentRepository.findOneById(agentId.getAgentId());
				if (agent != null) {
					// inActivateAgent(agent);
					agentId.setFirstName(agent.getFirstName());
					agentId.setLastName(agent.getLastName());
					offflineAgents.add(agentId);
				}
			}
		}
		try {
			createExcelFile(offflineAgents);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void inActivateAgent(Agent agent) {
		agent.setStatus(AgentStatus.INACTIVE);
		agentRepository.save(agent);
		logger.debug("Inactivated agent as absent for last two working days is " + agent.getId());
	}

	public void createExcelFile(List<DeActivateAgentDTO> agents) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Deactivated agent list");
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeExcelHeaders(headerRow);
		if (agents.size() > 0) {
			for (DeActivateAgentDTO dto : agents) {
				Row row = sheet.createRow(++rowCount);
				writeToExcel(dto, row);
			}
		}
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ");
		String dateStr = sdf.format(date);
		String fileName = dateStr + "_deactivatedagents" + ".xlsx";
		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		workbook.close();
		String subject = dateStr + " - Deactivated agents";
		String message = " Please find the attached deactivated agent file.";
		XtaasEmailUtils.sendEmail("reports@xtaascorp.com", "data@xtaascorp.com", subject, message, fileNames);
		logger.debug("Deactivated agent list sent successfully to reports@xtaascorp.com");
	}

	private void writeExcelHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("PARTNER ID");
		cell = headerRow.createCell(1);
		cell.setCellValue("TEAM NAME");
		cell = headerRow.createCell(2);
		cell.setCellValue("FIRST NAME");
		cell = headerRow.createCell(3);
		cell.setCellValue("LAST NAME");
		cell = headerRow.createCell(4);
		cell.setCellValue("AGENT ID");
	}

	private void writeToExcel(DeActivateAgentDTO deActivateAgentDTO, Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue(deActivateAgentDTO.getPartnerId());
		cell = row.createCell(1);
		cell.setCellValue(deActivateAgentDTO.getTeamName());
		cell = row.createCell(2);
		cell.setCellValue(deActivateAgentDTO.getFirstName());
		cell = row.createCell(3);
		cell.setCellValue(deActivateAgentDTO.getLastName());
		cell = row.createCell(4);
		cell.setCellValue(deActivateAgentDTO.getAgentId());
	}

	private class DeActivateAgentDTO {
		String partnerId;
		String teamName;
		String firstName;
		String lastName;
		String agentId;

		public String getPartnerId() {
			return partnerId;
		}

		public void setPartnerId(String partnerId) {
			this.partnerId = partnerId;
		}

		public String getTeamName() {
			return teamName;
		}

		public void setTeamName(String teamName) {
			this.teamName = teamName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getAgentId() {
			return agentId;
		}

		public void setAgentId(String agentId) {
			this.agentId = agentId;
		}
	}
}