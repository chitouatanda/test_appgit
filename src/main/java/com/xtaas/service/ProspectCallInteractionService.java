package com.xtaas.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.web.dto.DailyBillingDTO;

public interface ProspectCallInteractionService {
	
	public ProspectCallInteraction getRecentDialerProcessed(String campaignId);

	public Map<String, Integer> getAgentCallStats(String campaignId,
			String agentId, Date fromDate);
	
	public List<String> getDistinctAgents(String campaignId, Date fromDate/*, Date toDate*/) ;
	
	public DailyBillingDTO getConnectData(String span);
	
	public Map<String, Integer> getTelcoDuration(List<String> campaignId,String agentId,Date startDate,Date endDate);

}
