package com.xtaas.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.filter.StateCodeFilter;
import com.xtaas.filter.TimeZoneFilter;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.ProspectCallCacheDTO;

@Service
public class ListCacheOperationService implements ProspectCacheOperationService {

	private static final Logger logger = LoggerFactory.getLogger(ListCacheOperationService.class);

	private Map<String, List<ProspectCallCacheDTO>> listOfProspectsInCache = new ConcurrentHashMap<String, List<ProspectCallCacheDTO>>();

	final private String thresHoldValue;

	final private CampaignRepository campaignRepository;

	final private ProspectInMemoryCacheServiceImpl prospInMemoryCacheServiceImpl;
	
	final private TimeZoneFilter timeZoneFilter;

	final private StateCodeFilter stateCodeFilter;

	public ListCacheOperationService(@Autowired CampaignRepository campaignRepository,
			@Autowired @Lazy ProspectInMemoryCacheServiceImpl prospInMemoryCacheServiceImpl,
			@Value("${caching_threshold}") String thresHoldValue, @Autowired TimeZoneFilter timeZoneFilter,
			@Autowired StateCodeFilter stateCodeFilter) {
		this.campaignRepository = campaignRepository;
		this.prospInMemoryCacheServiceImpl = prospInMemoryCacheServiceImpl;
		this.thresHoldValue = thresHoldValue;
		this.timeZoneFilter = timeZoneFilter;
		this.stateCodeFilter = stateCodeFilter;
		this.listOfProspectsInCache = new ConcurrentHashMap<String, List<ProspectCallCacheDTO>>();
	}

	@Override
	public synchronized void insert(String campaignId, List<ProspectCallCacheDTO> prospectsFromDB,
			Comparator<ProspectCallCacheDTO> prospectSortOrder) {
		if (campaignId != null) {
			long seconds = System.currentTimeMillis();
			if (this.listOfProspectsInCache.get(campaignId) == null) {
				this.listOfProspectsInCache.put(campaignId, new ArrayList<ProspectCallCacheDTO>());
			}
			List<ProspectCallCacheDTO> listOfProspects = this.listOfProspectsInCache.get(campaignId);
			synchronized (listOfProspects) {
				Set<String> prospectCallIDsFromCache = listOfProspects.stream().map(t -> t.getProspectCallId())
						.collect(Collectors.toSet());
				for (ProspectCallCacheDTO prospectCallDTO : prospectsFromDB) {
					if (!prospectCallIDsFromCache.contains(prospectCallDTO.getProspectCallId())) {
						listOfProspects.add(prospectCallDTO);
					}
				}
				this.listOfProspectsInCache.put(campaignId, listOfProspects);
				logger.info(
						"==========> Time taken to insert [{}] prospects in the list Cache is [{}] miliseconds. Campaign Id : [{}] <==========",
						prospectsFromDB.size(), (System.currentTimeMillis() - seconds), campaignId);
			}
		}
	}

	@Override
	public List<ProspectCallCacheDTO> getCurrentState(String campaignId) {
		List<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				return Collections.unmodifiableList(prospectsInCache);
			}
		}
		return prospectsInCache;
	}

	@Override
	public List<ProspectCallCacheDTO> remove(String campaignId, int count) {
		List<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				loadCacheForLowProspects(prospectsInCache, campaignId);
				List<ProspectCallCacheDTO> prospectDtoList = new ArrayList<ProspectCallCacheDTO>();
				if (prospectsInCache != null) {
					int listSize = prospectsInCache.size();
					int counter = 1;
					if (listSize <= count) {
						count = listSize;
					}
					Iterator<ProspectCallCacheDTO> it = prospectsInCache.iterator();
					while (it.hasNext()) {
						ProspectCallCacheDTO pDTO = it.next();
						if (counter <= count) {
							if (pDTO != null && pDTO.getProspect() != null) {
								pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
							}
							if (timeZoneFilter.execute(pDTO.getProspect().getTimeZone())) {
								prospectDtoList.add(pDTO);
							}
							if (stateCodeFilter.execute(pDTO.getProspect().getStateCode())) {
								prospectDtoList.add(pDTO);
							}
							counter++;
							it.remove();
						} else {
							break;
						}
					}
				}
				return prospectDtoList;
			}
		} else {
			return new ArrayList<ProspectCallCacheDTO>();
		}
	}

	@Override
	public List<ProspectCallCacheDTO> removeProspectsByRatio(String campaignId, Float size, List<Float> ratios) {
		List<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				loadCacheForLowProspects(prospectsInCache, campaignId);
				List<ProspectCallCacheDTO> prospectDTOList = new ArrayList<ProspectCallCacheDTO>();
				if (prospectsInCache != null) {
					int listSize = prospectsInCache.size();
					if (listSize <= size) {
						size = (float) listSize;
					}
					Iterator<ProspectCallCacheDTO> it = prospectsInCache.iterator();
					while (it.hasNext()) {
						ProspectCallCacheDTO pDTO = it.next();
						try {
							if (size > 0F) {
								if (pDTO != null && pDTO.getProspect() != null) {
									pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
								}
								if (stateCodeFilter.execute(pDTO.getProspect().getStateCode())) {
									prospectDTOList.add(pDTO);
								}
								size = size - ratios.get(pDTO.getCallRetryCount());
								it.remove();
							} else {
								break;
							}
						} catch (Exception e) {
							logger.debug(
									"Exception occurred in removeProspectsByRetrySpeed for campaignId [{}] & pcid [{}]",
									pDTO.getCampaignId(), pDTO.getProspectCallId());
							e.printStackTrace();
							it.remove();
						}
					}
				}
				return prospectDTOList;
			}
		} else {
			return new ArrayList<ProspectCallCacheDTO>();
		}
	}
	
	private void loadCacheForLowProspects(List<ProspectCallCacheDTO> prospectsInCache, String campaignId) {
		int prospectCount = prospectsInCache.size();
		int result = ((prospectCount * Integer.parseInt(thresHoldValue)) / 100);
		if (prospectCount < result) {
			Campaign campaign = campaignRepository.findOneById(campaignId);
			if (campaign != null) {
				prospInMemoryCacheServiceImpl.buildProspectCache(campaign);
			}
		}
	}

	@Override
	public void clearCampaignCache(String campaignId) {
		if (this.listOfProspectsInCache.containsKey(campaignId)) {
			this.listOfProspectsInCache.put(campaignId, new ArrayList<ProspectCallCacheDTO>());
		}
	}

	@Override
	public Map<String, PriorityQueue<ProspectCallCacheDTO>> getWholeCache() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCacheSize(String campaignId) {
		int size = 0;
		if (this.listOfProspectsInCache.containsKey(campaignId)) {
			size = this.listOfProspectsInCache.get(campaignId) != null
					? this.listOfProspectsInCache.get(campaignId).size()
					: 0;
		}
		return size;
	}

}