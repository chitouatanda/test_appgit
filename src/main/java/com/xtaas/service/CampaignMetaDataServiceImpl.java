package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.repository.CampaignMetaDataRepository;

@Service
public class CampaignMetaDataServiceImpl implements CampaignMetaDataService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignMetaDataServiceImpl.class);

	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;

	private List<CampaignMetaData> campaignMetaDataInMemory = new ArrayList<CampaignMetaData>();

	@Override
	public CampaignMetaData getCampaignMetaData() {
		if (campaignMetaDataInMemory == null || campaignMetaDataInMemory.size() == 0) {
			campaignMetaDataInMemory.addAll(campaignMetaDataRepository.findAll());
		}
		return campaignMetaDataInMemory.get(0);
	}

	@Override
	public void resetCampaignMetaData() {
		campaignMetaDataInMemory = new ArrayList<CampaignMetaData>();
	}

}
