package com.xtaas.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.SupervisorPreference;
import com.xtaas.db.repository.SupervisorPreferenceRepository;
import com.xtaas.domain.valueobject.PreferenceId;
import com.xtaas.utils.XtaasUserUtils;

@Service
public class SupervisorPreferenceServiceImpl implements SupervisorPreferenceService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(OrganizationServiceImpl.class);
	
	@Autowired
	private SupervisorPreferenceRepository supervisorPreferenceRepository;
	
	@Autowired
	private UserService userService;

	@Override
	public SupervisorPreference getPreference(PreferenceId preferenceId) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		SupervisorPreference listPreference = supervisorPreferenceRepository.findBySupervisorAndPreferenceId(userLogin.getUsername(), preferenceId);
		if (listPreference == null) {
			listPreference = supervisorPreferenceRepository.findBySupervisorAndPreferenceId("GlobalSupervisor", preferenceId);
		}
		return listPreference;

	}
	
	@Override
	public boolean updateSupervisorPreference(SupervisorPreference supervisorPreference) throws DuplicateKeyException  {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		PreferenceId preferenceId = supervisorPreference.getPreferenceId();
		if (preferenceId == null) {
			throw new IllegalArgumentException("Preference Id should not be empty");
		}
		SupervisorPreference listPreference = supervisorPreferenceRepository.findBySupervisorAndPreferenceId(userLogin.getUsername(), preferenceId);
		if (listPreference == null) {
			supervisorPreference.setSupervisorId(userLogin.getUsername());
			supervisorPreferenceRepository.save(supervisorPreference);
		} else {
			listPreference.setSupervisorId(userLogin.getUsername());
			listPreference.setValues(supervisorPreference.getValues());
				supervisorPreferenceRepository.save(listPreference);
			
		}
		logger.debug(supervisorPreference.getPreferenceId()+" Supervisor Preference created successfully");
		return true;
	}

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       