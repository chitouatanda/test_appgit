package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;

@Service
public class CampaignCallableMonitoringServiceImpl implements CampaignCallableMonitoringService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignCallableMonitoringServiceImpl.class);

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	TeamNoticeService teamNoticeService;

	@Autowired
	AgentService agentService;

	@Override
	public void monitorCallablesInCampaign() {
		try {
			ExecutorService executorService = Executors.newFixedThreadPool(10);
			List<String> diallingCampaignIds = new ArrayList<String>(
					agentRegistrationService.getCampaignActiveAgentCount().keySet());
			if (diallingCampaignIds != null && diallingCampaignIds.size() > 0) {
				List<Campaign> campaigns = campaignService.getCampaignByIds(diallingCampaignIds);
				if (campaigns != null && campaigns.size() > 0) {
					for (Campaign activeCampaign : campaigns) {
						Runnable runnable = () -> {
							if (activeCampaign.isCallableVanished()) {
								checkCallables(activeCampaign);
							}
						};
						executorService.submit(runnable);
					}
				}
			}
		} catch (Exception e) {
			logger.error(new SplunkLoggingUtils("monitorCallablesInCampaign", "")
					.eventDescription("Error occurred in monitorCallablesInCampaign")
					.methodName("monitorCallablesInCampaign").addThrowableWithStacktrace(e).build());
		}

	}

	private void checkCallables(Campaign campaign) {
		if (campaign != null && campaign.getDialerMode() != null
				&& campaign.getDialerMode().equals(DialerMode.RESEARCH)) {
			return;
		}
		Map<String, Integer> callableCountMap = prospectCallLogService.getCallableRecordsCount(campaign.getId());
		if (callableCountMap != null) {
			Integer count = callableCountMap.get("CALLABLE_COUNT");
			if (count != null && count == 0) {
				List<Team> teamsFromDB = teamRepository.findBySupervisorOverrideCampaignId(campaign.getId());
				if (teamsFromDB != null && teamsFromDB.size() > 0) {
					for (Team team : teamsFromDB) {
						if (team.getSupervisor() != null && team.getAgents() != null && team.getAgents().size() > 0) {
							PusherUtils.pushMessageToUser(team.getSupervisor().getResourceId(),
									XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
							String notice = " There are zero callables in the campaign " + campaign.getName();
							teamNoticeService.saveUserNotices(team.getSupervisor().getResourceId(), notice);
							logger.info(new SplunkLoggingUtils("checkCallables", campaign.getId())
									.eventDescription("There are zero callables in the campaign.")
									.addField("CampaignId", campaign.getId()).build());
							for (TeamResource agent : team.getAgents()) {
								if (agent != null && agent.getSupervisorOverrideCampaignId() != null
										&& campaign.getId().equals(agent.getSupervisorOverrideCampaignId())) {
									if (campaign != null && campaign.getDialerMode() != null
											&& campaign.getDialerMode().equals(DialerMode.HYBRID)) {
										Agent agentFromCache = agentService.getAgent(agent.getResourceId());
										if (agentFromCache != null && agentFromCache.getAgentType() != null
												&& agentFromCache.getAgentType().equals(agentSkill.MANUAL)) {
											continue;
										}
									}
									PusherUtils.pushMessageToUser(agent.getResourceId(),
											XtaasConstants.NO_CALLABLE_LOGOUT_EVENT, "");
									logger.debug(new SplunkLoggingUtils("checkCallables", campaign.getId())
											.eventDescription(
													"Agent logout of the system due to zero callables in the campaign.")
											.addField("agentId", agent.getResourceId()).build());
								}
							}
						}
					}
				}
			}
		}
	}

}