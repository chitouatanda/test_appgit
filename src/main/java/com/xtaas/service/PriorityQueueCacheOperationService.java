package com.xtaas.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.filter.StateCodeFilter;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.ProspectCallCacheDTO;

@Service
public class PriorityQueueCacheOperationService implements ProspectCacheOperationService {

	private static final Logger logger = LoggerFactory.getLogger(PriorityQueueCacheOperationService.class);

	private Map<String, PriorityQueue<ProspectCallCacheDTO>> listOfProspectsInCache = new ConcurrentHashMap<String, PriorityQueue<ProspectCallCacheDTO>>();

	final private String thresHoldValue;

	final private CampaignRepository campaignRepository;

	final private ProspectInMemoryCacheServiceImpl prospInMemoryCacheServiceImpl;
	
	final private ProspectCacheServiceImpl prospectCacheServiceImpl;

	final private StateCodeFilter stateCodeFilter;

	public PriorityQueueCacheOperationService(@Autowired CampaignRepository campaignRepository,
			@Autowired @Lazy ProspectInMemoryCacheServiceImpl prospInMemoryCacheServiceImpl,
			@Value("${caching_threshold}") String thresHoldValue,
			@Autowired StateCodeFilter stateCodeFilter, @Autowired ProspectCacheServiceImpl prospectCacheServiceImpl) {
		this.campaignRepository = campaignRepository;
		this.prospInMemoryCacheServiceImpl = prospInMemoryCacheServiceImpl;
		this.thresHoldValue = thresHoldValue;
		this.stateCodeFilter = stateCodeFilter;
		this.prospectCacheServiceImpl = prospectCacheServiceImpl;
		this.listOfProspectsInCache = new ConcurrentHashMap<String, PriorityQueue<ProspectCallCacheDTO>>();
	}

	@Override
	public synchronized void insert(String campaignId, List<ProspectCallCacheDTO> prospectsFromDB,
			Comparator<ProspectCallCacheDTO> prospectSortOrder) {
		if (campaignId != null) {
			long seconds = System.currentTimeMillis();
			if (this.listOfProspectsInCache.get(campaignId) == null) {
				this.listOfProspectsInCache.put(campaignId, new PriorityQueue<ProspectCallCacheDTO>(prospectSortOrder));
			}
			PriorityQueue<ProspectCallCacheDTO> listOfProspects = this.listOfProspectsInCache.get(campaignId);
			logger.info("Prospects already present in priority queue cache :  [{}] , Prospects from DB : [{}], Campaign Id : [{}]",listOfProspects.size(), prospectsFromDB.size() ,campaignId);
			synchronized (listOfProspects) {
				Set<String> prospectCallIDsFromCache = listOfProspects.stream().map(t -> t.getProspectCallId())
						.collect(Collectors.toSet());
				for (ProspectCallCacheDTO prospectCallDTO : prospectsFromDB) {
					if (!prospectCallIDsFromCache.contains(prospectCallDTO.getProspectCallId())) {
						listOfProspects.add(prospectCallDTO);
					}
				}
				this.listOfProspectsInCache.put(campaignId, listOfProspects);
				prospectCacheServiceImpl.removeCampaignIdsFromSet(campaignId);
				logger.info(
						"==========> Time taken to insert [{}] prospects in the priorityQueue Cache is [{}] miliseconds. Campaign Id : [{}] <==========",
						prospectsFromDB.size(), (System.currentTimeMillis() - seconds), campaignId);
			}
		}
	}

	@Override
	public List<ProspectCallCacheDTO> getCurrentState(String campaignId) {
		PriorityQueue<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				return Collections.unmodifiableList(new ArrayList<ProspectCallCacheDTO>(prospectsInCache));
			}
		}
		return null;
	}

	@Override
	public List<ProspectCallCacheDTO> remove(String campaignId, int count) {
		PriorityQueue<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				checkIfProspectCountBelowThreshold(prospectsInCache, campaignId);
				List<ProspectCallCacheDTO> prospectDtoList = new ArrayList<ProspectCallCacheDTO>();
				if (prospectsInCache != null) {
					int listSize = prospectsInCache.size();
					int counter = 1;
					if (listSize <= count) {
						count = listSize;
					}
					Iterator<ProspectCallCacheDTO> it = prospectsInCache.iterator();
					while (it.hasNext()) {
						ProspectCallCacheDTO pDTO = it.next();
						if (counter <= count) {
							if (pDTO != null && pDTO.getProspect() != null) {
								pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
							}
							if (pDTO != null && pDTO.getProspect() != null && pDTO.getProspect().getStateCode() != null && stateCodeFilter.execute(pDTO.getProspect().getStateCode())) {
								prospectDtoList.add(pDTO);
							}
							counter++;
							it.remove();
						} else {
							break;
						}
					}
				}
				return prospectDtoList;
			}
		} else {
			return new ArrayList<ProspectCallCacheDTO>();
		}
	}

	@Override
	public List<ProspectCallCacheDTO> removeProspectsByRatio(String campaignId, Float size, List<Float> ratios) {
		PriorityQueue<ProspectCallCacheDTO> prospectsInCache = this.listOfProspectsInCache.get(campaignId);
		if (prospectsInCache != null) {
			synchronized (prospectsInCache) {
				checkIfProspectCountBelowThreshold(prospectsInCache, campaignId);
				List<ProspectCallCacheDTO> prospectDTOList = new ArrayList<ProspectCallCacheDTO>();
				if (prospectsInCache != null) {
					int listSize = prospectsInCache.size();
					if (listSize <= size) {
						size = (float) listSize;
					}
					Iterator<ProspectCallCacheDTO> it = prospectsInCache.iterator();
					while (it.hasNext()) {
						ProspectCallCacheDTO pDTO = it.next();
						try {
							if (size == null) {
								size = Float.parseFloat("1");
							}
							if (size > 0F) {
								if (pDTO != null && pDTO.getProspect() != null) {
									pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
								}
								if (pDTO != null && pDTO.getProspect() != null && pDTO.getProspect().getStateCode() != null && stateCodeFilter.execute(pDTO.getProspect().getStateCode())) {
									prospectDTOList.add(pDTO);
								}
								if (size != null && ratios != null) {
									size = size - ratios.get(pDTO.getCallRetryCount());
								}
								it.remove();
							} else {
								break;
							}
						} catch (Exception e) {
							e.printStackTrace();
							it.remove();
						}
					}
				}
				return prospectDTOList;
			}
		} else {
			return new ArrayList<ProspectCallCacheDTO>();
		}
	}

	private void checkIfProspectCountBelowThreshold(PriorityQueue<ProspectCallCacheDTO> prospectsInCache, String campaignId) {
		int prospectCount = prospectsInCache.size();
		int prospects = prospInMemoryCacheServiceImpl.getProspectCountFromCache(campaignId);
		if (prospects > 0) {
			int result = ((prospects * Integer.parseInt(thresHoldValue)) / 100);
			if (prospectCount < result) {
				logger.info(
						"loadCacheForLowProspects() :: Triggering to load the cache while removing the prospects from cache. Campaign Id : [{}]",
						campaignId);
				Campaign campaign = campaignRepository.findOneById(campaignId);
				if (campaign != null) {
					prospInMemoryCacheServiceImpl.buildProspectCache(campaign);
				}
			}
		}
	}

	@Override
	public void clearCampaignCache(String campaignId) {
		if (this.listOfProspectsInCache.containsKey(campaignId)) {
			this.listOfProspectsInCache.put(campaignId, new PriorityQueue<ProspectCallCacheDTO>());
		}
	}
	
	@Override
	public Map<String, PriorityQueue<ProspectCallCacheDTO>> getWholeCache() {
		return this.listOfProspectsInCache;
	}

	@Override
	public int getCacheSize(String campaignId) {
		int size = 0;
		if (this.listOfProspectsInCache.containsKey(campaignId)) {
			size = this.listOfProspectsInCache.get(campaignId) != null
					? this.listOfProspectsInCache.get(campaignId).size()
					: 0;
		}
		return size;
	}

}