package com.xtaas.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentStatusLog;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.AgentActivityLogRepositoryImpl;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.AgentStatusLogRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.service.CRMMappingService;
import com.xtaas.service.ProspectCallInteractionService;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.AgentOnlineReportAPIDTO;

@Service
public class AgentOnlineReportAPIServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(AgentOnlineReportAPIServiceImpl.class);

	@Autowired
	private AgentService agentService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;

	@Autowired
	private AgentActivityLogRepositoryImpl agentActivityLogRepositoryImpl;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private ProspectCallInteractionService prospectCallInteractionService;
	
	@Autowired
	private AgentStatusLogRepository agentStatusLogRepository;

	public void downloadAgentReport(AgentOnlineReportAPIDTO agentOnlineReportAPIDTO) {
		sendAgentOnlineReport(agentOnlineReportAPIDTO);
	}

	private void sendAgentOnlineReport(AgentOnlineReportAPIDTO agentOnlineReportAPIDTO) {
		Date monthStartDate = agentOnlineReportAPIDTO.getStartDate();
		Date monthEndDate = agentOnlineReportAPIDTO.getEndDate();
		String fromDate = XtaasDateUtils.convertDateToString(XtaasDateUtils.getDateAtTime(monthStartDate, 0, 0, 0, 0),
				"MM/dd/yyyy HH:mm:ss"); // "12/01/2019 00:00:00";// UTC date MM/DD//YYYY

		String toDate = XtaasDateUtils.convertDateToString(XtaasDateUtils.getDateAtTime(monthEndDate, 23, 59, 59, 0),
				"MM/dd/yyyy HH:mm:ss"); // "12/31/2019 23:59:59"; // UTC date
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Map<String, List<AgentActivityLog>> activityMap = new HashMap<String, List<AgentActivityLog>>();
		Map<String, List<Date>> onlineMap = new HashMap<String, List<Date>>();
		Map<String, List<Date>> breakMap = new HashMap<String, List<Date>>();
		Map<String, List<Date>> offlineMap = new HashMap<String, List<Date>>();
		Map<String, List<Date>> otherMap = new HashMap<String, List<Date>>();
		Map<String, List<Date>> trainingmap = new HashMap<String, List<Date>>();
		Map<String, List<Date>> meetingmap = new HashMap<String, List<Date>>();
		Map<String, String> agentPartnerMap = new HashMap<String, String>();
		String FILENAME = "C:\\ex\\01-Dec to 31-Dec DEMANDSHORE.txt";
		List<String> missingagentIds = new ArrayList<String>();
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			Date startDate = XtaasDateUtils.getDate(fromDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			System.out.println(startDateInUTC);
			Date endDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
			System.out.println(endDateInUTC);
			Map<String, List<String>> agentCampaigns = new HashMap<String, List<String>>();
			Map<String, String> totalCampaignMap = new HashMap<String, String>();
			List<Team> teams = teamRepository.findInternalAndPartnerTeams(agentOnlineReportAPIDTO.getOrganizations());
			List<String> agentIds = new ArrayList<String>();
			BufferedReader reader;
			StringBuffer sb = new StringBuffer();
			sb.append("AgentId");
			sb.append("|");
			sb.append("AgentName");
			sb.append("|");
			sb.append("partnerId");
			sb.append("|");
			sb.append("Date");
			sb.append("|");
			sb.append("CampaignName");//
			sb.append("|");
			sb.append("Attempted");
			sb.append("|");
			sb.append("Contacted");
			sb.append("|");
			sb.append("Success");
			sb.append("|");
			sb.append("HT in hrs.");//
			sb.append("|");
			sb.append("OT in hrs.");
			Map<String, Agent> agentIDsMAp = new HashMap<String, Agent>();
			Map<String, String> supervisorAgentMap = new HashMap<String, String>();
			for (Team t : teams) {
				List<TeamResource> teamResources = t.getAgents();
				for (TeamResource tr : teamResources) {
					Agent agentDB = agentRepository.findOneById(tr.getResourceId());
					agentIDsMAp.put(tr.getResourceId(), agentDB);
					supervisorAgentMap.put(tr.getResourceId(), t.getSupervisor().getResourceId());
				}
			}

			for (Map.Entry<String, Agent> entry : agentIDsMAp.entrySet()) {
				agentIds.add(entry.getKey());
				List<AgentActivityLog> activityList = agentActivityLogRepository.getStatusesForReporting(entry.getKey(),
						startDateInUTC, endDateInUTC, Sort.by(Direction.ASC, "createdDate"));

				System.out.println(entry.getKey());
				if (activityList != null && activityList.size() > 0) {
					Agent agent = agentService.getAgent(entry.getKey());
					if (agent != null) {
						agentPartnerMap.put(entry.getKey(), agent.getPartnerId());
					}
					activityMap.put(entry.getKey(), activityList);
				}
			}
			if (missingagentIds != null && missingagentIds.size() > 0) {
				for (String missingId : missingagentIds) {
					List<AgentActivityLog> missingactivityList = agentActivityLogRepository.getStatusesForReporting(
							missingId, startDateInUTC, endDateInUTC, Sort.by(Direction.ASC, "createdDate"));
					System.out.println(missingId);
					if (missingactivityList != null && missingactivityList.size() > 0) {
						Agent magent = agentService.getAgent(missingId);
						if (magent != null) {
							agentPartnerMap.put(missingId, magent.getPartnerId());
						}
						activityMap.put(missingId, missingactivityList);
					}
				}
			}
			Date lastTime = null;
			for (Map.Entry<String, List<AgentActivityLog>> entry : activityMap.entrySet()) {
				String lastStatus = "";
				if (entry.getKey().equalsIgnoreCase("figntiwari")) {
					System.out.println("here go");
				}
				List<String> agentCids = agentCampaigns.get(entry.getKey());
				if (agentCids == null) {
					agentCids = new ArrayList<String>();
				}
				List<AgentActivityLog> alog = entry.getValue();
				if (alog != null && alog.size() > 0) {
					for (AgentActivityLog aalog : alog) {
						if (!agentCids.contains(aalog.getCampaignId())) {
							agentCids.add(aalog.getCampaignId());
							String cname = totalCampaignMap.get(aalog.getCampaignId());
							if (cname == null || cname.isEmpty()) {
								Campaign camp = campaignRepository.findOneById(aalog.getCampaignId());
								totalCampaignMap.put(aalog.getCampaignId(), camp.getName());
							}
							agentCampaigns.put(entry.getKey(), agentCids);
						}
						String agentStatus = aalog.getStatus();
						if (agentStatus.equalsIgnoreCase("ONLINE")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
								if (onlineDates == null) {
									onlineDates = new ArrayList<Date>();
								}
								onlineDates.add(aalog.getCreatedDate());
								onlineMap.put(aalog.getAgentId(), onlineDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("ONLINE"))
									continue;
								List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
								if (onlineDates == null) {
									onlineDates = new ArrayList<Date>();
								}
								onlineDates.add(aalog.getCreatedDate());
								onlineMap.put(aalog.getAgentId(), onlineDates);
								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}
						}
						if (agentStatus.equalsIgnoreCase("ONBREAK")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> breakDates = breakMap.get(aalog.getAgentId());
								if (breakDates == null) {
									breakDates = new ArrayList<Date>();
								}
								breakDates.add(aalog.getCreatedDate());
								breakMap.put(aalog.getAgentId(), breakDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("ONBREAK"))
									continue;
								List<Date> breakDates = breakMap.get(aalog.getAgentId());
								if (breakDates == null) {
									breakDates = new ArrayList<Date>();
								}
								breakDates.add(aalog.getCreatedDate());
								breakMap.put(aalog.getAgentId(), breakDates);

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}
						}
						if (agentStatus.equalsIgnoreCase("MEETING")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> meetingDates = meetingmap.get(aalog.getAgentId());
								if (meetingDates == null) {
									meetingDates = new ArrayList<Date>();
								}
								meetingDates.add(aalog.getCreatedDate());
								meetingmap.put(aalog.getAgentId(), meetingDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("MEETING"))
									continue;
								List<Date> meetingDates = meetingmap.get(aalog.getAgentId());
								if (meetingDates == null) {
									meetingDates = new ArrayList<Date>();
								}
								meetingDates.add(aalog.getCreatedDate());
								meetingmap.put(aalog.getAgentId(), meetingDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("TRAINING")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> trainingDates = trainingmap.get(aalog.getAgentId());
								if (trainingDates == null) {
									trainingDates = new ArrayList<Date>();
								}
								trainingDates.add(aalog.getCreatedDate());
								trainingmap.put(aalog.getAgentId(), trainingDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("TRAINING"))
									continue;
								List<Date> trainingDates = trainingmap.get(aalog.getAgentId());
								if (trainingDates == null) {
									trainingDates = new ArrayList<Date>();
								}
								trainingDates.add(aalog.getCreatedDate());
								trainingmap.put(aalog.getAgentId(), trainingDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("OFFLINE")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> offlineDates = offlineMap.get(aalog.getAgentId());
								if (offlineDates == null) {
									offlineDates = new ArrayList<Date>();
								}
								offlineDates.add(aalog.getCreatedDate());
								offlineMap.put(aalog.getAgentId(), offlineDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("OFFLINE"))
									continue;
								List<Date> offlineDates = offlineMap.get(aalog.getAgentId());
								if (offlineDates == null) {
									offlineDates = new ArrayList<Date>();
								}
								offlineDates.add(aalog.getCreatedDate());
								offlineMap.put(aalog.getAgentId(), offlineDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OTHER")) {
									List<Date> others = otherMap.get(aalog.getAgentId());
									if (others == null) {
										others = new ArrayList<Date>();
									}
									others.add(aalog.getCreatedDate());
									otherMap.put(aalog.getAgentId(), others);
									lastStatus = agentStatus;
								}
							}

						}
						if (agentStatus.equalsIgnoreCase("OTHER")) {
							if ("".equalsIgnoreCase(lastStatus)) {
								lastStatus = agentStatus;
								lastTime = aalog.getCreatedDate();
								List<Date> otherDates = otherMap.get(aalog.getAgentId());
								if (otherDates == null) {
									otherDates = new ArrayList<Date>();
								}
								otherDates.add(aalog.getCreatedDate());
								otherMap.put(aalog.getAgentId(), otherDates);
								continue;
							} else {
								if (lastStatus.equalsIgnoreCase("OTHER"))
									continue;
								List<Date> otherDates = otherMap.get(aalog.getAgentId());
								if (otherDates == null) {
									otherDates = new ArrayList<Date>();
								}
								otherDates.add(aalog.getCreatedDate());
								otherMap.put(aalog.getAgentId(), otherDates);

								if (lastStatus.equalsIgnoreCase("ONBREAK")) {
									List<Date> breakDates = breakMap.get(aalog.getAgentId());
									if (breakDates == null) {
										breakDates = new ArrayList<Date>();
									}
									breakDates.add(aalog.getCreatedDate());
									breakMap.put(aalog.getAgentId(), breakDates);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("MEETING")) {
									List<Date> meetings = meetingmap.get(aalog.getAgentId());
									if (meetings == null) {
										meetings = new ArrayList<Date>();
									}
									meetings.add(aalog.getCreatedDate());
									meetingmap.put(aalog.getAgentId(), meetings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("TRAINING")) {
									List<Date> trainings = trainingmap.get(aalog.getAgentId());
									if (trainings == null) {
										trainings = new ArrayList<Date>();
									}
									trainings.add(aalog.getCreatedDate());
									trainingmap.put(aalog.getAgentId(), trainings);
									lastStatus = agentStatus;
								}
								if (lastStatus.equalsIgnoreCase("OFFLINE")) {
									List<Date> offlines = offlineMap.get(aalog.getAgentId());
									if (offlines == null) {
										offlines = new ArrayList<Date>();
									}
									offlines.add(aalog.getCreatedDate());
									offlineMap.put(aalog.getAgentId(), offlines);
									lastStatus = agentStatus;
								}

								if (lastStatus.equalsIgnoreCase("ONLINE")) {
									List<Date> onlineDates = onlineMap.get(aalog.getAgentId());
									if (onlineDates == null) {
										onlineDates = new ArrayList<Date>();
									}
									onlineDates.add(aalog.getCreatedDate());
									onlineMap.put(aalog.getAgentId(), onlineDates);
									lastStatus = agentStatus;
								}
							}
						}
					}
				}
			}

			List<Date> onlineDates = onlineMap.get("figntiwari");
			List<Date> otherDates = otherMap.get("figntiwari");
			List<Date> offlineDates = offlineMap.get("figntiwari");
			List<Date> meetingDates = meetingmap.get("figntiwari");
			List<Date> trainingDates = trainingmap.get("figntiwari");
			List<Date> onBreakDates = breakMap.get("figntiwari");
			if (onlineDates != null && onlineDates.size() > 0) {
				int mod = onlineDates.size() % 2;
				int size = onlineDates.size();
				if (mod > 0) {
					size = onlineDates.size() - mod;
				}
				float online = 0;
				long breakvalue = 0;
				long meeting = 0;
				long offline = 0;
				long other = 0;
				long training = 0;
				int i = 0;
				while (i < size) {
					long temp = 0;
					Date d1 = onlineDates.get(i);
					i = i + 1;
					Date d2 = onlineDates.get(i);
					i++;
					temp = (d2.getTime() - d1.getTime()) / 1000;
					online = online + temp;
				}
				online = online / 3600;
				System.out.println("ONLINEMAP -- >" + online + " hrs\n");
			}

			Map<String, Map<String, List<Date>>> dayMap = new HashMap<String, Map<String, List<Date>>>();

			for (Map.Entry<String, List<Date>> entry : onlineMap.entrySet()) {
				List<Date> dates = entry.getValue();
				Map<String, List<Date>> mapByDay = new HashMap<String, List<Date>>();
				for (Date d : dates) {
					String strDate = formatter.format(d);
					// System.out.println(d);
					mapByDay = dayMap.get(entry.getKey());
					if (mapByDay == null || mapByDay.size() == 0) {
						mapByDay = new HashMap<String, List<Date>>();
						List<Date> dayDate = new ArrayList<Date>();
						dayDate.add(d);
						mapByDay.put(strDate, dayDate);
						dayMap.put(entry.getKey(), mapByDay);
					} else {
						List<Date> dayDate = mapByDay.get(strDate);
						if (dayDate == null) {
							dayDate = new ArrayList<Date>();
							// System.out.println("HI");
						}
						dayDate.add(d);
						mapByDay.put(strDate, dayDate);
						dayMap.put(entry.getKey(), mapByDay);
					}
				}
			}
			List<AgentOnlineReportDTO> agentList = new ArrayList<AgentOnlineReportDTO>();
			for (Map.Entry<String, Map<String, List<Date>>> entry : dayMap.entrySet()) {
				Map<String, List<Date>> daydates = entry.getValue();
				String partnerId = agentPartnerMap.get(entry.getKey());
				List<String> acids = agentCampaigns.get(entry.getKey());
				for (String cid : acids) {
					Campaign camp = campaignRepository.findOneById(cid);

					List<String> cids = new ArrayList<String>();
					cids.add(cid);

					for (Map.Entry<String, List<Date>> entry1 : daydates.entrySet()) {
						int connectCounter  = 0;
						List<Date> dates = entry1.getValue();
						String agentStart = entry1.getKey() + " 00:01:00";
						String agentEnd = entry1.getKey() + " 23:59:00";
						Date agentstartDate = XtaasDateUtils.getDate(agentStart,
								XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
						Date agentstartDateInUTC = XtaasDateUtils.convertToTimeZone(agentstartDate, "UTC");
						System.out.println(agentstartDateInUTC);
						Date agentendDate = XtaasDateUtils.getDate(agentEnd, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
						Date agentendDateInUTC = XtaasDateUtils.convertToTimeZone(agentendDate, "UTC");
						System.out.println(agentendDateInUTC);
						int dailed = prospectCallInteractionRepository.getAgentDialedCount(cids, entry.getKey(),
								agentstartDateInUTC, agentendDateInUTC);
						int contct = prospectCallInteractionRepository.getAgentContactCount(cids, entry.getKey(),
								agentstartDateInUTC, agentendDateInUTC);
						int connect = prospectCallInteractionRepository.getAgentConnectCount(cids, entry.getKey(),
								agentstartDateInUTC, agentendDateInUTC);
						List<ProspectCallInteraction> connectList = prospectCallInteractionRepository.getAgentConnectCountList(cids, entry.getKey(),
								agentstartDateInUTC, agentendDateInUTC);
						
						int success = prospectCallInteractionRepository.getAgentSuccessCount(cids, entry.getKey(),
								agentstartDateInUTC, agentendDateInUTC);
						if(camp.isSimpleDisposition())  {
							for(ProspectCallInteraction pci : connectList) {
								if(pci.getProspectCall().getProspect().isDirectPhone() && pci.getProspectCall().getCallDuration()>15) {
									connectCounter =connectCounter+1;
			
								}else if(camp.isSimpleDisposition()  && !pci.getProspectCall().getProspect().isDirectPhone() 
											&& pci.getProspectCall().getCallDuration()>30) {
									connectCounter =connectCounter+1;
								}
			
							}
						}else {
							connectCounter  = connect;
						}
						Map<String, Integer> intractionMap = prospectCallInteractionService.getTelcoDuration(cids,
								entry.getKey(), agentstartDateInUTC, agentendDateInUTC);
						Agent aag = agentIDsMAp.get(entry.getKey());
						Integer cds = intractionMap.get("callDuration");
						Pageable pageable = PageRequest.of(0, 10000, Direction.DESC, "updatedDate");
						List<AgentStatusLog> asLogList = agentStatusLogRepository.getOnlineByDate(entry.getKey(),
								agentstartDateInUTC,agentendDateInUTC,pageable);
						float newOTHRS = 0 ;
						if(asLogList!=null && asLogList.size()>0){
							for(AgentStatusLog agentStatuslog : asLogList){
								if(agentStatuslog.getStateDuration()>0)
									newOTHRS = newOTHRS+agentStatuslog.getStateDuration();
							}
						}

						double cdh = 0.0;
						if (cds != null && cds > 0) {
							Double cdd = Double.valueOf(cds.toString());
							cdh = (cdd / 3600);
						}
						DecimalFormat df = new DecimalFormat("#.####");
						df.setRoundingMode(RoundingMode.CEILING);
						sb.append(entry.getKey() + "|" + aag.getName() + "|" + partnerId + "|" + entry1.getKey() + "|"
								+ totalCampaignMap.get(cid) + "|" + dailed + "|" + contct + "|" + connect + "|" + success + "|"
								+ df.format(cdh) + "|");
						int mod = dates.size() % 2;
						int size = dates.size();
						if (mod > 0) {
							size = dates.size() - mod;
						}
						float online = 0;
						long breakvalue = 0;
						long meeting = 0;
						long offline = 0;
						long other = 0;
						long training = 0;
						int i = 0;
						while (i < size) {
							long temp = 0;
							Date d1 = dates.get(i);
							i = i + 1;
							Date d2 = dates.get(i);
							i++;
							temp = (d2.getTime() - d1.getTime()) / 1000;
							online = online + temp;
						}
						online = online / 3600;
						
						sb.append(online + " hrs");
						sb.append( "|");
						
						float newots  = 0;
						newots =newOTHRS/3600;

					//	sb.append(newots + " hrs");

						
						
					//	sb.append("\n");
						
						//sb.append(online +"\n");

						sb.append(newots +"\n");
						AgentOnlineReportDTO agentOnlineReportDTO = new AgentOnlineReportDTO();
						agentOnlineReportDTO.setAgentId(entry.getKey());
						agentOnlineReportDTO.setSupervisorId(supervisorAgentMap.get(entry.getKey()));
						agentOnlineReportDTO.setAgentName(aag.getName());
						agentOnlineReportDTO.setPartnerId(partnerId);
						agentOnlineReportDTO.setDate(entry1.getKey());
						agentOnlineReportDTO.setCampaignName(totalCampaignMap.get(cid));
						agentOnlineReportDTO.setConnected(connect);
						agentOnlineReportDTO.setAttempted(dailed);
						agentOnlineReportDTO.setContacted(contct);
						agentOnlineReportDTO.setConnected(connect);
						agentOnlineReportDTO.setSuccess(success);
						agentOnlineReportDTO.setHtInHrs(df.format(cdh));
						agentOnlineReportDTO.setOtInHrs(newots + "\n");
						agentList.add(agentOnlineReportDTO);
					}
				}
			}
			try {
				createExcelFile(agentList, agentOnlineReportAPIDTO);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void createExcelFile(List<AgentOnlineReportDTO> agentsOnlineReportDTO,
			AgentOnlineReportAPIDTO agentOnlineReportAPIDTO) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Agent_Online_Report");
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeExcelHeaders(headerRow);
		if (agentsOnlineReportDTO.size() > 0) {
			for (AgentOnlineReportDTO dto : agentsOnlineReportDTO) {
				if (dto.getAttempted() > 0) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(dto, row);
				}
			}
		}
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ");
		String dateStr = sdf.format(date);
		String fileName = dateStr + "_agent_performance_report" + ".xlsx";
		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		workbook.close();
		String subject = agentOnlineReportAPIDTO.getOrganizations().get(0) + " - Agent Performance Report MTD."; 
		String message = " Please find the attached agent performance report file of " + agentOnlineReportAPIDTO.getOrganizations().get(0);
		if (agentOnlineReportAPIDTO != null && agentOnlineReportAPIDTO.getEmails() != null) {
			for (String email : agentOnlineReportAPIDTO.getEmails()) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, message, fileNames);
				logger.debug("Agent online report sent successfully to " + email);
			}
		} else {
			XtaasEmailUtils.sendEmail("kchugh@xtaascorp.com", "data@xtaascorp.com", subject, message,
					fileNames);
		}
	}

	private void writeExcelHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("AGENT ID");
		cell = headerRow.createCell(1);
		cell.setCellValue("SUPERVISOR ID");
		cell = headerRow.createCell(2);
		cell.setCellValue("AGENT NAME");
		cell = headerRow.createCell(3);
		cell.setCellValue("PARTNER ID");
		cell = headerRow.createCell(4);
		cell.setCellValue("DATE");
		cell = headerRow.createCell(5);
		cell.setCellValue("CAMPAIGN NAME");
		cell = headerRow.createCell(6);
		cell.setCellValue("ATTEMPTED");
		cell = headerRow.createCell(7);
		cell.setCellValue("CONTACTED");
		cell = headerRow.createCell(8);
		cell.setCellValue("CONNECTED");
		cell = headerRow.createCell(9);
		cell.setCellValue("SUCCESS");
		cell = headerRow.createCell(10);
		cell.setCellValue("HT IN HRS");
		cell = headerRow.createCell(11);
		cell.setCellValue("OT IN HRS");
	}

	private void writeToExcel(AgentOnlineReportDTO agentOnlineReportDTO, Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue(agentOnlineReportDTO.getAgentId());
		cell = row.createCell(1);
		cell.setCellValue(agentOnlineReportDTO.getSupervisorId());
		cell = row.createCell(2);
		cell.setCellValue(agentOnlineReportDTO.getAgentName());
		cell = row.createCell(3);
		cell.setCellValue(agentOnlineReportDTO.getPartnerId());
		cell = row.createCell(4);
		cell.setCellValue(agentOnlineReportDTO.getDate());
		cell = row.createCell(5);
		cell.setCellValue(agentOnlineReportDTO.getCampaignName());
		cell = row.createCell(6);
		cell.setCellValue(agentOnlineReportDTO.getAttempted());
		cell = row.createCell(7);
		cell.setCellValue(agentOnlineReportDTO.getContacted());
		cell = row.createCell(8);
		cell.setCellValue(agentOnlineReportDTO.getConnected());
		cell = row.createCell(9);
		cell.setCellValue(agentOnlineReportDTO.getSuccess());
		cell = row.createCell(10);
		cell.setCellValue(agentOnlineReportDTO.getHtInHrs());
		cell = row.createCell(11);
		cell.setCellValue(agentOnlineReportDTO.getOtInHrs());
	}

	private class AgentOnlineReportDTO {
		String agentId;
		String supervisorId;
		String agentName;
		String partnerId;
		String date;
		String campaignName;
		int attempted;
		int contacted;
		int connected;
		int success;
		String htInHrs;
		String otInHrs;

		public String getAgentId() {
			return agentId;
		}

		public void setAgentId(String agentId) {
			this.agentId = agentId;
		}

		public String getSupervisorId() {
			return supervisorId;
		}

		public void setSupervisorId(String supervisorId) {
			this.supervisorId = supervisorId;
		}

		public String getAgentName() {
			return agentName;
		}

		public void setAgentName(String agentName) {
			this.agentName = agentName;
		}

		public String getPartnerId() {
			return partnerId;
		}

		public void setPartnerId(String partnerId) {
			this.partnerId = partnerId;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getCampaignName() {
			return campaignName;
		}

		public void setCampaignName(String campaignName) {
			this.campaignName = campaignName;
		}

		public int getAttempted() {
			return attempted;
		}

		public void setAttempted(int attempted) {
			this.attempted = attempted;
		}

		public int getContacted() {
			return contacted;
		}

		public void setContacted(int contacted) {
			this.contacted = contacted;
		}

		public int getConnected() {
			return connected;
		}

		public void setConnected(int connected) {
			this.connected = connected;
		}

		public int getSuccess() {
			return success;
		}

		public void setSuccess(int success) {
			this.success = success;
		}

		public String getHtInHrs() {
			return htInHrs;
		}

		public void setHtInHrs(String htInHrs) {
			this.htInHrs = htInHrs;
		}

		public String getOtInHrs() {
			return otInHrs;
		}

		public void setOtInHrs(String otInHrs) {
			this.otInHrs = otInHrs;
		}
	}

}