package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

public interface CampaignSuppressionService {

	public void downloadSuppressionListTemplate(HttpServletRequest request, HttpServletResponse response, String campaignId, String suppressionType)
			throws FileNotFoundException, IOException;

	public String createCampaignSuppressionList(String campaignId, String suppressionType, MultipartFile file)
			throws IOException;

	public Map<String,Integer> getSuppressionListByCampaignId(String campaignId);

	public String multiDownloadSupressionlist(String campaignId,String suppressionType, String loginId) throws FileNotFoundException, IOException;
	
	public String removeDublicateRecords();
}
