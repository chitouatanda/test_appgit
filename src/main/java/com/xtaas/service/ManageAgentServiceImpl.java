package com.xtaas.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.QaService;
import com.xtaas.application.service.TeamService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.QaRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.Qa;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.LoginStatus;
import com.xtaas.domain.valueobject.Status;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.QaDTO;

@Service
public class ManageAgentServiceImpl implements ManageAgentService {

	private static final Logger logger = LoggerFactory.getLogger(ManageAgentServiceImpl.class);

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private QaRepository qaRepository;

	@Autowired
	private QaService qaService;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private AgentService agentService;

	@Autowired
	private TeamService teamService;
	
	@Autowired
	OrganizationRepository organizationRepository;

	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;// this is the excel sheet to be read

	private List<String> errorList = new ArrayList<String>();

	private List<String> mandatoryFields; // contain mandatory fields
	
//	private List<String> errorListAgent;
	
//	private HashMap<Integer, List<String>> errorMap = new HashMap<>();

	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	Workbook book;
	
	@Override
	public void downloadTemplate(HttpServletRequest request, HttpServletResponse response) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeaders(headerRow);
		Organization organization = organizationRepository.findOneById(XtaasUserUtils.getCurrentLoggedInUserObject().getOrganization());
		String adminEmail = organization.getOrganizationEmail();
		String email = "";
		if (adminEmail != null) {
			email = adminEmail;
		} else {
			email = "";
		}
		List<Team> teams = teamRepository.findBySupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
		if (teams.size() > 0) {
			Team topMostTeam = teams.get(0);
			for (TeamResource qa : topMostTeam.getQas()) {
				Login loginFromDB = loginRepository.findOneById(qa.getResourceId());
				if (loginFromDB != null) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(loginFromDB, row, workbook,email);
				}
			}
			for (TeamResource agent : topMostTeam.getAgents()) {
				Login loginFromDB = loginRepository.findOneById(agent.getResourceId());
				if (loginFromDB != null) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(loginFromDB, row, workbook,email);
				}
			}
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=manage_resource_template.xlsx");
			workbook.write(response.getOutputStream());
			workbook.close();
		}
	}

	private void writeHeaders(Row headerRow) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("User Name");
		cell = headerRow.createCell(1);
		cell.setCellValue("Password");
		cell = headerRow.createCell(2);
		cell.setCellValue("Role");
		cell = headerRow.createCell(3);
		cell.setCellValue("Organization ID");
		cell = headerRow.createCell(4);
		cell.setCellValue("Admin Email ID");
		cell = headerRow.createCell(5);
		cell.setCellValue("First Name");
		cell = headerRow.createCell(6);
		cell.setCellValue("Last Name");
		cell = headerRow.createCell(7);
		cell.setCellValue("Email");
		cell = headerRow.createCell(8);
		cell.setCellValue("Status");
		cell = headerRow.createCell(9);
		cell.setCellValue("Action");
		cell = headerRow.createCell(10);
		cell.setCellValue("Sub Action");
		cell = headerRow.createCell(11);
		cell.setCellValue("Supervisor ID");
		cell = headerRow.createCell(12);
		cell.setCellValue("Team Action");
		cell = headerRow.createCell(13);
		cell.setCellValue("Reset MFA");
	}

	private void writeToExcel(Login login, Row row, Workbook workbook,String email) {
		Cell cell = row.createCell(0);
		cell.setCellValue(login.getId());
		cell = row.createCell(1);
		cell.setCellValue("");
		cell = row.createCell(2);
		cell.setCellValue(login.getRoles().get(0));
		cell = row.createCell(3);
		cell.setCellValue(login.getOrganization());
		cell = row.createCell(4);
		cell.setCellValue(email);
		cell = row.createCell(5);
		cell.setCellValue(login.getFirstName());
		cell = row.createCell(6);
		cell.setCellValue(login.getLastName());
		cell = row.createCell(7);
		cell.setCellValue(login.getEmail());
		cell = row.createCell(8);
		if (login.getStatus() != null) {
			cell.setCellValue(login.getStatus().toString());
		} else {
			cell.setCellValue("");
		}
		cell = row.createCell(9);
		cell.setCellValue("");
		cell = row.createCell(10);
		cell.setCellValue("");
		cell = row.createCell(11);
		cell.setCellValue(XtaasUserUtils.getCurrentLoggedInUsername());
		cell = row.createCell(12);
		cell.setCellValue("");
		cell = row.createCell(13);
		cell.setCellValue("");
	}

	@Override
	public String uploadDetails(MultipartFile file, HttpSession httpSession) throws IOException {
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
		if (!file.isEmpty()) {
			fileName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					// add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				// XSSFWorkbook book;
				//Workbook book;
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(fileName + " file is empty");
						throw new IllegalArgumentException("File cannot be empty.");
					}else if (totalRowsCount <= 1) {// check if nothing found in file
						book.close();
						logger.error("No records found in file " + fileName);
						throw new IllegalArgumentException("No records found in file.");
					}
					mandatoryFields = new ArrayList<String>() {
						private static final long serialVersionUID = 1L;
						{
							add("User Name");
							/* add("Password"); */
							add("Roles");
							add("Organization Id");
							add("First Name");
							add("Last Name");
							add("Email");
						}
					};
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords(loginUser,organization);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		} else {
			return "{\"code\": \"ERROR\", \"msg\" : \"EMPTY FILE\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);// get first sheet in file
		int colCounts = row.getPhysicalNumberOfCells();
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell == null || cell.getCellType() == CellType.BLANK) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
				throw new IllegalArgumentException(
						"Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
			}
			if (cell.getCellType() != CellType.STRING) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
				throw new IllegalArgumentException("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
			} else {
				headerIndex.put(j, cell.getStringCellValue().replace("*", ""));

				fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
			}
		}
		return true;
	}

	private void validateExcelFileRecords(Login loginUser,Organization organization) {
		int rowsCount = sheet.getPhysicalNumberOfRows();
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processRecords(loginUser,0, rowsCount, colCounts,organization);
	}

	public void processRecords(Login loginUser,int startPos, int EndPos, int colCounts,Organization organizationObj) {
		List<UploadFileDTO> uploadFileDTOs = new ArrayList<UploadFileDTO>();
		HashMap<Integer, List<String>> errorMap = new HashMap<>();
		for (int i = startPos; i < EndPos; i++) {
			List<String> errorListAgent = new ArrayList<String>();
			UploadFileDTO uploadFile = new UploadFileDTO();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			if (row == null || i == 0) {
				continue;
			}
			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				continue;
			}
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equals("User Name")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String userName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if(userName != null) {
							if(userName.length() <= 22) {
								if(!userName.matches("[^a-zA-Z0-9]") && !userName.contains("_")) {
									uploadFile.setUsername(userName);
								}else {
									errorListAgent.add("Special characters not allowed in the UserName");
								}
							}else {
								errorListAgent.add("Max 22 character is allowed");
							}
						}
					}else {
						errorListAgent.add(columnName+" is Missing");
					}
				} else if (columnName.equals("Password")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String str = cell.getCellType() == CellType.STRING ? str = cell.getStringCellValue()
								: new BigDecimal(cell.getNumericCellValue()).toPlainString();
						String password = XtaasUtils.removeNonAsciiChars(str);
						uploadFile.setPassword(password);
					}
				} else if (columnName.equals("Role")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String role = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						List<String> roles = new ArrayList<String>();
						roles.add(role.toUpperCase());
						if (roles.size() > 0) {
							uploadFile.setRoles(roles);
						}
					} else {
						errorListAgent.add(columnName + " is Missing");
					}
				} else if (columnName.equals("Organization ID")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String organization = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (organization == null || organization.isEmpty()) {
							errorListAgent.add(columnName + " is Missing");
						} else {
							uploadFile.setOrganization(organization.toUpperCase());
						}
					} else {
						errorListAgent.add(columnName + " is Missing");
					}
				} else if (columnName.equals("Admin Email ID")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String adminEmail = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (adminEmail == null || adminEmail.isEmpty()) {
							uploadFile.setOrganizationEmail(adminEmail.toUpperCase());
						}
					}
				} else if (columnName.equals("First Name")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String firstName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (firstName == null || firstName == "") {
							errorListAgent.add(columnName+" is Missing");
						} else {
							uploadFile.setFirstName(firstName);
						}
					} else {
						errorListAgent.add(columnName + " is Missing");
					}
				} else if (columnName.equals("Last Name")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String lastName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (lastName == null || lastName == "") {
							errorListAgent.add(columnName+" is Missing");
						} else {
							uploadFile.setLastName(lastName);
						}
					}else {
						errorListAgent.add(columnName + " is Missing");
					}
					
				} else if (columnName.equals("Email")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (email == null || email == "") {
							errorListAgent.add(columnName+" is Missing");
						}else {
							uploadFile.setEmail(email);
						}
					}else {
						errorListAgent.add(columnName + " is Missing");
					}
					
				} else if (columnName.equals("Action")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String action = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setAction(action);
					}
				} else if (columnName.equals("Sub Action")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String subAction = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setSubAction(subAction);
					}
				} else if (columnName.equals("Supervisor ID")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String supervisorId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if (supervisorId == null || supervisorId == "") {
							errorListAgent.add(columnName+" is Missing");
						}else {
							uploadFile.setSupervisorId(supervisorId);
						}
					}else {
						errorListAgent.add(columnName + " is Missing");
					}
				} else if (columnName.equals("Team Action")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String teamAction = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						uploadFile.setTeamAction(teamAction);
					}
				} else if (columnName.equals("Reset MFA")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String mfa = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if(mfa != null && !mfa.isEmpty()) {
							uploadFile.setResetMFA(mfa);
						}
					}
				}
			}
			if (uploadFileDTOs != null && !uploadFileDTOs.isEmpty()) {
				List<UploadFileDTO> newLoginList = new ArrayList<UploadFileDTO>();
				newLoginList = uploadFileDTOs.stream().filter(cc -> cc.getUsername().equals(uploadFile.getUsername()))
						.collect(Collectors.toList());
				if (!newLoginList.isEmpty()) {
					//isRecordValid = false;
				}
			}

			if(errorListAgent.size() > 0) {
				errorMap.put(i, errorListAgent);
			} else {
				if (uploadFile.getAction() != null && uploadFile.getAction().equalsIgnoreCase("update")) {
					if (uploadFile.getRoles() != null && uploadFile.getRoles().size() > 0 && uploadFile.getRoles().contains("AGENT")) {
						if (uploadFile.getSubAction() != null && uploadFile.getSubAction().equalsIgnoreCase("move")) {
							String errorMessage = checkNewSupervisorInSameOrganization(uploadFile);
							if (errorMessage != null && !errorMessage.isEmpty()) {
								errorListAgent.add(errorMessage);
								errorMap.put(i, errorListAgent);
								// isRecordValid = false;
								continue;
							}
						}
					} 
				}
				uploadFileDTOs.add(uploadFile);
			}
		}

		for (UploadFileDTO uploadFileDTO : uploadFileDTOs) {
			if (uploadFileDTO.getAction() != null && uploadFileDTO.getAction().equalsIgnoreCase("create")) {
				if (!isUniqueInLogin(uploadFileDTO.getUsername())) {
					Login loginFromDB = createLogin(uploadFileDTO);
					logger.debug("Login [{}] created successfully.", loginFromDB.getId());
				}
				if (uploadFileDTO.getRoles().contains("AGENT")) {
					createAgent(uploadFileDTO);
					addAgentInTeam(uploadFileDTO);
				} else if (uploadFileDTO.getRoles().contains("QA")
						|| uploadFileDTO.getRoles().contains("SECONDARYQA")) {
					createQa(uploadFileDTO);
					addQaInTeam(uploadFileDTO);
				}
			} else if (uploadFileDTO.getAction() != null && uploadFileDTO.getAction().equalsIgnoreCase("update")) {
				if (uploadFileDTO.getRoles().contains("AGENT")) {
					if (uploadFileDTO.getSubAction() != null && uploadFileDTO.getSubAction().equalsIgnoreCase("move")) {
						moveAgentToNewTeam(uploadFileDTO);
					} else {
						updateAgent(uploadFileDTO);
						updateUserDetails(uploadFileDTO);
						if (uploadFileDTO.getTeamAction() != null
								&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
							addAgentInTeam(uploadFileDTO);
						} else if (uploadFileDTO.getTeamAction() != null
								&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
							removeAgentFromTeam(uploadFileDTO);
						}
					}					
				} else if (uploadFileDTO.getRoles().contains("QA")
						|| uploadFileDTO.getRoles().contains("SECONDARYQA")) {
					updateQa(uploadFileDTO);
					updateUserDetails(uploadFileDTO);
					if (uploadFileDTO.getTeamAction() != null
							&& uploadFileDTO.getTeamAction().equalsIgnoreCase("insert")) {
						addQaInTeam(uploadFileDTO);
					} else if (uploadFileDTO.getTeamAction() != null
							&& uploadFileDTO.getTeamAction().equalsIgnoreCase("delete")) {
						removeQaFromTeam(uploadFileDTO);
					}
				}
			} else if (uploadFileDTO.getResetMFA() != null && !uploadFileDTO.getResetMFA().isEmpty()) {
				Login login = loginRepository.findOneById(uploadFileDTO.getUsername());
				if (uploadFileDTO.getResetMFA().equalsIgnoreCase("yes")) {
					login.setActivateMFAUser(false);
					login.setUserLock(false);
					login.setNoOfAttempt(0);
				} else if (uploadFileDTO.getResetMFA().equalsIgnoreCase("no")) {
					login.setActivateMFAUser(true);
				}
				loginRepository.save(login);
			}
		}
		List<String> reportingEmail = null;
		if ( loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
			reportingEmail = loginUser.getReportEmails();
		}
		if (errorMap.keySet().size() > 0) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(loginUser, book, fileName, "Agent/QA Creation", null,
						reportingEmail);
			} catch (IOException e) {
				logger.error(new SplunkLoggingUtils("manageTeam", UUID.randomUUID().toString())
						.eventDescription("Failed to send error list email for manage team API")
						.addField("error", e.getMessage()).build());
			}
			throw new IllegalArgumentException("Please check your email, there are Some errors in file");
		} else {
			System.out.println("Login List Added successfully");
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			String subject = "Agent/QA Creation Upload Status Success";
			if(reportingEmail != null && reportingEmail.size() > 0) {
				for(String email : reportingEmail) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject,
							"File Uploaded Successfully");	
				}
			}else {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject,
						"File Uploaded Successfully");
			}
		}
	}

	private Login createLogin(UploadFileDTO uploadFileDTO) {
		Login login = new Login();
		login.setUsername(uploadFileDTO.getUsername());
		login.setStatus(LoginStatus.ACTIVE);
		login.setPassword(getPasswordHash(uploadFileDTO.getUsername(), uploadFileDTO.getPassword()));
		login.setRoles(uploadFileDTO.getRoles());
		login.setFirstName(uploadFileDTO.getFirstName());
		login.setLastName(uploadFileDTO.getLastName());
		login.setEmail(uploadFileDTO.getEmail());
		login.setOrganization(uploadFileDTO.getOrganization());
		if (login.getRoles() != null && (login.getRoles().contains("QA") || login.getRoles().contains("SECONDARYQA"))) {
			login.setReportEmails(Arrays.asList(uploadFileDTO.getEmail()));
		}
		login = loginRepository.save(login);
		return login;
	}

	private void createAgent(UploadFileDTO uploadFileDTO) {
		Agent agentFromDB = agentRepository.findOneById(uploadFileDTO.getUsername());
		if (agentFromDB == null) {
			agentFromDB = agentRepository.findOneById("donotdeleteagent");
			if (agentFromDB != null) {
				agentFromDB.setUsername(uploadFileDTO.getUsername());
				agentFromDB.setPartnerId(uploadFileDTO.getOrganization());
				agentFromDB.setFirstName(uploadFileDTO.getFirstName());
				agentFromDB.setLastName(uploadFileDTO.getLastName());
				agentFromDB.setSupervisorId(uploadFileDTO.getSupervisorId());
				agentFromDB.setAgentType(agentSkill.AUTO);
				AgentDTO agentDTO = new AgentDTO(agentFromDB);
				AgentDTO dto = agentService.createAgent(agentDTO);
				logger.debug("Agent [{}] created successfully.", dto.getId());
			}
		}
	}

	private void addAgentInTeam(UploadFileDTO uploadFileDTO) {
		String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
		Organization organization = organizationRepository
				.findOneById(XtaasUserUtils.getCurrentLoggedInUserObject().getOrganization());
		if (organization != null && (organization.getId().equalsIgnoreCase("DEMANDSHORE")
				|| organization.getId().equalsIgnoreCase("DEMANDSHOREAG")
				|| organization.getId().equalsIgnoreCase("SALESIFY"))) {
			if (uploadFileDTO.getSupervisorId() != null) {
				insertInTeam(uploadFileDTO.getUsername(), uploadFileDTO.getSupervisorId());
			}
		} else {
			if (!loggedInUserName.equalsIgnoreCase("gsupervisor")
					&& loggedInUserName.equalsIgnoreCase(uploadFileDTO.getSupervisorId())) {
				if (uploadFileDTO.getSupervisorId() != null) {
					insertInTeam(uploadFileDTO.getUsername(), uploadFileDTO.getSupervisorId());
				}
			} else if (loggedInUserName.equalsIgnoreCase("gsupervisor")) {
				if (uploadFileDTO.getSupervisorId() != null
						&& !uploadFileDTO.getSupervisorId().equalsIgnoreCase("gsupervisor")) {
					insertInTeam(uploadFileDTO.getUsername(), uploadFileDTO.getSupervisorId());
				}
				insertInTeam(uploadFileDTO.getUsername(), loggedInUserName);
			}
		}
	}

	private void insertInTeam(String userId, String supervisorId) {
		List<Team> teams = teamRepository.findBySupervisorId(supervisorId);
		Team team = null;
		if (!teams.isEmpty()) {
			team = teams.get(0);
		}
		if (team != null) {
			teamService.addAgent(team.getId(), userId);
		}
	}

	private void removeAgentFromTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.removeAgent(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void createQa(UploadFileDTO uploadFileDTO) {
		Qa qaFromDB = qaRepository.findOneById(uploadFileDTO.getUsername());
		if (qaFromDB == null) {
			qaFromDB = qaRepository.findQaById("donotdeleteqa");
			if (qaFromDB != null) {
				qaFromDB.setUsername(uploadFileDTO.getUsername());
				qaFromDB.setPartnerId(uploadFileDTO.getOrganization());
				qaFromDB.setFirstName(uploadFileDTO.getFirstName());
				qaFromDB.setLastName(uploadFileDTO.getLastName());
				QaDTO qaDTO = new QaDTO(qaFromDB);
				QaDTO qaDB = qaService.createQa(qaDTO);
				logger.debug("Qa [{}] created successfully.", qaDB.getId());
			}
		}
	}

	private void updateAgent(UploadFileDTO uploadFileDTO) {
		Agent agentFromDB = agentRepository.findOneById(uploadFileDTO.getUsername());
		if (agentFromDB != null) {
			if (uploadFileDTO.getSubAction() != null) {
				if (uploadFileDTO.getSubAction().equalsIgnoreCase("active")) {
					agentFromDB.setStatus(AgentStatus.ACTIVE);
					changePassword(agentFromDB.getId(), uploadFileDTO.getPassword(), "active");
				} else if (uploadFileDTO.getSubAction().equalsIgnoreCase("inactive")) {
					agentFromDB.setStatus(AgentStatus.INACTIVE);
					changePassword(agentFromDB.getId(), uploadFileDTO.getPassword(), "inactive");
				}
			}
			// update Agent's firstName & lastName
			if (!StringUtils.isEmpty(uploadFileDTO.getFirstName())) {
				agentFromDB.setFirstName(uploadFileDTO.getFirstName());
			}
			if (!StringUtils.isEmpty(uploadFileDTO.getLastName())) {
				agentFromDB.setLastName(uploadFileDTO.getLastName());
			}
			Agent agentDB = agentRepository.save(agentFromDB);
			logger.debug("Agent [{}] details updated successfully.", agentDB.getId());
		}
	}

	private void updateQa(UploadFileDTO uploadFileDTO) {
		Qa qaFromDB = qaRepository.findOneById(uploadFileDTO.getUsername());
		if (qaFromDB != null) {
			if (uploadFileDTO.getSubAction() != null) {
				if (uploadFileDTO.getSubAction().equalsIgnoreCase("active")) {
					qaFromDB.setStatus(Status.ACTIVE);
					changePassword(qaFromDB.getId(), uploadFileDTO.getPassword(), "active");
				} else if (uploadFileDTO.getSubAction().equalsIgnoreCase("inactive")) {
					qaFromDB.setStatus(Status.INACTIVE);
					changePassword(qaFromDB.getId(), uploadFileDTO.getPassword(), "inactive");
				}
			}
			// update QA's firstName & lastName
			if (!StringUtils.isEmpty(uploadFileDTO.getFirstName())) {
				qaFromDB.setFirstName(uploadFileDTO.getFirstName());
			}
			if (!StringUtils.isEmpty(uploadFileDTO.getLastName())) {
				qaFromDB.setLastName(uploadFileDTO.getLastName());
			}
			Qa qaDB = qaRepository.save(qaFromDB);
			logger.debug("Qa [{}] details updated successfully.", qaDB.getId());
		}
	}

	private void addQaInTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.addQa(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void removeQaFromTeam(UploadFileDTO uploadFileDTO) {
		if (uploadFileDTO.getSupervisorId() != null) {
			List<Team> teams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
			Team team = null;
			if (!teams.isEmpty()) {
				team = teams.get(0);
			}
			if (team != null) {
				teamService.removeQa(team.getId(), uploadFileDTO.getUsername());
			}
		}
	}

	private void updateUserDetails(UploadFileDTO uploadFileDTO) {
		Login login = loginRepository.findOneById(uploadFileDTO.getUsername());
		if (!StringUtils.isEmpty(uploadFileDTO.getFirstName())) {
			login.setFirstName(uploadFileDTO.getFirstName());
		}
		if (!StringUtils.isEmpty(uploadFileDTO.getLastName())) {
			login.setLastName(uploadFileDTO.getLastName());
		}
		if (!StringUtils.isEmpty(uploadFileDTO.getEmail())) {
			login.setEmail(uploadFileDTO.getEmail());
		}
		if (uploadFileDTO.getResetMFA() != null && !uploadFileDTO.getResetMFA().isEmpty()) {
			if (uploadFileDTO.getResetMFA().equalsIgnoreCase("yes")) {
				login.setActivateMFAUser(false);
				login.setUserLock(false);
				login.setNoOfAttempt(0);
			} else if (uploadFileDTO.getResetMFA().equalsIgnoreCase("no")) {
				login.setActivateMFAUser(true);
			}
		}
		loginRepository.save(login);
	}

	private void changePassword(String id, String password, String status) {
		Login login = loginRepository.findOneById(id);
		if (status.equalsIgnoreCase("active")) {
			login.setPassword(getPasswordHash(id, password));
			login.setStatus(LoginStatus.ACTIVE);
		} else if (status.equalsIgnoreCase("inactive")) {
			login.setPassword("TERMINATE");
			login.setStatus(LoginStatus.INACTIVE);
		}
		login = loginRepository.save(login);
	}

	private String getPasswordHash(String username, String password) {
		// set username as a password if password is blank
		if (StringUtils.isEmpty(password)) {
			password = username;
		}
		StringBuilder passwordWithSalt = new StringBuilder(password).append("{").append(username).append("}");
		return EncryptionUtils.generateMd5Hash(passwordWithSalt.toString());
	}

	private boolean isUniqueInLogin(String userName) {
		return (loginRepository.findUser(userName.toLowerCase()) == null) ? false : true;
	}

	private void moveAgentToNewTeam(UploadFileDTO uploadFileDTO) {
		List<Team> currentTeams = teamRepository.findAllTeamsByAgentId(uploadFileDTO.getUsername());
		if (uploadFileDTO.getSupervisorId() != null) {
			if (currentTeams != null && currentTeams.size() > 0) {
				for (Team team : currentTeams) {
					if (team != null) {
						teamService.removeAgent(team.getId(), uploadFileDTO.getUsername());
					}
				}
			}
			insertInTeam(uploadFileDTO.getUsername(), uploadFileDTO.getSupervisorId());
		}
	}

	private String checkNewSupervisorInSameOrganization (UploadFileDTO uploadFileDTO) {
		List<Team> supervisorTeams = teamRepository.findBySupervisorId(uploadFileDTO.getSupervisorId());
		boolean teamFound = false;
		if (supervisorTeams != null && supervisorTeams.size() > 0) {
			for (Team team : supervisorTeams) {
				if (team.getPartnerId().equalsIgnoreCase(uploadFileDTO.getOrganization())) {
					teamFound = true;
					break;
				}
			}
		}
		if (teamFound) {
			return null;
		} else {
			return "No Team found for Supervisor: " + uploadFileDTO.getSupervisorId() + " in Organization: "
						+ uploadFileDTO.getOrganization();
		}
	}

	public class UploadFileDTO {
		public String username;
		public String password;
		public List<String> roles;
		public String firstName;
		public String lastName;
		public String organization;
		public String organizationEmail;
		public String email;
		public String action;
		public String subAction;
		public String supervisorId;
		public String teamAction;
		public String resetMFA;
		
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username.toLowerCase();
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public List<String> getRoles() {
			return roles;
		}

		public void setRoles(List<String> roles) {
			this.roles = roles;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getOrganization() {
			return organization;
		}

		public void setOrganization(String organization) {
			this.organization = organization;
		}
		
		public String getOrganizationEmail() {
			return organizationEmail;
		}

		public void setOrganizationEmail(String organizationEmail) {
			this.organizationEmail = organizationEmail;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action.toLowerCase();
		}

		public String getSubAction() {
			return subAction;
		}

		public void setSubAction(String subAction) {
			this.subAction = subAction.toLowerCase();
		}

		public String getSupervisorId() {
			return supervisorId;
		}

		public void setSupervisorId(String supervisorId) {
			this.supervisorId = supervisorId;
		}

		public String getTeamAction() {
			return teamAction;
		}

		public void setTeamAction(String teamAction) {
			this.teamAction = teamAction.toLowerCase();
		}

		public String getResetMFA() {
			return resetMFA;
		}

		public void setResetMFA(String resetMFA) {
			this.resetMFA = resetMFA;
		}
	}
}
