package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Domain;
import com.xtaas.db.entity.Vertical;
import com.xtaas.db.repository.VerticalRepository;
import com.xtaas.valueobjects.KeyValuePair;

@Service
public class VerticalServiceImpl implements VerticalService {
	@Autowired
	private VerticalRepository verticalRepository;
	
	@Override
	public List<Vertical> getVericals() {
		return verticalRepository.find();
	}

	@Override
	public List<KeyValuePair<String, ArrayList<Domain>>> getDomains(List<String> verticalList) {
		List<KeyValuePair<String, ArrayList<Domain>>> verticalDomainList = new ArrayList<KeyValuePair<String, ArrayList<Domain>>>();
		List<Vertical> verticals = verticalRepository.getByVertical(verticalList);
		for (Vertical vertical : verticals) {
			KeyValuePair<String, ArrayList<Domain>> verticalDomainPair = new KeyValuePair<String, ArrayList<Domain>>();
			verticalDomainPair.setKey(vertical.getName());
			verticalDomainPair.setValue(vertical.getDomains());
			verticalDomainList.add(verticalDomainPair);
		}
		return verticalDomainList;
	}

}
