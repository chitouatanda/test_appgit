package com.xtaas.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.GlobalContactCampaignStatus;
import com.xtaas.db.entity.GlobalContactInteraction;
import com.xtaas.db.entity.GlobalContactProspectStatus;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.GlobalContactCampaignStatusRepository;
import com.xtaas.db.repository.GlobalContactInteractionRepository;
import com.xtaas.db.repository.GlobalContactProspectStatusRepository;
import com.xtaas.db.repository.GlobalContactRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.web.dto.IdNameDTO;

@Service
public class GlobalContactServiceImpl implements GlobalContactService {

	private static final Logger logger = LoggerFactory.getLogger(GlobalContactServiceImpl.class);

	private static enum STATUS {
		QUEUED, INPROGRESS, ERROR, COMPLETED
	}

	@Autowired
	private GlobalContactRepository globalContactRepository;

	@Autowired
	private GlobalContactInteractionRepository globalContactInteractionRepository;
	
	@Autowired
	private PCIAnalysisRepository pCIAnalysisRepository;

	@Autowired
	private ProspectCallLogRepository prospectLogRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private GlobalContactCampaignStatusRepository globalContactCampaignStatusRepository;

	@Autowired
	private GlobalContactProspectStatusRepository globalContactProspectStatusRepository;

	@Async
	@Override
	public void generateGlobalContact() {
		logger.debug("Started generateGlobalContact.");
		List<ProspectCallLog> callLogs = new ArrayList<>();

		// fetch list of Campaign in ASC order on updatedDate
		List<Order> campaignSortOrders = new ArrayList<Order>();
		campaignSortOrders.add(new Order(Direction.ASC, "updatedDate"));
		List<IdNameDTO> campaigns = campaignRepository.findAllIdsAndNames(Sort.by(campaignSortOrders));

		int totalProspectCount = 0;
		for (IdNameDTO idNameDTO : campaigns) {
			GlobalContactCampaignStatus globalContactCampaignStatus = globalContactCampaignStatusRepository
					.findByCampaignId(idNameDTO.getId());
			if (globalContactCampaignStatus == null) {
				globalContactCampaignStatus = saveCampaignStatus(globalContactCampaignStatus, STATUS.QUEUED.toString(),
						idNameDTO.getId(), idNameDTO.getName(), totalProspectCount, null, true);
			}
		}

		// fetch list of GlobalContactCampaignStatus in DESC order on updatedDate
		List<Order> globalContactcampaignStatusSortOrders = new ArrayList<Order>();
		globalContactcampaignStatusSortOrders.add(new Order(Direction.DESC, "updatedDate"));
		List<String> statuses = Arrays.asList(STATUS.QUEUED.toString(), STATUS.INPROGRESS.toString());
		List<GlobalContactCampaignStatus> globalContactCampaignStatusList = globalContactCampaignStatusRepository
				.findByStatuses(statuses, Sort.by(globalContactcampaignStatusSortOrders));

		for (GlobalContactCampaignStatus globalContactCampaignStatus : globalContactCampaignStatusList) {
			String campaignErrorMessage = null;
			try {
				totalProspectCount = prospectLogRepository
						.countByCampaignId(globalContactCampaignStatus.getCampaignId());
				logger.debug("generateGlobalContact - Found [{}] total prospects for [{}]", totalProspectCount,
						globalContactCampaignStatus.getName());
				globalContactCampaignStatus = saveCampaignStatus(globalContactCampaignStatus,
						STATUS.INPROGRESS.toString(), "", "", totalProspectCount, null, false);
				int maxPageSize = 0;
				if (totalProspectCount != 0 && totalProspectCount > 5000) {
					maxPageSize = totalProspectCount / 5000;
				}
				for (int i = 0; i <= maxPageSize; i++) {
					try {
						Pageable pageable = PageRequest.of(i, 5000, Direction.DESC, "updatedDate");
						callLogs = prospectLogRepository
								.findByCampaignIdPageable(globalContactCampaignStatus.getCampaignId(), pageable);
						logger.debug(
								"generateGlobalContact - Inserting [{}] prospects into GlobalContact for [{}] campaign",
								callLogs.size(), globalContactCampaignStatus.getName());
						if (callLogs != null && callLogs.size() > 0) {
							generateGlobalContact(callLogs, globalContactCampaignStatus);
						}
					} catch (Exception ex) {
						campaignErrorMessage = ex.getMessage();
						globalContactCampaignStatus = saveCampaignStatus(globalContactCampaignStatus,
								STATUS.ERROR.toString(), "", "", totalProspectCount, campaignErrorMessage, false);
						logger.error(
								"generateGlobalContact - Error occurred while fetching prospects for [{}] campaign.",
								globalContactCampaignStatus.getName());
					}
				}
				globalContactCampaignStatus = saveCampaignStatus(globalContactCampaignStatus,
						STATUS.COMPLETED.toString(), "", "", totalProspectCount, campaignErrorMessage, false);
			} catch (Exception ex) {
				campaignErrorMessage = ex.getMessage();
				globalContactCampaignStatus = saveCampaignStatus(globalContactCampaignStatus, STATUS.ERROR.toString(),
						"", "", totalProspectCount, campaignErrorMessage, false);
				logger.error("generateGlobalContact - Error occurred while fetching prospects for [{}] campaign.",
						globalContactCampaignStatus.getName());
			}
		}
		logger.debug("Finish generateGlobalContact.");
	}

	private void generateGlobalContact(List<ProspectCallLog> callLogs,
			GlobalContactCampaignStatus globalContactCampaignStatus) {
		for (ProspectCallLog callLog : callLogs) {
			try {
				if (callLog.getProspectCall() != null && callLog.getProspectCall().getProspect() != null) {
					long uninqueRecordCount = countUniqueRecordByFLP(callLog.getProspectCall());
					if (uninqueRecordCount == 0) {
						GlobalContact globalContact = new GlobalContact();
						globalContact = globalContact.toGlobalContact(callLog.getProspectCall());
						// save new GlobalContact & GlobalContactInteraction
						saveGlobalContactAndInteraction(globalContact, true);
					}
				}
			} catch (Exception ex) {
				saveProspectStatus(new GlobalContactProspectStatus(), STATUS.ERROR.toString(),
						globalContactCampaignStatus.getCampaignId(), callLog.getProspectCall().getProspectCallId(),
						ex.getMessage(), true);
				logger.error("generateGlobalContact - Error occurred while inserting prospect [{}] into GlobalContact.",
						callLog.getProspectCall().getProspectCallId());
			}
		}
	}
	
	@Override
	public void pciAnalysisSave(List<PCIAnalysis> pcianList){
		pCIAnalysisRepository.saveAll(pcianList);
	}

	@Override
	public void generateGlobalContact(List<ProspectCallLog> callLogs) {
		for (ProspectCallLog callLog : callLogs) {
			try {
				if (callLog.getProspectCall() != null && callLog.getProspectCall().getProspect() != null) {
					long uninqueRecordCount = countUniqueRecordByFLP(callLog.getProspectCall());
					if (uninqueRecordCount == 0) {
						GlobalContact globalContact = new GlobalContact();
						globalContact = globalContact.toGlobalContact(callLog.getProspectCall());
						// save new GlobalContact & GlobalContactInteraction
						saveGlobalContactAndInteraction(globalContact, true);
					} else {
						logger.debug("Already in global contact skipping");
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private GlobalContactCampaignStatus saveCampaignStatus(GlobalContactCampaignStatus campaignStatus, String status,
			String campaignId, String campaignName, int totalProspectCount, String errorReason, boolean isNew) {
		if (isNew) {
			campaignStatus = new GlobalContactCampaignStatus();
			campaignStatus.setCampaignId(campaignId);
			campaignStatus.setName(campaignName);
		}
		campaignStatus.setTotalProspectCount(totalProspectCount);
		campaignStatus.setStatus(status);
		campaignStatus.setErrorReason(errorReason);
		return globalContactCampaignStatusRepository.save(campaignStatus);
	}

	private GlobalContactProspectStatus saveProspectStatus(GlobalContactProspectStatus prospectStatus, String status,
			String campaignId, String prospectCallId, String errorReason, boolean isNew) {
		if (isNew) {
			prospectStatus = new GlobalContactProspectStatus();
			prospectStatus.setProspectCallId(prospectCallId);
			prospectStatus.setCampaignId(campaignId);
		}
		prospectStatus.setStatus(status);
		prospectStatus.setErrorReason(errorReason);
		return globalContactProspectStatusRepository.save(prospectStatus);
	}

	@Override
	public GlobalContact saveGlobalContactAndInteraction(GlobalContact globalContact,
			boolean createGlobalContactInteraction) {
		GlobalContact savedGlobalContact = null;
		try {
			savedGlobalContact = globalContactRepository.save(globalContact);
		} catch (Exception e) {
			logger.error("Error occurred while saving GlobalContact");
			GlobalContact latestGlobalContact = globalContactRepository.findOneById(globalContact.getGlobalContactId());
			if (latestGlobalContact != null) {
				latestGlobalContact.setChangeLog(globalContact.getChangeLog());
				savedGlobalContact = globalContactRepository.save(latestGlobalContact);
			}
		}
		if (createGlobalContactInteraction && savedGlobalContact != null) {
			GlobalContactInteraction globalContactInteraction = new GlobalContactInteraction();
			globalContactInteraction = globalContactInteraction.toGlobalContactInteraction(savedGlobalContact);
			globalContactInteractionRepository.save(globalContactInteraction);
		}
		return savedGlobalContact;
	}

	@Override
	public GlobalContact checkContactIdentity(GlobalContact globalContact, GlobalContact newGlobalContact) {
		if (globalContact != null && newGlobalContact != null) {
			// read changeLog into new string
			String oldChangeLog = globalContact.getChangeLog();
			if (oldChangeLog == null) {
				oldChangeLog = "";
			}
			// reset changeLog to empty string
			globalContact.setChangeLog("");
			if ((globalContact.getFirstName() != null && newGlobalContact.getFirstName() != null
					&& !newGlobalContact.getFirstName().isEmpty())
					&& !globalContact.getFirstName().equalsIgnoreCase(newGlobalContact.getFirstName())) {
				globalContact.setFirstName(newGlobalContact.getFirstName());
				globalContact.setChangeLog(globalContact.getChangeLog() + "FirstName ");
			}
			if ((globalContact.getLastName() != null && newGlobalContact.getLastName() != null
					&& !newGlobalContact.getLastName().isEmpty())
					&& !globalContact.getLastName().equalsIgnoreCase(newGlobalContact.getLastName())) {
				globalContact.setLastName(newGlobalContact.getLastName());
				globalContact.setChangeLog(globalContact.getChangeLog() + "LastName ");
			}
			if ((globalContact.getEmail() != null && newGlobalContact.getEmail() != null
					&& !newGlobalContact.getEmail().isEmpty())
					&& !globalContact.getEmail().equalsIgnoreCase(newGlobalContact.getEmail())) {
				globalContact.setEmail(newGlobalContact.getEmail());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Email ");
			}
			if ((globalContact.getPhone() != null && newGlobalContact.getPhone() != null
					&& !newGlobalContact.getPhone().isEmpty())
					&& !globalContact.getPhone().equalsIgnoreCase(newGlobalContact.getPhone())) {
				globalContact.setPhone(newGlobalContact.getPhone());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Phone ");
			}
			if ((globalContact.getCompanyPhone() != null && newGlobalContact.getCompanyPhone() != null
					&& !newGlobalContact.getCompanyPhone().isEmpty())
					&& !globalContact.getCompanyPhone().equalsIgnoreCase(newGlobalContact.getCompanyPhone())) {
				globalContact.setCompanyPhone(newGlobalContact.getCompanyPhone());
				globalContact.setChangeLog(globalContact.getChangeLog() + "CompanyPhone ");
			}
			if ((globalContact.getExtension() != null && newGlobalContact.getExtension() != null
					&& !newGlobalContact.getExtension().isEmpty())
					&& !globalContact.getExtension().equalsIgnoreCase(newGlobalContact.getExtension())) {
				globalContact.setExtension(newGlobalContact.getExtension());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Extension ");
			}
			if ((globalContact.getTitle() != null && newGlobalContact.getTitle() != null
					&& !newGlobalContact.getTitle().isEmpty())
					&& !globalContact.getTitle().equalsIgnoreCase(newGlobalContact.getTitle())) {
				globalContact.setTitle(newGlobalContact.getTitle());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Title ");
			}
			if ((globalContact.getDepartment() != null && newGlobalContact.getDepartment() != null
					&& !newGlobalContact.getDepartment().isEmpty())
					&& !globalContact.getDepartment().equalsIgnoreCase(newGlobalContact.getDepartment())) {
				globalContact.setDepartment(newGlobalContact.getDepartment());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Department ");
			}
			if ((globalContact.getManagementLevel() != null && newGlobalContact.getManagementLevel() != null
					&& !newGlobalContact.getManagementLevel().isEmpty())
					&& !globalContact.getManagementLevel().equalsIgnoreCase(newGlobalContact.getManagementLevel())) {
				globalContact.setManagementLevel(newGlobalContact.getManagementLevel());
				globalContact.setChangeLog(globalContact.getChangeLog() + "ManagementLevel ");
			}
			int minRevenue = Double.compare(globalContact.getMinRevenue(), newGlobalContact.getMinRevenue());
			if (minRevenue > 0 || minRevenue < 0) {
				globalContact.setMinRevenue(newGlobalContact.getMinRevenue());
				globalContact.setChangeLog(globalContact.getChangeLog() + "MinRevenue ");
			}
			int maxRevenue = Double.compare(globalContact.getMaxRevenue(), newGlobalContact.getMaxRevenue());
			if (maxRevenue > 0 || maxRevenue < 0) {
				globalContact.setMaxRevenue(newGlobalContact.getMaxRevenue());
				globalContact.setChangeLog(globalContact.getChangeLog() + "MaxRevenue ");
			}
			int minEmployee = Double.compare(globalContact.getMinEmployeeCount(),
					newGlobalContact.getMinEmployeeCount());
			if (minEmployee > 0 || minEmployee < 0) {
				globalContact.setMinEmployeeCount(newGlobalContact.getMinEmployeeCount());
				globalContact.setChangeLog(globalContact.getChangeLog() + "MinEmployeeCount ");
			}
			int maxEmployee = Double.compare(globalContact.getMaxEmployeeCount(),
					newGlobalContact.getMaxEmployeeCount());
			if (maxEmployee > 0 || maxEmployee < 0) {
				globalContact.setMaxEmployeeCount(newGlobalContact.getMaxEmployeeCount());
				globalContact.setChangeLog(globalContact.getChangeLog() + "MaxEmployeeCount ");
			}
			if ((globalContact.getDomain() != null && newGlobalContact.getDomain() != null
					&& !newGlobalContact.getDomain().isEmpty())
					&& !globalContact.getDomain().equalsIgnoreCase(newGlobalContact.getDomain())) {
				globalContact.setDomain(newGlobalContact.getDomain());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Domain ");
			}
			if ((globalContact.getOrganizationName() != null && newGlobalContact.getOrganizationName() != null
					&& !newGlobalContact.getOrganizationName().isEmpty())
					&& !globalContact.getOrganizationName().equalsIgnoreCase(newGlobalContact.getOrganizationName())) {
				globalContact.setOrganizationName(newGlobalContact.getOrganizationName());
				globalContact.setChangeLog(globalContact.getChangeLog() + "OrganizationName ");
			}
			if ((globalContact.getAddressLine1() != null && newGlobalContact.getAddressLine1() != null
					&& !newGlobalContact.getAddressLine1().isEmpty())
					&& !globalContact.getAddressLine1().equalsIgnoreCase(newGlobalContact.getAddressLine1())) {
				globalContact.setAddressLine1(newGlobalContact.getAddressLine1());
				globalContact.setChangeLog(globalContact.getChangeLog() + "AddressLine1 ");
			}
			if ((globalContact.getAddressLine2() != null && newGlobalContact.getAddressLine2() != null
					&& !newGlobalContact.getAddressLine2().isEmpty())
					&& !globalContact.getAddressLine2().equalsIgnoreCase(newGlobalContact.getAddressLine2())) {
				globalContact.setAddressLine2(newGlobalContact.getAddressLine2());
				globalContact.setChangeLog(globalContact.getChangeLog() + "AddressLine2 ");
			}
			if ((globalContact.getCity() != null && newGlobalContact.getCity() != null
					&& !newGlobalContact.getCity().isEmpty())
					&& !globalContact.getCity().equalsIgnoreCase(newGlobalContact.getCity())) {
				globalContact.setCity(newGlobalContact.getCity());
				globalContact.setChangeLog(globalContact.getChangeLog() + "City ");
			}
			if ((globalContact.getState() != null && newGlobalContact.getState() != null
					&& !newGlobalContact.getState().isEmpty())
					&& !globalContact.getState().equalsIgnoreCase(newGlobalContact.getState())) {
				globalContact.setState(newGlobalContact.getState());
				globalContact.setChangeLog(globalContact.getChangeLog() + "State ");
			}
			if ((globalContact.getStateCode() != null && newGlobalContact.getStateCode() != null
					&& !newGlobalContact.getStateCode().isEmpty())
					&& !globalContact.getStateCode().equalsIgnoreCase(newGlobalContact.getStateCode())) {
				globalContact.setStateCode(newGlobalContact.getStateCode());
				globalContact.setChangeLog(globalContact.getChangeLog() + "StateCode ");
			}
			if ((globalContact.getCountry() != null && newGlobalContact.getCountry() != null
					&& !newGlobalContact.getCountry().isEmpty())
					&& !globalContact.getCountry().equalsIgnoreCase(newGlobalContact.getCountry())) {
				globalContact.setCountry(newGlobalContact.getCountry());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Country ");
			}
			if ((globalContact.getCountryCode() != null && newGlobalContact.getCountryCode() != null
					&& !newGlobalContact.getCountryCode().isEmpty())
					&& !globalContact.getCountryCode().equalsIgnoreCase(newGlobalContact.getCountryCode())) {
				globalContact.setCountryCode(newGlobalContact.getCountryCode());
				globalContact.setChangeLog(globalContact.getChangeLog() + "CountryCode ");
			}
			if ((globalContact.getPostalCode() != null && newGlobalContact.getPostalCode() != null
					&& !newGlobalContact.getPostalCode().isEmpty())
					&& !globalContact.getPostalCode().equalsIgnoreCase(newGlobalContact.getPostalCode())) {
				globalContact.setPostalCode(newGlobalContact.getPostalCode());
				globalContact.setChangeLog(globalContact.getChangeLog() + "PostalCode ");
			}
			if ((globalContact.getCompanyAddress() != null && newGlobalContact.getCompanyAddress() != null
					&& !newGlobalContact.getCompanyAddress().isEmpty())
					&& !globalContact.getCompanyAddress().toString()
							.equalsIgnoreCase(newGlobalContact.getCompanyAddress().toString())) {
				globalContact.setCompanyAddress(newGlobalContact.getCompanyAddress());
				globalContact.setChangeLog(globalContact.getChangeLog() + "CompanyAddress ");
			}
			if ((globalContact.getIndustries() != null && newGlobalContact.getIndustries() != null
					&& !newGlobalContact.getIndustries().isEmpty())
					&& !globalContact.getIndustries().toString()
							.equalsIgnoreCase(newGlobalContact.getIndustries().toString())) {
				globalContact.setIndustries(newGlobalContact.getIndustries());
				globalContact.setChangeLog(globalContact.getChangeLog() + "Industries ");
			}
			if ((globalContact.getZoomPersonUrl() != null && newGlobalContact.getZoomPersonUrl() != null
					&& !newGlobalContact.getZoomPersonUrl().isEmpty())
					&& !globalContact.getZoomPersonUrl().equalsIgnoreCase(newGlobalContact.getZoomPersonUrl())) {
				globalContact.setZoomPersonUrl(newGlobalContact.getZoomPersonUrl());
				globalContact.setChangeLog(globalContact.getChangeLog() + "PersonUrl ");
			}
			if ((globalContact.getZoomCompanyUrl() != null && newGlobalContact.getZoomCompanyUrl() != null
					&& !newGlobalContact.getZoomCompanyUrl().isEmpty())
					&& !globalContact.getZoomCompanyUrl().equalsIgnoreCase(newGlobalContact.getZoomCompanyUrl())) {
				globalContact.setZoomCompanyUrl(newGlobalContact.getZoomCompanyUrl());
				globalContact.setChangeLog(globalContact.getChangeLog() + "CompanyUrl ");
			}
			// if there are no changes in contact then set old changeLog
			if (globalContact.getChangeLog().isEmpty()) {
				globalContact.setChangeLog(oldChangeLog);
			}
		}
		return globalContact;
	}

	@Override
	public long countUniqueRecordByFLP(ProspectCall prospectCall) {
		String firstName = prospectCall.getProspect().getFirstName();
		String lastName = prospectCall.getProspect().getLastName();
		String phone = prospectCall.getProspect().getPhone();
		if (firstName == null || firstName.isEmpty()) {
			if (lastName != null && !lastName.isEmpty()) {
				firstName = lastName;
				prospectCall.getProspect().setFirstName(lastName);
			}
		} else if (lastName == null || lastName.isEmpty()) {
			lastName = firstName;
			prospectCall.getProspect().setLastName(firstName);
		}
		return globalContactRepository.countUniqueRecordByFLP(firstName, lastName, phone);
	}

	@Override
	public GlobalContact findUniqueRecordByFLP(ProspectCall prospectCall) {
		String firstName = prospectCall.getProspect().getFirstName();
		String lastName = prospectCall.getProspect().getLastName();
		String phone = prospectCall.getProspect().getPhone();
		if (firstName == null || firstName.isEmpty()) {
			if (lastName != null && !lastName.isEmpty()) {
				firstName = lastName;
				prospectCall.getProspect().setFirstName(lastName);
			}
		} else if (lastName == null || lastName.isEmpty()) {
			lastName = firstName;
			prospectCall.getProspect().setLastName(firstName);
		}
		return globalContactRepository.findUniqueRecordByFLP(firstName, lastName, phone);
	}

	@Override
	public GlobalContact findUniqueRecordByFLP(String firstName, String lastName, String phone) {
		return globalContactRepository.findUniqueRecordByFLP(firstName, lastName, phone);
	}

}
