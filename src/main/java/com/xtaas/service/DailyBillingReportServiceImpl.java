package com.xtaas.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.ConnectDetailsDTO;
import com.xtaas.web.dto.DailyBillingDTO;
import com.xtaas.web.dto.VoiceUsageDetailsDTO;

@Service
public class DailyBillingReportServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(DailyBillingReportServiceImpl.class);

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	private ProspectCallInteractionService prospectCallInteractionService;

	public static final String ACCOUNT_SID = "AC517a9586ed1afe81d4fda44f90778d53";
	public static final String AUTH_TOKEN = "e9eda3a5a24028435e472992be403dc1";

	public void createBillingReports() {
		try {
			createDailyBillingReport();
			createMonthlyBillingReport();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createMonthlyBillingReport() throws IOException {
		logger.debug("createMonthlyBillingReport() : fetching data for monthtodate report");
		DailyBillingDTO billingDTO = new DailyBillingDTO();
		billingDTO = prospectCallInteractionService.getConnectData("monthly");
		List<VoiceUsageDetailsDTO> voiceUsageDetailsDTO = getVoiceUsageDetails();
		if (voiceUsageDetailsDTO != null) {
			billingDTO.setVoiceUsageDetailsDTO(voiceUsageDetailsDTO);
		}
		createAndSendReport(billingDTO, "monthly");
	}

	public void createDailyBillingReport() throws IOException {
		logger.debug("createDailyBillingReport() : fetching data for daily report.");
		DailyBillingDTO billingDTO = new DailyBillingDTO();
		billingDTO = prospectCallInteractionService.getConnectData("daily");
		List<VoiceUsageDetailsDTO> voiceUsageDetailsDTO = getDailyVoiceUsageDetails();
		if (voiceUsageDetailsDTO != null) {
			billingDTO.setVoiceUsageDetailsDTO(voiceUsageDetailsDTO);
		}
		createAndSendReport(billingDTO, "daily");
	}

	private void writeConnectHeaders(Row headerRow, XSSFCellStyle labelCellStyle) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Camapign Name");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(1);
		cell.setCellValue("Attempts");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(2);
		cell.setCellValue("Contact");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(3);
		cell.setCellValue("Connects");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(4);
		cell.setCellValue("Success");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(5);
		cell.setCellValue("Cost per Connect");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(6);
		cell.setCellValue("Connect Cost");
		cell.setCellStyle(labelCellStyle);
	}

	private void writeConnectToExcel(ConnectDetailsDTO connectDto, DecimalFormat decimalFormat, Row row,
			Workbook workbook) {
		Cell cell = row.createCell(0);
		cell.setCellValue(connectDto.getCampaignName());
		cell = row.createCell(1);
		cell.setCellValue(connectDto.getRecordsAttempted());
		cell = row.createCell(2);
		cell.setCellValue(connectDto.getContacted());
		cell = row.createCell(3);
		cell.setCellValue(connectDto.getConnected());
		cell = row.createCell(4);
		cell.setCellValue(connectDto.getSuccessRecords());
		cell = row.createCell(5);
		cell.setCellValue(connectDto.getCostPerConnect());
		cell = row.createCell(6);
		cell.setCellValue("$	" + decimalFormat.format(connectDto.getConnectCost()));
	}

	private void writeVoiceDetailsHeaders(Row headerRow, XSSFCellStyle labelCellStyle) {
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Item");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(1);
		cell.setCellValue("Quantity");
		cell.setCellStyle(labelCellStyle);
		cell = headerRow.createCell(2);
		cell.setCellValue("Cost");
		cell.setCellStyle(labelCellStyle);
	}

	private void writeVoiceDetailsToExcel(VoiceUsageDetailsDTO voiceDetails, Row row, Workbook workbook,
			DecimalFormat decimalFormat) {
		Cell cell = row.createCell(0);
		cell.setCellValue(voiceDetails.getCategory().toString());
		cell = row.createCell(1);
		cell.setCellValue(voiceDetails.getUsage());
		cell = row.createCell(2);
		cell.setCellValue("$	" + decimalFormat.format(voiceDetails.getPrice().doubleValue()));
	}

	public List<VoiceUsageDetailsDTO> getVoiceUsageDetails() throws IOException {
		logger.debug("getVoiceUsageDetails() : start");
		URL url = null;
		String serverUrl = null;
		StringBuilder serverBaseUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl());
		if (serverBaseUrl.toString().contains("xtaascorp")) {
			url = new URL(XtaasConstants.XTAASCORP_URL + "/api/monthtodatebilling");
		} else if (serverBaseUrl.toString().contains("xtaascorp2")) {
			url = new URL(XtaasConstants.XTAASCORP2_URL + "/api/monthtodatebilling");
		} else if (serverBaseUrl.toString().contains("demandshore")) {
			url = new URL(XtaasConstants.DEMANDSHORE_URL + "/api/monthtodatebilling");
		} else {
			url = new URL("http://localhost:8081/api/monthtodatebilling");
		}

		List<VoiceUsageDetailsDTO> usageDetailsDTOs = new ArrayList<>();
		try {
			url = new URL(url.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		int status = con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		List<VoiceUsageDetailsDTO> voiceUsageDetailsDTOs = JSONUtils.toObject(content.toString(), List.class);
		if (voiceUsageDetailsDTOs != null && voiceUsageDetailsDTOs.size() > 0) {
			for (int i = 0; i < voiceUsageDetailsDTOs.size(); i++) {
				usageDetailsDTOs.add(
						JSONUtils.toObject(JSONUtils.toJson(voiceUsageDetailsDTOs.get(i)), VoiceUsageDetailsDTO.class));
			}
		}
		in.close();
		con.disconnect();
		logger.debug("getVoiceUsageDetails() : end");
		return usageDetailsDTOs;
	}

	public List<VoiceUsageDetailsDTO> getDailyVoiceUsageDetails() throws IOException {
		logger.debug("getDailyVoiceUsageDetails() : start");
		URL url = null;
		String serverUrl = null;
		StringBuilder serverBaseUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl());
		if (serverBaseUrl.toString().contains("xtaascorp")) {
			url = new URL(XtaasConstants.XTAASCORP_URL + "/api/dailybilling");
		} else if (serverBaseUrl.toString().contains("xtaascorp2")) {
			url = new URL(XtaasConstants.XTAASCORP2_URL + "/api/dailybilling");
		} else if (serverBaseUrl.toString().contains("demandshore")) {
			url = new URL(XtaasConstants.DEMANDSHORE_URL + "/api/dailybilling");
		} else {
			url = new URL("http://localhost:8081/api/dailybilling");
		}
		List<VoiceUsageDetailsDTO> usageDetailsDTOs = new ArrayList<>();
		try {
			url = new URL(url.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		int status = con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		List<VoiceUsageDetailsDTO> voiceUsageDetailsDTOs = JSONUtils.toObject(content.toString(), List.class);
		if (voiceUsageDetailsDTOs != null && voiceUsageDetailsDTOs.size() > 0) {
			for (int i = 0; i < voiceUsageDetailsDTOs.size(); i++) {
				usageDetailsDTOs.add(
						JSONUtils.toObject(JSONUtils.toJson(voiceUsageDetailsDTOs.get(i)), VoiceUsageDetailsDTO.class));
			}
		}
		in.close();
		con.disconnect();
		logger.debug("getDailyVoiceUsageDetails() : end");
		return usageDetailsDTOs;
	}

	private void createAndSendReport(DailyBillingDTO billingDTO, String span) throws IOException {
		logger.debug("createAndSendReport() : creating excel for [{}]", span);
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		sheet.setColumnWidth(0, 8000);
		sheet.setColumnWidth(1, 4000);
		sheet.setColumnWidth(2, 4000);
		XSSFFont headerCellFont = workbook.createFont();
		headerCellFont.setFontHeightInPoints((short) 12);
		headerCellFont.setFontName("IMPACT");
		XSSFFont labelCellFont = workbook.createFont();
		labelCellFont.setFontHeightInPoints((short) 12);
		labelCellFont.setFontName("ARIAL");
		XSSFCellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFillForegroundColor(HSSFColorPredefined.YELLOW.getIndex());
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFont(headerCellFont);
		XSSFCellStyle labelCellStyle = workbook.createCellStyle();
		labelCellStyle.setFillForegroundColor(HSSFColorPredefined.LIGHT_YELLOW.getIndex());
		labelCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		labelCellStyle.setFont(labelCellFont);

		int rowCount = 0;
		if (billingDTO.getConnectDetailsDTOs() != null && billingDTO.getConnectDetailsDTOs().size() > 0) {
			logger.debug("writing connect details for [{}]", span);
			Row row = sheet.createRow(rowCount);
			Cell cell = row.createCell(0);
			cell.setCellValue("Connect Details :");
			cell.setCellStyle(headerCellStyle);
			rowCount++;
			row = sheet.createRow(rowCount);
			writeConnectHeaders(row, labelCellStyle);
			rowCount++;
			Iterator<ConnectDetailsDTO> iterator = billingDTO.getConnectDetailsDTOs().iterator();
			while (iterator.hasNext()) {
				Row newRow = sheet.createRow(rowCount);
				ConnectDetailsDTO connectDetailsDTO = iterator.next();
				writeConnectToExcel(connectDetailsDTO, decimalFormat, newRow, workbook);
				rowCount++;
			}
		}
		rowCount++;
		if (billingDTO.getVoiceUsageDetailsDTO() != null && billingDTO.getVoiceUsageDetailsDTO().size() > 0) {
			logger.debug("writing voice usage details for [{}]", span);
			Row row = sheet.createRow(rowCount);
			Cell cell = row.createCell(0);
			cell.setCellValue("VoIP Charges(Platform + Connect) :");
			cell.setCellStyle(headerCellStyle);
			rowCount++;
			row = sheet.createRow(rowCount);
			writeVoiceDetailsHeaders(row, labelCellStyle);
			rowCount++;
			Iterator<VoiceUsageDetailsDTO> iterator = billingDTO.getVoiceUsageDetailsDTO().iterator();
			while (iterator.hasNext()) {
				Row newRow = sheet.createRow(rowCount);
				VoiceUsageDetailsDTO usageDetailsDTO = iterator.next();
				writeVoiceDetailsToExcel(usageDetailsDTO, newRow, workbook, decimalFormat);
				rowCount++;
			}
		}
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		int totalRows = sheet.getPhysicalNumberOfRows();
		Row row = sheet.createRow(totalRows + 1);
		String fileName = null;
		String message = "Please find the attached file.";
		String subject = null;
		if (span.equalsIgnoreCase("monthly")) {
			fileName = "Spends file_" + dateStr + ".xlsx";
			subject = "Spends file";

		} else if (span.equalsIgnoreCase("daily")) {
			fileName = "Daily spends file_" + dateStr + ".xlsx";
			subject = "Daily spends file";
		}

		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		workbook.close();
		XtaasEmailUtils.sendEmail("sudhir.gholap@invimatic.com", "data@xtaascorp.com", subject, message,
				fileNames);
		logger.debug("[{}] spend report send to [{}] ", span, "sudhir.gholap@invimatic.com");
	}

}
