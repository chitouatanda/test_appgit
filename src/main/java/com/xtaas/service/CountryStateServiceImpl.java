package com.xtaas.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CountryState;
import com.xtaas.db.repository.CountryStateRepository;

@Service
public class CountryStateServiceImpl implements CountryStateService {

	private static final Logger logger = LoggerFactory.getLogger(CountryNewServiceImpl.class);

	Map<String, String> countryCodeAndNameMap = new HashMap<String, String>();

	@Autowired
	private CountryStateRepository countryStateRepository;

	@Override
	public List<String> getStatesOfCountry(String countryName) {
		List<String> stateNames = new ArrayList<String>();
			List<CountryState> countriesFromDB = countryStateRepository.findStatesByCountry(countryName);
			if (countriesFromDB != null && countriesFromDB.size() > 0) {
				stateNames = countriesFromDB.stream().map(country -> country.getStateName()).filter(Objects::nonNull)
						.collect(Collectors.toList());
		}
		return stateNames;
	}

	@Override
	public void updateCountryDetails() {
		// TODO Auto-generated method stub
	}

	@Override
	public Map<String, String> getCountryDetails() {
		List<CountryState> countryDetailsFromDB = countryStateRepository.findAllCountries();
		if (countryDetailsFromDB != null && countryDetailsFromDB.size() > 0) {
			for (CountryState countryState : countryDetailsFromDB) {
				countryCodeAndNameMap.put(countryState.getCountryIso2(), countryState.getCountry());
			}
		}
		return countryCodeAndNameMap;
	}

	@Override
	public List<String> getCountries() {
		List<String> countryNames = new ArrayList<String>();
		List<CountryState> countryDetailsFromDB = countryStateRepository.findAllCountries();
		if (countryDetailsFromDB != null && countryDetailsFromDB.size() > 0) {
			countryNames = countryDetailsFromDB.stream().map(country -> country.getCountry()).filter(Objects::nonNull)
						.collect(Collectors.toList());
		}
		return countryNames;
	}

}
