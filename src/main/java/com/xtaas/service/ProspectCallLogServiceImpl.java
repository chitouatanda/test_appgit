/**
 *
 */
package com.xtaas.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.application.service.OutboundNumberService;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.application.service.TeamService;
import com.xtaas.application.service.TelnyxOutboundNumberService;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Country;
import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLogLeadIQ;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.QaFeedbackForm;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.StateCallOffHours;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.entity.TeamNotice;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.CountryRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogLeadIQRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.QaFeedbackFormRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.db.repository.TeamNoticeRepository;
//import com.xtaas.db.repository.SuppressionListRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.CallbackDispositionStatus;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerCodeDispositionStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.FailureDispositionStatus;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.domain.valueobject.ProspectCustomSortField;
import com.xtaas.domain.valueobject.QaDetails;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.IndirectProspectsCacheDTO;
import com.xtaas.web.dto.IndirectProspectsDTO;
import com.xtaas.web.dto.ProspectCallCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.ProspectCallLogDTO;
import com.xtaas.web.dto.QaFeedbackDTO;
import com.xtaas.web.dto.QaFeedbackFormDTO;
import com.xtaas.web.dto.QueuedProspectDTO;

/**
 * @author djain
 *
 */
@Service
public class ProspectCallLogServiceImpl implements ProspectCallLogService {

	private static final Logger logger = LoggerFactory.getLogger(ProspectCallLogServiceImpl.class);
	
	@Autowired
	ProspectInMemoryCacheServiceImpl prospectInMemoryCacheServiceImpl;

	@Autowired
	private ProspectCallLogRepository prospectLogRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private ProspectCallInteractionRepository prospectLogInteractionRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private CallLogRepository callLogRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PCIAnalysisRepository pciAnalysisRepository;

	@Autowired
	private DomoUtilsService domoUtilsService;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private CRMMappingService crmMappingService;

	@Autowired
	private QaFeedbackFormRepository qaFeedbackFormRepository;

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private OutboundNumberService outboundNumberService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	private SuppressionListService suppressionListService; 

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	ProspectCallLogLeadIQRepository  prospectCallLogLeadIQRepository;

	@Autowired
	LeadIQSearchServiceImpl leadIQSearchService;

	@Autowired
	TelnyxOutboundNumberService telnyxOutboundNumberService;

//	@Autowired
//	private GlobalContactService globalContactService;

	@Autowired
	private PriorityQueueCacheOperationService priorityQueueCacheOperationService;

	@Autowired
	private AbmListNewRepository abmListRepositoryNew;
	
	@Autowired
	private TeamNoticeService teamNoticeService;
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	CampaignMetaDataRepository campaignMetaDataRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	@Autowired
	private TeamNoticeRepository teamNoticeRepository;
	
	private Map<String, Date> lowCallableAlertMap = new HashMap<String, Date>(); 

	@Override
	public ProspectCallLog save(ProspectCallLog prospectCallLog, boolean createProspectCallInteraction) {
		ProspectCallLog savedProspectCallLog = null;
		try {
			ProspectCallLog latestProspectFromDB = prospectLogRepository
					.findByProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
			if (latestProspectFromDB != null) {
				prospectCallLog.setVersion(latestProspectFromDB.getVersion());
			}
			if(prospectCallLog.getProspectCall().getProspect().getLeadIQResult() != null) {
				prospectCallLog.getProspectCall().getProspect().setLeadIQResult(null);
			}
			if(prospectCallLog.getProspectCall().getDataSlice() == null){
				prospectCallLog.getProspectCall().setDataSlice("slice10");
			}
			if(prospectCallLog.getDataSlice() == null){
				prospectCallLog.setDataSlice("slice10");
			}
			savedProspectCallLog = prospectLogRepository.save(prospectCallLog);
		} catch (OptimisticLockingFailureException olfe) {
			logger.error("save(): OptimisticLockingFailureException occurred. Retrying to save the record. ", olfe);
			// If optimisitic lock failed, then refetch the object from DB and then save it
			ProspectCallLog latestProspectCallLogFromDB = prospectLogRepository
					.findByProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
			// copy values to this latest object from DB
			if (latestProspectCallLogFromDB != null && prospectCallLog != null
					&& prospectCallLog.getProspectCall() != null) {
				latestProspectCallLogFromDB.setProspectCall(prospectCallLog.getProspectCall());
				latestProspectCallLogFromDB.setStatus(prospectCallLog.getStatus());
				latestProspectCallLogFromDB.setCallbackDate(prospectCallLog.getCallbackDate());
				latestProspectCallLogFromDB.setQaFeedback(prospectCallLog.getQaFeedback());
				// save the latest Prospect Call object from DB
				if(prospectCallLog.getProspectCall().getProspect().getLeadIQResult() != null) {
					prospectCallLog.getProspectCall().getProspect().setLeadIQResult(null);
				}
				savedProspectCallLog = prospectLogRepository.save(latestProspectCallLogFromDB);
			}

		} catch (DuplicateKeyException duplicateKeyException) {
//			throw new IllegalArgumentException("Prospect is already created in the campaign.");
			savedProspectCallLog = handledDuplicateKeyException(prospectCallLog);
		}

		if (createProspectCallInteraction && (savedProspectCallLog != null && savedProspectCallLog.getProspectCall() != null)) {
			// create prospect call interaction history record
			ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
			prospectCallInteraction.setProspectCall(savedProspectCallLog.getProspectCall()); // it contains callSid, callRetryCount
			prospectCallInteraction.setStatus(savedProspectCallLog.getStatus());
			prospectCallInteraction.setCallbackDate(savedProspectCallLog.getCallbackDate());
			prospectCallInteraction.setQaFeedback(savedProspectCallLog.getQaFeedback());
			prospectCallInteraction.setResearchData(savedProspectCallLog.isResearchData());
			prospectCallInteraction.setProspectingData(savedProspectCallLog.isProspectingData());
			prospectLogInteractionRepository.save(prospectCallInteraction);

			Campaign campaign = campaignRepository.findOneById(prospectCallInteraction.getProspectCall().getCampaignId());
			String agentName = null;
			String qaName = null;
			PCIAnalysis latestPciAnalysis = null;
			if(prospectCallInteraction.getQaFeedback()!=null && prospectCallInteraction.getQaFeedback().getQaId()!=null){
				Login login = loginRepository.findOneById(prospectCallInteraction.getQaFeedback().getQaId());
				if(login!=null){
					qaName = login.getName();
				}
			}
			if(prospectCallInteraction.getProspectCall()!=null && prospectCallInteraction.getProspectCall().getAgentId()!=null){
				Login login= loginRepository.findOneById(prospectCallInteraction.getProspectCall().getAgentId());
				if(login!=null){
					agentName = login.getName();
				}
			}
			PCIAnalysis pcian = new PCIAnalysis();
			pcian.toPCIAnalysis(pcian,prospectCallInteraction, agentName, qaName, campaign);
			Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");

			List<PCIAnalysis> latestPciList = pciAnalysisRepository.findByProspectCallIdWithSort(pcian.getProspectcallid(), pageable);
			if(latestPciList!=null && latestPciList.size()>0) {
				latestPciAnalysis = latestPciList.get(0);
			}
			if(prospectCallInteraction.getStatus().equals(ProspectCallLog.ProspectCallStatus.CALL_DIALED)) {
				pcian.setCall_attempted(1);
				pcian.setCall_connected(0);
				pcian.setCall_contacted(0);
				if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
						&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
					// dont do anything
				}else {
					pciAnalysisRepository.save(pcian);
					domoUtilsService.postPciAnalysisWebhook(pcian);
				}
			}else if(/*latestPciAnalysis!=null && latestPciAnalysis.getInteraction_success()==0 &&*/ pcian.getInteraction_success()>0) {

				//getClonedConnectedPci
				PCIAnalysis pcian_connected = getClonedConnectedPci(pcian);
				PCIAnalysis pcian_contacted = getClonedContactedPci(pcian_connected);
				pcian.setCall_attempted(0);
				pcian.setCall_connected(0);
				pcian.setCall_contacted(0);
				if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
						&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
					// dont do anything
				}else {
					pciAnalysisRepository.save(pcian);
					domoUtilsService.postPciAnalysisWebhook(pcian);
				}


			}else if(/*latestPciAnalysis!=null && latestPciAnalysis.getCall_contacted()==0 &&*/ pcian.getCall_connected()>0  ) {
				PCIAnalysis pcian_connected = new PCIAnalysis();
				pcian_connected = pcian;


				pcian_connected.setCall_attempted(0);
				pcian_connected.setCall_connected(1);
				pcian_connected.setCall_contacted(0);
				if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
						&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
					// dont do anything
				}else {
					pciAnalysisRepository.save(pcian_connected);
					domoUtilsService.postPciAnalysisWebhook(pcian_connected);
				}


				PCIAnalysis pcian_contacted = getClonedContactedPci(pcian_connected);



			}else if(/*latestPciAnalysis!=null && latestPciAnalysis.getCall_connected() == 0 && */ pcian.getCall_contacted()>0) {
				PCIAnalysis pcian_connected = new PCIAnalysis();
				pcian_connected = pcian;

				pcian_connected.setCall_attempted(0);
				pcian_connected.setCall_connected(0);
				pcian_connected.setCall_contacted(1);
				if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
						&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
					// dont do anything
				}else {
					pciAnalysisRepository.save(pcian_connected);
					domoUtilsService.postPciAnalysisWebhook(pcian_connected);

				}


			}else {
				pcian.setCall_attempted(0);
				pcian.setCall_connected(0);
				pcian.setCall_contacted(0);
				pcian.setInteraction_success(0);
				if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
						&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
					// dont do anything
				}else {
					pciAnalysisRepository.save(pcian);
					domoUtilsService.postPciAnalysisWebhook(pcian);
				}
			}
		}

		return savedProspectCallLog;
	}

	private PCIAnalysis getClonedContactedPci(PCIAnalysis pcian) {
		PCIAnalysis pcian_contacted = new PCIAnalysis(pcian.getAgentId(), pcian.getAgentName(), pcian.getAssetName(), pcian.getCall_attempted(), pcian.getCall_connected(),
				pcian.getCall_contacted(), pcian.getCall_date(), pcian.getCall_time(), pcian.getCallduration(), pcian.getCallretrycount(),
				pcian.getCampaign_status(), pcian.getCampaignid(), pcian.getCampaignName(), pcian.getClientName(), pcian.getCompany(),
				pcian.getCountry1(), pcian.getCountry2(), pcian.getCountry3(), pcian.getCountry4(), pcian.getCountry5(), pcian.getCq1_certifyLink(),
				pcian.getCq1_question(), pcian.getCq1_response(), pcian.getCq1_validationresp(), pcian.getCq2_certifyLink(),
				pcian.getCq2_question(), pcian.getCq2_response(), pcian.getCq2_validationresp(), pcian.getCq3_certifyLink(),
				pcian.getCq3_question(), pcian.getCq3_response(), pcian.getCq3_validationresp(), pcian.getCq4_certifyLink(),
				pcian.getCq4_question(), pcian.getCq4_response(), pcian.getCq4_validationresp(), pcian.getCq4_certifyLink(),
				pcian.getCq5_question(), pcian.getCq5_response(), pcian.getCq5_validationresp(), pcian.getCreated_date(),
				pcian.getCreated_datetime(), pcian.getDepartment(), pcian.getDispositionstatus(), pcian.getDnc_added(), pcian.getFirstname(),
				pcian.getEmployeeCountMax(), pcian.getEmployeeCountMin(), pcian.getIndustry(), pcian.getIndustry1(), pcian.getIndustry2(),
				pcian.getIndustry3(), pcian.getIndustry4(), pcian.getInteraction_answer_machine(), pcian.getInteraction_busy(),
				pcian.getInteraction_call_abandoned(), pcian.getInteraction_dead_air(), pcian.getInteraction_FAILED_QUALIFICATION(),
				pcian.getInteraction_failure(), pcian.getInteraction_gatekeeper_answer_machine(), pcian.getInteraction_local_dnc_error(),
				pcian.getInteraction_max_retry_limit_reached(), pcian.getInteraction_national_dnc_error(), pcian.getInteraction_no_answer(),
				pcian.getInteraction_no_consent(), pcian.getInteraction_not_interested(), pcian.getInteraction_success(),
				pcian.getInteraction_unreachable(), pcian.getLastname(), pcian.getLead_validation_notes(), pcian.getLead_validation_valid(),
				pcian.getLeadStatus(), pcian.getManagement_level(), pcian.getMgt_level_c_level(), pcian.getMgt_level_director(),
				pcian.getMgt_level_manager(), pcian.getMgt_level_non_management(), pcian.getMgt_level_president_principal(),
				pcian.getMgt_level_vice_president(), pcian.getOrganizationid(), pcian.getOutboundnumber(), pcian.getP_domain(),
				pcian.getPartnerid(), pcian.getPhone(), pcian.getPrefix(), pcian.getProspectcallid(), pcian.getProspecthandleduration(),
				pcian.getQaid(), pcian.getQaname(), pcian.getQualitybucket(), pcian.getRecordingurl(), pcian.getRevenuemax(), pcian.getRevenuemin(),
				pcian.getStatecode(), pcian.getStatus(), pcian.getSubstatus(), pcian.getSuffix(), pcian.getTimezone(), pcian.getTitle(),
				pcian.getTransactiondate_day() ,pcian.getTransactiondate_dayofweek(), pcian.getTransactiondate_dayofyear(),
				pcian.getTransactiondate_hour(), pcian.getTransactiondate_minute(), pcian.getTransactiondate_month(), pcian.getTransactiondate_second(),
				pcian.getTransactiondate_week(), pcian.getTransactiondate_year(), pcian.getVoiceduration(), pcian.getTwiliocallsid(), pcian.getZipcode(),
				pcian.getDatasource(), pcian.getUnknownerror(), pcian.getDirectPhone(), pcian.getEmail(),pcian.getDataSlice(),pcian.getQualitybucket(),
				pcian.getQualityBucketSort(),pcian.getSourceType(),pcian.getAuto_machine_answered(),pcian.getCompanyPhone(),pcian.getDailyRetryCount(),
				pcian.getRejectionReason(), pcian.getClientDelivered(),pcian.isLeadDelivered(), pcian.getLastUpdatedDate(),pcian.getCallDetectedAs(),
				pcian.getAmdTimeDiffInSec(), pcian.getAmdTimeDiffInMilliSec(),pcian.isEmailRevealed(),pcian.getCallbackDate(),pcian.isResearchData(),
				pcian.isProspectingData(),pcian.isAutoMachineDetected(),pcian.getNotes(),pcian.getAgentLoginURL(), pcian.getVoiceProvider(), pcian.getSipProvider());
		pcian_contacted.setCall_attempted(0);
		pcian_contacted.setCall_connected(0);
		pcian_contacted.setCall_contacted(1);
		pcian_contacted.setInteraction_success(0);
		if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
				&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
			// dont do anything
		}else {
			pciAnalysisRepository.save(pcian_contacted);
			domoUtilsService.postPciAnalysisWebhook(pcian_contacted);
		}
		return pcian_contacted;
	}

	private PCIAnalysis getClonedConnectedPci(PCIAnalysis pcian) {
		PCIAnalysis pcian_connected = new PCIAnalysis(pcian.getAgentId(), pcian.getAgentName(), pcian.getAssetName(), pcian.getCall_attempted(), pcian.getCall_connected(), pcian.getCall_contacted(), pcian.getCall_date(), pcian.getCall_time(), pcian.getCallduration(), pcian.getCallretrycount(),
				pcian.getCampaign_status(), pcian.getCampaignid(), pcian.getCampaignName(), pcian.getClientName(), pcian.getCompany(),
				pcian.getCountry1(), pcian.getCountry2(), pcian.getCountry3(), pcian.getCountry4(), pcian.getCountry5(), pcian.getCq1_certifyLink(),
				pcian.getCq1_question(), pcian.getCq1_response(), pcian.getCq1_validationresp(), pcian.getCq2_certifyLink(),
				pcian.getCq2_question(), pcian.getCq2_response(), pcian.getCq2_validationresp(), pcian.getCq3_certifyLink(),
				pcian.getCq3_question(), pcian.getCq3_response(), pcian.getCq3_validationresp(), pcian.getCq4_certifyLink(),
				pcian.getCq4_question(), pcian.getCq4_response(), pcian.getCq4_validationresp(), pcian.getCq4_certifyLink(),
				pcian.getCq5_question(), pcian.getCq5_response(), pcian.getCq5_validationresp(), pcian.getCreated_date(),
				pcian.getCreated_datetime(), pcian.getDepartment(), pcian.getDispositionstatus(), pcian.getDnc_added(), pcian.getFirstname(),
				pcian.getEmployeeCountMax(), pcian.getEmployeeCountMin(), pcian.getIndustry(), pcian.getIndustry1(), pcian.getIndustry2(),
				pcian.getIndustry3(), pcian.getIndustry4(), pcian.getInteraction_answer_machine(), pcian.getInteraction_busy(),
				pcian.getInteraction_call_abandoned(), pcian.getInteraction_dead_air(), pcian.getInteraction_FAILED_QUALIFICATION(),
				pcian.getInteraction_failure(), pcian.getInteraction_gatekeeper_answer_machine(), pcian.getInteraction_local_dnc_error(),
				pcian.getInteraction_max_retry_limit_reached(), pcian.getInteraction_national_dnc_error(), pcian.getInteraction_no_answer(),
				pcian.getInteraction_no_consent(), pcian.getInteraction_not_interested(), pcian.getInteraction_success(),
				pcian.getInteraction_unreachable(), pcian.getLastname(), pcian.getLead_validation_notes(), pcian.getLead_validation_valid(),
				pcian.getLeadStatus(), pcian.getManagement_level(), pcian.getMgt_level_c_level(), pcian.getMgt_level_director(),
				pcian.getMgt_level_manager(), pcian.getMgt_level_non_management(), pcian.getMgt_level_president_principal(),
				pcian.getMgt_level_vice_president(), pcian.getOrganizationid(), pcian.getOutboundnumber(), pcian.getP_domain(),
				pcian.getPartnerid(), pcian.getPhone(), pcian.getPrefix(), pcian.getProspectcallid(), pcian.getProspecthandleduration(),
				pcian.getQaid(), pcian.getQaname(), pcian.getQualitybucket(), pcian.getRecordingurl(), pcian.getRevenuemax(), pcian.getRevenuemin(),
				pcian.getStatecode(), pcian.getStatus(), pcian.getSubstatus(), pcian.getSuffix(), pcian.getTimezone(), pcian.getTitle(),
				pcian.getTransactiondate_day() ,pcian.getTransactiondate_dayofweek(), pcian.getTransactiondate_dayofyear(),
				pcian.getTransactiondate_hour(), pcian.getTransactiondate_minute(), pcian.getTransactiondate_month(), pcian.getTransactiondate_second(),
				pcian.getTransactiondate_week(), pcian.getTransactiondate_year(), pcian.getVoiceduration(), pcian.getTwiliocallsid(), pcian.getZipcode(),
				pcian.getDatasource(), pcian.getUnknownerror(), pcian.getDirectPhone(), pcian.getEmail(),pcian.getDataSlice(),pcian.getQualitybucket(),
				pcian.getQualityBucketSort(),pcian.getSourceType(),pcian.getAuto_machine_answered(),pcian.getCompanyPhone(),pcian.getDailyRetryCount(),
				pcian.getRejectionReason(),pcian.getClientDelivered(), pcian.isLeadDelivered(),  pcian.getLastUpdatedDate(),pcian.getCallDetectedAs(),
				pcian.getAmdTimeDiffInSec(), pcian.getAmdTimeDiffInMilliSec(),pcian.isEmailRevealed(),pcian.getCallbackDate(),pcian.isResearchData(),
				pcian.isProspectingData(),pcian.isAutoMachineDetected(),pcian.getNotes(),pcian.getAgentLoginURL(), pcian.getVoiceProvider(), pcian.getSipProvider());
		pcian_connected.setCall_attempted(0);
		pcian_connected.setCall_connected(1);
		pcian_connected.setCall_contacted(0);
		pcian_connected.setInteraction_success(0);
		if(pcian.getUpdatedBy()!=null && !pcian.getUpdatedBy().isEmpty()
				&& pcian.getUpdatedBy().equalsIgnoreCase("anonymousUser")) {
			// dont do anything
		}else {
			pciAnalysisRepository.save(pcian_connected);
			domoUtilsService.postPciAnalysisWebhook(pcian_connected);
		}
		return pcian_connected;
	}



	@Override
	public void saveAll(List<ProspectCallLog> prospectLogList) {
		prospectLogRepository.saveAll(prospectLogList);
	}

	@Override
	public void updateCallStatus(String prospectCallId, ProspectCallStatus status, String callSid,
								 Integer callRetryCount, Long dailyCallRetryCount) {
		updateProspectCall(prospectCallId, status, callSid, null, callRetryCount, null, dailyCallRetryCount);
	}

	@Override
	public void updateCallStatus(String callSid, ProspectCallStatus status) {
		updateProspectCall(null, status, callSid, null, null, null, null);
	}

	@Override
	public void updateCallDetails(String prospectCallId, ProspectCallStatus status, String callSid,
								  Integer callRetryCount, String allocatedAgentId, Long dailyCallRetryCount) {
		updateProspectCall(prospectCallId, status, callSid, null, callRetryCount, allocatedAgentId,
				dailyCallRetryCount);
	}

	@Override
	public void updateCallbackStatusSystemGenerated(String prospectCallId, Date callbackDate) {
		if (callbackDate != null) {
			updateProspectCall(prospectCallId, ProspectCallStatus.CALL_TIME_RESTRICTION, null, callbackDate, null, null,
					null);
		}
	}

	private ProspectCall getProspectCallRetryCount(ProspectCall prospectCall, ProspectCallStatus status) {
		if (status != null && ProspectCallStatus.CALL_MACHINE_ANSWERED.equals(status)) {
			int machineDetectedIncrementer = prospectCall.getMachineDetectedIncrementer();
			machineDetectedIncrementer = machineDetectedIncrementer + 1;
			Long currentDailyCallRetryCount = prospectCall.getDailyCallRetryCount();
			String reminder = "0";
			if (currentDailyCallRetryCount != null && currentDailyCallRetryCount > 0) {
				String dailyCountStr = String.valueOf(currentDailyCallRetryCount);
				if (!StringUtils.isEmpty(dailyCountStr) && dailyCountStr.length() > 2) {
					reminder = String.valueOf(currentDailyCallRetryCount).substring(2);
				}
			}
			if (machineDetectedIncrementer == 1 && reminder.equals("3")) {
				Integer currentCallRetryCount = prospectCall.getCallDuration();
				if (currentCallRetryCount != null && currentCallRetryCount > 0) {
					prospectCall.setCallRetryCount(currentCallRetryCount - 1);
				}
				if (currentDailyCallRetryCount != null && currentDailyCallRetryCount > 0) {
					prospectCall.setDailyCallRetryCount(currentDailyCallRetryCount - 1);
				}
				prospectCall.setMachineDetectedIncrementer(machineDetectedIncrementer);
			} else if (machineDetectedIncrementer > 1) {
				Integer currentCallRetryCount = prospectCall.getCallDuration();
				if (currentCallRetryCount != null && currentCallRetryCount > 0) {
					prospectCall.setCallRetryCount(currentCallRetryCount - 1);
				}
				if (currentDailyCallRetryCount != null && currentDailyCallRetryCount > 0) {
					prospectCall.setDailyCallRetryCount(currentDailyCallRetryCount - 1);
				}
				prospectCall.setMachineDetectedIncrementer(0);
			} else {
				prospectCall.setMachineDetectedIncrementer(machineDetectedIncrementer);
			}
		}
		return prospectCall;
	}

	@SuppressWarnings("unlikely-arg-type")
	private void updateProspectCall(String prospectCallId, ProspectCallStatus status, String callSid, Date callbackDate,
									Integer callRetryCount, String allocatedAgentId, Long dailyCallRetryCount) {
		// update prospectcalllog and also create a prospect call interaction record for
		// maintaining the prospect call interaction history
		ProspectCallLog prospectCallLog = new ProspectCallLog();
		if (prospectCallId != null) {
			prospectCallLog = prospectLogRepository.findByProspectCallId(prospectCallId);
		} else if (callSid != null && !callSid.isEmpty()) {
			prospectCallLog = prospectLogRepository.findByTwilioCallId(callSid);
		}

		if (prospectCallLog == null) {
			logger.error("updateProspectCall() : Could not find prospect with prospectCallId [{}] in prospectCallLog",
					prospectCallId);
		} else {
			ProspectCallStatus previousStatus = prospectCallLog.getStatus();
			ProspectCall prospectCall = prospectCallLog.getProspectCall();

			// clean up previous statuses
			prospectCallLog.setCallbackDate(null);
			prospectCall.setDispositionStatus(null);
			prospectCall.setSubStatus(null);

			if (status == null) {
				status = prospectCallLog.getStatus(); // keeping the old status..this ideally should never happen
			} else {
				prospectCallLog.setStatus(status);
				if (ProspectCallStatus.CALL_DIALED.equals(status)) {
					prospectCall.setCallStartTime(new Date());
					// reset old agent & recording details on call dialed
					prospectCall.setAgentId(null);
					prospectCall.setPartnerId(null);
					prospectCall.setSupervisorId(null);
					prospectCall.setRecordingUrl(null);
					prospectCall.setRecordingUrlAws(null);
				}
				// Commented call disconnected by prospect.
				// logger.debug("OUTSIDE PUSHER MESSAGE ----> [{}]" + status);
				// if (ProspectCallStatus.CALL_COMPLETE.equals(status)) {
				// 	logger.debug("INSIDE PUSHER MESSAGE ----> [{}]" + agentId);
				// 	PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
				// }
				// set the disposition
				if (ProspectCallStatus.CALL_BUSY.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCall.setSubStatus(DialerCodeDispositionStatus.BUSY.name());
				} else if (ProspectCallStatus.CALL_NO_ANSWER.equals(status)
						|| ProspectCallStatus.CALL_CANCELED.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCall.setSubStatus(DialerCodeDispositionStatus.NOANSWER.name());
				} else if (ProspectCallStatus.CALL_MACHINE_ANSWERED.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCall.setSubStatus(DialerCodeDispositionStatus.ANSWERMACHINE.name());
					prospectCall.setAutoMachineDetected(true);
				} else if (ProspectCallStatus.CALL_FAILED.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.FAILURE.name());
					prospectCall.setSubStatus(FailureDispositionStatus.OTHER.name());
				} else if (ProspectCallStatus.CALL_ABANDONED.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCall.setSubStatus(DialerCodeDispositionStatus.ABANDONED.name());
				} else if (ProspectCallStatus.UNKNOWN_ERROR.equals(status)) {
					prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCall.setSubStatus(DialerCodeDispositionStatus.UNKNOWN.name());
				} else if (ProspectCallStatus.NATIONAL_DNC_ERROR.equals(status)
						|| ProspectCallStatus.MAX_RETRY_LIMIT_REACHED.equals(status)
						|| ProspectCallStatus.LOCAL_DNC_ERROR.equals(status)
						|| ProspectCallStatus.CALL_TIME_RESTRICTION.equals(status)) { // unset the InteractionSessionId
					prospectCall.setProspectInteractionSessionId(null);
				}
			}
			////////////////////////////////////////////////////////
			StringBuffer sb = new StringBuffer();
			/*
			 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
			 */
			sb.append(prospectCallLog.getStatus());
			sb.append("|");
			sb.append(prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount"));
			sb.append("-");
			sb.append(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount"));
			sb.append("|");
			sb.append(prospectCall.getProspect().getManagementLevel());
			prospectCall.setQualityBucket(sb.toString());
			///////////////////////////////////////////////////////////////////
			if (callSid != null) {
				prospectCall.setTwilioCallSid(callSid);
			}
			if (callbackDate != null) {
				prospectCallLog.setCallbackDate(callbackDate);
				prospectCall.setDispositionStatus(DispositionType.CALLBACK.name());
				prospectCall.setSubStatus(CallbackDispositionStatus.CALLBACK_SYSTEM_GENERATED.name());
			}
			if (callRetryCount != null && callRetryCount > 0) {
				prospectCall.setCallRetryCount(callRetryCount);
			}
			if (allocatedAgentId != null) {
				prospectCall.setAgentId(allocatedAgentId);
			}
			if (dailyCallRetryCount != null && dailyCallRetryCount > 0) {
				prospectCall.setDailyCallRetryCount(dailyCallRetryCount);
			}
//			prospectCall = getProspectCallRetryCount(prospectCall, status);
//			prospectCallLog.setProspectCall(prospectCall);
			updateIndirectProspects(prospectCallLog);
			setVoiceAndSIPProvider(prospectCallLog);
			XtaasUtils.callDisposedLogStatement(prospectCallLog, null, null);
			this.save(prospectCallLog, true);
		}
	}

	private void setVoiceAndSIPProvider(ProspectCallLog prospectCallLog) {
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
				&& prospectCallLog.getProspectCall().getCampaignId() != null) {
			Campaign campaignFromDB = campaignService.getCampaign(prospectCallLog.getProspectCall().getCampaignId());
			if (campaignFromDB != null && !campaignFromDB.getCallControlProvider().isEmpty()) {
				prospectCallLog.getProspectCall().setVoiceProvider(campaignFromDB.getCallControlProvider());
			}
			if (campaignFromDB != null && !campaignFromDB.getSipProvider().isEmpty()) {
				prospectCallLog.getProspectCall().setSipProvider(campaignFromDB.getSipProvider());
			}
		} else {
			new RuntimeException("Voice and SIP Provider not stamped in the Prospect.");
		}
	}

	@Override
	public void processCallStatus(String callSid, String callStatus) {
		System.out.println("I am here");

		/*
		 *
		 * if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(twilioCallStatus) ||
		 * XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus) ||
		 * XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus) ||
		 * XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus) ||
		 * XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus))
		 */

		// if (!callStatus.equals(callStatusMap.get(callSid))) {
		synchronized (callSid) {
			switch (callStatus) {
				case "answered":
				case "completed":
				case "failed":
				case "no-answer":
				case "in-progress":
					// callStatusMap.remove(callSid);
					// }
			}
		}
	}

	@Override
	public QaDetails getDetails(String prospectCallId) {
		ProspectCallLog prospectCallLog = prospectLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLog == null) {
			throw new IllegalArgumentException("Prospect calllog " + prospectCallId + "does not Exist");
		}

		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		if (prospectCall == null) {
			throw new IllegalArgumentException("Prospect Call" + prospectCallId + " does not Exist");
		}

		Prospect prospect = prospectCall.getProspect();
		if (prospect == null) {
			throw new IllegalArgumentException("Prospect " + prospectCallId + " does not Exist");
		}
		Campaign campaign = campaignService.getCampaign(prospectCall.getCampaignId());
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign" + prospectCall.getCampaignId() + " does not Exist");
		}
		CampaignDTO campaignDTO = new CampaignDTO(campaign);

		/*
		 * START DATE : 11/05/2017 REASON : Added to get all the questions set for
		 * particular campaign
		 */
		// HashMap<String, CRMAttribute> orgCRMMapping =
		// crmMappingService.getCRMAttributeMap(campaign.getOrganizationId());
		LinkedHashSet<String> uniqueAttributeMap = new LinkedHashSet<String>();
		HashMap<String, ArrayList<PickListItem>> qaQuestions = new HashMap<String, ArrayList<PickListItem>>();
		HashMap<String, String> qaQuestionAttribMapSkins = new HashMap<String, String>();
		HashMap<String, String> sequenceNumberMap = new HashMap<String, String>();
		// set the questions attributes
		if (campaign.getQualificationClause() != null) {
			uniqueAttributeMap.addAll(campaign.getQualificationCriteria().getUniqueAttributeMap());
			qaQuestions.putAll(campaign.getQualificationCriteria().getQuestions());
			qaQuestionAttribMapSkins.putAll(campaign.getQualificationCriteria().getQuestionSkins());
			sequenceNumberMap.putAll(campaign.getQualificationCriteria().getSequenceNumbers());
		}

		// HashMap<String, String> agentAnswers = prospectCall.getAnswers();
		HashMap<String, String> qaAnswer = new HashMap<String, String>();

		LinkedHashMap<String, CRMAttribute> questionAttributeMap = new LinkedHashMap<String, CRMAttribute>();
		// HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String,
		// ArrayList<PickListItem>>();
		// HashMap<String, String> questionAttribMapSkins = new HashMap<String,
		// String>();
		for (String attrName : uniqueAttributeMap) {
			// questionAttributeMap.put(attrName, orgCRMMapping.get(attrName));
			// questions.putAll(campaign.getAgentQualificationCriteria().getQuestions());
			// questionAttribMapSkins.putAll(campaign.getAgentQualificationCriteria().getQuestionSkins());
			// TODO below code should be removed after discussion, i am hacking this and
			// creating a virtual orgCRMMapping object
			String sequenceNumber = sequenceNumberMap.get(attrName);
			String questionSkin = qaQuestionAttribMapSkins.get(attrName);
			ArrayList<PickListItem> answers = qaQuestions.get(questionSkin);
			CRMAttribute crmAttrib = new CRMAttribute();
			crmAttrib.setSystemName(attrName);
			crmAttrib.setCrmDataType("string");
			crmAttrib.setCrmName(attrName);
			// crmAttrib.setExtraAttributes("");
			crmAttrib.setLabel(attrName);
			crmAttrib.setLength(512);
			crmAttrib.setPickList(answers);
			// CRMAttributes
			// [crmName=IS_YOUR_COMPANY_EVALUATING_MARKETING_AUTOMATION_PLATFORM?,
			// systemName=IS_YOUR_COMPANY_EVALUATING_MARKETING_AUTOMATION_PLATFORM?,
			// crmDataType=string, systemDataType=string, length=512,
			// pickList=[com.xtaas.db.entity.PickListItem@7ea465a3,
			// com.xtaas.db.entity.PickListItem@5b0cf149,
			// com.xtaas.db.entity.PickListItem@2eb51e52,
			// com.xtaas.db.entity.PickListItem@74d9b172], extraAttributes=null]
			crmAttrib.setProspectBeanPath("Prospect.CustomAttributes['" + attrName + "']");
			crmAttrib.setQuestionSkin(questionSkin);
			crmAttrib.setSystemDataType("string");
			crmAttrib.setSequence(sequenceNumber);

			crmAttrib.setAttributeType("CUSTOM");

			questionAttributeMap.put(attrName, crmAttrib);

			/*
			 * if (agentAnswers != null ) {
			 *
			 * if (agentAnswers.containsKey(attrName)) { qaAnswer.put(questionSkin,
			 * agentAnswers.get(attrName)); } prospectCall.setAnswers(qaAnswer); }
			 */
		}

		questionAttributeMap = XtaasUtils.addDefaultQuestions(questionAttributeMap);

		/* END */

		Team team = null;
		if (campaign.getTeam() != null) {
			team = teamRepository.findOneById(campaign.getTeam().getTeamId());
			if (team == null) {
				throw new IllegalArgumentException(
						campaign.getTeam().getTeamId() + "team does not Exist in prospect call");
			}
		}
		Agent agent = agentRepository.findOneById(prospectCall.getAgentId());
		if (agent == null) {
			if (prospectCall.getAgentId().equalsIgnoreCase(prospectCall.getOrganizationId()+"_EMAIL_AGENT")) {
				
			}else {
				throw new IllegalArgumentException(prospectCall.getAgentId() + "agent does not Exist in prospect call");
			}
		}
		
		AgentDTO agentDTO = null;
		String agentProfilePic = null;
		if(prospectCall.getAgentId().equalsIgnoreCase(prospectCall.getOrganizationId()+"_EMAIL_AGENT")) {
			agentDTO = new AgentDTO();
			agentDTO.setFirstName("EMAIL_AGENT");
			agentDTO.setLastName("EMAIL_AGENT");
			agentDTO.setUsername(prospectCall.getOrganizationId()+"_EMAIL_AGENT");
			agentDTO.setPartnerId(prospectCall.getOrganizationId());
		}else {
			Login login = loginRepository.findOneById(agent.getId());
			agentProfilePic = login.getProfilePicUrl();
			agentDTO = new AgentDTO(agent);
		}
		// HashMap<String, CRMAttribute> orgCRMMapping =
		// crmMappingService.getCRMAttributeMap(campaign.getOrganizationId());
		// Collection<CRMAttribute> crmAttributes = orgCRMMapping.values();
		/*
		 * HashMap<String, String> answers = prospectCall.getAnswers(); if (answers !=
		 * null ) { HashMap<String, String> answer = new HashMap<String, String>(); for
		 * (CRMAttribute crmAttribute : crmAttributes ) {
		 *
		 * if (answers.containsKey(crmAttribute.getCrmName())) {
		 * answer.put(crmAttribute.getQuestionSkin(),
		 * answers.get(crmAttribute.getCrmName())); } } prospectCall.setAnswers(answer);
		 * }
		 */

		// set qaFeedbackFormDTO
		QaFeedbackForm qaFeedbackForm = null;
		if (prospectCall.getCampaignId() != null) {
			qaFeedbackForm = qaFeedbackFormRepository.findOneByCampaignId(prospectCall.getCampaignId());
		}
		QaFeedbackFormDTO qaFeedbackFormDTO = null;
		if (qaFeedbackForm == null) {
			QaFeedbackForm systemQaFeedback = qaFeedbackFormRepository.findOneById("SYSTEM");
			qaFeedbackFormDTO = new QaFeedbackFormDTO(systemQaFeedback);
		} else {
			qaFeedbackFormDTO = new QaFeedbackFormDTO(qaFeedbackForm);
		}
		// set prospectCallLogDTO
		ProspectCallLogDTO prospectCallLogDTO = new ProspectCallLogDTO();
		prospectCallLogDTO.setProspectCall(prospectCall != null ? new ProspectCallDTO(prospectCall) : null);
		prospectCallLogDTO.setStatus(prospectCallLog.getStatus());
		if (prospectCallLog.getQaFeedback() != null) {
			prospectCallLogDTO.setQaFeedback(new QaFeedbackDTO(prospectCallLog.getQaFeedback()));
		}
		// set QaDetails values
		QaDetails qaDetails = new QaDetails(agentDTO, campaignDTO, prospectCallLogDTO, qaFeedbackFormDTO,
				questionAttributeMap);
		qaDetails.setAgentProfilePic(agentProfilePic);
		qaDetails.setTeamName(team != null ? team.getName() : null);
		if (prospectCall.getTwilioCallSid() != null) {
			String twilioCallSid = prospectCall.getTwilioCallSid();
			CallLog callLog = callLogRepository.findOneById(twilioCallSid);
			if (callLog != null) {
				List<HashMap<String, String>> callLogMapList = new ArrayList<HashMap<String, String>>();
				callLogMapList.add(callLog.getCallLogMap());
				qaDetails.setCallLogMap(callLogMapList);
			}
		}
		return qaDetails;
	}

	@Override
	public HashMap<String, Integer> getCallableRecordsCount(String campaignId) {
		int totalCallableCount = 0;
		int queuedProspectCount = 0;
		int expiredCallbackProspectCount = 0;
		HashMap<String, Integer> countsMap = new HashMap<String, Integer>();
		totalCallableCount = prospectCacheServiceImpl.getCachedProspectCallLogCount(campaignId);
		if (totalCallableCount > 0) {
			countsMap.put(XtaasConstants.CALLABLE_COUNT, totalCallableCount);
			logger.debug("Reading callable count for [{}] from cache & count is [{}].", campaignId, totalCallableCount);
			return countsMap;
		}

		Campaign campaign = campaignService.getCampaign(campaignId);
		// find the states which have business hours closed at this moment (exclude
		// these states from the Query)
		List<String> excludeStateCodes = getStateCodesWithBusinessClosed();
		logger.debug(
				"getCallableRecordsCount() for Campaign " + campaignId
						+ "  : Excluded States due to TZ restrictions : [{}]",
				StringUtils.join(excludeStateCodes, ","));

		// HACK: Calculating date in Pacific TZ because all other TZ will be in same
		// date of Pacific TZ during their business hours.
		String strCurrentDateYMD = getCurrentDateInPacificTZinYMD();
		int dailyCallMaxRetriesLimit = 1;
		if (campaign != null && campaign.getDailyCallMaxRetries() > 0) {
			// get daily call max retry count from campaign
			dailyCallMaxRetriesLimit = campaign.getDailyCallMaxRetries();
		} else {
			dailyCallMaxRetriesLimit = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.DAILY_CALL_MAX_RETRIES.name(), 1);
		}

		int maxCallRetryLimit = 5;
		if (campaign != null && campaign.getCallMaxRetries() > 0) {
			maxCallRetryLimit = campaign.getCallMaxRetries(); // get call max retry count from campaign
		} else {
			maxCallRetryLimit = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
		}
		Long maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));
		logger.debug("getCallableRecordsCount() for Campaign " + campaignId + "  : Max Daily Call Retry Count : [{}]",
				maxDailyCallRetryCount);

		long callBackTime = 0;
		if(campaign.getType().equals(CampaignTypes.LeadVerification)) {
			callBackTime  = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
		}else {
			callBackTime = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
		}

		// count prospects in dialer queue
		queuedProspectCount = prospectLogRepository.countProspectsForDialer(campaignId, excludeStateCodes,
				maxDailyCallRetryCount, maxCallRetryLimit, XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int)callBackTime));

		// count prospects with callback expired
		expiredCallbackProspectCount = prospectLogRepository.countByExpiredCallback(campaignId,new Date(), 
				excludeStateCodes, maxDailyCallRetryCount, XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int)callBackTime));

		totalCallableCount = queuedProspectCount + expiredCallbackProspectCount;
		prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
		countsMap.put(XtaasConstants.CALLABLE_COUNT, totalCallableCount);
		logger.debug("Reading callable count for [{}] from DB & count is [{}].", campaignId, totalCallableCount);
		return countsMap;
	}

	//TODO :: Need to remove the duplicate code in getQueuedProspects method addedto handle prospect caching feature on campaign flag basis.(enableProspectCaching : true/false)
	@Override
	public QueuedProspectDTO getQueuedProspects(String campaignId, int queuedCallCount) {
		QueuedProspectDTO queuedProspectDTO = new QueuedProspectDTO(new ArrayList<ProspectCallCacheDTO>(0), 0);
//		QueuedProspectDTO queuedProspectCallDTO = new QueuedProspectDTO(new ArrayList<ProspectCallDTO>(0), 0, 0);
		Campaign campaign = campaignService.getCampaign(campaignId);
		if (!campaign.getStatus().equals(CampaignStatus.RUNNING)) {
			logger.debug("==========> Campaign is not running <==========");
			return queuedProspectDTO; // return blank list
		}
		checkIfCallablesFallsBelowThreshold(campaign);
		Team team = teamService.getTeam(campaign.getTeam().getTeamId());

		boolean isTimeZoneChanged = false;
		// find the states which have business hours closed at this moment (exclude
		// these states from the Query)
		List<String> excludeStateCodes = getStateCodesWithBusinessClosed();

		List<String> cachedExcludedStateCodeList = prospectCacheServiceImpl.getExcludedStateCodeList(campaignId);
		if (cachedExcludedStateCodeList != null && cachedExcludedStateCodeList.size() > 0) {
			List<String> cloneExcludedStateCodes = excludeStateCodes.stream().collect(Collectors.toList());
			List<String> cloneCachedExcludedStateCodeList = cachedExcludedStateCodeList.stream()
					.collect(Collectors.toList());
			cloneCachedExcludedStateCodeList.removeAll(excludeStateCodes);
			if (!cloneCachedExcludedStateCodeList.isEmpty()) {
				isTimeZoneChanged = true;
				prospectCacheServiceImpl.setExcludedStateCodeList(campaignId, excludeStateCodes);
				logger.debug("########## TIMEZONE CHANGED FOR [{}] CAMPAIGN - FOUND [{}] ExcludeStateCodes ##########",
						campaign.getName(), excludeStateCodes.size());
			} else {
				cloneExcludedStateCodes.removeAll(cachedExcludedStateCodeList);
				if (!cloneExcludedStateCodes.isEmpty()) {
					isTimeZoneChanged = true;
					prospectCacheServiceImpl.setExcludedStateCodeList(campaignId, excludeStateCodes);
					logger.debug(
							"########## TIMEZONE CHANGED FOR [{}] CAMPAIGN - FOUND [{}] ExcludeStateCodes ##########",
							campaign.getName(), excludeStateCodes.size());
				} else {
					isTimeZoneChanged = false;
				}
			}
		} else {
			isTimeZoneChanged = true;
			prospectCacheServiceImpl.setExcludedStateCodeList(campaignId, excludeStateCodes);
			logger.debug("########## TIMEZONE CHANGED FOR [{}] CAMPAIGN - FOUND [{}] ExcludeStateCodes ##########",
					campaign.getName(), excludeStateCodes.size());
		}

		int callSpeed = campaign.getTeam().getCallSpeedPerMinPerAgent().getValue();

		// find the count of prospects needed
		HashMap<String, Integer> campaignActiveAgentCountMap = agentRegistrationService.getCampaignActiveAgentCount();
		Integer countOfActiveAgents = campaignActiveAgentCountMap.get(campaign.getId());
		if (countOfActiveAgents == null || countOfActiveAgents == 0) {
			return queuedProspectDTO; // return blank list
		}
		int size = 1;
		if (DialerMode.POWER.equals(campaign.getDialerMode()) || DialerMode.HYBRID.equals(campaign.getDialerMode())) {
			size = countOfActiveAgents * callSpeed;
		} else {
			size = 1;
		}
		/*
		 * subtract prospect count which are in queued and ringing state in twilio to
		 * reduce abandoned calls.
		 */
		size = size - queuedCallCount;

		if (!campaign.isRetrySpeedEnabled()) {
			queuedProspectDTO.setDelta(size);
			if (size <= 0) {
				return queuedProspectDTO;
			}
		}

		int queuedCallbackProspectCallLogCount = 0;
		int queuedCallbackProspectCallLogCountOld = 0;
		LinkedHashMap<String, List<ProspectCallCacheDTO>> linkedProspectCallLog = new LinkedHashMap<String, List<ProspectCallCacheDTO>>();
		LinkedHashMap<String, List<ProspectCallDTO>> linkedProspectCallLogOld = new LinkedHashMap<String, List<ProspectCallDTO>>();
		if (campaign.isEnableProspectCaching()) {
			linkedProspectCallLog = prospectCacheServiceImpl.getCachedProspectCallLog();
			if (linkedProspectCallLog != null && linkedProspectCallLog.size() != 0) {
				if (linkedProspectCallLog.get(campaign.getId()) != null) {
					queuedCallbackProspectCallLogCount = queuedCallbackProspectCallLogCount
							+ linkedProspectCallLog.get(campaign.getId()).size();
				}
			}
		} else {
			linkedProspectCallLogOld = prospectCacheServiceImpl.getCachedProspectCallLogOld();
			if (linkedProspectCallLogOld != null && linkedProspectCallLogOld.size() != 0) {
				if (linkedProspectCallLogOld.get(campaign.getId()) != null) {
					queuedCallbackProspectCallLogCountOld = queuedCallbackProspectCallLogCount
							+ linkedProspectCallLogOld.get(campaign.getId()).size();
				}
			}
		}
		

		logger.debug("getQueuedProspects() CacheRefresh Flag : [{}]", campaign.getCacheRefresh());

		int currentRetry = 0;
		boolean isIndirectFound = false;
		String cacheMode = propertyService
				.getApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CACHE_MODE.name(), "qualitybucket");
		if (isTimeZoneChanged || campaign.getCacheRefresh() || queuedCallbackProspectCallLogCountOld < 10) {
			if (isTimeZoneChanged && campaign.isEnableProspectCaching()) {
				logger.info("Reload the cache as timezone changed for [{}] prospect caching enabled campaign. ", campaign.getName());
				Set<String> registeredAgents = new HashSet<String>();
				ConcurrentHashMap<String, AgentCall> agentMap = agentRegistrationService.getAgentCallMap();
				if (agentMap != null && !agentMap.isEmpty()) {
					for (Map.Entry<String, AgentCall> entry : agentMap.entrySet()) {
						if (entry != null && entry.getValue() != null && entry.getValue().getCampaignId() != null
								&& entry.getValue().getCampaignId().equalsIgnoreCase(campaign.getId())) {
							registeredAgents.add(entry.getKey());
						}
					}
				}
				int activeAgents = registeredAgents.size();
				prospectInMemoryCacheServiceImpl.loadCache(campaign, activeAgents);
			}
			
			Date date = new Date();
			logger.debug("Cache refresh started for [{}] campaign.", campaign.getName());

			// HACK: Calculating date in Pacific TZ because all other TZ will be in same
			// date of Pacific TZ during their business hours.
			String strCurrentDateYMD = getCurrentDateInPacificTZinYMD();
			int dailyCallMaxRetriesLimit = 1;
			if (campaign != null && campaign.getDailyCallMaxRetries() > 0) {
				// get daily call max retry count from campaign
				dailyCallMaxRetriesLimit = campaign.getDailyCallMaxRetries();
			} else {
				dailyCallMaxRetriesLimit = propertyService.getIntApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DAILY_CALL_MAX_RETRIES.name(), 1);
			}
			int maxCallRetryLimit = 5;
			if (campaign != null && campaign.getCallMaxRetries() > 0) {
				// get call max retry count from campaign
				maxCallRetryLimit = campaign.getCallMaxRetries();
			} else {
				maxCallRetryLimit = propertyService
						.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
			}
			Long maxDailyCallRetryCount = Long
					.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));

			if (!campaign.isEnableProspectCaching()) {
				if (campaign.isUseSlice()) {
					buildCacheBySlicesOld(campaign, excludeStateCodes, currentRetry, strCurrentDateYMD,
							dailyCallMaxRetriesLimit, maxCallRetryLimit, maxDailyCallRetryCount);
				} else {
					buildCacheByQualityBucketOld(campaign, excludeStateCodes, strCurrentDateYMD, dailyCallMaxRetriesLimit,
							maxCallRetryLimit, maxDailyCallRetryCount);
				}
			}

			Date currentDate = new Date();
			long difference = currentDate.getTime() - date.getTime();
			logger.debug("Cache refresh completed for [{}] campaign. Cache Build Time in MS [{}]", campaign.getName(),
					difference);
		}
		// check current slice is for indirect prospects or not
		if (prospectCacheServiceImpl.getCampaignSliceIndirect(campaignId).equalsIgnoreCase("true")) {
			isIndirectFound = true;
			float campaignspeed  = campaign.getTeam().getCampaignSpeed();
			if(campaignspeed>0) {
				float conspeed = countOfActiveAgents * campaignspeed;
				size =  Math.round(conspeed);
			}else {
				size = countOfActiveAgents * 1;
			}
		}
		// fetch prospectCallLog list for call from cache.
		List<ProspectCallCacheDTO> queuedProspectCallLogList = new ArrayList<>();
		List<ProspectCallDTO> queuedProspectCallLogListOld = new ArrayList<>();
		if (isIndirectFound || !campaign.isRetrySpeedEnabled()) {
			if (campaign.isEnableProspectCaching()) {
				queuedProspectCallLogList = priorityQueueCacheOperationService.remove(campaign.getId(), size);
			} else {
				queuedProspectCallLogListOld = prospectCacheServiceImpl.removeProspectCallLogListOld(campaign.getId(), size);
			}
		} else {
			// TODO : currentRetry is 0. need an enhancement to adjust R0 based on retry count.
			if (campaign.isEnableProspectCaching()) {
				queuedProspectCallLogList = getCallableProspectsByRetrySpeed(campaign, countOfActiveAgents, queuedCallCount,
						queuedProspectDTO, currentRetry);
			} else {
				queuedProspectCallLogListOld = getCallableProspectsByRetrySpeedOld(campaign, countOfActiveAgents, queuedCallCount,
						queuedProspectDTO, currentRetry);
			}
		}

		// fetch list of companies for success prospects
		List<DomainCompanyCountPair> dataFromDB = getSuccessDomainsAndCompaniesCount(campaign);
		LinkedList<String> manualDialProspectCallIds = prospectCacheServiceImpl.getManualDialProspectCallIds();
		if (campaign.isEnableProspectCaching()) {
			Iterator<ProspectCallCacheDTO> iterator = queuedProspectCallLogList.iterator();
			while (iterator.hasNext()) {
				ProspectCallCacheDTO prospectCall = iterator.next();
				if (!prospectCall.getProspect().getProspectType().equals(XtaasConstants.PROSPECT_TYPE.RESEARCH.name())) {			
					String domain = "";
					if (prospectCall.getProspect().getCustomAttributes().get("domain") != null) {
						domain = prospectCall.getProspect().getCustomAttributes().get("domain").toString();
					}
					String company = prospectCall.getProspect().getCompany();
					String emailId = prospectCall.getProspect().getEmail();
					String domainEmail = prospectCall.getProspect().getEmail();

					if(domainEmail  !=null && !domainEmail.isEmpty() ) {
						domainEmail = domainEmail.substring(domainEmail.indexOf("@")+1, domainEmail.length());
						if((domain.contains("http://") || domain.contains("HTTP://"))){
							domain= domain.substring(7,domain.length());
						}

						if((domain.contains("https://") || domain.contains("HTTPS://"))){
							domain= domain.substring(8,domain.length());
						}

						if((domain.contains("www.") || domain.contains("WWW."))){
							domain= domain.substring(4,domain.length());
						}
					}

					boolean localDNCListCheckPassed = checkLocalDNCList(null, prospectCall, team.getPartnerId());
					// check if prospect is in the call in manual dial.
					if (manualDialProspectCallIds.contains(prospectCall.getProspectCallId())) {
						logger.debug("Prospect [{}] is already in the manual call.", prospectCall.getProspectCallId());
						prospectCacheServiceImpl.removeFromManualDialProspectCallIds(prospectCall.getProspectCallId());
						iterator.remove();
					} else if (checkIfProspectPresentInMap(campaignId, prospectCall.getProspectCallId())) {
						iterator.remove(); 
						removeProspectCallIdFromCache(campaignId, prospectCall.getProspectCallId());
					} else if (!localDNCListCheckPassed) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR, null, null,
								null, null, null);
						iterator.remove();
					} else if (isSuccessDuplicate(campaign, domain, domainEmail, prospectCall.getProspect().getCompany(),dataFromDB)) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.SUCCESS_DUPLICATE, null,
								null, null, null, null);
						iterator.remove();
					}else if (isSuppressed(campaign, domain, domainEmail,emailId)) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.SUCCESS_DUPLICATE, null,
								null, null, null, null);
						iterator.remove();
					}else if(isWrappedUp(prospectCall)) {
						//TODO  This i have added as an immediate fix to identify why success leads are coming in callin queue.
						// once identified, need to remove this logic. this should be handled either at query or at cache building.
						iterator.remove();
					}
				}
			}
		} else {
			Iterator<ProspectCallDTO> iterator = queuedProspectCallLogListOld.iterator();
			while (iterator.hasNext()) {
				ProspectCallDTO prospectCall = iterator.next();
				if (!prospectCall.getProspect().getProspectType().equals(XtaasConstants.PROSPECT_TYPE.RESEARCH.name())) {
					String domain = "";
					if (prospectCall.getProspect().getCustomAttributes().get("domain") != null) {
						domain = prospectCall.getProspect().getCustomAttributes().get("domain").toString();
					}
					String company = prospectCall.getProspect().getCompany();
					String emailId = prospectCall.getProspect().getEmail();
					String domainEmail = prospectCall.getProspect().getEmail();

					if(domainEmail  !=null && !domainEmail.isEmpty() ) {
						domainEmail = domainEmail.substring(domainEmail.indexOf("@")+1, domainEmail.length());
						if((domain.contains("http://") || domain.contains("HTTP://"))){
							domain= domain.substring(7,domain.length());
						}

						if((domain.contains("https://") || domain.contains("HTTPS://"))){
							domain= domain.substring(8,domain.length());
						}

						if((domain.contains("www.") || domain.contains("WWW."))){
							domain= domain.substring(4,domain.length());
						}
					}

					boolean localDNCListCheckPassed = checkLocalDNCList(prospectCall, null, team.getPartnerId());
					// check if prospect is in the call in manual dial.
					if (manualDialProspectCallIds.contains(prospectCall.getProspectCallId())) {
						logger.debug("Prospect [{}] is already in the manual call.", prospectCall.getProspectCallId());
						prospectCacheServiceImpl.removeFromManualDialProspectCallIds(prospectCall.getProspectCallId());
						iterator.remove();
					} else if (!localDNCListCheckPassed) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR, null, null,
								null, null, null);
						iterator.remove();
					} else if (isSuccessDuplicate(campaign, domain, domainEmail, prospectCall.getProspect().getCompany(),dataFromDB)) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.SUCCESS_DUPLICATE, null,
								null, null, null, null);
						iterator.remove();
					}else if (isSuppressed(campaign, domain, domainEmail,emailId)) {
						updateProspectCall(prospectCall.getProspectCallId(), ProspectCallStatus.SUCCESS_DUPLICATE, null,
								null, null, null, null);
						iterator.remove();
					}else if(isWrappedUp(prospectCall)) {
						iterator.remove();
					}
				}
			}
		}
		
		logger.info("##### getQueuedProspects() - Found total [{}] prospects for [{}] campaign.",
				queuedProspectCallLogList.size(), campaign.getName());
		if (campaign.isEnableProspectCaching()) {
			List<ProspectCallCacheDTO> totalProspectsInCache = priorityQueueCacheOperationService.getCurrentState(campaignId);
			if (totalProspectsInCache == null || totalProspectsInCache.size() == 0) {
				logger.info("Reload the cache as no prosects in  [{}] prospect caching enabled campaign. ", campaign.getName());
				prospectInMemoryCacheServiceImpl.loadCache(campaign, countOfActiveAgents);
			} else {
				logger.info("Prospects currently present in cache [{}] of campaign name [{}]", totalProspectsInCache.size(), campaign.getName());
			}
			if (queuedProspectCallLogList != null && queuedProspectCallLogList.size() > 0) {
				int totalCallableCount = prospectCacheServiceImpl.getCachedProspectCallLogCount(campaignId);
				totalCallableCount = totalCallableCount - queuedProspectCallLogList.size();
				prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
			}
			queuedProspectDTO.setQueuedProspects(queuedProspectCallLogList);
			if (prospectCacheServiceImpl.checkIfCampaignIdPresentInSet(campaignId)) {
				Set<String> prospectCallIds = queuedProspectCallLogList.stream().map(s -> s.getProspectCallId())
						.collect(Collectors.toSet());
				prospectCacheServiceImpl.storeProspectsGivenToDialer(campaignId, prospectCallIds);
			}
		} else {
			if (queuedProspectCallLogListOld != null && queuedProspectCallLogListOld.size() > 0) {
				int totalCallableCount = prospectCacheServiceImpl.getCachedProspectCallLogCount(campaignId);
				totalCallableCount = totalCallableCount - queuedProspectCallLogListOld.size();
				prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
			}
			List<ProspectCallCacheDTO> prospectListDTO = new ArrayList<ProspectCallCacheDTO>();
			for (ProspectCallDTO prospectCallDTO : queuedProspectCallLogListOld) {
				ProspectCallCacheDTO cacheDto = new ProspectCallCacheDTO(prospectCallDTO);
				prospectListDTO.add(cacheDto);
			}
			queuedProspectDTO.setQueuedProspects(prospectListDTO);
		}

		int cacheSize = prospectCacheServiceImpl.getCachedProspectCallLogCount(campaignId);
		logger.info(new SplunkLoggingUtils("CacheCallableCount", campaignId).campaignId(campaignId)
				.campaignName(campaign.getName()).addField("CacheCallableCount", cacheSize).build());

	   if (prospectCacheServiceImpl.getCachedCallableDataInDBCount(campaignId) > 0) {
			logger.info("CampaignId = {}, Callable Count = {} ",
					campaignId, prospectCacheServiceImpl.getCachedCallableDataInDBCount(campaignId));
		}
	   if (isTimeZoneChanged) {
		   generateMissedCallbackCache(campaign, false);
		   generateCallbackCache(campaign, false);
	   }
	    
		return queuedProspectDTO;
		
	}
	
	private void checkIfCallablesFallsBelowThreshold(Campaign campaignFromDB) {
		if (campaignFromDB != null && campaignFromDB.getOrganizationId() != null) {
			Organization orgFromMemory = organizationService
					.getOrganizationFromMemory(campaignFromDB.getOrganizationId());
			if (orgFromMemory != null && orgFromMemory.getLowCallableAlertThreshold() != 0
					&& orgFromMemory.isEnableAgentInactivity()) {
				long callBackTime = 0;
				if (campaignFromDB.getType().equals(CampaignTypes.LeadVerification)) {
					callBackTime = propertyService.getIntApplicationPropertyValue(
							XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
				} else {
					callBackTime = propertyService.getIntApplicationPropertyValue(
							XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
				}
				List<String> excludeStateCodes = getStateCodesWithBusinessClosed();
				String strCurrentDateYMD = getCurrentDateInPacificTZinYMD();
				int dailyCallMaxRetriesLimit = 1;
				dailyCallMaxRetriesLimit = campaignFromDB != null && campaignFromDB.getDailyCallMaxRetries() > 0
						? campaignFromDB.getDailyCallMaxRetries()
						: propertyService.getIntApplicationPropertyValue(
								XtaasConstants.APPLICATION_PROPERTY.DAILY_CALL_MAX_RETRIES.name(), 1);
				int maxCallRetryLimit = 5;
				maxCallRetryLimit = campaignFromDB != null && campaignFromDB.getCallMaxRetries() > 0
						? campaignFromDB.getCallMaxRetries()
						: propertyService.getIntApplicationPropertyValue(
								XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
				Long maxDailyCallRetryCount = Long
						.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));
				int queuedProspectCountFromDB = prospectLogRepository.countProspectsForDialer(campaignFromDB.getId(),
						excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit,
						XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
				if (queuedProspectCountFromDB < orgFromMemory.getLowCallableAlertThreshold()) {
					if (lowCallableAlertMap.containsKey(campaignFromDB.getId())) {
						long timeDiffInMinutes = XtaasDateUtils
								.getTimeDiffrenceInMinutes(new Date(), lowCallableAlertMap.get(campaignFromDB.getId()));
						long configuredTime = propertyService.getIntApplicationPropertyValue(
								XtaasConstants.APPLICATION_PROPERTY.LOW_CALLABLE_ALERT_TIME_IN_MIN.name(), 30);
						if (timeDiffInMinutes >= configuredTime) {
							sendLowCallableAlertToSupervisor(campaignFromDB);
						}
					} else {
						sendLowCallableAlertToSupervisor(campaignFromDB);
					}
				} else {
					lowCallableAlertMap.remove(campaignFromDB.getId());
				}
			}
		}
	}

	private void notifySupervisor(String supervisorId, String campaignName) {
		PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = " Low Callable Alert ( " + campaignName
				+ " ) : You need to do another data buy since callable level is low";
		teamNoticeService.saveUserNotices(supervisorId, notice);
	}

	private void sendLowCallableAlertToSupervisor(Campaign campaignFromDB) {
		Set<String> assignedSupervisors = new HashSet<String>();
		assignedSupervisors.addAll(campaignFromDB.getPartnerSupervisors());
		if (campaignFromDB.getTeam() != null && campaignFromDB.getTeam().getSupervisorId() != null) {
			assignedSupervisors.add(campaignFromDB.getTeam().getSupervisorId());
		}
		lowCallableAlertMap.put(campaignFromDB.getId(), new Date());
		if (assignedSupervisors != null && assignedSupervisors.size() > 0) {
			for (String supervisor : assignedSupervisors) {
				notifySupervisor(supervisor, campaignFromDB.getName());
			}
		}
	}
	
	private boolean checkIfProspectPresentInMap(String campaignId, String prospectCallId) {
		Set<String> callIdsFromMap = prospectCacheServiceImpl.getProspectCallIdsOfCampaign(campaignId);
		if (callIdsFromMap != null && callIdsFromMap.size() > 0 && callIdsFromMap.contains(prospectCallId)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void removeProspectCallIdFromCache(String campaignId, String prospectCallId) {
		Set<String> prospectIdsFromCache = prospectCacheServiceImpl.getProspectCallIdsOfCampaign(campaignId);
		if (prospectIdsFromCache != null && prospectIdsFromCache.size() > 0) {
			Iterator<String> iterator = prospectIdsFromCache.iterator();
			while (iterator.hasNext()) {
				String nextElement = iterator.next();
				if (nextElement.equalsIgnoreCase(prospectCallId)) {
					iterator.remove();
				}
			}
		}
	}
	
	public void buildCacheByQualityBucketOld(Campaign campaign, List<String> excludeStateCodes,
			String strCurrentDateYMD, int dailyCallMaxRetriesLimit, int maxCallRetryLimit,
			Long maxDailyCallRetryCount) {
		String campaignId = campaign.getId();
		long callBackTime = 0;
		if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
		} else {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
		}

		int queuedProspectCount = prospectLogRepository.countProspectsForDialer(campaignId, excludeStateCodes,
				maxDailyCallRetryCount, maxCallRetryLimit,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
		int expiredCallbackProspectCount = prospectLogRepository.countByExpiredCallbackOld(new Date(), campaignId,
				excludeStateCodes, maxDailyCallRetryCount,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));

		int totalCallableCount = queuedProspectCount + expiredCallbackProspectCount;
		prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
		prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "false");
		List<ProspectCallDTO> prospectCallDTOList = new ArrayList<ProspectCallDTO>(0);
		Pageable pageable = PageRequest.of(0, 10000, buildProspectSortOrder(campaign));
		List<ProspectCallLog> queuedProspectCallLogs = new ArrayList<>();
		queuedProspectCallLogs = prospectLogRepository.findProspectsForDialer(campaignId, excludeStateCodes,
				maxDailyCallRetryCount, maxCallRetryLimit,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), pageable);

		List<ProspectCallLog> callbackProspectCallLogList = new ArrayList<>();
		
		if (!campaign.isEnableCallbackFeature()) {
			Pageable callbackPageable = PageRequest.of(0, 5000, Direction.ASC, "callbackDate",
					"prospectCall.callRetryCount");
			callbackProspectCallLogList = prospectLogRepository.findByExpiredCallbackOld(new Date(), campaignId,
					excludeStateCodes, maxDailyCallRetryCount,
					XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), callbackPageable);
			List<ProspectCallLog> pcLogToCache = new ArrayList<ProspectCallLog>();
		}
		// get CALLBACK prospects
		Map<String, ProspectCallDTO> pDTOMap = getMultiProspectsQueue(queuedProspectCallLogs,
				callbackProspectCallLogList, dailyCallMaxRetriesLimit, strCurrentDateYMD);
		logger.info("buildCacheByQualityBucketOld() :  Campaign Id : [{}], ExcludedStateCodes : [{}], MaxDailyCallRetryCount : [{}], MaxCallRetryLimit : [{}]",
				campaignId, excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit);
		for (ProspectCallLog p : queuedProspectCallLogs) {
			ProspectCallDTO rPCLog = pDTOMap.get(p.getProspectCall().getProspectCallId());
			if (rPCLog != null) {
				prospectCallDTOList.add(rPCLog);
			}
		}
		if (!campaign.isEnableCallbackFeature()) {
			for (ProspectCallLog pc : callbackProspectCallLogList) {
				ProspectCallDTO rPCLog = pDTOMap.get(pc.getProspectCall().getProspectCallId());
				if (rPCLog != null) {
					prospectCallDTOList.add(rPCLog);
				}
			}
		}
		logger.info("Prospects found from DB by using quality bucket by old impl are :  [{}] , Campaign Id : [{}]",
				prospectCallDTOList.size(), campaignId);
		prospectCacheServiceImpl.addProspectCallLogListOld(campaignId, prospectCallDTOList);
	}
	
	public void buildCacheByQualityBucket(Campaign campaign, List<String> excludeStateCodes, List<String> openTimeZones,
			String strCurrentDateYMD, int dailyCallMaxRetriesLimit, int maxCallRetryLimit, Long maxDailyCallRetryCount,
			int noOfProspects) {
		String campaignId = campaign.getId();
		// read total callable count and add into map
		long callBackTime = 0;
		if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
		} else {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
		}

		int queuedProspectCount = 0;
		int expiredCallbackProspectCount = 0;
		queuedProspectCount = prospectLogRepository.countProspectsForDialer(campaignId, excludeStateCodes,
				maxDailyCallRetryCount, maxCallRetryLimit,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
		expiredCallbackProspectCount = prospectLogRepository.countByExpiredCallback(campaignId, new Date(),
				excludeStateCodes, maxDailyCallRetryCount,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));

		int totalCallableCount = queuedProspectCount + expiredCallbackProspectCount;
		prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
		prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "false");
		List<ProspectCallCacheDTO> prospectCallDTOList = new ArrayList<ProspectCallCacheDTO>(0);
		Pageable pageable = PageRequest.of(0, noOfProspects, buildProspectSortOrder(campaign));
		List<ProspectCallLog> queuedProspectCallLogs = new ArrayList<>();

		queuedProspectCallLogs = prospectLogRepository.findProspectsForDialer(campaignId, excludeStateCodes,
				maxDailyCallRetryCount, maxCallRetryLimit,
				XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), pageable);

		List<ProspectCallLog> callbackProspectCallLogList = new ArrayList<>();
		if (!campaign.isEnableCallbackFeature()) {
			Pageable callbackPageable = PageRequest.of(0, 5000, Direction.ASC, "callbackDate",
					"prospectCall.callRetryCount");
			callbackProspectCallLogList = prospectLogRepository.findByExpiredCallback(campaignId, new Date(),
					excludeStateCodes, maxDailyCallRetryCount,
					XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), callbackPageable);
			List<ProspectCallLog> pcLogToCache = new ArrayList<ProspectCallLog>();
		}
		// get CALLBACK prospects
		Map<String, ProspectCallDTO> pDTOMap = getMultiProspectsQueue(queuedProspectCallLogs,
				callbackProspectCallLogList, dailyCallMaxRetriesLimit, strCurrentDateYMD);
		for (ProspectCallLog p : queuedProspectCallLogs) {
			ProspectCallDTO rPCLog = pDTOMap.get(p.getProspectCall().getProspectCallId());
			if (rPCLog != null) {
				prospectCallDTOList.add(new ProspectCallCacheDTO(rPCLog));
			}
		}
		if (!campaign.isEnableCallbackFeature()) {
			for (ProspectCallLog pc : callbackProspectCallLogList) {
				ProspectCallDTO rPCLog = pDTOMap.get(pc.getProspectCall().getProspectCallId());
				if (rPCLog != null) {
					prospectCallDTOList.add(new ProspectCallCacheDTO(rPCLog));
				}
			}
		}
		logger.info("buildCacheByQualityBucket() :  Campaign Id : [{}], ExcludedStateCodes : [{}], MaxDailyCallRetryCount : [{}], MaxCallRetryLimit : [{}]",
				campaignId, excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit);
//		prospectCacheServiceImpl.addProspectCallLogList(campaignId, prospectCallDTOList);
		prospectInMemoryCacheServiceImpl.thresHoldMap.put(campaignId, prospectCallDTOList.size());
		List<ProspectCallCacheDTO> totalProspectsInCache = priorityQueueCacheOperationService
				.getCurrentState(campaignId);
		if (totalProspectsInCache != null && totalProspectsInCache.size() > 0) {
			prospectCacheServiceImpl.storeCampaignIdsInSet(campaignId);
		}
		priorityQueueCacheOperationService.insert(campaignId, prospectCallDTOList,
				buildProspectSortOrderPriorityQueue(campaign.getTeam().getLeadSortOrder()));
		prospectInMemoryCacheServiceImpl.duplicateJobCheckMap.remove(campaignId);
		getCallableList(campaignId);
	}
	
	public void buildCacheBySlicesOld(Campaign campaign, List<String> excludeStateCodes, int currentRetry,
            String strCurrentDateYMD, int dailyCallMaxRetriesLimit, int maxCallRetryLimit,
            Long maxDailyCallRetryCount) {
        String campaignId = campaign.getId();
        List<ProspectCallDTO> prospectCallDTOList = new ArrayList<ProspectCallDTO>(0);
        long callBackTime = 0;
        if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
            callBackTime = propertyService.getIntApplicationPropertyValue(
                    XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
        } else {
            callBackTime = propertyService.getIntApplicationPropertyValue(
                    XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
        }
        int queuedProspectCount = prospectLogRepository.countProspectsForDialer(campaignId, excludeStateCodes,
                maxDailyCallRetryCount, maxCallRetryLimit,
                XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
        int expiredCallbackProspectCount = prospectLogRepository.countByExpiredCallbackOld(new Date(), campaignId,
                excludeStateCodes, maxDailyCallRetryCount,
                XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
        int totalCallableCount = queuedProspectCount + expiredCallbackProspectCount;
        prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
        prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "false");
        List<ProspectCallLog> queuedProspectCallLogs = new ArrayList<>();
        List<String> slices = Arrays.asList("11111", "22222", "33333");
        for (String slice : slices) {
            long count = 0;
//          for (int j = 1; j <= dailyCallMaxRetriesLimit; j++) {
//              sortType = j % 2;
//              currentRetry = j - 1;
//              maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD + String.format("%02d", j));
//              count = prospectLogRepository.countProspectsForDialerBySlice(campaign.getId(), excludeStateCodes,
//                      maxDailyCallRetryCount, maxCallRetryLimit, slice,
//                      XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
//              if (count > 0) {
//                  prospectCacheServiceImpl.addInCachedDailyCallRetryCountMap(campaignId, j);
//                  break;
//              }
//          }
            List<String> slicesToBeFetched = new ArrayList<>();
            if (slice.equalsIgnoreCase("11111")) {
                slicesToBeFetched = Arrays.asList("slice0");
            } else if (slice.equalsIgnoreCase("22222")) {
                slicesToBeFetched = Arrays.asList("slice1", "slice2", "slice3",  "slice4", "slice5", "slice6");
            } else if (slice.equalsIgnoreCase("33333")) {
                slicesToBeFetched = Arrays.asList("slice10");
            }
            maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));
            count = prospectLogRepository.countProspectsForDialerBySlice(campaign.getId(), excludeStateCodes,
                    maxDailyCallRetryCount, maxCallRetryLimit, slicesToBeFetched,
                    XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
            if (count > 0) {
                logger.info("buildCacheBySlicesOld() : [{}] prospects found for slices [{}]",
                        count, slicesToBeFetched);
                prospectCacheServiceImpl.addInCachedDailyCallRetryCountMap(campaignId, (int)count);
                List<Order> orders = new ArrayList<Order>();
                if (slice.equalsIgnoreCase("11111")) {
                    orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
                    orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
                    orders.add(new Order(Direction.ASC, "updatedDate"));
                } else {
                    orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
                    orders.add(new Order(Direction.ASC, "updatedDate"));
                }
                int prospectsToBeFetchedFromDB = 0;
                if (count > 5000) {
                    prospectsToBeFetchedFromDB = 5000;
                } else {
                    prospectsToBeFetchedFromDB = (int)count;
                }
                Pageable pageable = PageRequest.of(0, prospectsToBeFetchedFromDB, Sort.by(orders));
                queuedProspectCallLogs = prospectLogRepository.findProspectsForDialerBySlice(campaign.getId(),
                        excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit, slicesToBeFetched,
                        XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), pageable);
                if (slice.equalsIgnoreCase("11111")) {
                    prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "true");
                }
                if (slice.equalsIgnoreCase("33333")) {
                    prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "true");
                }
                break;
            }
            logger.info("buildCacheBySlicesOld() :  Campaign Id : [{}], ExcludedStateCodes : [{}], MaxDailyCallRetryCount : [{}], MaxCallRetryLimit : [{}], Slices : [{}]",
                    campaignId, excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit, slice);
        }
        
        List<ProspectCallLog> callbackProspectCallLogList = new ArrayList<>();
        Map<String, ProspectCallDTO> pDTOMap = getMultiProspectsQueue(queuedProspectCallLogs,
                callbackProspectCallLogList, dailyCallMaxRetriesLimit, strCurrentDateYMD);
        for (ProspectCallLog p : queuedProspectCallLogs) {
            ProspectCallDTO rPCLog = pDTOMap.get(p.getProspectCall().getProspectCallId());
            if (rPCLog != null) {
                prospectCallDTOList.add(rPCLog);
            }
        }
        if (!campaign.isEnableCallbackFeature()) {
			for (ProspectCallLog pc : callbackProspectCallLogList) {
				ProspectCallDTO rPCLog = pDTOMap.get(pc.getProspectCall().getProspectCallId());
				if (rPCLog != null) {
					prospectCallDTOList.add(rPCLog);
				}
			}
		}
        logger.info("Prospects found from DB by using slices by old impl are :  [{}] , Campaign Id : [{}]",
                prospectCallDTOList.size(), campaignId);
        prospectCacheServiceImpl.addProspectCallLogListOld(campaign.getId(), prospectCallDTOList);
    }

    public void buildCacheBySlices(Campaign campaign, List<String> excludeStateCodes, List<String> openTimeZones,
            int currentRetry, String strCurrentDateYMD, int dailyCallMaxRetriesLimit, int maxCallRetryLimit,
            Long maxDailyCallRetryCount, int noOfProspects) {
        String campaignId = campaign.getId();
        // List<String> slices = XtaasConstants.slices;
        List<ProspectCallCacheDTO> prospectCallDTOList = new ArrayList<ProspectCallCacheDTO>();
        // read total callable count and add into map
        long callBackTime = 0;
        if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
            callBackTime = propertyService.getIntApplicationPropertyValue(
                    XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_AUTO_CALLBACK_MINUTES.name(), 0);
        } else {
            callBackTime = propertyService.getIntApplicationPropertyValue(
                    XtaasConstants.APPLICATION_PROPERTY.AUTO_CALLBACK_MINUTES.name(), 0);
        }

        int queuedProspectCount = 0;
        int expiredCallbackProspectCount = 0;
        queuedProspectCount = prospectLogRepository.countProspectsForDialer(campaignId, excludeStateCodes,
                maxDailyCallRetryCount, maxCallRetryLimit,
                XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
        expiredCallbackProspectCount = prospectLogRepository.countByExpiredCallback(campaignId, new Date(),
                excludeStateCodes, maxDailyCallRetryCount,
                XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));

        int totalCallableCount = queuedProspectCount + expiredCallbackProspectCount;
        prospectCacheServiceImpl.addInCachedProspectCallLogCountMap(campaignId, totalCallableCount);
        prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "false");
        List<ProspectCallLog> queuedProspectCallLogs = new ArrayList<>();
        List<String> slices = Arrays.asList("11111", "22222", "33333");
        for (String slice : slices) {
            long count = 0; 
            List<String> slicesToBeFetched = new ArrayList<>();
            if (slice.equalsIgnoreCase("11111")) {
                slicesToBeFetched = Arrays.asList("slice0");
            } else if (slice.equalsIgnoreCase("22222")) {
                slicesToBeFetched = Arrays.asList("slice1", "slice2", "slice3",  "slice4", "slice5", "slice6");
            } else if (slice.equalsIgnoreCase("33333")) {
                slicesToBeFetched = Arrays.asList("slice10");
            }

            maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));
            count = prospectLogRepository.countProspectsForDialerBySlice(campaign.getId(), excludeStateCodes,
                    maxDailyCallRetryCount, maxCallRetryLimit, slicesToBeFetched,
                    XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime));
            if (count > 0) {
                logger.info("buildCacheBySlices() : [{}] prospects found for slices [{}]",
                        count, slicesToBeFetched);
                List<Order> orders = new ArrayList<Order>();
                prospectCacheServiceImpl.addInCachedDailyCallRetryCountMap(campaignId, (int)count);
                if (slice.equalsIgnoreCase("11111")) {
                    orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
                    orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
                    orders.add(new Order(Direction.ASC, "updatedDate"));
                } else {
                    orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
                    orders.add(new Order(Direction.ASC, "updatedDate"));
                }
                Pageable pageable = PageRequest.of(0, noOfProspects, Sort.by(orders));
                queuedProspectCallLogs = prospectLogRepository.findProspectsForDialerBySlice(campaign.getId(),
                        excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit, slicesToBeFetched,
                        XtaasDateUtils.getTimeBeforeGivenMinutes(new Date(), (int) callBackTime), pageable);
                if (slice.equalsIgnoreCase("11111") || slice.equalsIgnoreCase("33333")) {
                    prospectCacheServiceImpl.addInCampaignSliceIndirectMap(campaignId, "true");
                }
                break;
            }   
            logger.info("buildCacheBySlices() :  Campaign Id : [{}], ExcludedStateCodes : [{}], MaxDailyCallRetryCount : [{}], MaxCallRetryLimit : [{}], Slices : [{}]",
                    campaignId, excludeStateCodes, maxDailyCallRetryCount, maxCallRetryLimit, slice);
        }

        // get CALLBACK prospects - removed callback query
        List<ProspectCallLog> callbackProspectCallLogList = new ArrayList<>();
        Map<String, ProspectCallDTO> pDTOMap = getMultiProspectsQueue(queuedProspectCallLogs,
                callbackProspectCallLogList, dailyCallMaxRetriesLimit, strCurrentDateYMD);
        for (ProspectCallLog p : queuedProspectCallLogs) {
            ProspectCallDTO rPCLog = pDTOMap.get(p.getProspectCall().getProspectCallId());
            if (rPCLog != null) {
                prospectCallDTOList.add(new ProspectCallCacheDTO(rPCLog));
            }
        }
        if (!campaign.isEnableCallbackFeature()) {
			for (ProspectCallLog pc : callbackProspectCallLogList) {
				ProspectCallDTO rPCLog = pDTOMap.get(pc.getProspectCall().getProspectCallId());
				if (rPCLog != null) {
					prospectCallDTOList.add(new ProspectCallCacheDTO(rPCLog));
				}
			}
		}
//      prospectCacheServiceImpl.addProspectCallLogList(campaign.getId(), prospectCallDTOList);
        List<ProspectCallCacheDTO> totalProspectsInCache = priorityQueueCacheOperationService
                .getCurrentState(campaignId);
        if (totalProspectsInCache != null && totalProspectsInCache.size() > 0) {
            prospectCacheServiceImpl.storeCampaignIdsInSet(campaignId);
        }
        prospectInMemoryCacheServiceImpl.thresHoldMap.put(campaignId, prospectCallDTOList.size());
        priorityQueueCacheOperationService.insert(campaign.getId(), prospectCallDTOList,
                buildProspectSortOrderPriorityQueue(campaign.getTeam().getLeadSortOrder()));
        prospectInMemoryCacheServiceImpl.duplicateJobCheckMap.remove(campaignId);
        
        getCallableList(campaignId);
    }

	
	private void getCallableList(String campaignId) {
		if (campaignId != null && !campaignId.isEmpty()) {
			Campaign campaign = campaignRepository.findOneById(campaignId);
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (organization.isLiteMode()) {
				if (campaign != null && campaign.getUpperThreshold() != 0) {
					int dailyCallMaxRetries = campaign.getDailyCallMaxRetries();
					int callMaxRetries = campaign.getCallMaxRetries();
					String strCurrentDateYMD = XtaasDateUtils.getCurrentDateInPacificTZinYMD();
					int maxDailyCallRetryCount = Integer
							.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetries));

					int prospectCallLogListCount = prospectCallLogRepository
							.countContactsByQueuedInCampaign(campaign.getId());

					if (callMaxRetries >= 1) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(1,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 2) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(2,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 3) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(3,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 4) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(4,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 5) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(5,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 6) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(6,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 7) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(7,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 8) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(8,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 9) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(9,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}
					if (callMaxRetries >= 10) {
						int count = prospectCallLogRepository
								.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(10,
										maxDailyCallRetryCount, campaign.getId());
						prospectCallLogListCount = prospectCallLogListCount + count;
					}

					if (prospectCallLogListCount > 0) {
						prospectCacheServiceImpl.addInCachedCallableDataInDBMap(campaign.getId(),
								prospectCallLogListCount);
						int callableCount = prospectCacheServiceImpl.getCachedCallableDataInDBCount(campaign.getId());
						if (callableCount > 0) {
							if (campaign != null && campaign.getLowerThreshold() > 0
									&& campaign.getUpperThreshold() > 0) {
								checkThreshold(campaign, callableCount);
							}
						}
					}
				}
			}
		}
	}
	 
	private void checkThreshold(Campaign campaign, int callableCount) {
		String thresholdType = null;
		String msg = null;
		if (callableCount > campaign.getUpperThreshold()) {
			campaign.setAutoGoldenPass(true);
			campaignRepository.save(campaign);
			thresholdType = "UpperThreshold";
		} else if (callableCount < campaign.getLowerThreshold()) {
			campaign.setAutoGoldenPass(false);
			campaignRepository.save(campaign);
			thresholdType = "LowerThreshold";
		}
		if (thresholdType != null && !thresholdType.isEmpty()) {
			if (thresholdType.equalsIgnoreCase("UpperThreshold")) {
				msg = "DIALING MODE CHANGED: The campaign " + campaign.getName()
						+ " can dial in AUTO mode since callable data is sufficient. Review dialing modes.";
			} else {
				msg = "DIALING MODE CHANGED: The campaign " + campaign.getName()
						+ " cannot dial in AUTO mode since callable data is insufficient. Review dialing modes.";
			}
			if (campaign.getAdminSupervisorIds() != null && !campaign.getAdminSupervisorIds().isEmpty()
					&& campaign.getAdminSupervisorIds().size() > 0) {
				String s = msg;
				for (String supervisorId : campaign.getAdminSupervisorIds()) {
					List<TeamNotice> teamNotice = teamNoticeRepository.getNoticesByRequestorId(new Date(), supervisorId);
					List<TeamNotice> match = teamNotice.stream().filter(obj -> obj.getNotice().equalsIgnoreCase(s)).collect(Collectors.toList());
					if(match != null && !match.isEmpty() && match.size() > 0) {
						teamNoticeRepository.deleteAll(match);
						PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						teamNoticeSerive.saveUserNotices(supervisorId, msg);
					}else {
						PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						teamNoticeSerive.saveUserNotices(supervisorId, msg);
					}
				}
			}
		}

	}

	private ProspectCallDTO createDummyProspect(Campaign campaign) {
		ProspectCallLog prospectCallLog = prospectCallLogRepository
				.findByProspectCallId("7aeee561-588e-4cb6-812e-ec6123456789");
		ProspectCallDTO prospectCallDTO = new ProspectCallDTO(prospectCallLog.getProspectCall());
		prospectCallDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.RESEARCH.name());
		prospectCallDTO.setCampaignId(campaign.getId());
		return prospectCallDTO;
	}

	private List<ProspectCallCacheDTO> getCallableProspectsByRetrySpeed(Campaign campaign, int countOfActiveAgents,
																   int queuedCallCount, QueuedProspectDTO queuedProspectDTO, int currentRetry) {
		List<ProspectCallCacheDTO> callableProspects = new ArrayList<>();
		Map<String, Float> retrySpeed = campaign.getRetrySpeed();
		List<Float> ratios = new ArrayList<>();
		Float defaultCallSpeed = (float) campaign.getTeam().getCallSpeedPerMinPerAgent().getValue();
		if (retrySpeed == null) {
			retrySpeed = new HashMap<>();
		}
		int callMaxRetries = campaign.getCallMaxRetries();
		for (int i = 0; i <= callMaxRetries; i++) {
			String str = "R" + i;
			if (retrySpeed.get(str) == null || retrySpeed.get(str) == 0) {
				retrySpeed.put(str, defaultCallSpeed);
			}
		}
		Float baseRetrySpeed = retrySpeed.get("R0") + currentRetry;
		Float requiredProspects = Float.parseFloat("1");
		requiredProspects = baseRetrySpeed * countOfActiveAgents;

		int delta = Math.round(requiredProspects - queuedCallCount);
		queuedProspectDTO.setDelta(delta); // set delta value to send it to dialer.
		if (delta <= 0) {
			// return empty prospect list with -ve delta to cancel calls.
			return callableProspects;
		}
		float retrySpeedFactor = campaign.getRetrySpeedFactor();
		int dailyRetryCount = prospectCacheServiceImpl.getCachedDailyCallRetryCount(campaign.getId());
		if (dailyRetryCount == 0) {
			dailyRetryCount = 1; // set default value if slicing is not enabled
		}
		/*
		 * if dailyRetryCount is 1 then don't consider retrySpeedFactor.
		 *
		 * if dailyRetryCount is 2 then use retrySpeedFactor as it is.
		 *
		 * if dailyRetryCount is greater than 2 then double the retrySpeedFactor.
		 * dailyRetryCount = 3 then retrySpeedFactor = retrySpeedFactor * 2
		 * dailyRetryCount = 4 then retrySpeedFactor = retrySpeedFactor * 3
		 */
		if (dailyRetryCount > 2) {
			for (int i = 2; i < dailyRetryCount; i++) {
				retrySpeedFactor = retrySpeedFactor + retrySpeedFactor;
			}
		}
		for (int i = 0; i <= callMaxRetries; i++) {
			String str = "R" + i;
			float ratio = retrySpeed.get("R0") / retrySpeed.get(str);
			// don't consider retrySpeedFactor when dailyRetryCount is 1
			if (dailyRetryCount > 1 && campaign.getRetrySpeedFactor() > 0) {
				ratio = ratio / retrySpeedFactor;
			}
			ratios.add(ratio);
		}

//		callableProspects = prospectCacheServiceImpl.removeProspectCallLogList(campaign.getId(), requiredProspects,
//				ratios);
		callableProspects = priorityQueueCacheOperationService.removeProspectsByRatio(campaign.getId(), requiredProspects, ratios);
		return callableProspects;
	}
	
	private List<ProspectCallDTO> getCallableProspectsByRetrySpeedOld(Campaign campaign, int countOfActiveAgents,
			int queuedCallCount, QueuedProspectDTO queuedProspectDTO, int currentRetry) {
		List<ProspectCallDTO> callableProspects = new ArrayList<>();
		Map<String, Float> retrySpeed = campaign.getRetrySpeed();
		List<Float> ratios = new ArrayList<>();
		Float defaultCallSpeed = (float) campaign.getTeam().getCallSpeedPerMinPerAgent().getValue();
		if (retrySpeed == null) {
			retrySpeed = new HashMap<>();
		}
		int callMaxRetries = campaign.getCallMaxRetries();
		for (int i = 0; i <= callMaxRetries; i++) {
			String str = "R" + i;
			if (retrySpeed.get(str) == null || retrySpeed.get(str) == 0) {
				retrySpeed.put(str, defaultCallSpeed);
			}
		}
		Float baseRetrySpeed = retrySpeed.get("R0") + currentRetry;
		Float requiredProspects = Float.parseFloat("1");
		requiredProspects = baseRetrySpeed * countOfActiveAgents;

		int delta = Math.round(requiredProspects - queuedCallCount);
		queuedProspectDTO.setDelta(delta); // set delta value to send it to dialer.
		if (delta <= 0) {
// return empty prospect list with -ve delta to cancel calls.
			return callableProspects;
		}
		float retrySpeedFactor = campaign.getRetrySpeedFactor();
		int dailyRetryCount = prospectCacheServiceImpl.getCachedDailyCallRetryCount(campaign.getId());
		if (dailyRetryCount == 0) {
			dailyRetryCount = 1; // set default value if slicing is not enabled
		}
		/*
		 * if dailyRetryCount is 1 then don't consider retrySpeedFactor.
		 *
		 * if dailyRetryCount is 2 then use retrySpeedFactor as it is.
		 *
		 * if dailyRetryCount is greater than 2 then double the retrySpeedFactor.
		 * dailyRetryCount = 3 then retrySpeedFactor = retrySpeedFactor * 2
		 * dailyRetryCount = 4 then retrySpeedFactor = retrySpeedFactor * 3
		 */
		if (dailyRetryCount > 2) {
			for (int i = 2; i < dailyRetryCount; i++) {
				retrySpeedFactor = retrySpeedFactor + retrySpeedFactor;
			}
		}
		for (int i = 0; i <= callMaxRetries; i++) {
			String str = "R" + i;
			float ratio = retrySpeed.get("R0") / retrySpeed.get(str);
// don't consider retrySpeedFactor when dailyRetryCount is 1
			if (dailyRetryCount > 1 && campaign.getRetrySpeedFactor() > 0) {
				ratio = ratio / retrySpeedFactor;
			}
			ratios.add(ratio);
		}

		callableProspects = prospectCacheServiceImpl.removeProspectCallLogListOld(campaign.getId(), requiredProspects,
				ratios);
		return callableProspects;
	}

	private Sort buildQueuedProspectSortOrder(Campaign campaign) {
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
		orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
		orders.add(new Order(Direction.DESC, "updatedDate"));
		return Sort.by(orders);
	}

	private Sort buildProspectSortOrder(Campaign campaign) {
		// build the sort order
		/*
		 * Ex: new Sort( new Order(Direction.ASC, "lastName"), new Order(Direction.DESC,
		 * "salary") )
		 */
		List<Order> orders = new ArrayList<Order>();
		if (LeadSortOrder.FIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.ASC, "createdDate"));
		} else if (LeadSortOrder.LIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.DESC, "createdDate"));
		} else if (LeadSortOrder.QUALITY_FIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			// get the sortable fields from CampaignTeam object
			orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.ASC, "updatedDate"));
		} else if (LeadSortOrder.QUALITY_LIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			// get the sortable fields from CampaignTeam object
			orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.DESC, "updatedDate"));
		} else if (LeadSortOrder.RETRY_FIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			// get the sortable fields from CampaignTeam object
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
			orders.add(new Order(Direction.ASC, "updatedDate"));
		} else if (LeadSortOrder.RETRY_LIFO.equals(campaign.getTeam().getLeadSortOrder())) {
			// get the sortable fields from CampaignTeam object
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			orders.add(new Order(Direction.ASC, "prospectCall.qualityBucketSort"));
			orders.add(new Order(Direction.DESC, "updatedDate"));
		} else if (LeadSortOrder.CUSTOM.equals(campaign.getTeam().getLeadSortOrder())) {
			// get the sortable fields from CampaignTeam object
			orders.add(new Order(Direction.ASC, "prospectCall.callRetryCount"));
			List<ProspectCustomSortField> prospectCustomSortFieldList = campaign.getTeam()
					.getProspectCustomSortFieldList();
			String sortFieldPath = "prospectCall.prospectSortIndex.";
			for (ProspectCustomSortField sortField : prospectCustomSortFieldList) {
				orders.add(new Order(Direction.ASC, sortFieldPath + sortField.getFieldName()));
			}
		}
		return Sort.by(orders);
	}

	private Comparator<ProspectCallCacheDTO> buildProspectSortOrderPriorityQueue(LeadSortOrder leadSortOrder) {
		Comparator<ProspectCallCacheDTO> result = null;
		LeadSortOrder campaignLeadSortOrder = leadSortOrder;
		switch (campaignLeadSortOrder) {
		case FIFO:
			Comparator<ProspectCallCacheDTO> fifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
					.thenComparing(ProspectCallCacheDTO::getCreatedDate);
			result = fifoOrder;
			break;
		case LIFO:
			Comparator<ProspectCallCacheDTO> lifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
					.thenComparing(ProspectCallCacheDTO::getCreatedDate, Comparator.reverseOrder());
			result = lifoOrder;
			break;
		case QUALITY_FIFO:
			Comparator<ProspectCallCacheDTO> qualityFifoOrder = Comparator.comparing(ProspectCallCacheDTO::getQualityBucketSort)
					.thenComparing(ProspectCallCacheDTO::getCallRetryCount).thenComparing(ProspectCallCacheDTO::getUpdatedDate);
			result = qualityFifoOrder;
			break;
		case QUALITY_LIFO:
			Comparator<ProspectCallCacheDTO> qualityLifoOrder = Comparator.comparing(ProspectCallCacheDTO::getQualityBucketSort)
					.thenComparing(ProspectCallCacheDTO::getCallRetryCount)
					.thenComparing(ProspectCallCacheDTO::getUpdatedDate, Comparator.reverseOrder());
			result = qualityLifoOrder;
			break;
		case RETRY_FIFO:
			Comparator<ProspectCallCacheDTO> retryFifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
					.thenComparing(ProspectCallCacheDTO::getQualityBucketSort)
					.thenComparing(ProspectCallCacheDTO::getUpdatedDate);
			result = retryFifoOrder;
			break;
		case RETRY_LIFO:
			Comparator<ProspectCallCacheDTO> retryLifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
					.thenComparing(ProspectCallCacheDTO::getQualityBucketSort)
					.thenComparing(ProspectCallCacheDTO::getUpdatedDate, Comparator.reverseOrder());
			result = retryLifoOrder;
			break;
		}
		return result;
	}

	/**
	 * Returns Pacific TZ date in YYYYMMDD format
	 *
	 * @return
	 */
	private String getCurrentDateInPacificTZinYMD() {
		String strCurrDateInPacificTZ = null;
		try {
			strCurrDateInPacificTZ = XtaasDateUtils.convertToTimeZone(new Date(), XtaasConstants.PACIFIC_TZ,
					"yyyyMMdd");
		} catch (ParseException e) {
			logger.error("getCurrentDateInPacificTZinYMD() : Exception occurred while converting date into Pacific TZ",
					e);
		}
		return strCurrDateInPacificTZ;
	}

	@Override
	public ProspectCallLog getLatestByPhoneNumber(String phoneNumber) {
		String plainPhoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars
		String phoneNumberRegex = null;

		// TODO: Below regex will match from behind. It will provide inconsistent result
		// when we go international, as for search by phone number +1-7744003867 may
		// return result for number +91-7744003867 as last 11 digits are same
		if (plainPhoneNumber.length() == 10) {
			// phone regex ex: /774.*400.*3867$/
			phoneNumberRegex = String.valueOf(plainPhoneNumber).replaceFirst("(\\d{3})(\\d{3})(\\d{4})", "$1.*$2.*$3");
			// phoneNumberRegex =
			// String.valueOf(plainPhoneNumber).replaceFirst("(\\d{3})(\\d{3})(\\d{4})",
			// "$1-$2-$3"); // 774-400-3867
		} else if (plainPhoneNumber.length() > 10) {
			phoneNumberRegex = String.valueOf(plainPhoneNumber).replaceFirst("(\\d+)(\\d{3})(\\d{3})(\\d{4})",
					"$1.*$2.*$3.*$4"); // phone regex ex: /91.*774.*400.*3867$/
		} else {
			throw new IllegalArgumentException("Invalid PhoneNumber: " + phoneNumber);
		}

		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		List<ProspectCallLog> prospectCallLogList = prospectLogRepository.findByPhoneNumber(phoneNumberRegex + "$",
				pageable);

		if (prospectCallLogList != null && !prospectCallLogList.isEmpty()) {
			return prospectCallLogList.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void updateProspectInteractionSessionId(String prospectCallId, String prospectInteractionSessionId) {
		ProspectCallLog prospectCallLog = prospectLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLog == null)
			logger.error("updateProspectCall() : Could not find prospect with prospectCallId [{}] in prospectCallLog",
					prospectCallId);
		prospectCallLog.getProspectCall().setProspectInteractionSessionId(prospectInteractionSessionId);
		this.save(prospectCallLog, false);
	}

	@Override
	public void updateInteractionSessionIdAndOutboundNumber(String prospectCallId, String prospectInteractionSessionId,
															String outboundNumber) {
		ProspectCallLog prospectCallLog = prospectLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLog != null) {
			prospectCallLog.getProspectCall().setProspectInteractionSessionId(prospectInteractionSessionId);
			prospectCallLog.getProspectCall().setOutboundNumber(outboundNumber);
			this.save(prospectCallLog, false);
		} else {
			logger.error("updateProspectCall() : Could not find prospect with prospectCallId [{}] in prospectCallLog",
					prospectCallId);
		}
	}

	@Override
	public int getSuccessCount(String campaignId) {
		return prospectLogRepository.getSuccessCount(campaignId);
	}
	
	public List<String> getStateCodesWithBusinessClosed() {
		ArrayList<String> stateCodes = new ArrayList<String>();
		
		Set<StateCallConfig> stateCallConfigFromCache = prospectCacheServiceImpl.getStateCallConfig();
		
		if (stateCallConfigFromCache.isEmpty() || stateCallConfigFromCache == null || stateCallConfigFromCache.size() == 0) {
			List<StateCallConfig> stateCallConfigListFromDB = stateCallConfigRepository.findAll();
			prospectCacheServiceImpl.setStateCallConfig(stateCallConfigListFromDB);
			stateCallConfigFromCache = stateCallConfigListFromDB.stream().collect(Collectors.toSet());
		}
		
		for (StateCallConfig stateCallConfig : stateCallConfigFromCache) {
			try {
				Date currentDate = new Date();
				String strCurrDateInStateTZ = XtaasDateUtils.convertToTimeZone(currentDate,
						stateCallConfig.getTimezone(), XtaasDateUtils.DATE_FORMAT_DEFAULT);

				// don't call if it is holiday
				if (stateCallConfig.getHolidayDates().contains(strCurrDateInStateTZ)) {
					// logger.debug("getStateCodesWithBusinessClosed() : State [{}] cannot be called
					// on Holidays. Date in State TZ : " + strCurrDateInStateTZ,
					// stateCallConfig.getStateCode());
					stateCodes.add(stateCallConfig.getStateCode());
					continue;
				}

				// check call time restrictions, if current time does not falls within the legal
				// call time
				int currentHour = Calendar.getInstance(TimeZone.getTimeZone(stateCallConfig.getTimezone()))
						.get(Calendar.HOUR_OF_DAY);
				if (currentHour < stateCallConfig.getStartCallHour()
						|| currentHour >= stateCallConfig.getEndCallHour()) {
					// logger.debug("getStateCodesWithBusinessClosed() : State [{}] cannot be called
					// out of call hour restrictions. Date in State TZ : " + strCurrDateInStateTZ +
					// ", CurrentHour: " + currentHour + ", Allowed range: " +
					// stateCallConfig.getStartCallHour() + " - " +
					// stateCallConfig.getEndCallHour(), stateCallConfig.getStateCode());
					stateCodes.add(stateCallConfig.getStateCode());
					continue;
				}
				// check if current time falls within the day's off call hours, if within the
				// day's off call hours then exclude this state
				if (stateCallConfig.getStateCallOffHours() != null) {
					for (StateCallOffHours stateCallOffHours : stateCallConfig.getStateCallOffHours()) {
						if (currentHour >= stateCallOffHours.getStartOffCallHour()
								&& currentHour < stateCallOffHours.getEndOffCallHour()) {
							stateCodes.add(stateCallConfig.getStateCode());
							logger.debug(
									"getStateCodesWithBusinessClosed() : State [{}] cannot be called due to OFF CALL HOURS. CurrentHour: "
											+ currentHour + ", Allowed range: "
											+ stateCallOffHours.getStartOffCallHour() + " - "
											+ stateCallOffHours.getEndOffCallHour(),
									stateCallConfig.getStateCode());
							break;
						}
					}
				}
			} catch (ParseException e) {
				logger.error("Exception occurred during checking call restrictions.", e);
			}
		} // end for

		if (stateCodes.size() == stateCallConfigFromCache.size()) {
			logger.error("getStateCodesWithBusinessClosed() : ALL STATES ARE EXCLUDED DUE TO TIMEZONE RESTRICTION.");
		}

		return stateCodes;
	}
	
	public List<String> getOpenTimeZones() {
		List<String> openTimeZones = new ArrayList<String>();
		Set<StateCallConfig> stateCallConfigFromCache = prospectCacheServiceImpl.getStateCallConfig();
		if (stateCallConfigFromCache.isEmpty() || stateCallConfigFromCache == null || stateCallConfigFromCache.size() == 0) {
			List<StateCallConfig> stateCallConfigListFromDB = stateCallConfigRepository.findAll();
			prospectCacheServiceImpl.setStateCallConfig(stateCallConfigListFromDB);
			stateCallConfigFromCache = stateCallConfigListFromDB.stream().collect(Collectors.toSet());
		}
		if (stateCallConfigFromCache != null && stateCallConfigFromCache.size() > 0) {
			for (StateCallConfig stateCallConfig : stateCallConfigFromCache) {
				try {
					String strCurrDateInStateTZ = XtaasDateUtils.convertToTimeZone(new Date(),
							stateCallConfig.getTimezone(), XtaasDateUtils.DATE_FORMAT_DEFAULT);

					if (stateCallConfig.getHolidayDates().contains(strCurrDateInStateTZ)) {
						continue;
					}

					int currentHour = Calendar.getInstance(TimeZone.getTimeZone(stateCallConfig.getTimezone()))
							.get(Calendar.HOUR_OF_DAY);
					if (currentHour >= stateCallConfig.getStartCallHour()
							&& currentHour <= stateCallConfig.getEndCallHour()) {
						openTimeZones.add(stateCallConfig.getTimezone());
						continue;
					}
					if (stateCallConfig.getStateCallOffHours() != null) {
						for (StateCallOffHours stateCallOffHours : stateCallConfig.getStateCallOffHours()) {
							if (currentHour < stateCallOffHours.getStartOffCallHour()
									|| currentHour > stateCallOffHours.getEndOffCallHour()) {
								openTimeZones.add(stateCallConfig.getTimezone());
								logger.debug(
										"getOpenTimeZones() : State [{}] cannot be called due to OFF CALL HOURS. CurrentHour: "
												+ currentHour + ", Allowed range: "
												+ stateCallOffHours.getStartOffCallHour() + " - "
												+ stateCallOffHours.getEndOffCallHour(),
										stateCallConfig.getTimezone());
								break;
							}
						}
					}
				} catch (ParseException e) {
					logger.error("Exception occurred during checking call restrictions.", e);
					e.printStackTrace();
				}
			}
		}
		return openTimeZones.stream().distinct().collect(Collectors.toList());
	}

	private Map<String, ProspectCallDTO> getMultiProspectsQueue(List<ProspectCallLog> queuedProspects,
																List<ProspectCallLog> callBackProspects, int dailyCallMaxRetriesLimit, String strCurrentDateYMD) {
		// Long maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD +
		// String.format("%02d", 01));
		List<ProspectCallDTO> prospectCallDTOList = new ArrayList<ProspectCallDTO>(0);
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		Map<String, List<ProspectCallLog>> prospectCallLogMap = new HashMap<String, List<ProspectCallLog>>();
		prospectCallLogList.addAll(queuedProspects);
		prospectCallLogList.addAll(callBackProspects);
		Map<String, ProspectCallDTO> outMap = new HashMap<String, ProspectCallDTO>();
		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			String key = prospectCallLog.getProspectCall().getProspect().getPhone();
			List<ProspectCallLog> pcList = prospectCallLogMap.get(key);
			if (pcList == null || pcList.size() == 0) {
				pcList = new ArrayList<ProspectCallLog>();
				pcList.add(prospectCallLog);
			} else {
				pcList.add(prospectCallLog);
			}
			prospectCallLogMap.put(key, pcList);
		}

		for (Map.Entry<String, List<ProspectCallLog>> prospectMap : prospectCallLogMap.entrySet()) {
			List<ProspectCallLog> pcList = prospectMap.getValue();
			ProspectCallLog prospectCallLog = pcList.get(0);

			ProspectCall prospectCall = prospectCallLog.getProspectCall();
			prospectCall.setCreatedDate(prospectCallLog.getCreatedDate());
			// stamping prospectCallLog createdDate onto ProspectCall
			if (prospectCall.getProspect().getPhone() != null && prospectCall.getProspect().getCompany() != null) {
				logger.debug("<--------- multiprostect - prospectCallId [{}] --------->",
						prospectCall.getProspectCallId());
				List<ProspectCallLog> multiProspectsList = pcList;
				// ProspectCallDTO prospectCallDTO = new ProspectCallDTO(prospectCall,
				// multiProspectsList);
				List<IndirectProspectsDTO> indirectList = new ArrayList<IndirectProspectsDTO>();
				int maxIndirectProspects = 0;
				for (ProspectCallLog pcLog : multiProspectsList) {
					if (!pcLog.getId().equalsIgnoreCase(prospectCallLog.getId())) {
						if (maxIndirectProspects >= 10) {
							break;
						}
						// increase counter to take max 4 indirect prospects (due to pusher limit).
						maxIndirectProspects++;
						// below puts the primary phone for which this number was in multiprospect list
						pcLog.getProspectCall().setMultiProspect(prospectCallLog.getProspectCall().getProspectCallId());

						IndirectProspectsDTO indirectProspectDTO = new IndirectProspectsDTO(
								pcLog.getProspectCall().getProspect().getFirstName(),
								pcLog.getProspectCall().getProspect().getLastName(),
								pcLog.getProspectCall().getProspect().getTitle(),
								pcLog.getProspectCall().getProspect().getDepartment(),
								pcLog.getProspectCall().getProspect().getCompany(),
								pcLog.getProspectCall().getProspect().getEmail(),
								pcLog.getProspectCall().getCampaignId(), pcLog.getProspectCall().getProspectCallId(),
								pcLog.getProspectCall().getQualityBucket(),
								pcLog.getProspectCall().getQualityBucketSort(),
								pcLog.getProspectCall().getMultiProspect(), "INDIRECT_CALLING", pcLog.getProspectCall().getProspect().getCustomFields());
						pcLog.setStatus(ProspectCallStatus.INDIRECT_CALLING);
						// prospectLogRepository.save(pcLog);
						indirectList.add(indirectProspectDTO);
					} else {
						// below is the primary call for whih multiprospect was initiated.
						pcLog.getProspectCall().setMultiProspect("PRIMARY");
						// prospectCall.setMultiProspect("PRIMARY");
						// prospectLogRepository.save(pcLog);
					}
				}
				if (indirectList != null && indirectList.size() > 0)
					prospectCall.setIndirectProspects(indirectList);
				logger.debug("##### getQueuedProspects() - Found [{}] indirect prospects for [{}] phone.",
						indirectList.size(), prospectCall.getProspect().getPhone());

				List<String> indirectProspectCallIds = new ArrayList<>();
				for (IndirectProspectsDTO indirectProspect : indirectList) {
					indirectProspectCallIds.add(indirectProspect.getProspectCallId());
				}
				prospectCacheServiceImpl.addInCachedIndirectProspectCallIdsMap(prospectCall.getProspectCallId(),
						indirectProspectCallIds);
				ProspectCallDTO prospectCallDTO = new ProspectCallDTO(prospectCallLog);
				prospectCallDTOList.add(prospectCallDTO);
				outMap.put(prospectCall.getProspectCallId(), prospectCallDTO);
			} else {
				ProspectCallDTO prospectCallDTO = new ProspectCallDTO(prospectCallLog);
				prospectCallDTOList.add(prospectCallDTO);
				outMap.put(prospectCall.getProspectCallId(), prospectCallDTO);
			}
		}
		return outMap;
	}

	@Override
	public ProspectCallLog findByProspectCallId(String prospectCallId) {
		return prospectLogRepository.findByProspectCallId(prospectCallId);
	}
	
	private boolean isWrappedUp(ProspectCallDTO prospectCall) {
		ProspectCallLog pc = prospectCallLogRepository.findByProspectCallId(prospectCall.getProspectCallId());
		if(pc.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE) && 
				pc.getProspectCall().getDispositionStatus()!=null && 
				pc.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
			return true;
		}
		return false;
	}

	private boolean isWrappedUp(ProspectCallCacheDTO prospectCall) {
		ProspectCallLog pc = prospectCallLogRepository.findByProspectCallId(prospectCall.getProspectCallId());
		if(pc.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE) &&
				pc.getProspectCall().getDispositionStatus()!=null &&
				pc.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
			return true;
		}
		return false;
	}

	
	@SuppressWarnings("unused")
	private boolean checkLocalDNCList(ProspectCallDTO prospectCall, ProspectCallCacheDTO prospectCallCacheDTO,
			String partnerId) {
		boolean isUSorCanada = false;
		String domain = "";
		if (prospectCall != null) {
			logger.debug("checkLocalDNCList() : Check if the number is not in local DNC List. Phonenumber: [{}]",
					prospectCall.getProspect().getPhone());
			if (prospectCall.getProspect().getCountry() != null
					&& (prospectCall.getProspect().getCountry().equalsIgnoreCase("USA")
							|| prospectCall.getProspect().getCountry().equalsIgnoreCase("UNITED STATES")
							|| prospectCall.getProspect().getCountry().equalsIgnoreCase("UNITEDSTATES")
							|| prospectCall.getProspect().getCountry().equalsIgnoreCase("US")
							|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CANADA")
							|| prospectCall.getProspect().getCountry().equalsIgnoreCase("CA"))) {
				isUSorCanada = true;
			}
			if (prospectCall.getProspect().getCustomAttributes() != null
					&& prospectCall.getProspect().getCustomAttributes().get("domain") != null) {
				domain = (String) prospectCall.getProspect().getCustomAttributes().get("domain");
			}
			return checkLocalDNCList(prospectCall.getProspect().getFirstName(),
					prospectCall.getProspect().getLastName(), prospectCall.getProspect().getPhone(), domain,
					isUSorCanada, partnerId, prospectCall, null);
		} else {
			logger.debug("checkLocalDNCList() : Check if the number is not in local DNC List. Phonenumber: [{}]",
					prospectCallCacheDTO.getProspect().getPhone());
			if (prospectCallCacheDTO.getProspect().getCountry() != null
					&& (prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("USA")
							|| prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("UNITED STATES")
							|| prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("UNITEDSTATES")
							|| prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("US")
							|| prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("CANADA")
							|| prospectCallCacheDTO.getProspect().getCountry().equalsIgnoreCase("CA"))) {
				isUSorCanada = true;
			}
			if (prospectCallCacheDTO.getProspect().getCustomAttributes() != null
					&& prospectCallCacheDTO.getProspect().getCustomAttributes().get("domain") != null) {
				domain = (String) prospectCallCacheDTO.getProspect().getCustomAttributes().get("domain");
			}
			return checkLocalDNCList(prospectCallCacheDTO.getProspect().getFirstName(),
					prospectCallCacheDTO.getProspect().getLastName(), prospectCallCacheDTO.getProspect().getPhone(), domain,
					isUSorCanada, partnerId, null, prospectCallCacheDTO);
		}
	}

	private void updateIndirectProspects(ProspectCallLog prospectCallLog) {
		String prospectCallId = prospectCallLog.getProspectCall().getProspectCallId();
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		ProspectCallStatus status = prospectCallLog.getStatus();
		if (status.equals(ProspectCallStatus.CALL_FAILED) || status.equals(ProspectCallStatus.CALL_CANCELED)
				|| status.equals(ProspectCallStatus.CALL_BUSY) || status.equals(ProspectCallStatus.CALL_NO_ANSWER)
				|| status.equals(ProspectCallStatus.CALL_MACHINE_ANSWERED)) {
			List<String> indirectProspectCallIds = prospectCacheServiceImpl.getCachedIndirectProspectCallIds()
					.get(prospectCallId);
			if (indirectProspectCallIds != null && !indirectProspectCallIds.isEmpty()) {
				for (String indirectProspectCallId : indirectProspectCallIds) {
					ProspectCallLog pCallLog = prospectLogRepository.findByProspectCallId(indirectProspectCallId);
					pCallLog.setStatus(status);
					pCallLog.setCallbackDate(prospectCallLog.getCallbackDate());
					pCallLog.getProspectCall().setCallRetryCount(prospectCall.getCallRetryCount());
					pCallLog.getProspectCall().setDailyCallRetryCount(prospectCall.getDailyCallRetryCount());
					pCallLog.getProspectCall().setSubStatus(prospectCall.getSubStatus());
					pCallLog.getProspectCall().setDispositionStatus(prospectCall.getDispositionStatus());

					// update Qauality Bucket
					StringBuffer sb = new StringBuffer();
					/*
					 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
					 */
					sb.append(prospectCallLog.getStatus());
					sb.append("|");
					sb.append(prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount"));
					sb.append("-");
					sb.append(prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount"));
					sb.append("|");
					sb.append(prospectCall.getProspect().getManagementLevel());
					prospectCall.setQualityBucket(sb.toString());

					save(pCallLog, true);
				}
				prospectCacheServiceImpl.getCachedIndirectProspectCallIds().remove(prospectCallId);
			}
		}
	}

	public void updateRecordingUrlByCallSid(String callSid) {
		List<ProspectCallLog> prospectCallLogs = prospectLogRepository.findAllByTwilioCallId(callSid);
		CallLog callLog = callLogRepository.findOneById(callSid);
		if (callLog != null && prospectCallLogs != null) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				if (prospectCallLog.getProspectCall().getRecordingUrl() == null
						|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()) {
					String recordingUrl = callLog.getCallLogMap().get("RecordingUrl");
					prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
					int lastIndex = recordingUrl.lastIndexOf("/");
					String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
					String rfileName = fileName + ".wav";
					String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					downloadRecordingsToAws.downloadRecording(recordingUrl,
							prospectCallLog.getProspectCall().getProspectCallId(),
							prospectCallLog.getProspectCall().getCampaignId(), false);
					prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
					String telcoDuration = (callLog.getCallLogMap().get("DialCallDuration") == null)
							? callLog.getCallLogMap().get("RecordingDuration")
							: callLog.getCallLogMap().get("DialCallDuration");
					if (telcoDuration != null && !telcoDuration.isEmpty()) {
						prospectCallLog.getProspectCall().setTelcoDuration(Integer.valueOf(telcoDuration));
					} else {
						prospectCallLog.getProspectCall().setTelcoDuration(null);
					}
					this.save(prospectCallLog, false);
				}
			}
		}
		markPrimaryProspectAsWrapupComplete(callSid);
	}

	private void markPrimaryProspectAsWrapupComplete(String callSid) {
		boolean primaryProspectSubmitted = false;
		boolean secondaryProspectFound = false;
		String primaryProspectCallId = null;
		ProspectCallLog primaryProspectCallLog = null;
		List<ProspectCallLog> prospectCallLogs = prospectLogRepository.findAllByTwilioCallId(callSid);
		for (ProspectCallLog prospectCallLog : prospectCallLogs) {
			if (prospectCallLog.getProspectCall().getMultiProspect() != null
					&& prospectCallLog.getProspectCall().getMultiProspect().equalsIgnoreCase("PRIMARY")) {
				/*
				 * this will be true only when primary prospect is submitted as
				 * dnc/failure/ans_mach from multiprospect popup
				 */
				primaryProspectSubmitted = true;
				primaryProspectCallLog = prospectCallLog;
			} else if (prospectCallLog.getProspectCall().getMultiProspect() != null
					&& !prospectCallLog.getProspectCall().getMultiProspect().equalsIgnoreCase("PRIMARY")) {
				secondaryProspectFound = true;
				primaryProspectCallId = prospectCallLog.getProspectCall().getMultiProspect();
			}
		}
		if (primaryProspectSubmitted) {
			savePrimaryProspect(primaryProspectCallLog, true);
		} else if (!primaryProspectSubmitted && secondaryProspectFound) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				if (prospectCallLog.getProspectCall().getProspectCallId().equalsIgnoreCase(primaryProspectCallId)) {
					savePrimaryProspect(prospectCallLog, true);
				}
			}
		}
	}

	private void savePrimaryProspect(ProspectCallLog prospectCallLog, boolean createProspectCallInteraction) {
		prospectCallLog.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
		StringBuffer sb = new StringBuffer();
		sb.append(prospectCallLog.getStatus());
		sb.append("|");
		sb.append(prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount"));
		sb.append("-");
		sb.append(prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount"));
		sb.append("|");
		sb.append(prospectCallLog.getProspectCall().getProspect().getManagementLevel());
		prospectCallLog.getProspectCall().setQualityBucket(sb.toString());
		this.save(prospectCallLog, createProspectCallInteraction);
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d= new Date();
		//System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid+Long.toString(d.getTime());
		return pcidv;
	}

	@Override
	public ProspectCallDTO createProspect(ProspectCallDTO prospectCallDTO) {
		ProspectCallLog prospectCallLog = null;
		String randomUUID = getUniqueProspect();
		String randomeUUIDWithoutHyphon = StringUtils.replace(randomUUID.toString(), "-", "");
		Campaign campaign = campaignService.getCampaign(prospectCallDTO.getCampaignId());
		if (!campaign.getStatus().equals(CampaignStatus.RUNNING)) {
			throw new IllegalArgumentException("Campaign is not in running state. Please check with your supervisor.");
		}
		Team team = teamService.getTeam(campaign.getTeam().getTeamId());
		prospectCallDTO.getProspect().setEncrypted(campaign.isEncrypted());
		prospectCallDTO.getProspect().setDirectPhone(false);
		if (prospectCallDTO.getProspect().getSource() == null ||
				(!prospectCallDTO.getProspect().getSource().equals("INSIDEVIEW") && !prospectCallDTO.getProspect().getSource().equals("LeadIQ") && !prospectCallDTO.getProspect().getSource().equals("ZOOINFO"))) {
			prospectCallDTO.getProspect().setSource("RESEARCH");
		}
		if (campaign.getOrganizationId().equalsIgnoreCase("XTAAS CALL CENTER")) {
			prospectCallDTO.getProspect().setSourceType("INTERNAL");
		} else {
			prospectCallDTO.getProspect().setSourceType("CLIENT");
		}
		prospectCallDTO.getProspect().setSourceType(prospectCallDTO.getPartnerId());
		if(StringUtils.isNotBlank(prospectCallDTO.getProspect().getSourceId())){
			prospectCallDTO.getProspect().setSourceId(prospectCallDTO.getProspect().getSourceId());
		}else{
			prospectCallDTO.getProspect().setSourceId(randomeUUIDWithoutHyphon);
		}
		prospectCallDTO.getProspect().setSourceCompanyId(randomeUUIDWithoutHyphon);
		// fetch list of companies for success prospects
		List<DomainCompanyCountPair> dataFromDB = getSuccessDomainsAndCompaniesCount(campaign);
		String domain = "";
		if (prospectCallDTO.getProspect().getCustomAttributes().get("domain") != null) {
			domain = prospectCallDTO.getProspect().getCustomAttributes().get("domain").toString();
			if((domain.contains("http://") || domain.contains("HTTP://"))){
				domain= domain.substring(7,domain.length());
			}

			if((domain.contains("https://") || domain.contains("HTTPS://"))){
				domain= domain.substring(8,domain.length());
			}

			if((domain.contains("www.") || domain.contains("WWW."))){
				domain= domain.substring(4,domain.length());
			}
			prospectCallDTO.getProspect().setCompany(domain);
		}
		String domainEmail = prospectCallDTO.getProspect().getEmail();

		if(domainEmail  !=null && !domainEmail.isEmpty() ) {
			domainEmail = domainEmail.substring(domainEmail.indexOf("@")+1, domainEmail.length());
		}
		if (isSuccessDuplicate(campaign, domain,domainEmail, prospectCallDTO.getProspect().getCompany(), dataFromDB)) {
			throw new IllegalArgumentException("Company success limit reached for the campaign.");
		}
		//SuppressionList suppressionList = getSuppressionList(campaign);
		if (prospectCallDTO.getProspect().getCustomAttributes().get("domain") != null) {
			String ddomain = prospectCallDTO.getProspect().getCustomAttributes().get("domain").toString();
			//logger.debug("ddomain1"+ddomain);
			if (ddomain != null && !ddomain.isEmpty()) {
				ddomain = XtaasUtils.getSuppressedHostName(ddomain);
				//logger.debug("ddomain2"+ddomain);
				//	logger.debug("campaign.getId()"+campaign.getId());

				long supListCount = suppressionListNewRepository.countSupressionListByCampaignDomain(campaign.getId(), ddomain.toLowerCase());
				if(supListCount > 0) {
					throw new IllegalArgumentException("Company is in suppression list.");
				}
			}

			/*boolean isCompanySuppressed = XtaasUtils.isCompanySuppressed(prospectCallDTO.getProspect().getCompany(),
					prospectCallDTO.getProspect().getCustomAttributes().get("domain").toString(),
					suppressionList.getDomains(), suppressionList.getCompanies());
			if (isCompanySuppressed) {
				throw new IllegalArgumentException("Company is in suppression list.");
			}*/
		}else if(prospectCallDTO.getProspect().getEmail()!=null && !prospectCallDTO.getProspect().getEmail().isEmpty()) {
			String email = prospectCallDTO.getProspect().getEmail();
			if(email  !=null && !email.isEmpty() ) {
				email = email.substring(email.indexOf("@")+1, email.length());
			}
			String ddomain = XtaasUtils.getSuppressedHostName(email);
			long supListCount = suppressionListNewRepository.countSupressionListByCampaignDomain(campaign.getId(), ddomain.toLowerCase());
			if(supListCount > 0) {
				throw new IllegalArgumentException("Company is in suppression list.");
			}
		}

		if (prospectCallDTO.getProspect() != null && !StringUtils.isEmpty(prospectCallDTO.getProspect().getEmail())) {
			String email = prospectCallDTO.getProspect().getEmail();
			if (suppressionListService.isEmailSuppressed(email, campaign.getId(), campaign.getOrganizationId(),
					campaign.isMdFiveSuppressionCheck())) {
				throw new IllegalArgumentException("Email is in suppression list.");
			}

			if (campaign.isABM() && prospectCallDTO.getProspect() != null
					&& prospectCallDTO.getProspect().getCustomAttributes() != null
					&& prospectCallDTO.getProspect().getCustomAttributeValue("domain") != null
					&& !prospectCallDTO.getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
				String ddomain = prospectCallDTO.getProspect().getCustomAttributeValue("domain").toString();
				if (ddomain != null && !ddomain.isEmpty()) {
					ddomain = XtaasUtils.getSuppressedHostName(ddomain);
					long supListCount = suppressionListNewRepository
							.countSupressionListByCampaignDomain(campaign.getId(), ddomain.toLowerCase());
					if (supListCount > 0) {
						throw new IllegalArgumentException("Prospect is not Matching the Target List.");
					}
				}
			}
		}

		setStateCodeAndTimezone(prospectCallDTO);
		Country country = countryRepository.findByCountry(prospectCallDTO.getProspect().getCountry());
		if (country != null && country.getContinent() != null && country.getContinent().equalsIgnoreCase("Europe")) {
			prospectCallDTO.getProspect().setEu(true);
		} else {
			prospectCallDTO.getProspect().setEu(false);
		}
		if (prospectCallDTO.getProspectCallId() == null) {
			prospectCallLog = new ProspectCallLog();
			ProspectCall prospectCallFromAgent = prospectCallDTO.toNewProspectCall(prospectCallDTO.getProspect());
			prospectCallFromAgent.setProspectCallId(randomUUID);
			if (prospectCallFromAgent.getProspectInteractionSessionId() == null) {
				prospectCallFromAgent.setProspectInteractionSessionId("IS" + randomeUUIDWithoutHyphon);
			}
			prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
			prospectCallLog.setProspectCall(prospectCallFromAgent);
		} else {
			ProspectCall prospectCallFromAgent = prospectCallDTO.toProspectCall();
			prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallDTO.getProspectCallId());
			prospectCallLog.setProspectCall(prospectCallFromAgent);
		}
		setOutboundNumber(prospectCallLog, campaign); // set out bound number
		prospectCallLog.getProspectCall().setTwilioCallSid(randomUUID);
		incrementDailyCallRetryCount(prospectCallLog.getProspectCall()); // increment daily call retry count
		XtaasUtils.getQualityBucket(prospectCallLog); // set Quality Bucket
		prospectCallLog.getProspectCall().setCallRetryCount(55555);
		prospectCallLog.setResearchData(true);
		// agentRegistrationService.setProspectByCallSid(prospectCallLog.getProspectCall().getTwilioCallSid(),
		// prospectCallLog.getProspectCall());
		ProspectCallLog prospectCallLogFromDB = prospectCallLog;
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		boolean localDNCListCheckPassed = checkLocalDNCList(new ProspectCallDTO(prospectCall), null, team.getPartnerId());
		if (!localDNCListCheckPassed) {
			throw new IllegalArgumentException("Can not call this prospect. Prospect is in DNC list.");
		}
		isProspectUniqueInCampaign(prospectCallDTO);
		ProspectCallLog uniqueProspectFromDB = null;
		ProspectCallDTO newProspectCallDTO = null;
		int uniqueRecord = prospectCallLogRepository.findManualProspectRecordCount(prospectCallDTO.getCampaignId(),
				prospectCallDTO.getProspect().getFirstName(), prospectCallDTO.getProspect().getLastName(),
				prospectCallDTO.getProspect().getPhone());
		if (uniqueRecord == 0) {
			int uniqueprospect = prospectCallLogRepository.findUniqueProspectRecord(prospectCallDTO.getCampaignId(),
					prospectCallDTO.getProspect().getFirstName(), prospectCallDTO.getProspect().getLastName(),
					prospectCallDTO.getProspect().getTitle(),prospectCallDTO.getProspect().getCompany());
			if (uniqueprospect>1) {
				List<ProspectCallLog> pcList = prospectCallLogRepository.findUinqueRecord(prospectCallDTO.getCampaignId(),
						prospectCallDTO.getProspect().getFirstName(), prospectCallDTO.getProspect().getLastName(),
						prospectCallDTO.getProspect().getTitle(),prospectCallDTO.getProspect().getCompany());
				ProspectCallLog pc = pcList.get(0);
				pc.getProspectCall().getProspect().setPhone(prospectCallLog.getProspectCall().getProspect().getPhone());
				prospectCallLogFromDB = save(pc, true);
				//throw new IllegalArgumentException("Prospect with Combination of Name, company and Title already exists in Campaign.");
			} else {
				prospectCallLogFromDB = save(prospectCallLog, true);
			}
//			GlobalContact globalContact = new GlobalContact();
//			globalContact = globalContact.toGlobalContact(prospectCallLog.getProspectCall());
//			globalContactService.saveGlobalContactAndInteraction(globalContact, true);
			if (prospectCallLogFromDB != null && prospectCallLogFromDB.getProspectCall() != null) {
				newProspectCallDTO = new ProspectCallDTO(prospectCallLogFromDB.getProspectCall());
			}
		} else {
			uniqueProspectFromDB = prospectCallLogRepository.findManualProspectRecord(prospectCallDTO.getCampaignId(),
					prospectCallDTO.getProspect().getFirstName(), prospectCallDTO.getProspect().getLastName(),
					prospectCallDTO.getProspect().getPhone());
			if (uniqueProspectFromDB != null && uniqueProspectFromDB.getProspectCall() != null) {
				isDispositionStatusInSuccess(uniqueProspectFromDB);
				if (uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.QUEUED)) {
					newProspectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
				} else if (uniqueProspectFromDB.getProspectCall().getAgentId() != null && uniqueProspectFromDB
						.getProspectCall().getAgentId().equalsIgnoreCase(prospectCallDTO.getAgentId())) {
					// If same agent enters the prospect details again.
					newProspectCallDTO = sameAgentEntersProspectDetails(uniqueProspectFromDB);
				} else {
					// If diffrent agent enters the prospect details again.
					newProspectCallDTO = diffrentAgentEntersProspectDetails(uniqueProspectFromDB);
				}
			}
		}
		if (newProspectCallDTO == null) {
			throw new IllegalArgumentException("Maximum prospect dial limit reached.");
		} else {
			prospectCacheServiceImpl.addInManualDialProspectCallIds(newProspectCallDTO.getProspectCallId());
		}

		if (campaign.isLeadIQ() && prospectCallDTO.getProspect().getSource()!=null &&
				prospectCallDTO.getProspect().getSource().equalsIgnoreCase("LeadIQ")) {
			List<Prospect> prospectLeadIQList = leadIQSearchService.getLeadIQData(prospectCallDTO.getProspect(),campaign);
			if (prospectLeadIQList != null && prospectLeadIQList.size() > 0) {
				for (Prospect prospectLeadIQ : prospectLeadIQList) {
					ProspectCallLogLeadIQ prospectCallLogLeadIQ = new ProspectCallLogLeadIQ();
					prospectCallLogLeadIQ.setProspectCall(prospectCallLog.getProspectCall());
					prospectCallLogLeadIQ.getProspectCall().setProspect(prospectLeadIQ);
					prospectCallLogLeadIQ.setRawLeadIqResponse(prospectLeadIQ.getLeadIQResult());
					prospectCallLogLeadIQ.getProspectCall().getProspect().setLeadIQResult(null);
					prospectCallLogLeadIQRepository.save(prospectCallLogLeadIQ);
				}
			}
		}
		return newProspectCallDTO;
	}

	private ProspectCallDTO sameAgentEntersProspectDetails(ProspectCallLog uniqueProspectFromDB) {
		ProspectCallDTO prospectCallDTO = null;
		if (isProspectStatusIsIn(uniqueProspectFromDB.getStatus())) {
			prospectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
		} else if (uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("DIALERCODE")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("AUTO")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("CALLBACK")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus()
				.equalsIgnoreCase("NOT_DISPOSED"))) {
			prospectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
		} else if ((uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.QUEUED)
				|| uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.CALL_DIALED))
				&& uniqueProspectFromDB.getProspectCall().getProspect().getSource().equalsIgnoreCase("RESEARCH")) {
			prospectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
		}
		return prospectCallDTO;
	}

	private ProspectCallLog sameAgentProspectDetails(ProspectCallLog uniqueProspectFromDB) {
		isDispositionStatusInSuccess(uniqueProspectFromDB);
		if (isProspectStatusIsIn(uniqueProspectFromDB.getStatus())) {
			return uniqueProspectFromDB;
		} else if (uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null && (uniqueProspectFromDB
				.getProspectCall().getDispositionStatus().equalsIgnoreCase("DIALERCODE")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("AUTO")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("CALLBACK")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("NOT_DISPOSED"))) {
			return uniqueProspectFromDB;
		}
		return uniqueProspectFromDB;
	}

	private ProspectCallDTO diffrentAgentEntersProspectDetails(ProspectCallLog uniqueProspectFromDB) {
		ProspectCallDTO prospectCallDTO = null;
		Campaign campaign = campaignService.getCampaign(uniqueProspectFromDB.getProspectCall().getCampaignId());
		long callBackTime = 0;
		if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_CALLBACK_MINUTES.name(), 0);
		} else {
			callBackTime = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALLBACK_MINUTES.name(), 0);
		}

		long duration = 0;
		if (uniqueProspectFromDB.getProspectCall().getCallStartTime() != null
				&& uniqueProspectFromDB.getProspectCall().getCallStartTime().getTime() >= 0) {
			duration = new Date().getTime() - uniqueProspectFromDB.getProspectCall().getCallStartTime().getTime();
		}
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (callBackTime > diffInMinutes) {
			throw new IllegalArgumentException(
					"Can't call the prospect as its been dialled in last " + callBackTime + " mins");
		} else {
			if (isProspectStatusIsIn(uniqueProspectFromDB.getStatus())) {
				prospectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
			} else if (uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
					&& uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
					&& (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("DIALERCODE")
					|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("AUTO")
					|| uniqueProspectFromDB.getProspectCall().getDispositionStatus()
					.equalsIgnoreCase("CALLBACK")
					|| uniqueProspectFromDB.getProspectCall().getDispositionStatus()
					.equalsIgnoreCase("NOT_DISPOSED"))) {
				prospectCallDTO = new ProspectCallDTO(uniqueProspectFromDB.getProspectCall());
			}
		}
		return prospectCallDTO;
	}

	private ProspectCallLog diffrentAgentProspectDetails(ProspectCallLog uniqueProspectFromDB) {
		isDispositionStatusInSuccess(uniqueProspectFromDB);

		Campaign campaign = campaignService.getCampaign(uniqueProspectFromDB.getProspectCall().getCampaignId());
		long callBackTime = 0;
		if (campaign.getType().equals(CampaignTypes.LeadVerification)) {
			callBackTime = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.VERIFICATION_CALLBACK_MINUTES.name(), 0);
		} else {
			callBackTime = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALLBACK_MINUTES.name(), 0);
		}

		long duration = 0;
		if (uniqueProspectFromDB.getProspectCall().getCallStartTime() != null
				&& uniqueProspectFromDB.getProspectCall().getCallStartTime().getTime() >= 0) {
			duration = new Date().getTime() - uniqueProspectFromDB.getProspectCall().getCallStartTime().getTime();
		}

//		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
//		if (callBackTime > diffInMinutes) {
//			throw new IllegalArgumentException(
//					"Canï¿½t call the prospect as its been dialled in last " + callBackTime + " mins");
//		} else {
		if (isProspectStatusIsIn(uniqueProspectFromDB.getStatus())) {
			return uniqueProspectFromDB;
		} else if (uniqueProspectFromDB.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("DIALERCODE")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("AUTO")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("CALLBACK")
				|| uniqueProspectFromDB.getProspectCall().getDispositionStatus()
				.equalsIgnoreCase("NOT_DISPOSED"))) {
			return uniqueProspectFromDB;
		}
//		}
		return uniqueProspectFromDB;
	}

	private boolean isProspectStatusIsIn(ProspectCallStatus status) {
		if (status != null) {
			if (status.equals(ProspectCallStatus.CALL_TIME_RESTRICTION)
					|| status.equals(ProspectCallStatus.CALL_NO_ANSWER) || status.equals(ProspectCallStatus.CALL_BUSY)
					|| status.equals(ProspectCallStatus.CALL_CANCELED) || status.equals(ProspectCallStatus.CALL_FAILED)
					|| status.equals(ProspectCallStatus.CALL_MACHINE_ANSWERED)
					|| status.equals(ProspectCallStatus.CALL_ABANDONED)
					|| status.equals(ProspectCallStatus.CALL_DIALED)) {
				return true;
			}
		}
		return false;
	}

	private void isDispositionStatusInSuccess(ProspectCallLog uniqueProspectFromDB) {
		if (uniqueProspectFromDB != null && uniqueProspectFromDB.getProspectCall() != null
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("FAILURE")) {
			if (uniqueProspectFromDB.getProspectCall().getSubStatus() != null
					&& uniqueProspectFromDB.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL")) {
				throw new IllegalArgumentException("Prospect is in DNC.");
			}
			throw new IllegalArgumentException(
					"Prospect is in FAILURE - " + uniqueProspectFromDB.getProspectCall().getSubStatus());
		} else if (uniqueProspectFromDB != null && uniqueProspectFromDB.getProspectCall() != null
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
			throw new IllegalArgumentException("Prospect is already lead in the campaign");
		}
	}

	public void setStateCodeAndTimezone(ProspectCallDTO prospectCallDTO) {
		StateCallConfig stateCallConfig = null;
		List<StateCallConfig> stateCallConfigList = new ArrayList<>();
		if (prospectCallDTO.getProspect().getStateCode() == null
				|| prospectCallDTO.getProspect().getStateCode().isEmpty()) {
			stateCallConfigList = stateCallConfigRepository
					.findByCountryName(prospectCallDTO.getProspect().getCountry());
		} else {
			stateCallConfig = stateCallConfigRepository.findByStateCode(prospectCallDTO.getProspect().getStateCode());
			if (stateCallConfig != null) {
				stateCallConfigList.add(stateCallConfig);
			}
		}
		if (stateCallConfigList != null && stateCallConfigList.size() > 0) {
			prospectCallDTO.getProspect().setStateCode(stateCallConfigList.get(0).getStateCode());
			prospectCallDTO.getProspect().setTimeZone(stateCallConfigList.get(0).getTimezone());
		} else {
			//commented if state not found for a country
			// prospectCallDTO.getProspect().setStateCode("CA");
			prospectCallDTO.getProspect().setStateCode("ZZ");
			prospectCallDTO.getProspect().setTimeZone("US/Pacific");
		}
	}

	public void setOutboundNumber(ProspectCallLog prospectCallLog, Campaign campaign) {
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		Prospect prospect = prospectCall.getProspect();
		String outboundNumber = "";
		if (campaign != null && campaign.getCallControlProvider() != null
				&& campaign.getCallControlProvider().equalsIgnoreCase("Plivo")) {
			outboundNumber = plivoOutboundNumberService.getOutboundNumber(prospect.getPhone(),
					prospectCall.getCallRetryCount(), prospect.getCountry(), campaign.getOrganizationId());
		} else if (campaign != null && campaign.getCallControlProvider() != null
				&& campaign.getCallControlProvider().equalsIgnoreCase("Telnyx")) {
			outboundNumber = telnyxOutboundNumberService.getOutboundNumber(prospect.getPhone(),
					prospectCallLog.getProspectCall().getCallRetryCount(), prospect.getCountry(),
					campaign.getOrganizationId());
		}
		prospectCallLog.getProspectCall().setOutboundNumber(outboundNumber);
	}

	private void incrementDailyCallRetryCount(ProspectCall prospectCall) {
		String strCurrentDateYMD = null;
		try {
			strCurrentDateYMD = XtaasDateUtils.convertToTimeZone(new Date(), "US/Pacific", "yyyyMMdd");
		} catch (ParseException e) {
			logger.error("Error occurred in converting date into string");
		}
		// dailyCallRetryCount has format : yyyyMMdd<2 digit counter>
		prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01"));
	}

	private String sanitizePhoneNumber(String phoneNumber) {
		phoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars
		return phoneNumber;
	}

	@Override
	public boolean checkLocalDNCList(String firstName, String lastName, String phone, String domain,
			boolean isUSorCanada, String partnerId, ProspectCallDTO prospectCall, ProspectCallCacheDTO prospectCallCacheDTO) {
		logger.debug("checkLocalDNCList() : Check if the number is not in local DNC List. Phonenumber: [{}]", phone);
		boolean companyDnc = false;
		boolean prospectDnc = false;
		// fetch DNC list by partnerId and domain
		List<DNCList> companyDncList = internalDNCListService.getDNCByPartnerIdAndDomain(partnerId, domain);
		if (companyDncList != null && companyDncList.size() > 0) {
			for (DNCList cdnc : companyDncList) {
				if (cdnc.getDncLevel() != null && !cdnc.getDncLevel().isEmpty()
						&& cdnc.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
					if (cdnc.getFirstName() != null && cdnc.getLastName() != null
							&& cdnc.getFirstName().equalsIgnoreCase(firstName)
							&& cdnc.getLastName().equalsIgnoreCase(lastName)) {
						prospectDnc = true;
					}
				}
				if (cdnc.getDncLevel() != null && !cdnc.getDncLevel().isEmpty()
						&& cdnc.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
					companyDnc = true;
				}
			}
			/*If company DNC found then companyDnc flag will be true which will remove all indirect prospects.*/
			if (prospectCall != null) {
				checkIndirectProspectInDNC(prospectCall,null, companyDncList, companyDnc);
			} else {
				checkIndirectProspectInDNC(null, prospectCallCacheDTO, companyDncList, companyDnc);
			}
		}
		// fetch DNC list by partnerId and phone
		List<DNCList> phoneDncList = internalDNCListService.getDNCByPartnerIdAndPhoneNumber(partnerId, phone);
		if (phoneDncList == null || phoneDncList.size() == 0) {
			if (isUSorCanada) {
				String ph = sanitizePhoneNumber(phone);
				if (ph.length() <= 10) {
					ph = "1" + ph;
					phoneDncList = internalDNCListService.getDNCByPartnerIdAndPhoneNumber(partnerId, ph);
				} else {
					ph = ph.substring(1, ph.length());
					phoneDncList = internalDNCListService.getDNCByPartnerIdAndPhoneNumber(partnerId, ph);
				}
			}
		}

		if (phoneDncList != null && phoneDncList.size() > 0) {
			for (DNCList dnc : phoneDncList) {
				if (dnc.getDncLevel() != null && !dnc.getDncLevel().isEmpty()
						&& dnc.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
					companyDnc = true;
					break;
				}
				if (dnc.getDncLevel() != null && !dnc.getDncLevel().isEmpty()
						&& dnc.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
					if (dnc.getFirstName() != null && dnc.getLastName() != null
							&& dnc.getFirstName().equalsIgnoreCase(firstName)
							&& dnc.getLastName().equalsIgnoreCase(lastName)) {
						prospectDnc = true;
						break;
					}
				}
				if (dnc.getDncLevel() == null || dnc.getDncLevel().isEmpty()) {
					if (dnc.getFirstName() != null && dnc.getLastName() != null
							&& dnc.getFirstName().equalsIgnoreCase(firstName)
							&& dnc.getLastName().equalsIgnoreCase(lastName)) {
						prospectDnc = true;
						break;
					}
				}
			}
			/*If company DNC found then companyDnc flag will be true which will remove all indirect prospects.*/
			if (prospectCall != null) {
				checkIndirectProspectInDNC(prospectCall,null, companyDncList, companyDnc);
			} else {
				checkIndirectProspectInDNC(null, prospectCallCacheDTO, companyDncList, companyDnc);
			}
		}
		if (companyDnc || prospectDnc) {
			logger.debug(
					"checkLocalDNCList() : Lead cannot be called as Phonenumber already exist in local DNC List. Phonenumber: [{}] ",
					phone);
			return false;
		}
		return true;
	}
	
	private void checkIndirectProspectInDNC(ProspectCallDTO prospectCall, ProspectCallCacheDTO prospectCallCacheDTO, List<DNCList> dncList, boolean isCompanyDNC) {
		if (prospectCall != null && prospectCall.getIndirectProspects() != null
				&& prospectCall.getIndirectProspects().size() > 0) {
			Iterator<IndirectProspectsDTO> iterator = prospectCall.getIndirectProspects().iterator();
			// If company dnc found then removing all indirect prospects.
			if (isCompanyDNC) {
				while (iterator.hasNext()) {
					IndirectProspectsDTO indirectProspect = iterator.next();
					iterator.remove();
					updateProspectCall(indirectProspect.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR, null,
							null, null, null, null);
				}
			} else {
				for (DNCList dncProspect : dncList) {
					while (iterator.hasNext()) {
						IndirectProspectsDTO indirectProspect = iterator.next();
						if (indirectProspect.getFirstName().equalsIgnoreCase(dncProspect.getFirstName())
								&& indirectProspect.getLastName().equalsIgnoreCase(dncProspect.getLastName())) {
							iterator.remove();
							updateProspectCall(indirectProspect.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR,
									null, null, null, null, null);
						}
					}
				}
			}
		} if (prospectCallCacheDTO != null && prospectCallCacheDTO.getIndirectProspects() != null
				&& prospectCallCacheDTO.getIndirectProspects().size() > 0) {
			Iterator<IndirectProspectsCacheDTO> iterator = prospectCallCacheDTO.getIndirectProspects().iterator();
			// If company dnc found then removing all indirect prospects.
			if (isCompanyDNC) {
				while (iterator.hasNext()) {
					IndirectProspectsCacheDTO indirectProspect = iterator.next();
					iterator.remove();
					updateProspectCall(indirectProspect.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR, null,
							null, null, null, null);
				}
			} else {
				for (DNCList dncProspect : dncList) {
					while (iterator.hasNext()) {
						IndirectProspectsCacheDTO indirectProspect = iterator.next();
						if (indirectProspect.getFirstName().equalsIgnoreCase(dncProspect.getFirstName())
								&& indirectProspect.getLastName().equalsIgnoreCase(dncProspect.getLastName())) {
							iterator.remove();
							updateProspectCall(indirectProspect.getProspectCallId(), ProspectCallStatus.LOCAL_DNC_ERROR,
									null, null, null, null, null);
						}
					}
				}
			}
		}
	}

	public void getBackProspectsFromMaxRetries(String campaignId, int dbCallMaxRetries,int dtoCallMaxRetries){

		List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.getBackProspectsFromMaxRetries(campaignId, dbCallMaxRetries, dtoCallMaxRetries);
		for(ProspectCallLog pcl : prospectCallLogList){
			pcl.getProspectCall().setDispositionStatus("DIALERCODE");
			pcl.getProspectCall().setSubStatus("BUSY");
			save(pcl, true);
		}
	}

	public boolean updateRecordingAndFailureProspects(String campaignId, String twilioCallSid) {
		if (campaignId == null || twilioCallSid == null) {
			return false;
		}
		List<ProspectCallLog> prospectCallLogs = prospectCallLogRepository.findByCampaignIdAndTwilioCallId(campaignId,
				twilioCallSid);
		if (prospectCallLogs != null && prospectCallLogs.size() > 1) {
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				CallLog callLog = callLogRepository.findOneById(twilioCallSid);
				if (callLog != null) {
					if (callLog.getCallLogMap().get("RecordingUrl") != null) {
						String recordingUrl = callLog.getCallLogMap().get("RecordingUrl");
						prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
						int lastIndex = recordingUrl.lastIndexOf("/");
						String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
						String rfileName = fileName + ".wav";
						String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
						prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
					}
				}
				if (prospectCallLog.getStatus().equals(ProspectCallStatus.CALL_DIALED)) {
					prospectCallLog.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
					prospectCallLog.getProspectCall().setDispositionStatus(DispositionType.DIALERCODE.name());
					prospectCallLog.getProspectCall().setSubStatus(DialerCodeDispositionStatus.ANSWERMACHINE.name());
					this.save(prospectCallLog, true);
				} else {
					this.save(prospectCallLog, false);
				}
			}
		}
		return true;
	}

	public List<DomainCompanyCountPair> getSuccessDomainsAndCompaniesCount(Campaign campaign) {
		List<String> campaignIds = new ArrayList<>();
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			campaignIds.addAll(campaign.getCampaignGroupIds());
			if (!campaignIds.contains(campaign.getId())) {
				campaignIds.add(campaign.getId());
			}
		} else {
			campaignIds.add(campaign.getId());
		}
		List<DomainCompanyCountPair> dataFromDB = new ArrayList<>();
		if (campaign.getDuplicateCompanyDuration() > 0 && campaign.getLeadsPerCompany() > 0) {
			logger.debug("Leads per company query start for [{}] campaign", campaign.getName());
			dataFromDB = prospectCallLogRepository.generateDuplicateProspectAggregationQuery(campaignIds,
					campaign.getDuplicateCompanyDuration());
//			logger.debug("Leads per company query end for [{}] campaign & distinct company list size is []",
//					campaign.getName(), dataFromDB.size());
		}
		return dataFromDB;
	}

	public boolean isSuccessDuplicate(Campaign campaign, String domain, String email, String companyName,
									   List<DomainCompanyCountPair> dataFromDB) {
		if (dataFromDB != null && !dataFromDB.isEmpty()) {
			for (DomainCompanyCountPair pair : dataFromDB) {
				if(companyName!=null && !companyName.isEmpty() && pair.getId() != null && !pair.getId().isEmpty() && pair.getId().equalsIgnoreCase(companyName)
						&& pair.getCount() >= campaign.getLeadsPerCompany()) {
					return true;
				}else if (email != null && !email.isEmpty() && pair.getDomain() != null &&  !pair.getDomain().isEmpty()  && pair.getDomain().equalsIgnoreCase(email)
						&& pair.getCount() >= campaign.getLeadsPerCompany()) {
					return true;
				} else if (domain != null && !domain.isEmpty() && pair.getDomain() != null && !pair.getDomain().isEmpty() &&  pair.getDomain().equalsIgnoreCase(domain)
						&& pair.getCount() >= campaign.getLeadsPerCompany()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isSuppressed(Campaign campaign, String domain, String domainEmail, String email) {
		boolean supEmail = false;
		boolean supDomain = false;

		if (email != null && !email.isEmpty()) {
			supEmail = suppressionListService.isEmailSuppressed(email.toLowerCase(), campaign.getId(),
					campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck());
		}
		if(domain!=null && !domain.isEmpty()) {
			supDomain = getsuppresionListByDomain(campaign.getId(), domain);
		}else if(domainEmail!=null && !domainEmail.isEmpty()) {
			supDomain = getsuppresionListByDomain(campaign.getId(), domainEmail);
		}
		if(supDomain || supEmail) {
			return true;
		}

		return false;
	}

	public boolean getsuppresionListByDomain(String campaignId, String domain){
		boolean  supEmail = false;
		if(domain!=null && !domain.isEmpty()) {
			long suppressionNewListsCount = suppressionListNewRepository
					.countSupressionListByCampaignDomain(campaignId, domain);
			if(suppressionNewListsCount > 0) {
				supEmail = true;
			}
		}

		return supEmail;
	}


	/*public SuppressionList getSuppressionList(Campaign campaign) {
		List<String> campaignIds = new ArrayList<>();
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			campaignIds.addAll(campaign.getCampaignGroupIds());
			if (!campaignIds.contains(campaign.getId())) {
				campaignIds.add(campaign.getId());
			}
		} else {
			campaignIds.add(campaign.getId());
		}
		List<SuppressionList> suppressionLists = suppressionListRepository
				.findSupressionListByCampaignIdsAndStatus(campaignIds);
		SuppressionList newSuppressionList = new SuppressionList();
		List<String> sDomains = new ArrayList<>();
		List<String> emails = new ArrayList<>();
		List<String> companies = new ArrayList<>();
		if (suppressionLists != null && !suppressionLists.isEmpty()) {
			for (SuppressionList suppressionList : suppressionLists) {
				if (suppressionList.getDomains() != null && !suppressionList.getDomains().isEmpty()) {
					sDomains.addAll(suppressionList.getDomains());
				}
				if (suppressionList.getEmailIds() != null && !suppressionList.getEmailIds().isEmpty()) {
					emails.addAll(suppressionList.getEmailIds());
				}
				if (suppressionList.getCompanies() != null && !suppressionList.getCompanies().isEmpty()) {
					companies.addAll(suppressionList.getCompanies());
				}
			}
		}
		List<String> domains = new ArrayList<String>();
		if (sDomains != null && sDomains.size() > 0) {
			for (String domain : sDomains) {
				if (domain != null && !domain.isEmpty()) {
					String cDomain = XtaasUtils.getSuppressedHostName(domain);
					if (!cDomain.isEmpty())
						domains.add(cDomain);
				}
			}
		}
		if (domains != null && domains.size() > 0) {
			newSuppressionList.setDomains(domains);
		}
		if (emails != null && emails.size() > 0) {
			newSuppressionList.setEmailIds(emails);
		}
		if (companies != null && companies.size() > 0) {
			newSuppressionList.setCompanies(companies);
		}
		return newSuppressionList;
	}*/


	@Override
	public int getbackCallableProspects(CallableGetbackCriteriaDTO criteriaDTO) {
		int recordsUpdatedCount = prospectCallLogRepository.getbackCallableProspects(criteriaDTO);
		logger.debug("[{}] records updated for [{}] criteria.", recordsUpdatedCount, criteriaDTO.toString());
		return recordsUpdatedCount;
	}

	private ProspectCallLog handledDuplicateKeyException(ProspectCallLog prospectCallLog) {
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		Prospect prospect = prospectCall.getProspect();
		List<ProspectCallLog> existingCallLogs = prospectCallLogRepository.findUinqueRecord(
				prospectCall.getCampaignId(), prospect.getFirstName(), prospect.getLastName(), prospect.getTitle(),
				prospect.getCompany());
		if (existingCallLogs != null && existingCallLogs.size() > 0) {
			ProspectCallLog existingCallLog = existingCallLogs.get(0);
			// check existing ProspectCallLog is in Failure/DNC/SUCCESS
			isDispositionStatusInSuccess(existingCallLog);
			String pcid = existingCallLog.getProspectCall().getProspectCallId();
			prospectCacheServiceImpl.addInManualDialProspectCallIds(pcid);
			// update existing ProspectCallLog with details received from agent and create interaction
			existingCallLog.setProspectCall(prospectCall);
			existingCallLog.getProspectCall().setProspectCallId(pcid);
			existingCallLog.setStatus(ProspectCallStatus.QUEUED);
			prospectCallLogRepository.save(existingCallLog);
			ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
			prospectCallInteraction.setProspectCall(existingCallLog.getProspectCall());
			prospectCallInteraction.setStatus(existingCallLog.getStatus());
			prospectCallInteraction.setCallbackDate(existingCallLog.getCallbackDate());
			prospectCallInteraction.setQaFeedback(existingCallLog.getQaFeedback());
			prospectLogInteractionRepository.save(prospectCallInteraction);

			// update prospectCallLog received from agent and create interaction
			if(prospectCallLog.getId() != null && !prospectCallLog.getId().isEmpty()) {
				ProspectCallLog prospectCallLogFromDB = prospectCallLogRepository.findOneById(prospectCallLog.getId());
				prospectCallLogFromDB.getProspectCall().setDispositionStatus(DispositionType.FAILURE.name());
				prospectCallLogFromDB.getProspectCall().setSubStatus(FailureDispositionStatus.PROSPECT_UNREACHABLE.name());
				prospectCallLogFromDB.setStatus(ProspectCallStatus.WRAPUP_COMPLETE);
				prospectCallLogRepository.save(prospectCallLogFromDB);
				ProspectCallInteraction prospectCallInteraction1 = new ProspectCallInteraction();
				prospectCallInteraction1.setProspectCall(prospectCallLogFromDB.getProspectCall());
				prospectCallInteraction1.setStatus(prospectCallLogFromDB.getStatus());
				prospectCallInteraction1.setCallbackDate(prospectCallLogFromDB.getCallbackDate());
				prospectCallInteraction1.setQaFeedback(prospectCallLogFromDB.getQaFeedback());
				prospectLogInteractionRepository.save(prospectCallInteraction1);
			}

			return existingCallLog;
		} else {
			/*ProspectCallLog latestProspectCallLogFromDB = prospectLogRepository
					.findByProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
			// return latestProspectCallLogFromDB;
			if (latestProspectCallLogFromDB != null && latestProspectCallLogFromDB.getProspectCall() != null) {
				if (prospectCallLog.getProspectCall().getAgentId() != null
						&& !prospectCallLog.getProspectCall().getAgentId().isEmpty()
						&& latestProspectCallLogFromDB.getProspectCall().getAgentId() != null
						&& !latestProspectCallLogFromDB.getProspectCall().getAgentId().isEmpty()
						&& latestProspectCallLogFromDB.getProspectCall().getAgentId()
								.equalsIgnoreCase(prospectCallLog.getProspectCall().getAgentId())) {
					return sameAgentProspectDetails(latestProspectCallLogFromDB);
				} else {
					// Below else block is for diffrent agent enters the prospect details again.
					return diffrentAgentProspectDetails(latestProspectCallLogFromDB);
				}
			}*/
			return prospectCallLog;
		}
	}

	@Override
	public void saveCallDetectedByInProspect(String callSid, String detectedAs, long timeDiffrence) {
		// TODO Auto-generated method stub
		if (callSid != null && detectedAs != null && !detectedAs.isEmpty()) {
			ProspectCallLog prospectFromDB = prospectLogRepository.findByTwilioCallId(callSid);
			if (prospectFromDB != null) {
				prospectFromDB.getProspectCall().setCallDetectedAs(detectedAs);
				prospectFromDB.getProspectCall().setAmdTimeDiffInSec(timeDiffrence);
				prospectCallLogRepository.save(prospectFromDB);
			}
		}
	}
	@Override
	public void saveCallDetectedByInProspectForMiliSecond(String callSid, String detectedAs, long timeDiffrenceInMilliSeconds) {
		// TODO Auto-generated method stub
		if (callSid != null && detectedAs != null && !detectedAs.isEmpty()) {
			ProspectCallLog prospectFromDB = prospectLogRepository.findByTwilioCallId(callSid);
			if (prospectFromDB != null) {
				prospectFromDB.getProspectCall().setCallDetectedAs(detectedAs);
				prospectFromDB.getProspectCall().setAmdTimeDiffInMilliSec(timeDiffrenceInMilliSeconds);
				prospectCallLogRepository.save(prospectFromDB);
			}
		}
	}

	@Override
	public int getProspectCountFromCache(String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		int prospectCount = 0;
		if (campaign != null) {
			if (campaign.isEnableProspectCaching()) {
				List<ProspectCallCacheDTO> totalProspectsInCache = priorityQueueCacheOperationService
						.getCurrentState(campaignId);
				if (totalProspectsInCache != null && totalProspectsInCache.size() > 0) {
					prospectCount = totalProspectsInCache.size();
				}
			} else {
				LinkedHashMap<String, List<ProspectCallDTO>> linkedProspectCallLog = prospectCacheServiceImpl
						.getCachedProspectCallLogOld();
				if (linkedProspectCallLog != null && linkedProspectCallLog.size() != 0) {
					if (linkedProspectCallLog.get(campaign.getId()) != null) {
						prospectCount = linkedProspectCallLog.get(campaign.getId()).size();
					}
				}
			}
		} else {
			throw new IllegalArgumentException("Campaign does not exist in the system.");
		}
		return prospectCount;
	}


	private void isProspectUniqueInCampaign( ProspectCallDTO prospectCallDTO) {
		String phoneNumber = prospectCallDTO.getProspect().getPhone(); // Removed + from phone number. as it gives error
		// in searching record from database.
		phoneNumber = phoneNumber.substring(1);
		ProspectCallLog uniqueProspectFromDB = prospectCallLogRepository.findManualProspectRecord(
				prospectCallDTO.getCampaignId(), prospectCallDTO.getProspect().getFirstName(),
				prospectCallDTO.getProspect().getLastName(), phoneNumber);
		if (uniqueProspectFromDB != null && uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& uniqueProspectFromDB.getStatus() != null) {
			if (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("FAILURE")) {
				if (uniqueProspectFromDB.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL")) {
					throw new IllegalArgumentException("Prospect is in DNC.");
				}
				throw new IllegalArgumentException(
						"Prospect is in FAILURE - " + uniqueProspectFromDB.getProspectCall().getSubStatus());
			} else if (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
				throw new IllegalArgumentException("Prospect is already lead in the campaign");
			}
		}
	}
	
	@Override
	public void generateCallbackCache(Campaign campaignFromDB,
			boolean isCallbackSubmitted) {
		try {
			if (isCallbackSubmitted) {
				addCallbackProspectsInCache(campaignFromDB);
			} else {
				List<String> diallingCampaignIds = new ArrayList<String>(
						agentRegistrationService.getCampaignActiveAgentCount().keySet());
				if (diallingCampaignIds != null && diallingCampaignIds.size() > 0) {
					for (String campaignId : diallingCampaignIds) {
						Campaign campaign = campaignService.getCampaign(campaignId);
						addCallbackProspectsInCache(campaign);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void generateMissedCallbackCache(Campaign campaignFromDB,
			boolean isCallbackSubmitted) {
		try {
			if (isCallbackSubmitted) {
				addMissedCallbacksInCache(campaignFromDB);
			} else {
				List<String> diallingCampaignIds = new ArrayList<String>(
						agentRegistrationService.getCampaignActiveAgentCount().keySet());
				if (diallingCampaignIds != null && diallingCampaignIds.size() > 0) {
					for (String campaignId : diallingCampaignIds) {
						Campaign campaign = campaignService.getCampaign(campaignId);
						addMissedCallbacksInCache(campaign);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addMissedCallbacksInCache(Campaign campaignFromDB) {
		if (campaignFromDB != null && campaignFromDB.isEnableCallbackFeature()) {
			List<String> excludeStateCodes = getStateCodesWithBusinessClosed();
			List<ProspectCallLog> callbackProspectsFromDB = prospectCallLogRepository.getMissedCallbackProspects(
					campaignFromDB.getId(), excludeStateCodes, XtaasDateUtils.getDateAtTime(new Date(), 0, 0, 0, 0),
					Sort.by(Direction.ASC, "callbackDate"));
			if (callbackProspectsFromDB != null && callbackProspectsFromDB.size() > 0) {
				List<CallbackProspectCacheDTO> prospectCallCacheDTOs = new ArrayList<CallbackProspectCacheDTO>();
				for (ProspectCallLog prospectCallLog : callbackProspectsFromDB) {
					prospectCacheServiceImpl.insertInMissedCallbackCache(prospectCallLog.getProspectCall().getAgentId(),
							new CallbackProspectCacheDTO(prospectCallLog));
				}
				logger.info(
						"generateMissedCallbackCache() :: Inserted [{}] callback prospects in callback cache having campaignId [{}]",
						prospectCallCacheDTOs.size(), campaignFromDB.getId());
			}
		}
	}

	private void addCallbackProspectsInCache(Campaign campaignFromDB) throws ParseException {
		if (campaignFromDB != null && campaignFromDB.isEnableCallbackFeature()) {
			List<String> excludeStateCodes = getStateCodesWithBusinessClosed();
			List<ProspectCallLog> callbackProspectsFromDB = prospectCallLogRepository.getTodayCallbackProspects(
					campaignFromDB.getId(), excludeStateCodes, XtaasDateUtils.getDateAtTime(new Date(), 0, 0, 0, 0),
					XtaasDateUtils.getDateAtTime(new Date(), 23, 59, 59, 0), Sort.by(Direction.ASC, "callbackDate"));
			if (callbackProspectsFromDB != null && callbackProspectsFromDB.size() > 0) {
				List<CallbackProspectCacheDTO> prospectCallCacheDTOs = new ArrayList<CallbackProspectCacheDTO>();
				for (ProspectCallLog prospectCallLog : callbackProspectsFromDB) {
					prospectCallCacheDTOs.add(new CallbackProspectCacheDTO(prospectCallLog));
				}
				prospectCacheServiceImpl.insertCallbackRecordsInCache(prospectCallCacheDTOs);
				logger.info(
						"generateCallbackCache() :: Inserted [{}] callback prospects in callback cache having campaignId [{}]",
						prospectCallCacheDTOs.size(), campaignFromDB.getId());
			}
		}
	}
}