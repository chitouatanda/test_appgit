package com.xtaas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.MasterData;
import com.xtaas.db.repository.MasterDataRepository;

@Service
public class MasterDataServiceImpl implements MasterDataService {
	
	@Autowired
	private MasterDataRepository masterDataRepository;
	
	@Override
	public MasterData getMasterData(String id) {
		return masterDataRepository.findOneByType(id);
	}

}
