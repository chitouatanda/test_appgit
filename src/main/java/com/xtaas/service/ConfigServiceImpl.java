/**
 * 
 */
package com.xtaas.service;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.OrgTypeExperienceMap;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.OrganizationTypesRepository;
import com.xtaas.db.repository.StateCallConfigRepository;

/**
 * @author djain
 *
 */
@Service
public class ConfigServiceImpl implements ConfigService {

	private static final Logger logger = LoggerFactory.getLogger(ConfigServiceImpl.class);
	
	private ConcurrentHashMap<String, StateCallConfig> stateCallConfigCache = new ConcurrentHashMap<String, StateCallConfig>();
	private ConcurrentHashMap<String, OrgTypeExperienceMap> organizationTypeMapCache = new ConcurrentHashMap<String, OrgTypeExperienceMap>();
	
	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;
	
	@Autowired
    private OrganizationTypesRepository organizationTypesRepository;
    
	
	@Override
	public void addStateCallConfig(StateCallConfig stateCallConfig) {
		stateCallConfigRepository.save(stateCallConfig);
		logger.debug("Saved State Call Config for State: " + stateCallConfig.getStateCode());
	}

	@Override
	public StateCallConfig getStateCallConfig(String stateCode) {
		StateCallConfig stateCallConfig = stateCallConfigCache.get(stateCode);
		if (stateCallConfig == null) { //if not found in cache then load from DB
			stateCallConfig = stateCallConfigRepository.findByStateCode(stateCode);
			if (stateCallConfig != null) {
				stateCallConfigCache.put(stateCode, stateCallConfig);
				logger.debug("Found StateCallConfig for State: " + stateCallConfig.getStateCode());
			} else {
				logger.debug("Couldn't find StateCallConfig for State: " + stateCode + " in DB");
			}
		}
		return stateCallConfig;
	}

    @Override
    public OrgTypeExperienceMap getOrgTypeExperienceMap(String organizationType) {
        OrgTypeExperienceMap orgTypeExperienceMap = organizationTypeMapCache.get(organizationType);
        if(orgTypeExperienceMap == null ){
             orgTypeExperienceMap = organizationTypesRepository.findOneById(organizationType);
             if (orgTypeExperienceMap != null) {
                 organizationTypeMapCache.put(organizationType, orgTypeExperienceMap);
                 logger.debug("Found OrgTypeExperienceMap for organizationType: " + orgTypeExperienceMap);
             } else {
                 logger.debug("Couldn't find OrgTypeExperienceMap for organizationType: " + organizationType + " in DB");
             }
        }
        return orgTypeExperienceMap;
    }
}
