/**
 * 
 */
package com.xtaas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.DialerPreProcessEntity;
import com.xtaas.db.repository.DialerPreProcessQueueRepository;

/**
 * @author djain
 *
 */
@Service
public class DialerPreProcessQueueServiceImpl implements DialerPreProcessQueueService {

	@Autowired
	private DialerPreProcessQueueRepository dialerPreProcessQueueRepository;
	
	@Override
	public DialerPreProcessEntity add(DialerPreProcessEntity entity) {
		return dialerPreProcessQueueRepository.save(entity);
	}

	@Override
	public void addAll(List<DialerPreProcessEntity> entityList) {
		dialerPreProcessQueueRepository.saveAll(entityList);
	}

	@Override
	public DialerPreProcessEntity getNext(String campaignId, String sortFieldPath) {
		return dialerPreProcessQueueRepository.findNext(campaignId, Sort.by(Sort.Direction.DESC, sortFieldPath));
	}

	@Override
	public DialerPreProcessEntity getNext(String campaignId) {
		return getNext(campaignId, "prospect.customAttributes.CreatedDate"); //default sort by DESC createddate to process prospects in LIFO manner
	}

	@Override
	public void delete(String id) {
		dialerPreProcessQueueRepository.deleteById(id);
	}
}
