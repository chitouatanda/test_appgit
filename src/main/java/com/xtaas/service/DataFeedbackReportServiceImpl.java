package com.xtaas.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.xtaas.aws.AwsS3Service;
import com.xtaas.db.entity.FeedbackJobTracker;
import com.xtaas.db.entity.PartnerFeedbackMapping;
import com.xtaas.db.entity.ProspectFeedback;
import com.xtaas.db.entity.FeedbackJobTracker.JobStatus;
import com.xtaas.db.repository.FeedbackJobTrackerRepository;
import com.xtaas.db.repository.PartnerFeedbackMappingRepository;
import com.xtaas.db.repository.ProspectFeedbackRepository;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.PartnerFeedbackMappingDTO;

/**
 * Generate data feedback report & store it on AWS S3
 * 
 * @author pranay
 */
@Service
public class DataFeedbackReportServiceImpl implements DataFeedbackReportService {

	public static final Logger logger = LoggerFactory.getLogger(DataFeedbackReportServiceImpl.class);

	private final AwsS3Service awsS3Service;
	private final PropertyService propertyService;
	private final ProspectFeedbackRepository prospectFeedbackRepository;
	private final PartnerFeedbackMappingRepository partnerFeedbackMappingRepository;
	private final FeedbackJobTrackerRepository feedbackJobTrackerRepository;

	public DataFeedbackReportServiceImpl(AwsS3Service awsS3Service, PropertyService propertyService,
			ProspectFeedbackRepository prospectFeedbackRepository,
			PartnerFeedbackMappingRepository partnerFeedbackMappingRepository,
			FeedbackJobTrackerRepository feedbackJobTrackerRepository) {
		this.awsS3Service = awsS3Service;
		this.propertyService = propertyService;
		this.prospectFeedbackRepository = prospectFeedbackRepository;
		this.partnerFeedbackMappingRepository = partnerFeedbackMappingRepository;
		this.feedbackJobTrackerRepository = feedbackJobTrackerRepository;
	}

	@Override
	public String autoDataFeedbackReport(String jobId, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
		feedbackJobTrackerRepository.findByJobId(jobId).ifPresent(feedbackJobTracker -> {
			feedbackJobTrackerRepository.save(
					feedbackJobTracker.withReportingJobStartTime(new Date()).withStatus(JobStatus.REPORTING_INPROCESS));
		});

		generateFeedbackReportByPartner(jobId, partnerFeedbackMappingDTO);
		return jobId;
	}

	@Override
	public String manualDataFeedbackReport(String name) throws Exception {
		PartnerFeedbackMapping partnerFeedbackMapping = partnerFeedbackMappingRepository
				.findByNameAndEnableDataFeedback(name, true);
		if (partnerFeedbackMapping == null) {
			throw new IllegalArgumentException(String.format(
					"Unable to fetch Partner [%s] from [%s] collection. Make sure partner exists & [%s] flag is enabled.",
					name, "partnerfeedbackmapping", "enableDataFeedback"));
		}
		String jobId = UUID.randomUUID().toString();
		feedbackJobTrackerRepository.save(new FeedbackJobTracker(jobId, JobStatus.REPORTING_INPROCESS,
				ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).withReportingJobStartTime(new Date()));
		return generateFeedbackReportByPartner(jobId, new PartnerFeedbackMappingDTO(partnerFeedbackMapping));
	}

	private String generateFeedbackReportByPartner(String jobId, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
		Instant startInstant = Instant.now();
		logger.info("Started generateFeedbackReportByPartner - Partner:[{}], JobId:[{}].",
				partnerFeedbackMappingDTO.getName(), jobId);
		try {
			createAndUploadCsv(partnerFeedbackMappingDTO);

			feedbackJobTrackerRepository.findByJobId(jobId).ifPresent(feedbackJobTracker -> {
				feedbackJobTrackerRepository.save(feedbackJobTracker.withReportingJobEndTime(new Date())
						.withStatus(JobStatus.REPORTING_COMPLETED));
			});
			logger.info(
					"Finished generateFeedbackReportByPartner - Partner:[{}], JobId:[{}]. Time taken in seconds: [{}]",
					partnerFeedbackMappingDTO.getName(), jobId,
					Duration.between(startInstant, Instant.now()).getSeconds());
		} catch (Exception ex) {
			logger.error("Failed to generate data feedback report for Partner:[{}], JobId:[{}]. Error: ",
					partnerFeedbackMappingDTO.getName(), jobId, ex);
			handleJobException(partnerFeedbackMappingDTO, jobId, ex);
		}
		return jobId;
	}

	private void createAndUploadCsv(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) throws Exception {
		// set default headers if not present
		if (CollectionUtils.isEmpty(partnerFeedbackMappingDTO.getHeaders())) {
			partnerFeedbackMappingDTO.setHeaders(XtaasConstants.DEFAULT_HEADERS_DATA_FEEDBACK);
		}
		// set default status mapping if not present
		if (CollectionUtils.isEmpty(partnerFeedbackMappingDTO.getStatusMapping())) {
			partnerFeedbackMappingDTO.setStatusMapping(XtaasConstants.DEFAULT_STATUS_MAPPING_DATA_FEEDBACK);
		}
		Calendar calendar = Calendar.getInstance();
		String fileName = partnerFeedbackMappingDTO.getName() + "_"
				+ new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + ".csv";
		calendar.add(Calendar.DATE, partnerFeedbackMappingDTO.getDataFeedbackInDays() * -1);
		Date callStartTime = calendar.getTime();

		int count = prospectFeedbackRepository.countBySourceStauseAndCallStartTime(partnerFeedbackMappingDTO.getName(),
				XtaasConstants.DATA_FEEDBACK_STATUSES, callStartTime);
		if (count < 1) {
			logger.info("No prospect found for [{}] Partner.", partnerFeedbackMappingDTO.getName());
			return;
		}

		int size = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.BULKREAD_BATCHSIZE.name(), 10000);
		int pages = count / size;
		int mod = count % size;
		if (mod > 0) {
			pages = pages + 1;
		}

		List<String[]> lines = new ArrayList<String[]>();

		// create headers string & add into list
		int headerCount = 0;
		String[] headerLine = new String[partnerFeedbackMappingDTO.getHeaders().size()];
		for (String header : partnerFeedbackMappingDTO.getHeaders().keySet()) {
			headerLine[headerCount] = header;
			headerCount++;
		}
		lines.add(headerLine);

		for (int page = 0; page < pages; page++) {
			Pageable pageable = PageRequest.of(page, size, Direction.DESC, "callStartTime");
			List<ProspectFeedback> prospectFeedbacks = prospectFeedbackRepository.findBySourceStauseAndCallStartTime(
					partnerFeedbackMappingDTO.getName(), XtaasConstants.DATA_FEEDBACK_STATUSES, callStartTime,
					pageable);
			if (CollectionUtils.isEmpty(prospectFeedbacks)) {
				logger.info("No prospects found for [{}] Partner", partnerFeedbackMappingDTO.getName());
				continue;
			}

			writeData(partnerFeedbackMappingDTO, prospectFeedbacks, lines);
		}

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputStreamWriter streamWriter = new OutputStreamWriter(stream, StandardCharsets.UTF_8);
		CSVWriter csvWriter = new CSVWriter(streamWriter, '|', ICSVWriter.NO_QUOTE_CHARACTER,
				ICSVWriter.DEFAULT_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END);

		csvWriter.writeAll(lines);
		csvWriter.flush();

		ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentLength(stream.toByteArray().length);

		// Upload file to AWS S3
		URI uri = awsS3Service.UploadFile(partnerFeedbackMappingDTO.getS3BucketName(), fileName,
				new ByteArrayInputStream(stream.toByteArray()), objectMetadata, true);
		logger.info("File {} uploaded successfully, URI {}", fileName, uri);

		csvWriter.close();
		streamWriter.close();
		stream.close();

		// send email
		XtaasEmailUtils.sendEmail(partnerFeedbackMappingDTO.getToEmails(), partnerFeedbackMappingDTO.getFromEmail(),
				partnerFeedbackMappingDTO.getEmailSubject(),
				partnerFeedbackMappingDTO.getEmailMessageBody() + " " + uri.toString());
	}

	// create data string & add into list
	private List<String[]> writeData(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO,
			List<ProspectFeedback> prospectFeedbacks, List<String[]> lines) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for (ProspectFeedback prospectFeedback : prospectFeedbacks) {
			int j = 0;
			String[] line = new String[partnerFeedbackMappingDTO.getHeaders().size()];
			for (String header : partnerFeedbackMappingDTO.getHeaders().keySet()) {
				Object value = getCellValue(prospectFeedback, partnerFeedbackMappingDTO, header);
				if (value instanceof String) {
					line[j] = value.toString();
				} else if (value instanceof Date) {
					line[j] = formatter.format((Date) value);
				} else {
					line[j] = value.toString();
				}
				j++;
			}
			lines.add(line);
		}

		return lines;
	}

	/**
	 * Get the value from ProspectFeedback for specified header. Add switch case if
	 * new fields are added into ProspectFeedback.
	 * 
	 * @param prospectFeedback
	 * @param key
	 * @return
	 */
	private Object getCellValue(ProspectFeedback prospectFeedback, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO,
			String header) {
		Object value = null;
		String key = partnerFeedbackMappingDTO.getHeaders().get(header);
		switch (key.toLowerCase()) {
		case "firstname":
			value = prospectFeedback.getFirstName();
			break;
		case "lastname":
			value = prospectFeedback.getLastName();
			break;
		case "phone":
			value = prospectFeedback.getPhone();
			break;
		case "email":
			value = prospectFeedback.getEmail();
			break;
		case "title":
			value = prospectFeedback.getTitle();
			break;
		case "company":
			value = prospectFeedback.getCompany();
			break;
		case "source":
			value = prospectFeedback.getSource();
			break;
		case "dispositionstatus":
			value = prospectFeedback.getDispositionStatus();
			break;
		case "substatus":
			value = prospectFeedback.getSubStatus();
			break;
		case "status":
			value = prospectFeedback.getStatus();
			break;
		case "recordingurl":
			value = prospectFeedback.getRecordingUrl();
			break;
		case "callstarttime":
			value = prospectFeedback.getCallStartTime();
			break;
		case "domain":
			value = prospectFeedback.getDomain();
			break;
		case "maxemployeecount":
			value = prospectFeedback.getMaxEmployeeCount();
			break;
		case "minemployeecount":
			value = prospectFeedback.getMinEmployeeCount();
			break;
		case "maxrevenue":
			value = prospectFeedback.getMaxRevenue();
			break;
		case "minrevenue":
			value = prospectFeedback.getMinRevenue();
			break;
		case "campaignid":
			value = prospectFeedback.getCampaignId();
			break;
		case "prospectcalllogid":
			value = prospectFeedback.getProspectCallLogId();
			break;
		case "updateddate":
			value = prospectFeedback.getUpdatedDate();
			break;
		case "callstatusexplanation":
			value = partnerFeedbackMappingDTO.getStatusMapping().get(prospectFeedback.getSubStatus());
			break;
		default:
			value = "NA";
			break;
		}
		if (value == null) {
			value = "NA";
		}
		return value;
	}

	private void handleJobException(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO, String jobId, Exception ex) {
		// Update job entry into DB
		feedbackJobTrackerRepository.findByJobId(jobId).ifPresent(feedbackJobTracker -> {
			feedbackJobTrackerRepository.save(feedbackJobTracker.withReportingJobEndTime(new Date())
					.withStatus(JobStatus.REPORTING_ERROR).withErrorMsg(ex.getMessage()));
		});

		// send email with details
		String subject = "failed data feedback report job";
		String message = String.format("Failed to generate data feedback report. Partner:[%s] Job:[%s - %s]. Error: %s",
				partnerFeedbackMappingDTO.getName(), jobId, ex.getMessage());
		XtaasEmailUtils.sendEmail(partnerFeedbackMappingDTO.getErrorToEmails(),
				partnerFeedbackMappingDTO.getFromEmail(), subject, message);
	}

}
