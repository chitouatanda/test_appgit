package com.xtaas.service;

import com.amazonaws.services.s3.model.S3Object;
import com.monitorjbl.xlsx.StreamingReader;
import com.opencsv.CSVWriter;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.*;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CurrentEmployment;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.insideview.*;
import com.xtaas.utils.*;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.Criteria;
import com.xtaas.web.dto.ManualProspectDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

@Service
public class ManualProspectSearchServiceImpl implements ManualProspectSearchService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ManualProspectSearchServiceImpl.class);

	private String partnerKey = "Xtaascorp.partner";

	private static final String manualCompanyUrl = "https://api.insideview.com/api/v1/company/{companyId}";

	private static final String manualPersonDetailUrl = "https://api.insideview.com/api/v1/contact/{contactId}";//GET

	final private String insideViewToken = "f0VbD34ZWUJkB7DuzKfiKE8v+3lniKgTcC9lOkPm1R/u0PNIAT1BD2AVC08ZFdWIu+pXWAB361zZWxQ3Dv5/81wTqDchuL5SyAHKprPeLztLqMvI6AvofZUggIDldY2fUFq+o+lgLnJajEfhxSsa8iNmjeJQAh6s4tI4BNWBMbs=.eyJmZWF0dXJlcyI6InsgfSIsImNsaWVudElkIjoibnZhODZpbGI0YzFtdm83Mmdva2wiLCJncmFudFR5cGUiOiJjcmVkIiwidHRsIjoiMzE1MzYwMDAiLCJpYXQiOiIxNjE3MTM2MDU1IiwidmVyc2lvbiI6InYxIiwianRpIjoiYTgxNGFhYWQtM2Y5ZS00YzczLThlMDktMTQ1MzRhYzMwMzgzIn0=";

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;

	@Autowired
	private GlobalContactRepository globalContactRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private EntityMappingRepository entityMappingRepository;

	@Autowired
	private PropertyService propertyService;

	String mTokenKey = "";

	Date mCachedDate;

	@Autowired
	CampaignService campaignService;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	CampaignMetaDataRepository campaignMetaDataRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private CompanyMasterRepository companyMasterRepository;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Autowired
	private ZoomInfoDataBuy zoomInfoDataBuy;


	//public Map<String,List<ManualProspectDTO>> mapOfManualProspect = new HashMap<>();

	public Map<String,List<Prospect>> manualProspectMap = new ConcurrentHashMap<>();

	public static final Integer SIMILAR_PROSPECT_SIZE = 10;

	private static CountDownLatch latch;

	@Override
	public List<Prospect> searchManualProspectDetails(String campaignId, Prospect prospect) {
		List<Prospect> prospectsList = new ArrayList<Prospect>();
		Campaign campaign =campaignService.getCampaign(campaignId);

		List<KeyValuePair<String, Integer>> manualPriorityList = new ArrayList<KeyValuePair<String,Integer>>();
		//check if you have priority set at campaign level
		manualPriorityList = campaign.getManualDataSourcePriority();
		if(manualPriorityList==null || manualPriorityList.isEmpty()) {
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			CampaignMetaData cmd = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
			if(organization.getCampaignDefaultSetting()!=null && organization.getCampaignDefaultSetting().getManualDataSourcePriority()!=null) {
				manualPriorityList = organization.getCampaignDefaultSetting().getManualDataSourcePriority();
			}else if(cmd!=null && cmd.getManualDataSourcePriority()!=null && !cmd.getManualDataSourcePriority().isEmpty()) {
				manualPriorityList = cmd.getManualDataSourcePriority();
			}
		}
		CampaignCriteria cc = getCampaignCriteria(campaign);
		// TreeMap to store values of HashMap
		TreeMap<Integer, String> sortedMap = new TreeMap<>();
		if(manualPriorityList!=null && manualPriorityList.size()>0) {
			for(KeyValuePair<String, Integer> keyvaluepair : manualPriorityList) {
				sortedMap.put(keyvaluepair.getValue(), keyvaluepair.getKey());
			}
			for (Map.Entry<Integer, String> entry : sortedMap.entrySet())  {
				if((prospectsList==null || prospectsList.size() <= 10) && entry.getValue().equalsIgnoreCase("ZOOMINFO")) {
					//get Zoom data
					prospectsList = getZoomData(prospect,prospectsList,cc,campaign);
				}

				if((prospectsList==null || prospectsList.size() <= 10) && entry.getValue().equalsIgnoreCase("LEADIQ")) {
					//get LeadIqData
					//prospectsList = getLeadIQData(prospect);
				}
				if((prospectsList==null || prospectsList.size() <= 10) && entry.getValue().equalsIgnoreCase("INSIDEVIEW")) {
					//get insideView
					//prospectsList = getInsideViewData(prospect);
				}
			}

		}else {
			//default to leadiq data
			//prospectsList = getLeadIQData(prospect);

		}

		return prospectsList;
	}

	public List<Prospect> getSimilarProspect(Prospect prospect,Campaign campaign,String agentId,boolean manualSearch){
		int maxSearchTime = propertyService
				.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_SEARCH_TIME.name(), 10);

		if(!manualSearch && prospect != null
				&& StringUtils.isNotBlank(prospect.getEmail())
				&& StringUtils.isBlank(prospect.getCompany())){
			prospect.setCompany(zoomInfoDataBuy.getCompanyFromEmail(prospect.getEmail()));
		}
		TreeMap<Integer, String> similarProspectMap =  getSimilarMap(campaign);
		if(similarProspectMap != null && !similarProspectMap.isEmpty() && similarProspectMap.size() >0){
		//	List<DomainCompanyCountPair> successCompanyData = prospectCallLogService.getSuccessDomainsAndCompaniesCount(campaign);
			List<DomainCompanyCountPair> successCompanyData =new ArrayList<>();
			plivoOutboundNumberService.cacheAllPlivoOutboundNumberForDatabuy();
			latch = new CountDownLatch(similarProspectMap.size());
			Executor executor = Executors.newFixedThreadPool(similarProspectMap.size());
			logger.info("Thread started for fetching the similar prospect for campaing [{}] ", campaign.getId());

			for (Map.Entry<Integer, String> entry : similarProspectMap.entrySet())  {
				if(entry.getValue().equalsIgnoreCase("ZOOMINFO") && entry.getKey() > 0){
					executor.execute(new ZoomManualSearchProspectServiceImpl(prospect,campaign,agentId,latch,manualSearch,successCompanyData));
				}
				if(entry.getValue().equalsIgnoreCase("LEADIQ") && entry.getKey() > 0){
					executor.execute(new LeadIQManualSearchProspectServiceImpl(prospect,campaign,agentId,latch,manualSearch,successCompanyData));
				}
			}
		  // executor.execute(new ZoomManualSearchProspectServiceImpl(prospect,campaignId,agentId,latch,manualSearch));
		   //executor.execute(new LeadIQManualSearchProspectServiceImpl(prospect,campaignId,agentId,latch,manualSearch));
			//executor.execute(new InsideViewManualSearchProspectServiceImpl(prospect,campaignId,agentId,latch));
			try {
				latch.await(maxSearchTime, TimeUnit.SECONDS);

				if(manualProspectMap != null && manualProspectMap.get(agentId) != null && manualProspectMap.get(agentId).size() > 0){
					return manualProspectMap.get(agentId);
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}

	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();
			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
		}
		return cc;
	}

	private List<Prospect> getZoomData(Prospect prospect,List<Prospect> prospectsList,CampaignCriteria cc,Campaign campaign) {
		List<Prospect> prospectsZoomInfoList = getPurchaseResponse(prospect,cc,campaign);
		if(prospectsZoomInfoList != null && !prospectsZoomInfoList.isEmpty() && prospectsZoomInfoList.size() > 0) {
			prospectsList.addAll(prospectsZoomInfoList);
			return prospectsList;
		}
		return null;
	}


	private List<Prospect> getPurchaseResponse(Prospect prospect,CampaignCriteria cc,Campaign campaign) {
		List<Prospect> prospectsZoomInfoList = new ArrayList<Prospect>();
		try {
			boolean isDirectSearch = true;
			// first search for direct
			ZoomInfoResponse personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc);
			// if you dont get direct then search for indirect number;
			if (personZoomInfoResponse == null || personZoomInfoResponse.getPersonSearchResponse() == null
					|| personZoomInfoResponse.getPersonSearchResponse().getTotalResults() == 0) {
				isDirectSearch = false;
				personZoomInfoResponse = preparePersonSearch(prospect, isDirectSearch,cc);
			}

			if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
				int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();

				if (totalResult > 0 && personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
						.getPersonRecord() != null) {
					List<PersonRecord> list = personZoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults()
							.getPersonRecord();
					if (list != null && list.size() > 0) {
						for(PersonRecord pr : list){
							Prospect p = getProspectInfo(pr,isDirectSearch,prospect);
							if(p != null && p.getPhone() != null){
								try {
									int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaign.getId(), p.getFirstName(), p.getFirstName(),
											p.getTitle(), p.getCompany());
									if(uniqueRecord == 0){
										prospectsZoomInfoList.add(p);
										if(prospectsZoomInfoList != null && prospectsZoomInfoList.size() >= 10) {
											break;
										}
									}
								}catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prospectsZoomInfoList;
	}

	private Prospect getProspectInfo(PersonRecord personRecord,boolean isDirectSearch,Prospect prospect) {
		try {
			String personDetailRequestUrl = buildUrl(null, 0, 0, personRecord.getPersonId());
			URL detailUrl = new URL(personDetailRequestUrl); // purchase request
			ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(detailUrl);
			if (zoomInfoResponseDetail.getPersonDetailResponse() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getFirstName() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getLastName() != null
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getFirstName())
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getLastName())
					&& (zoomInfoResponseDetail.getPersonDetailResponse().getDirectPhone() != null
					|| zoomInfoResponseDetail.getPersonDetailResponse()
					.getCompanyPhone() != null)) {

				PersonDetailResponse personDetailResponse = zoomInfoResponseDetail
						.getPersonDetailResponse();
				List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
				int industryListSize = 0;
				if (industryList != null) {
					industryListSize = industryList.size();
				}
				List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
				List<String> topLevelIndustries = new ArrayList<String>();
				String zoomCountry = "United States";
				Prospect prospectInfo = new Prospect();
				if (personDetailResponse.getLocalAddress() != null
						&& personDetailResponse.getLocalAddress().getCountry() != null) {
					if (personDetailResponse.getLocalAddress().getStreet() != null)
						prospectInfo.setAddressLine1(personDetailResponse.getLocalAddress().getStreet());
					else
						prospectInfo.setAddressLine1("");
					if (personDetailResponse.getLocalAddress().getZip() != null)
						prospectInfo.setZipCode(personDetailResponse.getLocalAddress().getZip());
					else
						prospectInfo.setZipCode("");
					if (personDetailResponse.getLocalAddress().getCity() != null)
						prospectInfo.setCity(personDetailResponse.getLocalAddress().getCity());
					else
						prospectInfo.setCity("");
					// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
					// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
				} else if (personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCountry() != null) {
					prospectInfo.setAddressLine1(personDetailResponse.getCurrentEmployment().get(0)
							.getCompany().getCompanyAddress().getStreet());
					prospectInfo.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getZip());
					prospectInfo.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCity());
				}
				if (personDetailResponse.getLastUpdatedDate() != null
						&& !personDetailResponse.getLastUpdatedDate().isEmpty()) {
					String ld = personDetailResponse.getLastUpdatedDate();
					if (ld.contains("-"))
						ld = ld.replace("-", "/");
					Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);

					prospectInfo.setLastUpdatedDate(date1);
				}
				if (personDetailResponse.getLocalAddress() != null) {
					prospectInfo.setCountry(personDetailResponse.getLocalAddress().getCountry());
					zoomCountry = personDetailResponse.getLocalAddress().getCountry();
				} else if (personDetailResponse.getCurrentEmployment() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyAddress().getCountry() != null) {
					prospectInfo.setCountry(personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCountry());
					zoomCountry = personDetailResponse.getCurrentEmployment().get(0).getCompany()
							.getCompanyAddress().getCountry();
				}

				if (prospectInfo.getCountry().equalsIgnoreCase("USA")
						|| prospectInfo.getCountry().equalsIgnoreCase("United States")
						|| prospectInfo.getCountry().equalsIgnoreCase("US")
						|| prospectInfo.getCountry().equalsIgnoreCase("Canada")) {
					prospectInfo.setPhone("+1"+getUSPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getUSCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getStateCode(personDetailResponse));
				} else if (prospectInfo.getCountry().equalsIgnoreCase("United Kingdom")
						|| prospectInfo.getCountry().equalsIgnoreCase("UK")) {
					prospectInfo.setPhone(getUKPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getUKCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getUKStateCode(personDetailResponse));
				} else {
					prospectInfo.setPhone(getPhone(personDetailResponse));
					prospectInfo.setExtension(getExtension(personDetailResponse));
					// String phoneType =
					// phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					// prospect.setPhoneType(phoneType);
					prospectInfo.setCompanyPhone(getCompanyPhone(personDetailResponse));
					prospectInfo.setStateCode(getStateCode(personDetailResponse));
				}

				if (industryListSize > 0) {
					prospectInfo.setIndustry(industryList.get(0));
					prospectInfo.setIndustryList(industryList);
				}

				prospectInfo.setEu(personDetailResponse.isEU());

				String title = personRecord.getCurrentEmployment().getJobTitle();
				String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
				String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
				String empRange = personRecord.getCurrentEmployment().getCompany()
						.getCompanyEmployeeCountRange();
				long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();
				long revCountInZeros = personRecord.getCurrentEmployment().getCompany()
						.getCompanyRevenueIn000s();
				String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
				ZoomInfoResponse companyResponse = getCompanyResponse(null,
						personRecord.getCurrentEmployment().getCompany().getCompanyId());

				Map<String, Object> customAttributes = new HashMap<String, Object>();
				Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);

				customAttributes.put("minRevenue", revenueEmployeeList.get("minRevenue"));
				customAttributes.put("maxRevenue", revenueEmployeeList.get("maxRevenue"));

				customAttributes.put("maxEmployeeCount", revenueEmployeeList.get("maxEmployeeCount"));
				customAttributes.put("minEmployeeCount", revenueEmployeeList.get("minEmployeeCount"));

				String detailDomain = personDetailResponse.getCurrentEmployment().get(0).getCompany()
						.getCompanyWebsite();
				if (detailDomain != null && !detailDomain.isEmpty()) {
					customAttributes.put("domain", detailDomain);
				} else {
					customAttributes.put("domain", domain);
				}
				customAttributes.put("revCount", revCount);
				customAttributes.put("revCountIn000s", new Long(revCountInZeros));
				customAttributes.put("empCount", new Long(empCount));
				prospectInfo.setCustomAttributes(customAttributes);

				prospectInfo.setCompany(getOrganizationName(zoomInfoResponseDetail));
				if (personDetailResponse.getCurrentEmployment().get(0).getManagementLevel() != null)
					prospectInfo.setManagementLevel(
							personDetailResponse.getCurrentEmployment().get(0).getManagementLevel().get(0));

				String timez = getTimeZone(prospect);
				prospectInfo.setTimeZone(timez);
				// prospectInfo.setCompanyAddress(personDetailResponse.get);

				prospectInfo.setDepartment(getJobFunction(zoomInfoResponseDetail));

				prospectInfo.setDirectPhone(isDirectSearch);
				if(personDetailResponse.getEmailAddress() != null && !personDetailResponse.getEmailAddress().isEmpty()) {
					prospectInfo.setEmail(personDetailResponse.getEmailAddress());
				}else {
					if(prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
						prospectInfo.setEmail(prospect.getEmail());
					}
				}

				// prospectInfo.setEu(personDetailResponse.get);
				prospectInfo.setFirstName(personDetailResponse.getFirstName());

				prospectInfo.setLastName(personDetailResponse.getLastName());
				// prospectInfo.setPrefix(prefix);
				// prospectInfo.setProspectType(prospectType);
				prospectInfo.setSourceType("INTERNAL");
				prospectInfo.setSourceCompanyId(getOrganizationId(zoomInfoResponseDetail));
				prospectInfo.setSourceId(personDetailResponse.getPersonId());
				prospectInfo.setSource("ZOOINFO");
				// prospectInfo.setStatus(status);
				prospectInfo.setTitle(title);
				prospectInfo.setTopLevelIndustries(topLevelIndustries);
				prospectInfo.setZoomPersonUrl(personDetailResponse.getZoomPersonUrl());

				if (companyResponse.getCompanySearchResponse() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord() != null
						&& companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0) != null) {

					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getZoomCompanyUrl() != null) {
						prospectInfo.setZoomCompanyUrl(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getZoomCompanyUrl());
					}

					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanyNAICS() != null) {
						prospectInfo.setCompanyNAICS(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getCompanyNAICS());
					}
					if (companyResponse.getCompanySearchResponse().getCompanySearchResults()
							.getCompanyRecord().get(0).getCompanySIC() != null) {
						prospectInfo.setCompanySIC(companyResponse.getCompanySearchResponse()
								.getCompanySearchResults().getCompanyRecord().get(0).getCompanySIC());
					}

				}
				return prospectInfo;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private ZoomInfoResponse preparePersonSearch(Prospect prospect, boolean isDirectSearch,CampaignCriteria cc) {
		int pageNumber = 1;
		URL url;
		int size = 25;
		try {
			String queryString = buildQueryString(prospect, isDirectSearch,cc);
			String previewUrl = buildUrl(queryString, pageNumber, size, null);
			//logger.debug("\n previewUrl1: " + previewUrl);
			url = new URL(previewUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
			return personZoomInfoResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String buildUrl(String queryString, int offset, int size, String personId) throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		//logger.debug("Key : " + key);
		if (personId != null) {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&PersonID=" + personId
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key=" + key;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,companyAddress,companyRevenueNumeric,companyRevenueRange,companyEmployeeRange,companyEmployeeCount&key="
					+ key + "&rpp=" + size + "&page=" + offset;
			return url;
		}
	}

	private ZoomInfoResponse getZoomInfoResponse(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent",
				"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
		// int responseCode= connection.getResponseCode();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputline;
		StringBuffer response = new StringBuffer();
		while ((inputline = bufferedReader.readLine()) != null) {
			response.append(inputline);
		}
		bufferedReader.close();
		ZoomInfoResponse zoomInfoResponse = getResponse(response, false);
		return zoomInfoResponse;
	}

	private ZoomInfoResponse getCompanyResponse(String companyName, String companyId) {
		ZoomInfoResponse companyZoomInfoResponse = new ZoomInfoResponse();

		try {
			StringBuilder queryString = createCompanyCriteria(companyName, companyId, companyId);

			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			//logger.debug("\n previewUrl3 : " + previewUrl);
			URL url = new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent",
					"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			companyZoomInfoResponse = getResponse(response, true);
			return companyZoomInfoResponse;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return companyZoomInfoResponse;

	}

	private ZoomInfoResponse getResponse(StringBuffer response, boolean isCompanySearch) throws IOException {
		int countCA = 0;
		if (!response.toString().contains("personId") && !isCompanySearch) {
			return null;
		}
		String ignore = "\"companyAddress\":\"\"";
		if (response.toString().contains(ignore)) {
			countCA = StringUtils.countMatches(response.toString(), ignore);
		}

		String rep = "\"companyAddress\":{}";
		// String ignorewith = "CurrentEmployment\":[";
		for (int i = 0; i < countCA; i++) {

			int xindex = response.indexOf(ignore);
			String sub = response.substring(xindex);
			String sub1 = sub.substring(0, 19);
			int startindex = response.indexOf(sub1);
			int endindex = startindex + sub1.length();
			response.replace(xindex, endindex, rep);
			// logger.debug(response.toString());
		}
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null) {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
			return zoomInfoResponse;
		} else {
			return null;
		}
	}

	private String getOrganizationId(ZoomInfoResponse zoomInfoResponseDetail) {
		String companyId = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					companyId = currentEmployment.getCompany().getCompanyId();
					break;
				}
			}
		}

		return companyId;
	}

	public String getTimeZone(Prospect prospect) {
		// List<OutboundNumber> outboundNumberList;
		// int areaCode = Integer.parseInt(findAreaCode(prospect.getPhone()));
		if (prospect != null && prospect.getStateCode() != null) {

			StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(prospect.getStateCode());
			return stateCallConfig.getTimezone();
		}
		String s = prospect.getFirstName() + "" + prospect.getLastName();
		//logger.debug("prospect State is null : " + s);
		// if(stateCallConfig==null){
		return null;
		// }

		// find a random number from the list

	}

	private String getOrganizationName(ZoomInfoResponse zoomInfoResponseDetail) {
		String orgName = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					orgName = currentEmployment.getCompany().getCompanyName();
					break;
				}
			}
		}

		return orgName;
	}

	private String getJobFunction(ZoomInfoResponse zoomInfoResponseDetail) {
		String jobFunction = null;
		try {
			if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
				for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
						.getCurrentEmployment()) {
					if (currentEmployment.getJobFunction() != null) {
						jobFunction = currentEmployment.getJobFunction();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			//logger.error("Department null :" + e);
			jobFunction = "_";
		}
		return jobFunction;
	}

	private String buildQueryString(Prospect prospect, boolean isDirectSearch,CampaignCriteria cc) {
		StringBuilder sb = new StringBuilder();
		try {
			if (cc != null && cc.getCountry() != null && !cc.getCountry().isEmpty()) {
				sb.append("Country=" + URLEncoder.encode(cc.getCountry(), XtaasConstants.UTF8));
			}
			if (cc != null && cc.getManagementLevel() != null && !cc.getManagementLevel().isEmpty()) {
				sb.append("&TitleSeniority=" + URLEncoder.encode(cc.getManagementLevel(), XtaasConstants.UTF8));
			}
			if (cc != null && cc.getDepartment() != null && !cc.getDepartment().isEmpty()) {
				sb.append("&TitleClassification=" + URLEncoder.encode(cc.getDepartment(), XtaasConstants.UTF8));
			}
			if (prospect != null && prospect.getCompany() != null && !prospect.getCompany().isEmpty()) {
				sb.append("&companyName=" + URLEncoder.encode(prospect.getCompany(), XtaasConstants.UTF8));
			}
			if (isDirectSearch)
				sb.append("&ContactRequirements=" + URLEncoder.encode("5", XtaasConstants.UTF8));// require phone,
			else
				sb.append("&ContactRequirements=" + URLEncoder.encode("2", XtaasConstants.UTF8));// require phone,

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	private String getPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");

		} else {
			phone = getCompanyPhone(personDetailResponse);
			phone = phone.replaceAll("\\-", "");
			return phone.replaceAll("\\s+", "");
		}
	}

	private String getCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		return phone;
	}

	private String getExtension(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			String phonepp = phone.substring(0, phone.indexOf("ext"));
			ext = phone.substring(phone.indexOf("ext"));
			ext = ext.replaceAll("[^[A-Za-z]*$]", "");

			// System.out.println(ext);
		}
		return ext;
	}

	private String getUSPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		if (phone != null) {
			// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			phone = getUSCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUSCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private String getUKPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;

		} else {
			phone = getUKCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUKCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;
		} else {
			return phone;
		}
	}

	private String getStateCode(PersonDetailResponse personDetailResponse) {
		if (personDetailResponse.getLocalAddress() != null) {
			String state = personDetailResponse.getLocalAddress().getState();
			String stateCode = getState(state);
			if (personDetailResponse.getLocalAddress().getCountry() != null
					&& !personDetailResponse.getLocalAddress().getCountry().isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository
						.findByCountryName(personDetailResponse.getLocalAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		} else if (personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
				.getCountry() != null) {
			String state = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
					.getState();
			String stateCode = getState(state);
			if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry() != null
					&& !personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry()
					.isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository.findByCountryName(personDetailResponse
						.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		}
		return null;
	}

	private String getUKStateCode(PersonDetailResponse personDetailResponse) {

		return "GL";
	}

	private String getState(String state) {
		EntityMapping toMapping = getEntityMapping();
		AttributeMapping attributeMapping = getReverseAttributeMapping("state", toMapping);
		List<ValueMapping> stateCode = attributeMapping.getValues();
		Iterator<ValueMapping> iterator = stateCode.iterator();
		while (iterator.hasNext()) {
			ValueMapping valueMapping = (ValueMapping) iterator.next();
			String to = valueMapping.getTo().replaceAll("%20", " ");
			if (to.equals(state)) {
				return valueMapping.getFrom();
			}
		}
		return null;

	}

	private EntityMapping getEntityMapping() {
		List<EntityMapping> toMappings = entityMappingRepository.findByFromSystemToSystem("xtaas",
				ContactListProviderType.ZOOMINFO.name());
		if (toMappings.size() > 0) {
			return toMappings.get(0);
		}
		return null;
	}

	private AttributeMapping getReverseAttributeMapping(String netProspexAttribute, EntityMapping toMapping) {
		for (AttributeMapping attributeMapping : toMapping.getAttributes()) {
			if (attributeMapping.getTo().equals(netProspexAttribute)) {
				return attributeMapping;
			}
		}
		return null;
	}

	private String getZoomOAuth() {
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (mCachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}

		Date currDate = new Date();
		long duration = currDate.getTime() - mCachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			mTokenKey = zt.getOauthToken();
			mCachedDate = zt.getCreatedDate();
		}
		return mTokenKey;
	}

	private String buildCompanyUrl(String queryString, /* int offset, int size, */String personId)
			throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = mTokenKey;

		//logger.debug("Key : " + key);
		if (personId != null) {
			/*
			 * String
			 * baseUrl="https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			 * String url = baseUrl + "pc="+partnerKey +"&CompanyID="+personId +
			 * "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key="
			 * +key; return url;
			 */
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companysic,companyType&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		}
	}

	private StringBuilder createCompanyCriteria(String companyName, String companyId, String campaignId) {

		StringBuilder queryString = new StringBuilder();

		try {

			if (companyId != null) {
				queryString.append("companyIds=" + URLEncoder.encode(companyId, XtaasConstants.UTF8));
			} else if (companyName != null) {
				queryString.append("companyName=" + URLEncoder.encode(companyName, XtaasConstants.UTF8));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryString;

	}

	private Map<String, Long> findCompanyData(String revRange, String empRange) {
		List<Long> revenueEmployesData = new ArrayList<Long>();
		Map<String, Long> companyData = new HashMap<String, Long>();
		Long minRevenue = new Long(0);
		Long maxRevenue = new Long(0);
		Long minEmployeeCount = new Long(0);
		Long maxEmployeeCount = new Long(0);
		if (revRange != null) {
			String[] revenueParts = revRange.split(" - ");

			if (revenueParts.length == 2) { // Ex response from Zoominfo:
				minRevenue = getRevenue(revenueParts[0]);
				maxRevenue = getRevenue(revenueParts[1]);

			} else if (revRange.contains("Over")) {
				String revenuePart = revRange;
				minRevenue = getRevenue(revenuePart);

			} else if (revRange.contains("Under")) {
				String revenuePart = revRange;
				maxRevenue = getRevenue(revenuePart);
			}
		} else {
			minRevenue = new Long(0);
			maxRevenue = new Long(0);
		}
		if (empRange != null) {
			String[] employeeCountParts = empRange.split(" - ");
			if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
				minEmployeeCount = Long.parseLong(employeeCountParts[0].trim().replace(",", ""));
				maxEmployeeCount = Long.parseLong(employeeCountParts[1].trim().replace(",", ""));

			} else if (empRange.contains("Over")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				minEmployeeCount = Long.parseLong(employeeCountPart);

			} else if (empRange.contains("Under")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				maxEmployeeCount = Long.parseLong(employeeCountPart);
			}
		} else {
			minEmployeeCount = new Long(0);
			maxEmployeeCount = new Long(0);
		}
		revenueEmployesData.add(minRevenue);
		revenueEmployesData.add(maxRevenue);
		revenueEmployesData.add(minEmployeeCount);
		revenueEmployesData.add(maxEmployeeCount);

		if (minRevenue > 0 && maxRevenue > 0) {
			if (minRevenue > maxRevenue) {
				companyData.put("minRevenue", maxRevenue);
				companyData.put("maxRevenue", minRevenue);
			} else {
				companyData.put("minRevenue", minRevenue);
				companyData.put("maxRevenue", maxRevenue);
			}
		} else {
			companyData.put("minRevenue", minRevenue);
			companyData.put("maxRevenue", maxRevenue);
		}

		companyData.put("minRevenue", minRevenue);
		companyData.put("maxRevenue", maxRevenue);
		companyData.put("minEmployeeCount", minEmployeeCount);
		companyData.put("maxEmployeeCount", maxEmployeeCount);
		return companyData;
	}

	private Long getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long y = new Double(value * million).longValue();
			return new Double(value * million).longValue();

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long x = new Double(value * billion).longValue();
			return new Double(value * billion).longValue();
		}
		return new Long(0);
	}

	///////////////////////////////////////////////////

	private Prospect getProspectDetails(Contacts personRecord){
		Prospect prospectInfo = null;
		if (personRecord != null) {
			String contactId =  personRecord.getContactId();
			prospectInfo = getPersonDetail(manualPersonDetailUrl, contactId);
			Companies cresponse = getManualCompanyDetail(personRecord.getCompanyId());
			if (cresponse!=null) {
				List<CompanyMaster> cmList = companyMasterRepository.findCompanyMasterListByCompanyId(personRecord.getCompanyId());
				if(cmList ==null || cmList.size()==0) {
					CompanyMaster cm = new CompanyMaster();
					CompanyAddress ca = new CompanyAddress();
					ca.setCity(cresponse.getCity());
					ca.setCountry(cresponse.getCountry());
					ca.setState(cresponse.getState());
					ca.setStreet(cresponse.getStreet());
					ca.setZip(cresponse.getZip());
					cm.setCompanyAddress(ca);
					cm.setCompanyDescription(cresponse.getCompanyBlogProfile());
					cm.setCompanyDetailXmlUrl(cresponse.getCompanyFacebookProfile());
					// cm.setCompanyEmployeeCount(cresponse.getEmployees());
					cm.setCompanyEmployeeCountRange(cresponse.getEmployees());
					cm.setCompanyId(cresponse.getCompanyId());
					List<String> indus = new ArrayList<String>(1);
					indus.add(cresponse.getIndustry());
					cm.setCompanyIndustry(indus);
					if(cresponse.getNaics()!=null) {
						cm.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
					}
					cm.setCompanyName(cresponse.getName());
					//cm.setCompanyProductsAndServices(cresponse.get);
					// cm.setCompanyRanking(cresponse.getFortuneRanking());
					//cm.setCompanyRevenueIn000s(cresponse.getRevenueCurrency());
					cm.setCompanyRevenueRange(cresponse.getRevenue());
					if(cresponse.getSic()!=null) {
						cm.setCompanySIC(Arrays.asList(cresponse.getSic().split(",")));
					}
					if(cresponse.getTickers()!=null) {
						StringBuilder tickersb = new StringBuilder();

						for(Tickers ticker : cresponse.getTickers()) {
							tickersb.append(ticker.getTickerName());
						}
						cm.setCompanyTicker(tickersb.toString());
					}
					// cm.setCompanyTopLevelIndustry(companyTopLevelIndustry);
					cm.setCompanyType(cresponse.getCompanyType());
					if(cresponse.getWebsites()!=null) {
						cm.setCompanyWebsite(cresponse.getWebsites().get(0));

					}
					//cm.setDefunct(cresponse);
					cm.setEnrichSource("INSIDEVIEW");
					// cm.setHashtags(hashtags);
					//cm.setNumOfCompany(cresponse.get);
					cm.setPhone(cresponse.getPhone());
					//cm.setSimilarCompanies(similarCompanies);
					cm.setZoomCompanyUrl(cresponse.getCompanyTwitterProfile());
					companyMasterRepository.save(cm);
				}
				prospectInfo.setCity(cresponse.getCity());
				prospectInfo.setCountry(cresponse.getCountry());
				prospectInfo.setStateCode(cresponse.getState());
				prospectInfo.setAddressLine1(cresponse.getStreet());
				prospectInfo.setZipCode(cresponse.getZip());
					 	/*StateCallConfig scc = getStateCodeAndTimeZone(cresponse.getState(),cresponse.getCountry());
						if (scc != null) {
							prospectInfo.setTimeZone(scc.getTimezone());
							prospectInfo.setStateCode(scc.getStateCode());
						}*/
				prospectInfo.setIndustry(cresponse.getIndustry());
				prospectInfo.setSource("INSIDEVIEW");
				prospectInfo.setDataSource("Xtaas Data Source");
				prospectInfo.setSourceType("INTERNAL");
				prospectInfo.setCustomAttribute("maxRevenue", cresponse.getRevenue());
				prospectInfo.setCustomAttribute("maxEmployeeCount", cresponse.getEmployees());
				if (cresponse.getWebsites().size() > 0) {
					prospectInfo.setCustomAttribute("domain", cresponse.getWebsites().get(0));
				}
				if(cresponse.getNaics()!=null) {
					prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
				}
				if(cresponse.getSic()!=null) {
					prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getSic().split(",")));
				}

			}
		}
		return prospectInfo;
	}

	public Prospect getPersonDetail(String url, String urlParameters) {
		// {"contactId":157117130,"firstName":"Steve","lastName":"T","companyId":3645001,"companyName":"1
		// EDI Source, Inc.","phone":"+1 440 519
		// 7800","linkedinHandle":"https://www.linkedin.com/in/stevectuttle","active":true,"jobLevels":["Manager"],"jobFunctions":["IT
		// &
		// IS"],"peopleId":"72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-","fullName":"Steve
		// T","confidenceScore":45.0,"corporatePhone":"+1 440 519 7800","sources":["Web
		// References"],"titles":["IT Manager"]}
		Prospect prospectInfo = null;
		try {
			// StringBuilder params = new StringBuilder();
			StringBuilder response = getIVResponse(url, urlParameters);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				DetailedContact detailedContact = mapper.readValue(response.toString(), DetailedContact.class);
				prospectInfo = mapToProspectObject(detailedContact);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//logger.error(e.getMessage());
		}
		return prospectInfo;
	}

	private Prospect mapToProspectObject(DetailedContact detailedContact) {
		Prospect prospectInfo = new Prospect();
		prospectInfo.setSource("INSIDEVIEW");
		prospectInfo.setSourceType("INTERNAL");
		prospectInfo.setDataSource("Xtaas Data Source");
		prospectInfo.setSourceId(detailedContact.getContactId().toString());
		prospectInfo.setFirstName(detailedContact.getFirstName());
		prospectInfo.setLastName(detailedContact.getLastName());
		prospectInfo.setSourceCompanyId(Long.toString(detailedContact.getCompanyId()));
		prospectInfo.setCompany(detailedContact.getCompanyName());
		prospectInfo.setPhone(getFormattedPhone(detailedContact.getPhone()));
		prospectInfo.setCustomAttribute("linkedinUrl", detailedContact.getLinkedinHandle());
		prospectInfo.setZoomPersonUrl(detailedContact.getLinkedinHandle());
		prospectInfo.setCompanyPhone(getFormattedPhone(detailedContact.getCorporatePhone()));
		prospectInfo.setTitle(detailedContact.getTitles().get(0));
		prospectInfo.setConfidenceScore(detailedContact.getConfidenceScore().toString());
		if (detailedContact.getJobFunctions() != null && detailedContact.getJobFunctions().size() > 0) {
			prospectInfo.setDepartment(detailedContact.getJobFunctions().get(0));
		}
		if (detailedContact.getJobLevels() != null && detailedContact.getJobLevels().size() > 0) {
			prospectInfo.setManagementLevel(getStdMgmtLevl(detailedContact.getJobLevels().get(0)));
		}
		if (detailedContact.getCompanyDetails() != null) {
			prospectInfo.setCustomAttribute("maxRevenue", detailedContact.getCompanyDetails().getRevenue());
			prospectInfo.setCustomAttribute("maxEmployeeCount", detailedContact.getCompanyDetails().getEmployees());
			if (detailedContact.getCompanyDetails().getWebsites().size() > 0) {
				prospectInfo.setCustomAttribute("domain", detailedContact.getCompanyDetails().getWebsites().get(0));
			}
		}
		if (detailedContact.getEmail() != null && detailedContact.getEmailValidationStatus() != null
				&& detailedContact.getEmailValidationStatus().equalsIgnoreCase("ValidEmail")) {
			prospectInfo.setEmail(detailedContact.getEmail());
		} else if (detailedContact.getEmails() != null && detailedContact.getEmails().size() > 0) {
			for (Email emailObj : detailedContact.getEmails()) {
				if (emailObj.getValidationStatus().equalsIgnoreCase("ValidEmail")) {
					prospectInfo.setEmail(emailObj.getEmail());
					break;
				}
			}
			if (prospectInfo.getEmail() == null || prospectInfo.getEmail().isEmpty()) {
				prospectInfo.setEmail(detailedContact.getEmails().get(0).getEmail());
			}
		}
		return prospectInfo;
	}

	private String getFormattedPhone(String phone) {
		phone = phone.replaceAll("[^\\d]", "");
		phone = "+" + phone.trim();
		return phone;
	}

	private String getStdMgmtLevl(String mgmtLevel) {
		String ivmgmtLevel = "C-Level";
		if(mgmtLevel.equalsIgnoreCase("C-Level") ||mgmtLevel.equalsIgnoreCase("C Level")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Senior Executive")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("VP") || mgmtLevel.equalsIgnoreCase("Vice President")) {
			ivmgmtLevel = "VP-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Director")) {
			ivmgmtLevel = "DIRECTOR";
		}else if(mgmtLevel.equalsIgnoreCase("Manager")) {
			ivmgmtLevel = "MANAGER";
		}else if(mgmtLevel.equalsIgnoreCase("Board Member")) {
			ivmgmtLevel = "Board Members";
		}else if(mgmtLevel.equalsIgnoreCase("Other")) {
			ivmgmtLevel = "Non Management";
		}
		return ivmgmtLevel;
	}

	public Companies getManualCompanyDetail(String params) {
		//StringBuilder params = new StringBuilder();
		Companies companies = null;

		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getIVResponse(manualCompanyUrl, params);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), Companies.class);
				//System.out.println(companies);
			}
		} catch (Exception e) {
			//logger.error(e.getMessage());
		}

		return companies;
	}

	public StringBuilder getIVResponse(String url, String params) {
		StringBuilder response = new StringBuilder();

		try {
			if (url.contains("{newContactId}")) {
				url = url.replace("{newContactId}", params);
				url = url + "?retrieveCompanyDetails=true";
			} else if (url.contains("{newCompanyId}")) {
				url = url.replace("{newCompanyId}", params);
			} else if (url.contains("{companyId}")) {
				url = url.replace("{companyId}", params);
			}else if (url.contains("{contactId}")) {
				url = url.replace("{contactId}", params);
			}else {
				url = url + "?" + params;
			}
			HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();
			// add request header
			httpClient.setRequestMethod("GET");
			httpClient.setRequestProperty("accessToken", insideViewToken);
			httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
			httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			httpClient.setRequestProperty("Accept", "application/json");
			int responseCode = httpClient.getResponseCode();
			try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
				String line;

				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public TreeMap<Integer, String> getSimilarMap(Campaign campaign){
		TreeMap<Integer, String> sortedMap = new TreeMap<>();
		List<KeyValuePair<String, Integer>> similarPriorityList = new ArrayList<KeyValuePair<String,Integer>>();
		//check if you have priority set at campaign level
		similarPriorityList = campaign.getSimilarDataSourcePriority();
		if(similarPriorityList==null || similarPriorityList.isEmpty()) {
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			CampaignMetaData cmd = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
			if(organization.getCampaignDefaultSetting()!=null && organization.getCampaignDefaultSetting().getSimilarDataSourcePriority()!=null) {
				similarPriorityList = organization.getCampaignDefaultSetting().getSimilarDataSourcePriority();
			}else if(cmd!=null && cmd.getSimilarDataSourcePriority()!=null && !cmd.getSimilarDataSourcePriority().isEmpty()) {
				similarPriorityList = cmd.getSimilarDataSourcePriority();
			}
		}
		if(similarPriorityList!=null && similarPriorityList.size()>0) {
			for (KeyValuePair<String, Integer> keyvaluepair : similarPriorityList) {
				sortedMap.put(keyvaluepair.getValue(), keyvaluepair.getKey());
			}
		}
		return sortedMap;
	}

	@Override
	public Prospect searchProspectFromGlobalContact(String sourceId) {
		try {
			if(StringUtils.isNotBlank(sourceId)){
				Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
				List<GlobalContact> globalContactList = globalContactRepository.findUniqueRecordBySourceId(sourceId,pageable);
				if(globalContactList != null && globalContactList.size() > 0){
					return convertGlobalContactToProspect(globalContactList.get(0));
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private Prospect convertGlobalContactToProspect(GlobalContact globalContact) {
		Prospect prospect = new Prospect();
		prospect.setLastUpdatedDate(globalContact.getLastUpdatedDate());
		prospect.setEu(globalContact.isEu());

		if(CollectionUtils.isNotEmpty(globalContact.getTopLevelIndustries())){
			prospect.setTopLevelIndustries(globalContact.getTopLevelIndustries());
		}
		if (StringUtils.isNotBlank(globalContact.getSourceType())) {
			prospect.setSourceType(globalContact.getSourceType());
		}
		if (StringUtils.isNotBlank(globalContact.getExtension())) {
			prospect.setExtension(globalContact.getExtension());
		}
		if (StringUtils.isNotBlank(globalContact.getSource())) {
			prospect.setSource(globalContact.getSource());
		}
		if (StringUtils.isNotBlank(globalContact.getSourceId())) {
			prospect.setSourceId(globalContact.getSourceId());
		}
		if (StringUtils.isNotBlank(globalContact.getFirstName())) {
			prospect.setFirstName(globalContact.getFirstName());
		}
		if (StringUtils.isNotBlank(globalContact.getLastName())) {
			prospect.setLastName(globalContact.getLastName());
		}
		if (StringUtils.isNotBlank(globalContact.getOrganizationName())) {
			prospect.setCompany(globalContact.getOrganizationName());
		}
		if (StringUtils.isNotBlank(globalContact.getTitle())) {
			prospect.setTitle(globalContact.getTitle());
		}
		if (StringUtils.isNotBlank(globalContact.getDepartment())) {
			prospect.setDepartment(globalContact.getDepartment());
		}
		if (StringUtils.isNotBlank(globalContact.getPhone())) {
			prospect.setPhone(globalContact.getPhone());
		}
		if (StringUtils.isNotBlank(globalContact.getCompanyPhone())) {
			prospect.setCompanyPhone(globalContact.getCompanyPhone());
		}
		if (StringUtils.isNotBlank(globalContact.getIndustry())) {
			prospect.setIndustry(globalContact.getIndustry());
		}
		if (globalContact.getIndustries() != null && globalContact.getIndustries().size() > 0) {
			prospect.setIndustryList(globalContact.getIndustries());
		}
		if (StringUtils.isNotBlank(globalContact.getEmail())) {
			prospect.setEmail(globalContact.getEmail());
		}
		if (StringUtils.isNotBlank(globalContact.getCity())) {
			prospect.setCity(globalContact.getCity());
		}
		if (StringUtils.isNotBlank(globalContact.getAddressLine1())) {
			prospect.setAddressLine1(globalContact.getAddressLine1());
		}
		if (StringUtils.isNotBlank(globalContact.getAddressLine2())) {
			prospect.setAddressLine2(globalContact.getAddressLine2());
		}
		if (StringUtils.isNotBlank(globalContact.getPostalCode())) {
			prospect.setZipCode(globalContact.getPostalCode());
		}
		if (StringUtils.isNotBlank(globalContact.getCountry())) {
			prospect.setCountry(globalContact.getCountry());
		}
		if (StringUtils.isNotBlank(globalContact.getTimezone())) {
			prospect.setTimeZone(globalContact.getTimezone());
		}
		if (StringUtils.isNotBlank(globalContact.getExtension())) {
			prospect.setExtension(globalContact.getExtension());
		}
		if (StringUtils.isNotBlank(globalContact.getStateCode())) {
			prospect.setStateCode(globalContact.getStateCode());
		}
		prospect.setDirectPhone(globalContact.isDirectPhone());
		if (StringUtils.isNotBlank(globalContact.getSourceCompanyId())) {
			prospect.setSourceCompanyId(globalContact.getSourceCompanyId());
		}
		if (StringUtils.isNotBlank(globalContact.getManagementLevel())) {
			prospect.setManagementLevel(globalContact.getManagementLevel());
		}
		if (StringUtils.isNotBlank(globalContact.getZoomCompanyUrl())) {
			prospect.setZoomCompanyUrl(globalContact.getZoomCompanyUrl());
		}
		if (StringUtils.isNotBlank(globalContact.getZoomPersonUrl())) {
			prospect.setZoomPersonUrl(globalContact.getZoomPersonUrl());
		}
		if (globalContact.getCompanySIC() != null && globalContact.getCompanySIC().size() > 0) {
			prospect.setCompanySIC(globalContact.getCompanySIC());
		}
		if (globalContact.getCompanyNAICS() != null && globalContact.getCompanyNAICS() .size() > 0) {
			prospect.setCompanyNAICS(globalContact.getCompanyNAICS());
		}

		prospect.setDataSource("Xtaas Data Source");
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		if (globalContact.getMinRevenue() != null && globalContact.getMinRevenue() > 0) {
			customAttributes.put("minRevenue", globalContact.getMinRevenue());
		}
		if (globalContact.getMaxRevenue() != null && globalContact.getMaxRevenue() > 0) {
			customAttributes.put("maxRevenue", globalContact.getMaxRevenue());
		}
		if (globalContact.getMinEmployeeCount() != null && globalContact.getMinEmployeeCount() > 0) {
			customAttributes.put("minEmployeeCount", globalContact.getMinEmployeeCount());
		}
		if (globalContact.getMaxEmployeeCount() != null && globalContact.getMaxEmployeeCount() > 0) {
			customAttributes.put("maxEmployeeCount", globalContact.getMaxEmployeeCount());
		}
		if (globalContact.getRevCount() != null && globalContact.getRevCount() > 0) {
			customAttributes.put("revCount", globalContact.getRevCount());
		}
		if (globalContact.getRevCountIn000s() != null && globalContact.getRevCountIn000s() > 0) {
			customAttributes.put("revCountIn000s", globalContact.getRevCountIn000s());
		}
		if (globalContact.getEmpCount() != null && globalContact.getEmpCount() > 0) {
			customAttributes.put("empCount", globalContact.getEmpCount());
		}
		if (StringUtils.isNotBlank(globalContact.getDomain())) {
			customAttributes.put("domain", globalContact.getDomain());
		}

		if(customAttributes != null && customAttributes.size() > 0) {
			prospect.setCustomAttributes(customAttributes);
		}
		return prospect;
	}
}
