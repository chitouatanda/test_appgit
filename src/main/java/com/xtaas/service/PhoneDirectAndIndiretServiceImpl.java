package com.xtaas.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.PhoneDirectAndIndirect;
import com.xtaas.db.repository.PhoneDirectAndIndirectRepository;
import com.xtaas.infra.zoominfo.service.PhoneTypeList;


@Service
public class PhoneDirectAndIndiretServiceImpl {

	@Autowired
	PhoneDirectAndIndirectRepository phoneDirectAndIndirectRepository;
	
	
	public String tagDirectIndirectPhone(String phone) {
		if(StringUtils.isNotBlank(phone)){
			try{
				if(phone.startsWith("+")) {
					//dont do anything
				}else {
					phone= "+1"+phone;
				}
				Map<String,String> pdiMap = new HashMap<String,String>();
				String ph=phone.replaceAll("[^A-Za-z0-9]", "");
				List<PhoneDirectAndIndirect> pdiList = phoneDirectAndIndirectRepository.findByPhoneNumber(ph);
				if(pdiList!=null && pdiList.size()>0) {
					for(PhoneDirectAndIndirect pd : pdiList){
						//pdiMap.put(pd.getLocal_format(), pd.getLine_type());
						return pd.getLine_type();
					}
				}else {
					PhoneTypeList ptl = null;
					//CountryNew country = countryNewRepository.findByCountry(countryCode);
					//if(country!=null && !country.getCountryCode().isEmpty()) {

					ptl = sendGet(ph);
					if(ptl !=null && ptl.getLine_type()!=null){
						PhoneDirectAndIndirect pdi = new PhoneDirectAndIndirect();
						pdi.setCarrier(ptl.getCarrier());
						pdi.setCountry_code(ptl.getCountry_code());
						pdi.setCountry_name(ptl.getCountry_name());
						pdi.setCountry_prefix(ptl.getCountry_prefix());
						pdi.setInternationl_format(ptl.getInternationl_format());
						pdi.setLine_type(ptl.getLine_type());
						pdi.setLocal_format(ptl.getLocal_format());
						pdi.setLocation(ptl.getLocation());
						pdi.setNumber(ptl.getNumber());
						pdi.setValid(ptl.isValid());
						phoneDirectAndIndirectRepository.save(pdi);
						return ptl.getLine_type();
					}
					//}

				}

			}catch(Exception e){
				e.printStackTrace();
			}
		}

		return "null";

	}
	
	
	
	private PhoneTypeList sendGet(String phone) throws Exception {

		String url = "http://apilayer.net/api/validate?access_key=eb7239e08e9805ad56c5668f503be3b6&number="+phone;

		HttpURLConnection httpClient =
		(HttpURLConnection) new URL(url).openConnection();

		// optional default is GET
		httpClient.setRequestMethod("GET");

		//add request header
		httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = httpClient.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);

		try (BufferedReader in = new BufferedReader(
		new InputStreamReader(httpClient.getInputStream()))) {

		StringBuilder response = new StringBuilder();
		String line;

		while ((line = in.readLine()) != null) {
		response.append(line);
		}

		//print result
		//System.out.println(response.toString());
		
		PhoneTypeList zoomInfoResponse = null;
	        if (response != null) {
	            ObjectMapper mapper= new ObjectMapper().setVisibility(JsonMethod.FIELD,Visibility.ANY);
	            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
	            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
	            zoomInfoResponse=mapper.readValue(response.toString(), PhoneTypeList.class);
	           // return zoomInfoResponse;
	        }
	        return zoomInfoResponse;

		}
	//	return null;
		}
}
