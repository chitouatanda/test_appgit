/**
 *
 */
package com.xtaas.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This service returns the environment specific values loaded using Spring
 * profiles. Spring active profile is specified as JVM argument as
 * -Dspring.profiles.active=local|dev|staging|prod Each environment specific
 * properties files are located in classpath at
 * properties/application-{env}.properties
 *
 * @author djain
 */
@Service(value = "applicationEnvironmentPropertyService")
public class ApplicationEnvironmentPropertyServiceImpl implements ApplicationEnvironmentPropertyService {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationEnvironmentPropertyServiceImpl.class);

	@Value("${CLOUDAMQP_URL}")
	private String amqpUrl;

	@Value("${PUSHER_APP_Id}")
	private String pusherApplicationId;

	@Value("${PUSHER_APP_KEY}")
	private String pusherApplicationKey;

	@Value("${PUSHER_APP_SECRET_KEY}")
	private String pusherApplicationSecret;

	@Value("${SERVER_BASE_URL}")
	private String serverBaseUrl;

	@Value("${DIALER_URL}")
	private String dialerUrl;

	@Value("${TWIML_DIAL_APP_SID}")
	private String twimlDialAppSID;

	@Value("${TWIML_WHISPER_APP_SID}")
	private String twimlWhisperAppSID;

	@Value("${SALESFORCE_APP_CONSUMER_KEY}")
	private String salesforceAppConsumerKey;

	@Value("${SALESFORCE_APP_CONSUMER_SECRET}")
	private String salesforceAppConsumerSecret;

	@Value("${TWIML_DIAL_XML_URL}")
	private String twimlDialXmlUrl;

	@Value("${BSS_SERVER_BASE_URL}")
	private String bssServerBaseUrl;

	@Value("${TWILIO_ACCOUNT_SID}")
	private String twilioAccountSid;

	@Value("${TWILIO_AUTH_TOKEN}")
	private String twilioAuthToken;

	@Value("${PLIVO_AUTH_ID}")
	private String plivoAuthId;

	@Value("${PLIVO_AUTH_TOKEN}")
	private String plivoAuthToken;

	@Value("${PLIVO_APP_ID}")
	private String plivoAppId;

	@Value("${THINQ_DOMAIN_URL}")
	private String thinQDomainUrl;

	@Value("${THINQ_ID}")
	private String thinQId;

	@Value("${THINQ_TOKEN}")
	private String thinQToken;

	@Value("${TELNYX_DOMAIN_URL}")
	private String telnyxDomainUrl;

	@Value("${TELNYX_TOKEN}")
	private String telnyxToken;

	@Value("${GEOCODE_API_URL}")
	private String geoCodeApiUrl;

	@Value("AWS_ACCESS_KEY_ID")
	private String awsAccessKeyId;

	@Value("AWS_SECRET_ACCESS_KEY")
	private String awsSecretKey;

	@Value("${VIVA_ACCOUNTID}")
	private String vivaAccountId;

	@Value("${VIVA_PASSCODE}")
	private String vivaPasscode;

	@Value("${VIVA_DOMAIN}")
	private String vivaDomain;

	@Value("${TElNYX_CONNECTION_ID}")
	private String telnyxConnectionId;
	
	@Value("${LEADIQ_APIKEY}")
	private String leadIqAPIKey;
	
	@Value("${LEADIQ_URL}")
	private String leadIqUrl;
	
	@Value("${VIVA_TElNYX_ACCOUNTID}")
	private String vivaTelnyxAccountId;

	@Value("${VIVA_TElNYX_PASSCODE}")
	private String vivaTelnyxPasscode;

	@Value("${VIVA_TElNYX_DOMAIN}")
	private String vivaTelnyxDomain;
	
	@Value("${SAMESPACE_USERNAME}")
	private String samespaceUsername;

	@Value("${SAMESPACE_PASSWORD}")
	private String samespacePassword;

	@Value("${SAMESPACE_DOMAIN}")
	private String samespaceDomain;

	@Value("${SIGNALWIRE_DOMAINAPP}")
	private String signalwireDomainApp;
	
	@Value("${SIGNALWIRE_PROJECTID}")
	private String signalwireProjectId;

	@Value("${SIGNALWIRE_AUTHTOKEN}")
	private String signalwireAuthToken;

	@Value("${SIGNALWIRE_TELNYXTOKEN}")
	private String signalwireTelnyxToken;

	@Value("${PLIVO_SIP_DOMAIN}")
	private String plivoSIPDomain;

	@Value("${PLIVO_SIP_USERNAME}")
	private String plivoSIPUsername;

	@Value("${PLIVO_SIP_PASSWORD}")
	private String plivoSIPPassword;

	@Value("${SIGNALWIRE_VIVA_SIP_DOMAIN}")
	private String signalwireVivaSIPDomain;

	@Value("${SIGNALWIRE_VIVA_SIP_ACCOUNTID}")
	private String signalwireVivaSIPAccountId;

	@Value("${SIGNALWIRE_VIVA_PASSWORD}")
	private String signalwireVivaSIPPassword;
	
	@Value("${SIGNALWIRE_THINQ_SIP_TOKEN}")
	private String signalwireThinQSipToken;

	public ApplicationEnvironmentPropertyServiceImpl() {
		String activeProfile = (System.getenv("spring_profiles_active") != null)
				? System.getenv("spring_profiles_active")
				: System.getProperty("spring.profiles.active");
		logger.info("***** LOADING APPLICATION ENVIRONMENT PROFILE: " + activeProfile + " *****");
	}

	@Override
	public String getAMQPUrl() {
		String uri = System.getenv("CLOUDAMQP_URL");
		if (uri == null) {
			uri = amqpUrl;
		}
		logger.debug("getAMQPUrl() : AMQP URL: " + uri);
		return uri;
	}

	/**
	 * @return the pusherApplicationId
	 */
	public String getPusherApplicationId() {
		return pusherApplicationId;
	}

	/**
	 * @return the pusherApplicationKey
	 */
	public String getPusherApplicationKey() {
		return pusherApplicationKey;
	}

	/**
	 * @return the pusherApplicationSecret
	 */
	public String getPusherApplicationSecret() {
		return pusherApplicationSecret;
	}

	@Override
	public String getServerBaseUrl() {
		return serverBaseUrl;
	}

	@Override
	public String getDialerUrl() {
		return dialerUrl;
	}

	@Override
	public String getTwimlDialAppSID() {
		return twimlDialAppSID;
	}

	@Override
	public String getTwimlWhisperAppSID() {
		return twimlWhisperAppSID;
	}

	@Override
	public String getSalesforceAppConsumerKey() {
		return salesforceAppConsumerKey;
	}

	@Override
	public String getSalesforceAppConsumerSecret() {
		return salesforceAppConsumerSecret;
	}

	@Override
	public String getTwimlDialXmlUrl() {
		return twimlDialXmlUrl;
	}

	@Override
	public String getBssServerBaseUrl() {
		return bssServerBaseUrl;
	}

	@Override
	public String getTwilioAccountSid() {
		return twilioAccountSid;
	}

	@Override
	public String getTwilioAuthToken() {
		return twilioAuthToken;
	}

	@Override
	public String getPlivoAuthId() {
		return plivoAuthId;
	}

	@Override
	public String getPlivoAuthToken() {
		return plivoAuthToken;
	}

	@Override
	public String getPlivoAppId() {
		return plivoAppId;
	}

	@Override
	public String getThinQDomainUrl() {
		return thinQDomainUrl;
	}

	@Override
	public String getThinQId() {
		return thinQId;
	}

	@Override
	public String getThinQToken() {
		return thinQToken;
	}

	@Override
	public String getTelnyxDomainUrl() {
		return telnyxDomainUrl;
	}

	@Override
	public String getTelnyxToken() {
		return telnyxToken;
	}

	@Override
	public String getGeoCodeApiUrl() {
		return geoCodeApiUrl;
	}

	@Override
	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}

	@Override
	public String getAwsSecretKey() {
		return awsSecretKey;
	}

	@Override
	public String getVivaAccountId() {
		return vivaAccountId;
	}

	@Override
	public String getVivaPasscode() {
		return vivaPasscode;
	}

	@Override
	public String getVivaDomain() {
		return vivaDomain;
	}

	@Override
	public String getTelnyxConnectionId() {
		return telnyxConnectionId;
	}
	
	@Override
	public String getLeadIQAPIKey() {
		return leadIqAPIKey;
	}
	
	@Override
	public String getLeadIQUrl() {
		return leadIqUrl;
	}
	
	@Override
	public String getVivaTelnyxAccountId() {
		return vivaTelnyxAccountId;
	}

	@Override
	public String getVivaTelnyxPasscode() {
		return vivaTelnyxPasscode;
	}

	@Override
	public String getVivaTelnyxDomain() {
		return vivaTelnyxDomain;
	}
	
	@Override
	public String getSamespaceUsername() {
		return samespaceUsername;
	}
	
	@Override
	public String getSamespacePassword() {
		return samespacePassword;
	}
	
	@Override
	public String getSamespaceDomain() {
		return samespaceDomain;
	}

	@Override
	public String getSignalwireDomainApp() {
		return signalwireDomainApp;
	}

	@Override
	public String getSignalwireProjectId() {
		return signalwireProjectId;
	}

	@Override
	public String getSignalwireAuthToken() {
		return signalwireAuthToken;
	}

	@Override
	public String getSignalwireTelnyxToken() {
		return signalwireTelnyxToken;
	}

	@Override
	public String getPlivoSIPDomain() {
		return plivoSIPDomain;
	}

	@Override
	public String getPlivoSIPUsername() {
		return plivoSIPUsername;
	}

	@Override
	public String getPlivoSIPPassword() {
		return plivoSIPPassword;
	}

	@Override
	public String getSignalwireVivaSIPDomain() {
		return signalwireVivaSIPDomain;
	}

	@Override
	public String getSignalwireVivaSIPAccountId() {
		return signalwireVivaSIPAccountId;
	}

	@Override
	public String getSignalwireVivaSIPPassword() {
		return signalwireVivaSIPPassword;
	}
	@Override
	public String getSignalwireThinQSipToken() {
		return signalwireThinQSipToken;
	}

}
