package com.xtaas.service;

import java.util.Map;

import com.xtaas.web.dto.TwilioCallLogFilterDTO;
import com.xtaas.web.dto.TwilioNumberBuyDTO;

public interface TwilioCallLogAndDataBuyService {

	public void generateCallLog(TwilioCallLogFilterDTO twilioCallLogFilterDTO, boolean showPrice);

	public void purchaseNumbers(TwilioNumberBuyDTO filterDTO);

	public Map<String, Integer> getCallStats(TwilioCallLogFilterDTO filterDTO);

}
