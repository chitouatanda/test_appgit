package com.xtaas.service;

import java.text.ParseException;
import java.util.List;

import com.xtaas.db.entity.EmailBounces;
import com.xtaas.web.dto.WebHookEventDTO;

public interface EmailBouncesService {

	// public void handleBounceNotification(JSONObject json) throws ParseException;
	public void handleBounceNotification(List<WebHookEventDTO> webHookEvents);

	public EmailBounces getEmailStatus(String prospectCallId);

}
