package com.xtaas.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.monitorjbl.xlsx.StreamingReader;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;

@Service
public class AbmListServiceImpl implements AbmListService {

	@Autowired
	private AbmListNewRepository abmListRepositoryNew;
	
	//@Autowired
	//private AbmListRepository abmListRepository;

	@Autowired
	CampaignRepository campaignRepository;

	@Autowired
	DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	ZoomInfoContactListProviderNewImpl zoomInfoContactListProviderNewImpl;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	private String bucketName = "xtaasreports";
	
	private static final Logger logger = LoggerFactory.getLogger(AbmListServiceImpl.class);

	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	private static final String GET_COMPANY_ID = "GET_COMPANY_ID";

	private String abmType;

	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;

	private List<String> errorList = new ArrayList<String>();
	
	private List<String> errorListDomain;
	
	private HashMap<Integer, List<String>> errorMap = new HashMap<>();
	
	Workbook book;
	
	Workbook bookCopy;
	
	private String campId;
	
	private File myFile;

	@Override
	public void downloadABMListTemplate(HttpServletRequest request, HttpServletResponse response, String campaignId)
			throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		this.sheet = sheet;
		int rowCount = 0;
		String fileName = null;
		Row headerRow = this.sheet.createRow(rowCount);
		writeHeader(headerRow);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		//List<AbmListNew> abmListFromDB = abmListRepositoryNew.findABMListNewByCampaignIdAndStatus(campaignId);
		if (campaign != null) {
			//if (abmListFromDB.getCompanyList() != null && !abmListFromDB.getCompanyList().isEmpty()) {
				writeCompaniesToExcel(campaignId);
			//}
		}
		// fileName = (campaign != null) ? campaign.getName() + fileSuppressiontype
		fileName = (campaign != null) ? campaign.getName() + "_abm_list.xlsx": "new_abm_list.xlsx";
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	private void writeHeader(Row headerRow) {
		Cell domainCell = headerRow.createCell(0);
		domainCell.setCellValue("Domain");
		Cell companyIdCell = headerRow.createCell(1);
		companyIdCell.setCellValue("Company ID");
		Cell action = headerRow.createCell(2);
		action.setCellValue("Action");
		Cell date = headerRow.createCell(3);
		date.setCellValue("Created Date");
	}

	private void writeCompaniesToExcel(String campaignId) {
		int rowCount = 1;
		int companyCount = abmListRepositoryNew
					.countABMListNewByCampaignCompanyName(campaignId);
		
		int  batchsize = 0;
		if(companyCount>0) {
			batchsize = companyCount /5000;
			long modSize = companyCount % 5000;
			if(modSize > 0){
				batchsize = batchsize +1;
			}
		}
			
		if(batchsize > 0) {
			for(int i=0;i<batchsize;i++) {
				Pageable pageable =  PageRequest.of(i, 5000);
				List<AbmListNew> newABMList =abmListRepositoryNew.pageABMListNewByCampaignCompanyName(campaignId,pageable);
				for (AbmListNew data : newABMList) {
					Row dataRow = this.sheet.createRow(rowCount);
					Cell cell = dataRow.createCell(0);
					cell.setCellValue(data.getCompanyName());
					Cell cell1 = dataRow.createCell(1);
					cell1.setCellValue(data.getCompanyId());
					rowCount++;
				}
			}
		} 
	}

	@Override
	public String createCampaignABMList(String campaignId, MultipartFile file) throws IOException {

		if (!file.isEmpty()) {
			fileName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				myFile = convertMultiPartToFile(file);
				campId = campaignId;
				ABMFileUploader abmFileUploader = new ABMFileUploader();
				SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
				DelegatingSecurityContextExecutor suppressionFileUploadThreadExecutor = new DelegatingSecurityContextExecutor(
						delegateExecutor, SecurityContextHolder.getContext());
				suppressionFileUploadThreadExecutor.execute(abmFileUploader);
				logger.debug("AbmList File - UPLOADING-THREAD is started...");
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}
	class ABMFileUploader implements Runnable {

		@Override
		public void run() {
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
			Campaign campaign = campaignRepository.findOneById(campId);
				try {
					book = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(myFile);
					bookCopy = WorkbookFactory.create(myFile);
					sheet = book.getSheetAt(0);
					// int totalRowsCount = sheet.getPhysicalNumberOfRows();
					int totalRowsCount = sheet.getLastRowNum() + 1;
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(fileName + " file is empty");
						// throw new IllegalArgumentException("File Can not be Empty.");
						sendEmail(campId, "File Can not be Empty.",organization,loginUser);
					} else if (totalRowsCount <= 1) {// check if nothing found in file
						book.close();
						logger.error("No records found in file " + fileName);
						// throw new IllegalArgumentException("No Records Found in File.");
						sendEmail(campId, "No Records Found in File.",organization,loginUser);
					}
					String count;
					if (!validateExcelFileheader())
						return;
					count = validateExcelFileRecords(loginUser,campId,organization,campaign);
					book.close();
					return;
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					e.printStackTrace();
					SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campId,"ABM", "INPROCESS");
					if(supervisorStats != null) {
						supervisorStats.setStatus("ERROR");
						supervisorStats.setError(ExceptionUtils.getStackTrace(e));
						supervisorStatsRepository.save(supervisorStats);
						PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						String notice = campaign.getName() + " : ABM list upload failed, please contact support";
						teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
					}
					return;
				} catch (Exception e) {
					e.printStackTrace();
					SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campId,"ABM", "INPROCESS");
					if(supervisorStats != null) {
						supervisorStats.setStatus("ERROR");
						supervisorStats.setError(ExceptionUtils.getStackTrace(e));
						supervisorStatsRepository.save(supervisorStats);
						PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
						String notice = campaign.getName() + " : ABM list upload failed, please contact support";
						teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
					}
				}
			}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		//final Row row = sheet.getRow(0);// get first sheet in file
		Iterator<Row> rowIterator = sheet.rowIterator();
		if (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			int colCounts = row.getPhysicalNumberOfCells();
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell == null || cell.getCellType() == CellType.BLANK) {
					logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
					return false;
				}
				if (cell.getCellType() != CellType.STRING) {
					logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
							+ " Header must contain only String values");
					return false;
				} else {
					headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
					// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
					fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
				}
			}
		}
		return true;
	}

	private String validateExcelFileRecords(Login loginUser,String campaignId,Organization organization,Campaign campaign) {
		int companyCount = 0;
		try {
			int rowsCount = sheet.getLastRowNum() + 1;
			if (rowsCount < 2) {
				logger.error("No records found in file " + fileName);
				errorList.add("No records found in file " + fileName);
				return "";
			}
//		final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
//		int colCounts = tempRow.getPhysicalNumberOfCells();
			companyCount = processRecords(loginUser, rowsCount, campaignId, organization);
			DataBuyQueue dbq = new DataBuyQueue();
			dbq.setCampaignId(campaignId);
			dbq.setStatus(DataBuyQueueStatus.QUEUED.toString());
			dbq.setSearchCompany(true);
			dbq.setJobRequestTime(new Date());
			dbq.setRequestorId(loginUser.getId());
			dataBuyQueueRepository.save(dbq);
		} catch (Exception e) {
			e.printStackTrace();
			SupervisorStats supervisorStats = supervisorStatsRepository
					.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId, "ABM", "INPROCESS");
			if (supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : ABM list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}
		logger.debug("abm List Added successfully");
		return Integer.toString(companyCount);
	}

	@SuppressWarnings("static-access")
	public int processRecords(Login loginUser,int rowsCount, String campaignId,Organization organization) {
		
		int  batchsize = 0;
		if(rowsCount>0) {
			batchsize = rowsCount /1000;
			long modSize = rowsCount % 1000;
			if(modSize > 0){
				batchsize = batchsize +1;
			}
		}
		int countSuccess = 0;
		List<AbmListNew> companies = new ArrayList<AbmListNew>();
		boolean invaliedfile = false;
		int rowCount = 0;
		int errorMapCount = 0;
		for (Row row : sheet) {
			errorListDomain = new ArrayList<String>();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + rowCount);
			//Row row = sheet.getRow(i);
			if (rowCount == 0) {
				errorMap.put(0, null);
				errorMapCount++;
				//continue;
			}
			boolean emptyCell = true;
			int maxCol = row.getLastCellNum();
			for (int j = 0; j < maxCol; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				continue;
			}
			for (int j = 0; j < 1; j++) {
				// String columnName = headerIndex.get(j).replace("*", "").trim();
				// DATE:07/11/2017 REASON:Added trim() to trim leading and trailing spaces.
				Cell cell = row.getCell(j);
				if (cell == null || cell.getCellType() == CellType.BLANK) {
					logger.debug("Cell is empty");
				}

				if (cell.getCellType() == CellType.STRING) {
					String company = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
//					if (company.contains(" ")) {
//						errorListDomain.add("Please Remove Space from Domain");
//					}else {
						//DomainValidator validator = DomainValidator.getInstance();
						if (company != null) {
							Cell cellAction = row.getCell(2);
							Cell cellAbmId = row.getCell(1);
							if (cellAction != null && cellAction.getCellType() != CellType.BLANK) {
								String action = cellAction.getStringCellValue().trim();
								if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("INSERT")) {
									AbmListNew abm = new AbmListNew();
									abm.setCampaignId(campaignId);
									abm.setCompanyName(company);
									abm.setStatus("ACTIVE");
									if(cellAbmId != null && cellAbmId.getCellType() != CellType.BLANK) {
										String companyId = cellAbmId.getStringCellValue().trim();
										abm.setCompanyId(companyId);
									}else {
										abm.setCompanyId(GET_COMPANY_ID);
									}
									companies.add(abm);
									countSuccess++;
								} else if (action.equalsIgnoreCase("REMOVE") || action.equalsIgnoreCase("DELETE")) {
									List<AbmListNew> removeList = abmListRepositoryNew
											.findAbmListNewByCampaignCompanyName(campaignId, company.toLowerCase());
									removeList.forEach(obj -> obj.setStatus("INACTIVE"));
									abmListRepositoryNew.saveAll(removeList);
								} 
							}
							else {
								logger.error("cell is empty");
							}
						}else {
							errorListDomain.add("Invalied Domain");
						}
					//}
				}
			}
			if(errorListDomain != null  && errorListDomain.size() > 0) {
				invaliedfile = true;
			}
			errorMap.put(errorMapCount, errorListDomain);
			rowCount++;
			errorMapCount++;
			
			boolean save =  false;
			
			if(rowCount == rowsCount-1 && batchsize==1) {
				save  = true;
			}else if((rowCount % 1000)==  0) {
				save = true;
				batchsize--;
		    }
			if (save) {
				abmListRepositoryNew.bulkinsert(companies);
				companies = new ArrayList<AbmListNew>();
			}
			
		}
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<String> reportingEmail = null;
		//if (organization != null && organization.getReportingEmail() != null &&  organization.getReportingEmail().size() > 0) {
			reportingEmail = loginUser.getReportEmails();
		//}
		if (invaliedfile) {
			ErrorHandlindutils.excelFileChecking(bookCopy, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(loginUser,bookCopy, fileName, "ABM", campaign.getName(),reportingEmail);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(),campaignId,"ABM", "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError("Excel file uploaded with errors");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : ABM list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}else {
			if (countSuccess > 0) {
				if (campaign != null && !campaign.isABM()) {
					campaign.setABM(true);
					campaignRepository.save(campaign);
				}
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.ABM_COUNT_REFRESH_EVENT, "");
			}
			sendEmail(campaignId, fileName + " File Uploaded with " + countSuccess + " Records",
					organization, loginUser);
		}
		
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		//String subject = "ABM Upload Status Success";
		//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
		int abmCompanyCount = 0;
//		AbmList abmListFromDB = abmListRepository.findABMListListByCampaignIdAndStatus(campaignId);
//		if (abmListFromDB != null) {
//			List<KeyValuePair<String, String>> dbCompanies = abmListFromDB.getCompanyList();
//			for (KeyValuePair<String, String> kv : dbCompanies) {
//				if (!companyNameList.contains(kv.getKey())) {
//					companies.add(kv);
//				}
//			}
//			abmCompanyCount = saveABMList(abmListFromDB, companies);
//		} else {
//			AbmList abmList = new AbmList();
//			abmList.setCampaignId(campaignId);
//			abmList.setStatus("ACTIVE");
//			abmCompanyCount = saveABMList(abmList, companies);
//		}
//		
//		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
//		String subject = "ABM Upload Status Success";
//		XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
		
		return abmCompanyCount;
	}

	public int saveABMList(List<AbmListNew> abmList) {
		if (abmList.size() > 0) {
			abmListRepositoryNew.saveAll(abmList);
		}
		return abmList.size();
	}
	
	public void saveABMListNew(AbmListNew abmList) {
		//if (abmList.size() > 0) {
			abmListRepositoryNew.save(abmList);
		//}
		//return abmList;
	}
	
	@Override
	public List<AbmListNew> getABMListByCampaignIdByPage(String campaignId,Pageable pageable) {
		List<AbmListNew> abmList = abmListRepositoryNew.findABMListNewByCampaignIdAndStatusPage(campaignId,pageable);
		return abmList;
	}

	@Override
	public String getABMListCountOfCampaign(String campaignId) {
		int abmListCount = 0;
		abmListCount = abmListRepositoryNew.countABMListNewByCampaignIdAndStatusLeadIq(campaignId);
		
		return Integer.toString(abmListCount);
	}
	
	private void sendEmail(String campaignId, String body,Organization organization, Login loginUser) {
			Campaign campaign = campaignRepository.findOneById(campaignId);
		try {
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			String subject = "ABM List Upload Status For " + campaign.getName();
//			XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, body);
//			List<String> listOfEmail = loginUser.getReportEmails();
//			if (listOfEmail != null && listOfEmail.size() > 0) {
//				XtaasEmailUtils.sendEmail(listOfEmail, "data@xtaascorp.com", subject, body);
//			}
			if( loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
				for(String email : loginUser.getReportEmails()) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, body);	
				}
			}else {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, body);
			}
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(),campaignId,"ABM", "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("COMPLETED");
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName()+" : "+body;
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		} catch (Exception e) {
			e.printStackTrace();
			SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(loginUser.getId(), campaignId,"ABM", "INPROCESS");
			if(supervisorStats != null) {
				supervisorStats.setStatus("ERROR");
				supervisorStats.setError(ExceptionUtils.getStackTrace(e));
				supervisorStatsRepository.save(supervisorStats);
				PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = campaign.getName() + " : ABM list upload failed, please contact support";
				teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			}
		}
	}
	
	private File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File( file.getOriginalFilename() );
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

	@Override
	public List<AbmListNew> searchABMListByCompanyName(String campaignId, String companyName) {
		List<AbmListNew> abmList = abmListRepositoryNew.searchAbmListNewByCampaignCompanyName(campaignId, companyName);
		return abmList;
	}
	
	@Override
	public List<AbmListNew> getABMListByPagination(String campaignId, int pageSize) {
		Pageable pageable = PageRequest.of(pageSize, 10, Direction.ASC, "createdDate");
		List<AbmListNew> abmList = abmListRepositoryNew.findABMListByPagination(campaignId, pageable);
		return abmList;
	}
	
	@Override
	public boolean removeABMListByCampaignId(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		ABMRemove abmRemove = new ABMRemove();
		abmRemove.setCampaignId(campaignId);
		abmRemove.setCampaign(campaign);
		abmRemove.setLogin(loginUser);
		abmRemove.setOrganization(organization);
		SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
		DelegatingSecurityContextExecutor abmRemoveThreadExecutor = new DelegatingSecurityContextExecutor(
				delegateExecutor, SecurityContextHolder.getContext());
		abmRemoveThreadExecutor.execute(abmRemove);
		logger.debug("Campaign : "+campaign.getName() +"ABM File - REMOVE-THREAD is started...");
		return true;
	}

	class ABMRemove implements Runnable{
		
		private String campaignId;
		private Organization organization;
		private Login login;
		private Campaign campaign;
		
		public Campaign getCampaign() {
			return campaign;
		}

		public void setCampaign(Campaign campaign) {
			this.campaign = campaign;
		}

		public Organization getOrganization() {
			return organization;
		}

		public void setOrganization(Organization organization) {
			this.organization = organization;
		}

		public Login getLogin() {
			return login;
		}

		public void setLogin(Login login) {
			this.login = login;
		}

		public String getCampaignId() {
			return campaignId;
		}

		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}

		@Override
		public void run() {
			try {
				long deletedCount = abmListRepositoryNew.deleteAllByCampaignIdAndStatus(campaignId);
				Campaign campaign = campaignRepository.findOneById(campaignId);
				if (campaign != null && campaign.isABM()) {
					campaign.setABM(false);
					campaignRepository.save(campaign);
				}
				Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

				if (loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
					for (String email : loginUser.getReportEmails()) {
						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
								"ABM List Remove Status for " + campaign.getName(), "Total Remove count : " + deletedCount);
					}
				} else {
					XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com",
							"ABM List Remove Status for " + campaign.getName(), "Total Remove count : " + deletedCount);
				}
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"AbmDelete", "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("COMPLETED");
					supervisorStatsRepository.save(supervisorStats);
				}
			} catch (Exception e) {
				e.printStackTrace();
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(), campaignId,"AbmDelete", "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
				}
			}
		}
	}
	
	@Override
	public String multiDownloadABMlist(String campaignId,String loginId) {
		ABMDownload abmDownload = new ABMDownload();
		abmDownload.setCampaignId(campaignId);
		abmDownload.setSheet(sheet);
		abmDownload.setLoginId(loginId);
		SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
		DelegatingSecurityContextExecutor abmDownloadThreadExecutor = new DelegatingSecurityContextExecutor(
				delegateExecutor, SecurityContextHolder.getContext());
		abmDownloadThreadExecutor.execute(abmDownload);
		logger.debug("CampaignId : "+campaignId+". ABM File - DOWNLOADING-THREAD is started...");
		return null;
	}
	
	@Override
	public String multiDownloadABMlistCheckAndSave(String campaignId, Login login) throws FileNotFoundException, IOException {
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"ABM", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("ABM");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("Download ABM Already in Process. Please Check After Sometime.");
		}
		return multiDownloadABMlist(campaignId, login.getId());
	}

	@Override
	public String uploadCampaignABMFile(String campaignId, MultipartFile file, Login login) throws IOException {
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"ABM", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("ABM");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("An upload process is in prgoress, please try later.");
		}
		return createCampaignABMList(campaignId, file);
	}
	
	class ABMDownload implements Runnable{

		private String campaignId;
		private Sheet sheet;
		private String loginId;
		
		public String getCampaignId() {
			return campaignId;
		}

		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}

		public Sheet getSheet() {
			return sheet;
		}

		public void setSheet(Sheet sheet) {
			this.sheet = sheet;
		}

		public String getLoginId() {
			return loginId;
		}

		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}

		@Override
		public void run() {
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet();
			this.sheet = sheet;
			int rowCountHeader = 0;
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			String fileName = null;
			Row headerRow = this.sheet.createRow(rowCountHeader);
			writeHeader(headerRow);
			Campaign campaign = campaignRepository.findOneById(getCampaignId());
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (campaign != null) {
				int rowCount = 1;
				int companyCount = abmListRepositoryNew
							.countABMListNewByCampaignCompanyName(getCampaignId());
				
				int  batchsize = 0;
				if(companyCount>0) {
					batchsize = companyCount /5000;
					long modSize = companyCount % 5000;
					if(modSize > 0){
						batchsize = batchsize +1;
					}
				}
					
				if(batchsize > 0) {
					for(int i=0;i<batchsize;i++) {
						Pageable pageable = PageRequest.of(i, 5000);
						List<AbmListNew> newABMList =abmListRepositoryNew.pageABMListNewByCampaignCompanyName(getCampaignId(),pageable);
						for (AbmListNew data : newABMList) {
							Row dataRow = this.sheet.createRow(rowCount);
							Cell cell = dataRow.createCell(0);
							cell.setCellValue(data.getCompanyName());
							Cell cell1 = dataRow.createCell(1);
							cell1.setCellValue(data.getCompanyId());
							Cell cell3 = dataRow.createCell(3);
							cell3.setCellValue(dateToString(data.getCreatedDate()));
							rowCount++;
						}
					}
				} 
			}
			fileName = (campaign != null) ? campaign.getName() + "_abm_list.xlsx": "new_abm_list.xlsx";
			FileOutputStream out;
			try {
				File file = new File(fileName);
				file.createNewFile();
				out = new FileOutputStream(file);
				workbook.write(out);
				workbook.close();
				String link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketName, fileName,
						file);
				String linkrefNotice = "<" + link + ">";
				String linkref = "<a href=\" "+link+" \">Download here</a>";
				logger.debug("ABM File :" + link);
				if (loginUser.getReportEmails() != null
						&& loginUser.getReportEmails().size() > 0) {
					for (String email : loginUser.getReportEmails()) {
						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
								"ABM List Download Status for " + campaign.getName(),
								"ABM List Download Link :  " + linkref);
					}
				} else {
					XtaasEmailUtils.sendEmail(loginUser.getEmail(), "deliveryteam@xtaascorp.com",
							"ABM List Download Status for " + campaign.getName(), "ABM List Download Link :  " + linkref);
				}
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(getLoginId(), getCampaignId(),"ABM", "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("COMPLETED");
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName() + " : ABM List Download Link : Download here " + linkrefNotice;
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
			} catch (Exception e) {
				e.printStackTrace();
				SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(getLoginId(), getCampaignId(),"ABM", "INPROCESS");
				if(supervisorStats != null) {
					supervisorStats.setStatus("ERROR");
					supervisorStats.setError(ExceptionUtils.getStackTrace(e));
					supervisorStatsRepository.save(supervisorStats);
					PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					String notice = campaign.getName() + " : ABM list download failed, please contact support";
					teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
				}
			}
			
		}
	
	}
	
	public Login getLogin(String loginId) {
		Login login = loginRepository.findOneById(loginId);
		return login;
	}
	
	public String dateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String sDate = sdf.format(date); 
		return sDate;
	}
}
