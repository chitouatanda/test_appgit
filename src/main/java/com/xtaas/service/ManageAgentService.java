package com.xtaas.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

public interface ManageAgentService {

	public void downloadTemplate(HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException;

	public String uploadDetails(MultipartFile file, HttpSession httpSession) throws IOException;

}