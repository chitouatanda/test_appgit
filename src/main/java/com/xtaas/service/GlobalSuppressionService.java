package com.xtaas.service;

import org.springframework.web.multipart.MultipartFile;

public interface GlobalSuppressionService {

	public String uploadEmailSuppressionList(MultipartFile file) throws Exception;

}
