package com.xtaas.service;

import java.util.HashMap;
import java.util.List;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.CRMMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.web.dto.CRMAttributeDTO;

public interface CRMMappingService {
	public CRMMapping getSystemMapping();
	public CRMMapping findByOrgId(String organizationId);
	public CRMMapping getCRMMapping(Organization organization);
	public HashMap<String, CRMAttribute> getCRMAttributeMap(String organizationId);
	public HashMap<String, CRMAttribute> getCRMAttributeMap(String organizationId, String campaignId);
	public void addCRMAttribute(String organization, CRMAttribute crmAttribute);
	public HashMap<String, CRMAttribute> getAllQuestions(String organizationId, List<CRMAttributeDTO> crmAttributeDTOs);
	public HashMap<String, CRMAttribute> getSystemCRMAttrubuteMap();
}
