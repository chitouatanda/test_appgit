package com.xtaas.service;

public interface SuppressionListService {

	/**
	 * Check email suppression by (campaignId and emailId) or (organizationId and
	 * emailId)
	 * 
	 * @param emailId
	 * @param campaignId
	 * @param organizationId
	 * @param isMdFiveSuppressionCheck
	 * @return boolean
	 */
	public boolean isEmailSuppressed(String emailId, String campaignId, String organizationId,
			boolean isMdFiveSuppressionCheck);

	/**
	 * 
	 * @param emailId
	 * @param campaignId
	 * @param organizationId
	 * @return boolean
	 */
	public boolean isEmailSuppressed(String emailId, String campaignId, String organizationId);

}
