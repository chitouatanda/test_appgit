package com.xtaas.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.xtaas.db.entity.AgentStatusLog;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;


@Service
public class DomoUtilsServiceImpl implements DomoUtilsService {

	

	private static final Logger logger = LoggerFactory.getLogger(DomoUtilsServiceImpl.class);

	public  void  postPciAnalysisWebhook(PCIAnalysis pcian) {
		try {
			
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			pcian.setCreated_date(pcian.getCreatedDate());
			pcian.setCreated_datetime(pcian.getCreatedDate());
			Calendar d = XtaasDateUtils.getPreviousDate(Calendar.getInstance(), 1);
			Date eDate = d.getTime();
			pcian.setRetentationDate(eDate);
		if (serverUrl != null  && serverUrl.toLowerCase().contains("app.xtaascorp.com")) {
			URL url = new URL ("https://xtaascorp.domo.com/api/iot/v1/webhook/data/eyJhbGciOiJIUzI1NiJ9.eyJzdHJlYW0iOiI4ZmFlODQwZTJiMjM0NTRjYTRiYmU5ZDc5ZjE4YWRlMzptbW1tLTAwMTAtNjIwMjoxMTU5OTAxMjEzIn0.OFhinavNp1HWn3A7O21B2FGxhTVzanfP-b1KxBZQHis");
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setDoOutput(true);
			
			//Creating the ObjectMapper object
	        ObjectMapper mapper= new ObjectMapper();//.setVisibility(JsonMethod.FIELD,Visibility.ANY);
	    	mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);  

		      //Converting the Object to JSONString
		      String jsonInputString = mapper.writeValueAsString(pcian);
			//String jsonInputString = pcian
			
			try(OutputStream os = con.getOutputStream()) {
			    byte[] input = jsonInputString.getBytes("utf-8");
			    os.write(input, 0, input.length);           
			}catch(Exception ose) {
				ose.printStackTrace();
			}
			
			
			try(BufferedReader br = new BufferedReader(
				new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				logger.debug(response.toString());
			}
		}	
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public  void  postAgentStatusLogWebhook(AgentStatusLog AgentStatusLog) {
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			if (serverUrl != null  && serverUrl.toLowerCase().contains("app.xtaascorp.com")) {
				URL url = new URL ("https://xtaascorp.domo.com/api/iot/v1/webhook/data/eyJhbGciOiJIUzI1NiJ9.eyJzdHJlYW0iOiJkZTgzNWY2OTNiZjQ0NDBjODU2YzdiNjRjNzVlOGEwYTptbW1tLTAwMTAtNjIwMjoxMTU5OTAxMjEzIn0._pVVYK7eYSO3UTUyZE6i1efCW8dqWuBVYLAn2GEFLDU");
				HttpURLConnection con = (HttpURLConnection)url.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/json; utf-8");
				con.setDoOutput(true);
				//Creating the ObjectMapper object
		        ObjectMapper mapper= new ObjectMapper();//.setVisibility(JsonMethod.FIELD,Visibility.ANY);
		    	mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);  

			      //Converting the Object to JSONString
			      String jsonInputString = mapper.writeValueAsString(AgentStatusLog);
				
				try(OutputStream os = con.getOutputStream()) {
				    byte[] input = jsonInputString.getBytes("utf-8");
				    os.write(input, 0, input.length);           
				}catch(Exception ose) {
					ose.printStackTrace();
				}
				
				
				try(BufferedReader br = new BufferedReader(
					new InputStreamReader(con.getInputStream(), "utf-8"))) {
					StringBuilder response = new StringBuilder();
					String responseLine = null;
					while ((responseLine = br.readLine()) != null) {
						response.append(responseLine.trim());
					}
					logger.debug(response.toString());
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
