package com.xtaas.service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.xtaas.db.entity.FeedbackJobTracker;
import com.xtaas.db.entity.PartnerFeedbackMapping;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectFeedback;
import com.xtaas.db.entity.FeedbackJobTracker.JobStatus;
import com.xtaas.db.repository.FeedbackJobTrackerRepository;
import com.xtaas.db.repository.PartnerFeedbackMappingRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectFeedbackRepository;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.PartnerFeedbackMappingDTO;

/**
 * Service to perform operations related to ProspectFeedback collection
 * 
 * @author pranay
 *
 */
@Service
public class ProspectFeedbackServiceImpl implements ProspectFeedbackService {
	public static final Logger logger = LoggerFactory.getLogger(ProspectFeedbackServiceImpl.class);

	private final PropertyService propertyService;
	private final DataFeedbackReportService dataFeedbackReportService;
	private final ProspectCallLogRepository prospectCallLogRepository;
	private final ProspectFeedbackRepository prospectFeedbackRepository;
	private final PartnerFeedbackMappingRepository partnerFeedbackMappingRepository;
	private final FeedbackJobTrackerRepository feedbackJobTrackerRepository;

	public ProspectFeedbackServiceImpl(PropertyService propertyService,
			DataFeedbackReportService dataFeedbackReportService, ProspectCallLogRepository prospectCallLogRepository,
			ProspectFeedbackRepository prospectFeedbackRepository,
			PartnerFeedbackMappingRepository partnerFeedbackMappingRepository,
			FeedbackJobTrackerRepository feedbackJobTrackerRepository) {
		this.propertyService = propertyService;
		this.dataFeedbackReportService = dataFeedbackReportService;
		this.prospectCallLogRepository = prospectCallLogRepository;
		this.prospectFeedbackRepository = prospectFeedbackRepository;
		this.partnerFeedbackMappingRepository = partnerFeedbackMappingRepository;
		this.feedbackJobTrackerRepository = feedbackJobTrackerRepository;
	}

	@Override
	public void autoDataBackupAndReport() {
		List<PartnerFeedbackMapping> partnerFeedbackMappings = partnerFeedbackMappingRepository
				.findByEnableDataFeedback(true);
		if (CollectionUtils.isEmpty(partnerFeedbackMappings)) {
			logger.info("DataFeedback is not enabled for any partner.");
			return;
		}

		// for each partner create new thread
		for (PartnerFeedbackMapping partnerFeedbackMapping : partnerFeedbackMappings) {
			try {
				ProspectFeedbackThread prospectFeedbackThread = new ProspectFeedbackThread(
						new PartnerFeedbackMappingDTO(partnerFeedbackMapping));
				Thread thread = new Thread(prospectFeedbackThread);
				thread.start();
			} catch (Exception ex) {
				logger.error("Failed to start ProspectFeedbackThread for [{}] partner. Error: ",
						partnerFeedbackMapping.getName(), ex);
			}
		}
	}

	@Override
	public String manualDataBackup(String name) throws Exception {
		PartnerFeedbackMapping partnerFeedbackMapping = partnerFeedbackMappingRepository
				.findByNameAndEnableDataFeedback(name, true);
		if (partnerFeedbackMapping == null) {
			throw new IllegalArgumentException(String.format(
					"Unable to fetch Partner [%s] from [%s] collection. Make sure partner exists & [%s] flag is enabled.",
					name, "partnerfeedbackmapping", "enableDataFeedback"));
		}
		String jobId = UUID.randomUUID().toString();
		PartnerFeedbackMappingDTO partnerFeedbackMappingDTO = new PartnerFeedbackMappingDTO(partnerFeedbackMapping);
		Date currentDate = new Date();
		try {
			dataBackupByPartner(jobId, partnerFeedbackMappingDTO);
		} catch (Exception ex) {
			logger.error(
					"Failed to take the backup of ProspectCallLog into ProspectFeedback for Partner:[{}], JobId:[{}]. Error: ",
					new Object[] { partnerFeedbackMappingDTO.getName(), jobId, ex });
			handleJobException(partnerFeedbackMappingDTO, currentDate, jobId, ex);
		}
		return jobId;
	}

	@Override
	public String manualDataBackupAndReport(String name) throws Exception {
		PartnerFeedbackMapping partnerFeedbackMapping = partnerFeedbackMappingRepository
				.findByNameAndEnableDataFeedback(name, true);
		if (partnerFeedbackMapping == null) {
			throw new IllegalArgumentException(String.format(
					"Unable to fetch Partner [%s] from [%s] collection. Make sure partner exists & [%s] flag is enabled.",
					name, "partnerfeedbackmapping", "enableDataFeedback"));
		}
		String jobId = UUID.randomUUID().toString();
		return dataBackupAndReportByPartner(jobId, new PartnerFeedbackMappingDTO(partnerFeedbackMapping));
	}

	private String dataBackupAndReportByPartner(String jobId, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
		Date currentDate = new Date();
		try {
			dataBackupByPartner(jobId, partnerFeedbackMappingDTO);

			// generate data feedback report & send to partner
			dataFeedbackReportService.autoDataFeedbackReport(jobId, partnerFeedbackMappingDTO);
		} catch (Exception ex) {
			logger.error(
					"Failed to take the backup of ProspectCallLog into ProspectFeedback for Partner:[{}], JobId:[{}]. Error: ",
					new Object[] { partnerFeedbackMappingDTO.getName(), jobId, ex });
			handleJobException(partnerFeedbackMappingDTO, currentDate, jobId, ex);
		}
		return jobId;
	}

	private String dataBackupByPartner(String jobId, PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
		feedbackJobTrackerRepository.save(new FeedbackJobTracker(jobId, JobStatus.BACKUP_INPROCESS,
				ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).withBackupJobStartTime(new Date()));

		backup(partnerFeedbackMappingDTO);

		feedbackJobTrackerRepository.findByJobId(jobId).ifPresent(feedbackJobTracker -> {
			feedbackJobTrackerRepository
					.save(feedbackJobTracker.withBackupJobEndTime(new Date()).withStatus(JobStatus.BACKUP_COMPLETED));
		});

		return jobId;
	}

	private void backup(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
		List<String> sourceList = new ArrayList<String>();
		sourceList.add(partnerFeedbackMappingDTO.getName());
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, partnerFeedbackMappingDTO.getDataFeedbackInDays() * -1);
		Date callStartTime = calendar.getTime();

		int count = prospectCallLogRepository.countBySourceStauseAndCallStartTime(sourceList,
				XtaasConstants.DATA_FEEDBACK_STATUSES, callStartTime);
		if (count < 1) {
			logger.info("No prospect found for [{}] Partner.", partnerFeedbackMappingDTO.getName());
			return;
		}

		int size = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.BULKREAD_BATCHSIZE.name(), 10000);
		int pages = count / size;
		int mod = count % size;
		if (mod > 0) {
			pages = pages + 1;
		}

		for (int i = 0; i < pages; i++) {
			Pageable pageable = PageRequest.of(i, size, Direction.DESC, "updatedDate");
			List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();

			prospectCallLogs = prospectCallLogRepository.findBySourceStauseAndCallStartTime(sourceList,
					XtaasConstants.DATA_FEEDBACK_STATUSES, callStartTime, pageable);

			if (CollectionUtils.isEmpty(prospectCallLogs)) {
				logger.info("No prospect found for [{}] Partner.", partnerFeedbackMappingDTO.getName());
				continue;
			}

			List<ProspectFeedback> prospectFeedbacks = new ArrayList<ProspectFeedback>();
			for (ProspectCallLog prospectCallLog : prospectCallLogs) {
				ProspectFeedback prospectFeedback = new ProspectFeedback();
				prospectFeedback.toProspectFeedback(prospectCallLog);
				prospectFeedbacks.add(prospectFeedback);
			}

			prospectFeedbackRepository.bulkInsert(prospectFeedbacks);
		}
	}

	private void handleJobException(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO, Date currentDate, String jobId,
			Exception ex) {
		// Update job error entry into DB
		feedbackJobTrackerRepository.findByJobId(jobId).ifPresent(feedbackJobTracker -> {
			feedbackJobTrackerRepository.save(feedbackJobTracker.withBackupJobEndTime(new Date())
					.withStatus(JobStatus.BACKUP_ERROR).withErrorMsg(ex.getMessage()));
		});

		// send email with details
		String subject = "failed prospect feedback job";
		String message = String.format("Failed prospect feedback job. Partner:[%s] JobId[%s]. Error: %s",
				partnerFeedbackMappingDTO.getName(), jobId, ex.getMessage());
		XtaasEmailUtils.sendEmail(partnerFeedbackMappingDTO.getErrorToEmails(),
				partnerFeedbackMappingDTO.getFromEmail(), subject, message);

		/*
		 * delete newly created records from prospectFeedback collection to avoid
		 * duplicate records
		 */
		prospectFeedbackRepository.deleteAllBySourceAndCreatedDate(partnerFeedbackMappingDTO.getName(), currentDate);
	}

	private class ProspectFeedbackThread implements Runnable {

		private PartnerFeedbackMappingDTO partnerFeedbackMappingDTO;

		public ProspectFeedbackThread(PartnerFeedbackMappingDTO partnerFeedbackMappingDTO) {
			this.partnerFeedbackMappingDTO = partnerFeedbackMappingDTO;
		}

		@Override
		public void run() {
			Instant startInstant = Instant.now();
			String jobId = UUID.randomUUID().toString();
			logger.info("Started ProspectFeedbackThread - Partner:[{}], JobId:[{}]",
					new Object[] { partnerFeedbackMappingDTO.getName(), jobId });
			dataBackupAndReportByPartner(jobId, partnerFeedbackMappingDTO);
			logger.info("Finished ProspectFeedbackThread - Partner:[{}], JobId:[{}]. Time taken in seconds:[{}]",
					new Object[] { partnerFeedbackMappingDTO.getName(), jobId,
							Duration.between(startInstant, Instant.now()).getSeconds() });
		}

	}

}
