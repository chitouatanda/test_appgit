package com.xtaas.service;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.domain.valueobject.*;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CurrentEmployment;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.insideview.*;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.ManualProspectDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class InsideViewManualSearchProspectServiceImpl implements Runnable {

	CampaignRepository campaignRepository;

	CompanyMasterRepository companyMasterRepository;

	private ManualProspectSearchServiceImpl manualProspectSearchServiceImpl;

	private ProspectCallLogRepository prospectCallLogRepository;

	private static final String manualPersonSearchUrl = "https://api.insideview.com/api/v1/contacts";//GET

	final private String insideViewToken = "f0VbD34ZWUJkB7DuzKfiKE8v+3lniKgTcC9lOkPm1R/u0PNIAT1BD2AVC08ZFdWIu+pXWAB361zZWxQ3Dv5/81wTqDchuL5SyAHKprPeLztLqMvI6AvofZUggIDldY2fUFq+o+lgLnJajEfhxSsa8iNmjeJQAh6s4tI4BNWBMbs=.eyJmZWF0dXJlcyI6InsgfSIsImNsaWVudElkIjoibnZhODZpbGI0YzFtdm83Mmdva2wiLCJncmFudFR5cGUiOiJjcmVkIiwidHRsIjoiMzE1MzYwMDAiLCJpYXQiOiIxNjE3MTM2MDU1IiwidmVyc2lvbiI6InYxIiwianRpIjoiYTgxNGFhYWQtM2Y5ZS00YzczLThlMDktMTQ1MzRhYzMwMzgzIn0=";

	private static final String manualCompanyUrl = "https://api.insideview.com/api/v1/company/{companyId}";

	private static final String manualPersonDetailUrl = "https://api.insideview.com/api/v1/contact/{contactId}";//GET

	private Prospect prospect;

	private String agentId;

	private CountDownLatch latch;

	private String campaignId;

	private GlobalContactRepository globalContactRepository;

	public InsideViewManualSearchProspectServiceImpl(Prospect prospect,String campaignId,String agentId,CountDownLatch latch) {
		this.prospect = prospect;
		this.agentId = agentId;
		this.latch = latch;
		this.campaignId = campaignId;
		campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		companyMasterRepository = BeanLocator.getBean("companyMasterRepository", CompanyMasterRepository.class);
		manualProspectSearchServiceImpl = BeanLocator.getBean("manualProspectSearchServiceImpl", ManualProspectSearchServiceImpl.class);
		prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
		globalContactRepository = BeanLocator.getBean("globalContactRepository", GlobalContactRepository.class);
	}

	@Override
	public void run() {
			try {
				getInsideViewProspect(prospect);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.latch.countDown();
	}

	public void getInsideViewProspect(Prospect prospect){
		try {
			if (StringUtils.isNotBlank(prospect.getCompany()) && StringUtils.isNotBlank(prospect.getTitle())) {
				Campaign campaign = campaignRepository.findOneById(campaignId);
				String companyName = prospect.getCompany();
				String title = prospect.getTitle();
				CampaignCriteria cc = getCampaignCriteria(campaign);
				String urlParameters = "companyName=" + URLEncoder.encode(companyName, XtaasConstants.UTF8) +
						"&titles=" + URLEncoder.encode(title, XtaasConstants.UTF8)  +
						"&country=" + URLEncoder.encode(cc.getCountry(), XtaasConstants.UTF8)  +
						"&active=true&phoneType=DIRECT&page=1&resultsPerPage=500";
				getManualPersonsByCriteria(manualPersonSearchUrl, urlParameters,campaign);
			}
		}catch(Exception e){

		}
	}

	public void getManualPersonsByCriteria(String url, String urlParameters,Campaign campaign) {
		try {
			StringBuilder response = getIVResponse(url, urlParameters);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				PersonResponse personResponse = mapper.readValue(response.toString(), PersonResponse.class);
				if (personResponse == null || personResponse.getContacts() == null
						|| personResponse.getContacts().isEmpty()) {
				}else {
					getManualDetail(personResponse,campaign);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StringBuilder getIVResponse(String url, String params) {
		StringBuilder response = new StringBuilder();

		try {
			if (url.contains("{newContactId}")) {
				url = url.replace("{newContactId}", params);
				url = url + "?retrieveCompanyDetails=true";
			} else if (url.contains("{newCompanyId}")) {
				url = url.replace("{newCompanyId}", params);
			} else if (url.contains("{companyId}")) {
				url = url.replace("{companyId}", params);
			}else if (url.contains("{contactId}")) {
				url = url.replace("{contactId}", params);
			}else {
				url = url + "?" + params;
			}
			HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();
			// add request header
			httpClient.setRequestMethod("GET");
			httpClient.setRequestProperty("accessToken", insideViewToken);
			httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
			httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			httpClient.setRequestProperty("Accept", "application/json");
			int responseCode = httpClient.getResponseCode();
			try (BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()))) {
				String line;

				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		List<Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();
			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
		}
		return cc;
	}


	private void getManualDetail(PersonResponse personSearchResponse, Campaign campaign) {
		Prospect prospectInfo = null;
		if (personSearchResponse!=null && personSearchResponse.getTotalResults() > 0 && personSearchResponse.getContacts() != null) {
			List<Contacts> list = personSearchResponse.getContacts();
			for(Contacts personRecord  : list){
				if (personRecord != null) {
					String contactId = personRecord.getContactId();
					int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordBySourceId(campaign.getId(),contactId);
					if(uniqueRecord == 0) {
						Prospect prospectDetailsResponse = getProspectDetails(personRecord);
						if(prospectDetailsResponse != null && prospectDetailsResponse.getPhone() != null){
							List<Prospect> listDto = manualProspectSearchServiceImpl.manualProspectMap.get(agentId);
							if(CollectionUtils.isNotEmpty(listDto)){
								manualProspectSearchServiceImpl.manualProspectMap.get(agentId).add(prospectDetailsResponse);
							}else {
								List<Prospect> emptyListDto = new ArrayList<>();
								emptyListDto.add(prospectDetailsResponse);
								manualProspectSearchServiceImpl.manualProspectMap.put(agentId,emptyListDto);
							}
							saveGlobalContact(prospectDetailsResponse);
						}
						if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null &&
								manualProspectSearchServiceImpl.manualProspectMap.get(agentId).size() >= manualProspectSearchServiceImpl.SIMILAR_PROSPECT_SIZE) {
							return;
						}
					}
				}
			}
		}
	}

	private void saveGlobalContact(Prospect prospect){
		try {
			ProspectCall prospectCall = new ProspectCall();
			prospectCall.setProspect(prospect);
			GlobalContact globalContact = new GlobalContact();
			globalContact = globalContact.toGlobalContact(prospectCall);
			globalContactRepository.save(globalContact);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	///////////////////////////////////////////////////

	private Prospect getProspectDetails(Contacts personRecord){
		Prospect prospectInfo = null;
		if (personRecord != null) {
			String contactId =  personRecord.getContactId();
			prospectInfo = getPersonDetail(manualPersonDetailUrl, contactId);
			Companies cresponse = getManualCompanyDetail(personRecord.getCompanyId());
			if (cresponse!=null) {
				List<CompanyMaster> cmList = companyMasterRepository.findCompanyMasterListByCompanyId(personRecord.getCompanyId());
				if(cmList ==null || cmList.size()==0) {
					CompanyMaster cm = new CompanyMaster();
					CompanyAddress ca = new CompanyAddress();
					ca.setCity(cresponse.getCity());
					ca.setCountry(cresponse.getCountry());
					ca.setState(cresponse.getState());
					ca.setStreet(cresponse.getStreet());
					ca.setZip(cresponse.getZip());
					cm.setCompanyAddress(ca);
					cm.setCompanyDescription(cresponse.getCompanyBlogProfile());
					cm.setCompanyDetailXmlUrl(cresponse.getCompanyFacebookProfile());
					// cm.setCompanyEmployeeCount(cresponse.getEmployees());
					cm.setCompanyEmployeeCountRange(cresponse.getEmployees());
					cm.setCompanyId(cresponse.getCompanyId());
					List<String> indus = new ArrayList<String>(1);
					indus.add(cresponse.getIndustry());
					cm.setCompanyIndustry(indus);
					if(cresponse.getNaics()!=null) {
						cm.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
					}
					cm.setCompanyName(cresponse.getName());
					//cm.setCompanyProductsAndServices(cresponse.get);
					// cm.setCompanyRanking(cresponse.getFortuneRanking());
					//cm.setCompanyRevenueIn000s(cresponse.getRevenueCurrency());
					cm.setCompanyRevenueRange(cresponse.getRevenue());
					if(cresponse.getSic()!=null) {
						cm.setCompanySIC(Arrays.asList(cresponse.getSic().split(",")));
					}
					if(cresponse.getTickers()!=null) {
						StringBuilder tickersb = new StringBuilder();

						for(Tickers ticker : cresponse.getTickers()) {
							tickersb.append(ticker.getTickerName());
						}
						cm.setCompanyTicker(tickersb.toString());
					}
					// cm.setCompanyTopLevelIndustry(companyTopLevelIndustry);
					cm.setCompanyType(cresponse.getCompanyType());
					if(cresponse.getWebsites()!=null) {
						cm.setCompanyWebsite(cresponse.getWebsites().get(0));

					}
					//cm.setDefunct(cresponse);
					cm.setEnrichSource("INSIDEVIEW");
					// cm.setHashtags(hashtags);
					//cm.setNumOfCompany(cresponse.get);
					cm.setPhone(cresponse.getPhone());
					//cm.setSimilarCompanies(similarCompanies);
					cm.setZoomCompanyUrl(cresponse.getCompanyTwitterProfile());
					companyMasterRepository.save(cm);
				}
				prospectInfo.setCity(cresponse.getCity());
				prospectInfo.setCountry(cresponse.getCountry());
				prospectInfo.setStateCode(cresponse.getState());
				prospectInfo.setAddressLine1(cresponse.getStreet());
				prospectInfo.setZipCode(cresponse.getZip());
					 	/*StateCallConfig scc = getStateCodeAndTimeZone(cresponse.getState(),cresponse.getCountry());
						if (scc != null) {
							prospectInfo.setTimeZone(scc.getTimezone());
							prospectInfo.setStateCode(scc.getStateCode());
						}*/
				prospectInfo.setIndustry(cresponse.getIndustry());
				prospectInfo.setSource("INSIDEVIEW");
				prospectInfo.setDataSource("Xtaas Data Source");
				prospectInfo.setSourceType("INTERNAL");
				prospectInfo.setCustomAttribute("maxRevenue", cresponse.getRevenue());
				prospectInfo.setCustomAttribute("maxEmployeeCount", cresponse.getEmployees());
				if (cresponse.getWebsites().size() > 0) {
					prospectInfo.setCustomAttribute("domain", cresponse.getWebsites().get(0));
				}
				if(cresponse.getNaics()!=null) {
					prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getNaics().split(",")));
				}
				if(cresponse.getSic()!=null) {
					prospectInfo.setCompanyNAICS(Arrays.asList(cresponse.getSic().split(",")));
				}

			}
		}
		return prospectInfo;
	}

	public Prospect getPersonDetail(String url, String urlParameters) {
		// {"contactId":157117130,"firstName":"Steve","lastName":"T","companyId":3645001,"companyName":"1
		// EDI Source, Inc.","phone":"+1 440 519
		// 7800","linkedinHandle":"https://www.linkedin.com/in/stevectuttle","active":true,"jobLevels":["Manager"],"jobFunctions":["IT
		// &
		// IS"],"peopleId":"72rIihOXZC-Z8Mz3BIQo3e5czy8uedrXysKH0uo5VkeIDQu-ddvMFHc7VRqn02K-","fullName":"Steve
		// T","confidenceScore":45.0,"corporatePhone":"+1 440 519 7800","sources":["Web
		// References"],"titles":["IT Manager"]}
		Prospect prospectInfo = null;
		try {
			// StringBuilder params = new StringBuilder();
			StringBuilder response = getIVResponse(url, urlParameters);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				DetailedContact detailedContact = mapper.readValue(response.toString(), DetailedContact.class);
				prospectInfo = mapToProspectObject(detailedContact);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//logger.error(e.getMessage());
		}
		return prospectInfo;
	}

	private Prospect mapToProspectObject(DetailedContact detailedContact) {
		Prospect prospectInfo = new Prospect();
		prospectInfo.setSource("INSIDEVIEW");
		prospectInfo.setSourceType("INTERNAL");
		prospectInfo.setDataSource("Xtaas Data Source");
		prospectInfo.setSourceId(detailedContact.getContactId().toString());
		prospectInfo.setFirstName(detailedContact.getFirstName());
		prospectInfo.setLastName(detailedContact.getLastName());
		prospectInfo.setSourceCompanyId(Long.toString(detailedContact.getCompanyId()));
		prospectInfo.setCompany(detailedContact.getCompanyName());
		prospectInfo.setPhone(getFormattedPhone(detailedContact.getPhone()));
		prospectInfo.setCustomAttribute("linkedinUrl", detailedContact.getLinkedinHandle());
		prospectInfo.setZoomPersonUrl(detailedContact.getLinkedinHandle());
		prospectInfo.setCompanyPhone(getFormattedPhone(detailedContact.getCorporatePhone()));
		prospectInfo.setTitle(detailedContact.getTitles().get(0));
		prospectInfo.setConfidenceScore(detailedContact.getConfidenceScore().toString());
		if (detailedContact.getJobFunctions() != null && detailedContact.getJobFunctions().size() > 0) {
			prospectInfo.setDepartment(detailedContact.getJobFunctions().get(0));
		}
		if (detailedContact.getJobLevels() != null && detailedContact.getJobLevels().size() > 0) {
			prospectInfo.setManagementLevel(getStdMgmtLevl(detailedContact.getJobLevels().get(0)));
		}
		if (detailedContact.getCompanyDetails() != null) {
			prospectInfo.setCustomAttribute("maxRevenue", detailedContact.getCompanyDetails().getRevenue());
			prospectInfo.setCustomAttribute("maxEmployeeCount", detailedContact.getCompanyDetails().getEmployees());
			if (detailedContact.getCompanyDetails().getWebsites().size() > 0) {
				prospectInfo.setCustomAttribute("domain", detailedContact.getCompanyDetails().getWebsites().get(0));
			}
		}
		if (detailedContact.getEmail() != null && detailedContact.getEmailValidationStatus() != null
				&& detailedContact.getEmailValidationStatus().equalsIgnoreCase("ValidEmail")) {
			prospectInfo.setEmail(detailedContact.getEmail());
		} else if (detailedContact.getEmails() != null && detailedContact.getEmails().size() > 0) {
			for (Email emailObj : detailedContact.getEmails()) {
				if (emailObj.getValidationStatus().equalsIgnoreCase("ValidEmail")) {
					prospectInfo.setEmail(emailObj.getEmail());
					break;
				}
			}
			if (prospectInfo.getEmail() == null || prospectInfo.getEmail().isEmpty()) {
				prospectInfo.setEmail(detailedContact.getEmails().get(0).getEmail());
			}
		}
		return prospectInfo;
	}

	private String getFormattedPhone(String phone) {
		phone = phone.replaceAll("[^\\d]", "");
		phone = "+" + phone.trim();
		return phone;
	}

	private String getStdMgmtLevl(String mgmtLevel) {
		String ivmgmtLevel = "C-Level";
		if(mgmtLevel.equalsIgnoreCase("C-Level") ||mgmtLevel.equalsIgnoreCase("C Level")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Senior Executive")) {
			ivmgmtLevel = "C-Level";
		}else if(mgmtLevel.equalsIgnoreCase("VP") || mgmtLevel.equalsIgnoreCase("Vice President")) {
			ivmgmtLevel = "VP-Level";
		}else if(mgmtLevel.equalsIgnoreCase("Director")) {
			ivmgmtLevel = "DIRECTOR";
		}else if(mgmtLevel.equalsIgnoreCase("Manager")) {
			ivmgmtLevel = "MANAGER";
		}else if(mgmtLevel.equalsIgnoreCase("Board Member")) {
			ivmgmtLevel = "Board Members";
		}else if(mgmtLevel.equalsIgnoreCase("Other")) {
			ivmgmtLevel = "Non Management";
		}
		return ivmgmtLevel;
	}

	public Companies getManualCompanyDetail(String params) {
		//StringBuilder params = new StringBuilder();
		Companies companies = null;

		try {
			/*
			 * params.append("name=").append("Google");
			 * params.append("website=").append("google.com");
			 * params.append("ticker=").append("GOOGL"); ///// above either of one is
			 * required. params.append("&country=");params.append("United States");
			 * params.append("&state=");params.append("CA");
			 * params.append("&city=");params.append("Mountain View");
			 * params.append("&street=");params.append("street name");
			 * params.append("&zip=");params.append("GOOGL");
			 * params.append("&page=");params.append(1);
			 * params.append("&resultsPerPage=");params.append(10);// min1, max 50, default
			 * 10
			 */
			StringBuilder response = getIVResponse(manualCompanyUrl, params);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				companies = mapper.readValue(response.toString(), Companies.class);
				//System.out.println(companies);
			}
		} catch (Exception e) {
			//logger.error(e.getMessage());
		}

		return companies;
	}
}
