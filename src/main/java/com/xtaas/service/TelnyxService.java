package com.xtaas.service;

import java.util.List;
import java.util.Map;

import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.web.dto.BillingGroupOutputDTO;
import com.xtaas.web.dto.CallInfoDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;
import com.xtaas.web.dto.TelnyxCallStatusDTO;
import com.xtaas.web.dto.TelnyxCallWebhookDTO;
import com.xtaas.web.dto.TelnyxDTMFRequestDTO;
import com.xtaas.web.dto.TelnyxPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.TelnyxRecordingUrlObjForMap;

public interface TelnyxService {

  public String handleVoiceCall(TelnyxCallWebhookDTO telnyxCallWebhookDTO);

  public Map<String, String> createConnectionForAgent(String username);

  public String deleteConnectionForAgent(String connectionId);

  public String purchasePhoneNumbers(TelnyxPhoneNumberPurchaseDTO telnyxPhoneNumberPurchaseDTO);

  public String handleIncomingCall(TelnyxCallWebhookDTO telnyxCallWebhookDTO);

  public String saveRecording(TelnyxCallWebhookDTO telnyxCallWebhookDTO);

  public Map<String, Object> callAgentForConferenceCreation(CallInfoDTO callInfo);

  // added for callservice and to send call status info to Dialer
  public TelnyxCallStatusDTO getCallStatus(String callLegId);

  // added to start recording for manual agent
  // changed implementation so commented
  // public String startRecording(TelnyxCallWebhookDTO telnyxCallWebhookDTO);

  public Map<String, Object> callAgentForManualDial(CallInfoDTO callInfo);

  public void handleCallHangup(String callLegId);

  public TelnyxRecordingUrlObjForMap getRecordingUrlsFromMap (String pcid);

  public String getCallControlId (String callLegId);

  public void mapAlternatePospectWithMainProspect (String pcid, String altPcid);
  
  public void sendDtmf(TelnyxDTMFRequestDTO dtmfObj);
  
  public BillingGroupOutputDTO createBillingGroup(String accountName, String partnerId);
  
  public List<TelnyxOutboundNumber> purchaseUSPhoneNumbersByLimit(TelnyxPhoneNumberPurchaseDTO phoneDTO);
  
  public int getSizeOfCallLegAndControlIdMap();
  
  public Map<String, String> getTelnyxOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO);
  
  public List<String> deleteTelnyxOutboundNumber(List<String> phoneNumberIdList);

}