package com.xtaas.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpService {
    private final Logger log = LoggerFactory.getLogger(HttpService.class);



//    @Bean
//public RestTemplate restTemplate() {
//    return new RestTemplate();
//}

// @Autowired
//RestTemplate restTemplate;

//    public HttpService(RestTemplate restTemplate) {
//        this.restTemplate = restTemplate;
//    }

    public <T> T get(String url, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {
        try {
        	RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>("", headers),
                    responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("GET request Failed for '" + url + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

    public <T> List<T> getAsList(String url, HttpHeaders headers, ParameterizedTypeReference<T[]> responseType) {
        T[] result = get(url, headers, responseType);

        return result == null ? Arrays.asList() : Arrays.asList(result);
    }

    public <T, R> T post(String url, HttpHeaders headers, ParameterizedTypeReference<T> responseType, String jsonBody) {
        try {
        	RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jsonBody, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("POST request Failed for '" + url + "': " + ex.getResponseBodyAsString());
            if (url.contains("/actions/transfer")) {
            	Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", ex.getResponseBodyAsString());
                return (T) errorMap;
            }
        }
        return null;
    }

    public <T, R> T put(String url, HttpHeaders headers, ParameterizedTypeReference<T> responseType, String jsonBody) {
        try {
        	RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.PUT,
                    new HttpEntity<>(jsonBody, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("PUT request Failed for '" + url + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

    public <T, R> T patch(String url, HttpHeaders headers, ParameterizedTypeReference<T> responseType, String jsonBody) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            int TIMEOUT = 15000;
            requestFactory.setConnectTimeout(TIMEOUT);
            requestFactory.setReadTimeout(TIMEOUT);

            restTemplate.setRequestFactory(requestFactory);
            ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.PATCH,
                    new HttpEntity<>(jsonBody, headers), responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("PATCH request Failed for '" + url + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

    public <T> T delete(String url, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {
        try {
        	RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
                    new HttpEntity<>("", headers), responseType);

            return responseEntity.getBody();
        } catch (HttpClientErrorException ex) {
            log.error("DELETE request Failed for '" + url + "': " + ex.getResponseBodyAsString());
        }
        return null;
    }

}
