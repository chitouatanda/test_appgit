package com.xtaas.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;

@Service
public class NewClientMappingUploadServiceImpl implements NewClientMappingUploadService {

	@Autowired
	NewClientMappingRepository newclientMappingRepository;

	@Autowired
	CampaignRepository campaignRepository;

	@Autowired
	CampaignService campaignService;

	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	
	private static final Logger logger = LoggerFactory.getLogger(NewClientMappingUploadServiceImpl.class);

	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;

	private HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	private List<String> errorList;
	
	private	Workbook book;
	
	private String xlsFlieName;
	
	private HashMap<Integer,List<String>> errorMap=new HashMap<>();
	
	private String bucketName = "xtaasreports";

	@Override
	public String createNewClientMappings(String campaignId, MultipartFile file) throws IOException {
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (!file.isEmpty()) {
			xlsFlieName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(fileName + " file is empty");
						throw new IllegalArgumentException("File Can not be Empty.");
					}else if (totalRowsCount <= 1) {// check if nothing found in file
						book.close();
						logger.error("No records found in file " + xlsFlieName);
						throw new IllegalArgumentException("No Records Found in File.");
					}
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords(loginUser,campaignId);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);// get first sheet in file
		int colCounts = row.getPhysicalNumberOfCells(); // 4 count
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell.getCellType() != CellType.BLANK) {
				if (cell.getCellType() != CellType.STRING) {
					logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
							+ " Header must contain only String values");
					return false;
				} else {
					headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
					fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
				}
			}
		}
		return true;
	}

	private void validateExcelFileRecords(Login loginUser,String campaignId) {
//		int rowsCount = sheet.getPhysicalNumberOfRows();
		int rowsCount = ErrorHandlindutils.getNonBlankRowCountNumber(sheet);
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		processRecords(loginUser,0, rowsCount, campaignId);
	}

	public void processRecords(Login loginUser,int startPos, int EndPos, String campaignId) {
	
		Campaign campaignFromDB = campaignRepository.findOneById(campaignId);
		List<NewClientMapping> clientMappings = new ArrayList<>();
		if (campaignFromDB != null) {
			clientMappings = newclientMappingRepository.findAllByCampaignIdAndBySequenceSort(
					campaignFromDB.getId(),
					PageRequest.of(0, Integer.MAX_VALUE, Direction.ASC, "sequence"));
			if (clientMappings.size() > 0 || clientMappings != null) {
				for (NewClientMapping clientMapping : clientMappings) {
					newclientMappingRepository.deleteById(clientMapping.getId());
				}
			}
		}
		List<String> clientValues = new ArrayList<String>();
		boolean invalidFile=false;
		for (int i = startPos; i < EndPos; i++) {
			final Row tempRow = sheet.getRow(i);// getting first row for counting number of header columns
			int colCounts = tempRow.getPhysicalNumberOfCells();
			NewClientMapping clientMapping = new NewClientMapping();
			errorList=new ArrayList<>();
			DataFormatter dataFormatter = new DataFormatter();
			if (campaignId != null) {
				clientMapping.setCampaignId(campaignId);
			} else {
				throw new IllegalArgumentException("Campaign ID is required to upload client mapping file");
			}
			if (campaignFromDB.getClientName() != null) {
				clientMapping.setClientName(campaignFromDB.getClientName());
			} else {
				clientMapping.setClientName(campaignFromDB.getOrganizationId());
			}
			logger.debug("excelFileRead(): validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			if (i == 0) {
				continue;
			}
			if (row == null) {
				errorList.add("Record is Empty ");
				errorMap.put(i, errorList);
				invalidFile=true;
				EndPos++;
				continue;
			}
			

			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				errorMap.put(i, errorList);
				invalidFile=true;
				EndPos++;
				continue;
			}
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j) != null ? headerIndex.get(j).replace("*", "").trim() : "";
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("Report Sequence") || columnName.equalsIgnoreCase("ReportSequence")) {
					if (cell != null) {
						String sequence = cell.getCellType() == CellType.STRING
								? sequence = cell.getStringCellValue().trim()
								: new BigDecimal(cell.getNumericCellValue()).toPlainString().trim();
						//String sequence = dataFormatter.formatCellValue(cell).trim();
						clientMapping.setSequence(Integer.parseInt(sequence.trim()));
					}
				} else if (columnName.equalsIgnoreCase("Report Column Header")
						|| columnName.equalsIgnoreCase("ReportColumnHeader")) {
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						String colheader = cell.getCellType() == CellType.STRING
								? colheader = cell.getStringCellValue().trim()
								: new BigDecimal(cell.getNumericCellValue()).toPlainString().trim();
						//String colheader = dataFormatter.formatCellValue(cell).trim();
						clientMapping.setColHeader(colheader.trim());
					}
					else {
						errorList.add(columnName+" is missinng ");
					}
				} else if (columnName.equalsIgnoreCase("DB Standard Attribute")
						|| columnName.equalsIgnoreCase("DBStandardAttribute")) {
					if (cell != null && cell.getCellType() != CellType.BLANK)  {
						String stdAttribute = cell.getCellType() == CellType.STRING
								? stdAttribute = cell.getStringCellValue().trim()
								: new BigDecimal(cell.getNumericCellValue()).toPlainString().trim();
						//String stdAttribute = dataFormatter.formatCellValue(cell).trim();
						clientMapping.setStdAttribute(stdAttribute.trim());
					}
					else {
						errorList.add(columnName + "is missinng");
					}
				} else if (columnName.equalsIgnoreCase("Client Values") || columnName.equalsIgnoreCase("")) {
					if (cell != null) {
						String clientValue = dataFormatter.formatCellValue(cell).trim();
						if (clientValue != null && !clientValue.isEmpty() && !clientValue.equalsIgnoreCase("")) {
							clientValues.add(clientValue);
						}
					}
				}
			}
			
			clientMapping.setClientValues(clientValues);
			newclientMappingRepository.save(clientMapping);
			clientValues = new ArrayList<String>();
			if (errorList != null && errorList.size() > 0 ) {
				invalidFile=true;
			}
			errorMap.put(i, errorList);
		}
		Campaign campaign = campaignService.getCampaign(campaignId);
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		List<String> reportingEmail = null;
		if (loginUser.getReportEmails() != null && loginUser.getReportEmails().size() > 0) {
			reportingEmail = loginUser.getReportEmails();
		}
		if (errorMap != null && invalidFile) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(loginUser,book,xlsFlieName,"Client Mapping",campaign.getName(),reportingEmail);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
			String notice = "Client Mapping list upload failed, please contact support";
			teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			throw new IllegalArgumentException("Please Check Your Email There is Some Error in File");

		}
		logger.debug("New version Client mapping file uploaded successfully for campaignId : [{}]", campaignId);
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		String subject = "Client Mapping Upload Status Success For " + campaign.getName();
		//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
		if(reportingEmail != null && reportingEmail.size() > 0) {
			for(String email : reportingEmail) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",subject, "File Uploaded Successfully");
			}
		}else {
			XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
		}
		PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = xlsFlieName + " File Uploaded Successfully";
		teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
	}

	@Override
	public void downloadNewTemplate(String campaignId, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		if (campaignId == null || campaignId.equals("")) {
			throw new IllegalArgumentException("Campaign ID is required to download the client mapping file.");
		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		String fileName = null;
		XSSFSheet sheet = workbook.createSheet();
		int rowCount = 0;
		Row headerRow = sheet.createRow(rowCount);
		writeHeaders(headerRow, workbook);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (campaign != null) {


			List<NewClientMapping> clientMappings = newclientMappingRepository
					.findAllByCampaignIdAndBySequenceSort(campaign.getId(),
							PageRequest.of(0, Integer.MAX_VALUE, Direction.ASC, "sequence"));
			if(clientMappings == null || clientMappings.size() == 0){
				clientMappings = newclientMappingRepository
						.findClientMappingByClientName("SYSTEM_DEFAULT");
			}else {
				List<NewClientMapping> clientMappingsList = newclientMappingRepository
						.findClientMappingByClientName("SYSTEM_DEFAULT");
				List<NewClientMapping> newList = new ArrayList<>();
				List<NewClientMapping> finalClientMappings = clientMappings;
				newList = clientMappingsList.stream()
						.filter(o1 -> finalClientMappings.stream().noneMatch(o2 -> o2.getStdAttribute().equalsIgnoreCase(o1.getStdAttribute())))
						.collect(Collectors.toList());
				clientMappings.addAll(newList);
			}

			if (clientMappings.size() > 0) {
				for (NewClientMapping clientMapping : clientMappings) {
					Row row = sheet.createRow(++rowCount);
					writeToExcel(clientMapping, row);
				}
			}
		}
		fileName = (campaign != null) ? campaign.getName() + "_mapping.xlsx" : "client_mapping.xlsx";
		
		FileOutputStream out;
		try {
			File file = new File(fileName);
			file.createNewFile();
			out = new FileOutputStream(file);
			workbook.write(out);
			workbook.close();
			String link = downloadRecordingsToAws.uploadSuppresstionExcelFile(bucketName, fileName,
					file);
			String linkrefNotice = "<" + link + ">";
			String linkref = "<a href=\" "+link+" \">Download here</a>";
			logger.debug("Client Mapping :" + link);
			if (loginUser.getReportEmails() != null
					&& loginUser.getReportEmails() .size() > 0) {
				for (String email : loginUser.getReportEmails() ) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
							"Client Mapping Download Status for " + campaign.getName(),
							"Client Mapping Download Link :  " + linkref);
				}
			} else {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",
						"Client Mapping Download Status for " + campaign.getName(), "Client Mapping List Download Link :  " + linkref);
			}
			
			PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
			String notice = "Client Mapping Download Link : Download here " + linkrefNotice;
			teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
			
		} catch (Exception e) {
			e.printStackTrace();
			PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
			String notice = "Client Mapping download failed, please contact support";
			teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
		}
		
//		response.setContentType("application/octet-stream");
//		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
//		workbook.write(response.getOutputStream());
//		workbook.close();
	}

	private void writeHeaders(Row headerRow, Workbook workbook) {
		CellStyle backgroundStyle = applyBackgroundColourToCell(workbook);
		Cell cell = headerRow.createCell(0);
		cell.setCellValue("Report Sequence");
		cell.setCellStyle(backgroundStyle);
		cell = headerRow.createCell(1);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("Report Column Header");
		cell = headerRow.createCell(2);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("DB Standard Attribute");
		cell = headerRow.createCell(3);
		cell.setCellStyle(backgroundStyle);
		cell.setCellValue("Client Values");
	}

	private CellStyle applyBackgroundColourToCell(Workbook workbook) {
		CellStyle backgroundStyle = workbook.createCellStyle();
		backgroundStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		backgroundStyle.setBorderBottom(BorderStyle.THIN);
		backgroundStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderLeft(BorderStyle.THIN);
		backgroundStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderRight(BorderStyle.THIN);
		backgroundStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		backgroundStyle.setBorderTop(BorderStyle.THIN);
		backgroundStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return backgroundStyle;
	}

	private void writeToExcel(NewClientMapping clientMapping, Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue(clientMapping.getSequence());
		cell = row.createCell(1);
		cell.setCellValue(clientMapping.getColHeader());
		cell = row.createCell(2);
		cell.setCellValue(clientMapping.getStdAttribute());
		int recordCounter = 0;
		if ((clientMapping.getClientValues() != null && clientMapping.getClientValues().size() > 0)) {
			for (int i = 3; i < clientMapping.getClientValues().size() + 3; i++) {
				String clientValue = clientMapping.getClientValues().get(recordCounter);
				cell = row.createCell(i);
				cell.setCellValue(clientValue);
				recordCounter++;
			}
		}
	}

	@Override
	public List<NewClientMapping> getNewCampaignClientMappings(String campaignId) {
		List<NewClientMapping> mappings = new ArrayList<NewClientMapping>();
		if (campaignId == null) {
			throw new IllegalArgumentException("CampaignId is mandatory for client mappings.");
		}
		Campaign campaign = campaignService.getCampaign(campaignId);
		if (campaign != null) {
			if (campaign.getClientName() != null && !campaign.getClientName().isEmpty()) {
				mappings = newclientMappingRepository.findByCampaignIdAndClientName(campaignId, campaign.getClientName());
			} else {
				mappings = newclientMappingRepository.findByCampaignIdAndClientName(campaignId, campaign.getOrganizationId());
			}
		}
		return mappings;
	}

}