package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.xtaas.application.service.CampaignService;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.ProspectCallCacheDTO;

@Service
public class ProspectInMemoryCacheServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(ProspectInMemoryCacheServiceImpl.class);

	final private ProspectCallLogServiceImpl prospectCallLogServiceImpl;

	final private CampaignService campaignService;

	final private PropertyService propertyService;

	final private AgentRegistrationService agentRegistrationService;

	final private String thresholdValue;

	final PriorityQueueCacheOperationService priorityQueueCacheOperationService;

	public final Map<String, Integer> thresHoldMap = new HashMap<String, Integer>();

	public final Map<String, String> duplicateJobCheckMap = new ConcurrentHashMap<String, String>();

	public ProspectInMemoryCacheServiceImpl(@Autowired ProspectCallLogServiceImpl prospectCallLogServiceImpl,
			@Autowired CampaignService campaignService, @Autowired PropertyService propertyService,
			@Autowired AgentRegistrationService agentRegistrationService,
			@Value("${caching_threshold}") String thresholdValue,
			@Autowired @Lazy PriorityQueueCacheOperationService priorityQueueCacheOperationService) {
		this.prospectCallLogServiceImpl = prospectCallLogServiceImpl;
		this.campaignService = campaignService;
		this.propertyService = propertyService;
		this.agentRegistrationService = agentRegistrationService;
		this.thresholdValue = thresholdValue;
		this.priorityQueueCacheOperationService = priorityQueueCacheOperationService;
	}

	public void cacheProspectsInMemory() {
		String threadPoolSize = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.PROSPECT_CACHE_THREAD_POOL_SIZE.name(), "10");
		ExecutorService executorService = Executors.newFixedThreadPool(Integer.parseInt(threadPoolSize));
		List<Campaign> campaigns = campaignService.getActiveCampaigns();
		if (campaigns != null && campaigns.size() > 0) {
			for (Campaign activeCampaign : campaigns) {
				Runnable runnable = () -> {
					if (activeCampaign.isEnableProspectCaching()) {
						buildProspectCache(activeCampaign);
					}
				};
				executorService.submit(runnable);
			}
		}
	}

	public void buildProspectCache(Campaign activeCampaign) {
		if (!duplicateJobCheckMap.containsKey(activeCampaign.getId())) {
			int prospectCount = 0;
//			HashMap<String, Integer> campaignActiveAgentCountMap = agentRegistrationService
//					.getCampaignActiveAgentCount();
//			Integer countOfActiveAgents = campaignActiveAgentCountMap.get(activeCampaign.getId());
//			int countOfOnlineAgents = agentRegistrationService.getAvailableAgentsCount(activeCampaign.getId());
			Set<String> registeredAgents = new HashSet<String>();
			ConcurrentHashMap<String, AgentCall> agentMap = agentRegistrationService.getAgentCallMap();
			if (agentMap != null && !agentMap.isEmpty()) {
				for (Map.Entry<String, AgentCall> entry : agentMap.entrySet()) {
					if (entry != null && entry.getValue() != null && entry.getValue().getCampaignId() != null
							&& entry.getValue().getCampaignId().equalsIgnoreCase(activeCampaign.getId())) {
						registeredAgents.add(entry.getKey());
					}
				}
			}
			int countOfOnlineAgents = registeredAgents.size();
			logger.info(" [{}] Online agents found in [{}] campaign.", countOfOnlineAgents, activeCampaign.getName());
			if (countOfOnlineAgents > 0) {
				List<ProspectCallCacheDTO> prospectsInCache = priorityQueueCacheOperationService
						.getCurrentState(activeCampaign.getId());
				if (prospectsInCache != null) {
					prospectCount = prospectsInCache.size();
					int result = thresHoldMap.get(activeCampaign.getId()) != null
							? ((thresHoldMap.get(activeCampaign.getId()) * Integer.parseInt(thresholdValue)) / 100)
							: 0;
					if (prospectCount == 0 || prospectCount < result) {
						loadCache(activeCampaign, countOfOnlineAgents);
					}
				}
			}
		}
	}

	private int getAgentMaxRange(int range, int countOfOnlineAgents) {
		int agentRange = 0;
		for (int i = 1; i < 100; i++) {
			int calculatedRange = range * i;
			if (calculatedRange < countOfOnlineAgents) {
				continue;
			} else {
				agentRange = calculatedRange;
				break;
			}
		}
		logger.info(" Agent range :: [{}], Count of online agents :: [{}]", agentRange, countOfOnlineAgents);
		return agentRange;
	}

	public int getProspectCountFromCache(String campaignId) {
		if (this.thresHoldMap != null && this.thresHoldMap.get(campaignId) != null) {
			return this.thresHoldMap.get(campaignId);
		}
		return 0;
	}

	public void loadCache(Campaign activeCampaign, Integer countOfActiveAgents) {
		duplicateJobCheckMap.put(activeCampaign.getId(), "IN_PROCESS");
		int range = 0;
		if (activeCampaign.getProspectCacheAgentRange() != 0) {
			range = activeCampaign.getProspectCacheAgentRange();
		} else {
			range = 10;
		}
		int prospectPerAgent = 0;
		if (activeCampaign.getProspectCacheNoOfProspectsPerAgent() != 0) {
			prospectPerAgent = activeCampaign.getProspectCacheNoOfProspectsPerAgent();
		} else {
			prospectPerAgent = 800;
		}
		int prospectsToBeCached = (getAgentMaxRange(range, countOfActiveAgents.intValue()) * prospectPerAgent);
		thresHoldMap.put(activeCampaign.getId(), prospectsToBeCached);
		logger.info("==========> Prospects in the map for campaign [{}], campaignId [{}] <==========",
				thresHoldMap.get(activeCampaign.getId()),activeCampaign.getId());
		List<String> excludeStateCodes = prospectCallLogServiceImpl.getStateCodesWithBusinessClosed();
		String strCurrentDateYMD = XtaasDateUtils.getCurrentDateInPacificTZinYMD();
		int dailyCallMaxRetriesLimit = 1;
		dailyCallMaxRetriesLimit = activeCampaign != null && activeCampaign.getDailyCallMaxRetries() > 0
				? activeCampaign.getDailyCallMaxRetries()
				: propertyService.getIntApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DAILY_CALL_MAX_RETRIES.name(), 1);
		int maxCallRetryLimit = 5;
		maxCallRetryLimit = activeCampaign != null && activeCampaign.getCallMaxRetries() > 0
				? activeCampaign.getCallMaxRetries()
				: propertyService
						.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
		Long maxDailyCallRetryCount = Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetriesLimit));
		logger.info(
				"==========> As campaign cache falls below threshold value so reloaded cache of [{}] with [{}] prospects. <==========",
				activeCampaign.getName(), prospectsToBeCached);
		try {
			if (activeCampaign.isEnableProspectCaching()) {
				List<String> openTimeZones = new ArrayList<String>();
				openTimeZones = prospectCallLogServiceImpl.getOpenTimeZones();
				if (activeCampaign.isUseSlice()) {
					prospectCallLogServiceImpl.buildCacheBySlices(activeCampaign, excludeStateCodes, openTimeZones, 0,
							strCurrentDateYMD, dailyCallMaxRetriesLimit, maxCallRetryLimit, maxDailyCallRetryCount,
							prospectsToBeCached);
				} else {
					prospectCallLogServiceImpl.buildCacheByQualityBucket(activeCampaign, excludeStateCodes,
							openTimeZones, strCurrentDateYMD, dailyCallMaxRetriesLimit, maxCallRetryLimit,
							maxDailyCallRetryCount, prospectsToBeCached);
				}
			} else {
				if (activeCampaign.isUseSlice()) {
					prospectCallLogServiceImpl.buildCacheBySlicesOld(activeCampaign, excludeStateCodes, 0,
							strCurrentDateYMD, dailyCallMaxRetriesLimit, maxCallRetryLimit, maxDailyCallRetryCount);
				} else {
					prospectCallLogServiceImpl.buildCacheByQualityBucketOld(activeCampaign, excludeStateCodes,
							strCurrentDateYMD, dailyCallMaxRetriesLimit, maxCallRetryLimit, maxDailyCallRetryCount);
				}
			}
		} catch (Exception e) {
			duplicateJobCheckMap.remove(activeCampaign.getId());
			e.printStackTrace();
		}
	}

}