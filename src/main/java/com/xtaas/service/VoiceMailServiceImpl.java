/**
 * 
 */
package com.xtaas.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.VoiceMailRepository;
import com.xtaas.domain.entity.VoiceMail;

/**
 * @author djain
 *
 */
@Service
public class VoiceMailServiceImpl implements VoiceMailService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(VoiceMailServiceImpl.class);

	@Autowired
	private VoiceMailRepository voicemailRepository;

	@Override
	public void add(VoiceMail voicemail) {
		voicemailRepository.save(voicemail);
	}

	public List<VoiceMail> findByProviderAndUpdatedDate(String provider, String partnerId, Date startDate,
			Date endDate) {
		return voicemailRepository.findByProviderAndUpdatedDate(provider, partnerId, startDate, endDate);
	}
}
