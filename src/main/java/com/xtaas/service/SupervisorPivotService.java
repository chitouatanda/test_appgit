package com.xtaas.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Login;
import com.xtaas.web.dto.SupervisorPivotDTO;

public interface SupervisorPivotService {

	public SupervisorPivotDTO getSupervisorPivots(String campaignId, boolean isLimit);
	
	public SupervisorPivotDTO getSupervisorPivotsForUI(String campaignId, boolean isLimit, String pivotSelectedTab);

	public void downloadPivots(String campaignId, HttpServletRequest request, HttpServletResponse response,
			Boolean downloadFlag) throws IOException;

	public String nukeCallableProspect(MultipartFile file, String campaignId) throws IOException;

	public void sendPivotStats(String campaignId, Login login) throws IOException;

}
