package com.xtaas.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;

public interface PlivoService {

	public String handleVoiceCall(HttpServletRequest request, HttpServletResponse response);

	public String handleProspectCallHangup(HttpServletRequest request, HttpServletResponse response);

	public String handleFallback(HttpServletRequest request, HttpServletResponse response);

	public String handleSyncMachineDetection(HashMap<String, String> paramMap);

	public String handleIncomingCall(HttpServletRequest request, HttpServletResponse response);

	public String handleIncomingMessage(HttpServletRequest request, HttpServletResponse response);

	public String handleVoicemail(HttpServletRequest request, HttpServletResponse response);

	public String handleRecordingCallback(HttpServletRequest request, HttpServletResponse response);

	public Map<String, String> createEndpoint(String username);

	public boolean deleteEndpoint(String endpointId);

	public boolean deleteAllEndpoints();

	public String saveCallMetrics(HttpServletRequest request, HttpServletResponse response);

	public void kickParticipantFromConference(String conferenceName, String callUUID, boolean isConferenceCall);

	public boolean holdParticipantInConference(String conferenceName, String callUUID, boolean isConferenceCall);

	public boolean unholdParticipantInConference(String conferenceName, String callUUID, boolean isConferenceCall);

	public List<PlivoOutboundNumber> purchasePhoneNumbers(PlivoPhoneNumberPurchaseDTO phoneDTO);
	
	public List<PlivoOutboundNumber> purchaseUSPhoneNumbersForOtherCountry(PlivoPhoneNumberPurchaseDTO phoneDTO);

	public Map<String, Integer> getCallStats();

	public Map<String, List<String>> getRecordingUrlsByCallUUID(String callUUID);
	
	public Map<String, String> getPlivoOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO);

	public String handleManualcallCallback(HttpServletRequest request, HttpServletResponse response);
	
	// public void removePcidFromMap(String pcid);

}
