package com.xtaas.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.CampaignServiceImpl;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.DNCList.DNCTrigger;
import com.xtaas.db.entity.EmailBounces;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.EmailBouncesRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.WebHookEventDTO;

@Service
public class EmailBouncesServiceImpl implements EmailBouncesService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignServiceImpl.class);

	@Autowired
	private EmailBouncesRepository emailBouncesRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PCIAnalysisRepository pciAnalysisRepository;

	@Autowired
	private DomoUtilsService domoUtilsService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Override
	public void handleBounceNotification(List<WebHookEventDTO> webHookEvents) {
		for (WebHookEventDTO webHookEvent : webHookEvents) {
			try {
				if (webHookEvent.getEvent().equalsIgnoreCase("unsubscribe")) {
					logger.debug("SendGrid Webhook request for Unsubscribe. Webhook Email details ==> {}",
							webHookEvent.toString());
					addProspectToDNC(webHookEvent);
				}else if(webHookEvent.getEvent().equalsIgnoreCase("Open")) {
					logger.debug("SendGrid Webhook request for Open. Webhook Email details ==> {}",
							webHookEvent.toString());
					updateEmailValidated(webHookEvent);
				}else {
					logger.debug("SendGrid Webhook request for bounce/drop. Webhook Email details ==> {}",
							webHookEvent.toString());
					List<EmailBounces> emailBounceList = new ArrayList<>();
					if (webHookEvent.getProspectCallId() != null) {
						emailBounceList = emailBouncesRepository.findByProspectCallId(webHookEvent.getProspectCallId());
					}
					if (emailBounceList != null && emailBounceList.size() == 0) {
						addEmailBounceInDB(webHookEvent);
					}
				}
			} catch (Exception e) {
				logger.error("Exception in Webhook Email Bounce ==> " + e);
			}
		}
	}

	private void updateEmailValidated(WebHookEventDTO webHookEvent) {
		if (webHookEvent.getCampaignId() != null && webHookEvent.getProspectCallId() != null) {
			ProspectCallLog prospectCallLog = prospectCallLogRepository
					.getProspectByCIDAndPCID(webHookEvent.getCampaignId(), webHookEvent.getProspectCallId());
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getEmail() != null && prospectCallLog
					.getProspectCall().getProspect().getEmail().equalsIgnoreCase(webHookEvent.getEmail())) {
				prospectCallLog.getProspectCall().getProspect().setEmailValidated(true);
				if(prospectCallLog.getProspectCall().getProspect().isDirectPhone()) {
					prospectCallLog.setDataSlice("slice0");
					prospectCallLog.getProspectCall().setDataSlice("slice0");
				}

				prospectCallLogRepository.save(prospectCallLog);

			}

		}
	}

	private void addProspectToDNC(WebHookEventDTO webHookEvent) {
		if (webHookEvent.getCampaignId() != null && webHookEvent.getProspectCallId() != null) {
			ProspectCallLog prospectCallLog = prospectCallLogRepository
					.getProspectByCIDAndPCID(webHookEvent.getCampaignId(), webHookEvent.getProspectCallId());
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getEmail() != null && prospectCallLog
					.getProspectCall().getProspect().getEmail().equalsIgnoreCase(webHookEvent.getEmail())) {
				String phoneNumber = prospectCallLog.getProspectCall().getProspect().getPhone();
				String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
				DNCList dncNumber = new DNCList(normalizedPhoneNumber,
						prospectCallLog.getProspectCall().getProspect().getFirstName(),
						prospectCallLog.getProspectCall().getProspect().getLastName(), DNCTrigger.DIRECT);
				dncNumber.setNote("Email Unsubscribe");
				IDNCNumberDTO idncNumberDTO = new IDNCNumberDTO(dncNumber);
				internalDNCListService.addNumber(idncNumberDTO);
			}
		}
	}

	private void addEmailBounceInDB(WebHookEventDTO webHookEvent) {
		if (webHookEvent.getCampaignId() != null && webHookEvent.getProspectCallId() != null) {
			ProspectCallLog prospectCallLog = prospectCallLogRepository
					.getProspectByCIDAndPCID(webHookEvent.getCampaignId(), webHookEvent.getProspectCallId());
			EmailBounces emailBounces = new EmailBounces();
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getEmail() != null && prospectCallLog
					.getProspectCall().getProspect().getEmail().equalsIgnoreCase(webHookEvent.getEmail())) {
				ProspectCall prospectCall = prospectCallLog.getProspectCall();
				Campaign campaign = campaignRepository.findOneById(prospectCall.getCampaignId());
				Team team = teamRepository.findOneById(campaign.getTeam().getTeamId());
				emailBounces.setCampaignId(prospectCall.getCampaignId());
				emailBounces.setPartnerId(team.getPartnerId());
				emailBounces.setProspectCallId(prospectCall.getProspectCallId());
				emailBounces.setEmailAddress(webHookEvent.getEmail());
				emailBounces.setEmailSentDate(prospectCall.getCallStartTime());
				emailBounces.setProspectInteractionSessionId(prospectCall.getProspectInteractionSessionId());
				emailBounces.setWebHookEvent(webHookEvent);
				emailBounces.setFeedbackID(webHookEvent.getSg_event_id());
				emailBounces.setEmailBounceMessageID(webHookEvent.getSg_message_id());
				emailBounces.setAction(webHookEvent.getEvent());
				emailBounces.setStatus(webHookEvent.getReason());
				emailBouncesRepository.save(emailBounces);
				updateEmailBounceInProspectCallLog(prospectCallLog);
				updateEmailBounceInProspectCallInteraction(prospectCall.getProspectInteractionSessionId());
			}
		}
	}

	private void updateEmailBounceInProspectCallLog(ProspectCallLog prospectCallLog) {
		if (prospectCallLog != null) {
			try {
				prospectCallLog.getProspectCall().setEmailBounce(true);
				prospectCallLogRepository.save(prospectCallLog);
			} catch (Exception e) {
				logger.error(
						"Exception in saving prospectCallLogRepository / prospectCallInteractionRepository for emailBounce ==> "
								+ e);
			}
		}
	}

	private void updateEmailBounceInProspectCallInteraction(String prospectCallInteractionSessionId) {
		List<ProspectCallInteraction> prospectCallInteractionList = prospectCallInteractionRepository
				.findByProspectInteractionSessionId(prospectCallInteractionSessionId);


		if (prospectCallInteractionList.size() > 0) {
			ProspectCallInteraction prospectCallInteraction = prospectCallInteractionList.get(0);
			for (ProspectCallInteraction pCallInteraction : prospectCallInteractionList) {
				if (pCallInteraction.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE)) {
					prospectCallInteraction = pCallInteraction;
				}
			}
			prospectCallInteraction.getProspectCall().setEmailBounce(true);
			prospectCallInteractionRepository.save(prospectCallInteraction);

			Campaign campaign = campaignRepository.findOneById(prospectCallInteraction.getProspectCall().getCampaignId());
			String agentName = null;
			String qaName = null;
			if(prospectCallInteraction.getQaFeedback()!=null && prospectCallInteraction.getQaFeedback().getQaId()!=null){
				Login login = loginRepository.findOneById(prospectCallInteraction.getQaFeedback().getQaId());
				if(login!=null){
					qaName = login.getName();
				}
			}
			if(prospectCallInteraction.getProspectCall()!=null && prospectCallInteraction.getProspectCall().getAgentId()!=null){
				Login login= loginRepository.findOneById(prospectCallInteraction.getProspectCall().getAgentId());
				if(login!=null){
					agentName = login.getName();
				}
			}
			//PCIAnalysis pcian = new PCIAnalysis();
			Pageable pageable = PageRequest.of(0, 100, Direction.DESC, "updatedDate");
			List<PCIAnalysis> pcianList = pciAnalysisRepository.findByProspectCallIdWithSort(prospectCallInteraction.getProspectCall().getProspectCallId(),
					pageable);

			for(PCIAnalysis pcian : pcianList) {
				if(pcian.getStatus().equals(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE.toString())) {
					pcian.toPCIAnalysis(pcian,prospectCallInteraction, agentName, qaName, campaign);
					pciAnalysisRepository.save(pcian);
					domoUtilsService.postPciAnalysisWebhook(pcian);

					break;
				}
			}
		}
	}

	@Override
	public EmailBounces getEmailStatus(String prospectCallId) {
		List<EmailBounces> emailBounces = emailBouncesRepository.findByProspectCallId(prospectCallId);
		EmailBounces emailBounce = new EmailBounces();
		if (emailBounces != null && emailBounces.size() > 0) {
			emailBounce = emailBounces.get(0);
		}
		return emailBounce;
	}

	/*
	 * @Override public void handleBounceNotification(JSONObject emailBounceDetails)
	 * throws ParseException { logger.debug("Emails Notification Bounce Receive..."
	 * +fromEmailAddress(emailBounceDetails)); String isBounceResponse = (String)
	 * emailBounceDetails.get("Subject");
	 * if(isBounceResponse.equals("Test subject")){
	 * logger.debug("Webhook call for URL checking: "); return; }else{ EmailBounces
	 * emailBounces = new EmailBounces(); String prospectCallInteractionSessionId =
	 * getProspectCallInteractionId(emailBounceDetails).trim();
	 * List<ProspectCallInteraction> prospectCallInteractionList =
	 * prospectCallInteractionRepository.findByProspectInteractionSessionId(
	 * prospectCallInteractionSessionId); if (prospectCallInteractionList.size() >
	 * 0) { ProspectCallInteraction prospectCallInteraction =
	 * prospectCallInteractionList.get(0);
	 * logger.debug("ProspectCallInteraction........" +prospectCallInteraction);
	 * Campaign campaign =
	 * campaignRepository.findOneById(prospectCallInteraction.getProspectCall().
	 * getCampaignId()); Team team =
	 * teamRepository.findOneById(campaign.getTeam().getTeamId());
	 * emailBounces.setCampaignId(prospectCallInteraction.getProspectCall().
	 * getCampaignId()); emailBounces.setPartnerId(team.getPartnerId());
	 * emailBounces.setProspectCallId(prospectCallInteraction.getProspectCall().
	 * getProspectCallId());
	 * emailBounces.setEmailAddress(prospectCallInteraction.getProspectCall().
	 * getProspect() != null ?
	 * prospectCallInteraction.getProspectCall().getProspect().getEmail() : "");
	 * emailBounces.setEmailSentDate(prospectCallInteraction.getCreatedDate());
	 * emailBounces.setProspectInteractionSessionId(prospectCallInteraction.
	 * getProspectCall().getProspectInteractionSessionId() == ""? ""
	 * :prospectCallInteraction.getProspectCall().getProspectInteractionSessionId())
	 * ; sendBounceEmailDetails(emailBounceDetails,team,
	 * prospectCallInteractionSessionId); }
	 * emailBounces.setPostmarkBounceMap(emailBounceDetails);
	 * setRegexPostmarkData(emailBounces,emailBounceDetails);
	 * emailBouncesRepository.save(emailBounces);
	 *
	 * }
	 *
	 * }
	 */

	/*
	 * private void setRegexPostmarkData(EmailBounces emailBounces,JSONObject
	 * emailBounceDetails){ for (Object key : emailBounceDetails.keySet()) { String
	 * keyStr = (String)key; if(keyStr.equals("Content")){ // key in postmark
	 * response map it will change according to postmark response map String
	 * keyvalue = (String) emailBounceDetails.get(keyStr);
	 * emailBounces.setFeedbackID(setFeedBackID(keyvalue,XtaasConstants.FEEDBACK_ID)
	 * .trim());
	 * emailBounces.setFromEmailAddress(setRegxData(keyvalue,XtaasConstants.
	 * FROM_EMAIL).trim());
	 * emailBounces.setAction(setRegxData(keyvalue,XtaasConstants.ACTION).trim());
	 * emailBounces.setStatus(setRegxData(keyvalue,XtaasConstants.STATUS).trim());
	 * emailBounces.setEmailBounceMessageID(setRegxData(keyvalue,XtaasConstants.
	 * EMAIL_BOUNCE_MESSAGE_ID).trim()); break; } } }
	 */

	/*
	 * private String setRegxData(String keyvalue, String regxString){ Matcher
	 * matcher = getMatcher(keyvalue, regxString); String strString = null; while
	 * (matcher.find()) { String tempString = matcher.group().split(":")[1];
	 * strString = tempString; } return strString; }
	 *
	 * private String setFeedBackID(String keyvalue, String regxString){ Matcher
	 * matcher = getMatcher(keyvalue, regxString); String strString = null; while
	 * (matcher.find()) { String tempString = matcher.group(); String str =
	 * tempString.substring(12); strString = str; } return strString; }
	 *
	 * private Matcher getMatcher(String keyvalue, String regxString){ Pattern
	 * pattern = Pattern.compile(regxString); Matcher matcher =
	 * pattern.matcher(keyvalue); return matcher; }
	 *
	 * private String fromEmailAddress(JSONObject emailBounceDetails){ String
	 * strString = null; for (Object key : emailBounceDetails.keySet()) { String
	 * keyStr = (String)key; if(keyStr.equals("Content")){ String keyvalue =
	 * (String) emailBounceDetails.get(keyStr); strString
	 * =setRegxData(keyvalue,XtaasConstants.FROM_EMAIL); return strString; } }
	 * return strString; }
	 *
	 * private String getProspectCallInteractionId(JSONObject emailBounceDetails){
	 * String strString = null; for (Object key : emailBounceDetails.keySet()) {
	 * String keyStr = (String)key; if(keyStr.equals("Content")){ String keyvalue =
	 * (String) emailBounceDetails.get(keyStr); strString
	 * =setRegxData(keyvalue,XtaasConstants.EMAIL_BOUNCE_MESSAGE_ID); return
	 * strString; } } return strString; }
	 */

	// This method is used for to send bounce email details to AssetDeliveryEmail
	/*
	 * private void sendBounceEmailDetails(JSONObject emailBounceDetails, Team
	 * team,String prospectCallInteractionSessionId){ Partner partner =
	 * partnerRepository.findOneById(team.getPartnerId()); String subject ="BOUNCED - "+
	 * (String) emailBounceDetails.get("Email") +"  " +(String)
	 * emailBounceDetails.get("Subject"); StringBuffer message = new
	 * StringBuffer(" "); Iterator itr = emailBounceDetails.entrySet().iterator();
	 * while(itr.hasNext()){ Map.Entry pair = (Map.Entry)itr.next();
	 * message.append(pair.getKey().toString() +" : " +pair.getValue().toString()
	 * +" <br/>"); } List<String> to = new ArrayList<String>(); if(partner != null
	 * && partner.getAssetDeliveryEmail() != null){
	 * to.add(partner.getAssetDeliveryEmail()); }else{
	 * to.add("assets@xtaascorp.com"); } EmailMessage emailMessage = new
	 * EmailMessage(subject,message.toString());
	 * emailMessage.setFromEmail("support+asset@xtaascorp.com");
	 * emailMessage.setTo(to);
	 * XtaasEmailUtils.sendEmail(emailMessage,prospectCallInteractionSessionId); }
	 */

}
