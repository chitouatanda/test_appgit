package com.xtaas.service;

import com.xtaas.db.entity.ApplicationProperty;
import com.xtaas.valueobjects.KeyValuePair;

import java.util.List;

/**
 * @author djain
 *
 */
public interface PropertyService {
	
	public void addApplicationProperty(ApplicationProperty property);
	
	public String getApplicationPropertyValue(String propertyName);
	
	public String getApplicationPropertyValue(String propertyName, String defaultValue);
	
	public boolean getBooleanApplicationPropertyValue(String propertyName, boolean defaultValue);
	
	public int getIntApplicationPropertyValue(String propertyName, int defaultValue);
	
	public long getLongApplicationPropertyValue(String propertyName, long defaultValue);
	
	public void loadApplicationPropertyCacheForcefully();

	public List<KeyValuePair<String, Double>> getDataBuyRateLimiterValue();

}
