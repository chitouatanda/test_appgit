package com.xtaas.service;

import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.domain.entity.Campaign;

public interface ZoomInfoSearchService {	
	  public Prospect getZoomProspect(Prospect prospect);

	public Prospect getCampaignZoomProspect(Prospect prospect, Campaign campaign);
}
