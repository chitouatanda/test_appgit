package com.xtaas.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class DialerService {

	private static final Logger logger = LoggerFactory.getLogger(DialerService.class);

	@Autowired
	private RestClientService restClientService;

	private final String URL = "/api/plivo/call/status/";

	private final String telnyxURL = "/api/telnyx/call/status/";

	private final String signalwireURL = "/api/signalwire/call/status/";

	@Async
	public void removeAnsweredCallSidFromDialer(String callSid) {
		logger.debug("Removing CallSid [{}] from dialer cache.", callSid);
		StringBuilder dialerUrl = new StringBuilder();
		dialerUrl.append(ApplicationEnvironmentPropertyUtils.getDialerUrl()).append(URL).append(callSid);
		HashMap<String, String> headers = new HashMap<String, String>();
		restClientService.makeGetRequest(dialerUrl.toString(), headers);
	}

	@Async
	public void removeTelnyxCallSidFromDialer(String callSid) {
		logger.debug("Removing CallSid [{}] from dialer cache.", callSid);
		StringBuilder dialerUrl = new StringBuilder();
		dialerUrl.append(ApplicationEnvironmentPropertyUtils.getDialerUrl()).append(telnyxURL).append(callSid);
		HashMap<String, String> headers = new HashMap<String, String>();
		restClientService.makeGetRequest(dialerUrl.toString(), headers);
	}

	@Async
	public void removeSignalwireAnsweredCallSidFromDialer(String callSid) {
		logger.debug("Removing CallSid [{}] from dialer cache.", callSid);
		StringBuilder dialerUrl = new StringBuilder();
		dialerUrl.append(ApplicationEnvironmentPropertyUtils.getDialerUrl()).append(signalwireURL).append(callSid);
		HashMap<String, String> headers = new HashMap<String, String>();
		restClientService.makeGetRequest(dialerUrl.toString(), headers);
	}

}
