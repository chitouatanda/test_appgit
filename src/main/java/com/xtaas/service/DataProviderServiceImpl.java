package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.DataProvider;
import com.xtaas.db.repository.DataProviderRepository;

@Service
public class DataProviderServiceImpl implements DataProviderService {

	@Autowired
	private DataProviderRepository dataProviderRepository;

	@Override
	public List<String> getXtaasDataProvider() {
		List<String> dpList = new ArrayList<String>();
		List<DataProvider> dbDataProviderList = dataProviderRepository.findAll();
		if(dbDataProviderList != null && dbDataProviderList.size() > 0) {
			for (DataProvider dp : dbDataProviderList) {
				dpList.add(dp.getDataProvider());
			}
		}
		return dpList;
	}

	@Override
	public Map<String, String> getXtaasDataProviderByIndex() {
		Map<String, String> dataProviderByIndex = new HashMap<String, String>();
		List<DataProvider> dbDataProviderList = dataProviderRepository.findAll();
		if(dbDataProviderList != null && dbDataProviderList.size() > 0) {
			for (DataProvider dp : dbDataProviderList) {
				dataProviderByIndex.put(dp.getDataProvider(),"Xtaas Data Source "+dp.getSourceIndex());
			}
		}
		return dataProviderByIndex;
	}
}