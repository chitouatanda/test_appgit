package com.xtaas.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.web.dto.AgentCampaignDTO;

public interface AgentRegistrationService {

	public void manageAgent(String agentId, String agentType,DialerMode campaignType, AgentStatus agentStatus, AgentCampaignDTO agentCampaign);

	void setProspectByCallSid(String callSid, ProspectCall prospectCall);

	void unsetProspectByCallSid(String callSid);

	public AgentCall getAgentCall(String agentId);
	
	public void setAgentCall(String agentId, AgentCall agentCall);

	AgentCall allocateAgent(String callSid);

	void unallocateAgent(AgentCall agentCall);

	ProspectCall getProspectByCallSid(String callSid);

	Collection<AgentCall> getAgentCallList();

	int getAvailableAgentsCount(String campaignId);

	AgentCall getAvailableAgent(String campaignId);

	HashMap<String, Integer> getCampaignActiveAgentCount();

	public HashMap<String, Integer> getCampaignBusyAgentCount();

	void registerHeartbeat(String agentId, AgentStatus newStatus);

	public AgentCall allocateAgentFromUI(String prospectCallSid, String agentId, String dialerMode, String agentType);

	public ConcurrentHashMap<String, AgentCall> getAgentCallMap();
	
	public void unregisterAgent(AgentCall agentCall);

}
