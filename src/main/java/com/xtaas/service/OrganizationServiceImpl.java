package com.xtaas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.valueobject.OrganizationStatus;
import com.xtaas.domain.valueobject.OrganizationTypes;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.CampaignDefaultSetting;
import com.xtaas.web.dto.OrganizationDTO;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	private static final Logger logger = LoggerFactory
			.getLogger(OrganizationServiceImpl.class);

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;
	
	private Map<String, Organization> organizationsInMemory = new HashMap<String, Organization>();
	
	@Override
	public Organization findOne(String organizationId) {
		if (organizationsInMemory.isEmpty()) {
			storeOrganizationsInMemory();
		}
		return getOrganizationFromMemory(organizationId);
	}

	@Override
	public void updateOrganization(Organization organization) {
		organizationRepository.save(organization);

	}

	@Override
	public Organization createOrganization(OrganizationDTO organizationDTO) throws DuplicateKeyException{
		Organization organization = organizationDTO.toOrganization();
		organization.setId(organization.getName().toUpperCase());
		organization.setAgency(false);
		organization.setOrganizationEmail(organizationDTO.getOrganizationEmail());
		organization.setReportingEmail(organizationDTO.getReportingEmail());
		organization.setStatus(OrganizationStatus.ACTIVE.name());
		organization.setOrganizationType(OrganizationTypes.CONTACTCENTER);
		organization.setUnHideSocialLink(false);
		CampaignMetaData campaignMetaData = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
		if(campaignMetaData != null) {
			organization.setCampaignDefaultSetting(setCampaignDefaultSetting(campaignMetaData));
		}
		Organization savedOrg = organizationRepository.save(organization);
		logger.debug("Organization [{}] created successfully.", organization.getName());
		return savedOrg;
	}
	
	public CampaignDefaultSetting setCampaignDefaultSetting(CampaignMetaData campaignMetaData) {
		CampaignDefaultSetting campaignDefaultSetting = new CampaignDefaultSetting();
		campaignDefaultSetting.setEnableMachineAnsweredDetection(campaignMetaData.isEnableMachineAnsweredDetection());
		campaignDefaultSetting.setDailyCallMaxRetries(campaignMetaData.getDailyCallMaxRetries()); 
		campaignDefaultSetting.setCallMaxRetries(campaignMetaData.getCallMaxRetries()); 
		campaignDefaultSetting.setHidePhone(campaignMetaData.isHidePhone()); 
		campaignDefaultSetting.setHideEmail(campaignMetaData.isHideEmail());
		campaignDefaultSetting.setQaRequired(campaignMetaData.isQaRequired());
		campaignDefaultSetting.setSipProvider(campaignMetaData.getSipProvider());
		campaignDefaultSetting.setSupervisorAgentStatusApprove(campaignMetaData.isSupervisorAgentStatusApprove());
		campaignDefaultSetting.setRetrySpeedEnabled(campaignMetaData.isRetrySpeedEnabled());
		campaignDefaultSetting.setLocalOutboundCalling(campaignMetaData.isLocalOutboundCalling());
		campaignDefaultSetting.setLeadsPerCompany(campaignMetaData.getLeadsPerCompany());
		campaignDefaultSetting.setDuplicateCompanyDuration(campaignMetaData.getDuplicateCompanyDuration());
		campaignDefaultSetting.setDuplicateLeadsDuration(campaignMetaData.getDuplicateLeadsDuration());
		campaignDefaultSetting.setRetrySpeed(campaignMetaData.getRetrySpeed());
		campaignDefaultSetting.setRecordProspect(campaignMetaData.getRecordProspect());
		campaignDefaultSetting.setMultiProspectCalling(campaignMetaData.isMultiProspectCalling());
		campaignDefaultSetting.setEmailSuppressed(campaignMetaData.isEmailSuppressed());
		campaignDefaultSetting.setCallControlProvider(campaignMetaData.getCallControlProvider());
		campaignDefaultSetting.setUseSlice(campaignMetaData.isUseSlice());
		campaignDefaultSetting.setNotConference(campaignMetaData.isNotConference());
		campaignDefaultSetting.setABM(campaignMetaData.isABM());
		campaignDefaultSetting.setLeadIQ(campaignMetaData.isLeadIQ());
		campaignDefaultSetting.setSimpleDisposition(campaignMetaData.isSimpleDisposition());
		campaignDefaultSetting.setCallSpeedPerMinPerAgent(campaignMetaData.getCallSpeedPerMinPerAgent());
		campaignDefaultSetting.setCampaignSpeed(campaignMetaData.getCampaignSpeed());
		campaignDefaultSetting.setLeadSortOrder(campaignMetaData.getLeadSortOrder());
		campaignDefaultSetting.setIndirectBuyStarted(campaignMetaData.isIndirectBuyStarted());
		campaignDefaultSetting.setMdFiveSuppressionCheck(campaignMetaData.isMdFiveSuppressionCheck());
		campaignDefaultSetting.setCallableEvent(campaignMetaData.getCallableEvent());
		campaignDefaultSetting.setEnableProspectCaching(campaignMetaData.isEnableProspectCaching());
		campaignDefaultSetting.setRemoveMobilePhones(campaignMetaData.isRemoveMobilePhones());
		campaignDefaultSetting.setProspectCacheAgentRange(campaignMetaData.getProspectCacheAgentRange());
		campaignDefaultSetting.setProspectCacheNoOfProspectsPerAgent(campaignMetaData.getProspectCacheNoOfProspectsPerAgent());
		campaignDefaultSetting.setAutoModeCallProvider(campaignMetaData.getAutoModeCallProvider());
		campaignDefaultSetting.setManualModeCallProvider(campaignMetaData.getManualModeCallProvider());
		campaignDefaultSetting.setRevealEmail(campaignMetaData.isRevealEmail());
		campaignDefaultSetting.setUpperThreshold(campaignMetaData.getUpperThreshold());
		campaignDefaultSetting.setLowerThreshold(campaignMetaData.getLowerThreshold());
		// campaignDefaultSetting.setTelnyxHold(campaignMetaData.isTelnyxHold());
		campaignDefaultSetting.setHideNonSuccessPII(campaignMetaData.isHideNonSuccessPII());
		// campaignDefaultSetting.setHoldAudioUrl(campaignMetaData.getHoldAudioUrl());
		List<KeyValuePair<String, Integer>> orgDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
		if (campaignMetaData.getDataSourcePriority() != null && campaignMetaData.getDataSourcePriority().size() > 0) {
			for (KeyValuePair<String,Integer> keyValuePair : campaignMetaData.getDataSourcePriority()) {
				orgDataSourcePriority.add(keyValuePair);
			}
			campaignDefaultSetting.setDataSourcePriority(orgDataSourcePriority);
		}
		
		List<KeyValuePair<String, Integer>> orgManualDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
		if (campaignMetaData.getManualDataSourcePriority() != null && campaignMetaData.getManualDataSourcePriority().size() > 0) {
			for (KeyValuePair<String,Integer> keyValuePair : campaignMetaData.getManualDataSourcePriority()) {
				orgManualDataSourcePriority.add(keyValuePair);
			}
			campaignDefaultSetting.setDataSourcePriority(orgManualDataSourcePriority);
		}
		
		return campaignDefaultSetting;
	}

	@Override
	public void storeOrganizationsInMemory() {
		if (this.organizationsInMemory == null || this.organizationsInMemory.size() == 0) {
			List<Organization> organizationFromDB = organizationRepository.findAll();
			if (organizationFromDB != null && organizationFromDB.size() > 0) {
				for (Organization organization : organizationFromDB) {
					organizationsInMemory.put(organization.getId(), organization);
				}
			}
		}
	}

	@Override
	public Organization getOrganizationFromMemory(String organizationId) {
		if (this.organizationsInMemory.containsKey(organizationId)) {
			return organizationsInMemory.get(organizationId);
		}
		return null;
	}

	@Override
	public Boolean clearOrganizationsInMemory() {
		this.organizationsInMemory = new HashMap<String, Organization>();
		return true;
	}
}