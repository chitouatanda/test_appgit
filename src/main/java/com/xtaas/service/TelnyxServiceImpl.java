package com.xtaas.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.application.service.AgentConferenceService;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.application.service.ProspectCallService;
import com.xtaas.application.service.TelnyxOutboundNumberService;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.AgentConference;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.VoiceMail.VoiceMessageStatus;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.BillingGroupOutputDTO;
import com.xtaas.web.dto.CallInfoDTO;
import com.xtaas.web.dto.CountryWiseDetailsDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;
import com.xtaas.web.dto.TelnyxAnswerCallRequestDTO;
import com.xtaas.web.dto.TelnyxBillingGroupDTO;
import com.xtaas.web.dto.TelnyxCallDTO;
import com.xtaas.web.dto.TelnyxCallStatusDTO;
import com.xtaas.web.dto.TelnyxCallWebhookDTO;
import com.xtaas.web.dto.TelnyxCallWebhookDTOPayload;
import com.xtaas.web.dto.TelnyxCallWebhookDTORecordingUrls;
import com.xtaas.web.dto.TelnyxConnectionRequestDTO;
import com.xtaas.web.dto.TelnyxCreateCallControlAppDTO;
import com.xtaas.web.dto.TelnyxCreateConferenceDTO;
import com.xtaas.web.dto.TelnyxDTMFRequest;
import com.xtaas.web.dto.TelnyxDTMFRequestDTO;
import com.xtaas.web.dto.TelnyxHangupRequest;
import com.xtaas.web.dto.TelnyxHoldConferenceDTO;
import com.xtaas.web.dto.TelnyxPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.TelnyxPhoneOrderRequestDTO;
import com.xtaas.web.dto.TelnyxPhoneOrderRequestPhoneNumber;
import com.xtaas.web.dto.TelnyxPhoneOrderResponseDTO;
import com.xtaas.web.dto.TelnyxPhoneOrderResponsePhoneNumber;
import com.xtaas.web.dto.TelnyxRecordingUrlObjForMap;
import com.xtaas.web.dto.TelnyxSIPInviteObject;
import com.xtaas.web.dto.TelnyxSpeakTextRequestDTO;
import com.xtaas.web.dto.TelnyxStartRecordingRequestDTO;
import com.xtaas.web.dto.TelnyxTransferCallRequest;
import com.xtaas.web.dto.TelnyxUpdatePhoneSettingsRequestDTO;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class TelnyxServiceImpl implements TelnyxService {

	private final Logger logger = LoggerFactory.getLogger(TelnyxServiceImpl.class);

	private static final String COUNTRYDIALCODE_US = "+1";

	private static final String COUNTRYDIALCODE_US_VIVA = "1";
	
	private static final String holdAudioUrlDefault = "https://xtaasuploadfiles.s3-us-west-2.amazonaws.com/Silent.mp3";

	private ConcurrentHashMap<String, TelnyxCallStatusDTO> telnyxProspectCallInfoMap = new ConcurrentHashMap<String, TelnyxCallStatusDTO>();

	private ConcurrentHashMap<String, String> telnyxCallLegAndControlIdMap = new ConcurrentHashMap<String, String>();

	private ConcurrentHashMap<String, String> telnyxAlternateProspectIdMap = new ConcurrentHashMap<String, String>();

	private ConcurrentHashMap<String, TelnyxRecordingUrlObjForMap> telnyxCallLegAndRecordingUrlMap = new ConcurrentHashMap<String, TelnyxRecordingUrlObjForMap>();

	private ConcurrentHashMap<String, String> telnyxAgentIdCallLegMap = new ConcurrentHashMap<String, String>();
	
	private HashMap<String, Date> telnyxAMDMap = new HashMap<String, Date>();

	private List<Integer> waitTimeForRetries = new ArrayList<>();

	@Autowired
	HttpService httpService;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private VoiceMailService voicemailService;

	@Autowired
	private TelnyxOutboundNumberService telnyxOutboundNumberService;

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private AgentConferenceService agentConferenceService;

	@Autowired
	private DialerService dialerService;

	@Autowired
	private ProspectCallService prospectCallService;

	@Autowired
	private ProspectCallLogService prospectLogService;

	@Autowired
	private CallLogService callLogService;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();

		headers.add("accept", "application/json");
		headers.add("content-type", "application/json");
		headers.add("Authorization", "Bearer KEY0172084AA30002F82C7A7F8E2BCE2D7E_BgM5PHypNaVMLkSmhD4UrE");
		return headers;
	}

	private int getWaitTimeForRetries(int retryCount) {
		if (waitTimeForRetries.isEmpty()) {
			waitTimeForRetries.add(0, 5000);
			waitTimeForRetries.add(1, 10000);
			waitTimeForRetries.add(2, 15000);
		}
		int waitTime = waitTimeForRetries.get(retryCount);
		return waitTime;
	}

	private CallInfoDTO decodeCallInfo(String clientState) {
		try {
			CallInfoDTO callInfo = new ObjectMapper().readValue(new String(Base64.getDecoder().decode(clientState)),
					CallInfoDTO.class);
			return callInfo;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private String encodeCallInfo(CallInfoDTO callInfo) {
		try {
			String encodedString = Base64.getEncoder()
					.encodeToString(new ObjectMapper().writeValueAsString(callInfo).getBytes());
			return encodedString;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	private boolean checkIfProspect(String toNumber) {
		List<String> allMatches = new ArrayList<String>();
		Matcher m = Pattern.compile("^sip:[+0-9]{5}|^[+0-9]{5}").matcher(toNumber);
		while (m.find()) {
			allMatches.add(m.group());
		}
		if (allMatches.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	private String getAgentName(String sipNumber) {
		Pattern pattern = Pattern.compile("sip:(.*?)@sip.telnyx.com", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(sipNumber);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			return "";
		}

	}

	@Override
	public String handleVoiceCall(TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		String eventType = telnyxCallWebhookDTO.getData().getEvent_type();
		String eventId = telnyxCallWebhookDTO.getData().getId();
		if (telnyxCallWebhookDTO.getData().getPayload() != null) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
				CallInfoDTO callInfo = decodeCallInfo(telnyxCallWebhookDTO.getData().getPayload().getClient_state());
				if (callInfo != null && callInfo.getCommandStartTime() != null) {
					logger.info("TelnyxServiceImpl========>Event type : " + eventType + " , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()));	
				}
			}
		}		
		if (eventId != null && prospectCacheServiceImpl.isPresentInTelnyxEventIds(eventId)) {
			logger.info("Duplicate Event type Ignored : [{}]" + eventType);
			return "OK";
		} else {
			logger.info("Event type : [{}]" + eventType + " CallLegId : " + telnyxCallWebhookDTO.getData().getPayload().getCall_leg_id());
			prospectCacheServiceImpl.addInTelnyxEventIds(eventId);
		}
		if (eventType.equalsIgnoreCase("call.initiated")) {
			if (telnyxCallWebhookDTO.getData().getPayload().getDirection().equalsIgnoreCase("incoming")) {
				String callControlId = handleIncomingCall(telnyxCallWebhookDTO);
			} else {
				TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
				if (telnyxCallLegAndControlIdMap != null
						&& telnyxCallLegAndControlIdMap.containsKey(payloadInfo.getCall_leg_id())) {
					telnyxCallLegAndControlIdMap.replace(payloadInfo.getCall_leg_id(), payloadInfo.getCall_control_id());
				} else {
					telnyxCallLegAndControlIdMap.put(payloadInfo.getCall_leg_id(), payloadInfo.getCall_control_id());
				}
			}
		} else if (eventType.equalsIgnoreCase("call.speak.ended")) {
			startRecording(telnyxCallWebhookDTO.getData().getPayload(), true);
		} else if (eventType.equalsIgnoreCase("call.bridged")) {
			startRecording(telnyxCallWebhookDTO.getData().getPayload(), false);
		} else if (eventType.equalsIgnoreCase("call.recording.saved")) {
			if (telnyxCallWebhookDTO.getData().getPayload().getClient_state() != null
					&& telnyxCallWebhookDTO.getData().getPayload().getClient_state() != "") {
				CallInfoDTO callInfo = decodeCallInfo(telnyxCallWebhookDTO.getData().getPayload().getClient_state());
				if (callInfo.getNextCallCommand() != null
						&& !callInfo.getNextCallCommand().equalsIgnoreCase("StartRecording")) {
					saveVoiceMailRecording(telnyxCallWebhookDTO);
					return "OK";
				}
			}
			saveRecording(telnyxCallWebhookDTO);

			// } else if (eventType.equalsIgnoreCase("call.machine.detection.ended")) {
			// // Hangup call
			// handleProspectCallHangup(telnyxCallWebhookDTO);
		} else if (eventType.equalsIgnoreCase("call.answered")) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String toNumber = payloadInfo.getTo();
			String fromNumber = payloadInfo.getFrom();
			String callControlId = payloadInfo.getCall_control_id();
			String callLegId = payloadInfo.getCall_leg_id();
			String connectionId = payloadInfo.getConnection_id();

			logger.info("callControlId [{}]: " + callControlId);
			logger.info("callLegId : [{}]" + callLegId);
			boolean isProspect = true;
			if (toNumber != null) {
				isProspect = checkIfProspect(toNumber);
			}

			if (!isProspect) {
				if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
					CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
					String dialerMode = callInfo.getDialerMode();
					String agentType = callInfo.getAgentType();
					String agentId = callInfo.getAgentId();
					String pcid = callInfo.getPcid();
					// String nextCommand = callInfo.getNextCallCommand();
					logger.info("pcid : [{}]" + pcid);
					if (callInfo != null && callInfo.getCommandStartTime() != null) {
						logger.info("TelnyxServiceImpl========>Event type : call.answered , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()) + " pcid : [{}]" + pcid);
					}
					if (dialerMode != null && (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode))
							|| (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode))) && agentType != null
									&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
						// handle manual call answered
						callInfo.setNextCallCommand("StartRecording");
						callInfo.setCommandStartTime(new Date());
						String newClientState = encodeCallInfo(callInfo);
						String sessionId = payloadInfo.getCall_session_id();
						if (telnyxAgentIdCallLegMap.containsKey(callInfo.getAgentId())) {
							telnyxAgentIdCallLegMap.replace(callInfo.getAgentId(), callLegId);
						} else {
							telnyxAgentIdCallLegMap.put(callInfo.getAgentId(), callLegId);
						}
						prospectCacheServiceImpl.addInTelnyxManualSessionIdCallLegIdMap(sessionId, callLegId);
						handleManualDialCall(payloadInfo, callInfo.getTo(), newClientState, callInfo.getCampaignId(), callInfo.getPcid(), agentId);
						return "OK";
					}
				}
				createConference(payloadInfo);
			} else {
				String status = "answered";
				saveProspectCallStatusDetailsInMap(callControlId, status, callLegId);
				if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
					CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
					String dialerMode = callInfo.getDialerMode();
					String agentType = callInfo.getAgentType();
					String pcid = callInfo.getPcid();
					String campaignId = callInfo.getCampaignId();
					boolean enableMachineAnsweredDetection = callInfo.isMachineAnsweredDetection();
					boolean autoMachineHangup = callInfo.isAutoMachineHangup();
					if (dialerMode != null && (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode))
							|| (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode))) && agentType != null
									&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
						// handle manual call answered
						removeBlinkerFromDialButton(payloadInfo);
						return "OK";
					} else {
						if (enableMachineAnsweredDetection) {
							if (!telnyxAMDMap.containsValue(callLegId)) {
								telnyxAMDMap.put(payloadInfo.getCall_leg_id(), new Date());
							}
						}
						if (enableMachineAnsweredDetection && autoMachineHangup) {
							
						} else {
							logger.info("TelnyxServiceImpl========>Event type : call.answered , Call LegId : " + payloadInfo.getCall_leg_id() + ", pcid : [{}]" + pcid + ", CampaignId : " + campaignId);
							handleDialerProspectCall(payloadInfo);
						}
					}
				} else {
					removeBlinkerFromDialButton(payloadInfo);
					return "OK";
				}
			}
		} else if (eventType.equalsIgnoreCase("conference.created")) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String callControlId = payloadInfo.getCall_control_id();
			String callLegId = payloadInfo.getCall_leg_id();
			String conferenceId = payloadInfo.getConference_id();
			if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
				CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
				if (callInfo != null && callInfo.getCommandStartTime() != null) {
					logger.info("TelnyxServiceImpl========>Event type : conference.created , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()));
				}
				String campaignId = callInfo.getCampaignId();
				if (campaignId != null) {
					Campaign campaign = campaignService.getCampaign(campaignId);
					if (campaign.isTelnyxHold()) {
						String holdAudioUrl;
						if (campaign.getHoldAudioUrl() != null && !campaign.getHoldAudioUrl().isEmpty()) {
							holdAudioUrl = campaign.getHoldAudioUrl();
						} else {
							holdAudioUrl = holdAudioUrlDefault;
						}
						holdAgentCall(conferenceId, callControlId, holdAudioUrl);
					}
				}
				String agentId = callInfo.getAgentId();
				logger.info("TelnyxServiceImpl========>Conference created for AgentId : " + agentId + ", CallLegId : " + callLegId + ", CampaignId : " + campaignId);
				registerAgent(callInfo);
				saveAgentConferenceDetails(agentId, conferenceId, callControlId, callLegId);
			}
			// } else {
			// if (payloadInfo.getClient_state() != null &&
			// !payloadInfo.getClient_state().isEmpty()) {
			// CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
			// String dialerMode = callInfo.getDialerMode();
			// String agentType = callInfo.getAgentType();
			// if (dialerMode != null &&
			// (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode)) ||
			// DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode)) ||
			// DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode)) ||
			// (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode))) &&
			// agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
			// // handle manual dial
			// return "OK";
			// }
			// }
			// }
		} else if (eventType.equalsIgnoreCase("conference.ended")) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String conferenceId = payloadInfo.getConference_id();
			AgentConference agentConferencesFromDB = agentConferenceService.findByConferenceId(conferenceId);
			if (agentConferencesFromDB != null && payloadInfo.getClient_state() != null
					&& !payloadInfo.getClient_state().isEmpty()) {
				CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
				unRegisterAgent(callInfo);
				logger.info("Event type : [{}], Agent [{}] unregistered from map.", eventType, callInfo.getAgentId());
			} else {
				logger.info("Event type : [{}], ConferenceId not found in DB, CallLegId [{}]", eventType, payloadInfo.getCall_leg_id());
			}
		} else if (eventType.equalsIgnoreCase("call.hangup")) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String toNumber = payloadInfo.getTo();
			boolean isProspect = true;
			if (toNumber != null) {
				isProspect = checkIfProspect(toNumber);
			}
			if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
				CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
				if (callInfo != null && callInfo.getCommandStartTime() != null) {
					logger.info("TelnyxServiceImpl========>Event type : call.hangup , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()) + " , hangup source : " + payloadInfo.getHangup_source());
				}
			}
			if (!isProspect) {
//				if (payloadInfo.getHangup_cause().equalsIgnoreCase("not_found") && payloadInfo.getHangup_source().equalsIgnoreCase("unknown")) {
//					retryCallingAgentForConferenceCreation(payloadInfo);
//				}
				if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
					CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
					String dialerMode = callInfo.getDialerMode();
					String agentType = callInfo.getAgentType();
					if (dialerMode != null && (DialerMode.PREVIEW.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(dialerMode))
							|| DialerMode.RESEARCH.equals(DialerMode.valueOf(dialerMode))
							|| (DialerMode.HYBRID.equals(DialerMode.valueOf(dialerMode))) && agentType != null
									&& agentType.equalsIgnoreCase(agentSkill.MANUAL.name()))) {
						updateHangupCallStatusToDialer(payloadInfo);
						return "Ok";
					}
					if (callInfo != null && !callInfo.isConferenceInitiated()) {
						retryCallingAgentForConferenceCreation(payloadInfo);
						return "Ok";
					} 
					logger.info("Event Type : [{}], isConferenceInitiated : [{}], CalllegId : [{}], Agent Id : [{}] ", eventType, callInfo.isConferenceInitiated(), payloadInfo.getCall_leg_id(), callInfo.getAgentId());
				}
			} else {
				updateHangupCallStatusToDialer(payloadInfo);
			}
		} else if (eventType.equalsIgnoreCase("conference.participant.left")) {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String conferenceId = payloadInfo.getConference_id();
			AgentConference agentConferenceObj = agentConferenceService.findByConferenceId(conferenceId);
			if (agentConferenceObj != null && !agentConferenceObj.getCallControlId().equalsIgnoreCase(payloadInfo.getCall_control_id())) {
				if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
					CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
					String campaignId = callInfo.getCampaignId();
					if (campaignId != null) {
						Campaign campaign = campaignService.getCampaign(campaignId);
						if (campaign.isTelnyxHold()) {
							String holdAudioUrl;
							if (campaign.getHoldAudioUrl() != null && !campaign.getHoldAudioUrl().isEmpty()) {
								holdAudioUrl = campaign.getHoldAudioUrl();
							} else {
								holdAudioUrl = holdAudioUrlDefault;
							}
							holdAgentCall(conferenceId, agentConferenceObj.getCallControlId(), holdAudioUrl);
						}
					}
				}
			}
		} else if (eventType.equalsIgnoreCase("call.machine.detection.ended")) {
			String campaignId = null;
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			prospectLogService.saveCallDetectedByInProspect(payloadInfo.getCall_leg_id(), payloadInfo.getResult(),
					getAMDTimeDiffInSeconds(payloadInfo.getCall_leg_id()));
			if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
				CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
				campaignId = callInfo.getCampaignId();
			}
			handleAnsweringMachineModes(campaignId, payloadInfo);
			removeCallLegIdFromAMDMap(payloadInfo.getCall_leg_id());
		}
		return "OK";
	}

	private void removeCallLegIdFromAMDMap(String callLegId) {
		if (callLegId != null) {
			if (telnyxAMDMap.containsKey(callLegId)) {
				telnyxAMDMap.remove(callLegId);
			}
		}
	}
	
	@SuppressWarnings("unlikely-arg-type")
	private long getAMDTimeDiffInSeconds(String call_leg_id) {
		long seconds = 0;
		if (call_leg_id != null && !call_leg_id.isEmpty()) {
			for (Entry<String, Date> entry : telnyxAMDMap.entrySet()) {
				if (call_leg_id.equals(entry.getKey())) {
					Date timeFromMap = entry.getValue();
					Date now = new Date();
					seconds = (now.getTime() - timeFromMap.getTime()) / 1000;
					break;
				}
			}
		}
		return seconds;
	}

	private void handleAnsweringMachineModes(String campaignId,TelnyxCallWebhookDTOPayload payloadInfo) {
		if (campaignId != null && !campaignId.isEmpty()) {
			Campaign campaignFromDB = campaignService.getCampaign(campaignId);
			if (campaignFromDB != null) {
				// If autoMachineHangup flag is true then hangup the call and stamp the prospect as CALL_MACHINE_ANSWERED.
				if (campaignFromDB.isAutoMachineHangup()) {
					if (payloadInfo.getResult().equalsIgnoreCase("machine")) {
						handleCallHangup(payloadInfo.getCall_leg_id());
						prospectLogService.updateCallStatus(null, ProspectCallStatus.CALL_MACHINE_ANSWERED,
								payloadInfo.getCall_leg_id(), 0, null);
					} else {
						handleDialerProspectCall(payloadInfo);
					}
				}
				// If autoMachineHangup flag is false then send the message to the agent.
				if (!campaignFromDB.isAutoMachineHangup()) {
					String agentId = getAgentIdFromMap(payloadInfo.getCall_leg_id());
					if (agentId != null && !agentId.isEmpty()) {
						PusherUtils.pushMessageToUser(agentId, XtaasConstants.AMD_DETECTED_EVENT, "");
					}
				}
			}
		}
	}

	private void registerAgent(CallInfoDTO callInfo) {
		AgentCampaignDTO agentCampaign = agentService.getAgentCampaign(callInfo.getCampaignId());
		agentRegistrationService.manageAgent(callInfo.getAgentId(), "AUTO", agentCampaign.getDialerMode(), AgentStatus.ONLINE, agentCampaign);		
	}
	
	private long getTimeDiffrenceInSeconds(Date startTime, Date endTime) {
		return XtaasDateUtils.getTimeInSeconds(endTime) - XtaasDateUtils.getTimeInSeconds(startTime); 
	}

	private void unRegisterAgent(CallInfoDTO callInfo) {
		AgentCampaignDTO agentCampaign = agentService.getAgentCampaign(callInfo.getCampaignId());
		agentRegistrationService.manageAgent(callInfo.getAgentId(), "AUTO", agentCampaign.getDialerMode(), AgentStatus.OFFLINE, agentCampaign);		
	}

	private void updateHangupCallStatusToDialer(TelnyxCallWebhookDTOPayload payloadInfo) {
		String status = "";
		String oldCallLegId = null;
		String callLegId = payloadInfo.getCall_leg_id();
		String sessionId = payloadInfo.getCall_session_id();
		CallInfoDTO callInfo = null;
		String agentCallLegId = prospectCacheServiceImpl.getTelnyxManualSessionIdCallLegIdMap(sessionId);
		if (agentCallLegId != null && !agentCallLegId.equals(callLegId)) {
			oldCallLegId = callLegId;
			callLegId = agentCallLegId;
		}
		ProspectCallStatus prospectCallStatus = null;
		if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
			callInfo = decodeCallInfo(payloadInfo.getClient_state());
		}
		if (callInfo != null && callInfo.isAbandoned()) {
			status = "abandoned";
			prospectCallStatus = ProspectCallStatus.CALL_ABANDONED;
		} else {
			if (payloadInfo.getHangup_cause() != null && payloadInfo.getHangup_cause().equalsIgnoreCase("timeout")) {
				status = "no-answer";
				prospectCallStatus = ProspectCallStatus.CALL_NO_ANSWER;
			} else if (payloadInfo.getHangup_cause() != null
					&& payloadInfo.getHangup_cause().equalsIgnoreCase("normal_clearing")) {
				status = "completed";
			} else if (payloadInfo.getSip_hangup_cause() != null) {
				String sipHangupCause = payloadInfo.getSip_hangup_cause();
				if (sipHangupCause.equalsIgnoreCase("486") || sipHangupCause.equalsIgnoreCase("503") || sipHangupCause.equalsIgnoreCase("603")) {
					status = "busy";
					prospectCallStatus = ProspectCallStatus.CALL_BUSY;
				} else if (sipHangupCause.equalsIgnoreCase("480")) {
					status = "no-answer";
					prospectCallStatus = ProspectCallStatus.CALL_NO_ANSWER;
				} else if (sipHangupCause.equalsIgnoreCase("487")) {
					status = "call-cancelled";
					prospectCallStatus = ProspectCallStatus.CALL_CANCELED;
				} else if (sipHangupCause.equalsIgnoreCase("403") || sipHangupCause.equalsIgnoreCase("404") || sipHangupCause.equalsIgnoreCase("408") || sipHangupCause.equalsIgnoreCase("410") || sipHangupCause.equalsIgnoreCase("483") || sipHangupCause.equalsIgnoreCase("484") || sipHangupCause.equalsIgnoreCase("488") || sipHangupCause.equalsIgnoreCase("501") || sipHangupCause.equalsIgnoreCase("502") || sipHangupCause.equalsIgnoreCase("504")) {
					status = "failed";
					prospectCallStatus = ProspectCallStatus.CALL_FAILED;
				} else {
					status = "no-answer";
					prospectCallStatus = ProspectCallStatus.CALL_NO_ANSWER;
				}
			} else {
				status = "no-answer";
				prospectCallStatus = ProspectCallStatus.CALL_NO_ANSWER;
			}
		}

		// CallInfoDTO callInfo = new CallInfoDTO();
		// callInfo.setCallStatus(status);
		// callInfo.setCallSid(payloadInfo.getCall_leg_id());

		if (!status.equalsIgnoreCase("completed") && !payloadInfo.getHangup_cause().equalsIgnoreCase("originator_cancel")) {
			prospectLogService.updateCallStatus(null, prospectCallStatus, callLegId, 0, null);
		}

		// remove callsid from dialer app map
		dialerService.removeTelnyxCallSidFromDialer(callLegId);
		String agentId = "";
		agentId = getAgentIdFromMap(callLegId);
		if (agentId != null && !agentId.equals("")) {
			if ((oldCallLegId != null && !payloadInfo.getHangup_cause().equalsIgnoreCase("originator_cancel")) && !status.equalsIgnoreCase("completed")) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "no-answer");
			} else {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");	
			}
		}
		prospectCacheServiceImpl.removeFromTelnyxCallLegIds(callLegId);
		telnyxCallLegAndControlIdMap.remove(callLegId);
		if (oldCallLegId != null) {
			prospectCacheServiceImpl.removeFromTelnyxCallLegIds(oldCallLegId);
			telnyxCallLegAndControlIdMap.remove(oldCallLegId);
		}
		if (callInfo != null) {
			String pcid = callInfo.getPcid();
			String campaignId = callInfo.getCampaignId();
			logger.info("Event Type : [{}], CalllegId : [{}], Agent Id : [{}] , pcid : [{}], CampaignId : [{}]", "call.hangup", payloadInfo.getCall_leg_id(), agentId, pcid, campaignId);
		}
	}

	private String getAgentIdFromMap(String callLegId) {
		String agentId = "";
		if (callLegId != null && !callLegId.isEmpty()) {
			for (Entry<String, String> entry : telnyxAgentIdCallLegMap.entrySet()) {
				if (callLegId.equals(entry.getValue())) {
					agentId = entry.getKey();
					break;
				}
			}
		}
		return agentId;
	}

	private void handleManualDialCall(TelnyxCallWebhookDTOPayload payloadInfo, String toNumber, String clientState, String campaignId, String pcid, String agentId) {
		// System.out.println("callLegId : " + payloadInfo.getCall_leg_id());
		String callControlId = payloadInfo.getCall_control_id();
		TelnyxTransferCallRequest request = new TelnyxTransferCallRequest();
		Campaign campaign = campaignService.getCampaign(campaignId);
		if (campaign.getTelnyxCallTimeLimitSeconds() > 0 && campaign.getTelnyxCallTimeLimitSeconds() < 60) {
			request.setTime_limit_secs(60);
		} else if (campaign.getTelnyxCallTimeLimitSeconds() >= 60 && campaign.getTelnyxCallTimeLimitSeconds() <= 14400) {
			request.setTime_limit_secs(campaign.getTelnyxCallTimeLimitSeconds());
		} else if (campaign.getTelnyxCallTimeLimitSeconds() > 14400) {
			request.setTime_limit_secs(14400);
		} else {
			request.setTime_limit_secs(1800);
		}
		String countryName = "United States";
		ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(pcid);
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
				&& prospectCallLog.getProspectCall().getProspect() != null
				&& prospectCallLog.getProspectCall().getProspect().getCountry() != null) {
			countryName = prospectCallLog.getProspectCall().getProspect().getCountry();
		}
		if (campaign != null) {
			if (campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) {
				// TelnyxSIPInviteObject thinQId = new TelnyxSIPInviteObject();
				// thinQId.setName("thinQid");
				// thinQId.setValue(ApplicationEnvironmentPropertyUtils.getThinQId());
				// sipInfo.add(thinQId);
				// TelnyxSIPInviteObject thinQToken = new TelnyxSIPInviteObject();
				// thinQToken.setName("thinQtoken");
				// thinQToken.setValue(ApplicationEnvironmentPropertyUtils.getThinQToken());
				// sipInfo.add(thinQToken);
				// toNumber = createThinQNumber(toNumber, countryName);
			} else if (campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Viva")) {
				TelnyxSIPInviteObject vivaSIPObj = new TelnyxSIPInviteObject();
				vivaSIPObj.setName("dailType");
				vivaSIPObj.setValue("Manual");
				List<TelnyxSIPInviteObject> sipInfo = new ArrayList<>();
				sipInfo.add(vivaSIPObj);
				// request.setCustom_headers(sipInfo);
				request.setSip_auth_username(ApplicationEnvironmentPropertyUtils.getVivaTelnyxAccountId());
				request.setSip_auth_password(ApplicationEnvironmentPropertyUtils.getVivaTelnyxPasscode());
				toNumber = createVivaNumber(toNumber, countryName);
			} else if (campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("Samespace")) {
				request.setSip_auth_username(ApplicationEnvironmentPropertyUtils.getSamespaceUsername());
				request.setSip_auth_password(ApplicationEnvironmentPropertyUtils.getSamespacePassword());
				toNumber = createSamespaceNumber(toNumber, countryName);
			}
		}
		// if (fromNumber == null) {
		// 	logger.info("***** fromNumber is NULL for campaignId [{}] & phone [{}] *****", campaignId, toNumber);
		// 	campaign = campaignService.getCampaign(prospectCall.getCampaignId());
		// 	fromNumber = plivoOutboundNumberService.getOutboundNumber(prospect.getPhone(),
		// 			prospectCall.getCallRetryCount(), prospect.getCountry(), campaign.getOrganizationId());
		// }
		request.setTo(toNumber);
		request.setFrom(payloadInfo.getFrom());
		request.setClient_state(clientState);
		try {
			Map<String, Object> response = httpService.post(
					"https://api.telnyx.com/v2/calls/" + callControlId + "/actions/transfer", headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request));
			if (response.containsKey("error")) {
				String error = (String) response.get("error");
				if (error.contains("Destination Number is invalid")) {
					PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "invalid-phone");
				} else {
//					PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "no-answer");
					PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
				}
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveProspectCallStatusDetailsInMap(String callControlId, String status, String callLegId) {
		TelnyxCallStatusDTO callStatus = new TelnyxCallStatusDTO();
		callStatus.setCallControlId(callControlId);
		callStatus.setStatus(status);
		if (telnyxProspectCallInfoMap.containsKey(callLegId)) {
			telnyxProspectCallInfoMap.replace(callLegId, callStatus);
		} else {
			telnyxProspectCallInfoMap.put(callLegId, callStatus);
		}
	}

	private void saveAgentConferenceDetails(String agentId, String conferenceId, String callControlId, String callLegId) {
		AgentConference agentConference = new AgentConference(agentId, conferenceId, callControlId, callLegId);
		agentConferenceService.saveAgentConference(agentConference);
	}

	private void createConference(TelnyxCallWebhookDTOPayload payloadInfo) {
		logger.info("Conference creation request initiated=====> CalllegId : " + payloadInfo.getCall_leg_id());
		String clientState = payloadInfo.getClient_state();
		TelnyxCreateConferenceDTO request = new TelnyxCreateConferenceDTO();
		request.setCall_control_id(payloadInfo.getCall_control_id());
		request.setStart_conference_on_create(true);
		String conferenceName = getAgentName(payloadInfo.getTo());
		request.setName(conferenceName);
		CallInfoDTO callInfo = decodeCallInfo(clientState);
		if (callInfo != null && callInfo.getCommandStartTime() != null) {
			logger.info("TelnyxServiceImpl========>Event type : call.answered , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()));	
		}
		callInfo.setCommandStartTime(new Date());
		callInfo.setConferenceInitiated(true);
		String newClientState = encodeCallInfo(callInfo);
		request.setClient_state(newClientState);
		try {
			HashMap<String, Object> response = (HashMap<String, Object>) httpService
					.post("https://api.telnyx.com/v2/conferences", headers(),
							new ParameterizedTypeReference<Map<String, Object>>() {
							}, new ObjectMapper().writeValueAsString(request))
					.get("data");
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void handleDialerProspectCall(TelnyxCallWebhookDTOPayload payloadInfo) {
		String callControlId = payloadInfo.getCall_control_id();
		// In Dialer application, call leg Id is saved for a Prospectso we pass callSid
		// with value of callLegId
		String callSid = payloadInfo.getCall_leg_id();
		CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
		String campaignId = callInfo.getCampaignId();
		String prospectCallId = callInfo.getPcid();
		String callerId = callInfo.getCallerId();
		AgentCall allocatedAgent = null;
		String prospectCallAsJson = null;
		ProspectCall prospectCall = null;
		String xmlString = "";
		// find the available agent to connect with the prospect
		logger.info("==========> Finding available agent for CallSid [{}] <==========", callSid);
		allocatedAgent = agentRegistrationService.allocateAgent(callSid);
		if (allocatedAgent != null) {
			prospectCall = allocatedAgent.getProspectCall();
			String campaignInfoId = prospectCall.getCampaignId();
			Campaign campInfo = campaignService.getCampaign(campaignInfoId);
			try {
				//hideEmailAndPhone(prospectCall, campInfo);
				prospectCallAsJson = JSONUtils.toJson(prospectCall);
				PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
						prospectCallAsJson);
				startRecording(payloadInfo, false);
				addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid, payloadInfo, campInfo.isTelnyxHold());
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("==========> Allocated Agent [{}] to ProspectCallId [{}]. <==========",
					allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
		} else {
			try {
				// wait for a second and attempt again to find an agent
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("==========> TelnyxServiceImpl - Error occurred in handleDialerProspectCall <==========");
				logger.error("Expection : {} ", e);
			}
			// re-attempting to find the available agent
			allocatedAgent = agentRegistrationService.allocateAgent(callSid);
			if (allocatedAgent == null) {
				// still no agent available then play the abandon message
				logger.info("==========> No Agent available for CallSid  [{}] <==========", callSid);
				callInfo.setAbandoned(true);
				hangupCallForAbandonedCalls(callSid, callInfo);
				// xmlString = playAbandonMessage(callSid, prospectCallId, campaignId,
				// callerId);
			} else {
				prospectCall = allocatedAgent.getProspectCall();
				String campaignInfoId = prospectCall.getCampaignId();
				Campaign campInfo = campaignService.getCampaign(campaignInfoId);
				try {
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
							prospectCallAsJson);
					startRecording(payloadInfo, false);
					addProspectInConference(allocatedAgent.getAgentId(), prospectCallId, callerId, callSid, payloadInfo, campInfo.isTelnyxHold());
				} catch (IOException e) {
					e.printStackTrace();
				}
				logger.info("==========> Allocated Agent [{}] to ProspectCallId [{}] <==========", allocatedAgent.getAgentId(),
						prospectCall.getProspectCallId());
			}
		}
		// remove Call UUID from plivoCallStatusMap on dialer
		try {
			dialerService.removeTelnyxCallSidFromDialer(callSid);
		} catch (Exception e) {
			logger.error("==========> TelnyxServiceImpl - Error occurred in handleDialerProspectCall <==========");
			logger.error("Expection {} : ", e.getMessage());
		}
	}

	/**
	 * This will be called when agent dial prospect's phone manually from agent
	 * screen.
	 */
	private String handleManualAgentCall(TelnyxCallWebhookDTOPayload payloadInfo) {
		return null;

	}

	private void addProspectInConference(String agentId, String pcid, String callerId, String callLegId,
			TelnyxCallWebhookDTOPayload payloadInfo,boolean unholdAgent) {
		if (!prospectCacheServiceImpl.isPresentInTelnyxCallLegIds(callLegId)) {
			TelnyxCreateConferenceDTO request = new TelnyxCreateConferenceDTO();
			// CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
			String conferenceId = null;
			AgentConference agentConferenceObj = agentConferenceService.findConferenceIdByAgentId(agentId);
			if (agentConferenceObj != null) {
				conferenceId = agentConferenceObj.getConferenceId();
				request.setCall_control_id(payloadInfo.getCall_control_id());
				try {
					HashMap<String, Object> response = httpService.post(
							"https://api.telnyx.com/v2/conferences/" + conferenceId + "/actions/join", headers(),
							new ParameterizedTypeReference<HashMap<String, Object>>() {
							}, new ObjectMapper().writeValueAsString(request));
					prospectCacheServiceImpl.addInTelnyxCallLegIds(callLegId);
					if (unholdAgent) {
						unholdAgentCall(conferenceId, agentConferenceObj.getCallControlId());	
					}
					if (telnyxAgentIdCallLegMap.containsKey(agentId)) {
						telnyxAgentIdCallLegMap.replace(agentId, callLegId);
					} else {
						telnyxAgentIdCallLegMap.put(agentId, callLegId);
					}
					// dialerService.removeTelnyxCallSidFromDialer(callLegId);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	}
	
	
	/**
	 * Unhold Agent in Conference
	 * */
	private void unholdAgentCall(String conferenceId, String callControlId) {
		TelnyxHoldConferenceDTO request = new TelnyxHoldConferenceDTO();
		List<String> callControlIds = new ArrayList<>();
		callControlIds.add(callControlId);
		request.setCall_control_ids(callControlIds);
		try {
			HashMap<String, Object> response = httpService.post(
					"https://api.telnyx.com/v2/conferences/" + conferenceId + "/actions/unhold", headers(),
					new ParameterizedTypeReference<HashMap<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request));
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Unhold Agent in Conference
	 * */
	private void holdAgentCall(String conferenceId, String callControlId, String holdAudioUrl) {
		TelnyxHoldConferenceDTO request = new TelnyxHoldConferenceDTO();
		List<String> callControlIds = new ArrayList<>();
		callControlIds.add(callControlId);
		request.setAudio_url(holdAudioUrl);
		request.setCall_control_ids(callControlIds);
		try {
			HashMap<String, Object> response = httpService.post(
					"https://api.telnyx.com/v2/conferences/" + conferenceId + "/actions/hold", headers(),
					new ParameterizedTypeReference<HashMap<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request));
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This method is used to create a connection for an agent.
	 *
	 * NOTE - Connection username must be alphanumeric.
	 *
	 * @return Map
	 */
	@Override
	public Map<String, String> createConnectionForAgent(String username) {
		Map<String, String> telnyxConnectionCredentials = new HashMap<>();
		String randomPassword = RandomStringUtils.random(10, true, true);
		try {

			TelnyxConnectionRequestDTO data = new TelnyxConnectionRequestDTO();
			// TelnyxConnectionRequestDTOOutbound outbound = new
			// TelnyxConnectionRequestDTOOutbound();
			// outbound.setOutbound_voice_profile_id("1348977000835974669");
			data.setActive(true);
			data.setConnection_name(username + randomPassword);
			data.setUser_name(username + randomPassword);
			data.setPassword(randomPassword);
			data.setSip_uri_calling_preference("unrestricted");
			// data.setOutbound(outbound);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> response = httpService.post("https://api.telnyx.com/v2/credential_connections",
					headers(), new ParameterizedTypeReference<Map<String, Object>>() {
					}, mapper.writeValueAsString(data));
			if (response.containsKey("data")) {
				if (response.get("data") instanceof HashMap) {
					Map<String, Object> connectiondetails = (HashMap<String, Object>) response.get("data");
					telnyxConnectionCredentials.put(XtaasConstants.TELNYX_CONNECTION_USERNAME,
							connectiondetails.get("user_name").toString());
					telnyxConnectionCredentials.put(XtaasConstants.TELNYX_CONNECTION_PASSWORD,
							connectiondetails.get("password").toString());
							telnyxConnectionCredentials.put(XtaasConstants.TELNYX_CONNECTION_ID,
							connectiondetails.get("id").toString());
					logger.debug("Endpoint [{}] created for [{}] agent.", connectiondetails.get("id"),
							connectiondetails.get("user_name"));
				}

			}

		} catch (IOException e) {
			logger.error("==========> TelnyxServiceImpl - Error occurred in createConnectionForAgent <==========");
			logger.error("Expection : {} ", e.getMessage());
			throw new IllegalArgumentException("An error occurred while creating telnyx connection.");
		}
		return telnyxConnectionCredentials;
	}

	@Override
	public String deleteConnectionForAgent(String connectionId) {
		Map<String, Object> response = httpService.delete(
				"https://api.telnyx.com/v2/credential_connections/" + connectionId, headers(),
				new ParameterizedTypeReference<Map<String, Object>>() {
				});
		return "ok";
	}

	@Override
	public String purchasePhoneNumbers(TelnyxPhoneNumberPurchaseDTO phoneDTO) {
		Runnable runnable = () -> {
			List<TelnyxOutboundNumber> telnyxOutboundNumbers = new ArrayList<TelnyxOutboundNumber>();
			if (phoneDTO != null && phoneDTO.getStates() != null) {
				// NumberType numberType = NumberType.valueOf(phoneDTO.getType());
				for (String state : phoneDTO.getStates()) {
					try {
						StringBuilder filters = new StringBuilder();
						filters.append("filter[locality]=");
						filters.append(state);
						if (phoneDTO.getFeatures() != null & phoneDTO.getFeatures().size() != 0) {
							for (String feature : phoneDTO.getFeatures()) {
								filters.append("&");
								filters.append("filter[features][]=");
								filters.append(feature);
							}
						}
						if (phoneDTO.getLimit() > 0) {
							filters.append("&");
							filters.append("filter[limit]=");
							filters.append(Integer.toString(phoneDTO.getLimit()));
						}
						if (!phoneDTO.getCountryISO().isEmpty()) {
							filters.append("&");
							filters.append("filter[country_code]=");
							filters.append(phoneDTO.getCountryISO());
						}

						Map<String, Object> response = httpService.get(
								"https://api.telnyx.com/v2/available_phone_numbers?" + filters.toString(), headers(),
								new ParameterizedTypeReference<Map<String, Object>>() {
								});
						if (response.containsKey("data") && response.get("data") instanceof List) {
							List<HashMap<String, String>> dataList = (List<HashMap<String, String>>) response.get("data");
							if (dataList.size() != 0) {
								String phoneNumber = dataList.get(0).get("phone_number");
								if (!phoneNumber.isEmpty()) {
									TelnyxPhoneOrderRequestDTO phoneOrderRequest = new TelnyxPhoneOrderRequestDTO();
									phoneOrderRequest.setConnectionId(phoneDTO.getConnectionId());

									List<TelnyxPhoneOrderRequestPhoneNumber> phoneNumbers = new ArrayList<TelnyxPhoneOrderRequestPhoneNumber>();
									TelnyxPhoneOrderRequestPhoneNumber phoneNumberRequest = new TelnyxPhoneOrderRequestPhoneNumber();
									phoneNumberRequest.setPhoneNumber(phoneNumber);
									phoneNumbers.add(phoneNumberRequest);
									phoneOrderRequest.setPhoneNumbers(phoneNumbers);

									TelnyxPhoneOrderResponseDTO phoneOrderResponse = httpService.post(
											"https://api.telnyx.com/v2/number_orders", headers(),
											new ParameterizedTypeReference<TelnyxPhoneOrderResponseDTO>() {
											}, new ObjectMapper().writeValueAsString(phoneOrderRequest));
									if (phoneOrderResponse.getData().getPhoneNumbers().size() != 0) {
										TelnyxPhoneOrderResponseDTO phoneOrderStatusResponse = phoneOrderResponse;
										String number = phoneOrderResponse.getData().getPhoneNumbers().get(0)
												.getPhoneNumber();
										String telnyxPhoneNumberId = phoneOrderResponse.getData().getPhoneNumbers().get(0).getId();
										logger.info("Purchased Telnyx Phone Number - [{}].", number);
										String partnerId = phoneDTO.getPartnerId();
										int areaCode = Integer.valueOf(number.substring(1, 4));
										String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
										TelnyxOutboundNumber telnyxOutboundNumber = new TelnyxOutboundNumber(number, number,
												areaCode, phoneDTO.getIsdCode(), "TELNYX", partnerId, partnerId,
												voiceMessage, phoneDTO.getPool(), phoneDTO.getTimezone(),
												phoneDTO.getCountryISO(), phoneDTO.getRegion(), phoneDTO.getConnectionId(),telnyxPhoneNumberId);
										telnyxOutboundNumber = telnyxOutboundNumberService.save(telnyxOutboundNumber);
										telnyxOutboundNumbers.add(telnyxOutboundNumber);

										// if
										// (!phoneOrderResponse.getData().getPhoneNumbers().get(0).getStatus().equalsIgnoreCase("success"))
										// {
										// String orderId = phoneOrderResponse.getData().getId();
										// phoneOrderStatusResponse =
										// httpService.get("https://api.telnyx.com/v2/number_orders/" + orderId,
										// headers(), new ParameterizedTypeReference<TelnyxPhoneOrderResponseDTO>() {
										// });
										// } else {
										// phoneOrderStatusResponse = phoneOrderResponse;
										// }
										List<Map<String, String>> phoneNumberInfo = (List<Map<String, String>>) httpService
												.get("https://api.telnyx.com/v2/phone_numbers?filter[phone_number]=" + number, headers(),
														new ParameterizedTypeReference<Map<String, Object>>() {}).get("data");

										if (phoneNumberInfo != null && phoneNumberInfo.size() > 0) {
											TelnyxUpdatePhoneSettingsRequestDTO phoneSettingsRequest = new TelnyxUpdatePhoneSettingsRequestDTO();
											phoneSettingsRequest.setBillingGroupId(phoneDTO.getBillingAccountId());
											String phoneNumberId = phoneNumberInfo.get(0).get("id");
											Map<String, Object> updatePhoneSettingResponse = httpService.patch(
													"https://api.telnyx.com/v2/phone_numbers/" + phoneNumberId, headers(),
													new ParameterizedTypeReference<Map<String, Object>>() {
													}, new ObjectMapper().writeValueAsString(phoneSettingsRequest));
										}

									}
								}
							} else {
								// Purchase any single phone number of USA if not found by state name.
								List<TelnyxOutboundNumber> numbers = new ArrayList<TelnyxOutboundNumber>();
								numbers = purchaseUSPhoneNumbersByLimit(createSinglePhoneNumberPurchaseDTO(phoneDTO.getPartnerId(),
										phoneDTO.getConnectionId(), phoneDTO.getBillingAccountId(), phoneDTO.getPool()));
								telnyxOutboundNumbers.add(numbers.get(0));
							}
						}
					} catch (Exception e) {
						logger.error("==========> TelnyxServiceImpl - Error occurred in purchasePhoneNumbers <==========");
						logger.error("Expection : {} ", e);
					}
				}
			}
			if (phoneDTO.isAddNumberInOtherCountries()) {
				addTelnyxNumbersInCountries(telnyxOutboundNumbers, phoneDTO);
			}
		};
		Thread numberPurchaseThread = new Thread(runnable);
		numberPurchaseThread.start();
		return "Telnyx number purchase started.";
	}

	/**
	 * Answers the Incoming call
	 */
	@Override
	public String handleIncomingCall(TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		try {
			TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String callControlId = payloadInfo.getCall_control_id();
			String toNumber = payloadInfo.getTo();
			toNumber = toNumber.replaceAll("[^\\d]", "");
			TelnyxAnswerCallRequestDTO answerCallRequest = new TelnyxAnswerCallRequestDTO();
			answerCallRequest.setCommand_id(callControlId);

			Map<String, Object> answerCallResponse = httpService.post(
					"https://api.telnyx.com/v2/calls/" + callControlId + "/actions/answer", headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(answerCallRequest));

			String message = "Hi, You have reached the sales associate voicemail at " + toNumber
					+ ". We are terribly sorry we missed your call, please leave us a message and we will return your call as soon as we can.  We hope you enjoy the rest of your day!";

			CallInfoDTO info = new CallInfoDTO();
			info.setCallControllId(callControlId);
			info.setVoiceMail(true);
			info.setFrom(payloadInfo.getFrom());
			info.setTo(payloadInfo.getTo());

			String encodedClientState = encodeCallInfo(info);

			TelnyxSpeakTextRequestDTO speakRequest = new TelnyxSpeakTextRequestDTO();
			speakRequest.setLanguage("en-US");
			speakRequest.setPayload(message);
			speakRequest.setCommand_id("speakTest:" + callControlId);
			speakRequest.setVoice("female");
			speakRequest.setPayload_type("text");
			speakRequest.setClient_state(encodedClientState);

			Map<String, Object> speakTextResponse = httpService.post(
					"https://api.telnyx.com/v2/calls/" + callControlId + "/actions/speak", headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(speakRequest));

			// startRecording(telnyxCallWebhookDTO);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private String startRecording(TelnyxCallWebhookDTOPayload payloadInfo, boolean beep) {
		try {
			// TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
			String callControlId = payloadInfo.getCall_control_id();
			logger.info("Recording started: [{}]" + callControlId);

			TelnyxStartRecordingRequestDTO request = new TelnyxStartRecordingRequestDTO();
			request.setChannels("dual");
			request.setCommand_id("StartRecord:" + callControlId);
			request.setFormat("wav");
			request.setPlay_beep(beep);
			if (payloadInfo.getClient_state() != null && payloadInfo.getClient_state() != "") {
				CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
				callInfo.setCallControllId(callControlId);
				callInfo.setCommandStartTime(new Date());
				String newClientState = encodeCallInfo(callInfo);
				request.setClient_state(newClientState);
			} else {
				CallInfoDTO callInfo = new CallInfoDTO();
				callInfo.setCallSid(payloadInfo.getCall_leg_id());
				String newClientState = encodeCallInfo(callInfo);
				request.setClient_state(newClientState);
			}
			Map<String, Object> startRecordingResponse = httpService.post(
					"https://api.telnyx.com/v2/calls/" + callControlId + "/actions/record_start", headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ok";
	}

	@Override
	public void handleCallHangup(String callLegId) {
		if (telnyxCallLegAndControlIdMap != null && telnyxCallLegAndControlIdMap.containsKey(callLegId)) {
			String callControlId = telnyxCallLegAndControlIdMap.get(callLegId);
			// URI uri = buildUri("/calls/" + callControlId + "/actions/hangup");
			logger.info("Hangup source: Agent, Call leg Id : " + callLegId);
//			TelnyxHangupRequest request = new TelnyxHangupRequest();
//			CallInfoDTO callInfo = new CallInfoDTO();
//			callInfo.setCommandStartTime(new Date());
//			String clientState = encodeCallInfo(callInfo);
//			request.setClient_state(clientState);
			String telnyxAPIUrl = "https://api.telnyx.com/v2/calls/" + callControlId + "/actions/hangup";
			try {
				Map<String, Object> response = httpService.post(telnyxAPIUrl, headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
				}, "");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		prospectCacheServiceImpl.removeFromTelnyxCallLegIds(callLegId);
	}
	
	private void hangupCallForAbandonedCalls (String callLegId, CallInfoDTO callInfo) {
		if (telnyxCallLegAndControlIdMap != null && telnyxCallLegAndControlIdMap.containsKey(callLegId)) {
			String callControlId = telnyxCallLegAndControlIdMap.get(callLegId);
			logger.info("Hangup source: Server , Hangup reason: CALL_ABANDONED, Call leg Id : " + callLegId);
			TelnyxHangupRequest request = new TelnyxHangupRequest();
			String clientState = encodeCallInfo(callInfo);
			request.setClient_state(clientState);
			String telnyxAPIUrl = "https://api.telnyx.com/v2/calls/" + callControlId + "/actions/hangup";
			try {
				Map<String, Object> response = httpService.post(telnyxAPIUrl, headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
				}, new ObjectMapper().writeValueAsString(request));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		prospectCacheServiceImpl.removeFromTelnyxCallLegIds(callLegId);
	}

	@Override
	public void sendDtmf(TelnyxDTMFRequestDTO dtmfObj) {
		if (telnyxCallLegAndControlIdMap != null && telnyxCallLegAndControlIdMap.containsKey(dtmfObj.getCallLegId())) {
			String callControlId = telnyxCallLegAndControlIdMap.get(dtmfObj.getCallLegId());
			TelnyxDTMFRequest request = new TelnyxDTMFRequest();
			request.setDigits(dtmfObj.getDigits());
			String telnyxAPIUrl = "https://api.telnyx.com/v2/calls/" + callControlId + "/actions/send_dtmf";
			try {
				Map<String, Object> response = httpService.post(telnyxAPIUrl, headers(),
						new ParameterizedTypeReference<Map<String, Object>>() {
						}, new ObjectMapper().writeValueAsString(request));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Move recording to Aws
	 */
	@Override
	public String saveRecording(TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		String xmlString = "";
		TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
		TelnyxCallWebhookDTORecordingUrls urls = payloadInfo.getRecording_urls();
		CallInfoDTO callInfo = null;
		TelnyxRecordingUrlObjForMap recordings = new TelnyxRecordingUrlObjForMap();
		String pcid = null;
		String callControlId = null;
		String recordingUrl = "";
		String fileFormat = "";
		if (urls.getWav() != null) {
			recordingUrl = urls.getWav();
			fileFormat = ".wav";
		} else {
			recordingUrl = urls.getMp3();
			fileFormat = ".mp3";
		}
		saveCallLog(payloadInfo, urls, fileFormat);
		String recordingID = payloadInfo.getRecording_id();
		// String callUUID = request.getParameter("CallUUID");
		if (payloadInfo.getClient_state() != null && payloadInfo.getClient_state() != "") {
			callInfo = decodeCallInfo(payloadInfo.getClient_state());
		}
		if (callInfo != null) {
			if (callInfo != null && callInfo.getCommandStartTime() != null) {
				logger.info("TelnyxServiceImpl========>Event type : call.recording.saved , Call LegId : " + payloadInfo.getCall_leg_id() + " , command initiation time : " + callInfo.getCommandStartTime() + " , Delay in Seconds : " + getTimeDiffrenceInSeconds(callInfo.getCommandStartTime(), new Date()));	
			}
			pcid = callInfo.getPcid();
			callControlId = callInfo.getCallControllId();
			recordings.setRecordingUrl(recordingUrl);
			logger.info("Inside SaveRecording=====> CallLegId : " + payloadInfo.getCall_leg_id() + " pcid : " + pcid);
			if (telnyxCallLegAndRecordingUrlMap.containsKey(pcid)) {
				telnyxCallLegAndRecordingUrlMap.replace(pcid, recordings);
			} else {
				telnyxCallLegAndRecordingUrlMap.put(pcid, recordings);
			}
			// prospectCallService.updateRecordingUrl(pcid, recordingUrl);

		}
		try {
			if (recordingUrl != null && recordingID != null) {
				InputStream inputStream = new URL(recordingUrl).openStream();
				String fileName = recordingID;
				// String fileFormat = recordingUrl.substring(recordingUrl.length() - 4, recordingUrl.length());
				fileName = fileName + fileFormat;
				String awsRecordingURL = downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream,
						true);
				recordings.setAwsRecordingUrl(awsRecordingURL);
				if (telnyxCallLegAndRecordingUrlMap.containsKey(pcid)) {
					telnyxCallLegAndRecordingUrlMap.replace(pcid, recordings);
				} else {
					telnyxCallLegAndRecordingUrlMap.put(pcid, recordings);
				}
				if (telnyxAlternateProspectIdMap.containsKey(pcid)) {
					String oldPcid = pcid;
					pcid = telnyxAlternateProspectIdMap.get(oldPcid);
					telnyxAlternateProspectIdMap.remove(oldPcid);
				}
				logger.info("Saving aws recording : [{}]" + awsRecordingURL);
				prospectCallService.updateAWSRecordingUrl(pcid, awsRecordingURL);

				// deletePlivoCallRecording(recordingID, callUUID);
			}
		} catch (Exception e) {
			logger.error("==========> TelnyxServiceImpl - Error occurred in handleRecordingCallback <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
		return xmlString;
	}

	private void saveCallLog (TelnyxCallWebhookDTOPayload payloadInfo, TelnyxCallWebhookDTORecordingUrls urls, String fileFormat) {
		Map<String, Object> payloadInfoMap = new ObjectMapper().convertValue(payloadInfo, Map.class);
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Map.Entry<String, Object> entry : payloadInfoMap.entrySet()) {
			if (entry.getValue() instanceof LinkedHashMap) {
				continue;
			} else {
				if (entry.getKey().equalsIgnoreCase("recording_id")) {
					paramMap.put(entry.getKey(), (String) entry.getValue() + fileFormat);
				} else {
					paramMap.put(entry.getKey(), (String) entry.getValue());
				}
			}

		}
		if (urls.getWav() != null) {
			paramMap.put("recordingUrl", urls.getWav());
		} else {
			paramMap.put("recordingUrl", urls.getMp3());
		}
		paramMap.put("CallSid", payloadInfo.getCall_leg_id());
		CallLog callLog = callLogService.findCallLog(paramMap.get("CallSid"));
		if (callLog == null) {
			callLog = new CallLog();
			callLog.setCallLogMap(paramMap);
		} else {
			HashMap<String, String> callLogMap = callLog.getCallLogMap();
			paramMap.forEach((key, value) -> {
				if (callLogMap.get(key) == null) {
					callLogMap.put(key, value);
				}
			});
			callLog.setCallLogMap(callLogMap);
		}
		callLogService.saveCallLog(callLog);

	}

	public TelnyxRecordingUrlObjForMap getRecordingUrlsFromMap (String pcid) {
		if (telnyxCallLegAndRecordingUrlMap.containsKey(pcid)) {
			return telnyxCallLegAndRecordingUrlMap.get(pcid);
		} else {
			return null;
		}
	}

	public void mapAlternatePospectWithMainProspect (String pcid, String altPcid) {
		if (telnyxAlternateProspectIdMap.containsKey(pcid)) {
			telnyxAlternateProspectIdMap.replace(pcid, altPcid);
		} else {
			telnyxAlternateProspectIdMap.put(pcid, altPcid);
		}
	}

	/**
	 * Save VoiceMail recordings
	 */
	private void saveVoiceMailRecording(TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		TelnyxCallWebhookDTOPayload payloadInfo = telnyxCallWebhookDTO.getData().getPayload();
		TelnyxCallWebhookDTORecordingUrls urls = payloadInfo.getRecording_urls();
		String recordingUrl = "";
		if (urls.getWav() != null) {
			recordingUrl = urls.getWav();
		} else {
			recordingUrl = urls.getMp3();
		}
		String recordingID = payloadInfo.getRecording_id();

		if (!recordingUrl.isEmpty()) {
			CallInfoDTO info = decodeCallInfo(payloadInfo.getClient_state());
			String fromNumber = info.getFrom();
			String toNumber = info.getTo();
			String callSid = info.getCallControllId();
			toNumber = toNumber.replaceAll("[^\\d]", "");
			TelnyxOutboundNumber telnyxOutboundNumber = null;
			List<TelnyxOutboundNumber> telnyxOutboundNumbers = telnyxOutboundNumberService
					.findByDisplayPhoneNumber(toNumber);
			if (telnyxOutboundNumbers != null && telnyxOutboundNumbers.size() > 0) {
				telnyxOutboundNumber = telnyxOutboundNumbers.get(new Random().nextInt(telnyxOutboundNumbers.size()));
			}
			String partnerId = null;
			if (telnyxOutboundNumber != null) {
				partnerId = telnyxOutboundNumber.getPartnerId();
			}
			logger.debug("parternerId == " + partnerId + "  toNumber = " + toNumber);
			try {
				InputStream inputStream = new URL(recordingUrl).openStream();
				String fileName = recordingID;
				String fileFormat = recordingUrl.substring(recordingUrl.length() - 4, recordingUrl.length());
				fileName = fileName + fileFormat;
				String awsRecordingUrl;
				awsRecordingUrl = downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME,
						fileName, inputStream, true);
				VoiceMail voicemail = new VoiceMail(fromNumber, toNumber, callSid, recordingUrl, partnerId,
						XtaasConstants.PLIVO);
				// voicemail.setCallParams(paramMap);
				voicemail.setMessageStatus(VoiceMessageStatus.NEW);
				voicemail.setAwsAudioUrl(awsRecordingUrl);
				voicemailService.add(voicemail);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Async
	private void retryCallingAgentForConferenceCreation(TelnyxCallWebhookDTOPayload payloadInfo) {
		if (payloadInfo.getClient_state() != null && !payloadInfo.getClient_state().isEmpty()) {
			CallInfoDTO callInfo = decodeCallInfo(payloadInfo.getClient_state());
			int retryCount = callInfo.getRetryCount();
			int totalRetryLimit = 3;
			if (retryCount < totalRetryLimit) {
				int waitTime = getWaitTimeForRetries(retryCount);
				try {
					Thread.sleep(waitTime);
					retryCount++;
					callInfo.setRetryCount(retryCount);
					logger.info("TelnyxServiceImpl====> Retry count -  [{}] , for Conference Creation<===== call legId : [{}], AgentId : [{}]", callInfo.getRetryCount(), payloadInfo.getCall_leg_id(), callInfo.getAgentId());
					callAgentForConferenceCreation(callInfo);
				} catch (InterruptedException e) {
					logger.error("TelnyxServiceImpl Error in retry Calling Agent For Conference Creation");
				}
			} else {
				PusherUtils.pushMessageToUser(callInfo.getAgentId(), XtaasConstants.TELNYX_CONFERENCE_CREATION_ISSUE_EVENT, "Telnyx_ConferenceCreation_Issue");
			}			
		}
	}

	public Map<String, Object> callAgentForConferenceCreation(CallInfoDTO callInfo) {
		try {
			String fromNumber = "+18883021231";
			String serverbaseUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl().toLowerCase();
			if (serverbaseUrl.contains("demandshore")) {
				fromNumber = "+17733377629";
			} else if (serverbaseUrl.contains("xtaascorp")) {
				fromNumber = "+16304030514";
			} else if (serverbaseUrl.contains("xtaasupgrade")) {
				fromNumber = "+13322161699";
			} else if (serverbaseUrl.contains("peaceful-castle-6000")) {
				fromNumber = "+12255009001";
			}
			String outboundNumber = null;
			if (callInfo != null && callInfo.getPcid() != null) {
				ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(callInfo.getPcid());
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null && prospectCallLog.getProspectCall().getProspect() != null) {
					Prospect prospect = prospectCallLog.getProspectCall().getProspect();
					outboundNumber = telnyxOutboundNumberService.getOutboundNumber(prospect.getPhone(),
							prospectCallLog.getProspectCall().getCallRetryCount(), prospect.getCountry(), callInfo.getOrganizationId());
				}
			}
			if (outboundNumber == null) {
				outboundNumber = "+13322161699";
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			callInfo.setCommandStartTime(new Date());
			String encodedString = encodeCallInfo(callInfo);
			TelnyxCallDTO request = new TelnyxCallDTO();
			String telnyxConnectionId = ApplicationEnvironmentPropertyUtils.getTelnyxConnectionId();
			request.setConnection_id(telnyxConnectionId);
			request.setFrom(outboundNumber);
			request.setTo("sip:" + callInfo.getTo() + "@sip.telnyx.com");
			request.setClient_state(encodedString);
			Map<String, Object> response = (Map<String, Object>) httpService
					.post("https://api.telnyx.com/v2/calls", headers(), new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request)).get("data");
			if (response != null && response.get("call_leg_id") != null) {
				logger.info("Agent call initiated for Conference creation=====>AgentId : " + callInfo.getAgentId() + " calllegId : " + response.get("call_leg_id"));
			} else {
				logger.error("Agent call initiated for Conference creation=====> Error in call creation. AgentId : " + callInfo.getAgentId());
			}
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// added for callservice and to send call status info to Dialer
	public TelnyxCallStatusDTO getCallStatus(String callLegId) {
		if (telnyxProspectCallInfoMap.containsKey(callLegId)) {
			return telnyxProspectCallInfoMap.get(callLegId);
		} else {
			return null;
		}
	}

	@Override
	public Map<String, Object> callAgentForManualDial(CallInfoDTO callInfo) {
		try {
			callInfo.setCommandStartTime(new Date());
			String encodedString = encodeCallInfo(callInfo);
			String telnyxConnectionId = ApplicationEnvironmentPropertyUtils.getTelnyxConnectionId();
			TelnyxCallDTO request = new TelnyxCallDTO();
			request.setConnection_id(telnyxConnectionId);
			Campaign campaign = campaignService.getCampaign(callInfo.getCampaignId());
			if (campaign.getTelnyxCallTimeLimitSeconds() > 0 && campaign.getTelnyxCallTimeLimitSeconds() < 60) {
				request.setTime_limit_secs(60);
			} else if (campaign.getTelnyxCallTimeLimitSeconds() >= 60 && campaign.getTelnyxCallTimeLimitSeconds() <= 14400) {
				request.setTime_limit_secs(campaign.getTelnyxCallTimeLimitSeconds());
			} else if (campaign.getTelnyxCallTimeLimitSeconds() > 14400) {
				request.setTime_limit_secs(14400);
			} else {
				request.setTime_limit_secs(1800);
			}
//			if (callInfo != null && callInfo.getPcid() != null) {
//				ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(callInfo.getPcid());
//				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null && prospectCallLog.getProspectCall().getProspect() != null) {
//					Prospect prospect = prospectCallLog.getProspectCall().getProspect();
//					outboundNumber = telnyxOutboundNumberService.getOutboundNumber(prospect.getPhone(),
//							prospectCallLog.getProspectCall().getCallRetryCount(), prospect.getCountry(), callInfo.getOrganizationId());
//				}
//			}
			String outboundNumber = null;
			outboundNumber = callInfo.getOutboundNumber();
			if (outboundNumber == null) {
				outboundNumber = "+13322161699";
			}
			request.setFrom(outboundNumber);
			request.setTo("sip:" + callInfo.getFrom() + "@sip.telnyx.com");
			request.setClient_state(encodedString);
			Map<String, Object> response = (Map<String, Object>) httpService.post("https://api.telnyx.com/v2/calls",
					headers(), new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(request)).get("data");
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getCallControlId(String callLegId) {
		if (telnyxCallLegAndControlIdMap.containsKey(callLegId)) {
			String callControlId = telnyxCallLegAndControlIdMap.get(callLegId);
			return callControlId;
		} else {
			return null;
		}
	}
		
	private String createThinQNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getThinQDomainUrl());
		return thinQNumber.toString();
	}

	private String createTelnyxNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			toNumber = "+" + toNumber;
		}
		StringBuilder thinQNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getTelnyxDomainUrl());
		return thinQNumber.toString();
	}

	private String createVivaNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedVivaUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			// toNumber = "+" + toNumber;
		}
		StringBuilder vivaNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getVivaTelnyxDomain());
		return vivaNumber.toString();
	}
	
	private String createSamespaceNumber(String phoneNumber, String countryName) {
		String toNumber = "";
		if (countryName != null && (countryName.equalsIgnoreCase("United States") || countryName.equalsIgnoreCase("USA")
				|| countryName.equalsIgnoreCase("US") || countryName.equalsIgnoreCase("Canada")
				|| countryName.equalsIgnoreCase("CA"))) {
			toNumber = getSimpleFormattedSamespaceUSPhoneNumber(phoneNumber);
		} else {
			toNumber = phoneNumber.replaceAll("[^\\d]", "");
			// toNumber = "+" + toNumber;
		}
		StringBuilder samespaceNumber = new StringBuilder("sip:").append(toNumber).append("@")
				.append(ApplicationEnvironmentPropertyUtils.getSamespaceDomain());
		return samespaceNumber.toString();
	}

	/*
	 * SIP forwarding to ThinQ from Plivo Rest Client seems to reject number w/o
	 * country code and other formatting. This method should strip all formatting
	 * and add country code. For now it will check whether the country is US and add
	 * 1 else return null. Assumption. given number is a valid US number. doesn't
	 * validate.
	 */
	private String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US + normalizedPhoneNumber;
		} else {
			formattedPhoneNumber = "+" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	private String getSimpleFormattedVivaUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US_VIVA + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}
	
	private String getSimpleFormattedSamespaceUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		// removes all non-numeric chars
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", "");
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US_VIVA + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	@Override
	public BillingGroupOutputDTO createBillingGroup(String accountName, String partnerId) {
		BillingGroupOutputDTO billingDTO = new BillingGroupOutputDTO();
		TelnyxBillingGroupDTO billingGroupRequestDTO = new TelnyxBillingGroupDTO();
		billingGroupRequestDTO.setName(accountName);
		try {
			String billingGroupId = null;
			Map<String, Object> billingGroupResponse = httpService.post("https://api.telnyx.com/v2/billing_groups",
					headers(), new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(billingGroupRequestDTO));
			if (billingGroupResponse != null) {
				if (billingGroupResponse.get("data") instanceof HashMap) {
					Map<String, Object> billingDetails = (HashMap<String, Object>) billingGroupResponse.get("data");
					billingGroupId = billingDetails.get("id").toString();
				}
			}
			String connectionId = createCallControlApplication(partnerId);
			if (billingGroupId != null && connectionId != null) {
				buyPhoneNumbers(partnerId, billingGroupId, connectionId);
			}
			billingDTO.setBillingGroupId(billingGroupId);
			billingDTO.setConnectionId(connectionId);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return billingDTO;
	}
	
	private void buyPhoneNumbers(String partnerId, String billingGroupId, String connectionId) {
		purchasePhoneNumbers(
				createPhoneNumberPurchaseDTO(partnerId, connectionId, billingGroupId));
	}
	
	private String createCallControlApplication(String partnerId) {
		String connectionId = null;
		TelnyxCreateCallControlAppDTO callControlAppDTO = new TelnyxCreateCallControlAppDTO();
		String appName = partnerId + "_App";
		callControlAppDTO.setApplication_name(appName);
		callControlAppDTO.setWebhook_event_url("https://xtaasupgrade.herokuapp.com/spr/telnyx/voice");
		callControlAppDTO.getOutbound().setOutbound_voice_profile_id("1348977000835974669");
		callControlAppDTO.setWebhook_api_version("2");
		try {
			Map<String, Object> callControlResponse = httpService.post(
					"https://api.telnyx.com/v2/call_control_applications", headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, new ObjectMapper().writeValueAsString(callControlAppDTO));
			if (callControlResponse != null) {
				if (callControlResponse.get("data") instanceof HashMap) {
					Map<String, Object> callControlDetails = (HashMap<String, Object>) callControlResponse.get("data");
					connectionId = callControlDetails.get("id").toString();
				}
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connectionId;
	}
	
	private TelnyxPhoneNumberPurchaseDTO createPhoneNumberPurchaseDTO(String partnerId, String connectionId, String billingGroupId) {
		TelnyxPhoneNumberPurchaseDTO phonePurchaseDTO = new TelnyxPhoneNumberPurchaseDTO();
		List<String> features = new ArrayList<String>();
		features.add("voice");
		features.add("sms");
		phonePurchaseDTO.setStates(XtaasUtils.getTenUSStates());
		phonePurchaseDTO.setCountryISO("US");
		phonePurchaseDTO.setRegion("United States");
		phonePurchaseDTO.setFeatures(features);
		phonePurchaseDTO.setConnectionId(connectionId);
		phonePurchaseDTO.setBillingAccountId(billingGroupId);
		phonePurchaseDTO.setTimezone("US/Pacific");
		phonePurchaseDTO.setPartnerId(partnerId);
		phonePurchaseDTO.setIsdCode(1);
		phonePurchaseDTO.setPool(1);
		phonePurchaseDTO.setLimit(1);
		phonePurchaseDTO.setAddNumberInOtherCountries(true);
		return phonePurchaseDTO;
	}
	
	private TelnyxPhoneNumberPurchaseDTO createSinglePhoneNumberPurchaseDTO(String partnerId, String connectionId,
			String billingGroupId, int pool) {
		TelnyxPhoneNumberPurchaseDTO phonePurchaseDTO = new TelnyxPhoneNumberPurchaseDTO();
		List<String> features = new ArrayList<String>();
		features.add("voice");
		features.add("sms");
		phonePurchaseDTO.setCountryISO("US");
		phonePurchaseDTO.setRegion("United States");
		phonePurchaseDTO.setFeatures(features);
		phonePurchaseDTO.setConnectionId(connectionId);
		phonePurchaseDTO.setBillingAccountId(billingGroupId);
		phonePurchaseDTO.setTimezone("US/Pacific");
		phonePurchaseDTO.setPartnerId(partnerId);
		phonePurchaseDTO.setIsdCode(1);
		phonePurchaseDTO.setPool(pool);
		phonePurchaseDTO.setLimit(1);
		return phonePurchaseDTO;
	}
	
	private List<CountryWiseDetailsDTO> getUniqueCountryDetails() {
		List<CountryWiseDetailsDTO> countryWiseList = new ArrayList<CountryWiseDetailsDTO>();
		List<StateCallConfig> stateCallConfigDetails = stateCallConfigRepository.findAll();
		if (stateCallConfigDetails != null && stateCallConfigDetails.size() > 0) {
			for (StateCallConfig stateCallConfig : stateCallConfigDetails) {
				if (stateCallConfig.getCountryName() != "United States") {
					List<CountryWiseDetailsDTO> filterList = new ArrayList<CountryWiseDetailsDTO>();
					if (countryWiseList != null && countryWiseList.size() > 0) {
						filterList = countryWiseList.stream()
								.filter(stateCall -> stateCall.getCountryName()
										.equalsIgnoreCase(stateCallConfig.getCountryName()))
								.collect(Collectors.toList());
					}
					if (filterList.size() == 0) {
						CountryWiseDetailsDTO countryDTO = new CountryWiseDetailsDTO();
						countryDTO.setCountryCode(stateCallConfig.getCountryCode());
						countryDTO.setCountryName(stateCallConfig.getCountryName());
						countryDTO.setTimeZone(stateCallConfig.getTimezone());
						countryWiseList.add(countryDTO);
					}
				}
			}
		}
		return countryWiseList;
	}
	
	public List<TelnyxOutboundNumber> purchaseUSPhoneNumbersByLimit(TelnyxPhoneNumberPurchaseDTO phoneDTO) {
		List<TelnyxOutboundNumber> telnyxOutboundNumbers = new ArrayList<TelnyxOutboundNumber>();
		try {
			StringBuilder filters = new StringBuilder();
			if (phoneDTO.getLimit() > 0) {
				filters.append("&");
				filters.append("filter[limit]=");
				filters.append(Integer.toString(phoneDTO.getLimit()));
			}
			if (!phoneDTO.getCountryISO().isEmpty()) {
				filters.append("&");
				filters.append("filter[country_code]=");
				filters.append(phoneDTO.getCountryISO());
			}

			Map<String, Object> response = httpService.get(
					"https://api.telnyx.com/v2/available_phone_numbers?" + filters.toString(), headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			if (response.containsKey("data") && response.get("data") instanceof List) {
				List<HashMap<String, String>> dataList = (List<HashMap<String, String>>) response.get("data");
				if (dataList.size() != 0) {
					List<String> numbers = new ArrayList<String>();
					for (Map<String, String> entry : dataList) {
						for (String key : entry.keySet()) {
							String value = entry.get("phone_number");
							if (!numbers.contains(value)) {
								numbers.add(value);
							}
						}
					}
					if (numbers != null && numbers.size() > 0) {
						TelnyxPhoneOrderRequestDTO phoneOrderRequest = new TelnyxPhoneOrderRequestDTO();
						phoneOrderRequest.setConnectionId(phoneDTO.getConnectionId());
						List<TelnyxPhoneOrderRequestPhoneNumber> phoneNumbers = new ArrayList<TelnyxPhoneOrderRequestPhoneNumber>();
						for (String number : numbers) {
							TelnyxPhoneOrderRequestPhoneNumber phoneNumberRequest = new TelnyxPhoneOrderRequestPhoneNumber();
							phoneNumberRequest.setPhoneNumber(number);
							phoneNumbers.add(phoneNumberRequest);
						}
						phoneOrderRequest.setPhoneNumbers(phoneNumbers);

						TelnyxPhoneOrderResponseDTO phoneOrderResponse = httpService.post(
								"https://api.telnyx.com/v2/number_orders", headers(),
								new ParameterizedTypeReference<TelnyxPhoneOrderResponseDTO>() {
								}, new ObjectMapper().writeValueAsString(phoneOrderRequest));
						if (phoneOrderResponse.getData().getPhoneNumbers().size() != 0) {
							TelnyxPhoneOrderResponseDTO phoneOrderStatusResponse = phoneOrderResponse;
							for (TelnyxPhoneOrderResponsePhoneNumber telnyxPhoneOrderRequestPhoneNumber : phoneOrderResponse
									.getData().getPhoneNumbers()) {
								String number = telnyxPhoneOrderRequestPhoneNumber.getPhoneNumber();
								String telnyxPhoneNumberId = telnyxPhoneOrderRequestPhoneNumber.getId();
								logger.info("Purchased Telnyx Phone Number - [{}].", number);
								String partnerId = phoneDTO.getPartnerId();
								int areaCode = Integer.valueOf(number.substring(1, 4));
								String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
								TelnyxOutboundNumber telnyxOutboundNumber = new TelnyxOutboundNumber(number, number,
										areaCode, phoneDTO.getIsdCode(), "TELNYX", partnerId, partnerId, voiceMessage,
										phoneDTO.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(),
										phoneDTO.getRegion(), phoneDTO.getConnectionId(), telnyxPhoneNumberId);
								telnyxOutboundNumber = telnyxOutboundNumberService.save(telnyxOutboundNumber);
								telnyxOutboundNumbers.add(telnyxOutboundNumber);
								List<Map<String, String>> phoneNumberInfo = (List<Map<String, String>>) httpService
										.get("https://api.telnyx.com/v2/phone_numbers?filter[phone_number]=" + number,
												headers(), new ParameterizedTypeReference<Map<String, Object>>() {
												})
										.get("data");
								if (phoneNumberInfo != null && phoneNumberInfo.size() > 0) {
									TelnyxUpdatePhoneSettingsRequestDTO phoneSettingsRequest = new TelnyxUpdatePhoneSettingsRequestDTO();
									phoneSettingsRequest.setBillingGroupId(phoneDTO.getBillingAccountId());
									String phoneNumberId = phoneNumberInfo.get(0).get("id");
									Map<String, Object> updatePhoneSettingResponse = httpService.patch(
											"https://api.telnyx.com/v2/phone_numbers/" + phoneNumberId, headers(),
											new ParameterizedTypeReference<Map<String, Object>>() {
											}, new ObjectMapper().writeValueAsString(phoneSettingsRequest));
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("==========> TelnyxServiceImpl - Error occurred in purchasePhoneNumbers <==========");
			e.printStackTrace();
		}
		return telnyxOutboundNumbers;
	}

	private void addTelnyxNumbersInCountries(List<TelnyxOutboundNumber> telnyxNumbers, TelnyxPhoneNumberPurchaseDTO phoneDTO) {
		List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
		for (TelnyxOutboundNumber telnyxOutboundNumber : telnyxNumbers) {
			for (CountryWiseDetailsDTO countryWiseDetailsDTO : countryDetails) {
				TelnyxOutboundNumber number = new TelnyxOutboundNumber(
						telnyxOutboundNumber.getDisplayPhoneNumber(), telnyxOutboundNumber.getDisplayPhoneNumber(),
						telnyxOutboundNumber.getAreaCode(), telnyxOutboundNumber.getIsdCode(), "TELNYX",
						phoneDTO.getPartnerId(), phoneDTO.getPartnerId(), telnyxOutboundNumber.getVoiceMessage(),
						phoneDTO.getPool(), countryWiseDetailsDTO.getTimeZone(),
						countryWiseDetailsDTO.getCountryCode(), countryWiseDetailsDTO.getCountryName(),
						phoneDTO.getConnectionId(), telnyxOutboundNumber.getTelnyxPhoneNumberId());
				telnyxOutboundNumberService.save(number);
			} 
		}
	}
	
	// private void addTelnyxNumbersInCountries(List<TelnyxOutboundNumber> telnyxNumbers, TelnyxPhoneNumberPurchaseDTO phoneDTO) {
	// 	List<List<String>> nestedList = new ArrayList<List<String>>();
	// 	List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
	// 	Map<String, List<String>> numberAndCountryAssociationMap = new HashMap<String, List<String>>();
	// 	List<String> distinctCountryNames = countryDetails.stream().distinct().map(t -> t.getCountryName())
	// 			.collect(Collectors.toList());
	// 	int divideFactor = distinctCountryNames.size() / telnyxNumbers.size();
	// 	Collection<List<String>> subCollection = partitionBasedOnSize(distinctCountryNames, divideFactor);
	// 	nestedList.addAll(subCollection);
	// 	for (int i = 0; i < telnyxNumbers.size(); i++) {
	// 		TelnyxOutboundNumber outNumber = telnyxNumbers.get(i);
	// 		numberAndCountryAssociationMap.put(outNumber.getDisplayPhoneNumber(), nestedList.get(i));
	// 	}
	// 	for (Map.Entry<String, List<String>> entry : numberAndCountryAssociationMap.entrySet()) {
	// 		Optional<TelnyxOutboundNumber> outboundNumber = telnyxNumbers.stream()
	// 				.filter(number -> number.getDisplayPhoneNumber().equalsIgnoreCase(entry.getKey())).findAny();
	// 		List<String> countriesToBeAdded = entry.getValue();
	// 		for (String countryName : countriesToBeAdded) {
	// 			Optional<CountryWiseDetailsDTO> countryWiseDetail = countryDetails.stream()
	// 					.filter(country -> country.getCountryName().equalsIgnoreCase(countryName)).findAny();
	// 			TelnyxOutboundNumber telnyxOutboundNumber = new TelnyxOutboundNumber(
	// 					outboundNumber.get().getDisplayPhoneNumber(), outboundNumber.get().getDisplayPhoneNumber(),
	// 					outboundNumber.get().getAreaCode(), outboundNumber.get().getIsdCode(), "TELNYX",
	// 					phoneDTO.getPartnerId(), phoneDTO.getPartnerId(), outboundNumber.get().getVoiceMessage(),
	// 					phoneDTO.getPool(), countryWiseDetail.get().getTimeZone(),
	// 					countryWiseDetail.get().getCountryCode(), countryWiseDetail.get().getCountryName(),
	// 					phoneDTO.getConnectionId(), outboundNumber.get().getTelnyxPhoneNumberId());
	// 			telnyxOutboundNumberService.save(telnyxOutboundNumber);
	// 		}

	// 	}
	// }

	// <T> Collection<List<T>> partitionBasedOnSize(List<T> inputList, int size) {
	// 	final AtomicInteger counter = new AtomicInteger(0);
	// 	return inputList.stream().collect(Collectors.groupingBy(s -> counter.getAndIncrement() / size)).values();
	// }
	
	private void removeBlinkerFromDialButton (TelnyxCallWebhookDTOPayload payloadInfo) {
		String callLegId = payloadInfo.getCall_leg_id();
		String sessionId = payloadInfo.getCall_session_id();
		String agentCallLegId = prospectCacheServiceImpl.getTelnyxManualSessionIdCallLegIdMap(sessionId);
		if (agentCallLegId != null && !agentCallLegId.equals(callLegId)) {
			String agentId = getAgentIdFromMap(agentCallLegId);
			if (agentId!= null && !agentId.isEmpty()) {
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.TELNYX_MANUAL_CALL_ANSWERED_EVENT, "");
			}
		}
	}
	
	@Override
	public int getSizeOfCallLegAndControlIdMap() {
		return this.telnyxCallLegAndControlIdMap.size();
	}
	
	@Override
	public Map<String, String> getTelnyxOutboundNumberForRedial(RedialOutboundNumberDTO redialNumberDTO) {
		Map<String, String> outboundNumberMap = new HashMap<String, String>();
		String outboundNumber = null;
		if (redialNumberDTO != null && redialNumberDTO.getPhone() != null && redialNumberDTO.getCountry() != null
				&& redialNumberDTO.getOrganizationId() != null) {
			outboundNumber = telnyxOutboundNumberService.getOutboundNumber(redialNumberDTO.getPhone(),
					redialNumberDTO.getCallRetryCount(), redialNumberDTO.getCountry(),
					redialNumberDTO.getOrganizationId());
		}
		outboundNumberMap.put("redialoutboundnumber", outboundNumber);
		return outboundNumberMap;
	}

	@Override
	public List<String> deleteTelnyxOutboundNumber(List<String> phoneNumberList) {
		List<String> unDeletedPhoneNumbers = new ArrayList<>();
		List<Map<String, String>> response = (List<Map<String, String>>) httpService.get(
				"https://api.telnyx.com/v2/phone_numbers?page[size]=250", headers(), new ParameterizedTypeReference<Map<String, Object>>() {
				}).get("data");
		Map<String, String> finalMap = new HashMap<>();
		for (Map<String,String> singleEntry : response) {
			finalMap.put(singleEntry.get("phone_number"), singleEntry.get("id"));
		}
		for (String phoneNumber : phoneNumberList) {
			String phoneNumberId = finalMap.get(phoneNumber);
			if (phoneNumberId!= null && !phoneNumberId.isEmpty()) {
				Map<String, Object> phoneNumberDeleteResponse = httpService.delete("https://api.telnyx.com/v2/phone_numbers/" + phoneNumberId, headers(), new ParameterizedTypeReference<Map<String, Object>>() {});
				if (phoneNumberDeleteResponse != null) {
					unDeletedPhoneNumbers.add(phoneNumber);
				} else {
					telnyxOutboundNumberService.deleteManyByTelnyxPhoneNumber(phoneNumber);
				}
			}
		}
		return unDeletedPhoneNumbers;
	}
	
}