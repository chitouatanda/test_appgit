package com.xtaas.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.ProspectCallCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;

@Service
public class ProspectCacheServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(ProspectCacheServiceImpl.class);
	// used to maintain QUEUED, CALLBACK & CALL_DIALED ProspectCallLog list
	private LinkedHashMap<String, List<ProspectCallCacheDTO>> cachedProspectCallLog = new LinkedHashMap<>();
	private LinkedHashMap<String, List<ProspectCallDTO>> cachedProspectCallLogOld = new LinkedHashMap<>();
	private LinkedHashMap<String, Integer> cachedCallSpeed = new LinkedHashMap<>();
	private LinkedHashMap<String, Campaign> cachedCampaignMap = new LinkedHashMap<>();
	private LinkedHashMap<String, List<String>> cachedExcludedStateCodeList = new LinkedHashMap<>();
	private Map<String, List<String>> cachedTimeZoneList = new ConcurrentHashMap<>();
	private LinkedHashMap<String, List<String>> cachedIndirectProspectCallIds = new LinkedHashMap<>();
	private HashMap<String, Integer> cachedProspectCallLogCountMap = new HashMap<>();
	private HashMap<String, String> campaignSliceIndirectMap = new HashMap<>();
	private LinkedList<String> manualDialProspectCallIds = new LinkedList<>();
	private HashMap<String, Integer> cachedDailyCallRetryCountMap = new HashMap<>();
	private LinkedList<String> telnyxCallLegIds = new LinkedList<>();
	private Map<String, String> telnyxManualSessionIdCallLegIdMap = new HashMap<String, String>();
	private ConcurrentHashMap<String, DateTime> cachedTelnyxEventIds = new ConcurrentHashMap<>();
	private static final int timeDifferenceInMinutes = 3;
	private ConcurrentHashMap<String, String> multiRingingControlMap = new ConcurrentHashMap<String, String>();
	private Set<StateCallConfig> stateCallConfigInCache = new HashSet<StateCallConfig>();
	private Map<String, Set<String>> prospectCallIdsGivenToDialer = new HashMap<String, Set<String>>();
	// Added below flag to avoid duplicate prospects given to dialer when we merge the prospects in cache(Priorityqueue).
	private Set<String> campaignIds = new HashSet<String>();
	private HashMap<String, Integer> callableDataInDB = new HashMap<>();
	private DelayQueue<CallbackProspectCacheDTO> callbackNotificationCache = new DelayQueue<CallbackProspectCacheDTO>();
	private DelayQueue<CallbackProspectCacheDTO> callbackEventCache = new DelayQueue<CallbackProspectCacheDTO>();
	// private HashMap<String, List<CallbackProspectCacheDTO>> missedCallbacksCache = new HashMap<String, List<CallbackProspectCacheDTO>>();
	private Map<String, CallbackProspectCacheDTO> missedCallbacksCache = new HashMap<String, CallbackProspectCacheDTO>();
	
	public List<String> getExcludedStateCodeList(String campaignId) {
		List<String> cachedExcludedStateCodeList = this.cachedExcludedStateCodeList.get(campaignId);
		if (cachedExcludedStateCodeList == null) {
			cachedExcludedStateCodeList = new ArrayList<>();
			this.cachedExcludedStateCodeList.put(campaignId, cachedExcludedStateCodeList);
		}
		return cachedExcludedStateCodeList;
	}
	
	public void setExcludedStateCodeList(String campaignId, List<String> newExcludedStateCodeList) {
		List<String> cachedExcludedStateCodeList = this.cachedExcludedStateCodeList.get(campaignId);
		if (cachedExcludedStateCodeList == null) {
			cachedExcludedStateCodeList = new ArrayList<>();
			this.cachedExcludedStateCodeList.put(campaignId, cachedExcludedStateCodeList);
		}
		cachedExcludedStateCodeList.clear();
		cachedExcludedStateCodeList.addAll(newExcludedStateCodeList);
	}
	
	public void emptyStateCodeList() {
		this.cachedExcludedStateCodeList = new LinkedHashMap<String, List<String>>();
	}
	
	public void setStateCallConfig(List<StateCallConfig> stateCallConfigs) {
		stateCallConfigInCache.addAll(stateCallConfigs);
	}

	public Set<StateCallConfig> getStateCallConfig() {
		return stateCallConfigInCache;
	}
	
	public void emptyStateCallConfig() {
		this.stateCallConfigInCache = new HashSet<StateCallConfig>();
	}
	
	public List<String> getCachedOpenTimeZoneList(String campaignId) {
		List<String> cachedTimeZoneList = this.cachedTimeZoneList.get(campaignId);
		if (cachedTimeZoneList == null) {
			cachedTimeZoneList = new ArrayList<>();
			this.cachedTimeZoneList.put(campaignId, cachedTimeZoneList);
		}
		return cachedTimeZoneList;
	}
	
	public void setOpenTimeZoneList(String campaignId, List<String> openTimeZoneList) {
		List<String> cachedOpenTimeZones = this.cachedTimeZoneList.get(campaignId);
		if (cachedOpenTimeZones == null) {
			cachedOpenTimeZones = new ArrayList<>();
			this.cachedTimeZoneList.put(campaignId, cachedOpenTimeZones);
		}
		cachedOpenTimeZones.clear();
		cachedOpenTimeZones.addAll(openTimeZoneList);
	}

	public LinkedHashMap<String, List<ProspectCallCacheDTO>> getCachedProspectCallLog() {
		return cachedProspectCallLog;
	}
	
	public LinkedHashMap<String, List<ProspectCallDTO>> getCachedProspectCallLogOld() {
		return cachedProspectCallLogOld;
	}

	public void setCachedProspectCallLog(LinkedHashMap<String, List<ProspectCallCacheDTO>> cachedProspectCallLog) {
		this.cachedProspectCallLog = cachedProspectCallLog;
	}

	public void addProspectCallLogList(String key, List<ProspectCallCacheDTO> prospectCallLogList) {
		if (this.cachedProspectCallLog.get(key) == null || this.cachedProspectCallLog.get(key).size() == 0) {
			logger.info("addProspectCallLogList() : Adding prospects to the cache [{}]", prospectCallLogList);
			this.cachedProspectCallLog.put(key, new ArrayList<ProspectCallCacheDTO>());
		}
		this.cachedProspectCallLog.get(key).clear();
		this.cachedProspectCallLog.get(key).addAll(prospectCallLogList);
	}
	
	public void addProspectCallLogListOld(String key, List<ProspectCallDTO> prospectCallLogList) {
		if (this.cachedProspectCallLogOld.get(key) == null || this.cachedProspectCallLogOld.get(key).size() == 0) {
			this.cachedProspectCallLogOld.put(key, new ArrayList<ProspectCallDTO>());
		}
		this.cachedProspectCallLogOld.get(key).clear();
		this.cachedProspectCallLogOld.get(key).addAll(prospectCallLogList);
	}
	
	public Set<String> returnCampaignIds() {
		return this.cachedProspectCallLogOld.keySet();
	}

	public List<ProspectCallCacheDTO> removeProspectCallLogList(String key, int size) {
		int listSize = this.cachedProspectCallLog.get(key).size();
		List<ProspectCallCacheDTO> prospectDtoList = new ArrayList<ProspectCallCacheDTO>();
		int counter = 1;
		if (listSize <= size) {
			size = listSize;
			// return this.cachedProspectCallLog.get(key).subList(0, listSize);
		}

		List<ProspectCallCacheDTO> pclogList = this.cachedProspectCallLog.get(key);
		Iterator<ProspectCallCacheDTO> it = pclogList.iterator();
		while (it.hasNext()) {
			ProspectCallCacheDTO pDTO = it.next();
			if (counter <= size) {
				pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
				prospectDtoList.add(pDTO);
				counter++;
				it.remove();
			} else {
				break;
			}
		}
		return prospectDtoList;
		// return this.cachedProspectCallLog.get(key).subList(0, size);
	}
	
	public List<ProspectCallDTO> removeProspectCallLogListOld(String key, Float size, List<Float> ratios) {
		int listSize = this.cachedProspectCallLogOld.get(key).size();
		List<ProspectCallDTO> prospectDtoList = new ArrayList<ProspectCallDTO>();
		if (listSize <= size) {
			size = (float) listSize;
		}
		List<ProspectCallDTO> pclogList = this.cachedProspectCallLogOld.get(key);
		Iterator<ProspectCallDTO> it = pclogList.iterator();
		while (it.hasNext()) {
			ProspectCallDTO pDTO = it.next();
			try {
				if (size > 0F) {
					pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
					prospectDtoList.add(pDTO);
					size = size - ratios.get(pDTO.getCallRetryCount());
					it.remove();
				} else {
					break;
				}
			} catch (Exception e) {
				logger.debug("Exception occurred in removeProspectCallLogList for campaignId [{}] & pcid [{}]",
						pDTO.getCampaignId(), pDTO.getProspectCallId());
				logger.error("Exception is : ", e);
				it.remove();
			}
		}
		return prospectDtoList;
	}
	
	public List<ProspectCallDTO> removeProspectCallLogListOld(String key, int size) {
		int listSize = this.cachedProspectCallLogOld.get(key).size();
		List<ProspectCallDTO> prospectDtoList = new ArrayList<ProspectCallDTO>();
		int counter = 1;
		if (listSize <= size) {
			size = listSize;
			// return this.cachedProspectCallLog.get(key).subList(0, listSize);
		}

		List<ProspectCallDTO> pclogList = this.cachedProspectCallLogOld.get(key);
		Iterator<ProspectCallDTO> it = pclogList.iterator();
		while (it.hasNext()) {
			ProspectCallDTO pDTO = it.next();
			if (counter <= size) {
				pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
				prospectDtoList.add(pDTO);
				counter++;
				it.remove();
			} else {
				break;
			}
		}
		return prospectDtoList;
		// return this.cachedProspectCallLog.get(key).subList(0, size);
	}

	public void addCachedCallSpeed(String key, int speed) {
		this.cachedCallSpeed.put(key, speed);
	}

	public int getCachedCallSpeed(String key) {
		int speed = this.cachedCallSpeed.get(key);
		return speed;
	}

	public void addCampaignToCache(String key, Campaign campaign) {
		this.cachedCampaignMap.put(key, campaign);
	}

	public void removeCampaignCache(String key) {
		if (this.cachedCampaignMap.get(key) != null) {
			this.cachedCampaignMap.remove(key);
		}
	}

	public Campaign getCachedCampaign(String key) {
		Campaign campaign = this.cachedCampaignMap.get(key);
		return campaign;
	}

	public LinkedHashMap<String, List<String>> getCachedIndirectProspectCallIds() {
		return cachedIndirectProspectCallIds;
	}

	public void setCachedIndirectProspectCallIds(LinkedHashMap<String, List<String>> cachedIndirectProspectCallIds) {
		this.cachedIndirectProspectCallIds = cachedIndirectProspectCallIds;
	}

	public void addInCachedIndirectProspectCallIdsMap(String prospectCallId,
			List<String> cachedIndirectProspectCallIds) {
		if (this.cachedIndirectProspectCallIds.get(prospectCallId) == null
				|| this.cachedIndirectProspectCallIds.get(prospectCallId).size() == 0) {
			this.cachedIndirectProspectCallIds.put(prospectCallId, new ArrayList<String>());
		}
		this.cachedIndirectProspectCallIds.put(prospectCallId, cachedIndirectProspectCallIds);
	}

	public void clearCache(String key) {
		if (this.cachedProspectCallLog.get(key) == null || this.cachedProspectCallLog.get(key).size() == 0) {
			this.cachedProspectCallLog.put(key, new ArrayList<ProspectCallCacheDTO>());
		}
		this.cachedProspectCallLog.get(key).clear();
	}

	// clear old prospect cache
	public void clearCachedProspectCallLogOld(String key) {
		if (this.cachedProspectCallLogOld.get(key) == null || this.cachedProspectCallLogOld.get(key).size() == 0) {
			this.cachedProspectCallLogOld.put(key, new ArrayList<ProspectCallDTO>());
		}
		this.cachedProspectCallLogOld.get(key).clear();
	}

	public List<ProspectCallCacheDTO> removeProspectCallLogList(String key, Float size, List<Float> ratios) {
		int listSize = this.cachedProspectCallLog.get(key).size();
		List<ProspectCallCacheDTO> prospectDtoList = new ArrayList<ProspectCallCacheDTO>();
		if (listSize <= size) {
			size = (float) listSize;
		}
		List<ProspectCallCacheDTO> pclogList = this.cachedProspectCallLog.get(key);
		Iterator<ProspectCallCacheDTO> it = pclogList.iterator();
		while (it.hasNext()) {
			ProspectCallCacheDTO pDTO = it.next();
			try {
				if (size > 0F) {
					pDTO.getProspect().setProspectType(XtaasConstants.PROSPECT_TYPE.POWER.name());
					prospectDtoList.add(pDTO);
					size = size - ratios.get(pDTO.getCallRetryCount());
					it.remove();
				} else {
					break;
				}
			} catch (Exception e) {
				logger.debug("Exception occurred in removeProspectCallLogList for campaignId [{}] & pcid [{}]",
						pDTO.getCampaignId(), pDTO.getProspectCallId());
				logger.error("Exception is : ", e);
				it.remove();
			}
		}
		return prospectDtoList;
	}

	public void addInCachedProspectCallLogCountMap(String campaignId, int callableCount) {
		if (this.cachedProspectCallLogCountMap.get(campaignId) == null || callableCount < 0) {
			this.cachedProspectCallLogCountMap.put(campaignId, 0);
		}
		this.cachedProspectCallLogCountMap.put(campaignId, callableCount);
	}

	public int getCachedProspectCallLogCount(String campaignId) {
		Integer callableCount = 0;
		if (this.cachedProspectCallLogCountMap.get(campaignId) != null) {
			callableCount = this.cachedProspectCallLogCountMap.get(campaignId);
		}
		return callableCount.intValue();
	}
	
	public void addInCachedCallableDataInDBMap(String campaignId, int callableCount) {
		if (this.callableDataInDB.get(campaignId) == null || callableCount < 0) {
			this.callableDataInDB.put(campaignId, 0);
		}
		this.callableDataInDB.put(campaignId, callableCount);
	}

	public int getCachedCallableDataInDBCount(String campaignId) {
		Integer callableCount = 0;
		if (this.callableDataInDB.get(campaignId) != null) {
			callableCount = this.callableDataInDB.get(campaignId);
		}
		return callableCount.intValue();
	}

	public void addInCampaignSliceIndirectMap(String campaignId, String isIndirectFound) {
		if (campaignSliceIndirectMap.get(campaignId) == null) {
			campaignSliceIndirectMap.put(campaignId, isIndirectFound);
		} else {
			campaignSliceIndirectMap.put(campaignId, isIndirectFound);
		}
	}

	public String getCampaignSliceIndirect(String campaignId) {
		if (campaignSliceIndirectMap.get(campaignId) == null) {
			campaignSliceIndirectMap.put(campaignId, "false");
		}
		return campaignSliceIndirectMap.get(campaignId);
	}

	public LinkedList<String> getManualDialProspectCallIds() {
		return manualDialProspectCallIds;
	}

	/**
	 * add prospectCallId in cache which is dialed by manual agent.
	 */
	public void addInManualDialProspectCallIds(String prospectCallId) {
		this.manualDialProspectCallIds.add(prospectCallId);
	}

	/**
	 * remove prospectCallId from cache which is dialed by manual agent.
	 */
	public void removeFromManualDialProspectCallIds(String prospectCallId) {
		this.manualDialProspectCallIds.remove(prospectCallId);
	}
	
	
	public LinkedList<String> getTelnyxCallLegIds() {
		return telnyxCallLegIds;
	}
	
	/**
	 * chech if callLegId present in cache which is dialed by auto agent (Telnyx).
	 */	
	public boolean isPresentInTelnyxCallLegIds(String callLegIds) {
		if (telnyxCallLegIds.contains(callLegIds)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * add callLegIds in cache which is dialed by auto agent (Telnyx).
	 */
	public void addInTelnyxCallLegIds(String callLegIds) {
		this.telnyxCallLegIds.add(callLegIds);
	}

	/**
	 * remove callLegIds from cache which is dialed by auto agent (Telnyx).
	 */
	public void removeFromTelnyxCallLegIds(String callLegIds) {
		this.telnyxCallLegIds.remove(callLegIds);
	}

	public void addInCachedDailyCallRetryCountMap(String campaignId, int dialyRetryCount) {
		if (this.cachedDailyCallRetryCountMap.get(campaignId) == null) {
			this.cachedDailyCallRetryCountMap.put(campaignId, dialyRetryCount);
		}
		this.cachedProspectCallLogCountMap.put(campaignId, dialyRetryCount);
	}

	public int getCachedDailyCallRetryCount(String campaignId) {
		Integer dialyRetryCount = 0;
		if (this.cachedDailyCallRetryCountMap.get(campaignId) != null) {
			dialyRetryCount = this.cachedDailyCallRetryCountMap.get(campaignId);
		}
		return dialyRetryCount.intValue();
	}
	
	/**
	 *  Get cache of EventIds of webhook event from Telnyx
	 */
	public ConcurrentHashMap<String, DateTime> getTelnyxEventIds() {
		return cachedTelnyxEventIds;
	}
	
	/**
	 * check if EventId of webhook event present in cache (Telnyx).
	 */	
	public boolean isPresentInTelnyxEventIds(String eventId) {
		if (cachedTelnyxEventIds.containsKey(eventId)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * add Event ids of webhook events in cache (Telnyx).
	 */
	public void addInTelnyxEventIds(String eventId) {
		this.cachedTelnyxEventIds.put(eventId, DateTime.now());
	}

	/**
	 * remove Event ids of webhook events from cache (Telnyx).
	 */
	private void removeFromTelnyxEventIds(String eventId) {
		this.cachedTelnyxEventIds.remove(eventId);
	}
	
	public void cleanObsoleteTelnyxEventEntries() {
		List<String> telnyxEventIdsListForDeletion = new ArrayList<>();
		int oldSize = this.cachedTelnyxEventIds.size();
		for (Entry<String, DateTime> entry : this.cachedTelnyxEventIds.entrySet()) {
			if (entry.getValue().isBefore(DateTime.now().minusMinutes(timeDifferenceInMinutes))) {
				telnyxEventIdsListForDeletion.add(entry.getKey());
			}
		}
		if (!telnyxEventIdsListForDeletion.isEmpty()) {
			for (String eventIds : telnyxEventIdsListForDeletion) {
				removeFromTelnyxEventIds(eventIds);
			}
		}
		int newSize = this.cachedTelnyxEventIds.size();
		logger.info("Telnyx EventId Map size: old - [{}] and new - [{}]", oldSize, newSize);
	}
	
	/**
	 * Telny manual Dial SessionId and Agent Call leg Id Map => Add, Get and Remove operations
	 */
	
	public void addInTelnyxManualSessionIdCallLegIdMap(String sessionId, String agentCallLegId) {
		this.telnyxManualSessionIdCallLegIdMap.put(sessionId, agentCallLegId);
	}

	public String getTelnyxManualSessionIdCallLegIdMap(String sessionId) {
		String agentCallLegId = null;
		agentCallLegId = this.telnyxManualSessionIdCallLegIdMap.get(sessionId);
		return agentCallLegId;
	}
	
	public void removeTelnyxManualSessionIdCallLegIdMap(String sessionId) {
		this.telnyxManualSessionIdCallLegIdMap.remove(sessionId);
	}
	
	/**
	 * add Event ids of webhook events for unit testing.
	 */
	public void addInTelnyxEventIdsForTest(String eventId, DateTime time) {
		this.cachedTelnyxEventIds.put(eventId, time);
	}
	
	///*** reporting problem multiple entrie of call dailed are getting created for manaul dial,
	// as plivo is sending multiple call rining initiated webhooks to handle this we are using the below map
	
	public void addInMultiRingingControlMap(String agentId, String prospectCallLogId) {
		this.multiRingingControlMap.put(agentId, prospectCallLogId);
	}

	public String getMultiRingingControlMap(String agentId) {
		String prospectCallLogId = null;
		prospectCallLogId = this.multiRingingControlMap.get(agentId);
		return prospectCallLogId;
	}

	public void removeMultiRingingControlMapCallLegIdMap(String agentId) {
		this.multiRingingControlMap.remove(agentId);
	}
	
	public void storeProspectsGivenToDialer(String campaignId, Set<String> prospectCallIds) {
		if (prospectCallIdsGivenToDialer.containsKey(campaignId)) {
			prospectCallIdsGivenToDialer.get(campaignId).addAll(prospectCallIds);
		} else {
			prospectCallIdsGivenToDialer.put(campaignId, prospectCallIds);
		}
	}
	
	public void storeCampaignIdsInSet(String campaignId) {
		if (!campaignIds.contains(campaignId)) {
			campaignIds.add(campaignId);
		}
	}
	
	public void removeCampaignIdsFromSet(String campaignId) {
		if (campaignIds != null && campaignIds.size() > 0) {
			Iterator<String> campaignIterator = campaignIds.iterator();
			while (campaignIterator.hasNext()) {
				String nextElement = campaignIterator.next();
				if (nextElement.equalsIgnoreCase(campaignId)) {
					campaignIterator.remove();
				}
			}
		}
	}
	
	public boolean checkIfCampaignIdPresentInSet(String campaignId) {
		if (campaignIds != null && campaignIds.contains(campaignId)) {
			return true;
		} else {
			return false;
		}
	}

	public void resetProspectsGivenToDialerMap() {
		prospectCallIdsGivenToDialer = new HashMap<String, Set<String>>();
	}

	public Set<String> getProspectCallIdsOfCampaign(String campaignId) {
		return prospectCallIdsGivenToDialer.get(campaignId);
	}
	
	/**
	 *  Below method stores the callback prospects for all campaign in memory cache.
	 * @param prospectsFromDB
	 * @throws ParseException 
	 */
	public void insertCallbackRecordsInCache(List<CallbackProspectCacheDTO> prospectsFromDB) throws ParseException {
		for (CallbackProspectCacheDTO callBackDTO : prospectsFromDB) {
			if (callBackDTO.getCallbackDate() != null) {
				List<CallbackProspectCacheDTO> filteredCallback = this.callbackNotificationCache.stream().filter(
						callback -> callback.getProspectCallId().equalsIgnoreCase(callBackDTO.getProspectCallId()))
						.collect(Collectors.toList());
				if (filteredCallback != null && filteredCallback.size() == 0) {
					if (callBackDTO.getCallbackDate() != null && callBackDTO.getProspect() != null && callBackDTO.getProspect().getTimeZone() != null) {
						Date callbackTimeTimeZoneWise = XtaasDateUtils.convertToTimeZone(callBackDTO.getCallbackDate(), callBackDTO.getProspect().getTimeZone());
						Date currentTimeTImeZoneWise = XtaasDateUtils.convertToTimeZone(new Date(), callBackDTO.getProspect().getTimeZone());
						if (callbackTimeTimeZoneWise.after(currentTimeTImeZoneWise)) { // Added this because we wouldnt notify the agent whose
							// callback time is over
							long duration = callBackDTO.getCallbackDate().getTime() - new Date().getTime();
							// long before10Minutes = duration - 180000;
							if (duration <= 600000) {
								this.callbackNotificationCache.add(new CallbackProspectCacheDTO(callBackDTO, 60000));
							} else {
								long before10Minutes = duration - 600000; // we are subtracting duration from 600000 miliseconds as we need to send notification before 10 minutes.
								this.callbackNotificationCache
										.add(new CallbackProspectCacheDTO(callBackDTO, before10Minutes));
							}
							logger.info(
									"insertCallbackRecordsInCache() :: Inserted callback prospect with prospectcallid [{}] in callback cache having campaignId [{}]",
									callBackDTO.getProspectCallId(), callBackDTO.getCampaignId());
						}
					}					
				}
			}
		}
	}
	
	/**
	 * Below method returns the callback prospect from callback cache whose delay time is expired.
	 * @return
	 * @throws InterruptedException
	 */
	public CallbackProspectCacheDTO getCallbackRecordsFromCache() throws InterruptedException {
		return this.callbackNotificationCache.poll();
	}
	
	public DelayQueue<CallbackProspectCacheDTO> retrieveAllCallbackRecordsFromCache() {
		return this.callbackNotificationCache;
	}
	
	/**
	 * Below method stores the callback prospects whose are notified to agent before 10 minutes.
	 * @param prospectsFromDB
	 */
	public void insertCallbackRecordsInEventCache(CallbackProspectCacheDTO callBackDTO) {
		if (!callbackEventCache.contains(callBackDTO)) {
			long duration = callBackDTO.getCallbackDate().getTime() - new Date().getTime();					
			if (duration >= 0 && duration < 700000) {
				this.callbackEventCache.add(new CallbackProspectCacheDTO(callBackDTO, duration));				
			} 
		}
	}
	
	/**
	 * Below method returns the callback prospect from callback event cache whose delay time is expired.
	 * We will send this prospect to agent for callback.
	 * @return
	 * @throws InterruptedException
	 */
	public CallbackProspectCacheDTO getCallbackRecordsFromEventCache() throws InterruptedException {
		CallbackProspectCacheDTO callbackProspectCacheDTO = this.callbackEventCache.poll();
		return callbackProspectCacheDTO;
	}

	/**
	 * Below method stores the missed callback prospects which are missed by agent.
	 * @param prospectsFromDB
	 */
	public void insertInMissedCallbackCache(String agentId, CallbackProspectCacheDTO missedCallBackDTO) {
		if (!missedCallbacksCache.containsValue(missedCallBackDTO)) {
			this.missedCallbacksCache.put(agentId, missedCallBackDTO);
		}
	}

	/**
	 * Below method returns the missed callback prospect from missedCallbackCache of agent.
	 * @return
	 * @throws InterruptedException
	 */
	public CallbackProspectCacheDTO getMissedCallbackRecord(String agentId) {
		return this.missedCallbacksCache.remove(agentId);
	}
	
	public int retrieveAllMissedCallbacksFromCache() {
		return this.missedCallbacksCache.size();
	}

	public int getCacheSize(String campaignId) {
		int size = 0;
		if (this.cachedProspectCallLogOld.containsKey(campaignId)) {
			size = this.cachedProspectCallLogOld.get(campaignId) != null
					? this.cachedProspectCallLogOld.get(campaignId).size()
					: 0;
		}
		return size;
	}

}