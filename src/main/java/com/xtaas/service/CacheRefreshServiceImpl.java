package com.xtaas.service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.OutboundNumberService;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.application.service.SignalwireOutboundNumberService;
import com.xtaas.application.service.TelnyxOutboundNumberService;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.infra.zoominfo.service.TenCompanies;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.ProspectCallCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;

@Service
public class CacheRefreshServiceImpl implements CacheRefreshService {

	private static final Logger logger = LoggerFactory.getLogger(CacheRefreshServiceImpl.class);

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private TenCompanies tenCompanies;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private OutboundNumberService outboundNumberService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;
	
	@Autowired
	private TelnyxOutboundNumberService telnyxOutboundNumberService;

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private AgentRegistrationService agentRegistrationService;
	
	@Autowired
	private PriorityQueueCacheOperationService priorityQueueCacheOperationService;

	@Autowired
	private RestClientService restClientService;

	@Autowired
	private SignalwireOutboundNumberService signalwireOutboundNumberService;
	
	@Autowired
	private AgentService agentService;


	private static final String HTTPS = "https://";
	private static final String HTTP = "http://";
	private static final String API_URL = "/spr/cacherefresh/campaign/";
	public static final String LOCALHOST = "localhost";

	@Override
	public void refreshCache(String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		logger.debug("########### CACHE REFRESH STARTED FOR [{}] ###########", campaign.getName());
		prospectCacheServiceImpl.clearCache(campaign.getId());
		logger.debug("########### CACHE REFRESH COMPLETED FOR [{}] ###########", campaign.getName());
	}

	@Override
	public void refreshAllCache() {
		logger.debug("########### ALL CACHE REFRESH STARTED ###########");
		List<CampaignDTO> campaignList = campaignService.findActiveWithProjectedFields(Arrays.asList());
		for (CampaignDTO campaign : campaignList) {
			prospectCacheServiceImpl.clearCache(campaign.getId());
		}
		logger.debug("########### ALL CACHE REFRESH COMPLETED ###########");
	}

	@Override
	public Boolean loadApplicationPropertyCacheForcefully() {
		logger.debug("########### REFRESHING APPLICATION PROPERTY CACHE ###########");
		propertyService.loadApplicationPropertyCacheForcefully();
		return true;
	}

	@Override
	public Boolean refreshTwilioOutboundNumbers() {
		logger.debug("########### TWILIO OUTBOUND NUMBER CACHE REFRESH STARTED ###########");
		outboundNumberService.refreshOutboundNumbers();
		logger.debug("########### TWILIO OUTBOUND NUMBER CACHE REFRESH FINISHED ###########");
		return true;
	}

	@Override
	public Boolean refreshPlivoOutboundNumbers() {
		logger.debug("########### PLIVO OUTBOUND NUMBER CACHE REFRESH STARTED ###########");
		plivoOutboundNumberService.refreshOutboundNumbers();
		logger.debug("########### PLIVO OUTBOUND NUMBER CACHE REFRESH FINISHED ###########");
		return true;
	}
	
	@Override
	public Boolean refreshTelnyxOutboundNumbers() {
		logger.debug("########### TELNYX OUTBOUND NUMBER CACHE REFRESH STARTED ###########");
		telnyxOutboundNumberService.refreshOutboundNumbers();
		logger.debug("########### TELNYX OUTBOUND NUMBER CACHE REFRESH FINISHED ###########");
		return true;
	}

	@Override
	public Boolean refreshSignalwireOutboundNumbers() {
		logger.debug("########### SIGNALWIRE OUTBOUND NUMBER CACHE REFRESH STARTED ###########");
		signalwireOutboundNumberService.refreshOutboundNumbers();
		logger.debug("########### SIGNALWIRE OUTBOUND NUMBER CACHE REFRESH FINISHED ###########");
		return true;
	}

	@Override
	public Boolean updateTenCompanies(String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		logger.debug("########### TenCompanies Started [{}] ###########", campaign.getName());
		tenCompanies.getTenProspectsForACompany(campaignId);
		logger.debug("########### TenCompanies COMPLETED FOR [{}] ###########", campaign.getName());
		logger.debug("########### CACHE REFRESH STARTED FOR [{}] ###########", campaign.getName());
		refreshCache(campaignId);
		logger.debug("########### CACHE REFRESH COMPLETED FOR [{}] ###########", campaign.getName());
		return true;
	}

	@Override
	public String resetSupervisorOverrideCampaignId(String campaignId) {
		List<Team> teamList = teamRepository.findBySupervisorOverrideCampaignId(campaignId);
		if (teamList != null && teamList.size() > 0) {
			for (Team team : teamList) {
				boolean isModified = false;
				if (team.getAgents() != null && team.getAgents().size() > 0) {
					for (TeamResource agent : team.getAgents()) {
						if (agent.getSupervisorOverrideCampaignId() != null) {
							if (agent.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
								agent.setSupervisorOverrideCampaignId(null);
								isModified = true;
							}
						}
					}
				}
				if (isModified) {
					teamRepository.save(team);
				}
			}
		}
		return "Successfully updated teams.";
	}

	@Override
	public String resetSupervisorOverrideCampaignIdBySupervisorId(String supervisorId) {
		List<Team> teamList = teamRepository.findBySupervisorId(supervisorId);
		if (teamList != null && teamList.size() > 0) {
			for (Team team : teamList) {
				boolean isModified = false;
				if (team.getAgents() != null && team.getAgents().size() > 0) {
					for (TeamResource agent : team.getAgents()) {
						if (agent.getSupervisorOverrideCampaignId() != null) {
							agent.setSupervisorOverrideCampaignId(null);
							isModified = true;
						}
					}
				}
				if (isModified) {
					teamRepository.save(team);
				}
			}
		}
		return "Successfully updated teams.";
	}

	@Override
	public void clearCache(Campaign campaign) {
		try {
			logger.debug("clearCache : " + campaign.getId());
			URL url = new URL("https://");
			if (campaign.getHostingServer().contains("xtaascorp")) {
				url = new URL(XtaasConstants.XTAASCORP_URL + "/spr/cacherefresh/campaign/" + campaign.getId());
			} else if (campaign.getHostingServer().contains("xtaascorp2")) {
				url = new URL(XtaasConstants.XTAASCORP2_URL + "/spr/cacherefresh/campaign/" + campaign.getId());
			} else if (campaign.getHostingServer().contains("demandshore")) {
				url = new URL(XtaasConstants.DEMANDSHORE_URL + "/spr/cacherefresh/campaign/" + campaign.getId());
			} else {
				url = new URL(XtaasConstants.PEACEFUL_URL + "/spr/cacherefresh/campaign/" + campaign.getId());
			}
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			if (responseCode != 200) {
				logger.debug("Error occured while clearing cache.");
			}
		} catch (Exception e) {
			logger.error("Exception in building URL ; " + e);
			e.printStackTrace();
		}
	}

	@Override
	public Boolean resetAgentCampaignByCampaignId(String campaignId) {
		List<Team> teamList = teamRepository.findBySupervisorOverrideCampaignId(campaignId);
		if (teamList != null && teamList.size() > 0) {
			for (Team team : teamList) {
				if (team.getAgents() != null && team.getAgents().size() > 0) {
					for (TeamResource agent : team.getAgents()) {
						if (agent.getSupervisorOverrideCampaignId() != null) {
							if (agent.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
								PusherUtils.pushMessageToUser(agent.getResourceId(),
										XtaasConstants.CAMPAIGN_SETTINGS_CHANGE,
										"Campaign settings are modified, please relogin.");
							}
						}
					}
				}
			}
		}
		return true;
	}

	@Override
	public Boolean resetAgentCampaignForMultipleCampaigns(List<String> campaignIds) {
		List<Team> teamList = teamRepository.findListBySupervisorOverrideCampaignId(campaignIds);
		if (teamList != null && teamList.size() > 0 && campaignIds.size() > 0) {
			for (String campaignId : campaignIds) {
				for (Team team : teamList) {
					if (team.getAgents() != null && team.getAgents().size() > 0) {
						for (TeamResource agent : team.getAgents()) {
							if (agent.getSupervisorOverrideCampaignId() != null) {
								if (agent.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
									PusherUtils.pushMessageToUser(agent.getResourceId(),
											XtaasConstants.CAMPAIGN_SETTINGS_CHANGE,
											"Campaign settings are modified, please relogin.");
								}
							}
						}
					}
				}
			}
		}
		return true;
	}

	@Override
	public Boolean refreshOrganizations() {
		return organizationService.clearOrganizationsInMemory();
	}

	@Override
	public Boolean refreshAgents() {
		return agentService.clearAgentsInMemory();
	}
	
	@Override
	public Boolean refreshStateCodeCache() {
		prospectCacheServiceImpl.emptyStateCallConfig();
		prospectCacheServiceImpl.emptyStateCodeList();
		return true;
	}

	@Override
	public void getCountOfProspectsLoadedInMemory(String email) {
		Map<String, Integer> cacheCountMap = new HashMap<String, Integer>();
		List<String> activeCampaignIds = new ArrayList<String>(
				agentRegistrationService.getCampaignActiveAgentCount().keySet());
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		Runnable runnable = () -> {
			List<CampaignDTO> campaigns = campaignService
					.findActiveWithProjectedFields(Arrays.asList("enableProspectCaching"));
			if (campaigns != null && campaigns.size() > 0) {
				for (CampaignDTO activeCampaign : campaigns) {
					if (activeCampaignIds != null && activeCampaignIds.size() > 0
							&& !activeCampaignIds.contains(activeCampaign.getId())) {
						if (activeCampaign.isEnableProspectCaching()) {
							List<ProspectCallCacheDTO> dataInQueue = priorityQueueCacheOperationService
									.getCurrentState(activeCampaign.getId());
							if (dataInQueue != null && dataInQueue.size() > 0) {
								cacheCountMap.put(activeCampaign.getName(), dataInQueue.size());
							}
						} else {
							LinkedHashMap<String, List<ProspectCallDTO>> prospectsFromCache = prospectCacheServiceImpl
									.getCachedProspectCallLogOld();
							if (prospectsFromCache != null && prospectsFromCache.size() != 0) {
								if (prospectsFromCache.get(activeCampaign.getId()) != null
										&& prospectsFromCache.get(activeCampaign.getId()).size() > 0) {
									cacheCountMap.put(activeCampaign.getName(),
											prospectsFromCache.get(activeCampaign.getId()).size());
								}
							}
						}
					}
				}
				sendEmail(cacheCountMap, email);
			}
		};
		executorService.submit(runnable);
	}

	private void sendEmail(Map<String, Integer> cacheCountMap, String email) {
		String subject = "CampaignWise cache details.";
		String text = "<table width='100%' border='1' align='center'>" + "<tr align='center'>"
				+ "<td><b>Campaign Name <b></td>" + "<td><b>Cache Count<b></td>" + "</tr>";
		for (Map.Entry<String, Integer> cacheMap : cacheCountMap.entrySet()) {
			text = text + "<tr align='center'>" + "<td>" + cacheMap.getKey() + "</td>" + "<td>" + cacheMap.getValue()
					+ "</td>" + "</tr>";
		}
		XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, text);
	}

	@Override
	public String refreshCacheForCampaignIds(List<String> campaignIds, String hostEnv) {
		String val = "Cache Refreshed";
		StringBuilder sb = new StringBuilder();
		try {
			String host = HTTPS;
			if(hostEnv.contains(LOCALHOST)) {
				host = HTTP;
			}
			String url = host + hostEnv + API_URL;
			campaignIds.parallelStream().forEach(campaignId -> {
				String response = restClientService.makeGetRequest(url + campaignId, new HashMap<>());
				logger.debug("clearing cache for campaignId : [{}] and response : [{}] ", campaignId, response);
				if(!response.equals("true")){
					sb.append(response);
				}
			});
		} catch (Exception e) {
			logger.error("Error occured while clearing cache for campaign.", e);
		}
		String errorResponse = sb.toString();
		if(errorResponse.isEmpty()){
			return val;
		}
		return errorResponse;
	}

	@Override
	public void hardCacheRefreshCountryIsdCOdeMapForDatabuy() {
		logger.debug("########### DATABUY HARD CACHE REFRESH ISDCODE MAP STARTED ###########");
		plivoOutboundNumberService.hardCacheRefeshCountryIsdCodeMapForDatabuy();
		logger.debug("########### DATABUY HARD CACHE REFRESH ISDCODE MAP FINISHED ###########");
	}

	@Override
	public List<String> removeProspectCacheInactiveCampaigns() {
		List<String> prospectCacheRemovedCampaignIds = new ArrayList<String>();
		String eventId = UUID.randomUUID().toString();
		List<String> activeCampaignIds = new ArrayList<String>(
				agentRegistrationService.getCampaignActiveAgentCount().keySet());
		List<String> cachedProspectCallLogCampaignIds = new ArrayList<String>(
				priorityQueueCacheOperationService.getWholeCache().keySet());
		List<String> cachedProspectCallLogOldCampaignIds = new ArrayList<String>(
				prospectCacheServiceImpl.getCachedProspectCallLogOld().keySet());

		// remove active campaignIds from list
		cachedProspectCallLogCampaignIds.removeAll(activeCampaignIds);
		cachedProspectCallLogOldCampaignIds.removeAll(activeCampaignIds);

		for (String cachedProspectCallLogCampaignId : cachedProspectCallLogCampaignIds) {
			// remove prospect cache of inactive campaignIds
			priorityQueueCacheOperationService.clearCampaignCache(cachedProspectCallLogCampaignId);
			prospectCacheRemovedCampaignIds.add(cachedProspectCallLogCampaignId);
		}
		for (String cachedProspectCallLogOldCampaignId : cachedProspectCallLogOldCampaignIds) {
			// remove prospect cache of inactive campaignIds
			prospectCacheServiceImpl.clearCachedProspectCallLogOld(cachedProspectCallLogOldCampaignId);
			prospectCacheRemovedCampaignIds.add(cachedProspectCallLogOldCampaignId);
		}
		logger.info(new SplunkLoggingUtils("removeProspectCacheInactiveCampaigns", eventId)
				.eventDescription("removed prospect cache of inactive campaigns")
				.addField("activeCampaignIds", activeCampaignIds.toString())
				.addField("prospectCacheRemovedCampaignIds", prospectCacheRemovedCampaignIds.toString()).build());
		return prospectCacheRemovedCampaignIds;
	}

}
