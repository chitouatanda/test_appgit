package com.xtaas.service;

import java.util.List;

import org.springframework.dao.DuplicateKeyException;
import com.xtaas.db.entity.Partner;
import com.xtaas.web.dto.PartnerDTO;


public interface PartnerService {
	Partner findOne(String partnerId);
	void updatePartner(Partner partner);
	public Partner createPartner(PartnerDTO  partnerDTO) throws DuplicateKeyException;
	public PartnerDTO updatePartner(String partnerId, PartnerDTO partnerDTO);
	public List<String> retriveAll();
	
}
