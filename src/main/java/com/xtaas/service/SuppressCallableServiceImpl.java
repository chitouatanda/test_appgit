package com.xtaas.service;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;

@Service
public class SuppressCallableServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(SuppressCallableServiceImpl.class);

	@Autowired
	CampaignRepository campaignRepository;

	@Autowired
	SuppressionListService suppressionListService;

	@Autowired
	SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	@Async
	public void suppressCallable(String campaignId) {
		//	System.out.println("suppreesion called");
		//String campaignId = supressionList.getCampaignId();
		
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(campaignId);
		prospectCallLogList.addAll(prospectCallLogList0);
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1, campaignId);
		prospectCallLogList.addAll(prospectCallLogList1);
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2, campaignId);
		prospectCallLogList.addAll(prospectCallLogList2);
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3, campaignId);
		prospectCallLogList.addAll(prospectCallLogList3);
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4, campaignId);
		prospectCallLogList.addAll(prospectCallLogList4);
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5, campaignId);
		prospectCallLogList.addAll(prospectCallLogList5);
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6, campaignId);
		prospectCallLogList.addAll(prospectCallLogList6);
		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7, campaignId);
		prospectCallLogList.addAll(prospectCallLogList7);
		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8, campaignId);
		prospectCallLogList.addAll(prospectCallLogList8);
		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9, campaignId);
		prospectCallLogList.addAll(prospectCallLogList9);
		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,
				campaignId);
		prospectCallLogList.addAll(prospectCallLogList10);

		// List<ProspectCallLog> prospectCallLogList886 =
		// prospectCallLogRepository.findContactsByRetryCount(886, campaignId);
		// prospectCallLogList.addAll(prospectCallLogList886);
		List<ProspectCallLog> suppresionPCLOG = new ArrayList<ProspectCallLog>();
		int emailCount  = 0;
		int domainCount  = 0;

		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
//        	if(companies!=null && companies.size()>0){
//        		for(String companyName : companies){
//        			if(prospectCallLog.getProspectCall().getProspect().getCompany()!=null && !prospectCallLog.getProspectCall().getProspect().getCompany().isEmpty()){
//        				String pComapanyLower = prospectCallLog.getProspectCall().getProspect().getCompany().toLowerCase();
//        				if(companyName.contains(pComapanyLower) || pComapanyLower.contains(companyName)){
//        					suppresionPCLOG.add(prospectCallLog);
//        				}
//        			}
//        		}
//        	}
			//if (emailIds != null && emailIds.size() > 0) {
				//for (String email : emailIds) {
			if (prospectCallLog.getProspectCall().getProspect().getEmail() != null
				&& !prospectCallLog.getProspectCall().getProspect().getEmail().isEmpty()) {
				String pEmailLower = prospectCallLog.getProspectCall().getProspect().getEmail().toLowerCase();
				boolean isEmailSuppressed = suppressionListService.isEmailSuppressed(pEmailLower, campaignId,
						campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck());
				if (isEmailSuppressed) {
					suppresionPCLOG.add(prospectCallLog);
					emailCount = emailCount + 1;
				}
			}
				//}
			//}
			//if (domains != null && domains.size() > 0) {
			//for (String domain : domains) {
			if (prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
					&& !prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain")
							.toString().isEmpty()) {
				String pdomainLower = getSuppressedHostName(prospectCallLog.getProspectCall().getProspect()
						.getCustomAttributeValue("domain").toString()).toLowerCase();
				long supListCount =suppressionListNewRepository.countSupressionListByCampaignDomain(campaignId, pdomainLower);

				if (supListCount > 0) {
					suppresionPCLOG.add(prospectCallLog);
					domainCount =  domainCount+1;

				}
			}
			//}
			//}
		}
		try {
			if (suppresionPCLOG != null && suppresionPCLOG.size() > 0) {
				logger.debug("Please wait we are updating Record, It will take some time ......");
				for (ProspectCallLog prospectCallLog : suppresionPCLOG) {
					if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
						prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						prospectCallLog.getProspectCall().setCallRetryCount(144);
						prospectCallLog.setUpdatedDate(new Date());
						prospectCallLog.setUpdatedBy("SYSTEM_SUPRESSIONLIST");
						prospectCallLogRepository.save(prospectCallLog);
					}
				}
				logger.debug("suppresed records found :" + suppresionPCLOG.size());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	

		String message = null;
		String subject = null;
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (emailCount>0) {
			message = "File Uploaded Successfully" + "<br>" + "Total Suppression Email Upload : "
					+ Integer.toString(emailCount) + "<br>" + " Total Suppressed Prospects : "
					+ Integer.toString(suppresionPCLOG.size());
			subject = "Contact Supression Upload Status Success For " + campaign.getName();
		} 
		if(domainCount>0) {
			message = "File Uploaded Successfully" + "<br>" + "Total Companies Upload : "
					+ Integer.toString(domainCount) + "<br>" + "Total Suppressed Prospects : "
					+ Integer.toString(suppresionPCLOG.size());
			subject = "Company Supression Upload Status Success For " + campaign.getName();
		}

		XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, message);
		
	}

	// TODO need to check if we need to consider after dot in the domain anme
	private String getSuppressedHostName(String domain) {
		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}
			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty()) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if (domain.indexOf(".") > 0) { domain = domain.substring(0,
					 * domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if (domain.indexOf(".") > 0) { //domain = domain.substring(0,
					 * domain.indexOf(".")); return domain; } else {
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}
}