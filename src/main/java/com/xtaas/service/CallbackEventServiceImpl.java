package com.xtaas.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;

@Service
public class CallbackEventServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(CallbackEventServiceImpl.class);

	final private ProspectCacheServiceImpl prospectCacheServiceImpl;

	final private CallbackNotificationServiceImpl callbackNotificationServiceImpl;

	final private AgentRegistrationService agentRegistrationService;

	public CallbackEventServiceImpl(@Autowired ProspectCacheServiceImpl prospectCacheServiceImpl,
			@Autowired CallbackNotificationServiceImpl callbackNotificationServiceImpl,
			@Autowired AgentRegistrationService agentRegistrationService) {
		this.prospectCacheServiceImpl = prospectCacheServiceImpl;
		this.callbackNotificationServiceImpl = callbackNotificationServiceImpl;
		this.agentRegistrationService = agentRegistrationService;
	}

	public void sendCallbackProspectToAgent()
			throws InterruptedException, JsonGenerationException, JsonMappingException, IOException {
		CallbackProspectCacheDTO callBackEventDTO = prospectCacheServiceImpl.getCallbackRecordsFromEventCache();
		logger.info("Callback from cache :- [{}]", callBackEventDTO);
		if (callBackEventDTO != null) {
			 String notifiedAgentId = callbackNotificationServiceImpl
			 		.getCallbackNotifiedAgentId(callBackEventDTO.getProspectCallId());
			unRegisterAgent(notifiedAgentId);
			String prospectCallAsJson = JSONUtils.toJson(callBackEventDTO);
			PusherUtils.pushMessageToUser(callBackEventDTO.getAgentId(), XtaasConstants.CALLBACK_EVENT, prospectCallAsJson);
			logger.info(
					"sendCallbackProspectToAgent() :: Sent callback prospect to agent [{}], campaignId [{}], prospectCallId [{}]",
					callBackEventDTO.getAgentId(), callBackEventDTO.getCampaignId(),
					callBackEventDTO.getProspectCallId());
			 callbackNotificationServiceImpl.removeCallbackNotifiedAgentId(callBackEventDTO.getProspectCallId());
		} else {
			logger.info("sendCallbackProspectToAgent() :: No callback found from DB." + new Date());
		}
	}

	private void unRegisterAgent(String agentId) {
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		if (agentCall != null) {
			logger.info(
					"unRegisterAgent() :: unregistered agent with [{}] and campaignId [{}]",agentId,agentCall.getCampaignId());
			agentRegistrationService.unregisterAgent(agentCall);
		} else {
			logger.info(
					"unRegisterAgent() :: agentCall not found to unregister agent id [{}]",agentId);
		}
	}

}