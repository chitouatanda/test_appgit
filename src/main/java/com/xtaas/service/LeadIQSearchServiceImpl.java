package com.xtaas.service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.zoominfo.service.DataSlice;
import com.xtaas.infra.zoominfo.service.ZoomInfoDataBuy;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.core.MediaType;

import static java.util.stream.Collectors.toList;

@Service
public class LeadIQSearchServiceImpl implements LeadIQSearchService {

	private static final Logger logger = LoggerFactory.getLogger(LeadIQSearchServiceImpl.class);

	static Properties properties = new Properties();

	@Autowired
	private ManualProspectSearchServiceImpl manualProspectSearchServiceImpl;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	PlivoOutboundNumberService plivoOutboundNumberSerivce;

	@Autowired
	private AbmListServiceImpl abmListServiceImpl;

	@Autowired
	StateCallConfigRepository stateCallConfigRespository;

	@Autowired
	ZoomInfoSearchServiceImpl zoomInfoSearchServiceImpl;

	@Autowired
	InsideviewService insideviewService;

	@Autowired
	CampaignService campaignService;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	CampaignMetaDataRepository campaignMetaDataRepository;

	@Autowired
	GlobalContactRepository globalContactRepository;

	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ZoomInfoDataBuy zoomInfoDataBuy;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private PhoneDncService phoneDncService;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private DataSlice dataSlice;

	@Autowired
	private LeadIQDataSourceRepository leadIQDataSourceRepository;

	@Autowired
	private DataBuyMappingRepository dataBuyMappingRepository;

	@Autowired
	private DNCListRepository dncListRepository;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;

	@Override
	public ManualProspectSearchDTO manualProspectSearch(Prospect prospectInfo, String campaignId, String agentId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		ManualProspectSearchDTO dto = new ManualProspectSearchDTO();
		dto.setSimilarProspect(campaign.isSimilarProspect());
		// get the exact match prospect
		List<Prospect> responseProspects = searchContactDetailsMultiple(prospectInfo, campaign);
		if (CollectionUtils.isNotEmpty(responseProspects)) {
			dto.setManualSearchProspect(convertProspectObj(responseProspects));
			logger.info(new SplunkLoggingUtils("manualProspectSearch", campaignId)
					.eventDescription("primary prospect found for manual search prospect").campaignId(campaignId)
					.agentId(agentId).build());
		}
		if (campaign.isSimilarProspect()) {
			if (responseProspects != null && responseProspects.size() > 0) {
				if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null) {
					manualProspectSearchServiceImpl.manualProspectMap.remove(agentId);
				}
				List<Prospect> similarProspect = manualProspectSearchServiceImpl
						.getSimilarProspect(responseProspects.get(0), campaign, agentId, true);
				logger.info("Campaing [{}] fetch similar prospects [{}]", campaignId, similarProspect);
				dto.setSimilarSearchProspect(convertProspectObj(similarProspect));
				if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null) {
					manualProspectSearchServiceImpl.manualProspectMap.remove(agentId);
				}
			} else {
				logger.info("Primary prospects [{}] not present for campaing [{}] ", responseProspects, campaignId);
				List<Prospect> similarProspect = manualProspectSearchServiceImpl.getSimilarProspect(prospectInfo,
						campaign, agentId, false);
				logger.info("Campaing [{}] fetch similar prospects [{}]", campaignId, similarProspect);
				dto.setSimilarSearchProspect(convertProspectObj(similarProspect));
				if (manualProspectSearchServiceImpl.manualProspectMap.get(agentId) != null) {
					manualProspectSearchServiceImpl.manualProspectMap.remove(agentId);
				}
			}
		}
		return dto;
	}

	private List<LeadIQProspectDTO> convertProspectObj(List<Prospect> pList) {
		List<LeadIQProspectDTO> leadIQProspectList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(pList)) {
			for (Prospect p : pList) {
				try {
					if (p != null) {
						LeadIQProspectDTO leadIQProspect = new LeadIQProspectDTO();
						if (StringUtils.isNotBlank(p.getSourceId())) {
							leadIQProspect.setSourceId(p.getSourceId());
						}
						if (StringUtils.isNotBlank(p.getFirstName()) || StringUtils.isNotBlank(p.getLastName())) {
							leadIQProspect.setName("" + p.getFirstName() + " " + p.getLastName());
						}
						if (StringUtils.isNotBlank(p.getTitle())) {
							leadIQProspect.setTitle(p.getTitle());
						}
						if (StringUtils.isNotBlank(p.getCompany())) {
							leadIQProspect.setCompany(p.getCompany());
						}
						if (p.getCustomAttributes() != null && p.getCustomAttributes().get("domain") != null
								&& p.getCustomAttributes().get("domain").toString() != null) {
							leadIQProspect.getCustomAttributes().put("domain",
									p.getCustomAttributes().get("domain").toString());
						}
						leadIQProspectList.add(leadIQProspect);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return leadIQProspectList;
	}

	@Override
	public Prospect searchContactDetails(Prospect prospect) {
		LeadIQSearchResponse responseObj = null;
		if (prospect.getName() != null && prospect.getCompany() != null && !prospect.getName().equalsIgnoreCase(" ")
				&& !prospect.getCompany().isEmpty()) {
			// Search by Name and Company
			responseObj = searchContactByNameAndCompany(prospect);
		} else if (prospect.getName() != null && prospect.getCustomAttributes().get("domain") != null
				&& !prospect.getName().equalsIgnoreCase(" ")
				&& !((String) prospect.getCustomAttributes().get("domain")).isEmpty()) {
			// Search by Name and Domain
			String domain = (String) prospect.getCustomAttributes().get("domain");
			responseObj = searchContactByNameAndDomain(prospect, domain);
		} else if (prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
			// Search by getEmail
			responseObj = searchContactByEmail(prospect);
		}
		if (responseObj != null) {
			Prospect prospectInfo = new Prospect();
			if (responseObj.getData().getSearchPeople().getTotalResults() > 0) {
				prospectInfo.setSource("LeadIQ");
				prospectInfo.setDataSource("Xtaas Data Source");
				prospectInfo.setSourceType("INTERNAL");
				prospectInfo.setPhone("");
				prospectInfo.setPhoneType("");
				LeadIQResult result = responseObj.getData().getSearchPeople().getResults().get(0);
				String personIDLeadIQ = result.getId();
				prospectInfo.setSourceId(personIDLeadIQ);
				if (result.getCurrentPositions() != null && result.getCurrentPositions().size() > 0) {
					LeadIQCurrentPosition contactDetails = result.getCurrentPositions().get(0);
					prospectInfo.setCompany(contactDetails.getCompanyInfo().getName());
					prospectInfo.setIndustry(contactDetails.getCompanyInfo().getIndustry());
					prospectInfo.setFirstName(result.getName().getFirst());
					prospectInfo.setLastName(result.getName().getLast());
					for (LeadIQEmail email : contactDetails.getEmails()) {
						if (email.getStatus().equalsIgnoreCase("Verified") && email.getType().equalsIgnoreCase("workemail") && !email.getValue().equalsIgnoreCase("*redacted*")) {
							prospectInfo.setEmail(email.getValue());
							break;
						}
					}
					boolean first = true;
					for (LeadIQPhone phone : contactDetails.getPhones()) {
						// System.out.println("phone number" + phone);
						if (phone.getStatus().equalsIgnoreCase("Verified")) {
							if (phone.getType().equalsIgnoreCase("WorkPhone")) {
								prospectInfo.setPhone(phone.getValue());
								prospectInfo.setPhoneType(phone.getType());
								break;
							}
							if (first) {
								first = false;
								prospectInfo.setPhone(phone.getValue());
								prospectInfo.setPhoneType(phone.getType());
							}
						}
					}
					if (contactDetails.getCompanyInfo().getPhones() != null
							&& contactDetails.getCompanyInfo().getPhones().size() > 0) {
						prospectInfo.setCompanyPhone(contactDetails.getCompanyInfo().getPhones().get(0));
					}

					prospectInfo.setTitle(contactDetails.getTitle());

					if (!prospectInfo.getPhone().isEmpty()) {
						String extension = "";
						String countryCode = getCountryCode(prospectInfo);
						String phoneNumber = getPhoneNumber(prospectInfo);
						if (prospectInfo.getPhone().contains("ext")) {
							extension = getExtension(prospectInfo);
						}
//						if (countryCode != null) {
//							prospectInfo.setPhone("+".concat(countryCode.concat(" " + phoneNumber)));
//						}
						if (!prospectInfo.getPhone().contains("+")) {
							prospectInfo.setPhone("+".concat(" " + phoneNumber));
						}else {
							prospectInfo.setPhone(phoneNumber);
						}
						// System.out.println("country Code" + countryCode + "PhoneNumber" + phoneNumber
						// + "Extension" + extension);
						// if (!countryCode.equals("1")) {
						// prospectInfo.setPhone(countryCode.concat(phoneNumber));
						// } else {
						// prospectInfo.setPhone(phoneNumber);
						// }
						// setCountry
						if (contactDetails.getCompanyInfo().getCountry() != null
								&& !contactDetails.getCompanyInfo().getCountry().isEmpty()) {
							prospectInfo.setCountry(contactDetails.getCompanyInfo().getCountry());
						} else {
							prospectInfo.setCountry(getCountryName(countryCode));
						}
						// get state code and timezone
						StateCallConfig scc = getStateCodeAndTimeZone(
								contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1(), prospectInfo.getCountry());
						if (scc != null) {
							prospectInfo.setTimeZone(scc.getTimezone());
							prospectInfo.setStateCode(scc.getStateCode());
						}
						if (contactDetails.getCompanyInfo() != null && contactDetails.getCompanyInfo().getLocationInfo() != null) {
							Map<String, String> companyAddress = insertCompanyAddressInMap(contactDetails); // added company address
							prospectInfo.setCompanyAddress(companyAddress);
						}
						prospectInfo.setCustomAttribute("numberOfEmployees",
								contactDetails.getCompanyInfo().getNumberOfEmployees().toString());
						prospectInfo.setZoomPersonUrl(result.getLinkedin().getLinkedinUrl());
						prospectInfo.setCustomAttribute("domain", contactDetails.getCompanyInfo().getDomain()); // added domain
						if (result.getLinkedin() != null) {
							prospectInfo.setCustomAttribute("linkedinId", result.getLinkedin().getLinkedinId());
							prospectInfo.setCustomAttribute("linkedinUrl", result.getLinkedin().getLinkedinUrl());
						}

						return prospectInfo;
					}
				}

			}
		}
		return null;
	}

	@Override
	public List<Prospect> searchContactDetailsMultiple(Prospect prospect, Campaign campaign) {
		List<Prospect> prospectsLeadIQList = new ArrayList<Prospect>();
		plivoOutboundNumberService.cacheAllPlivoOutboundNumberForDatabuy();

		// check email suppression before searching prospect
		if (!StringUtils.isEmpty(prospect.getEmail()) && suppressionListService.isEmailSuppressed(prospect.getEmail(),
				campaign.getId(), campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck())) {
			throw new IllegalArgumentException("Email is in suppression list.");
		}

		List<KeyValuePair<String, Integer>> manualPriorityList = new ArrayList<KeyValuePair<String, Integer>>();
		// check if you have priority set at campaign level
		manualPriorityList = campaign.getManualDataSourcePriority();
		if (CollectionUtils.isEmpty(manualPriorityList)) {
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			CampaignMetaData cmd = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
			if (organization.getCampaignDefaultSetting() != null
					&& organization.getCampaignDefaultSetting().getManualDataSourcePriority() != null) {
				manualPriorityList = organization.getCampaignDefaultSetting().getManualDataSourcePriority();
			} else if (cmd != null && cmd.getManualDataSourcePriority() != null
					&& !cmd.getManualDataSourcePriority().isEmpty()) {
				manualPriorityList = cmd.getManualDataSourcePriority();
			}
		}

		TreeMap<Integer, String> sortedMap = new TreeMap<>();
		if (manualPriorityList != null && manualPriorityList.size() > 0) {
			for (KeyValuePair<String, Integer> keyvaluepair : manualPriorityList) {
				sortedMap.put(keyvaluepair.getValue(), keyvaluepair.getKey());
			}
			for (Map.Entry<Integer, String> entry : sortedMap.entrySet()) {
				if (CollectionUtils.isEmpty(prospectsLeadIQList) && entry.getValue().equalsIgnoreCase("ZOOMINFO")
						&& entry.getKey() > 0) {
					// get Zoom data
					prospectsLeadIQList = getZoomData(prospect, campaign);
				}
				if (CollectionUtils.isEmpty(prospectsLeadIQList) && entry.getValue().equalsIgnoreCase("LEADIQ")
						&& entry.getKey() > 0) {
					// get LeadIqData
					prospectsLeadIQList = getLeadIQData(prospect, campaign);
				}
				if (CollectionUtils.isEmpty(prospectsLeadIQList) && entry.getValue().equalsIgnoreCase("INSIDEVIEW")
						&& entry.getKey() > 0) {
					// get insideView
					prospectsLeadIQList = getInsideViewData(prospect);
				}
			}
		} else {
			// default to leadiq data
			prospectsLeadIQList = getLeadIQData(prospect, campaign);
		}
		saveGlobalContact(prospectsLeadIQList);
		return prospectsLeadIQList;
	}

	private void saveGlobalContact(List<Prospect> prospectList){
		try {
			if(prospectList != null && !prospectList.isEmpty() && prospectList.size() > 0){
				for(Prospect prospect : prospectList){
					zoomInfoDataBuy.saveGlobalContact(prospect);
				}
			}
		}catch (Exception e){
			//e.printStackTrace();
		}
	}

	private List<Prospect> getZoomData(Prospect prospect,Campaign campaign) {
		List<Prospect> prospectsZoomInfoList = new ArrayList<Prospect>();
		//Check in zoominfo first, if not found the check for leadiq
		Prospect zoomProspect = zoomInfoSearchServiceImpl.getCampaignZoomProspect(prospect,campaign);
		if(zoomProspect!=null && zoomProspect.getPhone()!=null  && zoomProspect.getPhone().startsWith("+")) {
			prospectsZoomInfoList.add(zoomProspect);
			return prospectsZoomInfoList;
		}

		return prospectsZoomInfoList;
	}

	private List<Prospect> getInsideViewData(Prospect prospect) {
		List<Prospect> prospectsInsideViewList = new ArrayList<Prospect>();
		//Check in InsideView first, if not found the check for leadiq
		Prospect ivProspect = insideviewService.getManualProspect(prospect);
		if(ivProspect!=null && StringUtils.isNotBlank(ivProspect.getPhone())) {
			prospectsInsideViewList.add(ivProspect);
			return prospectsInsideViewList;
		}

		return prospectsInsideViewList;
	}


	public List<Prospect> getLeadIQData(Prospect prospect, Campaign campaign) {
		LeadIQSearchResponse responseObj = null;
		CampaignCriteria cc = getCampaignCriteria(campaign);
		List<Prospect> prospectsLeadIQList = new ArrayList<Prospect>();
		if (prospect.getFirstName() != null && prospect.getName() != null && prospect.getCompany() != null
				&& !prospect.getName().equalsIgnoreCase(" ") && !prospect.getCompany().isEmpty()) {
			// Search by Name and Company
			responseObj = searchContactByNameAndCompany(prospect);
			// if  no result from Name and Company, search by Name and Domain
			if (responseObj == null || responseObj.getData() == null || responseObj.getData().getSearchPeople() == null || responseObj.getData().getSearchPeople().getTotalResults() <= 0) {
				String domain = (String) prospect.getCustomAttributes().get("domain");
				responseObj = searchContactByNameAndDomain(prospect, domain);
			}
		} else if (prospect.getFirstName() != null && prospect.getName() != null && prospect.getCustomAttributes().get("domain") != null
				&& !prospect.getName().equalsIgnoreCase(" ")
				&& !((String) prospect.getCustomAttributes().get("domain")).isEmpty()) {
			// Search by Name and Domain
			String domain = (String) prospect.getCustomAttributes().get("domain");
			responseObj = searchContactByNameAndDomain(prospect, domain);
		} else if (prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
			// Search by getEmail
			responseObj = searchContactByEmail(prospect);
		}
		else if (prospect.getLinkedInURL() != null && !prospect.getLinkedInURL().isEmpty()){
			responseObj = searchContactByLinkedIn(prospect);
		}

		if (responseObj != null && responseObj.getData() != null && responseObj.getData().getSearchPeople() != null) {
			if (responseObj.getData().getSearchPeople().getTotalResults() > 0) {
				for (LeadIQResult result : responseObj.getData().getSearchPeople().getResults()) {
					try{
					//	int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordBySourceId(campaign.getId(),result.getId());
					//	if(uniqueRecord == 0) {
							Prospect prospectInfo = new Prospect();
							prospectInfo.setLeadIQResult(result);
							prospectInfo.setSource("LeadIQ");
							prospectInfo.setDataSource("Xtaas Data Source");
							prospectInfo.setSourceType("INTERNAL");
							prospectInfo.setPhone("");
							prospectInfo.setPhoneType("");
							// LeadIQResult result = responseObj.getData().getSearchPeople().getResults().get(0);
							String personIDLeadIQ = result.getId();
							prospectInfo.setSourceId(personIDLeadIQ);
							if (result.getCurrentPositions() != null && result.getCurrentPositions().size() > 0) {
								LeadIQCurrentPosition contactDetails = result.getCurrentPositions().get(0);
								prospectInfo.setCompany(contactDetails.getCompanyInfo().getName());
								prospectInfo.setIndustry(contactDetails.getCompanyInfo().getIndustry());
								prospectInfo.setFirstName(result.getName().getFirst());
								prospectInfo.setLastName(result.getName().getLast());
								for (LeadIQEmail email : contactDetails.getEmails()) {
									if (email.getStatus().equalsIgnoreCase("Verified")
											&& email.getType().equalsIgnoreCase("workemail") && !email.getValue().equalsIgnoreCase("*redacted*")) {
										prospectInfo.setEmail(email.getValue());
										break;
									}
								}
								if(prospectInfo.getEmail() == null || prospectInfo.getEmail().isEmpty()) {
									if(prospect.getEmail() != null && !prospect.getEmail().isEmpty()) {
										prospectInfo.setEmail(prospect.getEmail());
									}
								}
								if(prospect.getLinkedInURL() != null && !prospect.getLinkedInURL().isEmpty()) {
									prospectInfo.setLinkedInURL(prospect.getLinkedInURL());
								}
								if(result.getPersonalPhones() != null) {
									prospectInfo.setPhone(result.getPersonalPhones().getValue());
									prospectInfo.setPhoneType(result.getPersonalPhones().getType());
								}else {
									boolean first = true;
									for (LeadIQPhone phone : contactDetails.getPhones()) {
										//System.out.println("phone number" + phone);
										if (phone.getStatus().equalsIgnoreCase("Verified")) {
											if (phone.getType().toLowerCase().contains("personalphone")) {
												prospectInfo.setPhone(phone.getValue());
												prospectInfo.setPhoneType(phone.getType());
												break;
											}
											if (phone.getType().equalsIgnoreCase("workphone") || first) {
												first = false;
												prospectInfo.setPhone(phone.getValue());
												prospectInfo.setPhoneType(phone.getType());
											}
										}
									}
								}

								if (contactDetails.getCompanyInfo().getPhones() != null && contactDetails.getCompanyInfo().getPhones().size() > 0) {
									prospectInfo.setCompanyPhone(contactDetails.getCompanyInfo().getPhones().get(0));
								}


								prospectInfo.setTitle(contactDetails.getTitle());

								if (prospectInfo.getPhone() != null && !prospectInfo.getPhone().isEmpty()
										&& contactDetails.getTitle() != null && !contactDetails.getTitle().isEmpty()) {
									String extension = "";
									String countryCode = getCountryCode(prospectInfo);
									String phoneNumber = getPhoneNumber(prospectInfo);
									if (prospectInfo.getPhone().contains("ext")) {
										extension = getExtension(prospectInfo);
									}
//						if (countryCode != null) {
//							prospectInfo.setPhone("+".concat(countryCode.concat(" " + phoneNumber)));
//						}
									if (!prospectInfo.getPhone().contains("+")) {
										prospectInfo.setPhone("+".concat(" " + phoneNumber));
									}else {
										prospectInfo.setPhone(phoneNumber);
									}
									//System.out.println("country Code" + countryCode + "PhoneNumber" + phoneNumber + "Extension" + extension);
//						if (!countryCode.equals("1")) {
//							prospectInfo.setPhone(countryCode.concat(phoneNumber));
//						} else {
//							prospectInfo.setPhone(phoneNumber);
//						}
									//setCountry
									if (contactDetails.getCompanyInfo().getCountry() != null && !contactDetails.getCompanyInfo().getCountry().isEmpty()) {
										prospectInfo.setCountry(contactDetails.getCompanyInfo().getCountry());
									} else {
										prospectInfo.setCountry(getCountryName(countryCode));
									}
									//  get state code and timezone
									if(contactDetails.getCompanyInfo().getLocationInfo() != null && contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1() != null && prospectInfo.getCountry() != null) {
										StateCallConfig scc = getStateCodeAndTimeZone(contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1(), prospectInfo.getCountry());
										if (scc != null) {
											prospectInfo.setTimeZone(scc.getTimezone());
											prospectInfo.setStateCode(scc.getStateCode());
										}
									}
									if (contactDetails.getCompanyInfo() != null && contactDetails.getCompanyInfo().getLocationInfo() != null) {
										Map<String ,String> companyAddress = insertCompanyAddressInMap(contactDetails);  //added company address
										prospectInfo.setCompanyAddress(companyAddress);
									}
									if(contactDetails.getCompanyInfo()!=null && contactDetails.getCompanyInfo().getNumberOfEmployees()!=null && !contactDetails.getCompanyInfo().getNumberOfEmployees().toString().isEmpty()) {
										prospectInfo.setCustomAttribute("numberOfEmployees", contactDetails.getCompanyInfo().getNumberOfEmployees().toString());
									}
									prospectInfo.setZoomPersonUrl(result.getLinkedin().getLinkedinUrl());
									prospectInfo.setCustomAttribute("domain", contactDetails.getCompanyInfo().getDomain()); // added domain
									if (result.getLinkedin() != null) {
										prospectInfo.setCustomAttribute("linkedinId", result.getLinkedin().getLinkedinId());
										prospectInfo.setCustomAttribute("linkedinUrl", result.getLinkedin().getLinkedinUrl());
									}
									boolean itemExists = prospectsLeadIQList.stream().anyMatch(c -> c.getPhone().equals(prospectInfo.getPhone()));
									if(itemExists){
										continue;
									}
									boolean isValidCountryPhone = true;
									boolean isValidCountry = isValidCountry(cc, prospectInfo.getCountry());
									if(CollectionUtils.isNotEmpty(cc.getCountryList())){
										isValidCountryPhone =  zoomInfoDataBuy.isValidCountryPhone(prospectInfo);
									}
									
									if (checkDNC(prospectInfo, campaign)) {
										continue;
									}

									if(isValidCountry && isValidCountryPhone){
										prospectsLeadIQList.add(prospectInfo);
										if(prospectsLeadIQList.size() > 5){
											return prospectsLeadIQList;
										}
									}

								}
							}
					//	}

					}catch (Exception e){

					}

				}
			}
		}
		return prospectsLeadIQList;
	}

	public boolean isValidCountry(CampaignCriteria cc, String prospectCountry) {
		boolean isValid = false;
		if(StringUtils.isNotBlank(prospectCountry)){
			List<String> ccCountryList = cc.getCountryList();
			List<String> newList  = new ArrayList<>();
			newList.addAll(ccCountryList);
			boolean containsUSA = newList.stream().anyMatch("usa"::equalsIgnoreCase);
			boolean containsUnitedStates = newList.stream().anyMatch("United States"::equalsIgnoreCase);
			if(containsUSA){
				newList.add("United States");
			}else if(containsUnitedStates){
				newList.add("usa");
			}
			boolean isValidCntry = newList.stream().anyMatch(prospectCountry::equalsIgnoreCase);
			if(isValidCntry){
				isValid = true;
			}
		}else{
			isValid = false;
		}

		return isValid;
	}

	private LeadIQSearchResponse getContactSearchResponse(String payload) {
		logger.debug("Records are activating of LeadIq, Details buy URL : [{}] ", payload);
		String apiKey = ApplicationEnvironmentPropertyUtils.getLeadIQAPIKey();
		String leadIQAPIURL = ApplicationEnvironmentPropertyUtils.getLeadIQUrl();
		String apiKeyEncoded = Base64.getEncoder().encodeToString(apiKey.getBytes());
		String responseJsonString = null;
		try {
			URL url =new URL(leadIQAPIURL);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty ("Authorization", "Basic " + apiKeyEncoded);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_TYPE.toString());
			connection.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
				wr.writeBytes(payload);
				wr.flush();
			}
			int responseCode = connection.getResponseCode();
			logger.debug("Detail buy response code of LeadIQ [{}] ", responseCode);

			BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response1= new StringBuffer();
			while((inputline=bufferedReader.readLine())!=null)
			{
				response1.append(inputline);
			}
			bufferedReader.close();
			responseJsonString = response1.toString();
		} catch (Exception e) {
			logger.error("Error while actual buy the records from LeadIQ, getContactSearchResponse() Exception at activation of Data LeadIQ. Exception [{}] : ", e);
			return null;
		}

		/*Client client = ClientBuilder.newClient();
		Entity payloadEntity = Entity.json(payload);
		Response response = client.target(leadIQAPIURL).request(MediaType.APPLICATION_JSON_TYPE)
				.header("Authorization", "Basic " + apiKeyEncoded).post(payloadEntity);
		String responseJsonString = response.readEntity(String.class);*/

		LeadIQSearchResponse reponseObj = null;
		if (responseJsonString != "") {
			try {
				reponseObj = new ObjectMapper().readValue(responseJsonString, LeadIQSearchResponse.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reponseObj;
	}

	private LeadIQSearchResponse searchContactByNameAndCompany(Prospect prospectInfo) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }\",  \"variables\": {  \"input\": {      \"fullName\": \"");
		payload.append(prospectInfo.getName());
		payload.append("\",  \"company\": { \"name\": \"");
		payload.append(prospectInfo.getCompany());
		payload.append("\" }}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactByNameAndDomain(Prospect prospectInfo, String domain) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }\",    \"variables\": { \"input\": {        \"fullName\": \"");
		payload.append(prospectInfo.getName());
		payload.append("\", \"company\": { \"domain\": \"");
		payload.append(domain);
		payload.append("\" }}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactById(String id) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{   \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }  \",   \"variables\": {  \"input\": {  \"id\":\"");
		payload.append(id);
		payload.append("\"}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactByEmail(Prospect prospectinfo) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{   \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo {name numberOfEmployees industry domain address country  phones locationInfo { street1 street2 city areaLevel1 country postalCode formattedAddress} } emails { value type status } phones { value type status } } } } }  \",   \"variables\": {  \"input\": {  \"email\":\"");
		payload.append(prospectinfo.getEmail());
		payload.append("\"}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	private LeadIQSearchResponse searchContactByLinkedIn(Prospect prospectInfo) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo { name industry } emails { value type status } phones { value type status } } } } }\", \"variables\": {\"input\": { \"linkedinUrl\": \"");
				//"{ \"query\": \"query SearchPeople($input: SearchPeopleInput!) { searchPeople(input: $input) { totalResults hasMore results { _id name { first last } linkedin { linkedinUrl linkedinId } currentPositions { title companyInfo { name industry } emails { value type status } phones { value type status } } } } }\",   \"variables\\\": {\"input\\\": { \"linkedinUrl\": \"");
		payload.append(""+prospectInfo.getLinkedInURL()); // need to append linkedinurl
		payload.append("\"}}}");
		LeadIQSearchResponse responseObj = getContactSearchResponse(payload.toString());
		return responseObj;
	}

	public boolean isValidProspect(Prospect prospect, Campaign campaign,Set<String> countryList){
		boolean isValid = true;
		try{
			if(prospect == null){
				logger.error("Validation failed Prospect is empty due to phone is not present for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			if( StringUtils.isBlank(prospect.getPhone())){
				logger.error("Validation failed phone is not present for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			if(!isValidState(prospect)){
				logger.error("Validation failed state code is not present for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			if(checkDNC(prospect,campaign)){
				logger.error("Validation failed in DNC for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			boolean isValidCountryPhone = zoomInfoDataBuy.isValidCountryPhone(prospect);
			if(!isValidCountryPhone) {
				logger.error("Validation failed in not a valid country phone for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			if(prospect.getTimeZone().equalsIgnoreCase("Not Found")){
				logger.error("Validation failed timezone not present for leadIQ in campaign [{}] ", campaign.getId());
				return false;
			}
			if(!isValidCountry(prospect,countryList)){
				logger.error("Validation failed country is not match for leadIQ in campaign [{}] . Campaign country [{}] and leadIq country [{}]", campaign.getId(),countryList, prospect.getCountry());
				return false;
			}
		}catch (Exception e){
			logger.error("Error while validating prospect for leadIQ in campaign [{}] . Exception [{}] . isValidProspect() ",campaign.getId(), e);
			return false;
		}

		return isValid;
	}

	public boolean isValidCountry(Prospect prospect, Set<String> countryList){
		if(CollectionUtils.isNotEmpty(countryList) && StringUtils.isNotBlank(prospect.getCountry())){
			boolean containsSearchStr = countryList.stream().anyMatch(prospect.getCountry() ::equalsIgnoreCase);
			if(containsSearchStr){
				return true;
			}
		}
		return false;
	}

	public boolean isValidState(Prospect prospect){
		if(StringUtils.isBlank(prospect.getStateCode())){
			return false;
		}else {
			return true;
		}
	}



	private boolean checkDNCByNameAndNumber(Prospect prospect) {
		boolean isDNC = false;
		List<DNCList> dncDBList = dncListRepository.findPhoneNumber(prospect.getPhone(),
				prospect.getFirstName(),prospect.getLastName());
		if(dncDBList != null && dncDBList.size() > 0) {
			isDNC = true;
		}
		return isDNC;
	}

	public void leadIQDetailedAPI(List<String> ids,List<ProspectCallLog> callLogList, Campaign campaign, DataBuyQueue dbq){
		FileReader reader = null;
		try {
			reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
			properties.load(reader);
		} catch (Exception e) {
			logger.error("Error while loading timezonareacodemapping.properties for campaign [{}]. Exception [{}] ", campaign.getId(),e);
			return;
		}
		List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(ids)){
			Set<String> countryList  =  getCountryList(callLogList);
			for(String id : ids){
				try {
					LeadIQSearchResponse response = searchContactById(id);
					if(response != null){
						ProspectCallLog dbProspectCallLog = getProspectCallLog(id, callLogList);
						Prospect prospect = mapProspectObject(response,dbProspectCallLog);
						if(isValidProspect(prospect,campaign, countryList)){
							if(dbProspectCallLog != null){
								ProspectCall prospectCall = dbProspectCallLog.getProspectCall();
								prospectCall.setProspect(prospect);
								dbProspectCallLog.setProspectCall(prospectCall);
								if(dbq !=  null && dbq.isRestFullAPI()){
									prospectCall.setCallRetryCount(786);
									dbProspectCallLog.setStatus(ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
								}else {
									prospectCall.setCallRetryCount(0);
									dbProspectCallLog.setStatus(ProspectCallLog.ProspectCallStatus.QUEUED);
								}
								dbProspectCallLog.getProspectCall().setSubStatus("BUSY");
								dbProspectCallLog.setLeadIQBuy("Yes");
								// will not buy those record whose empsize greater then 10k
								dbProspectCallLog = insertProspectCallLog(dbProspectCallLog);

								LeadIQDataSource leadIQDataSource = leadIQDataSourceRepository.findByPeopleId(id);
								if(leadIQDataSource == null){
									LeadIQDataSource leadIQData = new LeadIQDataSource();
									leadIQData.setPeopleId(id);
									leadIQData.setProspect(prospect);
									leadIQDataSourceRepository.save(leadIQData);
								}
								prospectCallLogs.add(dbProspectCallLog);
							}
						}else{
							ProspectCall prospectCall = dbProspectCallLog.getProspectCall();
							if(prospect != null){
								prospectCall.setProspect(prospect);
							}
							dbProspectCallLog.setProspectCall(prospectCall);
							updateLeadIQStatusToFailed(dbProspectCallLog);
						}
					}else {
						logger.error("No records found in LeadIq for personId : [{}] ", id);
					}

				}catch (Exception e){
					logger.error("Error while actual buy the records from LeadIQ, leadIQDetailedAPI() Exception at activation of Data LeadIQ, Exception [{}]  : ", e);
					e.printStackTrace();
				}
			}
		}
		if(prospectCallLogs != null && !prospectCallLogs.isEmpty() && prospectCallLogs.size() > 0){
			dataSlice.tagSliceAndQualityScore(prospectCallLogs,campaign);

			if(dbq != null && dbq.isRestFullAPI()){
				dbq = dataBuyQueueRepository.findOneById(dbq.getId());
				dbq.setLeadIqPurchasedCount(Long.valueOf(prospectCallLogs.size()));
				dbq.setStatus(DataBuyQueue.DataBuyQueueStatus.COMPLETED.toString());
				dataBuyQueueRepository.save(dbq);
				logger.info("Data buy completed for campaign [{}] by RestFull API. Total records bought [{}] ",
						campaign.getId(), String.valueOf(prospectCallLogs.size()));
			}
		}

	}

	private Set<String> getCountryList(List<ProspectCallLog> prospectReviwedList){
		Set<String> countryList = new HashSet<>();
		if(CollectionUtils.isNotEmpty(prospectReviwedList)){
			countryList = prospectReviwedList.stream()
					.map(rev -> rev.getProspectCall().getProspect().getCountry())
					.collect(Collectors.toSet());
		}
		return countryList;
	}

	private void updateLeadIQStatusToFailed(ProspectCallLog dbProspectCallLog){
		dbProspectCallLog.getProspectCall().setCallRetryCount(1000);
		dbProspectCallLog.setStatus(ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		dbProspectCallLog.getProspectCall().setSubStatus("Failed");
		dbProspectCallLog.setLeadIQBuy("Failed");
		prospectCallLogRepository.save(dbProspectCallLog);
	}

	private ProspectCallLog getProspectCallLog(String peopleId, List<ProspectCallLog> prospectCallLogList){
		for(ProspectCallLog prospectCallLog : prospectCallLogList){
			String dbPerpleId = prospectCallLog.getProspectCall().getProspect().getSourceId();
			if(peopleId.equals(dbPerpleId)){
				return prospectCallLog;
			}
		}
		return null;
	}

	private Prospect mapProspectObject(LeadIQSearchResponse responseObj,ProspectCallLog prospectCallLog){
		try {
			if (responseObj != null) {
				Prospect prospectInfo = new Prospect();
				if (responseObj.getData().getSearchPeople().getTotalResults() > 0) {
					prospectInfo.setSource("LeadIQ");
					prospectInfo.setDataSource("Xtaas Data Source");
					prospectInfo.setSourceType("INTERNAL");
					prospectInfo.setPhone("");
					prospectInfo.setPhoneType("");
					LeadIQResult result = responseObj.getData().getSearchPeople().getResults().get(0);
					String personIDLeadIQ = result.getId();
					prospectInfo.setSourceId(personIDLeadIQ);
					if (result.getCurrentPositions() != null && result.getCurrentPositions().size() > 0) {
						LeadIQCurrentPosition contactDetails = result.getCurrentPositions().get(0);

						prospectInfo.setIndustry(contactDetails.getCompanyInfo().getIndustry());
						prospectInfo.setFirstName(prospectCallLog.getProspectCall().getProspect().getFirstName());
						prospectInfo.setLastName(prospectCallLog.getProspectCall().getProspect().getLastName());
						for (LeadIQEmail email : contactDetails.getEmails()) {
							if (email.getStatus().equalsIgnoreCase("Verified") && email.getType().equalsIgnoreCase("workemail") && !email.getValue().equalsIgnoreCase("*redacted*")) {
								prospectInfo.setEmail(email.getValue());
								break;
							}
						}
						boolean first = true;
						for (LeadIQPhone phone : contactDetails.getPhones()) {
							// System.out.println("phone number" + phone);
							if (phone.getStatus().equalsIgnoreCase("Verified")) {
								if (phone.getType().equalsIgnoreCase("WorkPhone")) {
									prospectInfo.setPhone(phone.getValue());
									prospectInfo.setPhoneType(phone.getType());
									break;
								}
								if (first) {
									first = false;
									prospectInfo.setPhone(phone.getValue());
									prospectInfo.setPhoneType(phone.getType());
								}
							}
						}
						if (contactDetails.getCompanyInfo().getPhones() != null
								&& contactDetails.getCompanyInfo().getPhones().size() > 0) {
							prospectInfo.setCompanyPhone(contactDetails.getCompanyInfo().getPhones().get(0));
						}

						prospectInfo.setTitle(contactDetails.getTitle());

						if (!prospectInfo.getPhone().isEmpty()) {
							String extension = "";
							String countryCode = getCountryCode(prospectInfo);
							String phoneNumber = getPhoneNumber(prospectInfo);
							if (prospectInfo.getPhone().contains("ext")) {
								extension = getExtension(prospectInfo);
							}
//						if (countryCode != null) {
//							prospectInfo.setPhone("+".concat(countryCode.concat(" " + phoneNumber)));
//						}
							if (!prospectInfo.getPhone().contains("+")) {
								prospectInfo.setPhone("+".concat(" " + phoneNumber));
							}else {
								prospectInfo.setPhone(phoneNumber);
							}
							// System.out.println("country Code" + countryCode + "PhoneNumber" + phoneNumber
							// + "Extension" + extension);
							// if (!countryCode.equals("1")) {
							// prospectInfo.setPhone(countryCode.concat(phoneNumber));
							// } else {
							// prospectInfo.setPhone(phoneNumber);
							// }
							// setCountry
							if (contactDetails.getCompanyInfo().getCountry() != null
									&& !contactDetails.getCompanyInfo().getCountry().isEmpty()) {
								prospectInfo.setCountry(contactDetails.getCompanyInfo().getCountry());
							} else {
								prospectInfo.setCountry(getCountryName(countryCode));
							}
							if(StringUtils.isNotBlank(prospectInfo.getCountry())){
								String leadIqCountry = prospectInfo.getCountry().toLowerCase();
								if(leadIqCountry.equalsIgnoreCase("usa") ||
										leadIqCountry.equalsIgnoreCase("united states")){
									String st = "";
									String timezone = "";
									timezone = properties
											.getProperty(findAreaCode(prospectInfo.getPhone()));
									if (timezone != null && !timezone.isEmpty()) {
										if (timezone.equalsIgnoreCase("US/Pacific")) {
											st = "CA";
										} else if (timezone.equalsIgnoreCase("US/Eastern")) {
											st = "DE";
										} else if (timezone.equalsIgnoreCase("US/Mountain")) {
											st = "CO";
										} else if (timezone.equalsIgnoreCase("US/Central")) {
											st = "FL";
										}
									}
									prospectInfo.setTimeZone(timezone);
									prospectInfo.setStateCode(st);

									if(StringUtils.isBlank(prospectInfo.getTimeZone())){
										prospectInfo.setTimeZone("US/Pacific");
									}
									if(StringUtils.isBlank(prospectInfo.getStateCode())){
										prospectInfo.setStateCode("CA");
									}

								}else {
									// get state code and timezone except usa
									StateCallConfig scc = getStateCodeAndTimeZone(
											contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1(), prospectInfo.getCountry());
									if (scc != null) {
										prospectInfo.setTimeZone(scc.getTimezone());
										prospectInfo.setStateCode(scc.getStateCode());
									}else {
										prospectInfo.setTimeZone("Not Found");
									}
								}
							}else {
								return null;
							}

							if (contactDetails.getCompanyInfo() != null && contactDetails.getCompanyInfo().getLocationInfo() != null) {
								Map<String, String> companyAddress = insertCompanyAddressInMap(contactDetails); // added company address
								prospectInfo.setCompanyAddress(companyAddress);
							}
							if(prospectCallLog != null){
								Prospect p = prospectCallLog.getProspectCall().getProspect();
								prospectInfo.setCustomAttributes(p.getCustomAttributes());
								prospectInfo.setIndustry(p.getIndustry());
								prospectInfo.setDepartment(p.getDepartment());
								prospectInfo.setManagementLevel(p.getManagementLevel());
								prospectInfo.setTitle(p.getTitle());
								if(StringUtils.isBlank(prospectInfo.getStateCode())){
									prospectInfo.setStateCode(p.getStateCode());
								}
								prospectInfo.setCompany(p.getCompany());
								if(StringUtils.isBlank(prospectInfo.getCompany())){
									prospectInfo.setCompany(contactDetails.getCompanyInfo().getName());
								}
							}

							return prospectInfo;
						}
					}

				}
			}
		}catch (Exception e){
			logger.error("Error while actual buy the records from LeadIQ, mapProspectObject() Exception at activation of Data LeadIQ. Exception  [{}] : ",e);
			return null;
		}
		return null;
	}

	public String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replace("-", "");
		int length = removeSpclChar.length();
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = phone.substring(startPoint, endPoint);
		return areaCode;
	}

	private String getCountryCode(Prospect prospectinfo) {
		String phone = prospectinfo.getPhone();
		int firstPosition = -1;
		if (phone != null && phone.contains("-")) {
			firstPosition = phone.indexOf("-");
		}
		if (phone != null && phone.contains(" ")) {
			firstPosition = phone.indexOf(" ");
		}
		if (firstPosition != -1) {
			return phone.substring(0, firstPosition).replace("+", "");
		}
		return null;
	}

	private String getExtension(Prospect prospectinfo) {
		String phone = prospectinfo.getPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			int beginIndex = phone.indexOf("ext");
			ext = phone.substring(beginIndex);
			ext = ext.replaceAll("[^\\d]", "");
		}
		return ext;
	}

	private String getPhoneNumber(Prospect prospectInfo) {
		String phone = prospectInfo.getPhone();
		if (phone != null && phone.contains("ext")) {
			int beginIndex = phone.indexOf("ext");
			String validphone = phone.substring(0, beginIndex);
			validphone = validphone.replaceAll("[^\\d]", "");
			phone = "+" + validphone.trim();
		} else {
			phone = phone.replaceAll("[^\\d]", "");
			phone = "+" + phone.trim();
		}
		return phone;
	}

	private String getCountryName(String countryCode) {
		PlivoOutboundNumber plivoOutboundNumber = plivoOutboundNumberSerivce
				.findOneByIsdCode(Integer.parseInt(countryCode.trim()));
		if (plivoOutboundNumber != null) {
			return plivoOutboundNumber.getCountryName();
		}
		return null;
	}

	public StateCallConfig getStateCodeAndTimeZone(String stateName,String countryName) {
		StateCallConfig stateCallConfigFromDB = null;
		List<StateCallConfig> stateCallConfigFromDBList = null;

		if (stateName != null && !stateName.isEmpty()) {
			stateCallConfigFromDBList = stateCallConfigRespository.findByStateName(stateName);
			if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
				stateCallConfigFromDB = stateCallConfigFromDBList.get(0);
			}else {
				stateCallConfigFromDBList = stateCallConfigRespository.findByCountryName(countryName);
				if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
					stateCallConfigFromDB = stateCallConfigFromDBList.get(0);
				}
			}
		}else if(StringUtils.isNotBlank(countryName)){
			stateCallConfigFromDBList = stateCallConfigRespository.findByCountryName(countryName);
			if(stateCallConfigFromDBList!=null && stateCallConfigFromDBList.size() > 0) {
				stateCallConfigFromDB = stateCallConfigFromDBList.get(0);
			}
		}
		return stateCallConfigFromDB;
	}

	private Map<String, String> insertCompanyAddressInMap(LeadIQCurrentPosition contactDetails) {
		Map<String, String> companyAddress = new HashedMap<>();
		companyAddress.put("areaLevel1", contactDetails.getCompanyInfo().getLocationInfo().getAreaLevel1());
		companyAddress.put("city", contactDetails.getCompanyInfo().getLocationInfo().getCity());
		companyAddress.put("country", contactDetails.getCompanyInfo().getLocationInfo().getCountry());
		companyAddress.put("formattedAddress", contactDetails.getCompanyInfo().getLocationInfo().getFormattedAddress());
		companyAddress.put("postalCode", contactDetails.getCompanyInfo().getLocationInfo().getPostalCode());
		companyAddress.put("street1", contactDetails.getCompanyInfo().getLocationInfo().getStreet1());
		companyAddress.put("street2", contactDetails.getCompanyInfo().getLocationInfo().getStreet2());
		return companyAddress;
	}


	private CampaignCriteria getCampaignCriteria(Campaign campaign) {
		List<com.xtaas.domain.valueobject.Expression> expressions = campaign.getQualificationCriteria().getExpressions();
		CampaignCriteria cc = new CampaignCriteria();
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();

			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
				return cc;
			}
		}
		return cc;
	}

	/*
	 * Check phone DNC in local DNC list if not present then check with third-party
	 */
	private boolean checkDNC(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		boolean isLocalDNC = internalDNCListService.checkLocalDNCList(prospect.getPhone(),
				prospect.getFirstName(), prospect.getLastName());
		String pospectCountry = !StringUtils.isEmpty(prospect.getCountry())
				? prospect.getCountry().toUpperCase()
				: "US";
		if (isLocalDNC) {
			isDNC = isLocalDNC;
		} else if (campaign.isUsPhoneDncCheck() && XtaasConstants.US_COUNTRY.contains(pospectCountry)
				&& phoneDncService.usPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		} else if (campaign.isUkPhoneDncCheck() && XtaasConstants.UK_COUNTRY.contains(pospectCountry)
				&& phoneDncService.ukPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		}
		return isDNC;
	}


	public void getData(Campaign campaign, DataBuyQueue dbq, Long ivRequestBuy) {
		List<String> abmCompanyDomain = new ArrayList<>();
		logger.info("Data buy started for campaing [{}] in LeadIQ ", campaign.getId());
		CampaignCriteria campaignCriteria = new CampaignCriteria();
		if(dbq.isRestFullAPI()){
			campaignCriteria = dbq.getCampaignCriteria();
			if(StringUtils.isNotBlank(campaignCriteria.getManagementLevel())){
				List<String> mgmtSortedList = Arrays.asList(campaignCriteria.getManagementLevel().split(","));
				campaignCriteria.setSortedMgmtSearchList(mgmtSortedList);
			}
			if(StringUtils.isNotBlank(campaignCriteria.getCountry())){
				List<String> countryList = Arrays.asList(campaignCriteria.getCountry().split(","));
				campaignCriteria.setCountryList(countryList);
			}
			if(StringUtils.isNotBlank(campaignCriteria.getMaxRevenue())){
				campaignCriteria.setMaxRevenue(null);
			}

			dbq.setCampaignCriteria(campaignCriteria);
			dbq = dataBuyQueueRepository.save(dbq);
		}else {
			List<Expression> expressions = dbq.getCampaignCriteriaDTO();
			campaignCriteria.setExpressions(expressions);
			campaignCriteria.setAborted(false);
			campaignCriteria.setRequestJobDate(dbq.getJobRequestTime());
			campaignCriteria.setOrganizationId(campaign.getOrganizationId());
			campaignCriteria.setCampaignId(campaign.getId());
			campaignCriteria.setTotalPurchaseReq(dbq.getLeadIqRequestCount());
			if (campaign.getLeadsPerCompany() > 0) {
				campaignCriteria.setLeadPerCompany(campaign.getLeadsPerCompany());
			}
			if (campaign.getDuplicateCompanyDuration() > 0) {
				campaignCriteria.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration() + " Days");
			}
			if (campaign.getDuplicateLeadsDuration() > 0) {
				campaignCriteria.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration() + " Days");
			}

			campaignCriteria = setCampaignCriteria(campaignCriteria, expressions);
		}

		if(campaign.isABM()){
			List<String> campanyName = getABMCompanyName(campaign,abmCompanyDomain);
			if(CollectionUtils.isNotEmpty(campanyName)){
				campaignCriteria.setAbmCompanyIds(campanyName);
			}
			if(CollectionUtils.isNotEmpty(abmCompanyDomain)){
				campaignCriteria.setAbmCompanyDomain(abmCompanyDomain);
			}
		}


		boolean isBuyCompleted = false;
		long totalPurchasedData = 0L;
		if(campaignCriteria == null) {
			logger.info("No data available to Buy from LeadIQ for CamapaignId : " + dbq.getCampaignId() + " , dbq id : "
					+ dbq.getId() + " , LeadIQ purchased data now : " + totalPurchasedData);
			return;
		}
		if(campaignCriteria != null && StringUtils.isNotBlank(campaignCriteria.getMaxRevenue())) {
			logger.info("LeadIQ is not applicable, because revenue is present in campaign criteria, for CamapaignId : " + dbq.getCampaignId() + " , dbq id : "
					+ dbq.getId() + " , LeadIQ purchased data now : " + totalPurchasedData);
			return;
		}
		List<Integer> empRange = findEmpRange(campaignCriteria,campaign);

		if (CollectionUtils.isNotEmpty(campaignCriteria.getSortedMgmtSearchList())) {
			List<String> mlist = findManagementList(campaignCriteria);
			for (String mLevel : mlist) {
				try {
					if(isBuyCompleted)
						break;
					for(int i = 0; i < empRange.size()-1; i++){
						try {
							boolean isFound = true;
							int skip = 0;
							int count = 0;
							if(isBuyCompleted)
								break;
							while (isFound) {
								if(CollectionUtils.isNotEmpty(campaignCriteria.getAbmCompanyIds()) && campaignCriteria.getAbmCompanyIds().size() <= count){
									break;
								}
								if (isBuyCompleted)
									break;
								AdvanceLeadIQSearchResponse leadIQResponse = leadIQAdvancedSearch(campaignCriteria, mLevel,skip,empRange.get(i),empRange.get(i+1),count,campaign);
								if (leadIQResponse != null && leadIQResponse.getData() != null && leadIQResponse.getData().getGroupedAdvancedSearch() != null
										&& leadIQResponse.getData().getGroupedAdvancedSearch().getTotalCompanies() > 0 ) {
									List<Companies> companiesList = leadIQResponse.getData().getGroupedAdvancedSearch().getCompanies();
									if (CollectionUtils.isNotEmpty(companiesList)) {
										for (Companies companies : companiesList) {
											if(isBuyCompleted)
												break;
											if (companies != null && companies.getPeople() != null && companies.getCompany() != null) {
												Company company = companies.getCompany();
												for (People people : companies.getPeople()) {
													if (people != null) {
														try {
															if(totalPurchasedData < ivRequestBuy){
																ProspectCallLog prospectCallLoginfo = prospectCallLogRepository
																		.findByCampaignIdSourceIdAndSource(campaign.getId(), people.getId(), "LeadIQ");
																if (prospectCallLoginfo == null){
																	Prospect prospect = mapProspectObject(people, company, campaignCriteria);
																	if(isValidSearchProspect(prospect,campaignCriteria,campaign)){
																		//LeadIQDataSource dataSource = leadIQDataSourceRepository.findByPeopleId(people.getId());
																		LeadIQDataSource dataSource = null;
																		boolean valid = false;
																		if(dataSource != null){
																			valid = saveSearchCriteriaInPropectCallLog(dataSource.getProspect(), campaign, campaignCriteria,true);
																		} else {
																			valid = saveSearchCriteriaInPropectCallLog(prospect, campaign, campaignCriteria,false);
																		}
																		if(valid)
																			totalPurchasedData++;
																		DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
																				DataBuyQueue.DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
																		if (dbq1 != null) {
																			isBuyCompleted = true;
																			break;
																		}
																	}
																}
															}else {
																isBuyCompleted = true;
																break;
															}
														}catch (Exception e){
															logger.error("Error while purchasing the data from leadIQ for campaign [{}] . getData() : Exception [{}] ",campaign.getId(), e);
														}
													}
												}
											}
										}  // company for loop closed
										if(campaign.isABM()){
											count++;
										}else{
											skip = skip + 20;// skip purchased records (we are getting only 20 records in a single hit)
										}
									}else {
										if(campaign.isABM()){
											count++;
										}else{
											isFound = false;
										}
									}
								}else {
									if(campaign.isABM()){
										count++;
									}else{
										isFound = false;
									}

								}
							}
						}catch (Exception e){
							e.printStackTrace();
						}
					}
				}catch (Exception e){
					logger.error("Error while purchasing the data from leadIQ for campaign [{}] . getData() : Exception [{}] ",campaign.getId(), e);
				}
			}
		}
		logger.info("Total records purchased [{}] by LeadIQ for campaign [{}] ", totalPurchasedData, campaign.getId());
		dbq = dataBuyQueueRepository.findOneById(dbq.getId());
		dbq.setLeadIqPurchasedCount(totalPurchasedData);
		dbq = dataBuyQueueRepository.save(dbq);
		if(dbq != null && dbq.isRestFullAPI()){
			List<ProspectCallLog> prospectList= prospectCallLogRepository
					.findProspectsByCampaignIdAndLeadIQBuysAndSource(campaign.getId(), "No","LeadIQ");
			if(prospectList != null && prospectList.size() > 0){
				leadIQDetailedAPI(getPeopleIdList(prospectList),prospectList,campaign,dbq);
			}
		}

	}

	public List<String> getPeopleIdList(List<ProspectCallLog> prospectReviwedList){
		List<String> contactList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(prospectReviwedList)){
			contactList = prospectReviwedList.stream()
					.map(rev -> rev.getProspectCall().getProspect().getSourceId())
					.collect(toList());
		}
		return contactList;
	}

	public boolean isValidSearchProspect(Prospect prospect,CampaignCriteria campaignCriteria,Campaign campaign){
		boolean isValid = true;
		if(!isValidCountry(campaignCriteria,prospect.getCountry())) {
			return false;
		}
		if(!isValidManagementLevel(prospect,campaignCriteria)){
			return false;
		}
		if(!isValidDepartment(prospect,campaignCriteria)){
			return false;
		}
		if(!isValidIndustry(prospect,campaignCriteria)){
			return false;
		}
		if(isNegativeTitle(campaignCriteria,  prospect)){
			return false;
		}
		if(!isValidEmployee(prospect,campaignCriteria)){
			return false;
		}
		if(!buyGreterEmpData(prospect,campaignCriteria,campaign)){
			return false;
		}
		if(!isValidCompany(campaignCriteria, prospect)){
			return false;
		}
		return isValid;

	}

	public boolean isValidCompany(CampaignCriteria cc,Prospect prospect){
		boolean isValid = false;
		if(CollectionUtils.isEmpty(cc.getAbmCompanyDomain())){
			return true;
		}
		if(prospect != null && prospect.getCustomAttributes() != null && prospect.getCustomAttributes().get("domain") != null
				&& prospect.getCustomAttributes().get("domain").toString() != null){
			String domain = prospect.getCustomAttributes().get("domain").toString().toLowerCase();
			List<String> companyNames = cc.getAbmCompanyDomain();
			for(String name : companyNames){
				String cName = name.toLowerCase();
				boolean isValidDomain = XtaasUtils.isValidDomain(cName);
				String newCName = null;
				if(isValidDomain){
					newCName = XtaasUtils.getHostName(cName);
					domain = XtaasUtils.getHostName(domain);
					if(domain.equalsIgnoreCase(newCName)){
						return true;
					}
				}else{
					newCName = cName;
					if(domain.contains(newCName)){
						return true;
					}
				}
			}
		}else{
			return isValid;
		}
		return isValid;
	}





	private boolean buyGreterEmpData(Prospect prospect,CampaignCriteria campaignCriteria, Campaign campaign){
		boolean isValid = true;
		if(!campaign.isABM() && !campaign.isMaxEmployeeAllow()
				&& prospect.getCustomAttributeValue("leadIQEmployeeCount") != null
				&& !prospect.getCustomAttributeValue("leadIQEmployeeCount").toString().isEmpty()
				&& Double.valueOf(prospect.getCustomAttributeValue("leadIQEmployeeCount").toString()) >= 10000){
			return false;

		}
		return isValid;
	}

	private boolean isValidEmployee(Prospect prospect,CampaignCriteria campaignCriteria){
		boolean found = false;
		if (prospect == null)
			return false;
		String maxEmp = campaignCriteria.getMaxEmployeeCount();
		if(StringUtils.isBlank(maxEmp)){
			return true;
		}

		if(StringUtils.isNotEmpty(maxEmp)  && prospect.getCustomAttributeValue("leadIQEmployeeCount") != null
				&& !prospect.getCustomAttributeValue("leadIQEmployeeCount").toString().isEmpty() &&
				Double.valueOf(prospect.getCustomAttributeValue("leadIQEmployeeCount").toString()) <= Double.valueOf(maxEmp)){
			found = true;
		}
		return found;
	}
	private ProspectCall preparedProspectCall(Prospect prospect, Campaign campaign){
		ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospect);
		return prospectCall;
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d = new Date();
		String pcidv = uniquepcid + Long.toString(d.getTime());
		return pcidv;
	}
	private boolean saveSearchCriteriaInPropectCallLog(Prospect prospect, Campaign campaign, CampaignCriteria campaignCriteria,boolean isBuy){
		try {
			ProspectCall prospectCall  = preparedProspectCall(prospect,campaign);
			prospectCall.setSubStatus("BUSY");

			ProspectCallLog prospectCallLogObj = new ProspectCallLog();
			prospectCallLogObj.setProspectCall(prospectCall);
			prospectCallLogObj.setStatus(ProspectCallLog.ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			if(isBuy){
				prospectCallLogObj.setLeadIQBuy("Yes");
			}else {
				prospectCallLogObj.setLeadIQBuy("No");
			}
			if (isCompanySuppressedDetail(prospectCallLogObj, campaignCriteria)
					|| isEmailSuppressed(prospectCallLogObj, campaignCriteria,campaign)) {
				prospectCall.setCallRetryCount(144);
			} else {
				prospectCall.setCallRetryCount(786);
				insertProspectCallLog(prospectCallLogObj);
				return true;
			}
		}catch (Exception e){
			return false;
		}
		return false;
	}

	private ProspectCallLog insertProspectCallLog(ProspectCallLog prospectCallLogObj) {
		return prospectCallLogRepository.save(prospectCallLogObj);
	}
	public Prospect mapProspectObject(People people, Company company, CampaignCriteria campaignCriteria) {
		try {
			Prospect prospectInfo = new Prospect();
			prospectInfo.setSource("LeadIQ");
			prospectInfo.setSourceType("INTERNAL");
			prospectInfo.setDataSource("Xtaas Data Source");
			prospectInfo.setSourceId(people.getId());
			if(StringUtils.isNotBlank(people.getName())){
				String name[] = StringUtils.split(people.getName());
				if(name.length  == 1){
					prospectInfo.setFirstName(name[0]);
				}else{
					prospectInfo.setFirstName(name[0]);
					prospectInfo.setLastName(name[1]);
				}
			}
			prospectInfo.setSourceCompanyId(company.getId());
			prospectInfo.setCompany(company.getName());
			prospectInfo.setCountry(company.getCountry());
			prospectInfo.setStateCode(company.getState());
			if(StringUtils.isNotBlank(people.getTitle())){
				prospectInfo.setTitle(people.getTitle());
			}
			if(StringUtils.isNotBlank(company.getIndustry())){
				prospectInfo.setIndustry(company.getIndustry());
			}
			if(StringUtils.isNotBlank(people.getRole())){
				prospectInfo.setDepartment(people.getRole());
			}
			if(StringUtils.isNotBlank(people.getSeniority())) {
				Map<String,String> leadZoomMLevel = getLeadZoomMLevel();
				prospectInfo.setManagementLevel(leadZoomMLevel.get(people.getSeniority()));
			}
			Map<String, Object> customAttributes = new HashMap<>();
			customAttributes.put("peopleLinkedinId", people.getLinkedinId());
			customAttributes.put("companyLinkedinId", company.getLinkedinId());
			customAttributes.put("linkedinUrl", people.getLinkedinUrl());
			customAttributes.put("domain",company.getDomain());
			customAttributes.put("leadIQEmployeeCount",company.getEmployeeCount());
			customAttributes.put("maxRevenue","NA");
			Map<String, String> empRangeMap = getMinAndMaxEmployeeCount(String.valueOf(company.getEmployeeCount()));
			if (empRangeMap != null) {
				customAttributes.put("maxEmployeeCount", empRangeMap.get("maxEmp"));
				customAttributes.put("minEmployeeCount", empRangeMap.get("minEmp"));
			}
			if(customAttributes != null && customAttributes.size() > 0){
				prospectInfo.setCustomAttributes(customAttributes);
			}
			return prospectInfo;
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, String> getMinAndMaxEmployeeCount(String emplyeeCount) {
		if(emplyeeCount!=null && !emplyeeCount.isEmpty()) {
			int empCountInt = Integer.parseInt(emplyeeCount);
			Map<String, String> empRangeMain = new HashMap<>();
			List<Map<String, Integer>> empRangeList = XtaasUtils.getEmployeeRangesList();
			for (Map<String, Integer> employeeMap : empRangeList) {
				int minEmp = employeeMap.get("minEmp");
				if (empCountInt >= minEmp) {
					if (employeeMap.containsKey("maxEmp")) {
						int maxEmp = employeeMap.get("maxEmp");
						if (empCountInt < maxEmp) {
							empRangeMain.put("minEmp", String.valueOf(minEmp));
							empRangeMain.put("maxEmp", String.valueOf(maxEmp));
							return empRangeMain;
						}
					} else {
						empRangeMain.put("minEmp", String.valueOf(minEmp));
						empRangeMain.put("maxEmp", "");
						return empRangeMain;
					}
				}
			}
		}
		return null;
	}

	private String getRandomItem(String str){
		List<String> list = Arrays.asList(str.split(","));
		Random rand = new Random();
		return list.get(rand.nextInt(list.size()));
	}

	private String getCriteria(CampaignCriteria campaignCriteria,String type,List<String> valueList){
		for (Expression exp : campaignCriteria.getExpressions()) {
			String attribute = exp.getAttribute();
			if (attribute.equalsIgnoreCase(type)) {
				List<String> iLIst = new ArrayList<>();
				for (PickListItem item : exp.getPickList()) {
					if(valueList.contains(item.getValue())) {
						iLIst.add(item.getLabel());
					}
				}
				if(iLIst != null && iLIst.size() > 0) {
					Random rand = new Random();
					return iLIst.get(rand.nextInt(iLIst.size()));
				}else{
					return null;
				}
			}
		}
		return null;
	}

	private String getRealValue(CampaignCriteria campaignCriteria,String type,List<String> valueList){
		for (Expression exp : campaignCriteria.getExpressions()) {
			String attribute = exp.getAttribute();
			if (attribute.equalsIgnoreCase(type)) {
				List<String> iLIst = new ArrayList<>();
				for (PickListItem item : exp.getPickList()) {
					if(valueList.contains(item.getValue())) {
						iLIst.add(item.getLabel());
					}
				}
				if(iLIst != null && iLIst.size() > 0) {
					return String.join(",",iLIst);
				}else{
					return null;
				}
			}
		}
		return null;
	}



	private AdvanceLeadIQSearchResponse leadIQAdvancedSearch(CampaignCriteria campaignCriteria,String mLevel,int skip,
															 Integer empMinValue,Integer empMaxValue, int count, Campaign campaign) {
		StringBuilder payload = new StringBuilder();
		payload.append(
				"{\"query\": \"query AdvancedSearch($input: GroupedSearchInput!) {  groupedAdvancedSearch(input: $input) { totalCompanies companies { company { id name industry linkedinId domain employeeCount country state } people { id name title linkedinId linkedinUrl workEmails workPhones personalEmails personalPhones role seniority } } }}\",\"variables\": {\"input\":{\"companyFilter\":{");
		if(StringUtils.isNotBlank(campaignCriteria.getCountry())){
			String str = convertToString(campaignCriteria.getCountry());
			if(str.contains("USA")) {
				str = str.replace("USA","United States");
			}
			payload.append("\"countries\":[");
			payload.append(str);
			payload.append("]");
		}
		if(empMinValue != null && empMaxValue != null){
			payload.append(",");
			payload.append("\"sizes\":[{");
			payload.append("\"min\":");
			payload.append(empMinValue);
			payload.append(",");
			payload.append("\"max\":");
			payload.append(empMaxValue);
			payload.append("}]");
		}
		if(StringUtils.isNotBlank(campaignCriteria.getIndustry())){
			String str = campaignCriteria.getIndustry();
			if(StringUtils.isNotBlank(str)) {
				payload.append(",");
				payload.append("\"industries\":[");
				payload.append(convertToString(str));
				payload.append("]");
			}
		}
		if(CollectionUtils.isNotEmpty(campaignCriteria.getAbmCompanyIds())){
			payload.append(",");
			payload.append("\"names\":[");
			payload.append("\""+campaignCriteria.getAbmCompanyIds().get(count)+"\"");
			payload.append("]");
		}
		payload.append("}, \"contactFilter\":{");
		if(StringUtils.isNotBlank(mLevel)){
			payload.append("\"seniorities\":[");
			payload.append("\""+mLevel+"\"");
			payload.append("]");
		}
		if(StringUtils.isNotBlank(campaignCriteria.getDepartment())){
			String str = campaignCriteria.getDepartment();
			if(StringUtils.isNotBlank(str))
			{
				payload.append(",");
				payload.append("\"roles\":[");
				payload.append(convertToString(str));
				payload.append("]");
			}
		}
		if(StringUtils.isNotBlank(campaignCriteria.getTitleKeyWord())){
			payload.append(",");
			payload.append("\"titles\":[");
			payload.append(convertToString(campaignCriteria.getTitleKeyWord()));
			payload.append("]");
		}
		payload.append("},\"limitPerCompany\":40,\"limit\":20,\"skip\":"+skip+",\"sortContactsBy\":[\"NameAsc\"]}},\"operationName\":\"AdvancedSearch\"}");
		AdvanceLeadIQSearchResponse responseObj = getAdvanceLeadIQSearchResponse(payload.toString());
		if(responseObj  == null){
			logger.error("No records available for campaign [{}] in leadiq, Campaign criteria :  [{}] ",campaign.getId(), payload.toString());
		}
		return responseObj;
	}

	private AdvanceLeadIQSearchResponse getAdvanceLeadIQSearchResponse(String payload) {
		logger.info("Purchase URL of LeadIQ : [{}] ", payload);
		String apiKey = ApplicationEnvironmentPropertyUtils.getLeadIQAPIKey();
		String leadIQAPIURL = ApplicationEnvironmentPropertyUtils.getLeadIQUrl();
		String apiKeyEncoded = Base64.getEncoder().encodeToString(apiKey.getBytes());
		String responseJsonString = null;
		try{
			URL url =new URL(leadIQAPIURL);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty ("Authorization", "Basic " + apiKeyEncoded);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_TYPE.toString());
			connection.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
				wr.writeBytes(payload);
				wr.flush();
			}
			int responseCode = connection.getResponseCode();
			logger.info("Response code LeadIQ : [{}] ", responseCode);

			BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response1= new StringBuffer();
			while((inputline=bufferedReader.readLine())!=null)
			{
				response1.append(inputline);
			}
			bufferedReader.close();
			responseJsonString = response1.toString();
		}catch (UnknownHostException uhe){
			logger.error("Unknow Host Exception from leadIQ, again calling same method . Exception  [{}] ", uhe);
			getAdvanceLeadIQSearchResponse(payload);
		}
		catch (Exception e){
			logger.error("Error while purchasing the data from leadIQ . getAdvanceLeadIQSearchResponse(): Exception [{}] ", e);
			return null;
		}

		AdvanceLeadIQSearchResponse reponseObj = null;
		if (StringUtils.isNotBlank(responseJsonString)) {
			try {
				reponseObj = new com.fasterxml.jackson.databind.ObjectMapper().readValue(responseJsonString, AdvanceLeadIQSearchResponse.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reponseObj;
	}

	private String convertToString(String Value){
		if(StringUtils.isNotBlank(Value)){
			List<String> list = Arrays.asList(Value.split(","));
			return "\"" + StringUtils.join(list, "\",\"") + "\"";
		}
		return  "";
	}

	private boolean isValidDepartment(Prospect prospect,CampaignCriteria campaignCriteria){
		if (prospect == null)
			return false;
		if(StringUtils.isBlank(campaignCriteria.getDepartment())){
			return true;
		}
		List<String> dLIst = Arrays.asList(campaignCriteria.getDepartment().split(","));
		boolean found = false;
		if(CollectionUtils.isNotEmpty(dLIst) && StringUtils.isNotBlank(prospect.getDepartment())){
			boolean containsSearchStr = dLIst.stream().anyMatch(prospect.getDepartment() ::equalsIgnoreCase);
			if(containsSearchStr){
				found = true;
			}
		}
		return found;
	}

	private boolean isValidIndustry(Prospect prospect,CampaignCriteria campaignCriteria){
		boolean found = false;
		if (prospect == null || StringUtils.isBlank(prospect.getIndustry()))
			return false;
		if(StringUtils.isBlank(campaignCriteria.getIndustry())){
			List<String> removeIndustryList = removeDefaultIndustry();
			for(String s: removeIndustryList){
				if(s.contains(prospect.getIndustry())) {
					return false;
				}
			}
			return true;
		}else {
			List<String> inLIst = Arrays.asList(campaignCriteria.getIndustry().split(","));
			if(CollectionUtils.isNotEmpty(inLIst) && StringUtils.isNotBlank(prospect.getIndustry())){
				boolean containsSearchStr = inLIst.stream().anyMatch(prospect.getIndustry() ::equalsIgnoreCase);
				if(containsSearchStr){
					found = true;
				}
			}
		}
		return found;
	}


	public List<String> removeDefaultIndustry(){
		List<String> newRemoveInd = new ArrayList<>();
		newRemoveInd.add("Education");
		newRemoveInd.add("Education General");
		newRemoveInd.add("K-12 Schools");
		newRemoveInd.add("Training");
		newRemoveInd.add("Colleges & Universities");
		newRemoveInd.add("Government");
		newRemoveInd.add("Religious Organizations");
		newRemoveInd.add("Organizations");
		newRemoveInd.add("Membership Organizations");
		newRemoveInd.add("Charitable Organizations & Foundations");
		newRemoveInd.add("Organizations General");
		newRemoveInd.add("Cities, Towns & Municipalities");
		newRemoveInd.add("Cities, Towns & Municipalities General");
		newRemoveInd.add("Public Safety");
		return newRemoveInd;
	}

	private boolean isValidManagementLevel(Prospect prospect,CampaignCriteria campaignCriteria){
		if (prospect == null)
			return false;
		List<String> mLevel = campaignCriteria.getSortedMgmtSearchList();
		boolean found = false;
		if(campaignCriteria.isRestFullAPI()){
			Map<String,String> leadZoomMLevel = getZoomMLeadLevel();
			boolean containsSearchStr = mLevel.stream().anyMatch(leadZoomMLevel.get(prospect.getManagementLevel()) ::equalsIgnoreCase);
			if(containsSearchStr){
				found = true;
			}
		}else{
			boolean containsSearchStr = mLevel.stream().anyMatch(prospect.getManagementLevel() ::equalsIgnoreCase);
			if(containsSearchStr){
				found = true;
			}
		}
		return found;
	}


	private CampaignCriteria setCampaignCriteria(CampaignCriteria cc, List<Expression> expressions) {

		Map<String, String[]> revMap = XtaasUtils.getRevenueMap();
		Map<String, String[]> empMap = XtaasUtils.getEmployeeMap();
		List<Integer> minRevList = new ArrayList<Integer>();
		List<Integer> maxRevList = new ArrayList<Integer>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		boolean isEmployeeInCriteria = false;

		StringBuffer sb = new StringBuffer();
		if(CollectionUtils.isNotEmpty(expressions)){
			for (Expression exp : expressions) {
				String attribute = exp.getAttribute();
				String value = exp.getValue();
				String operator = exp.getOperator();
				if (value.equalsIgnoreCase("All")) {
					if (attribute.equalsIgnoreCase("INDUSTRY")) {
						String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570,1802,263946,132874,198410,67338";
						cc.setRemoveIndustries(removeIndusCodes);
					}
				} else {
					if (attribute.equalsIgnoreCase("INDUSTRY")) {
						DataBuyMapping dataBuyMapping = dataBuyMappingRepository.findBySourceAndAttribute("LEADIQ", "INDUSTRY");
						List<String> inLIst = Arrays.asList(value.split(","));
						List<String> newList = new ArrayList<>();
						if(dataBuyMapping != null){
							List<KeyValuePair<String,String>> keyValuePairs = dataBuyMapping.getMapping();
							for(KeyValuePair<String,String> kv : keyValuePairs){
								if(inLIst.contains(kv.getKey())){
									newList.add(kv.getValue());
								}
							}
						}
						if(CollectionUtils.isNotEmpty(newList)){
							cc.setIndustry(String.join(",",newList));
						}else {
							logger.error("No industries found in insideview for leadIQ industries [{}] ", cc.getIndustry());
							return null;
						}
					}
					if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
						cc.setManagementLevel(value);
						List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
						cc.setSortedMgmtSearchList(mgmtSortedList);
					}
					if (attribute.equalsIgnoreCase("DEPARTMENT")) {
						String[] depArr = value.split(",");
						Set<String> dList = new HashSet<>();
						Map<String,String> zoomLeadDep = getLeadZoomDept();
						for(int i = 0; i < depArr.length; i++){
							if(zoomLeadDep.containsKey(depArr[i])){
								dList.add(zoomLeadDep.get(depArr[i]));
							}
						}
						if(dList != null && dList.size() > 0){
							cc.setDepartment(String.join(",",dList));
						}
					}
					if (attribute.equalsIgnoreCase("REVENUE")) {
						String[] revenueArr = value.split(",");
						cc.setSortedRevenuetSearchList(XtaasUtils.getRevenuSortedeMap(revenueArr));
						for (int i = 0; i < revenueArr.length; i++) {
							String revVal = revenueArr[i];
							String[] revArr = revMap.get(revVal);

							minRevList.add(Integer.parseInt(revArr[0]));
							maxRevList.add(Integer.parseInt(revArr[1]));
						}

						Collections.sort(minRevList);
						Collections.sort(maxRevList, Collections.reverseOrder());
						if (minRevList.contains(new Integer(0))) {

						} else if (minRevList.get(0) > 0) {
							cc.setMinRevenue(minRevList.get(0).toString());
						}
						if (maxRevList.contains(new Integer(0))) {

						} else if (maxRevList.get(0) > 0) {
							cc.setMaxRevenue(maxRevList.get(0).toString());
						}
					}

					if (attribute.equalsIgnoreCase("EMPLOYEE")) {
						isEmployeeInCriteria=true;
						String[] employeeArr = value.split(",");
						cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
						for (int i = 0; i < employeeArr.length; i++) {
							String empVal = employeeArr[i];
							String[] empArr = empMap.get(empVal);

							minEmpList.add(Integer.parseInt(empArr[0]));
							maxEmpList.add(Integer.parseInt(empArr[1]));
						}

						Collections.sort(minEmpList);
						Collections.sort(maxEmpList, Collections.reverseOrder());
						if (minEmpList.get(0) >= 0) {
							cc.setMinEmployeeCount(minEmpList.get(0).toString());
						}
						if (maxEmpList.get(0) >= 0) {
							cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
						}
					}

					if (attribute.equalsIgnoreCase("SIC_CODE")) {
						cc.setSicCode(value);
					}
					if (attribute.equalsIgnoreCase("NAICS_CODE")) {
						cc.setNaicsCode(value);
					}
					if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
						List<String> countryList = Arrays.asList(value.split(","));
						cc.setCountryList(countryList);
						cc.setCountry(value);
					}
					if (attribute.equalsIgnoreCase("VALID_STATE")) {
						cc.setState(value);
					}
					if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
						cc.setTitleKeyWord(value);
					}
					if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
						cc.setNegativeKeyWord(value);
					}
					if (attribute.equalsIgnoreCase("STATE (US ONLY)")) {
						cc.setState(value);
					}

					if (attribute.equalsIgnoreCase("ZIP_CODE")) {
						cc.setZipcodes(value);
					}

				}

			}
		}


		if(!isEmployeeInCriteria) {
			String defaultEmp = "1-5,5-10,10-20,20-50,50-100,100-250,250-500,500-1000,1000-5000,5000-10000";
			String[] employeeArr = defaultEmp.split(",");
			cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
			for (int i = 0; i < employeeArr.length; i++) {
				String empVal = employeeArr[i];
				String[] empArr = empMap.get(empVal);

				minEmpList.add(Integer.parseInt(empArr[0]));
				maxEmpList.add(Integer.parseInt(empArr[1]));
			}

			Collections.sort(minEmpList);
			Collections.sort(maxEmpList, Collections.reverseOrder());
			if (minEmpList.contains(new Integer(0))) {

			} else if (minEmpList.get(0) > 0) {
				cc.setMinEmployeeCount(minEmpList.get(0).toString());
			}
			if (maxEmpList.contains(new Integer(0))) {

			} else if (maxEmpList.get(0) > 0) {
				cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
			}
		}

		return cc;
	}


	private boolean isCompanySuppressedDetail(ProspectCallLog domain, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		if (domain.getProspectCall() != null && domain.getProspectCall().getProspect() != null
				&& domain.getProspectCall().getProspect().getCustomAttributes() != null
				&& domain.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
			String pdomain = domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
			if (pdomain != null && !pdomain.isEmpty()) {
				pdomain = getSuppressedHostName(pdomain);
			}
			if (domain.getProspectCall().getProspect().getEmail() != null
					&& !domain.getProspectCall().getProspect().getEmail().isEmpty()) {
				String pEmail = domain.getProspectCall().getProspect().getEmail();
				String domEmail = pEmail.substring(pEmail.indexOf("@") + 1, pEmail.length());

				long compSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), domEmail);
				if (compSupCount > 0) {
					suppressed = true;
				}
			}

			if (pdomain != null && !pdomain.isEmpty()) {
				long domainSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), pdomain);
				if (domainSupCount > 0) {
					suppressed = true;
				}
			}
		}
		return suppressed;
	}

	private boolean isEmailSuppressed(ProspectCallLog personRecord,
									  CampaignCriteria campaignCriteria, Campaign campaign) {
		boolean suppressed = false;
		if (personRecord.getProspectCall().getProspect().getEmail() != null
				&& !personRecord.getProspectCall().getProspect().getEmail().isEmpty()) {
			int emailsup = suppressionListNewRepository.countByCampaignEmailOrganization(
					campaignCriteria.getCampaignId(), personRecord.getProspectCall().getProspect().getEmail(),
					campaign.getOrganizationId());
			if (emailsup == 0 && campaign.isMdFiveSuppressionCheck()) {
				emailsup = suppressionListNewRepository.countByCampaignEmailOrganization(
						campaignCriteria.getCampaignId(),
						XtaasUtils.convertStringtoMD5Hex(personRecord.getProspectCall().getProspect().getEmail()),
						campaign.getOrganizationId());
			}
			if (emailsup > 0) {
				suppressed = true;
			}
		}
		if (!suppressed && campaignCriteria.getRemoveIndustries() != null
				&& !campaignCriteria.getRemoveIndustries().isEmpty()
				&& personRecord.getProspectCall().getProspect().getEmail() != null
				&& !personRecord.getProspectCall().getProspect().getEmail().isEmpty()) {

			String[] indValsArr = campaignCriteria.getRemoveIndustries().split(",");
			List<String> inIndValsList = Arrays.asList(indValsArr);
			// Educatin
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".edu")) {
				if (inIndValsList.contains("1802") || inIndValsList.contains("263946") || inIndValsList.contains("132874")
						|| inIndValsList.contains("198410") || inIndValsList.contains("67338")) {
					suppressed = true;
				}
			}
			// Government
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".gov")) {
				if (inIndValsList.contains("2570")) {
					suppressed = true;
				}
			}
			// religious organization
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".org")) {
				if (inIndValsList.contains("201482") || inIndValsList.contains("4874") || inIndValsList.contains("135946")
						|| inIndValsList.contains("70410") || inIndValsList.contains("267018")) {
					suppressed = true;
				}
			}
			// municipal
			// if(inIndValsList.contains("4618") || inIndValsList.contains("135690") ||
			// inIndValsList.contains("70154")) {
			// do nothing
			// }
		}
		return suppressed;
	}

	private String getSuppressedHostName(String domain) {
		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}

	private Map<String,String> getLeadZoomMLevel(){
		Map<String,String> leadZoomMLevel = new HashMap<>();
		leadZoomMLevel.put("Other","Board Members");
		leadZoomMLevel.put("Manager","MANAGER");
		leadZoomMLevel.put("Executive","C_EXECUTIVES");
		leadZoomMLevel.put("VP","VP_EXECUTIVES");
		leadZoomMLevel.put("Director","DIRECTOR");
		return leadZoomMLevel;
	}

	private Map<String,String> getLeadZoomDept(){
		Map<String,String> leadZoomDept = new HashMap<>();
		leadZoomDept.put("4980839","Consulting");
		leadZoomDept.put("4784231","Engineering");
		leadZoomDept.put("2228226","Engineering");
		leadZoomDept.put("3211266","Engineering");
		leadZoomDept.put("3801090","Engineering");
		leadZoomDept.put("4653058","Information Technology");
		leadZoomDept.put("4849666","Engineering");
		leadZoomDept.put("1376258","Engineering");
		leadZoomDept.put("5570562","Engineering");
		leadZoomDept.put("5505026","Information Technology");
		leadZoomDept.put("6029314","Quality Assurance");
		leadZoomDept.put("4718594","Engineering");
		leadZoomDept.put("1114114","Engineering");
		leadZoomDept.put("5308418","Information Technology");
		leadZoomDept.put("4587623","Finance");
		leadZoomDept.put("262146","Accounting");
		leadZoomDept.put("1769474","Finance");
		leadZoomDept.put("327682","Finance");
		leadZoomDept.put("2621442","Finance");
		leadZoomDept.put("1441794","Finance");
		leadZoomDept.put("4718695","Human Resources");
		leadZoomDept.put("2162690","Human Resources");
		leadZoomDept.put("4915202","Human Resources");
		leadZoomDept.put("3473410","Human Resources");
		leadZoomDept.put("5177346","Human Resources");
		leadZoomDept.put("5242882","Human Resources");
		leadZoomDept.put("5832807","Legal");
		leadZoomDept.put("1572866","Legal");
		leadZoomDept.put("1507330","Legal");
		leadZoomDept.put("4456551","Marketing");
		leadZoomDept.put("786434","Marketing");
		leadZoomDept.put("2555906","Marketing");
		leadZoomDept.put("3932162","Marketing");
		leadZoomDept.put("393216","Marketing");
		leadZoomDept.put("3276802","Marketing");
		leadZoomDept.put("4325378","Product Management");
		leadZoomDept.put("5701735","Healthcare Services");
		leadZoomDept.put("4784130","Healthcare Services");
		leadZoomDept.put("3670018","Healthcare Services");
		leadZoomDept.put("589826","Healthcare Services");
		leadZoomDept.put("3145730","Healthcare Services");
		leadZoomDept.put("4653159","Operations");
		leadZoomDept.put("3014658","Operations");
		leadZoomDept.put("4063234","Operations");
		leadZoomDept.put("4390914","Operations");
		leadZoomDept.put("2752514","Operations");
		leadZoomDept.put("5373954","Operations");
		leadZoomDept.put("5701634","Operations");
		leadZoomDept.put("2686978","Operations");
		leadZoomDept.put("6094850","Quality Assurance");
		leadZoomDept.put("4522087","Sales");
		leadZoomDept.put("393218","Business Development");
		leadZoomDept.put("196610","Sales");
		leadZoomDept.put("4849767","Research");
		leadZoomDept.put("2293762","Engineering");
		leadZoomDept.put("4259942","Business Development");
		return leadZoomDept;
	}

	public List<String> getABMCompanyName(Campaign campaign,List<String> abmCompanyDomain){
		int count = Integer.parseInt(abmListServiceImpl.getABMListCountOfCampaign(campaign.getId()));
		List<String> companyIds = new ArrayList<String>();

		if(count >0) {
			int  batchsize = 0;
			if(count>0) {
				batchsize = count /1000;
				long modSize = count % 1000;
				if(modSize > 0){
					batchsize = batchsize +1;
				}
			}
			for(int i=0;i<batchsize;i++) {
				Pageable pageable =  PageRequest.of(i, 1000);
				List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
				if(abmList!=null && abmList.size()>0){
					// if(abmList.getCompanyList()!=null && abmList.getCompanyList().size()>0){
					for(AbmListNew abm : abmList){
						try {
							if(abm.getCompanyId() !=null &&  !abm.getCompanyId().isEmpty()
									&& !abm.getCompanyId().equalsIgnoreCase("NOT_FOUND")
									&& !abm.getCompanyId().equalsIgnoreCase("GET_COMPANY_ID")) {

								List<AbmWeightScore> list = abm.getResponseList();
								if(CollectionUtils.isNotEmpty(list)){
									String cName = list.get(0).getCompanyName();
									if(StringUtils.isNotBlank(cName)){
										boolean isValidDomain = XtaasUtils.isValidDomain(cName);
										if(isValidDomain){
											companyIds.add(XtaasUtils.getHostName(cName));
										}else{
											companyIds.add(cName);
										}
									}else{
										boolean isValidDomain = XtaasUtils.isValidDomain(abm.getCompanyName());
										if(isValidDomain){
											companyIds.add(XtaasUtils.getHostName(abm.getCompanyName()));
										}else{
											companyIds.add(abm.getCompanyName());
										}

									}
								}else {
									boolean isValidDomain = XtaasUtils.isValidDomain(abm.getCompanyName());
									if(isValidDomain){
										companyIds.add(XtaasUtils.getHostName(abm.getCompanyName()));
									}else{
										companyIds.add(abm.getCompanyName());
									}
								}
								abmCompanyDomain.add(abm.getCompanyName());
							}
						}

						catch (Exception e){

						}

					}
				}
			}
		}
		return companyIds;
	}

	public boolean isNegativeTitle(CampaignCriteria cc, Prospect personRecord){
		boolean isValid = false;
		List<String> negativeTitles = getNegativeTitles(cc);
		if(StringUtils.isNotBlank(personRecord.getTitle())){
			String lowerTitle = personRecord.getTitle().toLowerCase();
			for (String neg : negativeTitles) {
				if (lowerTitle.contains(neg)) {
					return true;
				}
			}
		}
		return isValid;
	}



	private List<String> getNegativeTitles(CampaignCriteria campaignCriteria) {
		List<String> negativeTitles = new ArrayList<String>();
		List<String> cNegativeTitles = new ArrayList<String>();
		List<String> retNegativeTitles = new ArrayList<String>();
		if (campaignCriteria.getNegativeKeyWord() != null) {
			negativeTitles = Arrays.asList(campaignCriteria.getNegativeKeyWord().split(","));
			for (String negtit : negativeTitles) {
				cNegativeTitles.add(negtit.toLowerCase());
			}
		}
		cNegativeTitles.add("coordinator");
		cNegativeTitles.add("interim");
		cNegativeTitles.add("program");
		cNegativeTitles.add("special");
		cNegativeTitles.add("assistant");
		cNegativeTitles.add("associate");
		cNegativeTitles.add("part time");
		cNegativeTitles.add("supervisor");
		cNegativeTitles.add("program");
		cNegativeTitles.add("lead");
		cNegativeTitles.add("head");
		cNegativeTitles.add("intern");
		cNegativeTitles.add("consulting");
		cNegativeTitles.add("consultant");
		cNegativeTitles.add("temp");
		cNegativeTitles.add("sale");


		for (String negitive : cNegativeTitles) {
			retNegativeTitles.add(negitive.toLowerCase());
		}

		return retNegativeTitles;

	}

	private Map<String,String> getZoomMLeadLevel(){
		Map<String,String> leadZoomMLevel = new HashMap<>();
		leadZoomMLevel.put("Board Members","Other");
		leadZoomMLevel.put("MANAGER","Manager");
		leadZoomMLevel.put("C_EXECUTIVES","Executive");
		leadZoomMLevel.put("VP_EXECUTIVES","VP");
		leadZoomMLevel.put("DIRECTOR","Director");
		return leadZoomMLevel;
	}

	public static List<Integer> getNumberRange(int start, int gap, int end) {
		return IntStream.iterate(start, i -> i+gap)
				.limit(end/gap)
				.boxed()
				.collect(toList());
	}


	public List<Integer> findEmpRange(CampaignCriteria campaignCriteria, Campaign campaign){
		List<Integer> empRange = null;
		if(StringUtils.isNotBlank(campaignCriteria.getMaxEmployeeCount()) &&
				StringUtils.isNotBlank(campaignCriteria.getMinEmployeeCount())){
			if(campaign.isABM()){
				empRange = new ArrayList<>();
				empRange.add(Integer.valueOf(campaignCriteria.getMinEmployeeCount()));
				empRange.add(Integer.valueOf(campaignCriteria.getMaxEmployeeCount()));
			}else{
				empRange = getNumberRange(Integer.valueOf(campaignCriteria.getMinEmployeeCount()),10,
						Integer.valueOf(campaignCriteria.getMaxEmployeeCount()));
			}
		}else{
			if(campaign.isABM()){
				empRange = new ArrayList<>();
				empRange.add(1);
				empRange.add(9000);
			}else{
				empRange = getNumberRange(1,20, 10000);
			}
		}
		return empRange;
	}

	public List<String> findManagementList(CampaignCriteria campaignCriteria){
		List<String> mlist = null;
		if(campaignCriteria.isRestFullAPI()){
			mlist = campaignCriteria.getSortedMgmtSearchList();
		}else{
			mlist = XtaasUtils.getSortedMgmtSearchForLeadIQ(campaignCriteria.getSortedMgmtSearchList());
		}
		return mlist;
	}

}