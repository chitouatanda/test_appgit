/**
 * 
 */
package com.xtaas.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.QaDetails;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.ProspectCallCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.QueuedProspectDTO;

/**
 * @author djain
 *
 */
public interface ProspectCallLogService {

	public ProspectCallLog save(ProspectCallLog prospectLog, boolean createProspectCallInteraction);
	
	public void processCallStatus(String callSid, String callStatus);

	public void saveAll(List<ProspectCallLog> prospectLogList);
	
	public QaDetails  getDetails(String prospectLogId);

	public QueuedProspectDTO getQueuedProspects(String campaignId, int queuedCallCount);

	void updateCallbackStatusSystemGenerated(String prospectCallId, Date callbackDate);

	void updateCallStatus(String prospectCallId, ProspectCallStatus status, String callSid, Integer callRetryCount, Long dailyCallRetryCount);
	
	void updateCallStatus(String callSid, ProspectCallStatus status);

	public ProspectCallLog getLatestByPhoneNumber(String phoneNumber);

	void updateCallDetails(String prospectCallId, ProspectCallStatus status, String callSid, Integer callRetryCount, String allocatedAgentId, Long dailyCallRetryCount);
	
	void updateProspectInteractionSessionId(String prospectCallId, String prospectInteractionSessionId);
	
	void updateInteractionSessionIdAndOutboundNumber(String prospectCallId, String prospectInteractionSessionId,
	String outboundNumber);

	public int getSuccessCount(String campaignId);
	
	public int getProspectCountFromCache(String campaignId);

	HashMap<String, Integer> getCallableRecordsCount(String campaignId);

	public ProspectCallLog findByProspectCallId(String prospectCallId);
	
	public void updateRecordingUrlByCallSid(String callSid);
	
	public ProspectCallDTO createProspect(ProspectCallDTO prospectCallDTO);

	public boolean checkLocalDNCList(String firstName, String lastName, String phone,String domain,boolean isUSorCanada, String partnerId, ProspectCallDTO prospectCall, ProspectCallCacheDTO prospectCallCacheDTO);

	public boolean updateRecordingAndFailureProspects(String campaignId, String twilioCallSid);

	public int getbackCallableProspects(CallableGetbackCriteriaDTO criteriaDTO);

	public boolean getsuppresionListByDomain(String campaignId, String  domain);

	void saveCallDetectedByInProspect(String callSid, String detectedAs, long timeDiffrence);
	
	public void saveCallDetectedByInProspectForMiliSecond(String callSid, String detectedAs, long timeDiffrenceInMilliSeconds);

	public void generateCallbackCache(Campaign campaignFromDB, boolean isCallbackSubmitted);

	public void generateMissedCallbackCache(Campaign campaignFromDB, boolean isCallbackSubmitted);

}