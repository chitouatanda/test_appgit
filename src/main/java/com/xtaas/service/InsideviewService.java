package com.xtaas.service;

import com.xtaas.db.entity.Prospect;

public interface InsideviewService {
  
  public void getJobResults(String jobId);
  
  public Prospect getManualProspect(Prospect prospect);
  
}