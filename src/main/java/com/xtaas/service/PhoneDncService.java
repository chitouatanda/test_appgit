package com.xtaas.service;

import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.entity.Campaign;

public interface PhoneDncService {

	public boolean ukPhoneDncCheck(Prospect prospect, Campaign campaign);

	public boolean usPhoneDncCheck(Prospect prospect, Campaign campaign);

}
