package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;

import com.xtaas.db.entity.Domain;
import com.xtaas.db.entity.Vertical;
import com.xtaas.valueobjects.KeyValuePair;

public interface VerticalService {
	public List<Vertical> getVericals();
	public List<KeyValuePair<String, ArrayList<Domain>>> getDomains(List<String> vertical);
}
