package com.xtaas.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.QaService;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.AbmListDetail;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.QaRejectReason;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.db.repository.AbmListDetailRepository;
//import com.xtaas.db.repository.AbmListDetailRepositoryImpl;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.db.repository.QaRejectReasonRepository;
import com.xtaas.db.repository.RealTimeDeliveryRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.domain.valueobject.QaFeedback;
import com.xtaas.utils.XtaasConstants.LEAD_STATUS;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.ClientMappingComparator;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.valueobjects.NewClientMappingComparator;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.worker.DemandshoreDownloadReportThread;

@Service
public class LeadReportServiceImpl implements LeadReportService {

	private static final Logger logger = LoggerFactory.getLogger(LeadReportServiceImpl.class);
	// private String campaignId = "56b4a92fe4b0c2b75250c717";
	//private String startDateStr = "31 Days";// 1 Months, 1 Weeks, 1 Years
	// private String endDateStr = "05/01/2016 13:30:00";
	//private String recordingDateStr = "31 Days";
	private static final String PRIMARY_CONTACT = "Primary Contact";
	String clientRegion = "us-west-2";// This should be part of campaign object
	String bucketName = "insideup";// This should be part of campaign object.
	String recordingBucketName = "xtaasrecordings";
	String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
	String awsSecretKey = "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";
	HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();
	private List<String> errorList = new ArrayList<String>();
	String ext = ".zip";
	String fileNameWithoutExt = null;
	//private static String COMMA_DELIMITER = ",";
	private static final String SUFFIX = "/";
	private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;

	@Autowired
	private ClientMappingRepository clientMappingRepository;
	
	@Autowired
	private NewClientMappingRepository newClientMappingRepository;
	
	@Autowired
	private DownloadClientMappingReportServiceImpl downloadClientMappingReportServiceImpl;
	
	@Autowired
	private ProspectCallLogService prospectCallLogService;
	
	@Autowired
	private RealTimeDeliveryRepository realTimeDeliveryRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Autowired
	private QaRejectReasonRepository qaRejectReasonRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private QaService qaService;
	
	@Autowired
	private AbmListNewRepository abmListNewRepository;
	
	@Autowired
	private AbmListDetailRepository abmListDetailRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;
	
	private Workbook book;
	
	private String xlsFlieName;
	
	private HashMap<Integer,List<String>> errorMap= new HashMap<>();

	@Override
	public void downloadZoomReport(String date) throws ParseException, Exception {
		
		if(date==null)
			return;
		
		Date startDate = XtaasDateUtils.getDate(getStartDate(date), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
		try {
		
				downloadZoomReport(startDateInUTC);

		} catch (Exception e) {
			logger.error("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} 

	}
	
	@Override
	public void downloadClientReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO, Login login)
			throws ParseException, Exception {
		Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
		if (campaign == null)
			return;
		AmazonS3 s3client = createAWSConnection(clientRegion);
		/*Date date = new Date();
		// String dateFolder = sdf.format(date);
		DateFormat pstFolder = new SimpleDateFormat("MM/dd/yyyy");
		pstFolder.setTimeZone(TimeZone.getTimeZone("PST"));
		String dateFolder = pstFolder.format(date);

		boolean exists = s3client.doesObjectExist(bucketName, dateFolder);
		if (exists) {

		} else {
			createFolder(bucketName, dateFolder, s3client);
		}

		Date startDate = XtaasDateUtils.getDate(getStartDate(startDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
		// System.out.println(startDateInUTC);

		Date recordingDate = XtaasDateUtils.getDate(getStartDate(recordingDateStr),
				XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		Date recordingDateInUTC = XtaasDateUtils.convertToTimeZone(recordingDate, "UTC");
		// System.out.println(recordingDateInUTC);
*/
		try {
			processDownloadQA(campaign, prospectCallSearchDTO, /*startDateInUTC, recordingDateInUTC, dateFolder,*/
					s3client,login);
		} catch (Exception e) {
			logger.error("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}

	}


	
		
		
		
	
	private String getStartDate(String startDate) {

		// String toDate = "02/10/2018 00:00:00";
		// mm/dd/yyyy
		String toDate = "";
		Calendar c = Calendar.getInstance();
		/*System.out.println("Current date : " + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DATE) + "-"
				+ c.get(Calendar.YEAR));
*/
		String periodActual = startDate;
		int beginIndex = periodActual.indexOf(" ");
		String periodStr = periodActual.substring(0, beginIndex);
		int period = Integer.parseInt(periodStr);
		// logger.debug(period);

		if (startDate.contains("Years")) {
			c.add(Calendar.YEAR, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR)
					+ " 00:00:00";
		} else if (startDate.contains("Months")) {
			c.add(Calendar.MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR)
					+ " 00:00:00";
		} else if (startDate.contains("Weeks")) {
			c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR)
					+ " 00:00:00";
		} else if (startDate.contains("Days")) {
			c.add(Calendar.DAY_OF_MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR)
					+ " 00:00:00";
		}

		return toDate;

	}
	
	
	
	private String getColumnValue(Campaign campaign, Map<String, Map<String, ClientMapping>> clientStdAttribMap,
			Map<String, Map<String, List<ClientMapping>>> titleStdAttribMap, ProspectCallLog pcl, String column,
			Map<String, String> colNames) {
		ProspectCall pc = pcl.getProspectCall();
		
		if("prospect.companyDomain".equalsIgnoreCase(colNames.get(column))){
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.companyDomain");
			if(pc.getProspect().getCustomAttributeValue("domain")!=null && 
					!pc.getProspect().getCustomAttributeValue("domain").toString().isEmpty()){
				if(clientMappingMap !=null && clientMappingMap.size()>0 ){
						ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCustomAttributeValue("domain").toString());
						if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
							return fcm.getValue(); 
						}
						if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
							return fcm.getDefaultValue();
						}
						return pc.getProspect().getCustomAttributeValue("domain").toString();

				}
			}else{
				return "";
			}
		}
		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.zoomCompanyUrl".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zoomCompanyUrl");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getZoomCompanyUrl());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getZoomCompanyUrl();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.zoomPersonUrl".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zoomPersonUrl");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getZoomPersonUrl());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getZoomPersonUrl();

			}
		}
		// }
		if("prospect.jobFunction".equalsIgnoreCase(colNames.get(column))){
          	 Map<String, List<ClientMapping>> clientMappingMap = titleStdAttribMap.get("prospect.jobFunction");
	       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
	       		Map<String,String> titleMap = getTitlesMap();
	       		String titleCode = titleMap.get(pc.getProspect().getDepartment());
	       		 List<ClientMapping> fcmList = clientMappingMap.get(titleCode);
	       		 if(fcmList!=null){
		       		for(ClientMapping fcm : fcmList){
			       		 if(fcm!=null && fcm.getKey()!=null && !fcm.getKey().isEmpty() && fcm.getValue()!=null 
			       				 && !fcm.getValue().isEmpty() &&  fcm.getTitleMgmtLevel()!=null 
					       				 && !fcm.getTitleMgmtLevel().isEmpty()){
			       			 if(fcm.getTitleMgmtLevel().equalsIgnoreCase(pc.getProspect().getManagementLevel()))
			       				 return fcm.getValue();
			       		 }
		       		}
	       		 }else{
	       			for (Map.Entry<String,List<ClientMapping>> entry : clientMappingMap.entrySet()){
	       				List<ClientMapping> cmMap = entry.getValue();
	       				for(ClientMapping cm : cmMap){
	       					if(cm.getDefaultValue()!=null && !cm.getDefaultValue().isEmpty()){
	       						return cm.getDefaultValue();
	       					}
	       				}
	       			}
	       		 }
	       		 return pc.getProspect().getTitle();
	       		 
	       	 }
		}

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.title".equalsIgnoreCase(colNames.get(column))) {
			Map<String, List<ClientMapping>> clientMappingMap = titleStdAttribMap.get("prospect.title");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				Map<String, String> titleMap = getTitlesMap();
				String titleCode = titleMap.get(pc.getProspect().getDepartment());
				List<ClientMapping> fcmList = clientMappingMap.get(titleCode);
				if (fcmList != null) {
					for (ClientMapping fcm : fcmList) {
						if (fcm != null && fcm.getKey() != null && !fcm.getKey().isEmpty() && fcm.getValue() != null
								&& !fcm.getValue().isEmpty() && fcm.getTitleMgmtLevel() != null
								&& !fcm.getTitleMgmtLevel().isEmpty()) {
							if (fcm.getTitleMgmtLevel().equalsIgnoreCase(pc.getProspect().getManagementLevel()))
								return fcm.getValue();
						}
					}
				} else {
					for (Map.Entry<String, List<ClientMapping>> entry : clientMappingMap.entrySet()) {
						List<ClientMapping> cmMap = entry.getValue();
						for (ClientMapping cm : cmMap) {
							if (cm.getDefaultValue() != null && !cm.getDefaultValue().isEmpty()) {
								return cm.getDefaultValue();
							}
						}
					}
				}
				return pc.getProspect().getTitle();

			}
		}

		if ("prospect.prospectCallId".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.prospectCallId");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspectCallId());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspectCallId();

			}
		}
		
		if ("prospect.recordingUrlDownload".equalsIgnoreCase(colNames.get(column))) {
			
			try {
				// URL url = new URL(pc.getRecordingUrl());
				// TODO here you can change the file name as needed
				String campaignName="";
				 if(campaign.getClientCampaignName()!=null && !campaign.getClientCampaignName().isEmpty() ){
					 campaignName = campaign.getClientCampaignName();
				 }else{
					 campaignName = campaign.getName();
				 }
				 String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
				String fileName = pc.getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "") + "_"
						+ pc.getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "") + "_" + cleanCampaignName
						+ ".wav";

				
				return fileName;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if("prospect.recordingUrl".equalsIgnoreCase(colNames.get(column))){
	           //return pc.getRecordingUrl();
			if(pc.getRecordingUrlAws()==null || pc.getRecordingUrlAws().isEmpty()){
				if(pc.getRecordingUrl()==null || pc.getRecordingUrl().isEmpty()){
					return "";
				}
					String rUrl = pc.getRecordingUrl();
					int lastIndex = rUrl.lastIndexOf("/");
					String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
					String rdfile = xfileName + ".wav";
					pc.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
				}
			return pc.getRecordingUrlAws();
 		}
		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.managementLevel".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.managementLevel");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getManagementLevel());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getManagementLevel();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.department".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.department");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getDepartment());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getDepartment();

			}
		}
		
		if("prospect.naics".equalsIgnoreCase(colNames.get(column))){
			if(pc.getProspect().getSourceCompanyId()!=null && !pc.getProspect().getSourceCompanyId().isEmpty()){
				AbmListDetail abmListDetail = abmListDetailRepository.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if(abmListDetail != null){
					if(abmListDetail.getCompanyNAICS()!=null && abmListDetail.getCompanyNAICS().size()>0){
						String naics = String.join(",", abmListDetail.getCompanyNAICS());
						return naics;
					}
				}
			}
			
		}
		
		if("prospect.sic".equalsIgnoreCase(colNames.get(column))){
			if(pc.getProspect().getSourceCompanyId()!=null && !pc.getProspect().getSourceCompanyId().isEmpty()){
				AbmListDetail abmListDetail = abmListDetailRepository.findOneByCompanyId(pc.getProspect().getSourceCompanyId());
				if(abmListDetail != null){
					if(abmListDetail.getCompanySIC()!=null && abmListDetail.getCompanySIC().size()>0){
						String sic = String.join(",", abmListDetail.getCompanySIC());
						return sic;
					}
				}
			}
		}
		
		if("prospect.inputABMValue".equalsIgnoreCase(colNames.get(column))){
			//List<AbmListNew> abmList = abmListNewRepository.findABMListByCampaignId(pc.getCampaignId());
			/*Map<String,String> abmMap = new HashMap<String, String>();
			if(abmList!=null && abmList.size()>0){
				for(AbmList abm : abmList){
					List<KeyValuePair<String, String>> companies = abm.getCompanyList();
					for (KeyValuePair<String, String> companykv : companies) {
						if(companykv.getValue()!=null && !companykv.getValue().isEmpty() 
								&& !companykv.getValue().equalsIgnoreCase("NOT_FOUND") 
								&& !companykv.getValue().equalsIgnoreCase("GET_COMPANY_ID")){
							
							abmMap.put(companykv.getValue(), companykv.getKey());	
						}
					}
				}*/
				if(pc.getProspect().getSourceCompanyId()!=null && !pc.getProspect().getSourceCompanyId().isEmpty()){
					AbmListNew abmListNew = abmListNewRepository.findOneByCampaignIdCompanyId(pc.getCampaignId(), 
							pc.getProspect().getSourceCompanyId());

					if(abmListNew != null) {
						return abmListNew.getCompanyName();
					}
				}
			//}
			return "";
		}

		
		// For exact employee count
		if ("prospect.exactEmployeeCount".equalsIgnoreCase(colNames.get(column))) {
			if (pc.getProspect().getCustomAttributeValue("empCount") == null
					|| pc.getProspect().getCustomAttributeValue("empCount").equals("")) {
				return "";
			}
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.exactEmployeeCount");
			String exactEmployeeCount = pc.getProspect().getCustomAttributeValue("empCount").toString();
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(exactEmployeeCount);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCustomAttributeValue("empCount").toString();
			}
		}

		// For exact revenue count
		if ("prospect.exactRevenueCount".equalsIgnoreCase(colNames.get(column))) {
			if (pc.getProspect().getCustomAttributeValue("revCount") == null
					|| pc.getProspect().getCustomAttributeValue("revCount").equals("")) {
				return "000";
			}
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.exactRevenueCount");
			String exactEmployeeCount = pc.getProspect().getCustomAttributeValue("revCount").toString();
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(exactEmployeeCount);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCustomAttributeValue("revCount").toString();
			}
		}
				
		// }
		// 1-5, 5-10,10-20,20-50,50-100,100-250,250-500,500-1,000,1,000-5,000,
		// 5,000-10,000,Over 10,000
		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.employeeRange".equalsIgnoreCase(colNames.get(column))) {
			// String maxempcount =
			// pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
			if (pc.getProspect().getCustomAttributeValue("maxEmployeeCount") == null
					|| pc.getProspect().getCustomAttributeValue("maxEmployeeCount").equals(""))
				return "";
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.employeeRange");
			String maxEmp = pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();

			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(maxEmp);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();

			}
		}
		// }

		// Under
		// $500K,$500K-$1M,$1M-$5M,$5M-$10M,$10M-$25M,$25M-$50M,$25M-$50M,$50M-$100M,
		// $100M-$250M,$250M-$500M,$500M-$1B,$1B-$5B,Over $5B
		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.revenueRange".equalsIgnoreCase(colNames.get(column))) {
			if (pc.getProspect().getCustomAttributeValue("maxRevenue") == null
					|| pc.getProspect().getCustomAttributeValue("maxRevenue").equals(""))
				return "";
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.revenueRange");
			String maxRev = pc.getProspect().getCustomAttributeValue("maxRevenue").toString();
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(maxRev);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCustomAttributeValue("maxRevenue").toString();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.country".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.country");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCountry());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCountry();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.zip".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zip");
			if (clientMappingMap != null && clientMappingMap.size() > 0 && pc.getProspect().getZipCode() != null) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getZipCode());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				String zip = pc.getProspect().getZipCode();

				/////
				// String str = "abc$def^ghi#jkl";

				Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(zip);

				//System.out.println(zip);
				int count = 0;
				while (m.find()) {
					count = m.start();
					//System.out.println("position " + m.start() + ": " + zip.charAt(m.start()));
				}
				if (count > 0) {
					int ziplength = zip.length();
					String tempString = zip.substring(0, count);
					return tempString;
				}
				/////////////////////

				return zip;

			} else {
				return "";
			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.state".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.state");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getStateCode());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getStateCode();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.city".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.city");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCity());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCity();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.address".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.address");
			if (clientMappingMap != null && clientMappingMap.size() > 0 && pc.getProspect().getAddressLine1() != null) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getAddressLine1());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getAddressLine1().replaceAll("[^a-zA-Z0-9\\s]", "");

			} else {
				return "";
			}
		}
		if("prospect.fullAddress".equalsIgnoreCase(colNames.get(column))){
          	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.fullAddress");
	       	 if(clientMappingMap !=null && clientMappingMap.size()>0  && pc.getProspect().getAddressLine1() !=null ){
	       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getAddressLine1());
	       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
	       			return fcm.getValue(); 
	       		 }
	       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
	       			 return fcm.getDefaultValue();
	       		 }
	       		 
	       		String fullAddress = pc.getProspect().getAddressLine1()
	       				+" "+pc.getProspect().getCity()+" "+pc.getProspect().getStateCode();
	       		return fullAddress.replaceAll("[^a-zA-Z0-9\\s]", ""); 
	       		  
	       		 
	       	 }else{
	       		 return "";
	       	 }
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.organization".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.organization");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCompany());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getCompany();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.email".equalsIgnoreCase(colNames.get(column))) {

			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.email");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getEmail());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getEmail();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.phone".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.phone");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getPhone());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return pc.getProspect().getPhone();

			}
		}
		// }

		if ("prospect.fullName".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.fullName");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				
				String fn = StringUtils.capitalize(pc.getProspect().getFirstName());
				String ln = StringUtils.capitalize(pc.getProspect().getLastName());
				String fullName = fn + " " + ln;
				return fullName;

			}
		}

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.firstName".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.firstName");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getFirstName());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return StringUtils.capitalize(fcm.getValue());
				}

				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return StringUtils.capitalize(fcm.getDefaultValue());
				}
				return StringUtils.capitalize(pc.getProspect().getFirstName());

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.lastName".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.lastName");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getProspect().getLastName());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return StringUtils.capitalize(fcm.getValue());
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return StringUtils.capitalize(fcm.getDefaultValue());
				}
				return StringUtils.capitalize(pc.getProspect().getLastName());

			}
		}
		// }

		if ("prospect.industryGroup".equalsIgnoreCase(colNames.get(column))) {
			Map<String, String> industryGroupMap = getIndustriesGroupMap();
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.industryGroup");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				String key = industryGroupMap.get(pc.getProspect().getIndustry());
				ClientMapping fcm = clientMappingMap.get(key);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					if (fcm.getValue().equalsIgnoreCase("")) {
						//System.out.println("BUG");
					}
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					if (fcm.getDefaultValue().equalsIgnoreCase("")) {
						//System.out.println("BUG");
					}
					return fcm.getDefaultValue();
				}
				/*if (pc.getProspect().getIndustry().equalsIgnoreCase("")) {
					//System.out.println("BUG");
				}*/
				ClientMapping defaultFCM = clientMappingMap.get("UNKNOWN INDUSTRY");
				if (defaultFCM != null) {
					return defaultFCM.getValue();
				}
				return pc.getProspect().getIndustry();

			}
		}

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.industry".equalsIgnoreCase(colNames.get(column))) {
			Map<String, String> industryMap = getIndustriesMap();
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.industry");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				String key = industryMap.get(pc.getProspect().getIndustry());
				ClientMapping fcm = clientMappingMap.get(key);
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					if (fcm.getValue().equalsIgnoreCase("")) {
						//System.out.println("BUG");
					}
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					if (fcm.getDefaultValue().equalsIgnoreCase("")) {
						//System.out.println("BUG");
					}
					return fcm.getDefaultValue();
				}
				/*if (pc.getProspect().getIndustry().equalsIgnoreCase("")) {
					//System.out.println("BUG");
				}*/
				ClientMapping defaultFCM = clientMappingMap.get("UNKNOWN INDUSTRY");
				if (defaultFCM != null) {
					return defaultFCM.getValue();
				}
				return pc.getProspect().getIndustry();

			}
		}
		// }

		// if(clientStdAttribMap.containsKey(colNames.get(column))){
		if ("prospect.created".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.created");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getCallStartTime());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				// DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
				DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
				pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
				String pstStr = pstFormat.format(pc.getCallStartTime());
				// Date pstDate = pstFormat.parse(pstStr);

				// System.out.println(pstFormat.format(pstDate));

				return pstStr;

			}
		}
		if ("prospect.createdTime".equalsIgnoreCase(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.createdTime");
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(pc.getCallStartTime());
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					DateFormat pstFormat = new SimpleDateFormat(fcm.getValue());
					pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
					String fcmpstStr = pstFormat.format(pc.getCallStartTime());
					return fcmpstStr;
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				// DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
				DateFormat pstFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
				String pstStr = pstFormat.format(pc.getCallStartTime());
				// Date pstDate = pstFormat.parse(pstStr);

				// System.out.println(pstFormat.format(pstDate));

				return pstStr;

			}
		}
		if("prospect.assetName".equalsIgnoreCase(colNames.get(column))){
       	 	Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.assetName");
	       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
	       		// if(campaign.getActiveAsset() != null && campaign.getActiveAsset().getName()!=null){
	       		if(pc.getDeliveredAssetId()!=null && !pc.getDeliveredAssetId().isEmpty()){
	       			String assetName = getAssetNameFromId(campaign,pc.getDeliveredAssetId());
            			
        			//if(campaign.getActiveAsset()!=null && campaign.getActiveAsset().getName()!=null && !campaign.getActiveAsset().getName().isEmpty()){
        			
		       		 ClientMapping fcm = clientMappingMap.get(assetName);
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
	       		 }
	       		String assetName = "";
	       		if(pc.getDeliveredAssetId()!=null && !pc.getDeliveredAssetId().isEmpty()){
        			//if(campaign.getActiveAsset()!=null && campaign.getActiveAsset().getName()!=null && !campaign.getActiveAsset().getName().isEmpty()){
        				assetName = getAssetNameFromId(campaign,pc.getDeliveredAssetId());
        		}
                   
	       		 return assetName;
	       		 
	       	 }
		}

		// }
		if (pc.getAnswers() != null)
			for (AgentQaAnswer agentQaAnswer : pc.getAnswers()) {
				
				/*
				 * if(column=="C1.ValidLead"){ System.out.println("NULL HERE"); }
				 */
				
				/*if(column!=null){
					System.out.println(column);
				}else{
					System.out.println("NULL HERE")
				}*/
				
				if (column != null && colNames != null && agentQaAnswer != null && agentQaAnswer.getQuestion() != null
						&& colNames.get(column).equalsIgnoreCase(agentQaAnswer.getQuestion())
						&& clientStdAttribMap.containsKey(agentQaAnswer.getQuestion())) {
					Map<String, ClientMapping> crm = clientStdAttribMap.get(agentQaAnswer.getQuestion());
					for (Map.Entry<String, ClientMapping> crmentry : crm.entrySet()) {
						if (crmentry.getKey().equalsIgnoreCase(agentQaAnswer.getAnswer())) {
							ClientMapping cm = crmentry.getValue();
							if (cm.getValue() != null && !cm.getValue().isEmpty())
								return cm.getValue();

							if (cm.getDefaultValue() != null && !cm.getDefaultValue().isEmpty())
								return cm.getDefaultValue();

						}

					}
				}

			}
		if(pcl.getQaFeedback()!=null && pcl.getQaFeedback().getQaAnswers() != null){
			for (AgentQaAnswer qaAnswer : pcl.getQaFeedback().getQaAnswers()) {
				String qaQuest = "QA-"+qaAnswer.getQuestion();
				if (column!=null && qaAnswer.getQuestion() != null
					&& colNames.get(column).equalsIgnoreCase(qaQuest)) {
				//for (Map.Entry<String, ClientMapping> crmentry : crm.entrySet()) {
					//if (pcl.getQaFeedback()!= null && pcl.getQaFeedback().getQaAnswers()!=null) {
						//for (AgentQaAnswer qaAnswer : pcl.getQaFeedback().getQaAnswers()) {
        		   			//if (qaAnswer.getQuestion()!=null 
        		   				//	&& qaAnswer.getQuestion().equalsIgnoreCase(agentQaAnswer.getQuestion())){
        		   				String answerCertification = qaAnswer.getAnswerCertification();
        		   				return answerCertification;
        		   			//}
        		       	 //}
        			//}

					}
			}
		}
		if (clientStdAttribMap.containsKey(colNames.get(column))) {
			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get(colNames.get(column));
			if (clientMappingMap != null && clientMappingMap.size() > 0) {
				ClientMapping fcm = clientMappingMap.get(colNames.get(column));
				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
					return fcm.getValue();
				}
				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
					return fcm.getDefaultValue();
				}
				return "";

			}
		}

		return "";
	}

	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
			s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			return s3client;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(
					"Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :"
							+ e);
			return s3client;
		}
	}

	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName, emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}

	/**
	 * This method first deletes all the files in given folder and than the folder
	 * itself
	 */
	/*
	 * public static void deleteFolder(String bucketName, String folderName,
	 * AmazonS3 client) { List<S3ObjectSummary> fileList =
	 * client.listObjects(bucketName, folderName).getObjectSummaries(); for
	 * (S3ObjectSummary file : fileList) { client.deleteObject(bucketName,
	 * file.getKey()); } client.deleteObject(bucketName, folderName); }
	 */

	void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				deleteDir(f);
			}
		}
		file.delete();
	}

	public void zipIt(String zipFile, String folderName, List<String> fileList) {
		byte[] buffer = new byte[1024];
		String source = new File(folderName).getName();
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		try {
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(fos);

			logger.debug("Output to Zip : " + zipFile);
			FileInputStream in = null;

			for (String file : fileList) {
				logger.debug("File Added : " + file);
				ZipEntry ze = new ZipEntry(source + File.separator + file);
				zos.putNextEntry(ze);
				try {
					in = new FileInputStream(folderName + File.separator + file);
					int len;
					while ((len = in.read(buffer)) > 0) {
						zos.write(buffer, 0, len);
					}
				} finally {
					in.close();
				}
			}

			zos.closeEntry();
			logger.debug("Folder successfully compressed");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				zos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void generateFileList(File node, String folderName, List<String> fileList) {
		// add file only
		if (node.isFile()) {
			fileList.add(generateZipEntry(node.toString(), folderName));
		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename), folderName, fileList);
			}
		}
	}

	private String generateZipEntry(String file, String folderName) {
		return file.substring(folderName.length() + 1, file.length());
	}

	private Map<String, String> getTitlesMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("Consultants", "TitleCode.Consultants");
		titleMap.put("Consulting", "TitleCode.Consultants");

		titleMap.put("Biometrics", "TitleCode.Biometrics");
		titleMap.put("Engineering & Technical", "TitleCode.Technical");
		titleMap.put("Biotechnical Engineering", "TitleCode.Technical");
		titleMap.put("Chemical Engineering", "TitleCode.Technical");
		titleMap.put("Civil Engineering", "TitleCode.Technical");
		titleMap.put("Database Engineering &  Design", "TitleCode.Technical");
		titleMap.put("Hardware Engineering", "TitleCode.Technical");
		titleMap.put("Industrial Engineering", "TitleCode.Technical");
		titleMap.put("Information Technology", "TitleCode.Technical");
		titleMap.put("Quality Assurance", "TitleCode.Technical");
		titleMap.put("Software Engineering", "TitleCode.Technical");
		titleMap.put("Technical", "TitleCode.Technical.Technical");
		titleMap.put("Web Development", "TitleCode.Technical");

		titleMap.put("Engineering", "TitleCode.Technical.Engineer");

		titleMap.put("Finance", "TitleCode.Finance");
		titleMap.put("Accounting", "TitleCode.Finance");
		titleMap.put("Banking", "TitleCode.Finance");
		titleMap.put("General Finance", "TitleCode.Finance");
		titleMap.put("Investment Banking", "TitleCode.Finance");
		titleMap.put("Wealth Management", "TitleCode.Finance");

		titleMap.put("Human Resources", "TitleCode.HR");
		titleMap.put("Benefits & Compensation", "TitleCode.HR");
		titleMap.put("Diversity", "TitleCode.HR");
		titleMap.put("Human Resources Generalist", "TitleCode.HR");
		titleMap.put("Recruiting", "TitleCode.HR");
		titleMap.put("Sourcing", "TitleCode.HR");

		titleMap.put("Legal", "TitleCode.Law");
		titleMap.put("Governmental Lawyers & Legal Professionals", "TitleCode.Law");
		titleMap.put("Lawyers & Legal Professionals", "TitleCode.Law");

		titleMap.put("Marketing", "TitleCode.Marketing");
		titleMap.put("Advertising", "TitleCode.Marketing");
		titleMap.put("Branding", "TitleCode.Marketing");
		titleMap.put("Communications", "TitleCode.Marketing");
		titleMap.put("E-biz", "TitleCode.Marketing");
		titleMap.put("General Marketing", "TitleCode.Marketing");
		titleMap.put("Product Management", "TitleCode.Marketing");

		titleMap.put("Medical & Health", "TitleCode.Health");
		titleMap.put("Dentists", "TitleCode.Health");
		titleMap.put("Doctors - General Practice", "TitleCode.Health");
		titleMap.put("Health Professionals", "TitleCode.Health");
		titleMap.put("Nurses", "TitleCode.Health");

		titleMap.put("Operations", "TitleCode.Operations");
		titleMap.put("Change Management", "TitleCode.Operations");
		titleMap.put("Compliance", "TitleCode.Operations");
		titleMap.put("Contracts", "TitleCode.Operations");
		titleMap.put("Customer Relations", "TitleCode.Operations");
		titleMap.put("Facilities", "TitleCode.Operations");
		titleMap.put("Logistics", "TitleCode.Operations");
		titleMap.put("General Operations", "TitleCode.Operations");
		titleMap.put("Quality Management", "TitleCode.Operations");

		titleMap.put("Sales", "TitleCode.Sales");
		titleMap.put("Business Development", "TitleCode.Sales");
		titleMap.put("General Sales", "TitleCode.Sales");

		titleMap.put("Scientists", "TitleCode.Scientist");
		titleMap.put("General Management", "TitleCode.TopExec");

		return titleMap;

	}

	private Map<String, String> getIndustriesGroupMap() {
		Map<String, String> industriesMap = new HashMap<String, String>();

		industriesMap.put("Agriculture", "Industry.agriculture");
		industriesMap.put("Animals & Livestock", "Industry.agriculture");
		industriesMap.put("Crops", "Industry.agriculture");
		industriesMap.put("Agriculture General", "Industry.agriculture");

		industriesMap.put("Business Services", "Industry.bizservice");
		industriesMap.put("Accounting & Accounting Services", "Industry.bizservice");
		industriesMap.put("Auctions", "Industry.bizservice");
		industriesMap.put("Call Centers & Business Centers", "Industry.bizservice");
		industriesMap.put("Debt Collection", "Industry.bizservice");
		industriesMap.put("Management Consulting", "Industry.bizservice");
		industriesMap.put("Information & Document Management", "Industry.bizservice");
		industriesMap.put("Multimedia & Graphic Design", "Industry.bizservice");
		industriesMap.put("Food Service", "Industry.bizservice");
		industriesMap.put("Business Services General", "Industry.bizservice");
		industriesMap.put("Human Resources & Staffing", "Industry.bizservice");
		industriesMap.put("Facilities Management & Commercial Cleaning", "Industry.bizservice");
		industriesMap.put("Translation & Linguistic Services", "Industry.bizservice");
		industriesMap.put("Advertising & Marketing", "Industry.bizservice");
		industriesMap.put("Commercial Printing", "Industry.bizservice");
		industriesMap.put("Security Products & Services", "Industry.bizservice");

		industriesMap.put("Chambers of Commerce", "Industry.chamber");

		industriesMap.put("Construction", "Industry.construction");
		industriesMap.put("Architecture, Engineering & Design", "Industry.construction");
		industriesMap.put("Commercial & Residential Construction", "Industry.construction");
		industriesMap.put("Construction General", "Industry.construction");

		industriesMap.put("Consumer Services", "Industry.consumerservices");
		industriesMap.put("Automotive Service & Collision Repair", "Industry.consumerservices");
		industriesMap.put("Car & Truck Rental", "Industry.consumerservices");
		industriesMap.put("Funeral Homes & Funeral Related Services", "Industry.consumerservices");
		industriesMap.put("Consumer Services General", "Industry.consumerservices");
		industriesMap.put("Hair Salons", "Industry.consumerservices");
		industriesMap.put("Laundry & Dry Cleaning Services", "Industry.consumerservices");
		industriesMap.put("Photography Studio", "Industry.consumerservices");
		industriesMap.put("Travel Agencies & Services", "Industry.consumerservices");
		industriesMap.put("Veterinary Care", "Industry.consumerservices");
		industriesMap.put("Weight & Health Management", "Industry.consumerservices");

		industriesMap.put("Cultural", "Industry.cultural");
		industriesMap.put("Cultural General", "Industry.cultural");
		industriesMap.put("Libraries", "Industry.cultural");
		industriesMap.put("Museums & Art Galleries", "Industry.cultural");

		industriesMap.put("Education", "Industry.education");
		industriesMap.put("Education General", "Industry.education");
		industriesMap.put("K-12 Schools", "Industry.education");
		industriesMap.put("Training", "Industry.education");
		industriesMap.put("Colleges & Universities", "Industry.education");

		industriesMap.put("Energy, Utilities & Waste Treatment", "Industry.energy");
		industriesMap.put("Energy, Utilities & Waste Treatment General", "Industry.energy");
		industriesMap.put("Electricity, Oil & Gas", "Industry.energy");
		industriesMap.put("Waste Treatment, Environmental Services & Recycling", "Industry.energy");
		industriesMap.put("Energy, Utilities, & Waste Treatment General", "Industry.energy");
		industriesMap.put("Oil & Gas Exploration & Services", "Industry.energy");
		industriesMap.put("Water & Water Treatment", "Industry.energy");

		industriesMap.put("Finance", "Industry.finance");
		industriesMap.put("Banking", "Industry.finance");
		industriesMap.put("Brokerage", "Industry.finance");
		industriesMap.put("Credit Cards & Transaction  Processing", "Industry.finance");
		industriesMap.put("Finance General", "Industry.finance");
		industriesMap.put("Investment Banking", "Industry.finance");
		industriesMap.put("Venture Capital & Private Equity", "Industry.finance");

		industriesMap.put("Government", "Industry.government");

		industriesMap.put("Healthcare", "Industry.healthcare");
		industriesMap.put("Emergency Medical Transportation & Services", "Industry.healthcare");
		industriesMap.put("Healthcare General", "Industry.healthcare");
		industriesMap.put("Hospitals & Clinics", "Industry.healthcare");
		industriesMap.put("Medical Testing & Clinical Laboratories", "Industry.healthcare");
		industriesMap.put("Pharmaceuticals", "Industry.healthcare");
		industriesMap.put("Biotechnology", "Industry.healthcare");
		industriesMap.put("Drug Manufacturing & Research", "Industry.healthcare");

		industriesMap.put("Hospitality", "Industry.hospitality");
		industriesMap.put("Hospitality General", "Industry.hospitality");
		industriesMap.put("Lodging & Resorts", "Industry.hospitality");
		industriesMap.put("Recreation", "Industry.hospitality");
		industriesMap.put("Movie Theaters", "Industry.hospitality");
		industriesMap.put("Fitness & Dance Facilities", "Industry.hospitality");
		industriesMap.put("Gambling & Gaming", "Industry.hospitality.recreation");
		industriesMap.put("Amusement Parks, Arcades & Attractions", "Industry.hospitality");
		industriesMap.put("Zoos & National Parks", "Industry.hospitality");
		industriesMap.put("Restaurants", "Industry.hospitality");
		industriesMap.put("Sports Teams & Leagues", "Industry.hospitality");

		industriesMap.put("Insurance", "Industry.insurance");

		industriesMap.put("Law Firms & Legal Services", "Industry.legal");

		industriesMap.put("Media & Internet", "Industry.media");
		industriesMap.put("Broadcasting", "Industry.media");
		industriesMap.put("Film/Video Production & Services", "Industry.media");
		industriesMap.put("Radio Stations", "Industry.media");
		industriesMap.put("Television Stations", "Industry.media");
		industriesMap.put("Media & Internet General", "Industry.media");
		industriesMap.put("Information Collection & Delivery", "Industry.media");
		industriesMap.put("Search Engines & Internet Portals", "Industry.media");
		industriesMap.put("Music & Music Related Services", "Industry.media");
		industriesMap.put("Newspapers & News Services", "Industry.media");
		industriesMap.put("Publishing", "Industry.media");

		industriesMap.put("Manufacturing", "Industry.mfg");
		industriesMap.put("Aerospace & Defense", "Industry.mfg");
		industriesMap.put("Boats & Submarines", "Industry.mfg");
		industriesMap.put("Building Materials", "Industry.mfg");
		industriesMap.put("Aggregates, Concrete & Cement", "Industry.mfg");
		industriesMap.put("Lumber, Wood Production & Timber Operations", "Industry.mfg");
		industriesMap.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)", "Industry.mfg");
		industriesMap.put("Plumbing & HVAC Equipment", "Industry.mfg");
		industriesMap.put("Motor Vehicles", "Industry.mfg");
		industriesMap.put("Motor Vehicle Parts", "Industry.mfg");
		industriesMap.put("Chemicals, Petrochemicals, Glass & Gases", "Industry.mfg");
		industriesMap.put("Chemicals", "Industry.mfg");
		industriesMap.put("Gases", "Industry.mfg");
		industriesMap.put("Glass & Clay", "Industry.mfg");
		industriesMap.put("Petrochemicals", "Industry.mfg");
		industriesMap.put("Computer Equipment & Peripherals", "Industry.mfg");
		industriesMap.put("Personal Computers & Peripherals", "Industry.mfg");
		industriesMap.put("Computer Networking Equipment", "Industry.mfg");
		industriesMap.put("Network Security Hardware & Software", "Industry.mfg");
		industriesMap.put("Computer Storage Equipment", "Industry.mfg");
		industriesMap.put("Consumer Goods", "Industry.mfg");
		industriesMap.put("Appliances", "Industry.mfg");
		industriesMap.put("Cleaning Products", "Industry.mfg");
		industriesMap.put("Textiles & Apparel", "Industry.mfg");
		industriesMap.put("Cosmetics, Beauty Supply & Personal Care Products", "Industry.mfg");
		industriesMap.put("Consumer Electronics", "Industry.mfg");
		industriesMap.put("Health & Nutrition Products", "Industry.mfg");
		industriesMap.put("Household Goods", "Industry.mfg");
		industriesMap.put("Pet Products", "Industry.mfg");
		industriesMap.put("Photographic & Optical Equipment", "Industry.mfg");
		industriesMap.put("Sporting Goods", "Industry.mfg");
		industriesMap.put("Hand, Power and Lawn-care Tools", "Industry.mfg");
		industriesMap.put("Jewelry & Watches", "Industry.mfg");
		industriesMap.put("Electronics", "Industry.mfg");
		industriesMap.put("Batteries, Power Storage Equipment & Generators", "Industry.mfg");
		industriesMap.put("Electronic Components", "Industry.mfg");
		industriesMap.put("Power Conversion & Protection Equipment", "Industry.mfg");
		industriesMap.put("Semiconductor & Semiconductor Equipment", "Industry.mfg");
		industriesMap.put("Food, Beverages & Tobacco", "Industry.mfg");
		industriesMap.put("Food & Beverages", "Industry.mfg");
		industriesMap.put("Tobacco", "Industry.mfg");
		industriesMap.put("Wineries & Breweries", "Industry.mfg");
		industriesMap.put("Furniture", "Industry.mfg");
		industriesMap.put("Manufacturing General", "Industry.mfg");
		industriesMap.put("Industrial Machinery & Equipment", "Industry.mfg");
		industriesMap.put("Medical Devices &  Equipment", "Industry.mfg");
		industriesMap.put("Pulp & Paper", "Industry.mfg");
		industriesMap.put("Plastic, Packaging & Containers", "Industry.mfg");
		industriesMap.put("Tires & Rubber", "Industry.mfg");
		industriesMap.put("Telecommunication  Equipment", "Industry.mfg");
		industriesMap.put("Test & Measurement Equipment", "Industry.mfg");
		industriesMap.put("Toys & Games", "Industry.mfg");
		industriesMap.put("Wire & Cable", "Industry.mfg");

		industriesMap.put("Metals & Mining", "Industry.mm");
		industriesMap.put("Metals & Mining General", "Industry.mm");
		industriesMap.put("Metals & Minerals", "Industry.mm");
		industriesMap.put("Mining", "Industry.mm");

		industriesMap.put("Cities, Towns & Municipalities", "Industry.municipal");
		industriesMap.put("Cities, Towns & Municipalities General", "Industry.municipal");
		industriesMap.put("Public Safety", "Industry.municipal");

		industriesMap.put("Organizations", "Industry.orgs");
		industriesMap.put("Membership Organizations", "Industry.orgs");
		industriesMap.put("Charitable Organizations & Foundations", "Industry.orgs");
		industriesMap.put("Organizations General", "Industry.orgs");
		industriesMap.put("Religious Organizations", "Industry.orgs");

		industriesMap.put("Real Estate", "Industry.realestate");

		industriesMap.put("Retail", "Industry.retail");
		industriesMap.put("Motor Vehicle Dealers", "Industry.retail");
		industriesMap.put("Automobile Parts Stores", "Industry.retail");
		industriesMap.put("Record, Video & Book Stores", "Industry.retail");
		industriesMap.put("Apparel & Accessories Retail", "Industry.retail");
		industriesMap.put("Gas Stations, Convenience & Liquor Stores", "Industry.retail");
		industriesMap.put("Department Stores, Shopping Centers & Superstores", "Industry.retail");
		industriesMap.put("Consumer Electronics & Computers Retail", "Industry.retail");
		industriesMap.put("Furniture", "Industry.retail");
		industriesMap.put("Retail General", "Industry.retail");
		industriesMap.put("Flowers, Gifts & Specialty Stores", "Industry.retail");
		industriesMap.put("Grocery Retail", "Industry.retail");
		industriesMap.put("Home Improvement & Hardware Retail", "Industry.retail");
		industriesMap.put("Vitamins, Supplements, & Health Stores", "Industry.retail");
		industriesMap.put("Jewelry & Watch Retail", "Industry.retail");
		industriesMap.put("Office Products Retail & Distribution", "Industry.retail");
		industriesMap.put("Pet Products", "Industry.retail");
		industriesMap.put("Drug Stores & Pharmacies", "Industry.retail");
		industriesMap.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)",
				"Industry.retail");
		industriesMap.put("Sporting & Recreational Equipment Retail", "Industry.retail");
		industriesMap.put("Toys & Games", "Industry.retail");
		industriesMap.put("Video & DVD Rental", "Industry.retail");

		industriesMap.put("Software", "Industry.software");
		industriesMap.put("Software & Technical Consulting", "Industry.software");
		industriesMap.put("Software General", "Industry.software");
		industriesMap.put("Software Development & Design", "Industry.software");
		industriesMap.put("Business Intelligence (BI) Software", "Industry.software");
		industriesMap.put("Content & Collaboration Software", "Industry.software");
		industriesMap.put("Customer Relationship Management (CRM) Software", "Industry.software");
		industriesMap.put("Database & File Management Software", "Industry.software");
		industriesMap.put("Engineering Software", "Industry.software");
		industriesMap.put("Enterprise Resource Planning (ERP) Software", "Industry.software");
		industriesMap.put("Financial, Legal & HR Software", "Industry.software");
		industriesMap.put("Healthcare Software", "Industry.software");
		industriesMap.put("Multimedia, Games and Graphics Software", "Industry.software");
		industriesMap.put("Networking Software", "Industry.software");
		industriesMap.put("Supply Chain Management (SCM) Software", "Industry.software");
		industriesMap.put("Security Software", "Industry.software");
		industriesMap.put("Storage & System Management Software", "Industry.software");
		industriesMap.put("Retail Software", "Industry.software");

		industriesMap.put("Telecommunications", "Industry.telecom");
		industriesMap.put("Cable & Satellite", "Industry.telecom");
		industriesMap.put("Telecommunications General", "Industry.telecom");
		industriesMap.put("Internet Service Providers, Website Hosting & Internet-related Services",
				"Industry.telecom");
		industriesMap.put("Telephony & Wireless", "Industry.telecom");

		industriesMap.put("Transportation", "Industry.transportation");
		industriesMap.put("Airlines, Airports & Air Services", "Industry.transportation");
		industriesMap.put("Freight & Logistics Services", "Industry.transportation");
		industriesMap.put("Transportation General", "Industry.transportation");
		industriesMap.put("Marine Shipping & Transportation", "Industry.transportation");
		industriesMap.put("Trucking, Moving & Storage", "Industry.transportation");
		industriesMap.put("Rail, Bus & Taxi", "Industry.transportation");
		industriesMap.put("Default Industry", "UNKNOWN INDUSTRY");

		return industriesMap;
	}

	private Map<String, String> getIndustriesMap() {
		Map<String, String> industries = new HashMap<String, String>();
		industries.put("Default Industry", "UNKNOWN INDUSTRY");
		industries.put("Agriculture", "Industry.agriculture");
		industries.put("Animals & Livestock", "Industry.agriculture.animals");
		industries.put("Crops", "Industry.agriculture.crops");
		industries.put("Agriculture General", "Industry.agriculture.general");
		industries.put("Business Services", "Industry.bizservice");
		industries.put("Accounting & Accounting Services", "Industry.bizservice.accounting");
		industries.put("Auctions", "Industry.bizservice.auction");
		industries.put("Call Centers & Business Centers", "Industry.bizservice.callcenter");
		industries.put("Debt Collection", "Industry.bizservice.collection");
		industries.put("Management Consulting", "Industry.bizservice.consulting");
		industries.put("Information & Document Management", "Industry.bizservice.datamgmt");
		industries.put("Multimedia & Graphic Design", "Industry.bizservice.design");
		industries.put("Food Service", "Industry.bizservice.foodserv");
		industries.put("Business Services General", "Industry.bizservice.general");
		industries.put("Human Resources & Staffing", "Industry.bizservice.hr");
		industries.put("Facilities Management & Commercial Cleaning", "Industry.bizservice.janitor");
		industries.put("Translation & Linguistic Services", "Industry.bizservice.language");
		industries.put("Advertising & Marketing", "Industry.bizservice.marketing");
		industries.put("Commercial Printing", "Industry.bizservice.printing");
		industries.put("Security Products & Services", "Industry.bizservice.security");
		industries.put("Chambers of Commerce", "Industry.chamber");
		industries.put("Construction", "Industry.construction");
		industries.put("Architecture, Engineering & Design", "Industry.construction.architecture");
		industries.put("Commercial & Residential Construction", "Industry.construction.construction");
		industries.put("Construction General", "Industry.construction.general");
		industries.put("Consumer Services", "Industry.consumerservices");
		industries.put("Automotive Service & Collision Repair", "Industry.consumerservices.auto");
		industries.put("Car & Truck Rental", "Industry.consumerservices.carrental");
		industries.put("Funeral Homes & Funeral Related Services", "Industry.consumerservices.funeralhome");
		industries.put("Consumer Services General", "Industry.consumerservices.general");
		industries.put("Hair Salons", "Industry.consumerservices.hairsalon");
		industries.put("Laundry & Dry Cleaning Services", "Industry.consumerservices.laundry");
		industries.put("Photography Studio", "Industry.consumerservices.photo");
		industries.put("Travel Agencies & Services", "Industry.consumerservices.travel");
		industries.put("Veterinary Care", "Industry.consumerservices.veterinary");
		industries.put("Weight & Health Management", "Industry.consumerservices.weight");
		industries.put("Cultural", "Industry.cultural");
		industries.put("Cultural General", "Industry.cultural.general");
		industries.put("Libraries", "Industry.cultural.library");
		industries.put("Museums & Art Galleries", "Industry.cultural.museum");
		industries.put("Education", "Industry.education");
		industries.put("Education General", "Industry.education.general");
		industries.put("K-12 Schools", "Industry.education.k12");
		industries.put("Training", "Industry.education.training");
		industries.put("Colleges & Universities", "Industry.education.university");
		industries.put("Energy, Utilities & Waste Treatment General", "Industry.energy");
		industries.put("Energy, Utilities & Waste Treatment", "Industry.energy");
		industries.put("Electricity, Oil & Gas", "Industry.energy.energy");
		industries.put("Waste Treatment, Environmental Services & Recycling", "Industry.energy.environment");
		industries.put("Energy, Utilities, & Waste Treatment General", "Industry.energy.general");
		industries.put("Oil & Gas Exploration & Services", "Industry.energy.services");
		industries.put("Water & Water Treatment", "Industry.energy.water");
		industries.put("Finance", "Industry.finance");
		industries.put("Banking", "Industry.finance.banking");
		industries.put("Brokerage", "Industry.finance.brokerage");
		industries.put("Credit Cards & Transaction  Processing", "Industry.finance.creditcards");
		industries.put("Finance General", "Industry.finance.general");
		industries.put("Investment Banking", "Industry.finance.investment");
		industries.put("Venture Capital & Private Equity", "Industry.finance.venturecapital");
		industries.put("Government", "Industry.government");
		industries.put("Healthcare", "Industry.healthcare");
		industries.put("Emergency Medical Transportation & Services", "Industry.healthcare.emergency");
		industries.put("Healthcare General", "Industry.healthcare.general");
		industries.put("Hospitals & Clinics", "Industry.healthcare.healthcare");
		industries.put("Medical Testing & Clinical Laboratories", "Industry.healthcare.medicaltesting");
		industries.put("Pharmaceuticals", "Industry.healthcare.pharmaceuticals");
		industries.put("Biotechnology", "Industry.healthcare.pharmaceuticals.biotech");
		industries.put("Drug Manufacturing & Research", "Industry.healthcare.pharmaceuticals.drugs");
		industries.put("Hospitality", "Industry.hospitality");
		industries.put("Hospitality General", "Industry.hospitality.general");
		industries.put("Lodging & Resorts", "Industry.hospitality.lodging");
		industries.put("Recreation", "Industry.hospitality.recreation");
		industries.put("Movie Theaters", "Industry.hospitality.recreation.cinema");
		industries.put("Fitness & Dance Facilities", "Industry.hospitality.recreation.fitness");
		industries.put("Gambling & Gaming", "Industry.hospitality.recreation.gaming");
		industries.put("Amusement Parks, Arcades & Attractions", "Industry.hospitality.recreation.park");
		industries.put("Zoos & National Parks", "Industry.hospitality.recreation.zoo");
		industries.put("Restaurants", "Industry.hospitality.restaurant");
		industries.put("Sports Teams & Leagues", "Industry.hospitality.sports");
		industries.put("Insurance", "Industry.insurance");
		industries.put("Law Firms & Legal Services", "Industry.legal");
		industries.put("Media & Internet", "Industry.media");
		industries.put("Broadcasting", "Industry.media.broadcasting");
		industries.put("Film/Video Production & Services", "Industry.media.broadcasting.film");
		industries.put("Radio Stations", "Industry.media.broadcasting.radio");
		industries.put("Television Stations", "Industry.media.broadcasting.tv");
		industries.put("Media & Internet General", "Industry.media.general");
		industries.put("Information Collection & Delivery", "Industry.media.information");
		industries.put("Search Engines & Internet Portals", "Industry.media.internet");
		industries.put("Music & Music Related Services", "Industry.media.music");
		industries.put("Newspapers & News Services", "Industry.media.news");
		industries.put("Publishing", "Industry.media.publishing");
		industries.put("Manufacturing", "Industry.mfg");
		industries.put("Aerospace & Defense", "Industry.mfg.aerospace");
		industries.put("Boats & Submarines", "Industry.mfg.boat");
		industries.put("Building Materials", "Industry.mfg.building");
		industries.put("Aggregates, Concrete & Cement", "Industry.mfg.building.concrete");
		industries.put("Lumber, Wood Production & Timber Operations", "Industry.mfg.building.lumber");
		industries.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)", "Industry.mfg.building.other");
		industries.put("Plumbing & HVAC Equipment", "Industry.mfg.building.plumbing");
		industries.put("Motor Vehicles", "Industry.mfg.car");
		industries.put("Motor Vehicle Parts", "Industry.mfg.carparts");
		industries.put("Chemicals, Petrochemicals, Glass & Gases", "Industry.mfg.chemicals");
		industries.put("Chemicals", "Industry.mfg.chemicals.chemicals");
		industries.put("Gases", "Industry.mfg.chemicals.gas");
		industries.put("Glass & Clay", "Industry.mfg.chemicals.glass");
		industries.put("Petrochemicals", "Industry.mfg.chemicals.petrochemicals");
		industries.put("Computer Equipment & Peripherals", "Industry.mfg.computers");
		industries.put("Personal Computers & Peripherals", "Industry.mfg.computers.computers");
		industries.put("Computer Networking Equipment", "Industry.mfg.computers.networking");
		industries.put("Network Security Hardware & Software", "Industry.mfg.computers.security");
		industries.put("Computer Storage Equipment", "Industry.mfg.computers.storage");
		industries.put("Consumer Goods", "Industry.mfg.consumer");
		industries.put("Appliances", "Industry.mfg.consumer.appliances");
		industries.put("Cleaning Products", "Industry.mfg.consumer.cleaning");
		industries.put("Textiles & Apparel", "Industry.mfg.consumer.clothes");
		industries.put("Cosmetics, Beauty Supply & Personal Care Products", "Industry.mfg.consumer.cometics");
		industries.put("Consumer Electronics", "Industry.mfg.consumer.electronics");
		industries.put("Health & Nutrition Products", "Industry.mfg.consumer.health");
		industries.put("Household Goods", "Industry.mfg.consumer.household");
		industries.put("Pet Products", "Industry.mfg.consumer.petproducts");
		industries.put("Photographic & Optical Equipment", "Industry.mfg.consumer.photo");
		industries.put("Sporting Goods", "Industry.mfg.consumer.sport");
		industries.put("Hand, Power and Lawn-care Tools", "Industry.mfg.consumer.tools");
		industries.put("Jewelry & Watches", "Industry.mfg.consumer.watch");
		industries.put("Electronics", "Industry.mfg.electronics");
		industries.put("Batteries, Power Storage Equipment & Generators", "Industry.mfg.electronics.batteries");
		industries.put("Electronic Components", "Industry.mfg.electronics.electronics");
		industries.put("Power Conversion & Protection Equipment", "Industry.mfg.electronics.powerequip");
		industries.put("Semiconductor & Semiconductor Equipment", "Industry.mfg.electronics.semiconductors");
		industries.put("Food, Beverages & Tobacco", "Industry.mfg.food");
		industries.put("Food & Beverages", "Industry.mfg.food.food");
		industries.put("Tobacco", "Industry.mfg.food.tobacco");
		industries.put("Wineries & Breweries", "Industry.mfg.food.winery");
		industries.put("Furniture", "Industry.mfg.furniture");
		industries.put("Manufacturing General", "Industry.mfg.general");
		industries.put("Industrial Machinery & Equipment", "Industry.mfg.industrialmachinery");
		industries.put("Medical Devices &  Equipment", "Industry.mfg.medical");
		industries.put("Pulp & Paper", "Industry.mfg.paper");
		industries.put("Plastic, Packaging & Containers", "Industry.mfg.plastic");
		industries.put("Tires & Rubber", "Industry.mfg.rubber");
		industries.put("Telecommunication  Equipment", "Industry.mfg.telecom");
		industries.put("Test & Measurement Equipment", "Industry.mfg.testequipment");
		industries.put("Toys & Games", "Industry.mfg.toys");
		industries.put("Wire & Cable", "Industry.mfg.wire");
		industries.put("Metals & Mining", "Industry.mm");
		industries.put("Metals & Mining General", "Industry.mm.general");
		industries.put("Metals & Minerals", "Industry.mm.metals");
		industries.put("Mining", "Industry.mm.mining");
		industries.put("Cities, Towns & Municipalities", "Industry.municipal");
		industries.put("Cities, Towns & Municipalities General", "Industry.municipal.general");
		industries.put("Public Safety", "Industry.municipal.publicsafety");
		industries.put("Organizations", "Industry.orgs");
		industries.put("Membership Organizations", "Industry.orgs.association");
		industries.put("Charitable Organizations & Foundations", "Industry.orgs.foundation");
		industries.put("Organizations General", "Industry.orgs.general");
		industries.put("Religious Organizations", "Industry.orgs.religion");
		industries.put("Real Estate", "Industry.realestate");
		industries.put("Retail", "Industry.retail");
		industries.put("Motor Vehicle Dealers", "Industry.retail.auto");
		industries.put("Automobile Parts Stores", "Industry.retail.autoparts");
		industries.put("Record, Video & Book Stores", "Industry.retail.book");
		industries.put("Apparel & Accessories Retail", "Industry.retail.clothes");
		industries.put("Gas Stations, Convenience & Liquor Stores", "Industry.retail.conveniencestore");
		industries.put("Department Stores, Shopping Centers & Superstores", "Industry.retail.departmentstore");
		industries.put("Consumer Electronics & Computers Retail", "Industry.retail.electronics");
		industries.put("Furniture", "Industry.retail.furniture");
		industries.put("Retail General", "Industry.retail.general");
		industries.put("Flowers, Gifts & Specialty Stores", "Industry.retail.gifts");
		industries.put("Grocery Retail", "Industry.retail.grocery");
		industries.put("Home Improvement & Hardware Retail", "Industry.retail.hardware");
		industries.put("Vitamins, Supplements, & Health Stores", "Industry.retail.health");
		industries.put("Jewelry & Watch Retail", "Industry.retail.jewelry");
		industries.put("Office Products Retail & Distribution", "Industry.retail.office");
		industries.put("Pet Products", "Industry.retail.pet");
		industries.put("Drug Stores & Pharmacies", "Industry.retail.pharmacy");
		industries.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)",
				"Industry.retail.rental");
		industries.put("Sporting & Recreational Equipment Retail", "Industry.retail.sports");
		industries.put("Toys & Games", "Industry.retail.toys");
		industries.put("Video & DVD Rental", "Industry.retail.videorental");
		industries.put("Software", "Industry.software");
		industries.put("Software & Technical Consulting", "Industry.software.consulting");
		industries.put("Software General", "Industry.software.general");
		industries.put("Software Development & Design", "Industry.software.mfg");
		industries.put("Business Intelligence (BI) Software", "Industry.software.mfg.bi");
		industries.put("Content & Collaboration Software", "Industry.software.mfg.content_col");
		industries.put("Customer Relationship Management (CRM) Software", "Industry.software.mfg.crm");
		industries.put("Database & File Management Software", "Industry.software.mfg.db");
		industries.put("Engineering Software", "Industry.software.mfg.eng");
		industries.put("Enterprise Resource Planning (ERP) Software", "Industry.software.mfg.erp");
		industries.put("Financial, Legal & HR Software", "Industry.software.mfg.finance");
		industries.put("Healthcare Software", "Industry.software.mfg.health");
		industries.put("Multimedia, Games and Graphics Software", "Industry.software.mfg.multimedia");
		industries.put("Networking Software", "Industry.software.mfg.network");
		industries.put("Supply Chain Management (SCM) Software", "Industry.software.mfg.scm");
		industries.put("Security Software", "Industry.software.mfg.security");
		industries.put("Storage & System Management Software", "Industry.software.mfg.storage");
		industries.put("Retail Software", "Industry.software.mfg.retail");
		industries.put("Telecommunications", "Industry.telecom");
		industries.put("Cable & Satellite", "Industry.telecom.cable");
		industries.put("Telecommunications General", "Industry.telecom.general");
		industries.put("Internet Service Providers, Website Hosting & Internet-related Services",
				"Industry.telecom.internet");
		industries.put("Telephony & Wireless", "Industry.telecom.telephone");
		industries.put("Transportation", "Industry.transportation");
		industries.put("Airlines, Airports & Air Services", "Industry.transportation.airline");
		industries.put("Freight & Logistics Services", "Industry.transportation.freight");
		industries.put("Transportation General", "Industry.transportation.general");
		industries.put("Marine Shipping & Transportation", "Industry.transportation.marine");
		industries.put("Trucking, Moving & Storage", "Industry.transportation.moving");
		industries.put("Rail, Bus & Taxi", "Industry.transportation.railandbus");

		return industries;
	}
	
	/////////////
	 private void downloadZoomReport(Date startDateInUTC) throws ParseException,Exception  {
		 	try{
	    
		 				int recordsCounter = 0;
		            	
		            	//if(campaign.getClientName()!=null && !campaign.getClientName().isEmpty()){	
		 				logger.debug("CampaignName : zoomReport" );
		            	//TODO need to get the s3 bucket name of client from the campaign, this should be configured inc ampaign
		            	File dirFile = new File("zoomReport");
	    		        if (!dirFile.exists()) {
	    		            if (dirFile.mkdirs()) {
	    		            	logger.debug("Directory is created!");
	    		            } else {
	    		            	logger.debug("Failed to create directory!");
	    		            }
	    		        }
	    		        
		            
		            	String folderName = "zoomReport";
	               	 
		            	
		            	logger.debug("Processing Data... PLEASE WAIT!!! :)" );
		                List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findSuccessByDate(startDateInUTC);
		                if(prospectCallLogList.size()<=0){
		                	return;
		                }
		                logger.debug("Total records found : " + prospectCallLogList.size() + " for campaign : zoomReport");
		            	String fileName = folderName+SUFFIX+"zoomReport"+".xlsx";
		            	File file = new File(fileName);
		            	Workbook workbook = new XSSFWorkbook();
		            	CreationHelper createHelper = workbook.getCreationHelper();
		            	Sheet sheet = workbook.createSheet("Leads");
		            	
		            	Map<String,String> colNames = new HashMap<String,String>();
		            	int i=0;
		            	String[] columns = new String[200];
		            	 colNames.put("Zoom ID","Zoom ID");
		                 columns[i]= "Zoom ID";
		                 i++;
		                 colNames.put("First Name","First Name");
		                 columns[i]= "First Name";
		                 i++;
		                 colNames.put("Last Name","Last Name");
		                 columns[i]= "Last Name";
		                 i++;
		                 colNames.put("Phone Number","Phone Number");
		                 columns[i]= "Phone Number";
		                 i++;
		                 colNames.put("Email","Email");
		                 columns[i]= "Email";
		                 i++;
		                 colNames.put("Call Status","Call Status");
		                 columns[i]= "Call Status";
		                 i++;
		                
		                 colNames.put("Call Status Explanation","Call Status Explanation");
		                 columns[i]= "Call Status Explanation";
		                 i++;
		                 colNames.put("Call Recording URL","Call Recording URL");
		                 columns[i]= "Call Recording URL";
		                 i++;
		                 colNames.put("Last Updated Date","Last Updated Date");
		                 columns[i]= "Last Updated Date";
		                 i++;
		                 colNames.put("Call Start Date","Call Start Date");
		                 columns[i]= "Call Start Date";
		                 i++;
		                 colNames.put("Title Validation Link","Title Validation Link");
		                 columns[i]= "Title Validation Link";
		                 i++;
		                 colNames.put("Company Validation Link","Company Validation Link");
		                 columns[i]= "Company Validation Link";
		                 /**/                 
		
		            
		        		 //Add a new line separator after the header
		        		// Create a Font for styling header cells
		                Font headerFont = workbook.createFont();
		                headerFont.setBold(true);
		                headerFont.setFontHeightInPoints((short) 14);
		                headerFont.setColor(IndexedColors.RED.getIndex());
		
		                // Create a CellStyle with the font
		                CellStyle headerCellStyle = workbook.createCellStyle();
		                headerCellStyle.setFont(headerFont);
		
		                // Create a Row
		                Row headerRow = sheet.createRow(0);
		                
		                for(int x = 0; x < columns.length; x++) {
		                	if(columns[x] !=null && !columns[x].isEmpty()){
			                    Cell cell = headerRow.createCell(x);
			                    cell.setCellValue(columns[x]);
			                    cell.setCellStyle(headerCellStyle);
		                	}
		                }
		
		                // Create Cell Style for formatting Date
		                CellStyle dateCellStyle = workbook.createCellStyle();
		                dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
			
		                 
		                 
		                 int rowNum = 1;
		                int j=0;
		                
		                
			            for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			            	Row row = sheet.createRow(rowNum++);
			            	ProspectCall prospectCall = prospectCallLog.getProspectCall();
			        		Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			        	   
			        	   for(int col = 0; col < columns.length; col++) {
			                	if(columns[col] !=null && !columns[col].isEmpty()){
			                		
			                		if(columns[col].contentEquals("Zoom ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getSourceId());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("First Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getFirstName());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Last Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getLastName());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Phone Number")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getPhone());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Email")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getEmail());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Call Status")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getStatus().toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Call Status Explanation")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue("");
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Call Recording URL")){
			                			Cell cellx = row.createCell(col);
			                			if(prospectCall.getRecordingUrlAws()!=null){
			                				cellx.setCellValue(prospectCall.getRecordingUrlAws());
			                			}else{
			                				if(prospectCall.getRecordingUrlAws()==null || prospectCall.getRecordingUrlAws().isEmpty()){
			                					if(prospectCall.getRecordingUrl()==null || prospectCall.getRecordingUrl().isEmpty()){
			                						cellx.setCellValue("");
			                					}else{
					         						String rUrl = prospectCall.getRecordingUrl();
					         						int lastIndex = rUrl.lastIndexOf("/");
					         						String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
					         						String rdfile = xfileName + ".wav";
					         						prospectCall.setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
					         						cellx.setCellValue(prospectCall.getRecordingUrlAws());
			                					}
				         						
				         					}
			                				
			                			}
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Last Updated Date")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getUpdatedDate());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Call Start Date")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCall.getCallStartTime());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Title Validation Link")){
			                			Cell cellx = row.createCell(col);
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getQaAnswers()!=null){
			                				List<AgentQaAnswer> aqaList = prospectCallLog.getQaFeedback().getQaAnswers();
			                				boolean foundTitlelink = false;
			                				for(AgentQaAnswer aqa : aqaList){
			                					if(aqa.getQuestion().equalsIgnoreCase("Is Title Valid?")){
			                						cellx.setCellValue(aqa.getAnswerCertification());
			                						foundTitlelink= true;
			                					}
			                				}
			                				if(!foundTitlelink)
			                					cellx.setCellValue("");
			                			}else{
			                				cellx.setCellValue("");
			                			}
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Validation Link")){
			                			Cell cellx = row.createCell(col);
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getQaAnswers()!=null){
			                				List<AgentQaAnswer> aqaList = prospectCallLog.getQaFeedback().getQaAnswers();
			                				boolean foundTitlelink = false;
			                				for(AgentQaAnswer aqa : aqaList){
			                					if(aqa.getQuestion().equalsIgnoreCase("Is Industry valid?")){
			                						cellx.setCellValue(aqa.getAnswerCertification());
			                						foundTitlelink= true;
			                					}
			                				}
			                				if(!foundTitlelink)
			                					cellx.setCellValue("");
			                			}else{
			                				cellx.setCellValue("");
			                			}
					                    cellx.setCellStyle(dateCellStyle);
			                		}
			                	}
			        	   }
				            
			                 recordsCounter++;
			            }
			            
			            FileOutputStream fileOut = new FileOutputStream(fileName);
			            workbook.write(fileOut);
			            fileOut.close();
		
			            workbook.close();
			            logger.debug("CSV file for campaign : zoomReport was created successfully. Records Exported : " + recordsCounter);
			            //SEnd email
			            List<File> fileNames = new ArrayList<File>();
			            fileNames.add(file);
			            String message = "zoomReport Attached Lead file for the previous day";
			            	            	
			            	
			            		XtaasEmailUtils.sendEmail("reports@xtaascorp.com", "data@xtaascorp.com",
			            				"zoomReport Lead Report File Attached.", message,fileNames);
			            	
			            
			            logger.debug("Lead file delivered for zoomReport");

	     				
		            deleteDir(dirFile);
		      
	            
	        } catch (Exception e) {
	        	logger.error("Error in CsvFileWriter !!!");
	            e.printStackTrace();
	        }	            
	    }
	/////////////
	
	
		 
	 
	 //////////
	 ///
	 ///
	 ///
	 /////////
	 
	 public void realTimeByCampaignId(ProspectCallLog prospectCallLog) throws ParseException, Exception {
			Campaign campaign = campaignService.getCampaign(prospectCallLog.getProspectCall().getCampaignId());
			if(campaign==null)
				return;
			AmazonS3 s3client = createAWSConnection(clientRegion);
			Date date = new Date();
			//String dateFolder = sdf.format(date);
			DateFormat pstFolder = new SimpleDateFormat("MM/dd/yyyy");
			pstFolder.setTimeZone(TimeZone.getTimeZone("PST"));
			String dateFolder = pstFolder.format(date);
		
			boolean exists = s3client.doesObjectExist(bucketName, dateFolder);
			if (exists) {
				//DO Nothing
			} else {
				createFolder(bucketName, dateFolder, s3client);
			}
			try {
				sendLeadToClient(campaign,prospectCallLog,dateFolder,s3client);

			} catch (Exception e) {
				logger.error("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} 

		}
	/////////////////////////////////////////////

	private void sendLeadToClient(Campaign campaign,ProspectCallLog prospectCallLog,String dateFolder,AmazonS3 s3client){
		String recordingFileFtpPath = "";
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		RealTimeDelivery realTimeDelivery = realTimeDeliveryRepository.findByXtaasCampaignId(
				prospectCallLog.getProspectCall().getCampaignId());
		//boolean recordingDownloadFlag = false;
		logger.error("CampaignName :" + campaign.getName());
			File dirFile = new File(campaign.getName());
			if (!dirFile.exists()) {
				if (dirFile.mkdirs()) {
					logger.debug("Directory is created!");
				} else {
					logger.debug("Failed to create directory!");
				}
			}

			String folderName = campaign.getName();
			createFolder(bucketName, dateFolder + SUFFIX + folderName, s3client);
			Map<String, String> colNames = new HashMap<String, String>();
			List<NewClientMapping> campaignCMList = newClientMappingRepository
					.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
			Collections.sort(campaignCMList, new NewClientMappingComparator());
			Map<String, String> colNamesMap = new HashMap<String, String>();
			Map<String, NewClientMapping> colNameObjMap = new HashMap<String, NewClientMapping>();
			Map<String, String> singleClientValueMap = new HashMap<String, String>();
			Map<String, Map<String, String>> customQuestionsMap = new HashMap<String, Map<String,String>>();

			int i = 0;
			String[] columns = new String[200];
			for (NewClientMapping ccm : campaignCMList) {
				if (ccm.getClientValues() != null && ccm.getClientValues().size() == 1) {
					if (!singleClientValueMap.containsKey(ccm.getColHeader())) {
						singleClientValueMap.put(ccm.getColHeader(), ccm.getClientValues().get(0));
					}
				}
				if (colNames.containsKey(ccm.getColHeader())) {
					continue;
				} else {
					//System.out.println(ccm.getColHeader());
					colNames.put(ccm.getColHeader(), ccm.getStdAttribute());
					columns[i] = ccm.getColHeader();
					colNamesMap.put(ccm.getColHeader(), ccm.getStdAttribute());
					colNameObjMap.put(ccm.getColHeader(), ccm);
					i++;
				}
			}
			
			Map<String, List<String>> clientMap = new HashMap<String, List<String>>();
			for (NewClientMapping mapping : campaignCMList) {
				clientMap.put(mapping.getStdAttribute(), mapping.getClientValues());
			}
			
			if(prospectCallLog !=null && prospectCallLog.getProspectCall() !=null && prospectCallLog.getProspectCall().getAnswers()!=null) {
				List<AgentQaAnswer> agentAnswers = prospectCallLog.getProspectCall().getAnswers();
				for(AgentQaAnswer aqa : agentAnswers) {
					if(aqa.getQuestion()!=null && !aqa.getQuestion().isEmpty()) {
						Map<String, String> cqmap = customQuestionsMap.get(prospectCallLog.getProspectCall().getCampaignId());
						if(cqmap==null) {
							cqmap = new HashMap<String,String>();
							customQuestionsMap.put(prospectCallLog.getProspectCall().getCampaignId(), cqmap);
						}
						if(!cqmap.containsKey(aqa.getQuestion())) {
							String customQuestionHeader = "CUSTOM_QUESTION_"+ (cqmap.size()+1);
							cqmap.put(aqa.getQuestion(), customQuestionHeader);
							customQuestionsMap.put(prospectCallLog.getProspectCall().getCampaignId(), cqmap);
						}
					}
				}
			}

		
			Map<String, Map<String, NewClientMapping>> clientStdAttribMap = new HashMap<String, Map<String, NewClientMapping>>();
			Map<String, Map<String, List<NewClientMapping>>> titleStdAttribMap = new HashMap<String, Map<String, List<NewClientMapping>>>();

			if (campaignCMList != null && campaignCMList.size() > 0){
				for (NewClientMapping cm : campaignCMList) {
					if (cm.getStdAttribute() != null) {

						if(cm.getStdAttribute()!=null){
	                 		if(cm.getStdAttribute().equalsIgnoreCase("RECORDING_URL_DOWNLOAD")){
	                 			try{
	                 				if(prospectCallLog.getProspectCall().getRecordingUrlAws()==null 
	                 						|| prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()){
	                 					if(prospectCallLog.getProspectCall().getRecordingUrl()==null 
		                 						|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()){
	                 						prospectCallLog.getProspectCall().setRecordingUrlAws(null);
	                 					}else{
			         						String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
			         						int lastIndex = rUrl.lastIndexOf("/");
			         						String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
			         						String rdfile = xfileName + ".wav";
			         						prospectCallLog.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
	                 					}
		         					}
		                       		createFolder(bucketName,dateFolder+SUFFIX+folderName,s3client);
		                       		if(prospectCallLog.getProspectCall().getRecordingUrlAws()!=null){
			        	        		URL url = new URL(prospectCallLog.getProspectCall().getRecordingUrlAws());
			        	        		String campaignName="";
				        				 if(campaign.getClientCampaignName()!=null && !campaign.getClientCampaignName().isEmpty() ){
				        					 campaignName = campaign.getClientCampaignName();
				        				 }else{
				        					 campaignName = campaign.getName();
				        				 }
				        				 String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
		       	         				String rfileName = prospectCallLog.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
			        	         						+prospectCallLog.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
			        	         						+cleanCampaignName+".wav";
			        	         				
		         				File rfile = new File(rfileName);
		         				File rfileTemp = new File(folderName+SUFFIX+rfileName);
		         				String afileName = folderName + SUFFIX + rfile;
		         				
		         						         				
		         				//String path = folderName+SUFFIX+folderName+SUFFIX+rfileName;
		         				 boolean fileDownloaded = s3client.doesObjectExist(bucketName, afileName);
		         				 if(!fileDownloaded){
		         					//fileDownload(url, rfile);
		         					String awsurl = prospectCallLog.getProspectCall().getRecordingUrlAws();
		         					String rFolder = "";
		         					String recordingFileName = "";
		         					if(awsurl!=null){
		         						String[] urlarr = awsurl.split(SUFFIX);
		         						rFolder = urlarr[4];
		         						recordingFileName = urlarr[5];
		         					}
		         					
		         					//File localFile = new File("/home/abc/Desktop/AmazonS3/");
		         					    s3client.getObject(new GetObjectRequest(recordingBucketName, rFolder+SUFFIX+recordingFileName), rfileTemp);
		         						s3client.putObject(new PutObjectRequest(bucketName, afileName, rfileTemp)
		    							.withCannedAcl(CannedAccessControlList.PublicRead));
		         						rfileTemp.delete(); 
		         					 }
			         				
		         				 }
		                       	
	                 			}catch(Exception e){
	                 				e.printStackTrace();
	                 			}
	                 			
	                 			//if(!"".equals(rFolder) && !"".equals(recordingFileName)){
         						//recordingFileFtpPath = rFolder+SUFFIX+recordingFileName;
         						if(realTimeDelivery.getFtpServer()!=null && realTimeDelivery.getFtpPort() > 0
         								&& realTimeDelivery.getFtpUserName()!=null && realTimeDelivery.getFtpPassword()!=null){
         							FTPClient ftpClient = new FTPClient();
         						   try {
         							  ///////
         							  String campaignName="";
				        				 if(campaign.getClientCampaignName()!=null && !campaign.getClientCampaignName().isEmpty() ){
				        					 campaignName = campaign.getClientCampaignName();
				        				 }else{
				        					 campaignName = campaign.getName();
				        				 }
				        				 String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
		       	         				String rfileName = prospectCallLog.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
			        	         						+prospectCallLog.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
			        	         						+cleanCampaignName+".wav";
		       	         			File rfile = new File(rfileName);
			         				File rfileTemp = new File(folderName+SUFFIX+rfileName);
			         				String afileName = folderName + SUFFIX + rfile;
			         				
         							  String awsurl = prospectCallLog.getProspectCall().getRecordingUrlAws();
  		         					String rFolder = "";
  		         					String recordingFileName = "";
  		         					if(awsurl!=null){
  		         						String[] urlarr = awsurl.split(SUFFIX);
  		         						rFolder = urlarr[4];
  		         						recordingFileName = urlarr[5];
  		         					}
  		         					
  		         					//File localFile = new File("/home/abc/Desktop/AmazonS3/");
  		         					    s3client.getObject(new GetObjectRequest(recordingBucketName, rFolder+SUFFIX+recordingFileName), rfileTemp);
         							   ////////
         							   ftpClient.connect(realTimeDelivery.getFtpServer(), realTimeDelivery.getFtpPort());
         					            ftpClient.login(realTimeDelivery.getFtpUserName(), realTimeDelivery.getFtpPassword());
         					            ftpClient.enterLocalPassiveMode();
         					 
         					            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
         					 
         					            // APPROACH #1: uploads first file using an InputStream
         					            File firstLocalFile = new File(afileName);
         					 
         					            String firstRemoteFile = rfileName;
         					            InputStream inputStream = new FileInputStream(firstLocalFile);
         					 
         					            logger.debug("Start uploading first file");
         					           boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
         					            inputStream.close();
         					            if (done) {
         					            	logger.debug("The file is uploaded successfully to FTP.");
         					            }else{
         					            	logger.debug("The file is uploaded FAILED to FTP.");
         					            }
         					           rfileTemp.delete(); 
         						   } catch (IOException ex) {
         					            System.out.println("Error: " + ex.getMessage());
         					            ex.printStackTrace();
         					        } finally {
         					            try {
         					                if (ftpClient.isConnected()) {
         					                    ftpClient.logout();
         					                    ftpClient.disconnect();
         					                }
         					            } catch (IOException ex) {
         					                ex.printStackTrace();
         					            }
         					        }
         						}	
                 		//}
	                 			
	                 		}
	                 		
	                 		if(cm.getStdAttribute().equalsIgnoreCase("ACTUAL_SEARCH_TITLE")){
							Map<String, List<NewClientMapping>> tClientMap = titleStdAttribMap
									.get(cm.getStdAttribute());
							if (tClientMap != null && tClientMap.size() > 0) {
								if (cm.getColHeader() != null && !cm.getColHeader().isEmpty()) {
									List<NewClientMapping> tClientList = tClientMap.get(cm.getColHeader());
									if (tClientList != null && tClientList.size() > 0) {
										tClientList.add(cm);
									} else {
										tClientList = new ArrayList<NewClientMapping>();
										tClientList.add(cm);
									}
									tClientMap.put(cm.getColHeader(), tClientList);
								}

							} else if (cm.getColHeader() != null && !cm.getColHeader().isEmpty()) {
								tClientMap = new HashMap<String, List<NewClientMapping>>();

								List<NewClientMapping> tClientList = new ArrayList<NewClientMapping>();
								tClientList.add(cm);
								tClientMap.put(cm.getColHeader(), tClientList);

							} else {
								tClientMap = new HashMap<String, List<NewClientMapping>>();

								List<NewClientMapping> tClientList = new ArrayList<NewClientMapping>();
								tClientList.add(cm);
								tClientMap.put(cm.getColHeader(), tClientList);
							}
							titleStdAttribMap.put(cm.getStdAttribute(), tClientMap);
						}

						Map<String, NewClientMapping> iClientMap = clientStdAttribMap.get(cm.getStdAttribute());
						if (iClientMap != null && iClientMap.size() > 0) {
							if (cm.getColHeader() != null && !cm.getColHeader().isEmpty())
								iClientMap.put(cm.getColHeader(), cm);
							else
								iClientMap.put(cm.getStdAttribute(), cm);
						} else {
							iClientMap = new HashMap<String, NewClientMapping>();
							if (cm.getColHeader() != null && !cm.getColHeader().isEmpty())
								iClientMap.put(cm.getColHeader(), cm);
							else
								iClientMap.put(cm.getStdAttribute(), cm);
						}
						clientStdAttribMap.put(cm.getStdAttribute(), iClientMap);
					}
				}

				}
			}
			
			////////////////////////////////
			ProspectCall prospectCall = prospectCallLog.getProspectCall();
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			for (int col = 0; col < columns.length; col++) {
				
				String val = downloadClientMappingReportServiceImpl.getColumnValue(campaign, prospectCallLog,columns[col],
						colNames,
						clientMap, singleClientValueMap,customQuestionsMap);
				if(val!=null && !val.isEmpty()) {
					NewClientMapping ncm = colNameObjMap.get(columns[col]);
					
					if(!ncm.isInternalUse()) {
						nameValuePairs.add(new BasicNameValuePair(columns[col], val));
					}
				}
				
					
			}
			
			
			
			String postUrl = realTimeDelivery.getPostUrl();
			
			nameValuePairs.add(new BasicNameValuePair(realTimeDelivery.getAuthCodeKey(), realTimeDelivery.getAuthCodeValue()));
			nameValuePairs.add(new BasicNameValuePair("success_email", realTimeDelivery.getSuccessMailId()));
			nameValuePairs.add(new BasicNameValuePair("error_email", realTimeDelivery.getErrorMailId()));
			if (realTimeDelivery.getTestMode() != null && !realTimeDelivery.getTestMode().equals("") 
					&&  realTimeDelivery.getTestMode().equalsIgnoreCase("true")) {
				nameValuePairs.add(new BasicNameValuePair("is_test_mode", realTimeDelivery.getTestMode()));
			}
			
			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(postUrl);
				logger.debug("Real time post lead URL is: [{}] and Form Data Enitty is: {}", postUrl,
						nameValuePairs.toString());
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpClient.execute(httpPost);
				logger.debug("Real time post lead Response is: {} and Enitty is: {}", response,
						response.getEntity().toString());
				ProspectCallLog callLog = prospectCallLogRepository
						.findByProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
				callLog.getProspectCall().setClientDelivered("YES");
				prospectCallLogService.save(callLog, false);
				httpClient.close();
			} catch (Exception e) {
				logger.error("Error while sending Real time lead for campaign Id: [{}] and exception is {}",
						prospectCall.getCampaignId(), e);
				e.printStackTrace();
			}


		
		// SEnd email
		String message = "FirstName:"+prospectCall.getProspect().getFirstName()+"\n"+
				"lastName:"+prospectCall.getProspect().getLastName()+"\n"+
				"company:"+prospectCall.getProspect().getCompany()+"\n"+
				"title:"+prospectCall.getProspect().getTitle()+"\n"+
				" was Uploaded to portal successfully.";
		
		
		logger.debug("Recording File uploade");

		deleteDir(dirFile);
			
	}
	
	@Override
	 public void downloadConnectReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO,HttpServletRequest request, HttpServletResponse response,Login login) throws ParseException,Exception  {
		 AmazonS3 s3client = createAWSConnection(clientRegion);
		 try{
		 		//NOT_SCORED, QA_ACCEPTED, QA_REJECTED, QA_RETURNED, ACCEPTED, REJECTED, CLIENT_ACCEPTED, CLIENT_REJECTED
		 		Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
		 		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		 		List<CampaignDTO> campaignsFromDB = campaignRepository.findByIdsWithProjectedFields(prospectCallSearchDTO.getCampaignIds(), Arrays.asList());
		 		Map<String, String> campaignMap = new HashMap<String, String>();
		 		if (campaignsFromDB != null && campaignsFromDB.size() > 0) {
		 			for (CampaignDTO camp : campaignsFromDB) {
						campaignMap.put(camp.getId(), camp.getName());
					}
		 		}
		 		///////////// For Failure
		 		ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
		 		ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
		 		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		 		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
		 			LocalDate todaydate = LocalDate.now();
		 			LocalDate fromDate = todaydate.minusDays(7);
		 			prospectCallSearchDTO.setFromDate(fromDate.toString());
		 			prospectCallSearchDTO.setToDate(todaydate.toString());
		 			prospectCallSearchDTO.setStartMinute(0);
		 			prospectCallSearchDTO.setEndMinute(0);
		 		}
		 		if(prospectCallSearchDTO.getLeadStatus()!=null && prospectCallSearchDTO.getLeadStatus().size()>0){
		 			List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
		 			List<String> successLeadStatus = new ArrayList<String>();
		 			List<String> failureLeadStatus = new ArrayList<String>();
		 			for(String lStatus : leadStatusList){
		 				if(lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
		 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
		 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
		 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())){
		 					successLeadStatus.add(lStatus);
		 				}else{
		 					failureLeadStatus.add(lStatus);
		 				}
		 			}
		 			if(successLeadStatus.size()>0){
		 				List<String> dispositionList = new ArrayList<String>(1);
		 				dispositionList.add("SUCCESS");
		 				successPCSDTO = prospectCallSearchDTO;
		 				successPCSDTO.setLeadStatus(successLeadStatus);
		 				successPCSDTO.setDisposition(dispositionList);
		 				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl.searchProspectCallByReport(successPCSDTO);
		 				prospectCallLogList.addAll(prospectCallLogSuccess);
		 			}
		 			if(failureLeadStatus.size()>0){
		 				List<String> dispositionList = new ArrayList<String>(1);
		 				dispositionList.add("FAILURE");
		 				failurePCSDTO = prospectCallSearchDTO;
		 				failurePCSDTO.setLeadStatus(null);
		 				failurePCSDTO.setDisposition(dispositionList);
		 				failurePCSDTO.setSubStatus(failureLeadStatus);
		 				List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl.searchProspectCallByReport(failurePCSDTO);
		 				prospectCallLogList.addAll(prospectCallLogFailure);
		 			}
		 		}else{
		 			prospectCallLogList = prospectCallLogRepositoryImpl.searchProspectCallByReport(prospectCallSearchDTO);
		 		}
		 				int recordsCounter = 0;
		 				logger.info("CampaignName :"+campaign.getName());
		            	File dirFile = new File(campaign.getName());
	    		        if (!dirFile.exists()) {
	    		            if (dirFile.mkdirs()) {
	    		            	logger.info("Directory is created!");
	    		            } else {
	    		            	logger.info("Failed to create directory!");
	    		            }
	    		        }
	    		        String folderName = campaign.getName();
	               	 	logger.info("Processing Data... PLEASE WAIT!!! :)" );
		                if(prospectCallLogList.size()<=0){
		                	return;
		                }
		            	String fileName = folderName+SUFFIX+campaign.getName()+".xlsx";
		            	File file = new File(fileName);
		            	Workbook workbook = new XSSFWorkbook();
		            	CreationHelper createHelper = workbook.getCreationHelper();
		            	Sheet sheet = workbook.createSheet("Leads");
		            	Map<String,String> colNames = new HashMap<String,String>();
		            	int i=0;
		            	String[] columns = new String[200];
		            	 colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
						 columns[i] = "ProspectCallId (for internal use only)";
						 i++;
						 colNames.put("Campaign Name", "Campaign Name");
						 columns[i] = "Campaign Name";
						 i++;
		                 colNames.put("First Name","First Name");
		                 columns[i]= "First Name";
		                 i++;
		                 colNames.put("Last Name","Last Name");
		                 columns[i]= "Last Name";
		                 i++;
		                 colNames.put("Job Title","Job Title");
		                 columns[i]= "Job Title";
		                 i++;
		                 colNames.put("Email ID","Email ID");
		                 columns[i]= "Email ID";
		                 i++;
		                 colNames.put("Phone No.","Phone No.");
		                 columns[i]= "Phone No.";
		                 i++;
		                 colNames.put("Company Name","Company Name");
		                 columns[i]= "Company Name";
		                 i++;
		                 colNames.put("Address Line 1","Address Line 1");
		                 columns[i]= "Address Line 1";
		                 i++;
		                 colNames.put("Address Line 2","Address Line 2");
		                 columns[i]= "Address Line 2";
		                 i++;
		                 colNames.put("City","City");
		                 columns[i]= "City";
		                 i++;
		                 colNames.put("State","State");
		                 columns[i]= "State";
		                 i++;
		                 colNames.put("Postal Code","Postal Code");
		                 columns[i]= "Postal Code";
		                 i++;
		                 colNames.put("Country","Country");
		                 columns[i]= "Country";
		                 i++;
		                 colNames.put("Company Employee Size Min","Company Employee Size Min");
		                 columns[i]= "Company Employee Size Min";
		                 i++;
		                 colNames.put("Company Employee Size Max","Company Employee Size Max");
		                 columns[i]= "Company Employee Size Max";
		                 i++;
		                 colNames.put("Company Revenue Min","Company Revenue Min");
		                 columns[i]= "Company Revenue Min";
		                 i++;
		                 colNames.put("Company Revenue Max","Company Revenue Max");
		                 columns[i]= "Company Revenue Max";
		                 i++;
		                 colNames.put("Industry Type","Industry Type");
		                 columns[i]= "Industry Type";

		                 List<String> questionKeyList = new ArrayList<String>();
		                 HashMap<String,ArrayList<PickListItem>> questionsList = campaign.getQualificationCriteria().getQuestions();
		                 for (Map.Entry<String,ArrayList<PickListItem>> entry : questionsList.entrySet())  {
		                 	questionKeyList.add(entry.getKey());
		                 	i++;
			                colNames.put(entry.getKey(),entry.getKey());
			                columns[i]= entry.getKey();
		                 }
		                 i++;
		                 colNames.put("Asset title","Asset title");
		                 columns[i]= "Asset title";
		                 i++;
		                 colNames.put("Download Date","Download Date");
		                 columns[i]= "Download Date";
		                 i++;
		                 colNames.put("Lead Status","Lead Status");
		                 columns[i]= "Lead Status";
		                 i++;
		                 colNames.put("QA ID","QA ID");
		                 columns[i]= "QA ID";
		                 i++;
		                 colNames.put("QA Status","QA Status");
		                 columns[i]= "QA Status";
		                 i++;
		                 colNames.put("C1. Lead Valid","C1. Lead Valid");
		                 columns[i]= "C1. Lead Valid";
		                 i++;
		                 colNames.put("Reject Reason","Reject Reason");
		                 columns[i]= "Reject Reason";
		                 i++;
		                 colNames.put("C1. Notes","C1. Notes");
		                 columns[i]= "C1. Notes";
		                 i++;
		                 colNames.put("Email Status","Email Status");
		                 columns[i]= "Email Status";
		                 i++;
		                 colNames.put("Em Line 1","Address Line 1");
		                 columns[i]= "Address Line 1";
		                 i++;
		                 colNames.put("Client Delivered","Client Delivered");
		                 columns[i]= "Client Delivered";
		                 i++;
		                 colNames.put("A1. Call Rcdg","A1. Call Rcdg");
		                 columns[i]= "A1. Call Rcdg";
		                 i++;
		                 colNames.put("Partner ID","Partner ID");
		                 columns[i]= "Partner ID";
		                 i++;
		                 colNames.put("Agent ID","Agent ID");
		                 columns[i]= "Agent ID";
		                 //////////
		                 i++;
		                 colNames.put("A2. Branding","A2. Branding");
		                 columns[i]= "A2. Branding";
		                 i++;
		                 colNames.put("A3. Pitch","A3. Pitch");
		                 columns[i]= "A3. Pitch";
		                 i++;
		                 colNames.put("B1. Interest","B1. Interest");
		                 columns[i]= "B1. Interest";
		                 i++;
		                 colNames.put("B2. Qual Ques","B2. Qual Ques");
		                 columns[i]= "B2. Qual Ques";
		                 i++;
		                 colNames.put("B3. Contact Dtls","B3. Contact Dtls");
		                 columns[i]= "B3. Contact Dtls";
		                 i++;
		                 colNames.put("B4. Direct Number","B4. Direct Number");
		                 columns[i]= "B4. Direct Number";
		                 i++;
		                 colNames.put("B5. Email Addy","B5. Email Addy");
		                 columns[i]= "B5. Email Addy";
		                 i++;
		                 colNames.put("B6. Consent","B6. Consent");
		                 columns[i]= "B6. Consent";
		                 //////////
		                 i++;
		                 colNames.put("Zoom Company Link","Zoom Company Link");
		                 columns[i]= "Zoom Company Link";
		                 i++;
		                 colNames.put("Zoom Employee Link","Zoom Employee Link");
		                 columns[i]= "Zoom Employee Link";
		                 i++;
						 colNames.put("Comments", "Comments");
						 columns[i] = "Comments";
						 i++;
						 colNames.put("Action", "Action");
						 columns[i] = "Action";
		                 
		                 colNames.put("Agent Notes","Agent Notes");
		                 columns[i]= "Agent Notes";
		                 List<String> qaQuestionKeyList = new ArrayList<String>();
		                 HashMap<String,ArrayList<PickListItem>> qaQuestionsList = campaign.getQualificationCriteria().getQuestions();
		                 for (Map.Entry<String,ArrayList<PickListItem>> entry : qaQuestionsList.entrySet())  {
		                	 String qaQuest = "QA-"+entry.getKey();
		                	 qaQuestionKeyList.add(qaQuest);
		                 	i++;
			                colNames.put(qaQuest,qaQuest);
			                columns[i]= qaQuest;
		                 }
		                 i++;
						 colNames.put("Action", "Action");
						 columns[i] = "Action";
		        		 //Add a new line separator after the header
		        		// Create a Font for styling header cells
		                Font headerFont = workbook.createFont();
		                headerFont.setBold(true);
		                headerFont.setFontHeightInPoints((short) 14);
		                headerFont.setColor(IndexedColors.RED.getIndex());
		                // Create a CellStyle with the font
		                CellStyle headerCellStyle = workbook.createCellStyle();
		                headerCellStyle.setFont(headerFont);
		                // Create a Row
		                Row headerRow = sheet.createRow(0);
		                for(int x = 0; x < columns.length; x++) {
		                	if(columns[x] !=null && !columns[x].isEmpty()){
			                    Cell cell = headerRow.createCell(x);
			                    cell.setCellValue(columns[x]);
			                    cell.setCellStyle(headerCellStyle);
		                	}
		                }
		                // Create Cell Style for formatting Date
		                CellStyle dateCellStyle = workbook.createCellStyle();
		                dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));int rowNum = 1;
		                int j=0;
			            for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			            	Row row = sheet.createRow(rowNum++);
			            	ProspectCall prospectCall = prospectCallLog.getProspectCall();
			        		Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			        	    String campaignName = campaignMap.get(prospectCall.getCampaignId());
			        	   for(int col = 0; col < columns.length; col++) {
			                	if(columns[col] !=null && !columns[col].isEmpty()){
			                		if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
										Cell cellx = row.createCell(col);
										if (prospectCall != null && prospectCall.getProspectCallId() != null) {
											cellx.setCellValue(prospectCall.getProspectCallId());
											cellx.setCellStyle(dateCellStyle);
										}
									} else if(columns[col].contentEquals("Campaign Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(campaignName);
					                    cellx.setCellStyle(dateCellStyle);
			                		} else if(columns[col].contentEquals("First Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getFirstName());
					                    cellx.setCellStyle(dateCellStyle);
			                		} else if(columns[col].contentEquals("Last Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getLastName());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Job Title")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getTitle());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Email ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getEmail());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Phone No.")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getPhone());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCompany());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Address Line 1")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getAddressLine1());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Address Line 2")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getAddressLine2());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("City")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCity());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("State")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getStateCode());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Postal Code")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getZipCode());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Country")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCountry());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Employee Size Min")){
			                			Cell cellx = row.createCell(col);
			                			if (prospect.getCustomAttributeValue("minEmployeeCount") != null) {
			                				cellx.setCellValue(prospect.getCustomAttributeValue("minEmployeeCount").toString());
						                    cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("Company Employee Size Max")){
			                			Cell cellx = row.createCell(col);
			                			if (prospect.getCustomAttributeValue("maxEmployeeCount") != null) {
			                				cellx.setCellValue(prospect.getCustomAttributeValue("maxEmployeeCount").toString());
					                    	cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("Company Revenue Min")){
			                			Cell cellx = row.createCell(col);
			                			if (prospect.getCustomAttributeValue("minRevenue") != null) {
			                				cellx.setCellValue(prospect.getCustomAttributeValue("minRevenue").toString());
					                    	cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("Company Revenue Max")){
			                			Cell cellx = row.createCell(col);
			                			if (prospect.getCustomAttributeValue("maxRevenue") != null) {
			                				cellx.setCellValue(prospect.getCustomAttributeValue("maxRevenue").toString());
					                    	cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("Industry Type")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getIndustry());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Asset title")){
			                			Cell cellx = row.createCell(col);
			                			String assetName = "";
			                			if(prospectCall.getDeliveredAssetId()!=null && !prospectCall.getDeliveredAssetId().isEmpty()){
				                			//if(campaign.getActiveAsset()!=null && campaign.getActiveAsset().getName()!=null && !campaign.getActiveAsset().getName().isEmpty()){
				                				assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
				                		}
					                    cellx.setCellValue(assetName);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Download Date")){
			                			Cell cellx = row.createCell(col);
			                			DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
			                			pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
			                			String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
			                			Date pstDate = pstFormat.parse(pstStr);
			                			
			                			logger.info(pstFormat.format(pstDate));
					                    cellx.setCellValue(pstFormat.format(pstDate));
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Lead Status")){
			                			Cell cellx = row.createCell(col);
			                			if (prospectCallLog.getProspectCall().getLeadStatus() != null) {
			                				cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus().toString());
						                    cellx.setCellStyle(dateCellStyle);
			                			}else{
			                				cellx.setCellValue(prospectCallLog.getProspectCall().getSubStatus().toString());
			                				 cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("QA ID")){
			                			if(prospectCallLog.getQaFeedback()!=null && 
			                					prospectCallLog.getQaFeedback().getQaId()!=null) {
			                				Cell cellx = row.createCell(col);
				                			cellx.setCellValue(prospectCallLog.getQaFeedback().getQaId().toString());
						                    cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("QA Status")){
			                			if (prospectCallLog.getStatus() != null) {
			                				Cell cellx = row.createCell(col);
				                			cellx.setCellValue(prospectCallLog.getStatus().toString());
						                    cellx.setCellStyle(dateCellStyle);
			                			}
			                		}else if(columns[col].contentEquals("C1. Lead Valid")){
			                			String valid="";
			                			if(prospectCallLog.getQaFeedback()!=null && 
			                					prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								valid = fra.getFeedback();
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(valid);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Reject Reason")){
			                			String qaReason = "";
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								if(fra.getRejectionReason()!=null){
			                									qaReason = qaReason+fra.getRejectionReason();
			                								}
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(qaReason);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("C1. Notes")){
			                			String callnotes = "";
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								if(fra.getAttributeComment()!=null){
			                									callnotes = fra.getAttributeComment();
			                								}
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(callnotes);
					                    cellx.setCellStyle(dateCellStyle);
			                		} else if (columns[col].contentEquals("Email Status")) {
			    						String isMailBounced = "";
			    						if (prospectCall.isEmailBounce()) {
			    							isMailBounced = "BOUNCED";
			    						} else {
			    							isMailBounced = "NOT-BOUNCED";
			    						}
			    						Cell cellx = row.createCell(col);
			    						cellx.setCellValue(isMailBounced);
			    						cellx.setCellStyle(dateCellStyle);
			    					} else if (columns[col].contentEquals("Client Delivered")) {
										Cell cellx = row.createCell(col);
										if(prospectCallLog.getProspectCall().getClientDelivered() != null && !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
											cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
										else
											cellx.setCellValue("");
										cellx.setCellStyle(dateCellStyle);
									}else if(columns[col].contentEquals("A1. Call Rcdg")){
			                			Cell cellx = row.createCell(col);
			                			if(prospectCallLog.getProspectCall().getRecordingUrlAws()==null || prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()){
			                				if(prospectCallLog.getProspectCall().getRecordingUrl()==null || prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()){
			                					 cellx.setCellValue("");
			                				}else{
				         						String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
				         						int lastIndex = rUrl.lastIndexOf("/");
				         						String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
				         						String rdfile = xfileName + ".wav";
				         						prospectCallLog.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
				         						 cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
			                				}
			         					} else {
			         						cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
			         					}
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Partner ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Agent ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Zoom Company link")){
			                			Cell cellx = row.createCell(col);
			                			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
												&& prospectCallLog.getProspectCall().getProspect() != null) {
											cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomCompanyUrl());
										} else {
											cellx.setCellValue("");
										}
										cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Zoom Employee Link")){
			                			Cell cellx = row.createCell(col);
										if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
												&& prospectCallLog.getProspectCall().getProspect() != null) {
											cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomPersonUrl());
										} else {
											cellx.setCellValue("");
										}
										cellx.setCellStyle(dateCellStyle);
			                		} else if(columns[col].contentEquals("Agent Notes")){
			                			Cell cellx = row.createCell(col);
			                			StringBuffer sb = new StringBuffer();
			                			if(prospectCallLog.getProspectCall().getNotes()!=null 
			                					&& prospectCallLog.getProspectCall().getNotes().size()>0){
			                				List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
			                				for(Note note : notesArr){
			                					sb.append(note.getAgentId()+" : "+note.getText());
			                				}
			                			}
					                    cellx.setCellValue("");
					                    cellx.setCellStyle(dateCellStyle);
			                		}
			                		else{
			                			for(String question :questionKeyList){
			                				String qaQuest = "QA-"+question;
				                			if(columns[col].contentEquals(question) && prospectCall.getAnswers()!=null){    		
				                		    	for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
				                		   			if (agentQaAnswer.getQuestion()!=null 
				                		   					&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())){
				                		   				String agentAns = agentQaAnswer.getAnswer();
				                		   				Cell cellx = row.createCell(col);
									                    cellx.setCellValue(agentAns);
									                    cellx.setCellStyle(dateCellStyle);
									                    break;
				                		   			}
				                		       	 }
				                			}else if(columns[col].contentEquals(qaQuest) && prospectCallLog.getQaFeedback()!=null 
				                					&& prospectCallLog.getQaFeedback().getQaAnswers()!=null){
				                				for (AgentQaAnswer agentQaAnswer : prospectCallLog.getQaFeedback().getQaAnswers()) {
				                		   			if (agentQaAnswer.getQuestion()!=null 
				                		   					&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())){
				                		   				String answerCertification = agentQaAnswer.getAnswerCertification();
				                		   				Cell cellx = row.createCell(col);
									                    cellx.setCellValue(answerCertification);
									                    cellx.setCellStyle(dateCellStyle);
									                    break;
				                		   			}
				                		       	 }
				                			}
				                		}
			                		}
			                	}
			        	   }
			                 recordsCounter++;
			            }
			            FileOutputStream fileOut = new FileOutputStream(fileName);
			            workbook.write(fileOut);
			            fileOut.close();
			            logger.info("CSV file for campaign : "+campaign.getName()+" was created successfully. Records Exported : " + recordsCounter);
			            sendEmailWithLeadFile(campaign, prospectCallSearchDTO, file, s3client,organization,login);
			            deleteDir(dirFile);
	        } catch (Exception e) {
	        	logger.info("Error in CsvFileWriter !!!");
	            e.printStackTrace();
	        }	            
	    }
	 
	 
	private void sendEmailWithLeadFile(Campaign campaign, ProspectCallSearchDTO prospectCallSearchDTO, File file,
			AmazonS3 s3client,Organization organization,Login login) {
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		String message = campaign.getName() + " - Attached lead report file of the previous day.";
		boolean leadReportSent = false;
		String subject;
		if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() == 1
				&& prospectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
			subject = " - DNC Report File Attached.";
		} else if (prospectCallSearchDTO.getSubStatus() != null && prospectCallSearchDTO.getSubStatus().size() > 1
				&& prospectCallSearchDTO.getSubStatus().contains("DNCL")) {
			subject = " - Failure Report File Attached.";
		} else {
			subject = " - Lead Report File Attached.";
		}
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

		//Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (loginUser.getReportEmails()!= null && loginUser.getReportEmails().size() > 0) {
			List<String> emails = loginUser.getReportEmails();
			if (emails != null && emails.size() > 0) {
				for (String email : emails) {
					if (email != null && !email.isEmpty()) {
						XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", campaign.getName() + subject,
								message, fileNames);
						logger.info("Lead report for the campaign [{}] sent  successfully to [{}] ", campaign.getName(),
								email);
						leadReportSent = true;
					}
				}
			}
		}else {
			XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", campaign.getName() + subject,
					message, fileNames);
			logger.info("Lead report for the campaign [{}] sent  successfully to [{}] ", campaign.getName(),
					login.getEmail());
			leadReportSent = true;
		}
		try {
			String leadfileName = "LEADREPORTS" + SUFFIX + file;
			s3client.putObject(new PutObjectRequest(bucketName, leadfileName, file)
					.withCannedAcl(CannedAccessControlList.PublicRead));
			logger.info("Lead report file uploaded successfully to s3 storage.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (!leadReportSent) {
			XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com",
					campaign.getName() + subject, message, fileNames);
			logger.info("Lead report for the campaign [{}] sent  successfully to leadreports@xtaascorp.com",
					campaign.getName());
		}

	}

	 private void processDownloadQA(Campaign campaign,ProspectCallSearchDTO prospectCallSearchDTO,/*Date startDateInUTC,Date recordingDateInUTC,String dateFolder,*/
				AmazonS3 s3client,Login login){
			try{
				ProspectCallSearchDTO tempProspectCallSearchDTO = prospectCallSearchDTO;
				//boolean recordingDownloadFlag= false;
				int recordsCounter=0;
				List<ClientMapping> campaignCMList = clientMappingRepository
						.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
				
				if(campaignCMList==null || campaignCMList.size()==0){
					return;
				}

				if (campaign.getClientName() != null && !campaign.getClientName().isEmpty()) {
					logger.debug("CampaignName :" + campaign.getName());
					// TODO need to get the s3 bucket name of client from the campaign, this should
					// be configured inc ampaign
					File dirFile = new File(campaign.getName());
					if (!dirFile.exists()) {
						if (dirFile.mkdirs()) {
							logger.debug("Directory is created!");
						} else {
							logger.debug("Failed to create directory!");
						}
					}
		///////////// For Failure
			 		ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
			 		ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
			 		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
			 		if(campaign.getCampaignGroupIds()!=null && campaign.getCampaignGroupIds().size()>0){
		 				prospectCallSearchDTO.setCampaignIds(campaign.getCampaignGroupIds());
		 			}
			 		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			 			LocalDate todaydate = LocalDate.now();
			 			LocalDate fromDate = todaydate.minusDays(7);
			 			prospectCallSearchDTO.setFromDate(fromDate.toString());
			 			prospectCallSearchDTO.setToDate(todaydate.toString());
			 			prospectCallSearchDTO.setStartMinute(0);
			 			prospectCallSearchDTO.setEndMinute(0);
			 		}
			 		if(prospectCallSearchDTO.getLeadStatus()!=null && prospectCallSearchDTO.getLeadStatus().size()>0){
			 			
			 			List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
			 			List<String> successLeadStatus = new ArrayList<String>();
			 			List<String> failureLeadStatus = new ArrayList<String>();
			 			for(String lStatus : leadStatusList){
			 				if(lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
			 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
			 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
			 					|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString()) || lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())){
			 					successLeadStatus.add(lStatus);
			 				}else{
			 					failureLeadStatus.add(lStatus);
			 				}
			 			}
			 			if(successLeadStatus.size()>0){
			 				List<String> dispositionList = new ArrayList<String>(1);
			 				dispositionList.add("SUCCESS");
			 				successPCSDTO = prospectCallSearchDTO;
			 				successPCSDTO.setLeadStatus(successLeadStatus);
			 				successPCSDTO.setDisposition(dispositionList);
			 				List<ProspectCallLog> prospectCallLogSuccess = prospectCallLogRepositoryImpl.searchProspectCallByReport(successPCSDTO);
			 				prospectCallLogList.addAll(prospectCallLogSuccess);
			 			}
			 			if(failureLeadStatus.size()>0){
			 				List<String> dispositionList = new ArrayList<String>(1);
			 				dispositionList.add("FAILURE");
			 				failurePCSDTO = prospectCallSearchDTO;
			 				failurePCSDTO.setLeadStatus(null);
			 				failurePCSDTO.setDisposition(dispositionList);
			 				failurePCSDTO.setSubStatus(failureLeadStatus);
			 				List<ProspectCallLog> prospectCallLogFailure = prospectCallLogRepositoryImpl.searchProspectCallByReport(failurePCSDTO);
			 				prospectCallLogList.addAll(prospectCallLogFailure);
			 			}
			 		}else{
			 			prospectCallLogList = prospectCallLogRepositoryImpl.searchProspectCallByReport(prospectCallSearchDTO);
			 		}
			 		
			 		
			 		
			 		
			 		//////////////// End Failure
			
			 		boolean dncFlag = false;
					String folderName = campaign.getName();
					//createFolder(bucketName, dateFolder + SUFFIX + folderName, s3client);
					logger.debug("Processing Data... PLEASE WAIT!!! :)");
					//List<ProspectCallLog> prospectCallLogList = prospectCallLogRepositoryImpl.searchProspectCallByReport(prospectCallSearchDTO);
					logger.debug("Total records found : " + prospectCallLogList.size() + " for campaign : "
							+ campaign.getName());
					String campName = campaign.getName();
					if(campaign.getCampaignGroupIds()!=null && campaign.getCampaignGroupIds().size()>0)
						campName = campaign.getName()+"_GROUP";
					if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1 && tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
		            	campName = campName + "_DNC";
		            	dncFlag = true;
		            } else if (prospectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1 && tempProspectCallSearchDTO.getSubStatus().contains("DNCL") ) {
		            	campName = campName + "_Failure";
					} else {
						campName = campName + "_Lead";
					}
					String fileName = folderName + SUFFIX + campName + ".xlsx";
					File file = new File(fileName);
					Workbook workbook = new XSSFWorkbook();
					CreationHelper createHelper = workbook.getCreationHelper();
					Sheet sheet = workbook.createSheet("Leads");

					Map<String, String> colNames = new HashMap<String, String>();

					
					/*List<ClientMapping> campaignCMList = clientMappingRepository
							.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());*/

				
					Collections.sort(campaignCMList, new ClientMappingComparator());

					int i = 0;
					String[] columns = new String[200];
					for (ClientMapping ccm : campaignCMList) {
						if (colNames.containsKey(ccm.getColHeader())) {
							continue;
						} else {
							//System.out.println(ccm.getColHeader());
							colNames.put(ccm.getColHeader(), ccm.getStdAttribute());
							columns[i] = ccm.getColHeader();
							i++;
						}
					}

					/*colNames.put("LeadStatus", "LeadStatus");
					columns[i] = "LeadStatus";*/
					/*i++;
					colNames.put("Reason", "Reason");
					columns[i] = "Reason";
					i++;
					colNames.put("Notes", "Notes");
					columns[i] = "Notes";
					i++;*/
					colNames.put("ProspectCallId (for internal use only)", "ProspectCallId (for internal use only)");
					columns[i] = "ProspectCallId (for internal use only)";
					i++;
					colNames.put("DownloadDate", "DownloadDate");
					columns[i] = "DownloadDate";
					i++;
					if (dncFlag) {
						colNames.put("Call Start Time", "Call Start Time");
						columns[i] = "Call Start Time";
						i++;
					}
					colNames.put("AssetName", "AssetName");
					columns[i] = "AssetName";
					i++;
					colNames.put("DeliveryDate", "DeliveryDate");
					columns[i] = "DeliveryDate";
					i++;
					colNames.put("PartnerId", "PartnerId");
					columns[i] = "PartnerId";
					i++;
					colNames.put("AgentId", "AgentId");
					columns[i] = "AgentId";
					i++;
					colNames.put("RecordingUrl", "RecordingUrl");
					columns[i] = "RecordingUrl";
					/*i++;
					colNames.put("QAStatus", "QAStatus");
					columns[i] = "QAStatus";
					i++;
					colNames.put("C1.ValidLead", "C1.ValidLead");
					columns[i] = "C1.ValidLead";*/
					i++;
					colNames.put("LeadStatus", "LeadStatus");
					columns[i] = "LeadStatus";
					i++;
					colNames.put("C1.Reason", "C1.Reason");
					columns[i] = "C1.Reason";
					i++;
					colNames.put("C1.CallNotes", "C1.CallNotes");
					columns[i] = "C1.CallNotes";
					i++;
					colNames.put("Client Delivered", "Client Delivered");
					columns[i] = "Client Delivered";
					i++;
					colNames.put("CompanyZoomURL", "CompanyZoomURL");
					columns[i] = "CompanyZoomURL";
					i++;
					colNames.put("PersonZoomURL", "PersonZoomURL");
					columns[i] = "PersonZoomURL";
					i++;
	                 colNames.put("Agent Notes","Agent Notes");
	                 columns[i]= "Agent Notes";
	                 List<String> qaQuestionKeyList = new ArrayList<String>();
	                 HashMap<String,ArrayList<PickListItem>> qaQuestionsList = campaign.getQualificationCriteria().getQuestions();
	                 for (Map.Entry<String,ArrayList<PickListItem>> entry : qaQuestionsList.entrySet())  {
	                	 String qaQuest = "QA-"+entry.getKey();
	                	 qaQuestionKeyList.add(qaQuest);
	                 	i++;
		                colNames.put(qaQuest,qaQuest);
		                columns[i]= qaQuest;
	                 }
	                 i++;
	         		colNames.put("Action", "Action");
	         		columns[i] = "Action";
					

					// Add a new line separator after the header
					// Create a Font for styling header cells
					Font headerFont = workbook.createFont();
					headerFont.setBold(true);
					headerFont.setFontHeightInPoints((short) 14);
					headerFont.setColor(IndexedColors.RED.getIndex());

					// Create a CellStyle with the font
					CellStyle headerCellStyle = workbook.createCellStyle();
					headerCellStyle.setFont(headerFont);

					// Create a Row
					Row headerRow = sheet.createRow(0);

					for (int x = 0; x < columns.length; x++) {
						if (columns[x] != null && !columns[x].isEmpty()) {
							Cell cell = headerRow.createCell(x);
							cell.setCellValue(columns[x]);
							cell.setCellStyle(headerCellStyle);
						}
					}

					// Create Cell Style for formatting Date
					CellStyle dateCellStyle = workbook.createCellStyle();
					dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));

					Map<String, Map<String, ClientMapping>> clientStdAttribMap = new HashMap<String, Map<String, ClientMapping>>();
					Map<String, Map<String, List<ClientMapping>>> titleStdAttribMap = new HashMap<String, Map<String, List<ClientMapping>>>();

					if (campaignCMList != null && campaignCMList.size() > 0)
						for (ClientMapping cm : campaignCMList) {

							if (cm.getStdAttribute() != null) {
								
								if(cm.getStdAttribute().equalsIgnoreCase("prospect.recordingUrlDownload")){
									try{
				               		//createFolder(bucketName,dateFolder+SUFFIX+folderName,s3client);
				               		List<ProspectCallLog> recordingCallLogList = prospectCallLogList;
					        			 for(ProspectCallLog pcl : recordingCallLogList){
					        				 if(pcl.getProspectCall().getRecordingUrlAws()==null || pcl.getProspectCall().getRecordingUrlAws().isEmpty()){
					        					 if(pcl.getProspectCall().getRecordingUrl()==null || pcl.getProspectCall().getRecordingUrl().isEmpty()){
					        						 System.out.println(pcl.getProspectCall().getProspectCallId());
					        						 pcl.getProspectCall().setRecordingUrlAws("");
					        					 }else{
					         						String rUrl = pcl.getProspectCall().getRecordingUrl();
					         						int lastIndex = rUrl.lastIndexOf("/");
					         						String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
					         						String rdfile = xfileName + ".wav";
					         						pcl.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
					        					 }
					         					}	
					        				 if(pcl.getProspectCall().getRecordingUrlAws()!=null 
					        						 && !pcl.getProspectCall().getRecordingUrlAws().isEmpty()) {
						        				 URL url = new URL(pcl.getProspectCall().getRecordingUrlAws());
						        				 String campaignName="";
						        				 if(campaign.getClientCampaignName()!=null && !campaign.getClientCampaignName().isEmpty() ){
						        					 campaignName = campaign.getClientCampaignName();
						        				 }else{
						        					 campaignName = campaign.getName();
						        				 }
						        				 String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
						        				
						         				String rfileName = pcl.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
						         						+pcl.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
						         						+cleanCampaignName+".wav";
						         				
/////////////////////////////////////////////////////
				         				File rfile = new File(rfileName);
				         				File rfileTemp = new File(folderName+SUFFIX+rfileName);
				         				String afileName = folderName + SUFFIX + rfile;
				         							         				
				         				//String path = folderName+SUFFIX+folderName+SUFFIX+rfileName;
				         				
				         				 boolean fileDownloaded = s3client.doesObjectExist(bucketName, afileName);
				         			    //System.out.println(fileDownloaded);
				         				 if(!fileDownloaded){
				         					//fileDownload(url, rfile);
				         					String awsurl = pcl.getProspectCall().getRecordingUrlAws();
				         					String rFolder = "";
				         					String recordingFileName = "";
				         					if(awsurl!=null){
				         						String[] urlarr = awsurl.split(SUFFIX);
				         						rFolder = urlarr[4];
				         						recordingFileName = urlarr[5];
				         					}
				         					
				         					//File localFile = new File("/home/abc/Desktop/AmazonS3/");
				         					 //
				         					if(!"".equals(rFolder) && !"".equals(recordingFileName)){
				         						 s3client.getObject(new GetObjectRequest(recordingBucketName, rFolder+SUFFIX+recordingFileName), rfileTemp);
				         						s3client.putObject(new PutObjectRequest(bucketName, afileName, rfileTemp)
				    							.withCannedAcl(CannedAccessControlList.PublicRead));
				         						rfileTemp.delete(); 
				         					 }
					         				
					    					
				         				 }
				         				/////////////////////////////////////////////////////
					        				 }
					        			 }
									}catch(Exception e){
										e.printStackTrace();
									}
					        			// recordingDownloadFlag = true;
		                 		}

								if (cm.getStdAttribute().equalsIgnoreCase("prospect.title") || cm.getStdAttribute().equalsIgnoreCase("prospect.jobFunction")) {
									Map<String, List<ClientMapping>> tClientMap = titleStdAttribMap
											.get(cm.getStdAttribute());
									if (tClientMap != null && tClientMap.size() > 0) {
										if (cm.getKey() != null && !cm.getKey().isEmpty()) {
											List<ClientMapping> tClientList = tClientMap.get(cm.getKey());
											if (tClientList != null && tClientList.size() > 0) {
												tClientList.add(cm);
											} else {
												tClientList = new ArrayList<ClientMapping>();
												tClientList.add(cm);
											}
											tClientMap.put(cm.getKey(), tClientList);
										}

									} else if (cm.getKey() != null && !cm.getKey().isEmpty()) {
										tClientMap = new HashMap<String, List<ClientMapping>>();

										List<ClientMapping> tClientList = new ArrayList<ClientMapping>();
										tClientList.add(cm);
										tClientMap.put(cm.getKey(), tClientList);

									} else {
										tClientMap = new HashMap<String, List<ClientMapping>>();

										List<ClientMapping> tClientList = new ArrayList<ClientMapping>();
										tClientList.add(cm);
										tClientMap.put(cm.getKey(), tClientList);
									}
									titleStdAttribMap.put(cm.getStdAttribute(), tClientMap);
								}

								Map<String, ClientMapping> iClientMap = clientStdAttribMap.get(cm.getStdAttribute());
								if (iClientMap != null && iClientMap.size() > 0) {
									if (cm.getKey() != null && !cm.getKey().isEmpty())
										iClientMap.put(cm.getKey(), cm);
									else
										iClientMap.put(cm.getStdAttribute(), cm);
								} else {
									iClientMap = new HashMap<String, ClientMapping>();
									if (cm.getKey() != null && !cm.getKey().isEmpty())
										iClientMap.put(cm.getKey(), cm);
									else
										iClientMap.put(cm.getStdAttribute(), cm);
								}
								clientStdAttribMap.put(cm.getStdAttribute(), iClientMap);
							}
						}
					int rowNum = 1;
					int j = 0;

					for (ProspectCallLog prospectCallLog : prospectCallLogList) {
						Row row = sheet.createRow(rowNum++);
						ProspectCall prospectCall = prospectCallLog.getProspectCall();
						// Prospect prospect = prospectCallLog.getProspectCall().getProspect();

						for (int col = 0; col < columns.length; col++) {
							if (columns[col] != null && !columns[col].isEmpty()) {
								if (columns[col].contentEquals("Status")) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								} /*else if (columns[col].contentEquals("Reason")) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("Notes")) {
									Cell cellx = row.createCell(col);
									
									cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								}*/ else if (columns[col].contentEquals("ProspectCallId (for internal use only)")) {
									Cell cellx = row.createCell(col);
									if (prospectCall != null && prospectCall.getProspectCallId() != null) {
										cellx.setCellValue(prospectCall.getProspectCallId());
										cellx.setCellStyle(dateCellStyle);
									}
								} else if (columns[col].contentEquals("DownloadDate")) {
									Cell cellx = row.createCell(col);
									
									DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
									pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
									String pstStr = pstFormat
											.format(prospectCallLog.getProspectCall().getCallStartTime());
									Date pstDate = pstFormat.parse(pstStr);

									//System.out.println(pstFormat.format(pstDate));
									cellx.setCellValue(pstFormat.format(pstDate));
									cellx.setCellStyle(dateCellStyle);
								} else if (dncFlag && columns[col].contentEquals("Call Start Time")) {
									Cell cellx = row.createCell(col);
									DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
									pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
									if (prospectCallLog.getProspectCall().getCallStartTime() != null) {
										String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
										Date pstDate = pstFormat.parse(pstStr);
										cellx.setCellValue(pstFormat.format(pstDate));
										cellx.setCellStyle(dateCellStyle);
									}
								} else if (columns[col].contentEquals("AssetName")) {
									Cell cellx = row.createCell(col);
									String assetName = "";
									if(prospectCall.getDeliveredAssetId()!=null && !prospectCall.getDeliveredAssetId().isEmpty()){
			                			//if(campaign.getActiveAsset()!=null && campaign.getActiveAsset().getName()!=null && !campaign.getActiveAsset().getName().isEmpty()){
			                				assetName = getAssetNameFromId(campaign, prospectCall.getDeliveredAssetId());
			                		}
									cellx.setCellValue(assetName);
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("DeliveryDate")) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("PartnerId")) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("AgentId")) {
									Cell cellx = row.createCell(col);
									cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("RecordingUrl")) {
									Cell cellx = row.createCell(col);
									if(prospectCallLog.getProspectCall().getRecordingUrlAws()==null 
											|| prospectCallLog.getProspectCall().getRecordingUrlAws().isEmpty()){
										if(prospectCallLog.getProspectCall().getRecordingUrl()==null 
												|| prospectCallLog.getProspectCall().getRecordingUrl().isEmpty()){
											cellx.setCellValue("");
										}else{
		         						String rUrl = prospectCallLog.getProspectCall().getRecordingUrl();
		         						int lastIndex = rUrl.lastIndexOf("/");
		         						String xfileName = rUrl.substring(lastIndex + 1, rUrl.length());
		         						String rdfile = xfileName + ".wav";
		         						prospectCallLog.getProspectCall().setRecordingUrlAws(XtaasConstants.CALL_RECORDING_AWS_URL + rdfile);
		         						cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
										}
		         					}else{
		         						cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrlAws());
		         					}
									
									cellx.setCellStyle(dateCellStyle);
								} /* else if (columns[col].contentEquals("QAStatus")) {
									if (prospectCallLog.getStatus() != null) {
										Cell cellx = row.createCell(col);
										cellx.setCellValue(prospectCallLog.getStatus().toString());
										cellx.setCellStyle(dateCellStyle);
									}
								} else if (columns[col].contentEquals("C1.ValidLead")) {
									String valid = "";
									if (prospectCallLog.getQaFeedback() != null
											&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
										List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
												.getFeedbackResponseList();
										for (FeedbackSectionResponse fsr : fsrList) {
											if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
												List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
												for (FeedbackResponseAttribute fra : fraList) {
													if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
														valid = fra.getFeedback();
													}
												}
											}
										}
									}
									Cell cellx = row.createCell(col);
									cellx.setCellValue(valid);
									cellx.setCellStyle(dateCellStyle);
								}*/ else if (columns[col].contentEquals("C1.Reason")) {
									String qaReason = "";
									if (prospectCallLog.getQaFeedback() != null
											&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
										List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
												.getFeedbackResponseList();
										for (FeedbackSectionResponse fsr : fsrList) {
											if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
												List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
												for (FeedbackResponseAttribute fra : fraList) {
													if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
														if (fra.getRejectionReason() != null) {
															qaReason = qaReason + fra.getRejectionReason();
														}
													}
												}
											}
										}
									}
									Cell cellx = row.createCell(col);
									cellx.setCellValue(qaReason);
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("C1.CallNotes")) {
									String callnotes = "";
									if (prospectCallLog.getQaFeedback() != null
											&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
										List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback()
												.getFeedbackResponseList();
										for (FeedbackSectionResponse fsr : fsrList) {
											if (fsr.getSectionName().equalsIgnoreCase("Lead Validation")) {
												List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
												for (FeedbackResponseAttribute fra : fraList) {
													if (fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")) {
														if (fra.getAttributeComment() != null) {
															callnotes = fra.getAttributeComment();
														}
													}
												}
											}
										}
									}
									Cell cellx = row.createCell(col);
									cellx.setCellValue(callnotes);
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("Client Delivered")) {
									Cell cellx = row.createCell(col);
									if(prospectCallLog.getProspectCall().getClientDelivered() != null && !prospectCallLog.getProspectCall().getClientDelivered().isEmpty())
										cellx.setCellValue(prospectCallLog.getProspectCall().getClientDelivered());
									else
										cellx.setCellValue("");
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("CompanyZoomURL")) {
									Cell cellx = row.createCell(col);
		                			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
											&& prospectCallLog.getProspectCall().getProspect() != null) {
										cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomCompanyUrl());
									} else {
										cellx.setCellValue("");
									}
									cellx.setCellStyle(dateCellStyle);
								} else if (columns[col].contentEquals("PersonZoomURL")) {
									Cell cellx = row.createCell(col);
		                			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
											&& prospectCallLog.getProspectCall().getProspect() != null) {
										cellx.setCellValue(prospectCallLog.getProspectCall().getProspect().getZoomPersonUrl());
									} else {
										cellx.setCellValue("");
									}
									cellx.setCellStyle(dateCellStyle);
								} else if(columns[col].contentEquals("LeadStatus")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(prospectCallLog.getProspectCall().getLeadStatus());
				                    cellx.setCellStyle(dateCellStyle);
		                		} else if(columns[col].contentEquals("Agent Notes")){
		                			Cell cellx = row.createCell(col);
		                			StringBuffer sb = new StringBuffer();
		                			if(prospectCallLog.getProspectCall().getNotes()!=null 
		                					&& prospectCallLog.getProspectCall().getNotes().size()>0){
		                				List<Note> notesArr = prospectCallLog.getProspectCall().getNotes();
		                				for(Note note : notesArr){
		                					sb.append(note.getAgentId()+" : "+note.getText());
		                				}
		                			}
				                    cellx.setCellValue("");
				                    cellx.setCellStyle(dateCellStyle);
		                		}else {
									String val = getColumnValue(campaign, clientStdAttribMap, titleStdAttribMap,
											prospectCallLog, columns[col], colNames);
									// System.out.println(text);
									row.createCell(col).setCellValue(val);
								}
							}
						}

						recordsCounter++;
					}

					/////////////////
			         // Write the output to a file
		            FileOutputStream fileOut = new FileOutputStream(fileName);
		            workbook.write(fileOut);
		            fileOut.close();
	
		            // Closing the workbook
		            // workbook.close();
		           // logger.debug("CSV file for campaign : "+campaign.getName()+" was created successfully. Records Exported : " + recordsCounter);
		            //SEnd email
		            workbook.close();
		            logger.debug("CSV file for campaign : "+campaign.getName()+" was created successfully. Records Exported : " + recordsCounter);
		            //SEnd email
		            List<File> fileNames = new ArrayList<File>();
		            fileNames.add(file);
		            String message = campaign.getName()+" Attached Lead file for the previous day";
		            boolean leadReportSent = false;
		            String subject;
		            if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() == 1 && tempProspectCallSearchDTO.getSubStatus().get(0).equalsIgnoreCase("DNCL")) {
		            	subject = "DNC Report File Attached.";
		            } else if (tempProspectCallSearchDTO.getSubStatus() != null && tempProspectCallSearchDTO.getSubStatus().size() > 1 && tempProspectCallSearchDTO.getSubStatus().contains("DNCL") ) {
		            	subject = "Failure Report File Attached.";
					} else {
						subject = "Lead Report File Attached.";
					}
		            Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		            if(organization.getContacts() !=null && organization.getContacts().size()>0){
		            	List<Contact> contacts = organization.getContacts();
		            	for(Contact contact : contacts){
		            		if(contact.getType().equalsIgnoreCase(PRIMARY_CONTACT) && contact.getEmail()!=null 
		            				&& !contact.getEmail().isEmpty()){
		            			XtaasEmailUtils.sendEmail(contact.getEmail(), "data@xtaascorp.com",
		            				campaign.getName()+ subject, message,fileNames);
		            			leadReportSent = true;
		            		}
		            		
		            	}
		            	if(!leadReportSent){
		            		XtaasEmailUtils.sendEmail("leadreports@xtaascorp.com", "data@xtaascorp.com",
		            				campaign.getName()+subject, message,fileNames);
		            	}
		            }
		            logger.debug("Lead file delivered for "+campaign.getName());
		            try {
		            	String leadfileName = "LEADREPORTS" + SUFFIX + file;
		            	s3client.putObject(new PutObjectRequest(bucketName, leadfileName, file)
		            			.withCannedAcl(CannedAccessControlList.PublicRead));
		            	logger.debug("Recording File uploaded successfully to AWS.");
					} catch(Exception ex) {
		            	ex.printStackTrace();
		            }
					
					


					deleteDir(dirFile);
				}
			
				
			}catch(Exception e){
				logger.debug("processDownloadQA() : Error occured while download lead report file.");
				List<String> emailList = new ArrayList<>();
				emailList.add(login.getEmail());
				emailList.add("data@xtaascorp.com");
				StringWriter sw = new StringWriter();
	            e.printStackTrace(new PrintWriter(sw));
	            String exceptionAsString = sw.toString();
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append("Follwing error occured.");
				stringBuilder.append("<br />");
				stringBuilder.append("<br />");
				stringBuilder.append(exceptionAsString);
				stringBuilder.append("<br />");
				stringBuilder.append("<br />");
				stringBuilder.append("Please contact your administrator.");
				XtaasEmailUtils.sendEmail(emailList, "data@xtaascorp.com", "Exception occured during lead report download", stringBuilder.toString());
				logger.debug("Download lead report email sent successfully to : " + login.getEmail() + ", data@xtaascorp.com" );
				e.printStackTrace();
			}
		}


		private String getAssetNameFromId(Campaign campaign, String assetId){
			String assetName = "";
			
			List<Asset> assets = campaign.getAssets();
			if(assets!=null && assets.size()>0){
				for(Asset asset : assets){
					if(asset.getAssetId().equalsIgnoreCase(assetId)){
						assetName = asset.getName();
						break;
					}
				}
			}
			
			return assetName;
		}

	@Override
	public String updateLeadDetails(String campaignId, MultipartFile file) throws IOException {
		String errorMsg = "";
		if (!file.isEmpty()) {
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
				Workbook book;
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(fileName + " file is empty");
						throw new IllegalArgumentException("File cannot be empty.");
					}
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords(campaignId);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					// return "Problem in reading the file " + fileName;
					errorMsg = e.getMessage();
				} catch (Exception e) {
					e.printStackTrace();
					errorMsg = e.getMessage();
				}
			}
			if (errorMsg != null && !errorMsg.isEmpty()) {
				return errorMsg;
			} else {
				return "Uploading in progress";
			}
		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);// get first sheet in file
		int colCounts = row.getPhysicalNumberOfCells();
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell == null || cell.getCellType() == CellType.BLANK) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
				return false;
			}
			if (cell.getCellType() != CellType.STRING) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
				return false;
			} else {
				headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
				fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
			}
		}
		return true;
	}

	private void validateExcelFileRecords(String campaignId) {
		int rowsCount = sheet.getPhysicalNumberOfRows();
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processRecords(0, rowsCount, colCounts, campaignId);
	}

	public void processRecords(int startPos, int EndPos, int colCounts, String campaignId) {
		List<LeadReportUploadDTO> leadReportDTOs = new ArrayList<LeadReportUploadDTO>();
		List<String> errors = new ArrayList<>();
		for (int i = startPos; i < EndPos; i++) {
			LeadReportUploadDTO leadReportDTO = new LeadReportUploadDTO();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			if (row == null || i == 0) {
				continue;
			}

			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				continue;
			}
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("Lead Status") || columnName.equalsIgnoreCase("LeadStatus")) {
					if (cell != null) {
						String leadStatus = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if(XtaasConstants.LEAD_STATUS.ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.ACCEPTED.toString());
						if(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString());
						if(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString());
						if(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
						if(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString());
						if(XtaasConstants.LEAD_STATUS.REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.REJECTED.toString());
						if(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString());
						if(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
					}
				} else if (columnName.equalsIgnoreCase("C1.Reason") || columnName.equalsIgnoreCase("Reason") 
						|| columnName.equalsIgnoreCase("Reject Reason")) {
					if (cell != null) {
						String rejectReason = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (leadReportDTO.getLeadStatus() != null) {
							if (leadReportDTO.getLeadStatus().equalsIgnoreCase("Rejected")
									|| leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_REJECTED")
									|| leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_RETURNED")) {
								if (rejectReason.equalsIgnoreCase("") || rejectReason.equalsIgnoreCase(null)
										|| rejectReason.isEmpty()) {
									throw new IllegalArgumentException(
											"Reject reason is mandatory as lead status is rejected. Row No:- "
													+ (row.getRowNum() + 1));
								} else {
									if (!setQaRejectReason(leadReportDTO, rejectReason)) {
										throw new IllegalArgumentException(
												"Reject reason is not specified as per qarejectreason standard list. Row No:- "
														+ (row.getRowNum() + 1));
									}else {
										leadReportDTO.setRejectReason(rejectReason);
									}
								}
							}
						} else {
							throw new IllegalArgumentException(
									"Lead status can't be empty for row: " + (row.getRowNum() + 1));
						}
					}
				} else if (columnName.equalsIgnoreCase("Notes") || columnName.equalsIgnoreCase("C1.Notes") || columnName.equalsIgnoreCase("C1. Notes") 
						|| columnName.equalsIgnoreCase("C1.CallNotes")) {
					if (cell != null) {
						String notes = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setNotes(notes);
					}
				} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("EmailAddress")
						|| columnName.equalsIgnoreCase("Email Address") || columnName.equalsIgnoreCase("Email ID")) {
					if (cell != null) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setEmail(email);
					}
				} else if (columnName.equalsIgnoreCase("Client Delivered")) {
					if (cell != null) {
						String clientDelivered = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setClientDelivered(clientDelivered);
					}
				} else if (columnName.equalsIgnoreCase("ProspectCallId (for internal use only)")) {
					if (cell != null) {
						String prospectCallId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setProspectCallId(prospectCallId);
						leadReportDTO.setRowNumber(row.getRowNum() + 1);
					}
				} else if (columnName.equalsIgnoreCase("QaId")) {
					if (cell != null) {
						String qaId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setQaId(qaId);
					}
				} else if (columnName.equalsIgnoreCase("Action")) {
					if (cell != null) {
						String action = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						leadReportDTO.setAction(action);
						leadReportDTOs.add(leadReportDTO);
					}
				}
			}
			
		}
		for (LeadReportUploadDTO leadFileDTO : leadReportDTOs) {
			if (leadFileDTO.getAction() != null && leadFileDTO.getAction().equalsIgnoreCase("update")) {
				updateCollection("ProspectCallLog", leadFileDTO, campaignId, errors);
				updateCollection("ProspectCallLogInteraction", leadFileDTO, campaignId, errors);
			}
		}
		Campaign campaign = campaignService.getCampaign(campaignId);
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		if (!errors.isEmpty() && errors.size() != 0) {
			StringBuilder stringBuilder = new StringBuilder();
			for (String error : errors) {
				stringBuilder.append(error);
				stringBuilder.append("<br />");
			}
//			Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
//			Campaign campaign = campaignService.getCampaign(campaignId);
//			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());

			List<String> emailList = new ArrayList<>();
			if (userLogin.getReportEmails() != null && userLogin.getReportEmails().size() > 0) {
				emailList.addAll(userLogin.getReportEmails());
			}else if(userLogin != null && userLogin.getEmail() != null) {
				emailList.add(userLogin.getEmail());
			} else {
				emailList.add("leadreports@xtaascorp.com");
			}
			String subject = "Lead Upload Error - " + campaign.getName();
			for (String email : emailList) {
				if (email != null && !email.isEmpty()) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, stringBuilder.toString());
					logger.debug("Lead report errorlist mail sent successfully to : " + email);
				}
			}
			throw new IllegalArgumentException("File not uploaded.Please check your email for errors.");
		}else{
			String subject = "Lead Upload Status For - " + campaign.getName();
			if (userLogin.getReportEmails() != null && userLogin.getReportEmails().size() > 0) {
				for(String email : userLogin.getReportEmails()) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",subject, "File Uploaded Successfully");
				}
			} else {
				XtaasEmailUtils.sendEmail(userLogin.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
			}
			PusherUtils.pushMessageToUser(userLogin.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
			String notice = "Lead Report File Uploaded Successfully ";
			teamNoticeSerive.saveUserNotices(userLogin.getId(), notice);
		}
	}

	private void updateCollection(String collectionName, LeadReportUploadDTO leadReportUploadDTO, String campaignId, List<String> errors) {
		if (collectionName.equals("ProspectCallLog")) {
			saveProspectCallLog(leadReportUploadDTO, campaignId, errors);
		} else {
			saveProspectCallInteraction(leadReportUploadDTO, campaignId, errors);
		}
	}

	private boolean setQaRejectReason(LeadReportUploadDTO leadReportDTO, String rejectReason) {
		List<QaRejectReason> rejectReasonsFromDB = qaRejectReasonRepository.findAll();
		QaRejectReason qaRejectReason = rejectReasonsFromDB.get(0);
		List<KeyValuePair<String, String>> rejectReasonValues = qaRejectReason.getQaRejectReason();
		for (KeyValuePair<String, String> keyValuePair : rejectReasonValues) {
			if (rejectReason.equalsIgnoreCase(keyValuePair.getValue())) {
				leadReportDTO.setRejectReason(rejectReason);
				return true;
			}
		}
		return false;
	}

	private void saveProspectCallLog(LeadReportUploadDTO leadReportUploadDTO, String campaignId, List<String> errors) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		ProspectCallLog prospectFromDB = null;
		List<ProspectCallLog> prospects = new ArrayList<ProspectCallLog>();
		if (campaignId == null || campaignId == "" || campaignId.isEmpty()) {
			throw new IllegalArgumentException("CampaignId is mandatory for the updating records.");
		}
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		prospects = prospectCallLogRepository.findAllByProspectCallId(leadReportUploadDTO.getProspectCallId(),pageable);
		if (prospects != null && prospects.size() > 0) {
			prospectFromDB = prospects.get(0);
		}
		if (prospectFromDB != null) {
			if (prospectFromDB.getProspectCall().getClientDelivered() == null
					|| prospectFromDB.getProspectCall().getClientDelivered().isEmpty())
				System.out.println(prospectFromDB.getProspectCall().getProspectCallId());
			if (prospectFromDB.getQaFeedback() != null) {
				List<FeedbackSectionResponse> feedbackSectionList = prospectFromDB.getQaFeedback()
						.getFeedbackResponseList();
				if (feedbackSectionList.size() > 0) {
					Optional<FeedbackSectionResponse> rejectReasonFeedback = feedbackSectionList.stream().filter(
							leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
							.findAny();

					List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
							.getResponseAttributes();
					if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setFeedback("NO");
							rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());

						}
					} else if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_ACCEPTED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())) {
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setFeedback("YES");
							rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());

						}
					} else if (leadReportUploadDTO.getLeadStatus()
							.equalsIgnoreCase(LEAD_STATUS.QA_RETURNED.toString())) {
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());
						}
					}
				}
				
			}
			
			if (prospectFromDB.getQaFeedback() == null) {
				List<FeedbackSectionResponse> feedbackResponseList = new ArrayList<FeedbackSectionResponse>();
				List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
				QaFeedback qf = new QaFeedback(leadReportUploadDTO.getQaId(), null, feedbackResponseList, 0.0, null,
						qaAnswers, "");
				prospectFromDB.setQaFeedback(qf);
				List<FeedbackSectionResponse> feedbackSectionList = prospectFromDB.getQaFeedback()
						.getFeedbackResponseList();
				List<FeedbackResponseAttribute> rejectReasonAttribute = new ArrayList<FeedbackResponseAttribute>();
				FeedbackResponseAttribute fra = new FeedbackResponseAttribute();
				if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_ACCEPTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())) {
					fra.setFeedback("YES");
				} else if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_RETURNED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())) {
					fra.setFeedback("NO");
				}
				fra.setRejectionReason(leadReportUploadDTO.getRejectReason());
				fra.setAttributeComment(leadReportUploadDTO.getNotes());
				fra.setRejectionReason(leadReportUploadDTO.getRejectReason());
				rejectReasonAttribute.add(fra);
				FeedbackSectionResponse fsr = new FeedbackSectionResponse("Lead Validation", rejectReasonAttribute);
				List<FeedbackSectionResponse> fsrList = new ArrayList<FeedbackSectionResponse>();
				fsrList.add(fsr);
				prospectFromDB.getQaFeedback().setFeedbackResponseList(fsrList);
			}

				if (leadReportUploadDTO.getClientDelivered() != null
						&& leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("YES")) {
					prospectFromDB.getProspectCall().setClientDelivered("YES");
				} else if (leadReportUploadDTO.getClientDelivered() != null
						&& leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("NO")) {
					prospectFromDB.getProspectCall().setClientDelivered("NO");
				} else if (leadReportUploadDTO.getClientDelivered() != null
						&& leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("IN HAND")) {
					prospectFromDB.getProspectCall().setClientDelivered("IN HAND");
				}

				if (leadReportUploadDTO.getEmailStatus() != null && !leadReportUploadDTO.getEmailStatus().isEmpty()) {
					if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("NOT-BOUNCED"))
						prospectFromDB.getProspectCall().setEmailBounce(false);
					else if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("BOUNCED"))
						prospectFromDB.getProspectCall().setEmailBounce(true);
				}
				prospectFromDB.getProspectCall().setLeadStatus(leadReportUploadDTO.getLeadStatus());
				
				/**
				 * DATE :- 06/01/2020 
				 * When leadstatus is ACCEPTED or REJECTED then prospect status in DB should be stamped as QA_COMPLETE.
				 */
				if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
					prospectFromDB.setStatus(ProspectCallStatus.QA_COMPLETE);
				}
				prospectCallLogRepository.save(prospectFromDB);
				logger.debug("saveProspectCallLog(): Updated prospectCallLog having prospectCallId: [{}]",
						prospectFromDB.getProspectCall().getProspectCallId());
			} else {
			errors.add("No lead found having email: " + leadReportUploadDTO.getEmail());
			logger.debug("saveProspectCallLog(): No prospect found having email: [{}]", leadReportUploadDTO.getEmail());
		}
	}

	private void saveProspectCallInteraction(LeadReportUploadDTO leadReportUploadDTO, String campaignId, List<String> errors) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		List<ProspectCallInteraction> prospectInteractionFromDB = new ArrayList<ProspectCallInteraction>();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		prospectInteractionFromDB = prospectCallInteractionRepository.findAllByProspectCallId(leadReportUploadDTO.getProspectCallId(),pageable);
		if (prospectInteractionFromDB != null && prospectInteractionFromDB.size() > 0) {
			if (prospectInteractionFromDB.get(0).getQaFeedback() != null) {
				List<FeedbackSectionResponse> feedbackSectionList = prospectInteractionFromDB.get(0).getQaFeedback()
						.getFeedbackResponseList();
				if (feedbackSectionList.size() > 0) {
					Optional<FeedbackSectionResponse> rejectReasonFeedback = feedbackSectionList.stream().filter(
							leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
							.findAny();

					List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
							.getResponseAttributes();
					if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_REJECTED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setFeedback("NO");
							rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());

						}
					} else if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.QA_ACCEPTED.toString())
							|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())) {
						if (rejectReasonAttribute.size() > 0) {
							rejectReasonAttribute.get(0).setFeedback("YES");
							rejectReasonAttribute.get(0).setAttributeComment(leadReportUploadDTO.getNotes());
							rejectReasonAttribute.get(0).setRejectionReason(leadReportUploadDTO.getRejectReason());

						}
					}
				}
				if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("YES")) {
					prospectInteractionFromDB.get(0).getProspectCall().setClientDelivered("YES");
				} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("NO")) {
					prospectInteractionFromDB.get(0).getProspectCall().setClientDelivered("NO");
				} else if (leadReportUploadDTO.getClientDelivered().equalsIgnoreCase("IN HAND")) {
					prospectInteractionFromDB.get(0).getProspectCall().setClientDelivered("IN HAND");
				}
				if (leadReportUploadDTO.getEmailStatus() != null && !leadReportUploadDTO.getEmailStatus().isEmpty()) {
					if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("NOT-BOUNCED"))
						prospectInteractionFromDB.get(0).getProspectCall().setEmailBounce(false);
					else if (leadReportUploadDTO.getEmailStatus().equalsIgnoreCase("BOUNCED"))
						prospectInteractionFromDB.get(0).getProspectCall().setEmailBounce(true);
				}
				prospectInteractionFromDB.get(0).getProspectCall().setLeadStatus(leadReportUploadDTO.getLeadStatus());
				/**
				 * DATE :- 06/01/2020 
				 * When leadstatus is ACCEPTED or REJECTED then prospect status in DB should be stamped as QA_COMPLETE.
				 */
				if (leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.ACCEPTED.toString())
						|| leadReportUploadDTO.getLeadStatus().equalsIgnoreCase(LEAD_STATUS.REJECTED.toString())) {
					prospectInteractionFromDB.get(0).setStatus(ProspectCallStatus.QA_COMPLETE);
				}
				prospectCallInteractionRepository.saveAll(prospectInteractionFromDB);
				logger.debug("saveProspectCallInteraction(): Updated prospectCallInteraction having id: [{}]",
						prospectInteractionFromDB.get(0).getId());
			} else {
//				errors.add("Qafeedback not found so prospect interaction not updated having prospectCallId"
//						+ prospectInteractionFromDB.getProspectCall().getProspectCallId());
				logger.debug(
						"saveProspectCallLog(): Qafeedback not found so prospect interaction not updated having prospectCallId: [{}] ",
						prospectInteractionFromDB.get(0).getProspectCall().getProspectCallId());
			}

		} else {
//			errors.add("No prospectinteraction found having email: [{}] " + leadReportUploadDTO.getEmail());
			logger.debug("saveProspectCallInteraction(): No prospectinteraction found having email: [{}] ",
					leadReportUploadDTO.getEmail());
		}
	}

	public class LeadReportUploadDTO {
		public String leadStatus;
		public String rejectReason;
		public String notes;
		public String action;
		public String email;
		public String clientDelivered;
		public String campaignName;
		public String emailStatus;
		public String partnerName;
		public String agentId;
		public String agentName;
		public String prospectCallId;
		public int rowNumber;
		public String leadValid;
		public String qaId;
		
		public String getEmailStatus() {
			return emailStatus;
		}

		public void setEmailStatus(String emailStatus) {
			this.emailStatus = emailStatus;
		}

		public String getPartnerName() {
			return partnerName;
		}

		public void setPartnerName(String partnerName) {
			this.partnerName = partnerName;
		}

		public String getAgentId() {
			return agentId;
		}

		public void setAgentId(String agentId) {
			this.agentId = agentId;
		}

		public String getAgentName() {
			return agentName;
		}

		public void setAgentName(String agentName) {
			this.agentName = agentName;
		}

		public String getLeadStatus() {
			return leadStatus;
		}

		public void setLeadStatus(String leadStatus) {
			this.leadStatus = leadStatus;
		}

		public String getRejectReason() {
			return rejectReason;
		}

		public void setRejectReason(String rejectReason) {
			this.rejectReason = rejectReason;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getClientDelivered() {
			return clientDelivered;
		}

		public void setClientDelivered(String clientDelivered) {
			this.clientDelivered = clientDelivered;
		}

		public String getCampaignName() {
			return campaignName;
		}

		public void setCampaignName(String campaignName) {
			this.campaignName = campaignName;
		}

		public String getProspectCallId() {
			return prospectCallId;
		}

		public void setProspectCallId(String prospectCallId) {
			this.prospectCallId = prospectCallId;
		}

		public int getRowNumber() {
			return rowNumber;
		}

		public void setRowNumber(int rowNumber) {
			this.rowNumber = rowNumber;
		}

		public String getQaId() {
			return qaId;
		}

		public void setQaId(String qaId) {
			this.qaId = qaId;
		}
		
		
	}

	@Override
	public void downloadDemandshoreReportByFilter(ProspectCallSearchDTO prospectCallSearchDTO, Login login) throws ParseException {
		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			LocalDate todaydate = LocalDate.now();
 			LocalDate fromDate = todaydate.minusDays(7);
 			prospectCallSearchDTO.setFromDate(fromDate.toString());
			prospectCallSearchDTO.setToDate(todaydate.toString());
			prospectCallSearchDTO.setStartMinute(0);
			prospectCallSearchDTO.setEndMinute(0);
		}
		List<String> agentIds = qaService.getAgentIds();
		prospectCallSearchDTO.setAgentIds(agentIds);
		if (prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			List<String> campaignIds = new ArrayList<String>();
			List<ProspectCallLog> prospects = new ArrayList<>();
			prospects = prospectCallLogRepository.getDistinctCampaignIds(agentIds, prospectCallSearchDTO);
			for (ProspectCallLog prospectCallLog : prospects) {
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null && !campaignIds.contains(prospectCallLog.getProspectCall().getCampaignId())) {
					campaignIds.add(prospectCallLog.getProspectCall().getCampaignId());
				}
			}
			prospectCallSearchDTO.setCampaignIds(campaignIds);
		}
		DemandshoreDownloadReportThread demandshoreLeadReportThread = new DemandshoreDownloadReportThread(
				prospectCallSearchDTO, login);
		Thread thread = new Thread(demandshoreLeadReportThread);
		thread.start();
	}

	@Override
	public String updateMultiCampaignLeadDetails(MultipartFile file) throws IOException {
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (!file.isEmpty()) {
			xlsFlieName = file.getOriginalFilename();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();
			
				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(xlsFlieName + " file is empty");
						throw new IllegalArgumentException("File cannot be empty.");
					}else if (totalRowsCount <= 1) {// check if nothing found in file
						book.close();
						logger.error("No records found in file " + xlsFlieName);
						throw new IllegalArgumentException("No Records Found in File.");
					}
					if (!validateExcelFileheader())
						return "";
					validateMultiCampaignExcelFileRecords(loginUser);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + fileName);
					return "Problem in reading the file " + fileName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
		//return null;
	}
	
	
	private void validateMultiCampaignExcelFileRecords(Login loginUser) {
	//	int rowsCount = sheet.getPhysicalNumberOfRows();
		int rowsCount = ErrorHandlindutils.getNonBlankRowCount(sheet);
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processMultiCampaignRecords(loginUser,0, rowsCount, colCounts);
	}
	
	
	public void processMultiCampaignRecords(Login loginUser,int startPos, int EndPos, int colCounts) {
		Map<String,String> campaignMap = new HashMap<String, String>();
		List<LeadReportUploadDTO> leadReportDTOs = new ArrayList<LeadReportUploadDTO>();
		List<String> errors = new ArrayList<>();
		boolean invalidFile=false;
		for (int i = startPos; i < EndPos; i++) {
		List< String > error=new ArrayList<>();
			LeadReportUploadDTO leadReportDTO = new LeadReportUploadDTO();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			if (i == 0) {
				continue;
			}
			if (row==null) {
				error.add("Record is missing");
				errorMap.put(i, error);
				invalidFile=true;
				EndPos++;
				continue;
			}
			
			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				error.add(" Record is empty");
				errorMap.put(i, error);
				invalidFile=true;
				EndPos++;
				continue;
			}
			
			String rejectString = "";
			String leadStatusString = "";
			String campaignNameString = "";
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("Lead Status") || columnName.equalsIgnoreCase("LeadStatus")) {
					if (cell != null && cell.getCellType()!=CellType.BLANK) {
						String leadStatus = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if(XtaasConstants.LEAD_STATUS.ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.ACCEPTED.toString());
						else if(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString());
						else if(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString());
						else	if(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString());
						else	if(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString());
						else	if(XtaasConstants.LEAD_STATUS.REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.REJECTED.toString());
						else	if(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString());
						else	if(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString().equalsIgnoreCase(leadStatus))
							leadReportDTO.setLeadStatus(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString());
						else {
							error.add(columnName+ " is not standard " );
						}
					}else
					{
						error.add(columnName+" is missing ");
					}
				
					//}
				} else if (columnName.equalsIgnoreCase("C1.Reason") || columnName.equalsIgnoreCase("Reason")) {
					if (cell != null &&  cell.getCellType()!=CellType.BLANK) {
						String rejectReason = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (leadReportDTO.getLeadStatus() != null) {
							if (leadReportDTO.getLeadStatus().equalsIgnoreCase("Rejected")
									|| leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_REJECTED")
									|| leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_RETURNED")) {
								if (rejectReason.equalsIgnoreCase("") || rejectReason.equalsIgnoreCase(null)
										|| rejectReason.isEmpty()) {
									//throw new IllegalArgumentException(
									if (leadReportDTO.getLeadStatus().equalsIgnoreCase("QA_RETURNED")) {
										rejectString = "Reject reason is mandatory as lead status is QA_RETURNED. Row No:- "
												+ (row.getRowNum() + 1);
										error.add(" Reject reason is mandatory as lead status is QA_RETURNED ");
									} else {
										rejectString = "Reject reason is mandatory as lead status is rejected. Row No:- "
												+ (row.getRowNum() + 1);
										error.add(" Reject reason is mandatory as lead status is rejected ");
									}
													//);
								} else {
									if (!setQaRejectReason(leadReportDTO, rejectReason)) {
										//throw new IllegalArgumentException(
												rejectString = "Reject reason is not specified as per qa rejectreason standard list. Row No:- "
														+ (row.getRowNum() + 1);
												error.add(" Reject reason is not specified as per qa reject reason standard list ");
														//);
									}
								}
							}
						} else {
							//throw new IllegalArgumentException(
									leadStatusString = "Lead status can't be empty for row: " + (row.getRowNum() + 1);
								
									//);
						}
					}
					 
				} else if (columnName.equalsIgnoreCase("Notes") || columnName.equalsIgnoreCase("C1.Notes") 
						|| columnName.equalsIgnoreCase("C1.CallNotes")) {
					if (cell != null) {
						String notes = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setNotes(notes);
					}
				} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("EmailAddress")
						|| columnName.equalsIgnoreCase("Email Address") || columnName.equalsIgnoreCase("Email ID")) {
					if (cell != null && cell.getCellType()!=CellType.BLANK) {
						String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setEmail(email);
					}
					else {
						error.add("Email id is missing");
					}
				} else if (columnName.equalsIgnoreCase("Client Delivered")) {
					if (cell != null) {
						String clientDelivered = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setClientDelivered(clientDelivered);
					}
				} else if (columnName.equalsIgnoreCase("ProspectCallId (for internal use only)")) {
					if (cell != null) {
						String prospectCallId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						leadReportDTO.setProspectCallId(prospectCallId);
					}
				} else if (columnName.equalsIgnoreCase("Campaign Name")) {
					if (cell != null) {
						String campaignName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setCampaignName(campaignName);
						String campaignId = campaignMap.get(campaignName);
						if(campaignId==null || campaignId.isEmpty()){
							Campaign campaign = campaignRepository.findByExactName(campaignName);
							if(campaign!=null && campaign.getId()!=null){
								campaignId = campaign.getId();
								campaignMap.put(campaignName, campaignId);
							}else {
								//throw new IllegalArgumentException(
								campaignNameString = "Campaign Name Doesnt Match in system in row: " + (row.getRowNum() + 1);
										//);
							}
						}
					}else {
						//throw new IllegalArgumentException(
						campaignNameString = "Campaign Name can't be empty for row: " + (row.getRowNum() + 1);
								//);
					}
				}else if (columnName.equalsIgnoreCase("Email Status")) {
					if (cell != null) {
						String emailStatus = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setEmailStatus(emailStatus);
					}
				}else if (columnName.equalsIgnoreCase("Partner Name")) {
					if (cell != null) {
						String partnerName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setPartnerName(partnerName);
					}
				}else if (columnName.equalsIgnoreCase("Agent Name")) {
					if (cell != null) {
						String agentName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setAgentName(agentName);
					}
				} else if (columnName.equalsIgnoreCase("QaId")) {
					if (cell != null) {
						String qaId = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						leadReportDTO.setQaId(qaId);
					}
				} else if (columnName.equalsIgnoreCase("Action")) {
					if (cell != null) {
						String action = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
						if(!action.isEmpty() && action.equalsIgnoreCase("update")){
							if(campaignNameString!=null && !campaignNameString.isEmpty()){
								throw new IllegalArgumentException(campaignNameString );
							}
							if(leadStatusString!=null && !leadStatusString.isEmpty()){
								throw new IllegalArgumentException(leadStatusString );
							}
							if(rejectString!=null && !rejectString.isEmpty()){
								throw new IllegalArgumentException(rejectString );
							}
						}
						leadReportDTO.setAction(action);
						leadReportDTOs.add(leadReportDTO);
					}
				}
				if (error!=null && error.size()>0) {
					invalidFile=true;
				}
			}
			errorMap.put(i, error);
		}
		if (errorMap !=null && invalidFile) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(loginUser,book,xlsFlieName,"LeadReport",null,null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw new IllegalArgumentException("Please Check Your Email There is Some Error in File");
		}
		
		
		for (LeadReportUploadDTO leadFileDTO : leadReportDTOs) {
			if (leadFileDTO.getAction() != null && leadFileDTO.getAction().equalsIgnoreCase("update")) {
				String campaignId = campaignMap.get(leadFileDTO.getCampaignName());
				updateCollection("ProspectCallLog", leadFileDTO, campaignId, errors);
				updateCollection("ProspectCallLogInteraction", leadFileDTO, campaignId, errors);
			}
		}
		PusherUtils.pushMessageToUser(loginUser.getId(), XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
		String notice = "Lead Report File Uploaded Successfully ";
		teamNoticeSerive.saveUserNotices(loginUser.getId(), notice);
	}

}
