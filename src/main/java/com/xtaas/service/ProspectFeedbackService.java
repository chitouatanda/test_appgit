package com.xtaas.service;

/**
 * Service to perform operations related to ProspectFeedback collection
 * 
 * @author pranay
 *
 */
public interface ProspectFeedbackService {

	public void autoDataBackupAndReport();

	public String manualDataBackup(String name) throws Exception;
	
	public String manualDataBackupAndReport(String name) throws Exception;

}
