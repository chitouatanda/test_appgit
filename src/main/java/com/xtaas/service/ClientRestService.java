package com.xtaas.service;

import java.util.List;

import com.xtaas.web.dto.RestCampaignDTO;

public interface ClientRestService {

	public RestCampaignDTO getCampaign(String sfdcCampaignId);

	public RestCampaignDTO createCampaign(RestCampaignDTO campaignDTO);
	
	public List<String> getCampaignManagers();

	public RestCampaignDTO updateCampaign(RestCampaignDTO campaignDTO);

}
