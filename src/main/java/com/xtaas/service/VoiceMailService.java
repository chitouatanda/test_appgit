package com.xtaas.service;

import java.util.Date;
import java.util.List;

import com.xtaas.domain.entity.VoiceMail;

public interface VoiceMailService {

	public void add(VoiceMail voicemail);

	public List<VoiceMail> findByProviderAndUpdatedDate(String provider, String partnerId, Date startDate, Date endDate);

}