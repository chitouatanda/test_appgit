package com.xtaas.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Partner;
import com.xtaas.db.repository.PartnerRepository;
import com.xtaas.web.dto.PartnerDTO;

@Service
public class PartnerServiceImpl implements PartnerService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(OrganizationServiceImpl.class);
	
	@Autowired
	private PartnerRepository partnerRepository; 
	
	@Override
	public Partner findOne(String partnerId) {
		return partnerRepository.findOneById(partnerId) ;
	}

	@Override
	public void updatePartner(Partner partner) {
		partnerRepository.save(partner);
	}

	@Override
	public Partner createPartner(PartnerDTO partnerDTO) throws DuplicateKeyException {
		Partner partner = partnerDTO.toPartner();
		partner.setId(partner.getCompanyName().toUpperCase());
		partnerRepository.save(partner);
		logger.debug(partner.getFirstName()+" partner created successfully");
		return  partnerRepository.save(partner);
	}

	@Override
	public PartnerDTO updatePartner(String partnerId, PartnerDTO partnerDTO) {
		Partner partner = partnerRepository.findOneById(partnerId);
		if(partner == null) {
			throw new IllegalArgumentException("Partner Id " + partnerId + " does not exist");
		}
		partner.setFirstName(partnerDTO.getFirstName());
		partner.setLastName(partnerDTO.getLastName());
		partner.setEmailId(partnerDTO.getEmailId());
		partner.setUserName(partnerDTO.getUserName());
		partner.setCompanyName(partnerDTO.getCompanyName());
		partner.setCompanySize(partnerDTO.getCompanySize());
		partner.setIndustry(partnerDTO.getIndustry());;
		partner.setPhoneNumber(partnerDTO.getPhoneNumber());
		partner.setAddress(partnerDTO.getAddress());
		return new PartnerDTO(partnerRepository.save(partner));
	}

	@Override
	public List<String> retriveAll() {
		List<Partner> partners = partnerRepository.findAll();
		List<String> tempPartners = new ArrayList<String>();
		for(Partner partner : partners) {
			tempPartners.add(partner.getId());
		}
		return tempPartners;
	}
}
