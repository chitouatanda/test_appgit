/**
 * 
 */
package com.xtaas.exception;

/**
 * @author djain
 *
 */
@SuppressWarnings("serial")
public class ResourceNotFoundException extends RuntimeException {
	private String message = null;
	
    public ResourceNotFoundException() {
    	super();
    }

    //Constructor that accepts a message
    public ResourceNotFoundException(String message) {
       super(message);
       this.message = message;
    }
    
    public ResourceNotFoundException(Throwable cause) {
    	super(cause);
    }
    
    @Override
    public String getMessage() {
        return this.message;
    }
}
