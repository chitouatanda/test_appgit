/**
 * 
 */
package com.xtaas.exception;

/**
 * @author djain
 *
 */
@SuppressWarnings("serial")
public class UnauthorizedAccessException extends RuntimeException {
	
	private String message = null;
	
    public UnauthorizedAccessException() {
    	super();
    }

    //Constructor that accepts a message
    public UnauthorizedAccessException(String message) {
       super(message);
       this.message = message;
    }
    
    public UnauthorizedAccessException(Throwable cause) {
    	super(cause);
    }
    
    @Override
    public String getMessage() {
        return this.message;
    }
}
