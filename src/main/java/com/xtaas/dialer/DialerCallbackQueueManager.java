package com.xtaas.dialer;

import java.util.concurrent.DelayQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.Prospect;
import com.xtaas.utils.XtaasConstants;

/**
 * @author djain
 *
 */
@Component
public class DialerCallbackQueueManager {

	private static final Logger logger = LoggerFactory.getLogger(DialerCallbackQueueManager.class);
	
	private DelayQueue<LeadCallback> leadCallbackQueue = new DelayQueue<LeadCallback>();
	
	public synchronized void publishMessage(ProspectCall prospectCall) {
		Prospect prospect = prospectCall.getProspect();
		if (prospect.getCallbackTimeInMS() == null) {
			prospect.setCallbackTimeInMS(System.currentTimeMillis() + XtaasConstants.ONE_HOUR_IN_MILLIS); // default to ONE HOUR from now
		}
		LeadCallback leadCallback = new LeadCallback(prospectCall);
		leadCallbackQueue.add(leadCallback);
		logger.debug("publishMessage() : Added lead to callback queue. Lead: " + prospect);
	}
	
	public ProspectCall getLeadToCall() throws InterruptedException {
		logger.debug("getLeadToCall() : Count of leads in callback queue : " + leadCallbackQueue.size());
		LeadCallback leadCallback = leadCallbackQueue.take(); //blocking call, wait till any lead delay has expired
		return leadCallback.getProspectCall();
	}
}
