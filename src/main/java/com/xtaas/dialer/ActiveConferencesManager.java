/**
 * 
 */
package com.xtaas.dialer;

import java.util.Set;

import org.springframework.stereotype.Component;

/**
 * Responsible for maintaining a list of all the active conferences happening on Twilio.
 * @author djain
 *
 */
@Component
public class ActiveConferencesManager {

	private Set<String> activeConferenceNames = null;

	/**
	 * @return the activeConferenceNames
	 */
	public Set<String> getActiveConferenceNames() {
		return activeConferenceNames;
	}

	/**
	 * @param activeConferenceNames the activeConferenceNames to set
	 */
	public void setActiveConferenceNames(Set<String> activeConferenceNames) {
		this.activeConferenceNames = activeConferenceNames;
	}
}
