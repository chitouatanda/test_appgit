/**
 * 
 */
package com.xtaas.dialer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/**
 * @author djain
 *
 */
@Component
public class DialerThreadPool {
	
	@Autowired
	private PropertyService propertyService;
	
	private ScheduledExecutorService scheduledThreadPool;
	
	@PostConstruct
	public void initialize() {
		int maxThreads = propertyService.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.DIALER_THREAD_COUNT.name(), 1);
		this.scheduledThreadPool = Executors.newScheduledThreadPool(maxThreads);
	}

	public void scheduleJob(Runnable job, int delay) {
		this.scheduledThreadPool.schedule(job, delay, TimeUnit.SECONDS);
	}
	
	public void shutdown() {
		this.scheduledThreadPool.shutdownNow();
	}
}
