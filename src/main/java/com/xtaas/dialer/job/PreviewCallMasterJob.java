/**
 * 
 */
package com.xtaas.dialer.job;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.xtaas.BeanLocator;
import com.xtaas.db.entity.ActivePreviewCall;
import com.xtaas.db.repository.ActivePreviewCallQueueRepository;

/**
 * 
 * @author djain
 */
@Component
public class PreviewCallMasterJob implements Runnable {
	
	private final Logger logger = LoggerFactory.getLogger(PreviewCallMasterJob.class);
	
	@Autowired
	private ActivePreviewCallQueueRepository activePreviewCallQueueRepository;
	
	/*public PreviewCallMasterJob(DialerCallBean callBean) {
		activePreviewCallQueueRepository = BeanLocator.getBean("activePreviewCallQueueRepository", ActivePreviewCallQueueRepository.class);
	}*/
	
	@Override
	public void run() {
		try {
			logger.info("run() : PreviewCallMasterJob started");
			
			int maxThreads = 10; //TODO: Set it in application property
			
			//Create the Thread Pool
			ExecutorService executorService = new ThreadPoolExecutor(
					maxThreads, // core thread pool size
					maxThreads, // maximum thread pool size
					1, // time to wait before resizing pool
					TimeUnit.MINUTES,
					new ArrayBlockingQueue<Runnable>(maxThreads, true),
					new ThreadPoolExecutor.CallerRunsPolicy());
			
			while (!Thread.interrupted()) {
				try {
					Pageable pageable = PageRequest.of(0, maxThreads*2, Direction.ASC, "createdDate"); //fetch oldest 20 callsids
					List<ActivePreviewCall> activePreviewCalls = activePreviewCallQueueRepository.findOldest(pageable);
					
					if (activePreviewCalls == null || activePreviewCalls.isEmpty()) {
						logger.info("run() : ActivePreviewCall Queue is empty. Waiting for 5 secs...");
						Thread.sleep(5000);
						continue;
					}
					
					for (ActivePreviewCall previewCall : activePreviewCalls) {
						logger.info("run(): Checking status of Preview Call from Queue: " + previewCall.getId());
						//set the processing flag to TRUE
						previewCall.setProcessing(true);
						try {
							activePreviewCallQueueRepository.save(previewCall);
						} catch (OptimisticLockingFailureException olfe) {
							logger.error("OptimisticLockingFailureException occurred. ThreadId: " + Thread.currentThread().getId(), olfe);
							ActivePreviewCall activePreviewCallFromDB = activePreviewCallQueueRepository.findOneById(previewCall.getId());
							if (!activePreviewCallFromDB.isProcessing()) {
								logger.info("OptimisticLockingFailureException occurred. Record is not in processing state. Resaving the record. PreviewCall Id: " + previewCall.getId());
								activePreviewCallFromDB.setProcessing(true);
								activePreviewCallQueueRepository.save(activePreviewCallFromDB);
							} else {
								logger.info("OptimisticLockingFailureException occurred. Record is already in processing state. Skipping it. PreviewCall Id: " + previewCall.getId(), olfe);
								continue;
							}
						}
						
						PreviewCallMonitorJob previewCallMonitorJob = BeanLocator.getBean("previewCallMonitorJob", PreviewCallMonitorJob.class);
						previewCallMonitorJob.setActivePreviewCall(previewCall);
						executorService.execute(previewCallMonitorJob);
					}
					
					while (((ThreadPoolExecutor) executorService).getQueue().size() == maxThreads) {
						logger.info("run() : ThreadPool's Queue is full. Waiting for 2 secs...");
						Thread.sleep(2000);
					}
					
					Thread.sleep(1000); //sleep for a second after processing all queued records
				} catch (Exception e) {
					logger.error("Error Occurred: ", e);
				} 
			} //end while
		} catch (Exception e) {
			logger.error("run(): Error Occurred in PreviewCallMasterJob. PreviewCallMasterJob is DEAD. Please restart DIALER.", e);
		}
	}
}
