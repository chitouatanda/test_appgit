package com.xtaas.dialer.job;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.ProspectCallSpeedLog;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.DialerUtils;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.ProspectCacheServiceImpl;
import com.xtaas.service.ProspectCallSpeedService;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasDateUtils;

@Component
public class CampaignSpeedJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(CampaignSpeedJob.class);

	@Autowired
	private RestClientService restClientService;

	@Autowired
	private DialerThreadPool dialerThreadPool;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallSpeedService prospectCallSpeedService;

	public CampaignSpeedJob() {
	}

	@Override
	public void run() {

		/*logger.info("run() : CampaignSpeedJob started");
		List<String> activeCampaignList = getActiveCampaigns();

		for (String campaignId : activeCampaignList) {
			calculateCallSpeed(campaignId);
		}
		dialerThreadPool.scheduleJob(this, 600);*/
	}

	private void calculateCallSpeed(String campaignId) {

		try {
			Calendar now = Calendar.getInstance();

			System.out.println("Current time : " + now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE) + ":"
					+ now.get(Calendar.SECOND));
			Date nowDate = now.getTime();
			String strCurrDateInStateTZ = XtaasDateUtils.convertToTimeZone(nowDate, XtaasDateUtils.TZ_UTC,
					XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date endDate = XtaasDateUtils.convertStringToDate(strCurrDateInStateTZ,
					XtaasDateUtils.DATETIME_FORMAT_DEFAULT);

			now = Calendar.getInstance();
			now.add(Calendar.MINUTE, -10);// TODO take minutes to configurable.

			System.out.println("Time before 10 minutes : " + now.get(Calendar.HOUR_OF_DAY) + ":"
					+ now.get(Calendar.MINUTE) + ":" + now.get(Calendar.SECOND));

			Date beforeDate = now.getTime();
			String strBeforeDateInStateTZ = XtaasDateUtils.convertToTimeZone(beforeDate, XtaasDateUtils.TZ_UTC,
					XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDate = XtaasDateUtils.convertStringToDate(strBeforeDateInStateTZ,
					XtaasDateUtils.DATETIME_FORMAT_DEFAULT);

			int abandonedCount = this.prospectCallLogRepository.getCountOfRecordsAbandoned(campaignId, startDate,
					endDate);
			int contactedCount = this.prospectCallLogRepository.getCountOfRecordsContacted(campaignId, startDate,
					endDate);
			int attemptedCount = this.prospectCallLogRepository.getCountOfRecordsAttempted(campaignId, startDate,
					endDate);

			int connectRatio = 0;
			int abandonedRatio = 0;
			int speedOfCall = 0;
			if (contactedCount != 0 && attemptedCount != 0) {
				connectRatio = (contactedCount * 100) / attemptedCount;
				speedOfCall = attemptedCount / contactedCount;
			}
			// This below condition to maintain the call speed always above or equal to
			// intermediate.
			if (speedOfCall < 1) {
				speedOfCall = 1;
			}
			// Highest thread limit so that it dont go and jam twilio,
			// even 10 speed too is higher but for now keeping it as 10
			if (speedOfCall > 10) {
				speedOfCall = 10;
			}

			if (abandonedCount != 0 && attemptedCount != 0)
				abandonedRatio = (abandonedCount * 100) / attemptedCount;

			if (abandonedRatio > 3) {
				// callSpeed=speedOfCall-1;
				speedOfCall = speedOfCall - 1;
			} else {
				// callSpeed=speedOfCall;
			}

			// TODO abandonnedRatio, Minutes to check , upper limit, lower limit of thread
			// speed should be configurable

			ProspectCallSpeedLog prospectCallSpeedLog = new ProspectCallSpeedLog();
			prospectCallSpeedLog.setRecordsAttempt(Integer.toString(contactedCount));
			prospectCallSpeedLog.setRecordsContact(Integer.toString(attemptedCount));
			prospectCallSpeedLog.setRecordsAbandoned(Integer.toString(abandonedCount));
			prospectCallSpeedLog.setConnectRatio(Integer.toString(connectRatio));
			prospectCallSpeedLog.setAbandonedRatio(Integer.toString(abandonedRatio));
			prospectCallSpeedLog.setCampaignId(campaignId);
			// prospectCallSpeedLog.setCallSpeed(Integer.toString(callSpeed));
			prospectCallSpeedLog.setCallSpeed(Integer.toString(speedOfCall));

			prospectCallSpeedService.save(prospectCallSpeedLog);

			prospectCacheServiceImpl.addCachedCallSpeed(campaignId, speedOfCall);

			logger.info("##### Speed for CampaignId [{}] is [{}].", campaignId, speedOfCall);

		} catch (Exception e) {
			logger.error("run(): Error Occurred in CampaingSpeedJob for CampaignId " + campaignId, e);
			dialerThreadPool.scheduleJob(this, 600);
		}

	}

	@SuppressWarnings("unchecked")
	private List<String> getActiveCampaigns() {
		StringBuilder activeCampaignsRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
				.append("/spr/api/agentqueue/campaigns");

		// set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());

		String activeCampaignsResponse = restClientService.makeGetRequest(activeCampaignsRestUrl.toString(), headers);
		List<String> activeCampaigns = null;
		try {
			activeCampaigns = JSONUtils.toObject(activeCampaignsResponse, List.class);
		} catch (IOException e) {
			logger.error("getActiveCampaigns() : Error Occurred", e);
		}
		logger.info("getActiveCampaigns() : Requested url [{}]. Response received : [{}] ", activeCampaignsRestUrl,
				activeCampaignsResponse);

		return activeCampaigns;
	}
}
