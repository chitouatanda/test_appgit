/**
 * 
 */
package com.xtaas.dialer.job;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import com.xtaas.utils.XtaasUtils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.application.service.OutboundNumberService;
import com.xtaas.application.service.TeamService;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.DialerUtils;
import com.xtaas.dialer.bean.DialerCallBean;
import com.xtaas.dialer.bean.DialerCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.ConfigService;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.ws.strikeiron.client.StrikeIronCellPhoneVerificationCaller;
import com.xtaas.ws.strikeiron.client.StrikeIronDNCCaller;

/**
 * Runs once and makes call using Twilio
 * @author djain
 */
public class CallJob implements Runnable {
    
    private final Logger logger = LoggerFactory.getLogger(CallJob.class);

    private DialerCallBean callBean;
    private ProspectCall prospectCall;
    private PropertyService propertyService;
    private ConfigService configService;
    private ProspectCallLogService prospectCallLogService;
    private RestClientService restClientService;
    private DialerThreadPool dialerThreadPool;
    private ProspectCallStatus prospectCallStatus;
    private InternalDNCListService internalDNCListService;
    private OutboundNumberService outboundNumberService;
    private CampaignService campaignService;
    private TeamService teamService;
   // private List<IndirectProspectsDTO> indirectProspectsDTO;
    
    public CallJob(ProspectCall prospectCall, DialerCallBean callBean) {
        setProspectCall(prospectCall);
        setCallBean(callBean);
        propertyService = BeanLocator.getBean("propertyServiceImpl", PropertyService.class);
        configService = BeanLocator.getBean("configServiceImpl", ConfigService.class);
        prospectCallLogService = BeanLocator.getBean("prospectCallLogServiceImpl", ProspectCallLogService.class);
        restClientService = BeanLocator.getBean("restClientService", RestClientService.class);
        dialerThreadPool = BeanLocator.getBean("dialerThreadPool", DialerThreadPool.class);
        internalDNCListService = BeanLocator.getBean("internalDNCListServiceImpl", InternalDNCListService.class);
        outboundNumberService = BeanLocator.getBean("outboundNumberServiceImpl", OutboundNumberService.class);
        campaignService = BeanLocator.getBean("campaignServiceImpl", CampaignService.class);
        teamService = BeanLocator.getBean("teamServiceImpl", TeamService.class);
    }
    
    @Override
    public void run() {
        //makes a twilio call
        logger.debug("run() : CallJob started for ProspectCall [{}]", prospectCall.getProspectCallId());
        boolean verifyPhoneAndDNC = propertyService.getBooleanApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CHECK_PHONENUMBER_USING_STRIKEIRON.name(), false); //False -> skip StrikeIron checks
        DialerCallStatus callBeanCallStatus = null;
        try {
            if (this.prospectCall != null) {
                //clean up previous statuses
                this.prospectCall.setDispositionStatus(null);
                this.prospectCall.setSubStatus(null);
                
                Prospect prospect = this.prospectCall.getProspect();
                Campaign campaign = campaignService.getCampaign(this.prospectCall.getCampaignId());
                Team team = teamService.getTeam(campaign.getTeam().getTeamId());
                
                logger.info("Processing Lead: " + prospect.getPhone() + " ProspectCallId : " + this.prospectCall.getProspectCallId());
                //check if number is added to local dnc list, if so then skip this lead
                boolean localDNCListCheckPassed = checkLocalDNCList(prospect, team.getPartnerId());
                           
                
                if (localDNCListCheckPassed) {  
                    //check call restrictions
                    boolean callRestrictionsPassed = checkCallRestrictions(prospect);
                    boolean canBeCalled = false;
                    if (callRestrictionsPassed) {
                        if (verifyPhoneAndDNC ) {
                            //Verify Phone number and DNC check
                            if (!prospect.isOptedIn()) { //if Lead has not OPTED-IN, then make DNC check else bypass 
                                canBeCalled = checkDNCList(prospect);
                            } else {
                                canBeCalled = checkPhoneNumber(prospect);
                                logger.info("run() : Prospect [{}] has Opted-In for the call, so DNC check is bypassed. Phone Verification : " + canBeCalled, this.prospectCall.getProspectCallId());
                            }
                            if (!canBeCalled) {
                                this.prospectCallStatus = ProspectCallStatus.NATIONAL_DNC_ERROR;
                            }
                        } else {
                            logger.info("run() : *** Skipped StrikeIron checks *** ");
                            canBeCalled = true;
                        }
                    } else {
                        canBeCalled = false;
                        logger.info("run() : Prospect [{}] couldn't pass the call restrictions. Phonenumber: " + prospect.getPhone(), this.prospectCall.getProspectCallId());
                    }
                    
                    if (canBeCalled) {
                        //check if call retry count is within CALL_MAX_RETRIES limit
                    	int callMaxRetriesLimit = 5;
						if (campaign != null && campaign.getCallMaxRetries() > 0) {
							callMaxRetriesLimit = campaign.getCallMaxRetries(); // get call max retry count from
																				// campaign
						} else {
							callMaxRetriesLimit = propertyService.getIntApplicationPropertyValue(
									XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
						}

                        if (this.prospectCall.getCallRetryCount() >= callMaxRetriesLimit) {
                            logger.info("run() : Prospect [{}] has been retried CALL_MAX_RETRIES times. Call retry cannot exceed beyond [{}] limit" , this.prospectCall.getProspectCallId(), callMaxRetriesLimit);
                            this.prospectCallStatus = ProspectCallStatus.MAX_RETRY_LIMIT_REACHED;
                            canBeCalled = false;
                        }
                        
                        //Daily Retry Limit check
                        int dailyCallMaxRetriesLimit = 1;
						if (campaign != null && campaign.getDailyCallMaxRetries() > 0) {
							// get daily call max retry count from campaign
							dailyCallMaxRetriesLimit = campaign.getDailyCallMaxRetries();
						} else {
							dailyCallMaxRetriesLimit = propertyService.getIntApplicationPropertyValue(
									XtaasConstants.APPLICATION_PROPERTY.DAILY_CALL_MAX_RETRIES.name(), 1);
						}
                        Long dailyCallMaxRetry = Long.valueOf(getCurrentDateInProspectTZinYMD(this.prospectCall.getProspect()) + String.format("%02d", dailyCallMaxRetriesLimit)); 
                        
                        if (this.prospectCall.getDailyCallRetryCount() != null && String.valueOf(this.prospectCall.getDailyCallRetryCount()).length() == 10) {
                            if (this.prospectCall.getDailyCallRetryCount() >= dailyCallMaxRetry) {
                                logger.info("run() : Prospect [{}] has reached DAILY_CALL_RETRY_LIMIT. Daily Call Limit [{}] " , this.prospectCall.getProspectCallId(), dailyCallMaxRetriesLimit);
                                canBeCalled = false;
                            } //end daily limit check
                        }
                    } //end check of max and daily limit
                    
                    if (canBeCalled) {
                        //initiate the session
                        initiateProspectInteractionSession();
                        
                        //String outboundNumber = outboundNumberService.getOutboundNumber(campaign.getTeam().getTeamId(), campaign.getTeam().getOutboundNumberBucketName(),this.prospectCall.getProspect().getPhone(), this.prospectCall.getProspectVersion()); //get the outbound number to be used to call this prospect
                        String outboundNumber = outboundNumberService.getOutboundNumber(campaign.getTeam().getTeamId(), "DEFAULT",this.prospectCall.getProspect().getPhone(), this.prospectCall.getCallRetryCount()); //get the outbound number to be used to call this prospect
                        this.prospectCall.setOutboundNumber(outboundNumber);
                        if (DialerMode.POWER.equals(prospectCall.getDialerMode())) {
                            String callSid = makeCall(campaign); //make a call
                            if (callSid != null) {
                                callBeanCallStatus = DialerCallStatus.RINGING;
                                this.prospectCallStatus = ProspectCallStatus.CALL_DIALED;
                                ///below code to remove call monitor job
                                this.prospectCall.setCallStartTime(this.callBean.getCallInitiatedTime());
                                this.prospectCall.setTwilioCallSid(this.callBean.getTwilioCallSid());
                               // this.callBean.getProspectCallId();
                                
                                ////end code to remove call monitor job
                                this.prospectCall.setCallRetryCount(this.prospectCall.getCallRetryCount()+1); //increment the count of calls made to this lead
                                incrementDailyCallRetryCount(); //increment daily call retry count
                                logger.info("run() : Made Call to PhoneNumber: " + prospect.getPhone());
                                //creates a call monitor job and pass on the callBean after setting call sid
                               /* CallMonitorJob callMonitorJob = new CallMonitorJob(this.callBean);
                                dialerThreadPool.scheduleJob(callMonitorJob, 2); //monitor calls after every 2 sec
                                logger.debug("run() : Created CallMonitorJob for Prospect [{}] ", callBean.getProspectCallId());*/
                            } else {
                                //if NULL CallSID then mark the call status as COMPLETE, so that CampaignMonitorJob finishes gracefully
                                callBeanCallStatus = DialerCallStatus.COMPLETED;
                                this.prospectCallStatus = ProspectCallStatus.CALL_FAILED;
                            }
                        } else {
                            //commented below line as the callStatus will be updated in DialTwimlFilter in case of Preview calls.
                            //this.prospectCallStatus = ProspectCallStatus.CALL_PREVIEW; //prospect pushed to agent but call not initiated
                            this.prospectCallStatus = null; //setting to NULL so that ProspectCallLog is not updated here but gets updated in DialTwimlFIlter
                            this.prospectCall.setCallRetryCount(this.prospectCall.getCallRetryCount()+1); //increment the count of calls made to this lead
                            incrementDailyCallRetryCount(); //increment daily call retry count
                            makePreviewCall(); //make preview call. callsid returned is not a real twilio call sid
                            callBeanCallStatus = DialerCallStatus.COMPLETED; //in case of Preview, no need to monitor call as it will be initiated by agent
                            logger.info("run() : Preview Call initiated for PhoneNumber: " + prospect.getPhone());
                        }
                    } else {
                        //not setting this.prospectCallStatus as it is set already in the conditions where canBeCalled flag is set to False with appropriate code
                        //if cannot be called then no need to monitor call, mark it as COMPLETED
                        callBeanCallStatus = DialerCallStatus.COMPLETED;
                        logger.info("run() : Phonenumber cannot be called : " + prospect.getPhone());
                    }
                } else {
                    //this.callBean.setCallStatus(DialerCallStatus.COMPLETED);
                    callBeanCallStatus = DialerCallStatus.COMPLETED;
                    this.prospectCallStatus = ProspectCallStatus.LOCAL_DNC_ERROR;
                } //end if localDNCListCheckPassed
            } //end if lead != null
            
            //update prospectCallStatus in database
            if (prospectCallStatus != null) {
                prospectCallLogService.updateCallStatus(callBean.getProspectCallId(), this.prospectCallStatus, this.callBean.getTwilioCallSid(), this.prospectCall.getCallRetryCount(), this.getProspectCall().getDailyCallRetryCount());
            }

            if (callBeanCallStatus != null) {
                this.callBean.setCallStatus(callBeanCallStatus);
            }
        } catch (Exception e) {
            this.callBean.setCallStatus(DialerCallStatus.COMPLETED);
            logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
        } finally {
            logger.debug("run() : CallJob finished for ProspectCall [{}]", prospectCall.getProspectCallId());
            this.prospectCall = null;
            this.prospectCallStatus = null;
        }
    } //end run
    
    private void incrementDailyCallRetryCount() {
        String strCurrentDateYMD = getCurrentDateInProspectTZinYMD(this.prospectCall.getProspect()); //calculate current date/time in prospect's timezone. returns date in 20160708 format
        if (this.prospectCall.getDailyCallRetryCount() == null) { //dailyCallRetryCount has format : yyyyMMdd<2 digit counter>
            this.prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01")); //set the daily retry to 01, since its first time
        } else { //if already set, then increment last 2 digits if date has not changed else reset to new date
            String strDailyCallRetryDate = StringUtils.substring(String.valueOf(this.prospectCall.getDailyCallRetryCount()), 0, 8); //returns date set in daily counter
            Integer dailyRetryCounter = Integer.parseInt(StringUtils.substring(String.valueOf(this.prospectCall.getDailyCallRetryCount()), 8)); //extract last 2 digits for counter
            if (!strDailyCallRetryDate.equals(strCurrentDateYMD)) { //if current date is not same as date in daily counter
                //then reset the counter with new date, set the daily retry to 01, since its first time
                this.prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01")); //format: yyyyMMdd<2 digit counter>
            } else { //if daily counter date is same as current date then
                dailyRetryCounter += 1; //increment the counter
                this.prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyRetryCounter)));
            }
        } 
    }
    
    private void initiateProspectInteractionSession() {
        String prospectInteractionSessionId = generateProspectInteractionSessionId();
        this.prospectCall.setProspectInteractionSessionId(prospectInteractionSessionId);
        prospectCallLogService.updateProspectInteractionSessionId(this.prospectCall.getProspectCallId(), prospectInteractionSessionId); //save the session id into database
    }
    
    private String generateProspectInteractionSessionId() {
        return "IS" + StringUtils.replace(UUID.randomUUID().toString(), "-", ""); //generating a new session id for this interaction; IS stands for Interaction Session
    }

    private String makeCall(Campaign campaign) {
        Prospect prospect = this.prospectCall.getProspect();
        String phoneNumber = prospect.getPhone(); //"202-762-1401"; -- test number for machine answered
        logger.info("##### makeCall() : Making Call to PhoneNumber: [{}] #####", phoneNumber);
        Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
        Account account = channelTwilio.getChannelConnection();
        CallFactory callFactory = account.getCallFactory(); //callfactory to make outbound call.
       
        Map<String, String> callParams = new HashMap<String, String>();
        callParams.put("From", this.prospectCall.getOutboundNumber()); // Twilio number                                     
		if (campaign != null && campaign.getSipProvider() != null && campaign.getSipProvider().equalsIgnoreCase("ThinQ")) { // ASSUMPTION only US support for now                            
			String fmtPhNum = XtaasUtils.getSimpleFormattedUSPhoneNumber(phoneNumber); 
			/*                                                                      
				Util only checks for null. Removing the check below as no change in behavior. Need to validate
				country code supported by ThinQ -currently only US. Util will also convert to ThinQ supported
				phone number formats:  1<10-digit-number> or +1<10-digit-number>    
																					   
			if (fmtPhNum == null) {                                                    
				logger.error("makeCall() : Illegal phone number or country code"); // Only supported country US
				return null;                                                        
			}                                                                          
			*/                                                                         
																					   
			// To=sip:+14085052222@wap.thinq.com?X-account-id=<id>&X-account-token=<access-token>
			StringBuilder to = new StringBuilder("sip:").append(fmtPhNum)           
							.append("@").append(ApplicationEnvironmentPropertyUtils.getThinQDomainUrl())
							.append("?X-account-id=").append(ApplicationEnvironmentPropertyUtils.getThinQId())
							.append("&X-account-token=").append(ApplicationEnvironmentPropertyUtils.getThinQToken());                       
			callParams.put("To", to.toString());                                    
		} else {                                                                    
			callParams.put("To", phoneNumber); // lead's phone number       
		}       
			
        
        // boolean enableMachineDetection = propertyService.getBooleanApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.ENABLE_MACHINE_ANSWERED_DETECTION.name(), true);
        boolean enableMachineDetection = campaign.isEnableMachineAnsweredDetection();
        logger.info("makeCall() : enableMachineDetection [{}] for campaign [{}] before call retry check.", enableMachineDetection, campaign.getName());

        // if enableMachineDetectionOnCallRetry flag is enabled then will enable machine detection if prospect's callRetry count is greater than equals to MAX_CALL_RETRY_COUNT_MACHINE_DETECTION.
        	if (this.prospectCall.getCallRetryCount() >= propertyService.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RETRY_COUNT_MACHINE_DETECTION.name(), 1)) {
    			enableMachineDetection = true;
    		}
		
        logger.info("makeCall() : enableMachineDetection [{}] after call retry [{}] check.", enableMachineDetection, this.prospectCall.getCallRetryCount());

        if (enableMachineDetection) {
        	callParams.put("IfMachine", "Hangup"); // to detect if phone is picked by human or machine. Hangup the call as soon as machine is detected. Call flow will not continue, which will results in no callog entry in CALLLOG collection
        }
        
        int maxCallRingingDurationInSecs = propertyService.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTODIAL.name(), 30); //secs        
        callParams.put("Timeout", Integer.toString(maxCallRingingDurationInSecs)); // to detect if phone is picked by human or machine. Hangup the call as soon as machine is detected. Call flow will not continue, which will results in no callog entry in CALLLOG collection
        
        String statusCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/call/twilio/status";
        callParams.put("StatusCallback", statusCallbackUrl);
        try {
            StringBuilder dialXMLUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getTwimlDialXmlUrl());
            dialXMLUrl.append("?dialer=1&pcid=").append(callBean.getProspectCallId());
            dialXMLUrl.append("&whisperMsg="+URLEncoder.encode(prospect.getFirstName() + " " + prospect.getLastName(), "UTF-8"));
            dialXMLUrl.append("&callerId="+URLEncoder.encode(this.prospectCall.getOutboundNumber(), "UTF-8"));
            
            callParams.put("Url", dialXMLUrl.toString());
        } catch (UnsupportedEncodingException e) {
            logger.error("makeCall() : Exception while encoding url parameters.", e);
        }
        
        // Make the call
        Call call;
        try {
            call = callFactory.create(callParams);
            String callSid = call.getSid();
            
            this.callBean.setTwilioCallSid(callSid);
            this.callBean.setCallInitiatedTime(new Date());
            logger.info("makeCall(): CallSid = {} | Prospect = {} ", callSid, prospect);
            
            registerCallSidWithServer(callSid);
            
            return call.getSid();
        } catch (Exception e) {
            logger.error("Error in calling " + phoneNumber, e);
        } 
        return null;
    }
    
    //this method is invoked when dialer mode is PREVIEW and it does not initiate call with Twilio, rather hit dial.xml directly 
    //which pushes the prospect to Agent
    private String makePreviewCall() {
        String callSid = UUID.randomUUID().toString();
        registerCallSidWithServer(callSid);
        requestDialXML(callSid);
        return callSid;
    }
    
    private void registerCallSidWithServer(String callSid) {
        StringBuilder callRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/call");
        StringBuilder callSidUrl = new StringBuilder(callRestUrl).append("/").append(callSid);
        
        //set headers
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", DialerUtils.generateAuthHeader());
        
        String setCallSidResponse = null;
        try {
            ProspectCallDTO prospectCallDTO = new ProspectCallDTO(this.prospectCall);
          //  prospectCallDTO.setIndirectProspects(this.indirectProspectsDTO);
            setCallSidResponse = restClientService.makePostRequest(callSidUrl.toString(), headers, JSONUtils.toJson(prospectCallDTO));
        } catch (IOException e) {
            logger.error("registerCallSidWithServer() : Error in registering callSid " + callSid, e);
        }
        logger.info("registerCallSidWithServer() : Made Rest Call to set Prospect's call sid. Response received : {} ", setCallSidResponse);
    }
    
    //ONLY for PREVIEW MODE
    private void requestDialXML(String callSid) {
        //Prospect prospect = this.prospectCall.getProspect();
        //try {
        StringBuilder dialXMLUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getTwimlDialXmlUrl()).append("?dialer=1&CallSid=").append(callSid);
                /*.append("&pcid=").append(callBean.getProspectCallId())
                .append("&whisperMsg=").append(URLEncoder.encode(prospect.getFirstName() + " " + prospect.getLastName(), "UTF-8"));*/
        String response = restClientService.makeGetRequest(dialXMLUrl.toString(), null);
        logger.info("requestDialXML() : PREVIEW MODE -> Made Call to [{}]. Response received : {} ", dialXMLUrl.toString(), response);
        /*} catch (UnsupportedEncodingException e) {
            logger.error("requestDialXML() : Error", e);
        }*/
    }
    
    private boolean checkLocalDNCList(Prospect prospect, String partnerId) {
        logger.info("checkLocalDNCList() : Check if the number is not in local DNC List. Phonenumber: [{}], PartnerId: [{}] ", prospect.getPhone(), partnerId);
        List<DNCList> dncList = internalDNCListService.getDNCList(prospect.getPhone(),prospect.getFirstName(),prospect.getLastName());
        DNCList dncListFromDB = null;
        if (dncList != null && dncList.size() > 0) {
			for (DNCList dnc : dncList) {
				if (dnc.getPartnerId().equalsIgnoreCase(partnerId)) {
					dncListFromDB = dnc;
				}
			}
		}
        if (dncListFromDB != null) {
            logger.info("checkLocalDNCList() : Lead cannot be called as Phonenumber already exist in local DNC List. Phonenumber: [{}] ", prospect.getPhone());
            return false;
        }
        return true;
    }
    
    
    private boolean checkDNCList(Prospect prospect) {
        logger.debug("checkDNCList() : Calling StrikeIron WS for DNC check : " + prospect.getPhone());
        StrikeIronDNCCaller dncCaller = new StrikeIronDNCCaller(prospect.getPhone());
        logger.info("checkDNCList() : DNC Check - StatusNbr: " + dncCaller.getStatusNbr() + " , Status Description: " + dncCaller.getStatusDescription());
        return dncCaller.canBeCalled();
    }
    
    private boolean checkPhoneNumber(Prospect prospect) {
        try {
            StrikeIronCellPhoneVerificationCaller caller = new StrikeIronCellPhoneVerificationCaller(prospect.getPhone());
            return caller.isPhoneNumberValid();
        } catch (Exception ex){
            logger.error("Error occurred in PhoneVerification API call.", ex);
            return false;
        }
    }

    private String getCurrentDateInProspectTZinYMD(Prospect prospect) {
        StateCallConfig stateCallConfig = configService.getStateCallConfig(prospect.getStateCode());
        String currDateInProspectsTZ = null;
        try {
            currDateInProspectsTZ = XtaasDateUtils.convertToTimeZone(new Date(), stateCallConfig.getTimezone(), "yyyyMMdd");
        } catch (ParseException e) {
            logger.error("getCurrentDateInPacificTZinYMD() : Exception occurred while converting date into Pacific TZ", e);
        }
        return currDateInProspectsTZ;
    }
        
    private boolean checkCallRestrictions(Prospect prospect) {
        logger.info("checkCallRestrictions() : Check if the number can be called now as per State level Call restrictions. Phonenumber: " + prospect.getPhone() + ", State: " + prospect.getStateCode());
        StateCallConfig stateCallConfig = configService.getStateCallConfig(prospect.getStateCode());
        if (stateCallConfig == null) {
            logger.error("checkCallRestrictions() : No STATECODE specified for Prospect [{}]. Phonenumber: " + prospect.getPhone(), this.prospectCall.getProspectCallId());
            this.prospectCallStatus = ProspectCallStatus.UNKNOWN_ERROR;
            return false;
        }
        
        // convert date into Lead's TZ
        try {
            Date currentDate = new Date();
            String strCurrDateInLeadsTZ = XtaasDateUtils.convertToTimeZone(currentDate, stateCallConfig.getTimezone(), XtaasDateUtils.DATE_FORMAT_DEFAULT);
            Date dtCurrDateInLeadsTZ = XtaasDateUtils.convertStringToDate(strCurrDateInLeadsTZ, XtaasDateUtils.DATE_FORMAT_DEFAULT);
            
            //don't call if today is weekend and weekend call is not allowed
            if (!stateCallConfig.isWeekendCallAllowed() && XtaasDateUtils.isWeekend(dtCurrDateInLeadsTZ)) {
                logger.info("Prospect [{}] cannot be called on Weekend. PhoneNumber: " + prospect.getPhone() + ", TimeZone: " + stateCallConfig.getTimezone() + ", Date in Leads TZ : " + strCurrDateInLeadsTZ, prospectCall.getProspectCallId());
                // Not setting the callback date as weekend condition applicable to all the prospect, so not updating the prospect with the callback as it will result in updating callback for all the prospects. 
                //Date callbackDate = XtaasDateUtils.getNextWeeksDate(GregorianCalendar.MONDAY, stateCallConfig.getStartCallHour()); //set callback to next day business hour.
                //updateCallbackStatus(callbackDate);
                return false;
            }
            
            //don't call if it is holiday
            if (stateCallConfig.getHolidayDates().contains(strCurrDateInLeadsTZ)) {
                logger.info("Prospect [{}] cannot be called on Holidays. PhoneNumber: " + prospect.getPhone() + ", Date in Leads TZ : " + strCurrDateInLeadsTZ, prospectCall.getProspectCallId());
                DateTime callbackDate = XtaasDateUtils.getNextDaysDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get next days date in prospect's TZ
                Date callbackDateInUTC = XtaasDateUtils.changeTimeZone(callbackDate, DateTimeZone.UTC.getID()).toDate();
                updateCallbackStatus(callbackDateInUTC);
                return false;
            }
            
            //check call time restrictions, if current time falls within the legal call time
            int currentHour = Calendar.getInstance(TimeZone.getTimeZone(stateCallConfig.getTimezone())).get(Calendar.HOUR_OF_DAY);
            if (currentHour < stateCallConfig.getStartCallHour() || currentHour >= stateCallConfig.getEndCallHour()) {
                logger.info("Prospect [{}] cannot be called out of call hour restrictions. Lead: " + prospect + " , Date in Leads TZ : " + strCurrDateInLeadsTZ + ", CurrentHour: " + currentHour + ", Allowed range: " + stateCallConfig.getStartCallHour() + " - " + stateCallConfig.getEndCallHour(), prospectCall.getProspectCallId() );
                DateTime callbackDate;
                if (currentHour < stateCallConfig.getStartCallHour()) {
                    callbackDate = XtaasDateUtils.getCurrentDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get current day in prospect's TZ
                } else {
                    callbackDate = XtaasDateUtils.getNextDaysDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get next days date in prospect's TZ
                }
                Date callbackDateInUTC = XtaasDateUtils.changeTimeZone(callbackDate, DateTimeZone.UTC.getID()).toDate();
                updateCallbackStatus(callbackDateInUTC);
                return false;
            }
        } catch (ParseException e) {
            logger.error("Exception occurred during checking call restrictions.", e);
            this.prospectCallStatus = ProspectCallStatus.UNKNOWN_ERROR;
            return false;
        }
        logger.info("Lead passes the call restrictions of weekend, holidays and call hours. Phonenumber: " + prospect.getPhone());
        return true;
    }
    
    
    private void updateCallbackStatus(Date callbackDate) {
        prospectCallLogService.updateCallbackStatusSystemGenerated(this.prospectCall.getProspectCallId(), callbackDate);
    }
    
    /**
     * @return the callBean
     */
    public DialerCallBean getCallBean() {
        return callBean;
    }

    /**
     * @param callBean the callBean to set
     */
    private void setCallBean(DialerCallBean callBean) {
        this.callBean = callBean;
    }

    /**
     * @param prospectCall the prospectCall to set
     */
    private void setProspectCall(ProspectCall prospectCall) {
        this.prospectCall = prospectCall;
    }

    /**
     * @return the prospectCall
     */
    public ProspectCall getProspectCall() {
        return prospectCall;
    }
    
   
}
