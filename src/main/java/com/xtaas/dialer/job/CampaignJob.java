/**
 * 
 */
package com.xtaas.dialer.job;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.xtaas.BeanLocator;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.DialerUtils;
import com.xtaas.dialer.bean.DialerCallBean;
import com.xtaas.dialer.bean.DialerCallStatus;
import com.xtaas.dialer.bean.DialerCampaignBean;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.ProspectCacheServiceImpl;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.ProspectSearchClauses;
import com.xtaas.web.dto.ProspectSearchDTO;
import com.xtaas.web.dto.QueuedProspectDTO;

/**
 * Runs once share DialerCampaignBean with MasterJob
 * @author djain
 *
 */
public class CampaignJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(CampaignJob.class);
	
	private RestClientService restClientService;
	
	private DialerThreadPool dialerThreadPool;

	private DialerCampaignBean campaignBean;
	
	// private CampaignService campaignService;

	private CampaignRepository campaignRepository;
	
	private ProspectCallLogRepository prospectCallLogRepository;
	
	private ProspectCacheServiceImpl prospectCacheServiceImpl;
	
	public CampaignJob(DialerCampaignBean dialerCampaignBean) {
		setCampaignBean(dialerCampaignBean);
		this.restClientService = BeanLocator.getBean("restClientService", RestClientService.class);
		this.dialerThreadPool = BeanLocator.getBean("dialerThreadPool", DialerThreadPool.class);
		this.prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
		this.campaignRepository = BeanLocator.getBean("campaignRepository", CampaignRepository.class);
		this.prospectCacheServiceImpl = BeanLocator.getBean("prospectCacheServiceImpl", ProspectCacheServiceImpl.class);
		
	}
	
	@Override
	public void run() {
		/*try {
			//int maxlimit =20;
			/*List<Campaign> activeCampaigns = campaignService.getActiveCampaigns();
			for(Campaign c :activeCampaigns){
				String cid = c.getId();
				if(cid.equals(campaignBean.getCampaignId()){
					maxlimit = c.getCampaignThreadCap();
				}
				
			}
			logger.info("run() : CampaignJob started");
			List<ProspectCallLog> prospects = getDialedProspects();
			int queuedCallCount = getQueuedProspects(prospects);
			
			Campaign campaign = campaignRepository.findOneById(campaignBean.getCampaignId());
			logger.info("Camapign [{}] - CampaignJobInterval time is [{}]", campaignBean.getCampaignId(), 1);
			
			//if campaign has active agents then get the list of prospects from api
			if (!campaignBean.isHasActiveAgents()) {
				
				if(queuedCallCount > 0){
					List<String> sidList = getSidList(prospects);
					Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
					Account account = channelTwilio.getChannelConnection();
					for(String sid : sidList){
						Call call = account.getCall(sid);
				    	if (XtaasConstants.TWILIO_CALL_STATUS_RINGING.equals(call.getStatus())) {
				    		call.cancel();
				    		logger.info("Call [{}]  canceling now", sid);
				    	}
					}
				}
				logger.info("run() : CampaignId [{}] does not have any active agents. So not proceeding further queuedCallCount", campaignBean.getCampaignId());
				return;
			}
				StringBuilder availableAgentCounterRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/agentqueue/").append(campaignBean.getCampaignId()).append("/counter");
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Authorization", DialerUtils.generateAuthHeader());
				String availableAgentCounterResponse = null;
				availableAgentCounterResponse = restClientService.makeGetRequest(availableAgentCounterRestUrl.toString(), headers);
				logger.info("getAvailableAgentsCount() : Made Call to url [{}]. Response received : [{}] ", availableAgentCounterRestUrl, availableAgentCounterResponse);
				int availableAgentCount = Integer.valueOf(availableAgentCounterResponse);
				//int campaignLimit = availableAgentCount * campaignBean.getCallSpeedPerAgent();
				
				logger.info("queuedCallCount [{}]. availableAgentCount : [{}] ", queuedCallCount, availableAgentCounterResponse);
				
			
//			int campaignActiveAgentCount = getCampaignActiveAgentCount();
//			int campaignBusyAgentCount = getCampaignBusyAgentCount();
//			if (threadCap == 0) {
//				threadCap = (campaignActiveAgentCount + campaignBusyAgentCount) * campaignBean.getCallSpeedPerAgent();
//			}
//			logger.info("<----- CampaignJob - campaignId [{}], campaignActiveAgentCount [{}] ----->", campaignBean.getCampaignId(), campaignActiveAgentCount);
//			logger.info("<----- campaignBusyAgentCount [{}], threadCap [{}] ----->", campaignBusyAgentCount, threadCap);
			//if(queuedCallCount <= campaignLimit){ 
				//TODO this is shit hack, need to revisit this once dailer stabilizes.
					List<ProspectCallDTO> prospectCallDTOList = getProspectList(queuedCallCount);
					if(prospectCallDTOList!=null){
						logger.info("run() : Received [{}] Prospects to be called.", prospectCallDTOList.size());
						//for each of prospect create calljob 
						List<DialerCallBean> callBeanList = new ArrayList<DialerCallBean>(prospectCallDTOList.size());
						for (ProspectCallDTO prospectCallDTO : prospectCallDTOList) {
							ProspectCall prospectCall = prospectCallDTO.toProspectCall();
							prospectCall.setDialerMode(campaignBean.getDialerMode());
						
							DialerCallBean callBean = new DialerCallBean(prospectCall.getProspectCallId());
							callBean.setCallStatus(DialerCallStatus.QUEUED);
							callBeanList.add(callBean);
						
							CallJob callJob = new CallJob(prospectCall, callBean);
							dialerThreadPool.scheduleJob(callJob, 0);
							logger.debug("run() : Created CallJob for ProspectCall [{}]", prospectCall.getProspectCallId());
						}
						//also create a campaignmonitorjob to monitor the progress on each of the calljob
						if (callBeanList.size() > 0) {
							dialerThreadPool.scheduleJob(this, 5);
			//				CampaignMonitorJob campaignMonitorJob = new CampaignMonitorJob(campaignBean, callBeanList);
			//				dialerThreadPool.scheduleJob(campaignMonitorJob, 3); //start campaign monitor job after 3 secs
		    //				logger.info("run() : Created CampaignMonitorJob for CampaignId [{}] to monitor [{}] calls", campaignBean.getCampaignId(), callBeanList.size());
						} else { //this will occur when there are no prospect available for this campaign. Reschedule this campaign job after 30 secs delay
							dialerThreadPool.scheduleJob(this, 5);
							logger.info("run() : NO PROSPECT AVAILABLE FOR CAMPAIGN [{}]. Rescheduling this CampaignJob to run after 30 secs", campaignBean.getCampaignId());
							//TODO: Send an email notification to supervisor that no prospect available for calling
						}
						logger.info("run() : CampaignJob finished");
					}
			/*} else {
				dialerThreadPool.scheduleJob(this, campaignBean.getCampaignJobInterval());
			}
		} catch (Exception e) {
			logger.error("run(): Error Occurred in CampaingJob for CampaignId " + campaignBean.getCampaignId(), e);
			dialerThreadPool.scheduleJob(this, 5);
		}*/
	}

	/**
	 * @return the campaignBean
	 */
	public DialerCampaignBean getCampaignBean() {
		return campaignBean;
	}

	/**
	 * @param campaignBean the campaignBean to set
	 */
	private void setCampaignBean(DialerCampaignBean campaignBean) {
		this.campaignBean = campaignBean;
	}
	
	@SuppressWarnings("unchecked")
	private List<ProspectCallDTO> getProspectList(int queuedCallCount) {
		StringBuilder prospectListRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/prospect");
		try {
			ProspectSearchDTO prospectSearchDTO = new ProspectSearchDTO();
			prospectSearchDTO.setClause(ProspectSearchClauses.ByCampaign);
			prospectSearchDTO.setCampaignId(this.campaignBean.getCampaignId());
			prospectSearchDTO.setQueuedCallCount(queuedCallCount);
			String queryString = URLEncoder.encode(JSONUtils.toJson(prospectSearchDTO), XtaasConstants.UTF8);
			prospectListRestUrl.append("?searchstring=").append(queryString);
		} catch (IOException e) {
			logger.error("getProspectList() : Error occurred", e);
			dialerThreadPool.scheduleJob(this, 5);
		}
		
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		
		String prospectListResponse = restClientService.makeGetRequest(prospectListRestUrl.toString(), headers);
		List<ProspectCallDTO> prospectList = null;
		List<ProspectCallDTO> prospectCallDTOList = null;
		try {
			// prospectList = JSONUtils.toObject(queuedProspectDTO.get, List.class);
			QueuedProspectDTO queuedProspectDTO = JSONUtils.toObject(prospectListResponse, QueuedProspectDTO.class);
			prospectList = JSONUtils.toObject(JSONUtils.toJson(queuedProspectDTO.getQueuedProspects()), List.class);
			prospectCallDTOList = new ArrayList<ProspectCallDTO>(prospectList.size());
			for (int i=0; i<prospectList.size(); i++) {
				prospectCallDTOList.add(JSONUtils.toObject(JSONUtils.toJson(prospectList.get(i)), ProspectCallDTO.class));
			}
		} catch (IOException e) {
			logger.error("getProspectList() : Error Occurred", e);
			dialerThreadPool.scheduleJob(this, 5);
		}
		logger.debug("getProspectList() : Made Call to url [{}]. Response received : [{}] ", prospectListRestUrl, prospectListResponse);
		
		return prospectCallDTOList;
	}
	
	private int getQueuedProspects(List<ProspectCallLog> prospectCallLogs){
		//List<String> sidList = new ArrayList<String>();
		int queuedCallCount = 0;
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		if (prospectCallLogs == null) {
			return queuedCallCount; 
		}
		for (ProspectCallLog prospectCallLog : prospectCallLogs) {
			Call call = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
			if (call != null) {
				logger.debug("getDialedProspects() : Dialed Call status [{}] for callSid [{}] ", call.getStatus(), call.getSid());
				String twilioCallStatus = call.getStatus();
				if (XtaasConstants.TWILIO_CALL_STATUS_QUEUED.equalsIgnoreCase(twilioCallStatus)
						|| XtaasConstants.TWILIO_CALL_STATUS_RINGING.equalsIgnoreCase(twilioCallStatus)) {
					queuedCallCount = queuedCallCount + 1;
				//	sidList.add(prospectCallLog.getProspectCall().getTwilioCallSid());
				}
			}
		}
		logger.info("getDialedProspects() : queuedCallCount [{}] ",this.campaignBean.getCampaignId()+"-------->"+ queuedCallCount);
		return queuedCallCount;
	}
	
	private List<String> getSidList(List<ProspectCallLog> prospectCallLogs){
		List<String> sidList = new ArrayList<String>();
	//	int queuedCallCount = 0;
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		for (ProspectCallLog prospectCallLog : prospectCallLogs) {
			Call call = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
			if (call != null) {
				logger.debug("getDialedProspects() : Dialed Call status [{}] for callSid [{}] ", call.getStatus(), call.getSid());
				String twilioCallStatus = call.getStatus();
				if (XtaasConstants.TWILIO_CALL_STATUS_QUEUED.equalsIgnoreCase(twilioCallStatus)
						|| XtaasConstants.TWILIO_CALL_STATUS_RINGING.equalsIgnoreCase(twilioCallStatus)) {
				//	queuedCallCount = queuedCallCount + 1;
					sidList.add(prospectCallLog.getProspectCall().getTwilioCallSid());
				}
			}
		}
		
		return sidList;
	}
	
	private List<ProspectCallLog> getDialedProspects() {
		//Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		//Account account = channelTwilio.getChannelConnection();
	//	int queuedCallCount = 0;
		//List<String> sidList = new ArrayList<String>();
		List<String> campaignStatuses = new ArrayList<String>();
		Date todaysDate = null;
		campaignStatuses.add("CALL_DIALED");
		try{
			todaysDate = XtaasDateUtils.getBizTime();
		}catch (Exception e){e.printStackTrace();}
			List<ProspectCallLog> prospectCallLogs = this.prospectCallLogRepository.findByCampaignIdAndStatus(this.campaignBean.getCampaignId(), campaignStatuses, todaysDate);
		//List<ProspectCallLog> prospectCallLogs = prospectCacheServiceImpl.getCachedProspectCallLogList(this.campaignBean.getCampaignId());
			if (prospectCallLogs != null) {
				logger.info("##### getDialedProspects() : prospectCallLogs size [{}] from dialed cache #####", prospectCallLogs.size());
			}
		
		
		/*for (ProspectCallLog prospectCallLog : prospectCallLogs) {
			Call call = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
			if (call != null) {
				logger.debug("getDialedProspects() : Dialed Call status [{}] for callSid [{}] ", call.getStatus(), call.getSid());
				String twilioCallStatus = call.getStatus();
				if (XtaasConstants.TWILIO_CALL_STATUS_QUEUED.equalsIgnoreCase(twilioCallStatus)
						|| XtaasConstants.TWILIO_CALL_STATUS_RINGING.equalsIgnoreCase(twilioCallStatus)) {
					queuedCallCount = queuedCallCount + 1;
					
				}
			}
		}*/
		//if(prospectCallLogs!=null)
		//	queuedCallCount=prospectCallLogs.size();
		
		return prospectCallLogs;
	}
	
	@SuppressWarnings("unchecked")
	private int getCampaignActiveAgentCount() {
		StringBuilder campaignActiveAgentCountUrl = new StringBuilder(
				ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/agentqueue/active");
		// set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		String campaignActiveAgentCountResponse = restClientService
				.makeGetRequest(campaignActiveAgentCountUrl.toString(), headers);
		HashMap<String, Integer> campaignActiveAgentCountMap = new HashMap<>();
		try {
			campaignActiveAgentCountMap = JSONUtils.toObject(campaignActiveAgentCountResponse, HashMap.class);
		} catch (IOException e) {
			logger.error(
					"<----- getCampaignActiveAgentCount() - Erro fetching available agent count. Exception is - [{}] ----->",
					e);
			dialerThreadPool.scheduleJob(this, 5);
		}
		Integer count = 0;
		if (campaignActiveAgentCountMap.size() != 0) {
			if (campaignActiveAgentCountMap.get(campaignBean.getCampaignId()) != null
					|| campaignActiveAgentCountMap.get(campaignBean.getCampaignId()) != 0) {
				count = campaignActiveAgentCountMap.get(campaignBean.getCampaignId());
			}
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	private int getCampaignBusyAgentCount() {
		StringBuilder campaignBusyAgentCountUrl = new StringBuilder(
				ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/agentqueue/busy");
		// set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		String campaignBusyAgentCountResponse = restClientService.makeGetRequest(campaignBusyAgentCountUrl.toString(),
				headers);
		HashMap<String, Integer> campaignBusyAgentCountMap = new HashMap<>();
		try {
			campaignBusyAgentCountMap = JSONUtils.toObject(campaignBusyAgentCountResponse, HashMap.class);
		} catch (IOException e) {
			logger.error(
					"<----- getCampaignBusyAgentCount() - Erro fetching busy agent count. Exception is - [{}] ----->",
					e);
			dialerThreadPool.scheduleJob(this, 5);
		}
		Integer count = 0;
		if (campaignBusyAgentCountMap.size() != 0) {
			if (campaignBusyAgentCountMap.get(campaignBean.getCampaignId()) != null
					|| campaignBusyAgentCountMap.get(campaignBean.getCampaignId()) != 0) {
				count = campaignBusyAgentCountMap.get(campaignBean.getCampaignId());
			}
		}
		return count;
	}
}
