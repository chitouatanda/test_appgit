/**
 * 
 */
package com.xtaas.dialer.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.CampaignService;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.DialerUtils;
import com.xtaas.dialer.bean.DialerCampaignBean;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;

/**
 * Runs at fixed interval (30 secs) and refreshes the list of active campaigns
 * @author djain
 *
 */
@Component
public class MasterJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(MasterJob.class);

	@Autowired
	private RestClientService restClientService;
	
	@Autowired
	private DialerThreadPool dialerThreadPool;
	
	@Autowired
	private CampaignService campaignService;
	
	private HashMap<String, DialerCampaignBean> activeCampaigns;
	
	public MasterJob() {
		activeCampaigns = new HashMap<String, DialerCampaignBean>();
	}
	
	@Override
	public void run() {
		try {
			logger.info("run() : Dialer MasterJob started");
			//first remove the campaigns from activeCampaigns who are not active since last run
			/*Iterator<String> itr = activeCampaigns.keySet().iterator();
	        while(itr.hasNext()){
	        	DialerCampaignBean campaignBean = activeCampaigns.get(itr.next());
	        	if (!campaignBean.isHasActiveAgents()) itr.remove();
	        }
			
			//Get list of Campaigns which have active agents
			List<String> activeCampaignList = getActiveCampaigns();
			if(activeCampaigns!=null){
				logger.info("run() : Found [{}] active campaigns", activeCampaignList.size());
			
				//remove all the campaigns which are active from previous pull, leaving us with the ones non-active
				List<String> nonActiveCampaigns = new ArrayList<String>(activeCampaigns.keySet());
				nonActiveCampaigns.removeAll(activeCampaignList);
			
				//update the hasActiveAgent flag for the nonActiveCampaigns
				for (String campaignId : nonActiveCampaigns) {
					DialerCampaignBean campaignBean = activeCampaigns.get(campaignId);
					campaignBean.setHasActiveAgents(Boolean.FALSE); //set the hasActiveAgent flag to FALSE for the ones which are now not active i.e. not existing in activeCampaignList
				}
			
				//for each of active campaign create CampaignJob
				for (String campaignId : activeCampaignList) {
					//add all the non-existing campaignId to activeCampaigns collections
					if (!activeCampaigns.containsKey(campaignId)) {
						Campaign campaign = campaignService.getCampaign(campaignId);
						DialerCampaignBean campaignBean = new DialerCampaignBean(campaignId, campaign.getDialerMode(),
								Boolean.TRUE,
								campaign.getTeam().getCallSpeedPerMinPerAgent().getValue());
						activeCampaigns.put(campaignId, campaignBean);
						CampaignJob campaignJob = new CampaignJob(activeCampaigns.get(campaignId)); 
						dialerThreadPool.scheduleJob(campaignJob, 0); //TODO: Later on push it to MQ to make it scalable
						logger.info("run() : Created CampaignJob for CampaignId [{}]", campaignId);
					}
				}
			}
			dialerThreadPool.scheduleJob(this, 10);
			logger.info("run() : Dialer MasterJob finished");*/
			} catch (Exception e) {
				logger.error("run(): Error Occurred in MasterJob", e);
				dialerThreadPool.scheduleJob(this, 10);
			}
	} //end run
	
	@SuppressWarnings("unchecked")
	private List<String> getActiveCampaigns() {
		StringBuilder activeCampaignsRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/agentqueue/campaigns");
		
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		
		String activeCampaignsResponse = restClientService.makeGetRequest(activeCampaignsRestUrl.toString(), headers);
		List<String> activeCampaigns = null;
		try {
			activeCampaigns = JSONUtils.toObject(activeCampaignsResponse, List.class);
		} catch (IOException e) {
			logger.error("getActiveCampaigns() : Error Occurred", e);
		}
		logger.info("getActiveCampaigns() : Requested url [{}]. Response received : [{}] ", activeCampaignsRestUrl, activeCampaignsResponse);
		
		return activeCampaigns;
	}
}
