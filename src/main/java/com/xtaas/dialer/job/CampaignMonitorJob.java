/**
 * 
 */
package com.xtaas.dialer.job;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.BeanLocator;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.bean.DialerCallBean;
import com.xtaas.dialer.bean.DialerCallStatus;
import com.xtaas.dialer.bean.DialerCampaignBean;

/**
 * Self replicating Job which checks the status of all the callsids for a campaign
 * @author djain
 */
public class CampaignMonitorJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(CampaignMonitorJob.class);
	private List<DialerCallBean> callBeanList = new ArrayList<DialerCallBean>();
	private DialerCampaignBean campaignBean;
	
	private DialerThreadPool dialerThreadPool;
	
	public CampaignMonitorJob(DialerCampaignBean dialerCampaignBean, List<DialerCallBean> callBeanList) {
		setCallBeanList(callBeanList);
		setDialerCampaignBean(dialerCampaignBean);
		dialerThreadPool = BeanLocator.getBean("dialerThreadPool", DialerThreadPool.class);
	}
	
	@Override
	public void run() {
		try {
			logger.debug("run() : CampaignMonitorJob started for Campaign " + campaignBean.getCampaignId());
			//Traverse thru the list call beans and check if all the calls has been initiated (NOT IN QUEUED or RINGING status)
			boolean isAnyCallQueued = false;
			for (DialerCallBean callBean : callBeanList) {
				if (DialerCallStatus.QUEUED.equals(callBean.getCallStatus()) || DialerCallStatus.RINGING.equals(callBean.getCallStatus())) {
					isAnyCallQueued = true;
					logger.debug("run() : Call to [{}] still QUEUED for Campaign [{}] ", callBean.getProspectCallId(), campaignBean.getCampaignId());
				}
			}
			//if all calls are not initiated then put itself back into queue to be reinvoked after 3 secs
			if (isAnyCallQueued) {
				dialerThreadPool.scheduleJob(this, 3); //start campaign monitor job after 3 secs
				logger.debug("run() : Calls still QUEUED. Recreated CampaignMonitorJob for Campaign [{}] ", campaignBean.getCampaignId());
			} else {
				//if all calls are initiated then create another CampaignJob and put it to the queue
				CampaignJob campaignJob = new CampaignJob(campaignBean); 
				dialerThreadPool.scheduleJob(campaignJob, 0); //TODO: Later on push it to MQ to make it scalable
				logger.debug("run() : All Calls INITIATED. Created CampaignJob for Campaign [{}] ", campaignBean.getCampaignId());
			}
			logger.debug("run() : CampaignMonitorJob finished for Campaign " + campaignBean.getCampaignId());
		} catch (Exception e) {
			logger.error("run(): Error Occurred in CampaignMonitorJob for Campaign " + campaignBean.getCampaignId(), e);
		}
	}

	/**
	 * @return the callBeanList
	 */
	public List<DialerCallBean> getCallBeanList() {
		return callBeanList;
	}

	/**
	 * @param callBeanList the callBeanList to set
	 */
	private void setCallBeanList(List<DialerCallBean> callBeanList) {
		this.callBeanList = callBeanList;
	}

	/**
	 * @param dialerCampaignBean the dialerCampaignBean to set
	 */
	private void setDialerCampaignBean(DialerCampaignBean dialerCampaignBean) {
		this.campaignBean = dialerCampaignBean;
	}

}
