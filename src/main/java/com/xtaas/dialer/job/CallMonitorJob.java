/**
 * 
 */
package com.xtaas.dialer.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.dialer.DialerUtils;
import com.xtaas.dialer.bean.DialerCallBean;
import com.xtaas.dialer.bean.DialerCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.CallLogService;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectCacheServiceImpl;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.XtaasConstants;

/**
 * Self replicating Job (runs every 2 secs) which checks the status of an individual call sid with Twilio and update the status in
 * DialerCallbean. This bean is shared with CampaignMonitoringJob 
 * @author djain
 */
@Component
public class CallMonitorJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(CallMonitorJob.class);
	private DialerCallBean callBean;
	@Autowired
	private RestClientService restClientService;
	@Autowired
	private CallLogService callLogService;
	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private PropertyService propertyService;
	@Autowired
	private CampaignService campaignService;
	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;
	
	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	public CallMonitorJob(){}

	/*public CallMonitorJob(DialerCallBean callBean) {
		setCallBean(callBean);
		restClientService = BeanLocator.getBean("restClientService", RestClientService.class);
		callLogService = BeanLocator.getBean("callLogServiceImpl", CallLogService.class);
		prospectCallLogService = BeanLocator.getBean("prospectCallLogServiceImpl", ProspectCallLogService.class);
		dialerThreadPool = BeanLocator.getBean("dialerThreadPool", DialerThreadPool.class);
		propertyService = BeanLocator.getBean("propertyServiceImpl", PropertyService.class);
	}*/
	
	@Override
	public void run() {
		try {
			List<Campaign> activeCampaigns = campaignService.getActiveCampaigns();
			logger.debug("found active campaigns = " + activeCampaigns.size());
			List<ProspectCallLog> tProspectCallLogs = new ArrayList<>();
			List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
			List<String> mCampaignIds = new ArrayList<String>();
			List<String> campaignIds = new ArrayList<String>();
			for (Campaign campaign : activeCampaigns) {
				campaignIds.add(campaign.getId());
				/*tProspectCallLogs = prospectCacheServiceImpl.getCachedProspectCallLogList(campaign.getId());
				if (prospectCacheServiceImpl.getCachedProspectCallLogList(campaign.getId()) == null || prospectCacheServiceImpl.getCachedProspectCallLogList(campaign.getId()).size() == 0) {
					mCampaignIds.add(campaign.getId());
				}else{
					prospectCallLogs.addAll(tProspectCallLogs);
				}*/
			}
			
			/*if(mCampaignIds.size()>0){
				tProspectCallLogs = prospectCallLogRepositoryImpl.getRingingProspects(mCampaignIds);
				prospectCallLogs.addAll(tProspectCallLogs);
			}*/
			//List<ProspectCallLog> prospectCallLogs = new ArrayList<>();
			//if (prospectCacheServiceImpl.getCachedProspectCallLogList("CALL_DIALED") == null || prospectCacheServiceImpl.getCachedProspectCallLogList("CALL_DIALED").size() == 0) {
				prospectCallLogs = prospectCallLogRepositoryImpl.getRingingProspects(campaignIds);
				/*if (prospectCallLogs != null && prospectCallLogs.size() > 0) {
					for (ProspectCallLog prospectCallLog : prospectCallLogs) {
						prospectCacheServiceImpl.addProspectCallLog("CALL_DIALED", prospectCallLog);
					}
				}
			}*/
			//prospectCallLogs = prospectCacheServiceImpl.getCachedProspectCallLogList("CALL_DIALED");

			//if (callBean == null || DialerCallStatus.COMPLETED.equals(callBean.getCallStatus())) return;
			logger.debug("run() : CallMonitorJob started for Prospects " );  //+ callBean.getProspectCallId()
			//ProspectCallStatus.CALL_DIALED;
			//checks with twilio if the call is over and if not recreate the next call monitoring job
			Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
	    	Account account = channelTwilio.getChannelConnection();
	    	for (ProspectCallLog prospectCallLog : prospectCallLogs) {
	    		Call call = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
	    			callBean = new DialerCallBean(prospectCallLog.getProspectCall().getProspectCallId());
	    			callBean.setTwilioCallSid(prospectCallLog.getProspectCall().getTwilioCallSid());
	    			//callBean.setProspectCallId(prospectCallLog.getProspectCall().getProspectCallId());
	    			if (call == null) {
	    				logger.info("CallSid [{}] does not exist with Twilio", callBean.getTwilioCallSid());
	    				prospectCallLogService.updateCallStatus(callBean.getProspectCallId(), ProspectCallStatus.UNKNOWN_ERROR, callBean.getTwilioCallSid(), null, null); //if null then mark the call as complete as there is no way to track this call
	    				//	prospectCacheServiceImpl.removeProspectCallLog(prospectCallLog.getProspectCall().getCampaignId(), prospectCallLog.getProspectCall().getProspectCallId());
	    				logger.info("##### removing prospectCallLog [{}] from dialed cache ##### ", prospectCallLog.getProspectCall().getProspectCallId());
	    			} else {
	    				logger.debug("CallSid [{}], Status [{}] ", callBean.getTwilioCallSid());
	    				handleTwilioCall(call, account,prospectCallLog);
	    			}
	    	}
	    //	dialerThreadPool.scheduleJob(this, 02); // runs every 10 sec.
		//	logger.info("run() : Dialer CallMonitorJob finished");
	    	
	    	
	    	
	    	/*Call call = account.getCall(callBean.getTwilioCallSid());
	    	if (call == null) {
	    		logger.info("CallSid [{}] does not exist with Twilio", callBean.getTwilioCallSid());
	    		prospectCallLogService.updateCallStatus(callBean.getProspectCallId(), ProspectCallStatus.UNKNOWN_ERROR, callBean.getTwilioCallSid(), null, null); //if null then mark the call as complete as there is no way to track this call
	    	} else {
	    		logger.debug("CallSid [{}], Status [{}] ", callBean.getTwilioCallSid(), call.getStatus());
	    		handleTwilioCall(call, account);
	    	}
	    	dialerThreadPool.scheduleJob(this, 10);
			logger.info("run() : Dialer CallMonitorJob finished");*/
		} catch (Exception e) {
			callBean.setCallStatus(DialerCallStatus.COMPLETED);
			logger.error("run(): Error Occurred in CallMonitorJob", e);
		//	dialerThreadPool.scheduleJob(this, 05);// runs every 10 sec.
		}
			//logger.debug("run() : CallMonitorJob finished for Prospect [{}], CallSid [{}]", callBean.getProspectCallId(), callBean.getTwilioCallSid());
		/*} catch (Exception e) {
			callBean.setCallStatus(DialerCallStatus.COMPLETED);
			logger.error("run(): Error Occurred in CallMonitorJob for Prospect " + callBean.getProspectCallId(), e);
		}*/
	}
	
	private boolean cancelMaxRinging(ProspectCallLog prospectCallLog){
		boolean maxRinging = false;
		int maxCallRingingDurationInSecs = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTODIAL.name(), 30); //secs
		//long callRingingDuration = System.currentTimeMillis() - callBean.getCallInitiatedTime().getTime();
		long callRingingDuration = System.currentTimeMillis() - prospectCallLog.getUpdatedDate().getTime();
	    if (callRingingDuration >= (maxCallRingingDurationInSecs * 1000)) { //if call has been RINGING beyond max call ringing duration
	    	ProspectCallStatus prospectCallStatus = ProspectCallStatus.CALL_CANCELED;
    		prospectCallLogService.updateCallStatus(prospectCallLog.getProspectCall().getProspectCallId(), prospectCallStatus, prospectCallLog.getProspectCall().getTwilioCallSid(), null, null);
    		maxRinging = true;
		    logger.info("In MAX CALL RINGING DURATION");
	    }
	    
	    return maxRinging;
	}
	
	private void handleTwilioCall(Call call, Account account,ProspectCallLog prospectCallLog) throws TwilioRestException {
		String twilioCallStatus = call.getStatus();
		// String twilioCallStatus = XtaasConstants.TWILIO_CALL_STATUS_FAILED;
		/*if (XtaasConstants.TWILIO_CALL_STATUS_RINGING.equals(twilioCallStatus)) {
			//if call is still ringing then check how long it has been ringing and if it is ringing beyond configurable duration then cancel it and mark it as NO-ANSWER
			int maxCallRingingDurationInSecs = propertyService.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION.name(), 45); //secs
			//long callRingingDuration = System.currentTimeMillis() - callBean.getCallInitiatedTime().getTime();
			long callRingingDuration = System.currentTimeMillis() - prospectCallLog.getUpdatedDate().getTime();
		    if (callRingingDuration >= (maxCallRingingDurationInSecs * 1000)) { //if call has been RINGING beyond max call ringing duration
		    	call.cancel(); //then cancel
		    	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error("handleTwilioCall() : Error in Thread sleep", e);
				}
		    	//recheck the call status again, to verify call has hung up
		    	Call callAfterHangup = account.getCall(prospectCallLog.getProspectCall().getTwilioCallSid());
		    	if (XtaasConstants.TWILIO_CALL_STATUS_RINGING.equals(callAfterHangup.getStatus())) {
		    		call.cancel();
		    		logger.info("Call [{}] was still active after hangup, so canceling now", callBean.getTwilioCallSid());
		    	}
		    	ProspectCallStatus prospectCallStatus = getProspectCallStatus(call.getStatus());
		    	if (prospectCallStatus != null) {
		    		prospectCallLogService.updateCallStatus(callBean.getProspectCallId(), prospectCallStatus, this.callBean.getTwilioCallSid(), null, null);
		    	}
			    logger.info("run(): Hungup CALLSID [{}] as it has been RINGING beyond the max duration. Call Status after hangup [{}]", callBean.getTwilioCallSid(), callAfterHangup.getStatus());
		    } else {
		    	logger.debug("CallSid [{}] has been RINGING for [{}] ms", callBean.getTwilioCallSid(), callRingingDuration);
		    }
		    //creates a call monitor job and pass on the callBean
			//callBean.setCallStatus(DialerCallStatus.RINGING);
			//CallMonitorJob callMonitorJob = new CallMonitorJob(this.callBean);
			//dialerThreadPool.scheduleJob(callMonitorJob, 2); //monitor calls after every 2 sec
			logger.debug("run() : Created CallMonitorJob for Prospect [{}], CallSid [{}] ", callBean.getProspectCallId(), callBean.getTwilioCallSid());
		}*/
		/* else if (XtaasConstants.TWILIO_CALL_STATUS_INPROGRESS.equals(twilioCallStatus)) {
			//if conversation is happening between prospect and agent, just reschedule this job object to track when it gets completed
			callBean.setCallStatus(DialerCallStatus.INPROGRESS);
			//CallMonitorJob callMonitorJob = new CallMonitorJob(this.callBean);
			//dialerThreadPool.scheduleJob(callMonitorJob, 10); //monitor conversing calls after every 10 sec, as it is just for monitoring..does not impact dialer calling next set of prospects
			//logger.debug("run() : Created CallMonitorJob for Prospect [{}], CallSid [{}] ", callBean.getProspectCallId(), callBean.getTwilioCallSid());
		} else if (XtaasConstants.TWILIO_CALL_STATUS_QUEUED.equals(twilioCallStatus)) {
			//if call is still queued
			callBean.setCallStatus(DialerCallStatus.QUEUED);
			//CallMonitorJob callMonitorJob = new CallMonitorJob(this.callBean);
			//dialerThreadPool.scheduleJob(callMonitorJob, 5);
			//logger.debug("run() : Created CallMonitorJob for Prospect [{}], CallSid [{}] ", callBean.getProspectCallId(), callBean.getTwilioCallSid());
		} else */
		if (isCallComplete(twilioCallStatus)) {
			handleCallComplete(call,prospectCallLog);
			//If the call is complete, set the call flag to completed, which then will be checked by CampaignMonitoringJob
			//callBean.setCallStatus(DialerCallStatus.COMPLETED);
			//logger.debug("CallSid [{}] COMPLETED. Call Duration [{}] seconds", callBean.getTwilioCallSid(), call.getDuration());
		} 
	}
	
	private void handleCallComplete(Call call, ProspectCallLog prospectCallLog) {
		//on call completion unregisterCallSidWithServer(callSid);
		//unregisterCallSidWithServer(callBean.getTwilioCallSid());
		
		String callStatus =  call.getStatus();
		// String callStatus =  XtaasConstants.TWILIO_CALL_STATUS_FAILED;
		//special handling when call is answered by Machine. Sometimes Twilio does not make call to dial.xml on call pickup. Ex: CA01ae30b56c2bdc23561588e7a6cf17a1
		if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(callStatus) && XtaasConstants.TWILIO_CALL_MACHINE_ANSWERED.equalsIgnoreCase(call.getAnsweredBy()) && call.getRecordings().getPageData().size() == 0) {
			logger.debug("handleCallComplete() : CallSid [{}] answered by machine", call.getSid());
			callStatus = XtaasConstants.TWILIO_CALL_MACHINE_ANSWERED;
    	} else if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(callStatus) && call.getAnsweredBy() == null) {
    		logger.info("handleCallComplete() : CallSid [{}] answered by machine", call.getSid());
			callStatus = XtaasConstants.TWILIO_CALL_MACHINE_ANSWERED;
    	}
		
		//if call is completed but the reason of completion is other than completed (no-answer, busy, failed, cancel, machine-answered but no recording) then save the call log as it will not be saved by DialerController.saveCallMetrics()
		if (!XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(callStatus) ) {
			//save the call log if it is NO-ANSWER or BUSY
			/*HashMap<String, String> callParamMap = new HashMap<String, String>();
	    	callParamMap.put("CallSid", call.getSid());
	    	callParamMap.put("AccountSid", call.getAccountSid());
	    	callParamMap.put("AnsweredBy", call.getAnsweredBy());
	    	callParamMap.put("CallerName", call.getCallerName());
	    	callParamMap.put("Direction", call.getDirection());
	    	callParamMap.put("Duration", call.getDuration());
	    	callParamMap.put("ForwardedFrom", call.getForwardedFrom());
	    	callParamMap.put("From", call.getFrom());
	    	callParamMap.put("ParentCallSid", call.getParentCallSid());
	    	callParamMap.put("PhoneNumberSid", call.getPhoneNumberSid());
	    	callParamMap.put("Price", call.getPrice());
	    	callParamMap.put("CallStatus", call.getStatus());
	    	callParamMap.put("To", call.getTo());
	    	if (call.getEndTime() != null) callParamMap.put("EndTime", call.getEndTime().toString());
	    	if (call.getStartTime() != null) callParamMap.put("StartTime", call.getStartTime().toString());
	    	
	    	logger.debug("Call details: " + callParamMap);
	    	
	    	try {
	    		CallLog callLog = new CallLog();
	    		callLog.setCallLogMap(callParamMap);
	    		callLogService.saveCallLog(callLog);
	    	} catch (Exception ex) {
	    		logger.error("makeCall() : Exception occurred. Call Logs could not be saved. " + callParamMap, ex);
	    	}*/
	    		    	
	    	//update the ProspectCallStatus as well
	    	ProspectCallStatus prospectCallStatus = getProspectCallStatus(callStatus);
	    	if (prospectCallStatus != null) {
	    		prospectCallLogService.updateCallStatus(callBean.getProspectCallId(), prospectCallStatus, this.callBean.getTwilioCallSid(), null, null);
	    	//	prospectCacheServiceImpl.removeProspectCallLog(prospectCallLog.getProspectCall().getCampaignId(),callBean.getProspectCallId());
	    		logger.info("##### removing prospectCallLog [{}] from dialed cache ##### ", prospectCallLog.getProspectCall().getProspectCallId());
	    	}
		}
	}

	private ProspectCallStatus getProspectCallStatus(String twilioCallStatus) {
		if (XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_NO_ANSWER;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_BUSY;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_CANCELED;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_FAILED;
		} else if (XtaasConstants.TWILIO_CALL_MACHINE_ANSWERED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_MACHINE_ANSWERED;
		} else {
			return null;
		}
	}

	private boolean isCallComplete(String twilioCallStatus) {
		if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void unregisterCallSidWithServer(String callSid) {
		StringBuilder callRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/call");
		StringBuilder callSidUrl = new StringBuilder(callRestUrl).append("/").append(callSid);
		
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		
		String unsetCallSidResponse = restClientService.makeDeleteRequest(callSidUrl.toString(), headers);
		logger.info("unregisterCallSidWithServer() : Made Rest Call to unset Prospect's call sid. Response received : {} ", unsetCallSidResponse);
	}
	
	/**
	 * @return the callBean
	 */
	public DialerCallBean getCallBean() {
		return callBean;
	}

	/**
	 * @param callBean the callBean to set
	 */
	private void setCallBean(DialerCallBean callBean) {
		this.callBean = callBean;
	}
}
