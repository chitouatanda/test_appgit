/**
 * 
 */
package com.xtaas.dialer.job;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.list.CallList;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.ActivePreviewCall;
import com.xtaas.db.repository.ActivePreviewCallQueueRepository;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/**
 * A monitoring Job which checks the status of an individual call sid with Twilio and update the status in
 * activepreviewcallqueue collection.
 *  
 * @author djain
 */
@Component
@Scope("prototype")
public class PreviewCallMonitorJob implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(PreviewCallMonitorJob.class);
	
	@Autowired
	private PropertyService propertyService;

	@Autowired
	private ActivePreviewCallQueueRepository activePreviewCallQueueRepository;
	
	private ActivePreviewCall activePreviewCall;
	
	@Override
	public void run() {
		try {
			logger.info("run() : PreviewCallMonitorJob started for CallSid: " + activePreviewCall.getId());
						
			if (this.activePreviewCall == null) return;
			
			if (this.activePreviewCall.isCallCompletedOrCancelled()) {
				activePreviewCallQueueRepository.deleteById(activePreviewCall.getId()); //remove it from the queue
			} else {
				Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		    	Account account = channelTwilio.getChannelConnection();
		    	
				//check if the parent call is still active, if not then cancel the child call as well
				Call parentCall = account.getCall(activePreviewCall.getParentCallSid());
				Call childCall = account.getCall(activePreviewCall.getId());
				if (parentCall == null || isCallComplete(parentCall.getStatus())) { //if parentCall is not active, then cancel child call
					logger.info("ParentCall [{}] is not active. Terminating child call [{}]", activePreviewCall.getParentCallSid(), activePreviewCall.getId());
					Call callAfterHangup = childCall.hangup();
					logger.info("Terminated child call [{}], Status [{}]", callAfterHangup.getSid(), callAfterHangup.getStatus());
					childCall = null;
				}

				//checks with twilio if the call is over 
		    	if (childCall == null) {
		    		logger.info("CallSid [{}] does not exist with Twilio", activePreviewCall.getId());
		    		activePreviewCallQueueRepository.deleteById(activePreviewCall.getId()); //remove it from the queue
		    	} else {
		    		logger.debug("CallSid [{}], Status [{}] ", activePreviewCall.getId(), childCall.getStatus());
		    		handleTwilioCall(childCall);
		    	}
			}
			logger.info("run() : PreviewCallMonitorJob finished for CallSid [{}]", activePreviewCall.getId());
		} catch (Exception e) {
			logger.error("run(): Error Occurred in PreviewCallMonitorJob for CallSid " + activePreviewCall.getId(), e);
		}
	}
	
	private void handleTwilioCall(Call call) throws TwilioRestException {
		String twilioCallStatus = call.getStatus();
		if (isCallComplete(twilioCallStatus)) {
			handleCallComplete(call);
			//If the call is complete, then remove it from the queue
			activePreviewCallQueueRepository.deleteById(activePreviewCall.getId()); //remove it from the queue
			logger.debug("handleTwilioCall() : CallSid [{}] COMPLETED. Call Duration [{}] seconds", activePreviewCall.getId(), call.getDuration());
		} else if (XtaasConstants.TWILIO_CALL_STATUS_RINGING.equals(twilioCallStatus)) {
			//if call is still ringing then check how long it has been ringing and if it is ringing beyond configurable duration then cancel it and mark it as NO-ANSWER
			logger.debug("It has been RINGING for [{}] ms", (System.currentTimeMillis() - activePreviewCall.getCreatedDate().getTime()));
			String currentStatus = XtaasConstants.TWILIO_CALL_STATUS_RINGING;
			int maxCallRingingDurationInSecs = propertyService.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_CALL_RINGING_DURATION_AUTODIAL.name(), 45); //secs
		    if ((System.currentTimeMillis() - activePreviewCall.getCreatedDate().getTime()) > (maxCallRingingDurationInSecs*1000)) { //if call has been RINGING beyond max call ringing duration
		    	String msg = "Call is not answered. Disconnecting";
				messageAgentAndKillConference(msg);
		    	//then Cancel it
				call.cancel();
			    currentStatus = XtaasConstants.TWILIO_CALL_STATUS_CANCELED;
			    logger.info("handleTwilioCall() : Canceled CALLSID [{}] as it has been RINGING beyond the max duration", activePreviewCall.getId());
		    } /*else {
			    String msg = "Call is Ringing";
			    announceMessageinConferenceRoom(msg);
		    }*/
		    
		    //update the status in database
		    ActivePreviewCall activePreviewCallFromDB = activePreviewCallQueueRepository.findOneById(activePreviewCall.getId());
		    if (activePreviewCallFromDB != null) {
		    	activePreviewCallFromDB.setCallStatus(currentStatus);
		    	activePreviewCallFromDB.setProcessing(false);
		    	activePreviewCallQueueRepository.save(activePreviewCallFromDB); //update the current status to Ringing
		    }
		} else if (XtaasConstants.TWILIO_CALL_STATUS_INPROGRESS.equals(twilioCallStatus)) {
			//if conversation is happening between prospect and agent then remove this call from monitoring queue
			activePreviewCallQueueRepository.deleteById(activePreviewCall.getId()); //remove it from the queue
			logger.info("handleTwilioCall() : Call [{}] status in-progress. Removing from Queue.", activePreviewCall.getId());
		}
	}
	
	@SuppressWarnings("unused")
	private void announceMessageinConferenceRoom(String msg) {
		Call confCall = findConferenceCall(activePreviewCall.getAgentId()); 
		if (confCall != null) { //play message in this conference call
			Map<String, String> agentsCallParams = new HashMap<String, String>();
			try {
				StringBuilder dialXMLUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/twiml/dialtoconf.xml.jsp");
	            dialXMLUrl.append("?cn=").append(URLEncoder.encode(activePreviewCall.getAgentId(), "UTF-8"));
	            dialXMLUrl.append("&pcid=").append(activePreviewCall.getProspectCallId());
	            dialXMLUrl.append("&msg=").append(URLEncoder.encode(msg, "UTF-8"));
	            
				agentsCallParams.put("Url", dialXMLUrl.toString());
			} catch (UnsupportedEncodingException e) {
				logger.error("announceMessageinConferenceRoom() : Exception occurred while requesting dialtoconf.xml.jsp", e);
			}
			agentsCallParams.put("Method", "GET");
			try {
				logger.debug("announceMessageinConferenceRoom() : Calling dialtoconf.xml for agentId: " + activePreviewCall.getAgentId());
				confCall.update(agentsCallParams);
			} catch (TwilioRestException tre) {
				if (tre.getErrorCode() == 21220) { //Call is not in-progress. Cannot redirect.
					handleCallNotInProgressException();
				} else {
					logger.error("announceMessageinConferenceRoom() : Exception occurred", tre);
				}
			} catch (Exception e) {
				logger.error("announceMessageinConferenceRoom() : Exception occurred", e);
			}
		}
	}
	
	private void handleCallComplete(Call call) {
		//if call is completed but the reason of completion is other than completed (no-answer, busy, failed, cancel) then play the message in the conference for agent to know about it
		if (!XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(call.getStatus()) ) {
			String msg = "Cannot reach prospect. Call status " + call.getStatus();
			messageAgentAndKillConference(msg);
		}
	}
	
	/**
	 * Say Message to Agent and kill the conference room
	 */
	private void messageAgentAndKillConference(String msg) {
		String confName = activePreviewCall.getAgentId() + "_" + activePreviewCall.getProspectCallId();
		Call confCall = findConferenceCall(confName);
		if (confCall != null) {
			Map<String, String> agentsCallParams = new HashMap<String, String>();
			try {
				agentsCallParams.put("Url", ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/twiml/saymessage.xml.jsp?msg="+URLEncoder.encode(msg,"UTF-8"));
			} catch (UnsupportedEncodingException e) {
				logger.error("run() : Exception occurred while requesting saymessage.xml.jsp", e);
			}
			agentsCallParams.put("Method", "GET");
			try {
				logger.debug("run() : Playing message [{}] for agentId [{}] ", msg, activePreviewCall.getAgentId());
				confCall.update(agentsCallParams);
			} catch (TwilioRestException tre) {
				if (tre.getErrorCode() == 21220) { //Call is not in-progress. Cannot redirect.
					handleCallNotInProgressException();
				} else {
					logger.error("handleCallComplete() : Exception occurred", tre);
				}
			} catch (Exception e) {
				logger.error("run() : Exception occurred", e);
			}
		}
	}
	
	private Call findConferenceCall(String confName) {
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		Map<String, String> filter = new HashMap<String, String>();
		filter.put("From", "client:"+confName);
		filter.put("Status", "in-progress");
		CallList confCallList = account.getCalls(filter); 
		
		if (confCallList != null) {
			for (Call inProgressCall : confCallList) {
			    return inProgressCall; //return the first one only..there shouldn't be more
			}
		}
		return null;
	}
	
	private void handleCallNotInProgressException() {
		int prepTimeForCallInitiation = 10;
		//check if this call is added recently and twilio might not have initiated the call yet. wait for few secs before removing it from queue
		if ((System.currentTimeMillis() - activePreviewCall.getCreatedDate().getTime()) > (prepTimeForCallInitiation*1000)) { 
			logger.info("handleCallComplete() : Call is no longer Active. Removing CallSid from Queue. CallSid: " + activePreviewCall.getId());
			activePreviewCallQueueRepository.deleteById(activePreviewCall.getId()); //remove it from the queue
		}
	}

	private boolean isCallComplete(String twilioCallStatus) {
		if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus) ||
				XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param callBean the callBean to set
	 */
	public void setActivePreviewCall(ActivePreviewCall activePreviewCall) {
		this.activePreviewCall = activePreviewCall;
	}
}
