/**
 * 
 */
package com.xtaas.dialer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Conference;
import com.twilio.sdk.resource.list.ConferenceList;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;

/**
 * Class responsible to query data from Twilio.
 * 
 * @author djain
 *
 */
@Component
public class TwilioQuerier {
	
	private static final Logger logger = LoggerFactory.getLogger(TwilioQuerier.class);
	
	public ConferenceList getInProgressConferences() {
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
    	Account account = channelTwilio.getChannelConnection();
    	//Build a filter for the ConferenceList
    	Map<String, String> params = new HashMap<String, String>();
    	params.put("Status", "in-progress");
    	ConferenceList conferences = account.getConferences(params);
    	return conferences;
	}
	
	public Set<String> getInProgressConferenceNames() {
		ConferenceList conferences = getInProgressConferences();
		
		Set<String> inProgressConferenceNameSet = new HashSet<String>();
		for (Conference conference : conferences) {
			inProgressConferenceNameSet.add(conference.getFriendlyName());
		}
		logger.debug("getInProgressConferenceNames() : Total In-Progress Conferences found: " + inProgressConferenceNameSet.size());
		return inProgressConferenceNameSet;
	}
}
