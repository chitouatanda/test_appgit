package com.xtaas.dialer;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.Prospect;

public class LeadCallback implements Delayed {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(LeadCallback.class);
	
	//callback Time in MS after adding the delay
	private long callbackTimeInMS;
		
	//lead which needs to be callback
	private ProspectCall prospectCall;
	
	public LeadCallback(ProspectCall prospectCall) {
		Prospect prospect = prospectCall.getProspect();
		this.setCallbackTimeInMS(prospect.getCallbackTimeInMS());
		this.setProspectCall(prospectCall);
	}

	/**
	 * @return the callbackInMS
	 */
	public long getCallbackTimeInMS() {
		return callbackTimeInMS;
	}

	/**
	 * @param callbackInMS the callbackInMS to set
	 */
	public void setCallbackTimeInMS(long callbackTimeInMS) {
		this.callbackTimeInMS = callbackTimeInMS;
	}

	@Override
	public int compareTo(Delayed o) {
		if (this.callbackTimeInMS < ((LeadCallback)o).getCallbackTimeInMS()) 
			return -1; //as delay time for current object is less than the specified object
		else if (this.callbackTimeInMS > ((LeadCallback)o).getCallbackTimeInMS())
			return 1;  //as delay time for current object is more than the specified object
		
		return 0; //as delay time for current object is same as the specified object
	}

	@Override
	public long getDelay(TimeUnit unit) {
		long delayTime = callbackTimeInMS - System.currentTimeMillis();
		return unit.convert(delayTime, TimeUnit.MILLISECONDS);
	}

	/**
	 * @return the prospectCall
	 */
	public ProspectCall getProspectCall() {
		return prospectCall;
	}

	/**
	 * @param prospectCall the prospectCall to set
	 */
	public void setProspectCall(ProspectCall prospectCall) {
		this.prospectCall = prospectCall;
	}
}
