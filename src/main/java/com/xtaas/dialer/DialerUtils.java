package com.xtaas.dialer;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.AuthTokenGeneratorService;
import com.xtaas.infra.security.AppManager.App;

public class DialerUtils {

	private static AuthTokenGeneratorService authTokenGeneratorService;
	
	/**
	 * Generates token in the format appname:data:hash where,
	 * 	appname is the name of the application which is generating the token
	 *  data is two-way encrypted data using the app's secret key. Data is in format username|org|role|time in MS 
	 *  hash is one-way encryption of data using the app's secret key
	 * @return token
	 */
	public static String generateAuthHeader() {
		/*long timeInMillis = System.currentTimeMillis();
        String authData = "dialer|XTAAS|ROLE_SYSTEM|" + timeInMillis; // username | org | role | time in MS
		return App.DIALER.name() + ":" + EncryptionUtils.encrypt(authData,  AppManager.getSecurityKey(App.DIALER)) + ":" + EncryptionUtils.generateHash(authData, AppManager.getSecurityKey(App.DIALER));
		*/
		return getAuthTokenGeneratorService().generateSystemToken(App.DIALER.name(), "dialer");
	}
	
	private static AuthTokenGeneratorService getAuthTokenGeneratorService() {
		if (authTokenGeneratorService == null) {
			authTokenGeneratorService = BeanLocator.getBean("authTokenGeneratorServiceImpl", AuthTokenGeneratorService.class);
		}
		return authTokenGeneratorService;
	}
}
