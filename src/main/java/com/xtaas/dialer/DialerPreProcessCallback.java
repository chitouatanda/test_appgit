/**
 * 
 */
package com.xtaas.dialer;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author djain
 *
 */
public class DialerPreProcessCallback implements Delayed {
	
	private String campaignId;
	
	private String agentUserName;
	
	//callback Time in MS with the delay
	private long callbackTimeInMS;

	@Override
	public int compareTo(Delayed o) {
		if (this.callbackTimeInMS < ((DialerPreProcessCallback)o).getCallbackTimeInMS()) 
			return -1; //as delay time for current object is less than the specified object
		else if (this.callbackTimeInMS > ((DialerPreProcessCallback)o).getCallbackTimeInMS())
			return 1;  //as delay time for current object is more than the specified object
		
		return 0; //as delay time for current object is same as the specified object
	}

	@Override
	public long getDelay(TimeUnit unit) {
		long delayTime = callbackTimeInMS - System.currentTimeMillis();
		return unit.convert(delayTime, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * @return the callbackInMS
	 */
	public long getCallbackTimeInMS() {
		return callbackTimeInMS;
	}

	/**
	 * @param callbackInMS the callbackInMS to set
	 */
	public void setCallbackTimeInMS(long callbackTimeInMS) {
		this.callbackTimeInMS = callbackTimeInMS;
	}
	
	public String getCampaignId() {
		return this.campaignId;
	}
	
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	public String getAgentUserName() {
		return this.agentUserName;
	}
	
	public void setAgentUserName(String agentUserName) {
		this.agentUserName = agentUserName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((agentUserName == null) ? 0 : agentUserName.hashCode());
		result = prime * result
				+ ((campaignId == null) ? 0 : campaignId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DialerPreProcessCallback other = (DialerPreProcessCallback) obj;
		if (agentUserName == null) {
			if (other.agentUserName != null)
				return false;
		} else if (!agentUserName.equals(other.agentUserName))
			return false;
		if (campaignId == null) {
			if (other.campaignId != null)
				return false;
		} else if (!campaignId.equals(other.campaignId))
			return false;
		return true;
	}
}
