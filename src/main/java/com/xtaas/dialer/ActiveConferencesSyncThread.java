/**
 * 
 */
package com.xtaas.dialer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * This thread fetches active conferences from AgentRegistrationService and send notification to Supervisor dashboard to present 
 * near real-time view
 * 
 * @author djain
 */
@Component
public class ActiveConferencesSyncThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ActiveConferencesSyncThread.class);
	
	//@Autowired
	//private ActiveConferencesManager activeConferenceManager;
	
	//@Autowired
	//private AgentRegistrationService agentRegistrationService;
	
	@Override
	public void run() {
		logger.debug("run() : ActiveConferencesSyncThread started");
		while (!Thread.interrupted()) {
			try {
				//logger.debug("run(): Sync Active Conferences for Supervisor dashboard - STARTED");
				//Set<String> activeConferences = twilioQuerier.getInProgressConferenceNames();
				/*Set<String> activeConferences = agentRegistrationService.getLiveAgentScreenNames();
				Set<String> activeConferencesFromPreviousPull = activeConferenceManager.getActiveConferenceNames();
				if (null == activeConferencesFromPreviousPull) {
					activeConferencesFromPreviousPull = new HashSet<String>(activeConferences);
				}*/
				//logger.debug("1. activeConferencesFromPreviousPull = " + activeConferencesFromPreviousPull);
				//1. find the delta between activeConferences and from previous pull, to find out new and completed conferences
				//1a. remove all the conferences which are still active from previous pull, leaving us with the ones completed
				/*Set<String> completedConferenceSet = new HashSet<String>(activeConferencesFromPreviousPull);
				completedConferenceSet.removeAll(activeConferences);*/ 
				//logger.debug("2. completedConferenceSet = " + completedConferenceSet);
				
				//logger.debug("3. activeConferences = " + activeConferences);
				//1b. remove all the conferences which were there in previous pull from current pull, leaving us with the ones newly started
				//Set<String> newConferenceSet = new HashSet<String>(activeConferences);
				//newConferenceSet.removeAll(activeConferencesFromPreviousPull);
				//logger.debug("4. newConferenceSet = " + newConferenceSet);
				
				//2. Add the conferences which are in newConferenceSet and Remove the conferences which are in completedConferenceSet from Supervior Dashboard view
				//2a. Remove the conferences which are no longer active (the ones in completedConferenceSet)
				/*for (String conferenceName : completedConferenceSet) {
					SimplePusherMessage spMsg = new SimplePusherMessage(conferenceName, "", "remove");
					pushMessage(spMsg);
				}*/
				
				//2b. Add the newly active conferences
				/*for (String conferenceName : newConferenceSet) {
					SimplePusherMessage spMsg = new SimplePusherMessage(conferenceName, conferenceName, "add");
					pushMessage(spMsg);
				}*/
				
				//3. Set most recent activeConferences in ActiveConferenceManager
				//activeConferenceManager.setActiveConferenceNames(activeConferences);
				
				//logger.debug("run(): Sync Active Conferences for Supervisor dashboard - DONE");
				Thread.sleep(2000);
			} catch (Exception e) {
				logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
			}
		}
	}
	
	/*private void pushMessage(SimplePusherMessage data) throws JsonGenerationException, JsonMappingException, IOException  {
		ObjectMapper mapper = new ObjectMapper();
		String msg = mapper.writeValueAsString(data);
		logger.debug(msg);
		Pusher.triggerPush("subscriber_dashboard", "active_conferences", msg);
	}*/
}