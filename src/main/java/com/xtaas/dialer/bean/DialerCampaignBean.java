package com.xtaas.dialer.bean;

import com.xtaas.domain.valueobject.DialerMode;

public class DialerCampaignBean {
	
	private String campaignId;
	private boolean hasActiveAgents;
	private DialerMode dialerMode;
	private int callSpeedPerAgent;
	
	public DialerCampaignBean(String campaignId, DialerMode dialerMode, boolean hasActiveAgents,
			int callSpeedPerAgent) {
		setCampaignId(campaignId);
		setDialerMode(dialerMode);
		setHasActiveAgents(hasActiveAgents);
		setCallSpeedPerAgent(callSpeedPerAgent);
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	private void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the hasActiveAgents
	 */
	public boolean isHasActiveAgents() {
		return hasActiveAgents;
	}

	/**
	 * @param hasActiveAgents the hasActiveAgents to set
	 */
	public void setHasActiveAgents(boolean hasActiveAgents) {
		this.hasActiveAgents = hasActiveAgents;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((campaignId == null) ? 0 : campaignId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DialerCampaignBean other = (DialerCampaignBean) obj;
		if (campaignId == null) {
			if (other.campaignId != null)
				return false;
		} else if (!campaignId.equals(other.campaignId))
			return false;
		return true;
	}

	/**
	 * @return the dialerMode
	 */
	public DialerMode getDialerMode() {
		return dialerMode;
	}

	/**
	 * @param dialerMode the dialerMode to set
	 */
	private void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public int getCallSpeedPerAgent() {
		return callSpeedPerAgent;
	}

	private void setCallSpeedPerAgent(int callSpeedPerAgent) {
		this.callSpeedPerAgent = callSpeedPerAgent;
	}
}
