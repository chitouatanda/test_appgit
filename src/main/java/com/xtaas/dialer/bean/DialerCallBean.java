package com.xtaas.dialer.bean;

import java.util.Date;

public class DialerCallBean {
	private String twilioCallSid;
	private DialerCallStatus callStatus;
	private String prospectCallId;
	private Date callInitiatedTime;
	
	public DialerCallBean(String prospectCallId) {
		setProspectCallId(prospectCallId);
	}
	
	/**
	 * @return the twilioCallSid
	 */
	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	/**
	 * @param twilioCallSid the twilioCallSid to set
	 */
	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	/**
	 * @return the callStatus
	 */
	public DialerCallStatus getCallStatus() {
		return callStatus;
	}

	/**
	 * @param callStatus the callStatus to set
	 */
	public void setCallStatus(DialerCallStatus callStatus) {
		this.callStatus = callStatus;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the callInitiatedTime
	 */
	public Date getCallInitiatedTime() {
		return callInitiatedTime;
	}

	/**
	 * @param callInitiatedTime the callInitiatedTime to set
	 */
	public void setCallInitiatedTime(Date callInitiatedTime) {
		this.callInitiatedTime = callInitiatedTime;
	}
}
