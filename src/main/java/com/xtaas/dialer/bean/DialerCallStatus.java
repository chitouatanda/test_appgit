package com.xtaas.dialer.bean;

public enum DialerCallStatus {
	QUEUED, RINGING, INPROGRESS, COMPLETED
}
