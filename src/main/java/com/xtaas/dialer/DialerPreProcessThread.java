/**
 * 
 */
package com.xtaas.dialer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.RestClientService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;


/**
 * Thread which marks Prospect records with:
 * 	 CallTimeRestriction if they are out of business hours, (DISABLED on July 6th, 2016)
 *   MaxRetryLimitReached for prospects which reached MaxRetryLimit
 * @author djain
 *
 */
@Component
public class DialerPreProcessThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(DialerPreProcessThread.class);
	
	@Autowired
	private ProspectCallLogService prospectCallLogService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private RestClientService restClientService;
	
	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			logger.debug("run(): DialerPreProcessThread started");
			
			while (!Thread.interrupted()) {
				try {
					logger.debug("run(): Dialer PreProcessing started");
					List<String> activeCampaigns = getActiveCampaigns(); //find active campaigns and only process them
					logger.debug("run() : Active Campaigns found : [{}]", StringUtils.join(activeCampaigns, ",") );
					
					if(activeCampaigns!=null){
						//Set MAX_RETRY_LIMIT_REACHED for record which have call retry count reached to MAX_RETRY_LIMIT
						for (String activeCampaignId : activeCampaigns) { //for each of the active campaign
							int recordsUpdated = 100;
							while (recordsUpdated > 0) {
								recordsUpdated = processProspectWithMaxRetryLimit(activeCampaignId);
								logger.debug("run() : Records updated with MAX_RETRY_LIMIT_REACHED : [{}]", recordsUpdated );
							}
						}
					
						logger.debug("run() : MAX_RETRY_LIMIT_REACHED processing done.");
					//Set Call_TIME_RESTRICTION for records which does not have business hour open
					//HashMap<String, Date> stateCodesCallbackDateMap = getStateCodesWithBusinessClosed();
					//for each of the closed state find its next callback date 
					/*for (String activeCampaignId : activeCampaigns) { //for each of the active campaign
						int recordsUpdatedWithCallTimeRestrictions = 100;
						while (recordsUpdatedWithCallTimeRestrictions > 0) { //one campaign might have multiple batches for a campaignId
							recordsUpdatedWithCallTimeRestrictions = processProspectWithCallTimeRestiction(activeCampaignId, stateCodesCallbackDateMap);
							logger.info("run() : Records updated with CALL_TIME_RESTRICTION : [{}]", recordsUpdatedWithCallTimeRestrictions );
						}
					}*/
					
					//logger.info("run() : CALL_TIME_RESTRICTION processing done");
					/*logger.info("run() : CALL_TIME_RESTRICTION processing done. Now starting with REQUEUING of callable CALL_TIME_RESTRICTION records");
					//REQUEUE records with CALL_TIME_RESTRICTION whenever the calbackDate has expired
					for (String activeCampaignId : activeCampaigns) { //for each of the active campaign
						//find prospects with callback expired and then update their status back as QUEUED
						int recordsRequeued = 100;
						while (recordsRequeued > 0) {
							recordsRequeued = processProspectWithExpiredCallTimeRestriction(activeCampaignId);
							logger.info("run() : Records updated with CALL_TIME_RESTRICTION : [{}]", recordsRequeued);
						}
					}*/
					
					logger.debug("run(): Dialer PreProcessing finished. Sleeping for 10 mins");
					}
					//Sleep for 10 minutes
					Thread.sleep(10*60*1000);
				} catch (Exception e) {
					logger.error("Error Occurred: ", e);
				}
				
			} //end while
		} catch (Exception e) {
			logger.error("run(): Error Occurred in DialerPreProcessThread. DialerPreProcessThread is DEAD. Please restart DIALER.", e);
		} 
	}
	
	@SuppressWarnings("unused")
	private int processProspectWithExpiredCallTimeRestriction(String campaignId) {
		//find prospects with callback expired and then update their status back as QUEUED (batch of 100)
		Pageable callbackPageable = PageRequest.of(0, 100, Direction.ASC, "callbackDate"); //Batch of 100 records ordered by oldest callbackDate
		List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findByCallTimeRestrictionAndExpiredCallback(new Date(), campaignId, callbackPageable);
		
		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			logger.debug("processProspectWithExpiredCallTimeRestriction() : REQUEUING ProspectCallId [{}]. CallbackDate [{}]", prospectCallLog.getProspectCall().getProspectCallId(), prospectCallLog.getCallbackDate());
			prospectCallLogService.updateCallStatus(prospectCallLog.getProspectCall().getProspectCallId(), ProspectCallStatus.QUEUED, null, null, null);
		}
		
		return prospectCallLogList.size();
	}
	
	private int processProspectWithCallTimeRestiction(String campaignId, HashMap<String, Date> stateCodesCallbackDateMap) {
		Pageable pageable = PageRequest.of(0, 100, Direction.ASC, "updatedDate"); //Batch of 100 records ordered by updated last
		
		//find all the prospects who are in dialing queue and their business hr is closed
		List<ProspectCallLog> callableProspects = prospectCallLogRepository.findCallableProspectsByStateCodes(campaignId, new ArrayList<String>(stateCodesCallbackDateMap.keySet()), pageable);
		
		for (ProspectCallLog prospectCallLog : callableProspects) { //set the CALLBACK_SYSTEM_GENERATED for all these prospects
			prospectCallLogService.updateCallbackStatusSystemGenerated(prospectCallLog.getProspectCall().getProspectCallId(), stateCodesCallbackDateMap.get(prospectCallLog.getProspectCall().getProspect().getStateCode()));
			logger.debug("processProspectWithCallTimeRestiction() : Updated ProspectCallId [{}] to CALL_TIME_RESTRICTION", prospectCallLog.getProspectCall().getProspectCallId());
		}
		
		return callableProspects.size();
	}
	
	private int processProspectWithMaxRetryLimit(String campaignId) {
		//find prospects which has reached max retry limit (batch of 100)
		Pageable pageable = PageRequest.of(0, 100, Direction.ASC, "updatedDate"); //Batch of 100 records ordered by updated last

		Campaign campaign = campaignRepository.findOneById(campaignId);
		int callMaxRetriesLimit = 5;
		if (campaign != null && campaign.getCallMaxRetries() > 0) {
			callMaxRetriesLimit = campaign.getCallMaxRetries(); // get call max retry count from campaign
		} else {
			callMaxRetriesLimit = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.CALL_MAX_RETRIES.name(), 5);
		}
		List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findByMaxCallRetryLimit(campaignId, callMaxRetriesLimit, pageable);
		
		for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			prospectCallLogService.updateCallStatus(prospectCallLog.getProspectCall().getProspectCallId(), ProspectCallStatus.MAX_RETRY_LIMIT_REACHED, null, null, null);
			logger.debug("processProspectWithMaxRetryLimit() : Updated ProspectCallId [{}] to MAX_RETRY_LIMIT_REACHED", prospectCallLog.getProspectCall().getProspectCallId());
		}
		
		return prospectCallLogList.size();
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getActiveCampaigns() {
		StringBuilder activeCampaignsRestUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/api/agentqueue/campaigns");
		
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", DialerUtils.generateAuthHeader());
		
		String activeCampaignsResponse = restClientService.makeGetRequest(activeCampaignsRestUrl.toString(), headers);
		List<String> activeCampaigns = null;
		try {
			activeCampaigns = JSONUtils.toObject(activeCampaignsResponse, List.class);
		} catch (IOException e) {
			logger.error("getActiveCampaigns() : Error Occurred", e);
		}
		logger.debug("getActiveCampaigns() : Requested url [{}]. Response received : [{}] ", activeCampaignsRestUrl, activeCampaignsResponse);
		
		return activeCampaigns;
	}
	
	private HashMap<String, Date> getStateCodesWithBusinessClosed() {
		HashMap<String, Date> stateCodesCallbackDateMap = new HashMap<String, Date>();
		
		List<StateCallConfig> stateCallConfigList = stateCallConfigRepository.findAll();
		for (StateCallConfig stateCallConfig : stateCallConfigList) {
			try {
				Date currentDate = new Date();
				String strCurrDateInStateTZ = XtaasDateUtils.convertToTimeZone(currentDate, stateCallConfig.getTimezone(), XtaasDateUtils.DATE_FORMAT_DEFAULT);
				
				//don't call if it is holiday
				if (stateCallConfig.getHolidayDates().contains(strCurrDateInStateTZ)) {
					//logger.info("getStateCodesWithBusinessClosed() : State [{}] cannot be called on Holidays. Date in State TZ : " + strCurrDateInStateTZ, stateCallConfig.getStateCode());
					DateTime callbackDate = XtaasDateUtils.getNextDaysDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get next days date in prospect's TZ
					Date callbackDateInUTC = XtaasDateUtils.changeTimeZone(callbackDate, DateTimeZone.UTC.getID()).toDate();
					
					stateCodesCallbackDateMap.put(stateCallConfig.getStateCode(), callbackDateInUTC);
					continue;
				}
				
				//check call time restrictions, if current time does not falls within the legal call time
				int currentHour = Calendar.getInstance(TimeZone.getTimeZone(stateCallConfig.getTimezone())).get(Calendar.HOUR_OF_DAY);
				if (currentHour < stateCallConfig.getStartCallHour() || currentHour >= stateCallConfig.getEndCallHour()) {
					//logger.info("getStateCodesWithBusinessClosed() : State [{}] cannot be called out of call hour restrictions. Date in State TZ : " + strCurrDateInStateTZ + ", CurrentHour: " + currentHour + ", Allowed range: " + stateCallConfig.getStartCallHour() + " - " + stateCallConfig.getEndCallHour(), stateCallConfig.getStateCode());
					DateTime callbackDate;
					if (currentHour < stateCallConfig.getStartCallHour()) {
						callbackDate = XtaasDateUtils.getCurrentDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get current day in prospect's TZ
					} else {
						callbackDate = XtaasDateUtils.getNextDaysDateInTZ(stateCallConfig.getStartCallHour(), stateCallConfig.getTimezone()); //get next days date in prospect's TZ
					}
					Date callbackDateInUTC = XtaasDateUtils.changeTimeZone(callbackDate, DateTimeZone.UTC.getID()).toDate();
					
					stateCodesCallbackDateMap.put(stateCallConfig.getStateCode(), callbackDateInUTC);
					continue;
				}
			} catch (ParseException e) {
				logger.error("Exception occurred during checking call restrictions.", e);
			}
		} //end for
		
		return stateCodesCallbackDateMap;
	}
}
