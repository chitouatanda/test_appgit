/**
 * 
 */
package com.xtaas.dialer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.Prospect;

/**
 * @author djain
 *
 */
@Component
public class DialerCallbackQueueMonitorThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(DialerCallbackQueueMonitorThread.class);
	
	@Autowired
    private DialerCallbackQueueManager dialerCallbackQueueManager;
	
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.debug("run(): Invoking DialerCallbackQueueMonitorThread");
		try
		{
			while (!Thread.interrupted())
			{
				logger.debug("run(): Again monitoring callback queue to get next Lead whose callback time has arrived.");
				
				//get the lead from the callbackqueue whose delay has expired. This is a blocking call
				ProspectCall prospectCall = dialerCallbackQueueManager.getLeadToCall();
				Prospect lead = prospectCall.getProspect();
				logger.debug("run(): Callback time arrived for lead " + lead);
				
				//erase the callback time
				lead.setCallbackTimeInMS(null);
				//add the lender from callback queue to main queue
				//leadQueueService.addLead(prospectCall);
			
				logger.debug("run(): Lead added to main queue. " + lead );
			}
		} catch (InterruptedException iex) {
			logger.error("run() : Exception occurred in DialerCallbackQueueMonitorThread.", iex);
		}
		logger.debug("run(): DialerCallbackQueueMonitorThread finished execution.");
	}

}
