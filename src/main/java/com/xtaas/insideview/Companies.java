package com.xtaas.insideview;

import java.util.List;

public class Companies {

	private String name;
	private String companyType;
	private String companyStatus;
	private List<Tickers> tickers;
	private String industry;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String phone;
	private String revenue;
	private String employees;
	private String subsidiary;
	private String sic;
	private String sicDescription;
	private String naics;
	private String naicsDescription;
	private String financialYearEnd;
	private String equifaxId;
	private String fortuneRanking;
	private String mostRecentQuarter;
	private String revenueCurrency;
	private String industryCode;
	private List<BritishSics> britishSics;
	private String foundationDate;
	private String companyFacebookProfile;
	private String companyTwitterProfile;
	private String companyBlogProfile;
	private List<String> sources;
	private List<String> websites;
	private String companyId;
	
	
	public String getName() {
		return name;
	}
	public String getCompanyType() {
		return companyType;
	}
	public String getCompanyStatus() {
		return companyStatus;
	}
	public List<Tickers> getTickers() {
		return tickers;
	}
	public String getIndustry() {
		return industry;
	}
	public String getStreet() {
		return street;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public String getZip() {
		return zip;
	}
	public String getCountry() {
		return country;
	}
	public String getPhone() {
		return phone;
	}
	public String getRevenue() {
		return revenue;
	}
	public String getEmployees() {
		return employees;
	}
	public String getSubsidiary() {
		return subsidiary;
	}
	public String getSic() {
		return sic;
	}
	public String getSicDescription() {
		return sicDescription;
	}
	public String getNaics() {
		return naics;
	}
	public String getNaicsDescription() {
		return naicsDescription;
	}
	public String getFinancialYearEnd() {
		return financialYearEnd;
	}
	public String getEquifaxId() {
		return equifaxId;
	}
	public String getFortuneRanking() {
		return fortuneRanking;
	}
	public String getMostRecentQuarter() {
		return mostRecentQuarter;
	}
	public String getRevenueCurrency() {
		return revenueCurrency;
	}
	public String getIndustryCode() {
		return industryCode;
	}
	public List<BritishSics> getBritishSics() {
		return britishSics;
	}
	public String getFoundationDate() {
		return foundationDate;
	}
	public String getCompanyFacebookProfile() {
		return companyFacebookProfile;
	}
	public String getCompanyTwitterProfile() {
		return companyTwitterProfile;
	}
	public String getCompanyBlogProfile() {
		return companyBlogProfile;
	}
	public List<String> getSources() {
		return sources;
	}
	public List<String> getWebsites() {
		return websites;
	}
	public String getCompanyId() {
		return companyId;
	}
	
	
	

}
