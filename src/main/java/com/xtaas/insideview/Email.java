package com.xtaas.insideview;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Email {

@JsonProperty("email")
private String email;
@JsonProperty("validationStatus")
private String validationStatus;

@JsonProperty("email")
public String getEmail() {
return email;
}

@JsonProperty("email")
public void setEmail(String email) {
this.email = email;
}

@JsonProperty("validationStatus")
public String getValidationStatus() {
return validationStatus;
}

@JsonProperty("validationStatus")
public void setValidationStatus(String validationStatus) {
this.validationStatus = validationStatus;
}

}