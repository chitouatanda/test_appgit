package com.xtaas.insideview;

import java.util.List;

public class CompanyResponse {
	int page;
	int resultsPerPage;
	int totalResults;
	List<Companies>  companies;
	public int getPage() {
		return page;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public int getTotalResults() {
		return totalResults;
	}
	public List<Companies> getCompanies() {
		return companies;
	}
	
	

}
