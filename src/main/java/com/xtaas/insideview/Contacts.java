package com.xtaas.insideview;

import java.util.List;

public class Contacts {

	private String contactId;
	private String firstName;
	private String lastName;
	private String imageUrl;
	private String companyId;
	private String companyName;
	private String confidenceScore;
	private String corporate;
	private String direct;
	private String email;
	private String emailValidStatus;
	private String description;
	private String twitterHandle;
	private String linkedinHandle;
	private String age;
	private String salary;
	private String salaryCurrency;
	private String active;
	private List<String> jobLevels;
	private List<String> jobFunctions;
	private String peopleId;
	private String fullName;
	private List<String> titles;
	private List<String> sources;
	private List<Education> education;
	private String id;
	private String city;
	private String state;
	private String country;
	
	public String getContactId() {
		return contactId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public String getCompanyId() {
		return companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getConfidenceScore() {
		return confidenceScore;
	}
	public String getCorporate() {
		return corporate;
	}
	public String getDirect() {
		return direct;
	}
	public String getEmail() {
		return email;
	}
	public String getEmailValidStatus() {
		return emailValidStatus;
	}
	public String getDescription() {
		return description;
	}
	public String getTwitterHandle() {
		return twitterHandle;
	}
	public String getLinkedinHandle() {
		return linkedinHandle;
	}
	public String getAge() {
		return age;
	}
	public String getSalary() {
		return salary;
	}
	public String getSalaryCurrency() {
		return salaryCurrency;
	}
	public String getActive() {
		return active;
	}
	public List<String> getJobLevels() {
		return jobLevels;
	}
	public List<String> getJobFunctions() {
		return jobFunctions;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public String getFullName() {
		return fullName;
	}
	public List<String> getTitles() {
		return titles;
	}
	public List<String> getSources() {
		return sources;
	}
	public List<Education> getEducation() {
		return education;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
