package com.xtaas.insideview;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class DetailedContactJobResultInsideView
{
    private String COMPANY_wbe;

    private String education;

    private String COMPANY_lgbt;

    private String COMPANY_name;

    private String companyName;

    private String source;

    private String COMPANY_employees;

    private String salary;

    private String COMPANY_companyStatus;

    private String COMPANY_revenueCurrency;

    private String COMPANY_mbe;

    private String peopleId;

    private String COMPANY_subIndustryCode;

    private String COMPANY_country;

    private String COMPANY_fortuneRanking;

    private String COMPANY_revenueRange;

    private String COMPANY_naics;

    private String COMPANY_employeeRange;

    private String jobLevels;

    private String COMPANY_revenue;

    private String COMPANY_fax;

    private String COMPANY_subsidiary;

    private String contactId;

    private String COMPANY_zip;

    private String COMPANY_subIndustry;

    private String COMPANY_companyLinkedInProfile;

    private String COMPANY_state;

    private String active;

    private String COMPANY_website;

    private String COMPANY_city;

    private String COMPANY_ticker;

    private String COMPANY_sicDescription;

    private String corporatePhone;

    private String COMPANY_parentCompanyCountry;

    private String firstName;

    private String COMPANY_ultimateParentCompanyName;

    private String companyId;

    private String phone;

    private String twitterHandle;

    private String COMPANY_parentCompanyName;

    private String COMPANY_ethnicity;

    private String jobFunctions;

    private String COMPANY_companyFacebookProfile;

    private String lastName;

    private String COMPANY_sources;

    private String COMPANY_companyBlogProfile;

    private String description;

    private String COMPANY_companyType;

    private String COMPANY_street;

    private String COMPANY_vbe;

    private String COMPANY_ultimateParentCompanyId;

    private String confidenceScore;

    private String COMPANY_longitude;

    private String COMPANY_britishSics;

    private String COMPANY_industry;

    private String COMPANY_companyTaxId;

    private String imageUrl;

    private String COMPANY_description;

    private String linkedinHandle;

    private String COMPANY_phone;

    private String directPhone;

    private String COMPANY_mostRecentQuarter;

    private String COMPANY_ultimateParentCompanyCountry;

    private String email;

    private String COMPANY_id;

    private String COMPANY_dbe;

    private String COMPANY_latitude;

    private String COMPANY_industryCode;

    private String COMPANY_naicsDescription;

    private String COMPANY_financialYearEnd;

    private String salaryCurrency;

    private String fullName;

    private String titles;

    private String emailValidationStatus;

    private String facebookHandle;

    private String COMPANY_sic;

    private String COMPANY_equifaxId;

    private String COMPANY_companyTwitterProfile;

    private String COMPANY_parentCompanyId;

    private String COMPANY_disabled;

    private String middleName;

    private String COMPANY_foundationDate;

    private String COMPANY_gender;

    private String age;

    public String getCOMPANY_wbe ()
    {
        return COMPANY_wbe;
    }

    public void setCOMPANY_wbe (String COMPANY_wbe)
    {
        this.COMPANY_wbe = COMPANY_wbe;
    }

    public String getEducation ()
    {
        return education;
    }

    public void setEducation (String education)
    {
        this.education = education;
    }

    public String getCOMPANY_lgbt ()
    {
        return COMPANY_lgbt;
    }

    public void setCOMPANY_lgbt (String COMPANY_lgbt)
    {
        this.COMPANY_lgbt = COMPANY_lgbt;
    }

    public String getCOMPANY_name ()
    {
        return COMPANY_name;
    }

    public void setCOMPANY_name (String COMPANY_name)
    {
        this.COMPANY_name = COMPANY_name;
    }

    public String getCompanyName ()
    {
        return companyName;
    }

    public void setCompanyName (String companyName)
    {
        this.companyName = companyName;
    }

    public String getSource ()
    {
        return source;
    }

    public void setSource (String source)
    {
        this.source = source;
    }

    public String getCOMPANY_employees ()
    {
        return COMPANY_employees;
    }

    public void setCOMPANY_employees (String COMPANY_employees)
    {
        this.COMPANY_employees = COMPANY_employees;
    }

    public String getSalary ()
    {
        return salary;
    }

    public void setSalary (String salary)
    {
        this.salary = salary;
    }

    public String getCOMPANY_companyStatus ()
    {
        return COMPANY_companyStatus;
    }

    public void setCOMPANY_companyStatus (String COMPANY_companyStatus)
    {
        this.COMPANY_companyStatus = COMPANY_companyStatus;
    }

    public String getCOMPANY_revenueCurrency ()
    {
        return COMPANY_revenueCurrency;
    }

    public void setCOMPANY_revenueCurrency (String COMPANY_revenueCurrency)
    {
        this.COMPANY_revenueCurrency = COMPANY_revenueCurrency;
    }

    public String getCOMPANY_mbe ()
    {
        return COMPANY_mbe;
    }

    public void setCOMPANY_mbe (String COMPANY_mbe)
    {
        this.COMPANY_mbe = COMPANY_mbe;
    }

    public String getPeopleId ()
    {
        return peopleId;
    }

    public void setPeopleId (String peopleId)
    {
        this.peopleId = peopleId;
    }

    public String getCOMPANY_subIndustryCode ()
    {
        return COMPANY_subIndustryCode;
    }

    public void setCOMPANY_subIndustryCode (String COMPANY_subIndustryCode)
    {
        this.COMPANY_subIndustryCode = COMPANY_subIndustryCode;
    }

    public String getCOMPANY_country ()
    {
        return COMPANY_country;
    }

    public void setCOMPANY_country (String COMPANY_country)
    {
        this.COMPANY_country = COMPANY_country;
    }

    public String getCOMPANY_fortuneRanking ()
    {
        return COMPANY_fortuneRanking;
    }

    public void setCOMPANY_fortuneRanking (String COMPANY_fortuneRanking)
    {
        this.COMPANY_fortuneRanking = COMPANY_fortuneRanking;
    }

    public String getCOMPANY_revenueRange ()
    {
        return COMPANY_revenueRange;
    }

    public void setCOMPANY_revenueRange (String COMPANY_revenueRange)
    {
        this.COMPANY_revenueRange = COMPANY_revenueRange;
    }

    public String getCOMPANY_naics ()
    {
        return COMPANY_naics;
    }

    public void setCOMPANY_naics (String COMPANY_naics)
    {
        this.COMPANY_naics = COMPANY_naics;
    }

    public String getCOMPANY_employeeRange ()
    {
        return COMPANY_employeeRange;
    }

    public void setCOMPANY_employeeRange (String COMPANY_employeeRange)
    {
        this.COMPANY_employeeRange = COMPANY_employeeRange;
    }

    public String getJobLevels ()
    {
        return jobLevels;
    }

    public void setJobLevels (String jobLevels)
    {
        this.jobLevels = jobLevels;
    }

    public String getCOMPANY_revenue ()
    {
        return COMPANY_revenue;
    }

    public void setCOMPANY_revenue (String COMPANY_revenue)
    {
        this.COMPANY_revenue = COMPANY_revenue;
    }

    public String getCOMPANY_fax ()
    {
        return COMPANY_fax;
    }

    public void setCOMPANY_fax (String COMPANY_fax)
    {
        this.COMPANY_fax = COMPANY_fax;
    }

    public String getCOMPANY_subsidiary ()
    {
        return COMPANY_subsidiary;
    }

    public void setCOMPANY_subsidiary (String COMPANY_subsidiary)
    {
        this.COMPANY_subsidiary = COMPANY_subsidiary;
    }

    public String getContactId ()
    {
        return contactId;
    }

    public void setContactId (String contactId)
    {
        this.contactId = contactId;
    }

    public String getCOMPANY_zip ()
    {
        return COMPANY_zip;
    }

    public void setCOMPANY_zip (String COMPANY_zip)
    {
        this.COMPANY_zip = COMPANY_zip;
    }

    public String getCOMPANY_subIndustry ()
    {
        return COMPANY_subIndustry;
    }

    public void setCOMPANY_subIndustry (String COMPANY_subIndustry)
    {
        this.COMPANY_subIndustry = COMPANY_subIndustry;
    }

    public String getCOMPANY_companyLinkedInProfile ()
    {
        return COMPANY_companyLinkedInProfile;
    }

    public void setCOMPANY_companyLinkedInProfile (String COMPANY_companyLinkedInProfile)
    {
        this.COMPANY_companyLinkedInProfile = COMPANY_companyLinkedInProfile;
    }

    public String getCOMPANY_state ()
    {
        return COMPANY_state;
    }

    public void setCOMPANY_state (String COMPANY_state)
    {
        this.COMPANY_state = COMPANY_state;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public String getCOMPANY_website ()
    {
        return COMPANY_website;
    }

    public void setCOMPANY_website (String COMPANY_website)
    {
        this.COMPANY_website = COMPANY_website;
    }

    public String getCOMPANY_city ()
    {
        return COMPANY_city;
    }

    public void setCOMPANY_city (String COMPANY_city)
    {
        this.COMPANY_city = COMPANY_city;
    }

    public String getCOMPANY_ticker ()
    {
        return COMPANY_ticker;
    }

    public void setCOMPANY_ticker (String COMPANY_ticker)
    {
        this.COMPANY_ticker = COMPANY_ticker;
    }

    public String getCOMPANY_sicDescription ()
    {
        return COMPANY_sicDescription;
    }

    public void setCOMPANY_sicDescription (String COMPANY_sicDescription)
    {
        this.COMPANY_sicDescription = COMPANY_sicDescription;
    }

    public String getCorporatePhone ()
    {
        return corporatePhone;
    }

    public void setCorporatePhone (String corporatePhone)
    {
        this.corporatePhone = corporatePhone;
    }

    public String getCOMPANY_parentCompanyCountry ()
    {
        return COMPANY_parentCompanyCountry;
    }

    public void setCOMPANY_parentCompanyCountry (String COMPANY_parentCompanyCountry)
    {
        this.COMPANY_parentCompanyCountry = COMPANY_parentCompanyCountry;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getCOMPANY_ultimateParentCompanyName ()
    {
        return COMPANY_ultimateParentCompanyName;
    }

    public void setCOMPANY_ultimateParentCompanyName (String COMPANY_ultimateParentCompanyName)
    {
        this.COMPANY_ultimateParentCompanyName = COMPANY_ultimateParentCompanyName;
    }

    public String getCompanyId ()
    {
        return companyId;
    }

    public void setCompanyId (String companyId)
    {
        this.companyId = companyId;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getTwitterHandle ()
    {
        return twitterHandle;
    }

    public void setTwitterHandle (String twitterHandle)
    {
        this.twitterHandle = twitterHandle;
    }

    public String getCOMPANY_parentCompanyName ()
    {
        return COMPANY_parentCompanyName;
    }

    public void setCOMPANY_parentCompanyName (String COMPANY_parentCompanyName)
    {
        this.COMPANY_parentCompanyName = COMPANY_parentCompanyName;
    }

    public String getCOMPANY_ethnicity ()
    {
        return COMPANY_ethnicity;
    }

    public void setCOMPANY_ethnicity (String COMPANY_ethnicity)
    {
        this.COMPANY_ethnicity = COMPANY_ethnicity;
    }

    public String getJobFunctions ()
    {
        return jobFunctions;
    }

    public void setJobFunctions (String jobFunctions)
    {
        this.jobFunctions = jobFunctions;
    }

    public String getCOMPANY_companyFacebookProfile ()
    {
        return COMPANY_companyFacebookProfile;
    }

    public void setCOMPANY_companyFacebookProfile (String COMPANY_companyFacebookProfile)
    {
        this.COMPANY_companyFacebookProfile = COMPANY_companyFacebookProfile;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getCOMPANY_sources ()
    {
        return COMPANY_sources;
    }

    public void setCOMPANY_sources (String COMPANY_sources)
    {
        this.COMPANY_sources = COMPANY_sources;
    }

    public String getCOMPANY_companyBlogProfile ()
    {
        return COMPANY_companyBlogProfile;
    }

    public void setCOMPANY_companyBlogProfile (String COMPANY_companyBlogProfile)
    {
        this.COMPANY_companyBlogProfile = COMPANY_companyBlogProfile;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getCOMPANY_companyType ()
    {
        return COMPANY_companyType;
    }

    public void setCOMPANY_companyType (String COMPANY_companyType)
    {
        this.COMPANY_companyType = COMPANY_companyType;
    }

    public String getCOMPANY_street ()
    {
        return COMPANY_street;
    }

    public void setCOMPANY_street (String COMPANY_street)
    {
        this.COMPANY_street = COMPANY_street;
    }

    public String getCOMPANY_vbe ()
    {
        return COMPANY_vbe;
    }

    public void setCOMPANY_vbe (String COMPANY_vbe)
    {
        this.COMPANY_vbe = COMPANY_vbe;
    }

    public String getCOMPANY_ultimateParentCompanyId ()
    {
        return COMPANY_ultimateParentCompanyId;
    }

    public void setCOMPANY_ultimateParentCompanyId (String COMPANY_ultimateParentCompanyId)
    {
        this.COMPANY_ultimateParentCompanyId = COMPANY_ultimateParentCompanyId;
    }

    public String getConfidenceScore ()
    {
        return confidenceScore;
    }

    public void setConfidenceScore (String confidenceScore)
    {
        this.confidenceScore = confidenceScore;
    }

    public String getCOMPANY_longitude ()
    {
        return COMPANY_longitude;
    }

    public void setCOMPANY_longitude (String COMPANY_longitude)
    {
        this.COMPANY_longitude = COMPANY_longitude;
    }

    public String getCOMPANY_britishSics ()
    {
        return COMPANY_britishSics;
    }

    public void setCOMPANY_britishSics (String COMPANY_britishSics)
    {
        this.COMPANY_britishSics = COMPANY_britishSics;
    }

    public String getCOMPANY_industry ()
    {
        return COMPANY_industry;
    }

    public void setCOMPANY_industry (String COMPANY_industry)
    {
        this.COMPANY_industry = COMPANY_industry;
    }

    public String getCOMPANY_companyTaxId ()
    {
        return COMPANY_companyTaxId;
    }

    public void setCOMPANY_companyTaxId (String COMPANY_companyTaxId)
    {
        this.COMPANY_companyTaxId = COMPANY_companyTaxId;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }

    public void setImageUrl (String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getCOMPANY_description ()
    {
        return COMPANY_description;
    }

    public void setCOMPANY_description (String COMPANY_description)
    {
        this.COMPANY_description = COMPANY_description;
    }

    public String getLinkedinHandle ()
    {
        return linkedinHandle;
    }

    public void setLinkedinHandle (String linkedinHandle)
    {
        this.linkedinHandle = linkedinHandle;
    }

    public String getCOMPANY_phone ()
    {
        return COMPANY_phone;
    }

    public void setCOMPANY_phone (String COMPANY_phone)
    {
        this.COMPANY_phone = COMPANY_phone;
    }

    public String getDirectPhone ()
    {
        return directPhone;
    }

    public void setDirectPhone (String directPhone)
    {
        this.directPhone = directPhone;
    }

    public String getCOMPANY_mostRecentQuarter ()
    {
        return COMPANY_mostRecentQuarter;
    }

    public void setCOMPANY_mostRecentQuarter (String COMPANY_mostRecentQuarter)
    {
        this.COMPANY_mostRecentQuarter = COMPANY_mostRecentQuarter;
    }

    public String getCOMPANY_ultimateParentCompanyCountry ()
    {
        return COMPANY_ultimateParentCompanyCountry;
    }

    public void setCOMPANY_ultimateParentCompanyCountry (String COMPANY_ultimateParentCompanyCountry)
    {
        this.COMPANY_ultimateParentCompanyCountry = COMPANY_ultimateParentCompanyCountry;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getCOMPANY_id ()
    {
        return COMPANY_id;
    }

    public void setCOMPANY_id (String COMPANY_id)
    {
        this.COMPANY_id = COMPANY_id;
    }

    public String getCOMPANY_dbe ()
    {
        return COMPANY_dbe;
    }

    public void setCOMPANY_dbe (String COMPANY_dbe)
    {
        this.COMPANY_dbe = COMPANY_dbe;
    }

    public String getCOMPANY_latitude ()
    {
        return COMPANY_latitude;
    }

    public void setCOMPANY_latitude (String COMPANY_latitude)
    {
        this.COMPANY_latitude = COMPANY_latitude;
    }

    public String getCOMPANY_industryCode ()
    {
        return COMPANY_industryCode;
    }

    public void setCOMPANY_industryCode (String COMPANY_industryCode)
    {
        this.COMPANY_industryCode = COMPANY_industryCode;
    }

    public String getCOMPANY_naicsDescription ()
    {
        return COMPANY_naicsDescription;
    }

    public void setCOMPANY_naicsDescription (String COMPANY_naicsDescription)
    {
        this.COMPANY_naicsDescription = COMPANY_naicsDescription;
    }

    public String getCOMPANY_financialYearEnd ()
    {
        return COMPANY_financialYearEnd;
    }

    public void setCOMPANY_financialYearEnd (String COMPANY_financialYearEnd)
    {
        this.COMPANY_financialYearEnd = COMPANY_financialYearEnd;
    }

    public String getSalaryCurrency ()
    {
        return salaryCurrency;
    }

    public void setSalaryCurrency (String salaryCurrency)
    {
        this.salaryCurrency = salaryCurrency;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    public String getTitles ()
    {
        return titles;
    }

    public void setTitles (String titles)
    {
        this.titles = titles;
    }

    public String getEmailValidationStatus ()
    {
        return emailValidationStatus;
    }

    public void setEmailValidationStatus (String emailValidationStatus)
    {
        this.emailValidationStatus = emailValidationStatus;
    }

    public String getFacebookHandle ()
    {
        return facebookHandle;
    }

    public void setFacebookHandle (String facebookHandle)
    {
        this.facebookHandle = facebookHandle;
    }

    public String getCOMPANY_sic ()
    {
        return COMPANY_sic;
    }

    public void setCOMPANY_sic (String COMPANY_sic)
    {
        this.COMPANY_sic = COMPANY_sic;
    }

    public String getCOMPANY_equifaxId ()
    {
        return COMPANY_equifaxId;
    }

    public void setCOMPANY_equifaxId (String COMPANY_equifaxId)
    {
        this.COMPANY_equifaxId = COMPANY_equifaxId;
    }

    public String getCOMPANY_companyTwitterProfile ()
    {
        return COMPANY_companyTwitterProfile;
    }

    public void setCOMPANY_companyTwitterProfile (String COMPANY_companyTwitterProfile)
    {
        this.COMPANY_companyTwitterProfile = COMPANY_companyTwitterProfile;
    }

    public String getCOMPANY_parentCompanyId ()
    {
        return COMPANY_parentCompanyId;
    }

    public void setCOMPANY_parentCompanyId (String COMPANY_parentCompanyId)
    {
        this.COMPANY_parentCompanyId = COMPANY_parentCompanyId;
    }

    public String getCOMPANY_disabled ()
    {
        return COMPANY_disabled;
    }

    public void setCOMPANY_disabled (String COMPANY_disabled)
    {
        this.COMPANY_disabled = COMPANY_disabled;
    }

    public String getMiddleName ()
    {
        return middleName;
    }

    public void setMiddleName (String middleName)
    {
        this.middleName = middleName;
    }

    public String getCOMPANY_foundationDate ()
    {
        return COMPANY_foundationDate;
    }

    public void setCOMPANY_foundationDate (String COMPANY_foundationDate)
    {
        this.COMPANY_foundationDate = COMPANY_foundationDate;
    }

    public String getCOMPANY_gender ()
    {
        return COMPANY_gender;
    }

    public void setCOMPANY_gender (String COMPANY_gender)
    {
        this.COMPANY_gender = COMPANY_gender;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [COMPANY_wbe = "+COMPANY_wbe+", education = "+education+", COMPANY_lgbt = "+COMPANY_lgbt+", COMPANY_name = "+COMPANY_name+", companyName = "+companyName+", source = "+source+", COMPANY_employees = "+COMPANY_employees+", salary = "+salary+", COMPANY_companyStatus = "+COMPANY_companyStatus+", COMPANY_revenueCurrency = "+COMPANY_revenueCurrency+", COMPANY_mbe = "+COMPANY_mbe+", peopleId = "+peopleId+", COMPANY_subIndustryCode = "+COMPANY_subIndustryCode+", COMPANY_country = "+COMPANY_country+", COMPANY_fortuneRanking = "+COMPANY_fortuneRanking+", COMPANY_revenueRange = "+COMPANY_revenueRange+", COMPANY_naics = "+COMPANY_naics+", COMPANY_employeeRange = "+COMPANY_employeeRange+", jobLevels = "+jobLevels+", COMPANY_revenue = "+COMPANY_revenue+", COMPANY_fax = "+COMPANY_fax+", COMPANY_subsidiary = "+COMPANY_subsidiary+", contactId = "+contactId+", COMPANY_zip = "+COMPANY_zip+", COMPANY_subIndustry = "+COMPANY_subIndustry+", COMPANY_companyLinkedInProfile = "+COMPANY_companyLinkedInProfile+", COMPANY_state = "+COMPANY_state+", active = "+active+", COMPANY_website = "+COMPANY_website+", COMPANY_city = "+COMPANY_city+", COMPANY_ticker = "+COMPANY_ticker+", COMPANY_sicDescription = "+COMPANY_sicDescription+", corporatePhone = "+corporatePhone+", COMPANY_parentCompanyCountry = "+COMPANY_parentCompanyCountry+", firstName = "+firstName+", COMPANY_ultimateParentCompanyName = "+COMPANY_ultimateParentCompanyName+", companyId = "+companyId+", phone = "+phone+", twitterHandle = "+twitterHandle+", COMPANY_parentCompanyName = "+COMPANY_parentCompanyName+", COMPANY_ethnicity = "+COMPANY_ethnicity+", jobFunctions = "+jobFunctions+", COMPANY_companyFacebookProfile = "+COMPANY_companyFacebookProfile+", lastName = "+lastName+", COMPANY_sources = "+COMPANY_sources+", COMPANY_companyBlogProfile = "+COMPANY_companyBlogProfile+", description = "+description+", COMPANY_companyType = "+COMPANY_companyType+", COMPANY_street = "+COMPANY_street+", COMPANY_vbe = "+COMPANY_vbe+", COMPANY_ultimateParentCompanyId = "+COMPANY_ultimateParentCompanyId+", confidenceScore = "+confidenceScore+", COMPANY_longitude = "+COMPANY_longitude+", COMPANY_britishSics = "+COMPANY_britishSics+", COMPANY_industry = "+COMPANY_industry+", COMPANY_companyTaxId = "+COMPANY_companyTaxId+", imageUrl = "+imageUrl+", COMPANY_description = "+COMPANY_description+", linkedinHandle = "+linkedinHandle+", COMPANY_phone = "+COMPANY_phone+", directPhone = "+directPhone+", COMPANY_mostRecentQuarter = "+COMPANY_mostRecentQuarter+", COMPANY_ultimateParentCompanyCountry = "+COMPANY_ultimateParentCompanyCountry+", email = "+email+", COMPANY_id = "+COMPANY_id+", COMPANY_dbe = "+COMPANY_dbe+", COMPANY_latitude = "+COMPANY_latitude+", COMPANY_industryCode = "+COMPANY_industryCode+", COMPANY_naicsDescription = "+COMPANY_naicsDescription+", COMPANY_financialYearEnd = "+COMPANY_financialYearEnd+", salaryCurrency = "+salaryCurrency+", fullName = "+fullName+", titles = "+titles+", emailValidationStatus = "+emailValidationStatus+", facebookHandle = "+facebookHandle+", COMPANY_sic = "+COMPANY_sic+", COMPANY_equifaxId = "+COMPANY_equifaxId+", COMPANY_companyTwitterProfile = "+COMPANY_companyTwitterProfile+", COMPANY_parentCompanyId = "+COMPANY_parentCompanyId+", COMPANY_disabled = "+COMPANY_disabled+", middleName = "+middleName+", COMPANY_foundationDate = "+COMPANY_foundationDate+", COMPANY_gender = "+COMPANY_gender+", age = "+age+"]";
    }
}
	