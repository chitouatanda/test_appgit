package com.xtaas.insideview;

import java.util.List;

public class PersonResponse {

	private List<Contacts> contacts;
	int page;
	int resultsPerPage;
	int totalResults;
	public List<Contacts> getContacts() {
		return contacts;
	}
	public int getPage() {
		return page;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public int getTotalResults() {
		return totalResults;
	}
	
	
	
}
