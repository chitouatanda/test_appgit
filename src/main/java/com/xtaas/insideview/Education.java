package com.xtaas.insideview;

public class Education {

	private String  degree;
	private String major;
	private String university;
	
	public String getDegree() {
		return degree;
	}
	public String getMajor() {
		return major;
	}
	public String getUniversity() {
		return university;
	}
	
	
}
