package com.xtaas.insideview;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyDetails {

@JsonProperty("name")
private String name;
@JsonProperty("companyType")
private String companyType;
@JsonProperty("companyStatus")
private String companyStatus;
@JsonProperty("industry")
private String industry;
@JsonProperty("subIndustry")
private String subIndustry;
@JsonProperty("street")
private String street;
@JsonProperty("city")
private String city;
@JsonProperty("state")
private String state;
@JsonProperty("zip")
private String zip;
@JsonProperty("country")
private String country;
@JsonProperty("phone")
private String phone;
@JsonProperty("revenue")
private String revenue;
@JsonProperty("employees")
private String employees;
@JsonProperty("subsidiary")
private String subsidiary;
@JsonProperty("sic")
private String sic;
@JsonProperty("sicDescription")
private String sicDescription;
@JsonProperty("naics")
private String naics;
@JsonProperty("naicsDescription")
private String naicsDescription;
@JsonProperty("parentCompanyId")
private String parentCompanyId;
@JsonProperty("ultimateParentId")
private String ultimateParentId;
@JsonProperty("equifaxId")
private String equifaxId;
@JsonProperty("revenueCurrency")
private String revenueCurrency;
@JsonProperty("industryCode")
private String industryCode;
@JsonProperty("subIndustryCode")
private String subIndustryCode;
@JsonProperty("britishSics")
private List<BritishSics> britishSics = null;
@JsonProperty("parentCompanyCountry")
private String parentCompanyCountry;
@JsonProperty("ultimateParentCompanyCountry")
private String ultimateParentCompanyCountry;
@JsonProperty("ultimateParentCompanyName")
private String ultimateParentCompanyName;
@JsonProperty("parentCompanyName")
private String parentCompanyName;
@JsonProperty("foundationDate")
private String foundationDate;
@JsonProperty("companyFacebookProfile")
private String companyFacebookProfile;
@JsonProperty("companyTwitterProfile")
private String companyTwitterProfile;
@JsonProperty("companyBlogProfile")
private String companyBlogProfile;
@JsonProperty("latitude")
private String latitude;
@JsonProperty("longitude")
private String longitude;
@JsonProperty("sources")
private List<String> sources = null;
@JsonProperty("companyId")
private Long companyId;
@JsonProperty("websites")
private List<String> websites = null;

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("companyType")
public String getCompanyType() {
return companyType;
}

@JsonProperty("companyType")
public void setCompanyType(String companyType) {
this.companyType = companyType;
}

@JsonProperty("companyStatus")
public String getCompanyStatus() {
return companyStatus;
}

@JsonProperty("companyStatus")
public void setCompanyStatus(String companyStatus) {
this.companyStatus = companyStatus;
}

@JsonProperty("industry")
public String getIndustry() {
return industry;
}

@JsonProperty("industry")
public void setIndustry(String industry) {
this.industry = industry;
}

@JsonProperty("subIndustry")
public String getSubIndustry() {
return subIndustry;
}

@JsonProperty("subIndustry")
public void setSubIndustry(String subIndustry) {
this.subIndustry = subIndustry;
}

@JsonProperty("street")
public String getStreet() {
return street;
}

@JsonProperty("street")
public void setStreet(String street) {
this.street = street;
}

@JsonProperty("city")
public String getCity() {
return city;
}

@JsonProperty("city")
public void setCity(String city) {
this.city = city;
}

@JsonProperty("state")
public String getState() {
return state;
}

@JsonProperty("state")
public void setState(String state) {
this.state = state;
}

@JsonProperty("zip")
public String getZip() {
return zip;
}

@JsonProperty("zip")
public void setZip(String zip) {
this.zip = zip;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonProperty("phone")
public String getPhone() {
return phone;
}

@JsonProperty("phone")
public void setPhone(String phone) {
this.phone = phone;
}

@JsonProperty("revenue")
public String getRevenue() {
return revenue;
}

@JsonProperty("revenue")
public void setRevenue(String revenue) {
this.revenue = revenue;
}

@JsonProperty("employees")
public String getEmployees() {
return employees;
}

@JsonProperty("employees")
public void setEmployees(String employees) {
this.employees = employees;
}

@JsonProperty("subsidiary")
public String getSubsidiary() {
return subsidiary;
}

@JsonProperty("subsidiary")
public void setSubsidiary(String subsidiary) {
this.subsidiary = subsidiary;
}

@JsonProperty("sic")
public String getSic() {
return sic;
}

@JsonProperty("sic")
public void setSic(String sic) {
this.sic = sic;
}

@JsonProperty("sicDescription")
public String getSicDescription() {
return sicDescription;
}

@JsonProperty("sicDescription")
public void setSicDescription(String sicDescription) {
this.sicDescription = sicDescription;
}

@JsonProperty("naics")
public String getNaics() {
return naics;
}

@JsonProperty("naics")
public void setNaics(String naics) {
this.naics = naics;
}

@JsonProperty("naicsDescription")
public String getNaicsDescription() {
return naicsDescription;
}

@JsonProperty("naicsDescription")
public void setNaicsDescription(String naicsDescription) {
this.naicsDescription = naicsDescription;
}

@JsonProperty("parentCompanyId")
public String getParentCompanyId() {
return parentCompanyId;
}

@JsonProperty("parentCompanyId")
public void setParentCompanyId(String parentCompanyId) {
this.parentCompanyId = parentCompanyId;
}

@JsonProperty("ultimateParentId")
public String getUltimateParentId() {
return ultimateParentId;
}

@JsonProperty("ultimateParentId")
public void setUltimateParentId(String ultimateParentId) {
this.ultimateParentId = ultimateParentId;
}

@JsonProperty("equifaxId")
public String getEquifaxId() {
return equifaxId;
}

@JsonProperty("equifaxId")
public void setEquifaxId(String equifaxId) {
this.equifaxId = equifaxId;
}

@JsonProperty("revenueCurrency")
public String getRevenueCurrency() {
return revenueCurrency;
}

@JsonProperty("revenueCurrency")
public void setRevenueCurrency(String revenueCurrency) {
this.revenueCurrency = revenueCurrency;
}

@JsonProperty("industryCode")
public String getIndustryCode() {
return industryCode;
}

@JsonProperty("industryCode")
public void setIndustryCode(String industryCode) {
this.industryCode = industryCode;
}

@JsonProperty("subIndustryCode")
public String getSubIndustryCode() {
return subIndustryCode;
}

@JsonProperty("subIndustryCode")
public void setSubIndustryCode(String subIndustryCode) {
this.subIndustryCode = subIndustryCode;
}

@JsonProperty("britishSics")
public List<BritishSics> getBritishSics() {
return britishSics;
}

@JsonProperty("britishSics")
public void setBritishSics(List<BritishSics> britishSics) {
this.britishSics = britishSics;
}

@JsonProperty("parentCompanyCountry")
public String getParentCompanyCountry() {
return parentCompanyCountry;
}

@JsonProperty("parentCompanyCountry")
public void setParentCompanyCountry(String parentCompanyCountry) {
this.parentCompanyCountry = parentCompanyCountry;
}

@JsonProperty("ultimateParentCompanyCountry")
public String getUltimateParentCompanyCountry() {
return ultimateParentCompanyCountry;
}

@JsonProperty("ultimateParentCompanyCountry")
public void setUltimateParentCompanyCountry(String ultimateParentCompanyCountry) {
this.ultimateParentCompanyCountry = ultimateParentCompanyCountry;
}

@JsonProperty("ultimateParentCompanyName")
public String getUltimateParentCompanyName() {
return ultimateParentCompanyName;
}

@JsonProperty("ultimateParentCompanyName")
public void setUltimateParentCompanyName(String ultimateParentCompanyName) {
this.ultimateParentCompanyName = ultimateParentCompanyName;
}

@JsonProperty("parentCompanyName")
public String getParentCompanyName() {
return parentCompanyName;
}

@JsonProperty("parentCompanyName")
public void setParentCompanyName(String parentCompanyName) {
this.parentCompanyName = parentCompanyName;
}

@JsonProperty("foundationDate")
public String getFoundationDate() {
return foundationDate;
}

@JsonProperty("foundationDate")
public void setFoundationDate(String foundationDate) {
this.foundationDate = foundationDate;
}

@JsonProperty("companyFacebookProfile")
public String getCompanyFacebookProfile() {
return companyFacebookProfile;
}

@JsonProperty("companyFacebookProfile")
public void setCompanyFacebookProfile(String companyFacebookProfile) {
this.companyFacebookProfile = companyFacebookProfile;
}

@JsonProperty("companyTwitterProfile")
public String getCompanyTwitterProfile() {
return companyTwitterProfile;
}

@JsonProperty("companyTwitterProfile")
public void setCompanyTwitterProfile(String companyTwitterProfile) {
this.companyTwitterProfile = companyTwitterProfile;
}

@JsonProperty("companyBlogProfile")
public String getCompanyBlogProfile() {
return companyBlogProfile;
}

@JsonProperty("companyBlogProfile")
public void setCompanyBlogProfile(String companyBlogProfile) {
this.companyBlogProfile = companyBlogProfile;
}

@JsonProperty("latitude")
public String getLatitude() {
return latitude;
}

@JsonProperty("latitude")
public void setLatitude(String latitude) {
this.latitude = latitude;
}

@JsonProperty("longitude")
public String getLongitude() {
return longitude;
}

@JsonProperty("longitude")
public void setLongitude(String longitude) {
this.longitude = longitude;
}

@JsonProperty("sources")
public List<String> getSources() {
return sources;
}

@JsonProperty("sources")
public void setSources(List<String> sources) {
this.sources = sources;
}

@JsonProperty("companyId")
public Long getCompanyId() {
return companyId;
}

@JsonProperty("companyId")
public void setCompanyId(Long companyId) {
this.companyId = companyId;
}

@JsonProperty("websites")
public List<String> getWebsites() {
return websites;
}

@JsonProperty("websites")
public void setWebsites(List<String> websites) {
this.websites = websites;
}

}