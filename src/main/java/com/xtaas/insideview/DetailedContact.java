package com.xtaas.insideview;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DetailedContact {

  @JsonProperty("contactId")
  private Long contactId;
  @JsonProperty("firstName")
  private String firstName;
  @JsonProperty("lastName")
  private String lastName;
  @JsonProperty("companyId")
  private Long companyId;
  @JsonProperty("companyName")
  private String companyName;
  @JsonProperty("email")
  private String email;
  @JsonProperty("emailValidationStatus")
  private String emailValidationStatus;
  @JsonProperty("phone")
  private String phone;
  @JsonProperty("companyDetails")
  private CompanyDetails companyDetails;
  @JsonProperty("active")
  private Boolean active;
  @JsonProperty("jobLevels")
  private List<String> jobLevels = null;
  @JsonProperty("jobFunctions")
  private List<String> jobFunctions = null;
  @JsonProperty("peopleId")
  private String peopleId;
  @JsonProperty("fullName")
  private String fullName;
  @JsonProperty("confidenceScore")
  private Float confidenceScore;
  @JsonProperty("directPhone")
  private String directPhone;
  @JsonProperty("corporatePhone")
  private String corporatePhone;
  @JsonProperty("sources")
  private List<String> sources = null;
  @JsonProperty("titles")
  private List<String> titles = null;
  @JsonProperty("emails")
  private List<Email> emails = null;
  @JsonProperty("linkedinHandle")
  private String linkedinHandle;
  @JsonProperty("education")
  private List<Education> education = null;
  
  @JsonProperty("contactId")
  public Long getContactId() {
  return contactId;
  }
  
  @JsonProperty("contactId")
  public void setContactId(Long contactId) {
  this.contactId = contactId;
  }
  
  @JsonProperty("firstName")
  public String getFirstName() {
  return firstName;
  }
  
  @JsonProperty("firstName")
  public void setFirstName(String firstName) {
  this.firstName = firstName;
  }
  
  @JsonProperty("lastName")
  public String getLastName() {
  return lastName;
  }
  
  @JsonProperty("lastName")
  public void setLastName(String lastName) {
  this.lastName = lastName;
  }
  
  @JsonProperty("companyId")
  public Long getCompanyId() {
  return companyId;
  }
  
  @JsonProperty("companyId")
  public void setCompanyId(Long companyId) {
  this.companyId = companyId;
  }
  
  @JsonProperty("companyName")
  public String getCompanyName() {
  return companyName;
  }
  
  @JsonProperty("companyName")
  public void setCompanyName(String companyName) {
  this.companyName = companyName;
  }
  
  @JsonProperty("email")
  public String getEmail() {
  return email;
  }
  
  @JsonProperty("email")
  public void setEmail(String email) {
  this.email = email;
  }
  
  @JsonProperty("emailValidationStatus")
  public String getEmailValidationStatus() {
  return emailValidationStatus;
  }
  
  @JsonProperty("emailValidationStatus")
  public void setEmailValidationStatus(String emailValidationStatus) {
  this.emailValidationStatus = emailValidationStatus;
  }
  
  @JsonProperty("phone")
  public String getPhone() {
  return phone;
  }
  
  @JsonProperty("phone")
  public void setPhone(String phone) {
  this.phone = phone;
  }
  
  @JsonProperty("companyDetails")
  public CompanyDetails getCompanyDetails() {
  return companyDetails;
  }
  
  @JsonProperty("companyDetails")
  public void setCompanyDetails(CompanyDetails companyDetails) {
  this.companyDetails = companyDetails;
  }
  
  @JsonProperty("active")
  public Boolean getActive() {
  return active;
  }
  
  @JsonProperty("active")
  public void setActive(Boolean active) {
  this.active = active;
  }
  
  @JsonProperty("jobLevels")
  public List<String> getJobLevels() {
  return jobLevels;
  }
  
  @JsonProperty("jobLevels")
  public void setJobLevels(List<String> jobLevels) {
  this.jobLevels = jobLevels;
  }
  
  @JsonProperty("jobFunctions")
  public List<String> getJobFunctions() {
  return jobFunctions;
  }
  
  @JsonProperty("jobFunctions")
  public void setJobFunctions(List<String> jobFunctions) {
  this.jobFunctions = jobFunctions;
  }
  
  @JsonProperty("peopleId")
  public String getPeopleId() {
  return peopleId;
  }
  
  @JsonProperty("peopleId")
  public void setPeopleId(String peopleId) {
  this.peopleId = peopleId;
  }
  
  @JsonProperty("fullName")
  public String getFullName() {
  return fullName;
  }
  
  @JsonProperty("fullName")
  public void setFullName(String fullName) {
  this.fullName = fullName;
  }
  
  @JsonProperty("confidenceScore")
  public Float getConfidenceScore() {
  return confidenceScore;
  }
  
  @JsonProperty("confidenceScore")
  public void setConfidenceScore(Float confidenceScore) {
  this.confidenceScore = confidenceScore;
  }
  
  @JsonProperty("directPhone")
  public String getDirectPhone() {
  return directPhone;
  }
  
  @JsonProperty("directPhone")
  public void setDirectPhone(String directPhone) {
  this.directPhone = directPhone;
  }
  
  @JsonProperty("corporatePhone")
  public String getCorporatePhone() {
  return corporatePhone;
  }
  
  @JsonProperty("corporatePhone")
  public void setCorporatePhone(String corporatePhone) {
  this.corporatePhone = corporatePhone;
  }
  
  @JsonProperty("sources")
  public List<String> getSources() {
  return sources;
  }
  
  @JsonProperty("sources")
  public void setSources(List<String> sources) {
  this.sources = sources;
  }
  
  @JsonProperty("titles")
  public List<String> getTitles() {
  return titles;
  }
  
  @JsonProperty("titles")
  public void setTitles(List<String> titles) {
  this.titles = titles;
  }
  
  @JsonProperty("emails")
  public List<Email> getEmails() {
  return emails;
  }
  
  @JsonProperty("emails")
  public void setEmails(List<Email> emails) {
  this.emails = emails;
  }

public String getLinkedinHandle() {
	return linkedinHandle;
}

public void setLinkedinHandle(String linkedinHandle) {
	this.linkedinHandle = linkedinHandle;
}

public List<Education> getEducation() {
	return education;
}

public void setEducation(List<Education> education) {
	this.education = education;
}
  
}