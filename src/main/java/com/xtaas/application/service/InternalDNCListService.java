package com.xtaas.application.service;

import java.io.IOException;
import java.util.List;

import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.entity.Campaign;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.DNCList;
import com.xtaas.domain.valueobject.IDNCSearchResult;
import com.xtaas.web.dto.DemandShoreDNCDTO;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.IDNCSearchDTO;

public interface InternalDNCListService {

	public void addNumber(IDNCNumberDTO idncNumberDTO);

	public String addNumber(DemandShoreDNCDTO dncNumberDTO);

	public IDNCSearchResult getDNCNumbers(IDNCSearchDTO idncSearchDTO);

	public DNCList getDNCList(String phoneNumber, String partnerId);

	public List<DNCList> getDNCList(String phoneNumber, String firstName, String lastName);

	public List<DNCList> getDNCListForBuy(String phoneNumber, String firstName, String lastName);

	public long countDNCListForBuy(String phoneNumber, String firstName, String lastName);

	public String createDNCList(MultipartFile file, String campaignId, String partnerId) throws IOException;

	public List<DNCList> getDNCList(String phoneNumber);

	public List<DNCList> getDNCListByDomain(String domain);

	public List<DNCList> getDNCByPartnerIdAndPhoneNumber(String partnerId, String phoneNumber);

	public List<DNCList> getDNCByPartnerIdAndDomain(String partnerId, String domain);

	public void createIDNCNumberDTO(Prospect prospect, Campaign campaign, String notes);

	public boolean checkLocalDNCList(String phone, String firstName, String lastName);

}
