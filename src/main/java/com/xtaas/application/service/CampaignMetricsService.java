package com.xtaas.application.service;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

import com.xtaas.web.dto.CampaignMetricsDTO;

public interface CampaignMetricsService {

	public CampaignMetricsDTO getCampaignMetricsByCampaign(String campaignId, String fromDate, String toDate, String timezone,int noOfDays) throws ParseException;
	
	public HashMap<String, Integer> getEmailBounceCount(String campaignId,String fromDate, String toDate,String timezone,int noOfDays) throws ParseException;

}
