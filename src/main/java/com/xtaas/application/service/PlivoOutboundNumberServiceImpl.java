package com.xtaas.application.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.plivo.api.Plivo;
import com.plivo.api.exceptions.PlivoRestException;
import com.plivo.api.models.base.ListResponse;
import com.plivo.api.models.number.NumberType;
import com.plivo.api.models.number.PhoneNumber;
import com.plivo.api.models.number.PhoneNumberCreateResponse;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.PlivoOutboundNumberRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.CountryDetails;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.CountryWiseDetailsDTO;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;

@Service
public class PlivoOutboundNumberServiceImpl implements PlivoOutboundNumberService {

	private static final Logger logger = LoggerFactory.getLogger(PlivoOutboundNumberServiceImpl.class);

	@Autowired
	private PlivoOutboundNumberRepository plivoOutboundNumberRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Value("#{new java.util.Random()}")
	private Random random;
	
	@Autowired
	MongoTemplate mongoTemplate;

	private Map<String, List<PlivoOutboundNumber>> plivoOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> plivoPoolMap = new HashMap<>();
	private Map<String, Integer> countryNameIsdMap = new HashMap<>();

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT })
	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool) {
		Team team = teamRepository.findOneById(teamId);
		String partnerId = team.getPartnerId();
		// String cacheKey = partnerId + "_" + bucketName;
		List<PlivoOutboundNumber> outboundNumberList;

		int areaCode = Integer.parseInt(findAreaCode(phone));
		outboundNumberList = plivoOutboundNumberRepository.findByBucketWithAreaCode(bucketName, areaCode);
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumberRepository.findByPartnerBucket(partnerId, bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumberRepository.findOutboundNumbers(partnerId);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumberRepository.findOutboundNumbersByBucketNumber(bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumberRepository.findByPartnerBucket("XTAAS CALL CENTER", "DEFAULT");
		}
		// find a random number from the list
		PlivoOutboundNumber outboundNumber = outboundNumberList.get(random.nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replaceAll("[^\\d]", ""); // phone.replace("-", "");
		int length = removeSpclChar.length();
		/*
		 * handled phone numbers with length less than 10 digit & return first two digit
		 * as area code - e.g. Germany: +494023750
		 */
		if (length < 10) {
			return removeSpclChar.substring(0, 2);
		}
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = removeSpclChar.substring(startPoint, endPoint);
		return areaCode;
	}

	@Override
	public List<PlivoOutboundNumber> getAllOutboundNumbers() {
		return plivoOutboundNumberRepository.findAll();
	}

	@Override
	public List<CountryDetails> getDistinctCountryInformation() {
		return plivoOutboundNumberRepository.getDistinctCountryDetails();
	}

	@Override
	public Boolean refreshOutboundNumbers() {
		if (plivoOutboundNumbersMap != null) {
			plivoOutboundNumbersMap.clear();
		}
		return true;
	}

	@Override
	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId) {
		List<PlivoOutboundNumber> outboundNumbers = new ArrayList<>();
		outboundNumbers = plivoOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllPlivoOutboundNumber();
				outboundNumbers = plivoOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
					plivoOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cachePlivoNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(phoneNumber, callRetryCount, countryName, organizationId, outboundNumbers,
				plivoPoolMap);
		return number;
	}

	private String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId,
			List<PlivoOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<PlivoOutboundNumber> outboundNumberList = new ArrayList<>();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = callRetryCount % maxPoolSize;
		int areaCode = Integer.parseInt(findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = plivoOutboundNumbersMap.get("XTAAS CALL CENTER");
		}

		PlivoOutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private void cacheAllPlivoOutboundNumber() {
		logger.info("cacheAllPlivoOutboundNumber() :: Fetching plivooutboundnumbers from DB to cache.");
		List<PlivoOutboundNumber> outboundNumbers = plivoOutboundNumberRepository.findAll();
		plivoOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("PLIVO"))
				.collect(Collectors.groupingBy(PlivoOutboundNumber::getPartnerId));
	}

	private void cachePlivoNumbersPoolDetails(List<PlivoOutboundNumber> outboundNumbers) {
		for (PlivoOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (plivoPoolMap.get(key) == null) {
				plivoPoolMap.put(key, outboundNumber.getPool());
			} else if (plivoPoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				plivoPoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

	@Override
	public List<PlivoOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber) {
		List<PlivoOutboundNumber> outboundNumbers = plivoOutboundNumberRepository
				.findByDisplayPhoneNumber(displayPhoneNumber);
		return outboundNumbers;
	}

	@Override
	public PlivoOutboundNumber save(PlivoOutboundNumber plivoOutboundNumber) {
		return plivoOutboundNumberRepository.save(plivoOutboundNumber);
	}

	@Override
	public List<PlivoOutboundNumber> findByIsdCode(int isd) {
		return plivoOutboundNumberRepository.findByIsdCode(isd);

	}

	@Override
	public PlivoOutboundNumber findOneByIsdCode(int isd) {
		return plivoOutboundNumberRepository.findOneByIsdCode(isd);
	}

	@Override
	public PhoneNumber searchNumber(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		try {
		    boolean flag = true;
			while (flag) {
				String randomPattern = XtaasUtils.getPlivoPhoneNumberPatterns()
						.get(new Random().nextInt(XtaasUtils.getPlivoPhoneNumberPatterns().size()));
				ListResponse<PhoneNumber> listResponse;
				NumberType numberType = NumberType.valueOf(phoneDTO.getType());
				listResponse = PhoneNumber.lister(phoneDTO.getCountryISO()).pattern(randomPattern)
						.services(phoneDTO.getServices()).type(numberType).limit(phoneDTO.getLimit())
						.offset(phoneDTO.getOffset()).list();
				if (listResponse != null && listResponse.getObjects() != null && listResponse.getObjects().size() > 0) {
					PhoneNumber phoneNumber = listResponse.getObjects().get(0);
					flag = false;
					return phoneNumber;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	private boolean deleteNumberFromPlivoPortal(String numberToDelete) {
		try {
			com.plivo.api.models.number.Number.deleter(numberToDelete).delete();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Async
	@Override
	public void refreshPhoneNumbers(PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		Plivo.init("MAYMZIYJC1YZMXZGE2N2", "MGQ1NmNhY2EzMTRmNWYzYzRkNzk3YTZiYWU1Y2Rm");
		List<PlivoOutboundNumber> numbersFromDB = plivoOutboundNumberRepository
				.findOutboundNumbers(plivoPhoneNumberPurchaseDTO.getPartnerId());
		List<PlivoOutboundNumber> filteredList = new ArrayList<PlivoOutboundNumber>();
		List<String> distinctOutBoundNumbers = getDistinctOutBoundNumbers(plivoPhoneNumberPurchaseDTO.getPartnerId());
		for (String number : distinctOutBoundNumbers) {
			List<PlivoOutboundNumber> num = numbersFromDB.stream()
					.filter(v -> v.getPhoneNumber().equalsIgnoreCase(number)).collect(Collectors.toList());
			filteredList.add(num.get(0));
		}
		int count = 0;
		if (filteredList != null && filteredList.size() > 0) {
			for (PlivoOutboundNumber pnumber : filteredList) {
				PhoneNumber phoneNumber = searchNumber(plivoPhoneNumberPurchaseDTO);
				if (phoneNumber != null) {
					count++;
					deleteNumberFromPlivoPortal(pnumber.getPhoneNumber());
					deletePlivoNumberFromDB(pnumber.getPhoneNumber());
					addPlivoNumbersInCountries(buyPlivoNumberAndCreateEntryInDB(plivoPhoneNumberPurchaseDTO,
							phoneNumber.getNumber(), pnumber), plivoPhoneNumberPurchaseDTO);
				}
			}
			this.refreshOutboundNumbers();
		}
		String msg = "Out of " + distinctOutBoundNumbers.size() + " Plivo numbers " + count + "got refreshed successfully.";
		if (plivoPhoneNumberPurchaseDTO.getEmails() != null && plivoPhoneNumberPurchaseDTO.getEmails().size() > 0) {
			for (String email : plivoPhoneNumberPurchaseDTO.getEmails()) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
						"Plivo Number Refresh of " + plivoPhoneNumberPurchaseDTO.getPartnerId(), msg);
			}
		}
	}

	private void deletePlivoNumberFromDB(String phoneNumber) {
		plivoOutboundNumberRepository.deleteManyByPlivoPhoneNumber(phoneNumber);
	}

	private PlivoOutboundNumber buyPlivoNumberAndCreateEntryInDB(PlivoPhoneNumberPurchaseDTO phoneDTO,
			String phoneNumberToPurchase, PlivoOutboundNumber plivoOutboundNumberFromDB) {
		PhoneNumberCreateResponse createResponse;
		try {
			createResponse = PhoneNumber.creator(phoneNumberToPurchase).appId(phoneDTO.getAppId()).create();
			if (createResponse != null && createResponse.getNumbers() != null
					&& createResponse.getNumbers().size() > 0) {
				String number = createResponse.getNumbers().get(0).getNumber();
				int areaCode = Integer.valueOf(number.substring(1, 4));
				PlivoOutboundNumber plivoOutboundNumber = new PlivoOutboundNumber(number, number, areaCode,
						plivoOutboundNumberFromDB.getIsdCode(), "PLIVO", plivoOutboundNumberFromDB.getPartnerId(),
						plivoOutboundNumberFromDB.getPartnerId(), plivoOutboundNumberFromDB.getVoiceMessage(),
						plivoOutboundNumberFromDB.getPool(), phoneDTO.getTimezone(), phoneDTO.getCountryISO(),
						phoneDTO.getRegion());
				plivoOutboundNumber = save(plivoOutboundNumber);
				com.plivo.api.models.number.Number.updater(number).alias(number).subaccount(phoneDTO.getSubAccountId())
						.appId(phoneDTO.getAppId()).update();
				return plivoOutboundNumber;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PlivoRestException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<CountryWiseDetailsDTO> getUniqueCountryDetails() {
		List<CountryWiseDetailsDTO> countryWiseList = new ArrayList<CountryWiseDetailsDTO>();
		List<StateCallConfig> stateCallConfigDetails = stateCallConfigRepository.findAll();
		if (stateCallConfigDetails != null && stateCallConfigDetails.size() > 0) {
			for (StateCallConfig stateCallConfig : stateCallConfigDetails) {
				if (stateCallConfig.getCountryName() != "United States") {
					List<CountryWiseDetailsDTO> filterList = new ArrayList<CountryWiseDetailsDTO>();
					if (countryWiseList != null && countryWiseList.size() > 0) {
						filterList = countryWiseList.stream()
								.filter(stateCall -> stateCall.getCountryName()
										.equalsIgnoreCase(stateCallConfig.getCountryName()))
								.collect(Collectors.toList());
					}
					if (filterList.size() == 0) {
						CountryWiseDetailsDTO countryDTO = new CountryWiseDetailsDTO();
						countryDTO.setCountryCode(stateCallConfig.getCountryCode());
						countryDTO.setCountryName(stateCallConfig.getCountryName());
						countryDTO.setTimeZone(stateCallConfig.getTimezone());
						countryWiseList.add(countryDTO);
					}
				}
			}
		}
		return countryWiseList;
	}

	private void addPlivoNumbersInCountries(PlivoOutboundNumber plivoNumber, PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
		for (CountryWiseDetailsDTO countryWiseDetailsDTO : countryDetails) {
			PlivoOutboundNumber plivoOutboundNumber = new PlivoOutboundNumber(plivoNumber.getDisplayPhoneNumber(),
					plivoNumber.getDisplayPhoneNumber(), plivoNumber.getAreaCode(), plivoNumber.getIsdCode(), "PLIVO",
					plivoNumber.getPartnerId(), plivoNumber.getPartnerId(), plivoNumber.getVoiceMessage(),
					plivoNumber.getPool(), countryWiseDetailsDTO.getTimeZone(), countryWiseDetailsDTO.getCountryCode(),
					countryWiseDetailsDTO.getCountryName());
			this.save(plivoOutboundNumber);
		}
	}

	public List<String> getDistinctOutBoundNumbers(String partnerId) {
		DistinctIterable<String> numbersFromDB = null;
		List<String> partnerIds = new ArrayList<String>();
		partnerIds.add(partnerId);
		try {
			Document elemMatch = new Document();
			elemMatch.put("partnerId", new Document("$in", partnerIds));
			numbersFromDB = mongoTemplate.getCollection("plivooutboundnumber").distinct("phoneNumber", elemMatch,
					String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> distinctNumbersFromDB = new ArrayList<>();
		if (numbersFromDB != null) {
			MongoCursor<String> cursor = numbersFromDB.iterator();
			while (cursor.hasNext()) {
				distinctNumbersFromDB.add(cursor.next());
			}
		}
		return distinctNumbersFromDB;
	}

	@Override
	public void cacheAllPlivoOutboundNumberForDatabuy() {
		if (countryNameIsdMap != null && countryNameIsdMap.size() == 0) {
			logger.info("cacheAllPlivoOutboundNumberForDatabuy() :: Fetching plivooutboundnumbers from DB to cache.");
			List<PlivoOutboundNumber> outboundNumbers = plivoOutboundNumberRepository.findAll();
			Map<String, List<PlivoOutboundNumber>> plivoOutboundNumbersMapForDatabuy = new HashMap<>();
			plivoOutboundNumbersMapForDatabuy = outboundNumbers.stream()
					.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("PLIVO"))
					.collect(Collectors.groupingBy(PlivoOutboundNumber::getPartnerId));
			if (plivoOutboundNumbersMapForDatabuy.containsKey("XTAAS CALL CENTER")) {
				List<PlivoOutboundNumber> outboundNumberList = new ArrayList<>();
				outboundNumberList = plivoOutboundNumbersMapForDatabuy.get("XTAAS CALL CENTER");
				if (outboundNumberList != null && outboundNumberList.size() > 0){
					for (PlivoOutboundNumber plivoOutboundNumber : outboundNumberList) {
						if (plivoOutboundNumber.getCountryName() != null && !plivoOutboundNumber.getCountryName().isEmpty()) {
							String country = plivoOutboundNumber.getCountryName().toLowerCase();
							countryNameIsdMap.put(country, plivoOutboundNumber.getIsdCode());
						}
					}
				}
	
			}
		}
	}

	@Override
	public Integer getIsdCodeFromCountryName (String countryName) {
		String country = countryName.toLowerCase();
		return countryNameIsdMap.get(country);
	}

	@Override
	public void hardCacheRefeshCountryIsdCodeMapForDatabuy() {
		countryNameIsdMap.clear();
		cacheAllPlivoOutboundNumberForDatabuy();
	}

	@Override
	public List<PlivoOutboundNumber> findAll() {
		return plivoOutboundNumberRepository.findAll();
	}
	
}
