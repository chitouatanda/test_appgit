package com.xtaas.application.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Conference;
import com.twilio.sdk.resource.instance.Member;
import com.twilio.sdk.resource.instance.Participant;
import com.twilio.sdk.resource.instance.Queue;
import com.twilio.sdk.resource.list.ConferenceList;
import com.twilio.sdk.resource.list.MemberList;
import com.twilio.sdk.resource.list.ParticipantList;
import com.twilio.sdk.resource.list.QueueList;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.CallLogService;
import com.xtaas.service.PlivoService;
import com.xtaas.service.SignalwireService;
import com.xtaas.service.TelnyxService;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.TelnyxCallStatusDTO;

@Service
public class CallServiceImpl implements CallService {

	private static final Logger logger = LoggerFactory.getLogger(CallServiceImpl.class);

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private PlivoService plivoService;

	@Autowired
	private CallLogService callLogService;

	@Autowired
	private TelnyxService telnyxService;
	
	@Autowired
	private CallLogRepository callLogRepository;

	@Autowired
	private SignalwireService signalwireService;

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void setCallAsComplete(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall) {
		if (usePlivo) {
			plivoKickParticipantFromConference(agentId, callSid, isConferenceCall);
		} else if (callSid.startsWith("CA")) {
			// Twilio call starts with CA
			twilioKickParticipantFromConference(agentId, callSid);
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void setCallAsCompleteNew(String agentId, String callSid, String callControlProvider, boolean isConferenceCall) {
		if (callControlProvider.equalsIgnoreCase("Plivo")) {
			plivoKickParticipantFromConference(agentId, callSid, isConferenceCall);
		}else if (callControlProvider.equalsIgnoreCase("Signalwire")) {
			signalwireKickParticipantFromConference(agentId, callSid, isConferenceCall);
		} else if (callSid.startsWith("CA")) {
			// Twilio call starts with CA
			twilioKickParticipantFromConference(agentId, callSid);
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void updateCallWithStatusCallbackUrl(String callSid) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		if (agentCall == null || agentCall.getProspectCall() == null) {
			logger.error(
					"updateCallWithStatusCallbackUrl() : Cannot update StatusCallbackUrl as Prospect on which agent [{}] working on Not Found",
					agentId);
			return;
		}
		String pcid = agentCall.getProspectCall().getProspectCallId();

		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		Call call = account.getCall(callSid);
		if (call != null) {
			StringBuilder callCompleteActionUrl = new StringBuilder(
					ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/spr/dialer/savecallmetrics?pcid=")
							.append(pcid);
			String statusCallbackUrl = callCompleteActionUrl.toString();
			Map<String, String> agentsCallParams = new HashMap<String, String>();
			agentsCallParams.put("StatusCallback", statusCallbackUrl);
			agentsCallParams.put("StatusCallbackUrl", statusCallbackUrl);
			try {
				logger.debug(
						"updateCallWithStatusCallbackUrl() : Update Call with StatusCallbackUrl. CallSid: [{}], StatusCallbackUrl: [{}]",
						callSid, statusCallbackUrl);
				call.update(agentsCallParams);
			} catch (Exception e) {
				logger.error("updateCallWithStatusCallbackUrl() : Exception occurred", e);
			}
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public boolean placeCallOnHold(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall) {
		boolean success = false;
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		DialerMode dialerMode = agentCall.getCurrentCampaign().getDialerMode();
		if (usePlivo) {
			success = plivoService.holdParticipantInConference(agentId, callSid, isConferenceCall);
		} else {
			success = twilioCallHold(callSid, success, agentId, agentCall, dialerMode);
		}
		return success;
	}

	private String getOutboundParticipantCallSid(Conference conf, String agentsCallSid) {
		ParticipantList participants = conf.getParticipants();

		// find the participant.
		for (Participant participant : participants) {
			if (participant.getCallSid().equals(agentsCallSid)) {
				continue;
			} else if (participant.isMuted()) { // if supervisor is also part of conference, then ignore that call as
												// well. Supervisor is always in MUTE mode
				continue;
			} else {
				return participant.getCallSid(); // return the call sid of participant which is not agent's callsid
			}
		}
		return null;
	}

	private Call getOutboundParticipantCall(String callSid) {
		if (callSid == null)
			return null;

		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		return account.getCall(callSid);
	}

	private Conference getActiveConferenceId(String conferenceName) {
		// Build a filter for the ConferenceList
		Map<String, String> params = new HashMap<String, String>();
		params.put("Status", "in-progress");
		params.put("FriendlyName", conferenceName);

		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		ConferenceList conferences = account.getConferences(params);

		for (Conference conference : conferences) {
			return conference; // returning first conference id. there shouldn't be more
		}
		return null;
	}

	private Queue getAgentsQueue(String queueName) {
		// Build a filter for the QueueList
		Map<String, String> params = new HashMap<String, String>();
		params.put("FriendlyName", queueName);

		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		QueueList queues = account.getQueues();

		for (Queue queue : queues) {
			if (queue.getFriendlyName().equals(XtaasUserUtils.getCurrentLoggedInUsername())) {
				return queue; // returning the queue belonging to the current logged in agent
			}
		}
		return null;
	}

	private String getQueueMembersCallSid(Queue agentsQueue) {
		MemberList members = agentsQueue.getMembers();

		// find the participant.
		for (Member member : members) {
			return member.getCallSid(); // return the call sid of participant which is not agent's callsid
		}
		return null;
	}

	/**
	 * Sends enqueue request to twilio to put the specified call in a queue, later
	 * to be picked by agent
	 *
	 * @param call
	 * @param queueName
	 * @return
	 */
	private boolean enqueueCall(Call call, String queueName) {
		boolean success = false;
		if (call != null) {
			// StringBuilder holdXMLUrl = new
			// StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl()).append("/twiml/hold.xml.jsp?qid=").append(queueName);
			String actionUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/twilio/call/hold?qid="
					+ queueName;
			StringBuilder holdXMLUrl = new StringBuilder(actionUrl);
			Map<String, String> agentsCallParams = new HashMap<String, String>();
			agentsCallParams.put("Url", holdXMLUrl.toString());
			agentsCallParams.put("Method", "POST");
			try {
				logger.debug("enqueueCall() : Sending HOLD request to TWILIO for CallSid : [{}]", call.getSid());
				call.update(agentsCallParams);
				success = true;
			} catch (Exception e) {
				logger.error("placeCallOnHold() : Exception occurred", e);
				success = false;
			}
		}
		return success;
	}

	@Override
	public boolean unHoldCall(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall) {
		boolean success = false;
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		DialerMode dialerMode = agentCall.getCurrentCampaign().getDialerMode();
		if (usePlivo) {
			success = plivoService.unholdParticipantInConference(agentId, callSid, isConferenceCall);
		} else {
			success = twilioCallUnhold(callSid, success, agentId, agentCall, dialerMode);
		}
		return success;
	}

	@Override
	public boolean playDTMF(String callSid, String digit) {
		boolean success = false;

		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();

		// find the call sid from participant list of the conference
		String conferenceName = agentId; // agentId is the conference name
		Conference conference = getActiveConferenceId(conferenceName);
		if (conference == null) {
			logger.error("No active conference found for the agent [{}]", conferenceName);
			return success;
		}

		// find the participant's call sid
		String outgoingCallSid = getOutboundParticipantCallSid(conference, callSid);
		Call outgoingCall = getOutboundParticipantCall(outgoingCallSid);

		success = playDTMF(outgoingCall, digit, agentId);
		return success;
	}

	/**
	 * Sends play DTMF request to twilio
	 *
	 * @param call
	 * @param queueName
	 * @return
	 */
	private boolean playDTMF(Call call, String digit, String agentId) {
		boolean success = false;
		if (call != null) {
			AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);

			Map<String, String> agentsCallParams = new HashMap<String, String>();
			try {
				StringBuilder dialXMLUrl = new StringBuilder(
						ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/twiml/dialtoconf.xml.jsp");
				dialXMLUrl.append("?cn=").append(URLEncoder.encode(agentId, "UTF-8"));
				dialXMLUrl.append("&pcid=").append(agentCall.getProspectCall().getProspectCallId());
				dialXMLUrl.append("&digit=").append(URLEncoder.encode(digit, "UTF-8"));

				agentsCallParams.put("Url", dialXMLUrl.toString());
			} catch (UnsupportedEncodingException e) {
				logger.error(
						"announceMessageinConferenceRoom() : Exception occurred while requesting dialtoconf.xml.jsp",
						e);
			}
			agentsCallParams.put("Method", "GET");

			try {
				logger.info("playDTMF() : Sending Play DTMF request to TWILIO for CallSid : [{}]", call.getSid());
				call.update(agentsCallParams);
				success = true;
			} catch (Exception e) {
				logger.error("playDTMF() : Exception occurred", e);
				success = false;
			}
		}
		return success;
	}

	private void twilioKickParticipantFromConference(String agentId, String callSid) {
		// first, remove participant from conference
		// agentId is the conference name
		Conference conference = getActiveConferenceId(agentId);
		if (conference == null) {
			logger.error(
					"setCallAsComplete(): No active conference found for the agent [{}]. Not removing participant from it.",
					agentId);
		} else {
			try {
				Participant participant = conference.getParticipant(callSid);
				boolean kickResult = participant.kick();
				if (kickResult) {
					logger.info("setCallAsComplete() : Successfully Kicked out Participant [{}] from Conference [{}]",
							callSid, conference.getFriendlyName());
				} else {
					logger.info("setCallAsComplete() : Unable to Kick out Participant [{}] from Conference [{}]",
							callSid, conference.getFriendlyName());
				}
			} catch (TwilioRestException e) {
				logger.error("setCallAsComplete() : Exception occurred while kicking out participant " + callSid);
			}
		}

		// then, terminate the call
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		Call call = account.getCall(callSid);
		if (call != null) {
			try {
				Call callAfterHangup = call.hangup();
				logger.info(
						"setCallAsComplete() : Changed Call status as COMPLETE at TWILIO. CallSid : [{}], Status after hangup [{}]",
						callSid, callAfterHangup.getStatus());
			} catch (Exception e) {
				logger.error("setCallAsComplete() : Exception occurred", e);
			}
		}
	}

	private void plivoKickParticipantFromConference(String agentId, String callSid, boolean isConferenceCall) {
		plivoService.kickParticipantFromConference(agentId, callSid, isConferenceCall);
	}

	private void signalwireKickParticipantFromConference(String agentId, String callSid, boolean isConferenceCall) {
		signalwireService.kickParticipantFromConference(agentId, callSid, isConferenceCall);
	}

	private boolean twilioCallHold(String callSid, boolean success, String agentId, AgentCall agentCall,
			DialerMode dialerMode) {
		if (DialerMode.POWER.equals(dialerMode)) {
			Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
			Account account = channelTwilio.getChannelConnection();
			// in POWER mode, there is just a single outbound call leg
			Call call = account.getCall(callSid);
			if (call != null) {
				// enqueue the prospect call leg into agent specific queue
				success = enqueueCall(call, agentId);
			}
		} else {
			// find the call sid from participant list of the conference
			String pcid = agentCall.getProspectCall().getProspectCallId();
			String conferenceName = XtaasUserUtils.getCurrentLoggedInUsername();
			Conference conference = getActiveConferenceId(conferenceName);
			if (conference == null) {
				// agentId_pcid is the conference name
				conferenceName = XtaasUserUtils.getCurrentLoggedInUsername() + "_" + pcid;
				conference = getActiveConferenceId(conferenceName);
			}
			if (conference == null) {
				logger.error("No active conference found for the agent [{}]", conferenceName);
				success = false;
			}
			// find the participant's call sid
			String outgoingCallSid = getOutboundParticipantCallSid(conference, callSid);
			Call outgoingCall = getOutboundParticipantCall(outgoingCallSid);

			// enqueue the call to agent specific queue
			success = enqueueCall(outgoingCall, agentId);
		}
		return success;
	}

	private boolean twilioCallUnhold(String callSid, boolean success, String agentId, AgentCall agentCall,
			DialerMode dialerMode) {
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		Account account = channelTwilio.getChannelConnection();
		// find the call sid from Member list of the queue
		String queueName = XtaasUserUtils.getCurrentLoggedInUsername(); // agentId is the conference name
		Queue queue = getAgentsQueue(queueName);
		if (queue == null) {
			logger.error("No QUEUE found for the agent [{}]", queueName);
			success = false;
		}
		// find the queue member's call sid
		String outgoingCallSid = getQueueMembersCallSid(queue);
		// find the outgoing call and add it back to agent's conference
		Call outgoingCall = account.getCall(outgoingCallSid);
		if (outgoingCall != null) {
			StringBuilder unholdXMLUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl());
			if (DialerMode.POWER.equals(dialerMode)) {
				// unholdXMLUrl.append("/twiml/unholdpower.xml.jsp?agentId=").append(agentId).append("&pcid=").append(agentCall.getProspectCall().getProspectCallId());
				unholdXMLUrl.append("/spr/twilio/call/unhold?agentId=").append(agentId).append("&pcid=")
						.append(agentCall.getProspectCall().getProspectCallId());
			} else {
				// unholdXMLUrl.append("/twiml/unhold.xml.jsp?agentId=").append(agentId).append("&pcid=").append(agentCall.getProspectCall().getProspectCallId());
				unholdXMLUrl.append("/spr/twilio/call/unhold?agentId=").append(agentId).append("&pcid=")
						.append(agentCall.getProspectCall().getProspectCallId());
			}
			Map<String, String> agentsCallParams = new HashMap<String, String>();
			agentsCallParams.put("Url", unholdXMLUrl.toString());
			agentsCallParams.put("Method", "POST");
			try {
				logger.info("unHoldCall() : Sending UNHOLD request to TWILIO for CallSid : [{}] by Agent [{}]", callSid,
						agentId);
				outgoingCall.update(agentsCallParams);
				success = true;
			} catch (Exception e) {
				logger.error(
						"unHoldCall() : Exception occurred for CallSid " + callSid + " Exception : " + e.getMessage(),
						e);
				success = false;
			}
		}
		return success;
	}

	@Override
	public TelnyxCallStatusDTO getTelnyxCallStatus(String callLegId) {
		TelnyxCallStatusDTO callStatus = telnyxService.getCallStatus(callLegId);
		return callStatus;
	}
	
	@Override
	public void notifyAgent(String callSid) {
		CallLog callLog = callLogRepository.findOneById(callSid);
		if (callLog != null) {
			String agentId = callLog.getCallLogMap().get("agentId");
			PusherUtils.pushMessageToUser(agentId, XtaasConstants.CALL_DISCONNECCT_EVENT, "");
		}
	}

}
