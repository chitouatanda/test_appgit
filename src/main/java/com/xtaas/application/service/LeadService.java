package com.xtaas.application.service;

import java.util.List;

import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.valueobject.ExpressionGroup;

public interface LeadService {
	int countLeads(ExpressionGroup leadFilterCriteria);
	List<Prospect> getLeads(ExpressionGroup leadFilterCriteria) throws Exception;
}
