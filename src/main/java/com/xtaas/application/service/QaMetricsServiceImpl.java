package com.xtaas.application.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;

import java.util.ArrayList;

import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@Service
public class QaMetricsServiceImpl implements QaMetricsService{

	@Autowired
	private QaService qaService;
	
	@Autowired
	private AgentRepository agentRepository;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private TeamRepository teamRepository;
	
//	DATE:19/12/2017 To show only active campaigns for qa
	@Autowired
	private CampaignRepository campaignRepository;
	
	public enum QaMetrics {
		COUNT_FAILURE_PROSPECTCALL,
		COUNT_SUCCESS_PROSPECTCALL,
		COUNT_CALLBACK_PROSPECTCALL,
		COUNT_DIALERCODE_PROSPECTCALL,
		COUNT_NOT_SCORED_PROSPECTCALL,
		COUNT_IN_PROGRESS_PROSPECTCALL,
		COUNT_COMPLETE_PROSPECTCALL,
		QA_PENDING_PROSPECTCALL_COUNT_LAST_WEEK,
		QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS,
		QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS,
		QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS,
		TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS
		
	}
	
	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByDispositionStatus(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<String> campaignIds = getProspectCallSearchCampaignIds(prospectCallSearchDTO);
		if (campaignIds != null && !campaignIds.isEmpty()) {
			return  prospectCallLogRepository.getQaMetricsByDispositionStatus(campaignIds, prospectCallSearchDTO);
		}
		return null;
	}
	
	private List<String> getProspectCallSearchCampaignIds(ProspectCallSearchDTO prospectCallSearchDTO) {
		List<String> campaignIds = new ArrayList<String>();;
		if (prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty()) {
			campaignIds = qaService.getCampaignIdsByQa();
		} else {
//			DATE:19/12/2017	Added to show only Active and qaRequired campaign to qa
			List<String> allCampaignIds = new ArrayList<String>();
			allCampaignIds.addAll(prospectCallSearchDTO.getCampaignIds());
			for (String campaignId : allCampaignIds) {
				Campaign campaign = campaignRepository.findOneById(campaignId);
				if (campaign.getStatus() == CampaignStatus.RUNNING && campaign.getQaRequired()) {
                    campaignIds.add(campaign.getId());
                }
			}
//			campaignIds = Arrays.asList(prospectCallSearchDTO.getCampaignId());
		}
		return campaignIds;
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByProspectCallStatus(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<String> campaignIds = getProspectCallSearchCampaignIds(prospectCallSearchDTO);
		if (campaignIds != null && !campaignIds.isEmpty()) {
			return prospectCallLogRepository.getQaMetricsByProspectCallStatus(campaignIds, prospectCallSearchDTO);
		}
		return null;
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetrics(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		HashMap<QaMetrics, Integer> hashMap = new HashMap<QaMetrics, Integer>();
		List<String> agentIds = getAgentIdsAndCampaignIds();
		List<String> dispositionStatuses = new ArrayList<>();
		dispositionStatuses.add("SUCCESS");
		List<String> notScoredStatuses = new ArrayList<>();
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.name());
		int notScoredCount = 0;
		notScoredCount = prospectCallLogRepository.findByAgentIdsAndLeadStatuses(dispositionStatuses, agentIds,
				notScoredStatuses);
		hashMap.put(QaMetrics.COUNT_NOT_SCORED_PROSPECTCALL, notScoredCount);
		List<String> scoredStatuses = new ArrayList<>();
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.name());
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.ACCEPTED.name());
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.REJECTED.name());
		int scoredCount = 0;
		scoredCount = prospectCallLogRepository.findByAgentIdsAndLeadStatuses(dispositionStatuses, agentIds, scoredStatuses);
		hashMap.put(QaMetrics.COUNT_COMPLETE_PROSPECTCALL, scoredCount);
		return hashMap;
	}

	@Override
	public HashMap<QaMetrics, Integer> getSecondaryQaMetrics(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		HashMap<QaMetrics, Integer> hashMap = new HashMap<QaMetrics, Integer>();
		List<String> agentIds = getAgentIdsAndCampaignIds();
		List<String> dispositionStatuses = new ArrayList<>();
		dispositionStatuses.add("SUCCESS");
		List<String> notScoredStatuses = new ArrayList<>();
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_REJECTED.name());
		notScoredStatuses.add(XtaasConstants.LEAD_STATUS.QA_RETURNED.name());
		int notScoredCount = 0;
		notScoredCount = prospectCallLogRepository.findByAgentIdsAndLeadStatuses(dispositionStatuses, agentIds,
				notScoredStatuses);
		hashMap.put(QaMetrics.COUNT_NOT_SCORED_PROSPECTCALL, notScoredCount);
		List<String> scoredStatuses = new ArrayList<>();
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.ACCEPTED.name());
		scoredStatuses.add(XtaasConstants.LEAD_STATUS.REJECTED.name());
		int scoredCount = 0;
		scoredCount = prospectCallLogRepository.findByAgentIdsAndLeadStatuses(dispositionStatuses, agentIds, scoredStatuses);
		hashMap.put(QaMetrics.COUNT_COMPLETE_PROSPECTCALL, scoredCount);
		return hashMap;
	}

	@Override
	public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate() {
		List<String> campaignIds = qaService.getCampaignIdsByQa();
		if (campaignIds != null && !campaignIds.isEmpty()) {
			return prospectCallLogRepository.getQaMetricsByScoreRate(campaignIds);
		}
		return null;
	}
	
	private List<String> getAgentIdsByQa() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
 		List<AgentSearchResult> results = agentRepository.findByPartnerIdAndStatus(userLogin.getOrganization());
		List<String> agentIds = new ArrayList<String>();
		for (AgentSearchResult agentSearchResult : results) {
			agentIds.add(agentSearchResult.getId());
		}
		return agentIds;	
	}

	private List<String> getAgentIdsAndCampaignIds() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		List<Team> teams = teamRepository.findTeamsByQaId(userLogin.getId());
		List<String> agentIds = new ArrayList<String>();
		for (Team team : teams) {
			List<TeamResource> teamResources = team.getAgents();
			for (TeamResource teamResource : teamResources) {
				if (!agentIds.contains(teamResource.getResourceId())) {
					agentIds.add(teamResource.getResourceId());
				}
			}
		}
		return agentIds;
	}

}
