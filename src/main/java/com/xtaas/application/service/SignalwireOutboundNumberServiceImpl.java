package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.SignalwireOutboundNumberRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.SignalwireOutboundNumber;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.HttpService;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.CountryWiseDetailsDTO;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SignalwireOutboundNumberServiceImpl implements SignalwireOutboundNumberService {

	private static final Logger logger = LoggerFactory.getLogger(SignalwireOutboundNumberServiceImpl.class);

	private Map<String, List<SignalwireOutboundNumber>> signalwireOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> signalwirePoolMap = new HashMap<>();

	@Autowired
	private SignalwireOutboundNumberRepository signalwireOutboundNumberRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	HttpService httpService;

	@Autowired
	StateCallConfigRepository stateCallConfigRepository;

	@Override
	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool) {
		// TODO Auto-generated method stub
		return null;
	}

	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("accept", "*/*");
		headers.add("content-type", "application/json");
		// headers.add("Authorization",
		// "Basic
		// YzQ5YTUxMWEtMmQ3YS00ODE2LWIxMzktMzAyYWU3ZDdjNzRkOlBUMTc2ZGE5Y2ZiMGRlYTk0ZGRlYTU0Njg1OWYyZjJlYjVlY2U4ZjYwN2Q3OTNiMThm");
		headers.add("Authorization", "Basic " + getBase64EncodedAuthorizationString());
		return headers;
	}

	private String getBase64EncodedAuthorizationString() {
		String signalwireProjectId = ApplicationEnvironmentPropertyUtils.getSignalwireProjectId();
		String signalwireAuthToken = ApplicationEnvironmentPropertyUtils.getSignalwireAuthToken();
		String encodedString = Base64.getEncoder()
				.encodeToString((signalwireProjectId + ":" + signalwireAuthToken).getBytes());
		return encodedString;
	}

	@Override
	public Boolean refreshOutboundNumbers() {
		if (signalwireOutboundNumbersMap != null) {
			signalwireOutboundNumbersMap.clear();
		}
		return true;
	}

	@Override
	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId) {
		List<SignalwireOutboundNumber> outboundNumbers = new ArrayList<>();
		outboundNumbers = signalwireOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllSignalwireOutboundNumber();
				outboundNumbers = signalwireOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
					signalwireOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cacheSignalwireNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(phoneNumber, callRetryCount, countryName, organizationId, outboundNumbers,
				signalwirePoolMap);
		return number;
	}

	private String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId,
			List<SignalwireOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<SignalwireOutboundNumber> outboundNumberList = new ArrayList<>();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = callRetryCount % maxPoolSize;
		int areaCode = Integer.parseInt(findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = signalwireOutboundNumbersMap.get("XTAAS CALL CENTER");
		}

		SignalwireOutboundNumber outboundNumber = outboundNumberList
				.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replaceAll("[^\\d]", ""); // phone.replace("-", "");
		int length = removeSpclChar.length();
		/*
		 * handled phone numbers with length less than 10 digit & return first two digit
		 * as area code - e.g. Germany: +494023750
		 */
		if (length < 10) {
			return removeSpclChar.substring(0, 2);
		}
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = removeSpclChar.substring(startPoint, endPoint);
		return areaCode;
	}

	private void cacheSignalwireNumbersPoolDetails(List<SignalwireOutboundNumber> outboundNumbers) {
		for (SignalwireOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (signalwirePoolMap.get(key) == null) {
				signalwirePoolMap.put(key, outboundNumber.getPool());
			} else if (signalwirePoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				signalwirePoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

	private void cacheAllSignalwireOutboundNumber() {
		logger.info("cacheAllSignalwireOutboundNumber() :: Fetching signalwireoutboundnumbers from DB to cache.");
		List<SignalwireOutboundNumber> outboundNumbers = signalwireOutboundNumberRepository.findAll();
		signalwireOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("SIGNALWIRE"))
				.collect(Collectors.groupingBy(SignalwireOutboundNumber::getPartnerId));
	}

	@Override
	public SignalwireOutboundNumber save(SignalwireOutboundNumber signalwireOutboundNumber) {
		return signalwireOutboundNumberRepository.save(signalwireOutboundNumber);
	}

	@Async
	@Override
	public void refreshPhoneNumbers(PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		List<SignalwireOutboundNumber> numbersFromDB = signalwireOutboundNumberRepository
				.findOutboundNumbers(plivoPhoneNumberPurchaseDTO.getPartnerId());
		List<SignalwireOutboundNumber> filteredList = new ArrayList<>();
		List<String> distinctOutBoundNumbers = getDistinctOutBoundNumbers(plivoPhoneNumberPurchaseDTO.getPartnerId());
		for (String number : distinctOutBoundNumbers) {
			List<SignalwireOutboundNumber> num = numbersFromDB.stream()
					.filter(v -> v.getPhoneNumber().equalsIgnoreCase(number)).collect(Collectors.toList());
			filteredList.add(num.get(0));
		}
		int count = 0;
		if (filteredList != null && filteredList.size() > 0) {
			for (SignalwireOutboundNumber pnumber : filteredList) {
				String phoneNumber = searchNumber(plivoPhoneNumberPurchaseDTO);
				if (phoneNumber != null) {
					count++;
					deleteNumberFromSignalwirePortal(pnumber.getSignalwireId());
					deleteSignalwireNumberFromDB(pnumber.getPhoneNumber());
					addSignalwireNumbersInCountries(
							buySignalwireNumberAndCreateEntryInDB(plivoPhoneNumberPurchaseDTO, phoneNumber, pnumber),
							plivoPhoneNumberPurchaseDTO);
				}
			}
			this.refreshOutboundNumbers();
		}
		String msg = "Out of " + distinctOutBoundNumbers.size() + " Signalwire numbers " + count
				+ "got refreshed successfully.";
		if (plivoPhoneNumberPurchaseDTO.getEmails() != null && plivoPhoneNumberPurchaseDTO.getEmails().size() > 0) {
			for (String email : plivoPhoneNumberPurchaseDTO.getEmails()) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",
						"Signalwire Number Refresh of " + plivoPhoneNumberPurchaseDTO.getPartnerId(), msg);
			}
		}
	}

	private void deleteSignalwireNumberFromDB(String phoneNumber) {
		signalwireOutboundNumberRepository.deleteManyBySignalwirePhoneNumber(phoneNumber);
	}

	public List<String> getDistinctOutBoundNumbers(String partnerId) {
		DistinctIterable<String> numbersFromDB = null;
		List<String> partnerIds = new ArrayList<String>();
		partnerIds.add(partnerId);
		try {
			Document elemMatch = new Document();
			elemMatch.put("partnerId", new Document("$in", partnerIds));
			numbersFromDB = mongoTemplate.getCollection("signalwireoutboundnumber").distinct("phoneNumber", elemMatch,
					String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> distinctNumbersFromDB = new ArrayList<>();
		if (numbersFromDB != null) {
			MongoCursor<String> cursor = numbersFromDB.iterator();
			while (cursor.hasNext()) {
				distinctNumbersFromDB.add(cursor.next());
			}
		}
		return distinctNumbersFromDB;
	}

	private String searchNumber(PlivoPhoneNumberPurchaseDTO phoneDTO) {
		try {
			boolean flag = true;
			while (flag) {
				String randomPattern = XtaasUtils.getPlivoPhoneNumberPatterns()
						.get(new Random().nextInt(XtaasUtils.getPlivoPhoneNumberPatterns().size()));
				// Map<String, String> data = new HashMap<>();
				// data.put("max_results", "1");
				// data.put("contains", randomPattern);
				// ObjectMapper mapper = new ObjectMapper();
				String searchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1&contains="
						+ randomPattern;
				Map<String, Object> response = httpService.get(searchUrl, headers(),
						new ParameterizedTypeReference<Map<String, Object>>() {
						});
				if (response != null && response.get("data") != null
						&& ((List<Map<String, String>>) response.get("data")).size() > 0) {
					Map<String, String> phoneNumberObj = ((List<Map<String, String>>) response.get("data")).get(0);
					String phoneNumber = phoneNumberObj.get("e164");
					flag = false;
					return phoneNumber;
				} else {
					String defaultSearchUrl = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/search?max_results=1";
					Map<String, Object> searchResponse = httpService.get(defaultSearchUrl, headers(),
							new ParameterizedTypeReference<Map<String, Object>>() {
							});
					if (searchResponse != null && searchResponse.get("data") != null
							&& ((List<Map<String, String>>) searchResponse.get("data")).size() > 0) {
						Map<String, String> phoneNumberObj = ((List<Map<String, String>>) searchResponse.get("data"))
								.get(0);
						String phoneNumber = phoneNumberObj.get("e164");
						flag = false;
						return phoneNumber;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void addSignalwireNumbersInCountries(SignalwireOutboundNumber signalwireNumber,
			PlivoPhoneNumberPurchaseDTO phoneDTO) {
		List<CountryWiseDetailsDTO> countryDetails = getUniqueCountryDetails();
		for (CountryWiseDetailsDTO countryWiseDetailsDTO : countryDetails) {
			SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(
					signalwireNumber.getDisplayPhoneNumber(), signalwireNumber.getDisplayPhoneNumber(),
					signalwireNumber.getAreaCode(), signalwireNumber.getIsdCode(), "SIGNALWIRE",
					signalwireNumber.getPartnerId(), signalwireNumber.getPartnerId(),
					signalwireNumber.getVoiceMessage(), signalwireNumber.getPool(), countryWiseDetailsDTO.getTimeZone(),
					countryWiseDetailsDTO.getCountryCode(), countryWiseDetailsDTO.getCountryName(),
					signalwireNumber.getSignalwireId());
			this.save(signalwireOutboundNumber);
		}
	}

	private List<CountryWiseDetailsDTO> getUniqueCountryDetails() {
		List<CountryWiseDetailsDTO> countryWiseList = new ArrayList<CountryWiseDetailsDTO>();
		List<StateCallConfig> stateCallConfigDetails = stateCallConfigRepository.findAll();
		if (stateCallConfigDetails != null && stateCallConfigDetails.size() > 0) {
			for (StateCallConfig stateCallConfig : stateCallConfigDetails) {
				if (stateCallConfig.getCountryName() != "United States") {
					List<CountryWiseDetailsDTO> filterList = new ArrayList<CountryWiseDetailsDTO>();
					if (countryWiseList != null && countryWiseList.size() > 0) {
						filterList = countryWiseList.stream()
								.filter(stateCall -> stateCall.getCountryName()
										.equalsIgnoreCase(stateCallConfig.getCountryName()))
								.collect(Collectors.toList());
					}
					if (filterList.size() == 0) {
						CountryWiseDetailsDTO countryDTO = new CountryWiseDetailsDTO();
						countryDTO.setCountryCode(stateCallConfig.getCountryCode());
						countryDTO.setCountryName(stateCallConfig.getCountryName());
						countryDTO.setTimeZone(stateCallConfig.getTimezone());
						countryWiseList.add(countryDTO);
					}
				}
			}
		}
		return countryWiseList;
	}

	private SignalwireOutboundNumber buySignalwireNumberAndCreateEntryInDB(PlivoPhoneNumberPurchaseDTO phoneDTO,
			String phoneNumberToPurchase, SignalwireOutboundNumber signalwireOutboundNumberFromDB) {
		try {
			String urlForPurchase = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers";
			Map<String, Object> purchaseResponse = httpService.post(urlForPurchase, headers(),
					new ParameterizedTypeReference<Map<String, Object>>() {
					}, "{\"number\": \"" + phoneNumberToPurchase + "\"}");
			if (purchaseResponse != null && purchaseResponse.get("number") != null) {
				String number = (String) purchaseResponse.get("number");
				String id = (String) purchaseResponse.get("id");
				String partnerId = phoneDTO.getPartnerId();
				String region = phoneDTO.getRegion();
				int areaCode = Integer.valueOf(number.substring(1, 4));
				String voiceMessage = "Thank you for calling. You may leave us a message at the tone. Thank You.";
				SignalwireOutboundNumber signalwireOutboundNumber = new SignalwireOutboundNumber(number, number,
						areaCode, signalwireOutboundNumberFromDB.getIsdCode(), "SIGNALWIRE",
						signalwireOutboundNumberFromDB.getPartnerId(), signalwireOutboundNumberFromDB.getPartnerId(),
						signalwireOutboundNumberFromDB.getVoiceMessage(), signalwireOutboundNumberFromDB.getPool(),
						phoneDTO.getTimezone(), phoneDTO.getCountryISO(), phoneDTO.getRegion(), id);
				signalwireOutboundNumber = save(signalwireOutboundNumber);
				// String urlForUpdate =
				// "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/" + id;
				// Map<String, Object> updateResponse = httpService.put(urlForUpdate, headers(),
				// new ParameterizedTypeReference<Map<String, Object>>() {
				// }, "{ \"name\": \"" + number + "\", \"call_handler\": \"laml_application\",
				// \"call_laml_application_id\": \""
				// + phoneDTO.getAppId() + "\"}");
				return signalwireOutboundNumber;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean deleteNumberFromSignalwirePortal(String id) {
		try {
			String urlForDelete = "https://xtaascorp.signalwire.com/api/relay/rest/phone_numbers/" + id;
			httpService.delete(urlForDelete, headers(), null);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<SignalwireOutboundNumber> getAllOutboundNumbers() {
		return signalwireOutboundNumberRepository.findAll();
	}

}
