package com.xtaas.application.service;

import com.xtaas.db.entity.QaRejectReason;

public interface QaRejectReasonService {

	public QaRejectReason getQaRejectReason(String id);

}
