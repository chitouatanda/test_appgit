package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.valueobject.CampaignContactSource;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.RestCampaignContactDTO;

@Component
public class RestDataUpload implements Callable<List<RestCampaignContactDTO>> {

	private static final Logger logger = LoggerFactory.getLogger(RestDataUpload.class);

	private CampaignContactRepository campaignContactRepository;

	private StateCallConfigRepository stateCallConfigRepository;

	private ProspectCallLogRepository prospectCallLogRepository;

	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	private List<RestCampaignContactDTO> campaignContacts;

	static Properties properties = new Properties();

	List<String> runningCampaignIds = new ArrayList<String>();

	public RestDataUpload() {

	}

	public RestDataUpload(List<RestCampaignContactDTO> restCampaignContactDTOs) {
		this.campaignContacts = restCampaignContactDTOs;
		stateCallConfigRepository = BeanLocator.getBean("stateCallConfigRepository", StateCallConfigRepository.class);
		campaignContactRepository = BeanLocator.getBean("campaignContactRepository", CampaignContactRepository.class);
		prospectCallLogRepository = BeanLocator.getBean("prospectCallLogRepository", ProspectCallLogRepository.class);
		prospectCallInteractionRepository = BeanLocator.getBean("prospectCallInteractionRepository",
				ProspectCallInteractionRepository.class);
	}

	@Override
	public List<RestCampaignContactDTO> call() throws Exception {
		logger.debug("call() :");
//		return ValidateData();
		return campaignContacts;
	}

//	private List<RestCampaignContactDTO> ValidateData() {
//		List<UploadedContact> uploadedContactList = new ArrayList<>();
//		List<CampaignContact> campaignContactList = new ArrayList<>();
//		List<String> errorList = new ArrayList<>();
//		if (campaignContacts != null && campaignContacts.size() > 0) {
//			logger.info("uploadRestData() : validating data");
//			for (RestCampaignContactDTO restCampaignContactDTO : campaignContacts) {
//				CampaignContact tempCampaignContact = null;
//				UploadedContact uploadedContact = new UploadedContact();
//				boolean isRecordValid = true;
//				if (restCampaignContactDTO.getFirstName() != null && !restCampaignContactDTO.getFirstName().isEmpty()) {
//					String firstName = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getFirstName());
//					firstName = (firstName.length() < 250) ? firstName : firstName.substring(0, 250);
//					uploadedContact.setFirstName(firstName);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "First Name should not empty. ");
//				}
//				if (restCampaignContactDTO.getLastName() != null && !restCampaignContactDTO.getLastName().isEmpty()) {
//					String lastName = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getLastName());
//					lastName = (lastName.length() < 250) ? lastName : lastName.substring(0, 250);
//					uploadedContact.setLastName(lastName);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Last Name should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getCampaignId() != null
//						&& !restCampaignContactDTO.getCampaignId().isEmpty()) {
//					uploadedContact.setCampaignId(restCampaignContactDTO.getCampaignId());
//
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "CampaignId should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getSource() != null && !restCampaignContactDTO.getSource().isEmpty()) {
//					try {
//						String source = XtaasUtils
//								.removeNonAsciiChars(validateSourceFieldValue(restCampaignContactDTO.getSource()));
//						uploadedContact.setSource(source);
//
//					} catch (IllegalArgumentException e) {
//						logger.info("excelFileRead():validateExcelFileRecords() : " + e.getMessage());
//						errorList.add(e.getMessage());
//						isRecordValid = false;
//					}
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Source should not empty. ");
//				}
//				if (restCampaignContactDTO.getSourceId() != null && !restCampaignContactDTO.getSourceId().isEmpty()) {
//					String sourceId = XtaasUtils.removeNonAsciiChars(
//							validateMultiTypeFieldValue(restCampaignContactDTO.getSourceId(), "SourceId"));
//					if (isSourceIdAlreadyPurchased(sourceId)) {
//						logger.info("UploadRestData() " + ":- " + sourceId + " PersonId is already purchased ");
//						errorList.add(sourceId + " PersonId is already purchased");
//						isRecordValid = false;
//					} else {
//						uploadedContact.setSourceId(sourceId);
//					}
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "SourceId should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getTitle() != null && !restCampaignContactDTO.getTitle().isEmpty()) {
//					String title = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getTitle());
//					title = (title.length() < 250) ? title : title.substring(0, 250);
//					uploadedContact.setTitle(title);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Title should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getDepartment() != null
//						&& !restCampaignContactDTO.getDepartment().isEmpty()) {
//					String department = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getDepartment());
//					department = (department.length() < 250) ? department : department.substring(0, 250);
//					uploadedContact.setDepartment(department);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Department should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getOrganizationName() != null
//						&& !restCampaignContactDTO.getOrganizationName().isEmpty()) {
//					String orgname = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getOrganizationName());
//					orgname = (orgname != null && orgname.length() < 250) ? orgname : orgname.substring(0, 250);
//					uploadedContact.setOrganizationName(orgname);
//				} else {
//					isRecordValid = false;
//					errorList
//							.add(restCampaignContactDTO.getSourceId() + ":- " + "Organization name should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getDomain() != null && !restCampaignContactDTO.getDomain().isEmpty()) {
//					String domain = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getDomain());
//					domain = (domain.length() < 250) ? domain : domain.substring(0, 250);
//					uploadedContact.setDomain(domain);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Domain should not empty. ");
//				}
//				try {
//					uploadedContact.setMinRevenue(
//							validateDoubleTypeFieldValue(restCampaignContactDTO.getMinRevenue(), "Minimum Revenue"));
//				} catch (Exception e) {
//					errorList.add(
//							restCampaignContactDTO.getSourceId() + ":- " + "Minimum revenue" + ":- " + e.getMessage());
//					isRecordValid = false;
//				}
//				try {
//					uploadedContact.setMaxRevenue(
//							validateDoubleTypeFieldValue(restCampaignContactDTO.getMaxRevenue(), "Maximum Revenue"));
//				} catch (Exception e) {
//					errorList.add(
//							restCampaignContactDTO.getSourceId() + ":- " + "Maximum revenue" + ":- " + e.getMessage());
//					isRecordValid = false;
//				}
//
//				try {
//					uploadedContact.setMinEmployeeCount(validateDoubleTypeFieldValue(
//							restCampaignContactDTO.getMinEmployeeCount(), "Minimum Employee Count"));
//				} catch (IllegalArgumentException e) {
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Minimum Employee Count" + ":- "
//							+ e.getMessage());
//					isRecordValid = false;
//				}
//
//				try {
//					uploadedContact.setMaxEmployeeCount(validateDoubleTypeFieldValue(
//							restCampaignContactDTO.getMaxEmployeeCount(), "Maximum Employee Count"));
//				} catch (IllegalArgumentException e) {
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Maximum Employee Count" + ":- "
//							+ e.getMessage());
//					isRecordValid = false;
//				}
//
//				if (restCampaignContactDTO.getEmail() != null && !restCampaignContactDTO.getEmail().isEmpty()) {
//					String email = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getEmail());
//					email = (email == null || email.length() < 250) ? email : email.substring(0, 250);
//					uploadedContact.setEmail(email);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "email should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getAddressLine() != null
//						&& !restCampaignContactDTO.getAddressLine().isEmpty()) {
//					String addressLine = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getAddressLine());
//					addressLine = (addressLine.length() < 250) ? addressLine : addressLine.substring(0, 250);
//					uploadedContact.setAddressLine(addressLine);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Address should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getCity() != null && !restCampaignContactDTO.getCity().isEmpty()) {
//					String city = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getCity());
//					city = (city == null || city.length() < 250) ? city : city.substring(0, 250);
//					uploadedContact.setCity(city);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "City should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getStateCode() != null && !restCampaignContactDTO.getStateCode().isEmpty()) {
//					String stateCode = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getStateCode());
//					stateCode = (stateCode == null || stateCode.length() < 250) ? stateCode
//							: stateCode.substring(0, 250);
//					StateCallConfig callConfig = null;
//					try {
//						callConfig = stateCallConfigRepository.findByStateCode(stateCode);
//						if (callConfig != null) {
//							uploadedContact.setStateCode(stateCode);
//						} else {
//							isRecordValid = false;
//							errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "State Code " + stateCode
//									+ " dosen't exist.");
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "State Code should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getCountry() != null && !restCampaignContactDTO.getCountry().isEmpty()) {
//					String country = XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getCountry());
//					country = (country == null || country.length() < 250) ? country : country.substring(0, 250);
//					uploadedContact.setCountry(country);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Country should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getPostalCode() != null
//						&& !restCampaignContactDTO.getPostalCode().isEmpty()) {
//					try {
//						uploadedContact.setPostalCode(
//								validateMultiTypeFieldValue(restCampaignContactDTO.getPostalCode(), "Postal Code"));
//					} catch (IllegalArgumentException e) {
//						errorList.add(e.getMessage());
//						isRecordValid = false;
//					}
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Postal Code should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getPhone() != null && !restCampaignContactDTO.getPhone().isEmpty()) {
//					try {
//						String phone = XtaasUtils.removeNonAsciiChars(
//								validateMultiTypeFieldValue(restCampaignContactDTO.getPhone(), "Phone"));
//						phone = (phone == null || phone.length() < 250) ? phone : phone.substring(0, 250);
//						if ((phone.startsWith("91") || phone.startsWith("44") || phone.startsWith("33")
//								|| phone.startsWith("49")) && !phone.startsWith("+")) {
//							phone = "+" + phone;
//						}
//						uploadedContact.setPhone(phone);
//					} catch (IllegalArgumentException e) {
//						errorList.add(e.getMessage());
//						isRecordValid = false;
//					}
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Phone should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getExtension() != null && !restCampaignContactDTO.getExtension().isEmpty()) {
//					try {
//						String ext = XtaasUtils.removeNonAsciiChars(
//								validateMultiTypeFieldValue(restCampaignContactDTO.getExtension(), "Extension"));
//						uploadedContact.setExtension(ext);
//					} catch (IllegalArgumentException e) {
//						errorList.add(e.getMessage());
//						isRecordValid = false;
//					}
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Extension should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getListPurchaseTransactionId() != null
//						&& !restCampaignContactDTO.getListPurchaseTransactionId().isEmpty()) {
//					uploadedContact.setListPurchaseTransactionId(restCampaignContactDTO.getListPurchaseTransactionId());
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- "
//							+ "List purchase transaction id should not empty. ");
//				}
//
//				if (restCampaignContactDTO.getManagementLevel() != null
//						&& !restCampaignContactDTO.getManagementLevel().isEmpty()) {
//					String managementLevel = XtaasUtils.removeNonAsciiChars(validateMultiTypeFieldValue(
//							restCampaignContactDTO.getManagementLevel(), "ManagementLevel"));
//					managementLevel = (managementLevel == null || managementLevel.length() < 250) ? managementLevel
//							: managementLevel.substring(0, 250);
//					uploadedContact.setManagementLevel(managementLevel);
//				} else {
//					isRecordValid = false;
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + "Management Level should not empty. ");
//				}
//
//				uploadedContact.setDirectPhone(restCampaignContactDTO.isDirectPhone());
//
//				if (restCampaignContactDTO.getCompanyValidationLink() != null) {
//					String zoomCompanyUrl = XtaasUtils
//							.removeNonAsciiChars(restCampaignContactDTO.getCompanyValidationLink());
//					uploadedContact.setZoomCompanyUrl(zoomCompanyUrl);
//				}
//
//				if (restCampaignContactDTO.getTitleValidationLink() != null
//						&& !restCampaignContactDTO.getTitleValidationLink().isEmpty()) {
//					String zoomPersonUrl = XtaasUtils
//							.removeNonAsciiChars(restCampaignContactDTO.getTitleValidationLink());
//					uploadedContact.setZoomPersonUrl(zoomPersonUrl);
//				}
//
//				if (isRecordValid && isPersonRecordAlreadyPurchased(uploadedContact)) {
//					logger.info("contactListRead():validateListRecords()" + restCampaignContactDTO.getSourceId() + ":- "
//							+ " Combination of First Name " + uploadedContact.getFirstName() + " and Last Name "
//							+ uploadedContact.getLastName() + " and Company Name "
//							+ uploadedContact.getOrganizationName() + " is already purchased");
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- " + " Combination of First Name "
//							+ uploadedContact.getFirstName() + " and Last Name " + uploadedContact.getLastName()
//							+ " and Company Name " + uploadedContact.getOrganizationName() + " is already purchased");
//					isRecordValid = false;
//				}
//				
//				if (!campaignContactList.isEmpty()) {
//					List<CampaignContact> filteredCampaignContactList = null;
//					filteredCampaignContactList = campaignContactList.stream()
//							.filter(cc -> cc.getFirstName().equals(restCampaignContactDTO.getFirstName())
//									&& cc.getLastName().equals(restCampaignContactDTO.getLastName())
//									&& cc.getOrganizationName().equals(restCampaignContactDTO.getOrganizationName())
//									&& cc.getTitle().equals(restCampaignContactDTO.getTitle()))
//							.collect(Collectors.toList());
//					if (!filteredCampaignContactList.isEmpty()) {
//						errorList.add(restCampaignContactDTO.getSourceId() + " Combination of First Name "
//								+ restCampaignContactDTO.getFirstName() + " and Last Name " + restCampaignContactDTO.getLastName()
//								+ " and Company Name " + restCampaignContactDTO.getOrganizationName() + " is already in file");
//						isRecordValid = false;
//					}
//				}
//				
//				if (isRecordValid && !isPhoneValid(restCampaignContactDTO.getPhone(), restCampaignContactDTO.getCountry())) {
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- Phone number is Invalid.");
//					isRecordValid = false;
//				}
//				
//				if (restCampaignContactDTO.getMaxRevenue() > restCampaignContactDTO.getMinRevenue()) {
//					KeyValuePair<Long, Long> revenueMap = getRevenueMap(restCampaignContactDTO.getMinRevenue(), restCampaignContactDTO.getMaxRevenue());
//					restCampaignContactDTO.setMinRevenue(revenueMap.getKey());
//					restCampaignContactDTO.setMaxRevenue(revenueMap.getValue());
//				} else {
//					errorList.add(restCampaignContactDTO.getSourceId() + ":- MaxRevenue should be greater than MinRevenue.");
//					isRecordValid = false;
//				}
//				
//				if ((restCampaignContactDTO.getMaxEmployeeCount() > 0.0 && restCampaignContactDTO.getMinEmployeeCount() > 0.0) && (restCampaignContactDTO.getMaxEmployeeCount() > restCampaignContactDTO.getMinEmployeeCount())) {
//					KeyValuePair<Long, Long> employeeMap = getEmployeeMap(restCampaignContactDTO.getMinEmployeeCount(),
//							restCampaignContactDTO.getMaxEmployeeCount());
//					restCampaignContactDTO.setMinEmployeeCount(employeeMap.getKey());
//					restCampaignContactDTO.setMaxEmployeeCount(employeeMap.getValue());
//				} else {
//					if (restCampaignContactDTO.getMaxEmployeeCount() == 0.0 || restCampaignContactDTO.getMinEmployeeCount() == 0.0) {
//						errorList.add(restCampaignContactDTO.getSourceId()
//								+ ":- Max employee count and min employee must be greater than 0.");
//						isRecordValid = false;
//					} else {
//						errorList.add(restCampaignContactDTO.getSourceId()
//								+ ":- Max employee count must be greater than min employee count.");
//						isRecordValid = false;
//					}
//				}		
//				
//				if (isRecordValid) {
//					tempCampaignContact = new CampaignContact(uploadedContact.getCampaignId(),
//							uploadedContact.getSource(), uploadedContact.getSourceId(), null,
//							uploadedContact.getFirstName(), uploadedContact.getLastName(), null,
//							uploadedContact.getTitle(), uploadedContact.getDepartment(), null,
//							uploadedContact.getOrganizationName(), uploadedContact.getDomain(), null, null,
//							uploadedContact.getMinRevenue(), uploadedContact.getMaxRevenue(),
//							uploadedContact.getMinEmployeeCount(), uploadedContact.getMaxEmployeeCount(),
//							uploadedContact.getEmail(), uploadedContact.getAddressLine(), null,
//							uploadedContact.getCity(), uploadedContact.getStateCode(), uploadedContact.getCountry(),
//							uploadedContact.getPostalCode(), uploadedContact.getPhone(), null, null,
//							uploadedContact.getManagementLevel(), null, false);
//					tempCampaignContact.setDirectPhone(uploadedContact.isDirectPhone());
//					tempCampaignContact.setZoomCompanyUrl(uploadedContact.getZoomCompanyUrl());
//					tempCampaignContact.setZoomPersonUrl(uploadedContact.getZoomPersonUrl());
//					tempCampaignContact.setExtension(uploadedContact.getExtension());
//					campaignContactList.add(tempCampaignContact);
//					uploadedContactList.add(uploadedContact);
//				} else {
//				restCampaignContactDTO.setErrors(errorList);
//				}
//			}
//
//			if (campaignContactList.size() > 0) {
//				List<CampaignContact> campaignContacts1 = campaignContactRepository.save(campaignContactList);
//				if (campaignContacts1 != null && !campaignContacts1.isEmpty()) {
//
//					List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
//					List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();
//					for (CampaignContact campaignContact : campaignContacts1) {
//						Prospect prospect = campaignContact.toProspectFromExcel(null);
//						prospect.setSource(campaignContact.getSource());
//						prospect.setSourceId(campaignContact.getSourceId());
//						prospect.setCampaignContactId(campaignContact.getId());
//						ProspectCallLog prospectCallLog = new ProspectCallLog();
//						ProspectCall prospectCall = new ProspectCall(UUID.randomUUID().toString(),
//								campaignContact.getCampaignId(), prospect);
//						// prospectCallLog.setProspectCall(new
//						// ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));
//
//						Optional<UploadedContact> contacts = uploadedContactList.stream()
//								.filter(contact -> contact.getFirstName().equals(prospect.getFirstName())
//										&& contact.getLastName().equals(prospect.getLastName())
//										&& contact.getOrganizationName().equals(prospect.getCompany()))
//								.findFirst();
//						if (contacts.isPresent()) {
//							prospectCall.setProspectVersion(contacts.get().getProspectVersion());
//						}
//
//						StateCallConfig scc = stateCallConfigRepository
//								.findByStateCode(prospectCall.getProspect().getStateCode());
//						prospectCall.getProspect().setTimeZone(scc.getTimezone());
//
//						StringBuffer sb = new StringBuffer();
//						String emp = "";
//						String mgmt = "";
//
//						String tz = getStateWeight(scc.getTimezone());
//
//						sb.append(prospectCallLog.getStatus());
//						sb.append("|");
//						if (prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number) {
//							Number n1 = (Number) prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount");
//							sb.append(getBoundry(n1, "min"));
//							sb.append("-");
//							emp = getEmployeeWeight(n1);
//						}
//
//						if (prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
//							Number n2 = (Number) prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount");
//							sb.append(getBoundry(n2, "max"));
//						}
//
//						sb.append("|");
//						sb.append(getMgmtLevel(prospectCall.getProspect().getManagementLevel()));
//						mgmt = getMgmtLevelWeight(prospectCall.getProspect().getManagementLevel());
//						String direct = "";
//						if (prospectCall.getProspect().isDirectPhone()) {
//							direct = "1";
//						} else {
//							direct = "2";
//						}
//						int qbSort = 0;
//						if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
//							String sortString = direct + tz + mgmt + emp;
//							qbSort = Integer.parseInt(sortString);
//						}
//
//						prospectCall.setQualityBucket(sb.toString());
//						prospectCall.setQualityBucketSort(qbSort);
//						prospectCallLog.setProspectCall(prospectCall);
//						prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
//						prospectCallLogs.add(prospectCallLog);
//						ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
//						prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
//						prospectCallInteraction.setProspectCall(prospectCall);
//						prospectCallInteractions.add(prospectCallInteraction);
//					}
//					prospectCallLogRepository.save(prospectCallLogs);
//					prospectCallInteractionRepository.save(prospectCallInteractions);
//
//				}
//			}
//		}
//		return campaignContacts;
//	}
	
	private KeyValuePair<Long, Long> getEmployeeMap(Double minEmployee, Double maxEmployee) {

		Long zoomMinEmp = new Long(0);
		Long zoomMaxEmp = new Long(0);

		long minEmp = minEmployee.longValue();
		long maxEmp = maxEmployee.longValue();

		KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

		if (minEmp > 0 && minEmp < 5) {
			zoomMinEmp = new Long(1);
		} else if (minEmp >= 5 && minEmp < 10) {
			zoomMinEmp = new Long(5);
		} else if (minEmp >= 10 && minEmp < 20) {
			zoomMinEmp = new Long(10);
		} else if (minEmp >= 20 && minEmp < 50) {
			zoomMinEmp = new Long(20);
		} else if (minEmp >= 50 && minEmp < 100) {
			zoomMinEmp = new Long(50);
		} else if (minEmp >= 100 && minEmp < 250) {
			zoomMinEmp = new Long(100);
		} else if (minEmp >= 250 && minEmp < 500) {
			zoomMinEmp = new Long(250);
		} else if (minEmp >= 500 && minEmp < 1000) {
			zoomMinEmp = new Long(500);
		} else if (minEmp >= 1000 && minEmp < 5000) {
			zoomMinEmp = new Long(1000);
		} else if (minEmp >= 5000 && minEmp < 10000) {
			zoomMinEmp = new Long(5000);
		} else if (minEmp >= 10000) {
			zoomMinEmp = new Long(10000);
		}

		if (maxEmp > 0 && maxEmp <= 5) {
			zoomMaxEmp = new Long(5);
		} else if (maxEmp > 5 && maxEmp <= 10) {
			zoomMaxEmp = new Long(10);
		} else if (maxEmp > 10 && maxEmp <= 20) {
			zoomMaxEmp = new Long(20);
		} else if (maxEmp > 20 && maxEmp <= 50) {
			zoomMaxEmp = new Long(50);
		} else if (maxEmp > 50 && maxEmp <= 100) {
			zoomMaxEmp = new Long(100);
		} else if (maxEmp > 100 && maxEmp <= 250) {
			zoomMaxEmp = new Long(250);
		} else if (maxEmp > 250 && maxEmp <= 500) {
			zoomMaxEmp = new Long(500);
		} else if (maxEmp > 500 && maxEmp <= 1000) {
			zoomMaxEmp = new Long(1000);
		} else if (maxEmp > 1000 && maxEmp <= 5000) {
			zoomMaxEmp = new Long(5000);
		} else if (maxEmp > 5000 && maxEmp <= 10000) {
			zoomMaxEmp = new Long(10000);
		} else if (maxEmp > 10000) {
			// TODO - need to validate
		}

		minMaxKeys.setKey(zoomMinEmp);
		minMaxKeys.setValue(zoomMaxEmp);
		return minMaxKeys;
	}
	
	private KeyValuePair<Long, Long> getRevenueMap(Double minRevenue, Double maxRevenue) {
		Long zoomMinRev = new Long(0);
		Long zoomMaxRev = new Long(0);
		KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

		long minRev = minRevenue.longValue();
		long maxRev = maxRevenue.longValue();

		if (minRev >= 0 && minRev < 5000000) {
			// TODO - need to validate
			// zoomMinRev = new Long(1);
		} else if (minRev >= 5000000 && minRev < 10000000) {
			zoomMinRev = new Long(5000000);
		} else if (minRev >= 10000000 && minRev < 25000000) {
			zoomMinRev = new Long(10000000);
		} else if (minRev >= 25000000 && minRev < 50000000) {
			zoomMinRev = new Long(25000000);
		} else if (minRev >= 50000000 && minRev < 100000000) {
			zoomMinRev = new Long(50000000);
		} else if (minRev >= 100000000 && minRev < 250000000) {
			zoomMinRev = new Long(100000000);
		} else if (minRev >= 250000000 && minRev < 500000000) {
			zoomMinRev = new Long(250000000);
		} else if (minRev >= 500000000 && minRev < 1000000000) {
			zoomMinRev = new Long(500000000);
		} else if (minRev >= 1000000000 && minRev < new Long("5000000000").longValue()) {
			zoomMinRev = new Long(1000000000);
		} else if (minRev >= new Long("5000000000").longValue()) {
			zoomMinRev = new Long("5000000000").longValue();
		}

		if (maxRev > 0 && maxRev <= 5000000) {
			zoomMaxRev = new Long(5000000);
		} else if (maxRev > 5000000 && maxRev <= 10000000) {
			zoomMaxRev = new Long(10000000);
		} else if (maxRev > 10000000 && maxRev <= 25000000) {
			zoomMaxRev = new Long(25000000);
		} else if (maxRev > 25000000 && maxRev <= 50000000) {
			zoomMaxRev = new Long(50000000);
		} else if (maxRev > 50000000 && maxRev <= 100000000) {
			zoomMaxRev = new Long(100000000);
		} else if (maxRev > 100000000 && maxRev <= 250000000) {
			zoomMaxRev = new Long(250000000);
		} else if (maxRev > 250000000 && maxRev <= 500000000) {
			zoomMaxRev = new Long(500000000);
		} else if (maxRev > 500000000 && maxRev <= 1000000000) {
			zoomMaxRev = new Long(1000000000);
		} else if (maxRev > 1000000000 && maxRev <= new Long("5000000000").longValue()) {
			zoomMaxRev = new Long("5000000000").longValue();
		} else if (maxRev > new Long("5000000000").longValue()) {
			// TODO - need to validate
		}

		minMaxKeys.setKey(zoomMinRev);
		minMaxKeys.setValue(zoomMaxRev);
		return minMaxKeys;
	}
	
	public boolean isPhoneValid(String phone, String country) {
		boolean valid = true;
		List<String> countries = Arrays.asList("US", "United States", "Canada");
		/**
		 * check for valid phone formats like "+123456489" "+1(850) 555-1234"
		 * "+1-850-555-1234" "+88 66 8880 1234" "3213123213"
		 */
		if (phone == null || phone.isEmpty()) {
			valid = false;
		} else if (!Pattern.compile("^[\\+\\d]?(?:[\\d-.\\s()]*)$").matcher(phone).matches()) {
			valid = false;
		} else if (country != null && countries.contains(country)
				&& !phone.matches("^\\(?([0-9]{3})\\)?[-]?([0-9]{3})[-.\\s]?([0-9]{4})$")) {
			valid = false;
		}
		return valid;
	}
	
	private boolean isPersonRecordAlreadyPurchased(UploadedContact uploadedContact) {
		String firstName = uploadedContact.getFirstName();
		String lastName = uploadedContact.getLastName();
		String phoneNumber = uploadedContact.getPhone();
		if (phoneNumber.contains("+")) {
			phoneNumber = phoneNumber.substring(1);
		}
		int uniqueRecord = prospectCallLogRepository.findManualProspectRecordCount(uploadedContact.campaignId,
				firstName, lastName, phoneNumber);
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;
		}
	}

	private String validateSourceFieldValue(String inputValue) {
		String value;
		if (inputValue.equalsIgnoreCase("ZOOMI")) {
			value = CampaignContactSource.ZOOMINFO.name();
		} else if (inputValue.equalsIgnoreCase("NETP")) {
			value = CampaignContactSource.NETPROSPEX.name();
		} else {
			value = inputValue;
		}
		return value;
	}

	private String validateMultiTypeFieldValue(String sourceId, String fieldName) {
		String value = "";
		if (fieldName.equals("Postal Code")) {
			int index = sourceId.indexOf('.');
			if (index > 0) {
				value = sourceId.substring(0, index);
			}
		} else {
			value = sourceId;
		}
		return value;
	}

	private boolean isSourceIdAlreadyPurchased(String sourceId) {
		boolean flag = false;
		if (sourceId == null || sourceId == "") {
			return false;
		}
		CampaignContact campaignContact = null;
		try {
			campaignContact = campaignContactRepository.findBySourceId(sourceId, "ZOOMINFO");
			if (campaignContact == null) {
				flag = false;
			} else {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	private double validateDoubleTypeFieldValue(double inputValue, String fieldName) {
		try {
			if (inputValue < 0) {
				throw new IllegalArgumentException("'" + fieldName + "' must be greater than 'zero'.");
			} else {
				return inputValue;
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("'" + inputValue + "' is not in correct format. ");
		}
	}

	public String getStateWeight(String timeZone) {

		String est = "US/Eastern";
		String cst = "US/Central";
		String mst = "US/Mountain";
		String pst = "US/Pacific";

		if (timeZone != null && timeZone.equalsIgnoreCase(pst)) {
			return "1";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(mst)) {
			return "2";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(cst)) {
			return "3";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(est)) {
			return "4";
		}
		return "5";
	}

	public String getBoundry(Number n, String minMax) {
		if (minMax.equalsIgnoreCase("min")) {
			if (n.intValue() < 5) {
				return "1";
			}
			if (n.intValue() < 10) {
				return "5";
			}
			if (n.intValue() < 20) {
				return "10";
			}
			if (n.intValue() < 50) {
				return "20";
			}
			if (n.intValue() < 100) {
				return "50";
			}
			if (n.intValue() < 250) {
				return "100";
			}
			if (n.intValue() < 500) {
				return "250";
			}
			if (n.intValue() < 1000) {
				return "500";
			}
			if (n.intValue() < 5000) {
				return "1000";
			}
			if (n.intValue() < 10000) {
				return "5000";
			}

			if (n.intValue() < 11000) {
				return "10000";
			}

		} else if (minMax.equalsIgnoreCase("max")) {
			if (n.intValue() <= 5) {
				return "5";
			}
			if (n.intValue() <= 10) {
				return "10";
			}
			if (n.intValue() <= 20) {
				return "20";
			}
			if (n.intValue() <= 50) {
				return "50";
			}
			if (n.intValue() <= 100) {
				return "100";
			}
			if (n.intValue() <= 250) {
				return "250";
			}
			if (n.intValue() <= 500) {
				return "500";
			}
			if (n.intValue() <= 1000) {
				return "1000";
			}
			if (n.intValue() <= 5000) {
				return "5000";
			}
			if (n.intValue() <= 10000) {
				return "10000";
			}

			if (n.intValue() <= 100000) {
				return "11000";
			}

		}

		return "11000";
		/*
		 * 1-5 5-10 10-20 20-50 50-100 100-250 250-500 500-1000 1000-5000 5000-10000
		 * 10000-11000
		 */
	}

	public String getMgmtLevelWeight(String managementLevel) {
		if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "5";
		} else if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "4";
		} else if (managementLevel.equalsIgnoreCase("Director")) {
			return "3";
		} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
			return "2";
		} else {
			return "1";
		}

		// return "";

	}

	public String getEmployeeWeight(Number n) {

		if (n.intValue() < 5) {
			return "01";
		}
		if (n.intValue() < 10) {
			return "02";
		}
		if (n.intValue() < 20) {
			return "03";
		}
		if (n.intValue() < 50) {
			return "04";
		}
		if (n.intValue() < 100) {
			return "05";
		}
		if (n.intValue() < 250) {
			return "06";
		}
		if (n.intValue() < 500) {
			return "07";
		}
		if (n.intValue() < 1000) {
			return "08";
		}
		if (n.intValue() < 5000) {
			return "09";
		}
		if (n.intValue() < 10000) {
			return "10";
		}

		if (n.intValue() < 11000) {
			return "11";
		}

		return "12";
	}

	public String getMgmtLevel(String managementLevel) {
		if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "VP-Level";
		} else if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "C-Level";
		} else if (managementLevel.equalsIgnoreCase("Director")) {
			return "DIRECTOR";
		} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
			return "MANAGER";
		} else {
			return "Non-Manager";
		}

	}

	class UploadedContact {

		private String campaignId;
		private String source;
		private String sourceId;
		private String firstName;
		private String lastName;
		private String title;
		private String department;
		private String organizationName;
		private String domain;
		private double minRevenue;
		private double maxRevenue;
		private double minEmployeeCount;
		private double maxEmployeeCount;
		private String email;
		private String addressLine;
		private String city;
		private String stateCode;
		private String country;
		private String postalCode;
		private String phone;
		private String extension;
		private String listPurchaseTransactionId;
		private String managementLevel;
		private boolean directPhone;
		private int prospectVersion;
		private String zoomCompanyUrl;
		private String zoomPersonUrl;

		public String getCampaignId() {
			return campaignId;
		}

		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getSourceId() {
			return sourceId;
		}

		public void setSourceId(String sourceId) {
			this.sourceId = sourceId;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDepartment() {
			return department;
		}

		public void setDepartment(String department) {
			this.department = department;
		}

		public String getOrganizationName() {
			return organizationName;
		}

		public void setOrganizationName(String organizationName) {
			this.organizationName = organizationName;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public double getMinRevenue() {
			return minRevenue;
		}

		public void setMinRevenue(double minRevenue) {
			this.minRevenue = minRevenue;
		}

		public double getMaxRevenue() {
			return maxRevenue;
		}

		public void setMaxRevenue(double maxRevenue) {
			this.maxRevenue = maxRevenue;
		}

		public double getMinEmployeeCount() {
			return minEmployeeCount;
		}

		public void setMinEmployeeCount(double minEmployeeCount) {
			this.minEmployeeCount = minEmployeeCount;
		}

		public double getMaxEmployeeCount() {
			return maxEmployeeCount;
		}

		public void setMaxEmployeeCount(double maxEmployeeCount) {
			this.maxEmployeeCount = maxEmployeeCount;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAddressLine() {
			return addressLine;
		}

		public void setAddressLine(String addressLine) {
			this.addressLine = addressLine;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getStateCode() {
			return stateCode;
		}

		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getExtension() {
			return extension;
		}

		public void setExtension(String extension) {
			this.extension = extension;
		}

		public String getListPurchaseTransactionId() {
			return listPurchaseTransactionId;
		}

		public void setListPurchaseTransactionId(String listPurchaseTransactionId) {
			this.listPurchaseTransactionId = listPurchaseTransactionId;
		}

		public String getManagementLevel() {
			return managementLevel;
		}

		public void setManagementLevel(String managementLevel) {
			this.managementLevel = managementLevel;
		}

		public boolean isDirectPhone() {
			return directPhone;
		}

		public void setDirectPhone(boolean directPhone) {
			this.directPhone = directPhone;
		}

		public int getProspectVersion() {
			return prospectVersion;
		}

		public void setProspectVersion(int prospectVersion) {
			this.prospectVersion = prospectVersion;
		}

		public String getZoomCompanyUrl() {
			return zoomCompanyUrl;
		}

		public void setZoomCompanyUrl(String zoomCompanyUrl) {
			this.zoomCompanyUrl = zoomCompanyUrl;
		}

		public String getZoomPersonUrl() {
			return zoomPersonUrl;
		}

		public void setZoomPersonUrl(String zoomPersonUrl) {
			this.zoomPersonUrl = zoomPersonUrl;
		}

	}

}
