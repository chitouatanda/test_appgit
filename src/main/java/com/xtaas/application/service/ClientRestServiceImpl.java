package com.xtaas.application.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.ExpressionGroup;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.service.ClientRestService;
import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.LoginDTO;
import com.xtaas.web.dto.RestCampaignDTO;
import com.xtaas.web.dto.RestClientQuestions;
import com.xtaas.web.dto.SupervisorDTO;

@Service
public class ClientRestServiceImpl implements ClientRestService {

	private static final Logger logger = LoggerFactory.getLogger(ClientRestServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Override
	public RestCampaignDTO getCampaign(String sfdcCampaignId) {
		Campaign campaign = campaignRepository.findCdpCampaign(sfdcCampaignId);
		if (campaign != null) {
			RestCampaignDTO response  =  new RestCampaignDTO(new CampaignDTO(campaign));
			if(campaign.getQualificationCriteria()!=  null) {
				List<RestClientQuestions> rsqList =  new ArrayList<RestClientQuestions>();
				List<Expression> expList = campaign.getQualificationCriteria().getExpressions();
				if(expList!=null && expList.size()>0) {
					for(Expression exp : expList) {
						List<String> options =  new ArrayList<String>();
						RestClientQuestions rsq = new RestClientQuestions();
						rsq.setAgentValidationRequired(exp.getAgentValidationRequired());
						rsq.setQuestionSkin(exp.getQuestionSkin());
						rsq.setOperator(exp.getOperator());
						rsq.setSelectedOptions(exp.getValue());
						if(exp.getPickList()!=null &&  exp.getPickList().size()>0) {
							for(PickListItem pli :  exp.getPickList()) {
								options.add(pli.getLabel());
							}
							rsq.setOptions(options);
						}else {
							rsq.setOptions(options);
						}
						rsqList.add(rsq);
					}
				}
				response.setQuestions(rsqList);
			}
			response.setTeamSupervisorId(campaign.getTeam().getSupervisorId());
			response.setPartnerSupervisors(campaign.getPartnerSupervisors());
			return response;
		} else {
			throw new IllegalArgumentException("Campaign does not exists.");
		}
	}

	@Override
	public RestCampaignDTO createCampaign(RestCampaignDTO campaignDTO) {
		if (campaignDTO.getSfdcCampaignId() == null || campaignDTO.getSfdcCampaignId().isEmpty()) {
			throw new IllegalArgumentException("Sfdc campaign id is required.");
		}
		Campaign cdpCampaign = campaignRepository.findCdpCampaign(campaignDTO.getSfdcCampaignId());
		if (cdpCampaign != null) {
			throw new IllegalArgumentException("Campaign already exists in the system.");
		}
		validateRequiredFields(campaignDTO);
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		Login cmLogin = userService.getUser(campaignDTO.getCampaignManagerId());
		if (cmLogin == null
				|| (cmLogin != null && (!cmLogin.getId().equalsIgnoreCase(campaignDTO.getCampaignManagerId())
						|| !cmLogin.getOrganization().equalsIgnoreCase(userLogin.getOrganization())))) {
			throw new IllegalArgumentException("Campaign manager is not valid.");
		}
		LinkedHashMap<String, Float> retrySpeedMap = new LinkedHashMap<>();
		QualificationCriteria qualificationCriteria = null;
		retrySpeedMap.put("R0", Float.parseFloat("1.5"));
		retrySpeedMap.put("R1", Float.parseFloat("1.5"));
		retrySpeedMap.put("R2", Float.parseFloat("1.5"));
		retrySpeedMap.put("R3", Float.parseFloat("1.5"));
		retrySpeedMap.put("R4", Float.parseFloat("2.0"));
		retrySpeedMap.put("R5", Float.parseFloat("2.0"));
		retrySpeedMap.put("R6", Float.parseFloat("2.0"));
		retrySpeedMap.put("R7", Float.parseFloat("2.2"));
		retrySpeedMap.put("R8", Float.parseFloat("2.2"));
		Campaign campaign = new Campaign("DEMANDSHORE", campaignDTO.getCampaignManagerId(), campaignDTO.getName(),
				campaignDTO.getStartDate(), campaignDTO.getEndDate(), CampaignTypes.LeadGeneration,
				qualificationCriteria, campaignDTO.getDeliveryTarget(), "", false);
		campaign.setDailyCap(campaignDTO.getDailyCap());
		campaign.setTimezone("US/Pacific");
		campaign.setSfdcCampaignId(campaignDTO.getSfdcCampaignId());
		if(campaignDTO.getType()!=null && (campaignDTO.getType().equalsIgnoreCase(CampaignTypes.LeadGeneration.name())
				|| campaignDTO.getType().equalsIgnoreCase(CampaignTypes.LeadVerification.name()))) {
			if(campaignDTO.getType().equalsIgnoreCase(CampaignTypes.LeadGeneration.name()))
				campaign.setType(CampaignTypes.LeadGeneration);
			else if(campaignDTO.getType().equalsIgnoreCase(CampaignTypes.LeadVerification.name()))
				campaign.setType(CampaignTypes.LeadVerification);
		}
		if (campaignDTO.getAssets() != null) {
			List<Asset> assets = new ArrayList<Asset>();
			for (String assetUrl : campaignDTO.getAssets()) {
				Asset asset = new Asset(null, "assetName", "description", encodeURL(assetUrl), false);
				assets.add(asset);
			}
			campaign.setAssets(assets);
		} else {
			campaign.setAssets(null);
		}
		List<String> supervisors = new ArrayList<String>();
		//setting up  partnersupervisors
		Organization org =  organizationRepository.findOneById(campaignDTO.getOrganizationId());
		if(org==null) {
			throw new IllegalArgumentException("Invalid  Organization Id");
		}else {
			StringBuffer sb = new  StringBuffer();
			List<String> supList  = new ArrayList<String>();
			if(campaignDTO.getPartnerSupervisors()!=null &&  campaignDTO.getPartnerSupervisors().size()>0) {
				supervisors =  campaignDTO.getPartnerSupervisors();
				for(String sup : supervisors) {
					List<Team> team = teamRepository.findBySupervisorId(sup);
					if(team==null) {
						supList.add(sup);
						sb.append(sup)	;
						sb.append(",")	;
					}
				}
				if(supList.size()>0) {
					throw new IllegalArgumentException(sb.toString()+" are Invalid  Supervisor Ids.");

				}
				campaign.setPartnerSupervisors(supervisors);
			}else {
				if(org.getPartnerSupervisors()!=null  &&  org.getPartnerSupervisors().size()>0) {
					campaign.setPartnerSupervisors(org.getPartnerSupervisors());
				}else {
					campaign.setPartnerSupervisors(supervisors);
				}
			}
		}
		//Configuring questions
		 if (campaignDTO.getQuestions() != null &&  campaignDTO.getQuestions().size()>0) {
				List<Expression> expressions = new ArrayList<Expression>();
				List<ExpressionGroup> expressionGroups = new ArrayList<ExpressionGroup>();
				for(RestClientQuestions  rcq: campaignDTO.getQuestions()) {
					List<PickListItem>  pickList  = new ArrayList<PickListItem>();
					if(rcq.getOptions()!=null  && rcq.getOptions().size()>0) {
						List<String> opList = rcq.getOptions();
						for(String op  : opList) {
							PickListItem pli  = new PickListItem();
							pli.setLabel(op);
							pli.setValue(op);

							pickList.add(pli);
						}
					}
					Boolean agentFlag  =  rcq.isAgentValidationRequired();
					Expression  ex = new Expression(rcq.getQuestionSkin().replaceAll("[^a-zA-Z0-9]", ""), rcq.getOperator(),
							rcq.getSelectedOptions(), agentFlag, rcq.getQuestionSkin(), (ArrayList<PickListItem>) pickList, "");
					expressions.add(ex);
				}

				QualificationCriteria qfc  = new QualificationCriteria("AND", expressions, expressionGroups);
				campaign.setQualificationCriteria(qfc);
		 }


		campaign.setCampaignRequirements(campaignDTO.getCampaignRequirements());
		campaign.setDialerMode(DialerMode.HYBRID);
		campaign.setBrand(campaignDTO.getOrganizationId());
		campaign.setDailyCallMaxRetries(3);
		campaign.setCallMaxRetries(10);
		campaign.setEnableMachineAnsweredDetection(false);
		campaign.setHidePhone(true);
		campaign.setQaRequired(true);
		campaign.setSipProvider("Telnyx");
		campaign.setUseSlice(false);
		campaign.setSimpleDisposition(false);
		campaign.setRetrySpeedEnabled(true);
		campaign.setLocalOutboundCalling(true);
		campaign.setLeadsPerCompany(campaignDTO.getLeadsPerCompany());
		if(campaignDTO.getDuplicateCompanyDuration()>0)
			campaign.setDuplicateCompanyDuration(campaignDTO.getDuplicateCompanyDuration());
		else
			campaign.setDuplicateCompanyDuration(180);// default
		if(campaignDTO.getDuplicateLeadsDuration()>0)
			campaign.setDuplicateLeadsDuration(campaignDTO.getDuplicateLeadsDuration());
		else
			campaign.setDuplicateLeadsDuration(365); // default
		campaign.setRetrySpeed(retrySpeedMap);
		campaign.setHostingServer("demandshore.herokuapp.com/");
		campaign.setMultiProspectCalling(true);
		campaign.setCallControlProvider("Plivo");
		campaign.setNotConference(false);
		campaign.setLeadIQ(false);

		//setting Team
		List<Team> teams = teamRepository.findBySupervisorId(campaignDTO.getTeamSupervisorId());
		if(teams!=null && teams.size()>0) {
			Team team  =  teams.get(0);
			CampaignTeam  cTeam = new  CampaignTeam(team.getId(), campaignDTO.getTeamSupervisorId(), 0);
			cTeam.setLeadSortOrder(LeadSortOrder.QUALITY_LIFO);
			cTeam.setCallSpeedPerMinPerAgent(CallSpeed.INTERMEDIATE);
			cTeam.setCampaignSpeed(0);
			campaign.setTeam(cTeam);
		}else {
			throw new IllegalArgumentException(campaignDTO.getTeamSupervisorId()+" is Invalid  Team Supervisor Id.");

		}

		RestCampaignDTO  response =  new RestCampaignDTO(new CampaignDTO(campaignRepository.save(campaign)));
		response.setQuestions(campaignDTO.getQuestions());
		response.setTeamSupervisorId(campaignDTO.getTeamSupervisorId());
		response.setPartnerSupervisors(campaignDTO.getPartnerSupervisors());

		return  response;
	}

	private void validateRequiredFields(RestCampaignDTO restCampaignDTO) {
		if (restCampaignDTO.getName() == null || restCampaignDTO.getName() == "") {
			throw new IllegalArgumentException("Campaign name is Required.");
		} else if (restCampaignDTO.getStartDate() == null) {
			throw new IllegalArgumentException("Start date is Required.");
		} else if (restCampaignDTO.getEndDate() == null) {
			throw new IllegalArgumentException("End date is required.");
		} else if (restCampaignDTO.getCampaignManagerId() == null || restCampaignDTO.getCampaignManagerId() == "") {
			throw new IllegalArgumentException("Campaign manager id is Required.");
		} else if (restCampaignDTO.getEndDate().compareTo(restCampaignDTO.getStartDate()) <= 0) {
			throw new IllegalArgumentException("End date should be greater than start date");
		} else if (restCampaignDTO.getDeliveryTarget() == 0) {
			throw new IllegalArgumentException("Delivery Target id is Required.");
		}else if (restCampaignDTO.getOrganizationId() == null) {
			throw new IllegalArgumentException("Organization id is Required.");
		}else if (restCampaignDTO.getTeamSupervisorId() == null) {
			throw new IllegalArgumentException("Team Supervisor id is Required.");
		}else if (restCampaignDTO.getPartnerSupervisors() == null  ||  restCampaignDTO.getPartnerSupervisors().isEmpty()) {
			throw new IllegalArgumentException("Partner Supervisor ids are Required.");
		}else if (restCampaignDTO.getQuestions() != null &&  restCampaignDTO.getQuestions().size()>0) {
			for(RestClientQuestions  rcq: restCampaignDTO.getQuestions()) {
				if(rcq.getOperator()!=null && !rcq.getOperator().isEmpty()) {
					if(rcq.getOperator().equalsIgnoreCase("IN") || rcq.getOperator().equalsIgnoreCase("NOT IN")) {

					}else {
						throw new IllegalArgumentException("Ivalid Operator,   supported operators are 'IN' and 'NOT IN' ");
					}

				}else {
					throw new IllegalArgumentException("Operater in Question is Required.");
				}

				if(rcq.getQuestionSkin()==null || rcq.getQuestionSkin().isEmpty()) {
					throw new IllegalArgumentException("Question Skin in Question is Required.");
				}
			}
		}else if(restCampaignDTO.getTeamSupervisorId()!=null &&  !restCampaignDTO.getTeamSupervisorId().isEmpty() ) {
			if(!isValidSupervisorId(restCampaignDTO.getTeamSupervisorId(),restCampaignDTO.getOrganizationId())) {
				throw new IllegalArgumentException("Team SupervisorId not exists, please verify.");
			}
		}



		if(restCampaignDTO.getTeamSupervisorId()==null ||  restCampaignDTO.getTeamSupervisorId().isEmpty() )
			throw new IllegalArgumentException("Team SupervisorId is Required.");

	}

	private boolean isValidSupervisorId(String supervisorId, String organizationId) {
		if(organizationId!=null && !organizationId.isEmpty()) {
			List<SupervisorDTO>  logins = userService.getsupervisorList(organizationId);
			for(SupervisorDTO login : logins) {
				if(supervisorId.equals(login.getId())){
					return  true;
				}
			}
		}
		return false;
	}

	private String encodeURL(String url) {
		String encodedURL = url;
		String urlArray[] = url.split("/");
		String fileName = urlArray[urlArray.length - 1];
		try {
			String decodedFileName = URLDecoder.decode(fileName, XtaasConstants.UTF8); // decoding it first, in case it
																						// is already encoded.
			String encodedFileName = URLEncoder.encode(decodedFileName, XtaasConstants.UTF8);
			encodedURL = url.replace(urlArray[urlArray.length - 1], encodedFileName);
		} catch (UnsupportedEncodingException e) {
			logger.error("Error in encoding url : " + url, e);
		}
		return encodedURL;
	}

	@Override
	public RestCampaignDTO updateCampaign(RestCampaignDTO campaignDTO) {
		Campaign cdpCampaign = campaignRepository.findCdpCampaign(campaignDTO.getSfdcCampaignId());
		if (cdpCampaign == null) {
			throw new IllegalArgumentException("Campaign does not exists in the system.");
		}
		// cdpCampaign.setName(campaignDTO.getName());
		if (campaignDTO.getAssets() != null) {
			List<Asset> assets = new ArrayList<Asset>();
			for (String assetUrl : campaignDTO.getAssets()) {
				Asset asset = new Asset("", "assetName", "description", encodeURL(assetUrl), false);
				assets.add(asset);
			}
			cdpCampaign.setAssets(assets);
		}
		cdpCampaign.setDailyCap(campaignDTO.getDailyCap());
		cdpCampaign.setRunDates(campaignDTO.getStartDate(), campaignDTO.getEndDate());
		cdpCampaign.setDeliveryTarget(campaignDTO.getDeliveryTarget());
		cdpCampaign.setCampaignRequirements(campaignDTO.getCampaignRequirements());
		cdpCampaign.setDuplicateCompanyDuration(campaignDTO.getDuplicateCompanyDuration());
		cdpCampaign.setDuplicateLeadsDuration(campaignDTO.getDuplicateLeadsDuration());
		cdpCampaign.setLeadsPerCompany(campaignDTO.getLeadsPerCompany());
		if(campaignDTO.getPartnerSupervisors()!=null && campaignDTO.getPartnerSupervisors().size()>0) {
			cdpCampaign.setPartnerSupervisors(campaignDTO.getPartnerSupervisors());
		}

		//Configuring questions
		 if (campaignDTO.getQuestions() != null &&  campaignDTO.getQuestions().size()>0) {
				List<Expression> expressions = new ArrayList<Expression>();
				List<ExpressionGroup> expressionGroups = new ArrayList<ExpressionGroup>();
				for(RestClientQuestions  rcq: campaignDTO.getQuestions()) {
					List<PickListItem>  pickList  = new ArrayList<PickListItem>();
					if(rcq.getOptions()!=null  && rcq.getOptions().size()>0) {
						List<String> opList = rcq.getOptions();
						for(String op  : opList) {
							PickListItem pli  = new PickListItem();
							pli.setLabel(op);
							pli.setValue(op);

							pickList.add(pli);
						}
					}
					Boolean agentFlag  =  rcq.isAgentValidationRequired();
					Expression  ex = new Expression(rcq.getQuestionSkin().replaceAll("[^a-zA-Z0-9]", ""), rcq.getOperator(),
							rcq.getSelectedOptions(), agentFlag, rcq.getQuestionSkin(), (ArrayList<PickListItem>) pickList, "");
					expressions.add(ex);
				}

				QualificationCriteria qfc  = new QualificationCriteria("AND", expressions, expressionGroups);
				cdpCampaign.setQualificationCriteria(qfc);
		 }
		if (campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.PLANNING.name())) {
			if (cdpCampaign.getStatus().equals(CampaignStatus.CREATED)
					&& campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.PLANNING.name())) {
				cdpCampaign.setStatus(CampaignStatus.PLANNING);
			} else {
				throw new IllegalArgumentException("Campaign status can be set as PLANNING only.");
			}
		}

		if (campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.RUNNING.name())) {
			if (cdpCampaign.getStatus().equals(CampaignStatus.PLANNING)
					&& campaignDTO.getStatus().equalsIgnoreCase("RUNNING")) {
				cdpCampaign.setStatus(CampaignStatus.RUNNING);
			} else {
				throw new IllegalArgumentException("Campaign status can be set as RUNNING only.");
			}
		}

		if (campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.PAUSED.name())
				|| campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.COMPLETED.name())) {
			if (cdpCampaign.getStatus().equals(CampaignStatus.RUNNING)
					&& campaignDTO.getStatus().equalsIgnoreCase("PAUSED")) {
				cdpCampaign.setStatus(CampaignStatus.PAUSED);
			} else if (cdpCampaign.getStatus().equals(CampaignStatus.RUNNING)
					&& campaignDTO.getStatus().equalsIgnoreCase("COMPLETED")) {
				cdpCampaign.setStatus(CampaignStatus.COMPLETED);
			} else {
				throw new IllegalArgumentException("Campaign status can be set as PAUSED/COMPLETED only.");
			}
		}

		if (campaignDTO.getStatus().equalsIgnoreCase(CampaignStatus.RUNNING.name())) {
			if (cdpCampaign.getStatus().equals(CampaignStatus.PAUSED)
					&& campaignDTO.getStatus().equalsIgnoreCase("RUNNING")) {
				cdpCampaign.setStatus(CampaignStatus.RUNNING);
			} else {
				throw new IllegalArgumentException("Paused campaigns can be set as RUNNING only.");
			}
		}
		cdpCampaign = campaignRepository.save(cdpCampaign);
		logger.debug(
				"Successfully updated campaign having salesforce campaignId [{}]" + campaignDTO.getSfdcCampaignId());
		RestCampaignDTO rsqDTO =  new RestCampaignDTO(new CampaignDTO(cdpCampaign));
		rsqDTO.setQuestions(campaignDTO.getQuestions());
		rsqDTO.setTeamSupervisorId(campaignDTO.getTeamSupervisorId());
		rsqDTO.setPartnerSupervisors(campaignDTO.getPartnerSupervisors());

		return  rsqDTO;
	}

	@Override
	public List<String> getCampaignManagers() {
		List<Login> userlist = new ArrayList<>();
		List<String> campaignManagers = new ArrayList<>();
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		if (userLogin != null) {
			String organization = userLogin.getOrganization();
			if (organization != null && !organization.isEmpty()) {
				userlist = loginRepository.findCampaignManagersByOrganization(organization);
				if (userlist != null && userlist.size() > 0) {
					for (Login login : userlist) {
						campaignManagers.add(login.getId());
					}
				}
			}
		}
		return campaignManagers;
	}
}