package com.xtaas.application.service;

import java.util.List;

import com.xtaas.db.repository.AgentConferenceRepository;
import com.xtaas.domain.entity.AgentConference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgentConferenceServiceImpl implements AgentConferenceService {

  private static final Logger logger = LoggerFactory.getLogger(AgentConferenceServiceImpl.class);

  @Autowired
  private AgentConferenceRepository agentConferenceRepository;

  @Override
  public String findAgentIdByConferenceId(String conferenceId) {
    String agentId = agentConferenceRepository.findAgentIdByConferenceId(conferenceId);
    return agentId;
  }

  @Override
  public AgentConference findConferenceIdByAgentId(String agentId) {
    AgentConference agentConference = agentConferenceRepository.findConferenceIdByAgentId(agentId);
    return agentConference;
  }

  @Override
  public void saveAgentConference(AgentConference agentConference) {
    List<AgentConference> agentConferences = agentConferenceRepository.findByAgentId(agentConference.getAgentId());
    if (agentConferences != null && agentConferences.size() > 0) {
      AgentConference agentConferenceObj = agentConferences.get(0);
      agentConferenceObj.setConferenceId(agentConference.getConferenceId());
      agentConferenceObj.setCallControlId(agentConference.getCallControlId());
      agentConferenceObj.setCallLegId(agentConference.getCallLegId());
      agentConferenceRepository.save(agentConferenceObj);
    } else {
      agentConferenceRepository.save(agentConference);
    }
  }
  
  @Override
  public AgentConference findByConferenceId(String conferenceId) {
    List<AgentConference> agentConferenceList = agentConferenceRepository.findByConferenceId(conferenceId);
    if (agentConferenceList != null && agentConferenceList.size() > 0) {
      AgentConference agentConferenceObj = agentConferenceList.get(0);
      return agentConferenceObj;
    }
    return null;
  }

}