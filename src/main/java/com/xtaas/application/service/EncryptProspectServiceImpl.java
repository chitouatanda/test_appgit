package com.xtaas.application.service;

import java.util.Base64;
import java.util.List;

import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLogEnc;
import com.xtaas.db.repository.EncryptProspectRepository;
import com.xtaas.db.repository.GlobalContactRepository;
import com.xtaas.db.repository.ProspectCallLogEncRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.PcLogNew;
import com.xtaas.domain.entity.ProspectName;

@Service
public class EncryptProspectServiceImpl implements EncryptProspectService {

	@Autowired
	EncryptProspectRepository encryptProspectRepository;
	
	@Autowired
	ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	ProspectCallLogEncRepository prospectCallLogEncRepository;
	
	@Autowired
	GlobalContactRepository globalContactRepository;

	@Override
	public List<ProspectCallLogEnc> getAllProspects(String campaignId) {
		 Pageable pageable = PageRequest.of(0, 50, Direction.DESC, "updatedDate");
		  List<ProspectCallLogEnc> dataFromDB = prospectCallLogEncRepository.findByCampaignIdPageable(campaignId,
					  pageable);
		  return dataFromDB;
	}

	// Creates Normal Client
	private static MongoClient createMongoClient(String connectionString) {
		return MongoClients.create(connectionString);
	}

	// Returns existing data encryption key
	public static String findDataEncryptionKey() {
		try (MongoClient mongoClient = createMongoClient(
				"mongodb+srv://qa_db_user:qR123QA1@qa-cluster-new.8ekee.mongodb.net/qaDBCluster?ssl=true;replicaSet=atlas-scbgm3-shard-0;authSource=admin")) {
			Document query = new Document("keyAltNames", "xtaas-key");
			MongoCollection<Document> collection = mongoClient.getDatabase("xtaas_encryption")
					.getCollection("__keyVaultCSFL");
			BsonDocument doc = collection.withDocumentClass(BsonDocument.class).find(query).first();

			if (doc != null) {
				return Base64.getEncoder().encodeToString(doc.getBinary("_id").getData());
			}
			return null;
		}
	}

	@Override
	public void createProspect(PcLogNew encryptProspect) {
			ProspectCallLog prospectCallog = new ProspectCallLog();
			prospectCallog.setProspectCall(new ProspectCall("qwerf234567sdfgh", "5e9e7c6ee64452f72e7902d8", new Prospect()));
			prospectCallog.getProspectCall().getProspect().setFirstName(encryptProspect.getFirstName());
			prospectCallog.getProspectCall().getProspect().setLastName(encryptProspect.getLastName());
			prospectCallog.getProspectCall().getProspect().setEmail(encryptProspect.getEmail());
			prospectCallog.getProspectCall().getProspect().setPhone(encryptProspect.getPhone());
			prospectCallLogRepository.save(prospectCallog);
			
			GlobalContact globalContact = new GlobalContact();
			globalContact.setFirstName(encryptProspect.getFirstName());
			globalContact.setLastName(encryptProspect.getLastName());
			globalContact.setEmail(encryptProspect.getEmail());
			globalContact.setPhone(encryptProspect.getPhone());
			globalContactRepository.save(globalContact);
			
//			PcLogNew prospect = new PcLogNew(encryptProspect.getFirstName(), encryptProspect.getLastName(),
//					encryptProspect.getEmail(), encryptProspect.getPhone(), encryptProspect.getCompany(),
//					encryptProspect.getComment());
//			return encryptProspectRepository.save(prospect);
		}
}
