package com.xtaas.application.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.Sort;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.monitorjbl.xlsx.StreamingReader;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.StateCallConfig;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ContactRepository;
import com.xtaas.db.repository.GeographyRepository;
import com.xtaas.db.repository.ListProviderUsageRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.OutboundNumberRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
//import com.xtaas.db.repository.SuppressionListRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.ListProviderUsage;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.CampaignContactSource;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.ContactPurchaseError;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.Location;
import com.xtaas.infra.zoominfo.service.DataSlice;
import com.xtaas.service.GeographyService;
import com.xtaas.service.GlobalContactService;
import com.xtaas.service.PropertyService;
import com.xtaas.service.SuppressionListService;
import com.xtaas.service.UserService;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.CampaignContactDTO;
import com.xtaas.web.dto.CampaignDTO;

@Service
public class CampaignContactServiceImpl implements CampaignContactService {

	private static final Logger logger = LoggerFactory.getLogger(CampaignContactServiceImpl.class);

	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private CampaignContactRepository campaignContactRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private OutboundNumberRepository outboundNumberRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private GeographyRepository geographyRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private ListProviderUsageRepository listProviderUsageRepository;

	@Autowired
	private GeographyService geographyService;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private PCIAnalysisRepository pCIAnalysisRepository;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private GlobalContactService globalContactService;
	
	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private DataSlice dataSlice;

	@Autowired
	private UserService userService;

	
	@Autowired
	private OrganizationRepository organizationRepository;

	static Properties properties = new Properties();

	private static final String NON_CALLABLE = "_NON_CALLABLE";
	
	private SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();

	
	//private static final String PIVOT_REMOVED = "_PIVOT_REMOVED";


	private ThreadLocal<HashMap<Integer, String>> headerIndex = new ThreadLocal<>();

	private ThreadLocal<Workbook> book = new ThreadLocal<>();
	
	private ThreadLocal<Workbook> bookCopy = new ThreadLocal<>();

	private ThreadLocal<HashMap<Integer, List<String>>> errorMap = new ThreadLocal<>();

	public ThreadLocal<Boolean> valid  = new ThreadLocal<>();

	private ThreadLocal<Boolean> invalidFile = ThreadLocal.withInitial(() -> false);

	public static final int BATCH_SIZE = 100;
	
	private ThreadLocal<String> xlsFlieName=new ThreadLocal<>();;
	
	//private ThreadLocal<File> myFile=new ThreadLocal<>();;

	@Override
	public String createCampaignContact(String campaignId, MultipartFile file, HttpSession httpSession) {
		if (!file.isEmpty()) {
			xlsFlieName.set(file.getOriginalFilename());
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
					// add("application/octet-stream"); // HACK: added to upload files.
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				CamapignContactFileUploader camapignContactFileUploader = new CamapignContactFileUploader();
				camapignContactFileUploader.setCampaignId(campaignId);
				camapignContactFileUploader.setFileName(file.getOriginalFilename());
				FileUploadTracker fileUploadTracker = new FileUploadTracker();
				httpSession.setAttribute("fileUploadStatus", fileUploadTracker); // Put FileUploadStatus object in
																					// session
				camapignContactFileUploader.setFileUploadTracker(fileUploadTracker);
				try {
					//camapignContactFileUploader.setFileInputStream(file.getInputStream());
					camapignContactFileUploader.setMyFile(convertMultiPartToFile(file));
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
				delegateExecutor.setConcurrencyLimit(2);
				//SimpleAsyncTaskExecutor delegateExecutor = new SimpleAsyncTaskExecutor();
				DelegatingSecurityContextExecutor contactFileUploadThreadExecutor = new DelegatingSecurityContextExecutor(
						delegateExecutor, SecurityContextHolder.getContext());
				contactFileUploadThreadExecutor.execute(camapignContactFileUploader);
				logger.info("purchaseContacts(): CONTACT-FILE UPLOADING-THREAD is started...for campaignID : [{}]"+campaignId);

			} else {
				logger.error("uploadFile() : Error: UNSUPPORTED FILE TYPE " + file);
				return "{\"code\": \"ERROR\", \"msg\" : \"UNSUPPORTED FILE TYPE : Only .xls, .xlsx Filetypes are supported.\", \"file\" : \""
						+ file.getOriginalFilename() + "\"}";
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		} else {
			return "{\"code\": \"ERROR\", \"msg\" : \"EMPTY FILE\", \"file\" : \"" + file.getOriginalFilename() + "\"}";
		}
	}

	private File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File( file.getOriginalFilename() );
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }
	@JsonIgnoreProperties("errorList")
	public class FileUploadTracker {

		private int totalRecordsCount;
		private int successRecordCount;
		private int failRecordsCount;
		private List<String> errorList;
		private String invalidFileError;

		public int getTotalRecordsCount() {
			return totalRecordsCount;
		}

		public void setTotalRecordsCount(int totalRecordsCount) {
			this.totalRecordsCount = totalRecordsCount;
		}

		public int getSuccessRecordCount() {
			return successRecordCount;
		}

		public void setSuccessRecordCount(int successRecordCount) {
			this.successRecordCount = successRecordCount;
		}

		public int getFailRecordsCount() {
			return failRecordsCount;
		}

		public void setFailRecordsCount(int failRecordsCount) {
			this.failRecordsCount = failRecordsCount;
		}

		public List<String> getErrorList() {
			return errorList;
		}

		public void setErrorList(List<String> errorList) {
			this.errorList = errorList;
		}

		public String getInvalidFileError() {
			return invalidFileError;
		}

		public void setInvalidFileError(String invalidFileError) {
			this.invalidFileError = invalidFileError;
		}

	}

	class CamapignContactFileUploader implements Runnable {

		private final Logger logger = LoggerFactory.getLogger(CamapignContactFileUploader.class);

		private String campaignId;
		
		private File myFile;

		private InputStream fileInputStream;

		private String fileName;

		private FileUploadTracker fileUploadTracker;// object for tracking the file upload status

		private Sheet sheet;// this is the excel sheet to be read

		private List<String> mandatoryFields;// contain mandatory fields of campaign conatct file

		private Set<String> presentEmails;// contain Emails of prospects presents into database for given campaign id

		private Set<String> presentOrganizations;// contain Organizations of prospects presents into database for given
													// campaign id

		private Set<String> presentPhones;// contain Phones of prospects presents into database for given campaign id

		private EmailValidator emailValidator = EmailValidator.getInstance();

		private List<String> errorList; /* = new ArrayList<String>(); */

		private Set<String> fileEmails = new HashSet<String>();

		private Set<String> fileOrganizations = new HashSet<String>();

		private Set<String> filePhones = new HashSet<String>();

		private int successCount;

		private int failCount;

		private static final int MAX_PROSPECT_VERSION = 3;

		List<String> runningCampaignIds = new ArrayList<String>();

		public FileUploadTracker getFileUploadTracker() {
			return fileUploadTracker;
		}

		public void setFileUploadTracker(FileUploadTracker fileUploadTracker) {
			this.fileUploadTracker = fileUploadTracker;
		}
		
		public File getMyFile() {
			return myFile;
		}

		public void setMyFile(File myFile) {
			this.myFile = myFile;
		}

		/**
		 * @return the campaignId
		 */
		public String getCampaignId() {
			return campaignId;
		}

		/**
		 * @param campaignId the campaignId to set
		 */
		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}

		/**
		 * @return the fileInputStream
		 */
		public InputStream getFileInputStream() {
			return fileInputStream;
		}

		/**
		 * @param fileInputStream the fileInputStream to set
		 */
		public void setFileInputStream(InputStream fileInputStream) {
			this.fileInputStream = fileInputStream;
		}

		/**
		 * @return the fileName
		 */
		public String getFileName() {
			return fileName;
		}

		/**
		 * @param fileName the fileName to set
		 */
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public List<String> getErrorList() {
			return errorList;
		}

		public void setErrorList(List<String> errorList) {
			this.errorList = errorList;
		}

		@Override
		public void run() {
			Set<CampaignContact> campaignContactList = null;
			// XSSFWorkbook book;
			Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();

			try {
//				FileReader reader = new FileReader("src/main/resources/properties/timezonareacodemapping.properties");
//				properties.load(reader);
				headerIndex.set(new HashMap<Integer, String>());
				errorMap.set(new HashMap<Integer, List<String>>());
				book.set(StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(myFile)); 
				bookCopy.set( WorkbookFactory.create(myFile));
				sheet = book.get().getSheetAt(0);
				int totalRowsCount = sheet.getLastRowNum() + 1;
				List<String> reportingEmail = null;
				//Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
				//if (organization != null && organization.getReportingEmail() != null && organization.getReportingEmail().size() > 0) {
					reportingEmail = loginUser.getReportEmails();
				//}
				Campaign campaign = campaignService.getCampaign(campaignId);

				if (totalRowsCount < 1) {// check if nothing found in file
					book.get().close();
					logger.error(fileName + " file is empty");
					//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
					String subject = "DataUpload Upload Status Failed For " + campaign.getName();
					//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, fileName + " File is Empty");
					//fileUploadTracker.setInvalidFileError(fileName + " file is empty");
					if(reportingEmail != null && reportingEmail.size() > 0) {
						for(String email : reportingEmail) {
							XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",subject, fileName + " File is Empty");	
						}
					}else {
						XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, fileName + " File is Empty");
					}
					return;
				}
				mandatoryFields = new ArrayList<String>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add("First Name");
						add("Last Name");
						/* add("Email"); */
						add("Phone");
						add("State Code");
						add("Organization Name");
						add("Country");
						add("Department");
						add("Direct Phone");
						add("ManagementLevel");
						add("Maximum Employee Count");
						add("Minimum Employee Count");
					}
				};
				if (!validateExcelFileheader())
					return;
				getActiveCampaigns();
				validateExcelFileRecords(campaign,loginUser,reportingEmail);
//				book.close();
			} catch (IOException e) {
				logger.error("Problem in reading the file " + fileName);
				fileUploadTracker.setInvalidFileError("Problem in reading the file " + fileName);
				return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (campaignContactList == null || campaignContactList.isEmpty()) {
				return;
			}
		}

		private boolean validateExcelFileheader() {
			Set<String> fileColumns = new HashSet<String>();
			//final Row row = sheet.getRow(0);// get first sheet in file
			Iterator<Row> rowIterator = sheet.rowIterator();
			if (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				int colCounts = row.getPhysicalNumberOfCells();
				for (int j = 0; j < colCounts; j++) {
					Cell cell = row.getCell(j);
					if (cell == null || cell.getCellType() == CellType.BLANK) {
						logger.error(
								"Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
						fileUploadTracker.setInvalidFileError(
								"Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
						return false;
					}
					if (cell.getCellType() != CellType.STRING) {
						logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
								+ " Header must contain only String values");
						fileUploadTracker.setInvalidFileError("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
								+ " Header must contain only String values");
						return false;
					} else {
						headerIndex.get().put(j, cell.getStringCellValue().replace("*", ""));
						fileColumns.add(cell.getStringCellValue().replace("*", "").trim()); // DATE:07/11/2017 REASON:Added
																							// trim() to trim leading and
																							// trailing spaces.
					}
				}
			}
			final List<String> absentColumnsNames = isMandatoryColumnsAvailable(fileColumns, mandatoryFields);
			if (!absentColumnsNames.isEmpty()) {
				logger.error(StringUtils.join(absentColumnsNames, ", ") + " must exist in header of file " + fileName);
				fileUploadTracker.setInvalidFileError(
						StringUtils.join(absentColumnsNames, ", ") + " must exist in header of file " + fileName);
				return false;
			}
			return true;
		}

		private HashMap<String, Set<String>> getPresentEmailsOrganizationsPhones() {
			HashMap<String, Set<String>> emailOrganizationPhoneMap = new HashMap<String, Set<String>>();
			Set<String> presentEmails = new HashSet<String>();
			Set<String> presentOrganizations = new HashSet<String>();
			Set<String> presentPhones = new HashSet<String>();
			presentOrganizations = getExistingOrganizationNames(campaignId);
			List<CampaignContact> availableCampaignContactList = campaignContactRepository.findByCampaignId(campaignId);
			// TODO:- get all emails list from database get all email list present in
			// database related to specific campaign
			for (CampaignContact campaignContactObj : availableCampaignContactList) {
				presentEmails.add(campaignContactObj.getEmail());
				// presentOrganizations.add(campaignContactObj.getOrganizationName().toUpperCase());
				presentPhones.add(campaignContactObj.getPhone());
			}
			emailOrganizationPhoneMap.put("Emails", presentEmails);
			emailOrganizationPhoneMap.put("Organizations", presentOrganizations);
			emailOrganizationPhoneMap.put("Phones", presentPhones);
			return emailOrganizationPhoneMap;
		}

		private void validateExcelFileRecords(Campaign campaign,Login loginUser,List<String> reportingEmail) {
	//		int rowsCount = sheet.getPhysicalNumberOfRows();
	//		System.out.println("method row count"+ rowsCount);
			//Campaign campaign = campaignService.getCampaign(campaignId);
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			int rowsCount = sheet.getLastRowNum() + 1;
			if (rowsCount < 2) {
				logger.error("No records found in file " + fileName);
				//errorList.add("No records found in file " + fileName);
				String subject = "DataUpload Upload Status Failed For " + campaign.getName();
				XtaasEmailUtils.sendEmail(Arrays.asList(loginUser.getEmail(),"data@xtaascorp.com"), "data@xtaascorp.com",subject, "No Records Found in File " + fileName);
				return;
			}
			/*
			 * boolean isEmailAndOrganizationRetrived = false; if
			 * (!isEmailAndOrganizationRetrived) { HashMap<String, Set<String>>
			 * emailOrganizationPhoneMap = getPresentEmailsOrganizationsPhones();
			 * presentEmails = emailOrganizationPhoneMap.get("Emails"); presentOrganizations
			 * = emailOrganizationPhoneMap.get("Organizations"); presentPhones =
			 * emailOrganizationPhoneMap.get("Phones"); isEmailAndOrganizationRetrived =
			 * true; }
			 */

			int recordCount = rowsCount - 1;// decrement record count by -1 for setting correct total records
			fileUploadTracker.setTotalRecordsCount(recordCount);
			//final Row tempRow = sheet.getRow(0);// getting first row for counting number of header columns
			//int colCounts = tempRow.getPhysicalNumberOfCells();
			List<Location> dbCountries = geographyService.getCountries();
			// Do batch processing of campaign contact file
			//makeBatchProcessingCall(rowsCount, 0, colCounts, dbCountries);
			try {
				processRecords(campaign,0,0,0,dbCountries);
			} catch (Exception e) {
				e.printStackTrace();
				if(reportingEmail != null && reportingEmail.size() > 0) {
					reportingEmail.add("data@xtaascorp.com");
					XtaasEmailUtils.sendEmail(reportingEmail, "data@xtaascorp.com","File Upload Status for "+campaign.getName(), ExceptionUtils.getStackTrace(e));
				}else {
					XtaasEmailUtils.sendEmail(Arrays.asList(loginUser.getEmail(),"data@xtaascorp.com"), "data@xtaascorp.com","File Upload Status for "+campaign.getName(), ExceptionUtils.getStackTrace(e));
				}
			}
			logger.info("==========> data save for campaignId  [{}] <========",campaign.getId());

			if(invalidFile.get())
			{
				ErrorHandlindutils.excelFileChecking(bookCopy.get(), errorMap.get());
				try {
					
					ErrorHandlindutils.sendExcelwithMsg(loginUser,bookCopy.get(),fileName,"Contacts",campaign.getName(),reportingEmail);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				invalidFile.set(false);
			}
			else {
				
				String subject = "Contacts Upload Status Success For " + campaign.getName();
				//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
				if(reportingEmail != null && reportingEmail.size() > 0) {
					//for(String email : reportingEmail) {
					reportingEmail.add("data@xtaascorp.com");
						XtaasEmailUtils.sendEmail(reportingEmail, "data@xtaascorp.com",subject, "File Uploaded Successfully");	
					//}
				}else {
					XtaasEmailUtils.sendEmail(Arrays.asList(loginUser.getEmail(),"data@xtaascorp.com"), "data@xtaascorp.com",subject, "File Uploaded Successfully");
				}
				
			}
			logger.info("==========> data cleanup for campaignId  [{}]  started <========",campaign.getId());

			// SET qualityBucket and set directphone
			//if (!invalidFile) {
				setMultiProspect(campaignId);
				setQualityBucket(campaignId, campaign);
			//}
				logger.info("==========> data cleanup for campaignId  [{}]  ended	 <========",campaign.getId());

		
		}

		public int makeBatchProcessingCall(Campaign  campaign, int remainingRowsCount, int posNo, int colCounts,
				List<Location> dbCountries) {
			if (remainingRowsCount <= BATCH_SIZE) {
				processRecords(campaign,posNo, posNo + remainingRowsCount, colCounts, dbCountries);
				return 1;
			} else {
				processRecords(campaign,posNo, posNo + BATCH_SIZE, colCounts, dbCountries);
				return makeBatchProcessingCall(campaign,remainingRowsCount - BATCH_SIZE, posNo + BATCH_SIZE, colCounts,
						dbCountries);
			}
		}

		public void processRecords(Campaign campaign, int startPos, int EndPos, int colCounts, List<Location> dbCountries) {
			//						Campaign campaign = campaignService.getCampaign(campaignId);

			Set<CampaignContact> campaignContactList = new HashSet<CampaignContact>();
			List<UploadedContact> uploadedContactList = new ArrayList<UploadedContact>();
			CampaignContact tempCampaignContact = null;
			/*
			 * DATE :- 16/12/19 Added sourceType flag in prospect to identify who's data it
			 * is. whether client's data or XTAAS(internal) data.
			 */
			String sourceType = null;
			String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
			if (loggedInUserName != null) {
				Login loggedInUser = userService.getUser(loggedInUserName);
				if (loggedInUser != null) {
					if (loggedInUser.getOrganization().equalsIgnoreCase("XTAAS CALL CENTER")) {
						sourceType = "INTERNAL";
					} else {
						sourceType = "CLIENT";
					}
				}
			}
			/* END */
			int rowCount = 0;
			for (Row row : sheet) {
				try {
					//for (int i = startPos; i < EndPos; i++) {
					errorList = new ArrayList<String>();
					logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + rowCount);
					//Row row = sheet.getRow(i);
					if (rowCount == 0) {
						errorMap.get().put(0, null);
						rowCount++;
						//continue;
					}
					if (row == null) {
						errorList.add(" Record is missing ");
						errorMap.get().put(rowCount, errorList);
						invalidFile.set(true);
						EndPos++;
						continue;

					}
					boolean isRecordValid = true;
					UploadedContact uploadedContact = new UploadedContact();

					boolean emptyCell = true;
					int maxColIx = row.getLastCellNum();
					for (int j = 0; j < maxColIx; j++) {
						Cell cell = row.getCell(j);
						if (cell != null && cell.getCellType() != CellType.BLANK) {
							emptyCell = false;
							break;
						}
					}
					if (emptyCell) {
						fileUploadTracker.setFailRecordsCount(++failCount);
						errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
						fileUploadTracker.setErrorList(errorList);
						errorMap.get().put(rowCount, errorList);
						invalidFile.set(true); 
						EndPos++;
						continue;
					}
					List<ContactIndustry> contactIndustries = new ArrayList<ContactIndustry>();
					Map<String, String> customFieldMap = new HashMap<String, String>();
					Map<String, AgentQaAnswer> defaultCustomfieldMap = new HashMap<>();
					for (int j = 0; j < maxColIx; j++) {
						String columnName = headerIndex.get().get(j).replace("*", "").trim();
						String customField = null;
						if (columnName != null && columnName.length() > 3) {
							if(columnName.contains("CF01_") || columnName.contains("CF02_")
							|| columnName.contains("CF03_") || columnName.contains("CF04_")
							|| columnName.contains("CF05_") || columnName.contains("CF06_")
							|| columnName.contains("CF07_") || columnName.contains("CF08_")
							|| columnName.contains("CF09_")
							|| columnName.contains("CF10_")){
								customField = columnName.substring(0, 5);

							}
						}
						// System.out.println(customField);
						// DATE:07/11/2017 REASON : Added trim() to trim leading and trailing spaces.
						Cell cell = row.getCell(j);
						if (mandatoryFields.contains(columnName)
								&& (cell == null || cell.getCellType() == CellType.BLANK)
								&& !columnName.equalsIgnoreCase("Direct Phone")) {
							logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1) + ":- "
									+ columnName + " must be present.");
							errorList.add("Row " + (row.getRowNum() + 1) + ":-" + columnName + " should not be empty. ");
							isRecordValid = false;
							continue;
						}
						if (!mandatoryFields.contains(columnName)
								&& (cell == null || cell.getCellType() == CellType.BLANK)) {
							if (columnName.equalsIgnoreCase("Maximum Revenue")) {
								// if min revenue is present and max revenue is empty then max revenue set -1
								if (uploadedContact.getMinRevenue() != 0.0) {
									uploadedContact.setMaxRevenue(-1);
								}
							}
							if (columnName.equalsIgnoreCase("Maximum Employee Count")) {
								// if min Employee Count is present and max Employee Count is empty then max
								// Employee Count set -1
								if (uploadedContact.getMinEmployeeCount() != 0.0) {
									uploadedContact.setMaxEmployeeCount(0);
								}
							}
							continue;
						}
						if (!columnName.equalsIgnoreCase("Postal Code") && !columnName.equalsIgnoreCase("Phone")
								&& !columnName.equalsIgnoreCase("Extension") && !columnName.equalsIgnoreCase("SourceId")
								&& !columnName.equalsIgnoreCase("OrganizationId")
								&& !columnName.equalsIgnoreCase("Direct Phone")) {
							if (customField!=null && !columnName.equalsIgnoreCase("Minimum Revenue")
									&& !columnName.equalsIgnoreCase("Maximum Revenue")
									&& !columnName.equalsIgnoreCase("Minimum Employee Count")
									&& !columnName.equalsIgnoreCase("Maximum Employee Count")
									&& !columnName.equalsIgnoreCase("Sort Order")) {
								if (customField.equalsIgnoreCase("CF01_") || customField.equalsIgnoreCase("CF02_")
										|| customField.equalsIgnoreCase("CF03_") || customField.equalsIgnoreCase("CF04_")
										|| customField.equalsIgnoreCase("CF05_") || customField.equalsIgnoreCase("CF06_")
										|| customField.equalsIgnoreCase("CF07_") || customField.equalsIgnoreCase("CF08_")
										|| customField.equalsIgnoreCase("CF09_")
										|| customField.equalsIgnoreCase("CF10_")) {

								} else if (cell.getCellType() != CellType.STRING) {
									errorList.add("Row " + (row.getRowNum() + 1) + ":- " + columnName
											+ " column must contain only String values.");
									isRecordValid = false;
									continue;
								}
							} else {
//								if (cell.getCellType() != CellType.NUMERIC) {
//									errorList.add("Row " + (row.getRowNum() + 1) + ":- " + columnName
//											+ " must contain only Number values.");
//									isRecordValid = false;
//									continue;
//								}
							}
						}
						if (columnName.equalsIgnoreCase("Prefix")) {
							String prefix = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							prefix = (prefix == null || prefix.length() < 250) ? prefix : prefix.substring(0, 250);
							uploadedContact.setPrefix(prefix);
						} else if (columnName.equalsIgnoreCase("First Name")
								|| columnName.equalsIgnoreCase("First Name*")) {
							String firstname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							firstname = (firstname == null || firstname.length() < 250) ? firstname
									: firstname.substring(0, 250);
							uploadedContact.setFirstName(firstname);
						} else if (columnName.equalsIgnoreCase("Last Name") || columnName.equalsIgnoreCase("Last Name*")) {
							String lastname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							lastname = (lastname == null || lastname.length() < 250) ? lastname
									: lastname.substring(0, 250);
							uploadedContact.setLastName(lastname);
						} else if (columnName.equalsIgnoreCase("Department")
								|| columnName.equalsIgnoreCase("Department*")) {
							String department = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							department = (department == null || department.length() < 250) ? department
									: department.substring(0, 250);
							uploadedContact.setDepartment(department);
						} else if (columnName.equalsIgnoreCase("Suffix")) {
							String suffix = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							suffix = (suffix == null || suffix.length() < 250) ? suffix : suffix.substring(0, 250);
							uploadedContact.setSuffix(suffix);
						} else if (columnName.equalsIgnoreCase("Organization Name")
								|| columnName.equalsIgnoreCase("Organization Name*")) {
							String orgname = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							if (orgname.isEmpty()) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + columnName + " must be present.");
								errorList
										.add("Row " + (row.getRowNum() + 1) + ":-" + columnName + " should not be empty. ");
								isRecordValid = false;
							} else {
								orgname = (orgname != null && orgname.length() < 250) ? orgname : orgname.substring(0, 250);
								// disabled Organizations filter file as well as DB level
								/*
								* if (presentOrganizations.contains(orgname.toUpperCase())) {
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +orgname +" organization already exist for this campaign.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+orgname
								* +"' already exist in this campaign. "); isRecordValid = false; } else {
								* if(fileOrganizations.contains(orgname.toUpperCase())) { int orgCount =
								* fileOrganizationMap.get(orgname.toUpperCase());
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +orgname +" organization "
								* +orgCount+" times already exist in this file.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+orgname +"' organization "
								* +orgCount+" times already exist in this file. "); isRecordValid = false; } }
								* fileOrganizations.add(orgname.toUpperCase());
								*/

								uploadedContact.setOrganizationName(orgname);
							}
						} else if (columnName.equalsIgnoreCase("Email") || columnName.equalsIgnoreCase("Email*")) {
							String email = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							email = (email == null || email.length() < 250) ? email : email.substring(0, 250);
							/*
							* if (!emailValidator.isValid(email)) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":-" +email +" must be a valid Email address. ");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+email
							* +"' must be a valid Email address. "); isRecordValid = false; }
							*/
							uploadedContact.setEmail(email);
							/*
							* if (!emailValidator.isValid(email)) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":-" +email +" must be a valid Email address. ");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+email
							* +"' must be a valid Email address. "); isRecordValid = false; } else { if
							* (presentEmails.contains(email)) {
							* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
							* ()+1)+":- " +email +" email already exist for this campaign.");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+email
							* +"' already exist in this campaign. "); isRecordValid = false; } else {
							* if(fileEmails.contains(email)) {
							* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
							* ()+1)+":- " +email +" email already exist in another records in a file.");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+email
							* +"' this email already exist in this file. "); isRecordValid = false; } }
							* fileEmails.add(email); }
							*/
						} else if (columnName.equalsIgnoreCase("Phone") || columnName.equalsIgnoreCase("Phone*")) {
							try {
	//							cell.setCellType(CellType.STRING);
								DataFormatter formatter = new DataFormatter();
								String phoneNumber = formatter.formatCellValue(cell);
								String phone = XtaasUtils
										.removeNonAsciiChars(phoneNumber).trim();
								phone = (phone == null || phone.length() < 250) ? phone : phone.substring(0, 250);
								/*
								* if (presentPhones.contains(phone)) {
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +phone +" phone already exist for this campaign.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+phone
								* +"' already exist in this campaign. "); isRecordValid = false; } else {
								* if(filePhones.contains(phone)) {
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +phone +" phone already exist in another records in a file.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+phone
								* +"' phone already exist in this file. "); isRecordValid = false; } }
								*/
								// filePhones.add(phone);
								if (!phone.startsWith("+")) {
									phone = "+" + phone;
								}
								uploadedContact.setPhone(phone);
							} catch (IllegalArgumentException e) {
								errorList.add(e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("CompanyPhone")
								|| columnName.equalsIgnoreCase("CompanyPhone*")) {
							try {
								String cphone = XtaasUtils
										.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "CompanyPhone")).trim();
								cphone = (cphone == null || cphone.length() < 250) ? cphone : cphone.substring(0, 250);
								/*
								* if (presentPhones.contains(phone)) {
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +phone +" phone already exist for this campaign.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+phone
								* +"' already exist in this campaign. "); isRecordValid = false; } else {
								* if(filePhones.contains(phone)) {
								* logger.info("excelFileRead():validateExcelFileRecords(): Row "+(row.getRowNum
								* ()+1)+":- " +phone +" phone already exist in another records in a file.");
								* errorList.add("Row "+(row.getRowNum()+1)+":- '"+phone
								* +"' phone already exist in this file. "); isRecordValid = false; } }
								*/
								// filePhones.add(phone);
								uploadedContact.setCompanyPhone(cphone);
							} catch (IllegalArgumentException e) {
								errorList.add(e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("State Code")
								|| columnName.equalsIgnoreCase("State Code*")) {
							String stateCode = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							stateCode = (stateCode == null || stateCode.length() < 250) ? stateCode
									: stateCode.substring(0, 250);
							StateCallConfig callConfig = stateCallConfigRepository.findByStateCode(stateCode);
							if (callConfig != null) {
								uploadedContact.setStateCode(stateCode);
							} else {
								isRecordValid = false;
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + "State Code " + stateCode
										+ " dosen't exist.");
							}
						} else if (columnName.equalsIgnoreCase("Country") || columnName.equalsIgnoreCase("Country*")) {
							String country = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							country = (country == null || country.length() < 250) ? country : country.substring(0, 250);
							uploadedContact.setCountry(country);
							/*
							* if (country == null || country.isEmpty()) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":- Country should not be empty. ");
							* errorList.add("Row "+(row.getRowNum()+1)+":- Country should not be empty. ");
							* isRecordValid = false; } else { if (dbCountries.isEmpty()) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":- " +country +" is not a valid country");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+country
							* +" is not a valid country"); isRecordValid = false; } if (country.length() <=
							* 2) {//check for country codes for (Location location : dbCountries) { if
							* (!location.getValue().equalsIgnoreCase(country)) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":- '" +country +"' is not a valid country code.");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+country
							* +"' is not a valid country code. "); isRecordValid = false; } } } else
							* {//check for country Names for (Location location : dbCountries) { if
							* (!location.getLabel().equalsIgnoreCase(country)) {
							* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
							* getRowNum()+1)+":- " +country +" is not a valid country name.");
							* errorList.add("Row "+(row.getRowNum()+1)+":- '"+country
							* +" is not a valid country name. "); isRecordValid = false; } } } }
							*/
						} else if (columnName.equalsIgnoreCase("Title")) {
							String title = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							title = (title == null || title.length() < 250) ? title : title.substring(0, 250);
							uploadedContact.setTitle(title);
						} else if (columnName.equalsIgnoreCase("Functions")) {
							String functionsList[] = row.getCell(j).getStringCellValue().split("\\|");
							List<ContactFunction> functions = new ArrayList<ContactFunction>();
							for (String function : functionsList) {
								ContactFunction contactFunction = new ContactFunction();
								contactFunction.setName(XtaasUtils.removeNonAsciiChars(function));
								functions.add(contactFunction);
							}
							uploadedContact.setFunctions(functions);
						} else if (columnName.equalsIgnoreCase("Domain")) {
							String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							domain = (domain == null || domain.length() < 250) ? domain : domain.substring(0, 250);
							uploadedContact.setDomain(domain);
						} else if (columnName.equalsIgnoreCase("Industries")) {
							String industryList[] = cell.getStringCellValue().split("\\|");
							List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
							for (String industry : industryList) {
								industry = (industry == null || industry.length() < 250) ? industry
										: industry.substring(0, 250);
								ContactIndustry contactIndustry = new ContactIndustry();
								contactIndustry.setName(XtaasUtils.removeNonAsciiChars(industry));
								industries.add(contactIndustry);
							}
							uploadedContact.setIndustries(industries);
						} else if (columnName.equalsIgnoreCase("Industry 1") || columnName.equalsIgnoreCase("Industry 2")
								|| columnName.equalsIgnoreCase("Industry 3") || columnName.equalsIgnoreCase("Industry 4")) {
							ContactIndustry contactIndustry = new ContactIndustry();
							String industry = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue());
							industry = (industry == null || industry.length() < 250) ? industry
									: industry.substring(0, 250);
							contactIndustry.setName(industry);
							contactIndustries.add(contactIndustry);
						} else if (columnName.equalsIgnoreCase("Minimum Revenue")) {
							try {
								uploadedContact.setMinRevenue(
										validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Minimum Revenue"));
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + e.getMessage());
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("Maximum Revenue")) {
							try {
								uploadedContact.setMaxRevenue(
										validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Maximum Revenue"));
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + e.getMessage());
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("Minimum Employee Count") || columnName.equalsIgnoreCase("Minimum Employee Count*")) {
							try {
								uploadedContact.setMinEmployeeCount(
										validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Minimum Employee Count"));
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + e.getMessage());
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("Maximum Employee Count") || columnName.equalsIgnoreCase("Maximum Employee Count*")) {
							try {
								uploadedContact.setMaxEmployeeCount(
										validateDoubleTypeFieldValue(cell.getNumericCellValue(), "Maximum Employee Count"));
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + e.getMessage());
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("AddressLine1")) {
							String addressLine1 = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							addressLine1 = (addressLine1 == null || addressLine1.length() < 250) ? addressLine1
									: addressLine1.substring(0, 250);
							uploadedContact.setAddressLine1(addressLine1);
						} else if (columnName.equalsIgnoreCase("AddressLine2")) {
							String addressLine2 = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							addressLine2 = (addressLine2 == null || addressLine2.length() < 250) ? addressLine2
									: addressLine2.substring(0, 250);
							uploadedContact.setAddressLine2(addressLine2);
						} else if (columnName.equalsIgnoreCase("City")) {
							String city = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
							city = (city == null || city.length() < 250) ? city : city.substring(0, 250);
							uploadedContact.setCity(city);
						} else if (columnName.equalsIgnoreCase("Source")) {
							try {
								String source = XtaasUtils.removeNonAsciiChars(
										validateSourceFieldValue(cell.getStringCellValue(), row, "Source")).trim();
								uploadedContact.setSource(source);
								uploadedContact.setDataSource(source);
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : " + e.getMessage());
								errorList.add(e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("SourceId")) {
							String sourceId = XtaasUtils
									.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "SourceId")).trim();
							if (isSourceIdAlreadyPurchased(sourceId)) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + sourceId + " PersonId is already purchased ");
								errorList.add("Row " + (row.getRowNum() + 1) + ":- '" + sourceId
										+ " PersonId is already purchased");
								isRecordValid = false;
							} /*
								* else if(isPersonRecordAlreadyPurchased(uploadedContact)){
								* logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
								* getRowNum()+1)+" Combination of First Name "+uploadedContact.getFirstName()+
								* " and Last Name "+uploadedContact.getLastName()+" and Company Name "
								* +uploadedContact.getOrganizationName()+" is already purchased");
								* errorList.add("Row "+(row.getRowNum()+1)+
								* " Combination of First Name "+uploadedContact.getFirstName()+
								* " and Last Name "+uploadedContact.getLastName()+" and Company Name "
								* +uploadedContact.getOrganizationName()+" is already purchased");
								* isRecordValid = false; }
								*/
							else {
								uploadedContact.setSourceId(sourceId);
							}
						} else if (columnName.equalsIgnoreCase("Postal Code")) {
							try {
								uploadedContact.setPostalCode(validateMultiTypeFieldValue(cell, row, "Postal Code"));
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : " + e.getMessage());
								errorList.add(e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("Extension")) {
							try {
								String ext = XtaasUtils
										.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "Extension"));
								uploadedContact.setExtension(ext);
							} catch (IllegalArgumentException e) {
								logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
										+ ":- " + e.getMessage());
								errorList.add("Row " + (row.getRowNum() + 1) + ":- " + e.getMessage());
								isRecordValid = false;
							}
						} else if (columnName.equalsIgnoreCase("OrganizationId")) {
							String sourceCompanyId = XtaasUtils
									.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "OrganizationId"));
							uploadedContact.setSourceCompanyId(sourceCompanyId);
							;
						} else if (columnName.equalsIgnoreCase("ManagementLevel")) {
							String managementLevel = XtaasUtils
									.removeNonAsciiChars(validateMultiTypeFieldValue(cell, row, "ManagementLevel")).trim();
							managementLevel = (managementLevel == null || managementLevel.length() < 250) ? managementLevel
									: managementLevel.substring(0, 250);
							uploadedContact.setManagementLevel(managementLevel);
						} else if (columnName.equalsIgnoreCase("Company Validation Link")) {
							if (cell != null) {
								String zoomCompanyUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
								uploadedContact.setZoomCompanyUrl(zoomCompanyUrl);
							}
						} else if (columnName.equalsIgnoreCase("Title Validation Link")) {
							if (cell != null) {
								String zoomPersonUrl = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
								uploadedContact.setZoomPersonUrl(zoomPersonUrl);
							}
						} else if (columnName.equalsIgnoreCase("Direct Phone")
								|| columnName.equalsIgnoreCase("Direct Phone*")) {
							if (cell != null) {
								if (cell.getCellType() == CellType.STRING) {
									String isDirectPhone = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
									if (isDirectPhone.equalsIgnoreCase("yes")) {
										uploadedContact.setDirectPhone(true);
									} else if (isDirectPhone.equalsIgnoreCase("no")) {
										uploadedContact.setDirectPhone(false);
									}
								} else {
									errorList.add("Row " + (row.getRowNum() + 1) + ":- " + columnName
											+ " column must contain only String values. Accepted values yes/no");
									isRecordValid = false;
								}
							} else {
								uploadedContact.setDirectPhone(false);
							}
						} else if (columnName.equalsIgnoreCase("isEuropean")
								|| columnName.equalsIgnoreCase("isEuropean*")) {
							if (cell != null) {
								if (cell.getCellType() == CellType.STRING) {
									String isEu = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
									if (isEu.equalsIgnoreCase("yes")) {
										uploadedContact.setEu(true);
									} else if (isEu.equalsIgnoreCase("no")) {
										uploadedContact.setEu(false);
									}
								} else {
									errorList.add("Row " + (row.getRowNum() + 1) + ":- " + columnName
											+ " column must contain only String values. Accepted values yes/no");
									isRecordValid = false;
								}
							} else {
								uploadedContact.setEu(false);
							}
						} else if (columnName.equalsIgnoreCase("Sort Order")) {
							if (cell != null) {
								if (cell.getCellType() == CellType.NUMERIC) {
									Integer in = new Integer((int) cell.getNumericCellValue());
									uploadedContact.setSortOrder(in);
								} else {
									errorList.add("Row " + (row.getRowNum() + 1) + ":- " + columnName
											+ " column must contain only Numeric values.");
									isRecordValid = false;
								}
							}
						} else if (columnName != null && customField != null
								&& (customField.equalsIgnoreCase("CF01_") || customField.equalsIgnoreCase("CF02_")
										|| customField.equalsIgnoreCase("CF03_") || customField.equalsIgnoreCase("CF04_")
										|| customField.equalsIgnoreCase("CF05_") || customField.equalsIgnoreCase("CF06_")
										|| customField.equalsIgnoreCase("CF07_") || customField.equalsIgnoreCase("CF08_")
										|| customField.equalsIgnoreCase("CF09_")
										|| customField.equalsIgnoreCase("CF10_"))) {
							if (campaign != null && campaign.isEnableCustomFields()) {
								String questionSkin = columnName.substring(5, columnName.length());
								String customFieldAns = "";
								if (cell != null && cell.getCellType() != null && cell.getCellType() == CellType.STRING) {
									customFieldAns = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue()).trim();
								} else if (cell != null && cell.getCellType() != null
										&& cell.getCellType() == CellType.NUMERIC) {
									customFieldAns = Double.toString(cell.getNumericCellValue());
								}
								if (customFieldAns == null || (customFieldAns != null && customFieldAns.isEmpty())) {
//									customFieldAns = "def campaign value";
									if (defaultCustomfieldMap != null && defaultCustomfieldMap.size() == 0) {
										defaultCustomfieldMap = getDefaultCustomFieldAnswersMapFromCampaign(campaign);
									}
									if (defaultCustomfieldMap != null) {
										customFieldAns = getDefaultCustomFieldAnswerFromCampaignFromColumnName(customField, defaultCustomfieldMap);
									}
									if (customFieldAns == null || (customFieldAns != null && customFieldAns.isEmpty())) {
										customFieldAns = "-";
									}
								}
								customFieldMap.put(questionSkin, customFieldAns);
							}
						}
					}
					uploadedContact.setIndustries(contactIndustries);

					List<AgentQaAnswer> answerList = new ArrayList<AgentQaAnswer>();
					
					/// Empty values are not getting saved so below logic to handle that
					if (campaign.getQualificationCriteria().getExpressions() != null
							&& !campaign.getQualificationCriteria().getExpressions().isEmpty()) {
						for (Expression expression : campaign.getQualificationCriteria().getExpressions()) {
							if(expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
									|| expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10")) {
								if (!customFieldMap.containsKey(expression.getQuestionSkin())) {
									String customFieldAns = "";
									if (customFieldAns == null
											|| (customFieldAns != null && customFieldAns.isEmpty())) {
										// customFieldAns = "def campaign value";
										if (defaultCustomfieldMap != null && defaultCustomfieldMap.size() == 0) {
											defaultCustomfieldMap = getDefaultCustomFieldAnswersMapFromCampaign(
													campaign);
										}
										if (defaultCustomfieldMap != null) {
											customFieldAns = getDefaultCustomFieldAnswerFromCampaignFromAttributeName(expression.getAttribute(), defaultCustomfieldMap);
										}
										if (customFieldAns == null
												|| (customFieldAns != null && customFieldAns.isEmpty())) {
											customFieldAns = "-";
										}
									}
									customFieldMap.put(expression.getQuestionSkin(), customFieldAns);
								}
							}


							
						}
					}
					///
					
					if (customFieldMap != null && customFieldMap.size() > 0) {
						for (Map.Entry<String, String> entry : customFieldMap.entrySet()) {
							if (campaign.getQualificationCriteria().getExpressions() != null
									&& !campaign.getQualificationCriteria().getExpressions().isEmpty()) {
								for (Expression expression : campaign.getQualificationCriteria().getExpressions()) {
									if (expression.getQuestionSkin().equalsIgnoreCase(entry.getKey())) {
										answerList.add(new AgentQaAnswer(entry.getKey(), entry.getValue(),
												expression.getAgentValidationRequired()));
									}
								}
							} else {
								answerList.add(new AgentQaAnswer(entry.getKey(), entry.getValue(), false));
							}
						}
					}
					uploadedContact.setAnswers(answerList);

					if (!isPhoneValid(uploadedContact.getPhone(), uploadedContact.getCountry())) {
						logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
								+ ":- Phone number is Invalid.");
						errorList.add("Row " + (row.getRowNum() + 1) + ":- Phone number is Invalid.");
						isRecordValid = false;
					}

					if (uploadedContact.getMaxRevenue() > uploadedContact.getMinRevenue()) {
						KeyValuePair<Long, Long> revenueMap = getRevenueMap(uploadedContact.getMinRevenue(),
								uploadedContact.getMaxRevenue());
						uploadedContact.setMinRevenue(revenueMap.getKey());
						uploadedContact.setMaxRevenue(revenueMap.getValue());
					}

					if (uploadedContact.getMaxEmployeeCount() > uploadedContact.getMinEmployeeCount()) {
						KeyValuePair<Long, Long> employeeMap = getEmployeeMap(uploadedContact.getMinEmployeeCount(),
								uploadedContact.getMaxEmployeeCount());
						uploadedContact.setMinEmployeeCount(employeeMap.getKey());
						uploadedContact.setMaxEmployeeCount(employeeMap.getValue());
					} else {
						if (uploadedContact.getMaxEmployeeCount() == 0.0 && uploadedContact.getMinEmployeeCount() == 0.0) {
							errorList.add("Row " + (row.getRowNum() + 1)
									+ ":- Max employee count and min employee must be greater than 0.");
							isRecordValid = false;
						} else {
							errorList.add("Row " + (row.getRowNum() + 1)
									+ ":- Max employee count must be greater than min employee count.");
							isRecordValid = false;
						}
					}

					if (isPersonRecordAlreadyPurchased(uploadedContact)) {
						logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
								+ " Combination of First Name " + uploadedContact.getFirstName() + " and Last Name "
								+ uploadedContact.getLastName() + " and Company Name "
								+ uploadedContact.getOrganizationName() + " is already purchased");
						errorList.add("Row " + (row.getRowNum() + 1) + " Combination of First Name "
								+ uploadedContact.getFirstName() + " and Last Name " + uploadedContact.getLastName()
								+ " and Company Name " + uploadedContact.getOrganizationName() + " is already purchased");
						isRecordValid = false;
					}

					if (!checkProspectVersion(uploadedContact)) {
						logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1)
								+ " Combination of First Name " + uploadedContact.getFirstName() + " and Last Name "
								+ uploadedContact.getLastName() + " and Company Name "
								+ uploadedContact.getOrganizationName() + " is already used in other running campaigns");
						errorList.add("Row " + (row.getRowNum() + 1) + " Combination of First Name "
								+ uploadedContact.getFirstName() + " and Last Name " + uploadedContact.getLastName()
								+ " and Company Name " + uploadedContact.getOrganizationName()
								+ " is already used in other running campaigns");
						isRecordValid = false;
					}

					if (!campaignContactList.isEmpty()) {
						List<CampaignContact> filteredCampaignContactList = null;
						filteredCampaignContactList = campaignContactList.stream()
								.filter(cc -> cc.getFirstName().equals(uploadedContact.getFirstName())
										&& cc.getLastName().equals(uploadedContact.getLastName())
										&& cc.getOrganizationName().equals(uploadedContact.getOrganizationName())
										&& cc.getTitle().equals(uploadedContact.getTitle()))
								.collect(Collectors.toList());
						if (!filteredCampaignContactList.isEmpty()) {
							errorList.add("Row " + (row.getRowNum() + 1) + " Combination of First Name "
									+ uploadedContact.getFirstName() + " and Last Name " + uploadedContact.getLastName()
									+ " and Company Name " + uploadedContact.getOrganizationName() + " is already in file");
							isRecordValid = false;
						}
					}

					if (isRecordValid) {
						fileUploadTracker.setSuccessRecordCount(++successCount);
						//Campaign campaign = campaignService.getCampaign(campaignId);
						tempCampaignContact = new CampaignContact(campaignId, uploadedContact.getSource(),
								uploadedContact.getSourceId(), uploadedContact.getPrefix(), uploadedContact.getFirstName(),
								uploadedContact.getLastName(), uploadedContact.getSuffix(), uploadedContact.getTitle(),
								uploadedContact.getDepartment(), uploadedContact.getFunctions(),
								uploadedContact.getOrganizationName(), uploadedContact.getDomain(),
								uploadedContact.getIndustries(), null, uploadedContact.getMinRevenue(),
								uploadedContact.getMaxRevenue(), uploadedContact.getMinEmployeeCount(),
								uploadedContact.getMaxEmployeeCount(), uploadedContact.getEmail(),
								uploadedContact.getAddressLine1(), uploadedContact.getAddressLine2(),
								uploadedContact.getCity(), uploadedContact.getStateCode(), uploadedContact.getCountry(),
								uploadedContact.getPostalCode(), getFormattedPhone(uploadedContact.getPhone()),
								uploadedContact.getCompanyPhone(), uploadedContact.getSourceCompanyId(),
								uploadedContact.getManagementLevel(), uploadedContact.getExtraAttributes(),
								campaign.isEncrypted());
						tempCampaignContact.setDirectPhone(uploadedContact.isDirectPhone());
						tempCampaignContact.setEu(uploadedContact.isEu());
						tempCampaignContact.setZoomCompanyUrl(uploadedContact.getZoomCompanyUrl());
						tempCampaignContact.setZoomPersonUrl(uploadedContact.getZoomPersonUrl());
						tempCampaignContact.setExtension(uploadedContact.getExtension());
						tempCampaignContact.setSourceType(sourceType);
						tempCampaignContact.setDataSource(uploadedContact.getDataSource());
						tempCampaignContact.setSortOrder(uploadedContact.getSortOrder());
						if (uploadedContact != null && uploadedContact.getAnswers() != null
								&& uploadedContact.getAnswers().size() > 0) {
							tempCampaignContact.setAnswers(uploadedContact.getAnswers());
						}
						campaignContactList.add(tempCampaignContact);
						uploadedContactList.add(uploadedContact);
					} else {
						fileUploadTracker.setFailRecordsCount(++failCount);
						fileUploadTracker.setErrorList(errorList);
					}
					if (errorList != null && !errorList.isEmpty() && errorList.size() > 0) {
						invalidFile.set(true); 
					}
					errorMap.get().put(rowCount, errorList);
					rowCount++;
				} catch (Exception e) {
					e.printStackTrace();
					String error = ExceptionUtils.getMessage(e);
					errorList.add("Unknown Error - " + error);
					errorMap.get().put(rowCount, errorList);
					rowCount++;
				}
			}
			logger.info("==========> Campaign contacts to be saved in DB size :  [{}] <======== for campaignId :  [{}]==",campaignContactList.size(),campaign.getId());
//			List<CampaignContact> campaignContacts = campaignContactRepository.saveAll(campaignContactList);
			List<CampaignContact> prospects = new ArrayList<CampaignContact>();
			int campaignCounter = 0;
			for (CampaignContact contact : campaignContactList) {
				prospects.add(contact);
				campaignCounter++;
				if (prospects.size() == 1000) {
					campaignContactRepository.saveAll(prospects);
					campaignCounter =0;
					prospects = new ArrayList<CampaignContact>();
				}
			}
			campaignContactRepository.saveAll(prospects);
			List<CampaignContact> campaignContacts = campaignContactList.stream().collect(Collectors.toList());
			if (!campaignContacts.isEmpty()) {
				List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
				List<ProspectCallLog> prospectCallLogsBatchInsert = new ArrayList<ProspectCallLog>();
				List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();
				List<PCIAnalysis> pcianList = new ArrayList<PCIAnalysis>();
				int counter = 0;
				for (CampaignContact campaignContact : campaignContacts) {
					Prospect prospect = campaignContact.toProspectFromExcel(null);
					prospect.setSource(campaignContact.getSource());
					prospect.setDataSource(campaignContact.getDataSource());
					prospect.setSourceId(campaignContact.getSourceId());
					prospect.setSourceType(campaignContact.getSourceType());
					prospect.setCampaignContactId(campaignContact.getId());
					prospect.setZoomCompanyUrl(campaignContact.getZoomCompanyUrl());
					prospect.setZoomPersonUrl(campaignContact.getZoomPersonUrl());
					ProspectCallLog prospectCallLog = new ProspectCallLog();
					ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaignId, prospect);
					if(campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign))//TODO this needs to be changed to type email once fixed
						prospectCall = new ProspectCall(getUniqueProspect(), campaignId, prospect,true);
					// prospectCallLog.setProspectCall(new
					// ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));

					Optional<UploadedContact> contacts = uploadedContactList.stream()
							.filter(contact -> contact.getFirstName().equals(prospect.getFirstName())
									&& contact.getLastName().equals(prospect.getLastName())
									&& contact.getOrganizationName().equals(prospect.getCompany()))
							.findFirst();
					if (contacts.isPresent()) {
						prospectCall.setProspectVersion(contacts.get().getProspectVersion());
					}


						///////////////////////////////////////////////////////////////////

						// TODO 1 below line is commented as the area code and the statecode matching is
						// completely off, 408 area code is pacific
						// but the state is NY , so this needs to be verfied well

						/*
						 * String timezone = getTimeZone(prospectCall.getProspect().getPhone());
						 * 
						 * if(timezone==null) timezone =
						 * properties.getProperty(findAreaCode(prospectCall.getProspect().getPhone()));
						 */

						// if(timezone==null){
						StateCallConfig scc = stateCallConfigRepository
								.findByStateCode(prospectCall.getProspect().getStateCode());
						prospectCall.getProspect().setTimeZone(scc.getTimezone());

						/*
						 * }else{ prospectCall.getProspect().setTimeZone(timezone); }
						 */

						//////////////////////////////////////////////////////////////////
						////////////////////////////////////////////////////////
						StringBuffer sb = new StringBuffer();
						String emp = "";
						String mgmt = "";

						// TODO 2 is linked to above todo so commented below code
						// String tz = getStateWeight(prospectCall.getProspect().getTimeZone());
						String tz = XtaasUtils.getStateWeight(scc.getTimezone());

						/*
						 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
						 */
						sb.append(prospectCallLog.getStatus());
						sb.append("|");
						if (prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number) {
							Number n1 = (Number) prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount");
							sb.append(XtaasUtils.getBoundry(n1, "min"));
							sb.append("-");
							emp = XtaasUtils.getEmployeeWeight(n1);
						}

						if (prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
							Number n2 = (Number) prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount");
							sb.append(XtaasUtils.getBoundry(n2, "max"));
						}

						/*
						 * sb.append(prospectCall.getProspect().getCustomAttributeValue(
						 * "minEmployeeCount")); sb.append("-");
						 * sb.append(prospectCall.getProspect().getCustomAttributeValue(
						 * "maxEmployeeCount"));
						 */
						sb.append("|");
						sb.append(XtaasUtils.getMgmtLevel(prospectCall.getProspect().getManagementLevel()));
						mgmt = XtaasUtils.getMgmtLevelWeight(prospectCall.getProspect().getManagementLevel());
						String direct = "";
						if (prospectCall.getProspect().isDirectPhone()) {
							direct = "1";
						} else {
							direct = "2";
						}
						int qbSort = 0;
						if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
							String sortString = "";
							if (prospectCall != null && prospectCall.getSortOrder() != null) {
								sortString = prospectCall.getSortOrder() + mgmt + emp;
							} else {
								sortString = direct + tz + mgmt + emp;
							}
							qbSort = Integer.parseInt(sortString);
						}
						

						if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
							String sortString = "";
							if (prospectCall != null && prospectCall.getSortOrder() != null) {
								sortString = prospectCall.getSortOrder() + mgmt + emp;
							} else {
								sortString = direct + tz + mgmt + emp;
							}
							qbSort = Integer.parseInt(sortString);
							String slice = "";
							Long employee = new Long(emp);
							Long management = new Long(mgmt);
							if (direct.equalsIgnoreCase("1") && employee <= 8 && management <= 3) {
								slice = "slice1";
							} else if (direct.equalsIgnoreCase("1") && employee <= 10 && management <= 3) {
								slice = "slice2";
							} else if (direct.equalsIgnoreCase("1") && employee > 10 && management <= 3) {
								slice = "slice3";
							} else if (direct.equalsIgnoreCase("1") && employee <= 8 && management >= 4) {
								slice = "slice4";
							} else if (direct.equalsIgnoreCase("1") && employee <= 10 && management >= 4) {
								slice = "slice5";
							} else if (direct.equalsIgnoreCase("1") && employee > 10 && management >= 4) {
								slice = "slice6";
							}
							prospectCallLog.setDataSlice(slice);
						}else {
							prospectCallLog.setDataSlice("slice10");
						}

						///////////////////////////////////////////////////////////////////

						prospectCall.setQualityBucket(sb.toString());
						prospectCall.setQualityBucketSort(qbSort);
						prospectCall.setOrganizationId(campaign.getOrganizationId());
						if (campaignContact != null && campaignContact.getAnswers() != null
								&& campaignContact.getAnswers().size() > 0) {
							prospectCall.getProspect().setCustomFields(campaignContact.getAnswers());
						}
						prospectCall.setSortOrder(campaignContact.getSortOrder());
						prospectCallLog.setProspectCall(prospectCall);
						prospectCallLog.getProspectCall().setCallRetryCount(786);
						prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						prospectCallLogs.add(prospectCallLog);
						prospectCallLogsBatchInsert.add(prospectCallLog);
						ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
						prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
						prospectCallInteraction.setProspectCall(prospectCall);
						
						prospectCallInteractions.add(prospectCallInteraction);
						counter++;
						if (counter == 500) {
							logger.info("==========> ProspectCallLog inside 1000 counter size :  [{}] <========for campaignId :  [{}]====",prospectCallLogs.size(),campaign.getId());
							//prospectCallLogRepository.saveAll(prospectCallLogs);
							globalContactService.generateGlobalContact(prospectCallLogs);
							counter = 0;
							prospectCallLogs = new ArrayList<ProspectCallLog>();
						}
						// PCIAnalysis pcin = new PCIAnalysis();
						// Campaign campaign = campaignService.getCampaign(campaignId);
						// pcin = pcin.toPCIAnalysis(pcin,prospectCallInteraction, null, null,
						// campaign);
						// pcianList.add(pcin);
					}
				// comment , direct call the batch Insert method
				//prospectCallLogRepository.saveAll(prospectCallLogs);
				prospectCallLogRepository.bulkinsert(prospectCallLogsBatchInsert);

//				:: TODO:: DATE :- 11/12/19 :- Need to uncomment changes.
//				prospectCallInteractionRepository.save(prospectCallInteractions);
				
//				//pCIAnalysisRepository.save(pcianList);
				globalContactService.generateGlobalContact(prospectCallLogs);
				logger.info("==========> ProspectCallLog outside 1000 counter size :  [{}] <==========",prospectCallLogs.size());
				
			}
//				List<String> campaignIds = new ArrayList<String>();
//				campaignIds.add(campaignId);
//				dataSlice.getQualityBucket(campaignIds);
				

				
		}
		
		private String getUniqueProspect() {
			String uniquepcid = UUID.randomUUID().toString();
			Date d= new Date();
			//System.out.println(uniquepcid+Long.toString(d.getTime()));
			String pcidv = uniquepcid+Long.toString(d.getTime());
			return pcidv;
		}


		public boolean isPhoneValid(String phone, String country) {
			boolean valid = true;
			List<String> countries = Arrays.asList("US", "United States", "Canada");
			/**
			 * check for valid phone formats like "+123456489" "+1(850) 555-1234"
			 * "+1-850-555-1234" "+88 66 8880 1234" "3213123213"
			 */
			if (phone == null || phone.isEmpty()) {
				valid = false;
			} else if (!Pattern.compile("^[\\+\\d]?(?:[\\d-.\\s()]*)$").matcher(phone).matches()) {
				valid = false;
			} else if (country != null && countries.contains(country)) {
				if (phone.contains("+")) {
					String ph = phone.replaceAll("\\+", "");
					phone = ph;
				}
//				if (!phone.matches("^\\(?([0-9]{3})\\)?[-]?([0-9]{3})[-.\\s]?([0-9]{4})$")) {
//					valid = false;
//				}
			}
			return valid;
		}

		private KeyValuePair<Long, Long> getRevenueMap(Double minRevenue, Double maxRevenue) {
			Long zoomMinRev = new Long(0);
			Long zoomMaxRev = new Long(0);
			KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

			long minRev = minRevenue.longValue();
			long maxRev = maxRevenue.longValue();

			if (minRev >= 0 && minRev < 5000000) {
				// TODO - need to validate
				// zoomMinRev = new Long(1);
			} else if (minRev >= 5000000 && minRev < 10000000) {
				zoomMinRev = new Long(5000000);
			} else if (minRev >= 10000000 && minRev < 25000000) {
				zoomMinRev = new Long(10000000);
			} else if (minRev >= 25000000 && minRev < 50000000) {
				zoomMinRev = new Long(25000000);
			} else if (minRev >= 50000000 && minRev < 100000000) {
				zoomMinRev = new Long(50000000);
			} else if (minRev >= 100000000 && minRev < 250000000) {
				zoomMinRev = new Long(100000000);
			} else if (minRev >= 250000000 && minRev < 500000000) {
				zoomMinRev = new Long(250000000);
			} else if (minRev >= 500000000 && minRev < 1000000000) {
				zoomMinRev = new Long(500000000);
			} else if (minRev >= 1000000000 && minRev < new Long("5000000000").longValue()) {
				zoomMinRev = new Long(1000000000);
			} else if (minRev >= new Long("5000000000").longValue()) {
				zoomMinRev = new Long("5000000000").longValue();
			}

			if (maxRev > 0 && maxRev <= 5000000) {
				zoomMaxRev = new Long(5000000);
			} else if (maxRev > 5000000 && maxRev <= 10000000) {
				zoomMaxRev = new Long(10000000);
			} else if (maxRev > 10000000 && maxRev <= 25000000) {
				zoomMaxRev = new Long(25000000);
			} else if (maxRev > 25000000 && maxRev <= 50000000) {
				zoomMaxRev = new Long(50000000);
			} else if (maxRev > 50000000 && maxRev <= 100000000) {
				zoomMaxRev = new Long(100000000);
			} else if (maxRev > 100000000 && maxRev <= 250000000) {
				zoomMaxRev = new Long(250000000);
			} else if (maxRev > 250000000 && maxRev <= 500000000) {
				zoomMaxRev = new Long(500000000);
			} else if (maxRev > 500000000 && maxRev <= 1000000000) {
				zoomMaxRev = new Long(1000000000);
			} else if (maxRev > 1000000000 && maxRev <= new Long("5000000000").longValue()) {
				zoomMaxRev = new Long("5000000000").longValue();
			} else if (maxRev > new Long("5000000000").longValue()) {
				// TODO - need to validate
			}

			minMaxKeys.setKey(zoomMinRev);
			minMaxKeys.setValue(zoomMaxRev);
			return minMaxKeys;
		}

		private KeyValuePair<Long, Long> getEmployeeMap(Double minEmployee, Double maxEmployee) {

			Long zoomMinEmp = new Long(0);
			Long zoomMaxEmp = new Long(0);

			long minEmp = minEmployee.longValue();
			long maxEmp = maxEmployee.longValue();

			KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

			if (minEmp > 0 && minEmp < 5) {
				zoomMinEmp = new Long(1);
			} else if (minEmp >= 5 && minEmp < 10) {
				zoomMinEmp = new Long(5);
			} else if (minEmp >= 10 && minEmp < 20) {
				zoomMinEmp = new Long(10);
			} else if (minEmp >= 20 && minEmp < 50) {
				zoomMinEmp = new Long(20);
			} else if (minEmp >= 50 && minEmp < 100) {
				zoomMinEmp = new Long(50);
			} else if (minEmp >= 100 && minEmp < 250) {
				zoomMinEmp = new Long(100);
			} else if (minEmp >= 250 && minEmp < 500) {
				zoomMinEmp = new Long(250);
			} else if (minEmp >= 500 && minEmp < 1000) {
				zoomMinEmp = new Long(500);
			} else if (minEmp >= 1000 && minEmp < 5000) {
				zoomMinEmp = new Long(1000);
			} else if (minEmp >= 5000 && minEmp < 10000) {
				zoomMinEmp = new Long(5000);
			} else if (minEmp >= 10000) {
				zoomMinEmp = new Long(10000);
			}

			if (maxEmp > 0 && maxEmp <= 5) {
				zoomMaxEmp = new Long(5);
			} else if (maxEmp > 5 && maxEmp <= 10) {
				zoomMaxEmp = new Long(10);
			} else if (maxEmp > 10 && maxEmp <= 20) {
				zoomMaxEmp = new Long(20);
			} else if (maxEmp > 20 && maxEmp <= 50) {
				zoomMaxEmp = new Long(50);
			} else if (maxEmp > 50 && maxEmp <= 100) {
				zoomMaxEmp = new Long(100);
			} else if (maxEmp > 100 && maxEmp <= 250) {
				zoomMaxEmp = new Long(250);
			} else if (maxEmp > 250 && maxEmp <= 500) {
				zoomMaxEmp = new Long(500);
			} else if (maxEmp > 500 && maxEmp <= 1000) {
				zoomMaxEmp = new Long(1000);
			} else if (maxEmp > 1000 && maxEmp <= 5000) {
				zoomMaxEmp = new Long(5000);
			} else if (maxEmp > 5000 && maxEmp <= 10000) {
				zoomMaxEmp = new Long(10000);
			} else if (maxEmp > 10000) {
				// TODO - need to validate
			}

			minMaxKeys.setKey(zoomMinEmp);
			minMaxKeys.setValue(zoomMaxEmp);
			return minMaxKeys;
		}

		private double validateDoubleTypeFieldValue(double inputValue, String fieldName) {
			try {
				if (inputValue < 0) {
					throw new IllegalArgumentException("'" + fieldName + "' must be greater than 'zero'.");
				} else {
					return inputValue;
				}
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("'" + inputValue + "' is not in correct format. ");
			}
		}

		private String getFormattedPhone(String phone) {
			String formattedPhone = "";
			if (phone != null) {
				String phoneNumberWithSpaces = phone.replaceAll("[^a-zA-Z0-9\\s+]", ""); // Removes all spacial
																							// characters except white
																							// spaces.
				formattedPhone = phoneNumberWithSpaces.replaceAll("\\s", ""); // Removes all white spaces.
			}
			return formattedPhone;
		}

		private String validateMultiTypeFieldValue(Cell cell, Row row, String fieldName) {
			String value;
			// DATE:20/02/2018 REASON:Added condition for postal code (connvert 12345.0 to
			// 12345)
			if (fieldName.equals("Postal Code")) {
				DataFormatter dataFormatter = new DataFormatter();
				value = dataFormatter.formatCellValue(cell);
			} else if (cell.getCellType() == CellType.STRING) {
				value = cell.getStringCellValue();
			} else if (cell.getCellType() == CellType.NUMERIC) {
				value = String.valueOf(cell.getNumericCellValue());
			} else {
				logger.debug("excelFileRead():validateExcelFileRecords() : Row " + (row.getRowNum() + 1) + ":- "
						+ fieldName + "  must contain only Number or String values.");
				throw new IllegalArgumentException("Row " + (row.getRowNum() + 1) + ":- " + fieldName
						+ "  must contain only Number or String values.");
			}
			return value;
		}

		private List<String> isMandatoryColumnsAvailable(Set<String> fileColumns, List<String> mandatoryFields) {
			List<String> absentColumnsList = new ArrayList<String>();
			for (String mandatoryField : mandatoryFields) {
				if (fileColumns.stream().noneMatch(s -> s.equalsIgnoreCase(mandatoryField))) {
					absentColumnsList.add(mandatoryField);
				}
			}
			return absentColumnsList;
		}

		private String validateSourceFieldValue(String inputValue, Row row, String fieldName) {
			String value;
			if (inputValue.equalsIgnoreCase("ZOOMI")) {
				value = CampaignContactSource.ZOOMINFO.name();
			} else if (inputValue.equalsIgnoreCase("NETP")) {
				value = CampaignContactSource.NETPROSPEX.name();
			} else {

				value = inputValue;

				/*
				 * logger.info("excelFileRead():validateExcelFileRecords() : Row "+(row.
				 * getRowNum()+1)+":- "+fieldName
				 * +"  must contain only FILE, ZOOMI, NETP values."); throw new
				 * IllegalArgumentException("Row "+(row.getRowNum()+1)+":- "+fieldName
				 * +"  must contain only FILE, ZOOMI, NETP values.");
				 */
			}
			return value;
		}

		private boolean isPersonRecordAlreadyPurchased(UploadedContact uploadedContact) {
			String firstName = uploadedContact.getFirstName();
			String lastName = uploadedContact.getLastName();
			String company = uploadedContact.getOrganizationName();
			String title = uploadedContact.getTitle();
			 List<String> campaignIds = new ArrayList<String>();
	         campaignIds.add(campaignId);
	         campaignIds.add(campaignId + NON_CALLABLE);
	        // campaignIds.add(campaignId + PIVOT_REMOVED);

			// List<ProspectCallLog> uniqueRecord =
			// prospectCallLogRepository.findUinqueRecord(campaignId,firstName, lastName,
			// title, company);
			int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName,
					title, company);
			// if (uniqueRecord.size() == 0) {
			if (uniqueRecord == 0) {
				return false;
			} else {
				return true;
			}
		}

		private List<String> getActiveCampaigns() {
			List<CampaignDTO> campaignList = new ArrayList<CampaignDTO>();
			runningCampaignIds = new ArrayList<String>();
			campaignList = campaignService.findActiveWithProjectedFields(new ArrayList<String>());
			// List<String> campaignIds = new ArrayList<String>();
			if (campaignList != null && !campaignList.isEmpty()) {
				for (CampaignDTO campaign : campaignList) {
					if (!runningCampaignIds.contains(campaign.getId())) {
						runningCampaignIds.add(campaign.getId());
					}
				}
			}
			return runningCampaignIds;
		}

		private boolean checkProspectVersion(UploadedContact uploadedContact) {
			String firstName = uploadedContact.getFirstName();
			String lastName = uploadedContact.getLastName();
			String company = uploadedContact.getOrganizationName();
			String title = uploadedContact.getTitle();
			int count = prospectCallLogRepository.findProspectCountbyRunningCampaigns(runningCampaignIds, firstName,
					lastName, title, company);// TODO we need to check for callable prospects only.
			int maxProspectVersion = propertyService.getIntApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.MAX_PROSPECT_VERSION.name(), MAX_PROSPECT_VERSION);
			if (count >= maxProspectVersion) {
				return false;
			} else {
				uploadedContact.setProspectVersion(count + 1);
			}
			return true;
		}

		class UploadedContact {
			private String campaignId;
			private String prefix;
			private String firstName;
			private String lastName;
			private String suffix;
			private String title;
			private String department;
			private List<ContactFunction> functions;
			private String organizationName;
			private String domain;
			private List<ContactIndustry> industries;
			private double minRevenue;
			private double maxRevenue;
			private double minEmployeeCount;
			private double maxEmployeeCount;
			private String email;
			private String addressLine1;
			private String addressLine2;
			private String city;
			private String stateCode;
			private String country;
			private String postalCode;
			private String phone;
			private String companyPhone;
			private String extension;
			private String source;
			private String sourceId;
			private String sourceCompanyId;
			private String managementLevel;
			private int prospectVersion;
			private String zoomCompanyUrl;
			private String zoomPersonUrl;
			private boolean isDirectPhone;
			private boolean isEu;
			private String dataSource;
			Map<String, String> extraAttributes;
			private List<AgentQaAnswer> answers;
			private Integer sortOrder;

			public Map<String, String> getExtraAttributes() {
				return extraAttributes;
			}

			public void setExtraAttributes(Map<String, String> extraAttributes) {
				this.extraAttributes = extraAttributes;
			}

			public UploadedContact() {

			}

			public String getCampaignId() {
				return campaignId;
			}

			public void setCampaignId(String campaignId) {
				this.campaignId = campaignId;
			}

			public String getSourceId() {
				return sourceId;
			}

			public void setSourceId(String sourceId) {
				this.sourceId = sourceId;
			}

			public String getSource() {
				return source;
			}

			public void setSource(String source) {
				this.source = source;
			}

			public String getPrefix() {
				return prefix;
			}

			public void setPrefix(String prefix) {
				this.prefix = prefix;
			}

			public String getFirstName() {
				return firstName;
			}

			public void setFirstName(String firstName) {
				this.firstName = firstName;
			}

			public String getLastName() {
				return lastName;
			}

			public void setLastName(String lastName) {
				this.lastName = lastName;
			}

			public String getSuffix() {
				return suffix;
			}

			public void setSuffix(String suffix) {
				this.suffix = suffix;
			}

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			public String getOrganizationName() {
				return organizationName;
			}

			public void setOrganizationName(String organizationName) {
				this.organizationName = organizationName;
			}

			public String getDomain() {
				return domain;
			}

			public void setDomain(String domain) {
				this.domain = domain;
			}

			public List<ContactFunction> getFunctions() {
				return functions;
			}

			public void setFunctions(List<ContactFunction> functions) {
				this.functions = functions;
			}

			public List<ContactIndustry> getIndustries() {
				return industries;
			}

			public void setIndustries(List<ContactIndustry> industries) {
				this.industries = industries;
			}

			public double getMinRevenue() {
				return minRevenue;
			}

			public void setMinRevenue(double minRevenue) {
				this.minRevenue = minRevenue;
			}

			public double getMaxRevenue() {
				return maxRevenue;
			}

			public void setMaxRevenue(double maxRevenue) {
				this.maxRevenue = maxRevenue;
			}

			public double getMinEmployeeCount() {
				return minEmployeeCount;
			}

			public void setMinEmployeeCount(double minEmployeeCount) {
				this.minEmployeeCount = minEmployeeCount;
			}

			public double getMaxEmployeeCount() {
				return maxEmployeeCount;
			}

			public void setMaxEmployeeCount(double maxEmployeeCount) {
				this.maxEmployeeCount = maxEmployeeCount;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}

			public String getAddressLine1() {
				return addressLine1;
			}

			public void setAddressLine1(String addressLine1) {
				this.addressLine1 = addressLine1;
			}

			public String getAddressLine2() {
				return addressLine2;
			}

			public void setAddressLine2(String addressLine2) {
				this.addressLine2 = addressLine2;
			}

			public String getCity() {
				return city;
			}

			public void setCity(String city) {
				this.city = city;
			}

			public String getStateCode() {
				return stateCode;
			}

			public void setStateCode(String stateCode) {
				this.stateCode = stateCode;
			}

			public String getCountry() {
				return country;
			}

			public void setCountry(String country) {
				this.country = country;
			}

			public String getPostalCode() {
				return postalCode;
			}

			public void setPostalCode(String postalCode) {
				this.postalCode = postalCode;
			}

			public String getPhone() {
				return phone;
			}

			public void setPhone(String phone) {
				this.phone = phone;
			}

			public String getCompanyPhone() {
				return companyPhone;
			}

			public void setCompanyPhone(String companyPhone) {
				this.companyPhone = companyPhone;
			}

			public String getExtension() {
				return extension;
			}

			public void setExtension(String extension) {
				this.extension = extension;
			}

			/**
			 * @return the department
			 */
			public String getDepartment() {
				return department;
			}

			/**
			 * @param department the department to set
			 */
			public void setDepartment(String department) {
				this.department = department;
			}

			public String getSourceCompanyId() {
				return sourceCompanyId;
			}

			public void setSourceCompanyId(String sourceCompanyId) {
				this.sourceCompanyId = sourceCompanyId;
			}

			public String getManagementLevel() {
				return managementLevel;
			}

			public void setManagementLevel(String managementLevel) {
				this.managementLevel = managementLevel;
			}

			public int getProspectVersion() {
				return prospectVersion;
			}

			public void setProspectVersion(int prospectVersion) {
				this.prospectVersion = prospectVersion;
			}

			public String getZoomCompanyUrl() {
				return zoomCompanyUrl;
			}

			public void setZoomCompanyUrl(String zoomCompanyUrl) {
				this.zoomCompanyUrl = zoomCompanyUrl;
			}

			public String getZoomPersonUrl() {
				return zoomPersonUrl;
			}

			public void setZoomPersonUrl(String zoomPersonUrl) {
				this.zoomPersonUrl = zoomPersonUrl;
			}

			public boolean isDirectPhone() {
				return isDirectPhone;
			}

			public void setDirectPhone(boolean isDirectPhone) {
				this.isDirectPhone = isDirectPhone;
			}

			public boolean isEu() {
				return isEu;
			}

			public void setEu(boolean isEu) {
				this.isEu = isEu;
			}

			public String getDataSource() {
				return dataSource;
			}

			public void setDataSource(String dataSource) {
				this.dataSource = dataSource;
			}

			public List<AgentQaAnswer> getAnswers() {
				return answers;
			}

			public void setAnswers(List<AgentQaAnswer> answers) {
				this.answers = answers;
			}

			public Integer getSortOrder() {
				return sortOrder;
			}

			public void setSortOrder(Integer sortOrder) {
				this.sortOrder = sortOrder;
			}
		}
	}

	@Override
	public List<ListProviderUsage> getListPurchaseHistory(String campaignId) {
		return listProviderUsageRepository.findPurchaseHitoryByCampaignId(campaignId,
				Sort.by(Sort.Direction.DESC, "createdDate"));
	}

	@Override
	public List<CampaignContactDTO> getListPurchasedRecords(String listPurchaseTransactionId) {

		List<CampaignContact> campaignContacts = campaignContactRepository
				.findByListPurchaseTransactionId(listPurchaseTransactionId);
		List<CampaignContactDTO> campaignContactDTOs = new ArrayList<CampaignContactDTO>();

		ListProviderUsage listProviderUsage = listProviderUsageRepository
				.findPurchaseSummaryByTransactionId(listPurchaseTransactionId);

		List<ContactPurchaseError> contactPurchaseErrors = null;
		if (listProviderUsage != null) {
			contactPurchaseErrors = listProviderUsage.getErrors();
		}
		for (CampaignContact campaignContact : campaignContacts) {
			CampaignContactDTO campaignContactDTO = new CampaignContactDTO(campaignContact);
			if (contactPurchaseErrors != null) {
				for (ContactPurchaseError contactPurchaseError : contactPurchaseErrors) {
					if (contactPurchaseError.getSourceId().equals(campaignContact.getSourceId())) {
						campaignContactDTO.setPurchaseError(contactPurchaseError.getErrorMsg());
					}
				}
			}
			campaignContactDTOs.add(campaignContactDTO);
		}
		return campaignContactDTOs;
	}

	private HashSet<String> getExistingOrganizationNames(String campaignId) {
		List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository
				.findOrganizationsExcludedFromDataPurchase(campaignId);
		HashSet<String> existOrganizationNames = new HashSet<String>();
		for (int i = 0; i < prospectCallLogList.size(); i++) {
			String company = prospectCallLogList.get(i).getProspectCall().getProspect().getCompany();
			if (company != null && !company.isEmpty()) {
				existOrganizationNames.add(company.toUpperCase());
			}
		}
		return existOrganizationNames;
	}

	private boolean isSourceIdAlreadyPurchased(String sourceId) {
		if (sourceId == null || sourceId == "") {
			return false;
		}
		CampaignContact campaignContact = null;
		try {
			campaignContact = campaignContactRepository.findBySourceId(sourceId, "ZOOMINFO");
		} catch (Exception e) {
			logger.error("Error occurred while quering campaign contact for sourceId [{}]", sourceId);
			logger.error("Exception is : ", e);
		}
		if (campaignContact == null) {
			return false;
		} else {
			return true;
		}
	}

	public String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replace("-", "");
		int length = removeSpclChar.length();
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = phone.substring(startPoint, endPoint);
		return areaCode;
	}

	public String getTimeZone(String phone) {
		List<OutboundNumber> outboundNumberList;
		int areaCode = Integer.parseInt(findAreaCode(phone));
		outboundNumberList = outboundNumberRepository.findTimeZone(areaCode);
		if (outboundNumberList.size() == 0) {
			return null;
		}

		// find a random number from the list
		OutboundNumber outboundNumber = outboundNumberList.get(0);

		return outboundNumber.getTimeZone();
	}

	private List<ProspectCallLog> getCallables(String cid) {
		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(cid);
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsTenByRetryCount(1, cid);
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsTenByRetryCount(2, cid);
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsTenByRetryCount(3, cid);
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsTenByRetryCount(4, cid);
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsTenByRetryCount(5, cid);
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsTenByRetryCount(6, cid);
		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsTenByRetryCount(7, cid);
		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsTenByRetryCount(8, cid);
		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsTenByRetryCount(9, cid);
		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsTenByRetryCount(10, cid);
		// List<ProspectCallLog> prospectCallLogList786 =
		// prospectCallLogRepository.findContactsByQueued(cid);
		// List<ProspectCallLog> prospectCallLogList786 =
		// prospectCallLogRepository.findContactsTenByRetryCount(786, cid);
		// List<ProspectCallLog> prospectCallLogList886 =
		// prospectCallLogRepository.findContactsTenByRetryCount(886, cid);

		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		prospectCallLogList.addAll(prospectCallLogList0);
		prospectCallLogList.addAll(prospectCallLogList1);
		prospectCallLogList.addAll(prospectCallLogList2);
		prospectCallLogList.addAll(prospectCallLogList3);
		prospectCallLogList.addAll(prospectCallLogList4);
		prospectCallLogList.addAll(prospectCallLogList5);
		prospectCallLogList.addAll(prospectCallLogList6);
		prospectCallLogList.addAll(prospectCallLogList7);
		prospectCallLogList.addAll(prospectCallLogList8);
		prospectCallLogList.addAll(prospectCallLogList9);
		prospectCallLogList.addAll(prospectCallLogList10);
		// prospectCallLogList.addAll(prospectCallLogList786);
		// prospectCallLogList.addAll(prospectCallLogList886);
		return prospectCallLogList;
	}

	private void setMultiProspect(String cid) {
		//
		int counterx = 0;
		List<ProspectCallLog> prospectCallLogList = getCallables(cid);
		// List<String> companies =
		// prospectCallLogRepositoryImpl.findDistinctCompaniesName(campaignids);
		// for(String cid : campaignids){
		int repcountner = 0;
		List<ProspectCallLog> repProspectCallLogList = prospectCallLogRepository.findReplinishData(cid);
		for (ProspectCallLog rpc : repProspectCallLogList) {
			rpc.getProspectCall().setCallRetryCount(1);
			rpc.setStatus(ProspectCallStatus.CALL_BUSY);
			rpc.getProspectCall().setSubStatus("BUSY");
			rpc.getProspectCall().setDispositionStatus("DIALERCODE");
			// System.out.println("state code is null");
			prospectCallLogRepository.save(rpc);
			repcountner++;
		}
		logger.debug("total data replenish before tencompanies : " + repcountner);

		Map<String, List<ProspectCallLog>> prospectCallMap = new java.util.HashMap<String, List<ProspectCallLog>>();
		// System.out.println(prospectCallLogList.size());
		int updatecounter = 0;
		for (ProspectCallLog pc : prospectCallLogList) {
			// for(String comp : companies){
			// if(pc.getProspectCall().getProspect().getCompany().equalsIgnoreCase(comp)){
			String key = pc.getProspectCall().getProspect().getPhone();

			List<ProspectCallLog> prospectl = prospectCallMap.get(key);
			if (prospectl != null && prospectl.size() > 0) {
				ProspectCallLog ep = prospectl.get(0);
				if (ep.getProspectCall().getProspect().isDirectPhone()) {
					// ep.getProspectCall().getProspect().setDirectPhone(false);
					prospectCallLogRepository.save(ep);
				}
				if (ep.getProspectCall().getProspect().getStateCode() != null) {
					if (pc.getProspectCall().getProspect().getStateCode() != null) {
						// System.out.println("ABC"+counterx++);
						/*
						 * if(counterx==48){ //System.out.println("HERE"); }
						 */
					}
					if (pc.getProspectCall().getProspect().getStateCode() != null && ep.getProspectCall().getProspect()
							.getStateCode().equalsIgnoreCase(pc.getProspectCall().getProspect().getStateCode())) {
						prospectl.add(pc);
					} else {
						/*pc.getProspectCall().getProspect()
								.setStateCode(ep.getProspectCall().getProspect().getStateCode());*/
						prospectl.add(pc);
					}
					if (pc.getProspectCall().getProspect().isDirectPhone()) {
						// pc.getProspectCall().getProspect().setDirectPhone(false);
						prospectCallLogRepository.save(pc);
					}
				} else {
					ep.getProspectCall().setCallRetryCount(222222);
					ep.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					// ep.getProspectCall().getProspect().setDirectPhone(false);
					// System.out.println(updatecounter++);
					prospectCallLogRepository.save(ep);
					updatecounter++;

				}

				prospectCallMap.put(key, prospectl);
			} else {
				prospectl = new ArrayList<ProspectCallLog>();
				prospectl.add(pc);
				prospectCallMap.put(key, prospectl);
			}
			// }
			// }
		}
		List<ProspectCallLog> toRemove = new ArrayList<ProspectCallLog>();
		for (Map.Entry<String, List<ProspectCallLog>> entry : prospectCallMap.entrySet()) {
			List<ProspectCallLog> pl = entry.getValue();

			if (entry.getValue().size() > 4) {
				int counter = 1;
				for (ProspectCallLog p : pl) {
					if (counter > 4) {
						// System.out.println(entry.getValue().size());
						// toRemove.add(p);// commenting as it is handled in cache level
					}
					counter++;
				}
			}
		}
		List<ProspectCallLog> upDateList = new ArrayList<ProspectCallLog>();
		for (ProspectCallLog removeP : toRemove) {
			removeP.getProspectCall().setCallRetryCount(222222);
			removeP.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			upDateList.add(removeP);
			updatecounter++;
		}

		logger.debug("Please wait we are updating Record, It will take some time ......");
		prospectCallLogRepository.saveAll(upDateList);
		logger.debug("Total record  updated - " + updatecounter);
		// totalCount = prospectCallLogList.size() + totalCount;
		// }

	}

	/*public SuppressionList getGlobalSuppressionListByCampaignId(SuppressionList supressionList, String campaignId) {
		Campaign campaign = campaignRepository.findOne(campaignId);
	
		SuppressionList gSupressionList = suppressionListRepository
				.findGlobalSupressionListByOrganization(campaign.getOrganizationId());
		if (supressionList != null) {
			List<String> cDomains = supressionList.getDomains();
			List<String> cCompanies = supressionList.getCompanies();
			List<String> cEmails = supressionList.getEmailIds();
			if (gSupressionList != null) {
				if (cDomains == null)
					cDomains = new ArrayList<String>();
				if (cCompanies == null)
					cCompanies = new ArrayList<String>();
				if (cEmails == null)
					cEmails = new ArrayList<String>();
				if (gSupressionList.getDomains() != null && gSupressionList.getDomains().size() > 0)
					cDomains.addAll(gSupressionList.getDomains());
				if (gSupressionList.getCompanies() != null && gSupressionList.getCompanies().size() > 0)
					cCompanies.addAll(gSupressionList.getCompanies());
				if (gSupressionList.getEmailIds() != null && gSupressionList.getEmailIds().size() > 0)
					cEmails.addAll(gSupressionList.getEmailIds());
				supressionList.setDomains(cDomains);
				supressionList.setCompanies(cCompanies);
				supressionList.setEmailIds(cEmails);
			}
		}
		return supressionList;
	}*/

	private String getSuppressedHostName(String domain) {

		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}
///////////// New Suppression   ///////////
	
	private boolean isCompanySuppressedNew(ProspectCallLog prospectCallLog) {
		boolean suppressed = false;
		if (prospectCallLog.getProspectCall().getProspect().getCompany() != null
				&& !prospectCallLog.getProspectCall().getProspect().getCompany().isEmpty()) {
			long supCompanyListCount = suppressionListNewRepository.countSupressionListByCampaignCompany(prospectCallLog.getProspectCall().getCampaignId(), 
					prospectCallLog.getProspectCall().getProspect().getCompany());
			if(supCompanyListCount > 0)
				suppressed = true;
		}
		return suppressed;
	}
	private boolean isDomainSuppressedNew(ProspectCallLog prospectCallLog) {
		boolean suppressed = false;
		String pdomain = null;
		if (prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain").toString()
						.isEmpty())
			pdomain = prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
		if (pdomain != null && !pdomain.isEmpty()) {
			pdomain = getSuppressedHostName(pdomain);
		}

		if (pdomain != null && !pdomain.isEmpty()) {
			long supDomainCount = suppressionListNewRepository.countSupressionListByCampaignDomain(prospectCallLog.getProspectCall().getCampaignId(), 
					pdomain);
			if(supDomainCount > 0)
				suppressed = true;
		} else  if(prospectCallLog.getProspectCall().getProspect().getEmail()!=null && !prospectCallLog.getProspectCall().getProspect().getEmail().isEmpty()) {
			 String pEmail = prospectCallLog.getProspectCall().getProspect().getEmail();
			 String domEmail = pEmail.substring(pEmail.indexOf("@")+1, pEmail.length());

			 long compSupCount = suppressionListNewRepository.countSupressionListByCampaignDomain(
					 prospectCallLog.getProspectCall().getCampaignId(), domEmail);
			 if(compSupCount > 0) {
				 suppressed = true;
			 }
		 }
		return suppressed;
	}

////////////////////////////////////
/*	private boolean isCompanySuppressed(ProspectCallLog prospectCallLog, List<String> suppressDomain,
			List<String> suppressCompanyName) {
		boolean suppressed = false;
		String pdomain = null;
		if (prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain").toString()
						.isEmpty())
			pdomain = prospectCallLog.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
		if (pdomain != null && !pdomain.isEmpty()) {
			pdomain = getSuppressedHostName(pdomain);
		}

		if (pdomain != null && !pdomain.isEmpty() && suppressDomain != null
				&& suppressDomain.contains(pdomain.toLowerCase())) {
			suppressed = true;
		} else if (prospectCallLog.getProspectCall().getProspect().getCompany() != null
				&& !prospectCallLog.getProspectCall().getProspect().getCompany().isEmpty()
				&& suppressCompanyName != null && suppressCompanyName
						.contains(prospectCallLog.getProspectCall().getProspect().getCompany().toLowerCase())) {
			suppressed = true;
		}
		return suppressed;
	}

	private boolean isEmailSuppressed(ProspectCallLog prospectCallLog, List<String> suppressEmail) {
		boolean suppressed = false;
		if (prospectCallLog.getProspectCall().getProspect().getEmail() != null
				&& !prospectCallLog.getProspectCall().getProspect().getEmail().isEmpty() && suppressEmail != null
				&& suppressEmail.contains(prospectCallLog.getProspectCall().getProspect().getEmail())) {
			suppressed = true;
		}
		return suppressed;
	}*/

	private void setQualityBucket(String cid, Campaign campaign) {
		boolean newSuppressedFlag = false;
//		Campaign campaign = campaignService.getCampaign(cid);
		List<String> suppressDomains = new ArrayList<String>();
		List<String> suppressEmails = new ArrayList<String>();
		List<String> suppressCompanyNames = new ArrayList<String>();
		/////// new suppression list  //////////
		int emailsuppression =  suppressionListNewRepository.countByCampaignOrganizationEmail(cid, campaign.getOrganizationId());
		int domainsuppression =  suppressionListNewRepository.countSupressionListByCampaignDomain(cid);
		int companysuppresion =  suppressionListNewRepository.countSupressionListByCampaignCompany(cid);

		if(emailsuppression>0 || domainsuppression > 0 || companysuppresion > 0) {
			newSuppressedFlag=true;
		}/*else {
	
			SuppressionList suppressionList = suppressionListRepository.findSupressionListByCampaignIdAndStatus(cid);
			/*SuppressionList gsuppressionList = getGlobalSuppressionListByCampaignId(suppressionList, cid);
			if(suppressionList!=null && gsuppressionList!=null) {
				List<String> domains= suppressionList.getDomains();
				domains.addAll(gsuppressionList.getDomains());
				List<String> companies= suppressionList.getCompanies();
				companies.addAll(gsuppressionList.getCompanies());
				List<String> emails= suppressionList.getEmailIds();
				emails.addAll(gsuppressionList.getEmailIds());
				suppressionList.setCompanies(companies);
				suppressionList.setEmailIds(emails);
				suppressionList.setDomains(domains);
			}
			
			if (suppressionList != null) {
				List<String> sDomains = suppressionList.getDomains();
				List<String> domains = new ArrayList<String>();
				if (sDomains != null && sDomains.size() > 0) {
					for (String domain : sDomains) {
						if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
							String cDomain = getSuppressedHostName(domain);
							if (!cDomain.isEmpty())
								domains.add(cDomain);
						}
					}
				}
				List<String> emails = suppressionList.getEmailIds();
				List<String> companies = suppressionList.getCompanies();

				if (domains != null && domains.size() > 0) {
					suppressDomains.addAll(domains);
					// campaignCriteria.setSuppressDomains(suppressDomains);
				}
				if (emails != null && emails.size() > 0) {
					suppressEmails.addAll(emails);
				}
				if (companies != null && companies.size() > 0) {
					suppressCompanyNames.addAll(companies);
					// campaignCriteria.setSuppressComapnies(suppressCompanyNames);
				}
			}
		}*/
		
		///////////////////////////////end of new suppression list  //////////////////////////////
		 
		

	

		List<ProspectCallLog> pList = getCallables(cid);
 		for (ProspectCallLog p : pList) {
			StringBuffer sb = new StringBuffer();
			
			////Below to give source priority
			
	    	String sourceWeightage = XtaasUtils.getSourceWeightage(p.getProspectCall().getProspect().getSource());
			
			///////////////end of source priority
			/*
			 * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
			 */
			String emp = "";
			String mgmt = "";
			String tz = XtaasUtils.getStateWeight(p.getProspectCall().getProspect().getTimeZone());
			String direct = "2";
			if (p.getProspectCall().getProspect().isDirectPhone()) {
				direct = "1";
			}

			sb.append(p.getStatus());
			sb.append("|");
			if (p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number) {
				Number n1 = (Number) p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount");
				sb.append(XtaasUtils.getBoundry(n1, "min"));
				sb.append("-");
				emp = XtaasUtils.getEmployeeWeight(n1);
			}

			if (p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
				Number n2 = (Number) p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount");
				sb.append(XtaasUtils.getBoundry(n2, "max"));
			}
			/*
			 * sb.append(p.getProspectCall().getProspect().getCustomAttributeValue(
			 * "minEmployeeCount")); sb.append("-");
			 * sb.append(p.getProspectCall().getProspect().getCustomAttributeValue(
			 * "maxEmployeeCount"));
			 */
			sb.append("|");
			sb.append(XtaasUtils.getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
			mgmt = XtaasUtils.getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());

			int qbSort = 0;
			if (!emp.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
				if (direct != null) {
					String sortString = "";
					if(sourceWeightage!=null && !sourceWeightage.isEmpty()) {
			    		sortString = sourceWeightage + direct + tz + mgmt + emp;
			    	}else if (p.getProspectCall() != null && p.getProspectCall().getSortOrder() != null) {
						sortString = p.getProspectCall().getSortOrder() + mgmt + emp;
					}else {
			    		sortString = direct + tz + mgmt + emp;
			    	}
					qbSort = Integer.parseInt(sortString);
				}
			}

			p.getProspectCall().setQualityBucket(sb.toString());
			p.getProspectCall().setQualityBucketSort(qbSort);

			//if(newSuppressedFlag) {
				boolean csup = false;
				boolean dsup = false;
				boolean esup = false;
				if(companysuppresion>0) {
					csup= isCompanySuppressedNew(p);
				}
				if(domainsuppression>0) {
					dsup= isDomainSuppressedNew(p);
				}
				if(emailsuppression>0) {
					esup = suppressionListService.isEmailSuppressed(p.getProspectCall().getProspect().getEmail(), campaign.getId(),
							campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck());
				}
				if(!csup && !dsup && !esup) {
					prospectCallLogRepository.save(p);
				}else {
					if (p != null && p.getProspectCall() != null) {
						p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						p.getProspectCall().setCallRetryCount(144);
						p.setUpdatedDate(new Date());
						p.setUpdatedBy("SYSTEM_SUPRESSIONLIST");
						prospectCallLogRepository.save(p);
					}
				}
			/*}else {
				if (!isCompanySuppressed(p, suppressDomains, suppressCompanyNames)
						&& !isEmailSuppressed(p, suppressEmails)) {
					prospectCallLogRepository.save(p);
				} else {
					if (p != null && p.getProspectCall() != null) {
						p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						p.getProspectCall().setCallRetryCount(144);
						p.setUpdatedDate(new Date());
						p.setUpdatedBy("SYSTEM_SUPRESSIONLIST");
						prospectCallLogRepository.save(p);
					}
				}
				// p.getProspectCall().getProspect().setTimeZone(timezone);
			}*/
		}
	}
	
	private Map<String, AgentQaAnswer> getDefaultCustomFieldAnswersMapFromCampaign(Campaign campaign) {
		Map<String, AgentQaAnswer> customfieldQandAnswerMap = campaign.getAllDefaultCustomFieldQuestionAndAnswerMap(campaign.getQualificationCriteria().getExpressions());
		if (customfieldQandAnswerMap != null && customfieldQandAnswerMap.size() > 0) {
			return customfieldQandAnswerMap;
		} else {
			return null;
		}
	}

	private String getDefaultCustomFieldAnswerFromCampaignFromColumnName(String columnName, Map<String, AgentQaAnswer> defaultCustomfieldMap) {
		String customfieldKey = "";
		switch (columnName) {
			case "CF01_" :
				customfieldKey = "CUSTOMFIELD_01";
				break;
			case "CF02_" :
				customfieldKey = "CUSTOMFIELD_02";
				break;
			case "CF03_" :
				customfieldKey = "CUSTOMFIELD_03";
				break;
			case "CF04_" :
				customfieldKey = "CUSTOMFIELD_04";
				break;
			case "CF05_" :
				customfieldKey = "CUSTOMFIELD_05";
				break;
			case "CF06_" :
				customfieldKey = "CUSTOMFIELD_06";
				break;
			case "CF07_" :
				customfieldKey = "CUSTOMFIELD_07";
				break;
			case "CF08_" :
				customfieldKey = "CUSTOMFIELD_08";
				break;
			case "CF09_" :
				customfieldKey = "CUSTOMFIELD_09";
				break;
			case "CF10_" :
				customfieldKey = "CUSTOMFIELD_10";
				break;
			default:
				customfieldKey = "";			
		}
		if (customfieldKey != null && !customfieldKey.isEmpty()) {
			AgentQaAnswer defAgentQaAnswer = defaultCustomfieldMap.get(customfieldKey);
			if (defAgentQaAnswer != null) {
				return defAgentQaAnswer.getAnswer();
			}
		}
		return null;
	}

	private String getDefaultCustomFieldAnswerFromCampaignFromAttributeName(String attribute, Map<String, AgentQaAnswer> defaultCustomfieldMap) {
			AgentQaAnswer defAgentQaAnswer = defaultCustomfieldMap.get(attribute);
			if (defAgentQaAnswer != null) {
				return defAgentQaAnswer.getAnswer();
			}
		return null;
	}

}
