package com.xtaas.application.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.AbmList;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.CRMMapping;
import com.xtaas.db.entity.CampaignDNorm;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Organization.OrganizationLevel;
import com.xtaas.db.entity.Partner;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.AbmListRepository;
import com.xtaas.db.repository.CRMMappingRepository;
import com.xtaas.db.repository.CampaignDNormRepository;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.PartnerRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.QaRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
//import com.xtaas.db.repository.SuppressionListRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;
import com.xtaas.domain.valueobject.CampaignNameSearchResult;
import com.xtaas.domain.valueobject.CampaignResource;
import com.xtaas.domain.valueobject.CampaignSearchResult;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.CampaignTeamDTO;
import com.xtaas.domain.valueobject.CampaignTeamInvitation;
import com.xtaas.domain.valueobject.CampaignTeamShortlisted;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.domain.valueobject.SupervisorCampaignResult;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.RestClientService;
import com.xtaas.service.UserService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.ActiveAssetDTO;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentInvitationDTO;
import com.xtaas.web.dto.AssetDTO;
import com.xtaas.web.dto.CallGuideDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.CampaignDetailsForAgentDTO;
import com.xtaas.web.dto.CampaignManagerMetricsDTO;
import com.xtaas.web.dto.CampaignNameSearchDTO;
import com.xtaas.web.dto.CampaignSearchDTO;
import com.xtaas.web.dto.CampaignTeamShortlistedDTO;
import com.xtaas.web.dto.CampaignWorkroomDTO;
import com.xtaas.web.dto.EmailMessageDTO;
import com.xtaas.web.dto.ProspectCallSearchClause;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.ProspectDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;
import com.xtaas.web.dto.WorkScheduleDTO;
import com.xtaas.web.vm.SupervisorCampaignsVM;

// TODO
// If the user is not a campaign manager he/she should not be able to create a campaign
// If the user is not the one who created the user he/she cannot modify the campaign
@Service
public class CampaignServiceImpl implements CampaignService {
	private static final Logger logger = LoggerFactory.getLogger(CampaignServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
    private ProspectCallLogService prospectCallLogService;

	@Autowired
	private AgentService agentService;

	@SuppressWarnings("unused")
	@Autowired
	private QaRepository qaRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private AuthTokenGeneratorService authTokenGeneratorService;

	@Autowired
	private RestClientService restClientService;

	@Autowired
	private CampaignDNormRepository campaignDNormRepository;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private QaService qaService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private TeamService teamService;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private NewClientMappingRepository newClientMappingRepository;

	@Autowired
	private CRMMappingRepository crmMappingRepository;


	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private AbmListNewRepository abmListNewRepository;
	
	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;
	
	@Autowired
	private CampaignService campaignService;


	@Override
	public Campaign getCampaign(String campaignId) {
	    Campaign campaign = campaignRepository.findOneById(campaignId);
	    if(campaign == null) {
			throw new IllegalArgumentException("Campaign Id: "+campaignId+ " does not exit");
		}

		return campaign;
	}

	@Override
	public List<Campaign> getAllCampaigns() {
		List<Campaign> campaigns = campaignRepository.findAll();
		return campaigns;

	}

	@Override
	public List<Campaign> getActiveCampaigns() {
		List<Campaign> activeCampaign = campaignRepository.getActiveCampaigns();
		return activeCampaign;
	}

	@Override
	public List<CampaignDTO> findActiveWithProjectedFields(List<String> fields) {
		List<CampaignDTO> activeCampaigns = campaignRepository.findActiveWithProjectedFields(fields);
		return activeCampaigns;
	}

	@Override
	public List<Campaign> getActiveCampaignsByOrg(String organizationId) {
	    List<Campaign> activeCampaign = campaignRepository.getActiveCampaignsByOrg(organizationId);

		return activeCampaign;
	}

	@Override
	public List<Campaign> getActiveXtaasCampaigns() {
	    List<Campaign> activeCampaign = campaignRepository.getActiveXtaasCampaigns();
		return activeCampaign;
	}

	@Override
	public List<Campaign> getActiveOtherCampaigns() {
	    List<Campaign> activeCampaign = campaignRepository.getActiveOtherCampaigns();
		return activeCampaign;
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public CampaignSearchResult listByCampaignManager(CampaignSearchDTO campaignSearchDTO) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		List<Campaign> campaigns = campaignRepository.searchCampaigns(userLogin.getOrganization(),userLogin.getUsername(), campaignSearchDTO
				.getName(), campaignSearchDTO.getStatus(), PageRequest.of(campaignSearchDTO.getPage(),
				campaignSearchDTO.getSize(), campaignSearchDTO.getDirection(), campaignSearchDTO.getSort()));

		List<CampaignDTO> campaignDTOs = new ArrayList<CampaignDTO>();
		for (Campaign campaign : campaigns) {
			int successCount = prospectCallLogService.getSuccessCount(campaign.getId());
			CampaignDTO campaignDTO = new CampaignDTO(campaign);
			campaignDTO.setDeliveryTargetAchieved(successCount);
			campaignDTOs.add(campaignDTO);
		}
		if (campaignSearchDTO.getDirection().equals(Direction.ASC)) {
			campaignDTOs.sort((object1, object2) -> object1.getName().compareToIgnoreCase(object2.getName()));
		} else {
			campaignDTOs.sort((object1, object2) -> object2.getName().compareToIgnoreCase(object1.getName()));
		}
		return new CampaignSearchResult(campaignRepository.searchCampaignCount(userLogin.getOrganization(),userLogin.getUsername(),
				campaignSearchDTO.getName(), campaignSearchDTO.getStatus()), campaignDTOs);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public CampaignSearchResult listBySupervisor(CampaignSearchDTO campaignSearchDTO) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		@SuppressWarnings("serial")
		List<String> statuses = new ArrayList<String>();

		/* START
		 * DATE : 19/04/2017
		 * REASON : modified to handle campaigns on supervisor screen, when flag is true will show campaigns with statuses RUNNING, PAUSED */
		if (campaignSearchDTO.isFlag()) {
//			DATE:27/02/2018 Do not show PAUSED campaign on supervisor screen
//			statuses.add(CampaignStatus.PAUSED.name());
			statuses.add(CampaignStatus.RUNNING.name());
		}
		/* END */
		else {
//			DATE:27/02/2018 Do not show PAUSED campaign on supervisor screen
//			statuses.add(CampaignStatus.PAUSED.name());
			statuses.add(CampaignStatus.RUNNING.name());
			statuses.add(CampaignStatus.PLANNING.name());
		}

		/* START	DATE : 29/06/2017 	REASON : adding hard core supervisorID to fetch all campaigns (It's a hack to give access to partner supervisor) */
		List<Campaign> campaigns = campaignRepository.findBySupervisorId(
				userLogin.getUsername(),
				statuses,
				PageRequest.of(campaignSearchDTO.getPage(), campaignSearchDTO.getSize(), campaignSearchDTO
						.getDirection(), campaignSearchDTO.getSort()));
		List<CampaignDTO> campaignDTOs = new ArrayList<CampaignDTO>();
		for (Campaign campaign : campaigns) {
			CampaignDTO campaignDTO = new CampaignDTO(campaign);
			if (campaign.getTeam() != null) {
				campaignDTO.setCallSpeedPerMinPerAgent(campaign.getTeam().getCallSpeedPerMinPerAgent());
				campaignDTO.setLeadSortOrder(campaign.getTeam().getLeadSortOrder());
			}

			/* START READ/WRITE ACCESS TO SUPERVISOR
			 * DATE : 29/06/2017
			 * REASON : adding below flag in campaign to give read only access to partner supervisors & write access only to 'gsupervisor' (It's a hack) */
			if (campaign.getTeam().getSupervisorId().equals(userLogin.getUsername())) {
				campaignDTO.setReadOnlyCampaign(false);
			} else {
				campaignDTO.setReadOnlyCampaign(true);
			}
			/* END READ/WRITE ACCESS TO SUPERVISOR */

			campaignDTOs.add(campaignDTO);
		}
		return new CampaignSearchResult(campaignRepository.countBySupervisorId(userLogin.getUsername(), statuses),
				campaignDTOs);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public SupervisorCampaignResult getSupervisorCampaigns(CampaignSearchDTO campaignSearchDTO) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		Organization org = organizationRepository.findOneById(userLogin.getOrganization());
		List<String> statuses = new ArrayList<String>();
		if (campaignSearchDTO.isFlag()) {
			statuses.add(CampaignStatus.RUNNING.name());
		} else {
			statuses.add(CampaignStatus.RUNNING.name());
			statuses.add(CampaignStatus.PLANNING.name());
		}

		List<Campaign> campaignsFromDB = campaignRepository.findBySupervisorId(userLogin.getUsername(), statuses,
				PageRequest.of(campaignSearchDTO.getPage(), campaignSearchDTO.getSize(),
						campaignSearchDTO.getDirection(), campaignSearchDTO.getSort()));
		List<SupervisorCampaignsVM> supervisorCampaigns = new ArrayList<SupervisorCampaignsVM>();
		if (campaignsFromDB != null && campaignsFromDB.size() > 0) {
			for (Campaign campaign : campaignsFromDB) {
				SupervisorCampaignsVM supervisorCampaignVM = new SupervisorCampaignsVM();
				supervisorCampaignVM.setCampaignId(campaign.getId());
				supervisorCampaignVM.setName(campaign.getName());
				supervisorCampaignVM.setType(campaign.getType().name());
				supervisorCampaignVM.setTimeZone(campaign.getTimezone());
				supervisorCampaignVM.setStatus(campaign.getStatus());
				supervisorCampaignVM.setStartDate(campaign.getStartDate());
				supervisorCampaignVM.setRetrySpeed(campaign.getRetrySpeed());
				supervisorCampaignVM.setCampaignRequirements(campaign.getCampaignRequirements());
				supervisorCampaignVM.setQualificationCriteria(campaign.getQualificationCriteria());
				supervisorCampaignVM.setAbm(campaign.isABM());
				supervisorCampaignVM.setMaxEmployeeAllow(campaign.isMaxEmployeeAllow());
				if (campaign.getTeam().getSupervisorId().equalsIgnoreCase(userLogin.getUsername())) {
					supervisorCampaignVM.setReadOnlyCampaign(false);
				} else {
					supervisorCampaignVM.setReadOnlyCampaign(true);
				}
				if (campaign.getTeam() != null) {
					supervisorCampaignVM.setCallSpeedPerMinPerAgent(campaign.getTeam().getCallSpeedPerMinPerAgent());
					supervisorCampaignVM.setLeadSortOrder(campaign.getTeam().getLeadSortOrder());
				}
				supervisorCampaignVM.setAssetEmailTemplate(campaign.getAssetEmailTemplate());
				supervisorCampaigns.add(supervisorCampaignVM);
			}
		}
		return new SupervisorCampaignResult(campaignRepository.countBySupervisorId(userLogin.getUsername(), statuses),
				supervisorCampaigns,org.isProspectUploadEnabled());
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public CampaignDTO createCampaign(CampaignDTO campaignDTO, HttpServletRequest request) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		CampaignMetaData campaignMetaData = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
		Campaign cam = campaignRepository.findByNameAndOrganizationId(campaignDTO.getName(),
				userLogin.getOrganization());
		if (cam != null) {
			throw new IllegalArgumentException("Campaign " + campaignDTO.getName() + " already exists in the system.");
		}
		Campaign campaign = new Campaign(userLogin.getOrganization(), userLogin.getUsername(), campaignDTO.getName(),
				campaignDTO.getStartDate(), campaignDTO.getEndDate(), campaignDTO.getType(),
				campaignDTO.getQualificationCriteria() != null
						? campaignDTO.getQualificationCriteria().toQualificationCriteria()
						: null,
				campaignDTO.getDeliveryTarget(), campaignDTO.getDomain(), campaignDTO.isSelfManageTeam());
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		/*if (organization != null) {
			campaign.setSimpleDisposition(organization.isSimpleDisposition());
		} else {*/
		//}
		campaign.setDailyCap(campaignDTO.getDailyCap());
		campaign.setBrand(campaignDTO.getBrand());
		campaign.setTimezone(findOrganizationTimeZone(campaign.getOrganizationId()));
		campaign.setRestrictions(campaignDTO.getRestrictions());
		campaign.setInformationCriteria(campaignDTO.getInformationCriteria());
		campaign.setLeadFilteringCriteria(campaignDTO.getLeadFilteringCriteria() == null ? null
				: campaignDTO.getLeadFilteringCriteria().toExpressionGroup());
		if (campaignDTO.getAssets() != null) {
			List<Asset> assets = new ArrayList<Asset>();
			for (AssetDTO assetDTO : campaignDTO.getAssets()) {
				if (assetDTO.getUrl().contains("/")) {
					assetDTO.setUrl(encodeURL(assetDTO.getUrl()));
				}
				assets.add(assetDTO.toAsset());
			}
			campaign.setAssets(assets);
		} else {
			campaign.setAssets(null);
		}
		campaign.setCampaignRequirements(campaignDTO.getCampaignRequirements()); // Added to set campaign requirements
		if (organization.getDefaultDialerMode() != null) {
            if (DialerMode.PREVIEW.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.PREVIEW);
            }
            if (DialerMode.POWER.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.POWER);
            }
            if (DialerMode.HYBRID.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.HYBRID);
            }
            if (DialerMode.RESEARCH.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.RESEARCH);
            }
            if (DialerMode.RESEARCH_AND_DIAL.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.RESEARCH_AND_DIAL);
            }
            if (DialerMode.PREVIEW_SYSTEM.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.PREVIEW_SYSTEM);
            }
            if (DialerMode.MANUALDIAL.equals(DialerMode.valueOf(organization.getDefaultDialerMode()))) {
                campaign.setDialerMode(DialerMode.MANUALDIAL);
            }
        } else {
            campaign.setDialerMode(DialerMode.POWER);
        }

//		campaign.setPartnerSupervisors(getOrganizationPartnerSupervisors(organization));	// Added to give show/hide access to supervisor screen for partner supervisor
		campaign.setAssetEmailTemplate(campaignDTO.getAssetEmailTemplate());
//		if(organization.getHostingServer()!=null && !organization.getHostingServer().isEmpty())
//			campaign.setHostingServer(organization.getHostingServer());
//		else
//			campaign.setHostingServer(trimServerUrl(request));
		
		if(organization.getCampaignDefaultSetting() != null) {
			campaign.setEnableMachineAnsweredDetection(organization.getCampaignDefaultSetting().isEnableMachineAnsweredDetection());
			campaign.setDailyCallMaxRetries(organization.getCampaignDefaultSetting().getDailyCallMaxRetries()); // setting daily call max retry as 1 for new campaign
			campaign.setHostingServer(organization.getCampaignDefaultSetting().getHostingServer());
			campaign.setCallMaxRetries(organization.getCampaignDefaultSetting().getCallMaxRetries()); // setting call max retry as 10 for new campaign
			campaign.setHidePhone(organization.getCampaignDefaultSetting().isHidePhone()); // Added to hide phone number on agent screen from third party center
			campaign.setHideEmail(organization.getCampaignDefaultSetting().isHideEmail()); // Added to hide email number on agent screen from third party center
			campaign.setQaRequired(organization.getCampaignDefaultSetting().isQaRequired());
			campaign.setSipProvider(organization.getCampaignDefaultSetting().getSipProvider());
			campaign.setSupervisorAgentStatusApprove(organization.getCampaignDefaultSetting().isSupervisorAgentStatusApprove());
			campaign.setEnableInternetCheck(organization.getCampaignDefaultSetting().isEnableInternetCheck());
			campaign.setTagSuccess(organization.getCampaignDefaultSetting().isTagSuccess());
			campaign.setRetrySpeedEnabled(organization.getCampaignDefaultSetting().isRetrySpeedEnabled());
			campaign.setLocalOutboundCalling(organization.getCampaignDefaultSetting().isLocalOutboundCalling());
			campaign.setLeadsPerCompany(organization.getCampaignDefaultSetting().getLeadsPerCompany());
			campaign.setDuplicateCompanyDuration(organization.getCampaignDefaultSetting().getDuplicateCompanyDuration());
			campaign.setDuplicateLeadsDuration(organization.getCampaignDefaultSetting().getDuplicateLeadsDuration());
			campaign.setRetrySpeed(organization.getCampaignDefaultSetting().getRetrySpeed());
			campaign.setRecordProspect(organization.getCampaignDefaultSetting().getRecordProspect());
			campaign.setMultiProspectCalling(organization.getCampaignDefaultSetting().isMultiProspectCalling());
			campaign.setEmailSuppressed(organization.getCampaignDefaultSetting().isEmailSuppressed());
			campaign.setEnableCustomFields(organization.getCampaignDefaultSetting().isEnableCustomFields());
			if(organization.getCampaignDefaultSetting().getAutoModeCallProvider() != null && 
					!organization.getCampaignDefaultSetting().getAutoModeCallProvider().isEmpty()
					&& organization.getCampaignDefaultSetting().getManualModeCallProvider() != null && 
							!organization.getCampaignDefaultSetting().getManualModeCallProvider().isEmpty()) {
				campaign.setAutoModeCallProvider(organization.getCampaignDefaultSetting().getAutoModeCallProvider());
				campaign.setManualModeCallProvider(organization.getCampaignDefaultSetting().getManualModeCallProvider());
		}else if(campaignMetaData.getAutoModeCallProvider() != null && 
				!campaignMetaData.getAutoModeCallProvider().isEmpty()
				&& campaignMetaData.getManualModeCallProvider() != null && 
						!campaignMetaData.getManualModeCallProvider().isEmpty()) {
			campaign.setAutoModeCallProvider(campaignMetaData.getAutoModeCallProvider());
			campaign.setManualModeCallProvider(campaignMetaData.getManualModeCallProvider());
		}
			campaign.setCallControlProvider(organization.getCampaignDefaultSetting().getCallControlProvider());
			campaign.setUseSlice(organization.getCampaignDefaultSetting().isUseSlice());
			campaign.setNotConference(organization.getCampaignDefaultSetting().isNotConference());
			campaign.setABM(organization.getCampaignDefaultSetting().isABM());
			campaign.setLeadIQ(organization.getCampaignDefaultSetting().isLeadIQ());
			campaign.setSimpleDisposition(organization.getCampaignDefaultSetting().isSimpleDisposition());
			campaign.setIndirectBuyStarted(organization.getCampaignDefaultSetting().isIndirectBuyStarted());
			campaign.setMdFiveSuppressionCheck(organization.getCampaignDefaultSetting().isMdFiveSuppressionCheck());
			campaign.setCallableEvent(organization.getCampaignDefaultSetting().getCallableEvent());
			campaign.setIgnorePrimaryIndustries(organization.getCampaignDefaultSetting().isIgnorePrimaryIndustries());
			campaign.setPrimaryIndustriesOnly(organization.getCampaignDefaultSetting().isPrimaryIndustriesOnly());
			campaign.setEnableProspectCaching(organization.getCampaignDefaultSetting().isEnableProspectCaching());
			campaign.setProspectCacheAgentRange(organization.getCampaignDefaultSetting().getProspectCacheAgentRange());
			campaign.setProspectCacheNoOfProspectsPerAgent(organization.getCampaignDefaultSetting().getProspectCacheNoOfProspectsPerAgent());
			campaign.setRemoveMobilePhones(organization.getCampaignDefaultSetting().isRemoveMobilePhones());
			campaign.setRevealEmail(organization.getCampaignDefaultSetting().isRevealEmail());
			campaign.setLowerThreshold(organization.getCampaignDefaultSetting().getLowerThreshold());
			campaign.setUpperThreshold(organization.getCampaignDefaultSetting().getUpperThreshold());
			// campaign.setTelnyxHold(organization.getCampaignDefaultSetting().isTelnyxHold());
			campaign.setHideNonSuccessPII(organization.getCampaignDefaultSetting().isHideNonSuccessPII());
			// campaign.setHoldAudioUrl(organization.getCampaignDefaultSetting().getHoldAudioUrl());
			List<KeyValuePair<String, Integer>> orgDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
			if (organization.getCampaignDefaultSetting().getDataSourcePriority() != null && organization.getCampaignDefaultSetting().getDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : organization.getCampaignDefaultSetting().getDataSourcePriority()) {
					orgDataSourcePriority.add(keyValuePair);
				}
				campaign.setDataSourcePriority(orgDataSourcePriority);
			}
			List<KeyValuePair<String, Integer>> orgManualDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
			if (organization.getCampaignDefaultSetting().getManualDataSourcePriority() != null && organization.getCampaignDefaultSetting().getManualDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : organization.getCampaignDefaultSetting().getManualDataSourcePriority()) {
					orgManualDataSourcePriority.add(keyValuePair);
				}
				campaign.setManualDataSourcePriority(orgManualDataSourcePriority);
			}
			List<KeyValuePair<String, Integer>> orgSimilarDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>();
			if (organization.getCampaignDefaultSetting().getSimilarDataSourcePriority() != null && organization.getCampaignDefaultSetting().getSimilarDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : organization.getCampaignDefaultSetting().getSimilarDataSourcePriority()) {
					orgSimilarDataSourcePriority.add(keyValuePair);
				}
				campaign.setSimilarDataSourcePriority(orgSimilarDataSourcePriority);
			}
			campaign.setAutoMachineHangup(organization.getCampaignDefaultSetting().isAutoMachineHangup());
			campaign.setUkPhoneDncCheck(organization.getCampaignDefaultSetting().isUkPhoneDncCheck());
			campaign.setUsPhoneDncCheck(organization.getCampaignDefaultSetting().isUsPhoneDncCheck());
			campaign.setCallableVanished(organization.getCampaignDefaultSetting().isCallableVanished());
			campaign.setEnableCallbackFeature(organization.getCampaignDefaultSetting().isEnableCallbackFeature());
		}else {
			campaign.setEnableMachineAnsweredDetection(campaignMetaData.isEnableMachineAnsweredDetection());
			campaign.setDailyCallMaxRetries(campaignMetaData.getDailyCallMaxRetries()); // setting daily call max retry as 1 for new campaign
			campaign.setHostingServer(campaignMetaData.getHostingServer());
			campaign.setCallMaxRetries(campaignMetaData.getCallMaxRetries()); // setting call max retry as 10 for new campaign
			campaign.setHidePhone(campaignMetaData.isHidePhone()); // Added to hide phone number on agent screen from third party center
			campaign.setHideEmail(campaignMetaData.isHideEmail()); // Added to hide email number on agent screen from third party center
			campaign.setQaRequired(campaignMetaData.isQaRequired());
			campaign.setSipProvider(campaignMetaData.getSipProvider());
			campaign.setSupervisorAgentStatusApprove(campaignMetaData.isSupervisorAgentStatusApprove());
			campaign.setTagSuccess(campaignMetaData.isTagSuccess());
			campaign.setRetrySpeedEnabled(campaignMetaData.isRetrySpeedEnabled());
			campaign.setLocalOutboundCalling(campaignMetaData.isLocalOutboundCalling());
			campaign.setLeadsPerCompany(campaignMetaData.getLeadsPerCompany());
			campaign.setDuplicateCompanyDuration(campaignMetaData.getDuplicateCompanyDuration());
			campaign.setDuplicateLeadsDuration(campaignMetaData.getDuplicateLeadsDuration());
			campaign.setRetrySpeed(campaignMetaData.getRetrySpeed());
			campaign.setEnableInternetCheck(campaignMetaData.isEnableInternetCheck());
			campaign.setRecordProspect(campaignMetaData.getRecordProspect());
			campaign.setMultiProspectCalling(campaignMetaData.isMultiProspectCalling());
			campaign.setEmailSuppressed(campaignMetaData.isEmailSuppressed());
			campaign.setEnableCustomFields(campaignMetaData.isEnableCustomFields());
			if(organization.getCampaignDefaultSetting() != null && organization.getCampaignDefaultSetting().getAutoModeCallProvider() != null && 
					!organization.getCampaignDefaultSetting().getAutoModeCallProvider().isEmpty()
					&& organization.getCampaignDefaultSetting().getManualModeCallProvider() != null && 
							!organization.getCampaignDefaultSetting().getManualModeCallProvider().isEmpty()) {
				campaign.setAutoModeCallProvider(organization.getCampaignDefaultSetting().getAutoModeCallProvider());
				campaign.setManualModeCallProvider(organization.getCampaignDefaultSetting().getManualModeCallProvider());
			}else if(campaignMetaData.getAutoModeCallProvider() != null && 
				!campaignMetaData.getAutoModeCallProvider().isEmpty()
				&& campaignMetaData.getManualModeCallProvider() != null && 
						!campaignMetaData.getManualModeCallProvider().isEmpty()) {
				campaign.setAutoModeCallProvider(campaignMetaData.getAutoModeCallProvider());
				campaign.setManualModeCallProvider(campaignMetaData.getManualModeCallProvider());
			}
			campaign.setCallControlProvider(campaignMetaData.getCallControlProvider());
			campaign.setUseSlice(campaignMetaData.isUseSlice());
			campaign.setNotConference(campaignMetaData.isNotConference());
			campaign.setABM(campaignMetaData.isABM());
			campaign.setLeadIQ(campaignMetaData.isLeadIQ());
			campaign.setIgnorePrimaryIndustries(campaignMetaData.isIgnorePrimaryIndustries());
			campaign.setPrimaryIndustriesOnly(campaignMetaData.isPrimaryIndustriesOnly());
			campaign.setSimpleDisposition(campaignMetaData.isSimpleDisposition());
			campaign.setIndirectBuyStarted(campaignMetaData.isIndirectBuyStarted());
			campaign.setMdFiveSuppressionCheck(campaignMetaData.isMdFiveSuppressionCheck());
			campaign.setCallableEvent(campaignMetaData.getCallableEvent());
			campaign.setAutoMachineHangup(campaignMetaData.isAutoMachineHangup());
			campaign.setEnableProspectCaching(campaignMetaData.isEnableProspectCaching());
			campaign.setProspectCacheAgentRange(campaignMetaData.getProspectCacheAgentRange());
			campaign.setProspectCacheNoOfProspectsPerAgent(campaignMetaData.getProspectCacheNoOfProspectsPerAgent());
			campaign.setRemoveMobilePhones(campaignMetaData.isRemoveMobilePhones());
			campaign.setRevealEmail(campaignMetaData.isRevealEmail());
			campaign.setLowerThreshold(campaignMetaData.getLowerThreshold());
			campaign.setUpperThreshold(campaignMetaData.getUpperThreshold());
			// campaign.setTelnyxHold(campaignMetaData.isTelnyxHold());
			campaign.setHideNonSuccessPII(campaignMetaData.isHideNonSuccessPII());
			// campaign.setHoldAudioUrl(campaignMetaData.getHoldAudioUrl());
			campaign.setUkPhoneDncCheck(campaignMetaData.isUkPhoneDncCheck());
			campaign.setUsPhoneDncCheck(campaignMetaData.isUsPhoneDncCheck());
			campaign.setCallableVanished(campaignMetaData.isCallableVanished());
			campaign.setEnableCallbackFeature(campaignMetaData.isEnableCallbackFeature());
			List<KeyValuePair<String, Integer>> metaDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
			if (campaignMetaData.getDataSourcePriority() != null && campaignMetaData.getDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : campaignMetaData.getDataSourcePriority()) {
					metaDataSourcePriority.add(keyValuePair);
				}
				campaign.setDataSourcePriority(metaDataSourcePriority);
			}

			List<KeyValuePair<String, Integer>> metaManualSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>();
			if (campaignMetaData.getSimilarDataSourcePriority() != null && campaignMetaData.getSimilarDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : campaignMetaData.getSimilarDataSourcePriority()) {
					metaManualSourcePriority.add(keyValuePair);
				}
				campaign.setSimilarDataSourcePriority(metaDataSourcePriority);
			}
			
			List<KeyValuePair<String, Integer>> metaManualDataSourcePriority =  new ArrayList<KeyValuePair<String,Integer>>(); 
			if (campaignMetaData.getManualDataSourcePriority() != null && campaignMetaData.getManualDataSourcePriority().size() > 0) {
				for (KeyValuePair<String,Integer> keyValuePair : campaignMetaData.getManualDataSourcePriority()) {
					metaManualDataSourcePriority.add(keyValuePair);
				}
				campaign.setManualDataSourcePriority(metaManualDataSourcePriority);
			}
		}
		if (campaign.getType() != null && campaign.getType().equals(CampaignTypes.Prospecting)) {
			campaign.setLeadIQ(false);
		}
		campaign.setTelnyxHold(true);
		campaign.setHoldAudioUrl("https://xtaasuploadfiles.s3-us-west-2.amazonaws.com/Silent.mp3");
		campaign.setRingingIcon(false);
		campaign.setUnHideSocialLink(false);
		campaign.setCallAbandon(false);
		campaign.setSimilarProspect(false);
		campaign.setEnableStateCodeDropdown(false);
		campaign.setMaxEmployeeAllow(false); // default false it will set retrycount 144 whose employee is 0 or greater then or equal 10000
		//campaign.setRemoveMobilePhones(false);
		campaign = campaignRepository.save(campaign);
		CampaignDNorm cdn = new CampaignDNorm();
		cdn = cdn.toCampaignDNorm(campaign, cdn);
		campaignDNormRepository.save(cdn);
		return new CampaignDTO(campaign);
	}






//	@Override
//	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})











	private String trimServerUrl(HttpServletRequest request) {
		String url = request.getRequestURL().toString();
		int index = url.indexOf("/spr");
		if (index >= 0) {
			url = url.substring(0, index);
		}
		index = url.indexOf("http://");
		if (index >= 0) {
			url = url.substring(7, url.length());
		} else {
			index = url.indexOf("https://");
			if (index >= 0) {
				url = url.substring(8, url.length());
			}
		}
		return url;
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public CampaignDTO editCampaign(CampaignDTO campaignDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignDTO.getId());

		Campaign cmp = campaignRepository.findByNameAndOrganizationId(campaignDTO.getName(),campaign.getOrganizationId());
		if (cmp != null && !campaign.getName().equals(cmp.getName())) {
			throw new IllegalArgumentException("Campaign "+campaignDTO.getName()+" already exists in the system.");
		}
		campaign.setName(campaignDTO.getName());
		campaign.setRunDates(campaignDTO.getStartDate(), campaignDTO.getEndDate());
		campaign.setDeliveryTarget(campaignDTO.getDeliveryTarget());
		campaign.setDomain(campaignDTO.getDomain());
		campaign.setBrand(campaignDTO.getBrand());
		campaign.setTimezone(findOrganizationTimeZone(campaign.getOrganizationId()));
//		campaign.setEnableMachineAnsweredDetection(isMachineDetection(campaign.getOrganizationId()));
		if (campaign.getStatus() == CampaignStatus.CREATED) {
			campaign.setType(campaignDTO.getType());
			if (campaign.isSelfManageTeam() != campaignDTO.isSelfManageTeam()) { //if there is change in self manage flag then change the team structure
				if (campaignDTO.isSelfManageTeam()) {
					campaign.setTeam(new CampaignTeam("self", campaign.getCampaignManagerId(), 0));
					campaign.setTeamInvitations(null);
					campaign.setAgentInvitations(new ArrayList<CampaignAgentInvitation>());
				} else {
					campaign.setTeam(null);
					campaign.setAgentInvitations(null);
					campaign.setTeamInvitations(new ArrayList<CampaignTeamInvitation>());
				}
			}
			campaign.setSelfManageTeam(campaignDTO.isSelfManageTeam());
		}
		//if (campaign.getType() == CampaignTypes.LeadQualification) {
			campaign.setQualificationCriteria(campaignDTO.getQualificationCriteria().toQualificationCriteria());
		//}
		campaign.setDailyCap(campaignDTO.getDailyCap());
		campaign.setRestrictions(campaignDTO.getRestrictions());
		campaign.setInformationCriteria(campaignDTO.getInformationCriteria());
		campaign.setLeadFilteringCriteria(campaignDTO.getLeadFilteringCriteria() == null ? null : campaignDTO
				.getLeadFilteringCriteria().toExpressionGroup());
		if (campaignDTO.getAssets() != null) {
			List<Asset> assets = new ArrayList<Asset>();
			for (AssetDTO assetDTO : campaignDTO.getAssets()) {
			    if(assetDTO.getUrl().contains("/")){
	                assetDTO.setUrl(encodeURL(assetDTO.getUrl()));
			    }
				assets.add(assetDTO.toAsset());
			}
			campaign.setAssets(assets);
		} else {
			campaign.setAssets(null);
		}
		if (campaign.getDialerMode() == null) { //Fix XP-986 : Not setting DialerMode if already set.
			campaign.setDialerMode(DialerMode.POWER); //XP-1033 : Default to POWER mode
		}
		campaign.setCampaignRequirements(campaignDTO.getCampaignRequirements());	// Added to set campaign requirements
		// campaign.setHidePhone(campaignDTO.isHidePhone());	// Added to hide phone number on agent screen from third party center
		campaign.setPartnerSupervisors(campaignDTO.getPartnerSupervisors());	// Added to give show/hide access to supervisor screen for partner supervisor
		campaign.setClientName(campaignDTO.getClientName());
		if (campaignDTO.getClientName() != null && !campaignDTO.getClientName().isEmpty() && campaign.getClientName() != null && !campaign.getClientName().isEmpty()) {
			List<NewClientMapping> clientMappings = newClientMappingRepository.findClientMappingByCampaignId(campaign.getId());
			List<NewClientMapping> modifiedClientMappings = new ArrayList<NewClientMapping>(); 
			if (clientMappings != null && clientMappings.size() > 0) {
				String clientNameFromDB = clientMappings.get(0).getClientName();
				if (clientNameFromDB != null && !clientNameFromDB .equalsIgnoreCase(campaignDTO.getClientName())) {
					for (NewClientMapping clientMapping : clientMappings) {
						NewClientMapping newMapping = new NewClientMapping();
						newMapping.setId(clientMapping.getId());
						newMapping.setCampaignId(clientMapping.getCampaignId());
						newMapping.setClientName(campaignDTO.getClientName());
						newMapping.setStdAttribute(clientMapping.getStdAttribute());
						newMapping.setSequence(clientMapping.getSequence());
						newMapping.setColHeader(clientMapping.getColHeader());
						newMapping.setClientValues(clientMapping.getClientValues());
						modifiedClientMappings.add(newMapping);
					}
					clientMappings = modifiedClientMappings;
					newClientMappingRepository.saveAll(clientMappings);
				}
			}
		} else {
			campaign.setClientName(campaignDTO.getClientName());
		}
		campaign.setDuplicateCompanyDuration(campaignDTO.getDuplicateCompanyDuration());
		campaign.setDuplicateLeadsDuration(campaignDTO.getDuplicateLeadsDuration());
		campaign.setLeadsPerCompany(campaignDTO.getLeadsPerCompany());
		campaign.setAdminSupervisorIds(campaignDTO.getAdminSupervisorIds());
		campaign.setCostPerLead(campaignDTO.getCostPerLead());
		campaign.setBandwidthCheck(campaignDTO.isBandwidthCheck());
		campaign.setNetworkCheck(campaignDTO.isNetworkCheck());
		campaign.setAutoMachineHangup(campaignDTO.isAutoMachineHangup());
		campaign.setPlivoAMDTime(campaignDTO.getPlivoAMDTime());
		campaign.setCallAbandon(campaignDTO.isCallAbandon());
		campaign.setEnableCustomFields(campaignDTO.isEnableCustomFields());
		campaign.setEnableInternetCheck(campaignDTO.isEnableInternetCheck());
		campaign.setCallableVanished(campaignDTO.isCallableVanished());
		campaign.setEnableCallbackFeature(campaignDTO.isEnableCallbackFeature());
		campaign.setEnableStateCodeDropdown(campaignDTO.isEnableStateCodeDropdown());
		//campaign.setRemoveMobilePhones(campaignDTO.isRemoveMobilePhones());
		campaign = campaignRepository.save(campaign);
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);

		return new CampaignDTO(campaignRepository.save(campaign));
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void inviteAgent(String campaignId, AgentInvitationDTO agentInvitationDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.addAgentInvitations(agentInvitationDTO.toCampaignAgentInvitation());
		campaignRepository.save(campaign);
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
		List<CampaignAgentInvitation> agentInvitations = campaign.getAgentInvitations();
		if(!agentInvitations.isEmpty()) {
			for(CampaignAgentInvitation campaignAgentInvitation : agentInvitations) {
				String agentId = campaignAgentInvitation.getAgentId();
				notifyWorkroom(campaign, "AGENT", agentId);
			}
		}
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void inviteTeam(String campaignId, List<String> teamIds) {
		if (teamIds.size() > 3) {
			throw new IllegalArgumentException("At max only three teams can be invited for bidding");
		}
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<String> error = validateCampaign(campaign);
		if (!error.isEmpty()) {
			throw new IllegalArgumentException("Invitation cannot be sent as some of the required information is missing : "+StringUtils.join(error, ","));
		}
		for (String teamId : teamIds) {
			campaign.addTeamInvitations(new CampaignTeamInvitation(teamId));

		}
		campaign.setTeamsShortlisted(null);
		campaignRepository.save(campaign);
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
		/*List<CampaignTeamInvitation> teamInvitations = campaign.getTeamInvitations();
		if(!teamInvitations.isEmpty()) {
			for(CampaignTeamInvitation campaignTeamInvitation : teamInvitations) {
				String teamId = campaignTeamInvitation.getTeamId();
				notifyWorkroom(campaign, "TEAM", teamId);
			}
		}*/
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void removeInviteTeam(String campaignId, String teamId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.removeTeamInvited(teamId);
		campaignRepository.save(campaign);
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	private List<String> validateCampaign(Campaign campaign) {
		List<String> absentCampaignFields = new ArrayList<String>();
		if (campaign.getType() == null) {
			absentCampaignFields.add("Campaign Type");
		}
		if (campaign.getStartDate() == null) {
			absentCampaignFields.add("Start Date");
		}
		if (campaign.getEndDate() == null) {
			absentCampaignFields.add("End Date");
		}
		if (campaign.getDeliveryTarget() <= 0) {
			absentCampaignFields.add("Delivery Target");
		}
		/*if (campaign.getDailyCap() <= 0) {
			absentCampaignFields.add("Daily Cap");
		}*/
		return absentCampaignFields;
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void shortlistTeam(String campaignId, CampaignTeamShortlistedDTO campaignTeamShortlistedDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.addTeamShortlisted(campaignTeamShortlistedDTO.toCampaignTeamShortlisted());
		campaignRepository.save(campaign);
		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	private void notifyWorkroom(Campaign campaign, String type, String invitee) {
		//set headers
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("token", getAuthToken());
		headers.put("content-type", "application/json"); //content-type: application/json
		//set CampaignWorkroomDTO
		CampaignWorkroomDTO campaignWorkroomDTO = new CampaignWorkroomDTO();
		campaignWorkroomDTO.setCampaignId(campaign.getId());
		campaignWorkroomDTO.setCampaignManagerId(campaign.getCampaignManagerId());
		campaignWorkroomDTO.setCampaignType(type);
		campaignWorkroomDTO.setInvitee(invitee);

		try {
			restClientService.makePostRequest(getWorkroomServiceUrl(), headers, JSONUtils.toJson(campaignWorkroomDTO));
		} catch (IOException e) {
			logger.error("beforeStep() : Error occurred while making a rest call to Email Notifier Service", e);
		}
	}

	private String getAuthToken() {
	    return authTokenGeneratorService.generateSystemToken(App.BACKEND_SUPPORT_SYSTEM.name(), "batch");
	}

	private String getWorkroomServiceUrl() {
	   	StringBuilder bssServerBaseUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getBssServerBaseUrl()).append("/api/campaign/invite");
	   	return bssServerBaseUrl.toString();
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public void setCallGuide(String campaignId, CallGuideDTO callGuide) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<Team> teamList = teamRepository.findBySupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
		if (teamList != null && teamList.size() > 0) {
			for (Team team : teamList) {
				if (team.getAgents() != null && team.getAgents().size() > 0) {
					for (TeamResource agentTeam : team.getAgents()) {
						if (agentTeam.getSupervisorOverrideCampaignId() != null
								&& agentTeam.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
							PusherUtils.pushMessageToUser(agentTeam.getResourceId(), XtaasConstants.SCRIPT_CHANGE,
							"Script has been changed, please check with your supervisor.");
						}
					}
				}
			}
		}
		campaign.setCallGuide(callGuide.toCallGuide());
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public CallGuideDTO getCallGuide(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for finding the call guide");
		}
		if (campaign.getCallGuide() == null) return null;
		return new CallGuideDTO(campaign.getCallGuide());
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public List<TeamSearchResultDTO> listTeamInvitations(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Team invitations are not available for self managed campaigns");
		}

		List<CampaignTeamInvitation> teamInvitations = campaign.getTeamInvitations();
		List<String> teamInvitationsTeamIds = new ArrayList<String>(teamInvitations.size());
		for (CampaignTeamInvitation campaignTeamInvitation : teamInvitations) {
			teamInvitationsTeamIds.add(campaignTeamInvitation.getTeamId());
		}
		return teamRepository.findByIds(teamInvitationsTeamIds);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public List<TeamSearchResultDTO>  listTeamsShortlisted(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Team shortlisted are not available for self managed campaigns");
		}

		List<CampaignTeamShortlisted> campaignTeamsShortlisted = campaign.getTeamsShortlisted();
		List<String> teamShortlistedTeamIds = new ArrayList<String>(campaignTeamsShortlisted.size());
		for (CampaignTeamShortlisted campaignTeamShortlisted : campaignTeamsShortlisted) {
			teamShortlistedTeamIds.add(campaignTeamShortlisted.getTeamId());
		}
		return teamRepository.findByIds(teamShortlistedTeamIds);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void cancelTeamsShortlisted(String campaignId, String teamId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Team shortlisted are not available for self managed campaigns");
		}
		campaign.removeTeamShortlisted(teamId);
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public List<AgentInvitationDTO> listAgentInvitations(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (!campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Agent invitations are not available for supervisor managed campaigns");
		}
		List<CampaignAgentInvitation> agentInvitations = campaign.getAgentInvitations();
		List<AgentInvitationDTO> agentInvitationDTOs = new ArrayList<AgentInvitationDTO>();
		for (CampaignAgentInvitation agentInvitation : agentInvitations) {
			agentInvitationDTOs.add(new AgentInvitationDTO(agentInvitation));
		}
		return agentInvitationDTOs;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public void chooseTeam(String campaignId, CampaignTeamDTO campaignTeamDTO) {
		Login login = new Login();
		Team team = null;
		Campaign campaign = campaignRepository.findOneById(campaignId);
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		CampaignMetaData campaignMetaData = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
		if (campaignTeamDTO.getSupervisorIds()!=null && campaignTeamDTO.getSupervisorIds().size()>0) {
			login = loginRepository.findUser(campaignTeamDTO.getSupervisorIds().get(0));
			//team = teams.get(0);
			List<Team> mainTeams = teamRepository.findByAdminSupervisorAndSupervisorId(login.getId());
			if(mainTeams!=null && mainTeams.size()>0) {
				for(Team t : mainTeams) {
					if(t.getSupervisor().getResourceId().equalsIgnoreCase(login.getId())) {
						team =t;
						break;
					}
				}
			}
		} else {
			throw new IllegalArgumentException("There is no Team under this Supervisor ");
		}
		List<String> error = validateCampaign(campaign);

		List<Team> teams = new ArrayList<Team>();
		if (!error.isEmpty()) {
			throw new IllegalArgumentException(
					"Team selection is not allow as some of the required information is missing : "
							+ StringUtils.join(error, ","));
		}
		if (campaignTeamDTO.getTeamId() != null) {
			team = teamRepository.findOneById(campaignTeamDTO.getTeamId());
		}
		if (campaignTeamDTO.getSupervisorId() != null) {
			/*if (XtaasUserUtils.getCurrentLoggedInUsersOrganization().getName().equals("XTAAS CALL CENTER")) {
				teams = teamRepository.findByAdminSupervisorAndSupervisorId(campaignTeamDTO.getSupervisorId());
			} else {*/
				teams = teamRepository.findByAdminSupervisorAndSupervisorId(login.getId());
			//}
		}
		if (teams == null && teams.size() == 0) {
			throw new IllegalArgumentException("There is no Team under this Supervisor ");
		}
		String teamSup= "";
		String teamId = "";
		//team = teams.get(0);
		if (campaignTeamDTO.getSupervisorIds() != null && campaignTeamDTO.getSupervisorIds().size() > 0) {
			campaign.setAdminSupervisorIds(campaignTeamDTO.getSupervisorIds());
			List<Team> multiTeams = teamRepository.findByAdminSupervisorAndSupervisorId(campaignTeamDTO.getSupervisorIds());
			List<String> supervisors = multiTeams.stream().distinct().map(t -> t.getSupervisor().getResourceId())
					.collect(Collectors.toList());
			//campaignTeamDTO.getSupervisorIds().addAll(supervisors);
			campaignTeamDTO.setSupervisorIds(supervisors);

			campaign.setPartnerSupervisors(supervisors);

			String adminsup = campaignTeamDTO.getSupervisorIds().get(0);
			Team asup = teamRepository.findTeamBySupervisorId(adminsup);
			teamSup = asup.getSupervisor().getResourceId();
			teamId= asup.getId();

			// if (campaign.getPartnerSupervisors() != null && campaign.getPartnerSupervisors().size() > 0) {
			// for (String partnrId : campaign.getPartnerSupervisors()) {
			// if (!campaignTeamDTO.getSupervisorIds().contains(partnrId)) {
			// newSupervisors.add(partnrId);
			// }
			// }
			// campaign.getPartnerSupervisors().removeAll(newSupervisors);
			// campaign.setPartnerSupervisors(campaignTeamDTO.getSupervisorIds());
			// } else {
			// campaign.setPartnerSupervisors(campaignTeamDTO.getSupervisorIds());
			// }
			for (String newSupervisor : supervisors) {
				Team newTeam = null;
				newTeam = teamRepository.findTeamBySupervisorId(newSupervisor);
				if (newTeam != null && newTeam.getAgents() != null && newTeam.getAgents().size() > 0) {
					/*if(teamSup.isEmpty() && teamId.isEmpty()) {
	teamSup = newTeam.getSupervisor().getResourceId();
	teamId= newTeam.getId();
	}*/
					for (TeamResource agent : newTeam.getAgents()) {
						if (agent.getSupervisorOverrideCampaignId() != null
								&& agent.getSupervisorOverrideCampaignId().equals(campaign.getId())) {
							agent.setSupervisorOverrideCampaignId(null);
						}
					}
				}
				if (newTeam != null) {
					teamRepository.save(newTeam);
				}
			}
		}else {
			teamSup = team.getSupervisor().getResourceId();
			teamId = team.getId();
		}
		if (team ==null) {
			throw new IllegalArgumentException("There is no Team under this Supervisor ");
		}
		if (team!=null && team.getId()!=null && team.getSupervisor().getResourceId()!=null) {
			if(organization != null && organization.getCampaignDefaultSetting() != null) {
				campaignTeamDTO.setCallSpeedPerMinPerAgent(organization.getCampaignDefaultSetting().getCallSpeedPerMinPerAgent());
				campaignTeamDTO.setCampaignSpeed(organization.getCampaignDefaultSetting().getCampaignSpeed());
				campaignTeamDTO.setLeadSortOrder(organization.getCampaignDefaultSetting().getLeadSortOrder());
			}else {
				campaignTeamDTO.setCallSpeedPerMinPerAgent(campaignMetaData.getCallSpeedPerMinPerAgent());
				campaignTeamDTO.setCampaignSpeed(campaignMetaData.getCampaignSpeed());
				campaignTeamDTO.setLeadSortOrder(campaignMetaData.getLeadSortOrder());
			}
			campaign.chooseTeam(team.getId(), team.getSupervisor().getResourceId(), campaignTeamDTO);
		}
		campaignRepository.save(campaign);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public void updateTeam(String campaignId, CampaignTeamDTO campaignTeamDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);

		if (campaignTeamDTO.getLeadSortOrder() != null) {
			campaign.setLeadSortOrder(campaignTeamDTO.getLeadSortOrder());
		}
		if (campaignTeamDTO.getCallSpeedPerMinPerAgent() != null) {
			campaign.setCallSpeedPerMinPerAgent(campaignTeamDTO.getCallSpeedPerMinPerAgent());
		}
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public CampaignTeamDTO getTeam(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for finding the campaign team");
		}
		return new CampaignTeamDTO(campaign.getTeam());
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public void allocateAgent(String campaignId, String agentId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.allocateAgent(agentId);
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public WorkScheduleDTO getWorkSchedule(String campaignId, String agentId, String scheduleId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		return new WorkScheduleDTO(campaign.getWorkSchedule(agentId, scheduleId));
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public void addWorkSchedule(String campaignId, String agentId, WorkScheduleDTO workScheduleDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.addWorkSchedule(agentId, workScheduleDTO.toWorkSchedule());
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public void updateWorkSchedule(String campaignId, String agentId,
			String scheduleId, WorkScheduleDTO workScheduleDTO) {
		//remove the existing scheduleId campaign and create the new schedule
		Campaign workScheduleCampaign = campaignRepository.findByWorkSchedule(scheduleId);
		if (workScheduleCampaign != null) {
			workScheduleCampaign.removeWorkSchedule(agentId, scheduleId);
			campaignRepository.save(workScheduleCampaign);

			CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(workScheduleCampaign.getId());
			if (cdn == null) {
				cdn = new CampaignDNorm();
			}
			cdn = cdn.toCampaignDNorm(workScheduleCampaign,cdn);
			campaignDNormRepository.save(cdn);
		}
		Campaign campaign = campaignRepository.findOneById(campaignId);
		try {
			campaign.allocateAgent(agentId); //TODO: Remove this when campaign allocation is done separately
		} catch (IllegalArgumentException iaex) {
			logger.error("updateWorkSchedule() : AgentId [{}] already part of CampaignId [{}]", agentId, campaignId);
		}
		campaign.updateWorkSchedule(agentId, scheduleId, workScheduleDTO.toWorkSchedule());
		campaignRepository.save(campaign);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR})
	public void deleteWorkSchedule(String campaignId, String agentId, String scheduleId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.removeWorkSchedule(agentId, scheduleId);
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT, Roles.ROLE_SUPERVISOR, Roles.ROLE_PARTNER})
	public void deallocateAgent(String campaignId, String agentId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.deallocateAgent(agentId);
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT})
	public void runCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.runCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT, Roles.ROLE_SUPERVISOR})
	public void pauseCamaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<Team> teams = teamRepository.findBySupervisorOverrideCampaignId(campaignId);
		if (teams != null && teams.size() > 0) {
			for (Team team : teams) {
				/*
				 * traverse through each agent in each team and find out campaign is allocated
				 * to that agent. If yes then set SupervisorOverrideCampaignId as NULL
				 */
				for (TeamResource agent : team.getAgents()) {
					if (agent.getSupervisorOverrideCampaignId() != null
							&& campaignId.equals(agent.getSupervisorOverrideCampaignId())) {
						agent.setSupervisorOverrideCampaignId(null);
					}
				}
				teamRepository.save(team);
			}
		}
		campaign.pauseCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT})
	public void stopCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.stopCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT})
	public void resumeCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.resumeCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_CLIENT, Roles.ROLE_SUPERVISOR, Roles.ROLE_PARTNER})
	 public void activateAsset(ActiveAssetDTO activeAssetDTO) {
		ArrayList<String> assetIds = new ArrayList<>();
	  Campaign campaign = campaignRepository.findOneById(activeAssetDTO.getCampaignId());
		for (HashMap<String, Integer> activeAsset : activeAssetDTO.getAssetTargetMap()) {
			for (Asset asset : campaign.getAssets()) {
				if (activeAsset.containsKey(asset.getAssetId())) {
					assetIds.add(asset.getAssetId());
					if (activeAsset.get(asset.getAssetId()) != null) {
						asset.setAssetTarget(activeAsset.get(asset.getAssetId()));
						break;
					} else {
						asset.setAssetTarget(0);
					}
				}
			}
		}
	  campaign.activateAssets(assetIds);
	  campaignRepository.save(campaign);

	  	CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
	  	if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	 }

	@Override
	public void planCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.planCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
		XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com",
				campaign.getName()+" has been assigned to PLANNING.",
				campaign.getOrganizationId()+ " has assigned the campaign name"+ campaign.getName()
				+" to PLANNING. Please Proceed with further process.");
	}

	@Override
    public void completeCampaign(String campaignId) {
        Campaign campaign = campaignRepository.findOneById(campaignId);
        List<Team> teams = teamRepository.findBySupervisorOverrideCampaignId(campaignId);
		if (teams != null && teams.size() > 0) {
			for (Team team : teams) {
				/*
				 * traverse through each agent in each team and find out campaign is allocated
				 * to that agent. If yes then set SupervisorOverrideCampaignId as NULL
				 */
				for (TeamResource agent : team.getAgents()) {
					if (agent.getSupervisorOverrideCampaignId() != null
							&& campaignId.equals(agent.getSupervisorOverrideCampaignId())) {
						agent.setSupervisorOverrideCampaignId(null);
					}
				}
				teamRepository.save(team);
			}
		}
        campaign.completeCampaign();
        campaignRepository.save(campaign);

        CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
        if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
    }

	@Override
	public void delistCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.delistCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	public void runReadyCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.runReadyCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	public void runningCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		campaign.runningCampaign();
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	public String deliverActiveAsset(ProspectCall prospectCall) {
		Prospect prospect = prospectCall.getProspect();
		String prospectInteractionSessionId = prospectCall.getProspectInteractionSessionId();
		Campaign campaign = campaignRepository.findOneById(prospectCall.getCampaignId());
		Team team = teamRepository.findOneById(campaign.getTeam().getTeamId());
		Partner partner = partnerRepository.findOneById(team.getPartnerId());
		String assetId = prospectCall.getDeliveredAssetId();
		Asset activeAsset = null;
		List<Asset> activeAssets = campaign.getActiveAsset();
		if (activeAssets != null) {
			for (Asset asset2 : activeAssets) {
				if (assetId != null) {
					if (assetId.equals(asset2.getAssetId())) {
						activeAsset = asset2;
						break;
					}
				} else {
					activeAsset = asset2;
					break;
				}
			}
		}
		EmailMessage emailMessageTemplate = campaign.getAssetEmailTemplate();
		if (activeAsset == null) return null;
		String msgBody;
		if (emailMessageTemplate == null) {
			//use a default template
			msgBody = "Hi, As per our conversation please access the white paper at [ASSET_URL]";
			emailMessageTemplate = new EmailMessage(activeAsset.getName(), msgBody);
		}
		String emailAddress = prospect.getEmail();
		List<String> to = new ArrayList<String>();
		to.add(emailAddress);
		if(partner != null && partner.getAssetDeliveryEmail() != null){
		    to.add(partner.getAssetDeliveryEmail());
		} else {
		    to.add("assets@xtaascorp.com");
		}
		if (emailMessageTemplate.getReplyTo() != null && !to.contains(emailMessageTemplate.getReplyTo())) {
			to.add(emailMessageTemplate.getReplyTo());
		}
		//Replace tags with Values
		msgBody = emailMessageTemplate.getMessage();
		////// TODO remove this code in future, as this is one of case where we are sharing 2 assets at a time. once the campaign is gone, remove it
		String url1= "https://s3.amazonaws.com/xtaas-assets/591e1870e4b0931b8f03ffee/Internet+of+Things+and+the+Rise+of+300+Gbps+DDoS+Attacks.pdf";
		String url2 ="https://s3.amazonaws.com/xtaas-assets/591e1870e4b0931b8f03ffee/Bots+to+the+Future.pdf";

		String url1name = "Internet of Things and the Rise of 300 Gbps DDoS Attacks";
		String url2name = "Bots to the Future";
		if(prospectCall.getCampaignId().equalsIgnoreCase("591e1870e4b0931b8f03ffee")){
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL1]", "<a href='" + url1 + "'>" + url1name + "</a>");
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL2]", "<a href='" + url2 + "'>" + url2name + "</a>");
		}else{
			msgBody = StringUtils.replace(msgBody, "[ASSET_URL]", "<a href='" + activeAsset.getUrl() + "'>" + activeAsset.getName() + "</a>");
		}
		msgBody = StringUtils.replace(msgBody, "[PROSPECT_NAME]", prospect.getFirstName());
		EmailMessage message = new EmailMessage(to, emailMessageTemplate.getSubject(), msgBody);
		if (emailMessageTemplate.getFromEmail() != null) {
			message.setFromEmail(emailMessageTemplate.getFromEmail());
		} else {
			message.setFromEmail("support+asset@xtaascorp.com");
		}
		if (emailMessageTemplate.getFromName() != null) message.setFromName(emailMessageTemplate.getFromName());
		if (emailMessageTemplate.getReplyTo() != null) message.setReplyTo(emailMessageTemplate.getReplyTo());
		//send email
		// XtaasEmailUtils.sendEmail(message,prospectInteractionSessionId);

		/*
		 * DATE : 07/11/2017
		 * REASON : send disposition success mail via sendgrid */
        try {
        	if(campaign.getOrganizationId().equals("XTAAS CALL CENTER")) {
        		XtaasEmailUtils.sendEmailSendGrid(message, prospectInteractionSessionId, prospectCall.getCampaignId(), prospectCall.getProspectCallId());
        	}else {
        		XtaasEmailUtils.sendGridEmailCampaign(message, prospectInteractionSessionId, prospectCall.getCampaignId(), prospectCall.getProspectCallId());
        	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("deliverActiveAsset() : Asset [{}] delivered to Email address [{}] ", activeAsset.getAssetId(), emailAddress);
		return activeAsset.getAssetId();
	}

	/* START
	 * REASON : Testing Asset Email Template */
	public String testAssetEmailTemplate(String campaignId, EmailMessageDTO emailMessageDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<Asset> activeAssets = campaign.getActiveAsset();
		EmailMessage emailMessageTemplate = emailMessageDTO.toEmailMessage();
		if (activeAssets == null || activeAssets.isEmpty()) {
			Asset activeAsset = new Asset("12345",campaign.getOrganizationId(),campaign.getName(), campaign.getName(), false);
			List<Asset> tempAssets =new  ArrayList<Asset>();
			tempAssets.add(activeAsset);
			activeAssets = tempAssets;
		}
		Asset activeAsset = activeAssets.get(0);
		String msgBody;
		if (emailMessageTemplate == null) {
			//use a default template
			msgBody = "Hi, As per our conversation please access the white paper at [ASSET_URL]";
			emailMessageTemplate = new EmailMessage(activeAsset.getName(), msgBody);
		}
		String emailAddress = emailMessageDTO.getReplyTo();
		List<String> to = new ArrayList<String>();
		if(!emailAddress.isEmpty()){
			to.add(emailAddress); //to.add(partner.getAssetDeliveryEmail());
		}else{
		    to.add("assets@xtaascorp.com");
		}
		//Replace tags with Values
		msgBody = emailMessageTemplate.getMessage();
		ProspectDTO prospect = demoProspect();	// using Dummy Prospect
		msgBody = StringUtils.replace(msgBody, "[ASSET_URL]", "<a href='" + activeAsset.getUrl() + "'>" + activeAsset.getName() + "</a>");
		msgBody = StringUtils.replace(msgBody, "[PROSPECT_NAME]", prospect.getFirstName());

		EmailMessage message = new EmailMessage(to, emailMessageTemplate.getSubject(), msgBody);
		if (emailMessageTemplate.getFromEmail() != null) {
			message.setFromEmail(emailMessageTemplate.getFromEmail());
		} else {
			message.setFromEmail("support+asset@xtaascorp.com");
		}
		if (emailMessageTemplate.getFromName() != null) message.setFromName(emailMessageTemplate.getFromName());
		if (emailMessageTemplate.getReplyTo() != null) message.setReplyTo(emailMessageTemplate.getReplyTo());
		// send email
		// XtaasEmailUtils.sendEmail(message, "");

		/*
		 * DATE : 07/11/2017
		 * REASON : send asset test mail via sendgrid from supervisor screen */
		try {
			// if(campaign.getOrganizationId().equals("XTAAS CALL CENTER")) {
			XtaasEmailUtils.sendGridEmailCampaign(message, "", "", "");
			// }else {
			// 	XtaasEmailUtils.sendEmailSendGrid(message, "", "", "");
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("deliverActiveAsset() : Asset [{}] delivered to Email address [{}] ", activeAsset.getAssetId(), emailAddress);
		return activeAsset.getAssetId();
	}

	// creating dummy Prospect for testing email template
	public ProspectDTO demoProspect() {
		ProspectDTO prospectDTO = new ProspectDTO();
		prospectDTO.setFirstName("First_Name");
		prospectDTO.setLastName("Last_Name");
		prospectDTO.setEmail("support@xtaascorp.com");
		prospectDTO.setIndustry("Information Technology");
		prospectDTO.setDepartment("Marketing");
		prospectDTO.setTitle("Manager");
		prospectDTO.setAddressLine1("425 2nd Street");
		prospectDTO.setAddressLine2("Suite 100");
		prospectDTO.setCity("San Francisco");
		prospectDTO.setCountry("United States");
		prospectDTO.setStateCode("CA");
		prospectDTO.setZipCode("94043");
		prospectDTO.setPhone("9999999");
		return prospectDTO;
	}
	/* END */

	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public void setAssetEmailTemplate(String campaignId , EmailMessageDTO emailMessageDTO) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		EmailMessage emailMessage = emailMessageDTO.toEmailMessage();
		campaign.setAssetEmailTemplate(emailMessage);
		campaignRepository.save(campaign);

		CampaignDNorm cdn = campaignDNormRepository.findByCampaignId(campaign.getId());
		if (cdn == null) {
			cdn = new CampaignDNorm();
		}
		cdn = cdn.toCampaignDNorm(campaign,cdn);
		campaignDNormRepository.save(cdn);
	}

	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public EmailMessageDTO retrivetAssetEmailTemplate(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		EmailMessage emailMessage = campaign.getAssetEmailTemplate();
		if (emailMessage == null) {
			return null;
		}
		return new EmailMessageDTO(emailMessage);
	}

	@Override
	public CampaignSearchResult listByteamIds(CampaignSearchDTO campaignSearchDTO) {
		List<Campaign> campaigns = campaignRepository.findByTeams(campaignSearchDTO.getIds());
		List<CampaignDTO> campaignDTOs = getCampaignDTOs(campaigns);
		return new CampaignSearchResult(campaigns.size(), campaignDTOs);
	}

	@Override
	public CampaignManagerMetricsDTO getCampaignManagerMetrics(
			String campaignManagerId) {
		CampaignManagerMetricsDTO campaignManagerMetricsDTO = new CampaignManagerMetricsDTO();
		campaignManagerMetricsDTO.setId(campaignManagerId);
		campaignManagerMetricsDTO.setCreatedCampaigns(campaignRepository.findByCampaignManagerId(campaignManagerId).size());
		campaignManagerMetricsDTO.setCompletedCampaigns(campaignRepository.findByCampaignManagerId(campaignManagerId, CampaignStatus.COMPLETED));
		return campaignManagerMetricsDTO;
	}

	@Override
	public void deleteCampaign(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign.getStatus() == CampaignStatus.CREATED || campaign.getStatus() == CampaignStatus.DELISTED) {
			campaignRepository.deleteById(campaignId);
		}
	}

/*	@Override
	 public CampaignSearchResult listByQa(CampaignSearchDTO campaignSearchDTO) {
	  List<TeamSearchResultDTO> teams = teamRepository.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
	  List<String> teamIds = new ArrayList<String>();
	  for (TeamSearchResultDTO teamSearchResult : teams) {
	   teamIds.add(teamSearchResult.getTeamId());
	  }

	  List<Campaign> campaigns = campaignRepository.findByTeams(teamIds);
	  List<CampaignDTO> campaignDTOs = getCampaignDTOs(campaigns);
	  return new CampaignSearchResult(campaigns.size(), campaignDTOs);
	 }*/

	//Added method for qafilter change
	@Override
	public CampaignSearchResult listByQa(CampaignSearchDTO campaignSearchDTO) throws ParseException {

			ProspectCallSearchDTO prospectCallSearchDTO = new ProspectCallSearchDTO();
			prospectCallSearchDTO.setClause(ProspectCallSearchClause.ByQa);
			prospectCallSearchDTO.setTimeZone(campaignSearchDTO.getTimeZone());
			return getCamapignsForProspectCallsByQa(prospectCallSearchDTO);
		}

	@Override
	public CampaignSearchResult listBySecQa(CampaignSearchDTO campaignSearchDTO) throws Exception {

		ProspectCallSearchDTO prospectCallSearchDTO = new ProspectCallSearchDTO();
		prospectCallSearchDTO.setClause(ProspectCallSearchClause.ByQa2);
		prospectCallSearchDTO.setProspectCallStatus(campaignSearchDTO.getProspectCallStatus());
		prospectCallSearchDTO.setTimeZone(campaignSearchDTO.getTimeZone());
		return getCamapignsForProspectCallsBySecQa(prospectCallSearchDTO);
	}

	@Override
	public CampaignSearchResult getCamapignsForProspectCallsBySecQa(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<Campaign> campaigns = new ArrayList<Campaign>();
		List<String> campaignIds =  new ArrayList<String>();
		if ((prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) &&
				(prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty())){
			List<TeamSearchResultDTO> teamSearchResults = teamRepository.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
			List<String> teamIds =  new ArrayList<String>();
			for (TeamSearchResultDTO teamSearchResult : teamSearchResults) {
				teamIds.add(teamSearchResult.getTeamId());
			}
			campaignIds = qaService.getCampaignIds(teamIds, prospectCallSearchDTO.getClientId());
		}else if ((prospectCallSearchDTO.getTeamIds() != null && !prospectCallSearchDTO.getTeamIds().isEmpty()) &&
				(prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty())) {
			campaignIds = qaService.getCampaignIds(prospectCallSearchDTO.getTeamIds(), prospectCallSearchDTO.getClientId());
		}else {

			campaignIds.addAll(prospectCallSearchDTO.getCampaignIds());
		}
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		if (!campaignIds.isEmpty()) {
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQaWithoutPagination(campaignIds, prospectCallSearchDTO);
		}
		if(prospectCalls != null)
		{
			Campaign campaign = null;
			String  campaignId;
			List<String> listCampaignIds=new ArrayList<String>();
			for (ProspectCallLog prospectCallLog : prospectCalls) {

				campaignId = prospectCallLog.getProspectCall().getCampaignId();

				if(!listCampaignIds.contains(prospectCallLog.getProspectCall().getCampaignId()))
				{
					listCampaignIds.add(campaignId);
				}


			}
			for (String campaigunId : listCampaignIds) {
				campaign = campaignRepository.findOneById(campaigunId);
				campaigns.add(campaign);
			}

		}
		List<CampaignDTO> campaignDTOs = getCampaignDTOs(campaigns);
		return new CampaignSearchResult(campaigns.size(), campaignDTOs);

	}

	@Override
	public CampaignSearchResult getCamapignsForProspectCallsByQa(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<Campaign> campaigns = new ArrayList<Campaign>();
		List<String> campaignIds =  new ArrayList<String>();
		if ((prospectCallSearchDTO.getTeamIds() == null || prospectCallSearchDTO.getTeamIds().isEmpty()) &&
				(prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty())){
			List<TeamSearchResultDTO> teamSearchResults = teamRepository.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
			List<String> teamIds =  new ArrayList<String>();
			for (TeamSearchResultDTO teamSearchResult : teamSearchResults) {
				teamIds.add(teamSearchResult.getTeamId());
			}
			campaignIds = qaService.getCampaignIds(teamIds, prospectCallSearchDTO.getClientId());
		}else if ((prospectCallSearchDTO.getTeamIds() != null && !prospectCallSearchDTO.getTeamIds().isEmpty()) &&
				(prospectCallSearchDTO.getCampaignIds() == null || prospectCallSearchDTO.getCampaignIds().isEmpty())) {
			campaignIds = qaService.getCampaignIds(prospectCallSearchDTO.getTeamIds(), prospectCallSearchDTO.getClientId());
		}else {

			campaignIds.addAll(prospectCallSearchDTO.getCampaignIds());
		}
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		if (!campaignIds.isEmpty()) {
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQaWithoutPagination(campaignIds, prospectCallSearchDTO);
		}
		if(prospectCalls != null)
		{
			Campaign campaign = null;
			String  campaignId;
			List<String> listCampaignIds=new ArrayList<String>();
			for (ProspectCallLog prospectCallLog : prospectCalls) {

				campaignId = prospectCallLog.getProspectCall().getCampaignId();

				if(!listCampaignIds.contains(prospectCallLog.getProspectCall().getCampaignId()))
				{
					listCampaignIds.add(campaignId);
				}


			}
			for (String campaigunId : listCampaignIds) {
				campaign = campaignRepository.findOneById(campaigunId);
				campaigns.add(campaign);
			}

		}
		List<CampaignDTO> campaignDTOs = getCampaignDTOs(campaigns);
		return new CampaignSearchResult(campaigns.size(), campaignDTOs);

	}

	private List<CampaignDTO> getCampaignDTOs(List<Campaign> campaigns) {
		List<CampaignDTO> campaignDTOs = new ArrayList<CampaignDTO>();
		for (Campaign campaign : campaigns) {
			CampaignDTO campaignDTO = new CampaignDTO();
			campaignDTO.setId(campaign.getId());
			campaignDTO.setName(campaign.getName());
			campaignDTO.setOrganizationId(campaign.getOrganizationId());
			campaignDTOs.add(campaignDTO);
		}
		return campaignDTOs;
	}

	@Override
	public List<String> getAllocatedAgents(String campaignId) {
		//create a unique list of agents which includes...
		Set<String> agentsOnCampaign = new HashSet<String>();
		String supervisorId = null;

		//a) agents allocated to this campaign
		Campaign campaign = getCampaign(campaignId);
		if (campaign.getTeam() != null) {
			supervisorId = campaign.getTeam().getSupervisorId();
			for (CampaignResource resource : campaign.getTeam().getAgents()) {
				agentsOnCampaign.add(resource.getResourceId());
			}
		}

		//b) ones which are allocated through supervisor override
		// List<Team> teamsTemporarilyOnCampaign = teamRepository.findTeamsTemporarilyOnCampaign(campaignId, supervisorId);
		// fetch teams by supervisorOverrideCampaignId in team >> agents >> supervisorOverrideCampaignId
		List<Team> teamsTemporarilyOnCampaign = teamRepository.findBySupervisorOverrideCampaignId(campaignId);
		if (teamsTemporarilyOnCampaign != null) {
			for (Team team : teamsTemporarilyOnCampaign) {
				for (TeamResource agent : team.getAgents()) { //traverse thru each agent in the team and find out if this is allocated to the campaign
					if (agent.getSupervisorOverrideCampaignId() != null && campaignId.equals(agent.getSupervisorOverrideCampaignId())) {
						agentsOnCampaign.add(agent.getResourceId());
					}
				}
			}
		}

		//for each of agent find its campaign based on current time indicating as per his work schedule, which campaign he is working on currently
		List<String> agentsAllocated = new ArrayList<String>();
		for (String agent : agentsOnCampaign) {
			AgentCampaignDTO agentCampaignDTO = agentService.getCurrentAllocatedCampaign(agent);

			if (agentCampaignDTO != null && campaignId.equals(agentCampaignDTO.getId())) {
				agentsAllocated.add(agent);
			}
		}

		return agentsAllocated;
	}

	public List<Campaign> getCampaignByIds(List<String> campaignIds){
		// Iterable<String> iterable = campaignIds;
		Iterable<Campaign> campaignsItr = campaignRepository.findAllById(campaignIds);
		 if(campaignsItr instanceof List) {
		      return (List<Campaign>) campaignsItr;
		    }
		return null;
	}

	public List<String> getCampaignIds(List<String> teamIds) {
		List<String> campaignIds =  new ArrayList<String>();
		List<Campaign> campaigns;
		campaigns = campaignRepository.findByTeams(teamIds);

		if (!campaigns.isEmpty()) {
			for (Campaign campaign : campaigns) {
				campaignIds.add(campaign.getId());
			}
		}
		return campaignIds;
	}

	private String encodeURL(String url){
	    String encodedURL = url;
	    String urlArray[] = url.split("/");
	    String fileName = urlArray[urlArray.length-1];
	    try {
            String decodedFileName = URLDecoder.decode(fileName, XtaasConstants.UTF8); //decoding it first, in case it is already encoded.
            String encodedFileName = URLEncoder.encode(decodedFileName, XtaasConstants.UTF8);
            encodedURL = url.replace(urlArray[urlArray.length -1], encodedFileName);
        } catch (UnsupportedEncodingException e) {
            logger.error("Error in encoding url : " + url, e);
        }
	    return encodedURL;
	}

	private String findOrganizationTimeZone(String organizationId){
	    String timeZone = "";
	    Organization organization = organizationRepository.findOneById(organizationId);
	    if(organization != null && organization.getTimezone() != null) {
	        timeZone =  organization.getTimezone();
	    }
	    return timeZone;
	}

	private List<String> getOrganizationPartnerSupervisors(Organization organization) {
		List<String> supervisors = new ArrayList<>();
		 if (organization != null && organization.getPartnerSupervisors() != null && organization.getPartnerSupervisors().size() > 0) {
			 supervisors = organization.getPartnerSupervisors();
		 }
		 return supervisors;
	}

	private boolean isSimpleDisposition(Organization organization){
	    boolean isSimpleDisposition = false;

        if(organization != null) {
        	if (organization.getOrganizationLevel() == OrganizationLevel.INTERNAL) {
        		isSimpleDisposition = true;
        	}
        	else {
        		isSimpleDisposition = false;
        	}
        }
        return isSimpleDisposition;
	}

	@Override
	public void sendEmail(String campaignId) {
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign.getStatus().equals(CampaignStatus.CREATED)) {
			campaign.setStatus(CampaignStatus.PLANNING);
			campaignRepository.save(campaign);
		}
		if (campaign != null) {
			XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com",
					campaign.getName()+" has been assigned to PLANNING.",
					campaign.getOrganizationId()+ " has assigned the campaign name"+ campaign.getName()
					+" to PLANNING. Please Proceed with further process.");
		}
	}

	@Override
	public CampaignNameSearchResult searchCampaigns(CampaignNameSearchDTO campaignSearchDTO) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		if (userLogin != null) {
			List<Campaign> campaigns = campaignRepository.searchCampaignByName(campaignSearchDTO.getCampaignName(),
					userLogin.getOrganization(), PageRequest.of(campaignSearchDTO.getPage(),
							campaignSearchDTO.getSize(), campaignSearchDTO.getDirection(), "createdDate"));
			List<CampaignDTO> campaignDTOs = new ArrayList<CampaignDTO>(campaigns.size());
			for (Campaign campaign : campaigns) {
				campaignDTOs.add(new CampaignDTO(campaign));
			}
			return new CampaignNameSearchResult(campaignRepository.countMatchingCampaignList(
					campaignSearchDTO.getCampaignName(), userLogin.getOrganization()), campaignDTOs);
		} else {
			throw new IllegalArgumentException("Logged in user not found.");
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public CampaignDTO cloneCampaign(String campaignId) {
		logger.debug("cloneCampaign start : " + campaignId);
		Campaign cloneCampaign = new Campaign();
		Campaign campaign = campaignRepository.findOneById(campaignId);
		List<NewClientMapping> newClientMapings = new ArrayList<>();
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		cloneCampaign = new Campaign(userLogin.getOrganization(), userLogin.getUsername(), campaign.getName(),
				null, null, campaign.getType(),
				campaign.getQualificationCriteria() != null
						? campaign.getQualificationCriteria()
						: null,
						campaign.getDeliveryTarget(), campaign.getDomain(), campaign.isSelfManageTeam());

		// Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		cloneCampaign.getCampaignClone(campaign);
		Campaign campaign2 = campaignRepository.save(cloneCampaign);
		logger.info(new SplunkLoggingUtils("CloneCampaign", campaign2.getId())
				.eventDescription("Campaign cloned successfully").build());

		CampaignDNorm cdn = new CampaignDNorm();
		cdn = cdn.toCampaignDNorm(campaign2,cdn);
		campaignDNormRepository.save(cdn);
		List<NewClientMapping> clientMappings = newClientMappingRepository.findClientMappingByCampaignId(campaignId);
		if (clientMappings != null && clientMappings.size() > 0) {
			for (NewClientMapping clientMapping : clientMappings) {
				NewClientMapping newMapping = new NewClientMapping();
				newMapping.getNewClientMappingClone(clientMapping, campaign2.getId());
				newClientMapings.add(newMapping);
			}
			newClientMappingRepository.saveAll(newClientMapings);
			logger.info(new SplunkLoggingUtils("CloneCampaign", campaign2.getId())
					.eventDescription("NewClientMapping cloned succefully for campaignId" + campaign2.getId()).build());
		}

		CRMMapping orgMapping = crmMappingRepository.findOneById(cloneCampaign.getOrganizationId());
		ArrayList<CRMAttribute> crmAttributes = new ArrayList<>();
		if (orgMapping != null && orgMapping.getAttributeMapping() != null
				&& orgMapping.getAttributeMapping().size() > 0) {
			for (CRMAttribute crmAttribute : orgMapping.getAttributeMapping()) {
				if (crmAttribute.getCampaignId() != null && crmAttribute.getCampaignId().equalsIgnoreCase(campaignId)) {
					CRMAttribute attribute = new CRMAttribute();
					attribute.getCRMAttributeClone(crmAttribute, campaign2.getId());
					crmAttributes.add(attribute);
				}
			}
			orgMapping.getAttributeMapping().addAll(crmAttributes);
			crmMappingRepository.save(orgMapping);
			logger.info(new SplunkLoggingUtils("CloneCampaign", campaign2.getId())
					.eventDescription("CRMMapping cloned succefully for campaignId" + campaign2.getId()).build());
		}
		List<String> cids = new ArrayList<String>();
		cids.add(campaignId);
		int newSupCount = suppressionListNewRepository.countSupressionListByCampaignIds(cids);
		int batchsize = 0;
		if (newSupCount > 0) {
			batchsize = newSupCount / 5000;
			long modSize = newSupCount % 5000;
			if (modSize > 0) {
				batchsize = batchsize + 1;
			}
			for (int i = 0; i < batchsize; i++) {
				Pageable pageable = PageRequest.of(i, 5000, Direction.DESC, "createdDate");
				List<SuppressionListNew> supListFromDB = suppressionListNewRepository
						.findSupressionListByCampaignToClone(campaignId, pageable);
				List<SuppressionListNew> newSupList = new ArrayList<SuppressionListNew>();
				for (SuppressionListNew suppressionListNew : supListFromDB) {
					SuppressionListNew listNew = new SuppressionListNew();
					listNew.getSuppressionListNewClone(suppressionListNew, campaign2.getId());
					newSupList.add(listNew);
				}
				suppressionListNewRepository.saveAll(newSupList);
			}
		}
		logger.info(new SplunkLoggingUtils("CloneCampaign", campaign2.getId())
				.eventDescription("SuppressionListNew cloned succefully for campaignId" + campaign2.getId()).build());

		List<AbmListNew> abmLists = abmListNewRepository.findABMListNewByCampaignId(campaignId);
		List<AbmListNew> cloneAbmList = new ArrayList<AbmListNew>();
		if(abmLists != null && abmLists.size() > 0) {
			for (AbmListNew abmList : abmLists) {
				if (abmList.getCampaignId() != null && abmList.getCampaignId().equalsIgnoreCase(campaignId)) {
					AbmListNew cloneAbm = new AbmListNew();
					cloneAbm.getAbmListNewClone(abmList, campaign2.getId());
					cloneAbmList.add(cloneAbm);
				}
			}
			abmListNewRepository.saveAll(cloneAbmList);
			logger.info(new SplunkLoggingUtils("CloneCampaign", campaign2.getId())
					.eventDescription("AbmListNew cloned succefully for campaignId" + campaign2.getId()).build());
		}
		return new CampaignDTO(campaign2);
	}

	@Override
	public SupervisorCampaignResult searchSupervisorCampaigns(String campaignName) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		Organization org = organizationRepository.findOneById(userLogin.getOrganization());
		List<SupervisorCampaignsVM> supervisorCampaigns = new ArrayList<SupervisorCampaignsVM>();
		List<Campaign> campaignsFromDB = campaignRepository.findCampaignsByNameAndSupervisorId(campaignName, userLogin.getOrganization(),userLogin.getUsername());
		if (campaignsFromDB != null && campaignsFromDB.size() > 0) {
			for (Campaign campaign : campaignsFromDB) {
				SupervisorCampaignsVM supervisorCampaignVM = new SupervisorCampaignsVM();
				supervisorCampaignVM.setCampaignId(campaign.getId());
				supervisorCampaignVM.setName(campaign.getName());
				supervisorCampaignVM.setType(campaign.getType().name());
				supervisorCampaignVM.setTimeZone(campaign.getTimezone());
				supervisorCampaignVM.setStatus(campaign.getStatus());
				supervisorCampaignVM.setStartDate(campaign.getStartDate());
				supervisorCampaignVM.setRetrySpeed(campaign.getRetrySpeed());
				supervisorCampaignVM.setCampaignRequirements(campaign.getCampaignRequirements());
				supervisorCampaignVM.setQualificationCriteria(campaign.getQualificationCriteria());
				supervisorCampaignVM.setAbm(campaign.isABM());
				supervisorCampaignVM.setMaxEmployeeAllow(campaign.isMaxEmployeeAllow());
				if (campaign.getTeam() != null && campaign.getTeam().getSupervisorId().equalsIgnoreCase(userLogin.getUsername())) {
					supervisorCampaignVM.setReadOnlyCampaign(false);
				} else {
					supervisorCampaignVM.setReadOnlyCampaign(true);
				}
				if (campaign.getTeam() != null) {
					supervisorCampaignVM.setCallSpeedPerMinPerAgent(campaign.getTeam().getCallSpeedPerMinPerAgent());
					supervisorCampaignVM.setLeadSortOrder(campaign.getTeam().getLeadSortOrder());
				}
				supervisorCampaigns.add(supervisorCampaignVM);
			}
		}
		return new SupervisorCampaignResult(supervisorCampaigns.size(),supervisorCampaigns,org.isProspectUploadEnabled());
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR })
	public CampaignDTO returnCampaigns(String id) {
		String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
		if (loggedInUserName != null) {
			if (loggedInUserName.equalsIgnoreCase("dialer")) {
				return getCampaignList(id);
			}
			Login userLogin = userService.getUser(loggedInUserName);
			if (userLogin != null && userLogin.getRoles().contains(Roles.ROLE_CAMPAIGN_MANAGER.toString())) {
				return getCampaignList(id);
			}
			Campaign cm = campaignService.getCampaign(id);
			if (cm != null) {
				List<String> cmSupervisorList = new ArrayList<String>();
				if (cm.getPartnerSupervisors() != null) {
					cmSupervisorList.addAll(cm.getPartnerSupervisors());
				}
				if (cm.getAdminSupervisorIds() != null) {
					cmSupervisorList.addAll(cm.getAdminSupervisorIds());
				}
				if (cm.getTeam() != null && cm.getTeam().getSupervisorId() != null) {
					cmSupervisorList.add(cm.getTeam().getSupervisorId());
				}
				if (cmSupervisorList.contains(XtaasUserUtils.getCurrentLoggedInUsername())) {
					return getCampaignList(id);
				} else {
					throw new IllegalArgumentException("You are not authorized.");
				}
			}
		}
		return null;
	}
	
	private CampaignDTO getCampaignList(String id) {
		CampaignDTO campaignDTO = new CampaignDTO(campaignService.getCampaign(id));
		List<ProspectCallLog> pcallLogList = prospectCallLogRepository.findContactsBySuccess(campaignDTO.getId(),
				campaignDTO.getStartDate());
		if (campaignDTO.getAssets() != null && pcallLogList != null) {
			for (AssetDTO assetDto : campaignDTO.getAssets()) {
				int assetCount = 0;
				for (ProspectCallLog prospectCallLog : pcallLogList) {
					if (assetDto.getAssetId().equals(prospectCallLog.getProspectCall().getDeliveredAssetId())) {
						assetCount++;
					}
				}
				assetDto.setAssetActualTarget(assetCount);
			}
		}
		return campaignDTO;
	}

	@Override
	public CampaignDetailsForAgentDTO getCampaignInfoForAgent(String id) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		if (userLogin != null && userLogin.getRoles().contains(Roles.ROLE_AGENT.toString())) {
			Campaign campaign = campaignService.getCampaign(id);
			CampaignDTO campaignDTO = new CampaignDTO(campaign);
			if (campaignDTO != null) {
				if (campaign.isEnableCustomFields()) {
					Map<String, AgentQaAnswer> cusfieldMap = campaign.getAllDefaultCustomFieldQuestionAndAnswerMap(campaign.getQualificationCriteria().getExpressions());
					if (cusfieldMap != null && cusfieldMap.size() > 0) {
						List<AgentQaAnswer> agentQaAnswerList =  new ArrayList<AgentQaAnswer>(cusfieldMap.values());
						return new CampaignDetailsForAgentDTO(campaignDTO, agentQaAnswerList);		
					}
				}
				return new CampaignDetailsForAgentDTO(campaignDTO);
			}
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
		return null;
	}

}