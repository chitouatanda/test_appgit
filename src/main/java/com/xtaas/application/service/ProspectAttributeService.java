package com.xtaas.application.service;

import java.util.List;

import com.xtaas.domain.entity.ProspectAttribute;

public interface ProspectAttributeService {
	public List<ProspectAttribute> getProspectAttributes();

}
