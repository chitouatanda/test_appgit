package com.xtaas.application.service;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;
import com.xtaas.domain.valueobject.CampaignContactSource;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.service.GlobalContactService;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.ProspectCallLogDTO;
import com.xtaas.web.dto.RestCampaignContactDTO;
import com.xtaas.web.dto.RestCdpCampaignContactDTO;
import com.xtaas.web.dto.RestProspectResponseDTO;
import com.xtaas.web.dto.RestProspectResponseStatusDTO;

@Service
public class RestCampaignContactServiceImpl implements RestCampaignContactService {

	private static final Logger logger = LoggerFactory.getLogger(RestCampaignContactServiceImpl.class);
	
	private static final Pattern ASCII_PATTERN = Pattern.compile("[^\\p{ASCII}]");
	private static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();

	@Autowired
	private CampaignContactRepository campaignContactRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;
	
	@Autowired
	private CampaignRepository campaignRepository; 
	
	@Autowired
	private GlobalContactService globalContactService;
	
	private ConcurrentHashMap<String, String> dataUploadRequestMap = new ConcurrentHashMap<String, String>();
	
	@Override
	public List<String> uploadRestData(List<RestCampaignContactDTO> campaignContacts) {

		List<String> errorList = new ArrayList<>();
		final int maxThreads = 4;
		logger.debug("uploadRestData(): Creating executer service.");
		ExecutorService executorService = new ThreadPoolExecutor(maxThreads, // core thread pool size
				maxThreads, // maximum thread pool size
				1, // time to wait before resizing pool
				TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(maxThreads, true),
				new ThreadPoolExecutor.CallerRunsPolicy());

		Future<List<RestCampaignContactDTO>> future = executorService.submit(new RestDataUpload(campaignContacts));
//		try {
//			for (RestCampaignContactDTO restCampaignContactDTO : future.get()) {
//				if (restCampaignContactDTO.getErrors() != null) {
//					errorList.addAll(restCampaignContactDTO.getErrors());
//					if (errorList != null && errorList.size() == 0) {
//						errorList.add("All records uploaded successfully.");
//					}
//				}
//			}
//		} catch (InterruptedException | ExecutionException e) {
//			e.printStackTrace();
//			logger.error("Exception is : ", e);
//		}
		return errorList;
		// List<UploadedContact> uploadedContactList = new ArrayList<>();
		// if (campaignContacts != null && campaignContacts.size() > 0) {
		// List<CampaignContact> campaignContactList = new ArrayList<>();
		// List<String> errorList = new ArrayList<>();
		// logger.info("uploadRestData()");
		// for (RestCampaignContactDTO restCampaignContactDTO : campaignContacts) {
		// CampaignContact tempCampaignContact = null;
		// UploadedContact uploadedContact = new UploadedContact();
		// boolean isRecordValid = true;
		// if (restCampaignContactDTO.getFirstName() != null &&
		// !restCampaignContactDTO.getFirstName().isEmpty()) {
		// String firstName =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getFirstName());
		// firstName = (firstName.length() < 250) ? firstName : firstName.substring(0,
		// 250);
		// uploadedContact.setFirstName(firstName);
		// } else {
		// isRecordValid = false;
		// errorList.add( "First Name
		// should not empty.");
		// }
		// if (restCampaignContactDTO.getLastName() != null &&
		// !restCampaignContactDTO.getLastName().isEmpty()) {
		// String lastName =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getLastName());
		// lastName = (lastName.length() < 250) ? lastName : lastName.substring(0, 250);
		// uploadedContact.setLastName(lastName);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Last Name
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getCampaignId() != null
		// && !restCampaignContactDTO.getCampaignId().isEmpty()) {
		// uploadedContact.setCampaignId(restCampaignContactDTO.getCampaignId());
		//
		// } else {
		// isRecordValid = false;
		// errorList.add( "CampaignId
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getSource() != null &&
		// !restCampaignContactDTO.getSource().isEmpty()) {
		// try {
		// String source = XtaasUtils
		// .removeNonAsciiChars(validateSourceFieldValue(restCampaignContactDTO.getSource()));
		// uploadedContact.setSource(source);
		//
		// } catch (IllegalArgumentException e) {
		// logger.info("excelFileRead():validateExcelFileRecords() : " +
		// e.getMessage());
		// errorList.add(e.getMessage());
		// isRecordValid = false;
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "Source should
		// not empty.");
		// }
		// if ( != null &&
		// !.isEmpty()) {
		// String sourceId = XtaasUtils.removeNonAsciiChars(
		// validateMultiTypeFieldValue(restCampaignContactDTO.getContactId(),
		// "SourceId"));
		// if (isSourceIdAlreadyPurchased(sourceId)) {
		// logger.info("UploadRestData() " + "" + sourceId + " PersonId is already
		// purchased ");
		// errorList.add(sourceId + " PersonId is already purchased");
		// isRecordValid = false;
		// } else {
		// uploadedContact.setSourceId(sourceId);
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "SourceId
		// should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getTitle() != null &&
		// !restCampaignContactDTO.getTitle().isEmpty()) {
		// String title =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getTitle());
		// title = (title.length() < 250) ? title : title.substring(0, 250);
		// uploadedContact.setTitle(title);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Title should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getDepartment() != null
		// && !restCampaignContactDTO.getDepartment().isEmpty()) {
		// String department =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getDepartment());
		// department = (department.length() < 250) ? department :
		// department.substring(0, 250);
		// uploadedContact.setDepartment(department);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Department
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getOrganizationName() != null
		// && !restCampaignContactDTO.getOrganizationName().isEmpty()) {
		// String orgname =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getOrganizationName());
		// orgname = (orgname != null && orgname.length() < 250) ? orgname :
		// orgname.substring(0, 250);
		// uploadedContact.setOrganizationName(orgname);
		// } else {
		// isRecordValid = false;
		// errorList
		// .add( "Organization name
		// should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getDomain() != null &&
		// !restCampaignContactDTO.getDomain().isEmpty()) {
		// String domain =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getDomain());
		// domain = (domain.length() < 250) ? domain : domain.substring(0, 250);
		// uploadedContact.setDomain(domain);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Domain should
		// not empty.");
		// }
		// try {
		// uploadedContact.setMinRevenue(
		// validateDoubleTypeFieldValue(restCampaignContactDTO.getMinRevenue(), "Minimum
		// Revenue"));
		// } catch (Exception e) {
		// errorList.add(
		// "Minimum revenue" + "" +
		// e.getMessage());
		// isRecordValid = false;
		// }
		// try {
		// uploadedContact.setMaxRevenue(
		// validateDoubleTypeFieldValue(restCampaignContactDTO.getMaxRevenue(), "Maximum
		// Revenue"));
		// } catch (Exception e) {
		// errorList.add(
		// "Maximum revenue" + "" +
		// e.getMessage());
		// isRecordValid = false;
		// }
		//
		// try {
		// uploadedContact.setMinEmployeeCount(validateDoubleTypeFieldValue(
		// restCampaignContactDTO.getMinEmployeeCount(), "Minimum Employee Count"));
		// } catch (IllegalArgumentException e) {
		// errorList.add( "Minimum
		// Employee Count" + ""
		// + e.getMessage());
		// isRecordValid = false;
		// }
		//
		// try {
		// uploadedContact.setMaxEmployeeCount(validateDoubleTypeFieldValue(
		// restCampaignContactDTO.getMaxEmployeeCount(), "Maximum Employee Count"));
		// } catch (IllegalArgumentException e) {
		// errorList.add( "Maximum
		// Employee Count" + ""
		// + e.getMessage());
		// isRecordValid = false;
		// }
		//
		// if (restCampaignContactDTO.getEmail() != null &&
		// !restCampaignContactDTO.getEmail().isEmpty()) {
		// String email =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getEmail());
		// email = (email == null || email.length() < 250) ? email : email.substring(0,
		// 250);
		// uploadedContact.setEmail(email);
		// } else {
		// isRecordValid = false;
		// errorList.add( "email should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getAddressLine() != null
		// && !restCampaignContactDTO.getAddressLine().isEmpty()) {
		// String addressLine =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getAddressLine());
		// addressLine = (addressLine.length() < 250) ? addressLine :
		// addressLine.substring(0, 250);
		// uploadedContact.setAddressLine(addressLine);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Address should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getCity() != null &&
		// !restCampaignContactDTO.getCity().isEmpty()) {
		// String city =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getCity());
		// city = (city == null || city.length() < 250) ? city : city.substring(0, 250);
		// uploadedContact.setCity(city);
		// } else {
		// isRecordValid = false;
		// errorList.add( "City should
		// not
		// empty.");
		// }
		//
		// if (restCampaignContactDTO.getStateCode() != null &&
		// !restCampaignContactDTO.getStateCode().isEmpty()) {
		// String stateCode =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getStateCode());
		// stateCode = (stateCode == null || stateCode.length() < 250) ? stateCode
		// : stateCode.substring(0, 250);
		// StateCallConfig callConfig =
		// stateCallConfigRepository.findByStateCode(stateCode);
		// if (callConfig != null) {
		// uploadedContact.setStateCode(stateCode);
		// } else {
		// isRecordValid = false;
		// errorList.add( "State Code " +
		// stateCode
		// + " dosen't exist.");
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "State Code
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getCountry() != null &&
		// !restCampaignContactDTO.getCountry().isEmpty()) {
		// String country =
		// XtaasUtils.removeNonAsciiChars(restCampaignContactDTO.getCountry());
		// country = (country == null || country.length() < 250) ? country :
		// country.substring(0, 250);
		// uploadedContact.setCountry(country);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Country should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getPostalCode() != null
		// && !restCampaignContactDTO.getPostalCode().isEmpty()) {
		// try {
		// uploadedContact.setPostalCode(
		// validateMultiTypeFieldValue(restCampaignContactDTO.getPostalCode(), "Postal
		// Code"));
		// } catch (IllegalArgumentException e) {
		// errorList.add(e.getMessage());
		// isRecordValid = false;
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "Postal Code
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getPhone() != null) {
		// try {
		// String phone = XtaasUtils.removeNonAsciiChars(
		// validateMultiTypeFieldValue(restCampaignContactDTO.getPhone(), "Phone"));
		// phone = (phone == null || phone.length() < 250) ? phone : phone.substring(0,
		// 250);
		// if ((phone.startsWith("91") || phone.startsWith("44") ||
		// phone.startsWith("33")
		// || phone.startsWith("49")) && !phone.startsWith("+")) {
		// phone = "+" + phone;
		// }
		// uploadedContact.setPhone(phone);
		// } catch (IllegalArgumentException e) {
		// errorList.add(e.getMessage());
		// isRecordValid = false;
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "Phone should
		// not empty.");
		// }
		//
		// if (restCampaignContactDTO.getExtension() != null &&
		// !restCampaignContactDTO.getExtension().isEmpty()) {
		// try {
		// String ext = XtaasUtils.removeNonAsciiChars(
		// validateMultiTypeFieldValue(restCampaignContactDTO.getExtension(),
		// "Extension"));
		// uploadedContact.setExtension(ext);
		// } catch (IllegalArgumentException e) {
		// errorList.add(e.getMessage());
		// isRecordValid = false;
		// }
		// } else {
		// isRecordValid = false;
		// errorList.add( "Extension
		// should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getListPurchaseTransactionId() != null
		// && !restCampaignContactDTO.getListPurchaseTransactionId().isEmpty()) {
		// uploadedContact.setListPurchaseTransactionId(restCampaignContactDTO.getListPurchaseTransactionId());
		// } else {
		// isRecordValid = false;
		// errorList.add(""
		// + "List purchase transaction id should not empty.");
		// }
		//
		// if (restCampaignContactDTO.getManagementLevel() != null
		// && !restCampaignContactDTO.getManagementLevel().isEmpty()) {
		// String managementLevel =
		// XtaasUtils.removeNonAsciiChars(validateMultiTypeFieldValue(
		// restCampaignContactDTO.getManagementLevel(), "ManagementLevel"));
		// managementLevel = (managementLevel == null || managementLevel.length() < 250)
		// ? managementLevel
		// : managementLevel.substring(0, 250);
		// uploadedContact.setManagementLevel(managementLevel);
		// } else {
		// isRecordValid = false;
		// errorList.add( "Management
		// Level should not empty.");
		// }
		//
		// uploadedContact.setDirectPhone(restCampaignContactDTO.isDirectPhone());
		//
		// if (restCampaignContactDTO.getCompanyValidationLink() != null) {
		// String zoomCompanyUrl = XtaasUtils
		// .removeNonAsciiChars(restCampaignContactDTO.getCompanyValidationLink());
		// uploadedContact.setZoomCompanyUrl(zoomCompanyUrl);
		// }
		//
		// if (restCampaignContactDTO.getTitleValidationLink() != null
		// && !restCampaignContactDTO.getTitleValidationLink().isEmpty()) {
		// String zoomPersonUrl = XtaasUtils
		// .removeNonAsciiChars(restCampaignContactDTO.getTitleValidationLink());
		// uploadedContact.setZoomPersonUrl(zoomPersonUrl);
		// }
		// if (isRecordValid) {
		// tempCampaignContact = new CampaignContact(uploadedContact.getCampaignId(),
		// uploadedContact.getSource(), uploadedContact.getSourceId(), null,
		// uploadedContact.getFirstName(), uploadedContact.getLastName(), null,
		// uploadedContact.getTitle(), uploadedContact.getDepartment(), null,
		// uploadedContact.getOrganizationName(), uploadedContact.getDomain(), null,
		// null,
		// uploadedContact.getMinRevenue(), uploadedContact.getMaxRevenue(),
		// uploadedContact.getMinEmployeeCount(), uploadedContact.getMaxEmployeeCount(),
		// uploadedContact.getEmail(), uploadedContact.getAddressLine(), null,
		// uploadedContact.getCity(), uploadedContact.getStateCode(),
		// uploadedContact.getCountry(),
		// uploadedContact.getPostalCode(), uploadedContact.getPhone(), null, null,
		// uploadedContact.getManagementLevel(), null, false);
		// tempCampaignContact.setDirectPhone(uploadedContact.isDirectPhone());
		// tempCampaignContact.setZoomCompanyUrl(uploadedContact.getZoomCompanyUrl());
		// tempCampaignContact.setZoomPersonUrl(uploadedContact.getZoomPersonUrl());
		// tempCampaignContact.setExtension(uploadedContact.getExtension());
		//
		// if (isPersonRecordAlreadyPurchased(uploadedContact)) {
		// logger.info("contactListRead():validateListRecords()" +
		// restCampaignContactDTO.getContactId()
		// + "" + " Combination of First Name " + uploadedContact.getFirstName()
		// + " and Last Name " + uploadedContact.getLastName() + " and Company Name "
		// + uploadedContact.getOrganizationName() + " is already purchased");
		// errorList.add( " Combination
		// of
		// First Name "
		// + uploadedContact.getFirstName() + " and Last Name " +
		// uploadedContact.getLastName()
		// + " and Company Name " + uploadedContact.getOrganizationName()
		// + " is already purchased");
		// isRecordValid = false;
		// } else {
		// campaignContactList.add(tempCampaignContact);
		// uploadedContactList.add(uploadedContact);
		// }
		// }
		// }
		// if (!errorList.isEmpty() && errorList.size() != 0) {
		// StringBuilder stringBuilder = new StringBuilder();
		// for (String error : errorList) {
		// stringBuilder.append(error);
		// stringBuilder.append("<br />");
		// }
		// XtaasEmailUtils.sendEmail("sudhir.gholap@invimatic.com",
		// "data@xtaascorp.com",
		// "Contact Upload Errorlist", stringBuilder.toString());
		// logger.info("Contact upload errorlist mail sent successfully to : " +
		// "sudhir.gholap@invimatic.com");
		// }
		// List<CampaignContact> campaignContacts1 =
		// campaignContactRepository.save(campaignContactList);
		// if (campaignContacts1 != null && !campaignContacts1.isEmpty()) {
		//
		// List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
		// List<ProspectCallInteraction> prospectCallInteractions = new
		// ArrayList<ProspectCallInteraction>();
		// for (CampaignContact campaignContact : campaignContacts1) {
		// Prospect prospect = campaignContact.toProspectFromExcel(null);
		// prospect.setSource(campaignContact.getSource());
		// prospect.setSourceId(campaignContact.getSourceId());
		// prospect.setCampaignContactId(campaignContact.getId());
		// ProspectCallLog prospectCallLog = new ProspectCallLog();
		// ProspectCall prospectCall = new ProspectCall(UUID.randomUUID().toString(),
		// campaignContact.getCampaignId(), prospect);
		// // prospectCallLog.setProspectCall(new
		// // ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));
		//
		// Optional<UploadedContact> contacts = uploadedContactList.stream()
		// .filter(contact -> contact.getFirstName().equals(prospect.getFirstName())
		// && contact.getLastName().equals(prospect.getLastName())
		// && contact.getOrganizationName().equals(prospect.getCompany()))
		// .findFirst();
		// if (contacts.isPresent()) {
		// prospectCall.setProspectVersion(contacts.get().getProspectVersion());
		// }
		//
		// ///////////////////////////////////////////////////////////////////
		//
		// // TODO 1 below line is commented as the area code and the statecode matching
		// is
		// // completely off, 408 area code is pacific
		// // but the state is NY , so this needs to be verfied well
		//
		// /*
		// * String timezone = getTimeZone(prospectCall.getProspect().getPhone());
		// *
		// * if(timezone==null) timezone =
		// *
		// properties.getProperty(findAreaCode(prospectCall.getProspect().getPhone()));
		// */
		//
		// // if(timezone==null){
		// StateCallConfig scc = stateCallConfigRepository
		// .findByStateCode(prospectCall.getProspect().getStateCode());
		// prospectCall.getProspect().setTimeZone(scc.getTimezone());
		//
		// /*
		// * }else{ prospectCall.getProspect().setTimeZone(timezone); }
		// */
		//
		// //////////////////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////////
		// StringBuffer sb = new StringBuffer();
		// String emp = "";
		// String mgmt = "";
		//
		// // TODO 2 is linked to above todo so commented below code
		// // String tz = getStateWeight(prospectCall.getProspect().getTimeZone());
		// String tz = getStateWeight(scc.getTimezone());
		//
		// /*
		// * sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
		// */
		// sb.append(prospectCallLog.getStatus());
		// sb.append("|");
		// if (prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount")
		// instanceof Number) {
		// Number n1 = (Number)
		// prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount");
		// sb.append(getBoundry(n1, "min"));
		// sb.append("-");
		// emp = getEmployeeWeight(n1);
		// }
		//
		// if (prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount")
		// instanceof Number) {
		// Number n2 = (Number)
		// prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount");
		// sb.append(getBoundry(n2, "max"));
		// }
		//
		// /*
		// * sb.append(prospectCall.getProspect().getCustomAttributeValue(
		// * "minEmployeeCount")); sb.append("-");
		// * sb.append(prospectCall.getProspect().getCustomAttributeValue(
		// * "maxEmployeeCount"));
		// */
		// sb.append("|");
		// sb.append(getMgmtLevel(prospectCall.getProspect().getManagementLevel()));
		// mgmt = getMgmtLevelWeight(prospectCall.getProspect().getManagementLevel());
		// String direct = "";
		// if (prospectCall.getProspect().isDirectPhone()) {
		// direct = "1";
		// } else {
		// direct = "2";
		// }
		// int qbSort = 0;
		// if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty())
		// {
		// String sortString = direct + tz + mgmt + emp;
		// qbSort = Integer.parseInt(sortString);
		// }
		//
		// ///////////////////////////////////////////////////////////////////
		//
		// prospectCall.setQualityBucket(sb.toString());
		// prospectCall.setQualityBucketSort(qbSort);
		// prospectCallLog.setProspectCall(prospectCall);
		// prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
		// prospectCallLogs.add(prospectCallLog);
		// ProspectCallInteraction prospectCallInteraction = new
		// ProspectCallInteraction();
		// prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
		// prospectCallInteraction.setProspectCall(prospectCall);
		// prospectCallInteractions.add(prospectCallInteraction);
		// }
		// prospectCallLogRepository.save(prospectCallLogs);
		// prospectCallInteractionRepository.save(prospectCallInteractions);
		//
		// }
		// }
	}

	public boolean isPureAscii(String str) {
		return asciiEncoder.canEncode(str);
	}

	public String removeNonAsciiChars(String str) {
		if (!isPureAscii(str)) {
			return ASCII_PATTERN.matcher(Normalizer.normalize(str, Normalizer.Form.NFD)).replaceAll(" ");
		} else {
			return str;
		}
	}
	
	private boolean isPersonRecordAlreadyPurchased(UploadedContact uploadedContact, String campaignId) {
		String firstName = uploadedContact.getFirstName();
		String lastName = uploadedContact.getLastName();
		String phoneNumber = uploadedContact.getPhone();
		if (phoneNumber.contains("+")) {
			phoneNumber = phoneNumber.substring(1);
		}
		int uniqueRecord = prospectCallLogRepository.findManualProspectRecordCount(campaignId,
				firstName, lastName, phoneNumber);
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;
		}
	}

	private String validateSourceFieldValue(String inputValue) {
		String value;
		if (inputValue.equalsIgnoreCase("ZOOMI")) {
			value = CampaignContactSource.ZOOMINFO.name();
		} else if (inputValue.equalsIgnoreCase("NETP")) {
			value = CampaignContactSource.NETPROSPEX.name();
		} else {
			value = inputValue;
		}
		return value;
	}

	private String validateMultiTypeFieldValue(String sourceId, String fieldName) {
		String value = "";
		if (fieldName.equals("Postal Code")) {
			int index = sourceId.indexOf('.');
			if (index > 0) {
				value = sourceId.substring(0, index);
			}
		} else {
			value = sourceId;
		}
		return value;
	}

	private boolean isSourceIdAlreadyPurchased(String sourceId) {
		if (sourceId == null || sourceId == "") {
			return false;
		}
		CampaignContact campaignContact = null;
		try {
			campaignContact = campaignContactRepository.findBySourceId(sourceId, "ZOOMINFO");
		} catch (Exception e) {
			logger.error("Error occurred while quering campaign contact for sourceId [{}]", sourceId);
			logger.error("Exception is : ", e);
		}
		if (campaignContact == null) {
			return false;
		} else {
			return true;
		}
	}

	private double validateDoubleTypeFieldValue(double inputValue, String fieldName) {
		try {
			if (inputValue < 0) {
				throw new IllegalArgumentException("'" + fieldName + "' must be greater than 'zero'.");
			} else {
				return inputValue;
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("'" + inputValue + "' is not in correct format.");
		}
	}

	public String getStateWeight(String timeZone) {

		String est = "US/Eastern";
		String cst = "US/Central";
		String mst = "US/Mountain";
		String pst = "US/Pacific";

		if (timeZone != null && timeZone.equalsIgnoreCase(pst)) {
			return "1";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(mst)) {
			return "2";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(cst)) {
			return "3";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(est)) {
			return "4";
		}
		return "5";
	}

	public String getBoundry(Number n, String minMax) {
		if (minMax.equalsIgnoreCase("min")) {
			if (n.intValue() < 5) {
				return "1";
			}
			if (n.intValue() < 10) {
				return "5";
			}
			if (n.intValue() < 20) {
				return "10";
			}
			if (n.intValue() < 50) {
				return "20";
			}
			if (n.intValue() < 100) {
				return "50";
			}
			if (n.intValue() < 250) {
				return "100";
			}
			if (n.intValue() < 500) {
				return "250";
			}
			if (n.intValue() < 1000) {
				return "500";
			}
			if (n.intValue() < 5000) {
				return "1000";
			}
			if (n.intValue() < 10000) {
				return "5000";
			}

			if (n.intValue() < 11000) {
				return "10000";
			}

		} else if (minMax.equalsIgnoreCase("max")) {
			if (n.intValue() <= 5) {
				return "5";
			}
			if (n.intValue() <= 10) {
				return "10";
			}
			if (n.intValue() <= 20) {
				return "20";
			}
			if (n.intValue() <= 50) {
				return "50";
			}
			if (n.intValue() <= 100) {
				return "100";
			}
			if (n.intValue() <= 250) {
				return "250";
			}
			if (n.intValue() <= 500) {
				return "500";
			}
			if (n.intValue() <= 1000) {
				return "1000";
			}
			if (n.intValue() <= 5000) {
				return "5000";
			}
			if (n.intValue() <= 10000) {
				return "10000";
			}

			if (n.intValue() <= 100000) {
				return "11000";
			}

		}

		return "11000";
		/*
		 * 1-5 5-10 10-20 20-50 50-100 100-250 250-500 500-1000 1000-5000 5000-10000
		 * 10000-11000
		 */
	}

	public String getMgmtLevelWeight(String managementLevel) {
		if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "5";
		} else if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "4";
		} else if (managementLevel.equalsIgnoreCase("Director")) {
			return "3";
		} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
			return "2";
		} else {
			return "1";
		}

		// return "";

	}

	public String getEmployeeWeight(Number n) {

		if (n.intValue() < 5) {
			return "01";
		}
		if (n.intValue() < 10) {
			return "02";
		}
		if (n.intValue() < 20) {
			return "03";
		}
		if (n.intValue() < 50) {
			return "04";
		}
		if (n.intValue() < 100) {
			return "05";
		}
		if (n.intValue() < 250) {
			return "06";
		}
		if (n.intValue() < 500) {
			return "07";
		}
		if (n.intValue() < 1000) {
			return "08";
		}
		if (n.intValue() < 5000) {
			return "09";
		}
		if (n.intValue() < 10000) {
			return "10";
		}

		if (n.intValue() < 11000) {
			return "11";
		}

		return "12";
	}

	public String getMgmtLevel(String managementLevel) {
		if (managementLevel.equalsIgnoreCase("C-Level")) {
			return "VP-Level";
		} else if (managementLevel.equalsIgnoreCase("VP-Level")) {
			return "C-Level";
		} else if (managementLevel.equalsIgnoreCase("Director")) {
			return "DIRECTOR";
		} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
			return "MANAGER";
		} else {
			return "Non-Manager";
		}

	}

	class UploadedContact {

		private String contactId;
		
		private String sfdcCampaignId;
		
		private String firstName;
		
		private String lastName;
		
		private String title;
		
		private String department;

		private String email;	
		
		private String phone;
		
		private String organizationName;
		
		private List<ContactIndustry> industries;

		private boolean directPhone;

		private String managementLevel;
		
		private String domain;
		
		private double minRevenue;
		
		private double maxRevenue;
		
		private double minEmployeeCount;
		
		private double maxEmployeeCount;
		
		private String addressLine1;
		
		private String addressLine2;
		
		private String city;
		
		private String stateCode;
		
		private String country;
		
		private String postalCode;
		
		private String companyValidationLink;
		
		private String titleValidationLink;
		
		private String source;
		
		private String extension;

		public String getExtension() {
			return extension;
		}

		public void setExtension(String extension) {
			this.extension = extension;
		}

		public String getContactId() {
			return contactId;
		}

		public void setContactId(String contactId) {
			this.contactId = contactId;
		}

		public String getSfdcCampaignId() {
			return sfdcCampaignId;
		}

		public void setSfdcCampaignId(String sfdcCampaignId) {
			this.sfdcCampaignId = sfdcCampaignId;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDepartment() {
			return department;
		}

		public void setDepartment(String department) {
			this.department = department;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getOrganizationName() {
			return organizationName;
		}

		public void setOrganizationName(String organizationName) {
			this.organizationName = organizationName;
		}

		public List<ContactIndustry> getIndustries() {
			return industries;
		}

		public void setIndustries(List<ContactIndustry> industries) {
			this.industries = industries;
		}

		public boolean isDirectPhone() {
			return directPhone;
		}

		public void setDirectPhone(boolean directPhone) {
			this.directPhone = directPhone;
		}

		public String getManagementLevel() {
			return managementLevel;
		}

		public void setManagementLevel(String managementLevel) {
			this.managementLevel = managementLevel;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public double getMinRevenue() {
			return minRevenue;
		}

		public void setMinRevenue(double minRevenue) {
			this.minRevenue = minRevenue;
		}

		public double getMaxRevenue() {
			return maxRevenue;
		}

		public void setMaxRevenue(double maxRevenue) {
			this.maxRevenue = maxRevenue;
		}

		public double getMinEmployeeCount() {
			return minEmployeeCount;
		}

		public void setMinEmployeeCount(double minEmployeeCount) {
			this.minEmployeeCount = minEmployeeCount;
		}

		public double getMaxEmployeeCount() {
			return maxEmployeeCount;
		}

		public void setMaxEmployeeCount(double maxEmployeeCount) {
			this.maxEmployeeCount = maxEmployeeCount;
		}

		public String getAddressLine1() {
			return addressLine1;
		}

		public void setAddressLine1(String addressLine1) {
			this.addressLine1 = addressLine1;
		}

		public String getAddressLine2() {
			return addressLine2;
		}

		public void setAddressLine2(String addressLine2) {
			this.addressLine2 = addressLine2;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getStateCode() {
			return stateCode;
		}

		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

		public String getCompanyValidationLink() {
			return companyValidationLink;
		}

		public void setCompanyValidationLink(String companyValidationLink) {
			this.companyValidationLink = companyValidationLink;
		}

		public String getTitleValidationLink() {
			return titleValidationLink;
		}

		public void setTitleValidationLink(String titleValidationLink) {
			this.titleValidationLink = titleValidationLink;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		

	}

	@Override
	public ProspectCallLogDTO updateCampaignAudience(RestCampaignContactDTO restCampaignDto) {
		ProspectCallLog prospectFromDB = prospectCallLogRepository.findByProspectContactId(restCampaignDto.getContactId());
		
		if (prospectFromDB == null) {
			// retun exception
			throw new IllegalArgumentException("ProspectCallId is already exists.");
		} else {
			if (restCampaignDto.getFirstName() != null && !restCampaignDto.getFirstName().isEmpty()) {
				prospectFromDB.getProspectCall().getProspect().setFirstName(restCampaignDto.getFirstName());
			}
			if (restCampaignDto.getLastName() != null && !restCampaignDto.getLastName().isEmpty()) {
				prospectFromDB.getProspectCall().getProspect().setLastName(restCampaignDto.getLastName());
			}
			prospectFromDB.getProspectCall().getProspect().setTitle(restCampaignDto.getTitle());
			if (restCampaignDto.getDepartment() != null && !restCampaignDto.getDepartment().isEmpty()) {
				prospectFromDB.getProspectCall().getProspect().setDepartment(restCampaignDto.getDepartment());
			}
			if (restCampaignDto.getEmail() != null && !restCampaignDto.getEmail().isEmpty()) {
				prospectFromDB.getProspectCall().getProspect().setEmail(restCampaignDto.getEmail());
			}
			if (restCampaignDto.getCountry() != null && !restCampaignDto.getCountry().isEmpty()) {
				prospectFromDB.getProspectCall().getProspect().setCountry(restCampaignDto.getCountry());
			}
			if (restCampaignDto.getPhone() != null && !restCampaignDto.getPhone().isEmpty()
					&& isPhoneValid(restCampaignDto.getPhone(), restCampaignDto.getCountry())) {
				prospectFromDB.getProspectCall().getProspect().setPhone(restCampaignDto.getPhone());
			}
			if (restCampaignDto.getStateCode() != null && !restCampaignDto.getStateCode().isEmpty()) {
				StateCallConfig callConfig = stateCallConfigRepository.findByStateCode(restCampaignDto.getStateCode());
				if (callConfig != null) {
					prospectFromDB.getProspectCall().getProspect().setStateCode(callConfig.getStateCode());
				}
			}
			prospectFromDB.getProspectCall().getProspect().setIndustryList(restCampaignDto.getIndustryList());
			prospectFromDB.getProspectCall().getProspect().setDirectPhone(restCampaignDto.isDirectPhone());
			prospectFromDB.getProspectCall().getProspect().setManagementLevel(restCampaignDto.getManagementLevel());
			prospectFromDB.getProspectCall().getProspect().setAddressLine1(restCampaignDto.getAddressLine1());
			prospectFromDB.getProspectCall().getProspect().setAddressLine2(restCampaignDto.getAddressLine2());
			prospectFromDB.getProspectCall().getProspect().setCity(restCampaignDto.getCity());
			prospectFromDB.getProspectCall().getProspect().setSource(restCampaignDto.getSource());
			prospectFromDB.getProspectCall().getProspect().setExtension(restCampaignDto.getExtension());
			prospectFromDB.getProspectCall().getProspect()
					.setZoomCompanyUrl(restCampaignDto.getCompanyValidationLink());
			prospectFromDB.getProspectCall().getProspect().setZoomPersonUrl(restCampaignDto.getTitleValidationLink());
			prospectFromDB.getProspectCall().getProspect().setCompany(restCampaignDto.getOrganizationName());
			prospectFromDB.getProspectCall().getProspect().setZipCode(restCampaignDto.getPostalCode());
			
			prospectFromDB.getProspectCall().getProspect().getCustomAttributes().put("domain", restCampaignDto.getDomain());
			if( restCampaignDto.getMaxRevenue() > 0 && (restCampaignDto.getMinRevenue() < restCampaignDto.getMaxRevenue())) {
				KeyValuePair<Long, Long> revenueMap = getRevenueMap(restCampaignDto.getMinRevenue(),
						restCampaignDto.getMaxRevenue());
				prospectFromDB.getProspectCall().getProspect().getCustomAttributes().put("minRevenue", revenueMap.getKey());
				prospectFromDB.getProspectCall().getProspect().getCustomAttributes().put("maxRevenue", revenueMap.getValue());
			}
			if((restCampaignDto.getMinEmployeeCount() > 0 && restCampaignDto.getMaxEmployeeCount() > 0) && (restCampaignDto.getMinEmployeeCount() < restCampaignDto.getMaxEmployeeCount())) {
				KeyValuePair<Long, Long> employeeMap = getEmployeeMap(restCampaignDto.getMinEmployeeCount(),
						restCampaignDto.getMaxEmployeeCount());
				prospectFromDB.getProspectCall().getProspect().getCustomAttributes().put("minEmployeeCount", employeeMap.getKey());
				prospectFromDB.getProspectCall().getProspect().getCustomAttributes().put("maxEmployeeCount", employeeMap.getValue());
			}
			
			prospectCallLogRepository.save(prospectFromDB);
		}
		return new ProspectCallLogDTO(prospectFromDB);
	}

	@Override
	@Async
	public RestProspectResponseStatusDTO uploadData(List<RestCampaignContactDTO> campaignContactDTOs,
			String sfdcCampaignId) {
		if (dataUploadRequestMap.get(sfdcCampaignId) == null) {
			dataUploadRequestMap.put(sfdcCampaignId, "INPROGRESS");
		} else {
			throw new IllegalArgumentException(
					"Data upload is in progress for this campaign. Please try after sometime.");
		}
		Campaign cdpCampaign = campaignRepository.findCdpCampaign(sfdcCampaignId);
		if (cdpCampaign != null) {
			return ValidateData(campaignContactDTOs, sfdcCampaignId, cdpCampaign, true);
		} else {
			throw new IllegalArgumentException("Campaign does not exist in system.");
		}
	}
	
	@Async
	private RestProspectResponseStatusDTO ValidateData(List<RestCampaignContactDTO> campaignContacts,
			String sfdcCampaignId, Campaign tempCampaign, boolean isCdpCampaign) {
		int sucessCount = 0;
		int failureCount = 0;
		RestProspectResponseStatusDTO restProspectResponseStatusDTO = new RestProspectResponseStatusDTO();
		List<RestProspectResponseDTO> restProspectResponseDTOs = new ArrayList<>();
		if (campaignContacts != null && campaignContacts.size() > 0) {
			validateAndCreateContacts(tempCampaign, sfdcCampaignId, campaignContacts, sucessCount, failureCount,
					restProspectResponseDTOs, isCdpCampaign);
		}
		dataUploadRequestMap.remove(sfdcCampaignId);
		restProspectResponseStatusDTO.setErrorList(restProspectResponseDTOs);
		restProspectResponseStatusDTO.setSuccess(sucessCount);
		restProspectResponseStatusDTO.setFailed(failureCount);
		return restProspectResponseStatusDTO;
	}
	
	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d= new Date();
		//System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid+Long.toString(d.getTime());
		return pcidv;
	}

	
	private void sendErrorListEmail(String campaignId,List<RestProspectResponseDTO> restProspectResponseDTOs,int successCount, int failureCount) {
		StringBuffer sb = new StringBuffer();
		sb.append("Uploaded Successfully : ");
		sb.append(successCount);
		sb.append("<br>");
		sb.append("Failed to Upload : ");
		sb.append(failureCount);
		sb.append("<br>");
		sb.append("<br>");


		if(restProspectResponseDTOs!=null && restProspectResponseDTOs.size()>0) {
			for(RestProspectResponseDTO rprDTO : restProspectResponseDTOs) {
				sb.append("contactId : ");
				sb.append(rprDTO.getContactId());
				sb.append("--->");
				sb.append(rprDTO.getErrorList().stream().collect(Collectors.joining("<br>")));
				
			}
		}
		//String str = restProspectResponseDTOs.stream().collect(Collectors.joining("<br>"));
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		String subject = "CampaignId : "+campaignId +" : contact upload Status";
		String message = sb.toString();
		XtaasEmailUtils.sendEmail("pratap.yemul@invimatic.com", "data@xtaascorp.com", subject, message);
	}
 
	public boolean isPhoneValid(String phone, String country) {
		boolean valid = true;
		List<String> countries = Arrays.asList("US", "United States", "Canada");
		/**
		 * check for valid phone formats like "+123456489" "+1(850) 555-1234"
		 * "+1-850-555-1234" "+88 66 8880 1234" "3213123213"
		 */
		if (phone == null || phone.isEmpty()) {
			valid = false;
		} else if (!Pattern.compile("^[\\+\\d]?(?:[\\d-.\\s()]*)$").matcher(phone).matches()) {
			valid = false;
		} else if (country != null && countries.contains(country)
				&& !phone.matches("^\\(?([0-9]{3})\\)?[-]?([0-9]{3})[-.\\s]?([0-9]{4})$")) {
			valid = false;
		}
		return valid;
	}

	private KeyValuePair<Long, Long> getRevenueMap(Double minRevenue, Double maxRevenue) {
		Long zoomMinRev = new Long(0);
		Long zoomMaxRev = new Long(0);
		KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

		long minRev = minRevenue.longValue();
		long maxRev = maxRevenue.longValue();

		if (minRev >= 0 && minRev < 5000000) {
			// TODO - need to validate
			// zoomMinRev = new Long(1);
		} else if (minRev >= 5000000 && minRev < 10000000) {
			zoomMinRev = new Long(5000000);
		} else if (minRev >= 10000000 && minRev < 25000000) {
			zoomMinRev = new Long(10000000);
		} else if (minRev >= 25000000 && minRev < 50000000) {
			zoomMinRev = new Long(25000000);
		} else if (minRev >= 50000000 && minRev < 100000000) {
			zoomMinRev = new Long(50000000);
		} else if (minRev >= 100000000 && minRev < 250000000) {
			zoomMinRev = new Long(100000000);
		} else if (minRev >= 250000000 && minRev < 500000000) {
			zoomMinRev = new Long(250000000);
		} else if (minRev >= 500000000 && minRev < 1000000000) {
			zoomMinRev = new Long(500000000);
		} else if (minRev >= 1000000000 && minRev < new Long("5000000000").longValue()) {
			zoomMinRev = new Long(1000000000);
		} else if (minRev >= new Long("5000000000").longValue()) {
			zoomMinRev = new Long("5000000000").longValue();
		}

		if (maxRev > 0 && maxRev <= 5000000) {
			zoomMaxRev = new Long(5000000);
		} else if (maxRev > 5000000 && maxRev <= 10000000) {
			zoomMaxRev = new Long(10000000);
		} else if (maxRev > 10000000 && maxRev <= 25000000) {
			zoomMaxRev = new Long(25000000);
		} else if (maxRev > 25000000 && maxRev <= 50000000) {
			zoomMaxRev = new Long(50000000);
		} else if (maxRev > 50000000 && maxRev <= 100000000) {
			zoomMaxRev = new Long(100000000);
		} else if (maxRev > 100000000 && maxRev <= 250000000) {
			zoomMaxRev = new Long(250000000);
		} else if (maxRev > 250000000 && maxRev <= 500000000) {
			zoomMaxRev = new Long(500000000);
		} else if (maxRev > 500000000 && maxRev <= 1000000000) {
			zoomMaxRev = new Long(1000000000);
		} else if (maxRev > 1000000000 && maxRev <= new Long("5000000000").longValue()) {
			zoomMaxRev = new Long("5000000000").longValue();
		} else if (maxRev > new Long("5000000000").longValue()) {
			// TODO - need to validate
		}

		minMaxKeys.setKey(zoomMinRev);
		minMaxKeys.setValue(zoomMaxRev);
		return minMaxKeys;
	}

	private KeyValuePair<Long, Long> getEmployeeMap(Double minEmployee, Double maxEmployee) {

		Long zoomMinEmp = new Long(0);
		Long zoomMaxEmp = new Long(0);

		long minEmp = minEmployee.longValue();
		long maxEmp = maxEmployee.longValue();

		KeyValuePair<Long, Long> minMaxKeys = new KeyValuePair<Long, Long>();

		if (minEmp > 0 && minEmp < 5) {
			zoomMinEmp = new Long(1);
		} else if (minEmp >= 5 && minEmp < 10) {
			zoomMinEmp = new Long(5);
		} else if (minEmp >= 10 && minEmp < 20) {
			zoomMinEmp = new Long(10);
		} else if (minEmp >= 20 && minEmp < 50) {
			zoomMinEmp = new Long(20);
		} else if (minEmp >= 50 && minEmp < 100) {
			zoomMinEmp = new Long(50);
		} else if (minEmp >= 100 && minEmp < 250) {
			zoomMinEmp = new Long(100);
		} else if (minEmp >= 250 && minEmp < 500) {
			zoomMinEmp = new Long(250);
		} else if (minEmp >= 500 && minEmp < 1000) {
			zoomMinEmp = new Long(500);
		} else if (minEmp >= 1000 && minEmp < 5000) {
			zoomMinEmp = new Long(1000);
		} else if (minEmp >= 5000 && minEmp < 10000) {
			zoomMinEmp = new Long(5000);
		} else if (minEmp >= 10000) {
			zoomMinEmp = new Long(10000);
		}

		if (maxEmp > 0 && maxEmp <= 5) {
			zoomMaxEmp = new Long(5);
		} else if (maxEmp > 5 && maxEmp <= 10) {
			zoomMaxEmp = new Long(10);
		} else if (maxEmp > 10 && maxEmp <= 20) {
			zoomMaxEmp = new Long(20);
		} else if (maxEmp > 20 && maxEmp <= 50) {
			zoomMaxEmp = new Long(50);
		} else if (maxEmp > 50 && maxEmp <= 100) {
			zoomMaxEmp = new Long(100);
		} else if (maxEmp > 100 && maxEmp <= 250) {
			zoomMaxEmp = new Long(250);
		} else if (maxEmp > 250 && maxEmp <= 500) {
			zoomMaxEmp = new Long(500);
		} else if (maxEmp > 500 && maxEmp <= 1000) {
			zoomMaxEmp = new Long(1000);
		} else if (maxEmp > 1000 && maxEmp <= 5000) {
			zoomMaxEmp = new Long(5000);
		} else if (maxEmp > 5000 && maxEmp <= 10000) {
			zoomMaxEmp = new Long(10000);
		} else if (maxEmp > 10000) {
			// TODO - need to validate
		}

		minMaxKeys.setKey(zoomMinEmp);
		minMaxKeys.setValue(zoomMaxEmp);
		return minMaxKeys;
	}

	@Override
	public ResponseEntity<String> uploadInternalData(RestCdpCampaignContactDTO restCdpCampaignContactDTO) {
		if (restCdpCampaignContactDTO.getCampaignName() != null
				&& !restCdpCampaignContactDTO.getCampaignName().isEmpty()
				&& restCdpCampaignContactDTO.getOrganizationId() != null
				&& !restCdpCampaignContactDTO.getOrganizationId().isEmpty()) {
			Campaign campaignFromDB = campaignRepository.findByNameAndOrganizationId(
					restCdpCampaignContactDTO.getCampaignName(), restCdpCampaignContactDTO.getOrganizationId());
			if (campaignFromDB != null && campaignFromDB.getName() != null) {
				if (dataUploadRequestMap.get(campaignFromDB.getId()) == null) {
					dataUploadRequestMap.put(campaignFromDB.getId(), "INPROGRESS");
				} else {
					return new ResponseEntity<>("Data upload is in progress for this campaign. Please try after sometime.", HttpStatus.BAD_REQUEST);
				}
				ValidateData(restCdpCampaignContactDTO.getCampaignContacts(), campaignFromDB.getId(),
						campaignFromDB, false);
				return new ResponseEntity<>("Contacts upload started.", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Campaign does not exist in system.", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("Campaign name and organizationId is mandatory.", HttpStatus.BAD_REQUEST);
		}
	}
	
	private void validateAndCreateContacts(Campaign tempCampaign, String sfdcCampaignId,
			List<RestCampaignContactDTO> campaignContacts, int sucessCount, int failureCount,
			List<RestProspectResponseDTO> restProspectResponseDTOs, boolean isCdpCampaign) {
		logger.debug("uploadRestData() : validating data");
		List<UploadedContact> uploadedContactList = new ArrayList<>();
		List<CampaignContact> campaignContactList = new ArrayList<>();
		List<String> errorList = new ArrayList<>();
		for (RestCampaignContactDTO restCampaignContactDTO : campaignContacts) {
			List<ContactIndustry> contactIndustries = new ArrayList<>();
			RestProspectResponseDTO prospectResponseDTO = new RestProspectResponseDTO();
			prospectResponseDTO.setContactId(restCampaignContactDTO.getContactId());
			prospectResponseDTO.setSfdcCampaignId(restCampaignContactDTO.getSfdcCampaignId());
			CampaignContact tempCampaignContact = null;
			UploadedContact uploadedContact = new UploadedContact();
			boolean isRecordValid = true;
			if (restCampaignContactDTO.getContactId() != null && !restCampaignContactDTO.getContactId().isEmpty()) {
				uploadedContact.setContactId(restCampaignContactDTO.getContactId());
			} else {
				isRecordValid = false;
				errorList.add("ContactId should not empty.");
			}
			
			if (isCdpCampaign) {
				if (restCampaignContactDTO.getSfdcCampaignId() != null
						&& !restCampaignContactDTO.getSfdcCampaignId().isEmpty()) {
					if (restCampaignContactDTO.getSfdcCampaignId().equalsIgnoreCase(sfdcCampaignId)) {
						uploadedContact.setSfdcCampaignId(restCampaignContactDTO.getSfdcCampaignId());
					} else {
						isRecordValid = false;
						errorList.add("sfdcCampaignId and prospects sfdc campaignId are differnt.");
					}
				} else {
					isRecordValid = false;
					errorList.add("sfdcCampaignId should not empty.");
				}
			}

			if (restCampaignContactDTO.getFirstName() != null && !restCampaignContactDTO.getFirstName().isEmpty()) {
				String firstName = removeNonAsciiChars(restCampaignContactDTO.getFirstName());
				firstName = (firstName.length() < 250) ? firstName : firstName.substring(0, 250);
				uploadedContact.setFirstName(firstName);
			} else {
				isRecordValid = false;
				errorList.add("First Name should not empty.");
			}
			if (restCampaignContactDTO.getLastName() != null && !restCampaignContactDTO.getLastName().isEmpty()) {
				String lastName = removeNonAsciiChars(restCampaignContactDTO.getLastName());
				lastName = (lastName.length() < 250) ? lastName : lastName.substring(0, 250);
				uploadedContact.setLastName(lastName);
			} else {
				isRecordValid = false;
				errorList.add("Last Name should not empty.");
			}

			if (restCampaignContactDTO.getSource() != null && !restCampaignContactDTO.getSource().isEmpty()) {
				try {
					String source = XtaasUtils
							.removeNonAsciiChars(validateSourceFieldValue(restCampaignContactDTO.getSource()));
					uploadedContact.setSource(source);

				} catch (IllegalArgumentException e) {
					logger.debug("excelFileRead():validateExcelFileRecords() : " + e.getMessage());
					errorList.add(e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			}

			if (restCampaignContactDTO.getTitle() != null && !restCampaignContactDTO.getTitle().isEmpty()) {
				String title = removeNonAsciiChars(restCampaignContactDTO.getTitle());
				title = (title.length() < 250) ? title : title.substring(0, 250);
				uploadedContact.setTitle(title);
			}

			if (restCampaignContactDTO.getDepartment() != null && !restCampaignContactDTO.getDepartment().isEmpty()) {
				String department = removeNonAsciiChars(restCampaignContactDTO.getDepartment());
				department = (department.length() < 250) ? department : department.substring(0, 250);
				uploadedContact.setDepartment(department);
			} else {
				isRecordValid = false;
				errorList.add("Department should not empty.");
			}

			if (restCampaignContactDTO.getOrganizationName() != null
					&& !restCampaignContactDTO.getOrganizationName().isEmpty()) {
				String orgname = removeNonAsciiChars(restCampaignContactDTO.getOrganizationName());
				orgname = (orgname != null && orgname.length() < 250) ? orgname : orgname.substring(0, 250);
				uploadedContact.setOrganizationName(orgname);
			} else {
				isRecordValid = false;
				errorList.add("Organization name should not empty.");
			}

			if (restCampaignContactDTO.getDomain() != null) {
				String domain = removeNonAsciiChars(restCampaignContactDTO.getDomain());
				domain = (domain.length() < 250) ? domain : domain.substring(0, 250);
				uploadedContact.setDomain(domain);
			}

			if (restCampaignContactDTO.getMinRevenue() >= 0.0) {
				try {
					uploadedContact.setMinRevenue(
							validateDoubleTypeFieldValue(restCampaignContactDTO.getMinRevenue(), "Minimum Revenue"));
				} catch (Exception e) {
					errorList.add("Minimum revenue" + "" + e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			}

			if (restCampaignContactDTO.getMaxRevenue() > 0.0) {
				try {
					uploadedContact.setMaxRevenue(
							validateDoubleTypeFieldValue(restCampaignContactDTO.getMaxRevenue(), "Maximum Revenue"));
				} catch (Exception e) {
					errorList.add("Maximum revenue" + "" + e.getMessage());
					isRecordValid = false;
				}
			}

			if (restCampaignContactDTO.getMinEmployeeCount() >= 0.0) {
				try {
					uploadedContact.setMinEmployeeCount(validateDoubleTypeFieldValue(
							restCampaignContactDTO.getMinEmployeeCount(), "Minimum Employee Count"));
				} catch (IllegalArgumentException e) {
					errorList.add("Minimum Employee Count" + "" + e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			}

			if (restCampaignContactDTO.getMaxEmployeeCount() >= 0.0) {
				try {
					uploadedContact.setMaxEmployeeCount(validateDoubleTypeFieldValue(
							restCampaignContactDTO.getMaxEmployeeCount(), "Maximum Employee Count"));
				} catch (IllegalArgumentException e) {
					errorList.add("Maximum Employee Count" + "" + e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			}

			if (restCampaignContactDTO.getEmail() != null && !restCampaignContactDTO.getEmail().isEmpty()) {
				String email = removeNonAsciiChars(restCampaignContactDTO.getEmail());
				email = (email == null || email.length() < 250) ? email : email.substring(0, 250);
				uploadedContact.setEmail(email);
			} else {
				isRecordValid = false;
				errorList.add("email should not empty.");
			}

			if (restCampaignContactDTO.getAddressLine1() != null) {
				String addressLine1 = removeNonAsciiChars(restCampaignContactDTO.getAddressLine1());
				addressLine1 = (addressLine1.length() < 250) ? addressLine1 : addressLine1.substring(0, 250);
				uploadedContact.setAddressLine1(addressLine1);
			}

			if (restCampaignContactDTO.getAddressLine2() != null) {
				String addressLine2 = removeNonAsciiChars(restCampaignContactDTO.getAddressLine2());
				addressLine2 = (addressLine2.length() < 250) ? addressLine2 : addressLine2.substring(0, 250);
				uploadedContact.setAddressLine2(addressLine2);
			}

			if (restCampaignContactDTO.getCity() != null) {
				String city = removeNonAsciiChars(restCampaignContactDTO.getCity());
				city = (city == null || city.length() < 250) ? city : city.substring(0, 250);
				uploadedContact.setCity(city);
			}

			if (restCampaignContactDTO.getStateCode() != null && !restCampaignContactDTO.getStateCode().isEmpty()) {
				String stateCode = removeNonAsciiChars(restCampaignContactDTO.getStateCode());
				stateCode = (stateCode == null || stateCode.length() < 250) ? stateCode : stateCode.substring(0, 250);
				StateCallConfig callConfig = null;
				try {
					callConfig = stateCallConfigRepository.findByStateCode(stateCode);
					if (callConfig != null) {
						uploadedContact.setStateCode(stateCode);
					} else {
						isRecordValid = false;
						errorList.add("State Code " + stateCode + " dosen't exist.");
					}
				} catch (Exception e) {
					e.printStackTrace();
					dataUploadRequestMap.remove(sfdcCampaignId);
				}

			} else {
				isRecordValid = false;
				errorList.add("State Code should not empty.");
			}

			if (restCampaignContactDTO.getCountry() != null && !restCampaignContactDTO.getCountry().isEmpty()) {
				String country = removeNonAsciiChars(restCampaignContactDTO.getCountry());
				country = (country == null || country.length() < 250) ? country : country.substring(0, 250);
				uploadedContact.setCountry(country);
			} else {
				isRecordValid = false;
				errorList.add("Country should not empty");
			}

			if (restCampaignContactDTO.getPostalCode() != null) {
				try {
					uploadedContact.setPostalCode(
							validateMultiTypeFieldValue(restCampaignContactDTO.getPostalCode(), "Postal Code"));
				} catch (IllegalArgumentException e) {
					errorList.add(e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			}

			if (restCampaignContactDTO.getPhone() != null && !restCampaignContactDTO.getPhone().isEmpty()) {
				try {
					String phone = removeNonAsciiChars(
							validateMultiTypeFieldValue(restCampaignContactDTO.getPhone(), "Phone"));
					phone = (phone == null || phone.length() < 250) ? phone : phone.substring(0, 250);
					if ((phone.startsWith("91") || phone.startsWith("44") || phone.startsWith("33")
							|| phone.startsWith("49")) && !phone.startsWith("+")) {
						phone = "+" + phone;
					}
					uploadedContact.setPhone(phone);
				} catch (IllegalArgumentException e) {
					errorList.add(e.getMessage());
					isRecordValid = false;
					dataUploadRequestMap.remove(sfdcCampaignId);
				}
			} else {
				isRecordValid = false;
				errorList.add("Phone should not empty.");
			}

			if (restCampaignContactDTO.getManagementLevel() != null) {
				String managementLevel = removeNonAsciiChars(
						validateMultiTypeFieldValue(restCampaignContactDTO.getManagementLevel(), "ManagementLevel"));
				managementLevel = (managementLevel == null || managementLevel.length() < 250) ? managementLevel
						: managementLevel.substring(0, 250);
				uploadedContact.setManagementLevel(managementLevel);
			}

			uploadedContact.setDirectPhone(restCampaignContactDTO.isDirectPhone());

			if (restCampaignContactDTO.getCompanyValidationLink() != null) {
				String zoomCompanyUrl = XtaasUtils
						.removeNonAsciiChars(restCampaignContactDTO.getCompanyValidationLink());
				uploadedContact.setCompanyValidationLink(zoomCompanyUrl);
			}

			if (restCampaignContactDTO.getTitleValidationLink() != null) {
				String zoomPersonUrl = removeNonAsciiChars(restCampaignContactDTO.getTitleValidationLink());
				uploadedContact.setTitleValidationLink(zoomPersonUrl);
			}

			if (isRecordValid && isPersonRecordAlreadyPurchased(uploadedContact, tempCampaign.getId())) {
				logger.debug("contactListRead():validateListRecords()" + " Combination of First Name "
						+ uploadedContact.getFirstName() + " and Last Name " + uploadedContact.getLastName()
						+ " and Company Name " + uploadedContact.getOrganizationName() + " is already purchased");
				errorList.add(" Combination of First Name " + uploadedContact.getFirstName() + " and Last Name "
						+ uploadedContact.getLastName() + " and Company Name " + uploadedContact.getOrganizationName()
						+ " is already purchased");
				isRecordValid = false;
			}

			if (!campaignContactList.isEmpty()) {
				List<CampaignContact> filteredCampaignContactList = null;
				filteredCampaignContactList = campaignContactList.stream()
						.filter(cc -> cc.getFirstName().equals(restCampaignContactDTO.getFirstName())
								&& cc.getLastName().equals(restCampaignContactDTO.getLastName())
								&& cc.getOrganizationName().equals(restCampaignContactDTO.getOrganizationName())
								&& cc.getTitle().equals(restCampaignContactDTO.getTitle()))
						.collect(Collectors.toList());
				if (!filteredCampaignContactList.isEmpty()) {
					errorList.add(" Combination of First Name " + restCampaignContactDTO.getFirstName()
							+ " and Last Name " + restCampaignContactDTO.getLastName() + " and Company Name "
							+ restCampaignContactDTO.getOrganizationName() + " is already in List");
					isRecordValid = false;
				}
			}

			if (isRecordValid
					&& !isPhoneValid(restCampaignContactDTO.getPhone(), restCampaignContactDTO.getCountry())) {
				errorList.add("Phone number is Invalid.");
				isRecordValid = false;
			}

			if (restCampaignContactDTO.getMaxRevenue() > 0) {
				if (restCampaignContactDTO.getMaxRevenue() > restCampaignContactDTO.getMinRevenue()) {
					KeyValuePair<Long, Long> revenueMap = getRevenueMap(restCampaignContactDTO.getMinRevenue(),
							restCampaignContactDTO.getMaxRevenue());
					restCampaignContactDTO.setMinRevenue(revenueMap.getKey());
					restCampaignContactDTO.setMaxRevenue(revenueMap.getValue());
				} else {
					errorList.add("MaxRevenue should be greater than MinRevenue.");
					isRecordValid = false;
				}
			}

			if (restCampaignContactDTO.getMaxEmployeeCount() > 0.0
					&& restCampaignContactDTO.getMinEmployeeCount() > 0.0) {
				if (restCampaignContactDTO.getMaxEmployeeCount() > restCampaignContactDTO.getMinEmployeeCount()) {
					KeyValuePair<Long, Long> employeeMap = getEmployeeMap(restCampaignContactDTO.getMinEmployeeCount(),
							restCampaignContactDTO.getMaxEmployeeCount());
					restCampaignContactDTO.setMinEmployeeCount(employeeMap.getKey());
					restCampaignContactDTO.setMaxEmployeeCount(employeeMap.getValue());
				} else {
					errorList.add("Max employee count must be greater than min employee count.");
					isRecordValid = false;
				}
			}

			if (restCampaignContactDTO.getIndustryList() != null
					&& restCampaignContactDTO.getIndustryList().size() > 0) {
				for (String industry : restCampaignContactDTO.getIndustryList()) {
					ContactIndustry contactIndustry = new ContactIndustry();
					String industry1 = removeNonAsciiChars(industry);
					industry1 = (industry1 == null || industry1.length() < 250) ? industry1
							: industry1.substring(0, 250);
					contactIndustry.setName(industry1);
					contactIndustries.add(contactIndustry);
				}
				uploadedContact.setIndustries(contactIndustries);
			}

			if (isRecordValid) {
				tempCampaignContact = new CampaignContact(tempCampaign.getId(), uploadedContact.getSource(),
						getUniqueProspect(), null, uploadedContact.getFirstName(), uploadedContact.getLastName(), null,
						uploadedContact.getTitle(), uploadedContact.getDepartment(), null,
						uploadedContact.getOrganizationName(), uploadedContact.getDomain(),
						uploadedContact.getIndustries(), null, uploadedContact.getMinRevenue(),
						uploadedContact.getMaxRevenue(), uploadedContact.getMinEmployeeCount(),
						uploadedContact.getMaxEmployeeCount(), uploadedContact.getEmail(),
						uploadedContact.getAddressLine1(), uploadedContact.getAddressLine2(), uploadedContact.getCity(),
						uploadedContact.getStateCode(), uploadedContact.getCountry(), uploadedContact.getPostalCode(),
						uploadedContact.getPhone(), null, null, uploadedContact.getManagementLevel(), null, false);
				tempCampaignContact.setDirectPhone(uploadedContact.isDirectPhone());
				tempCampaignContact.setZoomCompanyUrl(uploadedContact.getCompanyValidationLink());
				tempCampaignContact.setZoomPersonUrl(uploadedContact.getTitleValidationLink());
				tempCampaignContact.setExtension(uploadedContact.getExtension());
				tempCampaignContact.setContactId(uploadedContact.getContactId());
				tempCampaignContact.setSfdcCampaignId(uploadedContact.getSfdcCampaignId());
				campaignContactList.add(tempCampaignContact);
				uploadedContactList.add(uploadedContact);
				sucessCount++;
			} else {
				prospectResponseDTO.setStatus("Failed");
				prospectResponseDTO.setErrorList(errorList);
				failureCount++;
			}
			if (prospectResponseDTO.getStatus() != null && prospectResponseDTO.getStatus().equalsIgnoreCase("Failed")) {
				restProspectResponseDTOs.add(prospectResponseDTO);
			}
		}
		// if(errorList != null && errorList.size() > 0) {
		sendErrorListEmail(sfdcCampaignId, restProspectResponseDTOs, sucessCount, failureCount);
		// }

		if (campaignContactList.size() > 0) {
			List<CampaignContact> campaignContacts1 = campaignContactRepository.saveAll(campaignContactList);
			if (campaignContacts1 != null && !campaignContacts1.isEmpty()) {
				List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
				List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();
				for (CampaignContact campaignContact : campaignContacts1) {
					Prospect prospect = campaignContact.toProspectFromExcel(null);
					prospect.setSource(campaignContact.getSource());
					prospect.setSourceId(campaignContact.getSourceId());
					prospect.setCampaignContactId(campaignContact.getId());
					ProspectCallLog prospectCallLog = new ProspectCallLog();
					ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaignContact.getCampaignId(),
							prospect);
					// prospectCallLog.setProspectCall(new
					// ProspectCall(UUID.randomUUID().toString(), campaignId, prospect));
					prospectCall.setContactId(campaignContact.getContactId());
					prospectCall.setSfdcCampaignId(campaignContact.getSfdcCampaignId());
					Optional<UploadedContact> contacts = uploadedContactList.stream()
							.filter(contact -> contact.getFirstName().equals(prospect.getFirstName())
									&& contact.getLastName().equals(prospect.getLastName())
									&& contact.getOrganizationName().equals(prospect.getCompany()))
							.findFirst();
//					if (contacts.isPresent()) {
//						prospectCall.setProspectVersion(contacts.get().getProspectVersion());
//					}

					StateCallConfig scc = stateCallConfigRepository
							.findByStateCode(prospectCall.getProspect().getStateCode());
					prospectCall.getProspect().setTimeZone(scc.getTimezone());

					StringBuffer sb = new StringBuffer();
					String emp = "";
					String mgmt = "";

					String tz = getStateWeight(scc.getTimezone());

					sb.append(prospectCallLog.getStatus());
					sb.append("|");
					if (prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number) {
						Number n1 = (Number) prospectCall.getProspect().getCustomAttributeValue("minEmployeeCount");
						sb.append(getBoundry(n1, "min"));
						sb.append("-");
						emp = getEmployeeWeight(n1);
					}

					if (prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
						Number n2 = (Number) prospectCall.getProspect().getCustomAttributeValue("maxEmployeeCount");
						sb.append(getBoundry(n2, "max"));
					}

					sb.append("|");
					sb.append(getMgmtLevel(prospectCall.getProspect().getManagementLevel()));
					mgmt = getMgmtLevelWeight(prospectCall.getProspect().getManagementLevel());
					String direct = "";
					if (prospectCall.getProspect().isDirectPhone()) {
						direct = "1";
					} else {
						direct = "2";
					}
					int qbSort = 0;
					if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
						String sortString = direct + tz + mgmt + emp;
						qbSort = Integer.parseInt(sortString);
					}
					prospectCall.setOrganizationId(tempCampaign.getOrganizationId());
					prospectCall.setQualityBucket(sb.toString());
					prospectCall.setQualityBucketSort(qbSort);
					prospectCallLog.setProspectCall(prospectCall);
					prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
					prospectCallLogs.add(prospectCallLog);
					ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
					prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
					prospectCallInteraction.setProspectCall(prospectCall);
					prospectCallInteractions.add(prospectCallInteraction);
				}
				prospectCallLogRepository.saveAll(prospectCallLogs);
				prospectCallInteractionRepository.saveAll(prospectCallInteractions);
				globalContactService.generateGlobalContact(prospectCallLogs);

			}
		}
	}
}
