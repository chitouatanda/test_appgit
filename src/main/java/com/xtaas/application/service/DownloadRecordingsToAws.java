package com.xtaas.application.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.xtaas.db.entity.FailedRecordingsToAWS;
import com.xtaas.db.repository.FailedRecordingToAWSRepository;
import com.xtaas.db.repository.ZoomTokenRepository;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

@Component
public class DownloadRecordingsToAws {

	private static final Logger logger = LoggerFactory.getLogger(DownloadRecordingsToAws.class);

	@Autowired
	private FailedRecordingToAWSRepository failedRecordingToAWSRepository;
	
	@Autowired
	private ZoomTokenRepository zoomTokenRepository;
	

	@Autowired
	private PropertyService propertyService;

	private AmazonS3 s3client;
//	private String clientRegion = "us-west-2"; // This should be part of campaign object
	private String bucketName = "xtaasrecordings"; // This should be part of campaign object.
	private String zoomTokenFolder = "zoomtoken";
	private String SUFFIX = "/";
	private String zoomTokenFileName = "zoomtoken.txt";

	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		try {
			if (s3client == null) {
				BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_ACCESS_KEY_ID,
						XtaasConstants.AWS_SECRET_KEY);
				s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
						.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			}
		} catch (Exception ex) {
			logger.error("Error while creating the connection with AWS stroage : {}", ex);
		}
		return s3client;
	}
	
	

	@Async
	public String downloadRecording(String recordingUrl, String prospectCallId, String campaignId, boolean isFailed) {
		//boolean  voiceToText = propertyService
		//		.getBooleanApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.VOICE_TO_TEXT.name(), false);
		String awsRecordingUrl = "";
		URL url = null;
		File rfile = null;
		String filePath = "";
		try {
			File dirFile = new File("recordings");
			if (!dirFile.exists()) {
				if (dirFile.mkdirs()) {
					logger.debug("Directory is created!");
				} else {
					logger.debug("Failed to create directory!");
				}
			}
			int lastIndex = recordingUrl.lastIndexOf("/");
			String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
			String rfileName = fileName + ".wav";
			awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
			filePath = "recordings/" + rfileName;
			url = new URL(recordingUrl);
			rfile = new File(filePath);
			fileDownloadAndUploadFile(url, rfile, filePath, prospectCallId, campaignId);
		} catch (Exception e) {
			try {
				// wait for a sec and attempt again
				Thread.sleep(2000);
				logger.error("Error occurred while downloading AWS Recording URL. Retrying downloading.");
				fileDownloadAndUploadFile(url, rfile, filePath, prospectCallId, campaignId);
			} catch (Exception ex) {
				if (!isFailed) {
					FailedRecordingsToAWS fra = new FailedRecordingsToAWS();
					fra.setCampaignId(campaignId);
					fra.setProspectCallId(prospectCallId);
					fra.setTwilioRecordingUrl(url.toString());
					failedRecordingToAWSRepository.save(fra);
				}
				logger.info(
						"Recoridng File not found, so logging in failedrecordingstoaws collection" + prospectCallId);
			}
		}
		
		try {
			//if(voiceToText)
			//	speechToTextTranscribe.convertSpeechToText(awsRecordingUrl);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return awsRecordingUrl;
	}

	public void fileDownloadAndUploadFile(URL url, File file, String filePath, String prospectCallId, String campaignId)
			throws Exception {
		InputStream input = url.openStream();
		if (file.exists()) {
			if (file.isDirectory())
				throw new IOException("File '" + file + "' is a directory");

			if (!file.canWrite())
				throw new IOException("File '" + file + "' cannot be written");
		} else {
			File parent = file.getParentFile();
			if ((parent != null) && (!parent.exists()) && (!parent.mkdirs())) {
				throw new IOException("File '" + file + "' could not be created");
			}
		}
		FileOutputStream output = new FileOutputStream(file);
		byte[] buffer = new byte[4096];
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
		input.close();
		output.close();
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		logger.debug("File [{}] downloaded successfully.", file);
		s3client.putObject(
				new PutObjectRequest(bucketName, filePath, file).withCannedAcl(CannedAccessControlList.PublicRead));
		deleteFile(file);
	}
	

	private void deleteFile(File file) {
		file.delete();
	}

	public String createFile(String bucketName, String fileName, InputStream stream, boolean isPublic)
			throws IOException {
		logger.debug("Creating file with name {} and public status {}", fileName, isPublic);
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType("audio/mpeg"); // Content type is set because recordingurl should play not download in the browser.
		byte[] bytes = IOUtils.toByteArray(stream);
		metadata.setContentLength(bytes.length);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, byteArrayInputStream, metadata);
		if (isPublic) {
			putObjectRequest = putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
		}
		s3client.putObject(putObjectRequest);
		return String.format(XtaasConstants.S3_FILE_URL_TEMPLATE, bucketName, fileName);
	}
	
	public String uploadSuppresstionExcelFile(String bucketName, String fileName, File file)
			throws IOException {
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		s3client.putObject(new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
		deleteFile(file);
		return String.format(XtaasConstants.S3_FILE_URL_TEMPLATE, bucketName, fileName);
	}
	
	/*public String createCSVFile(String bucketName, String fileName, byte[] bytes, boolean isPublic)
			throws IOException {
		logger.debug("Creating file with name {} and public status {}", fileName, isPublic);
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType("text/csv"); // Content type is set because recordingurl should play not download in the browser.
		//byte[] bytes = IOUtils.toByteArray(stream);
		metadata.setContentLength(bytes.length);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, byteArrayInputStream, metadata);
		if (isPublic) {
			putObjectRequest = putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
		}
		s3client.putObject(putObjectRequest);
		return String.format(XtaasConstants.S3_FILE_URL_TEMPLATE, bucketName, fileName);
	}*/
	
	public String uploadCSVFile(String bucketName,File file, String fileName,String filePath) {
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		logger.debug("File [{}] downloaded successfully.", file);
		s3client.putObject(
				new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
		deleteFile(file);

		return String.format(XtaasConstants.S3_FILE_URL_TEMPLATE, bucketName, fileName);


	}
	
	public void updateZoomTokenToS3(String token) {
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		// create meta-data for your folder and set content-length to 0
	    ObjectMetadata metadata = new ObjectMetadata();
	    byte[] byteArrray = token.getBytes();
	    InputStream zoomtoken = new ByteArrayInputStream(byteArrray);
	    // create a PutObjectRequest passing the folder name suffixed by /
	    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
	    		zoomTokenFolder + SUFFIX+zoomTokenFileName, zoomtoken, metadata);

		s3client.putObject(putObjectRequest);

	}
	
	public void getZoomTokenFromS3(int retry) throws Exception {
		String oAuth = "";
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		Date currDate = new Date();
		List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
		ZoomToken ezt = zoomTokenList.get(0);
		long duration = currDate.getTime() - ezt.getCreatedDate().getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			oAuth = getZoomTokenFromS3();
			if (oAuth != null && !oAuth.isEmpty()  &&  !ezt.getOauthToken().equals(oAuth)) {
				ezt.setStatus("INACTIVE");
				zoomTokenRepository.save(ezt);
				ZoomToken newZoomToken = new ZoomToken();
				newZoomToken.setOauthToken(oAuth);
				newZoomToken.setStatus("ACTIVE");
				zoomTokenRepository.save(newZoomToken);
			} else {
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
				retry = 1;
				//System.out.println("Retrying");
				Thread.sleep(60000);
			}
		} /*
			 * else{ // oAuth = zt.getOauthToken(); // tokenKey = oAuth; }
			 */
		// return oAuth;

	}
	
	private String getZoomTokenFromS3() {
		createAWSConnection(XtaasConstants.CLIENT_REGION);
		StringBuilder zoomtoken = new StringBuilder();
		try {
			S3Object object = s3client.getObject(new GetObjectRequest(bucketName, zoomTokenFolder + SUFFIX+zoomTokenFileName));
			//Date lastUpdatedTime = object.getObjectMetadata().getLastModified();
			InputStream inputStream = object.getObjectContent();
			try (Reader reader = new BufferedReader(new InputStreamReader
			      (inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
			        int c = 0;
			        while ((c = reader.read()) != -1) {
			        	zoomtoken.append((char) c);
			        }
			    }
			inputStream.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return zoomtoken.toString();
	}
	
	
	public String downloadTelnyxRecording(String recordingUrl, String prospectCallId, String campaignId, boolean isFailed) {
		//boolean  voiceToText = propertyService
		//		.getBooleanApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.VOICE_TO_TEXT.name(), false);
		String awsRecordingUrl = "";
		URL url = null;
		File rfile = null;
		String filePath = "";
		try {
			File dirFile = new File("recordings");
			if (!dirFile.exists()) {
				if (dirFile.mkdirs()) {
					logger.debug("Directory is created!");
				} else {
					logger.debug("Failed to create directory!");
				}
			}
			int lastIndex = recordingUrl.lastIndexOf("/");
			int finalIndex = recordingUrl.indexOf("?");
			String rfileName = recordingUrl.substring(lastIndex + 1, finalIndex);
			//String rfileName = fileName + ".wav";
			awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
			filePath = "recordings/" + rfileName;
			url = new URL(recordingUrl);
			rfile = new File(filePath);
			fileDownloadAndUploadFile(url, rfile, filePath, prospectCallId, campaignId);
		} catch (Exception e) {
			try {
				// wait for a sec and attempt again
				Thread.sleep(2000);
				logger.error("Error occurred while downloading AWS Recording URL. Retrying downloading.");
				fileDownloadAndUploadFile(url, rfile, filePath, prospectCallId, campaignId);
			} catch (Exception ex) {
				if (!isFailed) {
					FailedRecordingsToAWS fra = new FailedRecordingsToAWS();
					fra.setCampaignId(campaignId);
					fra.setProspectCallId(prospectCallId);
					fra.setTwilioRecordingUrl(url.toString());
					failedRecordingToAWSRepository.save(fra);
				}
				logger.info(
						"Recoridng File not found, so logging in failedrecordingstoaws collection" + prospectCallId);
			}
		}
		
		try {
			//if(voiceToText)
			//	speechToTextTranscribe.convertSpeechToText(awsRecordingUrl);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return awsRecordingUrl;
	}


	public S3Object getFileBucketConnection(String bucketName, String fileName) throws Exception {
		BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_ACCESS_KEY_ID,
				XtaasConstants.AWS_SECRET_KEY);
		AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(XtaasConstants.CLIENT_REGION_EAST)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
		S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName, fileName));
		return s3object;
	}


	public AmazonS3 getFileBucketConnectionIMUser() throws Exception {
		BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_IAM_ACCESS_KEY_ID,
				XtaasConstants.AWS_IAM_SECRET_KEY);
		AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(XtaasConstants.CLIENT_REGION_EAST)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
		return s3client;
	}


}
