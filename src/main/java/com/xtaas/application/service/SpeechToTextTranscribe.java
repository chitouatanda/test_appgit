package com.xtaas.application.service;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
//import com.amazonaws.services.transcribe.AmazonTranscribe;
//import com.amazonaws.services.transcribe.AmazonTranscribeClient;
//import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest;
//import com.amazonaws.services.transcribe.model.LanguageCode;
//import com.amazonaws.services.transcribe.model.Media;
//import com.amazonaws.services.transcribe.model.Settings;
//import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest;
//import com.amazonaws.services.transcribe.model.TranscriptionJob;
//import com.amazonaws.services.transcribe.model.TranscriptionJobStatus;
import com.xtaas.db.repository.VoiceMailRepository;
import com.xtaas.domain.entity.VoiceMail;
//import com.xtaas.db.entity.ProspectCallLog;
//import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.valueobjects.AmazonTranscription;
import com.xtaas.valueobjects.Transcript;

//@Component
public class SpeechToTextTranscribe {
//	private static final Logger logger = LoggerFactory.getLogger(SpeechToTextTranscribe.class);
//
//	String clientRegion = "us-west-2";// This should be part of campaign object
//	String bucketName = "insideup";// This should be part of campaign object.
//	String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
//	String awsSecretKey = "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";
//	String speechBucket = "xtaasspeechtotext";
//	String recordingBucketName = "xtaasrecordings";
//	/////////////// Totem urls
//	String totem_base_url = "https://www.totemify.com";
//	String totme_reate_url = "/api/v1/graph/campaign/create";
//	String totem_score_url = "/api/v1/graph/campaign/score";
//	String totem_update_url = "/api/v1/graph/campaign/update";
//	String totem_list_url = "/api/v1/graph/list";
//	String totem_delete_url = "/api/v1/graph/delete";
//	///////// Totem urls
//	String totem_token = "3854b6909c4d8f74411e9802f0aa041c63c06";
//	String totem_api_key = "6a02c13cf74411e9802f0aa041c63c066a02c13df74411e9802f0aa041c63c06";
//	private static final String SUFFIX = "/";
//	boolean flag = false;
//	
//	@Autowired
//	VoiceMailRepository voiceMailRepository;
//
//	@Async
//	public String convertSpeechToText(String awsurl, VoiceMail voiceMail) throws Exception {
//
//		System.setProperty("aws.accessKeyId", "AKIAJZ5EWMT6OAKGOGNQ");
//		System.setProperty("aws.secretKey", "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");
//		//String oAuth = "";
//		AmazonS3 s3client = createAWSConnection(clientRegion);
//		AmazonTranscribe client = AmazonTranscribeClient.builder().withRegion(clientRegion).build();
//		
//		
//		
//	//	if(prospectCallLogList!=null) {
//		//	for(ProspectCallLog pcl : prospectCallLogList) {
//				StringBuffer speechText = new StringBuffer();
//
//				
//				//campaignList.add(campaignx);
//		       // String awsurl = pcl.getProspectCall().getRecordingUrlAws();
//		        String rFolder = "";
//		        String recordingFileName = "";
//				if (awsurl != null) {
//					String[] urlarr = awsurl.split(SUFFIX);
//					rFolder = urlarr[4];
//					recordingFileName = urlarr[5];
//				}
//		        boolean isFileExistsInS3 = s3client.doesObjectExist(recordingBucketName, "recordings" + SUFFIX + recordingFileName);
//		        if (isFileExistsInS3) {
//		        	
//		            String transcriptionJobName = recordingFileName+Math.random();
//		        	int endIndex = recordingFileName.length()-4;
//                	String speechFile = recordingFileName.substring(0, endIndex);
//                	speechFile = speechFile+".json";
//                	//System.out.println(pcl.getProspectCall().getProspectCallId()+"--->"+speechFile);
//					//boolean fileDownloaded = s3client.doesObjectExist(speechBucket, speechFile);
//					//if(!fileDownloaded) {
//						StartTranscriptionJobRequest request = new StartTranscriptionJobRequest();
//	
//			            Media media = new Media();
//	
//			            media.setMediaFileUri(s3client.getUrl(recordingBucketName, "recordings" + SUFFIX + recordingFileName).toString());
//	
//			            request.withMedia(media).withMediaSampleRateHertz(8000);
//						
//						request.withLanguageCode(LanguageCode.EnUS);
//			            request.setTranscriptionJobName(transcriptionJobName);
//			            request.withMediaFormat("wav");
//			            Settings settings = new Settings();
//			            settings.setChannelIdentification(true);
//			            request.setSettings(settings);
//			            client.startTranscriptionJob(request);
//			            
//			            
//			            GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest();
//			            jobRequest.setTranscriptionJobName(transcriptionJobName);
//			            TranscriptionJob transcriptionJob;
//	
//			            while( true ){
//			                transcriptionJob = client.getTranscriptionJob(jobRequest).getTranscriptionJob();
//			                if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.COMPLETED.name()) ){
//	
//			                	AmazonTranscription transcription = this.download( transcriptionJob.getTranscript().getTranscriptFileUri());
//			                	if(transcription!=null && transcription.getResults()!=null && transcription.getResults().getTranscripts() !=null 
//			                			&& transcription.getResults().getTranscripts().size()>0) {
//			                		List<Transcript> transcripts = transcription.getResults().getTranscripts();
//			                		if(transcripts !=null && transcripts.size()>0) {
//			                			for(Transcript transcript : transcripts) {
//			                			//	speechText.append("{");
//			                			//	speechText.append("\"transcript\": ");
//			                			//	speechText.append("\"");
//			                				speechText.append(transcript.getTranscript());
//			                				//speechText.append("\"");
//			                			//	speechText.append("}");
//
//			                			}
//			                		}
//			                		
//			                	}
//			                
//			                  //  System.out.println("------->"+speechText.toString());
//			                    
//			                  //  s3client.putObject(speechBucket, speechFile, speechText.toString());
//	
//			                	if(speechText==null ||  speechText.toString().isEmpty()) {
//			                	
//			                		speechText.append("EMPTY");
//			                	}
//			                    
//			                    break;
//	
//			                }else if( transcriptionJob.getTranscriptionJobStatus().equals(TranscriptionJobStatus.FAILED.name()) ){
//			                	speechText.append("ERROR OCCURED WHY CONVERTING VOICE TO TEXT.");
//			                        break;
//			                }
//			                // to not be so anxious
//			               /* synchronized ( this ) {
//			                    try {
//			                        this.wait(50);
//			                    } catch (InterruptedException e) { }
//			                }*/
//	
//			            }
//				//	}
//		        }else{
//		       	 //createFolder(bucketName,dateFolder,s3client);
//		        	speechText.append("RECORDING FILE NOT FOUND ON S3 STORAGE.");
//		        }
//				
//				
//		//	}
//	//	}
//		if (speechText != null 
//				&& !speechText.toString().equalsIgnoreCase("ERROR OCCURED WHY CONVERTING VOICE TO TEXT.")) {
//			voiceMail.setSpeech(speechText.toString());
//			voiceMail.setIsTranscribed(true);
//			voiceMailRepository.save(voiceMail);
//			logger.debug("Successfully transcribed the voice mail having Id : " + voiceMail.getId());
//		}
//		        return speechText.toString();
//	}
//
//	private AmazonTranscription getTranscription(String response) {
//		AmazonTranscription amazonTranscription = null;
//		try {
//			if (response != null) {
//				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
//				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//				amazonTranscription = mapper.readValue(response.toString(), AmazonTranscription.class);
//
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return amazonTranscription;
//	}
//
//	private AmazonTranscription download(String uri) {
//		CloseableHttpClient httpclient = HttpClients.createDefault();
//		AmazonTranscription transcription = null;
//		try {
//			/*
//			 * HttpGet httpget = new HttpGet(uri);
//			 * 
//			 * HttpResponse httpresponse = httpclient.execute(httpget);
//			 * httpresponse.setHeader("Accept-Encoding","UTF-8"); result =
//			 * httpresponse.charset("UTF-8").bodyText();
//			 */
//			URL url = new URL(uri);
//			URLConnection con = url.openConnection();
//			InputStream in = con.getInputStream();
//			String encoding = con.getContentEncoding();
//			encoding = encoding == null ? "UTF-8" : encoding;
//			String result = IOUtils.toString(in, encoding);
//			System.out.println(result);
//			transcription = getTranscription(result);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		// result is a json
//		return transcription;
//	}
//
//	private AmazonS3 createAWSConnection(String clientRegion) {
//		AmazonS3 s3client = null;
//		try {
//			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
//			s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
//					.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
//			return s3client;
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println(
//					"Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :"
//							+ e);
//			return s3client;
//		}
//	}
}
