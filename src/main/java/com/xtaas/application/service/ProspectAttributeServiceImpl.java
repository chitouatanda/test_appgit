package com.xtaas.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.ProspectAttributeRepository;
import com.xtaas.domain.entity.ProspectAttribute;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;

@Service
public class ProspectAttributeServiceImpl implements ProspectAttributeService {
	@Autowired
	private ProspectAttributeRepository prospectAttributeRepository;
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public List<ProspectAttribute> getProspectAttributes() {
		return prospectAttributeRepository.findAll();
	}

}
