package com.xtaas.application.service;


import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.data.domain.Pageable;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.AgentCallMonitoringDTO;
import com.xtaas.web.dto.AgentSearchDTO;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.ProspectCallDTO;

public interface AgentService {
	public List<AgentSearchResult> searchByCriteria(AgentSearchDTO agentSearchDTO, Pageable pageRequest);
	public List<Object> countByCriteria(AgentSearchDTO agentSearchDTO);
	public List<AgentSearchResult> searchByAutosuggest(String campaignId, Pageable pageRequest);
	public List<AgentSearchResult> searchByPreviousCampaigns(String campaignId);
	public List<AgentSearchResult> searchByCampaign(String campaignId);
	public List<AgentSearchResult> searchByCurrentSelection(String campaignId);
	public List<AgentSearchResult> searchByCurrentInvitations(String campaignId, Pageable pageRequest);
	public List<AgentSearchResult> searchBySupervisor();
	//Added method for qafilter change
	public List<AgentSearchResult> searchByQa(AgentSearchDTO agentSearchDTO) throws ParseException;
	public List<AgentSearchResult> searchBySecQa(AgentSearchDTO agentSearchDTO) throws ParseException;
	
	public AgentCampaignDTO getCurrentAllocatedCampaign();
	public void getCurrentAllocatedProspect();
	public AgentCampaignDTO getCurrentAllocatedCampaign(String agentUserName);
	public AgentDTO createAgent(AgentDTO agentDTO);
	public AgentDTO updateAgent(String agentId, AgentDTO agentDTO);
	public void addWorkSchedule(String agentId, WorkSchedule workSchedule);
	public void deleteWorkSchedule(String agentId, String scheduleId);
	public Agent getAgent(String agentId);
	public void changeStatus(String agentId, AgentStatus agentStatus, String agentType, String campaignId);
	public void changeStatus(AgentStatus agentStatus, String agentType, String campaignId);
	public void changeStatusOnBrowserClosed(String campaignId, String agentType, HttpSession session);
	public void submitProspectCall(ProspectCallDTO prospectCallDTO);
	public List<Agent> getAgents(List<String> agentIds);
	void changeCallStatus(CallStatusDTO callStatusDTO);
	public String getTwilioCapabilityToken(String currentLoggedInUsername, boolean isWhisperChannel);
	void changeCallStatus(String agentId, CallStatusDTO callStatusDTO);
	public void allocateAgentFromDialer(String callSid, boolean callAbandon);
	public void updateAgentMode(String agentId, String agentMode);
	public void approveAgentStatusChangeRequestBySupervisor(String agentId, AgentStatus agentStatus, String agentType, String campaignId, boolean job);
	public void handleAgentStatusChangeRequest(ProspectCall prospectCallFromAgent);
	public AgentCampaignDTO getAgentCampaign( String campaignId);
	public CallbackProspectCacheDTO getMissedCallbackProspect(String agentId);
	public void agentCallMonitoring(AgentCallMonitoringDTO agentCallMonitoringDTO);
	public Boolean clearAgentsInMemory();
}
