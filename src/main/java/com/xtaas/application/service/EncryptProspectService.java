package com.xtaas.application.service;

import java.util.List;

import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLogEnc;
import com.xtaas.domain.entity.PcLogNew;

public interface EncryptProspectService {

	public List<ProspectCallLogEnc> getAllProspects(String campaignId);

	public void createProspect(PcLogNew encryptProspect);

}
