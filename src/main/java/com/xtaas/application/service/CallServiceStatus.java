package com.xtaas.application.service;

public interface CallServiceStatus {
	
	public void processCallStatus(String callSid, String callStatus);

}
