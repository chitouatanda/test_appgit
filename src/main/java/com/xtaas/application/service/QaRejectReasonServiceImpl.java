package com.xtaas.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.QaRejectReason;
import com.xtaas.db.repository.QaRejectReasonRepository;

@Service
public class QaRejectReasonServiceImpl implements QaRejectReasonService {

	@Autowired
	private QaRejectReasonRepository qaRejectReasonRepository;

	@Override
	public QaRejectReason getQaRejectReason(String id) {
		return qaRejectReasonRepository.findOneByType(id);
	}

}
