/**
 * 
 */
package com.xtaas.application.service;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DuplicateKeyException;

import com.xtaas.db.entity.Login;
import com.xtaas.web.dto.AgentAllocationDTO;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.QaFeedbackFormDTO;

/**
 * @author djain
 *
 */
public interface SupervisorService {

	public AgentAllocationDTO getAgentAllocation();

	public boolean createFeedbackForm(QaFeedbackFormDTO qaFeedbackFormDTO) throws DuplicateKeyException;

	public void pushAgentMetricsToSupervisor(String agentId, CallStatusDTO callStatusDTO);

	public void downloadCampaignReport(Login login) throws ParseException, Exception;

	public void downloadAgentReport(Login login) throws ParseException, Exception;

	void inviteSupervisorToJoinCall(String agentId);

	public int moveDataToCallables(String campaignId);
	
	public String downloadCampaignReportCheckAndSave(Login login) throws ParseException, Exception;

	public String downloadAgentReportCheckAndSave(Login login) throws ParseException, Exception;
}
