package com.xtaas.application.service;

import java.util.List;

import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;

public interface TelnyxOutboundNumberService {

	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool);

	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId);

	public List<TelnyxOutboundNumber> getAllOutboundNumbers();

	public List<CountryDetails> getDistinctCountryInformation();

	public Boolean refreshOutboundNumbers();

	public List<TelnyxOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber);

	public TelnyxOutboundNumber save(TelnyxOutboundNumber telnyxOutboundNumber);
	
	public List<TelnyxOutboundNumber> deleteManyByTelnyxPhoneNumber(String phoneNumber);

}