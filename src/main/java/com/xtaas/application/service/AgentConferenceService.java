package com.xtaas.application.service;

import com.xtaas.domain.entity.AgentConference;

public interface AgentConferenceService {

  public String findAgentIdByConferenceId(String conferenceId);

  public AgentConference findConferenceIdByAgentId(String agentId);

  public void saveAgentConference(AgentConference agentConference);
  
  public AgentConference findByConferenceId(String conferenceId);

}