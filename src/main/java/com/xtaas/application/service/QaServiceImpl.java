package com.xtaas.application.service;

import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Domain;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.entity.Vertical;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.QaRepository;
import com.xtaas.db.repository.RealTimeDeliveryRepository;
import com.xtaas.db.repository.SuppressionListNewRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.db.repository.VerticalRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Qa;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CampaignSearchResult;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.domain.valueobject.QaDetails;
import com.xtaas.domain.valueobject.QaFeedback;
import com.xtaas.domain.valueobject.RealTimePost;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.mvc.model.ProspectCallSearch;
import com.xtaas.service.LeadReportServiceImpl;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.SuppressionListService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.FeedbackSectionResponseDTO;
import com.xtaas.web.dto.ProspectCallQaSearchResult;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.QaDTO;
import com.xtaas.web.dto.QaFeedbackDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;

@Service
public class QaServiceImpl implements QaService {
	private static final Logger logger = LoggerFactory.getLogger(QaServiceImpl.class);
	@Autowired
	private QaRepository qaRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private AgentRepository agentRepository;
	
	@Autowired
	private RealTimeDeliveryRepository realTimeDeliveryRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
    private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private VerticalRepository verticalRepository;

	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
    private LeadReportServiceImpl leadReportServiceImpl;
	
	@Autowired
    private OrganizationRepository organizationRepository;
	
	@Autowired
	private CampaignService campaignService;

	@Autowired
	SuppressionListNewRepository suppressionListNewRepository;

	@Autowired
	private SuppressionListService suppressionListService;

	@Override
	public QaDTO createQa(QaDTO qaDTO) {
		Qa qa = new Qa(qaDTO.getPartnerId(), qaDTO.getFirstName(), qaDTO.getLastName(),
				qaDTO.getAddress() != null ? qaDTO.getAddress().toAddress() : null);
		qa.setUsername(qaDTO.getUsername());
		qa.setOverview(qaDTO.getOverview());
		qa.setPerHourPrice(qaDTO.getPerHourPrice());
		if (qaDTO.getDomains() != null) {
			for (String domain : qaDTO.getDomains()) {
				qa.addDomain(domain);
			}
		}
		if (qaDTO.getLanguages() != null) {
			for (String language : qaDTO.getLanguages()) {
				qa.addLanguage(language);
			}
		}
		LocalDate date = new LocalDate(2030, 05, 30);
		ScheduleRecurrence recurrence = new ScheduleRecurrence(new Date(), date.toDate(), RecurrenceTypes.Daily, null);
		WorkSchedule workSchedule = new WorkSchedule(new Date(), 9, 20, recurrence);
		qa.addWorkSchedule(workSchedule);
		qa = qaRepository.save(qa);
		return new QaDTO(qa);
	}

	@Override
	public QaDTO updateQa(String qaId, QaDTO qaDTO) {
		Qa qa = qaRepository.findOneById(qaId);
		Login login = loginRepository.findOneById(qaId);
		if (!qa.getFirstName().equals(qaDTO.getFirstName()) || !qa.getLastName().equals(qaDTO.getLastName())) {
			
			login.setFirstName(qa.getFirstName());
			login.setLastName(qa.getLastName());
			loginRepository.save(login);
		}
		qa.setFirstName(qaDTO.getFirstName());
		qa.setLastName(qaDTO.getLastName());
		qa.setAddress(qaDTO.getAddress() != null ? qaDTO.getAddress().toAddress() : null);
		qa.setOverview(qaDTO.getOverview());
		qa.setPerHourPrice(qaDTO.getPerHourPrice());
		List<String> domains = qaDTO.getDomains();
		if (domains != null && !domains.isEmpty()) {
			List<Vertical> verticals = verticalRepository.findAll();
			HashMap<String, ArrayList<Domain>> mappedVerticals = convertVerticalListToVerticalMap(verticals);
			List<String> ObjAvailbaleDomains = qa.getDomains();
			for (String domain : domains) {
				if (!ObjAvailbaleDomains.contains(domain)) {// check domain is available in given QA object
					checkDomainValid(mappedVerticals, domain);
					qa.addDomain(domain);
				}
			}
		}
		if (qaDTO.getLanguages() != null) {
			List<String> ObjAvailbaleLanguages = qa.getLanguages();
			for (String language : qaDTO.getLanguages()) {
				if (!ObjAvailbaleLanguages.contains(language)) {// check language is available in given QA object
					qa.addLanguage(language);
				}
			}
		}
		qa = qaRepository.save(qa);
		return new QaDTO(qa);
	}

	private HashMap<String, ArrayList<Domain>> convertVerticalListToVerticalMap(List<Vertical> verticals) {
		HashMap<String, ArrayList<Domain>> mappedVerticals = new HashMap<String, ArrayList<Domain>>();
		for (Vertical vertical : verticals) {
			mappedVerticals.put(vertical.getName(), vertical.getDomains());
		}
		return mappedVerticals;
	}

	private void checkDomainValid(HashMap<String, ArrayList<Domain>> mappedVerticals, String verticalAndDomainPair) {
		if (!verticalAndDomainPair.contains("|")) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomainPair + "\" seperator(|) is not present in a given domain");
		}
		String verticalAndDomain[] = verticalAndDomainPair.split("\\|");
		if (verticalAndDomain.length != 2) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomainPair + "\" this domain does not contain valid Vertical Domain pair");
		}
		if (!mappedVerticals.containsKey(verticalAndDomain[0])) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomain[0] + "\" this vertical does not exist in the system");
		}
		ArrayList<Domain> domains = mappedVerticals.get(verticalAndDomain[0]);
		boolean isDomainAvailable = false;
		for (Domain domain : domains) {
			if (domain.getKey().equals(verticalAndDomain[1])) {
				isDomainAvailable = true;
				break;
			}
		}
		if (!isDomainAvailable) {
			throw new IllegalArgumentException(" \"" + verticalAndDomain[1]
					+ "\" this domain is not belonging to the vertical " + "\"" + verticalAndDomain[0] + "\" ");
		}
	}

	@Override
	public boolean createFeedback(String prospectCallId, String secondaryQaReview, QaFeedbackDTO qaFeedbackDTO,
			boolean finalSubmit) throws DuplicateKeyException {
		ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLog == null) {
			throw new IllegalArgumentException("prospectCallId: " + prospectCallId + " does not exit");
		}
		
		updateAgentNotes(qaFeedbackDTO, prospectCallLog);
		
		if (qaFeedbackDTO.getFeedbackResponseList() != null) {
			List<FeedbackSectionResponseDTO> feedbackSectionList = qaFeedbackDTO.getFeedbackResponseList();
			if (feedbackSectionList.size() > 0) {
				Optional<FeedbackSectionResponseDTO> rejectReasonFeedback = feedbackSectionList.stream().filter(
						leadValidation -> leadValidation.getSectionName().equalsIgnoreCase("Lead Validation"))
						.findAny();
				List<FeedbackResponseAttribute> rejectReasonAttribute = rejectReasonFeedback.get()
						.getResponseAttributes();
				if (rejectReasonAttribute.get(0).getFeedback().equalsIgnoreCase("YES")) {
					Campaign camp = campaignService.getCampaign(prospectCallLog.getProspectCall().getCampaignId());
					if (suppressionListService.isEmailSuppressed(
							prospectCallLog.getProspectCall().getProspect().getEmail(), camp.getId(),
							camp.getOrganizationId(), camp.isMdFiveSuppressionCheck())) {
						// check if person is already bought by name and company
						throw new IllegalArgumentException("Email is in suppression list.");
					}
				    if(isCompanySuppressedDetail(prospectCallLog,prospectCallLog.getProspectCall().getCampaignId())) {
				    	throw new IllegalArgumentException("Company is in suppression list.");
				    }
				}
			}
		}
		
		String campaignId = prospectCallLog.getProspectCall().getCampaignId();
		Campaign campaign = campaignRepository.findOneById(campaignId);

		QaFeedback qaFeedback = qaFeedbackDTO.toQaFeedbackResponse();
		try {
			qaFeedback.setOverallScore(qaFeedbackDTO.getOverallScore());
		} catch (NumberFormatException e) {
			logger.error("createFeedback() : Invalid Overall Score " + qaFeedbackDTO.getOverallScore(), e);
		}
		prospectCallLog.setQaFeedback(qaFeedback);
		boolean isFeebackCompleted = true;
		for (FeedbackSectionResponse feedbackSectionResponse : qaFeedback.getFeedbackResponseList()) {
			if (feedbackSectionResponse.getResponseAttributes() == null
					|| feedbackSectionResponse.getResponseAttributes().isEmpty()) {
				isFeebackCompleted = false;
				break;
			}
			for (FeedbackResponseAttribute feedbackResponseAttribute : feedbackSectionResponse
					.getResponseAttributes()) {
				/*
				 * DATE:13/12/2017 REASON:Changed isFeebackCompleted logic. Update status only
				 * when "Lead Validation" question (LEAD_VALIDATION_VALID) is answered.
				 */
				if (feedbackResponseAttribute.getAttribute().equals("LEAD_VALIDATION_VALID")
						&& feedbackResponseAttribute.getFeedback() == null
						&& feedbackResponseAttribute.getScore() == null) {
					isFeebackCompleted = false;
					break;
				}
			}
		}
		if (isFeebackCompleted && finalSubmit) {
			if (secondaryQaReview.equals("YES")) {
				/*
				 * DATE: 12/01/2018 REASON: added condition if campaigns organization is xtaas,
				 * lead goes to secondary qa
				 */
				// The line numbers 248, 255, 256, 257 are commented  because primary submitted prospects should go to the secondary qa irrespective of the organization.
				// if (campaign.getOrganizationId().equals("XTAAS CALL CENTER")) {
					if (prospectCallLog.getStatus().equals(ProspectCallStatus.PRIMARY_REVIEW)) {
						prospectCallLog.setStatus(ProspectCallStatus.SECONDARY_REVIEW);
					} else {
						prospectCallLog.setStatus(ProspectCallStatus.SECONDARY_QA_INITIATED);
					}
				// } else {
					// prospectCallLog.setStatus(ProspectCallStatus.QA_COMPLETE);
				// }
			} else {
				prospectCallLog.setStatus(ProspectCallStatus.QA_COMPLETE);
			}
		} else {
			// DATE : 05/06/2017 REASON : Added to check prospectCallLog status
			if (secondaryQaReview.equals("YES")) {
				if (prospectCallLog.getStatus().equals(ProspectCallStatus.SECONDARY_REVIEW)) {
					prospectCallLog.setStatus(ProspectCallStatus.SECONDARY_REVIEW);
				} else {
					prospectCallLog.setStatus(ProspectCallStatus.SECONDARY_QA_INITIATED);
				}
			} else {
				if (prospectCallLog.getStatus().equals(ProspectCallStatus.PRIMARY_REVIEW)) {
					prospectCallLog.setStatus(ProspectCallStatus.PRIMARY_REVIEW);
				} else {
					prospectCallLog.setStatus(ProspectCallStatus.QA_INITIATED);
				}
			}
		}

		/*
		 * DATE:06/03/2018 REASON:check if prospect interaction is available or not. if
		 * not then create new interaction.
		 */
		boolean createProspectCallInteraction = false;
		ProspectCallInteraction prospectCallInteraction = prospectCallInteractionRepository
				.findByProspectCallProspectCallIdAndStatus(prospectCallLog.getProspectCall().getProspectCallId(),
						prospectCallLog.getStatus());
		if (prospectCallInteraction == null) {
			createProspectCallInteraction = true;
		}

		boolean isLeadValidationValid = false;
		if (prospectCallLog.getQaFeedback() != null
				&& prospectCallLog.getQaFeedback().getFeedbackResponseList() != null) {
			for (FeedbackSectionResponse feedbackSectionResponse : prospectCallLog.getQaFeedback()
					.getFeedbackResponseList()) {
				if (feedbackSectionResponse.getSectionName().equals("Lead Validation")) {
					for (FeedbackResponseAttribute feedbackResponseAttribute : feedbackSectionResponse
							.getResponseAttributes()) {
						if (feedbackResponseAttribute.getAttribute().equals("LEAD_VALIDATION_VALID")
								&& feedbackResponseAttribute.getFeedback().equals("YES")) {
							isLeadValidationValid = true;
						}
					}
				}

			}
		}
		
		if (isLeadValidationValid) {
			if (checkLeadsPerCompanyCount(prospectCallLog, campaign)) {
				throw new IllegalArgumentException("Leads per company is already at maximum.");
			}
		}

		if (isLeadValidationValid && (prospectCallLog.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
				|| prospectCallLog.getStatus().equals(ProspectCallStatus.SECONDARY_QA_INITIATED)
				|| prospectCallLog.getStatus().equals(ProspectCallStatus.SECONDARY_REVIEW))) {
			prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.name());
			Organization organization =  organizationRepository.findOneById(campaign.getOrganizationId());

			if(organization.isSendAssetEmail() && (campaign.getDeliverAssetOnQA()!=null && campaign.getDeliverAssetOnQA().equalsIgnoreCase("PRIMARY")  
					&& !prospectCallLog.getProspectCall().isLeadDelivered())) {
				String deliveredAssetId = campaignService.deliverActiveAsset(prospectCallLog.getProspectCall());
				prospectCallLog.getProspectCall().setDeliveredAssetId(deliveredAssetId);
				prospectCallLog.getProspectCall().setLeadDelivered(true);
			}
				
		} else if (!isLeadValidationValid && (prospectCallLog.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
				|| prospectCallLog.getStatus().equals(ProspectCallStatus.SECONDARY_QA_INITIATED)
				|| prospectCallLog.getStatus().equals(ProspectCallStatus.SECONDARY_REVIEW))) {
			prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.QA_REJECTED.name());
		}
		if (isLeadValidationValid && prospectCallLog.getStatus().equals(ProspectCallStatus.QA_COMPLETE)) {
			prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.ACCEPTED.name());
			Organization organization =  organizationRepository.findOneById(campaign.getOrganizationId());

			if(organization.isSendAssetEmail() && (campaign.getDeliverAssetOnQA()!=null && campaign.getDeliverAssetOnQA().equalsIgnoreCase("SECONDARY") 
					&& !prospectCallLog.getProspectCall().isLeadDelivered())) {
				String deliveredAssetId = campaignService.deliverActiveAsset(prospectCallLog.getProspectCall());
				prospectCallLog.getProspectCall().setDeliveredAssetId(deliveredAssetId);
				prospectCallLog.getProspectCall().setLeadDelivered(true);
			}
		} else if (!isLeadValidationValid && prospectCallLog.getStatus().equals(ProspectCallStatus.QA_COMPLETE)) {
			prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.REJECTED.name());
		}

		ProspectCallLog prospectCallLogFromDB = prospectCallLogService.save(prospectCallLog,
				createProspectCallInteraction);

		/*
		 * prospect will be sent to client portal (real time lead delivery) after
		 * primary qa submitted (SECONDARY_QA_INITIATED).
		 */
		try {
			RealTimeDelivery realTimeDelivery = getRealTimeDelivery(
					prospectCallLogFromDB.getProspectCall().getCampaignId());

			if (realTimeDelivery != null && realTimeDelivery.getRealTimePost().equals(RealTimePost.QAREALTIMEPOST)) {
				if (prospectCallLogFromDB.getStatus().equals(ProspectCallStatus.SECONDARY_QA_INITIATED)
						&& !prospectCallLogFromDB.getProspectCall().isLeadDelivered() && isLeadValidationValid) {
					leadReportServiceImpl.realTimeByCampaignId(prospectCallLogFromDB);
				}
			} else if (realTimeDelivery != null
					&& realTimeDelivery.getRealTimePost().equals(RealTimePost.SECQAREALTIMEPOST)) {
				if (prospectCallLogFromDB.getStatus().equals(ProspectCallStatus.QA_COMPLETE)
						&& !prospectCallLogFromDB.getProspectCall().isLeadDelivered() && isLeadValidationValid) {
					leadReportServiceImpl.realTimeByCampaignId(prospectCallLogFromDB);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private void updateAgentNotes(QaFeedbackDTO qaFeedbackDTO, ProspectCallLog prospectCallLog) {
		ArrayList<Note> notes = new ArrayList<Note>();
		Note note = new Note(new Date(), qaFeedbackDTO.getAgentNotes(), "Notes updated by QA.");
		notes.add(note);
		prospectCallLog.getProspectCall().setNotes(notes);
	}
	
	private RealTimeDelivery getRealTimeDelivery(String xtaasCampaignId) {
		return realTimeDeliveryRepository.findByXtaasCampaignId(xtaasCampaignId);
	}
	
	// added to send record to primary qa from secondary qa.
	@Override
	public boolean returnToPrimary(String prospectCallId, QaFeedbackDTO qaFeedbackDTO) {
		ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLog == null) {
			throw new IllegalArgumentException("prospectCallId: " + prospectCallId + " does not exit");
		}
		QaFeedback qaFeedback = qaFeedbackDTO.toQaFeedbackResponse();
		try {
			qaFeedback.setOverallScore(qaFeedbackDTO.getOverallScore());
		} catch (NumberFormatException e) {
			logger.error("createFeedback() : Invalid Overall Score " + qaFeedbackDTO.getOverallScore(), e);
		}
		prospectCallLog.setQaFeedback(qaFeedback);
		prospectCallLog.setStatus(ProspectCallStatus.PRIMARY_REVIEW);
		prospectCallLog.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.QA_RETURNED.name());
		// check if prospect interaction is available or not. if not then create new
		// interaction.
//		boolean createProspectCallInteraction = false;
//		ProspectCallInteraction prospectCallInteraction = prospectCallInteractionRepository
//				.findByProspectCallProspectCallIdAndStatus(prospectCallLog.getProspectCall().getProspectCallId(),
//						prospectCallLog.getStatus());
//		if (prospectCallInteraction == null) {
//			createProspectCallInteraction = true;
//		}
		prospectCallLogService.save(prospectCallLog, true);
		return true;
	}

	// DATE:19/11/2018 Reason:Fetch prospects by  agentIds 
	@Override
	public ProspectCallQaSearchResult searchProspectCallsByQa(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		List<String> campaignIds = prospectCallSearchDTO.getCampaignIds();
		List<String> agentIds = getAgentIds();
		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			LocalDate todaydate = LocalDate.now();
			LocalDate fromDate = todaydate.minusDays(1);
			prospectCallSearchDTO.setFromDate(fromDate.toString());
			prospectCallSearchDTO.setToDate(todaydate.toString());
			prospectCallSearchDTO.setStartMinute(0);
			prospectCallSearchDTO.setEndMinute(0);
		}
		
		List<String> distinctCampaignIds = new ArrayList<String>();
		distinctCampaignIds = prospectCallLogRepository.getDistinctCampaignIdsForFilter(agentIds,prospectCallSearchDTO);
		// campaignIdsFromDB = prospectCallLogRepository.generateDistinctCampaignIdsAggregationQuery(prospectCallSearchDTO.getFromDate(), prospectCallSearchDTO.getToDate(), agentIds);
		// if (campaignIdsFromDB != null && campaignIdsFromDB.size() > 0) {
		// 	distinctCampaignIds = campaignIdsFromDB.stream().map(campaignId -> campaignId.getId())
		// 			.collect(Collectors.toList());
		// }
		//////////////////////////////
		/*Map<String,String> campaignIdMap = new HashMap<String, String>();
		Map<String,String> teamIdMap = new HashMap<String, String>();
		List<String> teamIds = new ArrayList<>();
		List<ProspectCallLog> prospectCallLogList  = prospectCallLogRepository.getDistinctCampaignIds(agentIds, prospectCallSearchDTO);
		if(prospectCallLogList !=null && prospectCallLogList.size()>0){
			for(ProspectCallLog pclog : prospectCallLogList){
				campaignIdMap.put(pclog.getProspectCall().getCampaignId(),pclog.getProspectCall().getCampaignId());
				teamIdMap.put(pclog.getProspectCall().getSupervisorId(), pclog.getProspectCall().getSupervisorId());
			}
		}
		for(Map.Entry<String, String> campaignEntry : campaignIdMap.entrySet()){
		campaignIds.add(campaignEntry.getKey());
		}
		
		for(Map.Entry<String, String> teamEntry : teamIdMap.entrySet()){
			teamIds.add(teamEntry.getKey());
		}*/
		
////////////////////////////////////////////
		/*List<String> activeCampaignIds = new ArrayList<>();
		List<Campaign> activeCampaigns = new ArrayList<>();
		
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		if (campaignIds != null && campaignIds.size() > 0) {
			List<Campaign> campaigns = campaignRepository.findCampaignByIds(campaignIds);
			for (Campaign campaign : campaigns) {
				if (campaign != null
						&& (campaign.getStatus() == CampaignStatus.RUNNING || campaign.getStatus() == CampaignStatus.PAUSED)
						&& campaign.getQaRequired() && !activeCampaignIds.contains(campaign.getId())) {
					activeCampaignIds.add(campaign.getId());
					activeCampaigns.add(campaign);
				}
			}
		}*/
		
		List<String> partnerIds = new ArrayList<>();
		if (prospectCallSearchDTO.getPartnerIds() != null && prospectCallSearchDTO.getPartnerIds().size() > 0) {
			partnerIds = prospectCallSearchDTO.getPartnerIds();
		}	
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();

		if (!agentIds.isEmpty() || !distinctCampaignIds.isEmpty()) {
			PageRequest pageRequest;
			if (prospectCallSearchDTO.getDirection() == null || prospectCallSearchDTO.getSort() == null) {
				pageRequest = PageRequest.of(prospectCallSearchDTO.getPage(), prospectCallSearchDTO.getSize(),
						Direction.DESC, "prospectCall.leadStatus");
			} else {
				pageRequest = PageRequest.of(prospectCallSearchDTO.getPage(), prospectCallSearchDTO.getSize(),
						prospectCallSearchDTO.getDirection(), prospectCallSearchDTO.getSort());
			}
			prospectCallSearchDTO =getUpdatedProspectSearchDTO(prospectCallSearchDTO);
			if((distinctCampaignIds==null || distinctCampaignIds.isEmpty()) && (prospectCallSearchDTO.getCampaignIds()==null || prospectCallSearchDTO.getCampaignIds().isEmpty())) {
				if(prospectCallSearchDTO.getAgentIds()==null || prospectCallSearchDTO.getAgentIds().isEmpty())
					prospectCallSearchDTO.setAgentIds(agentIds);
			}
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQa(agentIds, distinctCampaignIds,
					prospectCallSearchDTO, pageRequest);

		} else {
			return new ProspectCallQaSearchResult(0, new ArrayList<ProspectCallSearch>(), null, null, null,null);
		}
		
		List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(distinctCampaignIds, Arrays.asList("organizationId"));
		CampaignSearchResult campaignSearchResult = new CampaignSearchResult(campaigns.size(), campaigns);
		/* List<AgentSearchResult> agentSearchResult = getAgentList(agentIds);*/

		return new ProspectCallQaSearchResult(
				prospectCallLogRepository.countProspectCallsByQa(agentIds, distinctCampaignIds, prospectCallSearchDTO),
				getProspectCallSearchList(prospectCalls), campaignSearchResult, null, partnerIds,null);
		
	}

	public List<AgentSearchResult> getAgentList(List<String> agentIds) {
		if(agentIds != null && !agentIds.isEmpty()) {
			return agentRepository.findByIds(agentIds);
		}
		return new ArrayList<AgentSearchResult>();
	}

	@Override
	public ProspectCallQaSearchResult searchProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		List<String> campaignIds = prospectCallSearchDTO.getCampaignIds();
		List<String> agentIds = getAgentIds();
		if (prospectCallSearchDTO.getFromDate() == null && prospectCallSearchDTO.getToDate() == null) {
			LocalDate todaydate = LocalDate.now();
			LocalDate fromDate = todaydate.minusDays(7);
			prospectCallSearchDTO.setFromDate(fromDate.toString());
			prospectCallSearchDTO.setToDate(todaydate.toString());
			prospectCallSearchDTO.setStartMinute(0);
			prospectCallSearchDTO.setEndMinute(0);
		}
		
		List<String> distinctCampaignIds = new ArrayList<String>();
		distinctCampaignIds = prospectCallLogRepository.getDistinctCampaignIdsForFilter(agentIds,prospectCallSearchDTO);
		// campaignIdsFromDB = prospectCallLogRepository.generateDistinctCampaignIdsAggregationQuery(prospectCallSearchDTO.getFromDate(), prospectCallSearchDTO.getToDate(), agentIds);
		// if (campaignIdsFromDB != null && campaignIdsFromDB.size() > 0) {
		// 	distinctCampaignIds = campaignIdsFromDB.stream().map(campaignId -> campaignId.getId())
		// 			.collect(Collectors.toList());
		// }
		//////////////////////////////
		/*Map<String,String> campaignIdMap = new HashMap<String, String>();
		Map<String,String> teamIdMap = new HashMap<String, String>();
		List<String> teamIds = new ArrayList<>();
		List<ProspectCallLog> prospectCallLogList  = prospectCallLogRepository.getDistinctCampaignIds(agentIds, prospectCallSearchDTO);
		if(prospectCallLogList !=null && prospectCallLogList.size()>0){
			for(ProspectCallLog pclog : prospectCallLogList){
				campaignIdMap.put(pclog.getProspectCall().getCampaignId(),pclog.getProspectCall().getCampaignId());
				teamIdMap.put(pclog.getProspectCall().getSupervisorId(), pclog.getProspectCall().getSupervisorId());
			}
		}
		
		for(Map.Entry<String, String> campaignEntry : campaignIdMap.entrySet()){
			campaignIds.add(campaignEntry.getKey());
		}
		
		for(Map.Entry<String, String> teamEntry : teamIdMap.entrySet()){
			teamIds.add(teamEntry.getKey());
		}
		
////////////////////////////////////////////
		List<String> activeCampaignIds = new ArrayList<>();
		List<String> partnerIds = new ArrayList<>();
		List<Campaign> activeCampaigns = new ArrayList<>();
		
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		
		
		if (campaignIds != null && campaignIds.size() > 0) {
			List<Campaign> campaigns = campaignRepository.findCampaignByIds(campaignIds);
			for (Campaign campaign : campaigns) {
				if (campaign != null
						&& (campaign.getStatus() == CampaignStatus.RUNNING || campaign.getStatus() == CampaignStatus.PAUSED)
						&& campaign.getQaRequired() && !activeCampaignIds.contains(campaign.getId())) {
					activeCampaignIds.add(campaign.getId());
					activeCampaigns.add(campaign);
				}
			}
		}
		
		List<String> distinctPartnerIds = prospectCallLogRepository.getDistinctPartnerIds(activeCampaignIds);
		List<Organization> activePartners = organizationRepository.findByIdsAndStatus(distinctPartnerIds);
		if (activePartners.size() > 0 && !activePartners.isEmpty()) {
			partnerIds = activePartners.stream()
	                .map(Organization ::getId).collect(Collectors.toList());
		}*/
		List<String> partnerIds = new ArrayList<>();
		if (prospectCallSearchDTO.getPartnerIds() != null && prospectCallSearchDTO.getPartnerIds().size() > 0) {
			partnerIds = prospectCallSearchDTO.getPartnerIds();
		}		
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();

		if (!agentIds.isEmpty() || !distinctCampaignIds.isEmpty()) {
			PageRequest pageRequest;
			if (prospectCallSearchDTO.getDirection() == null || prospectCallSearchDTO.getSort() == null) {
				pageRequest = PageRequest.of(prospectCallSearchDTO.getPage(), prospectCallSearchDTO.getSize(),
						Direction.ASC, "prospectCall.leadStatus");
			} else {
				pageRequest = PageRequest.of(prospectCallSearchDTO.getPage(), prospectCallSearchDTO.getSize(),
						prospectCallSearchDTO.getDirection(), prospectCallSearchDTO.getSort());
			}
			prospectCallSearchDTO =getUpdatedProspectSearchDTO(prospectCallSearchDTO);
			if((distinctCampaignIds==null || distinctCampaignIds.isEmpty()) && (prospectCallSearchDTO.getCampaignIds()==null || prospectCallSearchDTO.getCampaignIds().isEmpty())) {
				if(prospectCallSearchDTO.getAgentIds()==null || prospectCallSearchDTO.getAgentIds().isEmpty())
					prospectCallSearchDTO.setAgentIds(agentIds);
			}
			prospectCalls = prospectCallLogRepository.searchProspectCallsBySecQa(distinctCampaignIds,
					prospectCallSearchDTO, pageRequest);
		} else {
			return new ProspectCallQaSearchResult(0, new ArrayList<ProspectCallSearch>(), null, null, null, null);
		}

	/*	CampaignSearchResult campaignSearchResult = getCampaignList(activeCampaigns);
		List<AgentSearchResult> agentSearchResult = getAgentList(agentIds);*/
		
		List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(distinctCampaignIds, Arrays.asList("organizationId"));
		CampaignSearchResult campaignSearchResult = new CampaignSearchResult(campaigns.size(), campaigns);

		return new ProspectCallQaSearchResult(
				prospectCallLogRepository.countProspectCallsBySecQa(distinctCampaignIds, prospectCallSearchDTO),
				getProspectCallSearchList(prospectCalls), campaignSearchResult, null, partnerIds, null);
	}

	
	
	private ProspectCallSearchDTO getUpdatedProspectSearchDTO(ProspectCallSearchDTO prospectCallSearchDTO){
///////////// For Failure
	 		ProspectCallSearchDTO successPCSDTO = new ProspectCallSearchDTO();
	 		ProspectCallSearchDTO failurePCSDTO = new ProspectCallSearchDTO();
	 		ProspectCallSearchDTO callbackPCSDTO = new ProspectCallSearchDTO();
	 		/**
			 *  DATE :- 02/01/2020
			 *  Added ANSWERING M/C and GK ANSWERING M/C options in leadstatus list to filter the records.
			 */
	 		if(prospectCallSearchDTO.getLeadStatus()!=null && prospectCallSearchDTO.getLeadStatus().size()>0){
	 			List<String> leadStatusList = prospectCallSearchDTO.getLeadStatus();
	 			List<String> successLeadStatus = new ArrayList<String>();
	 			List<String> failureLeadStatus = new ArrayList<String>();
	 			List<String> callbackLeadStatus = new ArrayList<String>();
	 			for(String lStatus : leadStatusList){
				if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CLIENT_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_SCORED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_ACCEPTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.QA_RETURNED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.REJECTED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.ANSWERMACHINE.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.NOT_DISPOSED.toString())
						|| lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.GK_ANSWERMACHINE.toString())) {
					successLeadStatus.add(lStatus);
				} else if (lStatus.equalsIgnoreCase(XtaasConstants.LEAD_STATUS.CALLBACK.toString())) {
					callbackLeadStatus.add(lStatus);
				} else {
					failureLeadStatus.add(lStatus);
				}
	 			}
	 			
	 			/*if(successLeadStatus.size()>0 && failureLeadStatus.size()>0){
	 				List<String> dispositionList = new ArrayList<String>(1);
	 				dispositionList.add("SUCCESS");
	 				dispositionList.add("FAILURE");
	 				successPCSDTO = prospectCallSearchDTO;
	 				successPCSDTO.setLeadStatus(successLeadStatus);
	 				successPCSDTO.setDisposition(dispositionList);
	 				successPCSDTO.setSubStatus(failureLeadStatus);
	 				return successPCSDTO;
	 		 }*/
	 			
		 		 if(successLeadStatus.size()>0 && failureLeadStatus.size()==0){
		 				List<String> dispositionList = new ArrayList<String>(1);
		 				dispositionList.add("SUCCESS");
		 				successPCSDTO = prospectCallSearchDTO;
		 				successPCSDTO.setLeadStatus(successLeadStatus);
		 				successPCSDTO.setDisposition(dispositionList);
		 				return successPCSDTO;
		 		 }
		 		 
		 		 if (callbackLeadStatus.size() > 0 && successLeadStatus.size() == 0 && failureLeadStatus.size() == 0) {
		 			List<String> dispositionList = new ArrayList<String>();
	 				dispositionList.add("CALLBACK");
	 				callbackPCSDTO = prospectCallSearchDTO;
	 				callbackPCSDTO.setDisposition(dispositionList);
	 				callbackPCSDTO.setSubStatus(callbackLeadStatus);
	 				return callbackPCSDTO;
		 		 }
		 		 
		 		 if(failureLeadStatus.size()>0){
		 				List<String> dispositionList = new ArrayList<String>(1);
		 				dispositionList.add("FAILURE");
		 				failurePCSDTO = prospectCallSearchDTO;
		 				failurePCSDTO.setDisposition(dispositionList);
		 				failurePCSDTO.setLeadStatus(null);
		 				failurePCSDTO.setSubStatus(failureLeadStatus);
		 				return failurePCSDTO;
		 		}
	 		}
	 		
	 		return prospectCallSearchDTO;
	 		
	 		
	 		//////////////// End Failure
	
	}
	
	@Override
	public List<AgentSearchResult> getAgentsForProspectCallsByQa(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		List<String> allAgentIds = getAgentIdsByQa();
		List<String> campaignIds=new ArrayList<String>();
		if (!allAgentIds.isEmpty()) {
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQaWithoutPagination(allAgentIds, prospectCallSearchDTO);
		}
		if (!prospectCalls.isEmpty()) {			
			Campaign campaign = null;
			String  campaignId;			
			for (ProspectCallLog prospectCallLog : prospectCalls) {
				
				campaignId = prospectCallLog.getProspectCall().getCampaignId();
				campaign = campaignRepository.findOneById(campaignId);
				if (campaign != null && campaign.getStatus() == CampaignStatus.RUNNING && campaign.getQaRequired() && !campaignIds.contains(campaignId)) {
					campaignIds.add(campaignId);
				}
			}
		}
		
		if (campaignIds.isEmpty()) {
			return new ArrayList<AgentSearchResult>();
		} else {
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQaWithoutPagination(allAgentIds, campaignIds, prospectCallSearchDTO);
		}

		if(prospectCalls != null) {
			List<String> agentIds = new ArrayList<String>();

			for (ProspectCallLog prospectCallLog : prospectCalls) {
				if (prospectCallLog.getProspectCall().getAgentId() != null) {
					
					if(!agentIds.contains(prospectCallLog.getProspectCall().getAgentId()))
					{
						agentIds.add(prospectCallLog.getProspectCall().getAgentId());					
					}
				}
			}
			return agentRepository.findByIds(agentIds);
		}
		return new ArrayList<AgentSearchResult>(); // returning an empty result;
	}

	@Override
	public List<AgentSearchResult> getAgentsForProspectCallsBySecQa(
			ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {	
		List<String> campaignIds =  new ArrayList<String>();
		List<TeamSearchResultDTO> teamSearchResults = teamRepository.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
		List<String> teamIds =  new ArrayList<String>();
		for (TeamSearchResultDTO teamSearchResult : teamSearchResults) {
			teamIds.add(teamSearchResult.getTeamId());
		}
		campaignIds = getCampaignIds(teamIds, prospectCallSearchDTO.getClientId());
		List<String> allAgentIds= null;
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		if (!campaignIds.isEmpty()) {
			prospectCalls = prospectCallLogRepository.searchProspectCallsByQaWithoutPagination(allAgentIds, campaignIds, prospectCallSearchDTO);
		}
		if(prospectCalls != null) {
			List<String> agentIds = new ArrayList<String>();

			for (ProspectCallLog prospectCallLog : prospectCalls) {
				if (prospectCallLog.getProspectCall().getAgentId() != null) {
					
					if(!agentIds.contains(prospectCallLog.getProspectCall().getAgentId()))
					{
						agentIds.add(prospectCallLog.getProspectCall().getAgentId());					
					}
				}
			}
			return agentRepository.findByIds(agentIds);
		}
		return new ArrayList<AgentSearchResult>(); // returning an empty result;
	}

	private List<ProspectCallSearch> getProspectCallSearchList(List<ProspectCallLog> prospectCalls) {
		List<ProspectCallSearch> prospectCallList = new ArrayList<ProspectCallSearch>();
		String qaId = null;
		String secQaId = null;
		for (ProspectCallLog prospectCallLog : prospectCalls) {
			if (prospectCallLog.getProspectCall() != null ) {
				if (prospectCallLog.getQaFeedback() != null) {
					if (prospectCallLog.getQaFeedback().getQaId() != null) {
						qaId = prospectCallLog.getQaFeedback().getQaId();
					}
					if (prospectCallLog.getQaFeedback().getSecQaId() != null) {
						secQaId = prospectCallLog.getQaFeedback().getSecQaId();
					}
				}
				ProspectCall prospectCall = prospectCallLog.getProspectCall();
				Campaign campaign = null;
				Team team = null;
				if (prospectCall.getCampaignId() != null) {
					campaign = campaignRepository.findOneById(prospectCall.getCampaignId());
					if (campaign != null) {
						if (campaign.getTeam() != null) {
							CampaignTeam campaignTeam = campaign.getTeam();
							team = teamRepository.findOneById(campaignTeam.getTeamId());
						}
					}
				}
				Agent agent = null;
				if (prospectCall.getAgentId() != null) {
					agent = agentRepository.findOneById(prospectCall.getAgentId());
				}
				
				List<String> recordingUrls = new ArrayList<String>();
				recordingUrls.add(prospectCall.getRecordingUrl());
				prospectCallList.add(new ProspectCallSearch(agent != null ? agent.getName() : null,
						campaign != null ? campaign.getName() : null,
						campaign != null ? campaign.getOrganizationId() : null, team != null ? team.getName() : null,
						prospectCall, prospectCallLog.getStatus() != null ? prospectCallLog.getStatus() : null,
						recordingUrls,
						prospectCallLog.getQaFeedback() != null ? prospectCallLog.getQaFeedback().getOverallScore()
								: null, qaId != null ? qaId : null, secQaId != null ? secQaId : null));
			}
		}
		return prospectCallList;
	}

	public List<String> getCampaignIds(List<String> teamIds, String clientId) {
		List<String> campaignIds = new ArrayList<String>();
		List<Campaign> campaigns;
		if (clientId == null || clientId.isEmpty()) {
			campaigns = campaignRepository.findByTeams(teamIds);
		} else {
			campaigns = campaignRepository.findByTeamsAndClient(teamIds, clientId);
		}
		if (!campaigns.isEmpty()) {
			for (Campaign campaign : campaigns) {
//				DATE:19/12/2017	Added to show only Active and qaRequired campaign to qa
				if (campaign.getStatus() == CampaignStatus.RUNNING && campaign.getQaRequired()) {
                    campaignIds.add(campaign.getId());
                }
			}
		}
		return campaignIds;
	}

	public DateTime getDate(Date quickDate, String timeZone) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(quickDate);
		DateTime dateInClientTZ = new DateTime(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
				cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE),
				DateTimeZone.forID("America/Los_Angeles"));
		System.out.println("Time in client timezone fromat" + dateInClientTZ);
		DateTime dateInUTC = XtaasDateUtils.changeTimeZone(dateInClientTZ, DateTimeZone.UTC.getID());
		return dateInUTC;
	}

	@Override
	public QaDetails getProspectCallForQA(String prospectCallId) {
		return prospectCallLogService.getDetails(prospectCallId);
	}

	@Override
	public ProspectCallQaSearchResult searchProspectCallsByAgent(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		String loggedInUser = XtaasUserUtils.getCurrentLoggedInUsername();
		prospectCalls = prospectCallLogRepository.searchProspectCallsByAgent(loggedInUser, prospectCallSearchDTO,
				PageRequest.of(prospectCallSearchDTO.getPage(), prospectCallSearchDTO.getSize(),
						prospectCallSearchDTO.getDirection(), prospectCallSearchDTO.getSort()));
		return new ProspectCallQaSearchResult(
				prospectCallLogRepository.countProspectCallsByAgent(loggedInUser, prospectCallSearchDTO),
				getProspectCallSearchList(prospectCalls), null, null, null, null);
	}

	@Override
	public List<String> getTeamIdsByQa() {
		List<TeamSearchResultDTO> teamSearchResults = teamRepository
				.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
		List<String> teamIds = new ArrayList<String>();
		for (TeamSearchResultDTO teamSearchResult : teamSearchResults) {
			teamIds.add(teamSearchResult.getTeamId());
		}
		return teamIds;
	}

	@Override
	public List<String> getCampaignIdsByQa() {
		List<String> campaignIds = new ArrayList<String>();
		List<Campaign> campaigns = campaignRepository.findByTeams(getTeamIdsByQa());
		if (!campaigns.isEmpty()) {
			for (Campaign campaign : campaigns) {
//				DATE:19/12/2017	Added to show only Active and qaRequired campaign to qa
				if (campaign.getStatus() == CampaignStatus.RUNNING && campaign.getQaRequired()) {
                    campaignIds.add(campaign.getId());
                }
			}
		}
		return campaignIds;
	}
	
	@Override
	public List<String> getAgentIdsByQa() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
 		List<AgentSearchResult> results = agentRepository.findByPartnerIdAndStatus(userLogin.getOrganization());
		List<String> agentIds = new ArrayList<String>();
		for (AgentSearchResult agentSearchResult : results) {
			agentIds.add(agentSearchResult.getId());
		}
		return agentIds;	
	}

	@Override
	public List<Team> getOrganizationTeams(String organizationId) {
		return teamRepository.findAllByPartnerId(organizationId);
	}

	@Override
	public List<String> getAgentIds() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		List<Team> teamsByQAs = teamRepository.findTeamsByQaId(userLogin.getId());
		List<String> supervisorIds = new ArrayList<>(); 
		for (Team team : teamsByQAs) {
			supervisorIds.add(team.getSupervisor().getResourceId());
		}
		List<Team> teamsBySupervisorIds = teamRepository.findByAdminSupervisorAndSupervisorId(supervisorIds);
		List<String> agentIds = new ArrayList<String>();
		agentIds.add(userLogin.getOrganization()+"_EMAIL_AGENT");
		for (Team team : teamsBySupervisorIds) {
			List<TeamResource> teamResources = team.getAgents();
			for (TeamResource teamResource : teamResources) {
				if (!agentIds.contains(teamResource.getResourceId())) {
					agentIds.add(teamResource.getResourceId());
				}
			}
		}
		return agentIds;
	}
	
	
	private List<String> getQaPartnerIds() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		List<Team> teamsByQAs = teamRepository.findTeamsByQaId(userLogin.getId());
		List<String> partnerIds = new ArrayList<String>();
		for (Team team : teamsByQAs) {
			if (!partnerIds.contains(team.getPartnerId())) {
				partnerIds.add(team.getPartnerId());
			}
		}
		return partnerIds;
	}

	@Override
	public ProspectCallQaSearchResult getQADetails(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		List<String> agentIds = getAgentIds();
		
		List<String> teamIds = new ArrayList<>();
		
		List<String> activeCampaignIds = new ArrayList<>();
//		List<Campaign> activeCampaigns = new ArrayList<>();
		
		// By default 7 days campaigns list in campaign dropdown.
		LocalDate todaydate = LocalDate.now();
		LocalDate fromDate = todaydate.minusDays(7);
		prospectCallSearchDTO.setFromDate(fromDate.toString());
		prospectCallSearchDTO.setToDate(todaydate.toString());
		prospectCallSearchDTO.setStartMinute(0);
		prospectCallSearchDTO.setEndMinute(0);
		
		List<String> distinctCampaignIds = new ArrayList<String>();
		// campaignIdsFromDB = prospectCallLogRepository.generateDistinctCampaignIdsAggregationQuery(prospectCallSearchDTO.getFromDate(), prospectCallSearchDTO.getToDate(), agentIds);
		// if (campaignIdsFromDB != null && campaignIdsFromDB.size() > 0) {
		// 	distinctCampaignIds = campaignIdsFromDB.stream().map(campaignId -> campaignId.getId())
		// 			.collect(Collectors.toList());
		// }
		distinctCampaignIds = prospectCallLogRepository.getDistinctCampaignIdsForFilter(agentIds,prospectCallSearchDTO);
		List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(distinctCampaignIds, Arrays.asList("organizationId"));
//		List<Campaign> campaigns = getActiveCampainsByPartner(true);
		for(CampaignDTO camp : campaigns) {
			activeCampaignIds.add(camp.getId());
//			activeCampaigns.add(camp);
		}
		
		List<Team>  teams =findTeamsByQaId();
		for(Team   team : teams) {
			teamIds.add(team.getSupervisor().getResourceId());
		}
		
		if (!agentIds.isEmpty() && !activeCampaignIds.isEmpty()) {
			//Do  Nothing

		} else {
			return new ProspectCallQaSearchResult(0, new ArrayList<ProspectCallSearch>(), null, null, null,teamIds);
		}

		CampaignSearchResult campaignSearchResult = new CampaignSearchResult(campaigns.size(), campaigns);
		List<AgentSearchResult> agentSearchResult = getAgentList(agentIds);
		return new ProspectCallQaSearchResult(
				0,
				null, campaignSearchResult, agentSearchResult, null,teamIds);
		
	}

	@Override
	public ProspectCallQaSearchResult getSecQADetails(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException {
		List<String> agentIds = getAgentIds();
		List<String> teamIds = new ArrayList<>();
		List<String> partnerIds = new ArrayList<>();

//		Map<String,String> partnerIdMap = new HashMap<String,String>();
		
		List<String> activeCampaignIds = new ArrayList<>();
		List<Campaign> activeCampaigns = new ArrayList<>();
		
//		By default 7 days campaigns list in campaign dropdown.
		LocalDate todaydate = LocalDate.now();
		LocalDate fromDate = todaydate.minusDays(7);
		prospectCallSearchDTO.setFromDate(fromDate.toString());
		prospectCallSearchDTO.setToDate(todaydate.toString());
		prospectCallSearchDTO.setStartMinute(0);
		prospectCallSearchDTO.setEndMinute(0);
		
		
		List<String> distinctCampaignIds = new ArrayList<String>();
		// campaignIdsFromDB = prospectCallLogRepository.generateDistinctCampaignIdsAggregationQuery(prospectCallSearchDTO.getFromDate(), prospectCallSearchDTO.getToDate(), agentIds);
		// if (campaignIdsFromDB != null && campaignIdsFromDB.size() > 0) {
		// 	distinctCampaignIds = campaignIdsFromDB.stream().map(campaignId -> campaignId.getId())
		// 			.collect(Collectors.toList());
		// }
		distinctCampaignIds = prospectCallLogRepository.getDistinctCampaignIdsForFilter(agentIds,prospectCallSearchDTO);
		List<CampaignDTO> campaigns = campaignRepository.findByIdsWithProjectedFields(distinctCampaignIds, Arrays.asList("organizationId"));
		
//		List<Campaign> campaigns = getActiveCampainsByPartner(false);
//		for(Campaign camp : campaigns) {
//			activeCampaignIds.add(camp.getId());
//			activeCampaigns.add(camp);
//			List<String> psups = camp.getPartnerSupervisors();
//			if (psups != null && psups.size() > 0) {
//				for(String sup : psups) {
//					partnerIdMap.put(sup,sup);
//				}
//			}
//		}
//		for(Map.Entry<String, String> partEntry : partnerIdMap.entrySet()){
//			partnerIds.add(partEntry.getKey());
//		}
//		List<Organization> activePartners = organizationRepository.findByIdsAndStatus(partnerIds);
//		if (activePartners.size() > 0 && !activePartners.isEmpty()) {
//			partnerIds = activePartners.stream()
//	                .map(Organization ::getId).collect(Collectors.toList());
//		}
		
		partnerIds = getQaPartnerIds();
		
		List<Team> teams =findTeamsByQaId();
		for(Team   team : teams) {
			teamIds.add(team.getSupervisor().getResourceId());
		}
		
		if (!agentIds.isEmpty() && !distinctCampaignIds.isEmpty()) {
			//Do  Nothing

		} else {
			return new ProspectCallQaSearchResult(0, new ArrayList<ProspectCallSearch>(), null, null, partnerIds,teamIds);
		}

		CampaignSearchResult campaignSearchResult = new CampaignSearchResult(campaigns.size(), campaigns);
		List<AgentSearchResult> agentSearchResult = getAgentList(agentIds);
		return new ProspectCallQaSearchResult(
				0,
				null, campaignSearchResult, agentSearchResult, partnerIds,teamIds);
		
	}

	public List<Campaign> getActiveCampainsByPartner(boolean qa) {
		//		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
				List<String> organizations = new ArrayList<String>();
				List<Team> teamsFromDiffrentOrganization = findTeamsByQaId();
				if (teamsFromDiffrentOrganization != null && teamsFromDiffrentOrganization.size() > 0) {
					organizations = teamsFromDiffrentOrganization.stream()
							.map(Team ::getPartnerId).collect(Collectors.toList());
				}
				List<String> status = new ArrayList<String>();
				status.add("RUNNING");
				List<Campaign> campaigns =  new ArrayList<Campaign>();
				if(qa)
					campaigns = campaignRepository.getActiveCampaignsByPartner(status,organizations);
				else {
					status.add("PAUSED");
					campaigns = campaignRepository.getActiveCampaignsByPartner(status,organizations);
				}	
				return campaigns;
	   }
	
	public List<Team> findTeamsByQaId() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		List<Team> teams = teamRepository.findTeamsByQaId(userLogin.getId());	
		return teams;
	}
	
	private boolean isCompanySuppressedDetail(ProspectCallLog domain,String campaignId){
		 boolean suppressed = false;
		 if(domain.getProspectCall()!=null &&domain.getProspectCall().getProspect()!=null 
				 &&domain.getProspectCall().getProspect().getCustomAttributes()!=null
				 &&domain.getProspectCall().getProspect().getCustomAttributeValue("domain") !=null
				 &&!domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
		 String pdomain = domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
		 if(pdomain!=null && !pdomain.isEmpty() ){
			pdomain = getSuppressedHostName(pdomain);
		 }
		 if(domain.getProspectCall().getProspect().getEmail()!=null && !domain.getProspectCall().getProspect().getEmail().isEmpty()) {
			 String pEmail = domain.getProspectCall().getProspect().getEmail();
			 String domEmail = pEmail.substring(pEmail.indexOf("@")+1, pEmail.length());

			 long compSupCount = suppressionListNewRepository.countSupressionListByCampaignDomain(
					 campaignId, domEmail);
			 if(compSupCount > 0) {
				 suppressed = true;
			 }
		 }

		 if(pdomain!=null  && !pdomain.isEmpty()) {
			 long domainSupCount = suppressionListNewRepository.countSupressionListByCampaignDomain(
					 campaignId, pdomain);
			 if(domainSupCount > 0) {
				 suppressed = true;
			 }
		 }
		 }
		 return suppressed;
	 }

	 private boolean checkLeadsPerCompanyCount(ProspectCallLog personRecord, Campaign campaign) {
		List<String> campaignIds = new ArrayList<>();
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		String domain = null;
		String email = personRecord.getProspectCall().getProspect().getEmail();
		if (personRecord.getProspectCall().getProspect().getCustomAttributeValue("domain") != null && !personRecord
				.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
			domain = personRecord.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
			if ((domain.contains("http://") || domain.contains("HTTP://"))) {
				domain = domain.substring(7, domain.length());
			}

			if ((domain.contains("https://") || domain.contains("HTTPS://"))) {
				domain = domain.substring(8, domain.length());
			}

			if ((domain.contains("www.") || domain.contains("WWW."))) {
				domain = domain.substring(4, domain.length());
			}
		} else if (email != null && !email.isEmpty()) {
			domain = email.substring(email.indexOf("@") + 1, email.length());
		}
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0 && domain != null) {
			campaignIds.addAll(campaign.getCampaignGroupIds());
			if (!campaignIds.contains(campaign.getId())) {
				campaignIds.add(campaign.getId());
			}
		} else {
			campaignIds.add(campaign.getId());
		}
		List<DomainCompanyCountPair> dataFromDB = new ArrayList<>();
		if (campaign.getDuplicateCompanyDuration() > 0 && campaign.getLeadsPerCompany() > 0) {
			logger.debug("Leads per company query start for [{}] campaign", campaign.getName());
			if (loginUser.getRoles().contains("SECONDARYQA")) {
				dataFromDB = prospectCallLogRepository.generateDuplicateProspectAggregationQuerySecondaryQA(campaignIds,
				campaign.getDuplicateCompanyDuration());
			} else {
				dataFromDB = prospectCallLogRepository.generateDuplicateProspectAggregationQueryQA(campaignIds,
				campaign.getDuplicateCompanyDuration());
			}
			logger.debug("Leads per company query end for [{}] campaign & distinct company list size is []",
					campaign.getName(), dataFromDB.size());
			for (DomainCompanyCountPair pair : dataFromDB) {
				if (domain != null && pair.getDomain() != null && pair.getDomain().contains(domain)
						&& pair.getCount() >= campaign.getLeadsPerCompany()) {
					return true;
				}
			}
		}
		return false;
	}

	 private String getSuppressedHostName(String domain){
			try {
				if (domain != null) {
					domain = domain.toLowerCase();
					if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
							|| domain.contains("HTTPS://")) {

					} else {
						domain = "http://" + domain;
					}
				}

				URL aURL = new URL(domain);
				domain = aURL.getHost();
				if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
					if ((domain.contains("www.") || domain.contains("WWW."))) {
						domain = domain.substring(4, domain.length());
						/*
						 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
						 */
						return domain;
						// }
					} else {
						/*
						 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
						 * return domain; }else{
						 */
						return domain;
						// }
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return domain;
		}
}
