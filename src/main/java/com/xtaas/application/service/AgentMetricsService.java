package com.xtaas.application.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.xtaas.application.service.AgentMetricsServiceImpl.AgentMetrics;
import com.xtaas.web.dto.AgentMetricsDTO;
import com.xtaas.web.dto.CallStatusDTO;

public interface AgentMetricsService {

	public List<AgentMetricsDTO> getAgentMetricsBySupervisor(String timeZone);

	public List<AgentMetricsDTO> getAgentMetricsByCampaign(String campaignId, Date fromDate, Date toDate, HashMap<AgentMetrics, String> metrics);

	public AgentMetricsDTO getAgentMetrics(String agentId, CallStatusDTO callStatusDTO);
	
	public List<AgentMetricsDTO> getAgentMetricsBySupervisorLiveQueue();
}
