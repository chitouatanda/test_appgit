package com.xtaas.application.service;

import com.xtaas.web.dto.ProspectDTO;

public interface ProspectCallService {

	public void updateProspectCall(ProspectDTO prospectDTO);	// DATE : 26/05/2017	REASON : used to update Prospect/Lead details from QA page

	public void updateProspectStatus(String prospectCallId, String leadStatus);

	public void updateRecordingUrl (String pcid, String recordingUrl);

	public void updateAWSRecordingUrl (String pcid, String awsRecordingUrl);

}
