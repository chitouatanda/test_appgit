/**
 * 
 */
package com.xtaas.application.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.xtaas.db.entity.Prospect;
import com.xtaas.service.PhoneDncService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.DNCList.DNCTrigger;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.DNCListRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.IDNCSearchResult;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.utils.ErrorHandlindutils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.DemandShoreDNCDTO;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.IDNCSearchDTO;

/**
 * @author djain
 *
 */
@Service
public class InternalDNCListServiceImpl implements InternalDNCListService {
	private static final Logger logger = LoggerFactory.getLogger(InternalDNCListServiceImpl.class);
	@Autowired
	private DNCListRepository dncListRepository;

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private PhoneDncService phoneDncService;

	@Autowired
	private InternalDNCListService internalDNCListService;
	
	private HashMap<Integer, String> headerIndex = new HashMap<Integer, String>();

	private InputStream fileInputStream;

	private String fileName;

	private Sheet sheet;

	private List<String> errorList = new ArrayList<String>(); 

	private Workbook book;

	private HashMap<Integer, List<String>> errorMap = new HashMap<>();

	private List<String> error12;
	
	private String xlsFlieName;

	@Override
	public void addNumber(IDNCNumberDTO idncNumberDTO) {
		DNCList dncListFromDB = null;
		String phoneNumber = null;
		List<DNCList> dncDBList = new ArrayList<DNCList>();
		if (idncNumberDTO.getPhoneNumber() != null) {
			phoneNumber = sanitizePhoneNumber(idncNumberDTO.getPhoneNumber());
			dncDBList = dncListRepository.findPhoneNumberByPartner(idncNumberDTO.getPartnerId(),
					idncNumberDTO.getPhoneNumber(), idncNumberDTO.getFirstName(), idncNumberDTO.getLastName());
		} else {
			// DATE :- 15/01/2020 Prospect DNC include firstname/lastname/phonenumber OR
			// firstname/lastname/domain.
			dncDBList = dncListRepository.findPartnerDNCByProspectDomain(idncNumberDTO.getPartnerId(),
					idncNumberDTO.getDomain(), idncNumberDTO.getFirstName(), idncNumberDTO.getLastName());
		}
		if (dncDBList != null && dncDBList.size() > 0) {
			for (DNCList dnc : dncDBList) {
				if (dnc.getPartnerId().equalsIgnoreCase(idncNumberDTO.getPartnerId())) {
					dncListFromDB = dnc;
				}
			}
		}

		if (dncListFromDB == null) { // only create if the number already does not exist in db
			if (idncNumberDTO.getTrigger() == null) {
				idncNumberDTO.setTrigger(DNCTrigger.DIRECT); // if no DNC Trigger then default to DIRECT
			}
			// add US number with and without country code
			if (phoneNumber != null && phoneNumber.length() == 11 && phoneNumber.startsWith("1")) {
				DNCList dncList1 = createDNC(phoneNumber, idncNumberDTO);
				dncListRepository.save(dncList1);
				logger.debug("addNumber() : Added [{}] to DNC", phoneNumber);

				String phoneNumberWithoutCountryCode = phoneNumber.substring(1, phoneNumber.length());
				DNCList dncList2 = createDNC(phoneNumberWithoutCountryCode, idncNumberDTO);
				dncListRepository.save(dncList2);
				logger.debug("addNumber() : Added [{}] to DNC", phoneNumber);
			} else {
				DNCList dncList = createDNC(phoneNumber, idncNumberDTO);
				dncListRepository.save(dncList);
				logger.debug("addNumber() : Added [{}] to DNC", phoneNumber);
			}
		} else {
			throw new IllegalArgumentException("This number is already present in the DNC list");
		}
	}

	@Override
	public String addNumber(DemandShoreDNCDTO dncNumberDTO) {
		String response = "Success";
		String phoneNumber = sanitizePhoneNumber(dncNumberDTO.getCompany_board_line());
		DNCList dncListFromDB = null;
		List<DNCList> dncDBList = dncListRepository.findPhoneNumber(phoneNumber, dncNumberDTO.getFirst_name(),
				dncNumberDTO.getLast_name());

		if (dncDBList != null && dncDBList.size() > 0) {
			for (DNCList dnc : dncDBList) {
				if (dnc.getPartnerId().equalsIgnoreCase(dncNumberDTO.getPartnerId())) {
					dncListFromDB = dnc;
				}
			}
		}

		IDNCNumberDTO idncNumberDTO = new IDNCNumberDTO();
		if (dncListFromDB == null) { // only create if the number already does not exist in db
			// if (dncNumberDTO.getTrigger() == null) {
			idncNumberDTO.setTrigger(DNCTrigger.DIRECT); // if no DNC Trigger then default to DIRECT
			// }

			DNCList dncList = new DNCList(phoneNumber, idncNumberDTO.getTrigger());
			// dncList.setNote(idncNumberDTO.getNote());
			dncList.setPartnerId(dncNumberDTO.getPartnerId());
			// dncList.setProspectCallId(idncNumberDTO.getProspectCallId());
			dncList.setCampaignId(dncNumberDTO.getCampaign_id());
			// dncList.setRecordingUrl(idncNumberDTO.getRecordingUrl());
			// dncList.setRecordingUrlAws(idncNumberDTO.getRecordingUrlAws());
			dncList.setFirstName(dncNumberDTO.getFirst_name());
			dncList.setLastName(dncNumberDTO.getLast_name());
			dncList.setCompany(dncNumberDTO.getCompany());
			dncList.setDomain(dncNumberDTO.getDomain());
			String dncLevel = XtaasConstants.DNC_PROSPECT;
			if (dncNumberDTO.getDomain() != null && !dncNumberDTO.getDomain().isEmpty())
				dncLevel = XtaasConstants.DNC_COMPANY;
			dncList.setDncLevel(dncLevel);
			dncListRepository.save(dncList);
			logger.debug("addNumber() : Added [{}] to DNC", phoneNumber);
		} else {
			response = "This number is already present in the DNC list";
			// throw new IllegalArgumentException("This number is already present in the DNC
			// list");
		}

		return response;
	}

	@Override
	public List<DNCList> getDNCByPartnerIdAndPhoneNumber(String partnerId, String phoneNumber) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findByPartnerIdAndPhoneNumber(partnerId, phoneNumber);
	}

	@Override
	public List<DNCList> getDNCByPartnerIdAndDomain(String partnerId, String domain) {
		if (StringUtils.isEmpty(domain)) {
			return new ArrayList<>();
		}
		return dncListRepository.findByPartnerIdAndDomain(partnerId, domain);
	}

	@Override
	public void createIDNCNumberDTO(Prospect prospect, Campaign campaign, String notes) {
		try{
			IDNCNumberDTO idncNumberDTO  = new IDNCNumberDTO();
			idncNumberDTO.setPhoneNumber(prospect.getPhone());
			idncNumberDTO.setNote(notes);
			idncNumberDTO.setTrigger(DNCList.DNCTrigger.DIRECT);
			idncNumberDTO.setFirstName(prospect.getFirstName());
			idncNumberDTO.setLastName(prospect.getLastName());
			idncNumberDTO.setCompany(prospect.getCompany());
			idncNumberDTO.setCampaignId(campaign.getId());
			idncNumberDTO.setPartnerId(campaign.getOrganizationId());
			if(prospect.getCustomAttributes() != null && prospect.getCustomAttributes().get("domain") != null &&
					org.apache.commons.lang.StringUtils.isNotBlank(prospect.getCustomAttributes().get("domain").toString())){
				idncNumberDTO.setDomain(prospect.getCustomAttributes().get("domain").toString());
			}
			idncNumberDTO.setDncLevel(XtaasConstants.DNC_PROSPECT);
			internalDNCListService.addNumber(idncNumberDTO);
		}catch (Exception e){
			logger.error("Error while creating DBC list for UK number. Exception [{}] ",e);
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT, Roles.ROLE_SUPERVISOR })
	public DNCList getDNCList(String phoneNumber, String partnerId) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findPhoneNumber(phoneNumber, partnerId);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT, Roles.ROLE_SUPERVISOR })
	public List<DNCList> getDNCList(String phoneNumber, String firstName, String lastName) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findPhoneNumber(phoneNumber, firstName, lastName);
	}

	public List<DNCList> getDNCListForBuy(String phoneNumber, String firstName, String lastName) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findPhoneNumber(phoneNumber, firstName, lastName);
	}

	@Override
	public long countDNCListForBuy(String phoneNumber, String firstName, String lastName) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.countByPhoneNumberFirstNameLastName(phoneNumber, firstName, lastName);
	}

	private String sanitizePhoneNumber(String phoneNumber) {
		phoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars
		return phoneNumber;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_ADMIN, Roles.ROLE_SUPERVISOR })
	public IDNCSearchResult getDNCNumbers(IDNCSearchDTO idncSearchDTO) {
		List<DNCList> dncNumbers = dncListRepository.findMatchingDNCList(idncSearchDTO.getPhoneNumber(),
				idncSearchDTO.getPartnerId(),  PageRequest.of(idncSearchDTO.getPage(), idncSearchDTO.getSize(),
						idncSearchDTO.getDirection(), "createdDate"));
		List<IDNCNumberDTO> dncNumberDTOs = new ArrayList<IDNCNumberDTO>(dncNumbers.size());
		for (DNCList dncNumber : dncNumbers) {
			dncNumberDTOs.add(new IDNCNumberDTO(dncNumber));
		}
		return new IDNCSearchResult(
				dncListRepository.countMatchingDNCList(idncSearchDTO.getPhoneNumber(), idncSearchDTO.getPartnerId()),
				dncNumberDTOs);
	}

	@Override
	public String createDNCList(MultipartFile file, String campaignId, String partnerId) throws IOException {
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		xlsFlieName = file.getOriginalFilename();
		if (!file.isEmpty()) {
			errorList = new ArrayList<String>();
			@SuppressWarnings("serial")
			final HashSet<String> excelSupportedContentTypes = new HashSet<String>() {
				{
					add("application/octet-stream");
					add("application/vnd.ms-excel");
					add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					add("application/ms-excel");
				}
			};
			if (excelSupportedContentTypes.contains(file.getContentType())) {
				fileInputStream = file.getInputStream();

				try {
					book = WorkbookFactory.create(fileInputStream);
					sheet = book.getSheetAt(0);
					int totalRowsCount = sheet.getPhysicalNumberOfRows();
					System.out.println(sheet.getLastRowNum());
					if (totalRowsCount < 1) {// check if nothing found in file
						book.close();
						logger.error(xlsFlieName + " file is empty");
						throw new IllegalArgumentException("File Can not be Empty.");
					}else if (totalRowsCount <= 1) {// check if nothing found in file
						book.close();
						logger.error("No records found in file " + xlsFlieName);
						throw new IllegalArgumentException("No Records Found in File.");
					}
					if (!validateExcelFileheader())
						return "";
					validateExcelFileRecords(loginUser,campaignId, partnerId);
					book.close();
				} catch (IOException e) {
					logger.error("Problem in reading the file " + xlsFlieName);
					return "Problem in reading the file " + xlsFlieName;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return "{\"code\": \"UPLOADING IN PROGRESS\", \"file\" : \"" + file.getOriginalFilename() + "\"}";

		} else {
			throw new IllegalArgumentException("File cannot be empty.");
		}
	}

	private boolean validateExcelFileheader() {
		Set<String> fileColumns = new HashSet<String>();
		final Row row = sheet.getRow(0);
		int colCounts = row.getPhysicalNumberOfCells();
		for (int j = 0; j < colCounts; j++) {
			Cell cell = row.getCell(j);
			if (cell == null || cell.getCellType() == CellType.BLANK) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1) + " Header should not be Empty");
				return false;
			}
			if (cell.getCellType() != CellType.STRING) {
				logger.error("Row " + (row.getRowNum() + 1) + ":- column " + (j + 1)
						+ " Header must contain only String values");
				return false;
			} else {
				headerIndex.put(j, cell.getStringCellValue().replace("*", ""));
				fileColumns.add(cell.getStringCellValue().replace("*", "").trim());
			}
		}
		return true;
	}

	private void validateExcelFileRecords(Login loginUser,String campaignId, String partnerId) {
	//	int rowsCount = sheet.getPhysicalNumberOfRows();
		int rowsCount = ErrorHandlindutils.getNonBlankRowCount(sheet);
		if (rowsCount < 2) {
			logger.error("No records found in file " + fileName);
			errorList.add("No records found in file " + fileName);
			return;
		}
		final Row tempRow = sheet.getRow(0);
		int colCounts = tempRow.getPhysicalNumberOfCells();
		processRecords(loginUser,0, rowsCount, colCounts, campaignId, partnerId);
	}

	public void processRecords(Login loginUser, int startPos, int EndPos, int colCounts, String campaignId, String partnerId) {
		List<IDNCbulkUpload> dncBulkList = new ArrayList<IDNCbulkUpload>();
		boolean invalidFile=false;
		for (int i = startPos; i < EndPos; i++) {
			IDNCbulkUpload dncBulkUpload = new IDNCbulkUpload();
			dncBulkUpload.setCampaignId(campaignId);
			dncBulkUpload.setPartnerId(partnerId);
			error12 = new ArrayList<>();
			logger.debug("excelFileRead():validateExcelFileRecords() : Reading record number = " + i);
			Row row = sheet.getRow(i);
			
			if ( i == 0  ) {
				continue;
			}
			if (row == null) {
				error12.add(" record is empty ");
				errorMap.put(i, error12);
				EndPos++;
				continue;
			}
			boolean emptyCell = true;
			for (int j = 0; j < colCounts; j++) {
				Cell cell = row.getCell(j);
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					emptyCell = false;
					break;
				}
			}
			if (emptyCell) {
//				errorList.add("Row " + (row.getRowNum() + 1) + ":- Record is Empty ");
				error12.add(" Record is missing ");
				errorMap.put(i, error12);
				EndPos++;
				continue;
			}
			
			boolean dncLevelValid = false;
			
			for (int j = 0; j < colCounts; j++) {
				String columnName = headerIndex.get(j).replace("*", "").trim();
				Cell cell = row.getCell(j);
				if (columnName.equalsIgnoreCase("DNC Level") || columnName.equalsIgnoreCase("DNCLevel")) {
					if (cell != null) {
						String dncLevel = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						if (!dncLevel.equalsIgnoreCase("") && !dncLevel.isEmpty()) {
							if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
								dncBulkUpload.setDncLevel(XtaasConstants.DNC_COMPANY);
								dncLevelValid = true;
								break;
							} else if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
								dncBulkUpload.setDncLevel(XtaasConstants.DNC_PROSPECT);
								dncLevelValid = true;
								break;
							}
						}
					}
					error12.add( columnName + " should  be Prospect or Company");
					invalidFile=true;
					break;
				}
			}
			if (dncLevelValid) {
				if (dncBulkUpload.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
					boolean isPhonePresent = false;
					boolean isDomainPresent = false;
					String phoneColumnName = "Phone";
					String domainColumnName = "Domain";
					for (int j = 0; j < colCounts; j++) {
						String columnName = headerIndex.get(j).replace("*", "").trim();
						Cell cell = row.getCell(j);
						if (columnName.equalsIgnoreCase("First Name") || columnName.equalsIgnoreCase("First*")
								|| columnName.equalsIgnoreCase("FirstName")) {
							if (cell != null) {
								String firstName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!firstName.equalsIgnoreCase("") && firstName != null && !firstName.isEmpty()) {
									dncBulkUpload.setFirstName(firstName);
								} else {
									error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- First Name is Empty ");
								error12.add( columnName + " is Empty ");
							}
						} else if (columnName.equalsIgnoreCase("Last Name") || columnName.equalsIgnoreCase("Last*")
								|| columnName.equalsIgnoreCase("LastName")) {
							if (cell != null) {
								String lastName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!lastName.equalsIgnoreCase("") && lastName != null && !lastName.isEmpty()) {
									dncBulkUpload.setLastName(lastName);
								}else {
									error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- LastName is Empty ");
								error12.add(columnName+ " is Empty ");
							}
		
						} else if (columnName.equalsIgnoreCase("Phone") || columnName.equalsIgnoreCase("Phone*")
								|| columnName.equalsIgnoreCase("Phone (country code is mandatory)")) {
							if (cell != null) {
								@SuppressWarnings("static-access")
								String phoneNumber = cell.getCellType() == CellType.STRING
										? phoneNumber = cell.getStringCellValue()
										: new BigDecimal(cell.getNumericCellValue()).toPlainString();
								if (!phoneNumber.equalsIgnoreCase("") && phoneNumber != null && !phoneNumber.isEmpty() && !phoneNumber.equalsIgnoreCase("0")) {
									phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
									dncBulkUpload.setPhoneNumber(phoneNumber);
									isPhonePresent = true;
								} else {
									// error12.add( columnName + " is Empty ");
									isPhonePresent = false;
									phoneColumnName = columnName;
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- Phone number is Empty ");
								// error12.add(columnName + " is Empty ");
								isPhonePresent = false;
								phoneColumnName = columnName;
							}
						} else if (columnName.equalsIgnoreCase("Company")) {
							if (cell != null) {
								String company = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!company.equalsIgnoreCase("") && !company.isEmpty()) {
									dncBulkUpload.setCompany(company);
								}else {
									// error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- Company is Empty ");
								// error12.add(columnName + " is Empty ");
							}
						} else if (columnName.equalsIgnoreCase("Domain")) {
							if (cell != null) {
								String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!domain.equalsIgnoreCase("") && !domain.isEmpty()) {
									dncBulkUpload.setDomain(domain);
									isDomainPresent = true;
								} else {
									isDomainPresent = false;
									domainColumnName = columnName;
								}
							} else {
									isDomainPresent = false;
									domainColumnName = columnName;
							}
						// } else if (columnName.equalsIgnoreCase("DNC Level") || columnName.equalsIgnoreCase("DNCLevel")) {
						// 	if (cell != null) {
						// 		String dncLevel = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						// 		if (!dncLevel.equalsIgnoreCase("") && !dncLevel.isEmpty()) {
						// 			if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
						// 				dncBulkUpload.setDncLevel(XtaasConstants.DNC_COMPANY);
						// 			} else if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
						// 				dncBulkUpload.setDncLevel(XtaasConstants.DNC_PROSPECT);
						// 			}	
						// 		}
						// 	}
						} else if (columnName.equalsIgnoreCase("Notes")) {
							if (cell != null) {
								String notes = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!notes.equalsIgnoreCase("") && !notes.isEmpty()) {
									dncBulkUpload.setNotes(notes);
								}
							}
						}
						if (j == colCounts - 1) {
							if (!isPhonePresent && !isDomainPresent) {
								error12.add(phoneColumnName + " or " + domainColumnName + " is Mandatory");
							}
						}
					}
				} else {
					boolean isPhonePresent = false;
					boolean isDomainPresent = false;
					String phoneColumnName = "Phone";
					String domainColumnName = "Domain";
					for (int j = 0; j < colCounts; j++) {
						String columnName = headerIndex.get(j).replace("*", "").trim();
						Cell cell = row.getCell(j);
						if (columnName.equalsIgnoreCase("First Name") || columnName.equalsIgnoreCase("First*")
								|| columnName.equalsIgnoreCase("FirstName")) {
							if (cell != null) {
								String firstName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!firstName.equalsIgnoreCase("") && firstName != null && !firstName.isEmpty()) {
									dncBulkUpload.setFirstName(firstName);
								} else {
									// error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- First Name is Empty ");
								// error12.add( columnName + " is Empty ");
							}
						} else if (columnName.equalsIgnoreCase("Last Name") || columnName.equalsIgnoreCase("Last*")
								|| columnName.equalsIgnoreCase("LastName")) {
							if (cell != null) {
								String lastName = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!lastName.equalsIgnoreCase("") && lastName != null && !lastName.isEmpty()) {
									dncBulkUpload.setLastName(lastName);
								}else {
									// error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- LastName is Empty ");
								// error12.add(columnName+ " is Empty ");
							}
		
						} else if (columnName.equalsIgnoreCase("Phone") || columnName.equalsIgnoreCase("Phone*")
								|| columnName.equalsIgnoreCase("Phone (country code is mandatory)")) {
							if (cell != null) {
								@SuppressWarnings("static-access")
								String phoneNumber = cell.getCellType() == CellType.STRING
										? phoneNumber = cell.getStringCellValue()
										: new BigDecimal(cell.getNumericCellValue()).toPlainString();
								if (!phoneNumber.equalsIgnoreCase("") && phoneNumber != null && !phoneNumber.isEmpty() && !phoneNumber.equalsIgnoreCase("0")) {
									phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
									dncBulkUpload.setPhoneNumber(phoneNumber);
									isPhonePresent = true;
								}else {
									// error12.add( columnName + " is Empty ");
									isPhonePresent = false;
									phoneColumnName = columnName;
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- Phone number is Empty ");
								// error12.add(columnName + " is Empty ");
								isPhonePresent = false;
								phoneColumnName = columnName;
							}
						} else if (columnName.equalsIgnoreCase("Company")) {
							if (cell != null) {
								String company = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!company.equalsIgnoreCase("") && !company.isEmpty()) {
									dncBulkUpload.setCompany(company);
								}else {
									// error12.add( columnName + " is Empty ");
								}
							} else {
							//						errorList.add("Row " + (row.getRowNum() + 1) + ":- Company is Empty ");
								// error12.add(columnName + " is Empty ");
							}
						} else if (columnName.equalsIgnoreCase("Domain")) {
							if (cell != null) {
								String domain = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!domain.equalsIgnoreCase("") && !domain.isEmpty()) {
									dncBulkUpload.setDomain(domain);
									isDomainPresent = true;
								} else {
									isDomainPresent = false;
									domainColumnName = columnName;
								}
							} else {
								isDomainPresent = false;
								domainColumnName = columnName;
							}
						// } else if (columnName.equalsIgnoreCase("DNC Level") || columnName.equalsIgnoreCase("DNCLevel")) {
						// 	if (cell != null) {
						// 		String dncLevel = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
						// 		if (!dncLevel.equalsIgnoreCase("") && !dncLevel.isEmpty()) {
						// 			if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
						// 				dncBulkUpload.setDncLevel(XtaasConstants.DNC_COMPANY);
						// 			} else if (dncLevel.equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
						// 				dncBulkUpload.setDncLevel(XtaasConstants.DNC_PROSPECT);
						// 			}	
						// 		}
						// 	}
						} else if (columnName.equalsIgnoreCase("Notes")) {
							if (cell != null) {
								String notes = XtaasUtils.removeNonAsciiChars(cell.getStringCellValue().trim());
								if (!notes.equalsIgnoreCase("") && !notes.isEmpty()) {
									dncBulkUpload.setNotes(notes);
								}
							}
						}
						if (j == colCounts - 1) {
							if (!isPhonePresent && !isDomainPresent) {
								error12.add(phoneColumnName + " or " + domainColumnName + " is Mandatory");
							}
						}
					}
				}
				
			}
			if (error12 != null && error12.size() > 0 ) {
				invalidFile=true;
			}

//		if (validateData(dncBulkUpload, row.getRowNum() + 1)) {
			if (!dncBulkList.contains(dncBulkUpload)) {
				dncBulkList.add(dncBulkUpload);
			}
//		}
				errorMap.put(i, error12);
		}
		Campaign campaign = campaignService.getCampaign(campaignId);
		//Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		List<String> reportingEmail = null;
		//if (organization != null && organization.getReportingEmail() != null && organization.getReportingEmail().size() > 0) {
			reportingEmail = loginUser.getReportEmails();
		//} 		
		if (errorMap != null && invalidFile) {
			ErrorHandlindutils.excelFileChecking(book, errorMap);
			try {
				ErrorHandlindutils.sendExcelwithMsg(loginUser,book,xlsFlieName,"IDNC",campaign.getName(),reportingEmail);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			throw new IllegalArgumentException("Please Check Your Email There is Some Error in + " xlsFlieName " + DNC File");
			logger.error("Please Check Your Email There is Some Error in " + xlsFlieName + " DNC File");
		} else {
			for (IDNCbulkUpload idnCbulkUpload : dncBulkList) {
				createDNCNumber(idnCbulkUpload);
			}
			
			//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
			String subject = "IDNC Upload Status Success For " + campaign.getName();
			//XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
			if(reportingEmail != null && reportingEmail.size() > 0) {
				for(String email : reportingEmail) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com",subject, "File Uploaded Successfully");	
				}
			}else {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com",subject, "File Uploaded Successfully");
			}
		}
	
		/*
		 * if (!errorList.isEmpty() && errorList.size() != 0) { StringBuilder
		 * stringBuilder = new StringBuilder(); for (String error : errorList) {
		 * stringBuilder.append(error); stringBuilder.append("<br />"); }
		 * XtaasEmailUtils.sendEmail(XtaasUserUtils.getCurrentLoggedInUserObject().
		 * getEmail(), "sup@xtaascorp.com", "DNC Errorlist", stringBuilder.toString());
		 * throw new IllegalArgumentException(
		 * "File not uploaded as some required fields are missing.Please check your email"
		 * ); } else { for (IDNCbulkUpload idnCbulkUpload : dncBulkList) {
		 * createDNCNumber(idnCbulkUpload); } }
		 */
	}

	private boolean validateData(IDNCbulkUpload dncBulkUpload, int rowNumber) {
		boolean valid = true;
		if (dncBulkUpload.getDncLevel() != null) {
			if (dncBulkUpload.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_PROSPECT)) {
				if (dncBulkUpload.getFirstName() == null || dncBulkUpload.getLastName() == null) {
					errorList.add("Row " + rowNumber + ":- First name, Last name, Phone/Domain is mandatory. ");
					valid = false;
				}
				if ((dncBulkUpload.getPhoneNumber() == null) && (dncBulkUpload.getDomain() == null)) {
					errorList.add("Row " + rowNumber + ":- First name, Last name, Phone/Domain is mandatory. ");
					valid = false;
				}
			} else if (dncBulkUpload.getDncLevel().equalsIgnoreCase(XtaasConstants.DNC_COMPANY)) {
				if ((dncBulkUpload.getCompany() == null)
						&& (dncBulkUpload.getPhoneNumber() == null || dncBulkUpload.getPhoneNumber().equals("0"))
						&& (dncBulkUpload.getDomain() == null)) {
					errorList.add("Row " + rowNumber + ":- Either Phone, Domain or Company name is mandatory. ");
					valid = false;
				}
			}
		} else {
			errorList.add("Row " + rowNumber + ":- Invalid DNC level. Please enter DNC level Prospect/Company.  ");
			valid = false;
		}
		return valid;
	}

	private void createDNCNumber(IDNCbulkUpload idnCbulkUpload) {
		Login loggedInUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		boolean isDNCFoundFromDB = false;
		List<DNCList> dncDBList = new ArrayList<DNCList>();
		if (loggedInUser != null) {
			String partnerId = loggedInUser.getOrganization();
			if (!StringUtils.isEmpty(idnCbulkUpload.getPartnerId())) {
				partnerId = idnCbulkUpload.getPartnerId();
			}
			String phoneNumber = idnCbulkUpload.getPhoneNumber();
			if (phoneNumber != null) {
				phoneNumber = sanitizePhoneNumber(idnCbulkUpload.getPhoneNumber());
				dncDBList = dncListRepository.findPhoneNumberByPartner(partnerId, phoneNumber,
						idnCbulkUpload.getFirstName(), idnCbulkUpload.getLastName());
			} else {
				dncDBList = dncListRepository.findPartnerDNCByProspectDomain(partnerId, idnCbulkUpload.getDomain(),
						idnCbulkUpload.getFirstName(), idnCbulkUpload.getLastName());
			}
			if (dncDBList != null && dncDBList.size() > 0) {
				isDNCFoundFromDB = true;
			}
			if (!isDNCFoundFromDB) {
				try {
					if (phoneNumber != null && phoneNumber.length() == 11 && phoneNumber.startsWith("1")) {
						DNCList dncNumber1 = new DNCList(idnCbulkUpload.getFirstName(), idnCbulkUpload.getLastName(),
								idnCbulkUpload.getPhoneNumber(), partnerId, idnCbulkUpload.getCompany(),
								idnCbulkUpload.getDomain(), idnCbulkUpload.getDncLevel(), DNCTrigger.DIRECT);
						dncNumber1.setCampaignId(idnCbulkUpload.getCampaignId());
						dncNumber1.setNote(idnCbulkUpload.getNotes());
						dncListRepository.save(dncNumber1);

						String phoneNumberWithoutCountryCode = phoneNumber.substring(1, phoneNumber.length());
						DNCList dncNumber2 = new DNCList(idnCbulkUpload.getFirstName(), idnCbulkUpload.getLastName(),
								phoneNumberWithoutCountryCode, partnerId, idnCbulkUpload.getCompany(),
								idnCbulkUpload.getDomain(), idnCbulkUpload.getDncLevel(), DNCTrigger.DIRECT);
						dncNumber2.setCampaignId(idnCbulkUpload.getCampaignId());
						dncNumber2.setNote(idnCbulkUpload.getNotes());
						dncListRepository.save(dncNumber2);
					} else {
						DNCList dncNumber = new DNCList(idnCbulkUpload.getFirstName(), idnCbulkUpload.getLastName(),
								idnCbulkUpload.getPhoneNumber(), partnerId, idnCbulkUpload.getCompany(),
								idnCbulkUpload.getDomain(), idnCbulkUpload.getDncLevel(), DNCTrigger.DIRECT);
						dncNumber.setCampaignId(idnCbulkUpload.getCampaignId());
						dncNumber.setNote(idnCbulkUpload.getNotes());
						dncListRepository.save(dncNumber);
					}
				} catch (Exception e) {
					// TODO: handle exception
					//e.printStackTrace();
				}
			}
		}
	}

	public class IDNCbulkUpload {
		public String firstName;
		public String lastName;
		public String phoneNumber;
		public String partnerId;
		public String campaignId;
		public String domain;
		public String company;
		public String dncLevel;
		public String notes;

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		public String getPartnerId() {
			return partnerId;
		}

		public void setPartnerId(String partnerId) {
			this.partnerId = partnerId;
		}

		public String getCampaignId() {
			return campaignId;
		}

		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getCompany() {
			return company;
		}

		public void setCompany(String company) {
			this.company = company;
		}

		public String getDncLevel() {
			return dncLevel;
		}

		public void setDncLevel(String dncLevel) {
			this.dncLevel = dncLevel;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT, Roles.ROLE_SUPERVISOR })
	public List<DNCList> getDNCList(String phoneNumber) {
		phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findPhoneNumber(phoneNumber);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT, Roles.ROLE_SUPERVISOR })
	public List<DNCList> getDNCListByDomain(String domain) {
		// phoneNumber = sanitizePhoneNumber(phoneNumber);
		return dncListRepository.findDomain(domain);
	}

	private DNCList createDNC(String phoneNumber, IDNCNumberDTO idncNumberDTO) {
		DNCList dncList = new DNCList(phoneNumber, idncNumberDTO.getTrigger());
		dncList.setNote(idncNumberDTO.getNote());
		dncList.setPartnerId(idncNumberDTO.getPartnerId());
		dncList.setProspectCallId(idncNumberDTO.getProspectCallId());
		dncList.setCampaignId(idncNumberDTO.getCampaignId());
		dncList.setRecordingUrl(idncNumberDTO.getRecordingUrl());
		dncList.setRecordingUrlAws(idncNumberDTO.getRecordingUrlAws());
		dncList.setFirstName(idncNumberDTO.getFirstName());
		dncList.setLastName(idncNumberDTO.getLastName());
		dncList.setCompany(idncNumberDTO.getCompany());
		dncList.setDomain(idncNumberDTO.getDomain());
		dncList.setDncLevel(idncNumberDTO.getDncLevel());
		if (idncNumberDTO.getDncLevel() != null && !idncNumberDTO.getDncLevel().isEmpty()
				&& idncNumberDTO.getDncLevel().equalsIgnoreCase("COMPANY")) {
			dncList.setProspectDNC(false);
			dncList.setCompanyDNC(true);
		} else {
			dncList.setProspectDNC(true);
			dncList.setCompanyDNC(false);
		}
		return dncList;
	}

	@Override
	public boolean checkLocalDNCList(String phone, String firstName, String lastName) {
		boolean isLocalDNC = false;
		phone = sanitizePhoneNumber(phone);
		long count = dncListRepository.countByPhoneNumberFirstNameLastName(phone, firstName, lastName);
		if (count > 0) {
			logger.debug(new SplunkLoggingUtils("checkLocalDNCList", phone)
					.eventDescription("Lead cannot be called as Phonenumber already exist in local DNC List")
					.addField("phoneNumber", phone).build());
			isLocalDNC = true;
		}
		return isLocalDNC;
	}

}
