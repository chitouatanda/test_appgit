package com.xtaas.application.service;

import java.util.List;

import com.xtaas.domain.entity.SignalwireOutboundNumber;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;

public interface SignalwireOutboundNumberService {

    public String getOutboundNumber(String teamId, String bucketName, String phone, int pool);

	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId);

    public SignalwireOutboundNumber save(SignalwireOutboundNumber signalwireOutboundNumber);

    public Boolean refreshOutboundNumbers();

    public void refreshPhoneNumbers(PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO);

    public List<SignalwireOutboundNumber> getAllOutboundNumbers();
    
}
