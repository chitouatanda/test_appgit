package com.xtaas.application.service;

public interface AuthTokenGeneratorService {

	public String generateUserToken(String appName, String userName);

	public String generateSystemToken(String appName, String systemUser);

	public String generateClientToken(String username, String password);

}
