package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.web.dto.CampaignCriteriaDTO;
import com.xtaas.web.dto.HealthChecksCriteriaDTO;
import com.xtaas.web.dto.HealthChecksDTO;
import com.xtaas.web.dto.HealthSearchDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;
import com.xtaas.db.entity.HealthChecks;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.repository.HealthChecksRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.service.ProspectCacheServiceImpl;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;

@Service
public class HealthCheckskServiceImpl implements HealthCheckskService {

	private static final Logger logger = LoggerFactory.getLogger(HealthCheckskServiceImpl.class);

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private HealthChecksRepository healthChecksRepository;
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Override
	public HealthSearchDTO getHealthCheckcount(ZoomBuySearchDTO zoomBuySearchDTO) {
		logger.debug(
				"getHealthCheckcount() : Health check count start for campaignId : " + zoomBuySearchDTO.getCampaignId());
		createHealthChecksEntry(zoomBuySearchDTO);
		long departmentHealthCount = 0;
		long titleHealthCount = 0;
		long industryHealthCount = 0;
		long employeeHealthCount = 0;
		long revenueHealthCount = 0;
		long countryHealthCount = 0;
		long sicCodeHealthCount = 0;
		long zipHealthCount = 0;
		long stateHealthCount = 0;
		CampaignCriteria campaignCriteria = new CampaignCriteria();
		setHealthCheckCriteria(campaignCriteria, zoomBuySearchDTO);
		if (campaignCriteria.getDepartment() != null && !campaignCriteria.getDepartment().isEmpty()) {
			departmentHealthCount = prospectCallLogRepository.getDepartmentHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		if (campaignCriteria.getTitleKeyWord() != null && !campaignCriteria.getTitleKeyWord().isEmpty()) {
			titleHealthCount = prospectCallLogRepository.getTitleHealthChecksCount(campaignCriteria, zoomBuySearchDTO);
		}
		if (campaignCriteria.getIndustry() != null && !campaignCriteria.getIndustry().isEmpty()) {
			industryHealthCount = prospectCallLogRepository.getIndustryHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}

		if ((campaignCriteria.getMinEmployeeCount() != null && !campaignCriteria.getMinEmployeeCount().isEmpty())
				|| (campaignCriteria.getMaxEmployeeCount() != null
						&& !campaignCriteria.getMaxEmployeeCount().isEmpty())) {
			employeeHealthCount = prospectCallLogRepository.getEmployeeHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}

		if ((campaignCriteria.getMinRevenue() != null && !campaignCriteria.getMinRevenue().isEmpty())
				|| (campaignCriteria.getMaxRevenue() != null && !campaignCriteria.getMaxRevenue().isEmpty())) {
			revenueHealthCount = prospectCallLogRepository.getRevenueHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getCountry() != null && !campaignCriteria.getCountry().isEmpty()) {
			countryHealthCount = prospectCallLogRepository.getCountryHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty()) {
			sicCodeHealthCount = prospectCallLogRepository.getSicCodeHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getNaicsCode() != null && !campaignCriteria.getNaicsCode().isEmpty()) {
			sicCodeHealthCount = prospectCallLogRepository.getNaicsCodeHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getState() != null && !campaignCriteria.getState().isEmpty()) {
			stateHealthCount = prospectCallLogRepository.getStateHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getUsStates() != null && !campaignCriteria.getUsStates().isEmpty()) {
			stateHealthCount = prospectCallLogRepository.getUSStateHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}
		
		if (campaignCriteria.getZipcodes() != null && !campaignCriteria.getZipcodes().isEmpty()) {
			zipHealthCount = prospectCallLogRepository.getZipHealthChecksCount(campaignCriteria,
					zoomBuySearchDTO);
		}

		long totalRecords = departmentHealthCount + titleHealthCount + industryHealthCount + employeeHealthCount
				+ revenueHealthCount + countryHealthCount + sicCodeHealthCount + stateHealthCount;
		HealthSearchDTO healthSearchDTO = new HealthSearchDTO();
		healthSearchDTO.setDepartmentHealthCount(departmentHealthCount);
		healthSearchDTO.setTitleHealthCount(titleHealthCount);
		healthSearchDTO.setIndustryHealthCount(industryHealthCount);
		healthSearchDTO.setEmployeeHealthCount(employeeHealthCount);
		healthSearchDTO.setRevenueHealthCount(revenueHealthCount);
		healthSearchDTO.setCountryHealthCount(countryHealthCount);
		healthSearchDTO.setSicCodeHealthCount(sicCodeHealthCount);
		healthSearchDTO.setStateHealthCount(stateHealthCount);
		healthSearchDTO.setZipHealthCount(zipHealthCount);
		healthSearchDTO.setTotalRecords(totalRecords);
		logger.debug("getHealthCheckcount() : Health check count : " + totalRecords);
		Campaign campaign = campaignService.getCampaign(zoomBuySearchDTO.getCampaignId());
		Map<String, Integer> nukeCountMap = new HashMap<String, Integer>();
		nukeCountMap.put(XtaasConstants.DEPARTMENT, (int)(departmentHealthCount));
		nukeCountMap.put(XtaasConstants.EMPLOYEE_COUNT, (int)(employeeHealthCount));
		nukeCountMap.put(XtaasConstants.REVENUE, (int)(revenueHealthCount));
		nukeCountMap.put(XtaasConstants.INDUSTRY, (int)(industryHealthCount));
		nukeCountMap.put(XtaasConstants.TITLE, (int)(titleHealthCount));
		nukeCountMap.put(XtaasConstants.VALID_COUNTRY, (int)(countryHealthCount));
		nukeCountMap.put(XtaasConstants.SIC_CODE, (int)(sicCodeHealthCount));
		nukeCountMap.put(XtaasConstants.VALID_STATE, (int)(stateHealthCount));
		nukeCountMap.put(XtaasConstants.ZIP_CODE, (int)(zipHealthCount));

		// clear prospect cache after data nuke
		prospectCacheServiceImpl.clearCache(zoomBuySearchDTO.getCampaignId());
		XtaasEmailUtils.sendNukeAndCallableCountEmail(nukeCountMap, XtaasUserUtils.getCurrentLoggedInUserObject(), Integer.toString(prospectCallLogRepository.findAllCallableRecordCount(campaign)),campaign);
		return healthSearchDTO;
	}
	
	private void createHealthChecksEntry(ZoomBuySearchDTO zoomBuySearchDTO) {
		HealthChecks healthChecks = new HealthChecks(zoomBuySearchDTO.getCampaignId(), zoomBuySearchDTO.getCriteria());
		healthChecksRepository.save(healthChecks);
		logger.debug("createHealthChecksEntry() : Created health checks entry for campaignId : " + zoomBuySearchDTO.getCampaignId());
	}

	// TODO - This method should be called from commonDataBuyServiceImpl once code
	// is merged
	public CampaignCriteria setCampaignCriteria(CampaignCriteria cc, List<Expression> expressions) {
		String removeIndustries = "";
		String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570";
		Map<String, String[]> revMap = getRevenueMap();
		Map<String, String[]> empMap = getEmployeeMap();
		List<Integer> minRevList = new ArrayList<Integer>();
		List<Integer> maxRevList = new ArrayList<Integer>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		if (!cc.isIgnoreDefaultIndustries()) {
			removeIndustries = "Public Safety|Cities,Towns & Municipalities|Cities,Towns & Municipalities General|Organizations|Membership Organizations|CharitableOrganizations & Foundations|Organizations General|ReligiousOrganizations|Government";
		}

		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();

			if (value.equalsIgnoreCase("All")) {
				if (attribute.equalsIgnoreCase("INDUSTRY")) {
					cc.setRemoveIndustries(removeIndustries);
				}
				continue;
			}

			if (operator.equalsIgnoreCase("NOT IN")) {
				boolean first = true;
				StringBuilder sb = new StringBuilder();
				List<PickListItem> pickList = exp.getPickList();
				if (attribute.equalsIgnoreCase("INDUSTRY")) {
					value = value + "," + removeIndusCodes;
				}
				String[] notInValsArr = value.split(",");
				List<String> notInValsList = Arrays.asList(notInValsArr);
				for (PickListItem pl : pickList) {
					if (notInValsList.contains(pl.getValue())) {
						// Do Nothing
					} else {
						if (first) {
							sb.append(pl.getValue());
							first = false;
						} else {
							sb.append(",");
							sb.append(pl.getValue());
						}
					}
				}
				value = sb.toString();
			}

			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("INDUSTRY")) {
				cc.setIndustry(value);

			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}

			if (attribute.equalsIgnoreCase("REVENUE")) {
				String[] revenueArr = value.split(",");
				for (int i = 0; i < revenueArr.length; i++) {
					String revVal = revenueArr[i];
					String[] revArr = revMap.get(revVal);

					minRevList.add(Integer.parseInt(revArr[0]));
					maxRevList.add(Integer.parseInt(revArr[1]));
				}

				Collections.sort(minRevList);
				Collections.sort(maxRevList, Collections.reverseOrder());
				if (minRevList.contains(new Integer(0))) {

				} else if (minRevList.get(0) > 0) {
					cc.setMinRevenue(minRevList.get(0).toString());
				}
				if (maxRevList.contains(new Integer(0))) {

				} else if (maxRevList.get(0) > 0) {
					cc.setMaxRevenue(maxRevList.get(0).toString());
				}
			}

			if (attribute.equalsIgnoreCase("EMPLOYEE")) {
				String[] employeeArr = value.split(",");
				for (int i = 0; i < employeeArr.length; i++) {
					String empVal = employeeArr[i];
					String[] empArr = empMap.get(empVal);

					minEmpList.add(Integer.parseInt(empArr[0]));
					maxEmpList.add(Integer.parseInt(empArr[1]));
				}

				Collections.sort(minEmpList);
				Collections.sort(maxEmpList, Collections.reverseOrder());
				if (minEmpList.contains(new Integer(0))) {

				} else if (minEmpList.get(0) > 0) {
					cc.setMinEmployeeCount(minEmpList.get(0).toString());
				}
				if (maxEmpList.contains(new Integer(0))) {

				} else if (maxEmpList.get(0) > 0) {
					cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
				}
			}
			if (attribute.equalsIgnoreCase("SIC_CODE")) {
				cc.setSicCode(value);
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
			if (attribute.equalsIgnoreCase("VALID_STATE")) {
				cc.setState(value);
			}
			if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
				cc.setTitleKeyWord(value);
			}
			if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
				cc.setNegativeKeyWord(value);
			}
		}
		if (cc.getIndustry() == null || cc.getIndustry().isEmpty()) {
			if (cc.getRemoveIndustries() != null && !cc.getRemoveIndustries().isEmpty()) {

			} else {
				cc.setRemoveIndustries(removeIndustries);
			}
		}
		return cc;
	}
	
	public CampaignCriteria setHealthCheckCriteria(CampaignCriteria cc, ZoomBuySearchDTO zoomBuySearchDTO) {
		
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for(HealthChecksCriteriaDTO dto : criteria) {
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC", dto.getPickList(), "");
			//Expression exp = new Expression(attribute, operator, value, agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}
		
		return setCampaignCriteria1(cc,expression);
		//return null;

//		for (HealthChecksCriteriaDTO exp : healthChecksDTO.getCriteria()) {
//			String attribute = exp.getAttribute();
//			String value = exp.getValue();
//
//			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
//				cc.setDepartment(value);
//			}
//			if (attribute.equalsIgnoreCase("INDUSTRY")) {
//				cc.setIndustry(value);
//			}
//			if (attribute.equalsIgnoreCase("empMin")) {
//				cc.setMinEmployeeCount(value);
//			}
//			if (attribute.equalsIgnoreCase("empMax")) {
//				cc.setMaxEmployeeCount(value);
//			}
//			if (attribute.equalsIgnoreCase("revMin")) {
//				cc.setMinRevenue(value);
//			}
//			if (attribute.equalsIgnoreCase("revMax")) {
//				cc.setMaxRevenue(value);
//			}
//			if (attribute.equalsIgnoreCase("title")) {
//				cc.setTitleKeyWord(value);
//			}
//		}
		
	}

	// TODO - This method should be called from DataBuyUtils once code is merged
	public Map<String, String[]> getRevenueMap() {
		Map<String, String[]> revMap = new HashMap<String, String[]>();
		/////
		String[] revArray1 = new String[2];
		revArray1[0] = "0";
		revArray1[1] = "10000000";
		revMap.put("0-$10M", revArray1);

		String[] revArray2 = new String[2];
		revArray2[0] = "5000000";
		revArray2[1] = "10000000";
		revMap.put("$5M-$10M", revArray2);

		String[] revArray3 = new String[2];
		revArray3[0] = "10000000";
		revArray3[1] = "25000000";
		revMap.put("$10M-$25M", revArray3);

		String[] revArray4 = new String[2];
		revArray4[0] = "25000000";
		revArray4[1] = "50000000";
		revMap.put("$25M-$50M", revArray4);

		String[] revArray5 = new String[2];
		revArray5[0] = "50000000";
		revArray5[1] = "100000000";
		revMap.put("$50M-$100M", revArray5);

		String[] revArray6 = new String[2];
		revArray6[0] = "100000000";
		revArray6[1] = "250000000";
		revMap.put("$100M-$250M", revArray6);

		String[] revArray7 = new String[2];
		revArray7[0] = "250000000";
		revArray7[1] = "500000000";
		revMap.put("$250M-$500M", revArray7);

		String[] revArray8 = new String[2];
		revArray8[0] = "500000000";
		revArray8[1] = "1000000000";
		revMap.put("$500M-$1B", revArray8);

		String[] revArray9 = new String[2];
		revArray9[0] = "1000000000";
		revArray9[1] = "5000000000";
		revMap.put("$1B-$5B", revArray9);

		String[] revArray10 = new String[2];
		revArray10[0] = "5000000000";
		revArray10[1] = "0";
		revMap.put("More than $5Billion", revArray10);

		return revMap;
	}

	// TODO - This method should be called from DataBuyUtils once code is merged
	public static Map<String, String[]> getEmployeeMap() {
		Map<String, String[]> empMap = new HashMap<String, String[]>();
		/////
		String[] empArray1 = new String[2];
		empArray1[0] = "0";
		empArray1[1] = "5";
		empMap.put("1-5", empArray1);

		String[] empArray2 = new String[2];
		empArray2[0] = "5";
		empArray2[1] = "10";
		empMap.put("5-10", empArray2);

		String[] empArray3 = new String[2];
		empArray3[0] = "10";
		empArray3[1] = "20";
		empMap.put("10-20", empArray3);

		String[] empArray4 = new String[2];
		empArray4[0] = "20";
		empArray4[1] = "50";
		empMap.put("20-50", empArray4);

		String[] empArray5 = new String[2];
		empArray5[0] = "50";
		empArray5[1] = "100";
		empMap.put("50-100", empArray5);

		String[] empArray6 = new String[2];
		empArray6[0] = "100";
		empArray6[1] = "250";
		empMap.put("100-250", empArray6);

		String[] empArray7 = new String[2];
		empArray7[0] = "250";
		empArray7[1] = "500";
		empMap.put("250-500", empArray7);

		String[] empArray8 = new String[2];
		empArray8[0] = "500";
		empArray8[1] = "1000";
		empMap.put("500-1000", empArray8);

		String[] empArray9 = new String[2];
		empArray9[0] = "1000";
		empArray9[1] = "5000";
		empMap.put("1000-5000", empArray9);

		String[] empArray10 = new String[2];
		empArray10[0] = "5000";
		empArray10[1] = "10000";
		empMap.put("5000-10000", empArray10);

		String[] empArray11 = new String[2];
		empArray11[0] = "10000";
		empArray11[1] = "0";
		empMap.put("Over 10000", empArray11);
		return empMap;
	}

	// TODO - This method should be called from DataBuyUtils once code is merged
	public static List<String> getSortedMgmtSearchList(String managementLevelStr) {
		List<String> sortedMgmtList = new ArrayList<String>();
		if (managementLevelStr != null && !managementLevelStr.isEmpty()) {
			String[] mgmtArray = managementLevelStr.split(",");
			List<String> searchList = Arrays.asList(mgmtArray);
			if (searchList.contains("Non Management"))
				sortedMgmtList.add("Non Management");
			if (searchList.contains("MANAGER"))
				sortedMgmtList.add("MANAGER");
			if (searchList.contains("DIRECTOR"))
				sortedMgmtList.add("DIRECTOR");
			if (searchList.contains("Mid Management"))
				sortedMgmtList.add("Mid Management");
			if (searchList.contains("VP_EXECUTIVES"))
				sortedMgmtList.add("VP_EXECUTIVES");
			if (searchList.contains("C_EXECUTIVES"))
				sortedMgmtList.add("C_EXECUTIVES");
			if (searchList.contains("Executives"))
				sortedMgmtList.add("Executives");
			if (searchList.contains("Board Members"))
				sortedMgmtList.add("Board Members");
		}
		return sortedMgmtList;
	}

	private Map<String, String> getManagementMap() {
		Map<String, String> mgmtMap = new HashMap<String, String>();
		mgmtMap.put("C_EXECUTIVES", "C-Level");
		mgmtMap.put("VP_EXECUTIVES", "VP-Level");
		mgmtMap.put("DIRECTOR", "Director");
		mgmtMap.put("MANAGER", "Manager");
		mgmtMap.put("Non Management", "Non-Manager");

		return mgmtMap;
	}

	private Map<String, String> getIndustryMap() {

		Map<String, String> industryMap = new HashMap<String, String>();
		industryMap.put("266", "Agriculture");
		industryMap.put("65802", " Animals & Livestock");
		industryMap.put("131338", " Crops");
		industryMap.put("196874", " Agriculture General");
		industryMap.put("522", " Business Services");
		industryMap.put("66058", " Accounting & Accounting Services");
		industryMap.put("197130", " Auctions");
		industryMap.put("262666", " Call Centers & Business Centers");
		industryMap.put("328202", " Debt Collection");
		industryMap.put("786954", " Management Consulting");
		industryMap.put("655882", " Information & Document Management");
		industryMap.put("852490", " Multimedia & Graphic Design");
		industryMap.put("393738", " Food Service");
		industryMap.put("983562", " Business Services General");
		industryMap.put("590346", " Human Resources & Staffing");
		industryMap.put("459274", " Facilities Management & Commercial Cleaning");
		industryMap.put("721418", " Translation & Linguistic Services");
		industryMap.put("131594", " Advertising & Marketing");
		industryMap.put("524810", " Commercial Printing");
		industryMap.put("918026", " Security Products & Services");
		industryMap.put("778", " Chambers of Commerce");
		industryMap.put("1034", " Construction");
		industryMap.put("66570", " Architecture, Engineering & Design");
		industryMap.put("132106", " Commercial & Residential Construction");
		industryMap.put("197642", " Construction General");
		industryMap.put("1290", " Consumer Services");
		industryMap.put("66826", " Automotive Service & Collision Repair");
		industryMap.put("132362", " Car & Truck Rental");
		industryMap.put("197898", " Funeral Homes & Funeral Related Services");
		industryMap.put("656650", " Consumer Services General");
		industryMap.put("263434", " Hair Salons");
		industryMap.put("328970", " Laundry & Dry Cleaning Services");
		industryMap.put("394506", " Photography Studio");
		industryMap.put("460042", " Travel Agencies & Services");
		industryMap.put("525578", " Veterinary Care");
		industryMap.put("591114", " Weight & Health Management");
		industryMap.put("1546", " Cultural");
		industryMap.put("198154", " Cultural General");
		industryMap.put("67082", " Libraries");
		industryMap.put("132618", " Museums & Art Galleries");
		industryMap.put("1802", " Education");
		industryMap.put("263946", " Education General");
		industryMap.put("132874", " K-12 Schools");
		industryMap.put("198410", " Training");
		industryMap.put("67338", " Colleges & Universities");
		industryMap.put("2058", " Energy, Utilities & Waste Treatment");
		industryMap.put("67594", " Electricity, Oil & Gas");
		industryMap.put("198666", " Waste Treatment, Environmental Services & Recycling");
		industryMap.put("329738", " Energy, Utilities, & Waste Treatment General");
		industryMap.put("133130", " Oil & Gas Exploration & Services");
		industryMap.put("264202", " Water & Water Treatment");
		industryMap.put("2314", " Finance");
		industryMap.put("67850", " Banking");
		industryMap.put("133386", " Brokerage");
		industryMap.put("198922", " Credit Cards & Transaction  Processing");
		industryMap.put("395530", " Finance General");
		industryMap.put("264458", " Investment Banking");
		industryMap.put("329994", " Venture Capital & Private Equity");
		industryMap.put("2570", " Government");
		industryMap.put("2826", " Healthcare");
		industryMap.put("133898", " Emergency Medical Transportation & Services");
		industryMap.put("330506", " Healthcare General");
		industryMap.put("68362", " Hospitals & Clinics");
		industryMap.put("199434", " Medical Testing & Clinical Laboratories");
		industryMap.put("264970", " Pharmaceuticals");
		industryMap.put("17042186", " Biotechnology");
		industryMap.put("33819402", " Drug Manufacturing & Research");
		industryMap.put("3082", " Hospitality");
		industryMap.put("330762", " Hospitality General");
		industryMap.put("68618", " Lodging & Resorts");
		industryMap.put("134154", " Recreation");
		industryMap.put("67243018", " Movie Theaters");
		industryMap.put("33688586", " Fitness & Dance Facilities");
		industryMap.put("50465802", " Gambling & Gaming");
		industryMap.put("16911370", " Amusement Parks, Arcades & Attractions");
		industryMap.put("84020234", " Zoos & National Parks");
		industryMap.put("199690", " Restaurants");
		industryMap.put("265226", " Sports Teams & Leagues");
		industryMap.put("3338", " Insurance");
		industryMap.put("3594", " Law Firms & Legal Services");
		industryMap.put("4106", " Media & Internet");
		industryMap.put("69642", " Broadcasting");
		industryMap.put("50401290", " Film/Video Production & Services");
		industryMap.put("16846858", " Radio Stations");
		industryMap.put("33624074", " Television Stations");
		industryMap.put("462858", " Media & Internet General");
		industryMap.put("135178", " Information Collection & Delivery");
		industryMap.put("200714", " Search Engines & Internet Portals");
		industryMap.put("266250", " Music & Music Related Services");
		industryMap.put("331786", " Newspapers & News Services");
		industryMap.put("397322", " Publishing");
		industryMap.put("3850", " Manufacturing");
		industryMap.put("69386", " Aerospace & Defense");
		industryMap.put("134922", " Boats & Submarines");
		industryMap.put("200458", " Building Materials");
		industryMap.put("16977674", " Aggregates, Concrete & Cement");
		industryMap.put("33754890", " Lumber, Wood Production & Timber Operations");
		industryMap.put("67309322", " Miscellaneous Building Materials (Flooring, Cabinets, etc.)");
		industryMap.put("50532106", " Plumbing & HVAC Equipment");
		industryMap.put("790282", " Motor Vehicles");
		industryMap.put("855818", " Motor Vehicle Parts");
		industryMap.put("265994", " Chemicals, Petrochemicals, Glass & Gases");
		industryMap.put("17043210", " Chemicals");
		industryMap.put("33820426", " Gases");
		industryMap.put("50597642", " Glass & Clay");
		industryMap.put("67374858", " Petrochemicals");
		industryMap.put("331530", " Computer Equipment & Peripherals");
		industryMap.put("17108746", " Personal Computers & Peripherals");
		industryMap.put("50663178", " Computer Networking Equipment");
		industryMap.put("67440394", " Network Security Hardware & Software");
		industryMap.put("33885962", " Computer Storage Equipment");
		industryMap.put("397066", " Consumer Goods");
		industryMap.put("17174282", " Appliances");
		industryMap.put("33951498", " Cleaning Products");
		industryMap.put("184946442", " Textiles & Apparel");
		industryMap.put("50728714", " Cosmetics, Beauty Supply & Personal Care Products");
		industryMap.put("67505930", " Consumer Electronics");
		industryMap.put("84283146", " Health & Nutrition Products");
		industryMap.put("101060362", " Household Goods");
		industryMap.put("117837578", " Pet Products");
		industryMap.put("134614794", " Photographic & Optical Equipment");
		industryMap.put("151392010", " Sporting Goods");
		industryMap.put("168169226", " Hand, Power and Lawn-care Tools");
		industryMap.put("201723658", " Jewelry & Watches");
		industryMap.put("462602", " Electronics");
		industryMap.put("34017034", " Batteries, Power Storage Equipment & Generators");
		industryMap.put("17239818", " Electronic Components");
		industryMap.put("50794250", " Power Conversion & Protection Equipment");
		industryMap.put("67571466", " Semiconductor & Semiconductor Equipment");
		industryMap.put("528138", " Food, Beverages & Tobacco");
		industryMap.put("17305354", " Food & Beverages");
		industryMap.put("34082570", " Tobacco");
		industryMap.put("50859786", " Wineries & Breweries");
		industryMap.put("593674", " Furniture");
		industryMap.put("1380106", " Manufacturing General");
		industryMap.put("659210", " Industrial Machinery & Equipment");
		industryMap.put("724746", " Medical Devices &  Equipment");
		industryMap.put("921354", " Pulp & Paper");
		industryMap.put("986890", " Plastic, Packaging & Containers");
		industryMap.put("1052426", " Tires & Rubber");
		industryMap.put("1117962", " Telecommunication  Equipment");
		industryMap.put("1183498", " Test & Measurement Equipment");
		industryMap.put("1249034", " Toys & Games");
		industryMap.put("1314570", " Wire & Cable");
		industryMap.put("4362", " Metals & Mining");
		industryMap.put("200970", " Metals & Mining General");
		industryMap.put("135434", " Metals & Minerals");
		industryMap.put("69898", " Mining");
		industryMap.put("4618", " Cities, Towns & Municipalities");
		industryMap.put("135690", " Cities, Towns & Municipalities General");
		industryMap.put("70154", " Public Safety");
		industryMap.put("4874", " Organizations");
		industryMap.put("135946", " Membership Organizations");
		industryMap.put("70410", " Charitable Organizations & Foundations");
		industryMap.put("267018", " Organizations General");
		industryMap.put("201482", " Religious Organizations");
		industryMap.put("5130", " Real Estate");
		industryMap.put("5386", " Retail");
		industryMap.put("136458", " Motor Vehicle Dealers");
		industryMap.put("201994", " Automobile Parts Stores");
		industryMap.put("1185034", " Record, Video & Book Stores");
		industryMap.put("70922", " Apparel & Accessories Retail");
		industryMap.put("333066", " Gas Stations, Convenience & Liquor Stores");
		industryMap.put("398602", " Department Stores, Shopping Centers & Superstores");
		industryMap.put("267530", " Consumer Electronics & Computers Retail");
		industryMap.put("595210", " Furniture");
		industryMap.put("1381642", " Retail General");
		industryMap.put("529674", " Flowers, Gifts & Specialty Stores");
		industryMap.put("660746", " Grocery Retail");
		industryMap.put("791818", " Home Improvement & Hardware Retail");
		industryMap.put("726282", " Vitamins, Supplements, & Health Stores");
		industryMap.put("857354", " Jewelry & Watch Retail");
		industryMap.put("988426", " Office Products Retail & Distribution");
		industryMap.put("922890", " Pet Products");
		industryMap.put("464138", " Drug Stores & Pharmacies");
		industryMap.put("1316106", " Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)");
		industryMap.put("1053962", " Sporting & Recreational Equipment Retail");
		industryMap.put("1119498", " Toys & Games");
		industryMap.put("1250570", " Video & DVD Rental");
		industryMap.put("5642", " Software");
		industryMap.put("136714", " Software & Technical Consulting");
		industryMap.put("267786", " Software General");
		industryMap.put("71178", " Software Development & Design");
		industryMap.put("16848394", " Business Intelligence (BI) Software");
		industryMap.put("33625610", " Content & Collaboration Software");
		industryMap.put("50402826", " Customer Relationship Management (CRM) Software");
		industryMap.put("67180042", " Database & File Management Software");
		industryMap.put("83957258", " Engineering Software");
		industryMap.put("100734474", " Enterprise Resource Planning (ERP) Software");
		industryMap.put("117511690", " Financial, Legal & HR Software");
		industryMap.put("134288906", " Healthcare Software");
		industryMap.put("151066122", " Multimedia, Games and Graphics Software");
		industryMap.put("167843338", " Networking Software");
		industryMap.put("184620554", " Supply Chain Management (SCM) Software");
		industryMap.put("201397770", " Security Software");
		industryMap.put("218174986", " Storage & System Management Software");
		industryMap.put("202250", " Retail Software");
		industryMap.put("5898", " Telecommunications");
		industryMap.put("71434", " Cable & Satellite");
		industryMap.put("268042", " Telecommunications General");
		industryMap.put("136970", " Internet Service Providers, Website Hosting & Internet-related Services");
		industryMap.put("202506", " Telephony & Wireless");
		industryMap.put("6154", " Transportation");
		industryMap.put("71690", " Airlines, Airports & Air Services");
		industryMap.put("137226", " Freight & Logistics Services");
		industryMap.put("399370", " Transportation General");
		industryMap.put("202762", " Marine Shipping & Transportation");
		industryMap.put("333834", " Trucking, Moving & Storage");
		industryMap.put("268298", " Rail, Bus & Taxi");

		return industryMap;
	}

	private Map<String, String> getDepartmentMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("4980839", "Consultants");
		titleMap.put("4980839", "Consulting");
		titleMap.put("4784231", "Engineering & Technical");
		titleMap.put("2293762", "Biometrics");
		titleMap.put("2228226", "Biotechnical Engineering");
		titleMap.put("3211266", "Chemical Engineering");
		titleMap.put("3801090", "Civil Engineering");
		titleMap.put("4653058", "Database Engineering &  Design");
		titleMap.put("4849666", "Engineering");
		titleMap.put("1376258", "Hardware Engineering");
		titleMap.put("5570562", "Industrial Engineering");
		titleMap.put("5505026", "Information Technology");
		titleMap.put("6029314", "Quality Assurance");
		titleMap.put("4718594", "Software Engineering");
		titleMap.put("1114114", "Technical");
		titleMap.put("5308418", "Web Development");
		titleMap.put("4587623", "Finance");
		titleMap.put("262146", "Accounting");
		titleMap.put("1769474", "Banking");
		titleMap.put("327682", "General Finance");
		titleMap.put("2621442", "Investment Banking");
		titleMap.put("1441794", "Wealth Management");
		titleMap.put("4718695", "Human Resources");
		titleMap.put("2162690", "Benefits & Compensation");
		titleMap.put("4915202", "Diversity");
		titleMap.put("3473410", "Human Resources Generalist");
		titleMap.put("5177346", "Recruiting");
		titleMap.put("5242882", "Sourcing");
		titleMap.put("5832807", "Legal");
		titleMap.put("1572866", "Governmental Lawyers & Legal Professionals");
		titleMap.put("1507330", "Lawyers & Legal Professionals");
		titleMap.put("4456551", "Marketing");
		titleMap.put("786434", "Advertising");
		titleMap.put("2555906", "Branding");
		titleMap.put("3932162", "Communications");
		titleMap.put("393216", "E-biz");
		titleMap.put("3276802", "General Marketing");
		titleMap.put("4325378", "Product Management");
		titleMap.put("5701735", "Medical & Health");
		titleMap.put("4784130", "Dentists");
		titleMap.put("3670018", "Doctors - General Practice");
		titleMap.put("589826", "Health Professionals");
		titleMap.put("3145730", "Nurses");
		titleMap.put("4653159", "Operations");
		titleMap.put("3014658", "Change Management");
		titleMap.put("4063234", "Compliance");
		titleMap.put("4390914", "Contracts");
		titleMap.put("2752514", "Customer Relations");
		titleMap.put("5373954", "Facilities");
		titleMap.put("5701634", "Logistics");
		titleMap.put("2686978", "General Operations");
		titleMap.put("6094850", "Quality Management");
		titleMap.put("4522087", "Sales");
		titleMap.put("393218", "Business Development");
		titleMap.put("196610", "General Sales");
		titleMap.put("4849767", "Scientists");
		titleMap.put("4259942", "General Management");

		return titleMap;

	}
	
	private CampaignCriteria setCampaignCriteria1(CampaignCriteria  cc,List<Expression> criteria){
		 String removeIndustries="";
		 String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570";
		 Map<String,String[]> revMap = getRevenueMap();
		 Map<String,String[]> empMap = getEmployeeMap();
		 List<Integer> minRevList = new ArrayList<Integer>();
		 List<Integer> maxRevList = new ArrayList<Integer>();
		 List<Integer> minEmpList = new ArrayList<Integer>();
		 List<Integer> maxEmpList = new ArrayList<Integer>();
		 if(!cc.isIgnoreDefaultIndustries()){
			 removeIndustries="Public Safety|Cities,Towns & Municipalities|Cities,Towns & Municipalities General|Organizations|Membership Organizations|Charitable Organizations & Foundations|Organizations General|Religious Organizations|Government";
		 }
		
		 for(Expression exp : criteria){
			 String attribute = exp.getAttribute(); 
			 String value = exp.getValue();
			 String operator = exp.getOperator();
			 
			 if(value.equalsIgnoreCase("All")){
				 if(attribute.equalsIgnoreCase("INDUSTRY")){
					 cc.setRemoveIndustries(removeIndustries);
				 }
				 continue;
			 }
			 
			 if(operator.equalsIgnoreCase("NOT IN")){
				 boolean first = true;
				 StringBuilder sb = new StringBuilder();
				 List<PickListItem> pickList = exp.getPickList();
				 if(attribute.equalsIgnoreCase("INDUSTRY")){
					 value = value+","+ removeIndusCodes;
				 }
				 String[] notInValsArr = value.split(",");
				 List<String> notInValsList = Arrays.asList(notInValsArr);
				 for(PickListItem pl : pickList){
					 if(notInValsList.contains(pl.getValue())){
						 //Do Nothing
						 if(removeIndustries==null || removeIndustries.isEmpty())
							 removeIndustries = pl.getLabel();
						 else
						 removeIndustries = removeIndustries+","+pl.getLabel();
					 }else{
						 if(first){
							 sb.append(pl.getValue());
							 first = false;
						 }else{
							 sb.append(",");
							 sb.append(pl.getValue());
						 }
					 }
				 }
				 value = sb.toString();
			 }
			
			
			 if(attribute.equalsIgnoreCase("DEPARTMENT")){
				 cc.setDepartment(value);
			 }
			 if(attribute.equalsIgnoreCase("INDUSTRY")){
				 cc.setIndustry(value);
				// cc.setPrimaryIndustriesOnly(true);
			 }
			 if(attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")){
				 cc.setManagementLevel(value);
				 List<String> mgmtSortedList = getSortedMgmtSearchList(value);
				 cc.setSortedMgmtSearchList(mgmtSortedList);
			 }
			/* if(attribute.equalsIgnoreCase("MAX_EMP")){
				 cc.setMaxEmployeeCount(value);
			 }
			 if(attribute.equalsIgnoreCase("MIN_EMP")){
				 cc.setMinEmployeeCount(value);
			 }
			 if(attribute.equalsIgnoreCase("REV_MAX")){
				 cc.setMaxRevenue(value);
			 }
			 if(attribute.equalsIgnoreCase("REV_MIN")){
				 cc.setMinRevenue(value);
			 }*/
			 //////////////////////
			 if(attribute.equalsIgnoreCase("REVENUE")){
				 String[] revenueArr = value.split(",");
				 cc.setSortedRevenuetSearchList(getRevenuSortedeMap(revenueArr));
				 for(int i=0;i<revenueArr.length;i++){
					 String revVal = revenueArr[i];
					 String[] revArr = revMap.get(revVal);
					 
					 minRevList.add(Integer.parseInt(revArr[0]));
					 maxRevList.add(Integer.parseInt(revArr[1]));
				 }
				 
				 Collections.sort(minRevList);
				 Collections.sort(maxRevList,Collections.reverseOrder());
				 if(minRevList.contains(new Integer(0))){
					 
				 }else if(minRevList.get(0)>0){
					 cc.setMinRevenue(minRevList.get(0).toString());
			 	}
			 	if(maxRevList.contains(new Integer(0))){
				 
			 	}else if(maxRevList.get(0)>0){
					 cc.setMaxRevenue(maxRevList.get(0).toString());
		 		}
			 }
			 
			 if(attribute.equalsIgnoreCase("EMPLOYEE")){
				 String[] employeeArr = value.split(",");
				 cc.setSortedEmpSearchList(getEmpSortedMap(employeeArr));
				 for(int i=0;i<employeeArr.length;i++){
					 String empVal = employeeArr[i];
					 String[] empArr = empMap.get(empVal);
					 
					 minEmpList.add(Integer.parseInt(empArr[0]));
					 maxEmpList.add(Integer.parseInt(empArr[1]));
				 }
				 
				 Collections.sort(minEmpList);
				 Collections.sort(maxEmpList,Collections.reverseOrder());
				 if(minEmpList.contains(new Integer(0))){
					 
				 }else if(minEmpList.get(0)>0){
					 cc.setMinEmployeeCount(minEmpList.get(0).toString());
				 }
				 if(maxEmpList.contains(new Integer(0))){
					 
				 }else if(maxEmpList.get(0)>0){
					 cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
				 }
			 }
			 
			 //////////////////////////
			 
			 if(attribute.equalsIgnoreCase("SIC_CODE")){
				 cc.setSicCode(value);
			 }
			 if(attribute.equalsIgnoreCase("VALID_COUNTRY")){
				 List<String> countryList =  Arrays.asList(value.split(","));
				 cc.setCountryList(countryList);
				 cc.setCountry(value);
			 }
			 if(attribute.equalsIgnoreCase("VALID_STATE")){
				 cc.setState(value);
			 }
			 if(attribute.equalsIgnoreCase("SEARCH_TITLE")){
				 cc.setTitleKeyWord(value);
			 }
			 if(attribute.equalsIgnoreCase("NEGATIVE_TITLE")){
				 cc.setNegativeKeyWord(value);
			 }
			 /*if(attribute.equalsIgnoreCase("REMOVE_DEPT")){
				 cc.setRemoveDepartments(value);
			 }
			 if(attribute.equalsIgnoreCase("REMOVE_INDUSTRIES")){
				 cc.setRemoveIndustries(value);
			 }*/
		 }
		 if(cc.getIndustry()==null || cc.getIndustry().isEmpty()){
			 if(cc.getRemoveIndustries()!=null && !cc.getRemoveIndustries().isEmpty()){
				 //Dont do any thing
			 }else{
				 cc.setRemoveIndustries(removeIndustries);
			 }
		 }
		 return cc;
	 }
	
	 private LinkedHashMap<String, String[]> getRevenuSortedeMap(String[] revarr){
		 List<String> revSearch = Arrays.asList(revarr);
			LinkedHashMap<String,String[]> revMap = new LinkedHashMap<String, String[]>();
			/////
			if(revSearch.contains("0-$10M")) {
				 String[] revArray1 = new String[2];
				 revArray1[0]="0";
				 revArray1[1]="10000";
				 revMap.put("0-$10M", revArray1);
			}
			if(revSearch.contains("$5M-$10M")) {	
				 String[] revArray2 = new String[2];
				 revArray2[0]="5000";
				 revArray2[1]="10000";
				 revMap.put("$5M-$10M", revArray2);
			}
			if(revSearch.contains("$10M-$25M")) {
				 String[] revArray3 = new String[2];
				 revArray3[0]="10000";
				 revArray3[1]="25000";
				 revMap.put("$10M-$25M", revArray3);
			}
			if(revSearch.contains("$25M-$50M")) {
				 String[] revArray4 = new String[2];
				 revArray4[0]="25000";
				 revArray4[1]="50000";
				 revMap.put("$25M-$50M", revArray4);
			}
			if(revSearch.contains("$50M-$100M")) {	 
				 String[] revArray5 = new String[2];
				 revArray5[0]="50000";
				 revArray5[1]="100000";
				 revMap.put("$50M-$100M", revArray5);
			}
			if(revSearch.contains("$100M-$250M")) {
				 String[] revArray6 = new String[2];
				 revArray6[0]="100000";
				 revArray6[1]="250000";
				 revMap.put("$100M-$250M", revArray6);
			}
			if(revSearch.contains("$250M-$500M")) {
				 String[] revArray7 = new String[2];
				 revArray7[0]="250000";
				 revArray7[1]="500000";
				 revMap.put("$250M-$500M", revArray7);
			}
			if(revSearch.contains("$500M-$1B")) {
				 String[] revArray8 = new String[2];
				 revArray8[0]="500000";
				 revArray8[1]="1000000";
				 revMap.put("$500M-$1B", revArray8);
			}
			if(revSearch.contains("$1B-$5B")) {
				 String[] revArray9 = new String[2];
				 revArray9[0]="1000000";
				 revArray9[1]="5000000";
				 revMap.put("$1B-$5B", revArray9);
			}
			if(revSearch.contains("More than $5Billion")) {
				 String[] revArray10 = new String[2];
				 revArray10[0]="5000000";
				 revArray10[1]="0";
				 revMap.put("More than $5Billion", revArray10);
			}
			
			return revMap;
		}
	 
	 private LinkedHashMap<String,String[]> getEmpSortedMap(String[] searchEmp){
		 List<String> empSearch = Arrays.asList(searchEmp);
		 LinkedHashMap<String,String[]> sortedEmpMap = new LinkedHashMap<String, String[]>();
		 if(empSearch.contains("1-5")) {
		 String[] empArray1 = new String[2];
		 empArray1[0]="0";
		 empArray1[1]="4";
		 sortedEmpMap.put("1-5", empArray1);
		 }
		 if(empSearch.contains("5-10")) {
		 String[] empArray2 = new String[2];
		 empArray2[0]="5";
		 empArray2[1]="9";
		 sortedEmpMap.put("5-10", empArray2);
		 }
		 if(empSearch.contains("20-50")) {
		 String[] empArray3 = new String[2];
		 empArray3[0]="10";
		 empArray3[1]="19";
		 sortedEmpMap.put("10-20", empArray3);
		 }
		 if(empSearch.contains("1-5")) {
		 String[] empArray4 = new String[2];
		 empArray4[0]="20";
		 empArray4[1]="49";
		 sortedEmpMap.put("20-50", empArray4);
		 }
		 if(empSearch.contains("1-5")) {
		 String[] empArray5 = new String[2];
		 empArray5[0]="50";
		 empArray5[1]="99";
		 sortedEmpMap.put("50-100", empArray5);
		 }
		 if(empSearch.contains("100-250")) {
		 String[] empArray6 = new String[2];
		 empArray6[0]="100";
		 empArray6[1]="249";
		 sortedEmpMap.put("100-250", empArray6);
		 }
		 if(empSearch.contains("250-500")) {
		 String[] empArray7 = new String[2];
		 empArray7[0]="250";
		 empArray7[1]="499";
		 sortedEmpMap.put("250-500", empArray7);
		 }
		 if(empSearch.contains("500-1000")) {
		 String[] empArray8 = new String[2];
		 empArray8[0]="500";
		 empArray8[1]="999";
		 sortedEmpMap.put("500-1000", empArray8);
		 }
		 if(empSearch.contains("1000-5000")) {
		 String[] empArray9 = new String[2];
		 empArray9[0]="1000";
		 empArray9[1]="4999";
		 sortedEmpMap.put("1000-5000", empArray9);
		 }
		 if(empSearch.contains("5000-10000")) {
		 String[] empArray10 = new String[2];
		 empArray10[0]="5000";
		 empArray10[1]="9999";
		 sortedEmpMap.put("5000-10000", empArray10);
		 }
		 if(empSearch.contains("Over 10000")) {
		 String[] empArray11 = new String[2];
		 empArray11[0]="10000";
		 empArray11[1]="0";
		 sortedEmpMap.put("Over 10000", empArray11);
		 }

		 return sortedEmpMap;
		 }
}
