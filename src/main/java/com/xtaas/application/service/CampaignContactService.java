package com.xtaas.application.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.xtaas.domain.entity.ListProviderUsage;
import com.xtaas.web.dto.CampaignContactDTO;

public interface CampaignContactService {
	
	public String createCampaignContact(String campaignId, MultipartFile file, HttpSession httpSession);
	
	public List<ListProviderUsage> getListPurchaseHistory(String campaignId);
	
	public List<CampaignContactDTO> getListPurchasedRecords(String listPurchaseTransactionId);
	
}