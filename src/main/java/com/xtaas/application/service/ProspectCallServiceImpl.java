package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.web.dto.ProspectDTO;

@Service
public class ProspectCallServiceImpl implements ProspectCallService {

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	/*
	 * START UPDATE PROSPECT DETAILS DATE : 26/05/2017 REASON : Below methods are
	 * used to update Prospect/Lead details from QA page
	 */
	@Override
	public void updateProspectCall(ProspectDTO prospectDTO) {
		ProspectCallLog prospectCallLogFromDB = prospectCallLogRepository
				.findByProspectCallId(prospectDTO.getProspectCallId());
		Prospect prospectFromDB = prospectCallLogFromDB.getProspectCall().getProspect();
		/*
		 * if string is NULL or less than 250 then take string as it is otherwise take
		 * substring from 0 to 250 characters.
		 */
		prospectFromDB.setFirstName((prospectDTO.getFirstName() == null || prospectDTO.getFirstName().length() < 250)
				? prospectDTO.getFirstName()
				: prospectDTO.getFirstName().substring(0, 250));
		prospectFromDB.setLastName((prospectDTO.getLastName() == null || prospectDTO.getLastName().length() < 250)
				? prospectDTO.getLastName()
				: prospectDTO.getLastName().substring(0, 250));
		prospectFromDB
				.setAddressLine1((prospectDTO.getAddressLine1() == null || prospectDTO.getAddressLine1().length() < 250)
						? prospectDTO.getAddressLine1()
						: prospectDTO.getAddressLine1().substring(0, 250));
		prospectFromDB
				.setAddressLine2((prospectDTO.getAddressLine2() == null || prospectDTO.getAddressLine2().length() < 250)
						? prospectDTO.getAddressLine2()
						: prospectDTO.getAddressLine2().substring(0, 250));
		prospectFromDB
				.setCity((prospectDTO.getCity() == null || prospectDTO.getCity().length() < 250) ? prospectDTO.getCity()
						: prospectDTO.getCity().substring(0, 250));
		prospectFromDB.setStateCode(prospectDTO.getStateCode());
		prospectFromDB.setCountry(prospectDTO.getCountry());
		prospectFromDB.setZipCode(prospectDTO.getZipCode());
		prospectFromDB.setExtension(prospectDTO.getExtension());
		prospectFromDB.setPhone(prospectDTO.getPhone());
		prospectFromDB.setEmail(prospectDTO.getEmail());
		prospectFromDB.setCompany(
				(prospectDTO.getCompany() == null || prospectDTO.getCompany().length() < 250) ? prospectDTO.getCompany()
						: prospectDTO.getCompany().substring(0, 250));
		prospectFromDB.setTitle(
				(prospectDTO.getTitle() == null || prospectDTO.getTitle().length() < 250) ? prospectDTO.getTitle()
						: prospectDTO.getTitle().substring(0, 250));
		prospectFromDB.setDepartment((prospectDTO.getDepartment() == null || prospectDTO.getDepartment().length() < 250)
				? prospectDTO.getDepartment()
				: prospectDTO.getDepartment().substring(0, 250));
		prospectFromDB.setManagementLevel(
				(prospectDTO.getManagementLevel() == null || prospectDTO.getManagementLevel().length() < 250)
						? prospectDTO.getManagementLevel()
						: prospectDTO.getManagementLevel().substring(0, 250));

		prospectFromDB.setIndustry((prospectDTO.getIndustry() == null || prospectDTO.getIndustry().length() < 250)
				? prospectDTO.getIndustry()
				: prospectDTO.getIndustry().substring(0, 250));
		prospectFromDB.setIndustryList(getAllIndustries(prospectDTO));
		prospectFromDB.setCustomAttributes(getcustomAttributesValue(prospectDTO));

		prospectFromDB.setCallbackTimeInMS(prospectDTO.getCallbackTimeInMS());
		prospectFromDB.setTopLevelIndustries(prospectDTO.getTopLevelIndustries());
		prospectFromDB.setZoomCompanyUrl(prospectDTO.getZoomCompanyUrl());
		prospectFromDB.setZoomPersonUrl(prospectDTO.getZoomPersonUrl());
		prospectFromDB.setCompanyAddress(prospectDTO.getCompanyAddress());
		ProspectCall prospectCall = prospectCallLogFromDB.getProspectCall();
		prospectCallLogFromDB.setProspectCall(prospectCall);
		prospectCallLogRepository.save(prospectCallLogFromDB);

		/*
		 * DATE:06/03/2018 REASON:fetching prospectCallInteraction from DB then
		 * modifying & storing it in DB. If it is null then creating new
		 * prospectCallInteraction
		 */
//		ProspectCallInteraction prospectCallInteraction = prospectCallInteractionRepository
//				.findByProspectCallProspectCallIdAndStatus(prospectDTO.getProspectCallId(),
//						prospectCallLogFromDB.getStatus());
//		if (prospectCallInteraction == null) {
//			prospectCallInteraction = new ProspectCallInteraction();
//		}
//		prospectCallInteraction.setProspectCall(prospectCall);
//		prospectCallInteraction.setStatus(prospectCallLogFromDB.getStatus());
//
//		prospectCallInteractionRepository.save(prospectCallInteraction);
	}

	private List<String> getAllIndustries(ProspectDTO prospectDTO) {
		if (prospectDTO.getIndustryList() == null)
			return null;
		List<String> allIndustries = new ArrayList<String>();
		ListIterator<String> itr = prospectDTO.getIndustryList().listIterator();
		while (itr.hasNext()) {
			String industry = itr.next();
			allIndustries.add((industry == null || industry.length() < 250) ? industry : industry.substring(0, 250));
		}
		return allIndustries;
	}

	private Map<String, Object> getcustomAttributesValue(ProspectDTO prospectDTO) {
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		Map<String, Object> agentCustomAttributes = prospectDTO.getCustomAttributes();
		customAttributes.put("minRevenue", prospectDTO.getCustomAttributes().get("minRevenue"));
		customAttributes.put("maxRevenue", prospectDTO.getCustomAttributes().get("maxRevenue"));
		customAttributes.put("maxEmployeeCount", prospectDTO.getCustomAttributes().get("maxEmployeeCount"));
		customAttributes.put("minEmployeeCount", prospectDTO.getCustomAttributes().get("minEmployeeCount"));
		customAttributes.put("domain", prospectDTO.getCustomAttributes().get("domain"));
		return customAttributes;
	}
	/* END UPDATE PROSPECT DETAILS */

	@Override
	public void updateProspectStatus(String prospectCallId, String leadStatus) {
		ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(prospectCallId);
		if(prospectCallLog != null) {
			if(leadStatus.equalsIgnoreCase("SUCCESS" )) {
				prospectCallLog.getProspectCall().setDispositionStatus("SUCCESS");
				prospectCallLog.getProspectCall().setSubStatus("SUBMITLEAD");
				prospectCallLog.getProspectCall().setLeadStatus("NOT_SCORED");
			} else {
				prospectCallLog.getProspectCall().setDispositionStatus("FAILURE");
				prospectCallLog.getProspectCall().setSubStatus(leadStatus);
				prospectCallLog.getProspectCall().setLeadStatus(null);
			}
			prospectCallLogRepository.save(prospectCallLog);
		}

	}

	public void updateRecordingUrl (String pcid, String recordingUrl) {
		ProspectCallLog prospectCallObj = prospectCallLogRepository.findByProspectCallId(pcid);
		if (prospectCallObj != null) {
			ProspectCall prospectObj = prospectCallObj.getProspectCall();
			prospectObj.setRecordingUrl(recordingUrl);
			prospectCallObj.setProspectCall(prospectObj);
			prospectCallLogRepository.save(prospectCallObj);
		}
	}

	public void updateAWSRecordingUrl (String pcid, String awsRecordingUrl) {
		ProspectCallLog prospectCallObj = prospectCallLogRepository.findByProspectCallId(pcid);
		if (prospectCallObj != null) {
			ProspectCall prospectObj = prospectCallObj.getProspectCall();
			prospectObj.setRecordingUrlAws(awsRecordingUrl);
			prospectCallObj.setProspectCall(prospectObj);
			prospectCallLogRepository.save(prospectCallObj);
		}
	}

}
