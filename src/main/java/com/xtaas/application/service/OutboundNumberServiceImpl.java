/**
 * 
 */
package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.OutboundNumberRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.CountryDetails;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;

/**
 * @author djain
 *
 */
@Service
public class OutboundNumberServiceImpl implements OutboundNumberService {

	@Autowired
	private OutboundNumberRepository outboundNumberRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Value("#{new java.util.Random()}")
	private Random random;

	private Map<String, List<OutboundNumber>> outboundNumbersMap = new HashMap<>();

	Map<String, Integer> poolMap = new HashMap<>();

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT })
	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool) {
		Team team = teamRepository.findOneById(teamId);
		String partnerId = team.getPartnerId();
		// String cacheKey = partnerId + "_" + bucketName;
		List<OutboundNumber> outboundNumberList;

		int areaCode = Integer.parseInt(findAreaCode(phone));
		outboundNumberList = outboundNumberRepository.findByBucketWithAreaCode(bucketName, areaCode);
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumberRepository.findByPartnerBucket(partnerId, bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumberRepository.findOutboundNumbers(partnerId);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumberRepository.findOutboundNumbersByBucketNumber(bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumberRepository.findByPartnerBucket("XTAAS CALL CENTER", "DEFAULT");
		}
		// find a random number from the list
		OutboundNumber outboundNumber = outboundNumberList.get(random.nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	@Override
	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId) {
		List<OutboundNumber> outboundNumbers = outboundNumbersMap.get(organizationId);
		List<OutboundNumber> outboundNumberList = new ArrayList<>();
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			getAllOutboundNumber();
			outboundNumbers = outboundNumbersMap.get(organizationId);
			getPoolDetails(outboundNumbers);
		}
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = callRetryCount % maxPoolSize;
		int areaCode = Integer.parseInt(findAreaCode(phoneNumber));

		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbersMap.get("XTAAS CALL CENTER");
		}

		OutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	public void getAllOutboundNumber() {
		List<OutboundNumber> outboundNumbers = outboundNumberRepository.findAll();
		outboundNumbersMap = outboundNumbers.stream().collect(Collectors.groupingBy(OutboundNumber::getPartnerId));
	}

	private void getPoolDetails(List<OutboundNumber> outboundNumbers) {
		for (OutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (poolMap.get(key) == null) {
				poolMap.put(key, outboundNumber.getPool());
			} else if (poolMap.get(key) < new Integer(outboundNumber.getPool())) {
				poolMap.put(key, outboundNumber.getPool());
			}
		}
	}

	private String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replaceAll("[^\\d]", ""); // phone.replace("-", "");
		int length = removeSpclChar.length();
		/*
		 * handled phone numbers with length less than 10 digit & return first two digit
		 * as area code - e.g. Germany: +494023750
		 */
		if (length < 10) {
			return removeSpclChar.substring(0, 2);
		}
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = removeSpclChar.substring(startPoint, endPoint);
		return areaCode;
	}

	@Override
	public List<OutboundNumber> getAllOutboundNumbers() {
		return outboundNumberRepository.findAll();
	}

	@Override
	public List<CountryDetails> getDistinctCountryInformation() {
		return outboundNumberRepository.getDistinctCountryDetails();
	}

	@Override
	public Boolean refreshOutboundNumbers() {
		if (outboundNumbersMap != null) {
			outboundNumbersMap.clear();
			return true;
		}
		return false;
	}

}
