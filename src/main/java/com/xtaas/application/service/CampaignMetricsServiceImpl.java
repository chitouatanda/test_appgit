package com.xtaas.application.service;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.EmailBouncesRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.CampaignMetricsDTO;

@Service
public class CampaignMetricsServiceImpl implements CampaignMetricsService {
    
    private static final Logger logger = LoggerFactory.getLogger(CampaignMetricsServiceImpl.class);

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;
	
	
	@Autowired
	private EmailBouncesRepository emailBouncesRepository;
	
	
	public enum CampaignMetrics {
		COUNT_CAMPAIGN_AGENTS,
		COUNT_ONLINE_AGENTS,
		COUNT_TOTAL_PROSPECTS,
		COUNT_PROSPECTS_CONTACTED,
		COUNT_PROSPECTS_DELIVERED,
		COUNT_PROSPECTS_DIALED,
		AVG_QA_SCORE,
		
	}

	@Override
	public CampaignMetricsDTO getCampaignMetricsByCampaign(String campaignId, String fromDateStr, String toDateStr, String timeZone, int noOfDays) throws ParseException {
		Interval todayTimeInterval = XtaasDateUtils.getDateTimeInterval(new Date(),timeZone);
//		int prospectContactedCount = 0;
		
		//findout date range based on number days selected by user
		DateTime toDate = XtaasDateUtils.getCurrentDateInTZ(0, timeZone);
        DateTime fromDate = XtaasDateUtils.getDaysAfterSpecifiedDate(toDate, timeZone, (noOfDays*-1) ); //go backwards in time to find the start date for data 
		Interval timeInterval = XtaasDateUtils.getDateTimeInterval(fromDate, toDate, timeZone);
		
		//Date fromDate = convertTimeZoneOfFromDate(fromDateStr,timeZone);
	    //Date toDate =   convertTimeZoneOfToDate(toDateStr,timeZone);
		
		//Retrieve count of agents allocated to the campaign
		int campaignAgentsCount = campaignService.getAllocatedAgents(campaignId).size();
		//Retrieve currently online  agents count
		int onlineCount = agentActivityLogRepository.getOnlineAgentsCount(campaignId, new Date(todayTimeInterval.getStartMillis()), new Date(todayTimeInterval.getEndMillis()));
		//Retrieve total prospects of campaign
		// int totalProspectCount = prospectCallLogRepository.countByCampaignId(campaignId);
		//Retrieve count of total contacted prospects of campaign
		/* Commented prospectContactedCount query for optimisation of supervisor screen. */
		int prospectContactedCount = prospectCallLogRepository.countProspectsContacted(campaignId, timeInterval.getStart().toDate(), timeInterval.getEnd().toDate());
		//Retrieve count of total delivered prospects of campaign
		int prospectDeliverdCount = prospectCallLogRepository.countProspectsDelivered(campaignId, timeInterval.getStart().toDate(), timeInterval.getEnd().toDate());
		//Retrieve count of total prospects dialed
		// 02/08/2019 commented dialed count query as it is not required on supervisor screen
		int prospectDialedCount = 1; // prospectCallLogRepository.countProspectsDialed(campaignId, timeInterval.getStart().toDate(), timeInterval.getEnd().toDate());
		// Retrieve score of campaign
		// 02/08/2019 commented score count query as it is not required on supervisor screen 
		double qaScore = 0; // prospectCallLogRepository.getAvgFeedbackScore(campaignId, timeInterval.getStart().toDate(), timeInterval.getEnd().toDate());
		
		//construct campaign metrics
		/* Removed non used object properties of campaign metrics object for optimisation of supervisor screen. */
		HashMap<CampaignMetrics, String> metrics = new HashMap<CampaignMetrics, String>();
		metrics.put(CampaignMetrics.COUNT_CAMPAIGN_AGENTS, String.valueOf(campaignAgentsCount));
		metrics.put(CampaignMetrics.COUNT_ONLINE_AGENTS, String.valueOf(onlineCount));
		// metrics.put(CampaignMetrics.COUNT_TOTAL_PROSPECTS, String.valueOf(totalProspectCount));
		metrics.put(CampaignMetrics.COUNT_PROSPECTS_CONTACTED, String.valueOf(prospectContactedCount));
		metrics.put(CampaignMetrics.COUNT_PROSPECTS_DELIVERED, String.valueOf(prospectDeliverdCount));
//		 metrics.put(CampaignMetrics.COUNT_PROSPECTS_DIALED, String.valueOf(prospectDialedCount));
		metrics.put(CampaignMetrics.AVG_QA_SCORE, String.valueOf(qaScore));
		
		CampaignMetricsDTO campaignMetricsDTO = new CampaignMetricsDTO();
		campaignMetricsDTO.setCampaignId(campaignId);
		campaignMetricsDTO.setMetrics(metrics);
		
		return campaignMetricsDTO;
	}

	private Date convertTimeZoneOfFromDate(String fromDateStr,String timeZone) throws ParseException{
	    Date fromDate = null;
	    final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        if (fromDateStr != null && !fromDateStr.trim().isEmpty()) {
            fromDate = XtaasDateUtils.convertTimeZoneOfDate(fromDateStr+" 00:00:00", timeZone, timeZone,  DATE_FORMAT); //fromDate  according to campaign specific time zone 
        }
        return fromDate;
	}
	private Date convertTimeZoneOfToDate(String toDateStr,String timeZone) throws ParseException{
	    Date toDate = null;
	    final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	    if (toDateStr != null && !toDateStr.trim().isEmpty()) {
            toDate = XtaasDateUtils.convertTimeZoneOfDate(toDateStr+" 23:59:59", timeZone, timeZone, DATE_FORMAT);//toDate  according to campaign specific time zone
        }
	    return toDate;
	}
	
    @Override
    public HashMap<String, Integer> getEmailBounceCount(String campaignId,String fromDateStr, String toDateStr,String timeZone,int noOfDays) throws ParseException {
        DateTime toDate = XtaasDateUtils.getCurrentDateInTZ(0, timeZone);
        DateTime fromDate = XtaasDateUtils.getDaysAfterSpecifiedDate(toDate, timeZone, (noOfDays*-1) ); //go backwards in time to find the start date for data 
        Interval timeInterval = XtaasDateUtils.getDateTimeInterval(fromDate, toDate, timeZone);
        
        // Date fromDate = convertTimeZoneOfFromDate(fromDateStr,timeZone);
        //Date toDate =   convertTimeZoneOfToDate(toDateStr,timeZone);
        int emailBounceCount= emailBouncesRepository.countEmailBounces(campaignId, timeInterval.getStart().toDate(), timeInterval.getEnd().toDate());
        logger.debug("getEmailBounceCount() for Campaign " +campaignId+"  : Total Email Bounce Count : [{}]", emailBounceCount);
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        hashMap.put("EMAIL_BOUNCE_COUNT", emailBounceCount);
        return hashMap;
    }

}
