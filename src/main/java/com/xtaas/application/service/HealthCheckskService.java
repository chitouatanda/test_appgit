package com.xtaas.application.service;

import com.xtaas.web.dto.HealthChecksDTO;
import com.xtaas.web.dto.HealthSearchDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;

public interface HealthCheckskService {

	public HealthSearchDTO getHealthCheckcount(ZoomBuySearchDTO zoomBuySearchDTO);
}
