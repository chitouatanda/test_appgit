package com.xtaas.application.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.client.TwilioCapability;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.instance.Recording;
import com.twilio.sdk.resource.list.RecordingList;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.ActivePreviewCall;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityCallStatus;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityType;
import com.xtaas.db.entity.AgentActivityLogDNorm;
import com.xtaas.db.entity.AgentStatusLog;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.DNCList.DNCTrigger;
import com.xtaas.db.entity.Domain;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.PCIAnalysis;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.entity.SuccessCallLog;
import com.xtaas.db.entity.TeamNotice;
//import com.xtaas.db.entity.SuppressionList;
//import com.xtaas.db.entity.SuppressionListNew;
import com.xtaas.db.entity.Vertical;
import com.xtaas.db.repository.ActivePreviewCallQueueRepository;
import com.xtaas.db.repository.AgentActivityLogDNormRepository;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.AgentStatusLogRepository;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.GlobalContactRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.PCIAnalysisRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.RealTimeDeliveryRepository;
import com.xtaas.db.repository.SuccessCallLogRepository;
import com.xtaas.db.repository.TeamNoticeRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.db.repository.VerticalRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.service.AgentCalendarService;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;
import com.xtaas.domain.valueobject.CampaignResource;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.DialerCodeDispositionStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.FailureDispositionStatus;
import com.xtaas.domain.valueobject.RealTimePost;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.domain.valueobject.WorkSlot;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.infra.zoominfo.service.DataSlice;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.pusher.Pusher;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.AgentRegistrationServiceImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.AutoApproveAgentRequestService;
import com.xtaas.service.ConfigService;
import com.xtaas.service.DomoUtilsService;
import com.xtaas.service.GeographyService;
import com.xtaas.service.LeadReportServiceImpl;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectCacheServiceImpl;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.SuppressionListService;
import com.xtaas.service.TelnyxService;
import com.xtaas.service.UserService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.web.dto.AddressComponent;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.AgentCallMonitoringDTO;
import com.xtaas.web.dto.AgentSearchDTO;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.DemandShoreDNCDTO;
import com.xtaas.web.dto.DemandShoreStatusDTO;
import com.xtaas.web.dto.GeoCodingDTO;
import com.xtaas.web.dto.GeoCodingDetilsDTO;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.IndirectProspectsDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.ProspectCallSearchClause;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.TelnyxRecordingUrlObjForMap;
import com.xtaas.domain.valueobject.CampaignTypes;

@Service
public class AgentServiceImpl implements AgentService {
	private static  Logger logger = LoggerFactory.getLogger(AgentServiceImpl.class);

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private RealTimeDeliveryRepository realTimeDeliveryRepository;

	@Autowired
	private VerticalRepository verticalRepository;

	@Autowired
	private CallLogRepository callLogRepository;
	
	@Autowired
	TeamNoticeService teamNoticeService;

	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;

	@Autowired
	private AgentActivityLogDNormRepository agentActivityLogDNormRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private GeographyService geographyService;

	@Autowired
	private UserService userService;

	@Autowired
	private AgentCalendarService agentCalendarService;

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private AgentRegistrationServiceImpl agentRegistrationServiceImpl;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectLogInteractionRepository;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Autowired
	private QaService qaService;

	@Autowired
	private ConfigService configService;

	@SuppressWarnings("unused")
	@Autowired
	private CallService callService;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private ActivePreviewCallQueueRepository activePreviewCallQueueRepository;

	@Autowired
	private LeadReportServiceImpl leadReportServiceImpl;

	@Autowired
	private SuccessCallLogRepository successCallLogRepository;

	@Autowired
	private ProspectCacheServiceImpl prospectCacheServiceImpl;

	@Autowired
	private AgentStatusLogRepository agentStatusLogRepository;

	@Autowired
	private PCIAnalysisRepository pciAnalysisRepository;

	@Autowired
	private DomoUtilsService  domoUtilsService;

	@Autowired
	private TelnyxService telnyxService;

	@Autowired 
	private CallClassificationJobQueueRepository callClassificationJobQueueRepository;

	@Autowired
    private GlobalContactRepository globalContactRepository;
	
	@Autowired
	private TeamNoticeService teamNoticeSerive;

	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;
	
	@Autowired
	private DataSlice dataSlice;
	
	@Autowired
	private TeamNoticeRepository teamNoticeRepository;
	
	@Autowired
	private AutoApproveAgentRequestService autoApproveAgentRequestService;

	@Autowired
	private SuppressionListService suppressionListService;

	@Value("${server.access.url}")
	private String serverAccessUrl;

	private Map<String, Agent> agentsInMemory = new HashMap<String, Agent>();
	
	@Override
//	@Authorize(roles = { Roles.ROLE_AGENT })
	public Agent getAgent( String agentId) {
		if (agentsInMemory.isEmpty()) {
			storeAgentsInMemory();
		}
		if (this.agentsInMemory.containsKey(agentId)) {
			return agentsInMemory.get(agentId);
		} else {
			return agentRepository.findOneById(agentId);
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public List<Agent> getAgents( List<String> agentIds) {
		return agentRepository.findAgentByIds(agentIds);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<AgentSearchResult> searchByCriteria( AgentSearchDTO agentSearchDTO,  Pageable pageRequest) {
		Campaign campaign = null;
		 StringBuilder domain = new StringBuilder();
		if (agentSearchDTO.isIncludeDomain()) {
			campaign = campaignRepository.findOneById(agentSearchDTO.getCampaignId());
			if (campaign == null) {
				throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
			}
			 Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			domain.append(organization.getVertical()).append('|').append(campaign.getDomain());
		}
		return agentRepository.searchAgents(agentSearchDTO.getCountries(), agentSearchDTO.getStates(),
				agentSearchDTO.getCities(), agentSearchDTO.getTimeZones(), agentSearchDTO.getLanguages(),
				agentSearchDTO.getRatings(), domain.toString(), agentSearchDTO.getMaximumPerHourPrice(),
				agentSearchDTO.getMinimumCompletedCampaigns(), agentSearchDTO.getMinimumTotalVolume(),
				agentSearchDTO.getMinimumAverageConversion(), agentSearchDTO.getMinimumAverageQuality(),
				agentSearchDTO.getMinimumAvailableHours(), pageRequest);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<Object> countByCriteria( AgentSearchDTO agentSearchDTO) {
		Campaign campaign = null;
		 StringBuilder domain = new StringBuilder();
		if (agentSearchDTO.isIncludeDomain()) {
			campaign = campaignRepository.findOneById(agentSearchDTO.getCampaignId());
			if (campaign == null) {
				throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
			}
			 Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			domain.append(organization.getVertical()).append('|').append(campaign.getDomain());
		}

		 List<String> countries = geographyService.getIncludedCountries(agentSearchDTO.getCountries(),
				agentSearchDTO.isExcludeCountries());
		 List<String> states = geographyService.getIncludedStates(countries, agentSearchDTO.getStates(),
				agentSearchDTO.isExcludeStates());
		 List<String> cities = geographyService.getIncludedCities(countries, states, agentSearchDTO.getCities(),
				agentSearchDTO.isExcludeCities());

		return agentRepository.countAgents(countries, states, cities, agentSearchDTO.getTimeZones(),
				agentSearchDTO.getLanguages(), agentSearchDTO.getRatings(), domain.toString(),
				agentSearchDTO.getMaximumPerHourPrice(), agentSearchDTO.getMinimumCompletedCampaigns(),
				agentSearchDTO.getMinimumTotalVolume(), agentSearchDTO.getMinimumAverageConversion(),
				agentSearchDTO.getMinimumAverageQuality(), agentSearchDTO.getMinimumAvailableHours());
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<AgentSearchResult> searchByPreviousCampaigns( String campaignId) {
		 Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		 List<Campaign> campaigns = campaignRepository.findBySelfManagedTeam(userLogin.getUsername(), campaignId,
				PageRequest.of(0, 1, Direction.DESC, "createdDate"));
		 List<String> agentIds = new ArrayList<String>();
		for ( Campaign campaign : campaigns) {
			for ( CampaignAgentInvitation agentInvitation : campaign.getAgentInvitations()) {
				agentIds.add(agentInvitation.getAgentId());
			}
		}
		return agentRepository.findByIds(agentIds);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA, Roles.ROLE_SUPERVISOR })
	public List<AgentSearchResult> searchByCampaign( String campaignId) {
		 Campaign campaign = campaignRepository.findOneById(campaignId);
		 List<AgentSearchResult> agentSearchResults = agentRepository.findByIds(getCampaignAgentIds(campaign));
		return getRequireFieldsAgentSearchResult(agentSearchResults);

	}

	private List<String> getCampaignAgentIds( Campaign campaign) {
		 CampaignTeam team = campaign.getTeam();
		 List<String> agentIds = new ArrayList<String>();
		if (team != null) {
			 List<CampaignResource> agents = team.getAgents();
			if (agents != null && !agents.isEmpty()) {
				for ( CampaignResource campaignResource : agents) {
					agentIds.add(campaignResource.getResourceId());
				}
			}
		}
		return agentIds;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<AgentSearchResult> searchByAutosuggest( String campaignId,  Pageable pageRequest) {
		Campaign campaign = null;
		 StringBuilder domain = new StringBuilder();
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for autosuggest based search");
		}
		 Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		domain.append(organization.getVertical()).append('|').append(campaign.getDomain());

		@SuppressWarnings("serial")

		List<Double> autoSuggestRatings = new ArrayList<Double>() {
			{
				add(3.0);
				add(4.0);
				add(4.5);
				add(5.0);
			}
		};

		// This is hardcoded search criteria based on campaign's domains with
		// teams with at least 20 completed campaigns
		// and 75 conversion rate. This will be modified in the future to
		// consider analytics
		return agentRepository.searchAgents(null, null, null, null, null, autoSuggestRatings, domain.toString(), 0, 20,
				0, 0, 75, 0, pageRequest);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<AgentSearchResult> searchByCurrentInvitations( String campaignId,  Pageable pageRequest) {
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for current invitation based search");
		} else if (!campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Agent invitations are only available for self managed campaigns");
		}
		 List<String> agentIds = new ArrayList<String>();
		for ( CampaignAgentInvitation agentInvitation : campaign.getAgentInvitations()) {
			agentIds.add(agentInvitation.getAgentId());
		}
		 List<AgentSearchResult> results = agentRepository.findByIds(agentIds);
		for ( AgentSearchResult result : results) {
			for ( CampaignAgentInvitation agentInvitation : campaign.getAgentInvitations()) {
				if (result.getId().equals(agentInvitation.getAgentId())) {
					result.setInvitationDate(agentInvitation.getInvitationDate());
					break;
				}
			}
		}
		return results;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<AgentSearchResult> searchByCurrentSelection( String campaignId) {
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for current invitation based search");
		} else if (!campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Agent invitations are only available for self managed campaigns");
		}
		 List<String> agentIds = new ArrayList<String>();
		if (campaign.getTeam() != null) {
			for ( CampaignResource resource : campaign.getTeam().getAgents()) {
				agentIds.add(resource.getResourceId());
			}
		}
		return agentRepository.findByIds(agentIds);

	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public List<AgentSearchResult> searchBySupervisor() {
		 List<Team> teams = teamRepository.findByAdminSupervisorAndSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
		 List<String> agentIds = new ArrayList<String>();

		for ( Team team : teams) {
			if (team.getAgents() != null && team.getAgents().size() > 0) {
				for ( TeamResource agent : team.getAgents()) {
					agentIds.add(agent.getResourceId());
				}
			}
		}

		// Date:11/10/2017 Reason:TO show only Active agents to supervisor
		 List<AgentSearchResult> agents = agentRepository.findByIdsAndByStatus(agentIds);
		for ( AgentSearchResult agent : agents) {
			// set the profile pic url
			 Login user = loginRepository.findOneById(agent.getId());
			if (user != null) {
				agent.setAgentProfilePicUrl(user.getProfilePicUrl());
			} else {
				logger.error("searchBySupervisor() : User not created for Agent [{}]", agent.getId());
			}
		}
		return agents;
	}

	@Override
//	@Authorize(roles = { Roles.ROLE_SUPERVISOR, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA })
	public AgentCampaignDTO getCurrentAllocatedCampaign( String agentUserName) {
		AgentCampaignDTO agentCampaignDTO = null;
		 Agent agent = agentRepository.findOneById(agentUserName);
		if (agent == null) {
			throw new IllegalArgumentException(agentUserName + " is not a valid Agent");
		}
		Map<LocalDate, TreeSet<WorkSlot>> calendar;
		if (!agent.isIndependent()) {
			// then check if supervisorOverrideCampaignId is set for an agent in Team
			// collection
			 List<Team> agentTeams = teamRepository.findByAgentIdWithCampaignOverriden(agentUserName);
			if (agentTeams != null && agentTeams.size() > 0) {
				for ( Team agentTeam : agentTeams) { // for each of the team of this agent is member of
					for ( TeamResource resource : agentTeam.getAgents()) { // for each agent to find the agent in
																			// question
						if (agentUserName.equals(resource.getResourceId())
								&& resource.getSupervisorOverrideCampaignId() != null) {
							 Campaign campaign = campaignRepository.findOneById(resource.getSupervisorOverrideCampaignId());
							if (campaign == null) {
								throw new IllegalArgumentException("Campaign doesn't exist for ["
										+ resource.getSupervisorOverrideCampaignId() + "] id.");
							}
							prospectCallLogService.generateCallbackCache(campaign, true);
							prospectCallLogService.generateMissedCallbackCache(campaign, true);
							agentCampaignDTO = prepareCampaignDTOForAgent(campaign);
							agentCampaignDTO.setTempAllocation(Boolean.TRUE);
							return agentCampaignDTO;
						}
					}
				}
			}
		}
		 DateTime dt = DateTime.now(DateTimeZone.forID(agent.getAddress().getTimeZone()));
		 LocalDate localDateinAgentTZ = dt.toLocalDate();
		calendar = agentCalendarService.getCalendar(localDateinAgentTZ, localDateinAgentTZ.plusDays(1), agent);
		 TreeSet<WorkSlot> workslots = calendar.get(localDateinAgentTZ); // get today's date calendar in Agent's TZ

		WorkSlot nextWorkSlot = null;
		// find the work slot by current time
		if (workslots != null) {
			for ( WorkSlot slot : workslots) {
				if (!slot.isAvailable()) {
					 int currHourOfDay = dt.getHourOfDay();
					if (slot.containsHour(currHourOfDay)) {
						 Campaign campaign = campaignRepository.findOneById(slot.getWorkUnitIdfier());
						agentCampaignDTO = prepareCampaignDTOForAgent(campaign);
						break;
					} else {
						if (nextWorkSlot == null) {
							nextWorkSlot = slot;
						} else {
							 int nextWorkSlotDistanceFromCurrHr = ((nextWorkSlot.getStartHour() - currHourOfDay) < 0)
									? (nextWorkSlot.getStartHour() + 24 - currHourOfDay)
									: (nextWorkSlot.getStartHour() - currHourOfDay);
							 int thisSlotDistanceFromCurrHr = ((slot.getStartHour() - currHourOfDay) < 0)
									? (slot.getStartHour() + 24 - currHourOfDay)
									: (slot.getStartHour() - currHourOfDay);

							if (thisSlotDistanceFromCurrHr < nextWorkSlotDistanceFromCurrHr) {
								nextWorkSlot = slot;
							}
						}
					}
				}
			}
		}

		// set the nextWorkSlot
		if (nextWorkSlot != null) {
			if (agentCampaignDTO == null)
				agentCampaignDTO = new AgentCampaignDTO();
			agentCampaignDTO.setNextWorkSlot(nextWorkSlot);
		}
		return agentCampaignDTO;
	}

	public AgentCampaignDTO getAgentCampaign( String campaignId) {
		return prepareCampaignDTOForAgent(campaignRepository.findOneById(campaignId));
	}

	private AgentCampaignDTO prepareCampaignDTOForAgent( Campaign campaign) {
		// TODO uncomment line after discussion
		// HashMap<String, CRMAttribute> orgCRMMapping =
		// crmMappingService.getCRMAttributeMap(campaign.getOrganizationId());
		 Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		// AgentCampaignDTO agentCampaignDTO = new AgentCampaignDTO(campaign,
		// orgCRMMapping);
		// TODO remove below line and uncomment above line after discussion
		 AgentCampaignDTO agentCampaignDTO = new AgentCampaignDTO(campaign);
		agentCampaignDTO.setVertical(organization.getVertical());
		agentCampaignDTO.setLiteMode(organization.isLiteMode());
		return agentCampaignDTO;
	}

	/**
	 * Returns the campaign an agent should work at this hour
	 */
	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public AgentCampaignDTO getCurrentAllocatedCampaign() {
		return getCurrentAllocatedCampaign(XtaasUserUtils.getCurrentLoggedInUsername());
	}

	@Override
	public void getCurrentAllocatedProspect() {
		 Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		 AgentCall agentCall = agentRegistrationService.getAgentCall(userLogin.getId());
		try {
			if (agentCall != null && agentCall.getProspectCall() != null) {
				pushMessageToAgent(userLogin.getId(), "prospect_call", JSONUtils.toJson(agentCall.getProspectCall()));
			}
		} catch ( IOException e) {
			logger.error("getCurrentAllocatedProspect() : Error occurred.", e);
		}
	}

	@Override
	// @Authorize(roles = {Roles.ROLE_SUPERVISOR,Roles.ROLE_PARTNER})
	public AgentDTO createAgent( AgentDTO agentDTO) {
		Agent agent = new Agent(agentDTO.getPartnerId(), agentDTO.getFirstName(), agentDTO.getLastName(),
				agentDTO.getAddress() != null ? agentDTO.getAddress().toAddress() : null);
		agent.setUsername(agentDTO.getUsername());
		agent.setOverview(agentDTO.getOverview());
		agent.setPerHourPrice(agentDTO.getPerHourPrice());
		agent.setSupervisorId(agentDTO.getSupervisorId());
		 List<String> domains = agentDTO.getDomains();
		if (domains != null && !domains.isEmpty()) {
			// Date:15/02/2018 Reason:commented below lines to insert domains as it is.
			// List<Vertical> verticals = verticalRepository.findAll();
			// HashMap<String, ArrayList<Domain>> mappedVerticals =
			// convertVerticalListToVerticalMap(verticals);
			for ( String domain : domains) {
				// checkDomainValid(mappedVerticals, domain);
				agent.addDomain(domain);
			}
		}
		if (agentDTO.getLanguages() != null) {
			for ( String language : agentDTO.getLanguages()) {
				agent.addLanguage(language);
			}
		}
		 LocalDate dt = new LocalDate(2030, 07, 31);
		 ScheduleRecurrence recurrence = new ScheduleRecurrence(new Date(), dt.toDate(), RecurrenceTypes.Daily, null);
		 WorkSchedule workSchedule = new WorkSchedule(new Date(), 9, 15, recurrence);
		agent.addWorkSchedule(workSchedule);
		agent = agentRepository.save(agent);
		return new AgentDTO(agent);
	}

	@Override
	public AgentDTO updateAgent( String agentId,  AgentDTO agentDTO) {
		Agent agent = agentRepository.findOneById(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent " + agentId + " does not Exist");
		}
		 Login login = loginRepository.findOneById(agentId);
		if (!agent.getFirstName().equals(agentDTO.getFirstName())
				|| !agent.getLastName().equals(agentDTO.getLastName())) {

			login.setFirstName(agentDTO.getFirstName());
			login.setLastName(agentDTO.getLastName());
			loginRepository.save(login);
		}
		agent.setFirstName(agentDTO.getFirstName());
		agent.setLastName(agentDTO.getLastName());
		agent.setAddress(agentDTO.getAddress() != null ? agentDTO.getAddress().toAddress() : null);
		agent.setPerHourPrice(agentDTO.getPerHourPrice());

		agent.setOverview(agentDTO.getOverview());
		 List<String> domains = agentDTO.getDomains();
		if (domains != null && !domains.isEmpty()) {
			 List<Vertical> verticals = verticalRepository.findAll();
			 HashMap<String, ArrayList<Domain>> mappedVerticals = convertVerticalListToVerticalMap(verticals);
			 List<String> ObjAvailbaleDomains = agent.getDomains();
			for ( String domain : domains) {
				if (!ObjAvailbaleDomains.contains(domain)) {// check domain is available in given Agent object
					checkDomainValid(mappedVerticals, domain);
					agent.addDomain(domain);
				}
			}
		}
		if (agentDTO.getLanguages() != null) {
			 List<String> ObjAvailbaleLanguages = agent.getLanguages();
			for ( String language : agentDTO.getLanguages()) {
				if (!ObjAvailbaleLanguages.contains(language)) {// check language is available in given Agent object
					agent.addLanguage(language);
				}
			}
		}
		agent = agentRepository.save(agent);
		return new AgentDTO(agent);

	}

	private HashMap<String, ArrayList<Domain>> convertVerticalListToVerticalMap( List<Vertical> verticals) {
		 HashMap<String, ArrayList<Domain>> mappedVerticals = new HashMap<String, ArrayList<Domain>>();
		for ( Vertical vertical : verticals) {
			mappedVerticals.put(vertical.getName(), vertical.getDomains());
		}
		return mappedVerticals;
	}

	private void checkDomainValid( HashMap<String, ArrayList<Domain>> mappedVerticals,  String verticalAndDomainPair) {
		if (!verticalAndDomainPair.contains("|")) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomainPair + "\" seperator(|) is not present in a given domain");
		}
		 String verticalAndDomain[] = verticalAndDomainPair.split("\\|");
		if (verticalAndDomain.length != 2) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomainPair + "\" this domain does not contain valid Vertical Domain pair");
		}
		if (!mappedVerticals.containsKey(verticalAndDomain[0])) {
			throw new IllegalArgumentException(
					" \"" + verticalAndDomain[0] + "\" this vertical does not exist in the system");
		}
		 ArrayList<Domain> domains = mappedVerticals.get(verticalAndDomain[0]);
		boolean isDomainAvailable = false;
		for ( Domain domain : domains) {
			if (domain.getKey().equals(verticalAndDomain[1])) {
				isDomainAvailable = true;
				break;
			}
		}
		if (!isDomainAvailable) {
			throw new IllegalArgumentException(" \"" + verticalAndDomain[1]
					+ "\" this domain is not belonging to the vertical " + "\"" + verticalAndDomain[0] + "\" ");
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void addWorkSchedule( String agentId,  WorkSchedule workSchedule) {
		 Agent agent = agentRepository.findOneById(agentId);
		agent.addWorkSchedule(workSchedule);
		agentRepository.save(agent);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void deleteWorkSchedule( String agentId,  String scheduleId) {
		 Agent agent = agentRepository.findOneById(agentId);
		agent.removeWorkSchedule(scheduleId);
		agentRepository.save(agent);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void changeStatus( AgentStatus agentStatus,  String agentType,  String campaignId) {
		 String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		doChangeStatus(agentId, agentStatus, agentType, campaignId);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM })
	public void changeStatus( String agentId, AgentStatus agentStatus, String agentType, String campaignId) {
		doChangeStatus(agentId, agentStatus, agentType, campaignId);
	}

	private void doChangeStatus(String agentId, AgentStatus agentStatus, String agentType, String campaignId) {
		switch (agentStatus) {
		case ONLINE:
			changeStatusOnline(agentId, agentType, campaignId);
			break;
		case REQUEST_BREAK:
		case ONBREAK:
		case REQUEST_MEETING:
		case MEETING:
		case REQUEST_TRAINING:
		case TRAINING:
		case REQUEST_OFFLINE:
		case OFFLINE:
		case REQUEST_OTHER:
		case OTHER:
		case OFFLINE_AUTO_LOGOUT:	
			changeStatusNonOnline(agentId, agentType, agentStatus, campaignId);
			break;
		}
		// refresh the agent metrics for supervisor and pushes the new metrics on
		// supervisor screen
		// supervisorService.pushAgentMetricsToSupervisor(agentId, null);
	}


	private void changeStatusOnline(String agentId, String agentType, String campaignId) {
		if (null == campaignId) {
			throw new IllegalArgumentException("Campaign id is required for changing status");
		}
		AgentCall existingAgentCall = agentRegistrationService.getAgentCall(agentId); // first get the existing
																						// AgentCall object
		logAgentActivity(agentId, AgentActivityType.AGENT_STATUS_CHANGE, AgentStatus.ONLINE.name(), null, campaignId,
				null, null, agentType); // log the status change event

		AgentCampaignDTO agentCampaign = null;
		if (existingAgentCall != null) {
			agentCampaign = existingAgentCall.getCurrentCampaign();
		}

		if (agentCampaign == null || !campaignId.equals(agentCampaign.getId())) {
			agentCampaign = getAgentCampaign(campaignId);
		}

		if (agentCampaign != null && agentCampaign.getType().toString().equals(CampaignTypes.Prospecting.name())) {
			return;
		}
		if ((agentCampaign.getCallControlProvider().equalsIgnoreCase("Telnyx") || agentCampaign.getCallControlProvider().equalsIgnoreCase("Signalwire"))  && (agentCampaign.getDialerMode().equals(DialerMode.POWER) || (agentType.equalsIgnoreCase("AUTO") && agentCampaign.getDialerMode().equals(DialerMode.HYBRID)))) {
			// Do not register Agent for Telnyx and Signalwire untill Conference gets created.
		} else {
			agentRegistrationService.manageAgent(agentId, agentType, agentCampaign.getDialerMode(), AgentStatus.ONLINE,
				agentCampaign); // register the agent -- sets/updates the agentcall object
		}

		// Test PUSH of lead when status is changed to ONLINE...to replicate DIALER
		// scenario
		// doTestProspectPushToAgent(agentId, campaignId);
	}

	/*
	 * private void doTestProspectPushToAgent(String agentId, String campaignId) {
	 * String activeProfile = (System.getenv("spring_profiles_active") != null) ?
	 * System.getenv("spring_profiles_active") :
	 * System.getProperty("spring.profiles.active"); if (activeProfile.equals("dev")
	 * || activeProfile.equals("local")) { //create dummy Prospect object Prospect
	 * prospect = new Prospect(); prospect.setFirstName("Bertha");
	 * prospect.setLastName("Boxer"); prospect.setPrefix("Ms");
	 * prospect.setCompany("ACME Corporation");
	 * prospect.setTitle("Vice President - Infrastructure Services");
	 * prospect.setPhone("+919975259204");
	 * prospect.setAddressLine1("12025 Santa Monica Blvd");
	 * prospect.setAddressLine2("Suite #104"); prospect.setCity("Los Angeles");
	 * prospect.setStateCode("CA"); prospect.setEmail("djain@xtaascorp.com");
	 * prospect.setIndustry("Communications");
	 * prospect.setCustomAttribute("AnnualRevenue", "$900,750,000");
	 * prospect.setCustomAttribute("NumberOfEmployees", "300");
	 * prospect.setSource("File");
	 *
	 * ProspectCall call = new ProspectCall(UUID.randomUUID().toString(),
	 * campaignId, prospect); call.setOutboundNumber("408-338-6066");
	 * ArrayList<Note> notes = new ArrayList<Note>(); Note n1 = new Note(new Date(),
	 * "My first note", "agentdev1"); n1.setDispositionStatus("FAILURE");
	 * n1.setSubStatus("DNCR"); n1.setAgentName("Agent Dev1"); Note n2 = new
	 * Note(new Date(), "My second note", "agentdev1");
	 * n2.setDispositionStatus("FAILURE"); n2.setSubStatus("BADNUMBER");
	 * n2.setAgentName("Agent Dev1"); Note n3 = new Note(new Date(),
	 * "My third note", "agentdev1"); n3.setDispositionStatus("FAILURE");
	 * n3.setSubStatus("DNCR"); n3.setAgentName("Agent Dev1"); Note n4 = new
	 * Note(new Date(), "My fourth note", "agentdev1");
	 * n4.setDispositionStatus("FAILURE"); n4.setSubStatus("DNCR");
	 * n4.setAgentName("Agent Dev1"); Note n5 = new Note(new Date(),
	 * "My fifth note", "agentdev1"); n5.setDispositionStatus("FAILURE");
	 * n5.setSubStatus("DNCR"); n5.setAgentName("Agent Dev1"); Note n6 = new
	 * Note(new Date(), "My sixth note", "agentdev1");
	 * n6.setDispositionStatus("FAILURE"); n6.setSubStatus("DNCR");
	 * n6.setAgentName("Agent Dev1");
	 *
	 * notes.add(n1); notes.add(n2); notes.add(n3); notes.add(n4); notes.add(n5);
	 * notes.add(n6); call.setNotes(notes); try { pushMessageToAgent(agentId,
	 * "prospect_call", JSONUtils.toJson(call)); } catch (IOException e) {
	 * logger.error("doTestProspectPushToAgent() : Error occurred.", e); } } }
	 */

	/**
	 * Change agent status to status other than online (Gearing, Away, OnBreak and
	 * Idle)
	 */
	private void changeStatusNonOnline(String agentId, String agentType, AgentStatus agentStatus, String campaignId) {
        AgentCall agentCall = persistAgentStatusChange(agentId, agentType, agentStatus, campaignId);
        AgentCampaignDTO agentCampaign = getAgentCampaign(campaignId);
        if (agentCampaign.isSupervisorAgentStatusApprove()) {
            Campaign campaign = campaignRepository.findOneById(campaignId);
            String notice = "";
            if (campaign != null && campaign.getAdminSupervisorIds() != null
                    && campaign.getAdminSupervisorIds().size() > 0) {
                for (String supId : campaign.getAdminSupervisorIds()) {
                    PusherUtils.pushMessageToUser(supId, XtaasConstants.RELOAD_AGENT_INFO, agentId);
                    PusherUtils.pushMessageToUser(supId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
                    notice = agentId + " has requested for " + agentStatus.toString() + ".";
                    teamNoticeSerive.saveUserNotices(supId, notice);
                    sendNotificationEmail(supId, notice);
                }
            }
            if (agentStatus.isRequestStatus()) {
                autoApproveAgentRequestService.insertAgentRequest(agentId, campaignId, agentType, agentStatus);
            }
        }
        if ((agentCall == null || (agentCall != null && agentCall.getProspectCall() == null))
                && agentStatus.isRequestStatus()) { // if Non-Online status request (Ex: REQUEST_OFFLINE etc and no
                                                    // prospect currently allocated
            if (!agentCampaign.isSupervisorAgentStatusApprove()) {
                approveNonOnlineStatusChangeRequest(agentId, campaignId, agentStatus); // then approve the request and send the approval notice to agent
            }
        }
    }

	private void sendNotificationEmail(String supervisor, String notice) {
		// Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		Login loginUser = loginRepository.findOneById(supervisor);
		if (loginUser != null) {
			List<String> reportingEmail = loginUser.getReportEmails();
			if (reportingEmail != null && reportingEmail.size() > 0) {
				for (String email : reportingEmail) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", "Approve agent status change request.", notice);
				}
			} else {
				if (loginUser.getEmail() != null && !loginUser.getEmail().isEmpty()) {
					XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", "Approve agent status change request.", notice);
				}
			}
			XtaasEmailUtils.sendEmail("supportteam@xtaascorp.com", "data@xtaascorp.com", "Approve agent status change request.", notice);
		}
	}

	private AgentCall persistAgentStatusChange(String agentId, String agentType, AgentStatus agentStatus,
			String campaignId) {
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		AgentCampaignDTO agentCampaign = null;
		if (agentCall != null) {
			agentCampaign = agentCall.getCurrentCampaign();
		}
		if (agentCampaign == null || !agentCampaign.getId().equals(campaignId)) {
			agentCampaign = getAgentCampaign(campaignId); // get the campaign object for Agent
		}
		agentRegistrationService.manageAgent(agentId, agentType, agentCampaign.getDialerMode(), agentStatus,
				agentCampaign); // unregister agent from available queue and update agentcall object
		logAgentActivity(agentId, AgentActivityType.AGENT_STATUS_CHANGE, agentStatus.name(), null, campaignId, null,
				null, agentType); // log this event in database
		return agentCall;
	}

	@Override
	public void approveAgentStatusChangeRequestBySupervisor(String agentId, AgentStatus agentStatus, String agentType, String campaignId, boolean isJob) {
		
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		/* Agent call will be NULL if agent reloads the page after requesting status
		   change in that scenario we are moving agent to offline status in below If block */
//		if (agentCall == null && !isJob) {
//			logAgentActivity(agentId, AgentActivityType.AGENT_STATUS_CHANGE, agentStatus.OFFLINE.name(), null,
//					campaignId, null, null, agentType);
//			throw new IllegalArgumentException(agentId +" went offline by refreshing the page.");
//		}
//		 if (agentCall.getProspectCall() != null) {
//			throw new IllegalArgumentException("Cant approve agent is on the call.");
//		}
//		AgentCall agentCall = persistAgentStatusChange(agentId, agentType, agentStatus, campaignId);
		if (agentStatus.equals(agentStatus.ONBREAK)) {
			agentStatus = agentStatus.REQUEST_BREAK;
		} else if (agentStatus.equals(agentStatus.MEETING)) {
			agentStatus = agentStatus.REQUEST_MEETING;
		} else if (agentStatus.equals(agentStatus.OTHER)) {
			agentStatus = agentStatus.REQUEST_OTHER;
		} else if (agentStatus.equals(agentStatus.TRAINING)) {
			agentStatus = agentStatus.REQUEST_TRAINING;
		} else if (agentStatus.equals(agentStatus.OFFLINE)) {
			agentStatus = agentStatus.REQUEST_OFFLINE;
		}
		agentCall.setCurrentStatus(agentStatus);
		agentCall.setSupervisorApproved(true);
		agentRegistrationService.setAgentCall(agentId, agentCall);
		// agentCall will be NULL if another Request status is sent after OFFLINE
		if ((agentCall == null || (agentCall != null && agentCall.getProspectCall() == null))
				&& agentStatus.isRequestStatus()) { // if Non-Online status request (Ex: REQUEST_OFFLINE etc and no prospect currently allocated
			agentRegistrationServiceImpl.unregisterAgent(agentCall);
			approveNonOnlineStatusChangeRequest(agentId, campaignId, agentStatus); // then approve the request and send the approval notice to agent
			autoApproveAgentRequestService.removeApprovedRequest(agentId);
		}
	}



	private void logAgentActivity(String agentId,  AgentActivityType activityType,  String agentActivity,  String pcid,
			  String campaignId,  Integer callStateTransitionDuration,  String prospectInteractionSessionId, String agentType) {
		 AgentActivityLog agentAction = new AgentActivityLog(agentId, activityType, agentActivity, agentType);
		 AgentActivityLogDNorm aalDNorm = new AgentActivityLogDNorm(agentId, activityType.toString(), agentActivity);
		if (pcid != null && !pcid.isEmpty()) {
			agentAction.setPcid(pcid);
			aalDNorm.setPcid(pcid);
		}
		if (campaignId != null && !campaignId.isEmpty()) {
			agentAction.setCampaignId(campaignId);
			aalDNorm.setCampaignId(campaignId);
		}
		try {
			 List<AgentActivityLog> agentLastStatusList = agentActivityLogRepository.getLastStatus(agentId,
					PageRequest.of(0, 1, Direction.DESC, "createdDate"));
			 List<AgentActivityLogDNorm> agentDNormLastStatusList = agentActivityLogDNormRepository.getLastStatus(agentId,
					PageRequest.of(0, 1, Direction.DESC, "createdDate"));
			if (agentLastStatusList != null && agentLastStatusList.size() > 0) {
				 AgentActivityLog agentActivityLog = agentLastStatusList.get(0);
				 Date previousDate = agentActivityLog.getCreatedDate();
				 Date curDate = new Date();
				 long stateDuration = (curDate.getTime() - previousDate.getTime()) / 60000;
				if (stateDuration > 0 && stateDuration < 540) {
					agentActivityLog.setStateDuration(stateDuration);
					agentActivityLogRepository.save(agentActivityLog);
					if(agentDNormLastStatusList!=null && agentDNormLastStatusList.size()>0){
						 AgentActivityLogDNorm agentActivityDNormLog = agentDNormLastStatusList.get(0);
						agentActivityDNormLog.setStateDuration(stateDuration);
						agentActivityLogDNormRepository.save(agentActivityDNormLog);
					}


				}
			}
		} catch ( Exception e) {
			logger.error("Error occurred while updating stateDuration of an agentActivityLog.");
		}
		if (callStateTransitionDuration != null) {
			agentAction.setCallStateTransitionDuration(callStateTransitionDuration);
			aalDNorm.setCallStateTransitionDuration(callStateTransitionDuration);
		}
		if (prospectInteractionSessionId != null && !prospectInteractionSessionId.isEmpty()) {
			agentAction.setProspectInteractionSessionId(prospectInteractionSessionId);
			aalDNorm.setProspectInteractionSessionId(prospectInteractionSessionId);
		}
		agentActivityLogRepository.save(agentAction);
		if(agentAction.getActivityType().equals(AgentActivityType.AGENT_STATUS_CHANGE))
			insertAgentStatusLog(agentAction); // DOMO
		//agentActivityLogDNormRepository.save(aalDNorm);

	}

	/**
	 * added AgentStatusLog for DOMO
	 *
	 * @param agentActivityLog
	 */
	private void insertAgentStatusLog(AgentActivityLog agentActivityLog) {
		boolean agentSaveFlag= true;
	  /*if (AgentActivityType.AGENT_STATUS_CHANGE.equals(agentActivityLog.getActivityType())) {
		  List<AgentStatusLog> agentStatusLogList = agentStatusLogRepository
				  .getLastStatus(agentActivityLog.getAgentId(), new PageRequest(0, 1, Direction.DESC, "createdDate"));
		  if (agentStatusLogList!= null && agentStatusLogList.size() > 0) {
			  AgentStatusLog agentStatusLog = agentStatusLogList.get(0);
			  Date previousDate = agentStatusLog.getCreatedDate();
			  Date curDate = new Date();
			  long stateDuration = (curDate.getTime() - previousDate.getTime()) / 60000;
			  agentStatusLog.setStateDuration(stateDuration);
			  agentStatusLogRepository.save(agentStatusLog);
		  }*/
		  Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		  List<AgentStatusLog> agentLatestRecord = agentStatusLogRepository.getLastStatus(agentActivityLog.getAgentId(),
					  pageable);
		  if (agentLatestRecord != null && agentLatestRecord.size()>0) {
			  AgentStatusLog asl = agentLatestRecord.get(0);
			  if (asl.getStatus().toString().equalsIgnoreCase(agentActivityLog.getStatus())) {
				  agentSaveFlag = false;
			  } else {
				  long agentCreatedTimeInSeconds = XtaasDateUtils
						  .getTimeInSeconds(agentLatestRecord.get(0).getCreatedDate());
				  long currentTimeInSeconds = XtaasDateUtils.getTimeInSeconds(new Date());
				  long timeDiffrence = currentTimeInSeconds - agentCreatedTimeInSeconds;
				  asl.setStateDuration(timeDiffrence);

				  Campaign campaign =campaignService.getCampaign(asl.getCampaignId());
				  Agent  agent = agentRepository.findOneById(asl.getAgentId());
				  agentStatusLogRepository.save(asl);
				  //domoUtilsService.postAgentStatusLogWebhook(asl);

				  if(agentLatestRecord.get(0).getStatus().equalsIgnoreCase("ONLINE")) {
					  PCIAnalysis pcian = new PCIAnalysis();
					  pcian.setAgentId(asl.getAgentId());
					  pcian.setCampaignid(asl.getCampaignId());
					  pcian.setAgentOnlineTime(timeDiffrence);
					  pcian.setCreated_date(new Date());
					  pcian.setCreated_datetime(new Date());
					  pcian.setAgentName(agent.getFirstName()+" "+agent.getLastName());
					  pcian.setCampaign_status("RUNNING");
					  pcian.setPartnerid(agent.getPartnerId());
					  pcian.setCampaignName(campaign.getName());
					  pcian.setOrganizationid(campaign.getOrganizationId());
					  //pcian.setStatus(asl.getStatus());
					  pciAnalysisRepository.save(pcian);
					 // domoUtilsService.postPciAnalysisWebhook(pcian);
				  }
			  }
		  }
		  if(agentSaveFlag) {
			  /*PCIAnalysis pcian = new PCIAnalysis();
			  pcian.setAgentId(agentActivityLog.getAgentId());
			  pcian.setCampaignid(agentActivityLog.getCampaignId());
			  pcian.setAgentOnlineTime(agentActivityLog.getStateDuration());
			  pcian.setCreated_date(new Date());
			  pcian.setCreated_datetime(new Date());*/
			  agentStatusLogRepository.save(new AgentStatusLog(agentActivityLog));

			  /*PCIAnalysis pcian = new PCIAnalysis();
			  pcian.setAgentId(asl.getAgentId());
			  pcian.setCampaignid(asl.getCampaignId());
			  pcian.setAgentOnlineTime(timeDiffrence);
			  pcian.setCreated_date(new Date());
			  pcian.setCreated_datetime(new Date());
			  pcian.setAgentName(agent.getFirstName()+" "+agent.getLastName());
			  pcian.setCampaign_status("RUNNING");
			  pcian.setPartnerid(agent.getPartnerId());
			  pcian.setCampaignName(campaign.getName());
			  pcian.setOrganizationid(campaign.getOrganizationId());*/
			  //domoUtilsService.postAgentStatusLogWebhook(new AgentStatusLog(agentActivityLog));
		  }
	  //}
  }

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public String getTwilioCapabilityToken( String agentId,  boolean isWhisperChannel) {
		 TwilioCapability capability = new TwilioCapability(ApplicationEnvironmentPropertyUtils.getTwilioAccountSid(),
				ApplicationEnvironmentPropertyUtils.getTwilioAuthToken());
		if (null != agentId) {
			if (isWhisperChannel) {
				capability.allowClientOutgoing(ApplicationEnvironmentPropertyUtils.getTwimlWhisperAppSID());
				capability.allowClientIncoming(agentId + "WHISPER");
			} else {
				capability.allowClientOutgoing(ApplicationEnvironmentPropertyUtils.getTwimlDialAppSID());
				capability.allowClientIncoming(agentId);
			}
		}
		String token = null;
		try {
			token = capability.generateToken();
		} catch ( Exception e) {
			logger.error("getTwilioCapabilityToken : Error occurred.", e);
		}
		return token;
	}

	private void pushMessageToAgent( String agentId,  String event,  String message) {
		try {
			Pusher.triggerPush(agentId, event, message);
			logger.debug("pushMessageToAgent() : Sent [{}] event to Agent [" + agentId + "]. Message: [{}]", event,
					message);
		} catch ( IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing message to agent: " + agentId + " Msg:"
					+ message, e);
		}
	}

	/**
	 * Method called by CallController whenever an agent is connected/disconnected
	 * with the call. It just logs call change event in agent activity log
	 *
	 * @param callStatusDTO
	 */
	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void changeCallStatus( CallStatusDTO callStatusDTO) {
		changeCallStatus(XtaasUserUtils.getCurrentLoggedInUsername(), callStatusDTO);
	}

	@Override
	public void changeCallStatus( String agentId,  CallStatusDTO callStatusDTO) {
		 AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		String pcid = null;
		String campaignId = null;
		String callSid = null;
		String prospectInteractionSessionId = null;
		if (agentCall != null) {
			if (agentCall.getProspectCall() != null) {
				pcid = agentCall.getProspectCall().getProspectCallId();
				callSid = agentCall.getProspectCall().getTwilioCallSid();
				prospectInteractionSessionId = agentCall.getProspectCall().getProspectInteractionSessionId();
			}
			campaignId = agentCall.getCampaignId();
		}

		String agentActivityCallStatus = null;
		switch (callStatusDTO.getCallState()) {
		case CONNECTED:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_CONNECTED.name();
			break;
		case DISCONNECTED:
			 boolean hangupOutboundCall = propertyService.getBooleanApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.OUTBOUND_CALL_HANGUP_ON_AGENT_DISCONNECTION.name(), false);
			if (hangupOutboundCall) {// hangup only for Preview and Preview_System mode
				if (agentCall != null && agentCall.getCurrentCampaign() != null) {
					/*
					 * Below code is commented because it programatically disconnects(hangup) the
					 * call.which is not at all required in our dialer modes. Previously this
					 * implementation is done for PREVIEW and SYSTEM_PREVIEW dialer mode. currently
					 * we are not using that dialer modes.
					 */
					// hangupPreviewOutboundCall(pcid,agentCall.getCurrentCampaign().getDialerMode(), agentId, callSid);
				} else {
					logger.error(
							"changeCallStatus() : Current allocated Campaign for agent [{}] is NULL. Cannot HangUp Preview Outbound Call.",
							agentId);
				}
			}
			if (agentCall != null && agentCall.getProspectCall() != null
					&& agentCall.getProspectCall().getTwilioCallSid() != null) {
				agentRegistrationService.unsetProspectByCallSid(agentCall.getProspectCall().getTwilioCallSid()); // on
			}
			agentActivityCallStatus = AgentActivityCallStatus.CALL_DISCONNECTED.name();
			break;
		case PREVIEW:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_PREVIEW.name();
			break;
		case DIALED:
			callSid = callStatusDTO.getTwilioCallSid(); // Setting the actual one now.
			if (agentCall == null || agentCall.getProspectCall() == null
					|| agentCall.getProspectCall().getTwilioCallSid() == null) {
				break;
			}
			if (callStatusDTO.getOutboundNumber() != null) {
				updateOutboundNumberInProspect(pcid,callStatusDTO.getOutboundNumber());
			}
			agentRegistrationService.unsetProspectByCallSid(agentCall.getProspectCall().getTwilioCallSid());
			agentRegistrationService.setProspectByCallSid(callSid, agentCall.getProspectCall());
			agentActivityCallStatus = AgentActivityCallStatus.CALL_DIALED.name();
			if (pcid != null) {
				// In manual dial create WRAPUP_COMPLETE entry after each rediall
				ProspectCallLog prospectCallLog = prospectCallLogRepository.findByProspectCallId(pcid);
				if (prospectCallLog != null && ProspectCallStatus.CALL_DIALED.equals(prospectCallLog.getStatus())) {
					if (!prospectCallLog.getProspectCall().getTwilioCallSid().equals(callSid)
							&& prospectCacheServiceImpl.getMultiRingingControlMap(agentId) != null
							&& !prospectCacheServiceImpl.getMultiRingingControlMap(agentId).equals(callSid)) {
							prospectCacheServiceImpl.addInMultiRingingControlMap(agentId, callSid);
						ProspectCall prospectCall = prospectCallLog.getProspectCall();
						prospectCall.setDispositionStatus(DispositionType.DIALERCODE.name());
						prospectCall.setSubStatus(DialerCodeDispositionStatus.NOANSWER.name());
						prospectCallLog.setStatus(ProspectCallStatus.CALL_NO_ANSWER);
						prospectCallLog = fetchAndSetRecordingUrl(prospectCall.getTwilioCallSid(), prospectCallLog);
						prospectCallLogService.save(prospectCallLog, true);
						prospectCallLogService.updateCallDetails(pcid, ProspectCallStatus.CALL_DIALED, callSid, null,
								agentId, null);
					}
				} else {
					if(prospectCacheServiceImpl.getMultiRingingControlMap(agentId) != null && !prospectCacheServiceImpl.getMultiRingingControlMap(agentId).isEmpty()) {
						if(!prospectCacheServiceImpl.getMultiRingingControlMap(agentId).equals(pcid)) {
							prospectCacheServiceImpl.addInMultiRingingControlMap(agentId, pcid);
							prospectCallLogService.updateCallDetails(pcid, ProspectCallStatus.CALL_DIALED, callSid, null,
									agentId, null);
						}
					}else {
						prospectCacheServiceImpl.addInMultiRingingControlMap(agentId, pcid);
						prospectCallLogService.updateCallDetails(pcid, ProspectCallStatus.CALL_DIALED, callSid, null,
								agentId, null);
					}
				}
			}
			break;
		case MUTE:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_ONMUTE.name();
			break;
		case UNMUTE:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_UNMUTE.name();
			break;
		case HOLD:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_ONHOLD.name();
			break;
		case UNHOLD:
			agentActivityCallStatus = AgentActivityCallStatus.CALL_UNHOLD.name();
			break;
		}
		if (agentActivityCallStatus != null && agentCall != null && agentCall.getAgentType() != null) {
			String agentType = agentCall.getAgentType().name();
			logAgentActivity(agentId, AgentActivityType.CALL_STATUS_CHANGE, agentActivityCallStatus, pcid, campaignId,
					callStatusDTO.getCallStateTransitionDuration(), prospectInteractionSessionId, agentType);
		}
	}
	
	private void updateOutboundNumberInProspect(String prospectCallId, String outboundNumber) {
		ProspectCallLog prospectCallLogFromDB = prospectCallLogRepository.findByProspectCallId(prospectCallId);
		if (prospectCallLogFromDB != null && prospectCallLogFromDB.getProspectCall() != null) {
			prospectCallLogFromDB.getProspectCall().setOutboundNumber(outboundNumber);
			prospectCallLogRepository.save(prospectCallLogFromDB);
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_AGENT })
	public void submitProspectCall( ProspectCallDTO prospectCallDTO) {
		if (prospectCallDTO != null && prospectCallDTO.getAgentId() != null && prospectCallDTO.getProspect() != null && prospectCallDTO.getProspect().getPhone() != null && prospectCallDTO.getCampaignId() != null) {
			logger.info("submitProspectCall() : Call Submission request received from Agent : "
					+ prospectCallDTO.getAgentId() + ", Prospect : " + prospectCallDTO.getProspect().getPhone()
					+ ", CampaignId : " + prospectCallDTO.getCampaignId());
			Agent agent = null;
			Campaign campaign = campaignService.getCampaign(prospectCallDTO.getCampaignId());
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (prospectCallDTO.getAgentId() != null && prospectCallDTO.getCampaignId() != null) {
				agent = agentRepository.findOneById(prospectCallDTO.getAgentId());
				DialerMode dialerMode = campaign.getDialerMode();
				logger.debug("Campaign dialer mode ===> " + campaign.getDialerMode(), prospectCallDTO.getCampaignId());
				if (campaign != null && agent != null && (dialerMode.equals(DialerMode.RESEARCH)
						|| dialerMode.equals(DialerMode.RESEARCH_AND_DIAL) || dialerMode.equals(DialerMode.MANUALDIAL)
						|| (dialerMode.equals(DialerMode.HYBRID) && agent.getAgentType().equals(agentSkill.MANUAL)))) {
					isProspectUniqueInCampaign(prospectCallDTO);
				}
			}

			// HACK : setting UTC callStartTime in case agent has wrong/different date-time
			// set on his machine.
			int duration = prospectCallDTO.getCallDuration();
			Calendar now = Calendar.getInstance();
			now = Calendar.getInstance();
			now.add(Calendar.SECOND, -duration);
			prospectCallDTO.setCallStartTime(now.getTime());
			if (!prospectCallDTO.isFinalSubmit()) {
				logger.debug("<----- Multiprospect selected for campaign [{}] & prospectCallId [{}] ----->",
						prospectCallDTO.getCampaignId(), prospectCallDTO.getProspectCallId());
			}
			ProspectCallLog prospectCallLogFromDB = null;
			ProspectCall prospectCallFromAgent = prospectCallDTO.toProspectCall();

			// added to cleanup agent screen and agent status if no create new prospect.
			String fName = prospectCallDTO.getProspect().getFirstName();
			String lName = prospectCallDTO.getProspect().getLastName();
			if (prospectCallDTO.isFinalSubmit() && (fName == null || fName.isEmpty())
					&& (lName == null || lName.isEmpty())) {
				logger.error(
						"submitProspectCall() - FirstName/LastName not present for agent submit. Handling agent status.");
				try {
					prospectCallLogService.updateRecordingAndFailureProspects(prospectCallDTO.getCampaignId(),
							prospectCallDTO.getTwilioCallSid());
				} catch ( Exception e) {
					logger.error("Error occurred in updateRecordingAndFailureProspects.");
				}
				handleAgentStatusChangeRequest(prospectCallFromAgent);
				return;
			}

			if (prospectCallDTO.getProspectCallId() == null) {
				/*
				* Added to create ProspectCallLog entries in prospectcalllog &
				* prospectcallinteraction collection for new prospect created by agent.
				*/
				prospectCallFromAgent.setProspectCallId(getUniqueProspect());	
				ProspectCallLog newProspectCallLog = new ProspectCallLog();
				String prospectCallId = createProspectCallLogs(prospectCallFromAgent, newProspectCallLog, true);
				prospectCallDTO.setProspectCallId(prospectCallId);
				prospectCallLogFromDB = prospectCallLogRepository.findByProspectCallId(prospectCallDTO.getProspectCallId());
			}
			// Dispositions for indirect numbers
			if (prospectCallDTO.getIndirectProspects() != null && prospectCallDTO.getIndirectProspects().size() > 0) {
				List<IndirectProspectsDTO> ipDTOList = prospectCallDTO.getIndirectProspects();
				for ( IndirectProspectsDTO indirectProspect : ipDTOList) {
					if (indirectProspect.getProspectCallId().equals(prospectCallDTO.getProspectCallId())) {
						prospectCallLogFromDB = prospectCallLogRepository
								.findByProspectCallId(prospectCallDTO.getProspectCallId());
						prospectCallLogFromDB.getProspectCall()
								.setCallRetryCount(prospectCallLogFromDB.getProspectCall().getCallRetryCount() + 1);
						incrementDailyCallRetryCount(prospectCallLogFromDB.getProspectCall());
						createProspectCallLogs(prospectCallFromAgent, prospectCallLogFromDB, false);
					}
				}
				if (prospectCallLogFromDB == null) {
					prospectCallLogFromDB = prospectCallLogRepository
							.findByProspectCallId(prospectCallDTO.getProspectCallId());
					prospectCallLogFromDB.getProspectCall().setIndirectProspects(null);
					if (!prospectCallDTO.isFinalSubmit()) {
						prospectCallFromAgent.setProspectHandleDuration(prospectCallDTO.getProspectHandleDuration());
						prospectCallLogFromDB = updatePrimaryProspectDetails(prospectCallLogFromDB, prospectCallFromAgent);
						prospectCallLogFromDB.setStatus(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE);
						// prospectCallLogFromDB = changeSliceAndDirectPhoneFlag(prospectCallLogFromDB);
						updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLogFromDB);
						checkProspectQualifiedForTranscription(campaign, prospectCallLogFromDB);
						prospectCallLogService.save(prospectCallLogFromDB, true);
						// prospectCallLogRepository.save(prospectCallLogFromDB);
						// log agent activity
						String agentType = agent.getAgentType().name();

						// Add log statement for call tracking
						ProspectCall pc = prospectCallLogFromDB.getProspectCall();
						logger.info(new SplunkLoggingUtils("SubmitProspectCall", pc.getTwilioCallSid())
								.eventDescription("Submit prospect call by Agent").methodName("submitProspectCall")
								.processName("submitProspectCall").prospectCallId(pc.getProspectCallId())
								.callSid(pc.getTwilioCallSid()).campaignId(pc.getCampaignId()).agentId(pc.getAgentId())
								.build());

						logAgentActivity(prospectCallFromAgent.getAgentId(), AgentActivityType.CALL_STATUS_CHANGE,
								AgentActivityCallStatus.CALL_WRAPPED_UP.name(), prospectCallDTO.getProspectCallId(),
								prospectCallDTO.getCampaignId(), prospectCallDTO.getWrapupDuration(),
								prospectCallLogFromDB.getProspectCall().getProspectInteractionSessionId(), agentType);
						return;
					}
				}
			} else {
				prospectCallLogFromDB = prospectCallLogRepository.findByProspectCallId(prospectCallDTO.getProspectCallId());
			}

			if (prospectCallLogFromDB == null) {
				prospectCallLogFromDB = new ProspectCallLog();
			}
			
			// DATE :- 17/12/2020 Added below condition to avoid the already disposed prospects not to dispose again.
			if (prospectCallLogFromDB != null && prospectCallLogFromDB.getStatus() != null
					&& prospectCallLogFromDB.getStatus().equals(ProspectCallStatus.WRAPUP_COMPLETE)
					&& prospectCallFromAgent.getDispositionStatus() != null
					&& prospectCallFromAgent.getDispositionStatus().equalsIgnoreCase("AUTO")) {
				logger.info("As this prospect already got submitted previously so returning to avoid resubmission. ProspectCallId : "
						+ prospectCallDTO.getProspectCallId());
				return;
			}

			if (prospectCallLogFromDB.getProspectCall() != null
					&& prospectCallLogFromDB.getProspectCall().getProspect() != null) {
				// if name not same then clone the prospect
				boolean cloneProspectForName = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isNameEquals(prospectCallDTO.getProspect());
				if (cloneProspectForName) {
					prospectCallFromAgent.logProspectIdentityChangeByName(
							prospectCallLogFromDB.getProspectCall().getProspect().getFirstName(),
							prospectCallLogFromDB.getProspectCall().getProspect().getLastName());
				}
				boolean cloneProspectForEmail = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isEmailEquals(prospectCallDTO.getProspect());
				if (cloneProspectForEmail) {
					prospectCallFromAgent.logProspectIdentityChangeByEmail(
							prospectCallLogFromDB.getProspectCall().getProspect().getEmail());
				}
				boolean cloneProspectForDepartment = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isDepartmentEquals(prospectCallDTO.getProspect());
				if (cloneProspectForDepartment) {
					prospectCallFromAgent.logProspectIdentityChangeByDepartment(
							prospectCallLogFromDB.getProspectCall().getProspect().getDepartment());
				}
				boolean cloneProspectForCompany = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isCompanyEquals(prospectCallDTO.getProspect());
				if (cloneProspectForCompany) {
					prospectCallFromAgent.logProspectIdentityChangeByCompany(
							prospectCallLogFromDB.getProspectCall().getProspect().getCompany());
				}
				boolean cloneProspectForRole = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isRoleEquals(prospectCallDTO.getProspect());
				if (cloneProspectForRole) {
					prospectCallFromAgent.logProspectIdentityChangeByRole(
							prospectCallLogFromDB.getProspectCall().getProspect().getTitle());
				}
				
				boolean cloneProspectForDomain = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isDomainEquals(prospectCallDTO.getProspect());
				if (cloneProspectForDomain) {
					if (prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("domain") != null) {
						prospectCallFromAgent.logProspectIdentityChangeByDomain(
								prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("domain").toString());
					}
				}

				boolean cloneProspectForAgentDialedExtension = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isAgentDialedExtensionEquals(prospectCallDTO.getProspect());
				if (cloneProspectForAgentDialedExtension) {
					prospectCallFromAgent.logProspectIdentityChangeByAgentDialedExtension(prospectCallLogFromDB.getProspectCall().getProspect().getAgentDialedExtension());
				}
			}

			if (prospectCallLogFromDB != null && prospectCallLogFromDB.getProspectCall() != null && prospectCallLogFromDB.getProspectCall().getProspect() != null) {
				boolean isCustomFieldsAnswerChanged = !prospectCallLogFromDB.getProspectCall().getProspect()
						.isCustomFieldsAnswerChanged(prospectCallDTO);
				if (isCustomFieldsAnswerChanged) {
					prospectCallFromAgent.logProspectIdentityChangeByQaAnswers(
							prospectCallLogFromDB.getProspectCall().getProspect().getCustomFields(), prospectCallFromAgent.getProspect().getCustomFields());
				}
			}

			// copy attributes which are not recd from agent screen but existing in database
			if (prospectCallLogFromDB.getProspectCall() != null) {
				// copying callretry count
				prospectCallFromAgent.setCallRetryCount(prospectCallLogFromDB.getProspectCall().getCallRetryCount());
				prospectCallFromAgent.setProspectInteractionSessionId(
						prospectCallLogFromDB.getProspectCall().getProspectInteractionSessionId()); // copying sessionid
				prospectCallFromAgent
						.setDailyCallRetryCount(prospectCallLogFromDB.getProspectCall().getDailyCallRetryCount());
				prospectCallFromAgent.setProspectSortIndex(prospectCallLogFromDB.getProspectCall().getProspectSortIndex());
				// prospectCallFromAgent.setRecordingUrlAws(prospectCallLogFromDB.getProspectCall().getRecordingUrlAws());
			}
			
			/**
			 * START
			 * DATE :- 03/07/2020
			 * Added below lines for telnyx AMD. 
			 */
			if (campaign != null && campaign.isEnableMachineAnsweredDetection() && campaign.getCallControlProvider() != null && prospectCallLogFromDB != null && prospectCallLogFromDB.getProspectCall() != null) {
				prospectCallFromAgent.setAmdTimeDiffInSec(prospectCallLogFromDB.getProspectCall().getAmdTimeDiffInSec());
				prospectCallFromAgent.setAmdTimeDiffInMilliSec(prospectCallLogFromDB.getProspectCall().getAmdTimeDiffInMilliSec());
				prospectCallFromAgent
						.setCallDetectedAs(prospectCallLogFromDB.getProspectCall().getCallDetectedAs());
			}
			// END

			prospectCallLogFromDB.setProspectCall(prospectCallFromAgent);
			prospectCallLogFromDB.setStatus(ProspectCallLog.ProspectCallStatus.WRAPUP_COMPLETE);
			prospectCallLogFromDB.getProspectCall().setIndirectProspects(null);
			// erasing the previous callbackdate. it will be set further down if it
			// qualifies
			prospectCallLogFromDB.setCallbackDate(null);
			prospectCallLogFromDB.getProspectCall().setProspectHandleDuration(prospectCallDTO.getProspectHandleDuration());

			////////////////////////////////////////////////////////
			StringBuffer sb = new StringBuffer();
			/*
			* sb.append(p.getProspectCall().getProspect().getSource()); sb.append("|");
			*/
			sb.append(prospectCallLogFromDB.getStatus());
			sb.append("|");
			sb.append(prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount"));
			sb.append("-");
			sb.append(prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount"));
			sb.append("|");
			sb.append(prospectCallLogFromDB.getProspectCall().getProspect().getManagementLevel());
			prospectCallLogFromDB.getProspectCall().setQualityBucket(sb.toString());
			///////////////////////////////////////////////////////////////////

			// set the recording url
			//TODO this condition is used to switch off/on the recording. uncomment if is required 
			//if (campaign.getRecordProspect() == null || campaign.getRecordProspect().equalsIgnoreCase("yes")) {
			CallLog callLog = callLogRepository.findOneById(prospectCallFromAgent.getTwilioCallSid());
			if (callLog != null) {
				if (callLog.getCallLogMap().get("RecordingUrl") != null) {
					String recordingUrl = callLog.getCallLogMap().get("RecordingUrl");
					prospectCallLogFromDB.getProspectCall().setRecordingUrl(recordingUrl);
					int lastIndex = recordingUrl.lastIndexOf("/");
					String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
					String rfileName = fileName + ".wav";
					String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					prospectCallLogFromDB.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
				} else if(callLog.getCallLogMap().get("RecordUrl") != null) {
					String recordingUrl = callLog.getCallLogMap().get("RecordUrl");
					prospectCallLogFromDB.getProspectCall().setRecordingUrl(recordingUrl);
					int lastIndex = recordingUrl.lastIndexOf("/");
					String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length() - 4);
					String rfileName = fileName + ".wav";
					String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					prospectCallLogFromDB.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
				} else if(callLog.getCallLogMap().get("recording_id") != null) {
					String recordingUrl = callLog.getCallLogMap().get("recordingUrl");
					prospectCallLogFromDB.getProspectCall().setRecordingUrl(recordingUrl);
					String rfileName = callLog.getCallLogMap().get("recording_id");
					String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					prospectCallLogFromDB.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
				} else if (prospectCallLogFromDB.getProspectCall().getRecordingUrl() != null && prospectCallLogFromDB.getProspectCall().getRecordingUrl() != "") {
					;
				} else {
					try {
						callLog = fetchAndUpdateCallRecording(callLog);
						if (callLog.getCallLogMap().get("RecordingUrl") != null) {
							prospectCallLogFromDB.getProspectCall()
									.setRecordingUrl(callLog.getCallLogMap().get("RecordingUrl"));
						}
						if (callLog.getCallLogMap().get("awsRecordingUrl") != null) {
							prospectCallLogFromDB.getProspectCall()
									.setRecordingUrlAws(callLog.getCallLogMap().get("awsRecordingUrl"));
						}
					} catch ( Exception e) {
						logger.error("Error occurred while updating CallLog for ", callLog.getId());
						logger.error("Exception is : ", e);
					}
				}
				// In preview mode, DialCallDuration param is available but not in power mode.
				// if DialCallDuration is not available then fallback on RecordingDuration
				String telcoDuration = (callLog.getCallLogMap().get("DialCallDuration") == null)
						? callLog.getCallLogMap().get("RecordingDuration")
						: callLog.getCallLogMap().get("DialCallDuration");
				if (telcoDuration != null && !telcoDuration.isEmpty()) {
					prospectCallLogFromDB.getProspectCall().setTelcoDuration(Integer.valueOf(telcoDuration));
				} else {
					prospectCallLogFromDB.getProspectCall().setTelcoDuration(null);
				}
			} else {
				TelnyxRecordingUrlObjForMap recordings = telnyxService.getRecordingUrlsFromMap(prospectCallDTO.getProspectCallId());
				if (recordings != null) {
					prospectCallLogFromDB.getProspectCall().setRecordingUrl(recordings.getRecordingUrl());
					prospectCallLogFromDB.getProspectCall().setRecordingUrlAws(recordings.getAwsRecordingUrl());
				} else if (prospectCallDTO.getTwilioCallSid() != null && !prospectCallDTO.getTwilioCallSid().isEmpty()) {
					List<ProspectCallLog> prospectCallLogs = prospectCallLogRepository.findMultipleByTwilioCallId(prospectCallDTO.getTwilioCallSid());
					if (prospectCallLogs != null && prospectCallLogs.size() > 1) {
						for (ProspectCallLog prospectCallLogForRecording : prospectCallLogs) {
							recordings = telnyxService.getRecordingUrlsFromMap(prospectCallLogForRecording.getProspectCall().getProspectCallId());
							if (recordings != null) {
								prospectCallLogFromDB.getProspectCall().setRecordingUrl(recordings.getRecordingUrl());
								prospectCallLogFromDB.getProspectCall().setRecordingUrlAws(recordings.getAwsRecordingUrl());
								telnyxService.mapAlternatePospectWithMainProspect(prospectCallLogForRecording.getProspectCall().getProspectCallId(), prospectCallDTO.getProspectCallId());
								prospectCallLogForRecording.getProspectCall().setTwilioCallSid(null);
								prospectCallLogRepository.save(prospectCallLogForRecording);
							}
						}
					}
				}
			//}
		}
			// DATE : 19/01/2018
			// Reason : Adding Agent's PartnerId in Prospect Calllog for QA Screen
			prospectCallLogFromDB.getProspectCall().setPartnerId(agent.getPartnerId());
			prospectCallLogFromDB.getProspectCall().setSupervisorId(agent.getSupervisorId());
			
			// deliver the asset if disposition status is SUCCESS
			if (prospectCallFromAgent.getDispositionStatus().equals(DispositionType.SUCCESS.name())) {
				if (prospectCallDTO.getProspect()!=null && prospectCallDTO.getProspect().getEmail() != null
						&& !prospectCallDTO.getProspect().getEmail().isEmpty() ) {

					boolean isEmailSuppressed = suppressionListService.isEmailSuppressed(prospectCallDTO.getProspect().getEmail().toLowerCase(), 
							campaign.getId(), campaign.getOrganizationId(), campaign.isMdFiveSuppressionCheck());
					if (isEmailSuppressed) {
						throw new IllegalArgumentException("email  is in suppression list.");
					}
				}
				if (prospectCallDTO.getProspect()!=null && prospectCallDTO.getProspect().getCustomAttributes().get("domain") != null ) {
					String ddomain = prospectCallDTO.getProspect().getCustomAttributes().get("domain").toString();
					ddomain = XtaasUtils.getSuppressedHostName(ddomain);
					boolean isDomainSuppressed = prospectCallLogService.getsuppresionListByDomain(campaign.getId(), ddomain.toLowerCase());
					if(isDomainSuppressed) {
						throw new IllegalArgumentException("Company is in suppression list.");
					}

				}
				String country = prospectCallLogFromDB.getProspectCall().getProspect().getCountry();
				if (prospectCallLogFromDB.getProspectCall().getProspect().getZipCode() != null && country != null
						&& (country.equalsIgnoreCase("United States") || country.equalsIgnoreCase("Canada")
								|| country.equalsIgnoreCase("India"))) {
	//				getGeoDetailsByZipCode(prospectCallLogFromDB);
				}
				// mark lead status as "NOT_SCORED"
				prospectCallLogFromDB.getProspectCall().setLeadStatus(XtaasConstants.LEAD_STATUS.NOT_SCORED.name());
				//Organization organization =  organizationRepository.findOneById(campaign.getOrganizationId());

				if( organization.isSendAssetEmail() && (campaign.getDeliverAssetOnQA()==null || campaign.getDeliverAssetOnQA().equalsIgnoreCase("AGENT"))) {
					String deliveredAssetId = campaignService.deliverActiveAsset(prospectCallFromAgent);
					prospectCallLogFromDB.getProspectCall().setDeliveredAssetId(deliveredAssetId);
				}
				// create new SuccessCallLog entry
				SuccessCallLog successCallLog = new SuccessCallLog();
				successCallLog = successCallLog.toSuccessCallLog(prospectCallLogFromDB);
				successCallLogRepository.save(successCallLog);
			} else if (prospectCallFromAgent.getDispositionStatus().equalsIgnoreCase("CALLBACK")) {
				String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";
				DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
				String selectedMinute = "";
				if (prospectCallDTO.getCallbackSelectedMin() == 0) {
					selectedMinute = "00";
				} else {
					selectedMinute = Integer.toString(prospectCallDTO.getCallbackSelectedMin());
				}
				if (prospectCallDTO.getCallbackSelectedDate() != null
						&& !prospectCallDTO.getCallbackSelectedDate().isEmpty()
						&& prospectCallDTO.getCallbackSelectedHours() != 0
						&& prospectCallDTO.getCallbackSelectedMin() >= 0 && prospectCallDTO.getCallbackAmPm() != null
						&& !prospectCallDTO.getCallbackAmPm().isEmpty()) {
					StringBuffer buffer = new StringBuffer();
					if (prospectCallDTO.getCallbackSelectedHours() == 10 || prospectCallDTO.getCallbackSelectedHours() == 11 || prospectCallDTO.getCallbackSelectedHours() == 12) {
						buffer.append(prospectCallDTO.getCallbackSelectedDate()).append(" ")
						.append(prospectCallDTO.getCallbackSelectedHours()).append(":")
						.append(selectedMinute).append(":00").append(" ")
						.append(prospectCallDTO.getCallbackAmPm());
					} else {
						buffer.append(prospectCallDTO.getCallbackSelectedDate()).append(" ").append("0")
						.append(prospectCallDTO.getCallbackSelectedHours()).append(":")
						.append(selectedMinute).append(":00").append(" ")
						.append(prospectCallDTO.getCallbackAmPm());
					}
					String callBackTime = buffer.toString();
					ProspectCallLog prospectFromDB = prospectCallLogRepository.findByProspectCallId(prospectCallDTO.getProspectCallId());
					LocalDateTime ldt = LocalDateTime.parse(callBackTime, DateTimeFormatter.ofPattern(DATE_FORMAT));
					String timeZone = prospectFromDB.getProspectCall().getProspect().getTimeZone();
					if (timeZone != null && callBackTime != null) {
						ZoneId prospectTimeZone = ZoneId.of(timeZone);
						ZonedDateTime propsectZoneDateTime = ldt.atZone(prospectTimeZone);
						ZoneId utcZoneId = ZoneId.of("UTC");	
						ZonedDateTime propsectTimeInUTC = propsectZoneDateTime.withZoneSameInstant(utcZoneId);
						Date callbackDate = Date.from(propsectTimeInUTC.toInstant());
						int alreadyScheduledCallbacks = prospectCallLogRepository.getCountAlreadyScheduledCallbackOfAgent(prospectCallDTO.getCampaignId(), prospectCallDTO.getAgentId(), callbackDate);
						if (alreadyScheduledCallbacks == 0) {
							prospectCallLogFromDB.setCallbackDate(Date.from(propsectTimeInUTC.toInstant()));
						} else {
							throw new IllegalArgumentException("You are having another callback at same time.");
						}
					}
				}
			} else if (prospectCallFromAgent.getDispositionStatus().equals(DispositionType.FAILURE.name())) {
				// DATE:- 19-02-2020 Add prospect in DNC when agent submits the prospect as INVALID_DATA from agent screen.
				if (prospectCallFromAgent.getSubStatus().equals(FailureDispositionStatus.INVALID_DATA.name())) {
					if (campaign != null && campaign.getDialerMode().equals(DialerMode.PREVIEW)) {
						prospectCallLogFromDB.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						prospectCallLogFromDB.getProspectCall().setCallRetryCount(144144);
					} else {
						prospectCallLogFromDB.getProspectCall().setSubStatus("PROSPECT_UNREACHABLE");
						addProspectInDNC(prospectCallFromAgent.getProspect().getPhone(), campaign.getOrganizationId(),
								"INVALID_DATA", prospectCallLogFromDB);
					}
				}
				// add to Do Not Call List, if status is DNCL
				if (prospectCallLogFromDB.getProspectCall().getProspect().getIsEu() || prospectCallFromAgent.getSubStatus().equals(FailureDispositionStatus.DNCL.name())) { // add to IDNC
					try {
						addProspectInDNC(prospectCallFromAgent.getProspect().getPhone(), campaign.getOrganizationId(),
								"DNCL requested by prospect while on the call", prospectCallLogFromDB);
					} catch ( Exception e) {
						logger.error("submitProspectCall() : Error occurred while adding PhoneNumber ["
								+ prospectCallFromAgent.getProspect().getPhone() + "] to IDNC.", e);
					}
				}

				Campaign dsCampaign = campaignService
						.getCampaign(prospectCallLogFromDB.getProspectCall().getCampaignId());
				if (dsCampaign.getOrganizationId().equalsIgnoreCase(XtaasConstants.DEMANDSHORE)
						|| dsCampaign.getOrganizationId().equalsIgnoreCase(XtaasConstants.SALESIFY)
						|| dsCampaign.getOrganizationId().equalsIgnoreCase(XtaasConstants.DEMANDSHOREAG)) {
					if ((prospectCallLogFromDB.getProspectCall().getProspect().getCampaignContactId() != null
							&& !prospectCallLogFromDB.getProspectCall().getProspect().getCampaignContactId()
									.isEmpty())
							|| (prospectCallLogFromDB.getProspectCall().getProspect().getSource() != null
									&& prospectCallLogFromDB.getProspectCall().getProspect().getSource()
											.equals(XtaasConstants.PROSPECT_TYPE.RESEARCH.name()))) {
						setDataInDemandShoreStatusDTO(dsCampaign,prospectCallLogFromDB,prospectCallFromAgent);
					}
				}
			}
			// prospectCallLogFromDB = changeSliceAndDirectPhoneFlag(prospectCallLogFromDB);
			updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLogFromDB);
			checkProspectQualifiedForTranscription(campaign, prospectCallLogFromDB);
			setVoiceProvider(campaign, agent.getAgentType().name(), prospectCallLogFromDB);
			setSipProvider(campaign, agent.getAgentType().name(), prospectCallLogFromDB);
			setDialerMode(campaign.getDialerMode().name(),agent, prospectCallLogFromDB);
			prospectCallLogFromDB.getProspectCall().setAgentLoginURL(serverAccessUrl);
			if (campaign.isRevealEmail()) {
				prospectCallLogFromDB.getProspectCall().setEmailRevealed(prospectCallDTO.isEmailRevealed());
			}
			prospectCallLogService.save(prospectCallLogFromDB, true);

			// send Lead/Prospect to client if disposition status is SUCCESS
			try {
				RealTimeDelivery realTimeDelivery = getRealTimeDelivery(
						prospectCallLogFromDB.getProspectCall().getCampaignId());

				if (realTimeDelivery != null && realTimeDelivery.getRealTimePost().equals(RealTimePost.AGENTREALTIMEPOST)) {
					if (!prospectCallLogFromDB.getProspectCall().isLeadDelivered()
							&& prospectCallFromAgent.getDispositionStatus().equals(DispositionType.SUCCESS.name())) {
						leadReportServiceImpl.realTimeByCampaignId(prospectCallLogFromDB);
					}
				}
			} catch ( Exception e) {
				e.printStackTrace();
			}

			XtaasUtils.callDisposedLogStatement(prospectCallLogFromDB, campaign, agent);

			// log agent activity
			String agentType = agent.getAgentType().name();
			logAgentActivity(prospectCallFromAgent.getAgentId(), AgentActivityType.CALL_STATUS_CHANGE,
					AgentActivityCallStatus.CALL_WRAPPED_UP.name(), prospectCallDTO.getProspectCallId(),
					prospectCallDTO.getCampaignId(), prospectCallDTO.getWrapupDuration(),
					prospectCallLogFromDB.getProspectCall().getProspectInteractionSessionId(), agentType);

			// act based on agent's current status. If agent has requested to change its
			// status then handle it.
			if (prospectCallDTO.isFinalSubmit()) {
				try {
					prospectCallLogService.updateRecordingAndFailureProspects(prospectCallDTO.getCampaignId(),
							prospectCallDTO.getTwilioCallSid());
				} catch ( Exception e) {
					logger.error("Error occurred in updateRecordingAndFailureProspects.");
				}
				handleAgentStatusChangeRequest(prospectCallFromAgent);
			}
			logger.info("submitProspectCall() : Call Submission request submitted by Agent processed SUCCESSFULLY. Agent : "
					+ prospectCallDTO.getAgentId());
			if (prospectCallFromAgent.getDispositionStatus() != null
					&& prospectCallFromAgent.getDispositionStatus().equalsIgnoreCase("CALLBACK")) {
				prospectCallLogService.generateCallbackCache(campaign, true);
				prospectCallLogService.generateMissedCallbackCache(campaign, true);
			}
			if(organization != null && organization.isLiteMode()) {
				if(prospectCallLogFromDB.isResearchData() && prospectCallLogFromDB.getProspectCall().getCallRetryCount() == 55555 && !(prospectCallFromAgent.getDispositionStatus().equals(DispositionType.SUCCESS.name()) || prospectCallFromAgent.getDispositionStatus().equals(DispositionType.FAILURE.name()))) {
					prospectCallLogFromDB.getProspectCall().setCallRetryCount(1);
					if(prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")!=null && !prospectCallLogFromDB.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString().isEmpty()) {
						dataSlice.tagSliceAndQualityScore(Arrays.asList(prospectCallLogFromDB),campaign);
					}else {
						prospectCallLogFromDB.getProspectCall().setDataSlice("slice10");
						prospectCallLogFromDB.setDataSlice("slice10");
						//TODO as there are no enough parameters to derive quality bucket we are defaulting to 555555
						prospectCallLogFromDB.getProspectCall().setQualityBucketSort(555555);
					}
					prospectCallLogService.save(prospectCallLogFromDB, false);
				}
				if(campaign != null && campaign.getUpperThreshold() != 0) {
					int count = getCallableList(campaign);
					prospectCacheServiceImpl.addInCachedCallableDataInDBMap(prospectCallLogFromDB.getProspectCall().getCampaignId(),count);
					int callableCount = prospectCacheServiceImpl.getCachedCallableDataInDBCount(prospectCallLogFromDB.getProspectCall().getCampaignId());
					if (callableCount > 0) {
						if (campaign != null && campaign.getLowerThreshold() > 0
								&& campaign.getUpperThreshold() > 0) {
							checkThreshold(campaign.getId(), callableCount);
						}
					}
				}
			}
		}
	}

	private int getCallableList(Campaign campaign) {
		int dailyCallMaxRetries = campaign.getDailyCallMaxRetries();
		int callMaxRetries = campaign.getCallMaxRetries();
		String strCurrentDateYMD = XtaasDateUtils.getCurrentDateInPacificTZinYMD();
		int maxDailyCallRetryCount = Integer.valueOf(strCurrentDateYMD + String.format("%02d", dailyCallMaxRetries));

		int prospectCallLogListCount = prospectCallLogRepository.countContactsByQueuedInCampaign(campaign.getId());

		if (callMaxRetries >= 1) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(1,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 2) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(2,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 3) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(3,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 4) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(4,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 5) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(5,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 6) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(6,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 7) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(7,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 8) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(8,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 9) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(9,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}
		if (callMaxRetries >= 10) {
			int count = prospectCallLogRepository.countContactsByCallRetryCountAndDailyCallRetryCountInCampaign(10,
					maxDailyCallRetryCount, campaign.getId());
			prospectCallLogListCount = prospectCallLogListCount + count;
		}

		return prospectCallLogListCount;
	}

	private void checkThreshold(String campaignId, int callableCount) {
		String thresholdType = null;
		String msg = null;
		if(campaignId != null && !campaignId.isEmpty()) {
			Campaign campaign = campaignRepository.findOneById(campaignId);
			if (callableCount > campaign.getUpperThreshold()) {
				campaign.setAutoGoldenPass(true);
				campaignRepository.save(campaign);
				thresholdType = "UpperThreshold";
			}else if (callableCount < campaign.getLowerThreshold()) {
				campaign.setAutoGoldenPass(false);
				campaignRepository.save(campaign);
				thresholdType = "LowerThreshold";
			}
			if (thresholdType != null && !thresholdType.isEmpty()) {
				if (thresholdType.equalsIgnoreCase("UpperThreshold")) {
					msg = "DIALING MODE CHANGED: The campaign " + campaign.getName()
							+ " can dial in AUTO mode since callable data is sufficient. Review dialing modes.";
				} else {
					msg = "DIALING MODE CHANGED: The campaign " + campaign.getName()
							+ " cannot dial in AUTO mode since callable data is insufficient. Review dialing modes.";
				}
				if (campaign.getAdminSupervisorIds() != null && !campaign.getAdminSupervisorIds().isEmpty()
						&& campaign.getAdminSupervisorIds().size() > 0) {
					String s = msg;
					for (String supervisorId : campaign.getAdminSupervisorIds()) {
						List<TeamNotice> teamNotice = teamNoticeRepository.getNoticesByRequestorId(new Date(), supervisorId);
						List<TeamNotice> match = teamNotice.stream().filter(obj -> obj.getNotice().equalsIgnoreCase(s)).collect(Collectors.toList());
						if(match != null && !match.isEmpty() && match.size() > 0) {
							teamNoticeRepository.deleteAll(match);
							PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
							teamNoticeSerive.saveUserNotices(supervisorId, msg);
						}else {
							PusherUtils.pushMessageToUser(supervisorId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
							teamNoticeSerive.saveUserNotices(supervisorId, msg);
						}
					}
				}
			}
		}	
	}
	
	private void setVoiceProvider(Campaign campaign, String agentType, ProspectCallLog prospectCallLog) {
		if (campaign != null && campaign.getDialerMode() != null && agentType != null
				&& (campaign.getAutoModeCallProvider() != null && !campaign.getAutoModeCallProvider().isEmpty()
						|| (campaign.getManualModeCallProvider() != null
								&& !campaign.getManualModeCallProvider().isEmpty()))) {
			if (campaign.getDialerMode().equals(DialerMode.POWER) || campaign.getDialerMode().equals(DialerMode.PREVIEW) || (campaign.getDialerMode().equals(DialerMode.HYBRID)
					&& agentType.equals(agentSkill.AUTO.name()))) {
				if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
					if (campaign.getAutoModeCallProvider() != null
							&& !campaign.getAutoModeCallProvider().isEmpty()) {
						prospectCallLog.getProspectCall().setVoiceProvider(campaign.getAutoModeCallProvider());
					}
				}
			} else {
				if (campaign.getManualModeCallProvider() != null
						&& !campaign.getManualModeCallProvider().isEmpty()) {
					prospectCallLog.getProspectCall().setVoiceProvider(campaign.getManualModeCallProvider());
				}
			}
		} else {
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
				if (campaign.getCallControlProvider() != null && !campaign.getCallControlProvider().isEmpty()) {
					prospectCallLog.getProspectCall().setVoiceProvider(campaign.getCallControlProvider());
				}
			} else {
				new RuntimeException("Voice Provider not stamped in the Prospect.");
			}
		}
	}

	private void setSipProvider(Campaign campaign, String agentType, ProspectCallLog prospectCallLog) {
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
			if (campaign.getSipProvider() != null && !campaign.getSipProvider().isEmpty()) {
				prospectCallLog.getProspectCall().setSipProvider(campaign.getSipProvider());
			}
		} else {
			new RuntimeException("SIP Provider not stamped in the Prospect.");
		}
	}
	
	private void setDialerMode(String dialerMode, Agent agent, ProspectCallLog prospectCallLog) {
		String campaignDialerMode;
		if (dialerMode != null && !dialerMode.isEmpty() && agent != null && agent.getAgentType() != null) {
			if (dialerMode.equalsIgnoreCase("HYBRID")) {
				campaignDialerMode = agent.getAgentType().toString();
			} else {
				campaignDialerMode = dialerMode;
			}
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
				prospectCallLog.getProspectCall().setCampaignMode(campaignDialerMode);
			}
		} else {
			new RuntimeException("Dialermode not stamped in the Prospect.");
		}
	}
	
	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d= new Date();
		//System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid+Long.toString(d.getTime());
		return pcidv;
	}

	private void setDataInDemandShoreStatusDTO( Campaign dsCampaign,  ProspectCallLog prospectCallLogFromDB,
			 ProspectCall prospectCallFromAgent) {
		 DemandShoreStatusDTO dsDTO = new DemandShoreStatusDTO();
		dsDTO.setSfdcCampaignId(dsCampaign.getName());
		dsDTO.setOrganizationName(prospectCallLogFromDB.getProspectCall().getProspect().getCompany());
		String domain = null;
		if (prospectCallLogFromDB.getProspectCall().getProspect()
				.getCustomAttributeValue("domain") != null
				&& !prospectCallLogFromDB.getProspectCall().getProspect()
						.getCustomAttributeValue("domain").toString().isEmpty()) {
			domain = prospectCallLogFromDB.getProspectCall().getProspect()
					.getCustomAttributeValue("domain").toString();
		}
		dsDTO.setDomain(domain);
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		 String strDate = formatter.format(prospectCallLogFromDB.getProspectCall().getCallStartTime());
		dsDTO.setCallStartDateandTime(strDate);
		dsDTO.setPhone(prospectCallFromAgent.getProspect().getPhone());
		dsDTO.setContactId(prospectCallLogFromDB.getProspectCall().getProspect().getCampaignContactId());
		dsDTO.setEmail(prospectCallLogFromDB.getProspectCall().getProspect().getEmail());
		dsDTO.setFirstName(prospectCallLogFromDB.getProspectCall().getProspect().getFirstName());
		dsDTO.setLastName(prospectCallLogFromDB.getProspectCall().getProspect().getLastName());
		//dsDNCDto.setFlc("");
		dsDTO.setSubStatus(prospectCallLogFromDB.getProspectCall().getSubStatus());
		dsDTO.setSource(prospectCallLogFromDB.getProspectCall().getProspect().getSource());
		dsDTO.setTitle(prospectCallLogFromDB.getProspectCall().getProspect().getTitle());
		dsDTO.setDepartment(prospectCallLogFromDB.getProspectCall().getProspect().getDepartment());
		dsDTO.setStateCode(prospectCallLogFromDB.getProspectCall().getProspect().getStateCode());
		dsDTO.setCountry(prospectCallLogFromDB.getProspectCall().getProspect().getCountry());
		dsDTO.setDispositionStatus(prospectCallLogFromDB.getProspectCall().getDispositionStatus());
		if (prospectCallLogFromDB != null && prospectCallLogFromDB.getProspectCall() != null
				&& prospectCallLogFromDB.getProspectCall().getCallDuration() != 0) {
			dsDTO.setCallDuration(Integer.toString(prospectCallLogFromDB.getProspectCall().getCallDuration()));
		}
		dsDTO.setRecordingURL(prospectCallLogFromDB.getProspectCall().getRecordingUrlAws());
		dsDTO.setAgentID(prospectCallLogFromDB.getProspectCall().getAgentId());
		dsDTO.setPartnerID(prospectCallLogFromDB.getProspectCall().getPartnerId());
		//dsDTO.set
		 String url = "https://d.ziffdavisb2b.com/integrations/getXTaaSDNC";
		// Creating Object of ObjectMapper define in Jakson Api
		 ObjectMapper Obj = new ObjectMapper();
		try {
			// get Oraganisation object as a json string
			 String jsonStr = Obj.writeValueAsString(dsDTO);
			try {
				sendDemandShorePost(jsonStr);
			} catch ( Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Displaying JSON String
			System.out.println(jsonStr);
		} catch ( IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * DATE:- 19-02-2020
	 * Add prospect in DNCL when agent submits prospect as DNCL or INVALID_DATA from agent screen.
	 * @param phoneNumber
	 * @param organizationId
	 * @param notes
	 * @param prospectCallLogFromDB
	 */
	private void addProspectInDNC( String phoneNumber,  String organizationId,  String notes,
			 ProspectCallLog prospectCallLogFromDB) {
		 IDNCNumberDTO idncNumberDTO = new IDNCNumberDTO();
		idncNumberDTO.setPhoneNumber(phoneNumber);
		idncNumberDTO.setNote(notes);
		idncNumberDTO.setTrigger(DNCTrigger.CALL);
		idncNumberDTO.setPartnerId(organizationId);
		idncNumberDTO.setCampaignId(prospectCallLogFromDB.getProspectCall().getCampaignId());
		idncNumberDTO.setProspectCallId(prospectCallLogFromDB.getProspectCall().getProspectCallId());
		idncNumberDTO.setRecordingUrl(prospectCallLogFromDB.getProspectCall().getRecordingUrl());
		idncNumberDTO.setRecordingUrlAws(prospectCallLogFromDB.getProspectCall().getRecordingUrlAws());
		idncNumberDTO.setFirstName(prospectCallLogFromDB.getProspectCall().getProspect().getFirstName());
		idncNumberDTO.setLastName(prospectCallLogFromDB.getProspectCall().getProspect().getLastName());
		idncNumberDTO.setDncLevel(XtaasConstants.DNC_PROSPECT);
		internalDNCListService.addNumber(idncNumberDTO);
	}

	private void isProspectUniqueInCampaign( ProspectCallDTO prospectCallDTO) {
		String phoneNumber = prospectCallDTO.getProspect().getPhone(); // Removed + from phone number. as it gives error
																		// in searching record from database.
		phoneNumber = phoneNumber.substring(1);
		 ProspectCallLog uniqueProspectFromDB = prospectCallLogRepository.findManualProspectRecord(
				prospectCallDTO.getCampaignId(), prospectCallDTO.getProspect().getFirstName(),
				prospectCallDTO.getProspect().getLastName(), phoneNumber);

		if (uniqueProspectFromDB != null && uniqueProspectFromDB.getProspectCall().getDispositionStatus() != null
				&& uniqueProspectFromDB.getStatus() != null) {
			if (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("FAILURE")) {
				if (uniqueProspectFromDB.getProspectCall().getSubStatus().equalsIgnoreCase("DNCL")) {
					throw new IllegalArgumentException("Prospect is in DNC.");
				}
				throw new IllegalArgumentException(
						"Prospect is in FAILURE - " + uniqueProspectFromDB.getProspectCall().getSubStatus());
			} else if (uniqueProspectFromDB.getProspectCall().getDispositionStatus().equalsIgnoreCase("SUCCESS")) {
				throw new IllegalArgumentException("Prospect is already lead in the campaign");
			}
		}
	}

	private RealTimeDelivery getRealTimeDelivery( String xtaasCampaignId) {
		return realTimeDeliveryRepository.findByXtaasCampaignId(xtaasCampaignId);
	}

	private CallLog fetchAndUpdateCallRecording( CallLog callLog) {
		if (callLog.getCallLogMap().get("ConferenceSid") != null) {
			 Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
			 Account account = channelTwilio.getChannelConnection();
			 Map<String, String> filter = new HashMap<String, String>();
			filter.put("ConferenceSid", callLog.getCallLogMap().get("ConferenceSid"));
			 RecordingList recordings = account.getRecordings(filter);
			String awsRecordingUrl = "";
			for ( Recording recording : recordings) {
				/*
				 * api returns with extension as .json, removing it so that it becomes playable.
				 * Ex: /2010-04-01/Accounts/ACab4d611aa1465a246cb58290ba930a5c/Recordings/
				 * REb64d147f6a4008ae4e308486e4029a5a.json
				 */
				 String recordingUrl = "https://api.twilio.com"
						+ StringUtils.replace(recording.getProperty("uri"), ".json", "");
				callLog.getCallLogMap().put("RecordingUrl", recordingUrl);
				callLog.getCallLogMap().put("RecordingDuration", recording.getProperty("duration"));
				 int lastIndex = recordingUrl.lastIndexOf("/");
				 String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
				 String rfileName = fileName + ".wav";
				awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
				callLog.getCallLogMap().put("awsRecordingUrl", awsRecordingUrl);
				 String prospectCallId = callLog.getCallLogMap().get("pcid");
				downloadRecordingsToAws.downloadRecording(recordingUrl, prospectCallId, "", true);
				break; // return the first recording only. support for multiple call recordings
			}
			return callLogRepository.save(callLog);
		}
		return callLog;
	}

	/*
	 * this will update primary prospect in case of multiprospect when disposition
	 * status of primary prospect is dnc/failure/ans-mach
	 */
	private ProspectCallLog updatePrimaryProspectDetails( ProspectCallLog prospectCallLogFromDB,
			 ProspectCall prospectCallFromAgent) {
		 Agent agent = agentRepository.findOneById(prospectCallFromAgent.getAgentId());
		prospectCallFromAgent.setPartnerId(agent.getPartnerId());

		// copying callRetryCount, interactionSessionId
		prospectCallFromAgent.setCallRetryCount(prospectCallLogFromDB.getProspectCall().getCallRetryCount());
		prospectCallFromAgent.setProspectInteractionSessionId(
				prospectCallLogFromDB.getProspectCall().getProspectInteractionSessionId());
		prospectCallFromAgent.setDailyCallRetryCount(prospectCallLogFromDB.getProspectCall().getDailyCallRetryCount());
		prospectCallFromAgent.setProspectSortIndex(prospectCallLogFromDB.getProspectCall().getProspectSortIndex());

		prospectCallLogFromDB.setProspectCall(prospectCallFromAgent);
		prospectCallLogFromDB.getProspectCall().setIndirectProspects(null);

		if (prospectCallFromAgent.getDispositionStatus().equals(DispositionType.CALLBACK.name())) {
			// set the callback date
			 DateTime callbackDateTime = new DateTime(prospectCallFromAgent.getCallbackTime(), DateTimeZone.UTC);
			prospectCallLogFromDB.setCallbackDate(callbackDateTime.toDate());
		} else if (prospectCallFromAgent.getDispositionStatus().equals(DispositionType.FAILURE.name())) {
			// add to Do Not Call List, if status is DNCL
			if (prospectCallFromAgent.getSubStatus().equals(FailureDispositionStatus.DNCL.name())) {
				// add to IDNC
				try {
					 IDNCNumberDTO idncNumberDTO = new IDNCNumberDTO();
					idncNumberDTO.setPhoneNumber(prospectCallFromAgent.getProspect().getPhone());
					idncNumberDTO.setNote("DNCL requested by prospect while on the call");
					idncNumberDTO.setTrigger(DNCTrigger.CALL);
					idncNumberDTO.setPartnerId(agent.getPartnerId());
					idncNumberDTO.setCampaignId(prospectCallLogFromDB.getProspectCall().getCampaignId());
					idncNumberDTO.setProspectCallId(prospectCallLogFromDB.getProspectCall().getProspectCallId());
					idncNumberDTO.setRecordingUrl(prospectCallLogFromDB.getProspectCall().getRecordingUrl());
					idncNumberDTO.setRecordingUrlAws(prospectCallLogFromDB.getProspectCall().getRecordingUrlAws());
					idncNumberDTO.setFirstName(prospectCallLogFromDB.getProspectCall().getProspect().getFirstName());
					idncNumberDTO.setLastName(prospectCallLogFromDB.getProspectCall().getProspect().getLastName());
					internalDNCListService.addNumber(idncNumberDTO);

					 Campaign dsCampaign = campaignService
							.getCampaign(prospectCallLogFromDB.getProspectCall().getCampaignId());
					if (dsCampaign.getOrganizationId().equalsIgnoreCase(XtaasConstants.DEMANDSHORE)) {
						 DemandShoreDNCDTO dsDNCDto = new DemandShoreDNCDTO();
						dsDNCDto.setCampaign_id(dsCampaign.getName());
						dsDNCDto.setCompany(prospectCallLogFromDB.getProspectCall().getProspect().getCompany());
						String domain = null;
						if (prospectCallLogFromDB.getProspectCall().getProspect()
								.getCustomAttributeValue("domain") != null
								&& !prospectCallLogFromDB.getProspectCall().getProspect()
										.getCustomAttributeValue("domain").toString().isEmpty()) {
							domain = prospectCallLogFromDB.getProspectCall().getProspect()
									.getCustomAttributeValue("domain").toString();
						}
						dsDNCDto.setDomain(domain);
						 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						 String strDate = formatter.format(prospectCallLogFromDB.getProspectCall().getCallStartTime());
						dsDNCDto.setDnc_call_time(strDate);
						dsDNCDto.setCompany_board_line(prospectCallFromAgent.getProspect().getPhone());
						dsDNCDto.setContact_id(prospectCallLogFromDB.getProspectCall().getProspect().getSourceId());
						dsDNCDto.setEmail_id(prospectCallLogFromDB.getProspectCall().getProspect().getEmail());
						dsDNCDto.setFirst_name(prospectCallLogFromDB.getProspectCall().getProspect().getFirstName());
						dsDNCDto.setLast_name(prospectCallLogFromDB.getProspectCall().getProspect().getLastName());
						dsDNCDto.setFlc("");
						dsDNCDto.setStatus(prospectCallLogFromDB.getProspectCall().getSubStatus());
						dsDNCDto.setList_name(prospectCallLogFromDB.getProspectCall().getProspect().getSource());
						 String url = "https://d.ziffdavisb2b.com/integrations/getXTaaSDNC";
						// Creating Object of ObjectMapper define in Jakson Api
						 ObjectMapper Obj = new ObjectMapper();
						try {
							// get Oraganisation object as a json string
							 String jsonStr = Obj.writeValueAsString(dsDNCDto);
							sendDemandShorePost(jsonStr);
							// Displaying JSON String
							System.out.println(jsonStr);
						} catch ( IOException e) {
							e.printStackTrace();
						}
					}
				} catch ( Exception e) {
					logger.error("submitProspectCall() : Error occurred while adding PhoneNumber ["
							+ prospectCallFromAgent.getProspect().getPhone() + "] to IDNC.", e);
				}
			}
		}
		return prospectCallLogFromDB;
	}

	private void incrementDailyCallRetryCount( ProspectCall prospectCall) {
		 String strCurrentDateYMD = getCurrentDateInProspectTZinYMD(prospectCall.getProspect()); // calculate current
																								// date/time in
																								// prospect's timezone.
																								// returns date in
																								// 20160708 format
		if (prospectCall.getDailyCallRetryCount() == null) { // dailyCallRetryCount has format : yyyyMMdd<2 digit
																// counter>
			prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01")); // set the daily retry to 01,
																							// since its first time
		} else if (String.valueOf(prospectCall.getDailyCallRetryCount()).length() < 10) {
			prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01")); // set the daily retry to 01,
																							// since its first time
		} else { // if already set, then increment last 2 digits if date has not changed else
					// reset to new date
			 String strDailyCallRetryDate = StringUtils.substring(String.valueOf(prospectCall.getDailyCallRetryCount()),
					0, 8); // returns date set in daily counter
			Integer dailyRetryCounter = Integer
					.parseInt(StringUtils.substring(String.valueOf(prospectCall.getDailyCallRetryCount()), 8)); // extract
																												// last
																												// 2
																												// digits
																												// for
																												// counter
			if (!strDailyCallRetryDate.equals(strCurrentDateYMD)) { // if current date is not same as date in daily
																	// counter
				// then reset the counter with new date, set the daily retry to 01, since its
				// first time
				prospectCall.setDailyCallRetryCount(Long.valueOf(strCurrentDateYMD + "01")); // format: yyyyMMdd<2 digit
																								// counter>
			} else { // if daily counter date is same as current date then
				dailyRetryCounter += 1; // increment the counter
				prospectCall.setDailyCallRetryCount(
						Long.valueOf(strCurrentDateYMD + String.format("%02d", dailyRetryCounter)));
			}
		}
	}

	private String getCurrentDateInProspectTZinYMD( Prospect prospect) {
		 StateCallConfig stateCallConfig = configService.getStateCallConfig(prospect.getStateCode());
		String currDateInProspectsTZ = null;
		try {
			currDateInProspectsTZ = XtaasDateUtils.convertToTimeZone(new Date(), stateCallConfig.getTimezone(),
					"yyyyMMdd");
		} catch ( ParseException e) {
			logger.error("getCurrentDateInPacificTZinYMD() : Exception occurred while converting date into Pacific TZ",
					e);
		}
		return currDateInProspectsTZ;
	}

	private String createProspectCallLogs( ProspectCall prospectCall,  ProspectCallLog prospectCallLog,
			 boolean isNewProspect) {
		if (prospectCall != null) {
			if (isNewProspect) {
				prospectCallLog.setProspectCall(prospectCall);
				prospectCallLog.getProspectCall().setIndirectProspects(null);
				prospectCallLog.setStatus(ProspectCallStatus.QUEUED);
				prospectCallLogService.save(prospectCallLog, true);
			}
			prospectCallLog.getProspectCall().setIndirectProspects(null);
			prospectCallLog.setStatus(ProspectCallStatus.CALL_DIALED);
			prospectCallLogService.save(prospectCallLog, true);
			// 25-10-2018 => removed CALL_INPROGRESS, CALL_COMPLETE entry on the request of
			// Manish
			// prospectCallLog.setStatus(ProspectCallStatus.CALL_INPROGRESS);
			// prospectCallLogService.save(prospectCallLog, true);
			// prospectCallLog.setStatus(ProspectCallStatus.CALL_COMPLETE);
			// prospectCallLogService.save(prospectCallLog, true);
			return prospectCall.getProspectCallId();
		}
		return null;
	}
	
	@Override
	public void handleAgentStatusChangeRequest( ProspectCall prospectCallFromAgent) {
		 AgentCall agentCall = agentRegistrationService.getAgentCall(prospectCallFromAgent.getAgentId());

		if (agentCall == null) {
			logger.error("handleAgentStatusChangeRequest() : Agent [{}] not ONLINE, may be already got unregistered",
					prospectCallFromAgent.getAgentId());
			return;
		}

		switch (agentCall.getCurrentStatus()) {
		case ONLINE:
			// Call completed..Make Agent available for next call
			agentRegistrationService.unallocateAgent(agentCall);
			break;
		case REQUEST_BREAK:
		case REQUEST_MEETING:
		case REQUEST_TRAINING:
		case REQUEST_OFFLINE:
		case REQUEST_OTHER:
			agentCall.setProspectCall(null);
			AgentCampaignDTO agentCampaign = null;
			agentCampaign = agentCall.getCurrentCampaign();
			if ((agentCampaign != null && !agentCampaign.isSupervisorAgentStatusApprove()) || agentCall.isSupervisorApproved()) {
				approveNonOnlineStatusChangeRequest(prospectCallFromAgent.getAgentId(),
					prospectCallFromAgent.getCampaignId(), agentCall.getCurrentStatus());
			}
			break;
		}
	}

	private void approveNonOnlineStatusChangeRequest( String agentId,  String campaignId,  AgentStatus status) {
		switch (status) {
		case REQUEST_BREAK:
			// doChangeStatus(agentId, AgentStatus.ONBREAK, campaignId);
			pushStatusApprovedNoticeToAgent(agentId,
					"Your BREAK request is approved. Your status is changed to On-Break",
					AgentStatus.ONBREAK.toString());
			break;
		case REQUEST_MEETING:
			// doChangeStatus(agentId, AgentStatus.MEETING, campaignId);
			pushStatusApprovedNoticeToAgent(agentId,
					"Your MEETING request is approved. Your status is changed to Meeting",
					AgentStatus.MEETING.toString());
			break;
		case REQUEST_TRAINING:
			// doChangeStatus(agentId, AgentStatus.TRAINING, campaignId);
			pushStatusApprovedNoticeToAgent(agentId,
					"Your TRAINING request is approved. Your status is changed to Training",
					AgentStatus.TRAINING.toString());
			break;
		case REQUEST_OFFLINE:
			// doChangeStatus(agentId, AgentStatus.OFFLINE, campaignId);
			pushStatusApprovedNoticeToAgent(agentId,
					"Your OFFLINE request is approved. Your status is changed to Offline",
					AgentStatus.OFFLINE.toString());
			prospectCacheServiceImpl.removeMultiRingingControlMapCallLegIdMap(agentId);
			break;
		case REQUEST_OTHER:
			// doChangeStatus(agentId, AgentStatus.OTHER, campaignId);
			pushStatusApprovedNoticeToAgent(agentId, "Your OTHER request is approved. Your status is changed to Other",
					AgentStatus.OTHER.toString());
			break;
		}
	}

	private void pushStatusApprovedNoticeToAgent( String agentId,  String notice,  String newStatus) {
		try {
			 HashMap<String, String> noticeMap = new HashMap<String, String>();
			noticeMap.put("notice", notice);
			noticeMap.put("new_status", newStatus);
			pushMessageToAgent(agentId, "status_approve_notice", JSONUtils.toJson(noticeMap));
		} catch ( IOException e) {
			logger.error("pushNoticeToAgent() : Error occurred while pushing Notice to Agent: " + agentId, e);
		}
	}

	/*
	 * public List<AgentSearchResult> searchByQa() { List<String> agentIds = new
	 * ArrayList<String>(); List<String> teamIds = qaService.getTeamIdsByQa();
	 * List<Campaign> campaigns = campaignRepository.findByTeams(teamIds); for
	 * (Campaign campaign : campaigns) { List<String> campaignAgents =
	 * getCampaignAgentIds(campaign); for (String campaignAgent : campaignAgents) {
	 * if (!agentIds.contains(campaignAgent)) { agentIds.add(campaignAgent); } } }
	 * List<AgentSearchResult> agentSearchResults =
	 * agentRepository.findByIds(agentIds); return
	 * getRequireFieldsAgentSearchResult(agentSearchResults);
	 */
	@Override
	public List<AgentSearchResult> searchByQa( AgentSearchDTO agentSearchDTO) throws ParseException {
		 ProspectCallSearchDTO prospectCallSearchDTO = new ProspectCallSearchDTO();
		prospectCallSearchDTO.setClause(ProspectCallSearchClause.ByQa);
		return qaService.getAgentsForProspectCallsByQa(prospectCallSearchDTO);
	}

	// Added method for qafilter change
	/*
	 * @Override public List<AgentSearchResult> searchBySecQa(AgentSearchDTO
	 * agentSearchDTO) throws ParseException { ProspectCallSearchDTO
	 * prospectCallSearchDTO = new ProspectCallSearchDTO();
	 * prospectCallSearchDTO.setClause(ProspectCallSearchClause.ByQa2);
	 * prospectCallSearchDTO.setProspectCallStatus(agentSearchDTO.
	 * getProspectCallStatus()); return
	 * qaService.getAgentsForProspectCallsBySecQa(prospectCallSearchDTO); }
	 */

	@Override
	public List<AgentSearchResult> searchBySecQa( AgentSearchDTO agentSearchDTO) throws ParseException {
		List<ProspectCallLog> prospectCalls = new ArrayList<ProspectCallLog>();
		 List<String> agentIds = new ArrayList<String>();
		prospectCalls = prospectCallLogRepository.findByStatus("SECONDARY_QA_INITIATED");
		if (!prospectCalls.isEmpty()) {
			Campaign campaign = null;
			String campaignId;
			String agentId;
			for ( ProspectCallLog prospectCallLog : prospectCalls) {
				agentId = prospectCallLog.getProspectCall().getAgentId();
				campaignId = prospectCallLog.getProspectCall().getCampaignId();
				campaign = campaignRepository.findOneById(campaignId);
				if (campaign != null && campaign.getStatus() == CampaignStatus.RUNNING && campaign.getQaRequired()
						&& !agentIds.contains(agentId)) {
					agentIds.add(agentId);
				}
			}
			if (!agentIds.isEmpty()) {
				return agentRepository.findByIds(agentIds);
			}
		}
		return new ArrayList<AgentSearchResult>(); // returning an empty result;
	}

	private List<AgentSearchResult> getRequireFieldsAgentSearchResult( List<AgentSearchResult> agentSearchResults) {
		for ( AgentSearchResult agentSearchResult : agentSearchResults) {// Set below sensitive information null, should
																		// not be send to QA screen . Just sending Id,
																		// name of Agent
			agentSearchResult.setAddress(null);
			agentSearchResult.setAgentProfilePicUrl(null);
			agentSearchResult.setAverageAvailableHours(null);
			agentSearchResult.setAverageConversion(null);
			agentSearchResult.setAverageQuality(null);
			agentSearchResult.setAverageRating(null);
			agentSearchResult.setAverageTalkTime(null);
			agentSearchResult.setCampaignTemporaryAllocated(null);
			agentSearchResult.setContactsMade(null);
			agentSearchResult.setCurrentStatus(null);
			agentSearchResult.setInvitationDate(null);
			agentSearchResult.setLeadsDelivered(null);
			agentSearchResult.setOverview(null);
			agentSearchResult.setPerLeadHour(null);
			agentSearchResult.setTotalCompletedCampaigns(null);
			agentSearchResult.setTotalVolume(null);
		}
		return agentSearchResults;
	}

	private boolean isCallComplete( String twilioCallStatus) {
		if (XtaasConstants.TWILIO_CALL_STATUS_COMPLETED.equals(twilioCallStatus)
				|| XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus)
				|| XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus)
				|| XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus)
				|| XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return true;
		} else {
			return false;
		}
	}

	private void hangupPreviewOutboundCall( String pcid,  DialerMode dialerMode,  String agentId,  String parentCallSid) {
		if (pcid != null && dialerMode != null && !DialerMode.POWER.equals(dialerMode)) { // if Preview or
																							// Preview_System campaign
																							// then disconnect the
																							// outgoing leg of the call
			logger.error("hangupPreviewOutboundCall() : Initiating HangUp of Preview Outbound Call for agent [{}].",
					agentId);
			// fetch the outboundCallSid from activePreviewCallQueueRepository
			 ActivePreviewCall activePreviewCall = activePreviewCallQueueRepository.findByParentCallSid(parentCallSid);
			logger.debug("hangupPreviewOutboundCall() : Fetched ActivePreviewCall [{}] based on ParentCallSid [{}]",
					activePreviewCall, parentCallSid);
			if (activePreviewCall != null) {
				 String outboundCallSid = activePreviewCall.getCallsid();
				logger.debug(
						"hangupPreviewOutboundCall() : Found OutboundCallSid [{}] from ActivePreviewCallQueue by ParentCallSid [{}] for agent "
								+ agentId,
						outboundCallSid, parentCallSid);
				hangupCall(outboundCallSid); // Disconnecting the call...
			} else {
				// find the callsid from prospectcallinteraction collection where
				// status=CALL_INPROGRESS and "prospectCall.prospectCallId" :
				// "7e107ee1-79e2-4cc1-887c-51205f2bc7b8"
				 Pageable latestCreatedPageable = PageRequest.of(0, 1, Direction.DESC, "createdDate");
				 Date todaysDate = XtaasDateUtils.getCurrentDateInTZ(0, DateTimeZone.UTC.getID()).toDate();
				 List<ProspectCallInteraction> prospectCallInteractionList = prospectLogInteractionRepository
						.findInProgressCallInteraction(pcid, todaysDate, latestCreatedPageable);
				if (prospectCallInteractionList != null && prospectCallInteractionList.size() > 0) {
					 String outboundCallSid = prospectCallInteractionList.get(0).getProspectCall().getTwilioCallSid();
					logger.debug(
							"hangupPreviewOutboundCall() : Found OutboundCallSid [{}] from ProspectCallInteraction. Disconnecting outbound call initiated by agent [{}]",
							outboundCallSid, agentId);
					hangupCall(outboundCallSid); // Disconnecting the call...
				} else {
					logger.error(
							"hangupPreviewOutboundCall() : Cannot DISCONNECT outbound call. No ProspectCallInteraction record found for pcid [{}], date [{}]",
							pcid, todaysDate);
				}
			}
		}
	}

	private void hangupCall( String callSid) {
		// Disconnecting the call...
		 Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
		 Account account = channelTwilio.getChannelConnection();
		 Call call = account.getCall(callSid);

		// hangup the call if still in-progress
		if (!isCallComplete(call.getStatus())) {
			try {
				 Call callAfterHangup = call.hangup();
				logger.debug("hangupPreviewOutboundCall() : Terminated outbound call [{}], Previous Status [{}]",
						callAfterHangup.getSid(), callAfterHangup.getStatus());
			} catch ( TwilioRestException e) {
				logger.error("hangupPreviewOutboundCall() : Error while hanging up the call for CallSid " + callSid, e);
			}
		} else {
			logger.debug(
					"hangupPreviewOutboundCall() : Call [{}] already in COMPLETE/TERMINATED state. Not initiating Disconnect.",
					callSid);
		}
	}

	@Override
	public void changeStatusOnBrowserClosed( String campaignId, String agentType, HttpSession session) {
		 Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		logger.debug(
				"changeStatusOnBrowserClosed() : Changing status offline of agent [{}] on browser closed, campaign [{}]",
				userLogin.getId(), campaignId);
		changeStatus(userLogin.getId(), AgentStatus.OFFLINE, agentType, campaignId); // then make the agent OFFLINE
		// Destroy session when browser closed
		session.invalidate();
		SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
	}

	@Override
	public void allocateAgentFromDialer( String callSid, boolean callAbandon) {
		logger.debug("==========> Finding available agent for CallSid [{}] <==========", callSid);
		AgentCall allocatedAgent = null;
		String prospectCallAsJson = null;
		ProspectCall prospectCall = null;
		allocatedAgent = agentRegistrationService.allocateAgent(callSid);
		if (allocatedAgent != null) {
			prospectCall = allocatedAgent.getProspectCall();
			try {
				prospectCallAsJson = JSONUtils.toJson(prospectCall);
				PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
						prospectCallAsJson);
			} catch ( IOException e) {
				e.printStackTrace();
			}
			logger.debug("==========> Allocated Agent [{}] to ProspectCallId [{}]. <==========",
					allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
		} else {
			try {
				// wait for a second and attempt again to find an agent
				Thread.sleep(1000);
			} catch ( InterruptedException e) {
				logger.error("==========> AgentServiceImpl - Error occurred in allocateAgentPreviewMode <==========");
				logger.error("Expection : {} ", e);
			}
			// re-attempting to find the available agent
			allocatedAgent = agentRegistrationService.allocateAgent(callSid);
			if (allocatedAgent == null) {
				// still no agent available then play the abandon message
				logger.debug("==========> No Agent available for CallSid  [{}] <==========", callSid);
				// update prospect call DIALERCODE - NOANSWER
				if (!callAbandon) {
					prospectCallLogService.updateCallStatus(callSid, ProspectCallStatus.CALL_CANCELED);
				}
			} else {
				prospectCall = allocatedAgent.getProspectCall();
				try {
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					PusherUtils.pushMessageToUser(allocatedAgent.getAgentId(), XtaasConstants.PROSPECT_CALL_EVENT,
							prospectCallAsJson);
				} catch ( IOException e) {
					e.printStackTrace();
				}
				logger.debug("==========> Allocated Agent [{}] to ProspectCallId [{}] <==========",
						allocatedAgent.getAgentId(), prospectCall.getProspectCallId());
			}
		}
	}

	private void getGeoDetailsByZipCode( ProspectCallLog prospectCallLogFromDB) {
		String url = "";
		try {
			url = String.format(ApplicationEnvironmentPropertyUtils.getGeoCodeApiUrl(),
					URLEncoder.encode(prospectCallLogFromDB.getProspectCall().getProspect().getZipCode(), "UTF-8"));
		} catch ( UnsupportedEncodingException e1) {
			logger.error("getGeoDetailsByZipCode - error occured while encoding URL");
		}
		 HttpClient client = HttpClientBuilder.create().build();
		 HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		try {
			 HttpResponse response = client.execute(request);
			 String json = EntityUtils.toString(response.getEntity());
			 ObjectMapper mapper = new ObjectMapper();
			 GeoCodingDTO geoCodingDTO = mapper.readValue(json, GeoCodingDTO.class);
			if (geoCodingDTO != null) {
				for ( GeoCodingDetilsDTO element : geoCodingDTO.getResults()) {
					for ( AddressComponent element1 : element.getAddress_components()) {
						if (element1.getTypes().contains("locality")) {
							if (prospectCallLogFromDB.getProspectCall().getProspect().getCity() == null) {
								prospectCallLogFromDB.getProspectCall().getProspect().setCity(element1.getLong_name());
							} else if (!prospectCallLogFromDB.getProspectCall().getProspect().getCity()
									.equalsIgnoreCase(element1.getLong_name())) {
								prospectCallLogFromDB.getProspectCall().getProspect().setCity(element1.getLong_name());
							}
						}
						if (element1.getTypes().contains("administrative_area_level_1")) {
							if (prospectCallLogFromDB.getProspectCall().getProspect().getStateCode() == null) {
								prospectCallLogFromDB.getProspectCall().getProspect()
										.setStateCode(element1.getShort_name());
							} else if (!prospectCallLogFromDB.getProspectCall().getProspect().getStateCode()
									.equalsIgnoreCase(element1.getShort_name())) {
								prospectCallLogFromDB.getProspectCall().getProspect()
										.setStateCode(element1.getShort_name());
							}
						}
						if (element1.getTypes().contains("country")) {
							if (prospectCallLogFromDB.getProspectCall().getProspect().getCountry() == null) {
								prospectCallLogFromDB.getProspectCall().getProspect()
										.setCountry(element1.getLong_name());
							} else if (!prospectCallLogFromDB.getProspectCall().getProspect().getCountry()
									.equalsIgnoreCase(element1.getLong_name())) {
								prospectCallLogFromDB.getProspectCall().getProspect()
										.setCountry(element1.getLong_name());
							}
						}
					}
				}
			}
		} catch ( ClientProtocolException e) {
			logger.error("getGeoDetailsByZipCode - Error occured while fetching API");
		} catch ( IOException e) {
			logger.error("getGeoDetailsByZipCode - google api key is invalid.");
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public void updateAgentMode(String agentId, String agentMode) {
		List<Team> teamsFromDB = teamRepository.findAllTeamsByAgentId(agentId);
		List<String> supervisorsFromDB = new ArrayList<String>();
		if (teamsFromDB != null && teamsFromDB.size() > 0) {
			supervisorsFromDB = teamsFromDB.stream().map(supervisor -> supervisor.getSupervisor().getResourceId())
					.collect(Collectors.toList());
		}
		String loggedInUser = XtaasUserUtils.getCurrentLoggedInUsername();
		if (loggedInUser != null && supervisorsFromDB.contains(loggedInUser) && !supervisorsFromDB.isEmpty()) {
			Agent agentFromDB = agentRepository.findOneById(agentId);
			if (agentFromDB != null) {
				if (agentMode.equalsIgnoreCase("AUTO")) {
					agentFromDB.setAgentType(agentSkill.AUTO);
				} else {
					agentFromDB.setAgentType(agentSkill.MANUAL);
				}
				agentFromDB = agentRepository.save(agentFromDB);
				clearAgentsInMemory();
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.AGENT_TYPE_CHANGE, agentMode);
				logger.debug("Agent mode is updated for agent having Id : [{}] with mode : [{}]", agentId, agentMode);
			} else {
				throw new IllegalArgumentException("Agent does not exist in the system.");
			}
		} else {
			throw new IllegalArgumentException("The agent is not associated with loggedin supervisors team.");
		}
	}

	public void sendDemandShorePost( String dncObject) throws Exception {
		 HttpClient httpclient = HttpClientBuilder.create().build();
		 HttpPost request = new HttpPost("https://d.ziffdavisb2b.com/integrations/getXTaaSDNC");
		// JSONObject result = new JSONObject();
		try {
			 String bodyContent = dncObject;
			 StringEntity requestBody = new StringEntity(bodyContent);
			request.setEntity(requestBody);
			request.setHeader("Content-Type", "application/json");
			request.setHeader("Authorization", "key=AIzaSyZ-1u...0GBYzPu7Udno5aA");
			 HttpResponse response = httpclient.execute(request);
			 HttpEntity entity = response.getEntity();
			 String responseString = EntityUtils.toString(entity);
			// result.put("status", response.getStatusLine().getStatusCode());
			// result.put("bodyContent", new JSONObject(responseString));
		} catch ( Exception e) {
			e.printStackTrace();
		}
		// return result;
	}

	private ProspectCallLog fetchAndSetRecordingUrl( String callSid,  ProspectCallLog prospectCallLog) {
		try {
			 CallLog callLog = callLogRepository.findOneById(callSid);
			if (callLog != null && callLog.getCallLogMap() != null) {
				if (callLog.getCallLogMap().get("RecordingUrl") != null) {
					 String recordingUrl = callLog.getCallLogMap().get("RecordingUrl");
					prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
					 int lastIndex = recordingUrl.lastIndexOf("/");
					 String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
					 String rfileName = fileName + ".wav";
					 String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
				} else if (callLog.getCallLogMap().get("RecordUrl") != null) {
					 String recordingUrl = callLog.getCallLogMap().get("RecordUrl");
					prospectCallLog.getProspectCall().setRecordingUrl(recordingUrl);
					 int lastIndex = recordingUrl.lastIndexOf("/");
					 String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length() - 4);
					 String rfileName = fileName + ".wav";
					 String awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
					prospectCallLog.getProspectCall().setRecordingUrlAws(awsRecordingUrl);
				}
			}
		} catch ( Exception e) {
			logger.error("==========> AgentServiceImpl - Error occurred in fetchAndSetRecordingUrl <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
		return prospectCallLog;
	}

	private ProspectCallLog changeSliceAndDirectPhoneFlag(ProspectCallLog prospectCallLog) {
		try {
			if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
					&& (prospectCallLog.getProspectCall().getCallDuration() == 0
							|| prospectCallLog.getProspectCall().getCallDuration() > 0)
					&& (prospectCallLog.getProspectCall().getCallRetryCount() == 0
							|| prospectCallLog.getProspectCall().getCallRetryCount() > 0)) {
				if (prospectCallLog.getProspectCall().getSubStatus() != null && prospectCallLog.getProspectCall().getSubStatus()
						.equals(DialerCodeDispositionStatus.ANSWERMACHINE.name())
						&& prospectCallLog.getProspectCall().getCallDuration() > 10
						&& prospectCallLog.getProspectCall().getCallRetryCount() == 1
						&& prospectCallLog.getProspectCall().getDataSlice() != null
						&& prospectCallLog.getProspectCall().getDataSlice().equalsIgnoreCase("slice1")) {
					prospectCallLog.setDataSlice("slice10");
					prospectCallLog.getProspectCall().setDataSlice("slice10");
					prospectCallLog.getProspectCall().getProspect().setDirectPhone(false);
				}
			}
		} catch (Exception e) {
			logger.error("Error occurred in changeSliceAndDirectPhoneFlag.");
			e.printStackTrace();
		}
		return prospectCallLog;
	}

	// FIXME  have to make this package access to start integeration testing. Ideally 
	// 		  testing should begin upstream and this method can be private
	Optional<CallClassificationJobQueue> updateDirectPhoneFlagOrEnqueueClassificationJob(
			ProspectCallLog prospectCallLog) {

		Campaign campaign = campaignRepository.findOneById(prospectCallLog.getProspectCall().getCampaignId());
		if (!campaign.isUseDirectClassificationML()) {
			return Optional.empty();
		}

		// if any GlobalContact with phone number classified use that value
		// 	   and update all GlobalContacts
		String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
		List<GlobalContact> contacts = globalContactRepository.findByPhone(phone);
		Optional<GlobalContact> contactWithDirectClassification = contacts.stream()
											.filter(c -> c.isDirectClassificationML())
											.findFirst();
		contactWithDirectClassification.ifPresent(classifiedContact -> {
			List<GlobalContact> updatedContacts = contacts.stream()
				.filter(c2 -> !c2.isDirectClassificationML())
				.map(c3 -> {
					c3.setDirectPhone(classifiedContact.isDirectPhone()); 
					c3.setDirectClassificationML(true);
					return c3;
				}).collect(Collectors.toList());
			if (!updatedContacts.isEmpty()) {
				globalContactRepository.saveAll(updatedContacts);
			}
			prospectCallLog.getProspectCall().getProspect().setDirectPhone(classifiedContact.isDirectPhone());
		});

		Optional<CallClassificationJobQueue> enqueued = 
			contactWithDirectClassification.isPresent() ? Optional.empty() :
			// new CallClassificationJobQueue()
			Optional.of(callClassificationJobQueueRepository.save(
				new CallClassificationJobQueue()
				.withStatus(CallClassificationJobQueue.Status.QUEUED)
				.withMessageLifecycleId(UUID.randomUUID().toString())
				.withTtl(3)	//TODO externalize to configurtion
				.withProducer("AgentServiceImpl")
				.withProducerServerId(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
				.withNamespace(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
				.withJobCreationTime(new Date())
				.withProspectCallLogId(prospectCallLog.getId())
				.withProspectCallId(prospectCallLog.getProspectCall().getProspectCallId())
				.withRecordingUrl(prospectCallLog.getProspectCall().getRecordingUrlAws())
				.withCallDuration(prospectCallLog.getProspectCall().getCallDuration())
				.withPhone(phone)));

		if (logger.isDebugEnabled() && enqueued.isPresent()) {
			logger.debug("Direct/Indirect binary classification job for phone {} enqueued with message id {} and messageLifecycleId {}", phone, enqueued.get().getId(), enqueued.get().getMessageLifecycleId());
		} else if (logger.isDebugEnabled()) {
			String reason = contactWithDirectClassification.isPresent() ? 
								"Found classified GlobalContact" :
								"Found duplicate job queued";
			logger.debug("Direct/Indirect binary classification job for phone {} not enqueued because {}", phone,  reason);
		}

		return enqueued;
	}
	
	
	private void checkProspectQualifiedForTranscription(Campaign campaign, ProspectCallLog prospectCallLog) {
		if (campaign != null && campaign.isTranscribeFilter()) {
			try {
				boolean isProspectShouldTranscribed = false;
				if (campaign.getTranscribeCriteria() != null
						&& campaign.getTranscribeCriteria().getDispositionStatus() != null
						&& campaign.getTranscribeCriteria().getDispositionStatus().size() > 0 && prospectCallLog != null
						&& prospectCallLog.getProspectCall() != null
						&& campaign.getTranscribeCriteria().getDispositionStatus()
								.contains(prospectCallLog.getProspectCall().getDispositionStatus())) {
					isProspectShouldTranscribed = true;
				}

				if (campaign.getTranscribeCriteria() != null && campaign.getTranscribeCriteria().getSubStatus() != null
						&& campaign.getTranscribeCriteria().getSubStatus().size() > 0 && prospectCallLog != null
						&& prospectCallLog.getProspectCall() != null && campaign.getTranscribeCriteria().getSubStatus()
								.contains(prospectCallLog.getProspectCall().getSubStatus())) {
					isProspectShouldTranscribed = true;
				}

				boolean emailReveal = false;
				boolean directPhone = false;
				if (campaign.getTranscribeCriteria() != null
						&& campaign.getTranscribeCriteria().getEmailRevealed() != null
						&& !campaign.getTranscribeCriteria().getEmailRevealed().isEmpty()
						&& campaign.getTranscribeCriteria().getEmailRevealed().equalsIgnoreCase("YES")) {
					emailReveal = true;
				}

				if (emailReveal == prospectCallLog.getProspectCall().isEmailRevealed()) {
					isProspectShouldTranscribed = true;
				}

				if (campaign.getTranscribeCriteria() != null
						&& campaign.getTranscribeCriteria().getDirectPhone() != null
						&& !campaign.getTranscribeCriteria().getDirectPhone().isEmpty()
						&& campaign.getTranscribeCriteria().getDirectPhone().equalsIgnoreCase("YES")) {
					directPhone = true;
				}

				if (directPhone == prospectCallLog.getProspectCall().getProspect().isDirectPhone()) {
					isProspectShouldTranscribed = true;
				}

				if (isProspectShouldTranscribed) {
					if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
							&& prospectCallLog.getProspectCall().getRecordingUrlAws() != null) {
						callClassificationJobQueueRepository.save(new CallClassificationJobQueue()
								.withStatus(CallClassificationJobQueue.Status.TRANSCRIBE_QUEUED)
								.withMessageLifecycleId(UUID.randomUUID().toString()).withTtl(3)
								.withProducer("TranscriptionServiceImpl")
								.withProducerServerId(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
								.withNamespace(ApplicationEnvironmentPropertyUtils.getServerBaseUrl())
								.withJobCreationTime(new Date()).withCampaignId(campaign.getId())
								.withProspectCallId(prospectCallLog.getProspectCall().getProspectCallId())
								.withProspectCallLogId(prospectCallLog.getId())
								.withRecordingUrl(prospectCallLog.getProspectCall().getRecordingUrlAws())
								.withCallDuration(prospectCallLog.getProspectCall().getCallDuration())
								.withPhone(prospectCallLog.getProspectCall().getProspect().getPhone()));
					}
				}
			} catch (Exception e) {
				logger.error(new SplunkLoggingUtils("checkProspectQualifiedForTranscription", "")
						.eventDescription("Failed to check prospect qualified for transcription.")
						.addThrowableWithStacktrace(e).build());
			}
		}
	}

	@Override
	public CallbackProspectCacheDTO getMissedCallbackProspect(String agentId) {
		CallbackProspectCacheDTO callBackDTO = prospectCacheServiceImpl.getMissedCallbackRecord(agentId);
		if (callBackDTO != null) {
			AgentCampaignDTO agentCampaignDTO = getCurrentAllocatedCampaign(callBackDTO.getAgentId());
			if (agentCampaignDTO != null && agentCampaignDTO.getId().equalsIgnoreCase(callBackDTO.getCampaignId())) {
				StringBuffer prospectName = new StringBuffer();
				prospectName = prospectName.append(callBackDTO.getProspect().getFirstName()).append(" ")
						.append(callBackDTO.getProspect().getLastName());
				PusherUtils.pushMessageToUser(agentId, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
				String notice = " There is missed callback with " + prospectName;
				teamNoticeService.saveUserNotices(agentId, notice);
			} else {
//				AgentCall agentCall = agentRegistrationService.getAvailableAgent(callBackDTO.getCampaignId());
//				if (agentCall != null) {
//					
//				}
				callBackDTO = new CallbackProspectCacheDTO();
			}
		}
		return callBackDTO;
	}
	/**
	 * log call details for Agent wait time or call monitoring
	 * 
	 * @param AgentCallMonitoringDTO
	 */
	@Override
	public void agentCallMonitoring(AgentCallMonitoringDTO agentCallMonitoringDTO) {
		try {
			DecimalFormat df = new DecimalFormat("#.##");
			logger.info(new SplunkLoggingUtils("AgentCallMonitoring", agentCallMonitoringDTO.getCallSid())
					.eventDescription("Agent wait time and call monitoring").methodName("agentCallMonitoring")
					.processName("agentCallMonitoring").agentId(agentCallMonitoringDTO.getAgentId())
					.agentName(agentCallMonitoringDTO.getAgentName())
					.prospectCallId(agentCallMonitoringDTO.getProspectCallId())
					.callSid(agentCallMonitoringDTO.getCallSid())
					.organizationId(agentCallMonitoringDTO.getOrganization())
					.campaignId(agentCallMonitoringDTO.getCampaignId())
					.campaignName(agentCallMonitoringDTO.getCampaignName())
					.dialerMode(agentCallMonitoringDTO.getDialerMode()).agentType(agentCallMonitoringDTO.getAgentType())
					.agentWaitTime(df.format(agentCallMonitoringDTO.getAgentWaitTime()))
					.telcoDuration(df.format(agentCallMonitoringDTO.getTelcoDuration()))
					.dispositionTime(df.format(agentCallMonitoringDTO.getDispositionTime()))
					.prospectHandleTime(df.format(agentCallMonitoringDTO.getProspectHandleTime()))
					.dispositionStatus(agentCallMonitoringDTO.getDispositionStatus())
					.subStatus(agentCallMonitoringDTO.getSubStatus()).build());
		} catch (Exception e) {
			logger.error("Failed to log the agent call details");
		}
	}

	private void storeAgentsInMemory() {
		if (this.agentsInMemory == null || this.agentsInMemory.size() == 0) {
			List<Agent> agentsFromDB = agentRepository.findAll();
			if (agentsFromDB != null && agentsFromDB.size() > 0) {
				for (Agent agent : agentsFromDB) {
					agentsInMemory.put(agent.getId(), agent);
				}
			}
		}
	}

	@Override
	public Boolean clearAgentsInMemory() {
		this.agentsInMemory = new HashMap<String, Agent>();
		return true;
	}

}
