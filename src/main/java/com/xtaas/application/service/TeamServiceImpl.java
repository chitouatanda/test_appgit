package com.xtaas.application.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.QaRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.Qa;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.CampaignTeamInvitation;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.GeographyService;
import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.TeamDTO;
import com.xtaas.web.dto.TeamResourceDTO;
import com.xtaas.web.dto.TeamSearchDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;

@Service
public class TeamServiceImpl implements TeamService {

	private static final Logger logger = LoggerFactory.getLogger(TeamServiceImpl.class);

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private QaRepository qaRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private GeographyService geographyService;

	@Autowired
	private UserService userService;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByCriteria(TeamSearchDTO teamSearchDTO, Pageable pageRequest) {
		Campaign campaign = null;
		StringBuilder domain = new StringBuilder();
		int minimumAverageHoursAvailable = 0;
		if (teamSearchDTO.isIncludeDomain() || teamSearchDTO.isIncludeTeamsWithBandwidth()) {
			campaign = campaignRepository.findOneById(teamSearchDTO.getCampaignId());
			if (campaign == null) {
				throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
			}
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (teamSearchDTO.isIncludeDomain()) {
				domain.append(organization.getVertical()).append('|').append(campaign.getDomain());
			}
			if (teamSearchDTO.isIncludeTeamsWithBandwidth()) {
				minimumAverageHoursAvailable = campaign.getRequiredHours();
			}
		}

		List<String> countries = geographyService.getIncludedCountries(teamSearchDTO.getCountries(),
				teamSearchDTO.isExcludeCountries());
		List<String> states = geographyService.getIncludedStates(countries, teamSearchDTO.getStates(),
				teamSearchDTO.isExcludeStates());
		List<String> cities = geographyService.getIncludedCities(countries, states, teamSearchDTO.getCities(),
				teamSearchDTO.isExcludeCities());

		if ((countries == null || countries.size() == 0) && (states == null || states.size() == 0)
				&& (cities == null || cities.size() == 0)
				&& (teamSearchDTO.getTimeZones() == null || teamSearchDTO.getTimeZones().size() == 0)
				&& (teamSearchDTO.getLanguages() == null || teamSearchDTO.getLanguages().size() == 0)
				&& !teamSearchDTO.isIncludeDomain()) {
			return teamRepository.searchTeams(teamSearchDTO.getRatings(), teamSearchDTO.getMaximumPerLeadPrice(),
					teamSearchDTO.getMinimumCompletedCampaigns(), teamSearchDTO.getMinimumTotalVolume(),
					teamSearchDTO.getMinimumAverageConversion(), teamSearchDTO.getMinimumAverageQuality(),
					minimumAverageHoursAvailable, pageRequest);
		} else {

			return teamRepository.searchTeams(countries, states, cities, teamSearchDTO.getTimeZones(),
					teamSearchDTO.getLanguages(), teamSearchDTO.getRatings(), domain.toString(),
					teamSearchDTO.getMaximumPerLeadPrice(), teamSearchDTO.getMinimumCompletedCampaigns(),
					teamSearchDTO.getMinimumTotalVolume(), teamSearchDTO.getMinimumAverageConversion(),
					teamSearchDTO.getMinimumAverageQuality(), minimumAverageHoursAvailable, pageRequest);
		}
	}

	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<Object> countByCriteria(TeamSearchDTO teamSearchDTO) {
		StringBuilder domain = new StringBuilder();
		int minimumAverageHoursAvailable = 0;
		Campaign campaign = null;
		if (teamSearchDTO.isIncludeDomain() || teamSearchDTO.isIncludeTeamsWithBandwidth()) {
			campaign = campaignRepository.findOneById(teamSearchDTO.getCampaignId());
			if (campaign == null) {
				throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
			}
			Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
			if (teamSearchDTO.isIncludeDomain()) {
				domain.append(organization.getVertical()).append('|').append(campaign.getDomain());
			}
			if (teamSearchDTO.isIncludeTeamsWithBandwidth()) {
				minimumAverageHoursAvailable = campaign.getRequiredHours();
			}
		}

		List<String> countries = geographyService.getIncludedCountries(teamSearchDTO.getCountries(),
				teamSearchDTO.isExcludeCountries());
		List<String> states = geographyService.getIncludedStates(countries, teamSearchDTO.getStates(),
				teamSearchDTO.isExcludeStates());
		List<String> cities = geographyService.getIncludedCities(countries, states, teamSearchDTO.getCities(),
				teamSearchDTO.isExcludeCities());

		if ((countries == null || countries.size() == 0) && (states == null || states.size() == 0)
				&& (cities == null || cities.size() == 0)
				&& (teamSearchDTO.getTimeZones() == null || teamSearchDTO.getTimeZones().size() == 0)
				&& (teamSearchDTO.getLanguages() == null || teamSearchDTO.getLanguages().size() == 0)
				&& !teamSearchDTO.isIncludeDomain()) {
			return teamRepository.countTeams(teamSearchDTO.getRatings(), teamSearchDTO.getMaximumPerLeadPrice(),
					teamSearchDTO.getMinimumCompletedCampaigns(), teamSearchDTO.getMinimumTotalVolume(),
					teamSearchDTO.getMinimumAverageConversion(), teamSearchDTO.getMinimumAverageQuality(),
					minimumAverageHoursAvailable);
		} else {

			return teamRepository.countTeams(countries, states, cities, teamSearchDTO.getTimeZones(),
					teamSearchDTO.getLanguages(), teamSearchDTO.getRatings(), domain.toString(),
					teamSearchDTO.getMaximumPerLeadPrice(), teamSearchDTO.getMinimumCompletedCampaigns(),
					teamSearchDTO.getMinimumTotalVolume(), teamSearchDTO.getMinimumAverageConversion(),
					teamSearchDTO.getMinimumAverageQuality(), minimumAverageHoursAvailable);
		}

	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByPreviousCampaigns(String campaignId) {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		List<Campaign> campaigns = campaignRepository.findBySupervisorManagedTeam(userLogin.getUsername(), campaignId,
				PageRequest.of(0, 1, Direction.DESC, "createdDate"));
		List<String> teamIds = new ArrayList<String>();
		for (Campaign campaign : campaigns) {
			for (CampaignTeamInvitation teamInvitation : campaign.getTeamInvitations()) {
				teamIds.add(teamInvitation.getTeamId());
			}
		}
		return teamRepository.findByIds(teamIds);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByPreviousWorkedCampaigns(String campaignId, int count)
			throws ParseException {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		List<Object> campaignWithTeamObjects = campaignRepository
				.searchByPreviousWorkedCampaign(userLogin.getUsername(), campaignId, count);
		List<String> teamIds = new ArrayList<String>();
		for (Object campaignWithTeamObject : campaignWithTeamObjects) {
			LinkedHashMap<?, ?> campaignWithTeamMap = (LinkedHashMap<?, ?>) campaignWithTeamObject;
			teamIds.add((String) campaignWithTeamMap.get("_id"));
		}
		List<TeamSearchResultDTO> teams = teamRepository.findByIds(teamIds);
		for (TeamSearchResultDTO team : teams) {
			for (Object campaignWithTeamObject : campaignWithTeamObjects) {
				LinkedHashMap<?, ?> campaignWithTeamMap = (LinkedHashMap<?, ?>) campaignWithTeamObject;
				if (((String) campaignWithTeamMap.get("_id")).equals(team.getId())) {
					team.setCampaignName((String) campaignWithTeamMap.get("name"));
					ObjectId campaignId1 = (ObjectId) campaignWithTeamMap.get("campaignId");
					team.setCampaignId(campaignId1.toString());
					team.setCampaignCreatedDate((Date) campaignWithTeamMap.get("createdDate"));
					break;
				}
			}
		}
		return teams;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByAutosuggest(String campaignId, Pageable pageRequest) {
		StringBuilder domain = new StringBuilder();
		int minimumAverageHoursAvailable = 0;
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
		}
		Organization organization = organizationRepository.findOneById(campaign.getOrganizationId());
		domain.append(organization.getVertical()).append('|').append(campaign.getDomain());
		minimumAverageHoursAvailable = campaign.getRequiredHours();

		@SuppressWarnings("serial")
		List<Double> autoSuggestRatings = new ArrayList<Double>() {
			{
				add(3.5);
				add(4.0);
				add(4.5);
				add(5.0);
			}
		};

		// This is hardcoded search criteria based on campaign's domains with
		// teams with at least 20 completed campaigns
		// and 75 conversion rate and minimum ratings 3.5. This will be modified
		// in the future to consider analytics
		return teamRepository.searchTeams(null, null, null, null, null, autoSuggestRatings, domain.toString(), 0, 0, 0,
				0, 75, minimumAverageHoursAvailable, pageRequest);

	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByRating(String campaignId, Pageable pageRequest) {

		int minimumAverageHoursAvailable = 0;
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for domain and team with bandwidth search");
		}
		/*
		 * Organization organization =
		 * organizationRepository.findOneById(campaign.getOrganizationId());
		 * domain.append(organization.getVertical()).append('|').append(campaign.
		 * getDomain());
		 */
		minimumAverageHoursAvailable = campaign.getRequiredHours();

		@SuppressWarnings("serial")
		List<Double> bestRatings = new ArrayList<Double>() {
			{
				add(3.5);
				add(4.0);
				add(4.5);
				add(5.0);
			}
		};

		// This is hardcoded search criteria based on campaign's domains with
		// teams with at least 20 completed campaigns
		// and 0 conversion rate and minimum ratings 3.5. This will be modified
		// in the future to consider analytics
		return teamRepository.searchTeams(null, null, null, null, null, bestRatings, null, 0, 0, 0, 0, 0,
				minimumAverageHoursAvailable, pageRequest);

	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByCurrentInvitations(String campaignId, Pageable pageRequest) {
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for current invitation based search");
		} else if (campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Team invitations are only available for supervisor managed campaigns");
		}
		List<String> teamIds = new ArrayList<String>();
		for (CampaignTeamInvitation teamInvitation : campaign.getTeamInvitations()) {
			teamIds.add(teamInvitation.getTeamId());
		}
		List<TeamSearchResultDTO> results = teamRepository.findByIds(teamIds);
		for (TeamSearchResultDTO result : results) {
			for (CampaignTeamInvitation teamInvitation : campaign.getTeamInvitations()) {
				if (result.getTeamId().equals(teamInvitation.getTeamId())) {
					result.setInvitationDate(teamInvitation.getInvitationDate());
					break;
				}
			}
		}
		return results;
	}

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public List<TeamSearchResultDTO> searchByCurrentSelection(String campaignId) {
		Campaign campaign = null;
		campaign = campaignRepository.findOneById(campaignId);
		if (campaign == null) {
			throw new IllegalArgumentException("Campaign id is required for current invitation based search");
		} else if (campaign.isSelfManageTeam()) {
			throw new IllegalArgumentException("Team invitations are only available for supervisor managed campaigns");
		}
		List<String> teamIds = new ArrayList<String>();
		if (campaign.getTeam() != null) {
			teamIds.add(campaign.getTeam().getTeamId());
		}
		return teamRepository.findByIds(teamIds);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_USER })
	public Team getTeam(String teamId) {
		return teamRepository.findOneById(teamId);
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public void temporaryAllocateCampaign(String agentId, String campaignId) {
		List<Team> teamList = new ArrayList<>();
		// Assign all agents to the campaign.
		if (agentId != null && agentId.equalsIgnoreCase("assignAllAgents")) {
			teamList = teamRepository.findByAdminSupervisorAndSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
			allocateCampaignToAgents(teamList, agentId, campaignId);
		} else {
			// Assign single agent to campaign.
			// teamList = teamRepository.findAllTeamsByAgentId(agentId);
			teamList = teamRepository.findByAdminSupervisorAndSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
			allocateCampaignToAgents(teamList, agentId, campaignId);
		}
	}

	private void allocateCampaignToAgents(List<Team> teamList, String agentId, String campaignId) {
		if (teamList != null && teamList.size() > 0) {
			for (Team team : teamList) {
				boolean isTeamModified = false;
				Map<String, AgentStatus> agentStatusMap = new HashMap<String, AgentStatus>();
				List<String> agentIds = team.getAgents().stream().map(agent -> agent.getResourceId())
						.collect(Collectors.toList());
				List<Agent> agentsFromDB = agentRepository.findAgentByIds(agentIds);
				// If Agent status is ACTIVE in database then only assign campaign to that
				// agent.
				if (agentsFromDB != null) {
					for (Agent agent : agentsFromDB) {
						if (agent != null) {
							agentStatusMap.put(agent.getId(), agent.getStatus());
						}
					}
				}
				if (agentId.equalsIgnoreCase("assignAllAgents")) {
					for (TeamResource agent : team.getAgents()) {
						AgentStatus status = agentStatusMap.get(agent.getResourceId());
						if (status != null && status.equals(AgentStatus.ACTIVE)) {
							agent.setSupervisorOverrideCampaignId(campaignId);
							isTeamModified = true;
						}
					}
				} else {
					for (TeamResource agent : team.getAgents()) {
						AgentStatus status = agentStatusMap.get(agent.getResourceId());
						if (agent.getResourceId().equalsIgnoreCase(agentId) && status != null
								&& status.equals(AgentStatus.ACTIVE)) {
							agent.setSupervisorOverrideCampaignId(campaignId);
							isTeamModified = true;
						}
					}
				}
				if (isTeamModified) {
					teamRepository.save(team);
				}
			}
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public void undoTemporaryAllocation(String agentId, String campaignId) {
		List<Team> teamList = new ArrayList<>();
		if (agentId.equalsIgnoreCase("unAssignAllAgents")) {
			teamList = teamRepository.findByAdminSupervisorAndSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
			unallocateAgentsCampaign(teamList, agentId, campaignId);
		} else {
			// teamList = teamRepository.findAllTeamsByAgentId(agentId);
			teamList = teamRepository.findByAdminSupervisorAndSupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
			unallocateAgentsCampaign(teamList, agentId, campaignId);
		}
	}

	private void unallocateAgentsCampaign(List<Team> teamList, String agentId, String campaignId) {
		for (Team team : teamList) {
			boolean isTeamModified = false;
			if (agentId.equalsIgnoreCase("unAssignAllAgents")) {
				for (TeamResource agent : team.getAgents()) {
					if (agent.getSupervisorOverrideCampaignId() != null) {
						if (agent.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
							agent.setSupervisorOverrideCampaignId(null);
							isTeamModified = true;
						}
					}
				}
			} else {
				for (TeamResource agent : team.getAgents()) {
					if (agent.getResourceId().equalsIgnoreCase(agentId)
							&& agent.getSupervisorOverrideCampaignId() != null) {
						if (agent.getSupervisorOverrideCampaignId().equalsIgnoreCase(campaignId)) {
							agent.setSupervisorOverrideCampaignId(null);
							isTeamModified = true;
						}
					}
				}
			}
			if (isTeamModified) {
				teamRepository.save(team);
			}
		}
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SUPERVISOR })
	public void undoTemporaryAllocation(String agentId) {
		List<Team> teamList = teamRepository.findBySupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
		if (teamList.isEmpty()) {
			throw new IllegalArgumentException("No team is available for this supervisior");
		}
		if (teamList.size() > 1) {
			throw new IllegalArgumentException("More than one team is available for this supervisior");
		}
		Team team = teamList.get(0);
			for (TeamResource agent : team.getAgents()) {
				if (agentId.equalsIgnoreCase(agent.getResourceId())) {
					agent.setSupervisorOverrideCampaignId(null);
				}
			}
		
		teamRepository.save(team);
	}

	@Override
	// @Authorize(roles = {Roles.ROLE_SUPERVISOR,Roles.ROLE_PARTNER})
	public TeamDTO createTeam(TeamDTO teamDTO) {
		if (teamDTO.getSupervisor() == null) {
			throw new IllegalArgumentException("Supervisor of team is required");
		}
		// Check supervisor is already part of some other team
		List<Team> listOfTeamBySupervisorId = teamRepository
				.findBySupervisorId(teamDTO.getSupervisor().getResourceId());
		if (!listOfTeamBySupervisorId.isEmpty()) {
			throw new IllegalArgumentException(
					"Supervisor " + teamDTO.getSupervisor().getResourceId() + " is already part of some other team");
		}

		if (teamDTO.getAddress() == null) {
			throw new IllegalArgumentException("Address of team is required");
		}
		Team team = new Team(teamDTO.getPartnerId(), new TeamResource(teamDTO.getSupervisor().getResourceId()),
				teamDTO.getName(), teamDTO.getAddress().toAddress());
		team.setOverview(teamDTO.getOverview());
		team.setPerLeadPrice(teamDTO.getPerLeadPrice());
		team.setAdminSupervisorIds(teamDTO.getAdminSupervisorIds());
		// allocate agent to a team
		List<TeamResourceDTO> agentList = teamDTO.getAgents();
		if (agentList.isEmpty()) {
			throw new IllegalArgumentException("Atleast one Agent must be allocated to the team");
		}

		List<String> agentIds = addIds(agentList);
		List<Agent> agents = agentRepository.findAgentByIds(agentIds);
		if (agents.isEmpty()) {
			throw new IllegalArgumentException("None of the specified Agents exist in the system");
		}
		if (agentIds.size() != agents.size()) {
			List<String> availableAgentIds = new ArrayList<String>();
			for (Agent agent : agents) {
				availableAgentIds.add(agent.getId());
			}
			List<String> unavailableAgentIds = new ArrayList<String>();
			for (String agentId : agentIds) {
				if (!availableAgentIds.contains(agentId)) {
					unavailableAgentIds.add(agentId);
				}
			}
			throw new IllegalArgumentException(unavailableAgentIds + " Agents does not exist in the system");
		}
		for (Agent agent : agents) {
			// DATE:28/02/2018 REASON:commented below code allow insert agent in two teams.
			// (for automation task - login, agent, team, etc. insertion through file).
			// Check Agent is already part of some other team
			/*
			 * Team teamByAgent = teamRepository.findByAgent(agent.getId()); if(teamByAgent
			 * != null) { throw new IllegalArgumentException("Agent " +agent.getId()+
			 * " is already part of some other team"); }
			 */
			team.addAgent(convertAgentToTeamResource(agent));
		}
		// Allocate QA to a team
		List<TeamResourceDTO> qaList = teamDTO.getQas();
		if (qaList.isEmpty()) {
			throw new IllegalArgumentException("Atleast one QA must be allocated to the team");
		}
		List<String> qaIds = addIds(qaList);
		List<Qa> Qas = qaRepository.findQaByIds(qaIds);
		if (Qas.isEmpty()) {
			throw new IllegalArgumentException("None of the specified QA exist in the system");
		}
		if (qaIds.size() != Qas.size()) {
			List<String> availableQaIds = new ArrayList<String>();
			for (Qa qa : Qas) {
				availableQaIds.add(qa.getId());
			}
			List<String> unavailableQaIds = new ArrayList<String>();
			for (String qaId : qaIds) {
				if (!availableQaIds.contains(qaId)) {
					unavailableQaIds.add(qaId);
				}
			}
			throw new IllegalArgumentException(unavailableQaIds + " Qas does not exist in the system");
		}
		for (Qa qa : Qas) {
			team.addQA(convertQaToTeamResource(qa));
		}
		return new TeamDTO(teamRepository.save(team));
	}

	public List<String> addIds(List<TeamResourceDTO> resourceList) {
		List<String> resourceIds = new ArrayList<String>();
		for (TeamResourceDTO teamResourceDTO : resourceList) {
			resourceIds.add(teamResourceDTO.toTeamResource().getResourceId());
		}
		return resourceIds;
	}

	private TeamResource convertAgentToTeamResource(Agent agent) {
		TeamResource teamResource = new TeamResource(agent.getId());
		teamResource.setAddress(agent.getAddress());
		List<String> domains = agent.getDomains();
		for (String domain : domains) {
			teamResource.addDomain(domain);
		}
		List<String> languages = agent.getLanguages();
		for (String language : languages) {
			teamResource.addLanguage(language);
		}
		List<WorkSchedule> workSchedules = agent.getWorkSchedules();
		for (WorkSchedule workSchedule : workSchedules) {
			teamResource.addWorkSchedule(workSchedule);
		}
		return teamResource;
	}

	public TeamResource convertQaToTeamResource(Qa qa) {
		TeamResource teamResource = new TeamResource(qa.getId());
		teamResource.setAddress(qa.getAddress());
		List<String> domains = qa.getDomains();
		for (String domain : domains) {
			teamResource.addDomain(domain);
		}
		List<String> languages = qa.getLanguages();
		for (String language : languages) {
			teamResource.addLanguage(language);
		}
		List<WorkSchedule> workSchedules = qa.getWorkSchedules();
		for (WorkSchedule workSchedule : workSchedules) {
			teamResource.addWorkSchedule(workSchedule);
		}
		return teamResource;
	}

	@Override
	public TeamDTO updateTeam(String teamId, TeamDTO teamDTO) {
		Team team = teamRepository.findOneById(teamId);
		team.setSupervisor(teamDTO.getSupervisor() != null ? teamDTO.getSupervisor().toTeamResource() : null);
		team.setOverview(teamDTO.getOverview());
		team.setPerLeadPrice(teamDTO.getPerLeadPrice());
		team.setAddress(teamDTO.getAddress() != null ? teamDTO.getAddress().toAddress() : null);
		return new TeamDTO(teamRepository.save(team));
	}

	@Override
	public void addAgent(String teamId, String agentId) {
		Team team = teamRepository.findOneById(teamId);
		if (team == null) {
			throw new IllegalArgumentException(teamId + " this team is not found");
		}
		List<TeamResource> allocatedAgents = team.getAgents();
		boolean isAllocated = true;
		if (!allocatedAgents.isEmpty()) {
			isAllocated = isResourceAvailable(allocatedAgents, agentId);
		} else {
			isAllocated = false;
		}
		if (isAllocated) {
			logger.debug("addAgent() : " + agentId + " is already allocated agent to a team");
		} else {
			// DATE:28/02/2018 REASON:commented below code allow insert agent in two teams.
			// (for automation task - login, agent, team, etc. insertion through file).
			// Check Agent is already part of some other team
			/*
			 * Team teamByAgent = teamRepository.findOtherTeamByAgent(agentId, teamId);
			 * if(teamByAgent != null) { throw new IllegalArgumentException("Agent "
			 * +agentId+ " is already part of some other team"); }
			 */
			Agent agent = agentRepository.findOneById(agentId);
			if (agent == null) {
				throw new IllegalArgumentException(agentId + " this agent is not found");
			}
			team.addAgent(convertAgentToTeamResource(agent));
		}
		Team teamDB = teamRepository.save(team);
		logger.debug("Added agent [{}] in team [{}]", agentId, teamDB.getName());
	}

	@Override
	public void removeAgent(String teamId, String agentId) {
		Team team = teamRepository.findOneById(teamId);
		if (team == null) {
			throw new IllegalArgumentException(teamId + " this team is not found");
		}
		List<TeamResource> allocatedAgents = team.getAgents();
		boolean isAllocated = true;
		if (!allocatedAgents.isEmpty()) {
			isAllocated = isResourceAvailable(allocatedAgents, agentId);
		}
		if (!isAllocated) {
			logger.debug("removeAgent() : " + agentId + "is not allocated Agent to a team");
		} else {
			team.removeAgent(agentId);
		}
		Team teamDB = teamRepository.save(team);
		logger.debug("Removed agent [{}] from team [{}]", agentId, teamDB.getName());
	}

	@Override
	public void addQa(String teamId, String qaId) {
		Team team = teamRepository.findOneById(teamId);
		if (team == null) {
			throw new IllegalArgumentException(teamId + " this team is not found");
		}
		List<TeamResource> availableQas = team.getQas();
		boolean isAllocated = true;
		if (!availableQas.isEmpty()) {
			isAllocated = isResourceAvailable(availableQas, qaId);
		} else {
			isAllocated = false;
		}
		if (isAllocated) {
			logger.debug("addQa() : " + qaId + "is already allocated Qa to a team");
		} else {
			Qa qa = qaRepository.findOneById(qaId);
			team.addQA(convertQaToTeamResource(qa));
		}
		Team teamDB = teamRepository.save(team);
		logger.debug("Added agent [{}] in team [{}]", qaId, teamDB.getName());
	}

	@Override
	public void removeQa(String teamId, String qaId) {
		Team team = teamRepository.findOneById(teamId);
		if (team == null) {
			throw new IllegalArgumentException(teamId + " this team is not available");
		}
		List<TeamResource> allocatedQas = team.getQas();
		boolean isAllocated = true;
		if (!allocatedQas.isEmpty()) {
			isAllocated = isResourceAvailable(allocatedQas, qaId);
		}
		if (!isAllocated) {
			logger.debug("removeQa() : " + qaId + "is not allocated Qa to a team");
		} else {
			team.removeQA(qaId);
		}
		Team teamDB = teamRepository.save(team);
		logger.debug("Removed agent [{}] from team [{}]", qaId, teamDB.getName());
	}

	public boolean isResourceAvailable(List<TeamResource> availableResource, String resourceId) {
		for (TeamResource availableteamResource : availableResource) {
			if (availableteamResource.getResourceId().equals(resourceId)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<TeamSearchResultDTO> searchByQa() {
		List<TeamSearchResultDTO> teamSearchResults = teamRepository
				.findByQaId(XtaasUserUtils.getCurrentLoggedInUsername());
		for (TeamSearchResultDTO teamSearchResult : teamSearchResults) {// Set below sensitive information null, should
																		// not be send to QA screen . Just sending Id,
																		// name of team
			teamSearchResult.setInvitationDate(null);
			teamSearchResult.setOverview(null);
			teamSearchResult.setAverageAvailableHours(null);
			teamSearchResult.setAverageConversion(null);
			teamSearchResult.setAverageQuality(null);
			teamSearchResult.setPerLeadPrice(null);
			teamSearchResult.setTeamSize(null);
			teamSearchResult.setTotalCompletedCampaigns(null);
			teamSearchResult.setTotalVolume(null);
			teamSearchResult.setAverageRating(null);
		}
		return teamSearchResults;
	}
}