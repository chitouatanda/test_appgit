package com.xtaas.application.service;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.Login;
import com.xtaas.exception.UnauthorizedAccessException;
import com.xtaas.infra.security.AppManager;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.service.UserService;
import com.xtaas.utils.EncryptionUtils;

@Service
public class AuthTokenGeneratorServiceImpl implements AuthTokenGeneratorService {

	@Autowired
	private UserService userService;

	/**
	 * Generates token in the format appname:data:hash where, appname is the name of
	 * the application which is requesting the token data is two-way encrypted data
	 * using the app's secret key. Data is in format username|org|role|time in MS
	 * hash is one-way encryption of data using the app's secret key
	 * 
	 * @return token
	 */
	@Override
	@Authorize(roles = { Roles.ROLE_USER })
	public String generateUserToken(String appName, String userName) {
		Login user = userService.getUser(userName);
		// generate authdata in format username|org|role|time in MS
		return generateToken(appName, userName, user.getOrganization(),
				StringUtils.join(user.getRoles().toArray(new String[0])));
	}

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_CAMPAIGN_MANAGER })
	public String generateSystemToken(String appName, String systemUser) {
		// generate authdata in format username|org|role|time in MS
		return generateToken(appName, systemUser, "XTAAS", Roles.ROLE_SYSTEM.name());
	}

	private String generateToken(String appName, String userName, String organization, String roles) {
		// generate authdata in format username|org|role|time in MS
		StringBuilder authDataBuilder = new StringBuilder();
		authDataBuilder.append(userName).append("|"); // user
		authDataBuilder.append(organization).append("|"); // org
		authDataBuilder.append(roles).append("|"); // roles (comma separated list)
		authDataBuilder.append(System.currentTimeMillis()); // time in MS when token was generated
		String authData = authDataBuilder.toString();

		// generate token in the format appname:data:hash
		String appSecurityKey = AppManager.getSecurityKey(EnumUtils.getEnum(App.class, appName));
		return appName + ":" + EncryptionUtils.encrypt(authData, appSecurityKey) + ":"
				+ EncryptionUtils.generateHash(authData, appSecurityKey);
	}

	/**
	 * 1. find user if does not exist then throw UnauthorizedAccessException. 2. if
	 * user exists and password is correct then send token else throw
	 * UnauthorizedAccessException.
	 * 
	 * @return String token
	 */
	@Override
	public String generateClientToken(String username, String password) {
		String token = "";
		Login user = userService.getUser(username);
		if (user != null) {
			String salt = password + "{" + username + "}";
			String passwordHash = EncryptionUtils.generateMd5Hash(salt);
			if (passwordHash.equalsIgnoreCase(user.getPassword())) {
				token = generateToken(AppManager.App.CLIENT.name(), username, user.getOrganization(),
						StringUtils.join(user.getRoles().toArray(new String[0])));
			} else {
				throw new UnauthorizedAccessException("Username or password is not valid.");
			}
		} else {
			throw new UnauthorizedAccessException("Username or password is not valid.");
		}
		return token;
	}

}
