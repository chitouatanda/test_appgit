package com.xtaas.application.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.xtaas.web.dto.ProspectCallLogDTO;
import com.xtaas.web.dto.RestCampaignContactDTO;
import com.xtaas.web.dto.RestCdpCampaignContactDTO;
import com.xtaas.web.dto.RestProspectResponseStatusDTO;

public interface RestCampaignContactService {

	public List<String> uploadRestData(List<RestCampaignContactDTO> campaignContactDTOs);
	
	public RestProspectResponseStatusDTO uploadData(List<RestCampaignContactDTO> campaignContactDTOs, String sfdcCampaignId);
	
	public ResponseEntity<String> uploadInternalData(RestCdpCampaignContactDTO restCdpCampaignContactDTO);
	
	public ProspectCallLogDTO updateCampaignAudience(RestCampaignContactDTO restCampaignDto);
}
