package com.xtaas.application.service;

import com.xtaas.web.dto.TelnyxCallStatusDTO;

public interface CallService {

	public void setCallAsComplete(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall);

	public void setCallAsCompleteNew(String agentId, String callSid, String callControlProvider, boolean isConferenceCall);

	public boolean placeCallOnHold(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall);

	public boolean unHoldCall(String agentId, String callSid, boolean usePlivo, boolean isConferenceCall);

	public void updateCallWithStatusCallbackUrl(String callSid);

	public boolean playDTMF(String callSid, String digit);

	public TelnyxCallStatusDTO getTelnyxCallStatus(String callLegId);
	
	public void notifyAgent(String callSid);
}
