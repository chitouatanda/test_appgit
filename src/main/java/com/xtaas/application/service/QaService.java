package com.xtaas.application.service;


import java.text.ParseException;
import java.util.List;

import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.QaDetails;
import com.xtaas.web.dto.ProspectCallQaSearchResult;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.QaDTO;
import com.xtaas.web.dto.QaFeedbackDTO;

public interface QaService {
	public QaDTO createQa(QaDTO qaDTO);
	public QaDTO updateQa(String qaId, QaDTO qaDTO);
	public boolean createFeedback(String prospectCallId, String secondaryQaReview, QaFeedbackDTO qaFeedbackResponseDTO, boolean finalSubmit);
	public ProspectCallQaSearchResult searchProspectCallsByQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	public ProspectCallQaSearchResult searchProspectCallsByAgent(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	public QaDetails getProspectCallForQA(String prospectCallId);
	public List<String> getTeamIdsByQa();
	public List<String> getCampaignIdsByQa();
	public List<AgentSearchResult> getAgentsForProspectCallsByQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	//Added method for qafilter
	public List<String> getCampaignIds(List<String> teamIds, String clientId);
	public List<AgentSearchResult> getAgentsForProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	public ProspectCallQaSearchResult searchProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO)	throws ParseException;
	public List<String> getAgentIdsByQa();	//get agentId for primaryqa

	// added to send record to primary qa from secondary qa.
	public boolean returnToPrimary(String prospectCallId, QaFeedbackDTO qaFeedbackResponseDTO);
	public List<Team> getOrganizationTeams(String organizationId);
	public List<String> getAgentIds();
	public ProspectCallQaSearchResult getQADetails(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	public ProspectCallQaSearchResult getSecQADetails(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

}
