package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityCallStatus;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentMetricsDTO;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.CallStatusDTO.CallState;

@Service
public class AgentMetricsServiceImpl implements AgentMetricsService {
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private AgentRegistrationService agentRegistrationService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private AgentActivityLogRepository agentActivityLogRepository;
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Autowired
	private UserService userService;
	
	public enum AgentMetrics {
		CONNECTED_WITH_PROSPECT_NAME,
		CONNECTED_WITH_PROSPECT_COMPANY,
		AGENT_CALL_STATE,
		CALL_START_TIME,
		LAST_LOGIN_TIME,
		AVG_TALK_TIME,
		CONTACT_RATE,
		SUCCESS_RATE,
		LEADS_DELIVERED,
		ONLINE_DURATION,
		AWAY_DURATION,
		ONBREAK_DURATION,
		IDLE_DURATION,
		OFFLINE_DURATION,
		GEARING_DURATION,//TODO: till here these all are OLD metrics...need to get rid off
		LAST_STATUS_DURATION,
		AGENT_CALL_STATUS,
		AGENT_CALL_STATUS_DURATION,
		CONNECTED_WITH_PROSPECT_PHONE,
		CONNECTED_WITH_PCID,
		CONNECTED_WITH_PISID,
		AGENT_LEAD_DURATION;
	}
	
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public List<AgentMetricsDTO> getAgentMetricsByCampaign(String campaignId, Date fromDate, Date toDate, HashMap<AgentMetrics, String> metrics) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		List<String> agentsOnCampaign = campaignService.getAllocatedAgents(campaignId);
		
		ArrayList<AgentMetricsDTO> agentMetricsList = new ArrayList<AgentMetricsDTO>(agentsOnCampaign.size());
		for (String agentId : agentsOnCampaign) {
			Login agentProfileFromDB = loginRepository.findOneById(agentId); //TODO: GET THE LOGIN OBJECT FROM CACHE.
			AgentMetricsDTO agentMetricsDTO = new AgentMetricsDTO();
			agentMetricsDTO.setAgentId(agentId);
			agentMetricsDTO.setAgentName(agentProfileFromDB.getFirstName() + " " + agentProfileFromDB.getLastName());
			HashMap<AgentMetrics, String> agentMetrics = new HashMap<AgentMetrics, String>(metrics);
			agentMetricsDTO.setMetrics(agentMetrics);
			agentMetricsDTO.setCampaignId(campaignId);
			agentMetricsDTO.setCampaignName(campaign.getName());
			
			List<AgentActivityLog> agentLastStatusList = agentActivityLogRepository.getLastStatus(agentId, PageRequest.of(0, 1, Direction.DESC, "createdDate")); //TODO: Put this method in service layer
			if (agentLastStatusList != null && agentLastStatusList.size() > 0) {
				AgentActivityLog agentLastStatus = agentLastStatusList.get(0);
				agentMetricsDTO.setAgentStatus(Enum.valueOf(AgentStatus.class, agentLastStatus.getStatus()));
			} else {
				agentMetricsDTO.setAgentStatus(AgentStatus.OFFLINE);
			}
			
			String prospectLastLoginTime = "";
			List<AgentActivityLog> agentLastLoginTimeList = agentActivityLogRepository.getLastLoginTime(agentId, PageRequest.of(0, 1, Direction.DESC, "createdDate")); //TODO: Put this method in service layer
			if (agentLastLoginTimeList != null && agentLastLoginTimeList.size() > 0) {
				AgentActivityLog agentLastLoginTime = agentLastLoginTimeList.get(0);
				prospectLastLoginTime = agentLastLoginTime.getCreatedDate().getTime() + "";
			}
			agentMetrics.put(AgentMetrics.LAST_LOGIN_TIME, prospectLastLoginTime);

			List<AgentSearchResult> agentSearchResult = prospectCallLogRepository.getAgentCallStats(Arrays.asList(agentId), fromDate, toDate);
			if (agentSearchResult != null && !agentSearchResult.isEmpty()) {
				agentMetrics.put(AgentMetrics.CONTACT_RATE, String.valueOf(agentSearchResult.get(0).getContactsMade()));
				agentMetrics.put(AgentMetrics.LEADS_DELIVERED, String.valueOf(agentSearchResult.get(0).getLeadsDelivered()));
				double successRate = (agentSearchResult.get(0).getLeadsDelivered()/(double)agentSearchResult.get(0).getContactsMade())*100; //calculating %
				agentMetrics.put(AgentMetrics.SUCCESS_RATE, String.valueOf(successRate));
				agentMetrics.put(AgentMetrics.AVG_TALK_TIME, String.valueOf(agentSearchResult.get(0).getAverageTalkTime()));
			}
			List<AgentActivityLog> agentStatusesWithinDatesList = agentActivityLogRepository.getStatusesWithinDateRange(agentId, fromDate, toDate, Sort.by(Direction.ASC, "createdDate")); 
			HashMap<AgentMetrics, String> statusDuration = calculateAgentStatusDuration(agentStatusesWithinDatesList);
			agentMetrics.putAll(statusDuration);
			agentMetricsList.add(agentMetricsDTO);
		} //end for
		
		return agentMetricsList;
	}

	private HashMap<AgentMetrics, String> calculateAgentStatusDuration(List<AgentActivityLog> agentStatusesWithinDatesList) {
		HashMap<AgentMetrics, String> statusDuration = new HashMap<AgentMetrics, String>();
		if (agentStatusesWithinDatesList != null) {
			for (int i=1; i<agentStatusesWithinDatesList.size(); i++) {
				AgentActivityLog thisAgentActivityLog = agentStatusesWithinDatesList.get(i-1);
				AgentActivityLog nextAgentActivityLog = agentStatusesWithinDatesList.get(i);
				AgentMetrics agentMetric = Enum.valueOf(AgentMetrics.class, thisAgentActivityLog.getStatus()+"_DURATION");
				long thisStatusDuration = nextAgentActivityLog.getCreatedDate().getTime() - thisAgentActivityLog.getCreatedDate().getTime();
				if (statusDuration.containsKey(agentMetric)) {
					long existingStatusDuration = Long.valueOf(statusDuration.get(agentMetric));
					thisStatusDuration = thisStatusDuration + existingStatusDuration;
				}
				statusDuration.put(agentMetric, thisStatusDuration+""); //duration in MILLIS
			}
		}
		return statusDuration;
	}
	
	@Override
	@Authorize(roles = {Roles.ROLE_AGENT})
	public AgentMetricsDTO getAgentMetrics(String agentId, CallStatusDTO callStatusDTO) {
		HashMap<AgentMetrics, String> metrics = new HashMap<AgentMetrics, String>();
		metrics.put(AgentMetrics.CONNECTED_WITH_PROSPECT_NAME, null);
		metrics.put(AgentMetrics.CONNECTED_WITH_PROSPECT_COMPANY, null);
		metrics.put(AgentMetrics.CALL_START_TIME, null);
		metrics.put(AgentMetrics.AGENT_CALL_STATE, null);
		
		Login agentProfileFromDB = loginRepository.findOneById(agentId); //TODO: GET THE LOGIN OBJECT FROM CACHE.
		AgentMetricsDTO agentMetricsDTO = new AgentMetricsDTO();
		agentMetricsDTO.setAgentId(agentId);
		agentMetricsDTO.setAgentName(agentProfileFromDB.getFirstName() + " " + agentProfileFromDB.getLastName());
		agentMetricsDTO.setMetrics(metrics);
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		if (agentCall != null) {
			if (agentCall.getCurrentCampaign() != null) {
				agentMetricsDTO.setCampaignId(agentCall.getCampaignId());
				agentMetricsDTO.setCampaignName(agentCall.getCurrentCampaign().getName());
			} else {
				agentMetricsDTO.setCampaignId(null);
				agentMetricsDTO.setCampaignName(null);
			}
			agentMetricsDTO.setAgentStatus(agentCall.getCurrentStatus());
			agentMetricsDTO.setSupervisorApproved(agentCall.isSupervisorApproved());
			
			for (AgentMetrics metric : metrics.keySet()) {
				switch(metric) {
					case CONNECTED_WITH_PROSPECT_NAME :
						if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspect() != null) {
							String prospectName = agentCall.getProspectCall().getProspect().getName();
							metrics.put(AgentMetrics.CONNECTED_WITH_PROSPECT_NAME, prospectName);
						}
						break;
					case CONNECTED_WITH_PROSPECT_COMPANY :
						if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspect() != null) {
							String prospectCompany = agentCall.getProspectCall().getProspect().getCompany();
							metrics.put(AgentMetrics.CONNECTED_WITH_PROSPECT_COMPANY, prospectCompany);
						}
						break;
					case CALL_START_TIME :
						String prospectCallStartTime = null;
						if (callStatusDTO != null && callStatusDTO.getCallStartTime() != null) {
							prospectCallStartTime = callStatusDTO.getCallStartTime().getTime() + "";
						} else if (agentCall.getProspectCall() != null) {
							prospectCallStartTime = agentCall.getProspectCall().getCallStartTime().getTime() + "";
						}
						metrics.put(AgentMetrics.CALL_START_TIME, prospectCallStartTime);
						break;
					case AGENT_CALL_STATE :
						if (callStatusDTO != null) { //callStatus may be when Call is CONNECTED / DISCONNECTED
							if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspect() != null) { //in that case there must be a valid prospect available
								metrics.put(AgentMetrics.AGENT_CALL_STATE, callStatusDTO.getCallState().name()); //if so then only set the AGENT_STATE which is either CONNECTED or DISCONNECTED. If this is DISCONNECTED and ProspectCall != null then supervisor UI will show (Wrapping Up)
							}
						} else if (agentCall.getProspectCall() != null) {
							metrics.put(AgentMetrics.AGENT_CALL_STATE, CallState.CONNECTED.name()); //TODO: don't know if Call is actually connected, it is possible that agent is in WRAPPING UP stage. Need to store callstate in AgentCall whenever changeCallStatus() is invoked
						} else {
							metrics.put(AgentMetrics.AGENT_CALL_STATE, CallState.DISCONNECTED.name());
						}
						break;
				}
			} //end for
		} else {
			//get everything from DB
			AgentCampaignDTO agentCampaignDTO;
			if (XtaasUserUtils.getCurrentLoggedInUsername().equals(agentId)) {
				agentCampaignDTO = agentService.getCurrentAllocatedCampaign();
			} else {
				agentCampaignDTO = agentService.getCurrentAllocatedCampaign(agentId); //it should go in this condition, only when the request is from supervisor
			}
			
			if (agentCampaignDTO != null && agentCampaignDTO.getId() != null) {
				agentMetricsDTO.setCampaignId(agentCampaignDTO.getId());
				agentMetricsDTO.setCampaignName(agentCampaignDTO.getName());
			} else {
				agentMetricsDTO.setCampaignId(null);
				agentMetricsDTO.setCampaignName(null);
			}
			agentMetricsDTO.setAgentStatus(AgentStatus.OFFLINE); //if agent is not in agentRegistrationService then he is OFFLINE
		} //end if
		return agentMetricsDTO;
	}
	
	private AgentMetricsDTO getAgentMetrics(String agentId, String timeZone) {
		Interval timeInterval = XtaasDateUtils.getDateTimeInterval(new Date(), timeZone);
		List<AgentActivityLog> agentLastStatusList = agentActivityLogRepository.getLastStatus(agentId,new Date(timeInterval.getStartMillis()), new Date(timeInterval.getEndMillis()), PageRequest.of(0, 1, Direction.DESC, "createdDate"));
		AgentMetricsDTO agentMetricsDTO = new AgentMetricsDTO();
		
		Agent agent = agentService.getAgent(agentId);
		agentMetricsDTO.setAgentId(agent.getId());
		agentMetricsDTO.setAgentName(agent.getName());
		agentMetricsDTO.setPartnerId(agent.getPartnerId());		// DATE:24/11/2017		REASON:setting partnerID of the agent for agent tab.
		agentMetricsDTO.setAgentType(agent.getAgentType());
		AgentCampaignDTO campaign = agentService.getCurrentAllocatedCampaign(agent.getId());
		if (campaign != null) {
			agentMetricsDTO.setCampaignId(campaign.getId());
			agentMetricsDTO.setCampaignName(campaign.getName());
		} else {
			agentMetricsDTO.setCampaignId(null);
			agentMetricsDTO.setCampaignName(null);
		}
		
		if (agentLastStatusList != null && agentLastStatusList.size() > 0) {
			AgentActivityLog agentLastActivityLog = agentLastStatusList.get(0);
			agentMetricsDTO.setAgentStatus(Enum.valueOf(AgentStatus.class, agentLastActivityLog.getStatus()));
			agentMetricsDTO.setMetrics(getMetrics(agentLastActivityLog));
		} else {
			agentMetricsDTO.setAgentStatus(AgentStatus.OFFLINE);
		}
		if (campaign != null && campaign.isSupervisorAgentStatusApprove()) {
			agentMetricsDTO.setAgentStatusChangeApprove(true);
		} else {
			agentMetricsDTO.setAgentStatusChangeApprove(false);
		}
		return agentMetricsDTO;
	}
	
	private HashMap<AgentMetrics, String> getMetrics(AgentActivityLog agentActivityLog){
		HashMap<AgentMetrics, String> agentMetrics = new HashMap<AgentMetrics, String>();
		AgentMetrics agentMetric = AgentMetrics.LAST_STATUS_DURATION;
		long statusDuration = (System.currentTimeMillis() - agentActivityLog.getCreatedDate().getTime())/XtaasConstants.MILLIS_IN_ONE_SEC;
		agentMetrics.put(agentMetric, String.valueOf(statusDuration)); //duration in MILLIS
		return agentMetrics;
	}
	
	
	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public List<AgentMetricsDTO> getAgentMetricsBySupervisor(String timeZone) {
		List<AgentSearchResult> agentsBySupervisor = agentService.searchBySupervisor();
		ArrayList<AgentMetricsDTO> agentMetricsList = new ArrayList<AgentMetricsDTO>(agentsBySupervisor.size());
		for (AgentSearchResult agent : agentsBySupervisor) {
			AgentMetricsDTO agentMetricsDTO = getAgentMetrics(agent.getId(),timeZone);
			agentMetricsList.add(agentMetricsDTO);
		} //end for
		return agentMetricsList;
	}

	@Override
	@Authorize(roles = {Roles.ROLE_SUPERVISOR})
	public List<AgentMetricsDTO> getAgentMetricsBySupervisorLiveQueue() {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());	// fetching logged in user id to check whether he is team supervisor or not.
		List<AgentSearchResult> agentsBySupervisor = agentService.searchBySupervisor();
		
		ArrayList<AgentMetricsDTO> agentMetricsList = new ArrayList<AgentMetricsDTO>(agentsBySupervisor.size());
		for (AgentSearchResult agent : agentsBySupervisor) {
			//for each of Agent get AgentCall object from AgentRegistration
			AgentMetricsDTO agentMetricsDTO = getAgentLiveQueueMetrics(agent.getId());
			if (agentMetricsDTO == null) continue;
			
			/* START READ/WRITE ACCESS TO SUPERVISOR
			 * DATE : 07/11/2017
			 * REASON : setting readOnlyCampaign flag to give read and write access to partner supervisors depending upon whether he is team's supervisor or partner supervisor */
			Campaign campaign = campaignRepository.findOneById(agentMetricsDTO.getCampaignId());
			if (campaign.getTeam().getSupervisorId().equals(userLogin.getUsername())) {
				agentMetricsDTO.setReadOnlyCampaign(false);
			} else {
				agentMetricsDTO.setReadOnlyCampaign(true);
			}
			/* END READ/WRITE ACCESS TO SUPERVISOR */	
			
			agentMetricsList.add(agentMetricsDTO);
		} //end for
		return agentMetricsList;
	}
	
	private AgentMetricsDTO getAgentLiveQueueMetrics(String agentId) {
		AgentCall agentCall = agentRegistrationService.getAgentCall(agentId);
		if (agentCall == null || agentCall.getCurrentCampaign() == null || agentCall.getCurrentStatus().isStatusAway()) {
			return null;
		}

		List<AgentActivityLog> agentLastStatusList = agentActivityLogRepository.getLastStatus(agentId, PageRequest.of(0, 1, Direction.DESC, "createdDate"));
		if (agentLastStatusList != null && agentLastStatusList.size() > 0) {
			AgentActivityLog agentLastStatus = agentLastStatusList.get(0);
			if (agentLastStatus != null && agentLastStatus.getStatus() != null
					&& !agentLastStatus.getStatus().equalsIgnoreCase("ONLINE"))
				return null;
		}

		AgentMetricsDTO agentMetricsDTO = new AgentMetricsDTO();
		agentMetricsDTO.setCampaignId(agentCall.getCampaignId());
		agentMetricsDTO.setCampaignName(agentCall.getCurrentCampaign().getName());

		Agent agentFromDB = agentService.getAgent(agentId);
		agentMetricsDTO.setAgentId(agentId);
		agentMetricsDTO.setAgentName(agentFromDB.getName());
		agentMetricsDTO.setAgentType(agentFromDB.getAgentType());
		agentMetricsDTO.setAgentStatus(agentCall.getCurrentStatus());
		agentMetricsDTO.setPartnerId(agentFromDB.getPartnerId());	// DATE:31/3/2017		REASON:Sets the partnerId field of agentMetricsDTO class.
		HashMap<AgentMetrics, String> metrics = initSupervisorLiveQueueMetrics();
		agentMetricsDTO.setMetrics(metrics);
		
		//fetch latest call status, preview call status and duration from database
		AgentActivityLog agentLastCallStatus = null;
		AgentActivityLog agentLastCallPreviewStatus = null;
		if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspect() != null) {
			agentLastCallStatus = fetchLatestAgentCallStatus(agentId, agentCall.getProspectCall().getProspectCallId()); 
			agentLastCallPreviewStatus = fetchLatestAgentCallPreviewStatus(agentId, agentCall.getProspectCall().getProspectCallId(), agentCall.getCurrentCampaign().getDialerMode()); 
		}
		for (AgentMetrics metric : metrics.keySet()) {
			switch(metric) {
				case AGENT_CALL_STATUS :
					String callStatus = "Waiting for Prospect";
					if (agentLastCallStatus != null) {
						callStatus = agentLastCallStatus.getStatus();
					}
					metrics.put(metric, callStatus);
					break;
				case AGENT_CALL_STATUS_DURATION :
					String callStatusDuration = null;
					AgentActivityLog lastAgentActivityLog = null;
					if (agentLastCallStatus == null) {
						List<String> statuses = new ArrayList<String>() {
							{
								add(AgentActivityCallStatus.CALL_WRAPPED_UP.name());
								add(AgentStatus.ONLINE.name());
							}
						};
						lastAgentActivityLog = fetchLatestAgentActivityLog(agentId, statuses);
						if (lastAgentActivityLog != null) {
							callStatusDuration = ((System.currentTimeMillis() - lastAgentActivityLog.getCreatedDate().getTime())/XtaasConstants.MILLIS_IN_ONE_SEC)+"";
						}
					} else {
						callStatusDuration = ((System.currentTimeMillis() - agentLastCallStatus.getCreatedDate().getTime())/XtaasConstants.MILLIS_IN_ONE_SEC)+"";
					}
					metrics.put(metric, callStatusDuration);
					break;
				case AGENT_LEAD_DURATION :
					String leadDuration = null;
					if (agentLastCallPreviewStatus != null) {
						leadDuration = ((System.currentTimeMillis() - agentLastCallPreviewStatus.getCreatedDate().getTime())/XtaasConstants.MILLIS_IN_ONE_SEC)+"";
					}
					metrics.put(metric, leadDuration);
					break;
				case CONNECTED_WITH_PROSPECT_PHONE :
					String prospectPhone = null;
					if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspect() != null) {
						prospectPhone = agentCall.getProspectCall().getProspect().getPhone();
					}
					metrics.put(metric, prospectPhone);
					break;
				case CONNECTED_WITH_PCID : 
					String prospectCallId = null;
					if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspectCallId() != null) {
						prospectCallId = agentCall.getProspectCall().getProspectCallId();
					}
					metrics.put(metric, prospectCallId);
					break;
				case CONNECTED_WITH_PISID : 
					String prospectInteractionSessionId = null;
					if (agentCall.getProspectCall() != null && agentCall.getProspectCall().getProspectInteractionSessionId() != null) {
						prospectInteractionSessionId = agentCall.getProspectCall().getProspectInteractionSessionId();
					}
					metrics.put(metric, prospectInteractionSessionId);
					break;
			} //end switch
		} //end for
		return agentMetricsDTO;
	}
	
	private AgentActivityLog fetchLatestAgentCallStatus(String agentId, String prospectCallId) {
		AgentActivityLog agentLastCallStatus = null;
		List<AgentActivityLog> agentLastCallStatusList = agentActivityLogRepository.getLastCallStatus(agentId, prospectCallId, PageRequest.of(0, 1, Direction.DESC, "createdDate")); //TODO: Put this method in service layer
		if (agentLastCallStatusList != null && agentLastCallStatusList.size() > 0) {
			agentLastCallStatus = agentLastCallStatusList.get(0);
		}
		return agentLastCallStatus;
	}
	
	private AgentActivityLog fetchLatestAgentActivityLog(String agentId, List<String> statuses) {
		AgentActivityLog agentLastCallStatus = null;
		List<AgentActivityLog> agentLastCallStatusList = agentActivityLogRepository.getLastAgentActivityLog(agentId, statuses, PageRequest.of(0, 1, Direction.DESC, "createdDate")); //TODO: Put this method in service layer
		if (agentLastCallStatusList != null && agentLastCallStatusList.size() > 0) {
			agentLastCallStatus = agentLastCallStatusList.get(0);
		}
		return agentLastCallStatus;
	}
	
	private AgentActivityLog fetchLatestAgentCallPreviewStatus(String agentId, String prospectCallId, DialerMode dialerMode) {
		AgentActivityLog agentLastCallPreviewStatus = null;
		AgentActivityCallStatus agentActivityCallStatus;
		if (dialerMode == DialerMode.PREVIEW) {
			agentActivityCallStatus = AgentActivityCallStatus.CALL_PREVIEW;
		} else {
			agentActivityCallStatus = AgentActivityCallStatus.CALL_CONNECTED;
		}
		List<AgentActivityLog> agentLastCallPreviewStatusList = agentActivityLogRepository.fetchLatestAgentCallPreviewStatus(agentId, prospectCallId, agentActivityCallStatus, PageRequest.of(0, 1, Direction.DESC, "createdDate")); //TODO: Put this method in service layer
		if (agentLastCallPreviewStatusList != null && agentLastCallPreviewStatusList.size() > 0) {
			agentLastCallPreviewStatus = agentLastCallPreviewStatusList.get(0);
		}
		return agentLastCallPreviewStatus;
	}
	
	private HashMap<AgentMetrics, String> initSupervisorLiveQueueMetrics() {
		HashMap<AgentMetrics, String> metrics = new HashMap<AgentMetrics, String>();
		metrics.put(AgentMetrics.AGENT_CALL_STATUS, null);
		metrics.put(AgentMetrics.AGENT_CALL_STATUS_DURATION, null);
		metrics.put(AgentMetrics.CONNECTED_WITH_PROSPECT_PHONE, null);
		metrics.put(AgentMetrics.AGENT_LEAD_DURATION, null);
		metrics.put(AgentMetrics.CONNECTED_WITH_PCID, null);
		metrics.put(AgentMetrics.CONNECTED_WITH_PISID, null);
		return metrics;
	}
}
