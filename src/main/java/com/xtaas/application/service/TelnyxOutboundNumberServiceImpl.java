package com.xtaas.application.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.xtaas.db.repository.TeamRepository;
import com.xtaas.db.repository.TelnyxOutboundNumberRepository;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TelnyxOutboundNumberServiceImpl implements TelnyxOutboundNumberService {
	
	private static final Logger logger = LoggerFactory.getLogger(TelnyxOutboundNumberServiceImpl.class);

	@Autowired
	private TelnyxOutboundNumberRepository telnyxOutboundNumberRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Value("#{new java.util.Random()}")
	private Random random;

	private Map<String, List<TelnyxOutboundNumber>> telnyxOutboundNumbersMap = new HashMap<>();
	private Map<String, Integer> telnyxPoolMap = new HashMap<>();

	@Override
	@Authorize(roles = { Roles.ROLE_SYSTEM, Roles.ROLE_AGENT })
	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool) {
		Team team = teamRepository.findOneById(teamId);
		String partnerId = team.getPartnerId();
		// String cacheKey = partnerId + "_" + bucketName;
		List<TelnyxOutboundNumber> outboundNumberList;

		int areaCode = Integer.parseInt(findAreaCode(phone));
		outboundNumberList = telnyxOutboundNumberRepository.findByBucketWithAreaCode(bucketName, areaCode);
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumberRepository.findByPartnerBucket(partnerId, bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumberRepository.findOutboundNumbers(partnerId);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumberRepository.findOutboundNumbersByBucketNumber(bucketName);
		}
		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumberRepository.findByPartnerBucket("XTAAS CALL CENTER", "DEFAULT");
		}
		// find a random number from the list
		TelnyxOutboundNumber outboundNumber = outboundNumberList.get(random.nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replaceAll("[^\\d]", ""); // phone.replace("-", "");
		int length = removeSpclChar.length();
		/*
		 * handled phone numbers with length less than 10 digit & return first two digit
		 * as area code - e.g. Germany: +494023750
		 */
		if (length < 10) {
			return removeSpclChar.substring(0, 2);
		}
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = removeSpclChar.substring(startPoint, endPoint);
		return areaCode;
	}

	@Override
	public List<TelnyxOutboundNumber> getAllOutboundNumbers() {
		return telnyxOutboundNumberRepository.findAll();
	}

	@Override
	public List<CountryDetails> getDistinctCountryInformation() {
		return telnyxOutboundNumberRepository.getDistinctCountryDetails();
	}

	@Override
	public Boolean refreshOutboundNumbers() {
		if (telnyxOutboundNumbersMap != null) {
			telnyxOutboundNumbersMap.clear();
		}
		return true;
	}

	@Override
	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId) {
		List<TelnyxOutboundNumber> outboundNumbers = new ArrayList<>();
		outboundNumbers = telnyxOutboundNumbersMap.get(organizationId);
		if (outboundNumbers == null || outboundNumbers.isEmpty()) {
			outboundNumbers = telnyxOutboundNumbersMap.get("COMMON_POOL");
			if (outboundNumbers == null || outboundNumbers.isEmpty()) {
				cacheAllTelnyxOutboundNumber();
				outboundNumbers = telnyxOutboundNumbersMap.get(organizationId);
				if (outboundNumbers == null || outboundNumbers.isEmpty()) {
					outboundNumbers = telnyxOutboundNumbersMap.get("COMMON_POOL");
					telnyxOutboundNumbersMap.put(organizationId, outboundNumbers);
				}
				cacheTelnyxNumbersPoolDetails(outboundNumbers);
			}
		}
		String number = getOutboundNumber(phoneNumber, callRetryCount, countryName, organizationId, outboundNumbers,
				telnyxPoolMap);
		return number;
	}

	private String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId,
			List<TelnyxOutboundNumber> outboundNumbers, Map<String, Integer> poolMap) {
		List<TelnyxOutboundNumber> outboundNumberList = new ArrayList<>();
		int maxPoolSize = 2;
		String key = organizationId + "_" + countryName;
		if (countryName != null && poolMap.get(key) != null) {
			maxPoolSize = poolMap.get(key);
		}
		int tempMaxPool = maxPoolSize;
		int pool = callRetryCount % maxPoolSize;
		int areaCode = Integer.parseInt(findAreaCode(phoneNumber));
		if (pool == 0) {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == tempMaxPool
						&& number.getCountryName().equalsIgnoreCase(countryName)).collect(Collectors.toList());
			}
		} else {
			outboundNumberList = outboundNumbers.stream().filter(number -> number.getPool() == pool
					&& number.getAreaCode() == areaCode && number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());

			if (outboundNumberList == null || outboundNumberList.size() == 0) {
				outboundNumberList = outboundNumbers.stream().filter(
						number -> number.getPool() == pool && number.getCountryName().equalsIgnoreCase(countryName))
						.collect(Collectors.toList());
			}
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers.stream()
					.filter(number -> number.getCountryName().equalsIgnoreCase(countryName))
					.collect(Collectors.toList());
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = outboundNumbers;
		}

		if (outboundNumberList == null || outboundNumberList.size() == 0) {
			outboundNumberList = telnyxOutboundNumbersMap.get("COMMON_POOL");
		}

		TelnyxOutboundNumber outboundNumber = outboundNumberList.get(new Random().nextInt(outboundNumberList.size()));
		return outboundNumber.getDisplayPhoneNumber();
	}

	private void cacheAllTelnyxOutboundNumber() {
		logger.info("cacheAllTelnyxOutboundNumber() :: Fetching telnyxoutboundnumbers from DB to cache.");
		List<TelnyxOutboundNumber> outboundNumbers = telnyxOutboundNumberRepository.findAll();
		telnyxOutboundNumbersMap = outboundNumbers.stream()
				.filter(number -> number.getProvider() != null && number.getProvider().equalsIgnoreCase("TELNYX"))
				.collect(Collectors.groupingBy(TelnyxOutboundNumber::getPartnerId));
	}

	private void cacheTelnyxNumbersPoolDetails(List<TelnyxOutboundNumber> outboundNumbers) {
		for (TelnyxOutboundNumber outboundNumber : outboundNumbers) {
			String key = outboundNumber.getPartnerId() + "_" + outboundNumber.getCountryName();
			if (telnyxPoolMap.get(key) == null) {
				telnyxPoolMap.put(key, outboundNumber.getPool());
			} else if (telnyxPoolMap.get(key) < new Integer(outboundNumber.getPool())) {
				telnyxPoolMap.put(key, outboundNumber.getPool());
			}
		}
	}

	@Override
	public List<TelnyxOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber) {
		List<TelnyxOutboundNumber> outboundNumbers = telnyxOutboundNumberRepository
				.findByDisplayPhoneNumber(displayPhoneNumber);
		return outboundNumbers;
	}

	@Override
	public TelnyxOutboundNumber save(TelnyxOutboundNumber telnyxOutboundNumber) {
		return telnyxOutboundNumberRepository.save(telnyxOutboundNumber);
	}
	
	@Override
	public List<TelnyxOutboundNumber> deleteManyByTelnyxPhoneNumber(String phoneNumber) {
		List<TelnyxOutboundNumber> outboundNumbers = telnyxOutboundNumberRepository
				.deleteManyByTelnyxPhoneNumber(phoneNumber);
		return outboundNumbers;
	}

}