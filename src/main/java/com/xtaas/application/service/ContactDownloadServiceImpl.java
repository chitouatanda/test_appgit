package com.xtaas.application.service;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Expression;

@Service
public class ContactDownloadServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(ContactDownloadServiceImpl.class);

	@Autowired
	private CampaignRepository campaignRepository;

	private Sheet sheet;

	public void downloadContactUploadTemplate(HttpServletRequest request, HttpServletResponse response,
			String campaignId) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet();
		this.sheet = sheet;
		int rowCount = 0;
		String fileName = null;
		Row headerRow = this.sheet.createRow(rowCount);
		Campaign campaign = campaignRepository.findOneById(campaignId);
		if (campaign != null) {
			if (campaign.getQualificationCriteria() != null
					&& campaign.getQualificationCriteria().getExpressions() != null) {
				List<Expression> questionsList = campaign.getQualificationCriteria().getExpressions();
				Set<Expression> customFields = questionsList.stream()
						.filter(m -> m.getAttribute() != null && (m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
								|| m.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10")))
						.collect(Collectors.toSet());
				LinkedHashSet<Expression> orderedCustomFields = new LinkedHashSet<Expression>();
				if (customFields != null && customFields.size() > 0) {
				    for (int i = 1; i <= 10; i++) {
						String searchString = (i == 10) ? "CUSTOMFIELD_10" : "CUSTOMFIELD_0" + i;
						List<Expression> data = customFields.stream().filter(s -> s.getAttribute() != null && s.getAttribute().equalsIgnoreCase(searchString)).collect(Collectors.toList());
						if (data != null && data.size() > 0) {
							orderedCustomFields.add(data.get(0));
						}
					}
				}
				writeHeader(headerRow, orderedCustomFields);
				writeDataRow();
			} else {
				writeHeader(headerRow, null);
				writeDataRow();
			}
		}
		fileName = (campaign != null) ? campaign.getName() + "_contacts_template.xlsx" : "contacts_template.xlsx";
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	private void writeHeader(Row headerRow, LinkedHashSet<Expression> customFields) {
		Cell prefixCell = headerRow.createCell(0);
		prefixCell.setCellValue("Prefix");
		Cell firstNameCell = headerRow.createCell(1);
		firstNameCell.setCellValue("First Name*");
		Cell lastNameCell = headerRow.createCell(2);
		lastNameCell.setCellValue("Last Name*");
		Cell titleCell = headerRow.createCell(3);
		titleCell.setCellValue("Title");
		Cell departmentCell = headerRow.createCell(4);
		departmentCell.setCellValue("Department*");
		Cell emailCell = headerRow.createCell(5);
		emailCell.setCellValue("Email");
		Cell phoneCell = headerRow.createCell(6);
		phoneCell.setCellValue("Phone*");
		Cell organizationNameCell = headerRow.createCell(7);
		organizationNameCell.setCellValue("Organization Name*");
		Cell stateCodeCell = headerRow.createCell(8);
		stateCodeCell.setCellValue("State Code*");
		Cell countryCell = headerRow.createCell(9);
		countryCell.setCellValue("Country*");
		Cell industry1Cell = headerRow.createCell(10);
		industry1Cell.setCellValue("Industry 1");
		Cell industry2Cell = headerRow.createCell(11);
		industry2Cell.setCellValue("Industry 2");
		Cell industry3Cell = headerRow.createCell(12);
		industry3Cell.setCellValue("Industry 3");
		Cell industry4Cell = headerRow.createCell(13);
		industry4Cell.setCellValue("Industry 4");
		Cell minRevenueCell = headerRow.createCell(14);
		minRevenueCell.setCellValue("Minimum Revenue");
		Cell maxRevenueCell = headerRow.createCell(15);
		maxRevenueCell.setCellValue("Maximum Revenue");
		Cell minEmpCell = headerRow.createCell(16);
		minEmpCell.setCellValue("Minimum Employee Count");
		Cell maxEmpCell = headerRow.createCell(17);
		maxEmpCell.setCellValue("Maximum Employee Count");
		Cell sourceCell = headerRow.createCell(18);
		sourceCell.setCellValue("Source");
		Cell mgmtCell = headerRow.createCell(19);
		mgmtCell.setCellValue("ManagementLevel");
		Cell addLine1Cell = headerRow.createCell(20);
		addLine1Cell.setCellValue("AddressLine1");
		Cell addLine2Cell = headerRow.createCell(21);
		addLine2Cell.setCellValue("AddressLine2");
		Cell cityCell = headerRow.createCell(22);
		cityCell.setCellValue("City");
		Cell extensionCell = headerRow.createCell(23);
		extensionCell.setCellValue("Extension");
		Cell postalCodeCell = headerRow.createCell(24);
		postalCodeCell.setCellValue("Postal Code");
		Cell domainCell = headerRow.createCell(25);
		domainCell.setCellValue("Domain");
		Cell directPhoneCell = headerRow.createCell(26);
		directPhoneCell.setCellValue("Direct Phone*");
		Cell companyValidationCell = headerRow.createCell(27);
		companyValidationCell.setCellValue("Company Validation Link");
		Cell titleValidationCell = headerRow.createCell(28);
		titleValidationCell.setCellValue("Title Validation Link");
		Cell europeanCell = headerRow.createCell(29);
		europeanCell.setCellValue("isEuropean");
		Cell sortOrderCell = headerRow.createCell(30);
		sortOrderCell.setCellValue("Sort Order");
		if (customFields != null && customFields.size() > 0) {
			int count = 31;
			for (Expression expression : customFields) {
				Cell customQuestions = headerRow.createCell(count);
				customQuestions.setCellValue("CF" + getDigitsFromString(expression.getAttribute()) + "_" + expression.getQuestionSkin());
				count++;
			}
		}
	}
	
	private StringBuilder getDigitsFromString(String attribute) {
		char[] chars = attribute.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : chars) {
			if (Character.isDigit(c)) {
				sb.append(c);
			}
		}
		return sb;
	}
	
	private void writeDataRow() {
		Row headerRow = this.sheet.createRow(1);
		Cell prefixCell = headerRow.createCell(0);
		prefixCell.setCellValue("");
		Cell firstNameCell = headerRow.createCell(1);
		firstNameCell.setCellValue("Chris");
		Cell lastNameCell = headerRow.createCell(2);
		lastNameCell.setCellValue("Roe");
		Cell titleCell = headerRow.createCell(3);
		titleCell.setCellValue("Manager");
		Cell departmentCell = headerRow.createCell(4);
		departmentCell.setCellValue("Information Technology");
		Cell emailCell = headerRow.createCell(5);
		emailCell.setCellValue("cris.roe@example.com");
		Cell phoneCell = headerRow.createCell(6);
		phoneCell.setCellValue("604-222-1111");
		Cell organizationNameCell = headerRow.createCell(7);
		organizationNameCell.setCellValue("Grey Academy");
		Cell stateCodeCell = headerRow.createCell(8);
		stateCodeCell.setCellValue("BC");
		Cell countryCell = headerRow.createCell(9);
		countryCell.setCellValue("Canada");
		Cell industry1Cell = headerRow.createCell(10);
		industry1Cell.setCellValue("NA");
		Cell industry2Cell = headerRow.createCell(11);
		industry2Cell.setCellValue("NA");
		Cell industry3Cell = headerRow.createCell(12);
		industry3Cell.setCellValue("NA");
		Cell industry4Cell = headerRow.createCell(13);
		industry4Cell.setCellValue("NA");
		Cell minRevenueCell = headerRow.createCell(14);
		minRevenueCell.setCellValue("");
		Cell maxRevenueCell = headerRow.createCell(15);
		maxRevenueCell.setCellValue("");
		Cell minEmpCell = headerRow.createCell(16);
		minEmpCell.setCellValue("0");
		Cell maxEmpCell = headerRow.createCell(17);
		maxEmpCell.setCellValue("4");
		Cell sourceCell = headerRow.createCell(18);
		sourceCell.setCellValue("Xtaas Data Source");
		Cell mgmtCell = headerRow.createCell(19);
		mgmtCell.setCellValue("Manager");
		Cell addLine1Cell = headerRow.createCell(20);
		addLine1Cell.setCellValue("123 West Ave");
		Cell addLine2Cell = headerRow.createCell(21);
		addLine2Cell.setCellValue("");
		Cell cityCell = headerRow.createCell(22);
		cityCell.setCellValue("Vancouver");
		Cell extensionCell = headerRow.createCell(23);
		extensionCell.setCellValue("8");
		Cell postalCodeCell = headerRow.createCell(24);
		postalCodeCell.setCellValue("V6R 4P9");
		Cell domainCell = headerRow.createCell(25);
		domainCell.setCellValue("www.google.com");
		Cell directPhoneCell = headerRow.createCell(26);
		directPhoneCell.setCellValue("Yes");
		Cell companyValidationCell = headerRow.createCell(27);
		companyValidationCell.setCellValue("");
		Cell titleValidationCell = headerRow.createCell(28);
		titleValidationCell.setCellValue("");
		Cell europeanCell = headerRow.createCell(29);
		europeanCell.setCellValue("Yes");
	}
}
