package com.xtaas.application.service;

import java.text.ParseException;
import java.util.List;
import org.springframework.data.domain.Pageable;
import com.xtaas.domain.entity.Team;
import com.xtaas.web.dto.TeamDTO;
import com.xtaas.web.dto.TeamSearchDTO; 
import com.xtaas.web.dto.TeamSearchResultDTO;

public interface TeamService {
	public List<TeamSearchResultDTO> searchByCriteria(TeamSearchDTO teamSearchDTO, Pageable pageRequest);
	public List<Object> countByCriteria(TeamSearchDTO teamSearchDTO);
	public List<TeamSearchResultDTO> searchByAutosuggest(String campaignId, Pageable pageRequest);
	public List<TeamSearchResultDTO> searchByRating(String campaignId, Pageable pageRequest);
	public List<TeamSearchResultDTO> searchByCurrentInvitations(String campaignId, Pageable pageRequest);
	public List<TeamSearchResultDTO> searchByCurrentSelection(String campaignId);
	public List<TeamSearchResultDTO> searchByPreviousCampaigns(String campaignId);
	public List<TeamSearchResultDTO> searchByPreviousWorkedCampaigns(String campaignId, int count) throws ParseException;
	public List<TeamSearchResultDTO> searchByQa();
	public void temporaryAllocateCampaign(String agentId, String campaignId);
	public void undoTemporaryAllocation(String agentId, String campaignId);
	public TeamDTO createTeam(TeamDTO teamDTO);
	public TeamDTO updateTeam(String teamId, TeamDTO teamDTO);
	public void addAgent(String teamId, String agentId);
	public void removeAgent(String teamId, String agentId);
	public void addQa(String teamId,  String qaId);
	public void removeQa(String teamId, String qaId);
	Team getTeam(String teamId);
	public void undoTemporaryAllocation(String agentId);
}

