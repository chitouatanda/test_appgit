package com.xtaas.application.service;

import java.util.List;

import com.plivo.api.models.number.PhoneNumber;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;

public interface PlivoOutboundNumberService {

	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool);

	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId);

	public List<PlivoOutboundNumber> getAllOutboundNumbers();

	public List<CountryDetails> getDistinctCountryInformation();

	public Boolean refreshOutboundNumbers();

	public List<PlivoOutboundNumber> findByDisplayPhoneNumber(String displayPhoneNumber);

	public PlivoOutboundNumber save(PlivoOutboundNumber plivoOutboundNumber);
	
	public List<PlivoOutboundNumber> findByIsdCode(int isd);

	public PlivoOutboundNumber findOneByIsdCode(int isd);

	public void refreshPhoneNumbers(PlivoPhoneNumberPurchaseDTO phoneDTO);

	public void cacheAllPlivoOutboundNumberForDatabuy();

	public Integer getIsdCodeFromCountryName (String countryName);

	public void hardCacheRefeshCountryIsdCodeMapForDatabuy();
	
	public List<PlivoOutboundNumber> findAll();

	public PhoneNumber searchNumber(PlivoPhoneNumberPurchaseDTO phoneDTO);

}
