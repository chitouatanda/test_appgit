package com.xtaas.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.utils.XtaasConstants;

@Service
public class CallServiceStatusImpl implements CallServiceStatus {
	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@Override
	public void processCallStatus(String callSid, String callStatus) {
		ProspectCallStatus prospectCallStatus = getProspectCallStatus(callStatus);
		if (prospectCallStatus != null) {
			prospectCallLogService.updateCallStatus(callSid, prospectCallStatus);
		}
	}

	/**
	 * Compare twilio status with XTaaS status
	 * @param twilioCallStatus
	 * @return ProspectCallStatus
	 */
	private ProspectCallStatus getProspectCallStatus(String twilioCallStatus) {
		if (XtaasConstants.TWILIO_CALL_STATUS_INPROGRESS.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_INPROGRESS;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_NO_ANSWER.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_NO_ANSWER;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_BUSY.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_BUSY;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_CANCELED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_CANCELED;
		} else if (XtaasConstants.TWILIO_CALL_STATUS_FAILED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_FAILED;
		} else if (XtaasConstants.TWILIO_CALL_MACHINE_ANSWERED.equals(twilioCallStatus)) {
			return ProspectCallStatus.CALL_MACHINE_ANSWERED;
		} else {
			return null;
		}
	}

}
