/**
 * 
 */
package com.xtaas.application.service;

import java.util.List;

import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;

/**
 * @author djain
 *
 */
public interface OutboundNumberService {

	public String getOutboundNumber(String teamId, String bucketName, String phone, int pool);

	public String getOutboundNumber(String phoneNumber, int callRetryCount, String countryName, String organizationId);

	public List<OutboundNumber> getAllOutboundNumbers();

	public List<CountryDetails> getDistinctCountryInformation();

	public Boolean refreshOutboundNumbers();

}
