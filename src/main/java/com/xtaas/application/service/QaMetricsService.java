package com.xtaas.application.service;

import java.text.ParseException;
import java.util.HashMap;

import com.xtaas.application.service.QaMetricsServiceImpl.QaMetrics;
import com.xtaas.web.dto.ProspectCallSearchDTO;

public interface QaMetricsService {

	public HashMap<QaMetrics, Integer> getQaMetricsByDispositionStatus(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	
	public HashMap<QaMetrics, Integer> getQaMetricsByProspectCallStatus(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	
	public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate();

	public HashMap<QaMetrics, Integer> getQaMetrics(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	public HashMap<QaMetrics, Integer> getSecondaryQaMetrics(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;
	
}
