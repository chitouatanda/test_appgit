package com.xtaas.application.service;


import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignNameSearchResult;
import com.xtaas.domain.valueobject.CampaignSearchResult;
import com.xtaas.domain.valueobject.CampaignTeamDTO;
import com.xtaas.domain.valueobject.IDNCSearchResult;
import com.xtaas.domain.valueobject.SupervisorCampaignResult;
import com.xtaas.domain.valueobject.TeamSearchResult;
import com.xtaas.web.dto.ActiveAssetDTO;
import com.xtaas.web.dto.AgentInvitationDTO;
import com.xtaas.web.dto.CallGuideDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.CampaignDetailsForAgentDTO;
import com.xtaas.web.dto.CampaignManagerMetricsDTO;
import com.xtaas.web.dto.CampaignNameSearchDTO;
import com.xtaas.web.dto.CampaignSearchDTO;
import com.xtaas.web.dto.CampaignTeamShortlistedDTO;
import com.xtaas.web.dto.EmailMessageDTO;
import com.xtaas.web.dto.IDNCSearchDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.TeamInvitationDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;
import com.xtaas.web.dto.WorkScheduleDTO;

public interface CampaignService {
	public Campaign getCampaign(String campaignId);
	
	public List<Campaign> getActiveCampaigns();
	
	public List<Campaign> getActiveCampaignsByOrg(String organizationId);

	public List<Campaign> getActiveXtaasCampaigns();

	public List<Campaign> getActiveOtherCampaigns();

	public List<Campaign> getAllCampaigns();

	public CampaignSearchResult listByCampaignManager(CampaignSearchDTO campaignSearchDTO);

	public CampaignSearchResult listBySupervisor(CampaignSearchDTO campaignSearchDTO);
	
	public SupervisorCampaignResult getSupervisorCampaigns(CampaignSearchDTO campaignSearchDTO);
	
	public CampaignSearchResult listByteamIds(CampaignSearchDTO campaignSearchDTO);
	
	public CampaignSearchResult listByQa(CampaignSearchDTO campaignSearchDTO) throws ParseException;
	
	public CampaignDTO createCampaign(CampaignDTO campaignDTO, HttpServletRequest request);

	public CampaignDTO editCampaign(CampaignDTO campaignDTO);
	
	public void deleteCampaign(String campaignId);
	
	public void sendEmail(String campaignId);

	public void inviteAgent(String campaignId, AgentInvitationDTO agentInvitation);

	public void inviteTeam(String campaignId, List<String> teamIds);
	
	public void removeInviteTeam(String campaignId, String teamId);
	
	public void shortlistTeam(String campaignId, CampaignTeamShortlistedDTO campaignTeamShortlistedDTO);

	public List<TeamSearchResultDTO> listTeamInvitations(String campaignId);
	
	public List<TeamSearchResultDTO>  listTeamsShortlisted(String campaignId);
	
	public void  cancelTeamsShortlisted(String campaignId, String teamId);

	public List<AgentInvitationDTO> listAgentInvitations(String campaignId);

	public CampaignTeamDTO getTeam(String campaignId);
	
	public void chooseTeam(String campaignId, CampaignTeamDTO campaignTeamDTO);

	public void updateTeam(String campaignId, CampaignTeamDTO campaignTeamDTO);

	public CallGuideDTO getCallGuide(String campaignId);
	
	public void setCallGuide(String campaignId, CallGuideDTO callGuide);
	
	public void allocateAgent(String campaignId, String agentId);

	public void addWorkSchedule(String campaignId, String agentId, WorkScheduleDTO workSchedule);
	
	public void updateWorkSchedule(String campaignId, String agentId, String scheduleId,  WorkScheduleDTO workSchedule);
	
	public WorkScheduleDTO getWorkSchedule(String campaignId, String agentId, String scheduleId);

	public void deleteWorkSchedule(String campaignId, String agentId, String scheduleId);

	public void deallocateAgent(String campaignId, String agentId);
	
	public void activateAsset(ActiveAssetDTO activeAssetDTO);
	
	public void planCampaign(String campaignId);
	
	public void runCampaign(String campaignId);
	
	public void runReadyCampaign(String campaignId);
	
	public void runningCampaign(String campaignId);

	public void resumeCampaign(String campaignId);

	public void pauseCamaign(String campaignId);

	public void stopCampaign(String campaignId);
	
	public void completeCampaign(String campaignId);
	
	public void delistCampaign(String campaignId);

	public String deliverActiveAsset( ProspectCall prospectCallFromAgent);
	
	public void setAssetEmailTemplate(String campaignId , EmailMessageDTO emailMessageDTO);
	
	public EmailMessageDTO retrivetAssetEmailTemplate(String name);
	
	public CampaignManagerMetricsDTO getCampaignManagerMetrics(String campaignManagerId);
	
	public List<String> getAllocatedAgents(String campaignId);
	
	public List<String> getCampaignIds(List<String> teamIds);

	public String testAssetEmailTemplate(String campaignId, EmailMessageDTO emailMessageDTO);

	//Added for qa filter
	public CampaignSearchResult getCamapignsForProspectCallsByQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException;

	CampaignSearchResult listBySecQa(CampaignSearchDTO campaignSearchDTO) throws Exception;

	CampaignSearchResult getCamapignsForProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO)
			throws ParseException;		
	
	public List<Campaign> getCampaignByIds(List<String> campaignIds);
	
	public CampaignNameSearchResult searchCampaigns(CampaignNameSearchDTO idncSearchDTO);
	
	public CampaignDTO cloneCampaign(String campaignId);

	public SupervisorCampaignResult searchSupervisorCampaigns(String campaignName);
	
	public CampaignDTO returnCampaigns(String id);
	
	public CampaignDetailsForAgentDTO getCampaignInfoForAgent(String id);

	/**
	 * find active campaigns with projected fields. "id & name" fields will be
	 * projected as default.
	 * 
	 * @param fields extra fields that you want to fetch
	 * @return
	 */
	public List<CampaignDTO> findActiveWithProjectedFields(List<String> fields);

}
