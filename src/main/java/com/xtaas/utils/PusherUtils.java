package com.xtaas.utils;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.pusher.Pusher;

public final class PusherUtils {

	private static final Logger logger = LoggerFactory.getLogger(PusherUtils.class);

	public static void pushMessageToUser(String userId, String event, String message) {
		try {
			if (userId != null && message != null) {
				logger.info("pushMessageToUser() : Sending pusher message to [{}] for [{}] event.", userId, event);
				String responseCode = Pusher.triggerPush(userId, event, XtaasUtils.removeNonAsciiChars(message));
				logger.debug("pushMessageToUser() : Response Code [{}]", responseCode);
			} else {
				logger.error("pushMessageToUser() : userId or message is not valid.");
			}
		} catch (IOException e) {
			logger.error(
					"pushMessageToUser() : Error occurred while pushing message to " + userId + " , Msg:" + message, e);
		}
	}

}
