/**
 * 
 */
package com.xtaas.utils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.xtaas.BeanLocator;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.valueobjects.UserPrincipal;

/**
 * @author djain
 *
 */
public class XtaasUserUtils {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(XtaasUserUtils.class);
	
	public static Login getCurrentLoggedInUserObject() {
		UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return principal.getLogin();
	}
	
	public static Organization getCurrentLoggedInUsersOrganization() {
	    UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    return principal.getOrganization();
	}
	
	public static String getCurrentLoggedInUsername() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) 
			return null;
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		String username;
		if (principal instanceof UserDetails) {
		  username = ((UserDetails)principal).getUsername();
		} else {
		  username = principal.toString();
		}
		//logger.debug("getCurrentLoggedInUsername() : Returning " + username);
		return username;
	}
	
	/**
	 * Returns all the applicable roles as per the role hierarchy
	 * @return
	 */
	public static List<GrantedAuthority> getGrantedRoles() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) 
			return null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		RoleHierarchyImpl roleHierarchy = BeanLocator.getBean("roleHierarchy", RoleHierarchyImpl.class);
        //get the list of all the applicable roles as per the role hierarchy based on the roles assigned to the user
        return (List<GrantedAuthority>) roleHierarchy.getReachableGrantedAuthorities(auth.getAuthorities());
	}

	
	public static String getPrimaryRole() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) 
			return null;
		
		@SuppressWarnings("unchecked")
		List<GrantedAuthority> grantedAuthorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		
        //primary authority - assuming the authority/role at 0th position is the primary role
        return grantedAuthorities.get(0).getAuthority();
	}
	
	public static void refreshSecurityContext() {
		UserDetailsService userDetailsService = BeanLocator.getBean("xtaasUserDetailsServiceImpl", UserDetailsService.class);
		UserDetails userPrincipal = userDetailsService.loadUserByUsername(getCurrentLoggedInUsername());
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userPrincipal, userPrincipal.getPassword(), userPrincipal.getAuthorities()));
	}
}
