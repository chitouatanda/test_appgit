package com.xtaas.utils;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;

import com.xtaas.utils.EditDistance.Cell;

public class EditDistance {
    final static int MATCH = 0;
    final static int INSERT = 1;
    final static int DELETE = 2;
    final static int SUBST = 3;
    
    final static class Cell {
        private int rowKey;
        private int colKey;

        public Cell(int rowKey, int colKey) {
            this.rowKey = rowKey;
            this.colKey = colKey;
        }
    }

    public static int dist(char[] p, char[] t) {
        int[][] costTable = new int[p.length + 1][t.length + 1];
        int[][] parent = new int[p.length + 1][t.length + 1];

        // init base cases
        costTable[0][0] = 0;
        parent[0][0] = -1;
        for (int i = 1; i <= p.length; i++) {
            leadColInit(costTable, parent, i);
        }
        for (int j = 1; j <= t.length; j++) {
            headRowInit(costTable, parent, j);
        }

        int[] options = new int[3];
        for (int i = 1; i <= p.length; i++) {
            for (int j = 1; j <= t.length; j++) {
                options[MATCH] = costTable[i - 1][j - 1] + matchCost(p, t , i , j);
                options[INSERT] = costTable[i][j - 1] + insertCost(p, t , i , j);
                options[DELETE] = costTable[i - 1][j] + deleteCost(p, t , i , j);

                costTable[i][j] = options[MATCH];
                parent[i][j] = MATCH;
                for (int k = INSERT; k <= DELETE; k++) {
                    if (options[k] < costTable[i][j]) {
                        costTable[i][j] = options[k];
                        parent[i][j] = k;
                    }
                }
            }
        }
        //diagnostics(p, t, costTable, parent);
        Cell g = goalCell(p, t, costTable);
        return costTable[g.rowKey][g.colKey];
    }

    protected static Cell goalCell(char[] p, char[] t, int[][] costTable) {
        return new Cell(p.length, t.length);
    }

    protected static void headRowInit(int[][] costTable, int[][] parent, int j) {
        costTable[0][j] = costTable[0][j - 1] + 1;
        parent[0][j] = INSERT;
    }

    private static void leadColInit(int[][] costTable, int[][] parent, int i) {
        costTable[i][0] = costTable[i - 1][0] + 1;
        parent[i][0] = DELETE;
    }
    
    // heuristics I have to build: at what threshold the name matching is considered similar
    // Build a human names phonetic database
    // Build a company names phonetic datatbase
    // Search for the names and phonetic variations in transcriptions
    // 
    // in my case transcription is phonetic by definition
    // spelling is cultural/name
    // insert/delete a punctuation should cost less than a char
    // if delete is related to phonetic

    static int deleteCost(char[] p, char[] t, int i, int j) {
        return 1;
    }

    static int insertCost(char[] p, char[] t, int i, int j) {
       return 1;
    }

    // phonetic variations can have lower costs
    static int matchCost(char[] p, char[] t, int i, int j) {
        if (p[i - 1] == t[j - 1]) {
            return 0;
        } else {
            return 1;
        } 
    }

    static void print(int [][] costTable, char[] t) {
        
        System.out.print("* ");
        for (int j = 0; j < t.length; j++) {
            System.out.print(t[j] + " ");
        }
        System.out.println();
        for (int i = 0; i < costTable.length; i++) {
            for (int j = 0; j < costTable[0].length; j++) {
                System.out.print(costTable[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void diagnostics(char[] p, char[] t, int[][] costTable, int[][] parent) {
        print(costTable, t);
        System.out.println();
        
        print(parent, t);
        System.out.println();
        Cell g = goalCell(p, t, costTable);
        int cost = costTable[g.rowKey][g.colKey];
        hasSubstr(t.length, cost);
        if (hasSubstr(p.length, cost)) {
            Queue<Character> m = new ArrayDeque<>();
            path(parent, p, t, g.rowKey, g.colKey, m);
            System.out.println();
            m.stream().forEach(c -> System.out.print(c));
            System.out.println();
        } else {
            System.out.println("Not Found");
        }
    }

    static void path(int[][] parent, char[] p, char[] t, int i, int j, Queue<Character> matchedSubstr) {
        if (parent[i][j] == -1) {
            return;
        }
        switch(parent[i][j]) {
            case INSERT: path(parent, p, t, i, j - 1, matchedSubstr);
                         System.out.print("I");
                         matchedSubstr.add(t[j - 1]);
                         break;
                         
            case DELETE: path(parent, p, t, i - 1, j, matchedSubstr);
                         System.out.print("D");
                         break;

            case MATCH: path(parent, p, t, i - 1, j - 1, matchedSubstr); 
                        if (p[i - 1] == t[j - 1]) {
                            System.out.print("M");
                        } else {
                            System.out.print("S"); 
                        }
                        matchedSubstr.add(t[j - 1]);
                        break;
        }
    }

    private static boolean hasSubstr(int length, int cost) {
        return (cost * 1.0/length < 0.3);
    }
}

