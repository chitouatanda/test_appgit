package com.xtaas.utils;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/31/14
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class CollectionUtils {
  public static Collection mappedCollection(Collection collection, Map map, int maxElements){
    if(map==null || map.size()==0) return collection;
    if(collection==null) return null;

    Collection ret=new ArrayList(maxElements);
    for(Object o:boundedCollection(collection, maxElements)){
      ret.add(map.get(o));
    }
    return ret;
  }

  public static Collection boundedCollection(Collection collection, int maxElements){
    if(collection==null) return null;
    if(maxElements==0 || collection.size()<maxElements) return collection;

    if(collection instanceof List) return ((List)collection).subList(0, maxElements);
    Collection ret=new ArrayList(maxElements);
    for(Object o:collection){
      ret.add(o);
      if(--maxElements<=0) break;
    }
    return ret;
  }

    public static String textIfContains(Collection collection, Object inCollection, String ifContains, String ifNotContains){
        if(collection==null || inCollection==null || collection.isEmpty()) return ifNotContains;
        return collection.contains(inCollection) ? ifContains : ifNotContains;
    }

    public static String firstValue(Object o){
        if(o==null) return "";
        if(o instanceof Object[]){
            Object[] a = (Object[])o;
            return a.length>0 ? a[0]+"" : "";
        }

        Iterator it=null;
        if(o instanceof Collection) it = ((Collection) o).iterator();
        else if(o instanceof Map) it = ((Map) o).values().iterator();
        if(it==null) return o+"";
        return it.hasNext() ? it.next().toString() : "";
    }

}
