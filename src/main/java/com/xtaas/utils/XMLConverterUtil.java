package com.xtaas.utils;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.xtaas.web.dto.LeadformAttributes;

/**
 * XMLConverterUtil
 */
public class XMLConverterUtil {

	public static LeadformAttributes convertXMLToLeadformAttributes(String xmlString) throws IOException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(LeadformAttributes.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			LeadformAttributes leadformAttributes = (LeadformAttributes) jaxbUnmarshaller
					.unmarshal(new StringReader(xmlString));
			return leadformAttributes;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
