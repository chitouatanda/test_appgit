/**
 *
 */
package com.xtaas.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author djain
 *
 */
public interface XtaasConstants {

	public static final String NEWLINE = System.getProperty("line.separator");

	public static final Long ONE_HOUR_IN_MILLIS = new Long(3600000);

	public static final Long ONE_DAY_IN_MILLIS = new Long(86400000);

	public static final Long MILLIS_IN_ONE_SEC = new Long(1000);

	public static final String SYSTEM_USERNAME = "SYSTEM";

	public static final String UTF8 = "UTF-8";

	public static final String PACIFIC_TZ = "US/Pacific";

	public static final String COMMA = ",";

	public final String CLIENT_REGION_EAST = "us-east-1";

	// Enum of all Application properties stored in "applicationproperty" collection
	public static enum APPLICATION_PROPERTY {
		CHECK_PHONENUMBER_USING_STRIKEIRON, // Flag to control whether to verify PhoneNumber and DNC using StrikeIron
											// APIs
		CALL_NO_ANSWER_CALLBACK_HRS, // Callback after specified hrs for call with no-answer status
		CALL_BUSY_CALLBACK_HRS, // Callback after specified hrs for call with busy status
		CALL_MAX_RETRIES, // Max number of retries for calls with no-answer and busy status
		DIALER_THREAD_COUNT, // Count of Dialer threads running in dialer component
		BATCH_JOB_NOTIFICATION_EMAILS, // Email addresses to whom batch job events (success/failure) needs to be sent
		OUTBOUND_CALL_HANGUP_ON_AGENT_DISCONNECTION, // Hangup Outbound call initiated by agent when agent hit
														// disconnect on dialer pad
		MAX_CALL_RINGING_DURATION_AUTODIAL, // Max duration till when a call can be in ringing state
		ENABLE_MACHINE_ANSWERED_DETECTION, // Enable check for detecting if call is answered by machine
		DAILY_CALL_MAX_RETRIES, // Daily call limit to a prospect
		MAX_CALL_RETRY_COUNT_MACHINE_DETECTION, // enable machine detection if prospect's callRetry count is greater
												// than equals to this count.
		MAX_PROSPECT_VERSION, // Added to eliminate duplicate leads in running campaigns.
		QA_PORTAL_ACCESS, MAX_DIALERCODE_LIMIT, MAX_MULTIPROSPECT_DIALERCODE_LIMIT, CALLBACK_MINUTES,
		AUTO_CALLBACK_MINUTES, MAX_CALL_RINGING_DURATION_MANUALDIAL, PROSPECT_CALL_DETAILS_UPDATE_THREAD_INSTANCE,
		FAILURE_RECORDINGS_DOWNLOAD_THREAD_INSTANCE, TWILIO_CALL_LOG_JOB_INSTANCE, ZOOM_TOKEN_THREAD_INSTANCE,
		DATA_BUY_THREAD_INSTANCE, CACHE_MODE, AGENT_ABSENT_DAYS, AGENT_DEACTIVATE_JOB_INSTANCE, VOICE_TO_TEXT,
		PLIVO_MAX_CALL_TIME_LIMIT, PLIVO_AMD_TYPE, PARTNER_NOT_SCORED_JOB_INSTANCE,
		RECORDING_TRANSCRIPTION_JOB_INSTANCE, VERIFICATION_AUTO_CALLBACK_MINUTES, AGENT_STATUS_LOG_UPDATE_JOB_INSTANCE,
		AGENT_ONLINE_REPORT_JOB_INSTANCE, AGENT_ONLINE_REPORT_EMAIL, EVERSTRING_API, CATCH_IO_SIGNAL_THREAD_INSTANCE,
		SPEECH_TO_TEXT_THREAD_INSTANCE, UPDATE_CALL_DIALED_TO_AUTO_THREAD_INSTANCE, VERIFICATION_CALLBACK_MINUTES,
		SUPPRESSION_REMOVE,AUTO_MACHINE_HANGUP,JITTER_RANGE,PACKETLOSS_RANGE,RTT_RANGE,TELNYX_MISSING_RECORDING,SENDGRID_EMAIL_SEND,
		DB_INSTANCE, PROSPECT_CACHE_NUMBER_OF_PROSPECTS_PER_AGENT, PROSPECT_CACHE_AGENT_RANGE, PROSPECT_CACHE_THREAD_POOL_SIZE, 
		DISCONNECT_BUTTON_AUTO_TIMER_IN_MS, DISCONNECT_BUTTON_MANUAL_TIMER_IN_MS, IS_FIFTEEN_SEC_DISCONNECT_TIMER,MAX_BUY_JOBS, MAX_SEARCH_TIME, 
		BSON_DOCUMENTSIZE,LOW_CALLABLE_ALERT_TIME_IN_MIN, BULKINSERT_BATCHSIZE, BULKREAD_BATCHSIZE, DATA_FEEDBACK_JOB_INSTANCE, BULK_MOVE_SALUATARY_BATCHSIZE,
		TPS_UK_PHONE_DNC_CHECK, TRANSCRIPTION_JOB_INSTANCE, AGENT_REQUEST_APPROVAL_TIME, CAMPAIGN_CALLABLE_MONITOR_JOB_INSTANCE
		  ,DATA_BUY_RATE_LIMITER,UPDATE_ZOOM_TOKEN

	};

	public static final String ADMIN_SUPERVISOR = "ADMIN_SUPERVISOR";

	// TWILIO CONSTANTS
	public static final String TWILIO = "TWILIO";

	// Twilio Call Statuses
	public static final String TWILIO_CALL_STATUS_QUEUED = "queued";
	public static final String TWILIO_CALL_STATUS_RINGING = "ringing";
	public static final String TWILIO_CALL_STATUS_INPROGRESS = "in-progress";
	public static final String TWILIO_CALL_STATUS_COMPLETED = "completed";
	public static final String TWILIO_CALL_STATUS_BUSY = "busy";
	public static final String TWILIO_CALL_STATUS_FAILED = "failed";
	public static final String TWILIO_CALL_STATUS_NO_ANSWER = "no-answer";
	public static final String TWILIO_CALL_STATUS_CANCELED = "canceled";
	public static final String TWILIO_CALL_MACHINE_ANSWERED = "machine";

	// Twilio IVR messages
	public static final String TWILIO_ABANDON_OPTOUT_MSG = "Sorry, No agent available. Press 1 followed by pound sign to opt out";
	public static final String TWILIO_VM_LEFT_BY_DIALER = "Hi, We at XTaaS were trying to reach you. Please call us back at 415-403-2426";

	// Webhook Email Bounce Regx constant
	public static final String FEEDBACK_ID = "(?m)Feedback-ID.*";
	public static final String FROM_EMAIL = "(?m)From:.*";
	public static final String ACTION = "(?m)Action:.*";
	public static final String STATUS = "(?m)Status:.*";
	public static final String EMAIL_BOUNCE_MESSAGE_ID = "(?m)Message-ID:.*";

	public static final String INSIDEUP_CLIENT = "INSIDEUP";
	public static final String WHEELHOUSE_CLIENT = "WHEELHOUSE";

	public static enum LEAD_STATUS {
		NOT_SCORED, QA_ACCEPTED, QA_REJECTED, QA_RETURNED, ACCEPTED, REJECTED, CLIENT_ACCEPTED, CLIENT_REJECTED, GK_ANSWERMACHINE,ANSWERMACHINE,NOT_DISPOSED, CALLBACK
	}

	public static final String PROSPECT_CALL_EVENT = "prospect_call";
	public static final String CALL_DISCONNECCT_EVENT = "call_disconnect";
	public static final String AMD_DETECTED_EVENT = "amd_detect";
	public static final String AMD_CALL_DISCONNECT_EVENT = "telnyx_amd_call_disconnect";
	public static final String AGENT_TYPE_CHANGE = "agent_type_change";
	public static final String SCRIPT_CHANGE = "script_change";
	public static final String CAMPAIGN_SETTINGS_CHANGE = "campaign_settings_change";
	public static final String TEAM_NOTIFICATION_EVENT = "team_notification";
	public static final String NO_CALLABLE_LOGOUT_EVENT = "no_callable_logout";
	public static final String CALLBACK_EVENT = "callback_prospect";
	public static final String TELNYX_CONFERENCE_CREATION_ISSUE_EVENT = "telnyx_conference_creation_issue";
	public static final String TELNYX_MANUAL_CALL_ANSWERED_EVENT = "telnyx_manual_call_answered";

	// Pivots checkPoints
	public static final String MANAGEMENT_LEVELS = "MANAGEMENT LEVELS";
	public static final String DEPARTMENT = "DEPARTMENT";
	public static final String EMPLOYEE_COUNT = "EMPLOYEE COUNT";
	public static final String REVENUE = "REVENUE";
	public static final String INDUSTRY = "INDUSTRY";
	public static final String TITLE = "TITLE";
	public static final String VALID_COUNTRY = "COUNTRY";
	public static final String SIC_CODE = "SIC_CODE";
	public static final String VALID_STATE = "VALID_STATE";
	public static final String ZIP_CODE = "ZIP_CODE";
	public static final String COMPANY = "COMPANY";
	public static final String SOURCE = "SOURCE";
	public static final String INDUSTRY_LIST = "INDUSTRY LIST";
	public static final String DNC_COMPANY = "COMPANY";
	public static final String DNC_PROSPECT = "PROSPECT";
	public static final String STATE_CODE = "STATE CODE";
	public static final String SOFT_DELETE = "DELETE";
	public static final String HARD_DELETE = "REMOVE";
	public static final String ISEU = "EUROPE(FOR GDPR)";
	public static final String SIC = "SIC LIST";
	public static final String NAICS = "NAICS LIST";
	public static final String DEMANDSHORE = "DEMANDSHORE";
	public static final String SALESIFY = "SALESIFY";
	public static final String DEMANDSHOREAG = "DEMANDSHOREAG";

	public static final String CALL_RECORDING_AWS_URL = "https://s3-us-west-2.amazonaws.com/xtaasrecordings/recordings/";

	public static final String XTAAS_RECORDING_BUCKET_NAME = "xtaasrecordings/recordings";
	public static final String S3_FILE_URL_TEMPLATE = "https://s3-us-west-2.amazonaws.com/%s/%s";

	public static final String DIRECT_PHONE = "DIRECT";

	public static final String INDIRECT_PHONE = "INDIRECT";

	public static enum PROSPECT_TYPE {
		POWER, PREVIEW, RESEARCH
	}

	public static enum ProspectCallStatus {
		QUEUED, LOCAL_DNC_ERROR, NATIONAL_DNC_ERROR, UNKNOWN_ERROR, MAX_RETRY_LIMIT_REACHED, CALL_TIME_RESTRICTION, CALL_NO_ANSWER, CALL_BUSY, CALL_CANCELED, CALL_FAILED, CALL_MACHINE_ANSWERED, CALL_ABANDONED, CALL_PREVIEW, CALL_DIALED, CALL_INPROGRESS, CALL_COMPLETE, WRAPUP_COMPLETE, QA_INITIATED, QA_COMPLETE, SECONDARY_QA_INITIATED, QA_WRAPUP_COMPLETE, CLIENT_DELIVERED, CLIENT_REJECTED, QA_REJECTED, PRIMARY_REVIEW, SECONDARY_REVIEW, INDIRECT_CALLING, SUCCESS_DUPLICATE
	}

	public static final String XTAASCORP_URL = "https://xtaascorp.herokuapp.com";
	public static final String XTAASCORP2_URL = "https://xtaascorp2.herokuapp.com";
	public static final String DEMANDSHORE_URL = "https://demandshore.herokuapp.com";
	public static final String PEACEFUL_URL = "https://peaceful-castle-6000.herokuapp.com";

	public final String CLIENT_REGION = "us-west-2";
	public final String AWS_ACCESS_KEY_ID = "AKIAJZ5EWMT6OAKGOGNQ";
	public final String AWS_SECRET_KEY = "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";

	public final String AWS_IAM_ACCESS_KEY_ID = "AKIAQLRQTJMRLYVBFP45";
	public final String AWS_IAM_SECRET_KEY = "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";
	
	public static final List<String> slices = Arrays.asList("slice0", "slice1", "slice2", "slice3",  "slice4", "slice5", "slice6", "slice10");
	// public static final List<String> indirect_slices = Arrays.asList("slice0",
	// "slice3", "slice4", "slice9", "slice10", "slice11", "slice12");
	public final String AUTH_MFA_ISSUER = "Xtaas Corp";
	public final String CALLABLE_COUNT = "CALLABLE_COUNT";

	// PLIVO CONSTANSTS
	public static final String PLIVO = "PLIVO";

	// Plivo Endpoint
	public static final String PLIVO_ENDPOINT_USERNAME = "PLIVO_ENDPOINT1";
	public static final String PLIVO_ENDPOINT_PASSWORD = "PLIVO_ENDPOINT2";
	public static final String PLIVO_ENDPOINT_ALIAS = "PLIVO_ENDPOINT_ALIAS";
	public static final String PLIVO_ENDPOINT_ID = "PLIVO_ENDPOINT_ID";

	// Telnyx Connection
	public static final String TELNYX_CONNECTION_USERNAME = "TELNYX_CONNECTION_USERNAME";
	public static final String TELNYX_CONNECTION_PASSWORD = "TELNYX_CONNECTION_PASSWORD";
	public static final String TELNYX_CONNECTION_ID = "TELNYX_CONNECTION_ID";

	// Plivo IVR messages
	public static final String PLIVO_ABANDON_OPTOUT_MSG = "Sorry, No agent available. Press 1 followed by pound sign to opt out";
	public static final String PLIVO_VM_LEFT_BY_DIALER = "Hi, We at XTaaS were trying to reach you. Please call us back at 415-403-2426";

	public static final String FILE_TYPE_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	public static final String TOKEN = "Token NTljNTFlNDAuNjlkOWU4ZjlmNWU5ZjQ5MjlmMDBmYTkyYjdiODhkODk=";

	public static final String DISCOVERURL = "https://api.everstring.com/v1/companies/discover";

	public static final String REALTIMEURL = "https://api.everstring.com/v1/companies/data_enrich";
	
	public static final String ABM_COUNT_REFRESH_EVENT = "abm_count_refresh";

	// Signalwire Endpoint
	public static final String SIGNALWIRE_ENDPOINT_USERNAME = "SIGNALWIRE_ENDPOINT_USERNAME";
	public static final String SIGNALWIRE_ENDPOINT_PASSWORD = "SIGNALWIRE_ENDPOINT_PASSWORD";
	public static final String SIGNALWIRE_ENDPOINT_ID = "SIGNALWIRE_ENDPOINT_ID";
	public static final String SIGNALWIRE_DOMAINAPP = "SIGNALWIRE_DOMAINAPP";
	public static final String SIGNALWIRE_MANUAL_CALLSID = "SIGNALWIRE_MANUAL_CALLSID";

	public static final String RELOAD_AGENT_INFO = "reload_agent_info";

	/*
	 * DATA_FEEDBACK_STATUSES is used to to take back up of prospectcalllog into
	 * prospectfeedback collection & to create data feedback report. Need to take
	 * care if we introduce any new status after WRAPUP_COMPLETE
	 */
	public static final List<String> DATA_FEEDBACK_STATUSES = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{
			add(ProspectCallStatus.WRAPUP_COMPLETE.name());
			add(ProspectCallStatus.QA_INITIATED.name());
			add(ProspectCallStatus.QA_REJECTED.name());
			add(ProspectCallStatus.QA_COMPLETE.name());
			add(ProspectCallStatus.SECONDARY_QA_INITIATED.name());
			add(ProspectCallStatus.SECONDARY_REVIEW.name());
			add(ProspectCallStatus.QA_WRAPUP_COMPLETE.name());
		}
	};

	public static final LinkedHashMap<String, String> DEFAULT_HEADERS_DATA_FEEDBACK = new LinkedHashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put("First Name", "firstName");
			put("Last Name", "lastName");
			put("Phone", "phone");
			put("Email ID", "email");
			put("Title", "title");
			put("Company Name", "company");
			put("Data Source", "source");
			put("Sub Status", "subStatus");
			put("Call Status Explanation", "callStatusExplanation");
			put("Recording URL", "recordingUrl");
			put("Updated Date", "updatedDate");
			put("Call Time", "callStartTime");
		}
	};

	public static final LinkedHashMap<String, String> DEFAULT_STATUS_MAPPING_DATA_FEEDBACK = new LinkedHashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put("ANSWERMACHINE", "The call reached prospect's Voicemail");
			put("CALLBACK", "Prospect/Gatekeeper asked the agent to Callback");
			put("SUBMITLEAD", "Prospect was interested in asset/whitepaper");
			put("CALLBACK_USER_GENERATED", "Prospect/Gatekeeper asked the agent to Callback");
			put("OTHER", "Failure call with Prospect Unreachable");
			put("GATEKEEPER_ANSWERMACHINE", "Agent talked to Gatekeeper/IVR but reached Prospect's Voicemail");
			put("NOTINTERESTED", "Prospect not interested in asset/whitepaper");
			put("DEADAIR", "Bad Number/Voice Issue");
			put("PROSPECT_UNREACHABLE", "Bad Data/Unable to reach the right party Prospect");
			put("FAILED_QUALIFICATION", "Agent talked to right Prospect but prospect didn't qualify for campaign");
			put("DNCL", "Prospect asked on call to be on DNC list");
			put("NO_CONSENT", "Prospect didn't give consent for further communication");
			put("CALLBACK_GATEKEEPER", "Prospect/Gatekeeper asked the agent to Callback");
		}
	};

	public static final List<String> US_COUNTRY = Arrays.asList("US", "USA", "UNITED STATES", "CANADA");
	public static final List<String> UK_COUNTRY = Arrays.asList("UK", "UNITED KINGDOM");
	public static final String TPS_UK_DNC = "TPS_UK_DNC";
	public static final String DNCSCRUB_US_DNC = "DNCSCRUB_US_DNC";
	public static final List<String> DNCSCRUB_CALLABLE_RESULTCODE = Arrays.asList("C", "W", "Y");

}
