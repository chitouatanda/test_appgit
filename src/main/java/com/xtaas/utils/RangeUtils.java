package com.xtaas.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.xtaas.range.*;

public class RangeUtils {
	

	public static int  checkRange(long leftBoundry, long rightBoundry) {
        IntervalTree<Integer> it = getSalutaryEmpRange();
       /* List<Integer> result1 = it.get(5L);
        List<Integer> result2 = it.get(10L);
        List<Integer> result3 = it.get(29L);*/
        List<Integer> result = it.get(leftBoundry, rightBoundry);
 
      /*  System.out.print("\nIntervals that contain 5L:");
        for (int r : result1)
            System.out.print(r + " ");
 
        System.out.print("\nIntervals that contain 10L:");
        for (int r : result2)
            System.out.print(r + " ");
 
        System.out.print("\nIntervals that contain 29L:");
        for (int r : result3)
            System.out.print(r + " ");
 
        System.out.print("\nIntervals that intersect (5L,15L):");*/
        for (int r : result) {
            System.out.print(r + " ");
            return r;
        }
        
        return 0;
	}
	
	
	private static IntervalTree<Integer> getSalutaryEmpRange(){
        IntervalTree<Integer> it = new IntervalTree<Integer>();
       
        it.addInterval(1L, 19L, 1);
        it.addInterval(20L, 99L, 2);
        it.addInterval(100L, 499L, 3);
        it.addInterval(500L, 999L, 4);
        it.addInterval(1000L, 4999L, 5);
        it.addInterval(5000L, 9999L, 6);
        it.addInterval(10000L, 100000L, 6);
        
        return it;
	}
}
