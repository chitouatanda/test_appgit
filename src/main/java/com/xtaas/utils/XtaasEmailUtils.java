/**
 * 
 */
package com.xtaas.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sendgrid.*;

import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;



import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.AuthTokenGeneratorService;
import com.xtaas.db.entity.Login;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.RestClientService;
import com.xtaas.web.dto.FileDTO;

/**
 * @author djain
 *
 */
public class XtaasEmailUtils  {

	private static final String userName = System.getenv("POSTMARK_API_TOKEN");
	private static final String password = System.getenv("POSTMARK_API_TOKEN");
	private static final String host = System.getenv("POSTMARK_SMTP_SERVER");
	// private static final String sendGridAPIKEY = System.getenv("sendGridAPIKEY");
	private static final String sendGridAPIKEY = "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";
	private static final String emailSendGridAPIKEY = "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";
	private static final Logger logger = LoggerFactory.getLogger(XtaasEmailUtils.class);
	private static AuthTokenGeneratorService authTokenGeneratorService = BeanLocator
			.getBean("authTokenGeneratorServiceImpl", AuthTokenGeneratorService.class);
	private static RestClientService restClientService = BeanLocator.getBean("restClientService",
			RestClientService.class);

	private String messageId = "";

	

	@SuppressWarnings("deprecation")
	public static void sendEmailSendGrid(EmailMessage message, String prospectInteractionSessionId, String campaignId,
			String prospectCallId) throws Exception {
		Email from = new Email(message.getFromEmail());
		from.setName(message.getFromName());
		String subject = message.getSubject();
		Content content = new Content("text/html", message.getMessage());

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Personalization personalization = new Personalization();
		for (int i = 0; i < message.getTo().size(); i++) {
			if (i == 0) {
				personalization.addTo(new Email(message.getTo().get(i)));
			} else {
				personalization.addCc(new Email(message.getTo().get(i)));
			}
		}
		personalization.addCustomArg("prospectInteractionSessionId", prospectInteractionSessionId);
		personalization.addCustomArg("campaignId", campaignId);
		personalization.addCustomArg("prospectCallId", prospectCallId);

		Mail mail = new Mail();
		mail.setFrom(from);
		mail.addContent(content);
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendEmailSendGrid() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}

	@SuppressWarnings("deprecation")
	public static void sendGridEmailCampaign(EmailMessage message, String prospectInteractionSessionId, String campaignId,
			String prospectCallId) throws Exception {
		Email from = new Email(message.getFromEmail());
		from.setName(message.getFromName());
		String subject = message.getSubject();
		Content content = new Content("text/html", message.getMessage());

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(emailSendGridAPIKEY, sendGridClient);
		Personalization personalization = new Personalization();
		for (int i = 0; i < message.getTo().size(); i++) {
			if (i == 0) {
				personalization.addTo(new Email(message.getTo().get(i)));
			} else {
				personalization.addCc(new Email(message.getTo().get(i)));
			}
		}
		personalization.addCustomArg("prospectInteractionSessionId", prospectInteractionSessionId);
		personalization.addCustomArg("campaignId", campaignId);
		personalization.addCustomArg("prospectCallId", prospectCallId);

		Mail mail = new Mail();
		mail.setFrom(from);
		mail.addContent(content);
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendEmailSendGrid() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void sendEmail(String to, String from, String subject, String message, List<File> filePaths) {
		Mail mail = new Mail();
		Email fromEmail = new Email(from);
		fromEmail.setName(from);
		Content content = new Content("text/html", message);
		mail.setFrom(fromEmail);
		mail.addContent(content);

		for (File fileData : filePaths) {
			try {
				Attachments attachments = new Attachments();
				String base64 = Base64.getEncoder().encodeToString(Files.readAllBytes(fileData.toPath()));
				// System.out.print(base64);
				attachments.setContent(base64);
				attachments.setType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				attachments.setContentId(fileData.getName());
				attachments.setFilename(fileData.getName());
				attachments.setDisposition("attachment");
				mail.addAttachments(attachments);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Personalization personalization = new Personalization();
		personalization.addTo(new Email(to));
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendMail() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}

	@SuppressWarnings("deprecation")
	public static void sendEmail(String to, String from, String subject, String message) {
		Mail mail = new Mail();
		Email fromEmail = new Email(from);
		fromEmail.setName(from);
		Content content = new Content("text/html", message);
		mail.setFrom(fromEmail);
		mail.addContent(content);

		Personalization personalization = new Personalization();
		personalization.addTo(new Email(to));
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendMail() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}

	@SuppressWarnings("deprecation")
	public static void sendEmail(List<String> to, String from, String subject, String message) {
		Mail mail = new Mail();
		Email fromEmail = new Email(from);
		fromEmail.setName(from);
		Content content = new Content("text/html", message);
		mail.setFrom(fromEmail);
		mail.addContent(content);

		Personalization personalization = new Personalization();
		for (String toMail : to) {
			personalization.addTo(new Email(toMail));
		}
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendMail() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}

	

	private static String getAuthToken() {
		return authTokenGeneratorService.generateUserToken(App.BACKEND_SUPPORT_SYSTEM.name(),
				XtaasUserUtils.getCurrentLoggedInUsername());
	}

	private static String getNotifierServiceUrl() {
		StringBuilder bssServerBaseUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getBssServerBaseUrl())
				.append("/api/notifier");
		return bssServerBaseUrl.toString();
	}

	public static void sendEmailWithAttachment(String to, String from, String subject, String message,
			List<FileDTO> files) {
		Mail mail = new Mail();
		Email fromEmail = new Email(from);
		fromEmail.setName(from);
		Content content = new Content("text/html", message);
		mail.setFrom(fromEmail);
		mail.addContent(content);
		try {
			for (FileDTO file : files) {
				Attachments attachments = new Attachments();
				attachments.setContent(file.getContent());
				attachments.setType(file.getType());
				attachments.setContentId(file.getName());
				attachments.setFilename(file.getName());
				attachments.setDisposition("attachment");
				mail.addAttachments(attachments);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Personalization personalization = new Personalization();
		personalization.addTo(new Email(to));
		personalization.setSubject(subject);
		mail.addPersonalization(personalization);

		@SuppressWarnings("deprecation")
		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		Client sendGridClient = new Client(defaultHttpClient);
		SendGrid sendGrid = new SendGrid(sendGridAPIKEY, sendGridClient);
		Request request = new Request();
		Response response = new Response();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sendGrid.api(request);
		} catch (Exception ex) {
			logger.error(
					"sendMail() : Error occurred while Sending email through SendGrid. Respone is : {} and Exception is : {} ",
					response, ex);
		}
	}
	
	public static void sendNukeAndCallableCountEmail(Map<String, Integer> map, Login login, String callableCount,Campaign campaign) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd ");
		String dateStr = sdf.format(date);
		String subject = campaign.getName()+" "+dateStr + " - Data pivot nuke count";
		String text = "<table width='100%' border='1' align='center'>" + "<tr align='center'>" + "<td><b> Name <b></td>"
				+ "<td><b>Nuke Count<b></td>" + "</tr>";
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			text = text + "<tr align='center'>" + "<td>" + entry.getKey() + "</td>" + "<td>" + entry.getValue()
					+ "</td>" + "</tr>";
		}
		text = text + "TOTAL CALLABLES COUNT : " + callableCount  + "</br>";
		XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", subject, text);
		logger.debug("Nuke pivots count email sent successfully to " + login.getEmail());
	}

}
