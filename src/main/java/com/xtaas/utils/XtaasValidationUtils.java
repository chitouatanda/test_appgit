package com.xtaas.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.CampaignContact;

public class XtaasValidationUtils {
    
    @Autowired
    private static CampaignContactRepository campaignContactRepository;
    
    @Autowired
    private static ProspectCallInteractionRepository prospectCallInteractionRepository;
    
    @Autowired
    private static ProspectCallLogRepository prospectCallLogRepository;
    
   
    
  
   
    
    /*
     *  This method is used for the particuler person is belong to US or not
     */
    public static  boolean isPersonBelongstoUS(String countryName) {
        if("United States".equals(countryName) || "US".equals(countryName) || "USA".equals(countryName)){
            return true;
        } else {
            return false;
        }
    }
    
  
    
    
    /*
     *  This method is used for validate Company Min Revenue (validate user enter revenue and zoominfo return )
     */
    public static boolean validateCompanyMinRevenue(double minRevenueFromZoominfo ,double minRevenueFromUser){
        boolean conditionCheck = true;
        if(minRevenueFromUser != 0){
            minRevenueFromUser = minRevenueFromUser*1000;
        }
        if(minRevenueFromUser != 0 && minRevenueFromUser > minRevenueFromZoominfo){
            String convertSearchMinRevenue = String.format("%1$.2f", minRevenueFromZoominfo);
            System.out.println("Comapany Min Revenue "+convertSearchMinRevenue+ " is greater then criteria Min Revenue. "+minRevenueFromUser+ " Skipping to next one.");
            return false;
        }  
        return conditionCheck;
        
    }
    
    /*
     *  This method is used for validate Company Max Revenue (validate user enter revenue and zoominfo return )
     */
    
    public static boolean validateCompanyMaxRevenue( double maxRevenueFromZoominfo, double maxRevenueFromUser){
        boolean conditionCheck = true;
        if(maxRevenueFromUser != 0){
            maxRevenueFromUser = maxRevenueFromUser*1000;
        }
        if(maxRevenueFromUser != 0 && maxRevenueFromUser < maxRevenueFromZoominfo){
            String convertSearchMaxRevenue = String.format("%1$.2f", maxRevenueFromZoominfo);
            System.out.println("Comapany Max Revenue "+convertSearchMaxRevenue+ " is greater then criteria Max Revenue. "+maxRevenueFromUser+ " Skipping to next one.");
            return false;
        }
        return conditionCheck;
        
    }
    
    
    /*
     *  This method is used for validate Company Min EmployeeCount (validate user enter revenue and zoominfo return EmployeeCount)
     */
    public static boolean validateCompanyMinEmployeeCount(double minEmployeeCountFromZoominfo, double minEmployeeCountFromUser){
        boolean conditionCheck = true;
        if(minEmployeeCountFromUser != 0 && minEmployeeCountFromUser > minEmployeeCountFromZoominfo){
            System.out.println("Comapany Min Emp "+minEmployeeCountFromZoominfo+ " is greater then criteria Min Emp. "+minEmployeeCountFromUser+ " Skipping to next one.");
            return false;
        }  
        return conditionCheck;
    }
    
    /*
     *  This method is used for validate Company Max EmployeeCount (validate user enter revenue and zoominfo return EmployeeCount)
     */
    public static boolean validateCompanyMaxEmployeeCount(double maxEmployeeCountFromZoominfo, double maxEmployeeCountFromUser){
        boolean conditionCheck = true;
        if(maxEmployeeCountFromUser != 0 && maxEmployeeCountFromUser < maxEmployeeCountFromZoominfo){
            System.out.println("Comapany Max Emp "+maxEmployeeCountFromZoominfo+ " is greater then criteria Max Emp. "+maxEmployeeCountFromUser+ " Skipping to next one.");
            return false;
        }
        return conditionCheck;
    }
    
    /*
     *  This method is used for to match industry  with top level indutry (validate user enter indutry and zoominfo return indutry )
     */
    public static boolean isTopLevelIndustryMatch(List<String> topLevelIndustryList, List<String> matchedIndustryList, int noOfIndustryMatch) {
        boolean conditionCheck = false;
        if (matchedIndustryList.size() == 0){
            return true;
        }
        List<String> contactListToMatchIndustry =topLevelIndustryList;
        if(contactListToMatchIndustry != null){
            for (int i = 0; i < matchedIndustryList.size(); i++) {
                for(int j = 0 ; j <contactListToMatchIndustry.size() ; j++){
                    if(j <  noOfIndustryMatch){
                        if(matchedIndustryList.get(i).equalsIgnoreCase(contactListToMatchIndustry.get(j))) {
                            return true;
                        }
                    }
                }
            }
        }
        System.out.println(matchedIndustryList +" Industry in not matching with "+contactListToMatchIndustry);
        return conditionCheck;
    }
    
    
    /*
     *  This method is used for to Management Level Match (validate user enter Management Level and zoominfo return Management Level )
     */
    public static boolean isManagementLevelMatch(String zoomInfoManagementLevel, List<String> userManagementLevel){
        boolean conditionCheck = false;
        Map<String,String> zoomResponseMgmtLevel = new HashMap<String, String>();
        zoomResponseMgmtLevel.put("C", "C-Level");
        zoomResponseMgmtLevel.put("VP", "VP-Level");
        zoomResponseMgmtLevel.put("DIRECTOR", "DIRECTOR");
        zoomResponseMgmtLevel.put("MANAGER", "MANAGER");
        zoomResponseMgmtLevel.put("OTHER", "Non-Manager");
        
        for (String managementLevel : userManagementLevel) {
        	
             if(zoomResponseMgmtLevel.get(managementLevel).equalsIgnoreCase(zoomInfoManagementLevel)){
                 return true;
             }
        }
        return conditionCheck;
    }
    
    /*
     *  This method is used for to Management Level Match (validate user enter Management Level and zoominfo return Management Level )
     */
    public static boolean isDepartmentMatch(String zoomInfoDepartment, List<String> userDepartmentList){
        boolean conditionCheck = false;
        for (String dept : userDepartmentList) {
             if(dept.equalsIgnoreCase(zoomInfoDepartment)){
                 return true;
             }
        }
        return conditionCheck;
    }
    
  
 
    
}
