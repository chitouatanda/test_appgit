/**
 * 
 */
package com.xtaas.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

/**
 * @author djain
 *
 */
public class XtaasDateUtils {
	
	public static final String DATETIME_FORMAT_DEFAULT = "MM/dd/yyyy HH:mm:ss";
	public static final String DATETIME_12HRFORMAT = "MM/dd/yyyy hh:mm:ss a";
	public static final String DATE_FORMAT_DEFAULT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_WITH_TIMEZONE_DEFAULT = "MM/dd/yyyyZ";
	public static final String DATETIME_FORMAT_WITH_TIMEZONE_DEFAULT = "MM/dd/yyyy HH:mm:ssZ";
	public static final String TZ_UTC = "UTC";
	
	/**
	 * Get the date object of specified Date
	 * @throws ParseException 
	 */
	public static Date getDate(String strDate, String format) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(format);

        // Get a Date object from the date string
        return dateFormat.parse(strDate);
	}
	
	
	public static Date convertTimeZoneOfDate(String dateToConvert, String sourceTZ, String destTZ, String format) throws ParseException {
		if (dateToConvert == null || dateToConvert.trim().isEmpty()) return null;
		
		String convertedDateAsString = convertTimeZone(dateToConvert, sourceTZ, destTZ, format);
		return convertStringToDate(convertedDateAsString, format);
	}

	/**
     * Converts time from one timezone to another
     * 
     * @param dateToConvert - String representation of Date to convert. It needs to be in the format passed in the format param.
     * @param sourceTZ - Time is currently in which TZ
     * @param destTZ - Convert time in this TZ
     * @param format - Date format in which dateToConvert is provided. This is also the format of the output
     * @return String representation after TZ conversion
     */
    public static String convertTimeZone(String dateToConvert, String sourceTZ, String destTZ, String format) throws ParseException
    {
    	if (dateToConvert == null || dateToConvert.trim().isEmpty()) return null;
	    SimpleDateFormat sdf = new SimpleDateFormat(format);
	     
    	if (sourceTZ != null)
    		sdf.setTimeZone(TimeZone.getTimeZone(sourceTZ));
    	else
    		sdf.setTimeZone(TimeZone.getDefault()); // default to server's timezone
    	
    	Date specifiedTime = sdf.parse(dateToConvert);
	     
	    // switch timezone
	    if (destTZ != null)
	    	sdf.setTimeZone(TimeZone.getTimeZone(destTZ));
	    else
	    	sdf.setTimeZone(TimeZone.getDefault()); // default to server's timezone
	     
	    return sdf.format(specifiedTime);
    }
    
    public static DateTime changeTimeZone(DateTime dateTimeToConvert, String destTZ) {
    	return dateTimeToConvert.withZone(DateTimeZone.forID(destTZ));
    }
    
    public static Date convertStringToDate(String dateToConvert, String format) throws ParseException {
	    SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(dateToConvert); 
    }
    
    public static String convertDateToString(Date date) {
    	return convertDateToString(date, "MMM dd, yyyy");
    }
    
    public static String convertDateToString(Date date, String format) {
    	return (new SimpleDateFormat(format).format(date));
    }
    
    /**
     * Calculates a new Date based on Timezone provided
     * 
     * @param dateToConvert - Date to convert
     * @param toTimeZone - Date to convert in which Timezone
     * @param format - Date format in which output is expected
     * @return String representation of date after TZ conversion
     */
    public static String convertToTimeZone(Date dateToConvert, String toTimeZone, String format) throws ParseException
    {
    	SimpleDateFormat df = new SimpleDateFormat(format);
    	df.setTimeZone(TimeZone.getTimeZone(toTimeZone));
    	return df.format(dateToConvert);
    }
    
    public static Date convertToTimeZone(Date dateToConvert, String toTimeZone) throws ParseException
    {
    	SimpleDateFormat df = new SimpleDateFormat(DATETIME_FORMAT_DEFAULT);
    	df.setTimeZone(TimeZone.getTimeZone(toTimeZone));
    	return convertStringToDate(df.format(dateToConvert), DATETIME_FORMAT_DEFAULT);
    }
    
    public static boolean isWeekend(Date dt) {
    	Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return (Calendar.SUNDAY == dayOfWeek || Calendar.SATURDAY == dayOfWeek);
    }
    
    /**
     * Returns date of DAY in next week. If DayofWeek passed is GregorianCalendar.MONDAY then it will return Date of Monday in next week. 
     * @param dayOfWeek : Day of Week in next week
     * @param hour : Time on the specified day in next week. Keep it 0 if no time t0 specify.
     * @return
     */
    public static Date getNextWeeksDate(int dayOfWeek, int hour)
    {
    	Calendar cal = Calendar.getInstance();
    	cal.add(GregorianCalendar.DAY_OF_MONTH, 7);
    	cal.set(GregorianCalendar.DAY_OF_WEEK, dayOfWeek);
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, 0);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getNextDaysDate(int hour) {
    	Calendar cal = Calendar.getInstance();
    	cal.add(GregorianCalendar.DAY_OF_MONTH, 1);
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, 0);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getDaysAfterToday(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(GregorianCalendar.DAY_OF_MONTH, days);
        cal.set(GregorianCalendar.HOUR_OF_DAY, 0);
        cal.set(GregorianCalendar.MINUTE, 0);
        cal.set(GregorianCalendar.SECOND, 0);
        return cal.getTime();
    }
    
    public static Date getYesterdaysDate(int hour) {
    	return getYesterdaysDate(hour, 0);
    }
    
    public static Date getYesterdaysDate(int hour, int minute) {
    	Calendar cal = Calendar.getInstance();
    	cal.add(GregorianCalendar.DAY_OF_MONTH, -1);
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, minute);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getDayBeforeYesterdayDate(int hour) {
    	return getDayBeforeYesterdayDate(hour, 0);
    }
    
    public static Date getDayBeforeYesterdayDate(int hour, int minute) {
    	Calendar cal = Calendar.getInstance();
    	cal.add(GregorianCalendar.DAY_OF_MONTH, -2);
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, minute);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return cal.getTime();
    }
    
    public static DateTime getDaysAfterSpecifiedDate(DateTime dateTime, String timezone, int days) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
        cal.setTimeInMillis(dateTime.getMillis());
        cal.add(GregorianCalendar.DAY_OF_MONTH, days);
        cal.set(GregorianCalendar.HOUR_OF_DAY, 0);
        cal.set(GregorianCalendar.MINUTE, 0);
        cal.set(GregorianCalendar.SECOND, 0);
        return new DateTime(cal.getTimeInMillis(), DateTimeZone.forID(timezone));
    }
      
    public static DateTime getNextDaysDateInTZ(int hour, String timezone) {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
    	cal.add(GregorianCalendar.DAY_OF_MONTH, 1);
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, 0);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return new DateTime(cal.getTimeInMillis(), DateTimeZone.forID(timezone));
    }
    
    public static Date getBizTime() throws ParseException{
    	Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
	      //getTime() returns the current date in default time zone
	      Date date = calendar.getTime();
	      int day = calendar.get(Calendar.DATE);
	      //Note: +1 the month for current month
	      int month = calendar.get(Calendar.MONTH) + 1;
	      int year = calendar.get(Calendar.YEAR);
	      
	      String d = month+"/"+day+"/"+year+" 00:00:00";
		
		Date startDate = getDate(d, DATETIME_FORMAT_DEFAULT);
		Date startDateInUTC = convertToTimeZone(startDate, TZ_UTC);
		return startDateInUTC;
		//System.out.println(startDateInUTC);
    }
    
    public static Date getCurrentDaysDate(int hour) {
    	Calendar cal = Calendar.getInstance();
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, 0);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return cal.getTime();
    }
    
    public static DateTime getCurrentDateInTZ(int hour, String timezone) {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
    	cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
    	cal.set(GregorianCalendar.MINUTE, 0);
    	cal.set(GregorianCalendar.SECOND, 0);
    	return new DateTime(cal.getTimeInMillis(), DateTimeZone.forID(timezone));
    }
    
    public static long convertHoursInMillis(int hours) {
    	return (long) (hours*60*60*1000);
    }
    
    public static Interval getWeekIntervalInTZ(int week, String timezone) {
    	
    	Calendar cal = Calendar.getInstance();// Get calendar set to current date and time
    	cal.set(Calendar.WEEK_OF_YEAR, week);//set the calendar to input week
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);// Set the calendar to sunday of the current week
    	
    	//get from date in client timezone
    	DateTime fromDateInCientTZ = new DateTime(cal.getTimeInMillis(), DateTimeZone.forID(timezone));
    	//Set previous week sunday date to 12.00 A.M as start date
    	DateTime fromDate = new DateTime(fromDateInCientTZ.getYear(), fromDateInCientTZ.getMonthOfYear(), fromDateInCientTZ.getDayOfMonth(), 
				0, 0);
    	
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);// Set the calendar to sunday of the current week
    	//get from date in client timezone
    	DateTime toDateInClientTZ = new DateTime(cal.getTimeInMillis(), DateTimeZone.forID(timezone));
    	//Set previous week sunday date to 12.00 A.M as start date
    	DateTime toDate = new DateTime(toDateInClientTZ.getYear(), toDateInClientTZ.getMonthOfYear(), toDateInClientTZ.getDayOfMonth(), 
				23, 59);
    	
    	//Convert from date of client to UTC timezone
    	DateTime fromDateInUTC = XtaasDateUtils.changeTimeZone(fromDate, DateTimeZone.UTC.getID());
    	//Convert end date of client to UTC timezone
    	DateTime toDateInUTC =XtaasDateUtils.changeTimeZone(toDate, DateTimeZone.UTC.getID());;
    	return new Interval(fromDateInUTC,toDateInUTC);
    }
    
    
    /**
     * 
     * @param timeZone
     * @return an Interval having todays date starting at 12 A.M to 11.59 P.M
     */
    public static Interval getDateTimeInterval(Date date, String timeZone) {
		//current date in UTC format
		//DateTime currentDate = new DateTime();
		//change UTC date to client TimeZone format
		DateTime currentDateInClientTZ = XtaasDateUtils.changeTimeZone(new DateTime(date), timeZone);
		//Set client TimeZone date to 12.00 A.M as start date
		DateTime fromDateInClientTZ = new DateTime(currentDateInClientTZ.getYear(), currentDateInClientTZ.getMonthOfYear(), currentDateInClientTZ.getDayOfMonth(), 
				0, 0, DateTimeZone.forID(timeZone));
		//Set client TimeZone date to 23.59 P.M as end date
		DateTime toDateInClientTZ = new DateTime(currentDateInClientTZ.getYear(), currentDateInClientTZ.getMonthOfYear(), currentDateInClientTZ.getDayOfMonth(), 
				23, 59, DateTimeZone.forID(timeZone));
		//Convert from date of client to UTC timezone
		DateTime fromDateInUTC = XtaasDateUtils.changeTimeZone(fromDateInClientTZ, DateTimeZone.UTC.getID());
		//Convert end date of client to UTC timezone
		DateTime toDateInUTC =XtaasDateUtils.changeTimeZone(toDateInClientTZ, DateTimeZone.UTC.getID());;
		return new Interval(fromDateInUTC,toDateInUTC);
	}
	
	public static Interval getDateTimeIntervalForTeamNotice(Date date, String timeZone) {
		DateTime currentDateInClientTZ = XtaasDateUtils.changeTimeZone(new DateTime(date), timeZone);
		//Set client TimeZone date to 12.00 A.M as start date
		DateTime fromDateInClientTZ = new DateTime(currentDateInClientTZ.getYear(), currentDateInClientTZ.getMonthOfYear(), currentDateInClientTZ.getDayOfMonth(), currentDateInClientTZ.getHourOfDay(), currentDateInClientTZ.getMinuteOfHour(), DateTimeZone.forID(timeZone));
		//Set client TimeZone date to 23.59 P.M as end date
		// DateTime toDateInClientTZ = new DateTime(currentDateInClientTZ.getYear(), currentDateInClientTZ.getMonthOfYear(), currentDateInClientTZ.getDayOfMonth(), 
		// currentDateInClientTZ.getHourOfDay() + 12, 59, DateTimeZone.forID(timeZone));
		DateTime toDateInClientTZ = fromDateInClientTZ.plusHours(12);
		//Convert from date of client to UTC timezone
		DateTime fromDateInUTC = XtaasDateUtils.changeTimeZone(fromDateInClientTZ, DateTimeZone.UTC.getID());
		//Convert end date of client to UTC timezone
		DateTime toDateInUTC =XtaasDateUtils.changeTimeZone(toDateInClientTZ, DateTimeZone.UTC.getID());;
		return new Interval(fromDateInUTC,toDateInUTC);
	}
    
    /**
     * 
     * @param fromDate,
     * @param toDate
     * @return an Interval having from date starting at 12 A.M and to date ending at 11.59 P.M
     */
    public static Interval getDateTimeInterval(Date fromDate, Date toDate, String timeZone) {
		DateTime fromDateTime = XtaasDateUtils.changeTimeZone(new DateTime(fromDate), timeZone);
    	DateTime toDateTime = XtaasDateUtils.changeTimeZone(new DateTime(toDate), timeZone);
		
		//Set client TimeZone from date to 12.00 A.M as start date
		DateTime fromDateInClientTZ = new DateTime(fromDateTime.getYear(), fromDateTime.getMonthOfYear(), fromDateTime.getDayOfMonth(), 0, 0, DateTimeZone.forID(timeZone));
		//Set client TimeZone to date to 23.59 P.M as end date
		DateTime toDateInClientTZ = new DateTime(toDateTime.getYear(), toDateTime.getMonthOfYear(), toDateTime.getDayOfMonth(), 23, 59, DateTimeZone.forID(timeZone));
		
		//Convert from date of client to UTC timezone
		DateTime fromDateInUTC = XtaasDateUtils.changeTimeZone(fromDateInClientTZ, DateTimeZone.UTC.getID());
		//Convert end date of client to UTC timezone
		DateTime toDateInUTC =XtaasDateUtils.changeTimeZone(toDateInClientTZ, DateTimeZone.UTC.getID());;
		
		return new Interval(fromDateInUTC,toDateInUTC);
	}
    
    
    public static Interval getDateTimeInterval(DateTime fromDateTime, DateTime toDateTime, String timeZone) {
        //Set client TimeZone from date to 12.00 A.M as start date
        DateTime fromDateInClientTZ = new DateTime(fromDateTime.getYear(), fromDateTime.getMonthOfYear(), fromDateTime.getDayOfMonth(), 0, 0, DateTimeZone.forID(timeZone));
        //Set client TimeZone to date to 23.59 P.M as end date
        DateTime toDateInClientTZ = new DateTime(toDateTime.getYear(), toDateTime.getMonthOfYear(), toDateTime.getDayOfMonth(), 23, 59, DateTimeZone.forID(timeZone));
        
        //Convert from date of client to UTC timezone
        DateTime fromDateInUTC = XtaasDateUtils.changeTimeZone(fromDateInClientTZ, DateTimeZone.UTC.getID());
        //Convert end date of client to UTC timezone
        DateTime toDateInUTC =XtaasDateUtils.changeTimeZone(toDateInClientTZ, DateTimeZone.UTC.getID());;
        
        return new Interval(fromDateInUTC,toDateInUTC);
    }
    
    public static Date getNextDate(Date  date, int days) {
		  Calendar calendar = Calendar.getInstance();
		  calendar.setTime(date);
		  calendar.add(Calendar.DAY_OF_YEAR, days);
		  return calendar.getTime(); 
	}
	
	public static Calendar getPreviousDate(final Calendar from, final long count) {
	    for (int daysBack = 0; daysBack < count; ++daysBack) {
	        do {
	            from.add(Calendar.DAY_OF_YEAR, -1);
	        } while(isWeekend(from));
	    }
	    return from;
	}
	
	public static Calendar getNextDateForGivenDays(final Calendar from, final long count) {
		for (int daysBack = 0; daysBack < count; ++daysBack) {
			do {
				from.add(Calendar.DAY_OF_YEAR, +1);
			} while (isWeekend(from));
		}
		return from;
	}
	
	public static String getDayName(Date time) {
		SimpleDateFormat simp = new SimpleDateFormat("EEEE");
		return simp.format(time);
	}

	public static boolean isWeekend(Calendar cal) {
	    return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
	           cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}
	
	public static Date getDateAtTime(Date date, int hours, int minutes, int seconds, int milliSeconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, seconds);
		cal.set(Calendar.MILLISECOND, milliSeconds);
		return cal.getTime();
	}
	
	public static Date getStartDateOfMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public static Date getEndDateOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}
	
	public static Date getTimeBeforeGivenHours(Date date, int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.HOUR, -hours);
		return cal.getTime();
	}
	
	public static long getTimeInSeconds(Date time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		long timeInSeconds = (calendar.get(Calendar.SECOND) + calendar.get(Calendar.MINUTE) * 60
				+ calendar.get(Calendar.HOUR) * 3600);
		return timeInSeconds;
	}
	
	public static Date getTimeBeforeGivenMinutes(Date date, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.MINUTE, -minutes);
		return cal.getTime();
	}

	public static Date yesterdayDate() {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}
    
	/**
	 * Returns Pacific TZ date in YYYYMMDD format
	 * 
	 * @return
	 */
	public static String getCurrentDateInPacificTZinYMD() {
		String strCurrDateInPacificTZ = null;
		try {
			strCurrDateInPacificTZ = XtaasDateUtils.convertToTimeZone(new Date(), XtaasConstants.PACIFIC_TZ,
					"yyyyMMdd");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return strCurrDateInPacificTZ;
	}
	
	public static long getTimeDiffrenceInSeconds(Date startTime, Date endTime) {
		return XtaasDateUtils.getTimeInSeconds(endTime) - XtaasDateUtils.getTimeInSeconds(startTime);
	}
	
	public static long getTimeDiffrenceInMinutes(Date startTime, Date endTime) {
		long diffInMilliSeconds = Math.abs(startTime.getTime() - endTime.getTime());
		long diffInMinutes = (diffInMilliSeconds / (60 * 1000)) % 60;
		return diffInMinutes;
	}
	
	public static Date getDateTimeAfterGivenMinutes(Date time, int minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		calendar.add(Calendar.MINUTE, minutes);
		return calendar.getTime();
	}
}