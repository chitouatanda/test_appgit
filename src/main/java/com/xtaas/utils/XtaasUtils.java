package com.xtaas.utils;

import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.logging.SplunkLoggingUtils;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;

public class XtaasUtils {

	private static final Logger logger = LoggerFactory.getLogger(XtaasUtils.class);
	private static final Pattern ASCII_PATTERN = Pattern.compile("[^\\p{ASCII}]");
	//private static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	private static final String COUNTRYDIALCODE_US = "+1";

	public static String getFormattedPhoneNumber(String phoneNumber) {
		if (phoneNumber == null)
			return null;

		String formattedPhoneNumber = phoneNumber;
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars

		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = normalizedPhoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3"); // returns
																													// (415)
																													// 403-2426
		} else if (normalizedPhoneNumber.length() > 10) {
			formattedPhoneNumber = normalizedPhoneNumber.replaceFirst("(\\d+)(\\d{3})(\\d{3})(\\d{4})",
					"+$1 ($2) $3-$4"); // returns +91 (774) 400-3867
		}

		return formattedPhoneNumber;
	}

	/*
	 * SIP forwarding to ThinQ from Twilio Rest Client seems to reject number w/o
	 * country code and other formatting. This method should strip all formatting
	 * and add country code. For now it will check whether the country is US and add
	 * 1 else return null. Assumption. given number is a valid US number. doesn't
	 * validate.
	 */
	public static String getSimpleFormattedUSPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return null;
		}
		String normalizedPhoneNumber = phoneNumber.replaceAll("[^\\d]", ""); // removes all non-numeric chars
		String formattedPhoneNumber = normalizedPhoneNumber;
		if (normalizedPhoneNumber.length() == 10) {
			formattedPhoneNumber = COUNTRYDIALCODE_US + normalizedPhoneNumber;
		} else {
			formattedPhoneNumber = "+" + normalizedPhoneNumber;
		}
		return formattedPhoneNumber;
	}

	public static boolean isPureAscii(String str) {
		CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
		return asciiEncoder.canEncode(str);
	}

	public static String removeNonAsciiChars(String str) {
		if (!isPureAscii(str)) {
			return ASCII_PATTERN.matcher(Normalizer.normalize(str, Normalizer.Form.NFD)).replaceAll(" ");
		} else {
			return str;
		}
	}

	public static ProspectCallLog getQualityBucket(ProspectCallLog prospectCallLog) {
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null
				&& prospectCallLog.getProspectCall().getProspect() != null) {
			ProspectCall prospectCall = prospectCallLog.getProspectCall();
			Prospect prospect = prospectCall.getProspect();
			StringBuffer stringBuffer = new StringBuffer();
			int qualityBucket = 0;
			String employeeWeightage = "1";
			String managementWeightage = "1";
			String timezoneWeightage = getStateWeight(prospect.getTimeZone());
			String directPhoneWeightage = "2";
			if (prospect.isDirectPhone()) {
				directPhoneWeightage = "1";
			}
			stringBuffer.append(prospectCallLog.getStatus());
			stringBuffer.append("|");
			if (prospect.getCustomAttributeValue("minEmployeeCount") instanceof Number) {
				Number n1 = (Number) prospect.getCustomAttributeValue("minEmployeeCount");
				stringBuffer.append(getBoundry(n1, "min"));
				stringBuffer.append("-");
				employeeWeightage = getEmployeeWeight(n1);
			}
			if (prospect.getCustomAttributeValue("maxEmployeeCount") instanceof Number) {
				Number n2 = (Number) prospect.getCustomAttributeValue("maxEmployeeCount");
				stringBuffer.append(getBoundry(n2, "max"));
			}
			stringBuffer.append("|");
			stringBuffer.append(getMgmtLevel(prospect.getManagementLevel()));
			managementWeightage = getMgmtLevelWeight(prospect.getManagementLevel());
			if (!employeeWeightage.isEmpty() && !managementWeightage.isEmpty() && !timezoneWeightage.isEmpty()) {
				if (directPhoneWeightage != null) {
					String sortString = directPhoneWeightage + timezoneWeightage + managementWeightage
							+ employeeWeightage;
					qualityBucket = Integer.parseInt(sortString);
				}
			}
			prospectCall.setQualityBucket(stringBuffer.toString());
			prospectCall.setQualityBucketSort(qualityBucket);
		}
		return prospectCallLog;
	}

	public static String getEmployeeWeight(Number number) {
		String weightage = "12";
		if (number.intValue() < 5) {
			weightage = "01";
		} else if (number.intValue() < 10) {
			weightage = "02";
		} else if (number.intValue() < 20) {
			weightage = "03";
		} else if (number.intValue() < 50) {
			weightage = "04";
		} else if (number.intValue() < 100) {
			weightage = "05";
		} else if (number.intValue() < 250) {
			weightage = "06";
		} else if (number.intValue() < 500) {
			weightage = "07";
		} else if (number.intValue() < 1000) {
			weightage = "08";
		} else if (number.intValue() < 5000) {
			weightage = "09";
		} else if (number.intValue() < 10000) {
			weightage = "10";
		} else if (number.intValue() < 11000) {
			weightage = "11";
		}
		return weightage;
	}

	/**
	 * 1-5 5-10 10-20 20-50 50-100 100-250 250-500 500-1000 1000-5000 5000-10000
	 * 10000-11000
	 */
	public static String getBoundry(Number number, String minMax) {
		String boundryValue = "11000";
		if (minMax.equalsIgnoreCase("min")) {
			if (number.intValue() < 5) {
				boundryValue = "1";
			} else if (number.intValue() < 10) {
				boundryValue = "5";
			} else if (number.intValue() < 20) {
				boundryValue = "10";
			} else if (number.intValue() < 50) {
				boundryValue = "20";
			} else if (number.intValue() < 100) {
				boundryValue = "50";
			} else if (number.intValue() < 250) {
				boundryValue = "100";
			} else if (number.intValue() < 500) {
				boundryValue = "250";
			} else if (number.intValue() < 1000) {
				boundryValue = "500";
			} else if (number.intValue() < 5000) {
				boundryValue = "1000";
			} else if (number.intValue() < 10000) {
				boundryValue = "5000";
			} else if (number.intValue() < 11000) {
				boundryValue = "10000";
			}
		} else if (minMax.equalsIgnoreCase("max")) {
			if (number.intValue() <= 5) {
				boundryValue = "5";
			} else if (number.intValue() <= 10) {
				boundryValue = "10";
			} else if (number.intValue() <= 20) {
				boundryValue = "20";
			} else if (number.intValue() <= 50) {
				boundryValue = "50";
			} else if (number.intValue() <= 100) {
				boundryValue = "100";
			} else if (number.intValue() <= 250) {
				boundryValue = "250";
			} else if (number.intValue() <= 500) {
				boundryValue = "500";
			} else if (number.intValue() <= 1000) {
				boundryValue = "1000";
			} else if (number.intValue() <= 5000) {
				boundryValue = "5000";
			} else if (number.intValue() <= 10000) {
				boundryValue = "10000";
			} else if (number.intValue() <= 100000) {
				boundryValue = "11000";
			}
		}
		return boundryValue;
	}

	public static String getMgmtLevel(String managementLevel) {
		String mgmtLevel = "C-Level";
		if (managementLevel != null) {
			if (managementLevel.equalsIgnoreCase("C-Level") || managementLevel.equalsIgnoreCase("C_EXECUTIVES")) {
				mgmtLevel = "C-Level";
			} else if (managementLevel.equalsIgnoreCase("VP-Level")
					|| managementLevel.equalsIgnoreCase("VP_EXECUTIVES")) {
				mgmtLevel = "VP-Level";
			} else if (managementLevel.equalsIgnoreCase("Director")) {
				mgmtLevel = "DIRECTOR";
			} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
				mgmtLevel = "MANAGER";
			} else {
				mgmtLevel = "Non-Manager";
			}
		}
		return mgmtLevel;
	}
	
	public static String convertStringtoMD5Hex(String str) {
		String md5String = DigestUtils
	  		      .md5Hex(str);
		return md5String;
	}
	
	public static boolean validateEmailId(String emailId) {
		
		//Regular Expression   
        String regex = "^(.+)@(.+)$";  
        //Compile regular expression to get the pattern  
        Pattern pattern = Pattern.compile(regex);  
        //Iterate emails array list  
        //Create instance of matcher   
        Matcher matcher = pattern.matcher(emailId);  
        // System.out.println(email +" : "+ matcher.matches()+"\n");   
		
		return matcher.matches();
	}

	public static String getMgmtLevelWeight(String managementLevel) {
		String weightage = "5";
		if (managementLevel != null) {
			if (managementLevel.equalsIgnoreCase("C-Level") || managementLevel.equalsIgnoreCase("C_EXECUTIVES")) {
				weightage = "5";
			} else if (managementLevel.equalsIgnoreCase("VP-Level")
					|| managementLevel.equalsIgnoreCase("VP_EXECUTIVES")) {
				weightage = "4";
			} else if (managementLevel.equalsIgnoreCase("Director")) {
				weightage = "3";
			} else if (managementLevel.equalsIgnoreCase("MANAGER")) {
				weightage = "2";
			} else {
				weightage = "1";
			}
		}
		return weightage;
	}
	
	public static String getSourceWeightage(String source) {
		String sourceWeight = "";
//		if(source!=null 
//    			&& !source.isEmpty()
//    			&& source.length()>4 ) {
//    		boolean isNumeric = false;
//    		//String temp = b.substring(0,4);
//    		int index = source.indexOf("_", 0);
//    		String value = source.substring(0,index);
//    		try {
//    			Integer.parseInt(value);
//    			isNumeric= true;
//    		}catch(NumberFormatException nfe) {
//    			isNumeric = false;
//    		}
//    		if(isNumeric) {
//    			int weight = 100 - Integer.parseInt(source);
//    			sourceWeight =  String.valueOf(weight);
//    		}
//    		
//    	}
		return sourceWeight;
	}

	public static String getStateWeight(String timeZone) {
		String est = "US/Eastern";
		String cst = "US/Central";
		String mst = "US/Mountain";
		String pst = "US/Pacific";
		String weightage = "5";
		if (timeZone != null) {
			if (timeZone.equalsIgnoreCase(pst)) {
				weightage = "1";
			} else if (timeZone.equalsIgnoreCase(mst)) {
				weightage = "2";
			} else if (timeZone.equalsIgnoreCase(cst)) {
				weightage = "3";
			} else if (timeZone.equalsIgnoreCase(est)) {
				weightage = "4";
			}
		}
		return weightage;
	}

	public static boolean isCompanySuppressed(String companyName, String domain, List<String> domains,
			List<String> companies) {
		boolean suppressed = false;
		if (domain != null && !domain.isEmpty()) {
			domain = getSuppressedHostName(domain);
		}
		if (domain != null && !domain.isEmpty() && domains != null && domains.contains(domain.toLowerCase())) {
			suppressed = true;
		} else if (companyName != null && !companyName.isEmpty() && companies != null
				&& companies.contains(companyName.toLowerCase())) {
			suppressed = true;
		}
		return suppressed;
	}
	public static boolean isEmailSuppresed(String email, List<String> emailAddresses
			) {
		boolean suppressed = false;
		
		if (email != null && !email.isEmpty() && emailAddresses != null && emailAddresses.contains(email.toLowerCase())) {
			suppressed = true;
		
		}
		return suppressed;
	}
	
	/*
	 * public static boolean isEmailSuppressed(String emailId,List<String> emailIds)
	 * 
	 * { if (!emailId.isEmpty() && !emailIds.isEmpty()) {
	 * 
	 * for (String string : emailIds) {
	 * 
	 * if (string.equalsIgnoreCase(emailId)) {
	 * 
	 * //throw new IllegalArgumentException("  email is in suppression list  ");
	 * 
	 * return true; } }
	 * 
	 * }
	 * 
	 * return false;
	 * 
	 * }
	 */

	public static String getSuppressedHostName(String domain) {
		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}

	public static LinkedHashMap<String, CRMAttribute> addDefaultQuestions(
			LinkedHashMap<String, CRMAttribute> questionAttributeMap) {
		ArrayList<PickListItem> answers = new ArrayList<PickListItem>();
		CRMAttribute departmentCrm = new CRMAttribute();
		departmentCrm.setSystemName("DEPARTMENT");
		departmentCrm.setLabel("DEPARTMENT");
		departmentCrm.setLength(512);
		departmentCrm.setPickList(answers);
		departmentCrm.setProspectBeanPath("Prospect.department");
		departmentCrm.setCrmDataType("string");
		departmentCrm.setQuestionSkin("Is Department valid?");
		departmentCrm.setAttributeType("DEPARTMENT");
		if (!questionAttributeMap.containsKey("DEPARTMENT")) {
			questionAttributeMap.put("DEPARTMENT", departmentCrm);
		}

		CRMAttribute industryCrm = new CRMAttribute();
		industryCrm.setSystemName("INDUSTRY");
		industryCrm.setLabel("INDUSTRY");
		industryCrm.setLength(120);
		industryCrm.setPickList(answers);
		industryCrm.setProspectBeanPath("Prospect.Industry");
		industryCrm.setCrmDataType("string");
		industryCrm.setQuestionSkin("Is Industry valid?");
		industryCrm.setAttributeType("INDUSTRY");
		if (!questionAttributeMap.containsKey("INDUSTRY")) {
			questionAttributeMap.put("INDUSTRY", industryCrm);
		}

		CRMAttribute employeeCrm = new CRMAttribute();
		employeeCrm.setSystemName("EMPLOYEE");
		employeeCrm.setLabel("EMPLOYEE");
		employeeCrm.setLength(50);
		employeeCrm.setPickList(answers);
		employeeCrm.setProspectBeanPath("Prospect.CustomAttributes['CompanySize']");
		employeeCrm.setCrmDataType("string");
		employeeCrm.setQuestionSkin("Employee Range");
		employeeCrm.setAttributeType("EMPLOYEE");
		if (!questionAttributeMap.containsKey("EMPLOYEE")) {
			questionAttributeMap.put("EMPLOYEE", employeeCrm);
		}

		CRMAttribute revenueCrm = new CRMAttribute();
		revenueCrm.setSystemName("REVENUE");
		revenueCrm.setLabel("REVENUE");
		revenueCrm.setLength(50);
		revenueCrm.setPickList(answers);
		revenueCrm.setProspectBeanPath("Prospect.CustomAttributes['AnnualRevenue']");
		revenueCrm.setCrmDataType("string");
		revenueCrm.setQuestionSkin("Annual Revenue?");
		revenueCrm.setAttributeType("REVENUE");
		if (!questionAttributeMap.containsKey("REVENUE")) {
			questionAttributeMap.put("REVENUE", revenueCrm);
		}

		CRMAttribute titleCrm = new CRMAttribute();
		titleCrm.setSystemName("VALID_TITLE");
		titleCrm.setLabel("VALID_TITLE");
		titleCrm.setLength(512);
		titleCrm.setPickList(answers);
		titleCrm.setProspectBeanPath("");
		titleCrm.setCrmDataType("string");
		titleCrm.setQuestionSkin("Is Title Valid?");
		titleCrm.setAttributeType("VALID_TITLE");
		if (questionAttributeMap.containsKey("MANAGEMENT_LEVEL")) {
			questionAttributeMap.remove("MANAGEMENT_LEVEL");
			questionAttributeMap.put("VALID_TITLE", titleCrm);
		} else {
			questionAttributeMap.put("VALID_TITLE", titleCrm);
		}

		CRMAttribute searchTitle = new CRMAttribute();
		searchTitle.setSystemName("SEARCH_TITLE");
		searchTitle.setLabel("SEARCH_TITLE");
		searchTitle.setLength(300);
		searchTitle.setPickList(answers);
		searchTitle.setProspectBeanPath("");
		searchTitle.setCrmDataType("string");
		searchTitle.setQuestionSkin("Person Title");
		searchTitle.setAttributeType("SEARCH_TITLE");
		if (!questionAttributeMap.containsKey("SEARCH_TITLE")) {
			questionAttributeMap.put("SEARCH_TITLE", searchTitle);
		}

		return questionAttributeMap;
	}
	
	public static List<String> getUSStateList() {
		List<String> states = new ArrayList<String>();
		states.add("Alabama");	
		states.add("Alaska");	
		states.add("Arizona");
		states.add("Arkansas");
		states.add("California");
		states.add("Colorado");
		states.add("Connecticut");
		states.add("Delaware");
		states.add("District of Columbia");
		states.add("Florida");
		states.add("Georgia");
		states.add("Hawaii");
		states.add("Idaho");
		states.add("Illinois");
		states.add("Indiana");
		states.add("Iowa");
		states.add("Kansas");
		states.add("Kentucky");
		states.add("Louisiana");
		states.add("Maine");
		states.add("Maryland");
		states.add("Massachusetts");
		states.add("Michigan");
		states.add("Minnesota");
		states.add("Mississippi");
		states.add("Missouri");
		states.add("Montana");
		states.add("Nebraska");
		states.add("Nevada");
		states.add("New Hampshire");
		states.add("New Jersey");
		states.add("New Mexico");
		states.add("New York");
		states.add("North Carolina");
		states.add("North Dakota");
		states.add("Ohio");
		states.add("Oklahoma");
		states.add("Oregon");
		states.add("Pennsylvania");
		states.add("Rhode Island");
		states.add("South Carolina");
		states.add("South Dakota");
		states.add("Tennessee");
		states.add("Texas");
		states.add("Utah");
		states.add("Vermont");
		states.add("Virginia");
		states.add("Washington");
		states.add("West Virginia");
		states.add("Wisconsin");
		states.add("Wyoming");
		return states;
	}
	
	public static List<String> getTenUSStates() {
		List<String> states = new ArrayList<String>();
		states.add("Alabama");	
		states.add("Alaska");	
		states.add("Arizona");
		states.add("Arkansas");
		states.add("California");
		states.add("Colorado");
		states.add("Connecticut");
		states.add("Delaware");
		states.add("District of Columbia");
		states.add("Florida");
		states.add("Georgia");
		return states;
	}
	
	
	 public static LinkedHashMap<String, String[]> getRevenuSortedeMap(String[] revarr){
		 List<String> revSearch = Arrays.asList(revarr);
			LinkedHashMap<String,String[]> revMap = new LinkedHashMap<String, String[]>();
			/////
			if(revSearch.contains("0-$10M")) {
				 String[] revArray1 = new String[2];
				 revArray1[0]="0";
				 revArray1[1]="10000";
				 revMap.put("0-$10M", revArray1);
			}
			if(revSearch.contains("$5M-$10M")) {	
				 String[] revArray2 = new String[2];
				 revArray2[0]="5000";
				 revArray2[1]="10000";
				 revMap.put("$5M-$10M", revArray2);
			}
			if(revSearch.contains("$10M-$25M")) {
				 String[] revArray3 = new String[2];
				 revArray3[0]="10000";
				 revArray3[1]="25000";
				 revMap.put("$10M-$25M", revArray3);
			}
			if(revSearch.contains("$25M-$50M")) {
				 String[] revArray4 = new String[2];
				 revArray4[0]="25000";
				 revArray4[1]="50000";
				 revMap.put("$25M-$50M", revArray4);
			}
			if(revSearch.contains("$50M-$100M")) {	 
				 String[] revArray5 = new String[2];
				 revArray5[0]="50000";
				 revArray5[1]="100000";
				 revMap.put("$50M-$100M", revArray5);
			}
			if(revSearch.contains("$100M-$250M")) {
				 String[] revArray6 = new String[2];
				 revArray6[0]="100000";
				 revArray6[1]="250000";
				 revMap.put("$100M-$250M", revArray6);
			}
			if(revSearch.contains("$250M-$500M")) {
				 String[] revArray7 = new String[2];
				 revArray7[0]="250000";
				 revArray7[1]="500000";
				 revMap.put("$250M-$500M", revArray7);
			}
			if(revSearch.contains("$500M-$1B")) {
				 String[] revArray8 = new String[2];
				 revArray8[0]="500000";
				 revArray8[1]="1000000";
				 revMap.put("$500M-$1B", revArray8);
			}
			if(revSearch.contains("$1B-$5B")) {
				 String[] revArray9 = new String[2];
				 revArray9[0]="1000000";
				 revArray9[1]="5000000";
				 revMap.put("$1B-$5B", revArray9);
			}
			if(revSearch.contains("More than $5Billion")) {
				 String[] revArray10 = new String[2];
				 revArray10[0]="5000000";
				 revArray10[1]="0";
				 revMap.put("More than $5Billion", revArray10);
			}
			
			return revMap;
		}
	 
	public static Map<String, String[]> getRevenueMap(){
		Map<String,String[]> revMap = new HashMap<String, String[]>();
		/////
		 String[] revArray1 = new String[2];
		 revArray1[0]="0";
		 revArray1[1]="10000";
		 revMap.put("0-$10M", revArray1);
		
		 String[] revArray2 = new String[2];
		 revArray2[0]="5000";
		 revArray2[1]="10000";
		 revMap.put("$5M-$10M", revArray2);
		 
		 String[] revArray3 = new String[2];
		 revArray3[0]="10000";
		 revArray3[1]="25000";
		 revMap.put("$10M-$25M", revArray3);
		 
		 String[] revArray4 = new String[2];
		 revArray4[0]="25000";
		 revArray4[1]="50000";
		 revMap.put("$25M-$50M", revArray4);
		 
		 String[] revArray5 = new String[2];
		 revArray5[0]="50000";
		 revArray5[1]="100000";
		 revMap.put("$50M-$100M", revArray5);
		 
		 String[] revArray6 = new String[2];
		 revArray6[0]="100000";
		 revArray6[1]="250000";
		 revMap.put("$100M-$250M", revArray6);
		 
		 String[] revArray7 = new String[2];
		 revArray7[0]="250000";
		 revArray7[1]="500000";
		 revMap.put("$250M-$500M", revArray7);
		 
		 String[] revArray8 = new String[2];
		 revArray8[0]="500000";
		 revArray8[1]="1000000";
		 revMap.put("$500M-$1B", revArray8);
		 
		 String[] revArray9 = new String[2];
		 revArray9[0]="1000000";
		 revArray9[1]="5000000";
		 revMap.put("$1B-$5B", revArray9);
		 
		 String[] revArray10 = new String[2];
		 revArray10[0]="5000000";
		 revArray10[1]="0";
		 revMap.put("More than $5Billion", revArray10);
		
		
		return revMap;
	}
	public static LinkedHashMap<String,String[]> getEmpSortedMap(String[] searchEmp){
	List<String> empSearch = Arrays.asList(searchEmp);
	 LinkedHashMap<String,String[]> sortedEmpMap = new LinkedHashMap<String, String[]>();
	 if(empSearch.contains("1-5")) {
		 String[] empArray1 = new String[2];
		 empArray1[0]="0";
		 empArray1[1]="4";
		 sortedEmpMap.put("1-5", empArray1);
	 }
	 if(empSearch.contains("5-10")) {
		 String[] empArray2 = new String[2];
			empArray2[0]="5";
			empArray2[1]="9";
			sortedEmpMap.put("5-10", empArray2);
	 }
	 if(empSearch.contains("10-20")) {
		 String[] empArray3 = new String[2];
			empArray3[0]="10";
			empArray3[1]="19";
			sortedEmpMap.put("10-20", empArray3);
	 }
	 if(empSearch.contains("20-50")) {
		 String[] empArray4 = new String[2];
			empArray4[0]="20";
			empArray4[1]="49";
			sortedEmpMap.put("20-50", empArray4);
	 }
	 if(empSearch.contains("50-100")) {
		 String[] empArray5 = new String[2];
			empArray5[0]="50";
			empArray5[1]="99";
			sortedEmpMap.put("50-100", empArray5);
	 }
	 if(empSearch.contains("100-250")) {
		 String[] empArray6 = new String[2];
			empArray6[0]="100";
			empArray6[1]="249";
			sortedEmpMap.put("100-250", empArray6);
	 }
	 if(empSearch.contains("250-500")) {
			String[] empArray7 = new String[2];
			empArray7[0]="250";
			empArray7[1]="499";
			sortedEmpMap.put("250-500", empArray7);
	 }
	 if(empSearch.contains("500-1000")) {
		 String[] empArray8 = new String[2];
			empArray8[0]="500";
			empArray8[1]="999";
			sortedEmpMap.put("500-1000", empArray8);
	 }
	 if(empSearch.contains("1000-5000")) {
			String[] empArray9 = new String[2];
			empArray9[0]="1000";
			empArray9[1]="4999";
			sortedEmpMap.put("1000-5000", empArray9);
	 }
	 if(empSearch.contains("5000-10000")) {
		 String[] empArray10 = new String[2];
			empArray10[0]="5000";
			empArray10[1]="9999";
			sortedEmpMap.put("5000-10000", empArray10);
	 }
	 if(empSearch.contains("Over 10000")) {
		 String[] empArray11 = new String[2];
			empArray11[0]="10000";
			empArray11[1]="0";
			sortedEmpMap.put("Over 10000", empArray11);
	 }
	 
	 return sortedEmpMap;
	}
	 
	
	public static Map<String, String[]> getEmployeeMap(){
		Map<String,String[]> empMap = new HashMap<String, String[]>();
		/////
		 String[] empArray1 = new String[2];
		 empArray1[0]="0";
		 empArray1[1]="4";
		 empMap.put("1-5", empArray1);
		
		String[] empArray2 = new String[2];
		empArray2[0]="5";
		empArray2[1]="9";
		empMap.put("5-10", empArray2);
		
		String[] empArray3 = new String[2];
		empArray3[0]="10";
		empArray3[1]="19";
		empMap.put("10-20", empArray3);
		
		String[] empArray4 = new String[2];
		empArray4[0]="20";
		empArray4[1]="49";
		empMap.put("20-50", empArray4);
		
		String[] empArray5 = new String[2];
		empArray5[0]="50";
		empArray5[1]="99";
		empMap.put("50-100", empArray5);
		
		String[] empArray6 = new String[2];
		empArray6[0]="100";
		empArray6[1]="249";
		empMap.put("100-250", empArray6);
		
		String[] empArray7 = new String[2];
		empArray7[0]="250";
		empArray7[1]="499";
		empMap.put("250-500", empArray7);
		
		String[] empArray8 = new String[2];
		empArray8[0]="500";
		empArray8[1]="999";
		empMap.put("500-1000", empArray8);
		
		String[] empArray9 = new String[2];
		empArray9[0]="1000";
		empArray9[1]="4999";
		empMap.put("1000-5000", empArray9);
		
		String[] empArray10 = new String[2];
		empArray10[0]="5000";
		empArray10[1]="9999";
		empMap.put("5000-10000", empArray10);
		
		String[] empArray11 = new String[2];
		empArray11[0]="10000";
		empArray11[1]="0";
		empMap.put("Over 10000", empArray11);
		
		
		
		return empMap;
	}
	
	public static List<Map<String, Integer>> getEmployeeRangesList () {
		List<Map<String, Integer>> employeeRangeList = new ArrayList<>();
		
		Map<String, Integer> empRange1 = new HashMap<String, Integer>();
		empRange1.put("minEmp", 0);
		empRange1.put("maxEmp", 5);
		employeeRangeList.add(empRange1);
		
		Map<String, Integer> empRange2 = new HashMap<String, Integer>();
		empRange2.put("minEmp", 5);
		empRange2.put("maxEmp", 10);
		employeeRangeList.add(empRange2);
		
		Map<String, Integer> empRange3 = new HashMap<String, Integer>();
		empRange3.put("minEmp", 10);
		empRange3.put("maxEmp", 20);
		employeeRangeList.add(empRange3);
		
		Map<String, Integer> empRange4 = new HashMap<String, Integer>();
		empRange4.put("minEmp", 20);
		empRange4.put("maxEmp", 50);
		employeeRangeList.add(empRange4);
		
		Map<String, Integer> empRange5 = new HashMap<String, Integer>();
		empRange5.put("minEmp", 50);
		empRange5.put("maxEmp", 100);
		employeeRangeList.add(empRange5);
		
		Map<String, Integer> empRange6 = new HashMap<String, Integer>();
		empRange6.put("minEmp", 100);
		empRange6.put("maxEmp", 250);
		employeeRangeList.add(empRange6);
		
		Map<String, Integer> empRange7 = new HashMap<String, Integer>();
		empRange7.put("minEmp", 250);
		empRange7.put("maxEmp", 500);
		employeeRangeList.add(empRange7);
		
		Map<String, Integer> empRange8 = new HashMap<String, Integer>();
		empRange8.put("minEmp", 500);
		empRange8.put("maxEmp", 1000);
		employeeRangeList.add(empRange8);
		
		Map<String, Integer> empRange9 = new HashMap<String, Integer>();
		empRange9.put("minEmp", 1000);
		empRange9.put("maxEmp", 5000);
		employeeRangeList.add(empRange9);
		
		Map<String, Integer> empRange10 = new HashMap<String, Integer>();
		empRange10.put("minEmp", 5000);
		empRange10.put("maxEmp", 10000);
		employeeRangeList.add(empRange10);
		
		Map<String, Integer> empRange11 = new HashMap<String, Integer>();
		empRange11.put("minEmp", 10000);
		employeeRangeList.add(empRange11);
		
		return employeeRangeList;
	}
	
	 public static List<String> getSortedMgmtSearchList(String managementLevelStr){
		 List<String> sortedMgmtList = new ArrayList<String>();
		 if(managementLevelStr!=null && !managementLevelStr.isEmpty()){
			 String[] mgmtArray = managementLevelStr.split(",");
			 List<String> searchList = Arrays.asList(mgmtArray);
			 if(searchList.contains("Non Management"))
				 sortedMgmtList.add("Non Management");
			 if(searchList.contains("MANAGER"))
				 sortedMgmtList.add("MANAGER");
			 if(searchList.contains("DIRECTOR"))
				 sortedMgmtList.add("DIRECTOR");
			 if(searchList.contains("Mid Management"))
				 sortedMgmtList.add("Mid Management");
			 if(searchList.contains("VP_EXECUTIVES"))
				 sortedMgmtList.add("VP_EXECUTIVES");
			 if(searchList.contains("C_EXECUTIVES"))
				 sortedMgmtList.add("C_EXECUTIVES");
			 if(searchList.contains("Executives"))
				 sortedMgmtList.add("Executives");
			 if(searchList.contains("Board Members"))
				 sortedMgmtList.add("Board Members");
		 }
		 return sortedMgmtList;
	 }

	 public static HashMap<String, HashMap<String, Integer>> getCallableDirectIndirectCompanyCountMap(
			List<ProspectCallLog> callableProspectList) {
		HashMap<String, HashMap<String, Integer>> directIndirectCallableCompanyCountMap = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, Integer> directPhoneCompanyMap = new HashMap<String, Integer>();
		HashMap<String, Integer> indirectPhoneCompanyMap = new HashMap<String, Integer>();
		for (ProspectCallLog prospectCallLog : callableProspectList) {
			if (prospectCallLog.getProspectCall() != null && prospectCallLog.getProspectCall().getProspect() != null
					&& prospectCallLog.getProspectCall().getProspect().getCompany() != null
					&& !prospectCallLog.getProspectCall().getProspect().getCompany().isEmpty()) {
				String companyName = prospectCallLog.getProspectCall().getProspect().getCompany();
				if (prospectCallLog.getProspectCall().getProspect().isDirectPhone()) {
					Integer directCount = directPhoneCompanyMap.get(companyName);
					directCount = directCount != null ? directCount : 0;
					directPhoneCompanyMap.put(companyName, directCount + 1);
				} else {
					Integer indirectCount = indirectPhoneCompanyMap.get(companyName);
					indirectCount = indirectCount != null ? indirectCount : 0;
					indirectPhoneCompanyMap.put(companyName, indirectCount + 1);
				}
			}
		}
		directIndirectCallableCompanyCountMap.put("directCompanyMap", directPhoneCompanyMap);
		directIndirectCallableCompanyCountMap.put("indirectCompanyMap", indirectPhoneCompanyMap);
		return directIndirectCallableCompanyCountMap;
	}
	 
	public static List<String> getPlivoPhoneNumberPatterns() {
		List<String> patterns = new ArrayList<String>();
		patterns.add("201");
		patterns.add("551");
		patterns.add("202");
		patterns.add("203");
		patterns.add("475");
		patterns.add("205");
		patterns.add("659");
		patterns.add("206");
		patterns.add("207");
		patterns.add("208");
		patterns.add("986");
		patterns.add("209");
		patterns.add("210");
		patterns.add("726");
		patterns.add("212");
		patterns.add("646");
		patterns.add("332");
		patterns.add("213");
		patterns.add("323");
		patterns.add("214");
		patterns.add("469");
		patterns.add("972");
		patterns.add("215");
		patterns.add("216");
		patterns.add("217");
		patterns.add("218");
		patterns.add("224");
		patterns.add("225");
		patterns.add("228");
		patterns.add("229");
		patterns.add("231");
		patterns.add("234");
		patterns.add("330");
		patterns.add("239");
		patterns.add("240");
		patterns.add("301");
		patterns.add("240");
		patterns.add("248");
		patterns.add("947");
		patterns.add("251");
		patterns.add("252");
		patterns.add("253");
		patterns.add("254");
		patterns.add("256");
		patterns.add("938");
		patterns.add("260");
		patterns.add("262");
		patterns.add("267");
		patterns.add("269");
		patterns.add("270");
		patterns.add("364");
		patterns.add("274");
		patterns.add("276");
		patterns.add("281");
		patterns.add("713");
		patterns.add("281");
		patterns.add("346");
		patterns.add("832");
		patterns.add("301");
		patterns.add("302");
		patterns.add("303");
		patterns.add("720");
		patterns.add("304");
		patterns.add("681");
		patterns.add("305");
		patterns.add("786");
		patterns.add("307");
		patterns.add("308");
		patterns.add("309");
		patterns.add("310");
		patterns.add("424");
		patterns.add("312");
		patterns.add("313");
		patterns.add("314");
		patterns.add("315");
		patterns.add("680");
		patterns.add("316");
		patterns.add("317");
		patterns.add("463");
		patterns.add("318");
		patterns.add("319");
		patterns.add("320");
		patterns.add("321");
		patterns.add("325");
		patterns.add("332");
		patterns.add("334");
		patterns.add("336");
		patterns.add("743");
		patterns.add("337");
		patterns.add("347");
		patterns.add("352");
		patterns.add("360");
		patterns.add("361");
		patterns.add("386");
		patterns.add("401");
		patterns.add("402");
		patterns.add("531");
		patterns.add("404");
		patterns.add("405");
		patterns.add("406");
		patterns.add("407");
		patterns.add("689");
		patterns.add("408");
		patterns.add("669");
		patterns.add("409");
		patterns.add("410");
		patterns.add("410");
		patterns.add("443");
		patterns.add("667");
		patterns.add("412");
		patterns.add("413");
		patterns.add("414");
		patterns.add("415");
		patterns.add("628");
		patterns.add("417");
		patterns.add("419");
		patterns.add("567");
		patterns.add("423");
		patterns.add("425");
		patterns.add("430");
		patterns.add("432");
		patterns.add("434");
		patterns.add("435");
		patterns.add("440");
		patterns.add("760");
		patterns.add("442");
		patterns.add("443");
		patterns.add("478");
		patterns.add("479");
		patterns.add("480");
		patterns.add("484");
		patterns.add("501");
		patterns.add("502");
		patterns.add("503");
		patterns.add("971");
		patterns.add("504");
		patterns.add("505");
		patterns.add("507");
		patterns.add("508");
		patterns.add("774");
		patterns.add("509");
		patterns.add("510");
		patterns.add("341");
		patterns.add("512");
		patterns.add("737");
		patterns.add("513");
		patterns.add("515");
		patterns.add("516");
		patterns.add("517");
		patterns.add("518");
		patterns.add("838");
		patterns.add("520");
		patterns.add("530");
		patterns.add("540");
		patterns.add("541");
		patterns.add("458");
		patterns.add("555");
		patterns.add("559");
		patterns.add("561");
		patterns.add("562");
		patterns.add("563");
		patterns.add("564");
		patterns.add("570");
		patterns.add("272");
		patterns.add("571");
		patterns.add("703");
		patterns.add("571");
		patterns.add("573");
		patterns.add("575");
		patterns.add("580");
		patterns.add("582");
		patterns.add("585");
		patterns.add("586");
		patterns.add("601");
		patterns.add("769");
		patterns.add("602");
		patterns.add("603");
		patterns.add("605");
		patterns.add("606");
		patterns.add("607");
		patterns.add("608");
		patterns.add("609");
		patterns.add("640");
		patterns.add("610");
		patterns.add("610");
		patterns.add("484");
		patterns.add("612");
		patterns.add("614");
		patterns.add("380");
		patterns.add("615");
		patterns.add("629");
		patterns.add("616");
		patterns.add("617");
		patterns.add("857");
		patterns.add("618");
		patterns.add("619");
		patterns.add("858");
		patterns.add("620");
		patterns.add("623");
		patterns.add("626");
		patterns.add("630");
		patterns.add("331");
		patterns.add("631");
		patterns.add("934");
		patterns.add("636");
		patterns.add("641");
		patterns.add("646");
		patterns.add("650");
		patterns.add("651");
		patterns.add("714");
		patterns.add("657");
		patterns.add("660");
		patterns.add("661");
		patterns.add("662");
		patterns.add("670");
		patterns.add("671");
		patterns.add("678");
		patterns.add("470");
		patterns.add("679");
		patterns.add("680");
		patterns.add("682");
		patterns.add("684");
		patterns.add("700");
		patterns.add("701");
		patterns.add("702");
		patterns.add("725");
		patterns.add("703");
		patterns.add("704");
		patterns.add("980");
		patterns.add("706");
		patterns.add("762");
		patterns.add("707");
		patterns.add("708");
		patterns.add("710");
		patterns.add("712");
		patterns.add("713");
		patterns.add("715");
		patterns.add("534");
		patterns.add("716");
		patterns.add("717");
		patterns.add("223");
		patterns.add("718");
		patterns.add("347");
		patterns.add("929");
		patterns.add("719");
		patterns.add("724");
		patterns.add("727");
		patterns.add("731");
		patterns.add("732");
		patterns.add("848");
		patterns.add("734");
		patterns.add("740");
		patterns.add("220");
		patterns.add("818");
		patterns.add("747");
		patterns.add("754");
		patterns.add("954");
		patterns.add("757");
		patterns.add("763");
		patterns.add("765");
		return patterns;
	}


	public static List<Map<String, Integer>> getRevenueListMap(){
		List<Map<String, Integer>> revenueRangeList = new ArrayList<>();

		Map<String, Integer> revenueRange1 = new HashMap<String, Integer>();
		revenueRange1.put("minRevenue", 0);
		revenueRange1.put("maxRevenue", 1000);
		revenueRangeList.add(revenueRange1);
		/////
		Map<String, Integer> revenueRange2 = new HashMap<String, Integer>();
		revenueRange2.put("minRevenue", 5000);
		revenueRange2.put("maxRevenue", 10000);
		revenueRangeList.add(revenueRange2);

		Map<String, Integer> revenueRange3 = new HashMap<String, Integer>();
		revenueRange3.put("minRevenue", 10000);
		revenueRange3.put("maxRevenue", 25000);
		revenueRangeList.add(revenueRange3);

		Map<String, Integer> revenueRange4 = new HashMap<String, Integer>();
		revenueRange4.put("minRevenue", 25000);
		revenueRange4.put("maxRevenue", 50000);
		revenueRangeList.add(revenueRange4);

		Map<String, Integer> revenueRange5 = new HashMap<String, Integer>();
		revenueRange5.put("minRevenue", 50000);
		revenueRange5.put("maxRevenue", 100000);
		revenueRangeList.add(revenueRange5);

		Map<String, Integer> revenueRange6 = new HashMap<String, Integer>();
		revenueRange6.put("minRevenue", 100000);
		revenueRange6.put("maxRevenue", 250000);
		revenueRangeList.add(revenueRange6);

		Map<String, Integer> revenueRange7 = new HashMap<String, Integer>();
		revenueRange7.put("minRevenue", 250000);
		revenueRange7.put("maxRevenue", 500000);
		revenueRangeList.add(revenueRange7);

		Map<String, Integer> revenueRange8 = new HashMap<String, Integer>();
		revenueRange8.put("minRevenue", 500000);
		revenueRange8.put("maxRevenue", 1000000);
		revenueRangeList.add(revenueRange8);

		Map<String, Integer> revenueRange9 = new HashMap<String, Integer>();
		revenueRange9.put("minRevenue", 1000000);
		revenueRange9.put("maxRevenue", 5000000);
		revenueRangeList.add(revenueRange9);

		Map<String, Integer> revenueRange10 = new HashMap<String, Integer>();
		revenueRange10.put("minRevenue", 5000000);
		revenueRange10.put("maxRevenue", 0);
		revenueRangeList.add(revenueRange10);

		return revenueRangeList;
	}

	public static List<String> getSortedMgmtSearchForLeadIQ(List<String> managementLevelStr){
		List<String> sortedMgmtList = new ArrayList<>();
		if(managementLevelStr!=null && !managementLevelStr.isEmpty()){
			List<String> searchList = managementLevelStr;
			if(searchList.contains("Non Management"))
				if(!sortedMgmtList.contains("Other"))
					sortedMgmtList.add("Other");
			if(searchList.contains("MANAGER"))
				if(!sortedMgmtList.contains("Manager"))
					sortedMgmtList.add("Manager");
			if(searchList.contains("DIRECTOR"))
				if(!sortedMgmtList.contains("Director"))
					sortedMgmtList.add("Director");
			if(searchList.contains("Mid Management"))
				if(!sortedMgmtList.contains("Other"))
					sortedMgmtList.add("Other");
			if(searchList.contains("VP_EXECUTIVES"))
				if(!sortedMgmtList.contains("VP"))
					sortedMgmtList.add("VP");
			if(searchList.contains("C_EXECUTIVES"))
				if(!sortedMgmtList.contains("Executive"))
					sortedMgmtList.add("Executive");
			if(searchList.contains("Executives"))
				if(!sortedMgmtList.contains("Executive"))
					sortedMgmtList.add("Executive");
			if(searchList.contains("Board Members"))
				if(!sortedMgmtList.contains("Other"))
					sortedMgmtList.add("Other");
		}
		return sortedMgmtList;
	}

	public static Map<String, String[]> getInsideviewRevenueMap(){
		Map<String,String[]> revMap = new HashMap<String, String[]>();
		/////
		String[] revArray1 = new String[2];
		revArray1[0]="0";
		revArray1[1]="10000000";
		revMap.put("0-$10M", revArray1);

		String[] revArray2 = new String[2];
		revArray2[0]="5000000";
		revArray2[1]="10000000";
		revMap.put("$5M-$10M", revArray2);

		String[] revArray3 = new String[2];
		revArray3[0]="10000000";
		revArray3[1]="25000000";
		revMap.put("$10M-$25M", revArray3);

		String[] revArray4 = new String[2];
		revArray4[0]="25000000";
		revArray4[1]="50000000";
		revMap.put("$25M-$50M", revArray4);

		String[] revArray5 = new String[2];
		revArray5[0]="50000000";
		revArray5[1]="100000000";
		revMap.put("$50M-$100M", revArray5);

		String[] revArray6 = new String[2];
		revArray6[0]="100000000";
		revArray6[1]="250000000";
		revMap.put("$100M-$250M", revArray6);

		String[] revArray7 = new String[2];
		revArray7[0]="250000000";
		revArray7[1]="500000000";
		revMap.put("$250M-$500M", revArray7);

		String[] revArray8 = new String[2];
		revArray8[0]="500000000";
		revArray8[1]="1000000000";
		revMap.put("$500M-$1B", revArray8);

		String[] revArray9 = new String[2];
		revArray9[0]="1000000000";
		revArray9[1]="5000000000";
		revMap.put("$1B-$5B", revArray9);

		String[] revArray10 = new String[2];
		revArray10[0]="5000000000";
		revArray10[1]="0";
		revMap.put("More than $5Billion", revArray10);


		return revMap;
	}

	public static List<Map<String, Long>> getInsideviewRevenueListMap(){
		List<Map<String, Long>> revenueRangeList = new ArrayList<>();

		Map<String, Long> revenueRange1 = new HashMap<String, Long>();
		revenueRange1.put("minRevenue", 0l);
		revenueRange1.put("maxRevenue", 10000000l);
		revenueRangeList.add(revenueRange1);
		/////
		Map<String, Long> revenueRange2 = new HashMap<String, Long>();
		revenueRange2.put("minRevenue", 5000000l);
		revenueRange2.put("maxRevenue", 10000000l);
		revenueRangeList.add(revenueRange2);

		Map<String, Long> revenueRange3 = new HashMap<String, Long>();
		revenueRange3.put("minRevenue", 10000000l);
		revenueRange3.put("maxRevenue", 25000000l);
		revenueRangeList.add(revenueRange3);

		Map<String, Long> revenueRange4 = new HashMap<String, Long>();
		revenueRange4.put("minRevenue", 25000000l);
		revenueRange4.put("maxRevenue", 50000000l);
		revenueRangeList.add(revenueRange4);

		Map<String, Long> revenueRange5 = new HashMap<String, Long>();
		revenueRange5.put("minRevenue", 50000000l);
		revenueRange5.put("maxRevenue", 100000000l);
		revenueRangeList.add(revenueRange5);

		Map<String, Long> revenueRange6 = new HashMap<String, Long>();
		revenueRange6.put("minRevenue", 100000000l);
		revenueRange6.put("maxRevenue", 250000000l);
		revenueRangeList.add(revenueRange6);

		Map<String, Long> revenueRange7 = new HashMap<String, Long>();
		revenueRange7.put("minRevenue", 250000000l);
		revenueRange7.put("maxRevenue", 500000000l);
		revenueRangeList.add(revenueRange7);

		Map<String, Long> revenueRange8 = new HashMap<String, Long>();
		revenueRange8.put("minRevenue", 500000000l);
		revenueRange8.put("maxRevenue", 1000000000l);
		revenueRangeList.add(revenueRange8);

		Map<String, Long> revenueRange9 = new HashMap<String, Long>();
		revenueRange9.put("minRevenue", 1000000000l);
		revenueRange9.put("maxRevenue", 5000000000l);
		revenueRangeList.add(revenueRange9);

		Map<String, Long> revenueRange10 = new HashMap<String, Long>();
		revenueRange10.put("minRevenue", 5000000000l);
		revenueRange10.put("maxRevenue", 0l);
		revenueRangeList.add(revenueRange10);

		return revenueRangeList;
	}

	/**
	 * add call disposed log statement for wait time calculation
	 * 
	 * @param pcl
	 * @param campaign
	 * @param agent
	 */
	public static void callDisposedLogStatement(ProspectCallLog pcl, Campaign campaign, Agent agent) {
		if (pcl == null || pcl.getProspectCall() == null) {
			return;
		}
		ProspectCall pc = pcl.getProspectCall();
		try {
			List<ProspectCallStatus> statuses = Arrays.asList(ProspectCallStatus.UNKNOWN_ERROR,
					ProspectCallStatus.CALL_TIME_RESTRICTION, ProspectCallStatus.CALL_NO_ANSWER,
					ProspectCallStatus.CALL_BUSY, ProspectCallStatus.CALL_CANCELED, ProspectCallStatus.CALL_FAILED,
					ProspectCallStatus.CALL_MACHINE_ANSWERED, ProspectCallStatus.CALL_ABANDONED,
					ProspectCallStatus.WRAPUP_COMPLETE);
			if (statuses.contains(pcl.getStatus())) {
				String callRetryCount = Integer.toString(pc.getCallRetryCount());
				String dailyCallRetryCount = "";
				if (pc.getDailyCallRetryCount() != null) {
					dailyCallRetryCount = Long.toString(pc.getDailyCallRetryCount());
				}
				SplunkLoggingUtils loggingUtils = new SplunkLoggingUtils("CallDisposed", pc.getProspectCallId())
						.prospectCallId(pc.getProspectCallId())
						.prospectInteractionSessionId(pc.getProspectInteractionSessionId())
						.callSid(pc.getTwilioCallSid()).callRetryCount(callRetryCount)
						.dailyCallRetryCount(dailyCallRetryCount).campaignId(pc.getCampaignId())
						.status(pcl.getStatus().name()).dispositionStatus(pc.getDispositionStatus())
						.subStatus(pc.getSubStatus());
				if (campaign != null) {
					loggingUtils.dialerMode(campaign.getDialerMode().name())
							.voiceProvider(campaign.getCallControlProvider()).sipProvider(campaign.getSipProvider());
				}
				if (agent != null) {
					if (agent.getAgentType() != null) {
						String agentType = agent.getAgentType().name();
						loggingUtils.agentId(pc.getAgentId()).agentType(agentType);
					}
				}
				logger.info(loggingUtils.build());
			}
		} catch (Exception ex) {
			logger.error(new SplunkLoggingUtils("CallDisposedError", pc.getProspectCallId())
					.prospectCallId(pc.getProspectCallId()).campaignId(pc.getCampaignId())
					.addThrowableWithStacktrace(ex).build());
		}
	}


	public static String getHostName(String url) {
		if (url != null && !url.isEmpty()) {
			url = url.toLowerCase();
			if (url.contains("http://") || url.contains("https://")
					|| url.contains("HTTP://")
					|| url.contains("HTTPS://")) {

			} else {
				url = "http://" + url;
			}
			try {
				URI uri = new URI(url);
				String hostname = uri.getHost();
				if (hostname != null && !hostname.isEmpty()) {
					if (hostname.contains("www.") || hostname.contains("WWW.")) {
						return hostname = hostname.substring(4, hostname.length());
					}else {
						return hostname;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}


	/**
	 * It will check, it is valid domain or not
	 * @param domain
	 * @return
	 */
	public static boolean isValidDomain(String domain)
	{
		if(StringUtils.isBlank(domain)){
			return false;
		}
		String regex = "^((?!-)[A-Za-z0-9-]"
				+ "{1,63}(?<!-)\\.)"
				+ "+[A-Za-z]{2,6}";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(domain);
		return m.matches();
	}

}
