/**
 * 
 */
package com.xtaas.utils;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author djain
 *
 */
public class JSONUtils {

	public static <T> T toObject(String json, Class<T> toClass)
			throws JsonParseException, JsonMappingException, IOException {
		T t = new ObjectMapper().readValue(json, toClass);
		return t;
	}

	public static <T> T toObject(InputStream stream, Class<T> toClass, boolean unwrapRoot)
			throws JsonParseException, JsonMappingException, IOException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			if (unwrapRoot) {
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
				objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			}
			T t = objectMapper.readValue(stream, toClass);
			return t;
		} catch (Exception ex) {
			return null;
		}
	}

	public static <T> T toObject(String json, Class<T> toClass, boolean unwrapRoot)
			throws JsonParseException, JsonMappingException, IOException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			if (unwrapRoot) {
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
				objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			}
			T t = objectMapper.readValue(json, toClass);
			return t;
		} catch (Exception ex) {
			return null;
		}
	}

	public static String toJson(Object object) throws JsonGenerationException, JsonMappingException, IOException {
		return new ObjectMapper().writeValueAsString(object);
	}
}
