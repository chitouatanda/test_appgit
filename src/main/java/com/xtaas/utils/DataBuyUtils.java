package com.xtaas.utils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.xtaas.domain.valueobject.CampaignCriteria;

public class DataBuyUtils {

	public static List<String> getMST() {
		List<String> mstList = new ArrayList<String>();
		mstList.add("Utah");
		mstList.add("Wyoming");
		mstList.add("Montana");
		mstList.add("New Mexico");
		mstList.add("Colorado");
		mstList.add("Alaska");
		mstList.add("Hawaii");
		return mstList;
	}

	public static List<String> getPST() {
		List<String> pstList = new ArrayList<String>();
		pstList.add("Idaho");
		pstList.add("Washington");
		pstList.add("Nevada");
		pstList.add("Oregon");
		pstList.add("Arizona");
		pstList.add("California");
		return pstList;
	}

	public static List<String> getCST() {
		List<String> cstList = new ArrayList<String>();
		cstList.add("Wisconsin");
		cstList.add("Illinois");
		cstList.add("Indiana");
		cstList.add("Kansas");
		cstList.add("Louisiana");
		cstList.add("Minnesota");
		cstList.add("Missouri");
		cstList.add("Mississippi");
		cstList.add("North Dakota");
		cstList.add("Nebraska");
		cstList.add("Oklahoma");
		cstList.add("South Dakota");
		cstList.add("Tennessee");
		cstList.add("Texas");
		cstList.add("Alabama");
		cstList.add("Arkansas");
		return cstList;
	}

	public static List<String> getEST() {
		List<String> estList = new ArrayList<String>();
		estList.add("Virginia");
		estList.add("West Virginia");
		estList.add("Massachusetts");
		estList.add("Maryland");
		estList.add("Maine");
		estList.add("Michigan");
		estList.add("North Carolina");
		estList.add("New Hampshire");
		estList.add("New Jersey");
		estList.add("New York");
		estList.add("Ohio");
		estList.add("Pennsylvania");
		estList.add("Rhode Island");
		estList.add("South Carolina");
		estList.add("Connecticut");
		estList.add("District of Columbia");
		estList.add("Delaware");
		estList.add("Florida");
		estList.add("Georgia");
		estList.add("Iowa");
		return estList;
	}

	public static List<String> getUKStates() {
		List<String> stateList = new ArrayList<String>();
		stateList.add("England");
		stateList.add("Greater London");
		stateList.add("Renfrewshire");
		stateList.add("Glasgow");
		stateList.add("Berkshire");
		stateList.add("Bedfordshire");
		stateList.add("Berkshire");
		stateList.add("Bristol");
		stateList.add("Buckinghamshire");
		stateList.add("Cambridgeshire");
		stateList.add("Cheshire");
		stateList.add("Cornwall");
		stateList.add("County Durham");
		stateList.add("Cumberland");
		stateList.add("Derbyshire");
		stateList.add("Devon");
		stateList.add("Dorset");
		stateList.add("Essex");
		stateList.add("Gloucestershire");
		stateList.add("Hampshire");
		stateList.add("Herefordshire");
		stateList.add("Hertfordshire");
		stateList.add("Huntingdonshire");
		stateList.add("Kent");
		stateList.add("Lancashire");
		stateList.add("Leicestershire");
		stateList.add("Lincolnshire");
		stateList.add("Middlesex");
		stateList.add("Norfolk");
		stateList.add("Northamptonshire");
		stateList.add("Northumberland");
		stateList.add("Nottinghamshire");
		stateList.add("Oxfordshire");
		stateList.add("Rutland");
		stateList.add("Shropshire");
		stateList.add("Somerset");
		stateList.add("Staffordshire");
		stateList.add("Suffolk");
		stateList.add("Surrey");
		stateList.add("Sussex");
		stateList.add("Warwickshire");
		stateList.add("Wiltshire");
		stateList.add("Worcestershire");
		stateList.add("Yorkshire");

		return stateList;
	}

	public static List<String> getCanadaStates() {
		List<String> stateList = new ArrayList<String>();
		stateList.add("Alberta");
		stateList.add("Northwest Territories");
		stateList.add("British Columbia");
		stateList.add("Nunavut");
		stateList.add("Ontario");
		stateList.add("Quebec");
		stateList.add("Manitoba");
		stateList.add("Saskatchewan");
		stateList.add("Yukon");
		stateList.add("New Brunswick");
		stateList.add("Nova Scotia");
		stateList.add("Prince Edward Island");
		stateList.add("Newfoundland and Labrador");
		return stateList;
	}

	public static String getEmployeeWeight(Number n) {

		if (n.intValue() == 0) {
			return "13";
		}
		if (n.intValue() < 5) {
			return "01";
		}
		if (n.intValue() < 10) {
			return "02";
		}
		if (n.intValue() < 20) {
			return "03";
		}
		if (n.intValue() < 50) {
			return "04";
		}
		if (n.intValue() < 100) {
			return "05";
		}
		if (n.intValue() < 250) {
			return "06";
		}
		if (n.intValue() < 500) {
			return "07";
		}
		if (n.intValue() < 1000) {
			return "08";
		}
		if (n.intValue() < 5000) {
			return "09";
		}
		if (n.intValue() < 10000) {
			return "10";
		}

		if (n.intValue() < 11000) {
			return "11";
		}

		return "12";
	}

	public static String getBoundry(Number n, String minMax) {
		if (minMax.equalsIgnoreCase("min")) {
			if (n.intValue() < 5) {
				return "1";
			}
			if (n.intValue() < 10) {
				return "5";
			}
			if (n.intValue() < 20) {
				return "10";
			}
			if (n.intValue() < 50) {
				return "20";
			}
			if (n.intValue() < 100) {
				return "50";
			}
			if (n.intValue() < 250) {
				return "100";
			}
			if (n.intValue() < 500) {
				return "250";
			}
			if (n.intValue() < 1000) {
				return "500";
			}
			if (n.intValue() < 5000) {
				return "1000";
			}
			if (n.intValue() < 10000) {
				return "5000";
			}

			if (n.intValue() < 11000) {
				return "10000";
			}

		} else if (minMax.equalsIgnoreCase("max")) {
			if (n.intValue() <= 5) {
				return "5";
			}
			if (n.intValue() <= 10) {
				return "10";
			}
			if (n.intValue() <= 20) {
				return "20";
			}
			if (n.intValue() <= 50) {
				return "50";
			}
			if (n.intValue() <= 100) {
				return "100";
			}
			if (n.intValue() <= 250) {
				return "250";
			}
			if (n.intValue() <= 500) {
				return "500";
			}
			if (n.intValue() <= 1000) {
				return "1000";
			}
			if (n.intValue() <= 5000) {
				return "5000";
			}
			if (n.intValue() <= 10000) {
				return "10000";
			}

			if (n.intValue() <= 100000) {
				return "11000";
			}

		}
		return "11000";
	}

	public static Map<String, String> getManagementMap() {
		Map<String, String> mgmtMap = new HashMap<String, String>();
		mgmtMap.put("C-Level", "C_EXECUTIVES");
		mgmtMap.put("VP-Level", "VP_EXECUTIVES");
		mgmtMap.put("Director", "DIRECTOR");
		mgmtMap.put("Manager", "MANAGER");
		mgmtMap.put("Non-Manager", "Non Management");

		return mgmtMap;
	}

	public static String getMgmtLevel(String managementLevel) {

		if (managementLevel != null && managementLevel.equalsIgnoreCase("C-Level")) {
			return "C-Level";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("VP-Level")) {
			return "VP-Level";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("Director")) {
			return "DIRECTOR";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("Manager")) {
			return "MANAGER";
		} else {
			return "Non-Manager";
		}

	}

	public static List<String> getSortedMgmtSearchList(String managementLevelStr) {
		List<String> sortedMgmtList = new ArrayList<String>();
		if (managementLevelStr != null && !managementLevelStr.isEmpty()) {
			String[] mgmtArray = managementLevelStr.split(",");
			List<String> searchList = Arrays.asList(mgmtArray);
			if (searchList.contains("Non Management"))
				sortedMgmtList.add("Non Management");
			if (searchList.contains("MANAGER"))
				sortedMgmtList.add("MANAGER");
			if (searchList.contains("DIRECTOR"))
				sortedMgmtList.add("DIRECTOR");
			if (searchList.contains("Mid Management"))
				sortedMgmtList.add("Mid Management");
			if (searchList.contains("VP_EXECUTIVES"))
				sortedMgmtList.add("VP_EXECUTIVES");
			if (searchList.contains("C_EXECUTIVES"))
				sortedMgmtList.add("C_EXECUTIVES");
			if (searchList.contains("Executives"))
				sortedMgmtList.add("Executives");
			if (searchList.contains("Board Members"))
				sortedMgmtList.add("Board Members");
		}
		return sortedMgmtList;
	}

	public static String getMgmtLevelWeight(String managementLevel) {
		if (managementLevel != null && managementLevel.equalsIgnoreCase("C-Level")) {
			return "5";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("VP-Level")) {
			return "4";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("Director")) {
			return "3";
		} else if (managementLevel != null && managementLevel.equalsIgnoreCase("Manager")) {
			return "2";
		} else {
			return "1";
		}
	}

	public static String getStateWeight(String timeZone) {
		String est = "US/Eastern";
		String cst = "US/Central";
		String mst = "US/Mountain";
		String pst = "US/Pacific";
		if (timeZone != null && timeZone.equalsIgnoreCase(pst)) {
			return "1";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(mst)) {
			return "2";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(cst)) {
			return "3";
		}
		if (timeZone != null && timeZone.equalsIgnoreCase(est)) {
			return "4";
		}
		return "5";
	}

	public static Map<String, String> getTitlesGroupMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		// Consultants
		titleMap.put("4980839", "4980839");
		// Technical
		titleMap.put("4784231", "4784231");
		titleMap.put("2228226", "4784231");
		titleMap.put("3211266", "4784231");
		titleMap.put("3801090", "4784231");
		titleMap.put("4653058", "4784231");
		titleMap.put("4849666", "4784231");
		titleMap.put("1376258", "4784231");
		titleMap.put("5570562", "4784231");
		titleMap.put("5505026", "4784231");
		titleMap.put("6029314", "4784231");
		titleMap.put("4718594", "4784231");
		titleMap.put("1114114", "4784231");
		titleMap.put("5308418", "4784231");
		// Biometrics
		titleMap.put("2293762", "2293762");
		// Finance
		titleMap.put("4587623", "4587623");
		titleMap.put("262146", "4587623");
		titleMap.put("1769474", "4587623");
		titleMap.put("327682", "4587623");
		titleMap.put("2621442", "4587623");
		titleMap.put("1441794", "4587623");
		// HR
		titleMap.put("4718695", "4718695");
		titleMap.put("2162690", "4718695");
		titleMap.put("4915202", "4718695");
		titleMap.put("3473410", "4718695");
		titleMap.put("5177346", "4718695");
		titleMap.put("5242882", "4718695");
		// LAW
		titleMap.put("5832807", "5832807");
		titleMap.put("1572866", "5832807");
		titleMap.put("1507330", "5832807");
		// marketing
		titleMap.put("4456551", "4456551");
		titleMap.put("786434", "4456551");
		titleMap.put("2555906", "4456551");
		titleMap.put("3932162", "4456551");
		titleMap.put("393216", "4456551");
		titleMap.put("3276802", "4456551");
		titleMap.put("4325378", "4456551");
		// health
		titleMap.put("5701735", "5701735");
		titleMap.put("4784130", "5701735");
		titleMap.put("3670018", "5701735");
		titleMap.put("589826", "5701735");
		titleMap.put("3145730", "5701735");
		// operation
		titleMap.put("4653159", "4653159");
		titleMap.put("3014658", "4653159");
		titleMap.put("4063234", "4653159");
		titleMap.put("4390914", "4653159");
		titleMap.put("2752514", "4653159");
		titleMap.put("5373954", "4653159");
		titleMap.put("5701634", "4653159");
		titleMap.put("2686978", "4653159");
		titleMap.put("6094850", "4653159");
		titleMap.put("4522087", "4522087");
		titleMap.put("393218", "4522087");
		titleMap.put("196610", "4522087");
		titleMap.put("4849767", "4849767");
		titleMap.put("4259942", "4259942");
		return titleMap;

	}

	public static Map<String, String> getTitlesMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("Consultants", "4980839");
		titleMap.put("Consulting", "4980839");
		titleMap.put("Engineering & Technical", "4784231");
		titleMap.put("Biometrics", "2293762");
		titleMap.put("Biotechnical Engineering", "2228226");
		titleMap.put("Chemical Engineering", "3211266");
		titleMap.put("Civil Engineering", "3801090");
		titleMap.put("Database Engineering &  Design", "4653058");
		titleMap.put("Engineering", "4849666");
		titleMap.put("Hardware Engineering", "1376258");
		titleMap.put("Industrial Engineering", "5570562");
		titleMap.put("Information Technology", "5505026");
		titleMap.put("Quality Assurance", "6029314");
		titleMap.put("Software Engineering", "4718594");
		titleMap.put("Technical", "1114114");
		titleMap.put("Web Development", "5308418");
		titleMap.put("Finance", "4587623");
		titleMap.put("Accounting", "262146");
		titleMap.put("Banking", "1769474");
		titleMap.put("General Finance", "327682");
		titleMap.put("Investment Banking", "2621442");
		titleMap.put("Wealth Management", "1441794");
		titleMap.put("Human Resources", "4718695");
		titleMap.put("Benefits & Compensation", "2162690");
		titleMap.put("Diversity", "4915202");
		titleMap.put("Human Resources Generalist", "3473410");
		titleMap.put("Recruiting", "5177346");
		titleMap.put("Sourcing", "5242882");
		titleMap.put("Legal", "5832807");
		titleMap.put("Governmental Lawyers & Legal Professionals", "1572866");
		titleMap.put("Lawyers & Legal Professionals", "1507330");
		titleMap.put("Marketing", "4456551");
		titleMap.put("Advertising", "786434");
		titleMap.put("Branding", "2555906");
		titleMap.put("Communications", "3932162");
		titleMap.put("E-biz", "393216");
		titleMap.put("General Marketing", "3276802");
		titleMap.put("Product Management", "4325378");
		titleMap.put("Medical & Health", "5701735");
		titleMap.put("Dentists", "4784130");
		titleMap.put("Doctors - General Practice", "3670018");
		titleMap.put("Health Professionals", "589826");
		titleMap.put("Nurses", "3145730");
		titleMap.put("Operations", "4653159");
		titleMap.put("Change Management", "3014658");
		titleMap.put("Compliance", "4063234");
		titleMap.put("Contracts", "4390914");
		titleMap.put("Customer Relations", "2752514");
		titleMap.put("Facilities", "5373954");
		titleMap.put("Logistics", "5701634");
		titleMap.put("General Operations", "2686978");
		titleMap.put("Quality Management", "6094850");
		titleMap.put("Sales", "4522087");
		titleMap.put("Business Development", "393218");
		titleMap.put("General Sales", "196610");
		titleMap.put("Scientists", "4849767");
		titleMap.put("General Management", "4259942");
		return titleMap;
	}

	public static Map<String, String> getIndustryMap() {

		Map<String, String> industryMap = new HashMap<String, String>();
		industryMap.put("Agriculture", "266");
		industryMap.put("Animals & Livestock", "65802");
		industryMap.put("Crops", "131338");
		industryMap.put("Agriculture General", "196874");
		industryMap.put("Business Services", "522");
		industryMap.put("Accounting & Accounting Services", "66058");
		industryMap.put("Auctions", "197130");
		industryMap.put("Call Centers & Business Centers", "262666");
		industryMap.put("Debt Collection", "328202");
		industryMap.put("Management Consulting", "786954");
		industryMap.put("Information & Document Management", "655882");
		industryMap.put("Multimedia & Graphic Design", "852490");
		industryMap.put("Food Service", "393738");
		industryMap.put("Business Services General", "983562");
		industryMap.put("Human Resources & Staffing", "590346");
		industryMap.put("Facilities Management & Commercial Cleaning", "459274");
		industryMap.put("Translation & Linguistic Services", "721418");
		industryMap.put("Advertising & Marketing", "131594");
		industryMap.put("Commercial Printing", "524810");
		industryMap.put("Security Products & Services", "918026");
		industryMap.put("Chambers of Commerce", "778");
		industryMap.put("Construction", "1034");
		industryMap.put("Architecture, Engineering & Design", "66570");
		industryMap.put("Commercial & Residential Construction", "132106");
		industryMap.put("Construction General", "197642");
		industryMap.put("Consumer Services", "1290");
		industryMap.put("Automotive Service & Collision Repair", "66826");
		industryMap.put("Car & Truck Rental", "132362");
		industryMap.put("Funeral Homes & Funeral Related Services", "197898");
		industryMap.put("Consumer Services General", "656650");
		industryMap.put("Hair Salons", "263434");
		industryMap.put("Laundry & Dry Cleaning Services", "328970");
		industryMap.put("Photography Studio", "394506");
		industryMap.put("Travel Agencies & Services", "460042");
		industryMap.put("Veterinary Care", "525578");
		industryMap.put("Weight & Health Management", "591114");
		industryMap.put("Cultural", "1546");
		industryMap.put("Cultural General", "198154");
		industryMap.put("Libraries", "67082");
		industryMap.put("Museums & Art Galleries", "132618");
		industryMap.put("Education", "1802");
		industryMap.put("Education General", "263946");
		industryMap.put("K-12 Schools", "132874");
		industryMap.put("Training", "198410");
		industryMap.put("Colleges & Universities", "67338");
		industryMap.put("Energy, Utilities & Waste Treatment", "2058");
		industryMap.put("Electricity, Oil & Gas", "67594");
		industryMap.put("Waste Treatment, Environmental Services & Recycling", "198666");
		industryMap.put("Energy, Utilities, & Waste Treatment General", "329738");
		industryMap.put("Oil & Gas Exploration & Services", "133130");
		industryMap.put("Water & Water Treatment", "264202");
		industryMap.put("Finance", "2314");
		industryMap.put("Banking", "67850");
		industryMap.put("Brokerage", "133386");
		industryMap.put("Credit Cards & Transaction  Processing", "198922");
		industryMap.put("Finance General", "395530");
		industryMap.put("Investment Banking", "264458");
		industryMap.put("Venture Capital & public Equity", "329994");
		industryMap.put("Government", "2570");
		industryMap.put("Healthcare", "2826");
		industryMap.put("Emergency Medical Transportation & Services", "133898");
		industryMap.put("Healthcare General", "330506");
		industryMap.put("Hospitals & Clinics", "68362");
		industryMap.put("Medical Testing & Clinical Laboratories", "199434");
		industryMap.put("Pharmaceuticals", "264970");
		industryMap.put("Biotechnology", "17042186");
		industryMap.put("Drug Manufacturing & Research", "33819402");
		industryMap.put("Hospitality", "3082");
		industryMap.put("Hospitality General", "330762");
		industryMap.put("Lodging & Resorts", "68618");
		industryMap.put("Recreation", "134154");
		industryMap.put("Movie Theaters", "67243018");
		industryMap.put("Fitness & Dance Facilities", "33688586");
		industryMap.put("Gambling & Gaming", "50465802");
		industryMap.put("Amusement Parks, Arcades & Attractions", "16911370");
		industryMap.put("Zoos & National Parks", "84020234");
		industryMap.put("Restaurants", "199690");
		industryMap.put("Sports Teams & Leagues", "265226");
		industryMap.put("Insurance", "3338");
		industryMap.put("Law Firms & Legal Services", "3594");
		industryMap.put("Media & Internet", "4106");
		industryMap.put("Broadcasting", "69642");
		industryMap.put("Film/Video Production & Services", "50401290");
		industryMap.put("Radio Stations", "16846858");
		industryMap.put("Television Stations", "33624074");
		industryMap.put("Media & Internet General", "462858");
		industryMap.put("Information Collection & Delivery", "135178");
		industryMap.put("Search Engines & Internet Portals", "200714");
		industryMap.put("Music & Music Related Services", "266250");
		industryMap.put("Newspapers & News Services", "331786");
		industryMap.put("Publishing", "397322");
		industryMap.put("Manufacturing", "3850");
		industryMap.put("Aerospace & Defense", "69386");
		industryMap.put("Boats & Submarines", "134922");
		industryMap.put("Building Materials", "200458");
		industryMap.put("Aggregates, Concrete & Cement", "16977674");
		industryMap.put("Lumber, Wood Production & Timber Operations", "33754890");
		industryMap.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)", "67309322");
		industryMap.put("Plumbing & HVAC Equipment", "50532106");
		industryMap.put("Motor Vehicles", "790282");
		industryMap.put("Motor Vehicle Parts", "855818");
		industryMap.put("Chemicals, Petrochemicals, Glass & Gases", "265994");
		industryMap.put("Chemicals", "17043210");
		industryMap.put("Gases", "33820426");
		industryMap.put("Glass & Clay", "50597642");
		industryMap.put("Petrochemicals", "67374858");
		industryMap.put("Computer Equipment & Peripherals", "331530");
		industryMap.put("Personal Computers & Peripherals", "17108746");
		industryMap.put("Computer Networking Equipment", "50663178");
		industryMap.put("Network Security Hardware & Software", "67440394");
		industryMap.put("Computer Storage Equipment", "33885962");
		industryMap.put("Consumer Goods", "397066");
		industryMap.put("Appliances", "17174282");
		industryMap.put("Cleaning Products", "33951498");
		industryMap.put("Textiles & Apparel", "184946442");
		industryMap.put("Cosmetics, Beauty Supply & Personal Care Products", "50728714");
		industryMap.put("Consumer Electronics", "67505930");
		industryMap.put("Health & Nutrition Products", "84283146");
		industryMap.put("Household Goods", "101060362");
		industryMap.put("Pet Products", "117837578");
		industryMap.put("Photographic & Optical Equipment", "134614794");
		industryMap.put("Sporting Goods", "151392010");
		industryMap.put("Hand, Power and Lawn-care Tools", "168169226");
		industryMap.put("Jewelry & Watches", "201723658");
		industryMap.put("Electronics", "462602");
		industryMap.put("Batteries, Power Storage Equipment & Generators", "34017034");
		industryMap.put("Electronic Components", "17239818");
		industryMap.put("Power Conversion & Protection Equipment", "50794250");
		industryMap.put("Semiconductor & Semiconductor Equipment", "67571466");
		industryMap.put("Food, Beverages & Tobacco", "528138");
		industryMap.put("Food & Beverages", "17305354");
		industryMap.put("Tobacco", "34082570");
		industryMap.put("Wineries & Breweries", "50859786");
		industryMap.put("Furniture", "593674");
		industryMap.put("Manufacturing General", "1380106");
		industryMap.put("Industrial Machinery & Equipment", "659210");
		industryMap.put("Medical Devices &  Equipment", "724746");
		industryMap.put("Pulp & Paper", "921354");
		industryMap.put("Plastic, Packaging & Containers", "986890");
		industryMap.put("Tires & Rubber", "1052426");
		industryMap.put("Telecommunication  Equipment", "1117962");
		industryMap.put("Test & Measurement Equipment", "1183498");
		industryMap.put("Toys & Games", "1249034");
		industryMap.put("Wire & Cable", "1314570");
		industryMap.put("Metals & Mining", "4362");
		industryMap.put("Metals & Mining General", "200970");
		industryMap.put("Metals & Minerals", "135434");
		industryMap.put("Mining", "69898");
		industryMap.put("Cities, Towns & Municipalities", "4618");
		industryMap.put("Cities, Towns & Municipalities General", "135690");
		industryMap.put("Public Safety", "70154");
		industryMap.put("Organizations", "4874");
		industryMap.put("Membership Organizations", "135946");
		industryMap.put("Charitable Organizations & Foundations", "70410");
		industryMap.put("Organizations General", "267018");
		industryMap.put("Religious Organizations", "201482");
		industryMap.put("Real Estate", "5130");
		industryMap.put("Retail", "5386");
		industryMap.put("Motor Vehicle Dealers", "136458");
		industryMap.put("Automobile Parts Stores", "201994");
		industryMap.put("Record, Video & Book Stores", "1185034");
		industryMap.put("Apparel & Accessories Retail", "70922");
		industryMap.put("Gas Stations, Convenience & Liquor Stores", "333066");
		industryMap.put("Department Stores, Shopping Centers & Superstores", "398602");
		industryMap.put("Consumer Electronics & Computers Retail", "267530");
		industryMap.put("Furniture", "595210");
		industryMap.put("Retail General", "1381642");
		industryMap.put("Flowers, Gifts & Specialty Stores", "529674");
		industryMap.put("Grocery Retail", "660746");
		industryMap.put("Home Improvement & Hardware Retail", "791818");
		industryMap.put("Vitamins, Supplements, & Health Stores", "726282");
		industryMap.put("Jewelry & Watch Retail", "857354");
		industryMap.put("Office Products Retail & Distribution", "988426");
		industryMap.put("Pet Products", "922890");
		industryMap.put("Drug Stores & Pharmacies", "464138");
		industryMap.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)", "1316106");
		industryMap.put("Sporting & Recreational Equipment Retail", "1053962");
		industryMap.put("Toys & Games", "1119498");
		industryMap.put("Video & DVD Rental", "1250570");
		industryMap.put("Software", "5642");
		industryMap.put("Software & Technical Consulting", "136714");
		industryMap.put("Software General", "267786");
		industryMap.put("Software Development & Design", "71178");
		industryMap.put("Business Intelligence (BI) Software", "16848394");
		industryMap.put("Content & Collaboration Software", "33625610");
		industryMap.put("Customer Relationship Management (CRM) Software", "50402826");
		industryMap.put("Database & File Management Software", "67180042");
		industryMap.put("Engineering Software", "83957258");
		industryMap.put("Enterprise Resource Planning (ERP) Software", "100734474");
		industryMap.put("Financial, Legal & HR Software", "117511690");
		industryMap.put("Healthcare Software", "134288906");
		industryMap.put("Multimedia, Games and Graphics Software", "151066122");
		industryMap.put("Networking Software", "167843338");
		industryMap.put("Supply Chain Management (SCM) Software", "184620554");
		industryMap.put("Security Software", "201397770");
		industryMap.put("Storage & System Management Software", "218174986");
		industryMap.put("Retail Software", "202250");
		industryMap.put("Telecommunications", "5898");
		industryMap.put("Cable & Satellite", "71434");
		industryMap.put("Telecommunications General", "268042");
		industryMap.put("Internet Service Providers, Website Hosting & Internet-related Services", "136970");
		industryMap.put("Telephony & Wireless", "202506");
		industryMap.put("Transportation", "6154");
		industryMap.put("Airlines, Airports & Air Services", "71690");
		industryMap.put("Freight & Logistics Services", "137226");
		industryMap.put("Transportation General", "399370");
		industryMap.put("Marine Shipping & Transportation", "202762");
		industryMap.put("Trucking, Moving & Storage", "333834");
		industryMap.put("Rail, Bus & Taxi", "268298");

		return industryMap;
	}

	public static Map<String, String> getIndustryGrpMap() {

		Map<String, String> industryMap = new HashMap<String, String>();
		industryMap.put("65802", "266");
		industryMap.put("131338", "266");
		industryMap.put("196874", "266");
		industryMap.put("197130", "522");
		industryMap.put("262666", "522");
		industryMap.put("328202", "522");
		industryMap.put("786954", "522");
		industryMap.put("655882", "522");
		industryMap.put("852490", "522");
		industryMap.put("393738", "522");
		industryMap.put("983562", "522");
		industryMap.put("590346", "522");
		industryMap.put("459274", "522");
		industryMap.put("721418", "522");
		industryMap.put("131594", "522");
		industryMap.put("524810", "522");
		industryMap.put("918026", "522");
		industryMap.put("66570", "1034");
		industryMap.put("132106", "1034");
		industryMap.put("197642", "1034");
		industryMap.put("66826", "1290");
		industryMap.put("132362", "1290");
		industryMap.put("197898", "1290");
		industryMap.put("656650", "1290");
		industryMap.put("263434", "1290");
		industryMap.put("328970", "1290");
		industryMap.put("394506", "1290");
		industryMap.put("460042", "1290");
		industryMap.put("525578", "1290");
		industryMap.put("591114", "1290");
		industryMap.put("198154", "1546");
		industryMap.put("67082", "1546");
		industryMap.put("132618", "1546");
		industryMap.put("263946", "1802");
		industryMap.put("263946", "1802");
		industryMap.put("132874", "1802");
		industryMap.put("198410", "1802");
		industryMap.put("67338", "1802");
		industryMap.put("67594", "2058");
		industryMap.put("198666", "2058");
		industryMap.put("329738", "2058");
		industryMap.put("133130", "2058");
		industryMap.put("264202", "2058");
		industryMap.put("67850", "2314");
		industryMap.put("133386", "2314");
		industryMap.put("198922", "2314");
		industryMap.put("395530", "2314");
		industryMap.put("264458", "2314");
		industryMap.put("329994", "2314");
		industryMap.put("133898", "2826");
		industryMap.put("330506", "2826");
		industryMap.put("68362", "2826");
		industryMap.put("199434", "2826");
		industryMap.put("264970", "2826");
		industryMap.put("17042186", "2826");
		industryMap.put("33819402", "2826");
		industryMap.put("330762", "3082");
		industryMap.put("68618", "3082");
		industryMap.put("134154", "3082");
		industryMap.put("67243018", "3082");
		industryMap.put("33688586", "3082");
		industryMap.put("50465802", "3082");
		industryMap.put("16911370", "3082");
		industryMap.put("84020234", "3082");
		industryMap.put("199690", "3082");
		industryMap.put("265226", "3082");
		industryMap.put("69642", "4106");
		industryMap.put("50401290", "4106");
		industryMap.put("16846858", "4106");
		industryMap.put("33624074", "4106");
		industryMap.put("462858", "4106");
		industryMap.put("135178", "4106");
		industryMap.put("200714", "4106");
		industryMap.put("266250", "4106");
		industryMap.put("331786", "4106");
		industryMap.put("397322", "4106");
		industryMap.put("69386", "3850");
		industryMap.put("134922", "3850");
		industryMap.put("200458", "3850");
		industryMap.put("16977674", "3850");
		industryMap.put("33754890", "3850");
		industryMap.put("67309322", "3850");
		industryMap.put("50532106", "3850");
		industryMap.put("790282", "3850");
		industryMap.put("855818", "3850");
		industryMap.put("265994", "3850");
		industryMap.put("17043210", "3850");
		industryMap.put("33820426", "3850");
		industryMap.put("50597642", "3850");
		industryMap.put("67374858", "3850");
		industryMap.put("331530", "3850");
		industryMap.put("17108746", "3850");
		industryMap.put("50663178", "3850");
		industryMap.put("67440394", "3850");
		industryMap.put("33885962", "3850");
		industryMap.put("397066", "3850");
		industryMap.put("17174282", "3850");
		industryMap.put("33951498", "3850");
		industryMap.put("184946442", "3850");
		industryMap.put("50728714", "3850");
		industryMap.put("67505930", "3850");
		industryMap.put("84283146", "3850");
		industryMap.put("101060362", "3850");
		industryMap.put("117837578", "3850");
		industryMap.put("134614794", "3850");
		industryMap.put("151392010", "3850");
		industryMap.put("168169226", "3850");
		industryMap.put("201723658", "3850");
		industryMap.put("462602", "3850");
		industryMap.put("34017034", "3850");
		industryMap.put("17239818", "3850");
		industryMap.put("50794250", "3850");
		industryMap.put("67571466", "3850");
		industryMap.put("528138", "3850");
		industryMap.put("17305354", "3850");
		industryMap.put("34082570", "3850");
		industryMap.put("50859786", "3850");
		industryMap.put("593674", "3850");
		industryMap.put("1380106", "3850");
		industryMap.put("659210", "3850");
		industryMap.put("724746", "3850");
		industryMap.put("921354", "3850");
		industryMap.put("986890", "3850");
		industryMap.put("1052426", "3850");
		industryMap.put("1117962", "3850");
		industryMap.put("1183498", "3850");
		industryMap.put("1249034", "3850");
		industryMap.put("1314570", "3850");
		industryMap.put("200970", "4362");
		industryMap.put("135434", "4362");
		industryMap.put("69898", "4362");
		industryMap.put("135690", "4618");
		industryMap.put("70154", "4618");
		industryMap.put("135946", "4874");
		industryMap.put("70410", "4874");
		industryMap.put("267018", "4874");
		industryMap.put("201482", "4874");
		industryMap.put("136458", "5386");
		industryMap.put("201994", "5386");
		industryMap.put("1185034", "5386");
		industryMap.put("70922", "5386");
		industryMap.put("333066", "5386");
		industryMap.put("398602", "5386");
		industryMap.put("267530", "5386");
		industryMap.put("595210", "5386");
		industryMap.put("1381642", "5386");
		industryMap.put("529674", "5386");
		industryMap.put("660746", "5386");
		industryMap.put("791818", "5386");
		industryMap.put("726282", "5386");
		industryMap.put("857354", "5386");
		industryMap.put("988426", "5386");
		industryMap.put("922890", "5386");
		industryMap.put("464138", "5386");
		industryMap.put("1316106", "5386");
		industryMap.put("1053962", "5386");
		industryMap.put("1119498", "5386");
		industryMap.put("1250570", "5386");
		industryMap.put("136714", "5642");
		industryMap.put("267786", "136714");
		industryMap.put("71178", "267786");
		industryMap.put("16848394", "71178");
		industryMap.put("33625610", "16848394");
		industryMap.put("50402826", "33625610");
		industryMap.put("67180042", "50402826");
		industryMap.put("83957258", "67180042");
		industryMap.put("100734474", "83957258");
		industryMap.put("117511690", "100734474");
		industryMap.put("134288906", "117511690");
		industryMap.put("151066122", "134288906");
		industryMap.put("167843338", "151066122");
		industryMap.put("184620554", "167843338");
		industryMap.put("201397770", "184620554");
		industryMap.put("218174986", "201397770");
		industryMap.put("202250", "218174986");
		industryMap.put("71434", "5898");
		industryMap.put("268042", "71434");
		industryMap.put("136970", "268042");
		industryMap.put("202506", "136970");
		industryMap.put("71690", "6154");
		industryMap.put("137226", "6154");
		industryMap.put("399370", "6154");
		industryMap.put("202762", "6154");
		industryMap.put("333834", "6154");
		industryMap.put("268298", "6154");

		return industryMap;
	}

	public static List<String> getNegativeTitles(CampaignCriteria campaignCriteria) {
		List<String> negativeTitles = new ArrayList<String>();
		List<String> cNegativeTitles = new ArrayList<String>();
		List<String> retNegativeTitles = new ArrayList<String>();
		if (campaignCriteria.getNegativeKeyWord() != null) {
			negativeTitles = Arrays.asList(campaignCriteria.getNegativeKeyWord().split(","));
			for (String negtit : negativeTitles) {
				cNegativeTitles.add(negtit.toLowerCase());
			}
		}
		if (!campaignCriteria.isIgnoreDefaultTitles()) {
			cNegativeTitles.add("coordinator");
			cNegativeTitles.add("interim");
			cNegativeTitles.add("program");
			cNegativeTitles.add("special");
			cNegativeTitles.add("assistant");
			cNegativeTitles.add("associate");
			cNegativeTitles.add("part time");
			cNegativeTitles.add("supervisor");
			cNegativeTitles.add("program");
			cNegativeTitles.add("lead");
			cNegativeTitles.add("head");
			cNegativeTitles.add("intern");
			cNegativeTitles.add("consulting");
			cNegativeTitles.add("consultant");
			cNegativeTitles.add("temp");
		}
		for (String negitive : cNegativeTitles) {
			retNegativeTitles.add(negitive.toLowerCase());
		}
		return retNegativeTitles;
	}

	public static Map<String, String[]> getRevenueMap() {
		Map<String, String[]> revMap = new HashMap<String, String[]>();
		/////
		String[] revArray1 = new String[2];
		revArray1[0] = "0";
		revArray1[1] = "10000";
		revMap.put("0-$10M", revArray1);

		String[] revArray2 = new String[2];
		revArray2[0] = "5000";
		revArray2[1] = "10000";
		revMap.put("$5M-$10M", revArray2);

		String[] revArray3 = new String[2];
		revArray3[0] = "10000";
		revArray3[1] = "25000";
		revMap.put("$10M-$25M", revArray3);

		String[] revArray4 = new String[2];
		revArray4[0] = "25000";
		revArray4[1] = "50000";
		revMap.put("$25M-$50M", revArray4);

		String[] revArray5 = new String[2];
		revArray5[0] = "50000";
		revArray5[1] = "100000";
		revMap.put("$50M-$100M", revArray5);

		String[] revArray6 = new String[2];
		revArray6[0] = "100000";
		revArray6[1] = "250000";
		revMap.put("$100M-$250M", revArray6);

		String[] revArray7 = new String[2];
		revArray7[0] = "250000";
		revArray7[1] = "500000";
		revMap.put("$250M-$500M", revArray7);

		String[] revArray8 = new String[2];
		revArray8[0] = "500000";
		revArray8[1] = "1000000";
		revMap.put("$500M-$1B", revArray8);

		String[] revArray9 = new String[2];
		revArray9[0] = "1000000";
		revArray9[1] = "5000000";
		revMap.put("$1B-$5B", revArray9);

		String[] revArray10 = new String[2];
		revArray10[0] = "5000000";
		revArray10[1] = "0";
		revMap.put("More than $5Billion", revArray10);

		return revMap;
	}

	public static  Map<String, String[]> getEmployeeMap(){
		Map<String,String[]> empMap = new HashMap<String, String[]>();
		/////
		String[] empArray1 = new String[2];
		empArray1[0]="0";
		empArray1[1]="4";
		empMap.put("1-5", empArray1);

		String[] empArray2 = new String[2];
		empArray2[0]="5";
		empArray2[1]="9";
		empMap.put("5-10", empArray2);

		String[] empArray3 = new String[2];
		empArray3[0]="10";
		empArray3[1]="19";
		empMap.put("10-20", empArray3);

		String[] empArray4 = new String[2];
		empArray4[0]="20";
		empArray4[1]="49";
		empMap.put("20-50", empArray4);

		String[] empArray5 = new String[2];
		empArray5[0]="50";
		empArray5[1]="99";
		empMap.put("50-100", empArray5);

		String[] empArray6 = new String[2];
		empArray6[0]="100";
		empArray6[1]="249";
		empMap.put("100-250", empArray6);

		String[] empArray7 = new String[2];
		empArray7[0]="250";
		empArray7[1]="499";
		empMap.put("250-500", empArray7);

		String[] empArray8 = new String[2];
		empArray8[0]="500";
		empArray8[1]="999";
		empMap.put("500-1000", empArray8);

		String[] empArray9 = new String[2];
		empArray9[0]="1000";
		empArray9[1]="4999";
		empMap.put("1000-5000", empArray9);

		String[] empArray10 = new String[2];
		empArray10[0]="5000";
		empArray10[1]="9999";
		empMap.put("5000-10000", empArray10);

		String[] empArray11 = new String[2];
		empArray11[0]="10000";
		empArray11[1]="0";
		empMap.put("Over 10000", empArray11);

		Map<String, String[]> sortedMap = new TreeMap<String, String[]>(empMap);



		return sortedMap;
	}

	public static String getSuppressedHostName(String domain) {
		try {
			domain = XtaasUtils.getSuppressedHostName(domain);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}
	
	/**
	 * 	key   --> zoom country name.  	    
	 *  value --> statecallconfig country name.
	 * 
	 * @return countrymap.
	 */
	
	public static Map<String, String> getZoomCountryMap() {
		Map<String, String> countryMap = new HashMap<String, String>();
		countryMap.put("Angola", "Angola");
		countryMap.put("Argentina", "Argentina");
		countryMap.put("Australia", "Australia");
		countryMap.put("Austria", "Austria");
		countryMap.put("Bahrain", "Bahrain");
		countryMap.put("Belgium", "Belgium");
		countryMap.put("Brazil", "Brazil");
		countryMap.put("Bulgaria", "Bulgaria");
		countryMap.put("Canada","CANADA");
		countryMap.put("Chile", "Chile");
		countryMap.put("China", "China");
		countryMap.put("Colombia", "Colombia");
		countryMap.put("CostaRica", "Costa Rica");
		countryMap.put("Croatia", "Croatia");
		countryMap.put("Czech Republic", "Czech Republic");
		countryMap.put("Denmark", "Denmark");
		countryMap.put("Egypt", "Egypt");
		countryMap.put("Equatorial Guinea", "Equatorial Guinea");
		countryMap.put("Finland", "Finland");
		countryMap.put("France", "France");
		countryMap.put("Germany", "Germany");
		countryMap.put("Ghana", "Ghana");
		countryMap.put("Greece", "Greece");
		countryMap.put("Hungary", "Hungary");
		countryMap.put("Iceland", "Iceland");
		countryMap.put("India", "India");
		countryMap.put("Indonesia", "Indonesia");
		countryMap.put("Iran", "Iran");
		countryMap.put("Ireland", "Ireland");
		countryMap.put("Israel", "Israel");
		countryMap.put("Italy", "Italy");
		countryMap.put("Japan", "Japan");
		countryMap.put("Jordan", "Jordan");
		countryMap.put("Kenya", "Kenya");
		countryMap.put("Kuwait", "Kuwait");
		countryMap.put("Luxembourg", "Luxembourg");
		countryMap.put("Malaysia", "Malaysia");
		countryMap.put("Mexico", "Mexico");
		countryMap.put("Morocco", "Morocco");
		countryMap.put("Netherlands", "Netherlands");
		countryMap.put("New Zealand", "New Zealand");
		countryMap.put("Nicaragua", "Nicaragua");
		countryMap.put("Nigeria", "Nigeria");
		countryMap.put("Norway", "Norway");
		countryMap.put("Oman", "Oman");
		countryMap.put("Pakistan", "Pakistan");
		countryMap.put("Panama", "Panama");
		countryMap.put("Peru", "Peru");
		countryMap.put("Philippines", "Philippines");
		countryMap.put("Poland", "Poland");
		countryMap.put("Portugal", "Portugal");
		countryMap.put("Qatar", "Qatar");
		countryMap.put("Romania", "Romania");
		countryMap.put("Saudi Arabia", "Saudi Arabia");
		countryMap.put("Singapore", "Singapore");
		countryMap.put("South Africa", "South Africa");
		countryMap.put("Spain", "Spain");
		countryMap.put("Sweden", "Sweden");
		countryMap.put("Switzerland", "Switzerland");
		countryMap.put("Thailand", "Thailand");
		countryMap.put("Tunisia", "Tunisia");
		countryMap.put("Turkey", "Turkey");
		countryMap.put("United Arab Emirates", "United Arab Emirates");
		countryMap.put("USA", "United States");
		countryMap.put("Venezuela", "Venezula");
		countryMap.put("Vietnam", "Vietnam");
		return countryMap;
	}
	
	public  static  LinkedHashMap<String, String[]> getRevenuSortedeMap(String[] revarr){
		 List<String> revSearch = Arrays.asList(revarr);
			LinkedHashMap<String,String[]> revMap = new LinkedHashMap<String, String[]>();
			/////
			if(revSearch.contains("0-$10M")) {
				 String[] revArray1 = new String[2];
				 revArray1[0]="0";
				 revArray1[1]="10000";
				 revMap.put("0-$10M", revArray1);
			}
			if(revSearch.contains("$5M-$10M")) {	
				 String[] revArray2 = new String[2];
				 revArray2[0]="5000";
				 revArray2[1]="10000";
				 revMap.put("$5M-$10M", revArray2);
			}
			if(revSearch.contains("$10M-$25M")) {
				 String[] revArray3 = new String[2];
				 revArray3[0]="10000";
				 revArray3[1]="25000";
				 revMap.put("$10M-$25M", revArray3);
			}
			if(revSearch.contains("$25M-$50M")) {
				 String[] revArray4 = new String[2];
				 revArray4[0]="25000";
				 revArray4[1]="50000";
				 revMap.put("$25M-$50M", revArray4);
			}
			if(revSearch.contains("$50M-$100M")) {	 
				 String[] revArray5 = new String[2];
				 revArray5[0]="50000";
				 revArray5[1]="100000";
				 revMap.put("$50M-$100M", revArray5);
			}
			if(revSearch.contains("$100M-$250M")) {
				 String[] revArray6 = new String[2];
				 revArray6[0]="100000";
				 revArray6[1]="250000";
				 revMap.put("$100M-$250M", revArray6);
			}
			if(revSearch.contains("$250M-$500M")) {
				 String[] revArray7 = new String[2];
				 revArray7[0]="250000";
				 revArray7[1]="500000";
				 revMap.put("$250M-$500M", revArray7);
			}
			if(revSearch.contains("$500M-$1B")) {
				 String[] revArray8 = new String[2];
				 revArray8[0]="500000";
				 revArray8[1]="1000000";
				 revMap.put("$500M-$1B", revArray8);
			}
			if(revSearch.contains("$1B-$5B")) {
				 String[] revArray9 = new String[2];
				 revArray9[0]="1000000";
				 revArray9[1]="5000000";
				 revMap.put("$1B-$5B", revArray9);
			}
			if(revSearch.contains("More than $5Billion")) {
				 String[] revArray10 = new String[2];
				 revArray10[0]="5000000";
				 revArray10[1]="0";
				 revMap.put("More than $5Billion", revArray10);
			}
			
			return revMap;
		}

}
