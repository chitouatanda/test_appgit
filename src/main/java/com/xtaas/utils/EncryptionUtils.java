/**
 * 
 */
package com.xtaas.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.application.service.AuthTokenGeneratorServiceImpl;
import com.xtaas.infra.security.AppManager;
import com.xtaas.infra.security.AppManager.App;

public final class EncryptionUtils {
	private static final Logger logger = LoggerFactory.getLogger(EncryptionUtils.class);
	
	private static final String SECRET_KEY = "ZX3DSASKDK98943JKDJSKAD9E23NMXD4";
	
	private static final String AES = "AES";
	
	/**
     * Computes an RFC 2104-compliant HMAC signature for an array of bytes and
     * returns the result as a Base64 encoded string.
     * 
     * Algos supported: http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Mac
     */
	public static String generateHash(String data, String secretKey) {
		try {
			Mac sha256HMAC = Mac.getInstance("HmacSHA256");
	
			sha256HMAC.init(new SecretKeySpec(secretKey.getBytes(), "HmacSHA256"));
			return Base64.encodeBase64String(sha256HMAC.doFinal(data.getBytes()));
		} catch (Exception e) {
			logger.error("generateHash(): Error occurred while generating a Hash.", e );
			return data;
		}
	}
	
	public static String generateMd5Hash(String data) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes());
			return Base64.encodeBase64String(md.digest());
		} catch (Exception e) {
			logger.error("generateHash(): Error occurred while generating a Hash.", e );
			return data;
		}
	}
	
	/**
	 * Two way encryption 
	 * @param stringIn
	 * @return
	 */
	public static String encrypt (String plainText) {
		return encrypt(plainText, SECRET_KEY);
	}
	
	public static String encrypt (String plainText, String secretKey) {
		try {
			Key key = generateKey(secretKey);
			
			Cipher cipher = Cipher.getInstance(AES);
			cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = cipher.doFinal(plainText.getBytes());
            
            return Base64.encodeBase64String(encVal);
		} catch (Exception e) {
			logger.error("Exception during encryption.", e);
			return plainText;
		}
	}
	
	public static String decrypt (String encryptedText) {
		return decrypt(encryptedText, SECRET_KEY);
	}

	public static String decrypt (String encryptedText, String secretKey) {
		try {
			Key key = generateKey(secretKey);
			
			// Instantiate the cipher
		    Cipher cipher = Cipher.getInstance(AES);
		    cipher.init(Cipher.DECRYPT_MODE, key);
		     
			byte[] encryptedTextBytes = Base64.decodeBase64(encryptedText);
		    byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		     
		    return new String(decryptedTextBytes);
		} catch (Exception e) {
			logger.error("Exception during decryption.", e);
			return encryptedText;
		}
	}
	
	private static Key generateKey(String secretKey) throws Exception {
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		byte[] keyBytes = sha.digest(secretKey.getBytes());
		keyBytes = Arrays.copyOf(keyBytes, 16); // use only first 128 bit

		Key key = new SecretKeySpec(keyBytes, AES);
        return key;
    }
		
	public static void main(String args[]) throws UnsupportedEncodingException 
	{
		String usernameWithSalt = "default{abodiga}";
		System.out.println("MD5 hash for " + usernameWithSalt + " is " + EncryptionUtils.generateMd5Hash(usernameWithSalt));
		
		String originalPassword = "5219775242407489880"; //salesforce secret key
        System.out.println("Original password: " + originalPassword);
        String encryptedPassword = encrypt(originalPassword);
        System.out.println("Encrypted password: " + encryptedPassword);
        System.out.println("URL ENCODED password: " + URLEncoder.encode(encryptedPassword,"UTF-8"));
        String urldecoded = URLDecoder.decode(encryptedPassword,"UTF-8").replaceAll(" ", "+");
        System.out.println("URL DECODED password: " + urldecoded);
        String decryptedPassword = decrypt(encryptedPassword);
        System.out.println("Decrypted password: " + decryptedPassword);
        
        System.out.println("======================= Generating Hash ================== " );
        long timeInMillis = System.currentTimeMillis();
        String authData = "dialer|XTAAS|ROLE_SYSTEM|" + timeInMillis; // username | org | role | time in MS
        System.out.println("authdata : " + authData);
		String token = App.DIALER.name() + ":" + EncryptionUtils.encrypt(authData,  AppManager.getSecurityKey(App.DIALER)) + ":" + EncryptionUtils.generateHash(authData, AppManager.getSecurityKey(App.DIALER));
        System.out.println("Generated Token is " + token);
        
        System.out.println("====================== Generating TOKEN =================== ");
        AuthTokenGeneratorServiceImpl authService = new AuthTokenGeneratorServiceImpl();
        String sysToken = authService.generateSystemToken(App.DIALER.name(), "dialer");
        System.out.println("DIALER SYSTEM TOKEN: " + sysToken);
        
        //generate authdata in format username|org|role|time in MS
  		StringBuilder authDataBuilder = new StringBuilder(); 
  		authDataBuilder.append("bssuser").append("|"); //user
  		authDataBuilder.append("XTAAS").append("|"); //org
  		authDataBuilder.append("ROLE_ADMIN").append("|"); //roles (comma separated list)
  		LocalDate ld = new LocalDate(2015,12,31);
  		authDataBuilder.append(ld.toDate().getTime()); //time in MS when token was generated
  		String authData1 = authDataBuilder.toString();
  		System.out.println("Encrypting DATA: " + authData1);
  		//generate token in the format appname:data:hash
  		String appSecurityKey = AppManager.getSecurityKey(EnumUtils.getEnum(App.class, "BACKEND_SUPPORT_SYSTEM"));
        
        token = "BACKEND_SUPPORT_SYSTEM" + ":" + EncryptionUtils.encrypt(authData1,  appSecurityKey) + ":" + EncryptionUtils.generateHash(authData1, appSecurityKey);
        System.out.println("Generated token: " + token);
        token = URLEncoder.encode(token, XtaasConstants.UTF8);
        
        System.out.println("======================= Decrypting TOKEN ================== " );
        //token = "DIALER%3As8iCItTKPrjLsbTc%2FdHat4gShl82Vv30WbSZh4JCdvEvojA8lT0N19%2FDs2fsWLNa%3A%2BltSRh4GAKqGF66fYYblTGqJ8GnX4N3gA9qnLgIcSDU%3D";
        System.out.println("Decrypting token: " + token);
        String urlDecodedToken1 = URLDecoder.decode(token, XtaasConstants.UTF8);
        System.out.println("Decoded token: " + urlDecodedToken1);
        String[] splittedToken = StringUtils.split(urlDecodedToken1,":");
        String appName = splittedToken[0];
        String encryptedData = splittedToken[1];
        String hash = splittedToken[2];
        String decryptedData = EncryptionUtils.decrypt(encryptedData, AppManager.getSecurityKey(appName));
        System.out.println("Decrypted Data= " + decryptedData);
        String[] splittedData = StringUtils.split(decryptedData, "|");
        
        String generatedHash = EncryptionUtils.generateHash(decryptedData, AppManager.getSecurityKey(appName));
        if (!hash.equals(generatedHash)) {
        	System.out.println("INVALID TOKEN");;
        } else {
        	System.out.println("VALID TOKEN");;
        }
        
        String username = splittedData[0];
        String org = splittedData[1];
        String role = splittedData[2];
        Long tokenGenTimeInMillis = Long.valueOf(splittedData[3]);
        System.out.println("UserName:" + username + ", Org:" + org + ", Role:" + role + ", TimeGenerated:"+tokenGenTimeInMillis);
        System.out.println("======================= TOKEN DECRYPTED ================== " );
	}
}
