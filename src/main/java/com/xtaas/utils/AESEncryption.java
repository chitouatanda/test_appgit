package com.xtaas.utils;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption {
	 //String plainText = "Hello World";
     
	private static final String sKey = "KqBJ0i0pvi5e2gZzPV4eLg==";//getSecretEncryptionKey();
	
    
     		
     // rebuild key using SecretKeySpec
     //SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
    /* String cipherText = encryptText(plainText, secKey);
     String decryptedText = decryptText(cipherText, secKey);*/

/*     System.out.println("Original Text:" + plainText);
     System.out.println("Encrypted Text :"+cipherText);
     System.out.println("Descrypted Text:"+decryptedText);
      
}
*/
  
/**
  * gets the AES encryption key. In your actual programs, this should be safely
  * stored.
  * @return
  * @throws Exception
  */

 public static String getSecretEncryptionKey() throws Exception{
     KeyGenerator generator = KeyGenerator.getInstance("AES");
     generator.init(256); // The AES key size in number of bits
     SecretKey secKey = generator.generateKey();
     String encodedKey = Base64.getEncoder().encodeToString(secKey.getEncoded());
     return encodedKey;
 }
  
 /**
  * Encrypts plainText in AES using the secret key
  * @param plainText
  * @param secKey
  * @return
  * @throws Exception
  */
 public static String encryptText(String plainText) throws Exception{
	 byte[] decodedKey = Base64.getDecoder().decode(sKey);
     SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
	 // AES defaults to AES/ECB/PKCS5Padding in Java 7
     Cipher aesCipher = Cipher.getInstance("AES");
     aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

   byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
  String enText =  Base64.getEncoder().encodeToString(byteCipherText);
     return enText;

}

  
 /**
  * Decrypts encrypted byte array using the key used for encryption.
  * @param byteCipherText
  * @param secKey
  * @return
  * @throws Exception
  */
 public static String decryptText(String cipherText) throws Exception {
	 byte[] decodedKey = Base64.getDecoder().decode(sKey);
     SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
	 // AES defaults to AES/ECB/PKCS5Padding in Java 7
     Cipher aesCipher = Cipher.getInstance("AES");
     aesCipher.init(Cipher.DECRYPT_MODE, secKey);
     byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(cipherText));
    /* byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
     return new String(original);*/
     return new String(bytePlainText);

}
}
