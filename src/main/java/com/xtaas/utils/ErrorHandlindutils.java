package com.xtaas.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.application.service.CampaignContactServiceImpl;
import com.xtaas.db.entity.Login;

public class ErrorHandlindutils {

	private static final Logger logger = LoggerFactory.getLogger(ErrorHandlindutils.class);

	private ErrorHandlindutils() {
	}

	public static void excelFileChecking(Workbook book, Map<Integer, List<String>> errorMap) {

//		System.out.println("111111111");
		Sheet sheet = book.getSheetAt(0);
		Row row = sheet.getRow(0);
//		int rowcount = sheet.getPhysicalNumberOfRows();
		int rowcount = ErrorHandlindutils.getNonBlankRowCount(sheet);
	//	System.out.println("Errorhandling" + rowcount);
		int colcounts = row.getPhysicalNumberOfCells();
		for (int i = 0; i <= sheet.getLastRowNum(); i++) {
			if (i == 0) {
				for (int j = 0; j < colcounts; j++) {
					if (j == row.getLastCellNum() - 1) {
						// row.createCell(row.getLastCellNum());
						row.createCell(row.getLastCellNum()).setCellValue((String) "Error");
						break;
					}
				}

			}
			break;
		}

		for (int i = 0; i < rowcount; i++) {
			if (i == 0) {
				continue;
			}
			row = sheet.getRow(i);
			List<String> errorList = errorMap.get(i);
			StringBuilder stringBuilder = new StringBuilder(" ");
			boolean first = true;
			if (errorList != null && errorList.size() > 0) {
				for (String string : errorList) {
					if (first) {
						first = false;
					} else {
						stringBuilder.append(", ");	
					}
					stringBuilder.append(string);
					stringBuilder.append("\n");
				}
			}
			if (row == null) {
				rowcount++;
				row = sheet.createRow(i);

			}else {
				boolean emptyRow= true;
				for (int j = 0; j < colcounts; j++) {
					Cell cell = row.getCell(j);
					if (cell != null && cell.getCellType() != CellType.BLANK) {
						emptyRow = false;
						break;
					}
				}
				if(emptyRow)
				{
					rowcount++;
				}
			}
			Cell c = row.createCell(colcounts);
			c.setCellValue((stringBuilder.toString()));

		}
		/*
		 * if (Boolean.TRUE.equals(errorFound)) { try { System.out.println("times");
		 * sendExcel(book);
		 * 
		 * 
		 * throw new IllegalArgumentException(
		 * "File not uploaded as some required fields are missing.Please check your email"
		 * );
		 * 
		 * 
		 * } catch (Throwable e) { // TODO Auto-generated catch block
		 * e.printStackTrace();
		 * 
		 * } }
		 */
	}

	public static void sendExcelwithMsg(Login loginUser,Workbook book,String xlsFileName,String msg,String campaignName,List<String> reportingEmail) throws IOException {
		Date date = new Date();
		String subject = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY ");
		String dateStr = sdf.format(date);
		if(xlsFileName==null ||  xlsFileName.isEmpty()) {
			xlsFileName=campaignName;
		}
		String fileName = dateStr + xlsFileName;

		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		try {
			book.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}

		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		book.close();//ABM Upload Status for MA-MIR-file uploaded with error
		if(campaignName != null) {
			subject = msg + " Upload Status For "+ campaignName + "- File Uploaded With Errors";
		}else {
			subject = msg + " Upload Status - File Uploaded With Errors";
		}
		
		String message = " Please Find The Attached Uploaded File.";
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if(reportingEmail != null && reportingEmail.size() > 0) {
			reportingEmail.add("data@xtaascorp.com");
			for(String email : reportingEmail) {
				XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", subject, message,
						fileNames);
			}
		}else {
			if(loginUser != null && loginUser.getEmail() != null) {
				XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, message,
						fileNames);
				XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com", subject, message,
						fileNames);
			}else {
				XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com", subject, message,
						fileNames);
			}
			
		}
	}
	
	public static void sendExcel(Login loginUser,Workbook book) throws IOException {

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY ");
		String dateStr = sdf.format(date);
		String fileName = dateStr + "error_handling" + ".xlsx";

		File file = new File(fileName);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		try {
			book.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}

		fileOut.close();
		List<File> fileNames = new ArrayList<File>();
		fileNames.add(file);
		book.close();
		String subject = dateStr + "XTaaS Error Handling";
		String message = " Please find the attached error handling file.";
		//Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
	//	System.out.println("login user name "+ loginUser.getUsername() +" firstname " + loginUser.getFirstName() +" lastname " + loginUser.getLastName());
		XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", subject, message,
				fileNames);

	}

	public static int getNonBlankRowCount(Sheet sheet) {
		int rowCount = 0;
		Row row = sheet.getRow(0);
		int colcount = row.getPhysicalNumberOfCells();
		Iterator<Row> rowIterator = sheet.rowIterator();
		rowCount = 0;
		while (rowIterator.hasNext()) {
			boolean valid =false;
			row = (Row) rowIterator.next();
			for (int i = 0; i < colcount; i++) {
				Cell cell = row.getCell(i);
//				String cellValue = cell.getCellType() == cell.CELL_TYPE_STRING
//						? cellValue = cell.getStringCellValue().trim()
//						: new BigDecimal(cell.getNumericCellValue()).toPlainString().trim();
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					valid=true;
					break;
				}
			}
			if (valid) {
				rowCount++;
			}

		}
		return rowCount;
	}
	
	public static int getNonBlankRowCountNumber(Sheet sheet) {
		int rowCount = 0;
		Row row = sheet.getRow(0);
		int colcount = row.getPhysicalNumberOfCells();
		Iterator<Row> rowIterator = sheet.rowIterator();
		rowCount = 0;
		while (rowIterator.hasNext()) {
			boolean valid =false;
			row = (Row) rowIterator.next();
			for (int i = 0; i < colcount; i++) {
				Cell cell = row.getCell(i);
//				String cellValue = cell.getCellType() == cell.CELL_TYPE_STRING
//						? cellValue = cell.getStringCellValue().trim()
//						: new BigDecimal(cell.getNumericCellValue()).toPlainString().trim();
				if (cell != null && cell.getCellType() != CellType.BLANK) {
					valid=true;
					break;
				}
			}
			if (valid) {
				rowCount++;
			}

		}
		return rowCount;
	}
	
}
