
package com.xtaas.ws.strikeiron.dnc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DoNotCallResult" type="{http://www.strikeiron.com/}SIWsOutputOfDoNotCallResult" minOccurs="0"/>
 *         &lt;element name="SISubscriptionInfo" type="{http://www.strikeiron.com/}SISubscriptionInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "doNotCallResult",
    "siSubscriptionInfo"
})
@XmlRootElement(name = "DoNotCallResponse")
public class DoNotCallResponse {

    @XmlElement(name = "DoNotCallResult")
    protected SIWsOutputOfDoNotCallResult doNotCallResult;
    @XmlElement(name = "SISubscriptionInfo", required = true)
    protected SISubscriptionInfo siSubscriptionInfo;

    /**
     * Gets the value of the doNotCallResult property.
     * 
     * @return
     *     possible object is
     *     {@link SIWsOutputOfDoNotCallResult }
     *     
     */
    public SIWsOutputOfDoNotCallResult getDoNotCallResult() {
        return doNotCallResult;
    }

    /**
     * Sets the value of the doNotCallResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SIWsOutputOfDoNotCallResult }
     *     
     */
    public void setDoNotCallResult(SIWsOutputOfDoNotCallResult value) {
        this.doNotCallResult = value;
    }

    /**
     * Gets the value of the siSubscriptionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SISubscriptionInfo }
     *     
     */
    public SISubscriptionInfo getSISubscriptionInfo() {
        return siSubscriptionInfo;
    }

    /**
     * Sets the value of the siSubscriptionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SISubscriptionInfo }
     *     
     */
    public void setSISubscriptionInfo(SISubscriptionInfo value) {
        this.siSubscriptionInfo = value;
    }

}
