
package com.xtaas.ws.strikeiron.dnc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DoNotCallResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DoNotCallResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CanBeCalled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StateDNCOrDMA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NationalDNC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsWireless" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DoNotCallResult", propOrder = {
    "phoneNumber",
    "canBeCalled",
    "stateDNCOrDMA",
    "nationalDNC",
    "isWireless"
})
public class DoNotCallResult {

    @XmlElement(name = "PhoneNumber")
    protected String phoneNumber;
    @XmlElement(name = "CanBeCalled")
    protected String canBeCalled;
    @XmlElement(name = "StateDNCOrDMA")
    protected String stateDNCOrDMA;
    @XmlElement(name = "NationalDNC")
    protected String nationalDNC;
    @XmlElement(name = "IsWireless")
    protected String isWireless;

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the canBeCalled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanBeCalled() {
        return canBeCalled;
    }

    /**
     * Sets the value of the canBeCalled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanBeCalled(String value) {
        this.canBeCalled = value;
    }

    /**
     * Gets the value of the stateDNCOrDMA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateDNCOrDMA() {
        return stateDNCOrDMA;
    }

    /**
     * Sets the value of the stateDNCOrDMA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateDNCOrDMA(String value) {
        this.stateDNCOrDMA = value;
    }

    /**
     * Gets the value of the nationalDNC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalDNC() {
        return nationalDNC;
    }

    /**
     * Sets the value of the nationalDNC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalDNC(String value) {
        this.nationalDNC = value;
    }

    /**
     * Gets the value of the isWireless property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWireless() {
        return isWireless;
    }

    /**
     * Sets the value of the isWireless property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWireless(String value) {
        this.isWireless = value;
    }

}
