package com.xtaas.ws.strikeiron.client;

import javax.xml.ws.Holder;

import com.xtaas.ws.strikeiron.phoneverification.SISubscriptionInfo;
import com.xtaas.ws.strikeiron.phoneverification.SIWsOutputOfCheckForCellPhoneResult;

public class StrikeIronCellPhoneVerificationCaller {
	String userID = "xtaascorp@strikeiron.com";
	String password = "strike1";
	
	Holder<SIWsOutputOfCheckForCellPhoneResult> siResponse = new Holder<SIWsOutputOfCheckForCellPhoneResult>();
	Holder<SISubscriptionInfo> subInfo = new Holder<SISubscriptionInfo>();
	
	public StrikeIronCellPhoneVerificationCaller(String phoneNumber) {
		StrikeIronCellPhoneVerificationClient.getServicePort().checkForCellPhone(userID, password, phoneNumber, siResponse, subInfo);
	}
	
	public boolean isPhoneNumberValid() {
		if (siResponse.value.getServiceStatus().getStatusNbr() == 401) {
			return false;
		}
		return true;
	}
	
	public boolean isCellPhone() {
		return (siResponse.value.getServiceResult().isIsCellPhone() == null) ? false : siResponse.value.getServiceResult().isIsCellPhone();  
	}

}
