package com.xtaas.ws.strikeiron.client;

import com.xtaas.ws.strikeiron.phoneverification.CellPhoneVerification;
import com.xtaas.ws.strikeiron.phoneverification.CellPhoneVerificationSoap;

public class StrikeIronCellPhoneVerificationClient {
	
	//Early instantiation of Service class. It's a Singleton object 
	private final static CellPhoneVerification instance = new CellPhoneVerification();
	
	/*
	 * Private Constructor. Don't let anyone else instantiate the Service class
	 */
	private StrikeIronCellPhoneVerificationClient() {   }
	
	public static CellPhoneVerificationSoap getServicePort() {
		return getServiceInstance().getCellPhoneVerificationSoap(); //return the port from service class
	}
	
	public static CellPhoneVerification getServiceInstance() {
		return instance;
	}
	
}
