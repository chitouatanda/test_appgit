package com.xtaas.ws.strikeiron.client;

import com.xtaas.ws.strikeiron.dnc.TelematchDoNotCall;
import com.xtaas.ws.strikeiron.dnc.TelematchDoNotCallSoap;

public class StrikeIronDNCClient {
	
	//Early instantiation of Service class. It's a Singleton object 
	private final static TelematchDoNotCall instance = new TelematchDoNotCall();
	
	/*
	 * Private Constructor. Don't let anyone else instantiate the Service class
	 */
	private StrikeIronDNCClient() {   }
	
	public static TelematchDoNotCallSoap getServicePort() {
		return getServiceInstance().getTelematchDoNotCallSoap(); //return the port from service class
	}
	
	public static TelematchDoNotCall getServiceInstance() {
		return instance;
	}
	
}
