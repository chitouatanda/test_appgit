package com.xtaas.ws.strikeiron.client;

import javax.xml.ws.Holder;

import com.xtaas.ws.strikeiron.dnc.SIWsOutputOfDoNotCallResult;

public class StrikeIronDNCCaller {
	String userID = "xtaascorp@strikeiron.com";
	String password = "strike1";
	
	Holder<SIWsOutputOfDoNotCallResult> doNotCallResult = new Holder<SIWsOutputOfDoNotCallResult>();
	Holder<com.xtaas.ws.strikeiron.dnc.SISubscriptionInfo> siSubscriptionInfo = new Holder<com.xtaas.ws.strikeiron.dnc.SISubscriptionInfo>();
	
	public StrikeIronDNCCaller(String phoneNumber) {
		StrikeIronDNCClient.getServicePort().doNotCall(userID, password, phoneNumber, doNotCallResult , siSubscriptionInfo);
	}
	
	public boolean isDNCCheckSuccess() {
		if (getStatusNbr() == 200) {
			return true;
		}
		return false;
	}
	
	public int getStatusNbr() {
		return doNotCallResult.value.getServiceStatus().getStatusNbr();
	}
	
	public String getStatusDescription() {
		return doNotCallResult.value.getServiceStatus().getStatusDescription();
	}
	
	public String getPhoneNumber() {
		return doNotCallResult.value.getServiceResult().getPhoneNumber();
	}
	
	public boolean canBeCalled() {
		return Boolean.parseBoolean(doNotCallResult.value.getServiceResult().getCanBeCalled());
	}
	
	public String getStateDNCOrDMA() {
		return doNotCallResult.value.getServiceResult().getStateDNCOrDMA();
	}
	
	public String getNationalDNC() {
		return doNotCallResult.value.getServiceResult().getNationalDNC();
	}
	
	public boolean isCellPhone() {
		return Boolean.parseBoolean(doNotCallResult.value.getServiceResult().getIsWireless());  
	}
}
