
package com.xtaas.ws.strikeiron.phoneverification;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.xtaas.strikeiron.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SISubscriptionInfo_QNAME = new QName("http://www.strikeiron.com", "SISubscriptionInfo");
    private final static QName _SILicenseInfo_QNAME = new QName("http://www.strikeiron.com", "SILicenseInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.xtaas.strikeiron.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SIWsResultArrayOfSIWsStatus }
     * 
     */
    public SIWsResultArrayOfSIWsStatus createSIWsResultArrayOfSIWsStatus() {
        return new SIWsResultArrayOfSIWsStatus();
    }

    /**
     * Create an instance of {@link GetServiceInfoResponse }
     * 
     */
    public GetServiceInfoResponse createGetServiceInfoResponse() {
        return new GetServiceInfoResponse();
    }

    /**
     * Create an instance of {@link GetStatusCodesResponse }
     * 
     */
    public GetStatusCodesResponse createGetStatusCodesResponse() {
        return new GetStatusCodesResponse();
    }

    /**
     * Create an instance of {@link CheckForCellPhone }
     * 
     */
    public CheckForCellPhone createCheckForCellPhone() {
        return new CheckForCellPhone();
    }

    /**
     * Create an instance of {@link SISubscriptionInfo }
     * 
     */
    public SISubscriptionInfo createSISubscriptionInfo() {
        return new SISubscriptionInfo();
    }

    /**
     * Create an instance of {@link SIWsResultArrayOfMethodStatusRecord }
     * 
     */
    public SIWsResultArrayOfMethodStatusRecord createSIWsResultArrayOfMethodStatusRecord() {
        return new SIWsResultArrayOfMethodStatusRecord();
    }

    /**
     * Create an instance of {@link GetStatusCodesForMethodResponse }
     * 
     */
    public GetStatusCodesForMethodResponse createGetStatusCodesForMethodResponse() {
        return new GetStatusCodesForMethodResponse();
    }

    /**
     * Create an instance of {@link GetStatusCodesForMethod }
     * 
     */
    public GetStatusCodesForMethod createGetStatusCodesForMethod() {
        return new GetStatusCodesForMethod();
    }

    /**
     * Create an instance of {@link CheckForCellPhoneResult }
     * 
     */
    public CheckForCellPhoneResult createCheckForCellPhoneResult() {
        return new CheckForCellPhoneResult();
    }

    /**
     * Create an instance of {@link ArrayOfSIWsStatus }
     * 
     */
    public ArrayOfSIWsStatus createArrayOfSIWsStatus() {
        return new ArrayOfSIWsStatus();
    }

    /**
     * Create an instance of {@link ArrayOfMethodStatusRecord }
     * 
     */
    public ArrayOfMethodStatusRecord createArrayOfMethodStatusRecord() {
        return new ArrayOfMethodStatusRecord();
    }

    /**
     * Create an instance of {@link GetServiceInfo }
     * 
     */
    public GetServiceInfo createGetServiceInfo() {
        return new GetServiceInfo();
    }

    /**
     * Create an instance of {@link ServiceInfoRecord }
     * 
     */
    public ServiceInfoRecord createServiceInfoRecord() {
        return new ServiceInfoRecord();
    }

    /**
     * Create an instance of {@link MethodStatusRecord }
     * 
     */
    public MethodStatusRecord createMethodStatusRecord() {
        return new MethodStatusRecord();
    }

    /**
     * Create an instance of {@link SIWsOutputOfSIWsResultArrayOfServiceInfoRecord }
     * 
     */
    public SIWsOutputOfSIWsResultArrayOfServiceInfoRecord createSIWsOutputOfSIWsResultArrayOfServiceInfoRecord() {
        return new SIWsOutputOfSIWsResultArrayOfServiceInfoRecord();
    }

    /**
     * Create an instance of {@link SIWsOutputOfCheckForCellPhoneResult }
     * 
     */
    public SIWsOutputOfCheckForCellPhoneResult createSIWsOutputOfCheckForCellPhoneResult() {
        return new SIWsOutputOfCheckForCellPhoneResult();
    }

    /**
     * Create an instance of {@link SIWsStatus }
     * 
     */
    public SIWsStatus createSIWsStatus() {
        return new SIWsStatus();
    }

    /**
     * Create an instance of {@link SILicenseInfo }
     * 
     */
    public SILicenseInfo createSILicenseInfo() {
        return new SILicenseInfo();
    }

    /**
     * Create an instance of {@link CheckForCellPhoneResponse }
     * 
     */
    public CheckForCellPhoneResponse createCheckForCellPhoneResponse() {
        return new CheckForCellPhoneResponse();
    }

    /**
     * Create an instance of {@link ArrayOfServiceInfoRecord }
     * 
     */
    public ArrayOfServiceInfoRecord createArrayOfServiceInfoRecord() {
        return new ArrayOfServiceInfoRecord();
    }

    /**
     * Create an instance of {@link SIWsOutputOfMethodStatusRecord }
     * 
     */
    public SIWsOutputOfMethodStatusRecord createSIWsOutputOfMethodStatusRecord() {
        return new SIWsOutputOfMethodStatusRecord();
    }

    /**
     * Create an instance of {@link GetStatusCodes }
     * 
     */
    public GetStatusCodes createGetStatusCodes() {
        return new GetStatusCodes();
    }

    /**
     * Create an instance of {@link SIWsResultArrayOfServiceInfoRecord }
     * 
     */
    public SIWsResultArrayOfServiceInfoRecord createSIWsResultArrayOfServiceInfoRecord() {
        return new SIWsResultArrayOfServiceInfoRecord();
    }

    /**
     * Create an instance of {@link SIWsOutputOfSIWsResultArrayOfMethodStatusRecord }
     * 
     */
    public SIWsOutputOfSIWsResultArrayOfMethodStatusRecord createSIWsOutputOfSIWsResultArrayOfMethodStatusRecord() {
        return new SIWsOutputOfSIWsResultArrayOfMethodStatusRecord();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SISubscriptionInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.strikeiron.com", name = "SISubscriptionInfo")
    public JAXBElement<SISubscriptionInfo> createSISubscriptionInfo(SISubscriptionInfo value) {
        return new JAXBElement<SISubscriptionInfo>(_SISubscriptionInfo_QNAME, SISubscriptionInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SILicenseInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.strikeiron.com", name = "SILicenseInfo")
    public JAXBElement<SILicenseInfo> createSILicenseInfo(SILicenseInfo value) {
        return new JAXBElement<SILicenseInfo>(_SILicenseInfo_QNAME, SILicenseInfo.class, null, value);
    }

}
