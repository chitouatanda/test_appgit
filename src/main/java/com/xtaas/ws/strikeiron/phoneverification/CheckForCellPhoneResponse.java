
package com.xtaas.ws.strikeiron.phoneverification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckForCellPhoneResult" type="{http://www.strikeiron.com}SIWsOutputOfCheckForCellPhoneResult" minOccurs="0"/>
 *         &lt;element name="SISubscriptionInfo" type="{http://www.strikeiron.com}SISubscriptionInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkForCellPhoneResult",
    "siSubscriptionInfo"
})
@XmlRootElement(name = "CheckForCellPhoneResponse")
public class CheckForCellPhoneResponse {

    @XmlElement(name = "CheckForCellPhoneResult")
    protected SIWsOutputOfCheckForCellPhoneResult checkForCellPhoneResult;
    @XmlElement(name = "SISubscriptionInfo", required = true)
    protected SISubscriptionInfo siSubscriptionInfo;

    /**
     * Gets the value of the checkForCellPhoneResult property.
     * 
     * @return
     *     possible object is
     *     {@link SIWsOutputOfCheckForCellPhoneResult }
     *     
     */
    public SIWsOutputOfCheckForCellPhoneResult getCheckForCellPhoneResult() {
        return checkForCellPhoneResult;
    }

    /**
     * Sets the value of the checkForCellPhoneResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SIWsOutputOfCheckForCellPhoneResult }
     *     
     */
    public void setCheckForCellPhoneResult(SIWsOutputOfCheckForCellPhoneResult value) {
        this.checkForCellPhoneResult = value;
    }

    /**
     * Gets the value of the siSubscriptionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SISubscriptionInfo }
     *     
     */
    public SISubscriptionInfo getSISubscriptionInfo() {
        return siSubscriptionInfo;
    }

    /**
     * Sets the value of the siSubscriptionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SISubscriptionInfo }
     *     
     */
    public void setSISubscriptionInfo(SISubscriptionInfo value) {
        this.siSubscriptionInfo = value;
    }

}
