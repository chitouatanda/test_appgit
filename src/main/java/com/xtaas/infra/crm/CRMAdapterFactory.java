package com.xtaas.infra.crm;

import java.util.Map;

import org.springframework.stereotype.Component;

public class CRMAdapterFactory {
	private Map<String, CRMAdapter> crmAdapters;
	
	/**
	 * @param crmAdapters the crmAdapters to set
	 */
	public void setCrmAdapters(Map<String, CRMAdapter> crmAdapters) {
		this.crmAdapters = crmAdapters;
	}



	public CRMAdapter getAdaptor(String crmName) {
		return crmAdapters.get(crmName);
	}
}
