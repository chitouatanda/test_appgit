/**
 * 
 */
package com.xtaas.infra.crm;

import java.util.ArrayList;

import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.valueobject.ExpressionGroup;

/**
 * An interface defining access methods for communicating with any external CRM or Marketing Automation solution. 
 * All CRM Adapters must implement this interface.
 * 
 * @author djain
 */
public interface CRMAdapter {
	
	/**
	 * Returns the list of Prospect objects as retrieved from CRM after applying the filter expression. 
	 * This method converts to XTaaS prospect object from the CRM specific lead object.
	 * 
	 * @param filterExpression
	 * @return List of Prospect object
	 */
	public ArrayList<Prospect> getFilteredLeads(Organization organization, ExpressionGroup filterCriteria) throws Exception;
	
	/**
	 * Returns the count of leads qualifying the filter criteria
	 * 
	 * @param filterExpression
	 * @return Count of leads qualifying the filter criteria
	 */
	public int getFilteredLeadsCount(Organization organization, ExpressionGroup filterCriteria) ;
	
	public Prospect getLead(Organization organization, String leadId) throws Exception;
}
