/**
 * 
 */
package com.xtaas.infra.crm.sf;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Class to represent Salesforce Lead Object
 * 
 * @author djain
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SFLead {
	@JsonProperty("totalSize")
	private Integer totalSize;
	@JsonProperty("records")
	private ArrayList<LinkedHashMap<String, Object>> recordMap;

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @return the recordMap
	 */
	public ArrayList<LinkedHashMap<String, Object>> getRecordMap() {
		return recordMap;
	}

	/**
	 * @param recordMap the recordMap to set
	 */
	public void setRecordMap(ArrayList<LinkedHashMap<String, Object>> recordMap) {
		this.recordMap = recordMap;
	}
}