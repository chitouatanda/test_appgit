package com.xtaas.infra.crm.sf;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SFLeadDescription {
	@JsonProperty("fields")
	private ArrayList<SFLeadField> fieldList;

	/**
	 * @return the fieldList
	 */
	public ArrayList<SFLeadField> getFieldList() {
		return fieldList;
	}
	/**
	 * @param fieldList the fieldList to set
	 */
	public void setFieldList(ArrayList<SFLeadField> fieldList) {
		this.fieldList = fieldList;
	} 
}

@JsonIgnoreProperties(ignoreUnknown = true)
class SFLeadField  {
	@JsonProperty("label")
	private String label;
	@JsonProperty("name")
	private String name;
	@JsonProperty("length")
	private Integer length;
	@JsonProperty("updateable")
	private Boolean updateable;
	@JsonProperty("type")
	private String dataType;
	@JsonProperty("picklistValues")
	private ArrayList<SFPickListValue> pickListValue;
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the pickListValue
	 */
	public ArrayList<SFPickListValue> getPickListValue() {
		return pickListValue;
	}
	/**
	 * @param pickListValue the pickListValue to set
	 */
	public void setPickListValue(ArrayList<SFPickListValue> pickListValue) {
		this.pickListValue = pickListValue;
	}
	/**
	 * @return the length
	 */
	public Integer getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(Integer length) {
		this.length = length;
	}
	/**
	 * @return the updateable
	 */
	public Boolean getUpdateable() {
		return updateable;
	}
	/**
	 * @param updateable the updateable to set
	 */
	public void setUpdateable(Boolean updateable) {
		this.updateable = updateable;
	}
	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}
	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
}

@JsonIgnoreProperties(ignoreUnknown = true)
class SFPickListValue {
	@JsonProperty("label")
	private String label;
	@JsonProperty("value")
	private String value;
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}