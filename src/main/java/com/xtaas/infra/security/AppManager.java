/**
 * 
 */
package com.xtaas.infra.security;

import org.apache.commons.lang3.EnumUtils;

/**
 * @author djain
 *
 */
public class AppManager {

	//Secret Key used for Hash generation for Rest API Authentication purpose
	public static final String SECRET_KEY_DIALER = "OE8QFDEAKD39108JBSYAUSQ8A32JNWA1";
	public static final String SECRET_KEY_CLIENT = "1QAZ2WSX3EDC4RFV5TGB6YHN7UJM";
	private static final String SECRET_KEY_BACKEND_SUPPORT_SYSTEM = "WE4EACFHHY54632RFCKDHNY6F35HMJF6";
	
	public enum App {
		DIALER,
		CLIENT,
		DIALER_PREPROCESS,
		BACKEND_SUPPORT_SYSTEM;
	}
	
	public static String getSecurityKey(App app) {
		String secretKey = null;
		switch (app) {
			case DIALER : 
				secretKey = SECRET_KEY_DIALER;
				break;
			case BACKEND_SUPPORT_SYSTEM :
				secretKey = SECRET_KEY_BACKEND_SUPPORT_SYSTEM;
				break;
			case CLIENT : 
				secretKey = SECRET_KEY_CLIENT;
		}
		return secretKey;
	}
	
	public static String getSecurityKey(String appName) {
		return AppManager.getSecurityKey(EnumUtils.getEnum(App.class, appName));
	}
}
