/**
 * 
 */
package com.xtaas.infra.security;

/**
 * Enum of the user Roles 
 * 
 * @author djain
 *
 */
public enum Roles {
	ROLE_ADMIN, ROLE_CLIENT, ROLE_PARTNER, ROLE_CAMPAIGN_MANAGER, ROLE_SUPERVISOR, ROLE_AGENT, ROLE_QA, ROLE_SECONDARYQA, ROLE_SYSTEM, ROLE_USER, ROLE_SUPPORT, ROLE_SUPPORT_ADMIN, ROLE_PREVIOUS_ADMINISTRATOR, ROLE_ANONYMOUS;
	
	@Override
    public String toString() {
		switch(this) {
        	case ROLE_ADMIN: return "ADMIN";
        	case ROLE_CLIENT: return "CLIENT";
        	case ROLE_PARTNER: return "PARTNER";
        	case ROLE_CAMPAIGN_MANAGER: return "CAMPAIGN_MANAGER";
        	case ROLE_SUPERVISOR: return "SUPERVISOR";
        	case ROLE_AGENT: return "AGENT";
        	case ROLE_QA: return "QA";
        	case ROLE_SECONDARYQA: return "SECONDARYQA";
        	case ROLE_SYSTEM: return "SYSTEM";
        	case ROLE_USER: return "USER";
        	case ROLE_SUPPORT: return "SUPPORT";
        	case ROLE_SUPPORT_ADMIN: return "SUPPORT_ADMIN";
        	case ROLE_PREVIOUS_ADMINISTRATOR: return "PREVIOUS_ADMINISTRATOR";
			case ROLE_ANONYMOUS: return "ANONYMOUS";
			default: throw new IllegalArgumentException();
		}
    }
}

