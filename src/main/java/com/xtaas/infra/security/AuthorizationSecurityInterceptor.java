/**
 * 
 */
package com.xtaas.infra.security;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.exception.UnauthorizedAccessException;
import com.xtaas.utils.XtaasUserUtils;

/**
 * Aspect which intercepts all the classes within application service layer and check for role authorization
 * before making call to the method.
 * 
 * @author djain
 */
@Component
@Aspect
public class AuthorizationSecurityInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(AuthorizationSecurityInterceptor.class);
	
	@Around("within(com.xtaas.application.service..*)")
	public Object checkAuthorization(ProceedingJoinPoint joinPoint) throws UnauthorizedAccessException, Throwable {
		//logger.info("checkAuthorization() : Checking Role Authorization for method call : [{}]", joinPoint.toShortString());
        //get the list of all the applicable roles as per the role hierarchy based on the roles assigned to the user
        List<GrantedAuthority> grantedAuthorities = XtaasUserUtils.getGrantedRoles();
        
        //identify the roles (using authorize annotation) granted to requested method
        List<Roles> allowedRoles = Arrays.asList(getAuthorizedRoles(joinPoint)); //list of roles who are authorized to access the requested method
        //role required for requested module
        boolean isAuthorized = false;
		
        if (allowedRoles != null && allowedRoles.get(0).equals(Roles.ROLE_USER) ) { //if no Authorize annotation speicifed on method then grant access..sometimes methods are called within application then Roles will not be specified
        	isAuthorized = true;
        } else {
			for (GrantedAuthority authority : grantedAuthorities) {
				if (allowedRoles.contains(Enum.valueOf(Roles.class, authority.getAuthority()))) {
					isAuthorized = true; 
					break;
				}
			}
        }
		
        if (isAuthorized) {
        	//logger.info("checkAuthorization() : User [{}] authorized for method call [{}]", auth.getName(), joinPoint.toShortString());
        	return joinPoint.proceed();
        } else {
        	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        	logger.error("checkAuthorization() : User [{}] not authorized for method call [{}] ", auth.getName(), joinPoint.toShortString());
        	throw new UnauthorizedAccessException("User [" + auth.getName() + "] not authorized");
        }
    }


	private Roles[] getAuthorizedRoles(ProceedingJoinPoint pjp) {
		MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
		Method method = methodSignature.getMethod();
		String methodName = pjp.getSignature().getName();
		
		if (method.getDeclaringClass().isInterface()) {
		    try {
				method = pjp.getTarget().getClass().getDeclaredMethod(methodName, method.getParameterTypes());
			} catch (SecurityException e) {
				logger.error("getAuthorizedRoles(): ERROR OCCURRED.", e);
			} catch (NoSuchMethodException e) {
				logger.error("getAuthorizedRoles(): ERROR OCCURRED.", e);
			}    
		}
		
		Authorize authorize = (Authorize) method.getAnnotation(Authorize.class);
		if (null == authorize)
			return new Roles[]{Roles.ROLE_USER};
		
		return authorize.roles();
	}
}
