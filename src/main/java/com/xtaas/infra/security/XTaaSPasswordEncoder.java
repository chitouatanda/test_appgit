package com.xtaas.infra.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component(value = "xTaaSPasswordEncoder")
public class XTaaSPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		return getPasswordHash(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		// HACK - Added because Spring MVC 5 doesn't support MD5 PasswordEncoding
		String passwordWithSalt = "";
		String[] encodedPasswordAndSalt = encodedPassword.split(" | ");
		if (encodedPasswordAndSalt.length > 1) {
			passwordWithSalt = rawPassword.toString() + "{" + encodedPasswordAndSalt[2] + "}";
		} else {
			passwordWithSalt = rawPassword.toString() + "{" + encodedPasswordAndSalt[0] + "}";
		}
		return getPasswordHash(passwordWithSalt).equals(encodedPasswordAndSalt[0]);
	}

	private String getPasswordHash(String passwordWithSalt) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(passwordWithSalt.toString().getBytes());
			return Base64.getEncoder().encodeToString(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}
