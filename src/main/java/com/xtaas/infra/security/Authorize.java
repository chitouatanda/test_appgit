/**
 * 
 */
package com.xtaas.infra.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for role authorization at method level
 * 
 * @author djain
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
public @interface Authorize {
	Roles[] roles() default {Roles.ROLE_USER};
}
