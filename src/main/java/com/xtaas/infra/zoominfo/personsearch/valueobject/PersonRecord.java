package com.xtaas.infra.zoominfo.personsearch.valueobject;
import java.util.List;

public class PersonRecord {
	
	private String personId;
	private String zoomPersonUrl;
	private String personDetailXmlUrl;
	private String lastMentioned;
	private String isUserPosted;
	private String isPast;
	private String imageUrl;
	private int referencesCount;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private LocalAddress localAddress;
	private String email;
	private String phone;
	private String fax;
	private CurrentEmployment currentEmployment;
	private List<String> topLevelIndustry;
	private List<String> industry;
	private String managementLevel;
	private String lastUpdatedDate;
	
	public String getPersonId() {
		return personId;
	}
	
	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}
	
	public String getPersonDetailXmlUrl() {
		return personDetailXmlUrl;
	}
	
	public String getLastMentioned() {
		return lastMentioned;
	}
	
	public String getIsUserPosted() {
		return isUserPosted;
	}
	
	public String getIsPast() {
		return isPast;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public int getReferencesCount() {
		return referencesCount;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	
	public String getMiddleInitial() {
		return middleInitial;
	}
	
	
	public String getLastName() {
		return lastName;
	}
	
	public LocalAddress getLocalAddress() {
		return localAddress;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPhone() {
		return phone;
	}
	
	
	public String getFax() {
		return fax;
	}
	
	public CurrentEmployment getCurrentEmployment() {
		return currentEmployment;
	}
	
	public List<String> getTopLevelIndustry() {
		return topLevelIndustry;
	}
	
	public List<String> getIndustry() {
		return industry;
	}

    public String getManagementLevel() {
        return managementLevel;
    }

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
    
    
}
