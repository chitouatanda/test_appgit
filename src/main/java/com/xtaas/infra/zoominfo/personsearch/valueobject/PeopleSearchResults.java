package com.xtaas.infra.zoominfo.personsearch.valueobject;

import java.util.List;

public class PeopleSearchResults {

	private List<PersonRecord> personRecord;
	
	public List<PersonRecord> getPersonRecord() {
		return personRecord;
	}
}
