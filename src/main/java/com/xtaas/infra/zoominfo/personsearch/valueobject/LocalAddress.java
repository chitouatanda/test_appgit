package com.xtaas.infra.zoominfo.personsearch.valueobject;

public class LocalAddress {
	
	private String street;
	private String city;
	private String state;
	private String zip;
	private String country;

	public String getCity() {
		return city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getState() {
		return state;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getZip() {
		return zip;
	}
}
