package com.xtaas.infra.zoominfo.personsearch.valueobject;

import java.util.List;
public class PersonDetailResponse {

	//private String personID;
	private String personId;
	private String zoomPersonUrl;
	private String lastUpdatedDate;
	private String isUserPosted;
	private String imageUrl;
	private int referencesCount;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String emailAddress;
	private String directPhone;
	private String companyPhone;
	private String fax;
	private List<CurrentEmployment> currentEmployment;
	private List<String> industry;
	private LocalAddress localAddress;
	private String managementLevel;
	private boolean isEU; 
	
	public LocalAddress getLocalAddress() {
		return localAddress;
	}
	/*public String getPersonID() {
		return personID;
	}*/
	public String getPersonId() {
		return personId;
	}
	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public String getIsUserPosted() {
		return isUserPosted;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public int getReferencesCount() {
		return referencesCount;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getDirectPhone() {
		return directPhone;
	}
	
	public String getCompanyPhone() {
		return companyPhone;
	}
	
	public String getFax() {
		return fax;
	}
	public List<CurrentEmployment> getCurrentEmployment() {
		return currentEmployment;
	}
	public List<String> getIndustry() {
		return industry;
	}
    public String getManagementLevel() {
        return managementLevel;
    }
	public boolean isEU() {
		return isEU;
	}
    
    
}
