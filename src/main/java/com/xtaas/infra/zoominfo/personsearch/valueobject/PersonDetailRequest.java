package com.xtaas.infra.zoominfo.personsearch.valueobject;

import java.util.List;
public class PersonDetailRequest {

	private String PersonID;
	private String ZoomPersonUrl;
	private String LastUpdatedDate;
	private String IsUserPosted;
	private String ImageUrl;
	private int ReferencesCount;
	private String FirstName;
	private String MiddleInitial;
	private String LastName;
	private String Email;
	private String DirectPhone;
	private String CompanyPhone;
	private String Fax;
	private List<CurrentEmployment> CurrentEmployment;
	private List<String> Industry;
	private LocalAddress LocalAddress;
	private String ManagementLevel;
	
	public LocalAddress getLocalAddress() {
		return LocalAddress;
	}
	public String getPersonID() {
		return PersonID;
	}
	public String getZoomPersonUrl() {
		return ZoomPersonUrl;
	}
	public String getLastUpdatedDate() {
		return LastUpdatedDate;
	}
	public String getIsUserPosted() {
		return IsUserPosted;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public int getReferencesCount() {
		return ReferencesCount;
	}
	public String getFirstName() {
		return FirstName;
	}
	public String getMiddleInitial() {
		return MiddleInitial;
	}
	public String getLastName() {
		return LastName;
	}
	public String getEmail() {
		return Email;
	}
	public String getDirectPhone() {
		return DirectPhone;
	}
	
	public String getCompanyPhone() {
		return CompanyPhone;
	}
	
	public String getFax() {
		return Fax;
	}
	public List<CurrentEmployment> getCurrentEmployment() {
		return CurrentEmployment;
	}
	public List<String> getIndustry() {
		return Industry;
	}
    public String getManagementLevel() {
        return ManagementLevel;
    }
}
