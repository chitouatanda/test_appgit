package com.xtaas.infra.zoominfo.personsearch.valueobject;

public class Company {

	private String companyId;
	private String zoomCompanyUrl;
	private String companyDetailXmlUrl;
	private String companyName;
	private String website;
	private String companyPhone;
	private CompanyAddress companyAddress;
	private String companyWebsite;
	private String companyLogo;
	private String companyRevenue;
	private long companyRevenueIn000s;
	private String companyRevenueRange;
	private long companyEmployeeCount;
	private String companyEmployeeCountRange;
	
	
	public String getWebsite() {
		return website;
	}
	
	public String getCompanyId() {
		return companyId;
	}
	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}
	public String getCompanyDetailXmlUrl() {
		return companyDetailXmlUrl;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	
	public CompanyAddress getCompanyAddress() {
		return companyAddress;
	}
	
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	
	public long getCompanyEmployeeCount() {
		return companyEmployeeCount;
	}
	
	public String getCompanyEmployeeCountRange() {
		return companyEmployeeCountRange;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	
	public String getCompanyRevenue() {
		return companyRevenue;
	}
	
	public long getCompanyRevenueIn000s() {
		return companyRevenueIn000s;
	}
	
	public String getCompanyRevenueRange() {
		return companyRevenueRange;
	}
}
