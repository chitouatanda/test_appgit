package com.xtaas.infra.zoominfo.personsearch.valueobject;

public class PeopleSearchRequest {
	
	public SearchParameters SearchParameters;
	public int TotalResults;
	public int MaxResults;
	public PeopleSearchResults PeopleSearchResults;
	
	
	public PeopleSearchResults getPeopleSearchResults() {
		return PeopleSearchResults;
	}
	
	public int getTotalResults() {
		return TotalResults;
	}
	
	public int getMaxResults() {
		return MaxResults;
	}
	
	public SearchParameters getSearchParameters() {
		return SearchParameters;
	}
	
}
