package com.xtaas.infra.zoominfo.personsearch.valueobject;

public class PersonSearchResponse {
	
	public SearchParameters searchParameters;
	public int totalResults;
	public int maxResults;
	public PeopleSearchResults peopleSearchResults;
	
	
	public PeopleSearchResults getPeopleSearchResults() {
		return peopleSearchResults;
	}
	
	public int getTotalResults() {
		return totalResults;
	}
	
	public int getMaxResults() {
		return maxResults;
	}
	
	public SearchParameters getSearchParameters() {
		return searchParameters;
	}
	
}
