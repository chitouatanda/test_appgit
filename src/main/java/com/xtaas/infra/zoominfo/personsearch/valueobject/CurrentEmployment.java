package com.xtaas.infra.zoominfo.personsearch.valueobject;

import java.util.List;

import org.slf4j.LoggerFactory;


public class CurrentEmployment {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CurrentEmployment.class);

	private String jobTitle;
	private String fromDate;
	private String toDate;
	private List<String> jobFunction;
	private List<String> managementLevel;
	private Company company;
	
	public String getFromDate() {
		return fromDate;
	}
	
	public String getToDate() {
		return toDate;
	}
	
	public String getJobTitle() {
		return jobTitle;
	}
	
	public String getJobFunction() {
		if(jobFunction==null){
			return "NA";
		}
		return jobFunction.get(0);
	}
	
	public List<String> getManagementLevel() {
		return managementLevel;
	}
	
	public Company getCompany() {
		return company;
	}
}
