 package com.xtaas.infra.zoominfo.companysearch.valueobject;

import java.util.List;

import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;

public class CompanyRecord {

    private String companyId;
    private String companyName;
    private String companyWebsite;
    private String zoomCompanyUrl;
    private String companyDetailXmlUrl;
    private boolean isDefunct;
    private String phone;
    private CompanyAddress companyAddress;
    private String companyRevenue;
    private Long companyRevenueIn000s;
    private String companyRevenueRange;
    private Long companyEmployeeCount;
    private String companyEmployeeCountRange;
    private String companyType;
    private List<String> companySIC;
    private List<String> companyNAICS;
    private List<String> companyProductsAndServices;
    //private Hashtags hashtags;
    private SimilarCompanies similarCompanies;
    private String companyTicker;
    private String companyDescription;
    private List<String> companyTopLevelIndustry;
    private List<String> companyIndustry;
    private List<String> companyRanking;
    
    
    public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}
	public String getCompanyDetailXmlUrl() {
		return companyDetailXmlUrl;
	}
	public boolean isDefunct() {
		return isDefunct;
	}
	public String getPhone() {
		return phone;
	}
	public CompanyAddress getCompanyAddress() {
		return companyAddress;
	}
	public String getCompanyRevenue() {
		return companyRevenue;
	}
	public Long getCompanyRevenueIn000s() {
		return companyRevenueIn000s;
	}
	public String getCompanyRevenueRange() {
		return companyRevenueRange;
	}
	public Long getCompanyEmployeeCount() {
		return companyEmployeeCount;
	}
	public String getCompanyEmployeeCountRange() {
		return companyEmployeeCountRange;
	}
	public String getCompanyType() {
		return companyType;
	}
	public List<String> getCompanySIC() {
		return companySIC;
	}
	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}
	public List<String> getCompanyProductsAndServices() {
		return companyProductsAndServices;
	}
	/*public Hashtags getHashtags() {
		return hashtags;
	}*/
	public SimilarCompanies getSimilarCompanies() {
		return similarCompanies;
	}
	public String getCompanyTicker() {
		return companyTicker;
	}
	public String getCompanyDescription() {
		return companyDescription;
	}
	public List<String> getCompanyTopLevelIndustry() {
		return companyTopLevelIndustry;
	}
	public List<String> getCompanyIndustry() {
		return companyIndustry;
	}
	public List<String> getCompanyRanking() {
		return companyRanking;
	}
	public String getCompanyId() {
        return companyId;
    }
    public String getCompanyName() {
        return companyName;
    }
    public String getCompanyWebsite() {
        return companyWebsite;
    }
}
