package com.xtaas.infra.zoominfo.companysearch.valueobject;

import com.xtaas.infra.zoominfo.personsearch.valueobject.SearchParameters;

public class CompanySearchRequest {

    public SearchParameters SearchParameters;
    public int TotalResults;
    public int MaxResults;
    public CompanySearchResults CompanySearchResults;
    public SearchParameters getSearchParameters() {
        return SearchParameters;
    }
    public int getTotalResults() {
        return TotalResults;
    }
    public int getMaxResults() {
        return MaxResults;
    }
    public CompanySearchResults getCompanySearchResults() {
        return CompanySearchResults;
    }
}
