 package com.xtaas.infra.zoominfo.companysearch.valueobject;

public class SimilarCompany {
	private String companyId;
	private String companyName;
	private String domain;
	private String revenue;
	private Long employeeCount;
	public String getCompanyId() {
		return companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getDomain() {
		return domain;
	}
	public String getRevenue() {
		return revenue;
	}
	public Long getEmployeeCount() {
		return employeeCount;
	}
}
