package com.xtaas.infra.zoominfo.companysearch.valueobject;

import java.util.List;

import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;

public class CompanySearchResults {

    private List<CompanyRecord> companyRecord;

    public List<CompanyRecord> getCompanyRecord() {
        return companyRecord;
    }
}
