package com.xtaas.infra.zoominfo.companysearch.valueobject;

import java.util.List;

public class CompanyDetailRequest {

    private String CompanyID;
    private String ZoomCompanyUrl;
    private String CompanyName;
    private String Phone;
    private String Website;
    private List<String> TopLevelIndustry;
    
    public String getCompanyID() {
        return CompanyID;
    }
    public String getZoomCompanyUrl() {
        return ZoomCompanyUrl;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public String getPhone() {
        return Phone;
    }
    public String getWebsite() {
        return Website;
    }
    public List<String> getTopLevelIndustry() {
        return TopLevelIndustry;
    }
}