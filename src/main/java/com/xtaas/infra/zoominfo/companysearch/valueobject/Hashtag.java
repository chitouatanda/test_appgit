 package com.xtaas.infra.zoominfo.companysearch.valueobject;

public class Hashtag {

	public String value;
	public String displayLabel;
	public String searchString;
	public String description;
	public String group;
	public Long priority;
	public boolean isCategorized;
	public String parentCategory;
	
	public String getValue() {
		return value;
	}
	public String getDisplayLabel() {
		return displayLabel;
	}
	public String getSearchString() {
		return searchString;
	}
	public String getDescription() {
		return description;
	}
	public String getGroup() {
		return group;
	}
	public Long getPriority() {
		return priority;
	}
	public boolean isCategorized() {
		return isCategorized;
	}
	public String getParentCategory() {
		return parentCategory;
	}
}
