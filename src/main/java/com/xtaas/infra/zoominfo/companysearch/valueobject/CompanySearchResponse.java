package com.xtaas.infra.zoominfo.companysearch.valueobject;

import com.xtaas.infra.zoominfo.personsearch.valueobject.SearchParameters;

public class CompanySearchResponse {

    public SearchParameters searchParameters;
    public int totalResults;
    public int maxResults;
    public CompanySearchResults companySearchResults;
    public SearchParameters getSearchParameters() {
        return searchParameters;
    }
    public int getTotalResults() {
        return totalResults;
    }
    public int getMaxResults() {
        return maxResults;
    }
    public CompanySearchResults getCompanySearchResults() {
        return companySearchResults;
    }
}
