package com.xtaas.infra.zoominfo.service;

public class PhoneTypeList {
private boolean valid;
private String number;
private String local_format;
private String internationl_format;
private String country_prefix;
private String country_code;
private String country_name;
private String location;
private String carrier;
private String line_type;
public boolean isValid() {
	return valid;
}
public String getNumber() {
	return number;
}
public String getLocal_format() {
	return local_format;
}
public String getInternationl_format() {
	return internationl_format;
}
public String getCountry_prefix() {
	return country_prefix;
}
public String getCountry_code() {
	return country_code;
}
public String getCountry_name() {
	return country_name;
}
public String getLocation() {
	return location;
}
public String getCarrier() {
	return carrier;
}
public String getLine_type() {
	return line_type;
}


}
