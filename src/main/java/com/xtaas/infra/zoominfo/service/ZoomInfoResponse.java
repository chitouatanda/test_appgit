package com.xtaas.infra.zoominfo.service;

import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanyDetailRequest;
import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanySearchRequest;
import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanySearchResponse;
import com.xtaas.infra.zoominfo.personmatch.valueobject.PersonMatchResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonSearchResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PeopleSearchRequest;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailRequest;

public class ZoomInfoResponse {
	
	private PeopleSearchRequest peopleSearchRequest;
	private PersonDetailRequest personDetailRequest;
	
	private PersonSearchResponse personSearchResponse;
	private PersonDetailResponse personDetailResponse;
	
	private PersonMatchResponse personMatchResponse ;
	
	private CompanySearchRequest companySearchRequest;
	private CompanyDetailRequest companyDetailRequest;
	
	private CompanySearchResponse companySearchResponse;
	
	 
	public CompanySearchResponse getCompanySearchResponse() {
		return companySearchResponse;
	}

	public PersonSearchResponse getPersonSearchResponse() {
		return personSearchResponse;
	}
	
	public PersonDetailResponse getPersonDetailResponse() {
		return personDetailResponse;
	}
	
	public PeopleSearchRequest getPeopleSearchRequest() {
		return peopleSearchRequest;
	}
	
	public PersonDetailRequest getPersonDetailRequest() {
		return personDetailRequest;
	}

	public PersonMatchResponse getPersonMatchResponse() {
		return personMatchResponse;
	}

    public CompanySearchRequest getCompanySearchRequest() {
        return companySearchRequest;
    }
    
    public CompanyDetailRequest getCompanyDetailRequest() {
        return companyDetailRequest;
    }

   

}
