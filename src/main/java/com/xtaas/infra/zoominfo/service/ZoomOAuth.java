package com.xtaas.infra.zoominfo.service;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;

/* <!-- https://mvnrepository.com/artifact/com.googlecode.jsontoken/jsontoken -->
  		<dependency>
       		<groupId>com.googlecode.jsontoken</groupId>
      		<artifactId>jsontoken</artifactId>
       		<version>1.0</version>
  		</dependency>*/

@Component
public class ZoomOAuth {

	private String partnerKey = "Xtaascorp.partner";
	private String password = "inKnEjJu0";

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		ZoomOAuth companySearchAndDetailPurchase = context
				.getBean(ZoomOAuth.class);
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken("djain", "secret",
						authorities));
		// companySearchAndDetailPurchase.getCompanyData();

		companySearchAndDetailPurchase.createJWT();

	}

	// Sample method to construct a JWT
	public String createJWT() {
		String oAuth = "";
		try {
		
			/*Date todaysDate = XtaasDateUtils.getCurrentDateInTZ(0, XtaasDateUtils.TZ_UTC).toDate();
			System.out.println(todaysDate);*/
			
			Calendar cal = Calendar.getInstance();
			HmacSHA256Signer signer = new HmacSHA256Signer("zoominfo", null,
					password.getBytes()); // The ISSUER is "zoominfo", the
												// keyID is null, and the
												// "secret value in bytes" is
												// the partner password.
			JsonToken token = new JsonToken(signer);
			token.setIssuedAt(new org.joda.time.Instant(cal.getTimeInMillis()));// Current
																				// time
																				// in
																				// milliseconds
			token.setExpiration(new org.joda.time.Instant(
					cal.getTimeInMillis() + 3600000));// current time + 1 hour
														// is the expiration
														// time.
			token.setParam("jti", UUID.randomUUID().toString());

			JsonObject infoObject = new JsonObject();
			infoObject.addProperty("ziPartnerCode", partnerKey);
			JsonObject payload = token.getPayloadAsJsonObject();
			payload.add("ziPayLoad", infoObject);

			String s = token.serializeAndSign();
			
			//System.out.println("token" + s);
			String zresponse = httpPost(s);
			// <token>9b62c009-9ba4-406f-9fbf-e835da820c66</token>
			int fIndex = zresponse.indexOf("<token>");
			if (fIndex > 0) {
				int eIndex = zresponse.indexOf("</token>");
				if (eIndex > 0) {
					oAuth = zresponse.substring(fIndex + 7, eIndex);
					//System.out.println(oAuth);
				}
			}
			/*String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String ur = baseUrl
					+ "pc="+partnerKey+"&personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ oAuth;*/
			//System.out.println(ur);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return oAuth;
		// Builds the JWT and serializes it to a compact, URL-safe string
		// return builder.compact();
	}

	public String httpPost(String token) {
		try {
			String url = "https://partnerapi.zoominfo.com/partnerapi/v2/token";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpP = new HttpPost(url);

			httpP.setHeader("Accept", "application/xml");
			httpP.setHeader("Content-Type", "application/xml");

			String jwt = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
					+ "<TokenPostInput xmlns=\"http://partnerapi.zoominfo.com/partnerapistatic/xsd/V1/TokenPostInput.xsd\">"
					+ "<pc>"+partnerKey+"</pc>" + "<ziToken>" + token
					+ "</ziToken></TokenPostInput>";

			StringEntity input = new StringEntity(jwt);
			input.setContentType("application/xml");
			httpP.setEntity(input);

			HttpResponse response = httpClient.execute(httpP);
			StatusLine responseCode = response.getStatusLine();
		/*	System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + token);
			System.out.println("Response Code : " + responseCode);*/

			HttpEntity entity = response.getEntity();


			return EntityUtils.toString(entity, "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
