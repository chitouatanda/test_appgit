package com.xtaas.infra.zoominfo.service;


import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.OutboundNumberRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.StateCallConfigRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.web.dto.ProspectCallDTO;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DataSlice {
	
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DataSlice.class);


	@Autowired
	private ProspectCallLogService prospectCallLogService;
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private OutboundNumberRepository outboundNumberRepository;
	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;
	
	static Properties properties = new Properties();
	
	private static final String NON_CALLABLE = "_NON_CALLABLE";
	
	
	public String findAreaCode(String phone){
	    int startPoint = 0;
        int endPoint = 3;
        String removeSpclChar = phone.replace("-", "");
        int length = removeSpclChar.length();
        int validatePhoneLength = length-10;
        startPoint = startPoint + validatePhoneLength;
        endPoint = endPoint + validatePhoneLength;
        String areaCode = phone.substring(startPoint, endPoint);
        return areaCode;
	}
	
	
	public String getTimeZone(String phone) {
		List<OutboundNumber> outboundNumberList;
		int areaCode =  Integer.parseInt(findAreaCode(phone)); 
		outboundNumberList = outboundNumberRepository.findTimeZone(areaCode);
		if(outboundNumberList.size() == 0){
			return null;
		}
		
		//find a random number from the list
		OutboundNumber outboundNumber = outboundNumberList.get(0);
		
		return outboundNumber.getTimeZone(); 
	}
	
	public void getQualityBucket(List<String> campaigns) {
		try{
			List<ProspectCallDTO> prospectCallList = new ArrayList<ProspectCallDTO>(0);
			
		           for(String campaignID : campaigns){
				        	/*   List<ProspectCallLog> prospectCallLogList0 =
		        				  prospectCallLogRepository.findContactsTenByQueued(campaignID);
		        				  List<ProspectCallLog> prospectCallLogList1 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(1,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList2 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(2,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList3 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(3,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList4 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(4,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList5 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(5,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList6 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(6,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList7 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(7,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList8 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(8,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList9 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(9,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList10 =
		        				  prospectCallLogRepository.findContactsTenByRetryCount(10,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList786 =
		        				  prospectCallLogRepository.findContactsByMaxRetryCount(786,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList886 =
		        				  prospectCallLogRepository.findContactsByMaxRetryCount(886,campaignID);
		        				  List<ProspectCallLog> prospectCallLogList444444 =
		        				  prospectCallLogRepository.findContactsByMaxRetryCount(444444,campaignID);


		        				List<ProspectCallLog> pList = new ArrayList<ProspectCallLog>();//prospectCallLogRepository.findAllRecordByCampaignId(cid);

		        				pList.addAll(prospectCallLogList0);
		        				pList.addAll(prospectCallLogList1);
		        				pList.addAll(prospectCallLogList2);
		        				pList.addAll(prospectCallLogList3);
		        				pList.addAll(prospectCallLogList4);
		        				pList.addAll(prospectCallLogList5);
		        				pList.addAll(prospectCallLogList6);
		        				pList.addAll(prospectCallLogList7);
		        				pList.addAll(prospectCallLogList8);
		        				pList.addAll(prospectCallLogList9);
		        				pList.addAll(prospectCallLogList10);
		        				pList.addAll(prospectCallLogList786);
		        				pList.addAll(prospectCallLogList886);
		        				pList.addAll(prospectCallLogList444444);*/
		        				logger.info("Started");
					   List<ProspectCallLog> pList = prospectCallLogRepository.findCallableContacts(campaignID);
					   Campaign campaign = campaignService.getCampaign(campaignID);
					   tagSliceAndQualityScore(pList,campaign);
		      //moveDataToNonCallable(campaignID);
		        	 //  logger.info("Done with slicing for campaign");
		     }
		          // logger.info("Campaign Slicing is done");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void tagSliceAndQualityScore(List<ProspectCallLog> pList,Campaign campaign) {
		try {
				Map<String, List<ProspectCallLog>> pMap = new HashMap<String,List<ProspectCallLog>>();
				
		 	   for (ProspectCallLog p : pList) {
		 		   List<ProspectCallLog> prospectList = pMap.get(p.getProspectCall().getProspect().getPhone());
		 		   if(prospectList!=null && prospectList.size()>0){
		 			   prospectList.add(p);
		 		   }else{
		 			   prospectList = new ArrayList<ProspectCallLog>();
		 			   prospectList.add(p);
		 		   }
		 		   pMap.put(p.getProspectCall().getProspect().getPhone(), prospectList);
		 	   }
			List<ProspectCallLog> updateProspectList = new ArrayList<ProspectCallLog>();
		 	   for (Map.Entry<String, List<ProspectCallLog>> entry : pMap.entrySet()) {
		 		   List<ProspectCallLog> prospectList = entry.getValue(); 
		 		   boolean directFlag= true;
		 		   if(prospectList.size()>1)
		 			   directFlag = false;

		 		   for (ProspectCallLog p : prospectList) {
		 			   if(prospectList.size()==1 && !p.getProspectCall().getProspect().isDirectPhone()){
		 				   //DO nothing
		 			   }else{
		 				   p.getProspectCall().getProspect().setDirectPhone(directFlag);
		 			   }
		 			   //p.getProspectCall().getProspect().setDirectPhone(directFlag);
		 			   StringBuffer sb= new StringBuffer();
		 			   /*sb.append(p.getProspectCall().getProspect().getSource());
					sb.append("|");*/
				
		 			   String emp ="";
				     String mgmt="";
				     String direct="";
				     if(directFlag ){
				    	 direct="1";
				     }else{
				    	 direct="2";
				     }
				     
				     
				     String tz = getStateWeight(p.getProspectCall().getProspect().getTimeZone());
					sb.append(p.getStatus());
					sb.append("|");
					if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") instanceof Number){
					      Number n1 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount");
					      sb.append(getBoundry(n1,"min"));
					      sb.append("-");
					      emp = getEmployeeWeight(n1);
					     }else{
					    	 String x = "0";
					    	 if(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount") != null)
					    		 x = p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount").toString();
					    	 Double a = Double.parseDouble(x);
					    	 sb.append(getBoundry(a,"min"));
						      sb.append("-");
						      emp = getEmployeeWeight(a);
					     }
					     
					     if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount") instanceof Number){
					      Number n2 = (Number)p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount");
					      sb.append(getBoundry(n2,"max"));
					     }else{
					    	 
					    	 String x ="0";
					    	 if(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount")!=null)
					    		 x = p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
								if(x == null || x.isEmpty()){
									x = "0";
								} 
							 Double a = Double.parseDouble(x);
					    	 sb.append(getBoundry(a,"max"));
					    	/* int a = Integer.parseInt(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount").toString());
					    	 sb.append(getBoundry(a,"max"));*/
						    //  sb.append("-");
						    //  emp = getEmployeeWeight(a);
					     }
					/*sb.append(p.getProspectCall().getProspect().getCustomAttributeValue("minEmployeeCount"));
					sb.append("-");
					sb.append(p.getProspectCall().getProspect().getCustomAttributeValue("maxEmployeeCount"));*/
					sb.append("|");
					sb.append(getMgmtLevel(p.getProspectCall().getProspect().getManagementLevel()));
					mgmt = getMgmtLevelWeight(p.getProspectCall().getProspect().getManagementLevel());
					
					int qbSort=0;
				     if(!direct.isEmpty() && !tz.isEmpty() && !mgmt.isEmpty() && !emp.isEmpty() ){
				      String sortString = direct+tz+mgmt+emp;
				      Map<String, Integer> dataSourceMap = campaign.getSourceCallingPriority();
							if (dataSourceMap != null && dataSourceMap.size() > 0) {
								if (dataSourceMap.get(p.getProspectCall().getProspect().getSource()) != null) {
									int sourceWeight = dataSourceMap
											.get(p.getProspectCall().getProspect().getSource());
									if (sourceWeight > 0) {
		
									} else {
										int dsSizeMap = dataSourceMap.size();
										sourceWeight = dsSizeMap + 1;
									}
									String qbstr = String.valueOf(sourceWeight) + sortString;
									qbSort = Integer.parseInt(qbstr);
								}
							} else {
								qbSort = Integer.parseInt(sortString);
							}					     
						}
					
				   
				     String slice = "slice10";
					p.getProspectCall().setQualityBucket(sb.toString());
					p.getProspectCall().setQualityBucketSort(qbSort);
					p.getProspectCall().getProspect().setDirectPhone(directFlag);
					Long employee = new Long(emp);
					Long management = new Long(mgmt);
					if(directFlag && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")){
						slice = "slice1";
					}else if(directFlag /*&& employee<=8 && management<=3*/){
						slice = "slice1";
					}
					   p.setDataSlice(slice);
					   p.getProspectCall().setDataSlice(slice);
					/*if(directFlag && employee<=8 && management<=3){
						slice = "slice1";
					}else if(directFlag && employee<=10 && management<=3){
						slice = "slice2";
					}else if(directFlag && employee>10 && management<=3){
						slice = "slice3";
					}else if(directFlag && employee<=8 && management>=4){
						slice = "slice4";
					}else if(directFlag && employee<=10 && management>=4){
						slice = "slice5";
					}else if(directFlag && employee>10 && f>=4){
						slice = "slice6";
					}
				    */ 
					if(!slice.isEmpty() && p.getDataSlice()!=null && !p.getDataSlice().isEmpty() && !p.getDataSlice().equalsIgnoreCase("slice0")) {
						if(directFlag){
							
							
							if(management>=4 || employee >= 12){
								p.setDataSlice("slice2");
								p.getProspectCall().setDataSlice("slice2");
							}else{
								p.setDataSlice("slice1");
								p.getProspectCall().setDataSlice("slice1");
							}
							p = prospectCallLogRepository.save(p);
						}else{
							p.setDataSlice("slice10");
							p.getProspectCall().setDataSlice("slice10");
							p = prospectCallLogRepository.save(p);
						}
				    }
					
					if(!p.getProspectCall().getProspect().isDirectPhone() && slice.isEmpty() || p.getDataSlice()==null || p.getDataSlice().isEmpty() ) {
						//logger.info(p.getProspectCall().getProspectCallId()+"--->"+p.getDataSlice());
						if(p.getDataSlice()==null || !p.getDataSlice().isEmpty() ||  !p.getDataSlice().equalsIgnoreCase("slice10")){
							//p = prospectCallLogRepository.findOneById(p.getId());
						p.setDataSlice("slice10");
						p.getProspectCall().setDataSlice("slice10");
						//p.getProspectCall().setCallRetryCount(696969);
						//p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
						p = prospectCallLogRepository.save(p);
						}
				    }//p.getProspectCall().getProspect().setTimeZone(timezone);
					//p.getProspectCall().getProspect().setTimeZone(timezone);
					
					if(p.getProspectCall().getDataSlice()==null && p.getDataSlice()!=null){
						p.getProspectCall().setDataSlice(p.getDataSlice());
						p = prospectCallLogRepository.save(p);
					}
					if(p.getProspectCall().getDataSlice() == null){
						p.getProspectCall().setDataSlice("slice10");
						p = prospectCallLogRepository.save(p);
					}
				   if(p.getDataSlice() == null){
					   p.setDataSlice("slice10");
					   p = prospectCallLogRepository.save(p);
				   }
					//updateProspectList.add(p);
				}
			}
			//prospectCallLogRepository.bulkinsert(updateProspectList);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void moveDataToNonCallable(String campaignId){
		int counter = 0;
		List<ProspectCallLog> updateList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> pciList = prospectCallLogRepository.findNonCallableToMove(campaignId);
		if(pciList!=null && pciList.size()>0){
			for(ProspectCallLog pci : pciList){
				pci.getProspectCall().setCampaignId(campaignId+NON_CALLABLE);
				counter++;
				updateList.add(pci);
				if(counter>=999){
					prospectCallLogRepository.saveAll(updateList);
					counter = 0;
					updateList.clear();
				}
			}
			if(updateList!=null && updateList.size()>0)
				prospectCallLogRepository.saveAll(updateList);
			if(pciList!=null && pciList.size()>0)
				logger.debug("Total data moved from campaign" +campaignId+ "to NON_CALLABLE : "+pciList.size() );
			else
				logger.debug("There is NO DATA MOVE from campaign" +campaignId+ "to NON_CALLABLE" );

		}
	}
	
	 public String getBoundry(Number n , String minMax){
		   if(minMax.equalsIgnoreCase("min")){
		    if(n.intValue()<5){
		     return "1";
		    }
		    if(n.intValue()<10){
		     return "5";
		    }
		    if(n.intValue()<20){
		     return "10";
		    }
		    if(n.intValue()<50){
		     return "20";
		    }
		    if(n.intValue()<100){
		     return "50";
		    }
		    if(n.intValue()<250){
		     return "100";
		    }
		    if(n.intValue()<500){
		     return "250";
		    }
		    if(n.intValue()<1000){
		     return "500";
		    }
		    if(n.intValue()<5000){
		     return "1000";
		    }
		    if(n.intValue()<10000){
		     return "5000";
		    }
		    
		    if(n.intValue()<11000){
		     return "10000";
		    }
		    
		   }else  if(minMax.equalsIgnoreCase("max")){
		    if(n.intValue()<=5){
		     return "5";
		    }
		    if(n.intValue()<=10){
		     return "10";
		    }
		    if(n.intValue()<=20){
		     return "20";
		    }
		    if(n.intValue()<=50){
		     return "50";
		    }
		    if(n.intValue()<=100){
		     return "100";
		    }
		    if(n.intValue()<=250){
		     return "250";
		    }
		    if(n.intValue()<=500){
		     return "500";
		    }
		    if(n.intValue()<=1000){
		     return "1000";
		    }
		    if(n.intValue()<=5000){
		     return "5000";
		    }
		    if(n.intValue()<=10000){
		     return "10000";
		    }
		    
		    if(n.intValue()<=100000){
		     return "11000";
		    }
		    
		   }
		   
		   return "1";
		  /* 1-5
		   5-10
		   10-20
		   20-50
		   50-100
		   100-250
		   250-500
		   500-1000
		   1000-5000
		   5000-10000
		   10000-11000*/
		  }
	 
	 public String getMgmtLevel(String managementLevel){
		 if(managementLevel == null)
			 return "VP-Level";
		   if(managementLevel.equalsIgnoreCase("VP-Level")){
		    return "VP-Level";
		   }else if(managementLevel.equalsIgnoreCase("C-Level")){
		    return "C-Level";
		   }else if(managementLevel.toLowerCase().contains("Director".toLowerCase())){
		    return "DIRECTOR";
		   }else if(managementLevel.toLowerCase().contains("MANAGER".toLowerCase())){
		    return "MANAGER";
		   }else {
		   
		    return "Non-Manager";
		   }
		  
		  }
	 
	 public String getStateWeight(String timeZone){
		 
		  		String est = "US/Eastern";
		        String cst = "US/Central";
		        String mst = "US/Mountain";
		        String pst =  "US/Pacific";
		  
		 
		  
		  if(timeZone!=null && timeZone.equalsIgnoreCase(pst)){
		   return "1";
		  }
		  if(timeZone!=null && timeZone.equalsIgnoreCase(mst)){
		   return "2";
		  }
		  if(timeZone!=null && timeZone.equalsIgnoreCase(cst)){
		   return "3";
		  }
		  if(timeZone!=null && timeZone.equalsIgnoreCase(est)){
		   return "4";
		  }
		  return "5";
		  }
	 
	 public String getEmployeeWeight(Number n) {
		   
		 	if(n.intValue()==0){
		     return "12";
		    }
		    if(n.intValue()<5){
		     return "01";
		    }
		    if(n.intValue()<10){
		     return "02";
		    }
		    if(n.intValue()<20){
		     return "03";
		    }
		    if(n.intValue()<50){
		     return "04";
		    }
		    if(n.intValue()<100){
		     return "05";
		    }
		    if(n.intValue()<250){
		     return "06";
		    }
		    if(n.intValue()<500){
		     return "07";
		    }
		    if(n.intValue()<1000){
		     return "08";
		    }
		    if(n.intValue()<5000){
		     return "09";
		    }
		    if(n.intValue()<10000){
		     return "10";
		    }
		    
		    if(n.intValue()<11000){
		     return "11";
		    }
		    
		    return "12";
		  }
		public String getMgmtLevelWeight(String managementLevel){
			if(managementLevel == null)
				return "6";
		   if(managementLevel.equalsIgnoreCase("VP-Level")){
		    return "4";
		   }else if(managementLevel.equalsIgnoreCase("Board Members")){
			   return "6";
		   }else if(managementLevel.equalsIgnoreCase("C-Level")){
		    return "5";
		   }else if(managementLevel.toLowerCase().contains("Director".toLowerCase())){
		    return "3";
		   }else if(managementLevel.toLowerCase().contains("MANAGER".toLowerCase())){
		    return "2";
		   }else {
		    return "1";
		   }
		   
		  // return "";
		  
		  }

}

