package com.xtaas.infra.zoominfo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallLogRepository;

@Service
public class TenCompanies {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TenCompanies.class);
	private int allowDuplicates = 4;//This values should be 4 for multiprospect campaigns
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	public Long getTenProspectsForACompany(String cid){
		

		   int repcountner = 0;
		  List<ProspectCallLog> repProspectCallLogList = prospectCallLogRepository.findReplinishData(cid);
		  for(ProspectCallLog rpc : repProspectCallLogList){
			 rpc.getProspectCall().setCallRetryCount(1);
				rpc.setStatus(ProspectCallStatus.CALL_BUSY);
				rpc.getProspectCall().setSubStatus("BUSY");
				rpc.getProspectCall().setDispositionStatus("DIALERCODE");
				//System.out.println("state code is null");
				prospectCallLogRepository.save(rpc);
				repcountner++;
		  }
		  System.out.println("total data replenish before tencompanies : " +repcountner);
		   
	    List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsTenByQueued(cid);
	    List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsTenByRetryCount(1,cid);
	    List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsTenByRetryCount(2,cid);
	    List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsTenByRetryCount(3,cid);
	    List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsTenByRetryCount(4,cid);
	    List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsTenByRetryCount(5,cid);
	    List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsTenByRetryCount(6,cid);
	    List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsTenByRetryCount(7,cid);
	    List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsTenByRetryCount(8,cid);
	    List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsTenByRetryCount(9,cid);
	    List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsTenByRetryCount(10,cid);
	    
	    List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
	    prospectCallLogList.addAll(prospectCallLogList0);
	    prospectCallLogList.addAll(prospectCallLogList1);
	    prospectCallLogList.addAll(prospectCallLogList2);
	    prospectCallLogList.addAll(prospectCallLogList3);
	    prospectCallLogList.addAll(prospectCallLogList4);
	    prospectCallLogList.addAll(prospectCallLogList5);
	    prospectCallLogList.addAll(prospectCallLogList6);
	    prospectCallLogList.addAll(prospectCallLogList7);
	    prospectCallLogList.addAll(prospectCallLogList8);
	    prospectCallLogList.addAll(prospectCallLogList9);
	    prospectCallLogList.addAll(prospectCallLogList10);
	    Map<String, List<ProspectCallLog>> prospectCallMap = new java.util.HashMap<String, List<ProspectCallLog>>();
	    //System.out.println(prospectCallLogList.size());
	    int updatecounter = 0;
	    for(ProspectCallLog pc :prospectCallLogList){
	    	//for(String comp : companies){
    		//if(pc.getProspectCall().getProspect().getCompany().equalsIgnoreCase(comp)){
	    		String key = pc.getProspectCall().getProspect().getPhone();
	    		if(key.equalsIgnoreCase("979-845-7800"))
	    			System.out.println("Here");
	    			List<ProspectCallLog> prospectl = prospectCallMap.get(key);
	    			if(prospectl!=null && prospectl.size()>0){
	    				ProspectCallLog ep = prospectl.get(0);
	    				if(ep.getProspectCall().getProspect().getStateCode()!=null && !ep.getProspectCall().getProspect().getStateCode().isEmpty()){
	    					
	    					if(pc.getProspectCall().getProspect().getStateCode()!=null 
	    							&& !pc.getProspectCall().getProspect().getStateCode().isEmpty()
	    							&& ep.getProspectCall().getProspect().getStateCode()
	    							.equalsIgnoreCase(pc.getProspectCall().getProspect().getStateCode())){
	    						prospectl.add(pc);
	    					}else{
	    						if(pc.getProspectCall().getProspect().getStateCode()!=null && 
			    						!pc.getProspectCall().getProspect().getStateCode().isEmpty()){
				    				pc.getProspectCall().getProspect().setStateCode(ep.getProspectCall().getProspect().getStateCode());
		    						prospectl.add(pc);
			    				}else{
			    					pc.getProspectCall().setCallRetryCount(5005);
			    					pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			    					//System.out.println(updatecounter++);
			    					prospectCallLogRepository.save(pc);
			    					updatecounter++;

			    				}
	    						
	    					}
	    				}else{
	    					ep.getProspectCall().setCallRetryCount(5005);
	    					ep.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
	    					//System.out.println(updatecounter++);
	    					prospectCallLogRepository.save(ep);
	    					updatecounter++;

	    				}


	    				prospectCallMap.put(key, prospectl);
	    			}else{
	    				if(pc.getProspectCall().getProspect().getStateCode()!=null && 
	    						!pc.getProspectCall().getProspect().getStateCode().isEmpty()){
	    					prospectl = new ArrayList<ProspectCallLog>();
		    				prospectl.add(pc);
		    				prospectCallMap.put(key, prospectl);
	    				}else{
	    					pc.getProspectCall().setCallRetryCount(5005);
	    					pc.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
	    					//System.out.println(updatecounter++);
	    					prospectCallLogRepository.save(pc);
	    				}
	    				
	    			}
    	//	}
    //	}
    }
	    List<ProspectCallLog> toRemove =  new ArrayList<ProspectCallLog>();
	    for (Map.Entry<String, List<ProspectCallLog>> entry : prospectCallMap.entrySet()) {
	    	List<ProspectCallLog> pl = entry.getValue();
	    	
	    	
	    	if(entry.getValue().size()>allowDuplicates){
	    		int counter =  1;
	    		for(ProspectCallLog p : pl){
	    			if(counter > allowDuplicates){
	    				//System.out.println(entry.getValue().size());
	    				toRemove.add(p);
	    			}
	    			counter++;
	    		}
	    		
	    	}
	    }
	    List<ProspectCallLog> upDateList = new ArrayList<ProspectCallLog>();
	    for(ProspectCallLog removeP : toRemove){
	    	removeP.getProspectCall().setCallRetryCount(222222);
	    	removeP.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
	    	upDateList.add(removeP);
	    	updatecounter++;
	    }
	    
	    logger.debug("Please wait we are updating Record, It will take some time ......");
        prospectCallLogRepository.saveAll(upDateList);
        logger.debug("Total record  updated - "+updatecounter);
        //totalCount = prospectCallLogList.size() + totalCount;
        return new Long(updatecounter);

		 
/*		    List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsTenByQueued(cid);
		    List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsTenByRetryCount(1,cid);
		    List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsTenByRetryCount(2,cid);
		    List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsTenByRetryCount(3,cid);
		    List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsTenByRetryCount(4,cid);
		    List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsTenByRetryCount(5,cid);
		    List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsTenByRetryCount(6,cid);
		    
		    List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		    prospectCallLogList.addAll(prospectCallLogList0);
		    prospectCallLogList.addAll(prospectCallLogList1);
		    prospectCallLogList.addAll(prospectCallLogList2);
		    prospectCallLogList.addAll(prospectCallLogList3);
		    prospectCallLogList.addAll(prospectCallLogList4);
		    prospectCallLogList.addAll(prospectCallLogList5);
		    prospectCallLogList.addAll(prospectCallLogList6);
		    Map<String, List<ProspectCallLog>> prospectCallMap = new java.util.HashMap<String, List<ProspectCallLog>>();
		   // logger.info(prospectCallLogList.size());
		    int counterupdate = 0;
		    for(ProspectCallLog pc :prospectCallLogList){
		    	//for(String comp : companies){
		    		//if(pc.getProspectCall().getProspect().getCompany().equalsIgnoreCase(comp)){
			    		String key = pc.getProspectCall().getProspect().getPhone();
			    		
			    			List<ProspectCallLog> prospectl = prospectCallMap.get(key);
			    			if(prospectl!=null && prospectl.size()>0){
			    				ProspectCallLog ep = prospectl.get(0);
			    				if(ep.getProspectCall().getProspect().getStateCode()!=null){
				    				if(ep.getProspectCall().getProspect().getStateCode().equalsIgnoreCase(pc.getProspectCall().getProspect().getStateCode())){
				    					prospectl.add(pc);
				    				}else{
				    					pc.getProspectCall().getProspect().setStateCode(ep.getProspectCall().getProspect().getStateCode());
				    					prospectl.add(pc);
				    				}
			    				}else{
			    					ep.getProspectCall().setCallRetryCount(222222);
			    			    	ep.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			    			    	//System.out.println("state code is null");
			    			    	prospectCallLogRepository.save(ep);
			    			    	counterupdate++;
			    					
			    				}
			    				
			    				
			    				prospectCallMap.put(key, prospectl);
			    			}else{
			    				prospectl = new ArrayList<ProspectCallLog>();
			    				prospectl.add(pc);
			    				prospectCallMap.put(key, prospectl);
			    			}
		    	//	}
		    //	}
		    }
		    List<ProspectCallLog> toRemove =  new ArrayList<ProspectCallLog>();
		    for (Map.Entry<String, List<ProspectCallLog>> entry : prospectCallMap.entrySet()) {
		    	List<ProspectCallLog> pl = entry.getValue();
		    	
		    	
		    	if(entry.getValue().size()>4){
		    		int counter =  1;
		    		for(ProspectCallLog p : pl){
		    			if(counter > 5){
		    				//System.out.println(entry.getValue().size());
		    				toRemove.add(p);
		    			}
		    			counter++;
		    		}
		    		
		    	}
		    }
		    List<ProspectCallLog> upDateList = new ArrayList<ProspectCallLog>();
		    for(ProspectCallLog removeP : toRemove){
		    	removeP.getProspectCall().setCallRetryCount(222222);
		    	removeP.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
		    	upDateList.add(removeP);
		    	counterupdate++;
		    }
		    
		    logger.info("Please wait we are updating Record, It will take some time ......");
         prospectCallLogRepository.save(upDateList);
         logger.info("Total record  updated - "+counterupdate);
         //totalCount = prospectCallLogList.size() + totalCount;
         return new Long(upDateList.size());*/
	   }
	

}
