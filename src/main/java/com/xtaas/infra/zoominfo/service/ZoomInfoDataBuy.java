package com.xtaas.infra.zoominfo.service;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
//import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
//import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
//import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.util.concurrent.RateLimiter;
import com.xtaas.application.service.InternalDNCListServiceImpl;
import com.xtaas.db.entity.*;
import com.xtaas.db.repository.*;
import com.xtaas.service.*;
import com.xtaas.web.dto.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.usermodel.Cell;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

//import com.mongodb.DBCollection;
//import com.mongodb.DBObject;
//import com.mongodb.WriteResult;
//import com.mongodb.util.JSON;
import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
//import com.xtaas.db.entity.SuppressionList;
//import com.xtaas.db.repository.AbmListDetailRepositoryImpl;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
//import com.xtaas.db.repository.DataBuyQueueRepository;
//import com.xtaas.db.repository.ListProviderUsageRepository;
//import com.xtaas.db.repository.ProspectAttributeRepository;
//import com.xtaas.db.repository.SuppressionListRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.entity.EntityMapping;
import com.xtaas.domain.entity.ZoomToken;
//import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.valueobject.AttributeMapping;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.domain.valueobject.ValueMapping;
//import com.xtaas.infra.contactlistprovider.service.ContactListProviderFactory;
import com.xtaas.infra.contactlistprovider.valueobject.ContactListProviderType;
import com.xtaas.infra.everstring.service.DiscoverAPIService;
import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanyRecord;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CompanyAddress;
//import com.xtaas.infra.service.ContactListProvider;
//import com.xtaas.infra.zoominfo.companysearch.valueobject.CompanyRecord;
import com.xtaas.infra.zoominfo.personsearch.valueobject.CurrentEmployment;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonDetailResponse;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.utils.EditDistance;
//import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
//import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.utils.XtaasUtils;
import com.xtaas.valueobjects.KeyValuePair;
//import com.xtaas.utils.XtaasValidationUtils;
//import com.xtaas.web.dto.ContactSearchResult;

import me.xdrop.fuzzywuzzy.FuzzySearch;

@Service
public class ZoomInfoDataBuy {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ZoomInfoDataBuy.class);


	private String partnerKey = "Xtaascorp.partner";
	// private String password = "inKnEjJu0";
	public static final String ZOOMINFO_PURCHASE_RESPONSE_COLLECTION = "zoominfopurchaseresponse";
	public List<Contact> listContact = new ArrayList<Contact>();
	public List<String> personIdList = new ArrayList<String>();
	public Long rowCount = new Long(0);
	public Long buyRowCount = new Long(0);

	private static final String NON_CALLABLE = "_NON_CALLABLE";
	// private static final String PIVOT_REMOVED = "_PIVOT_REMOVED";

	private static final String GET_COMPANY_ID = "GET_COMPANY_ID";

	private static final String NOT_FOUND = "NOT_FOUND";

	private static final int MAX_PROSPECT_VERSION = 3;

	private static final int MQL_SUCCESS_RATE = 50;

	private static final int HQL_SUCCESS_RATE = 100;

	public int maxRecordsIndirect = 12;

	public int maxDirectNumbers = 40;

	@Autowired
	private CampaignContactRepository campaignContactRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private StateCallConfigRepository stateCallConfigRepository;

	String tokenKey = "";

	Date cachedDate;

	//public int maxJobs = 50;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private GlobalContactService globalContactService;

	@Autowired
	private AbmListDetailRepository abmListDetailRepository;

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;

	@Autowired
	private EntityMappingRepository entityMappingRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private ReviewDataBoughtRepository reviewDataBoughtRepository;

	@Autowired
	private DataSufficiencyResultRepository dataSufficiencyResultRepository;

	@Autowired
	private ProspectCallInteractionRepository prospectCallInteractionRepository;

	@Autowired
	private AbmListServiceImpl abmListServiceImpl;

	/*@Autowired
	private RemoveSuccessDuplicates removeSuccessDuplicates;*/
	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	Contact contact;
	// @Autowired
	// private ZoomBuySearchDTO zoomBuySearchDTO;

	@Autowired
	private SuppressionListService suppressionListService;

	@Autowired
	private SuppressionListNewRepository suppressionListNewRepository;
	
	@Autowired
	private DiscoverAPIService discoverAPIService;
	
	/*@Autowired
	private DataSlice dataSlice;*/
	@Autowired
	private PhoneDirectAndIndiretServiceImpl phoneDirectAndIndiretServiceImpl;

	@Autowired
	private PlivoOutboundNumberService numberService;

	@Autowired
	private GlobalContactRepository globalContactRepository;

	@Autowired
	private PhoneDncService phoneDncService;

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private DataBuyRateLimiterService dataBuyRateLimiterService;

	public int getMaximumPageSize() {
		return 25;
	}

	int retry = 0;

	public String getZoomOAuth() {
		String oAuth = "";

		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		if (cachedDate == null) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		// Date dateold = zt.getUpdatedDate();

		Date currDate = new Date();

		long duration = currDate.getTime() - cachedDate.getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
			zt = zoomTokenList.get(0);
			tokenKey = zt.getOauthToken();
			cachedDate = zt.getCreatedDate();
		}
		return tokenKey;
	}
	// EST=Virginia,Vermont,West
	// Virginia,Kentucky,Massachusetts,Maryland,Maine,Michigan,North Carolina,New
	// Hampshire,New Jersey,New York,Ohio,Pennsylvania,Rhode Island,South
	// Carolina,Connecticut,District of Columbia,Delaware,Florida,Georgia,Iowa
	// CENTRAL=Wisconsin,Illinois,Indiana,Kansas,Louisiana,Minnesota,Missouri,Mississippi,North
	// Dakota,Nebraska,Oklahoma,South Dakota,Tennessee,Texas,Alabama,Arkansas
	// MOUNTAIN=Utah,Wyoming,Montana,New Mexico,Colorado,Alaska,Hawaii
	// PST=Idaho,Washington,Nevada,Oregon,Arizona,California

	private List<String> getPST() {
		List<String> pstList = new ArrayList<String>();
		pstList.add("Idaho");
		pstList.add("Washington");
		pstList.add("Nevada");
		pstList.add("Oregon");
		pstList.add("Arizona");
		pstList.add("California");
		return pstList;
	}

	private List<String> getMST() {
		List<String> mstList = new ArrayList<String>();
		mstList.add("Utah");
		mstList.add("Wyoming");
		mstList.add("Montana");
		mstList.add("New Mexico");
		mstList.add("Colorado");
		mstList.add("Alaska");
		mstList.add("Hawaii");
		return mstList;
	}

	private List<String> getCST() {
		List<String> cstList = new ArrayList<String>();
		cstList.add("Wisconsin");
		cstList.add("Illinois");
		cstList.add("Indiana");
		cstList.add("Kansas");
		cstList.add("Louisiana");
		cstList.add("Minnesota");
		cstList.add("Missouri");
		cstList.add("Mississippi");
		cstList.add("North Dakota");
		cstList.add("Nebraska");
		cstList.add("Oklahoma");
		cstList.add("South Dakota");
		cstList.add("Tennessee");
		cstList.add("Texas");
		cstList.add("Alabama");
		cstList.add("Arkansas");
		return cstList;
	}

	private List<String> getEST() {
		List<String> estList = new ArrayList<String>();
		estList.add("Virginia");
		estList.add("West Virginia");
		estList.add("Massachusetts");
		estList.add("Maryland");
		estList.add("Maine");
		estList.add("Michigan");
		estList.add("North Carolina");
		estList.add("New Hampshire");
		estList.add("New Jersey");
		estList.add("New York");
		estList.add("Ohio");
		estList.add("Pennsylvania");
		estList.add("Rhode Island");
		estList.add("South Carolina");
		estList.add("Connecticut");
		estList.add("District of Columbia");
		estList.add("Delaware");
		estList.add("Florida");
		estList.add("Georgia");
		estList.add("Iowa");
		return estList;
	}

	private List<String> getUKStates() {
		List<String> stateList = new ArrayList<String>();
		stateList.add("England");
		stateList.add("Greater London");
		stateList.add("Renfrewshire");
		stateList.add("Glasgow");
		stateList.add("Berkshire");
		stateList.add("Bedfordshire");
		stateList.add("Berkshire");
		stateList.add("Bristol");
		stateList.add("Buckinghamshire");
		stateList.add("Cambridgeshire");
		stateList.add("Cheshire");
		stateList.add("Cornwall");
		stateList.add("County Durham");
		stateList.add("Cumberland");
		stateList.add("Derbyshire");
		stateList.add("Devon");
		stateList.add("Dorset");
		stateList.add("Essex");
		stateList.add("Gloucestershire");
		stateList.add("Hampshire");
		stateList.add("Herefordshire");
		stateList.add("Hertfordshire");
		stateList.add("Huntingdonshire");
		stateList.add("Kent");
		stateList.add("Lancashire");
		stateList.add("Leicestershire");
		stateList.add("Lincolnshire");
		stateList.add("Middlesex");
		stateList.add("Norfolk");
		stateList.add("Northamptonshire");
		stateList.add("Northumberland");
		stateList.add("Nottinghamshire");
		stateList.add("Oxfordshire");
		stateList.add("Rutland");
		stateList.add("Shropshire");
		stateList.add("Somerset");
		stateList.add("Staffordshire");
		stateList.add("Suffolk");
		stateList.add("Surrey");
		stateList.add("Sussex");
		stateList.add("Warwickshire");
		stateList.add("Wiltshire");
		stateList.add("Worcestershire");
		stateList.add("Yorkshire");

		return stateList;
	}

	private List<String> getCanadaStates() {
		List<String> stateList = new ArrayList<String>();
		stateList.add("Alberta");
		stateList.add("Northwest Territories");
		stateList.add("British Columbia");
		stateList.add("Nunavut");
		stateList.add("Ontario");
		stateList.add("Quebec");
		stateList.add("Manitoba");
		stateList.add("Saskatchewan");
		stateList.add("Yukon");
		stateList.add("New Brunswick");
		stateList.add("Nova Scotia");
		stateList.add("Prince Edward Island");
		stateList.add("Newfoundland and Labrador");

		return stateList;
	}

	public List<DataBuyQueue> getCampaignsFromQueue() {

		List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>();
		List<String> dataBuyStatusList = new ArrayList<String>();
		try {
			dataBuyStatusList.add(DataBuyQueueStatus.QUEUED.toString());
			dataBuyStatusList.add(DataBuyQueueStatus.INPROCESS.toString());
			// dataBuyStatusList.add(DataBuyQueueStatus.ERROR);
			Pageable pageable = PageRequest.of(0, 1, Direction.ASC, "jobRequestTime");
			// dataBuyList =
			// dataBuyQueueRepository.findByStatus(dataBuyStatusList,pageable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return dataBuyList;
		return null;
	}

	/*
	 * public String PurchaseRecords(ZoomBuySearchDTO zoomBuySearchDTO){
	 * if(zoomBuySearchDTO.isSufficiency()){ getDataSufficiency(zoomBuySearchDTO);
	 * return "Success"; }
	 * 
	 * 
	 * Campaign campaign =
	 * campaignService.getCampaign(zoomBuySearchDTO.getCampaignId());
	 * 
	 * if(campaign==null ){ return
	 * "No Campaign Exists with campaignId : "+zoomBuySearchDTO.getCampaignId().
	 * toString(); }
	 * 
	 * String statusMessage=
	 * "Buy Process will be Initiated for Campaign : "+campaign.getName();
	 * List<DataBuyQueue> dataBuyQueueList = getCampaignsFromQueue();
	 * if(dataBuyQueueList!=null && dataBuyQueueList.size()>0){ for(DataBuyQueue dbq
	 * : dataBuyQueueList){
	 * if(dbq.getCampaignId().equalsIgnoreCase(zoomBuySearchDTO.getCampaignId())){
	 * 
	 * if(dbq.getStatus().equals(DataBuyQueueStatus.QUEUED.toString()) &&
	 * dbq.isBuyProspect()){ statusMessage =
	 * "Job for "+campaign.getName()+" with purchase Count : "+
	 * dbq.getStatus().toString()+" is already in Purchase Queue."; }else
	 * if(dbq.getStatus().equals(DataBuyQueueStatus.INPROCESS.toString()) &&
	 * dbq.isBuyProspect()){ statusMessage =
	 * "Job for "+campaign.getName()+" with purchase Count : "+
	 * dbq.getStatus().toString()+" is already in Process."; }else
	 * if(dbq.getStatus().equals(DataBuyQueueStatus.QUEUED.toString()) &&
	 * dbq.isSearchCompany()){ statusMessage = "Job for "+campaign.getName()
	 * +"  is in Queue for searching companies, Please try buying after some time.";
	 * }else if(dbq.getStatus().equals(DataBuyQueueStatus.INPROCESS.toString()) &&
	 * dbq.isSearchCompany()){ statusMessage = "Job for "+campaign.getName()
	 * +"  is in Process for searching companies, Please try buying after some time."
	 * ; } List<String> toemails = new ArrayList<String>();
	 * toemails.add("data@xtaascorp.com"); if(dbq.getRequestorId()!=null &&
	 * !dbq.getRequestorId().isEmpty()) { Login l =
	 * abmListServiceImpl.getLogin(dbq.getRequestorId()); if(l.getEmail()!=null &&
	 * !l.getEmail().isEmpty()) toemails.add(l.getEmail()); }
	 * 
	 * XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com",
	 * campaign.getName()+" Data purchase job status",statusMessage); return
	 * statusMessage; } } } int counter = 0; if(dataBuyQueueList!=null &&
	 * dataBuyQueueList.size()>0){ for(DataBuyQueue dbq : dataBuyQueueList){
	 * if(dbq.getStatus().equals(DataBuyQueueStatus.INPROCESS.toString())){ counter
	 * = counter+1; } } } DataBuyQueue dbq = new DataBuyQueue();
	 * dbq.setCampaignId(campaign.getId()); dbq.setCampaignName(campaign.getName());
	 * dbq.setStatus(DataBuyQueueStatus.QUEUED.toString());
	 * dbq.setBuyProspect(true); dbq.setJobRequestTime(new Date());
	 * dbq.setRequestCount(zoomBuySearchDTO.getTotalCount());
	 * dbq.setOrganizationId(zoomBuySearchDTO.getOrganizationId());
	 * //****************************************************************
	 * List<Expression> expression = new ArrayList<Expression>();
	 * List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
	 * for(HealthChecksCriteriaDTO dto : criteria) { Expression exp = new
	 * Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false,
	 * "ABC", dto.getPickList(), ""); //Expression exp = new Expression(attribute,
	 * operator, value, agentValidationRequired, questionSkin, pickList,
	 * sequenceNumber); expression.add(exp); }
	 * 
	 * dbq.setCampaignCriteriaDTO(expression); dataBuyQueueRepository.save(dbq);
	 * if(counter<maxJobs){ DataBuyQueue dbqp =
	 * dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
	 * DataBuyQueueStatus.QUEUED.toString(),dbq.getJobRequestTime());
	 * dbqp.setStatus(DataBuyQueueStatus.INPROCESS.toString());
	 * dbqp.setJobStartTime(new Date()); dataBuyQueueRepository.save(dbqp);
	 * 
	 * statusMessage = PurchaseRecordsFromQueue(dbqp);
	 * 
	 * 
	 * }else{ statusMessage = "Job for "+dbq.getCampaignId()
	 * +" is running, will pick up the next job from queue after this."; return
	 * statusMessage; }
	 * 
	 * 
	 * return statusMessage; }
	 */

	public String companySearchFromQueue(DataBuyQueue dbq) {
		// initiateBeans();
		int counter = 0;
		String campaignId = dbq.getCampaignId();

		String statusMessage = "";
		try {
			int count = Integer.parseInt(abmListServiceImpl.getABMListCountOfCampaign(dbq.getCampaignId()));
			if (count > 0) {
				int batchsize = 0;
				if (count > 0) {
					batchsize = count / 1000;
					long modSize = count % 1000;
					if (modSize > 0) {
						batchsize = batchsize + 1;
					}
				}
				for (int i = 0; i < batchsize; i++) {
					Pageable pageable = PageRequest.of(i, 1000);
					List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaignId, pageable);
					for (AbmListNew abmNew : abmList) {
						String companyName = abmNew.getCompanyName();
						String companyId = abmNew.getCompanyId();
						logger.debug("searching company :" + companyName + "\n");
						if (companyId == null || companyId.isEmpty() || companyId.equalsIgnoreCase(GET_COMPANY_ID)) {
							ZoomInfoResponse companyResponse = getCompanyResponse(companyName, null, campaignId);
							if (companyResponse != null && companyResponse.getCompanySearchResponse().getMaxResults() > 0) {
								List<CompanyRecord> companyRecordsList = companyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord();
								for(CompanyRecord cr : companyRecordsList) {
									if(abmNew.getDomain()==null || abmNew.getDomain().isEmpty()) {
										abmNew.setResponseList(getWeightByCompanyName("ZOOMINFO",companyName,cr.getCompanyName(),cr.getCompanyWebsite()));
									}
								}
								String zoomCompanyId = companyResponse.getCompanySearchResponse().getCompanySearchResults()
										.getCompanyRecord().get(0).getCompanyId();
								abmNew.setCompanyName(companyName);
								abmNew.setCompanyId(zoomCompanyId);
								// companiesWithIds.add(abmNew);
								abmListServiceImpl.saveABMListNew(abmNew);
								counter++;
							} else {
								/*List<AbmWeightScore> responseList = new ArrayList<AbmWeightScore>();
								EverStringRealTimeResponseDTO esrtResponseDTO = discoverAPIService.getCompanyDomainFromEverString(companyName);
								if(esrtResponseDTO != null && esrtResponseDTO.getData() != null) {
									List<EverStringRealTimeCompanyDTO> esrtList = esrtResponseDTO.getData();
									for(EverStringRealTimeCompanyDTO cr : esrtList) {
										if(abmNew.getDomain()==null || abmNew.getDomain().isEmpty()) {
											abmNew.setResponseList(getWeightByCompanyName("EVERSTRING",companyName,cr.getES_Name(),cr.getES_PrimaryWebsite()));
										}
									}
								}*/
								
								//TODO call everstring and leadiq
								abmNew.setCompanyName(companyName);
								abmNew.setCompanyId(NOT_FOUND);
								// companiesWithIds.add(abmNew);
								abmListServiceImpl.saveABMListNew(abmNew);
							}
							// logger.debug("HERE");
						}
					}
				}
			}

			// List<KeyValuePair<String, String>> companies = abmList.getCompanyList();
			// List<AbmListNew> companiesWithIds = new ArrayList<AbmListNew>();
			// List<String> countries = abmList.getCountry();
			/*
			 * StringBuffer countrysb = new StringBuffer(); boolean first = true;
			 * /*for(String c : countries){ if(!first) countrysb.append(",");
			 * countrysb.append(c); first = false; }
			 */

			// abmList.setCompanyList(companiesWithIds);
			logger.debug("ABM BUY COMPLETED");

			/*
			 * List<String> toemails = new ArrayList<String>();
			 * toemails.add("data@xtaascorp.com"); if(dbq.getRequestorId()!=null &&
			 * !dbq.getRequestorId().isEmpty()) { Login l =
			 * abmListServiceImpl.getLogin(dbq.getRequestorId()); if(l.getEmail()!=null &&
			 * !l.getEmail().isEmpty()) toemails.add(l.getEmail()); } Campaign campaign =
			 * campaignService.getCampaign(dbq.getCampaignId());
			 * XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com",
			 * campaign.getName()+" Companies search completed.", "Found "+ counter+
			 * " out of "+count+ " Companies.  You can now initiate Buy.");
			 */

			dbq.setBuyProspect(true);
			dbq.setSearchCompany(false);
			// dbq.setStatus(DataBuyQueueStatus.COMPLETED.toString());
			// dbq.setJobEndTime(new Date());
			dataBuyQueueRepository.save(dbq);
		} catch (Exception e) {
			// abmList.setCompanyList(companiesWithIds);
			// abmListServiceImpl.saveABMList(companiesWithIds);
			e.printStackTrace();

			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignId,
					DataBuyQueueStatus.INPROCESS.toString(), dbq.getJobRequestTime());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString() + "failed when searching for company name ";
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
		}

		return statusMessage;
	}
	
	public List<AbmWeightScore> getWeightByCompanyName(String source, String searchCompanyName, String responseCompanyName, String responseWebsite) {
		List<AbmWeightScore> responseList = new ArrayList<AbmWeightScore>();
		try {
			int ratioScore = FuzzySearch.ratio(searchCompanyName,responseCompanyName);
	        int weightedScore = FuzzySearch.weightedRatio(searchCompanyName,responseCompanyName);
	        int ealog = EditDistance.dist(searchCompanyName.toCharArray(),responseCompanyName.toCharArray());
	        AbmWeightScore weightage = new AbmWeightScore(source, responseWebsite, responseCompanyName, ratioScore, weightedScore, ealog);
	        responseList.add(weightage);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return responseList;
	}

	

	public String PurchaseRecordsFromQueue(ZoomBuySearchDTO zoomBuySearchDTO) {
		DataBuyQueue dbq = zoomBuySearchDTO.getDataBuyQueue();

		buyRowCount = new Long(0);
		List<String> runningCampaignIds = getActiveCampaignsByOrganization(dbq.getOrganizationId());// getActiveCampaigns();
		StringBuffer purchaseString = new StringBuffer();
		Campaign campaign = campaignService.getCampaign(zoomBuySearchDTO.getCampaignId());
		purchaseString.append("Total Number Of Records Purchased : ");

		// QualificationCriteria cc = zoomBuySearchDTO.getQualificationCriteria();
		// By default the buy will starts with contratcrequirement 5
		// zoomBuySearchDTO.setContactRequirement("5");
		List<Expression> expression = new ArrayList<Expression>();
		if (zoomBuySearchDTO.getCriteria() == null) {
			if (zoomBuySearchDTO.getDataBuyQueue() != null) {
				expression = zoomBuySearchDTO.getDataBuyQueue().getCampaignCriteriaDTO();
			}
		} else {
			List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
			for (HealthChecksCriteriaDTO dto : criteria) {
				Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC",
						dto.getPickList(), "");
				// Expression exp = new Expression(attribute, operator, value,
				// agentValidationRequired, questionSkin, pickList, sequenceNumber);
				expression.add(exp);
			}
		}
		List<Expression> expressions = expression;
		CampaignCriteria campaignCriteria = new CampaignCriteria();
		campaignCriteria.setAborted(false);
		campaignCriteria.setIgnoreDefaultIndustries(zoomBuySearchDTO.isIgnoreDefaultIndustries());
		campaignCriteria.setIgnoreDefaultTitles(zoomBuySearchDTO.isIgnoreDefaultTitles());
		// campaignCriteria.setParallelBuy(zoomBuySearchDTO.isParallelBuy());
		campaignCriteria.setRequestJobDate(dbq.getJobRequestTime());
		campaignCriteria.setOrganizationId(zoomBuySearchDTO.getOrganizationId());

		campaignCriteria.setCampaignId(zoomBuySearchDTO.getCampaignId());

		campaignCriteria.setTotalPurchaseReq(zoomBuySearchDTO.getTotalCount());

		// logger.debug("dbq.getTotalCount()"+dbq.getRequestCount()+"####campaignId###"+dbq.getCampaignId());

		if (campaign.getLeadsPerCompany() > 0) {
			campaignCriteria.setLeadPerCompany(campaign.getLeadsPerCompany());
		}
		if (campaign.getDuplicateCompanyDuration() > 0) {
			campaignCriteria.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration() + " Days");
		}
		if (campaign.getDuplicateLeadsDuration() > 0) {
			campaignCriteria.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration() + " Days");
		}

		if (zoomBuySearchDTO.isIgnoreANDTitles()) {
			campaignCriteria.setAndTitleIgnored(true);
		} else {
			campaignCriteria.setAndTitleIgnored(false);
		}

		zoomBuySearchDTO.setIgnorePrimaryIndustries(campaign.isIgnorePrimaryIndustries());
		zoomBuySearchDTO.setPrimaryIndustries(campaign.isPrimaryIndustriesOnly());

		if (zoomBuySearchDTO.isIgnorePrimaryIndustries()) {
			campaignCriteria.setPrimaryIndustriesOnly(true);
		} else {
			campaignCriteria.setPrimaryIndustriesOnly(false);
		}

		campaignCriteria = setCampaignCriteria(campaignCriteria, expressions, campaign);

		if (campaignCriteria.getManagementLevel() == null || campaignCriteria.getManagementLevel().isEmpty())
			campaignCriteria.setManagementLevel("Non Management,MANAGER,DIRECTOR,VP_EXECUTIVES,C_EXECUTIVES,Board Members");

		if ((campaignCriteria.getMinEmployeeCount() == null || campaignCriteria.getMinEmployeeCount().isEmpty())
				&& (campaignCriteria.getMaxEmployeeCount() == null || campaignCriteria.getMaxEmployeeCount().isEmpty())) {
			campaignCriteria.setMinEmployeeCount("0");
			campaignCriteria.setMaxEmployeeCount("0");
		}

		if (campaignCriteria == null) {
			return purchaseString.append("No Criteria Found For Campaign ID : " + campaignCriteria.getCampaignId())
					.toString();
		}
		List<String> suppressDomains = new ArrayList<String>();
		Map<String, Integer> successCompanyIds = getSuccessCompanyDomainList(campaignCriteria);
		List<String> suppressCompanyNames = new ArrayList<String>();
		Map<String, Integer> successCompanyNames = getSuccessCompanyList(campaignCriteria);
		if (successCompanyIds != null && successCompanyIds.size() > 0) {
			for (Map.Entry<String, Integer> entry : successCompanyIds.entrySet()) {
				int value = entry.getValue();
				if (value >= campaignCriteria.getLeadPerCompany()) {
					suppressDomains.add(entry.getKey());
				}
			}
		}
		if (successCompanyNames != null && successCompanyNames.size() > 0) {
			for (Map.Entry<String, Integer> entry : successCompanyNames.entrySet()) {
				int value = entry.getValue();
				if (value >= campaignCriteria.getLeadPerCompany()) {
					suppressCompanyNames.add(entry.getKey());
				}
			}
		}
		/*
		 * SuppressionList suppressionList =
		 * suppressionListRepository.findSupressionListByCampaignIdAndStatus(
		 * campaignCriteria.getCampaignId());
		 * 
		 * SuppressionList gsuppressionList =
		 * getGlobalSuppressionListByCampaignId(suppressionList,campaignCriteria.
		 * getCampaignId()); suppressionList = gsuppressionList;
		 * 
		 * if(suppressionList!=null){ List<String> sDomains =
		 * suppressionList.getDomains(); List<String> domains = new ArrayList<String>();
		 * if(sDomains!=null && sDomains.size()>0){ for(String domain : sDomains){
		 * if(domain!=null && !domain.isEmpty() && !"".equals(domain)){ String cDomain =
		 * getSuppressedHostName(domain); if(!cDomain.isEmpty()) domains.add(cDomain); }
		 * } } List<String> emails = suppressionList.getEmailIds(); List<String>
		 * companies = suppressionList.getCompanies();
		 * 
		 * if(domains!=null && domains.size()>0){ suppressDomains.addAll(domains);
		 * campaignCriteria.setSuppressDomains(suppressDomains); } if(emails!=null &&
		 * emails.size()>0){ campaignCriteria.setSuppressEmail(emails); }
		 * if(companies!=null && companies.size()>0){
		 * suppressCompanyNames.addAll(companies);
		 * campaignCriteria.setSuppressComapnies(suppressCompanyNames); } }
		 */

		int count = Integer.parseInt(abmListServiceImpl.getABMListCountOfCampaign(dbq.getCampaignId()));
		List<String> companyIds = new ArrayList<String>();

		if (count > 0 && campaign.isABM()) {
			int batchsize = 0;
			if (count > 0) {
				batchsize = count / 1000;
				long modSize = count % 1000;
				if (modSize > 0) {
					batchsize = batchsize + 1;
				}
			}
			for (int i = 0; i < batchsize; i++) {
				Pageable pageable = PageRequest.of(i, 1000);
				List<AbmListNew> abmList = abmListServiceImpl.getABMListByCampaignIdByPage(campaign.getId(), pageable);
				if (abmList != null && abmList.size() > 0) {
					// if(abmList.getCompanyList()!=null && abmList.getCompanyList().size()>0){
					for (AbmListNew abm : abmList) {
						if (abm.getCompanyId() != null && !abm.getCompanyId().isEmpty()
								&& !abm.getCompanyId().equalsIgnoreCase(NOT_FOUND)
								&& !abm.getCompanyId().equalsIgnoreCase(GET_COMPANY_ID)
						        && StringUtils.isNumeric(abm.getCompanyId()) ) {
							companyIds.add(abm.getCompanyId());
						}
					}
					// }
					if(companyIds==null || companyIds.size()==0) {
						logger.error("no records found in abm");
						return "no companies found for abm";
					}
				}
			}
			campaignCriteria.setAbmCompanyIds(companyIds);
		}

		/*
		 * if(zoomBuySearchDTO.isReplenishData()){
		 * replenishData(abmList,campaign,zoomBuySearchDTO.isInternalCampaign());
		 * abmList = abmListServiceImpl.getABMListByCampaignId(campaign.getId()); }
		 * if(abmList!=null && abmList.isReplenish()){ abmList = null; }
		 */
		// ABMList abmList = null;

		logger.debug("Purchasing records ");
		List<ProspectCallLog> callableList = getCallableList(campaign);
		if (callableList != null && callableList.size() > 0) {
			Map<String, Integer> indiMap = campaignCriteria.getCompanyIndirectMap();
			Map<String, Integer> dirMap = campaignCriteria.getCompanyDirectMap();
			if (indiMap == null) {
				indiMap = new HashMap<String, Integer>();
			}
			if (dirMap == null) {
				dirMap = new HashMap<String, Integer>();
			}
			for (ProspectCallLog pcLog : callableList) {
				Integer indcount = indiMap.get(pcLog.getProspectCall().getProspect().getSourceCompanyId());
				Integer dircount = dirMap.get(pcLog.getProspectCall().getProspect().getSourceCompanyId());

				if (!pcLog.getProspectCall().getProspect().isDirectPhone()) {
					if (dircount != null && dircount > 0) {
						dirMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), dircount + 1);
					} else {
						dirMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), 1);
					}
				} else {
					if (indcount != null && indcount > 0) {
						indiMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), indcount + 1);
					} else {
						indiMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), 1);
					}
				}
			}
			campaignCriteria.setCompanyIndirectMap(indiMap);
			campaignCriteria.setCompanyDirectMap(dirMap);

		}
		/*
		 * campaignCriteria.setContactRequirements("5");
		 * buyTotalData(campaign,runningCampaignIds,campaignCriteria); Long totalTarget
		 * = campaignCriteria.getTotalPurchaseReq();
		 */
		if (campaignCriteria.getAbmCompanyIds() != null && campaignCriteria.getAbmCompanyIds().size() > 0) {
			// Buying success data
			/*
			 * if(campaignCriteria.isFetchSuccessData()) { Long successCount =
			 * 0L;//getSuccessProspectsABM(campaignCriteria, suppressionList);
			 * if(successCount>0){ Long totalPurchaseReq =
			 * campaignCriteria.getTotalPurchaseReq(); Long pendingPurchaseReq =
			 * totalPurchaseReq-successCount; campaignCriteria.setTotalPurchaseReq(
			 * pendingPurchaseReq); } }
			 */
			if (campaign.isIndirectBuyStarted()) {
				campaignCriteria.setContactRequirements("2");
				buyTotalData(campaign, runningCampaignIds, campaignCriteria);
			} else {
				campaignCriteria.setContactRequirements("5");
				buyTotalData(campaign, runningCampaignIds, campaignCriteria);
				Long totalTarget = campaignCriteria.getTotalPurchaseReq();
				Long diff = totalTarget - buyRowCount;
				if (diff > 0) {
					campaign.setIndirectBuyStarted(true);
					campaignRepository.save(campaign);
				}
				campaignCriteria.setContactRequirements("2");// not got enough critiera so buying the data with contacttype2
				campaignCriteria.setTotalPurchaseReq(diff);
				buyTotalData(campaign, runningCampaignIds, campaignCriteria);
			}
		} else if (campaign.isMultiProspectCalling()) {
			// Buying success data
			/*
			 * if(campaignCriteria.isFetchSuccessData()) { Long successCount =
			 * 0L;//getSuccessProspectsByCriteria(campaignCriteria, suppressionList);
			 * if(successCount>0){ Long totalPurchaseReq =
			 * campaignCriteria.getTotalPurchaseReq(); Long pendingPurchaseReq =
			 * totalPurchaseReq-successCount; campaignCriteria.setTotalPurchaseReq(
			 * pendingPurchaseReq); } }
			 */

			if (campaign.isIndirectBuyStarted()) {
				campaignCriteria.setContactRequirements("2");
				buyMultiData(campaign, runningCampaignIds, campaignCriteria);
			} else {
				campaignCriteria.setContactRequirements("5");
				buyTotalData(campaign, runningCampaignIds, campaignCriteria);
				Long totalTarget = campaignCriteria.getTotalPurchaseReq();
				Long diff = totalTarget - buyRowCount;
				if (diff > 0) {
					campaign.setIndirectBuyStarted(true);
					campaignRepository.save(campaign);

					campaignCriteria.setContactRequirements("2");// not got enough critiera so buying the data with contacttype2
					campaignCriteria.setTotalPurchaseReq(diff);
					buyMultiData(campaign, runningCampaignIds, campaignCriteria);
				}
			}

		} else {
			// Buying success data
			/*
			 * if(campaignCriteria.isFetchSuccessData()) { Long successCount =
			 * 0L;//getSuccessProspectsByCriteria(campaignCriteria, suppressionList);
			 * if(successCount>0){ Long totalPurchaseReq =
			 * campaignCriteria.getTotalPurchaseReq(); Long pendingPurchaseReq =
			 * totalPurchaseReq-successCount; campaignCriteria.setTotalPurchaseReq(
			 * pendingPurchaseReq); } }
			 */

			campaignCriteria.setContactRequirements("5");
			buyTotalData(campaign, runningCampaignIds, campaignCriteria);
		}

		try {
			logger.debug("Total no of Record Purchased for this Job : " + buyRowCount);
			//Long successRemoved = removeSuccessDuplicates.removeSuccessLeads(campaign, campaignCriteria);
			//buyRowCount = buyRowCount - successRemoved;
			List<String> campaignIds = new ArrayList<String>();
			campaignIds.add(campaign.getId());
			//dataSlice.getQualityBucket(campaignIds);
			// Long tenCount =
			// tenCompanies.getTenProspectsForACompany(campaignCriteria.getCampaignId());
			// buyRowCount = buyRowCount - tenCount;
			if (campaignCriteria.getMultiprospectMap() != null && campaignCriteria.getMultiprospectMap().size() > 0) {
				Map<String, List<ProspectCallLog>> multiMap = new HashMap<String, List<ProspectCallLog>>();
				for (ProspectCallLog pc : callableList) {
					List<ProspectCallLog> pclList = multiMap.get(pc.getProspectCall().getProspect().getPhone());
					if (pclList == null) {
						pclList = new ArrayList<ProspectCallLog>();
					}
					pclList.add(pc);
					multiMap.put(pc.getProspectCall().getProspect().getPhone(), pclList);
				}
				for (Map.Entry<String, List<ProspectCallLog>> entry : multiMap.entrySet()) {
					List<ProspectCallLog> mpcList = entry.getValue();
					if (mpcList.size() == 5) {
						for (ProspectCallLog p : mpcList) {
							if (p.getProspectCall().getProspect().isDirectPhone()) {
								p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								p.getProspectCall().getProspect().setDirectPhone(false);
								int qSort = p.getProspectCall().getQualityBucketSort();
								int firstTwo = qSort / 1000;
								int secondTemp = firstTwo % 10;
								int second = secondTemp * 1000;
								qSort = qSort - second;
								qSort = qSort + 2000;
								p.getProspectCall().setQualityBucketSort(qSort);
								prospectCallLogRepository.save(p);
							}
						}
					}
					if (mpcList.size() == 4) {
						for (ProspectCallLog p : mpcList) {
							if (p.getProspectCall().getProspect().isDirectPhone()) {
								p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								p.getProspectCall().getProspect().setDirectPhone(false);
								int qSort = p.getProspectCall().getQualityBucketSort();
								int firstTwo = qSort / 1000;
								int secondTemp = firstTwo % 10;
								int second = secondTemp * 1000;
								qSort = qSort - second;
								qSort = qSort + 3000;
								p.getProspectCall().setQualityBucketSort(qSort);
								prospectCallLogRepository.save(p);
							}
						}

					}
					if (mpcList.size() == 3) {
						for (ProspectCallLog p : mpcList) {
							if (p.getProspectCall().getProspect().isDirectPhone()) {
								p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								p.getProspectCall().getProspect().setDirectPhone(false);
								int qSort = p.getProspectCall().getQualityBucketSort();
								int firstTwo = qSort / 1000;
								int secondTemp = firstTwo % 10;
								int second = secondTemp * 1000;
								qSort = qSort - second;
								qSort = qSort + 4000;
								p.getProspectCall().setQualityBucketSort(qSort);
								prospectCallLogRepository.save(p);
							}
						}
					}
					if (mpcList.size() == 2) {
						for (ProspectCallLog p : mpcList) {
							if (p.getProspectCall().getProspect().isDirectPhone()) {
								p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								p.getProspectCall().getProspect().setDirectPhone(false);
								int qSort = p.getProspectCall().getQualityBucketSort();
								int firstTwo = qSort / 1000;
								int secondTemp = firstTwo % 10;
								int second = secondTemp * 1000;
								qSort = qSort - second;
								qSort = qSort + 5000;
								p.getProspectCall().setQualityBucketSort(qSort);
								prospectCallLogRepository.save(p);
							}
						}
					}
					if (mpcList.size() == 1) {
						for (ProspectCallLog p : mpcList) {

							if (!p.getProspectCall().getProspect().isDirectPhone()) {
								p = prospectCallLogRepository.findByProspectCallId(p.getProspectCall().getProspectCallId());
								int qSort = p.getProspectCall().getQualityBucketSort();
								int firstTwo = qSort / 1000;
								int secondTemp = firstTwo % 10;
								int second = secondTemp * 1000;
								qSort = qSort - second;
								qSort = qSort + 6000;
								p.getProspectCall().setQualityBucketSort(qSort);
								prospectCallLogRepository.save(p);
							}
						}
					}

				}

			}
			/*
			 * List<String> toemails = new ArrayList<String>();
			 * toemails.add("data@xtaascorp.com"); if(dbq.getRequestorId()!=null &&
			 * !dbq.getRequestorId().isEmpty()) { Login l =
			 * abmListServiceImpl.getLogin(dbq.getRequestorId()); if(l.getEmail()!=null &&
			 * !l.getEmail().isEmpty()) toemails.add(l.getEmail()); }
			 * XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com",
			 * campaign.getName()+" Data purchase job completed",
			 * buyRowCount+" numbers of records purchased for this job");
			 */

			DataBuyQueue dbqc = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqc.getPurchasedCount() != null) {
				dbqc.setPurchasedCount(dbqc.getPurchasedCount() + buyRowCount);
				dbqc.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqc.setPurchasedCount(buyRowCount);
				dbqc.setZoomInfoPurchasedCount(buyRowCount);
			}
			// dbqc.setStatus(DataBuyQueueStatus.COMPLETED.toString());
			// dbqc.setJobEndTime(new Date());
			dataBuyQueueRepository.save(dbqc);

		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			dbqe.setPurchasedCount(buyRowCount);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}

		return purchaseString.toString();
	}

	private String getSuppressedHostName(String domain) {
		try {
			if (domain != null) {
				domain = domain.toLowerCase();
				if (domain.contains("http://") || domain.contains("https://") || domain.contains("HTTP://")
						|| domain.contains("HTTPS://")) {

				} else {
					domain = "http://" + domain;
				}
			}

			URL aURL = new URL(domain);
			domain = aURL.getHost();
			if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
				if ((domain.contains("www.") || domain.contains("WWW."))) {
					domain = domain.substring(4, domain.length());
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 */
					return domain;
					// }
				} else {
					/*
					 * if(domain.indexOf(".")>0){ domain= domain.substring(0,domain.indexOf("."));
					 * return domain; }else{
					 */
					return domain;
					// }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return domain;
	}

	private void buyTotalData(Campaign campaign, List<String> runningCampaignIds, CampaignCriteria campaignCriteria) {
		List<String> usStateList = new ArrayList<String>(0);
		/*
		 * if(campaignCriteria.getCountry().equalsIgnoreCase("USA")){
		 * 
		 * Long estBuy = Math.round(campaignCriteria.getTotalPurchaseReq()*0.3);// 30%
		 * est Long cstBuy = Math.round(campaignCriteria.getTotalPurchaseReq()*0.3);//
		 * 30% cst Long mstBuy =
		 * Math.round(campaignCriteria.getTotalPurchaseReq()*0.1);// 10% mst Long pstBuy
		 * = Math.round(campaignCriteria.getTotalPurchaseReq()*0.3);// 30% pst
		 * 
		 * campaignCriteria.setEstBuy(estBuy); campaignCriteria.setCstBuy(cstBuy);
		 * campaignCriteria.setMstBuy(mstBuy); campaignCriteria.setPstBuy(pstBuy); }
		 */

		if (campaignCriteria.getCountryList().size() == 1
				&& campaignCriteria.getCountryList().get(0).equalsIgnoreCase("USA")) {
			usStateList.addAll(getPST());
			usStateList.addAll(getEST());
			usStateList.addAll(getCST());
			usStateList.addAll(getMST());
		}
		if (campaignCriteria.getCountryList().size() == 2
				&& ((campaignCriteria.getCountryList().get(0).equalsIgnoreCase("Canada") 
						&& campaignCriteria.getCountryList().get(1).equalsIgnoreCase("USA"))
						|| (campaignCriteria.getCountryList().get(0).equalsIgnoreCase("USA") 
								&& campaignCriteria.getCountryList().get(1).equalsIgnoreCase("Canada")))) {
			usStateList.addAll(getPST());
			usStateList.addAll(getEST());
			usStateList.addAll(getCST());
			usStateList.addAll(getMST());
			usStateList.addAll(getCanadaStates());
		}
		// buyData(campaignCriteria,usStateList,campaign,runningCampaignIds);
		/*
		 * Long totalCount = campaignCriteria.getTotalPurchaseReq(); Long timeCount =
		 * new Long(0); usStateList= getEST(); if(campaignCriteria.getState()!=null &&
		 * !campaignCriteria.getState().isEmpty()){ //DO NOTHING }else{ // TODO Data is
		 * coming less, as we are restricting. so commented
		 * //campaignCriteria.setTotalPurchaseReq(campaignCriteria.getEstBuy()); }
		 * buyData(campaignCriteria,usStateList,campaign,runningCampaignIds); timeCount
		 * = timeCount+buyRowCount;
		 * 
		 * // rowCount = new Long(0); usStateList= getCST();
		 * if(campaignCriteria.getState()!=null &&
		 * !campaignCriteria.getState().isEmpty()){ //DO NOTHING }else{ // TODO Data is
		 * coming less, as we are restricting. so commented
		 * //campaignCriteria.setTotalPurchaseReq(campaignCriteria.getCstBuy()); }
		 * buyData(campaignCriteria,usStateList,campaign,runningCampaignIds); timeCount
		 * = timeCount+buyRowCount;
		 * 
		 * //rowCount = new Long(0); usStateList = getMST();
		 * if(campaignCriteria.getState()!=null &&
		 * !campaignCriteria.getState().isEmpty()){ //DO NOTHING }else{ // TODO Data is
		 * coming less, as we are restricting. so commented
		 * //campaignCriteria.setTotalPurchaseReq(campaignCriteria.getMstBuy()); }
		 * buyData(campaignCriteria,usStateList,campaign,runningCampaignIds); timeCount
		 * = timeCount+buyRowCount;
		 * 
		 * 
		 * //rowCount = new Long(0); usStateList= getPST(); //
		 * campaignCriteria.setTotalPurchaseReq(campaignCriteria.getPstBuy());
		 * if(campaignCriteria.getState()!=null &&
		 * !campaignCriteria.getState().isEmpty()){ //DO NOTHING }else{ // TODO Data is
		 * coming less, as we are restricting. so commented
		 * //campaignCriteria.setTotalPurchaseReq(totalCount-timeCount); }
		 * buyData(campaignCriteria,usStateList,campaign,runningCampaignIds);
		 */

		// }else{
		if (campaignCriteria.getCountryList().size() == 1
				&& campaignCriteria.getCountryList().get(0).equalsIgnoreCase("United Kingdom")) {
			usStateList.addAll(getUKStates());
		}
		if (campaignCriteria.getCountryList().size() == 1
				&& campaignCriteria.getCountryList().get(0).equalsIgnoreCase("Canada")) {
			usStateList.addAll(getCanadaStates());
		}
		buyData(campaignCriteria, usStateList, campaign, runningCampaignIds);
		// }
	}

	private List<ProspectCallLog> getCallableList(Campaign campaign) {

		// buyTotalData(campaign,runningCampaignIds,campaignCriteria);// bought the data
		// with 2
		// Fetching the callables below

		List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueued(campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList0);
		List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCount(1,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList1);
		List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCount(2,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList2);
		List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCount(3,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList3);
		List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCount(4,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList4);
		List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCount(5,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList5);
		List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCount(6,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList6);
		List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCount(7,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList7);
		List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCount(8,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList8);
		List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCount(9,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList9);
		List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCount(10,
				campaign.getId());
		prospectCallLogList.addAll(prospectCallLogList10);

		return prospectCallLogList;
	}

	private void buyMultiData(Campaign campaign, List<String> runningCampaignIds, CampaignCriteria campaignCriteria) {

		Map<String, Integer> multiProspectMap = new HashMap<String, Integer>();
		// buyTotalData(campaign,runningCampaignIds,campaignCriteria);// bought the data
		// with 2
		// Fetching the callables below
		List<ProspectCallLog> prospectCallLogList = getCallableList(campaign);
		Map<String, String> multiCompanyIdsMap = campaignCriteria.getMultiCompanyIdsMap();
		// Grouping the callables below for having minimum 4 records for duplicate
		// numbers.
		for (ProspectCallLog pc : prospectCallLogList) {
			Integer count = multiProspectMap.get(pc.getProspectCall().getProspect().getPhone());
			if (count == null) {
				count = 0;
			}
			multiProspectMap.put(pc.getProspectCall().getProspect().getPhone(), count + 1);
			if (multiCompanyIdsMap == null) {
				multiCompanyIdsMap = new HashMap<String, String>();
			}
			multiCompanyIdsMap.put(pc.getProspectCall().getProspect().getPhone(),
					pc.getProspectCall().getProspect().getSourceCompanyId());
		}
		campaignCriteria.setMultiCompanyIdsMap(multiCompanyIdsMap);
		campaignCriteria.setMultiprospectMap(multiProspectMap);

		List<String> personTitleList = new ArrayList<String>(0);
		if (campaignCriteria.getTitleKeyWord() != null) {
			personTitleList = Arrays.asList(campaignCriteria.getTitleKeyWord().split(","));
		}

		if (personTitleList != null && personTitleList.size() > 0) {
			buyTotalData(campaign, runningCampaignIds, campaignCriteria);
			for (String purchaseTitle : personTitleList) {
				DataBuyQueue dbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
				if (dbqk != null) {
					break;
				}
				if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
					break;
				}
				// getCompanyPurchaseResponseReplenish(purchaseTitle,campaignCriteria,campaign,runningCampaignIds);
			}

		} else {
			buyTotalData(campaign, runningCampaignIds, campaignCriteria);
			// getCompanyPurchaseResponseReplenish(null,campaignCriteria,campaign,runningCampaignIds);
		}
	}

	private void buyData(CampaignCriteria campaignCriteria, List<String> usStateList, Campaign campaign,
			List<String> runningCampaignIds) {
		List<String> personTitleList = new ArrayList<String>(0);
		List<String> stateList = new ArrayList<String>(0);
		List<String> industryCodeList = new ArrayList<String>(0);
		List<String> abmList = new ArrayList<String>(0);
		String searchAbmId = "";
		boolean foundAbmId = false;
		campaignCriteria.setSkipAbmCompany(false);

		if (campaignCriteria.getTitleKeyWord() != null) {
			personTitleList = Arrays.asList(campaignCriteria.getTitleKeyWord().split(","));
		}
		if (campaignCriteria.getState() != null) {
			stateList = Arrays.asList(campaignCriteria.getState().split(","));
		}

		if (campaignCriteria.getUsStates() != null) {
			stateList = Arrays.asList(campaignCriteria.getUsStates().split(","));
		}
		if (campaignCriteria.getZipcodes() != null) {
			stateList = Arrays.asList(campaignCriteria.getZipcodes().split(","));
		}
		if (campaignCriteria.getSicCode() != null) {
			industryCodeList = Arrays.asList(campaignCriteria.getSicCode().split(","));
		}

		if (campaignCriteria.getAbmCompanyIds() != null && campaignCriteria.getAbmCompanyIds().size() > 0) {
			abmList = campaignCriteria.getAbmCompanyIds();
		}
		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		if (abmList != null && abmList.size() > 0) {
			searchAbmId = searchdbqk.getAbmId();
		}
		int offset = 1;
		boolean maxFlag = false;// This flag to get rid of pageLimit
		int abmcounter = 1;
		if (abmList != null && abmList.size() > 0) {
			for (String companyId : abmList) {
				if (buyRowCount >= campaignCriteria.getTotalPurchaseReq())
					break;
				logger.debug(abmcounter++ + "-->companyID---->" + companyId);
				if (!foundAbmId && searchAbmId != null && !searchAbmId.isEmpty() && companyId.equalsIgnoreCase(searchAbmId)) {
					foundAbmId = true;
				} else if (!foundAbmId && (searchAbmId == null || searchAbmId.isEmpty())) {
					foundAbmId = true;
				} else if (!foundAbmId) {
					continue;
				}
				DataBuyQueue dbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
				if (dbqk != null) {
					break;
				}
				searchZoom(offset, campaignCriteria, campaignCriteria.getTitleKeyWord(), null, null, campaign, runningCampaignIds,companyId,campaignCriteria.getManagementLevel(),
       					campaignCriteria.getMinEmployeeCount(),campaignCriteria.getMaxEmployeeCount(),campaignCriteria.getMinRevenue(),campaignCriteria.getMaxRevenue());

				/*buyCriteriaLoop(campaignCriteria, usStateList, campaign, runningCampaignIds, offset, maxFlag, personTitleList,
						stateList, industryCodeList, companyId);*/
			}
		} else {

			buyCriteriaLoop(campaignCriteria, usStateList, campaign, runningCampaignIds, offset, maxFlag, personTitleList,
					stateList, industryCodeList, null);
		}

	}

	///////////////////////////////////// Success call log logic here
	///////////////////////////////////// ////////////////////
	/*
	 * public Long getSuccessProspectsByCriteria(CampaignCriteria
	 * campaignCriteria,SuppressionList suppression){ Long count = new Long(0);
	 * logger.debug("Fetching success data to campaign "+campaignCriteria.
	 * getCampaignId()); List<SuccessCallLog> successCallLogList =
	 * successCallLogRepositoryImpl.findByCriteria(campaignCriteria);
	 * logger.debug("Fetching Completed success data to campaign "+campaignCriteria.
	 * getCampaignId()); count =
	 * getSuccessData(successCallLogList,suppression,campaignCriteria,count);
	 * 
	 * return count; }
	 * 
	 * private Long getSuccessData(List<SuccessCallLog>
	 * successCallLogList,SuppressionList suppression,CampaignCriteria
	 * campaignCriteria,Long count){ List<String> supressedDomains = new
	 * ArrayList<String>(); List<String> emails = new ArrayList<String>();
	 * if(suppression!=null){ if(suppression.getEmailIds()!=null &&
	 * suppression.getEmailIds().size()>0){ emails = suppression.getEmailIds(); }
	 * 
	 * if(suppression.getDomains()!=null && suppression.getDomains().size()>0){
	 * List<String> domains = suppression.getDomains(); for(String domain :
	 * domains){ String supDomain = XtaasUtils.getSuppressedHostName(domain);
	 * supressedDomains.add(supDomain); } } } for(SuccessCallLog scl :
	 * successCallLogList){ if(scl.getProspectCall().getOrganizationId()!=null &&
	 * campaignCriteria.getOrganizationId()!=null &&
	 * scl.getProspectCall().getOrganizationId().equalsIgnoreCase(campaignCriteria.
	 * getOrganizationId())) { if(count > campaignCriteria.getTotalPurchaseReq())
	 * break; boolean emailSuppression = false; boolean domainSuppression = false;
	 * boolean negativeWord = false; if(emails !=null && emails.size()>0){
	 * if(scl.getProspectCall().getProspect().getEmail()!=null &&
	 * !scl.getProspectCall().getProspect().getEmail().isEmpty() &&
	 * !emails.contains(scl.getProspectCall().getProspect().getEmail())){
	 * //goodContacts.add(gc); emailSuppression = true; } }
	 * 
	 * //checking domain suppression if(!emailSuppression && supressedDomains !=null
	 * && supressedDomains.size()>0){
	 * if(scl.getProspectCall().getProspect().getCustomAttributeValue("domain")!=
	 * null &&
	 * !scl.getProspectCall().getProspect().getCustomAttributeValue("domain").
	 * toString().isEmpty() ){
	 * if(!supressedDomains.contains(XtaasUtils.getSuppressedHostName(scl.
	 * getProspectCall().getProspect().getCustomAttributeValue("domain").toString())
	 * )){ //goodContacts.add(gc); domainSuppression=true; } }else
	 * if(scl.getProspectCall().getProspect().getEmail()!=null &&
	 * !scl.getProspectCall().getProspect().getEmail().isEmpty()){
	 * if(!supressedDomains.contains(XtaasUtils.getSuppressedHostName(scl.
	 * getProspectCall().getProspect().getEmail()))){ //goodContacts.add(gc);
	 * domainSuppression=true; } } } //Checking negative words if(!domainSuppression
	 * && !emailSuppression && campaignCriteria.getNegativeKeyWord()!=null &&
	 * !campaignCriteria.getNegativeKeyWord().isEmpty()){ List<String> negList =
	 * Arrays.asList(campaignCriteria.getNegativeKeyWord().split(",")); for(String
	 * negWord : negList){
	 * if(scl.getProspectCall().getProspect().getTitle().contains(negWord)){
	 * negativeWord = true; } } } if(!emailSuppression && !domainSuppression &&
	 * !negativeWord){ ProspectCallLog pcLog = new ProspectCallLog(); Prospect p =
	 * scl.getProspectCall().getProspect(); Prospect prospect = new Prospect();
	 * prospect.setFirstName(p.getFirstName());
	 * prospect.setLastName(p.getLastName()); prospect.setTitle(p.getTitle());
	 * prospect.setDepartment(p.getDepartment());
	 * prospect.setInputDepartment(p.getInputDepartment());
	 * prospect.setEmail(p.getEmail()); prospect.setCompany(p.getCompany());
	 * prospect.setEncrypted(p.getIsEncrypted());
	 * prospect.setCountry(p.getCountry()); prospect.setPhone(p.getPhone());
	 * prospect.setCompanyPhone(p.getCompanyPhone());
	 * prospect.setStateCode(p.getStateCode());
	 * prospect.setIndustry(p.getIndustry());
	 * prospect.setIndustryList(p.getIndustryList());
	 * prospect.setCustomAttributes(p.getCustomAttributes());
	 * prospect.setSource(p.getSource()); prospect.setSourceId(p.getSourceId());
	 * prospect.setSourceCompanyId(p.getSourceCompanyId());
	 * prospect.setManagementLevel(p.getManagementLevel());
	 * prospect.setAddressLine1(p.getAddressLine1());
	 * prospect.setZipCode(p.getZipCode()); prospect.setCity(p.getCity());
	 * prospect.setDirectPhone(p.isDirectPhone());
	 * prospect.setTimeZone(p.getTimeZone()); prospect.setEu(p.getIsEu());
	 * 
	 * 
	 * ProspectCall spccall = new ProspectCall(UUID.randomUUID().toString(),
	 * campaignCriteria.getCampaignId(), prospect);
	 * spccall.setOrganizationId(scl.getProspectCall().getOrganizationId());
	 * pcLog.setProspectCall(spccall);
	 * pcLog.getProspectCall().setProspect(prospect);
	 * pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
	 * if(prospect.getCountry().equalsIgnoreCase("USA") ||
	 * prospect.getCountry().equalsIgnoreCase("United States") ||
	 * prospect.getCountry().equalsIgnoreCase("US")||
	 * prospect.getCountry().equalsIgnoreCase("Canada") ||
	 * prospect.getCountry().equalsIgnoreCase("United Kingdom") ||
	 * prospect.getCountry().equalsIgnoreCase("UK")){
	 * 
	 * pcLog.getProspectCall().setCallRetryCount(786); }else {
	 * if(formatPhone(prospect.getPhone())) {
	 * pcLog.getProspectCall().setCallRetryCount(148); }else {
	 * pcLog.getProspectCall().setCallRetryCount(786); } }
	 * pcLog.setDataSlice("slice0"); pcLog.getProspectCall().setDataSlice("slice0");
	 * logger.debug("saving success data to campaing"+count); boolean saved =
	 * saveProspectCallLog(pcLog, campaignCriteria); if(saved) count++; } } } return
	 * count; }
	 */
	private boolean saveProspectCallLog(ProspectCallLog prospectCallLog, CampaignCriteria campaignCriteria) {
		if (!isPersonRecordAlreadyPurchasedPCLog(prospectCallLog, campaignCriteria)) {
			logger.debug("Saving data #### : " + prospectCallLog.getProspectCall().getProspectCallId());
			prospectCallLog.getProspectCall().setCampaignId(campaignCriteria.getCampaignId());
			prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			if (prospect.getCountry().equalsIgnoreCase("USA") || prospect.getCountry().equalsIgnoreCase("United States")
					|| prospect.getCountry().equalsIgnoreCase("US") || prospect.getCountry().equalsIgnoreCase("Canada")
					|| prospect.getCountry().equalsIgnoreCase("United Kingdom") || prospect.getCountry().equalsIgnoreCase("UK")) {

				prospectCallLog.getProspectCall().setCallRetryCount(786);
			} else {
				if (formatPhone(prospect.getPhone())) {
					prospectCallLog.getProspectCall().setCallRetryCount(148);
				} else {
					prospectCallLog.getProspectCall().setCallRetryCount(786);
				}
			}
			prospectCallLogRepository.save(prospectCallLog);
			ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
			prospectCallInteraction.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			prospectCallInteraction.setProspectCall(prospectCallLog.getProspectCall());
			prospectCallInteraction.getProspectCall()
					.setCallRetryCount(prospectCallLog.getProspectCall().getCallRetryCount());
			// if (campaignCriteria.isParallelBuy()) {
			prospectCallLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			if (prospectCallLog.getProspectCall().getCallRetryCount() < 10)
				prospectCallLog.getProspectCall().setCallRetryCount(786);
			prospectCallInteraction.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);

			if (prospectCallInteraction.getProspectCall().getCallRetryCount() < 10)
				prospectCallInteraction.getProspectCall().setCallRetryCount(786);
			// }
			prospectCallInteractionRepository.save(prospectCallInteraction);
			return true;

		}
		return false;
	}

	/*
	 * public Long getSuccessProspectsABM(CampaignCriteria
	 * campaignCriteria,SuppressionList suppression){ Long count = new Long(0);
	 * List<SuccessCallLog> successCallLogList = new ArrayList<SuccessCallLog>();
	 * List<String> companyIds = campaignCriteria.getAbmCompanyIds();
	 * logger.debug("Fetching success data to campaign "+campaignCriteria.
	 * getCampaignId()); if(companyIds!=null && companyIds.size()>0){ long
	 * companySize = companyIds.size(); long companyBatch = companySize/100; long
	 * finalsize = companySize % 100; if(companyBatch>0){ for(int
	 * i=1;i<=companyBatch;i++){ long size = i*100; int start = (int)(size-99);
	 * List<SuccessCallLog> successCallLogListTemp =
	 * successCallLogRepositoryImpl.findByCompanies(campaignCriteria,size,start);
	 * successCallLogList.addAll(successCallLogListTemp); } if(finalsize>0){ long
	 * size = finalsize; int start = (int)(companyBatch*100); List<SuccessCallLog>
	 * successCallLogListTemp =
	 * successCallLogRepositoryImpl.findByCompanies(campaignCriteria,size,start);
	 * successCallLogList.addAll(successCallLogListTemp); } }else{
	 * List<SuccessCallLog> successCallLogListTemp =
	 * successCallLogRepositoryImpl.findByCompanies(campaignCriteria,finalsize,1);
	 * successCallLogList.addAll(successCallLogListTemp); }
	 * 
	 * }
	 * logger.debug("Fetching Completed success data to campaign "+campaignCriteria.
	 * getCampaignId());
	 * 
	 * //List<SuccessCallLog> successCallLogList =
	 * successCallLogRepositoryImpl.findByCompanies(campaignCriteria);
	 * 
	 * count =
	 * getSuccessData(successCallLogList,suppression,campaignCriteria,count);
	 * 
	 * return count; }
	 */
	/////////////////////// success call log ends here
	/////////////////////// /////////////////////////////////
	private void buyCriteriaLoop(CampaignCriteria campaignCriteria, List<String> usStateList, Campaign campaign,
			List<String> runningCampaignIds, int offset, boolean maxFlag, List<String> personTitleList,
			List<String> stateList, List<String> industryCodeList, String abmId) {

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());

		String searchSIC = "";
		// boolean searchSICFlag = false;
		if (searchdbqk.isInterupted()) {
			if (searchdbqk.getSicCode() != null) {
				searchSIC = searchdbqk.getSicCode();
			}
		}

		if (industryCodeList != null && industryCodeList.size() > 0) {
			for (String sic : industryCodeList) {
				if (searchdbqk.isInterupted() && searchSIC != null && !searchSIC.isEmpty()
						&& !searchSIC.equalsIgnoreCase(sic)) {
					continue;
				}
				DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
				if (dbq1 != null) {
					break;
				}
				///////////////////////////////////////////////////////////////
				if (campaignCriteria.getSortedMgmtSearchList() != null
						&& !campaignCriteria.getSortedMgmtSearchList().isEmpty()) {
					searchByManagement(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usStateList, abmId,
							maxFlag, stateList);

				} else if (campaignCriteria.getSortedEmpSearchList() != null
						&& !campaignCriteria.getSortedEmpSearchList().isEmpty()) {
					searchByEmployee(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usStateList, abmId,
							maxFlag, stateList, null);

				} else if (campaignCriteria.getSortedRevenuetSearchList() != null
						&& !campaignCriteria.getSortedRevenuetSearchList().isEmpty()) {
					searchByRevenue(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usStateList, abmId,
							maxFlag, stateList, null);

				} else {
					//////////////////////////////////////////////////////////////

					if (stateList == null || stateList.size() == 0) {
						if (abmId != null && !abmId.isEmpty()) {
							// DO nothing
						} else {
							maxFlag = getMaxFlag(null, sic, campaignCriteria, null, null, null, null, null);
						}
						if (maxFlag && usStateList != null && usStateList.size() > 0) {
							searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usStateList, abmId, null,
									null, null, null, null);
							maxFlag = false;// resetting after iteration
						} else {
							searchZoom(offset, campaignCriteria, null, sic, null, campaign, runningCampaignIds, abmId, null, null,
									null, null, null);
						}
					} else if (stateList.size() > 0) {
						searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, stateList, abmId, null,
								null, null, null, null);
					}
				}
			}
		} else {

			if (campaignCriteria.getSortedMgmtSearchList() != null && !campaignCriteria.getSortedMgmtSearchList().isEmpty()) {
				searchByManagement(offset, campaignCriteria, null, null, campaign, runningCampaignIds, usStateList, abmId,
						maxFlag, stateList);

			} else if (campaignCriteria.getSortedEmpSearchList() != null
					&& !campaignCriteria.getSortedEmpSearchList().isEmpty()) {
				searchByEmployee(offset, campaignCriteria, null, null, campaign, runningCampaignIds, usStateList, abmId,
						maxFlag, stateList, null);

			} else if (campaignCriteria.getSortedRevenuetSearchList() != null
					&& !campaignCriteria.getSortedRevenuetSearchList().isEmpty()) {
				searchByRevenue(offset, campaignCriteria, null, null, campaign, runningCampaignIds, usStateList, abmId, maxFlag,
						stateList, null);

			} else {
				if (stateList == null || stateList.size() == 0) {
					if (abmId != null && !abmId.isEmpty()) {
						// DO nothing
					} else {
						maxFlag = getMaxFlag(null, null, campaignCriteria, null, null, null, null, null);
					}
					if (maxFlag && usStateList != null && usStateList.size() > 0) {
						searchByState(offset, campaignCriteria, null, null, campaign, runningCampaignIds, usStateList, abmId, null,
								null, null, null, null);
						maxFlag = false;// resetting after iteration
					} else {
						searchZoom(offset, campaignCriteria, null, null, null, campaign, runningCampaignIds, abmId, null, null,
								null, null, null);
					}
				} else if (stateList.size() > 0) {
					searchByState(offset, campaignCriteria, null, null, campaign, runningCampaignIds, stateList, abmId, null,
							null, null, null, null);
				}
			}
		}
		// }
	}

	private void searchByManagement(int offset, CampaignCriteria campaignCriteria, String purchaseTitle, String sic,
			Campaign campaign, List<String> runningCampaignIds, List<String> usstateList, String abmId, boolean maxFlag,
			List<String> stateList) {

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		String searchManagementLevel = "";
		// boolean searchManagementLevelFlag = false;
		if (searchdbqk.isInterupted()) {
			if (searchdbqk.getSearchManagementlevel() != null) {
				searchManagementLevel = searchdbqk.getSearchManagementlevel();
			}
		}

		for (String mgmtlevel : campaignCriteria.getSortedMgmtSearchList()) {
			if (searchdbqk.isInterupted() && searchManagementLevel != null && !searchManagementLevel.isEmpty()
					&& !searchManagementLevel.equalsIgnoreCase(mgmtlevel)) {
				continue;
			}
			DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
			if (dbq1 != null) {
				break;
			}
			if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
				break;
			}
			if (campaignCriteria.getSortedEmpSearchList() != null && !campaignCriteria.getSortedEmpSearchList().isEmpty()) {
				searchByEmployee(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usstateList, abmId, maxFlag,
						stateList, mgmtlevel);

			} else if (campaignCriteria.getSortedRevenuetSearchList() != null
					&& !campaignCriteria.getSortedRevenuetSearchList().isEmpty()) {
				searchByRevenue(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usstateList, abmId, maxFlag,
						stateList, mgmtlevel);

			} else {
				if (stateList == null || stateList.size() == 0) {
					if (abmId != null && !abmId.isEmpty()) {
						// DO nothing
					} else {
						maxFlag = getMaxFlag(null, sic, campaignCriteria, mgmtlevel, null, null, null, null);
					}
					if (maxFlag && usstateList != null && usstateList.size() > 0) {
						searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usstateList, abmId,
								mgmtlevel, null, null, null, null);
						maxFlag = false;// resetting after iteration
					} else {
						searchZoom(offset, campaignCriteria, null, sic, null, campaign, runningCampaignIds, abmId, mgmtlevel, null,
								null, null, null);
					}
				} else if (stateList.size() > 0) {
					searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, stateList, abmId, mgmtlevel,
							null, null, null, null);
				}
			}
		}
	}

	private void searchByEmployee(int offset, CampaignCriteria campaignCriteria, String purchaseTitle, String sic,
			Campaign campaign, List<String> runningCampaignIds, List<String> usstateList, String abmId, boolean maxFlag,
			List<String> stateList, String mgmtlevel) {

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		String searchEmployeeMin = "";
		String searchEmployeeMax = "";

		// boolean searchEmployeeFlag = false;
		if (searchdbqk.isInterupted()) {
			if (searchdbqk.getSearchEmployeeMin() != null) {
				searchEmployeeMin = searchdbqk.getSearchEmployeeMin();
			}
			if (searchdbqk.getSearchEmployeeMax() != null) {
				searchEmployeeMax = searchdbqk.getSearchEmployeeMax();
			}
		}

		for (Map.Entry<String, String[]> entry : campaignCriteria.getSortedEmpSearchList().entrySet()) {
			String[] arr = entry.getValue();
			String minEmp = arr[0];
			String maxEmp = arr[1];

			if (searchdbqk.isInterupted() && searchEmployeeMin != null && searchEmployeeMax != null
					&& !searchEmployeeMin.isEmpty() && !searchEmployeeMax.isEmpty() && !searchEmployeeMin.equalsIgnoreCase(arr[0])
					&& !searchEmployeeMax.equalsIgnoreCase(arr[1])) {
				continue;
			}
			DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
			if (dbq1 != null) {
				break;
			}
			if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
				break;
			}

			if (stateList == null || stateList.size() == 0) {
				if (abmId != null && !abmId.isEmpty()) {
					// DO nothing
				} else {
					maxFlag = getMaxFlag(null, sic, campaignCriteria, mgmtlevel, minEmp, maxEmp, null, null);
				}
				if (maxFlag && usstateList != null && usstateList.size() > 0) {
					searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usstateList, abmId,
							mgmtlevel, minEmp, maxEmp, campaignCriteria.getMinRevenue(),campaignCriteria.getMaxRevenue());
					maxFlag = false;// resetting after iteration
				} else {
					searchZoom(offset, campaignCriteria, null, sic, null, campaign, runningCampaignIds, abmId, mgmtlevel, minEmp,
							maxEmp, campaignCriteria.getMinRevenue(),campaignCriteria.getMaxRevenue());
				}
			} else if (stateList.size() > 0) {
				searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, stateList, abmId, mgmtlevel,
						minEmp, maxEmp, campaignCriteria.getMinRevenue(),campaignCriteria.getMaxRevenue());
			}
		}
	}

	private void searchByRevenue(int offset, CampaignCriteria campaignCriteria, String purchaseTitle, String sic,
			Campaign campaign, List<String> runningCampaignIds, List<String> usstateList, String abmId, boolean maxFlag,
			List<String> stateList, String mgmtlevel) {

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		String searchRevenueMin = "";
		String searchRevenueMax = "";
		// boolean searchRevenueFlag = false;
		if (searchdbqk.isInterupted()) {
			if (searchdbqk.getSearchRevenueMin() != null) {
				searchRevenueMin = searchdbqk.getSearchRevenueMin();
				searchRevenueMax = searchdbqk.getSearchRevenueMax();
			}
		}

		for (Map.Entry<String, String[]> entry : campaignCriteria.getSortedRevenuetSearchList().entrySet()) {
			String[] arr = entry.getValue();
			String minRev = arr[0];
			String maxRev = arr[1];
			if (searchdbqk.isInterupted() && searchRevenueMin != null && searchRevenueMax != null
					&& !searchRevenueMin.isEmpty() && !searchRevenueMax.isEmpty() && !searchRevenueMin.equalsIgnoreCase(arr[0])
					&& !searchRevenueMax.equalsIgnoreCase(arr[1])) {
				continue;
			}
			DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
			if (dbq1 != null) {
				break;
			}
			if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
				break;
			}

			if (stateList == null || stateList.size() == 0) {
				if (abmId != null && !abmId.isEmpty()) {
					// DO nothing
				} else {
					maxFlag = getMaxFlag(null, sic, campaignCriteria, mgmtlevel, null, null, minRev, maxRev);
				}
				if (maxFlag && usstateList != null && usstateList.size() > 0) {
					searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, usstateList, abmId,
							mgmtlevel, null, null, minRev, maxRev);
					maxFlag = false;// resetting after iteration
				} else {
					searchZoom(offset, campaignCriteria, null, sic, null, campaign, runningCampaignIds, abmId, mgmtlevel, null,
							null, minRev, maxRev);
				}
			} else if (stateList.size() > 0) {
				searchByState(offset, campaignCriteria, null, sic, campaign, runningCampaignIds, stateList, abmId, mgmtlevel,
						null, null, minRev, maxRev);
			}
		}
	}

	private void searchByState(int offset, CampaignCriteria campaignCriteria, String purchaseTitle, String sic,
			Campaign campaign, List<String> runningCampaignIds, List<String> stateList, String abmId, String mgmtlevel,
			String minEmp, String maxEmp, String minRev, String maxRev) {

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		String searchState = "";
		// boolean searchStateFlag = false;
		if (searchdbqk.isInterupted()) {
			if (searchdbqk.getSearchState() != null) {
				searchState = searchdbqk.getSearchState();
			}
		}

		for (String state : stateList) {
			if (searchdbqk.isInterupted() && searchState != null && !searchState.isEmpty()
					&& !searchState.equalsIgnoreCase(state)) {
				continue;
			}
			DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
					DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
			if (dbq1 != null) {
				break;
			}
			if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
				break;
			}
			searchZoom(offset, campaignCriteria, purchaseTitle, sic, state, campaign, runningCampaignIds, abmId, mgmtlevel,
					minEmp, maxEmp, minRev, maxRev);
		}
	}

	private void searchZoom(int offset, CampaignCriteria campaignCriteria, String purchaseTitle, String sic, String state,
			Campaign campaign, List<String> runningCampaignIds, String abmId, String mgmtlevel, String minEmp, String maxEmp,
			String minRev, String maxRev) {
		getPurchaseResponse(abmId, purchaseTitle, sic, state, campaignCriteria, campaign, runningCampaignIds, mgmtlevel,
				minEmp, maxEmp, minRev, maxRev);
		// }
	}

	private String buildUrl(String queryString, int offset, int size, String personId) throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;

		logger.debug("Key : " + key);
		if (personId != null) {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&PersonID=" + personId
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key=" + key;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/person/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,companyAddress,companyRevenueNumeric,companyRevenueRange,companyEmployeeRange,companyEmployeeCount&key="
					+ key + "&rpp=" + size + "&page=" + offset;
			return url;
		}
	}

	private String buildCompanyUrl(String queryString, /* int offset, int size, */String personId)
			throws NoSuchAlgorithmException {
		String key = getZoomOAuth();
		if (key == null || key.isEmpty())
			key = tokenKey;

		logger.debug("Key : " + key);
		if (personId != null) {
			/*
			 * String
			 * baseUrl="https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			 * String url = baseUrl + "pc="+partnerKey +"&CompanyID="+personId +
			 * "&outputType=json&outputFieldOptions=ManagementLevel,LocalAddress,jobFunction,isEU&key="
			 * +key; return url;
			 */
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/detail?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companysic,companyType&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		} else {
			String baseUrl = "https://partnerapi.zoominfo.com/partnerapi/v4/company/search?";
			String url = baseUrl + "pc=" + partnerKey + "&" + queryString// personTitle=President&IndustryClassification=33624074&State=New%20York&outputType=json&key="
					+ "&outputType=json&outputFieldOptions=companyPhone,companyRevenueNumeric,companyRevenueRange,"
					+ "companyEmployeeRange,companyTopLevelIndustry,companyHashtags,companyProductsAndServices,"
					+ "companysic,companynaics,companymergeracquisition,companyranking,companysubsidiary,"
					+ "companystructure,continent,isdefunct,acquiredByCompany,orderHashtags,companyType,"
					+ "techAttributes,similarCompanies,funding,lowestSICSOnly,lowestNAICSOnly&key=" + key;
			// +"&rpp="+size
			// +"&page="+offset;
			return url;
		}

	}

	/*
	 * private int getCount(String purchaseTitle,String sicCode,CampaignCriteria
	 * campaignCriteria){ boolean maxFlag = false; try{ StringBuilder queryString=
	 * createPurchaseCriteria(null,null,sicCode,null,campaignCriteria);
	 * 
	 * String previewUrl = buildUrl(queryString.toString(),1,25,null);
	 * logger.debug("\n previewUrl2 : "+previewUrl); URL url=new URL(previewUrl);
	 * //Thread.sleep(100); HttpURLConnection connection= (HttpURLConnection)
	 * url.openConnection(); connection.setRequestMethod("GET"); // int
	 * responseCode= connection.getResponseCode(); BufferedReader bufferedReader=
	 * new BufferedReader(new InputStreamReader(connection.getInputStream()));
	 * String inputline; StringBuffer response= new StringBuffer();
	 * while((inputline=bufferedReader.readLine())!=null) {
	 * response.append(inputline); } bufferedReader.close(); ZoomInfoResponse
	 * personZoomInfoResponse = getResponse(response,false);
	 * if(personZoomInfoResponse!=null){ int totalResult =
	 * personZoomInfoResponse.getPersonSearchResponse().getMaxResults(); return
	 * totalResult; } }catch(Exception e){ DataBuyQueue dbqe =
	 * dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.
	 * getCampaignId(), DataBuyQueueStatus.INPROCESS.toString(),
	 * campaignCriteria.getRequestJobDate()); dbqe.setPurchasedCount(buyRowCount);
	 * StringWriter sw = new StringWriter(); e.printStackTrace(new PrintWriter(sw));
	 * String exceptionAsString = sw.toString(); // logger.debug(exceptionAsString);
	 * dbqe.setErrorMsg(exceptionAsString); dbqe.setJobEndTime(new Date());
	 * dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
	 * dataBuyQueueRepository.save(dbqe); e.printStackTrace(); } return 0; }
	 */

	private boolean getMaxFlag(String purchaseTitle, String sicCode, CampaignCriteria campaignCriteria, String mgmtlevel,
			String minEmp, String maxEmp, String minRev, String maxRev) {
		boolean maxFlag = false;

		try {
			StringBuilder queryString = createPurchaseCriteria(null, null, sicCode, null, campaignCriteria, mgmtlevel, minEmp,
					maxEmp, minRev, maxRev);

			String previewUrl = buildUrl(queryString.toString(), 1, 25, null);
			logger.debug("\n previewUrl2 : " + previewUrl);
			URL url = new URL(previewUrl);
			dataBuyRateLimiterService.acquire("getMaxFlag");
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {

				if(inputline.contains("Authentication Failed") ) {
					//|| inputline.contains("You have exceeded your API query rate limit")
					logger.error("Error from Zoominfo: Authentication Failed: "+ inputline);
					try {
						Thread.sleep(2000);
						getMaxFlag( purchaseTitle,  sicCode, campaignCriteria,mgmtlevel,minEmp,
								maxEmp, minRev, maxRev);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}

				response.append(inputline);
			}
			bufferedReader.close();
			ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
			if (personZoomInfoResponse != null) {
				int totalResult = personZoomInfoResponse.getPersonSearchResponse().getMaxResults();
				if (totalResult > 5000) {
					maxFlag = true;
				}
			}
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}
		return maxFlag;
	}

	private void getCompanyPurchaseResponseReplenish(String purchaseTitle, CampaignCriteria campaignCriteria,
			Campaign campaign, List<String> runningCampaignIds) {

		Map<String, Integer> itrMultiprospectMap = campaignCriteria.getMultiprospectMap();

		List<PersonRecord> personRecordList = new ArrayList<PersonRecord>();
		try {
			for (Map.Entry<String, Integer> replenish : itrMultiprospectMap.entrySet()) {
				DataBuyQueue dbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
				if (dbqk != null) {
					break;
				}
				if (replenish.getValue() < 4) {
					String key = replenish.getKey();
					String companyId = campaignCriteria.getMultiCompanyIdsMap().get(key);
					// campaignCriteria.setCompanyId(comp[1]);
					int pageNumber = 1;
					URL url;
					int offset = 1;
					int size = 25;

					StringBuilder queryString = searchByCompanyId(purchaseTitle, companyId, campaignCriteria);
					for (int pageCount = 1; pageCount <= offset; pageCount++) {

						String previewUrl = buildUrl(queryString.toString(), pageNumber, getMaximumPageSize(), null);
						logger.debug("\n previewUrl1: " + previewUrl);
						url = new URL(previewUrl);
						// Thread.sleep(100);
						HttpURLConnection connection = (HttpURLConnection) url.openConnection();
						connection.setRequestMethod("GET");
						connection.setRequestProperty("User-Agent",
								"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
						// int responseCode= connection.getResponseCode();
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						String inputline;
						StringBuffer response = new StringBuffer();
						while ((inputline = bufferedReader.readLine()) != null) {
							response.append(inputline);
						}
						bufferedReader.close();
						ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
						if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
							int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();
							int totalPageCount = (int) Math.ceil((double) totalResult / size);
							offset = totalPageCount;
							replenishDetailRecord(personZoomInfoResponse, personRecordList, campaignCriteria, purchaseTitle, campaign,
									runningCampaignIds);
							pageNumber++; // increase the page number

						}
					}
				}
			}
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}
	}

	private void getDataSufficiency(ZoomBuySearchDTO zoomBuySearchDTO) {
		StringBuffer sb = new StringBuffer();
		Campaign campaign = campaignService.getCampaign(zoomBuySearchDTO.getCampaignId());
		CampaignCriteria campaignCriteria = new CampaignCriteria();
		campaignCriteria.setCampaignId(zoomBuySearchDTO.getCampaignId());
		campaignCriteria.setCheckSufficiency(true);
		campaignCriteria.setContactRequirements("5");
		QualificationCriteria cc = campaign.getQualificationCriteria();
		List<Expression> expressions = cc.getExpressions();
		campaignCriteria = setCampaignCriteria(campaignCriteria, expressions, null);
		List<String> personTitleList = new ArrayList<String>(0);
		List<String> stateList = new ArrayList<String>(0);
		List<String> industryCodeList = new ArrayList<String>(0);
		Long counter = new Long(0);

		if (campaignCriteria.getTitleKeyWord() != null) {
			personTitleList = Arrays.asList(campaignCriteria.getTitleKeyWord().split(","));
		}
		if (campaignCriteria.getState() != null) {
			stateList = Arrays.asList(campaignCriteria.getState().split(","));
		}
		if (campaignCriteria.getUsStates() != null) {
			stateList = Arrays.asList(campaignCriteria.getUsStates().split(","));
		}
		if (campaignCriteria.getZipcodes() != null) {
			stateList = Arrays.asList(campaignCriteria.getZipcodes().split(","));
		}
		if (campaignCriteria.getSicCode() != null) {
			industryCodeList = Arrays.asList(campaignCriteria.getSicCode().split(","));
		}

		DataSufficiencyResult dsr = new DataSufficiencyResult();
		dsr.setCampaignId(campaign.getId());
		dsr.setCampaignName(campaign.getName());
		dsr.setCriteria(campaign.getQualificationClause());
		dsr.setDirectDailsToSuccess(zoomBuySearchDTO.getDirectDialsToSuccess());
		dsr.setInDirectDailsToSuccess(zoomBuySearchDTO.getInDirectDialsToSuccess());
		sb.append("################       DIRECT         ##################################\n");
		dsr = getSufficiencyResponse(personTitleList, industryCodeList, stateList, campaignCriteria, dsr);
		Integer directTotal = 0;
		Integer inDirectTotal = 0;
		if (dsr.getDirectActualResult() != null) {
			for (KeyValuePair<String, Integer> directR : dsr.getDirectActualResult()) {
				if (directR.getValue() != null) {
					directTotal = directTotal + directR.getValue();
				}
			}
		}
		if (directTotal > 0 && dsr.getDirectDailsToSuccess() != null && dsr.getDirectDailsToSuccess() > 0) {
			Long directLeads = new Long(Math.round(directTotal / dsr.getDirectDailsToSuccess()));
			dsr.setLeadsFromDirect(directLeads);
		}
		// sb.append(getSufficiencyResponse(personTitleList,industryCodeList,stateList,campaignCriteria,dsr));
		campaignCriteria.setContactRequirements("2");
		sb.append("################       IN-DIRECT         ##################################\n");
		dsr = getSufficiencyResponse(personTitleList, industryCodeList, stateList, campaignCriteria, dsr);
		// sb.append(getSufficiencyResponse(personTitleList,industryCodeList,stateList,campaignCriteria,dsr));
		if (dsr.getIndirectActualResult() != null) {
			for (KeyValuePair<String, Integer> indirectR : dsr.getIndirectActualResult()) {
				if (indirectR.getValue() != null) {
					inDirectTotal = inDirectTotal + indirectR.getValue();
				}
			}
		}
		if (inDirectTotal > 0 && dsr.getInDirectDailsToSuccess() != null && dsr.getInDirectDailsToSuccess() > 0) {
			Long inDirectLeads = new Long(Math.round(inDirectTotal / dsr.getInDirectDailsToSuccess()));
			dsr.setLeadsFromInDirect(inDirectLeads);
		}
		if (dsr.getLeadsFromDirect() > 0 && dsr.getLeadsFromInDirect() > 0) {
			Long totalDeliveryLeads = dsr.getLeadsFromDirect() + dsr.getLeadsFromInDirect();
			dsr.setEstimatedDeliveryTotal(totalDeliveryLeads);
		}

		dataSufficiencyResultRepository.save(dsr);
		// return sb.toString();
	}

	private KeyValuePair<String, Integer> getKeyValuePair(String key, Integer value) {
		KeyValuePair<String, Integer> kv = new KeyValuePair<String, Integer>();
		kv.setKey(key);
		kv.setValue(value);
		return kv;
	}

	private DataSufficiencyResult getSufficiencyResponse(List<String> personTitleList, List<String> industryCodeList,
			List<String> stateList, CampaignCriteria campaignCriteria, DataSufficiencyResult dsr) {
		Long total = dsr.getTotal();
		if (total == null)
			total = new Long(0);
		Long actualtotal = dsr.getActualTotal();
		if (actualtotal == null)
			actualtotal = new Long(0);
		List<KeyValuePair<String, Integer>> directTotalResults = new ArrayList<KeyValuePair<String, Integer>>();
		List<KeyValuePair<String, Integer>> directActualResults = new ArrayList<KeyValuePair<String, Integer>>();
		List<KeyValuePair<String, Integer>> inDirectTotalResults = new ArrayList<KeyValuePair<String, Integer>>();
		List<KeyValuePair<String, Integer>> inDirectActualResults = new ArrayList<KeyValuePair<String, Integer>>();
		if (dsr.getDirectActualResult() != null && dsr.getDirectActualResult().size() > 0) {
			directActualResults = dsr.getDirectActualResult();
		}
		if (dsr.getDirectTotalResult() != null && dsr.getDirectTotalResult().size() > 0) {
			directTotalResults = dsr.getDirectTotalResult();
		}
		if (dsr.getIndirectTotalResult() != null && dsr.getIndirectTotalResult().size() > 0) {
			inDirectTotalResults = dsr.getIndirectTotalResult();
		}
		if (dsr.getIndirectActualResult() != null && dsr.getIndirectActualResult().size() > 0) {
			inDirectActualResults = dsr.getIndirectActualResult();
		}
		StringBuffer sb = new StringBuffer();
		if (personTitleList != null && personTitleList.size() > 0) {
			for (String title : personTitleList) {
				if (industryCodeList != null && industryCodeList.size() > 0) {
					for (String industryCode : industryCodeList) {
						if (stateList != null && stateList.size() > 0) {
							for (String state : stateList) {
								ZoomInfoResponse zi = getSufficiecnyResult(title, industryCode, state, campaignCriteria);
								if (zi != null) {
									sb.append(title + "-->" + industryCode + "-->" + state + " : ");
									sb.append(zi.getPersonSearchResponse().getMaxResults());
									if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
										total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
										actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
										directTotalResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state,
												zi.getPersonSearchResponse().getMaxResults()));
										directActualResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state,
												(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
									} else {
										total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
										actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
										inDirectTotalResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state,
												zi.getPersonSearchResponse().getMaxResults()));
										inDirectActualResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state,
												(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
									}
									/*
									 * if(zi.getPersonSearchResponse().getMaxResults()>0){
									 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
									 * getMaxResults()*0 .8);
									 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
									 * getMaxResults()*0.75); }
									 */
									sb.append("\n");
								} else {
									if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
										directTotalResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state, 0));
										directActualResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state, 0));
									} else {
										inDirectTotalResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state, 0));
										inDirectActualResults.add(getKeyValuePair(title + "-->" + industryCode + "-->" + state, 0));
									}
								}
							}
						} else {
							ZoomInfoResponse zi = getSufficiecnyResult(title, industryCode, null, campaignCriteria);
							if (zi != null) {
								sb.append(title + "-->" + industryCode + " : ");
								sb.append(zi.getPersonSearchResponse().getMaxResults());
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
									directTotalResults
											.add(getKeyValuePair(title + "-->" + industryCode, zi.getPersonSearchResponse().getMaxResults()));
									directActualResults.add(getKeyValuePair(title + "-->" + industryCode,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
								} else {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
									inDirectTotalResults
											.add(getKeyValuePair(title + "-->" + industryCode, zi.getPersonSearchResponse().getMaxResults()));
									inDirectActualResults.add(getKeyValuePair(title + "-->" + industryCode,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
								}
								/*
								 * if(zi.getPersonSearchResponse().getMaxResults()>0){
								 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.8);
								 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.75); }
								 */
								sb.append("\n");
							} else {
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									directTotalResults.add(getKeyValuePair(title + "-->" + industryCode, 0));
									directActualResults.add(getKeyValuePair(title + "-->" + industryCode, 0));
								} else {
									inDirectTotalResults.add(getKeyValuePair(title + "-->" + industryCode, 0));
									inDirectActualResults.add(getKeyValuePair(title + "-->" + industryCode, 0));
								}
							}
						}
					}
				} else {
					if (stateList != null && stateList.size() > 0) {
						for (String state : stateList) {
							ZoomInfoResponse zi = getSufficiecnyResult(title, null, state, campaignCriteria);
							if (zi != null) {
								sb.append(title + "-->" + state + " : ");
								sb.append(zi.getPersonSearchResponse().getMaxResults());
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
									directTotalResults
											.add(getKeyValuePair(title + "-->" + state, zi.getPersonSearchResponse().getMaxResults()));
									directActualResults.add(getKeyValuePair(title + "-->" + state,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
								} else {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
									inDirectTotalResults
											.add(getKeyValuePair(title + "-->" + state, zi.getPersonSearchResponse().getMaxResults()));
									inDirectActualResults.add(getKeyValuePair(title + "-->" + state,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
								}
								/*
								 * if(zi.getPersonSearchResponse().getMaxResults()>0){
								 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.8);
								 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.75); }
								 */
								sb.append("\n");
							} else {
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									directTotalResults.add(getKeyValuePair(title + "-->" + state, 0));
									directActualResults.add(getKeyValuePair(title + "-->" + state, 0));
								} else {
									inDirectTotalResults.add(getKeyValuePair(title + "-->" + state, 0));
									inDirectActualResults.add(getKeyValuePair(title + "-->" + state, 0));
								}
							}
						}
					} else {
						ZoomInfoResponse zi = getSufficiecnyResult(title, null, null, campaignCriteria);
						sb.append(title + "--> : ");
						if (zi != null) {
							sb.append(zi.getPersonSearchResponse().getMaxResults());
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
								directTotalResults.add(getKeyValuePair(title, zi.getPersonSearchResponse().getMaxResults()));
								directActualResults
										.add(getKeyValuePair(title, (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
							} else {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
								inDirectTotalResults.add(getKeyValuePair(title, zi.getPersonSearchResponse().getMaxResults()));
								inDirectActualResults
										.add(getKeyValuePair(title, (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
							}
							/*
							 * if(zi.getPersonSearchResponse().getMaxResults()>0){
							 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.8);
							 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.75); }
							 */
							sb.append("\n");
						} else {
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								directTotalResults.add(getKeyValuePair(title, 0));
								directActualResults.add(getKeyValuePair(title, 0));
							} else {
								inDirectTotalResults.add(getKeyValuePair(title, 0));
								inDirectActualResults.add(getKeyValuePair(title, 0));
							}
						}
					}
				}
			}
		} else {
			if (industryCodeList != null && industryCodeList.size() > 0) {
				for (String industryCode : industryCodeList) {
					if (stateList != null && stateList.size() > 0) {
						for (String state : stateList) {
							ZoomInfoResponse zi = getSufficiecnyResult(null, industryCode, state, campaignCriteria);
							if (zi != null) {
								sb.append(industryCode + "-->" + state + " : ");
								sb.append(zi.getPersonSearchResponse().getMaxResults());
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
									directTotalResults
											.add(getKeyValuePair(industryCode + "-->" + state, zi.getPersonSearchResponse().getMaxResults()));
									directActualResults.add(getKeyValuePair(industryCode + "-->" + state,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
								} else {
									total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
									actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
									inDirectTotalResults
											.add(getKeyValuePair(industryCode + "-->" + state, zi.getPersonSearchResponse().getMaxResults()));
									inDirectActualResults.add(getKeyValuePair(industryCode + "-->" + state,
											(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
								}
								/*
								 * if(zi.getPersonSearchResponse().getMaxResults()>0){
								 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.8);
								 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
								 * getMaxResults()*0.75); }
								 */
								sb.append("\n");
							} else {
								if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
									directTotalResults.add(getKeyValuePair(industryCode + "-->" + state, 0));
									directActualResults.add(getKeyValuePair(industryCode + "-->" + state, 0));
								} else {
									inDirectTotalResults.add(getKeyValuePair(industryCode + "-->" + state, 0));
									inDirectActualResults.add(getKeyValuePair(industryCode + "-->" + state, 0));
								}
							}
						}
					} else {
						ZoomInfoResponse zi = getSufficiecnyResult(null, industryCode, null, campaignCriteria);
						if (zi != null) {
							sb.append(industryCode + " : ");
							sb.append(zi.getPersonSearchResponse().getMaxResults());
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
								directTotalResults.add(getKeyValuePair(industryCode, zi.getPersonSearchResponse().getMaxResults()));
								directActualResults.add(getKeyValuePair(industryCode,
										(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
							} else {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
								inDirectTotalResults.add(getKeyValuePair(industryCode, zi.getPersonSearchResponse().getMaxResults()));
								inDirectActualResults.add(getKeyValuePair(industryCode,
										(int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
							}
							/*
							 * if(zi.getPersonSearchResponse().getMaxResults()>0){
							 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.8);
							 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.75); }
							 */
							sb.append("\n");
						} else {
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								directTotalResults.add(getKeyValuePair(industryCode, 0));
								directActualResults.add(getKeyValuePair(industryCode, 0));
							} else {
								inDirectTotalResults.add(getKeyValuePair(industryCode, 0));
								inDirectActualResults.add(getKeyValuePair(industryCode, 0));
							}
						}
					}
				}
			} else {
				if (stateList != null && stateList.size() > 0) {
					for (String state : stateList) {
						ZoomInfoResponse zi = getSufficiecnyResult(null, null, state, campaignCriteria);
						if (zi != null) {
							sb.append(state + " : ");
							sb.append(zi.getPersonSearchResponse().getMaxResults());
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
								directTotalResults.add(getKeyValuePair(state, zi.getPersonSearchResponse().getMaxResults()));
								directActualResults
										.add(getKeyValuePair(state, (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
							} else {
								total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
								actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
								inDirectTotalResults.add(getKeyValuePair(state, zi.getPersonSearchResponse().getMaxResults()));
								inDirectActualResults
										.add(getKeyValuePair(state, (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
							}
							/*
							 * if(zi.getPersonSearchResponse().getMaxResults()>0){
							 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.8);
							 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
							 * getMaxResults()*0.75); }
							 */
							sb.append("\n");
						} else {
							if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
								directTotalResults.add(getKeyValuePair(state, 0));
								directActualResults.add(getKeyValuePair(state, 0));
							} else {
								inDirectTotalResults.add(getKeyValuePair(state, 0));
								inDirectActualResults.add(getKeyValuePair(state, 0));
							}
						}
					}
				} else {
					ZoomInfoResponse zi = getSufficiecnyResult(null, null, null, campaignCriteria);
					if (zi != null) {
						sb.append("--> : ");
						sb.append(zi.getPersonSearchResponse().getMaxResults());
						if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
							total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
							actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6);
							directTotalResults.add(getKeyValuePair("-->", zi.getPersonSearchResponse().getMaxResults()));
							directActualResults
									.add(getKeyValuePair("-->", (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.6)));
						} else {
							total = total + Math.round(zi.getPersonSearchResponse().getMaxResults());
							actualtotal = actualtotal + Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4);
							inDirectTotalResults.add(getKeyValuePair("-->", zi.getPersonSearchResponse().getMaxResults()));
							inDirectActualResults
									.add(getKeyValuePair("-->", (int) Math.round(zi.getPersonSearchResponse().getMaxResults() * 0.4)));
						}
						/*
						 * if(zi.getPersonSearchResponse().getMaxResults()>0){
						 * sb.append("-->(-10% for Direct) : "+zi.getPersonSearchResponse().
						 * getMaxResults()*0.8);
						 * sb.append("-->(-25% for InDirect) : "+zi.getPersonSearchResponse().
						 * getMaxResults()*0.75); }
						 */
						sb.append("\n");
					} else {
						if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
							directTotalResults.add(getKeyValuePair("-->", 0));
							directActualResults.add(getKeyValuePair("-->", 0));
						} else {
							inDirectTotalResults.add(getKeyValuePair("-->", 0));
							inDirectActualResults.add(getKeyValuePair("-->", 0));
						}
					}
				}
			}
		}

		dsr.setDirectActualResult(directActualResults);
		dsr.setIndirectActualResult(inDirectActualResults);
		dsr.setDirectTotalResult(directTotalResults);
		dsr.setIndirectTotalResult(inDirectTotalResults);
		dsr.setTotal(total);
		dsr.setActualTotal(actualtotal);
		return dsr;
	}

	private ZoomInfoResponse getSufficiecnyResult(String purchaseTitle, String sicCode, String state,
			CampaignCriteria campaignCriteria) {
		//////////////////
		int pageNumber = 1;
		URL url;
		int offset = 1;
		int size = 25;
		ZoomInfoResponse personZoomInfoResponse = new ZoomInfoResponse();
		try {

			StringBuilder queryString = createPurchaseCriteria(null, null, sicCode, state, campaignCriteria, null, null, null,
					null, null);
			String previewUrl = buildUrl(queryString.toString(), pageNumber, getMaximumPageSize(), null);
			logger.debug("\n previewUrl1: " + previewUrl);
			url = new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				if(inputline.contains("Authentication Failed") ) {
					//|| inputline.contains("You have exceeded your API query rate limit")
					logger.error("Error from Zoominfo: Authentication Failed: "+ inputline);
					try {
						Thread.sleep(2000);
						getSufficiecnyResult( purchaseTitle, sicCode, state, campaignCriteria);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				response.append(inputline);
			}
			bufferedReader.close();
			personZoomInfoResponse = getResponse(response, false);
			/*
			 * if(personZoomInfoResponse.getPersonSearchResponse() != null){ int totalResult
			 * = personZoomInfoResponse.getPersonSearchResponse().getMaxResults(); //int
			 * totalPageCount = (int) Math.ceil((double)totalResult/size);
			 * 
			 * }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		///////////////
		return personZoomInfoResponse;
	}

	private void writeToFile(String campaignId, String purchasetitle, String siccode, String state, int pageCount) {
		try {
			// input the file content to the StringBuffer "input"
			BufferedReader file = new BufferedReader(new FileReader("buyingDetails_DO_NOT_DELETE.txt"));
			StringBuffer inputBuffer = new StringBuffer();
			String line;
			String inputStrupdated = "";
			boolean found = false;
			String inputStr = inputBuffer.toString();
			while ((line = file.readLine()) != null) {
				inputBuffer.append(line);
				inputStr = inputBuffer.toString();
				if (inputStr.contains(campaignId)) {
					/*
					 * inputStr.replace(inputStr,
					 * "campaignid:"+campaignId+"|"+"purchasetitle:"+purchasetitle+"|"
					 * +"siccode:"+siccode+"|"+"state:"+state+"|"+"pageCount:"+pageCount);
					 */
					found = true;
					// break;
				}
			}
			file.close();
			// String inputStr = inputBuffer.toString();
			if (!found) {
				inputStrupdated = "campaignid:" + campaignId + "|" + "purchasetitle:" + purchasetitle + "|" + "siccode:"
						+ siccode + "|" + "state:" + state + "|" + "pageCount:" + pageCount + "\n";
			} else {
				inputStrupdated = "campaignid:" + campaignId + "|" + "purchasetitle:" + purchasetitle + "|" + "siccode:"
						+ siccode + "|" + "state:" + state + "|" + "pageCount:" + pageCount;
			}

			logger.debug(inputStrupdated); // display the original file for debugging

			// // logic to replace lines in the string (could use regex here to be generic)
			// if (type.equals("0")) {
			// inputStr = inputStr.replace(replaceWith + "1", replaceWith + "0");
			// } else if (type.equals("1")) {
			// inputStr = inputStr.replace(replaceWith + "0", replaceWith + "1");
			// }

			// display the new file for debugging
			logger.debug("----------------------------------\n" + inputStrupdated);

			// write the new string with the replaced line OVER the same file
			FileOutputStream fileOut = new FileOutputStream("buyingDetails_DO_NOT_DELETE.txt");
			fileOut.write(inputStrupdated.getBytes());
			fileOut.close();

		} catch (Exception e) {
			logger.debug("Problem reading file.");
		}
	}

	private void getPurchaseResponse(String abmId, String purchaseTitle, String sicCode, String state,
			CampaignCriteria campaignCriteria, Campaign campaign, List<String> runningCampaignIds, String mgmtlevel,
			String searchMinEmp, String searchMaxEmp, String searchMinRev, String searchMaxRev) {

		int pageNumber = 1;
		URL url;
		int offset = 1;
		int size = 25;

		DataBuyQueue searchdbqk = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
				DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
		if (searchdbqk!= null && searchdbqk.isInterupted()) {
			if (searchdbqk.getSearchPage() > 0) {
				pageNumber = searchdbqk.getSearchPage();
				offset = searchdbqk.getSearchPage();
			}
			searchdbqk.setInterupted(false);
			dataBuyQueueRepository.save(searchdbqk);
		}

		List<PersonRecord> personRecordList = new ArrayList<PersonRecord>();
		try {
			StringBuilder queryString = createPurchaseCriteria(abmId, null, sicCode, state, campaignCriteria, mgmtlevel,
					searchMinEmp, searchMaxEmp, searchMinRev, searchMaxRev);
			for (int pageCount = 1; pageCount <= offset; pageCount++) {
				if (pageCount < pageNumber) {
					continue;
				}
				DataBuyQueue dbq1 = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.KILL.toString(), campaignCriteria.getRequestJobDate());
				if (dbq1 != null) {
					break;
				}
				String previewUrl = buildUrl(queryString.toString(), pageNumber, getMaximumPageSize(), null);
				logger.debug("\n previewUrl1: " + previewUrl);
				url = new URL(previewUrl);
				// Thread.sleep(100);
				// if(!campaignCriteria.isApiCheck()){
				DataBuyQueue dbq = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaign.getId(),
						DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
				if (dbq != null) {
					dbq.setSearchResponseSample(previewUrl);
					dbq.setAbmId(abmId);
					dbq.setSearchPage(pageNumber);
					dbq.setSearchState(state);
					dbq.setSicCode(sicCode);
					dbq.setSearchManagementlevel(mgmtlevel);
					dbq.setSearchEmployeeMin(searchMinEmp);
					dbq.setSearchEmployeeMax(searchMaxEmp);
					dbq.setSearchRevenueMin(searchMinRev);
					dbq.setSearchRevenueMax(searchMaxRev);

					if (dbq.getZoomInfoPurchasedCount() != null && dbq.getZoomInfoPurchasedCount() > 0 && buyRowCount == 0) {
						buyRowCount = dbq.getZoomInfoPurchasedCount();
					}
					dbq.setTotalBought(buyRowCount);
					dataBuyQueueRepository.save(dbq);
				}
				campaignCriteria.setApiCheck(true);
				// }
				dataBuyRateLimiterService.acquire("getPurchaseResponse");
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent",
						"Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
				// int responseCode= connection.getResponseCode();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputline;
				StringBuffer response = new StringBuffer();
				while ((inputline = bufferedReader.readLine()) != null) {
					if(inputline.contains("Authentication Failed") ) {
						//|| inputline.contains("You have exceeded your API query rate limit")
						logger.error("Error from Zoominfo: Authentication Failed: "+ inputline);
						try {
							Thread.sleep(2000);
							getPurchaseResponse( abmId, purchaseTitle, sicCode, state, campaignCriteria,
									campaign, runningCampaignIds, mgmtlevel, searchMinEmp, searchMaxEmp, searchMinRev,
									searchMaxRev);
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					response.append(inputline);
				}
				bufferedReader.close();
				ZoomInfoResponse personZoomInfoResponse = getResponse(response, false);
				if (personZoomInfoResponse != null && personZoomInfoResponse.getPersonSearchResponse() != null) {
					int totalResult = personZoomInfoResponse.getPersonSearchResponse().getTotalResults();
					int totalPageCount = (int) Math.ceil((double) totalResult / size);
					offset = totalPageCount;
					// Long totalPurchaseReq = campaignCriteria.getTotalPurchaseReq();

					purchaseDetailRecord(personZoomInfoResponse, personRecordList, campaignCriteria, purchaseTitle, campaign,
							runningCampaignIds);
					if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
						break;
					}
					if (campaignCriteria.isSkipAbmCompany()) {
						campaignCriteria.setSkipAbmCompany(false);
						break;
					}

					pageNumber++; // increase the page number

				} else {
					logger.debug(
							"Search Response is null for CampaignID###" + campaign.getId() + "###at Page Number ####" + pageCount);
				}
			}
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if(dbqe != null){
				if (dbqe.getPurchasedCount() != null) {
					dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
					dbqe.setZoomInfoPurchasedCount(buyRowCount);
				} else {
					dbqe.setPurchasedCount(buyRowCount);
					dbqe.setZoomInfoPurchasedCount(buyRowCount);
				}
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionAsString = sw.toString();
				// logger.debug(exceptionAsString);
				dbqe.setErrorMsg(exceptionAsString);
				// dbqe.setJobEndTime(new Date());
				if (dbqe.getJobretry() >= 3) {
					dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
					dbqe.setJobEndTime(new Date());
				} else {
					dbqe.setJobretry(dbqe.getJobretry() + 1);
					dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
					dbqe.setInterupted(true);
				}
				dataBuyQueueRepository.save(dbqe);
			}
			e.printStackTrace();
		}
	}

	private void purchaseDetailRecord(ZoomInfoResponse zoomInfoResponse, List<PersonRecord> personRecordList,
			CampaignCriteria campaignCriteria, String purchaseTitle, Campaign campaign, List<String> runningCampaignIds) {

		personRecordList = new ArrayList<PersonRecord>();
		List<PersonRecord> personRecordListForPurchased = getPersonRecordList(zoomInfoResponse, personRecordList,
				campaignCriteria, purchaseTitle, campaign, runningCampaignIds);
		if (personRecordListForPurchased.size() == 0) {
			return;
		}

		for (int i = 0; i < personRecordListForPurchased.size(); i++) {
			PersonRecord personRecord = personRecordListForPurchased.get(i);
			String personId = personRecord.getPersonId();
			String title = personRecord.getCurrentEmployment().getJobTitle();
			String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
			String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
			String empRange = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange();
			long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();
			long revCountInZeros = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueIn000s();
			String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
			Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);

			if(campaign != null && !campaign.isABM() && !campaign.isMaxEmployeeAllow() && revenueEmployeeList != null && revenueEmployeeList.size() > 0
					&& revenueEmployeeList.get("maxEmployeeCount") != null
					&& (revenueEmployeeList.get("maxEmployeeCount").longValue() >= 10000 || revenueEmployeeList.get("maxEmployeeCount").longValue() == 0)){
				continue;
			}

			// int totalResult =
			// zoomInfoResponse.getPersonSearchResponse().getTotalResults();
			if (buyRowCount <= campaignCriteria.getTotalPurchaseReq()) {
				try {
					logger.debug("PersonId = " + personId);

					personIdList.add(personId);

					String personDetailRequestUrl = buildUrl(null, 0, 0, personId);
					// logger.debug("Person Detail Request " + personDetailRequestUrl);
					URL url = new URL(personDetailRequestUrl); // purchase request
					ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(url);
					if (zoomInfoResponseDetail != null && zoomInfoResponseDetail.getPersonDetailResponse() != null
							&& zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment().get(0)
									.getManagementLevel() != null) {
						// logger.debug("########## Entering rowCount #############=="+buyRowCount);

						createData(zoomInfoResponseDetail, title, revRange, empRange, revCount, revCountInZeros, empCount, domain,
								campaignCriteria, purchaseTitle, campaign, runningCampaignIds);
					}
				} catch (Exception e) {
					DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(
							campaignCriteria.getCampaignId(), DataBuyQueueStatus.INPROCESS.toString(),
							campaignCriteria.getRequestJobDate());
					if (dbqe.getPurchasedCount() != null) {
						dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
						dbqe.setZoomInfoPurchasedCount(buyRowCount);
					} else {
						dbqe.setPurchasedCount(buyRowCount);
						dbqe.setZoomInfoPurchasedCount(buyRowCount);
					}
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					String exceptionAsString = sw.toString();
					// logger.debug(exceptionAsString);
					dbqe.setErrorMsg(exceptionAsString);
					// dbqe.setJobEndTime(new Date());
					if (dbqe.getJobretry() >= 3) {
						dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
						dbqe.setJobEndTime(new Date());
					} else {
						dbqe.setJobretry(dbqe.getJobretry() + 1);
						dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
						dbqe.setInterupted(true);
					}
					dataBuyQueueRepository.save(dbqe);
					e.printStackTrace();
				}
			} else if (buyRowCount >= campaignCriteria.getTotalPurchaseReq()) {
				break;
			} else {
				logger.debug("PersonId is null");
			}
			// }
		}

	}

	private void replenishDetailRecord(ZoomInfoResponse zoomInfoResponse, List<PersonRecord> personRecordList,
			CampaignCriteria campaignCriteria, String purchaseTitle, Campaign campaign, List<String> runningCampaignIds) {

		personRecordList = new ArrayList<PersonRecord>();
		List<PersonRecord> personRecordListForPurchased = getPersonRecordList(zoomInfoResponse, personRecordList,
				campaignCriteria, purchaseTitle, campaign, runningCampaignIds);
		if (personRecordListForPurchased.size() == 0) {
			return;
		}

		for (int i = 0; i < personRecordListForPurchased.size(); i++) {
			PersonRecord personRecord = personRecordListForPurchased.get(i);
			String personId = personRecord.getPersonId();
			String title = personRecord.getCurrentEmployment().getJobTitle();
			String domain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
			String revRange = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange();
			String empRange = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange();
			String revCount = personRecord.getCurrentEmployment().getCompany().getCompanyRevenue();
			long revCountInZeros = personRecord.getCurrentEmployment().getCompany().getCompanyRevenueIn000s();
			long empCount = personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount();

			// String direct = arr[2];

			// int totalResult =
			// zoomInfoResponse.getPersonSearchResponse().getTotalResults();
			// if(count < 4){
			try {
				logger.debug("PersonId = " + personId);

				personIdList.add(personId);
				// if(!personId.equalsIgnoreCase("-2131681364")){

				String personDetailRequestUrl = buildUrl(null, 0, 0, personId);
				// logger.debug("Person Detail Request " + personDetailRequestUrl);
				URL url = new URL(personDetailRequestUrl); // purchase request
				ZoomInfoResponse zoomInfoResponseDetail = getZoomInfoResponse(url);
				if (zoomInfoResponseDetail != null) {
					PersonDetailResponse personDetailResponse = zoomInfoResponseDetail.getPersonDetailResponse();
					String ph = getPhone(personDetailResponse);
					if (campaignCriteria.getMultiprospectMap().containsKey(ph)) {
						Integer c = campaignCriteria.getMultiprospectMap().get(ph);
						if (c == null)
							c = 0;
						if (c > 4)
							break;
					}
					if (zoomInfoResponseDetail != null) {
						logger.debug("########## Entering rowCount #############==" + buyRowCount);
						createData(zoomInfoResponseDetail, title, revRange, empRange, revCount, revCountInZeros, empCount, domain,
								campaignCriteria, purchaseTitle, campaign, runningCampaignIds);
					}
				}
			} catch (Exception e) {
				DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
						DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
				if (dbqe.getPurchasedCount() != null) {
					dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
					dbqe.setZoomInfoPurchasedCount(buyRowCount);
				} else {
					dbqe.setPurchasedCount(buyRowCount);
					dbqe.setZoomInfoPurchasedCount(buyRowCount);
				}
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionAsString = sw.toString();
				// logger.debug(exceptionAsString);
				dbqe.setErrorMsg(exceptionAsString);
				// dbqe.setJobEndTime(new Date());
				if (dbqe.getJobretry() >= 3) {
					dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
					dbqe.setJobEndTime(new Date());
				} else {
					dbqe.setJobretry(dbqe.getJobretry() + 1);
					dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
					dbqe.setInterupted(true);
				}
				dataBuyQueueRepository.save(dbqe);
				e.printStackTrace();
			}
			// }
		}

	}

	private void createData(ZoomInfoResponse zoomInfoResponseDetail, String title, String revRange, String empRange,
			String revCount, long revCountInZeros, long empCount, String domain, CampaignCriteria campaignCriteria,
			String purchaseTitle, Campaign campaign, List<String> runningCampaignIds) {

		List<ProspectCallLog> prospectCallLogs = new ArrayList<ProspectCallLog>();
		List<ReviewDataBought> revewlogs = new ArrayList<ReviewDataBought>();
		List<ProspectCallLog> pProspectCallLogs = new ArrayList<ProspectCallLog>();
		List<ProspectCallInteraction> prospectCallInteractions = new ArrayList<ProspectCallInteraction>();
		List<PCIAnalysis> pcianList = new ArrayList<PCIAnalysis>();
		Set<CampaignContact> campaignContactList = new HashSet<CampaignContact>();
		List<ContactFunction> functions = new ArrayList<ContactFunction>();
		List<ContactIndustry> industries = new ArrayList<ContactIndustry>();
		List<String> topLevelIndustries = new ArrayList<String>();
		Map<String, String> companyAddress = new HashMap<String, String>();
		String zoomCountry = "United States";

		try {
			// logger.debug("---------------------->"+sheet.getSheetName());
			ProspectCallLog pcLog = new ProspectCallLog();

			Prospect prospect = new Prospect();
			StringBuffer sb = new StringBuffer();
			String emp = "";
			String mgmt = "";
			String tz = "";
			Map<String, Long> revenueEmployeeList = findCompanyData(revRange, empRange);
			PersonDetailResponse personDetailResponse = zoomInfoResponseDetail.getPersonDetailResponse();
			if (zoomInfoResponseDetail.getPersonDetailResponse() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getFirstName() != null
					&& zoomInfoResponseDetail.getPersonDetailResponse().getLastName() != null
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getFirstName())
					&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getLastName())
					&& (zoomInfoResponseDetail.getPersonDetailResponse().getDirectPhone() != null
							|| zoomInfoResponseDetail.getPersonDetailResponse().getCompanyPhone() != null)) {
				// rowCount = rowCount+1;
				List<String> industryList = zoomInfoResponseDetail.getPersonDetailResponse().getIndustry();
				int industryListSize = 0;
				if (industryList != null) {
					industryListSize = industryList.size();
				}

				// populating prospect.
				prospect.setFirstName(personDetailResponse.getFirstName());
				prospect.setLastName(personDetailResponse.getLastName());
				prospect.setTitle(title);
				prospect.setDepartment(getJobFunction(zoomInfoResponseDetail));
				prospect.setInputDepartment(getInputDepartment(campaignCriteria, prospect.getDepartment()));
				if(zoomInfoResponseDetail.getPersonDetailResponse().getEmailAddress() != null
						&& !"".equals(zoomInfoResponseDetail.getPersonDetailResponse().getEmailAddress())){
					prospect.setEmail(personDetailResponse.getEmailAddress());
				}

				prospect.setCompany(getOrganizationName(zoomInfoResponseDetail));
				prospect.setEncrypted(campaign.isEncrypted());
				if (personDetailResponse.getLastUpdatedDate() != null && !personDetailResponse.getLastUpdatedDate().isEmpty()) {
					String ld = personDetailResponse.getLastUpdatedDate();
					if (ld.contains("-"))
						ld = ld.replace("-", "/");
					Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);

					prospect.setLastUpdatedDate(date1);
				}

				if (personDetailResponse.getLocalAddress() != null) {
					prospect.setCountry(personDetailResponse.getLocalAddress().getCountry());
					zoomCountry = personDetailResponse.getLocalAddress().getCountry();
				} else if (personDetailResponse.getCurrentEmployment() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
								.getCountry() != null) {
					prospect.setCountry(
							personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry());
					zoomCountry = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
							.getCountry();
				}
				if (prospect.getCountry().equalsIgnoreCase("USA") || prospect.getCountry().equalsIgnoreCase("United States")
						|| prospect.getCountry().equalsIgnoreCase("US") || prospect.getCountry().equalsIgnoreCase("Canada")) {
					prospect.setPhone(getUSPhone(personDetailResponse));
					prospect.setExtension(getExtension(personDetailResponse));
					String phoneType = phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					prospect.setPhoneType(phoneType);
					prospect.setCompanyPhone(getUSCompanyPhone(personDetailResponse));
					prospect.setStateCode(getStateCode(personDetailResponse));
				} else if (prospect.getCountry().equalsIgnoreCase("United Kingdom")
						|| prospect.getCountry().equalsIgnoreCase("UK")) {
					prospect.setPhone(getUKPhone(personDetailResponse));
					prospect.setExtension(getExtension(personDetailResponse));
					String phoneType = phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					prospect.setPhoneType(phoneType);
					prospect.setCompanyPhone(getUKCompanyPhone(personDetailResponse));
					prospect.setStateCode(getUKStateCode(personDetailResponse));
				} else {
					prospect.setPhone(getPhone(personDetailResponse));
					prospect.setExtension(getExtension(personDetailResponse));
					String phoneType = phoneDirectAndIndiretServiceImpl.tagDirectIndirectPhone(prospect.getPhone());
					prospect.setPhoneType(phoneType);
					prospect.setCompanyPhone(getCompanyPhone(personDetailResponse));
					prospect.setStateCode(getStateCode(personDetailResponse));
				}

				if (industryListSize > 0) {
					prospect.setIndustry(industryList.get(0));
					prospect.setIndustryList(industryList);
				}
				prospect.setEu(personDetailResponse.isEU());
				prospect.setSourceType("INTERNAL");
				Map<String, Object> customAttributes = new HashMap<String, Object>();

				// if(revenueEmployeeList.get(1)>0 &&
				// revenueEmployeeList.get(0)>revenueEmployeeList.get(1)){
				customAttributes.put("minRevenue", revenueEmployeeList.get("minRevenue"));
				customAttributes.put("maxRevenue", revenueEmployeeList.get("maxRevenue"));
				/*
				 * }else{ customAttributes.put("minRevenue", revenueEmployeeList.get(0));
				 * customAttributes.put("maxRevenue", revenueEmployeeList.get(1)); }
				 */
				// if(revenueEmployeeList.get(3)>0 &&
				// revenueEmployeeList.get(2)>revenueEmployeeList.get(3)){
				customAttributes.put("maxEmployeeCount", revenueEmployeeList.get("maxEmployeeCount"));
				customAttributes.put("minEmployeeCount", revenueEmployeeList.get("minEmployeeCount"));
				/*
				 * }else{ customAttributes.put("maxEmployeeCount", revenueEmployeeList.get(2));
				 * customAttributes.put("minEmployeeCount", revenueEmployeeList.get(3)); }
				 */
				String detailDomain = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyWebsite();
				/*
				 * if(detailDomain==null || detailDomain.isEmpty()){ detailDomain = domain; }
				 */
				customAttributes.put("domain", detailDomain);
				customAttributes.put("revCount", revCount);
				customAttributes.put("revCountIn000s", new Long(revCountInZeros));
				customAttributes.put("empCount", new Long(empCount));
				prospect.setCustomAttributes(customAttributes);

				/*
				 * prospect.setCustomAttribute("minRevenue",
				 * Double.parseDouble(revenueEmployeeList.get(0).toString()));
				 * prospect.setCustomAttribute("maxRevenue",
				 * Double.parseDouble(revenueEmployeeList.get(1).toString()));
				 * prospect.setCustomAttribute("minEmployeeCount",
				 * Double.parseDouble(revenueEmployeeList.get(2).toString()));
				 * prospect.setCustomAttribute("maxEmployeeCount",
				 * Double.parseDouble(revenueEmployeeList.get(3).toString()));
				 */
				/*
				 * prospect.setCustomAttribute("revCount", new Long(revCount));
				 * prospect.setCustomAttribute("revCountIn000s",
				 * Long.toString(revCountInZeros)); prospect.setCustomAttribute("empCount",
				 * Long.toString(empCount));
				 */
				prospect.setSource("ZOOINFO");
				prospect.setDataSource("Xtaas Data Source");
				prospect.setSourceId(personDetailResponse.getPersonId());
				prospect.setSourceCompanyId(getOrganizationId(zoomInfoResponseDetail));
				if (personDetailResponse.getCurrentEmployment().get(0).getManagementLevel() != null)
					prospect.setManagementLevel(personDetailResponse.getCurrentEmployment().get(0).getManagementLevel().get(0));

				if (prospect.getSourceCompanyId() != null && !prospect.getSourceCompanyId().isEmpty()) {
					AbmListDetail abmListDetail = abmListDetailRepository
							.findOneByCompanyId(prospect.getSourceCompanyId());
					//List<AbmListDetail> abmdetailList = abmListDetailRepositoryImpl
						//	.getSicAndNaicsList(prospect.getSourceCompanyId());
					if (abmListDetail != null) {
						List<String> naicsList = new ArrayList<String>(1);
						List<String> sicList = new ArrayList<String>(1);

						if (abmListDetail.getCompanyNAICS() != null) {
							naicsList = abmListDetail.getCompanyNAICS();
						}
						if (abmListDetail.getCompanySIC() != null) {
							sicList = abmListDetail.getCompanySIC();
						}
						prospect.setCompanyNAICS(naicsList);
						prospect.setCompanySIC(sicList);

					}
				}

				if (personDetailResponse.getLocalAddress() != null
						&& personDetailResponse.getLocalAddress().getCountry() != null) {
					if (personDetailResponse.getLocalAddress().getStreet() != null)
						prospect.setAddressLine1(personDetailResponse.getLocalAddress().getStreet());
					else
						prospect.setAddressLine1("");
					if (personDetailResponse.getLocalAddress().getZip() != null)
						prospect.setZipCode(personDetailResponse.getLocalAddress().getZip());
					else
						prospect.setZipCode("");
					if (personDetailResponse.getLocalAddress().getCity() != null)
						prospect.setCity(personDetailResponse.getLocalAddress().getCity());
					else
						prospect.setCity("");
					// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
					// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
				} else if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
						&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress()
								.getCountry() != null) {
					prospect.setAddressLine1(
							personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getStreet());
					prospect
							.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
					prospect
							.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
				}
				// prospect.setCustomAttribute("domain",
				// personDetailResponse.getCurrentEmployment().get(0).getCompany().getWebsite());
				prospect.setSuffix(purchaseTitle);
				if (prospect.getPhone()!=null && prospect.getCompanyPhone()!=null && prospect.getPhone().equalsIgnoreCase(prospect.getCompanyPhone())) {
					prospect.setDirectPhone(false);
				} else {
					prospect.setDirectPhone(true);
				}

			}

			ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospect);
			if (campaign.getType().equals(CampaignTypes.LeadQualification) || campaign.getType().equals(CampaignTypes.TeleMailCampaign))// TODO this needs to be changed to type email once
																																			// fixed
				prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospect, true);
			sb.append(ProspectCallStatus.QUEUED);
			sb.append("|");
			if (prospect.getCustomAttributeValue("minEmployeeCount") != null) {
				String minEmpStr = prospect.getCustomAttributeValue("minEmployeeCount").toString();
				Double n1 = Double.parseDouble(minEmpStr);
				sb.append(XtaasUtils.getBoundry(n1, "min"));
				sb.append("-");
				emp = XtaasUtils.getEmployeeWeight(n1);
			}

			if (prospect.getCustomAttributeValue("maxEmployeeCount") != null) {
				String maxEmpStr = prospect.getCustomAttributeValue("maxEmployeeCount").toString();
				Double n2 = Double.parseDouble(maxEmpStr);
				sb.append(XtaasUtils.getBoundry(n2, "max"));
			}
			sb.append("|");
			sb.append(XtaasUtils.getMgmtLevel(prospect.getManagementLevel()));
			mgmt = XtaasUtils.getMgmtLevelWeight(prospect.getManagementLevel());

			int qbSort = 0;
			String timez = getTimeZone(prospect);
			prospect.setTimeZone(timez);
			tz = XtaasUtils.getStateWeight(timez);
			String direct = "2";
			if (prospect.isDirectPhone()) {
				direct = "1";
			}
			if (!emp.isEmpty() && !direct.isEmpty() && !mgmt.isEmpty() && !tz.isEmpty()) {
				String sortString = direct + tz + mgmt + emp;
				qbSort = Integer.parseInt(sortString);
				String slice = "slice10";
				Long employee = new Long(emp);
				Long management = new Long(mgmt);
				if (direct.equalsIgnoreCase("1") && employee <= 8 && management <= 3) {
					slice = "slice1";
				} else if (direct.equalsIgnoreCase("1") && employee <= 10 && management <= 3) {
					slice = "slice2";
				} else if (direct.equalsIgnoreCase("1") && employee > 10 && management <= 3) {
					slice = "slice3";
				} else if (direct.equalsIgnoreCase("1") && employee <= 8 && management >= 4) {
					slice = "slice4";
				} else if (direct.equalsIgnoreCase("1") && employee <= 10 && management >= 4) {
					slice = "slice5";
				} else if (direct.equalsIgnoreCase("1") && employee > 10 && management >= 4) {
					slice = "slice6";
				}

				pcLog.setDataSlice(slice);
			}

			/////////////////////////////////////////////////////////////////// ]]

			prospectCall.setQualityBucket(sb.toString());
			prospectCall.setQualityBucketSort(qbSort);
			prospectCall.getProspect()
					.setZoomCompanyUrl(personDetailResponse.getCurrentEmployment().get(0).getCompany().getZoomCompanyUrl());
			prospectCall.getProspect().setZoomPersonUrl(personDetailResponse.getZoomPersonUrl());
			prospectCall.setProspect(prospect);

			prospectCall.setOrganizationId(campaignCriteria.getOrganizationId());
			pcLog.setProspectCall(prospectCall);
			if (prospectCall.getProspect().getCountry() == null)
				prospectCall.getProspect().setCountry(zoomCountry);
			if (prospectCall.getProspect().getCountry() != null
					&& prospectCall.getProspect().getCountry().equalsIgnoreCase("United Kingdom")
					|| prospectCall.getProspect().getCountry().equalsIgnoreCase("UK")) {
				pcLog = massageUKPhone(pcLog);
			} else {
				pcLog.setStatus(ProspectCallStatus.QUEUED);
			}

			ProspectCallInteraction prospectCallInteraction = new ProspectCallInteraction();
			prospectCallInteraction.setStatus(ProspectCallStatus.QUEUED);
			prospectCallInteraction.setProspectCall(prospectCall);
			// if(campaignCriteria.isParallelBuy()){
			pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			if (pcLog.getProspectCall().getCallRetryCount() < 10)
				pcLog.getProspectCall().setCallRetryCount(786);

			prospectCallInteraction.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			if (prospectCallInteraction.getProspectCall().getCallRetryCount() < 10)
				prospectCallInteraction.getProspectCall().setCallRetryCount(786);

			// }

			if (prospect.getIndustryList() != null && prospect.getIndustryList().size() > 0) {
				for (String industry : prospect.getIndustryList()) {
					ContactIndustry contactIndustry = new ContactIndustry();
					contactIndustry.setName(XtaasUtils.removeNonAsciiChars(industry));
					industries.add(contactIndustry);
				}
			}

			try {
				StringBuilder reason = new StringBuilder();
				boolean deactivateFlag = false;
				boolean counterIncrementer = true;
				if (prospect.getFirstName() == null || prospect.getFirstName().isEmpty()) {
					prospect.setFirstName("a");
					 reason.append("FirstName is Empty|");
					deactivateFlag = true;
				}

				if (prospect.getLastName() == null || prospect.getLastName().isEmpty()) {
					prospect.setLastName("b");
					reason.append("LastName is Empty|");
					deactivateFlag = true;
				}
				int prospectVer = getprospectVersion(prospect.getFirstName(), prospect.getLastName(), prospect.getCompany(),
						prospect.getTitle(), runningCampaignIds);
				prospectCall.setProspectVersion(prospectVer);

				if (prospect.getCompany() == null || prospect.getCompany().isEmpty()) {
					prospect.setLastName("c");
					reason.append("Company is Empty|");
					deactivateFlag = true;
				}
				if (prospect.getPhone() == null || prospect.getPhone().isEmpty()) {
					prospect.setPhone("0");
					reason.append("Phone is Empty|");
					deactivateFlag = true;
				}
				if(campaign.isRemoveMobilePhones()  && prospect.getPhoneType()  != null && prospect.getPhoneType().equalsIgnoreCase("mobile")) {
					reason.append("Number is mobile|");
					deactivateFlag = true;
				}
				if (prospect.getStateCode() == null || prospect.getStateCode().isEmpty()) {
					prospect.setStateCode("NA");
					reason.append("StateCode Not Found|");
					deactivateFlag = true;
				}
				if (prospect.getDepartment() == null || prospect.getDepartment().isEmpty() || prospect.getDepartment().equalsIgnoreCase("NA")) {
					//prospect.setStateCode("NA");
					reason.append("Department is Empty");
					deactivateFlag = true;
				}
				CampaignContact tempCampaignContact = new CampaignContact(campaign.getId(), prospect.getSource(),
						prospect.getSourceId(), prospect.getPrefix(), prospect.getFirstName(), prospect.getLastName(),
						prospect.getSuffix(), prospect.getTitle(), prospect.getDepartment(), functions, prospect.getCompany(),
						personDetailResponse.getCurrentEmployment().get(0).getCompany().getWebsite(), industries,
						topLevelIndustries, revenueEmployeeList.get("minRevenue"), revenueEmployeeList.get("maxRevenue"),
						revenueEmployeeList.get("minEmployeeCount"), revenueEmployeeList.get("minEmployeeCount"),
						prospect.getEmail(), prospect.getAddressLine1(), prospect.getAddressLine2(), prospect.getCity(),
						prospect.getStateCode(), prospect.getCountry(), prospect.getZipCode(), prospect.getPhone(),
						prospect.getCompanyPhone(), prospect.getSourceCompanyId(), prospect.getManagementLevel(), companyAddress,
						campaign.isEncrypted());

				campaignContactList.add(tempCampaignContact);
				if (campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty()) {
					List<String> industryCodeList = Arrays.asList(campaignCriteria.getSicCode().split(","));
					deactivateFlag = isSICorNAICS(industryCodeList, prospect.getCompanySIC(), prospect.getCompanyNAICS());
					if(deactivateFlag) {
						reason.append("SIC not found, as this is SIC Campaign|");
					}
				}
				if (campaignCriteria.getZipcodes() != null && !campaignCriteria.getZipcodes().isEmpty()) {
					List<String> zipCodeList = Arrays.asList(campaignCriteria.getZipcodes().split(","));
					deactivateFlag = isZipCodeValid(zipCodeList, prospect.getZipCode());
					reason.append("ZIP not found, as this is Zip Campaign|");
				}

				if (deactivateFlag) {
					pcLog.getProspectCall().setCallRetryCount(144);
					pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					counterIncrementer = false;
				}
				// isEmailSuppressed(pcLog,campaignCriteria)
				if (isEmailSuppressed(pcLog, campaignCriteria, campaign)) { // check if person is already bought by name and company
					pcLog.getProspectCall().setCallRetryCount(144);
					pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					counterIncrementer = false;
					reason.append("Email is in suppression List|");
				}
				if (isCompanySuppressedDetail(pcLog, campaignCriteria)) {
					pcLog.getProspectCall().setCallRetryCount(144);
					pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					counterIncrementer = false;
					reason.append("Company is in suppression List|");
				}

				if (formatPhone(pcLog.getProspectCall().getProspect().getPhone())) {
					pcLog.getProspectCall().setCallRetryCount(144);
					pcLog.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
					counterIncrementer = false;
					reason.append("Phone Format Issue|");
				}

				if (checkDNC(prospect, campaign)) {
					return;
				}

				//HERE is the logic for tracking TODO put the flag here
				persistZoomForReview( pcLog, reason.toString(),null,campaign);
				if(StringUtils.isBlank(pcLog.getProspectCall().getDataSlice())){
					pcLog.getProspectCall().setDataSlice("slice10");
				}
				if(StringUtils.isBlank(pcLog.getDataSlice())){
					pcLog.setDataSlice("slice10");
				}
				prospectCallLogs.add(pcLog);
				prospectCallInteractions.add(prospectCallInteraction);

				PCIAnalysis pcin = new PCIAnalysis();
				// Campaign campaign = campaignService.getCampaign(campaignId);
				pcin = pcin.toPCIAnalysis(pcin, prospectCallInteraction, null, null, campaign);
				pcianList.add(pcin);

				// if(checkLocalDNCList(prospect.getPhone(), prospect.getFirstName(),
				// prospect.getLastName())){
				Map<String, Integer> multiMap = campaignCriteria.getMultiprospectMap();
				boolean isPurchase = true;
				if (multiMap != null && multiMap.size() > 0 && pProspectCallLogs != null && pProspectCallLogs.size() > 0) {
					for (ProspectCallLog p : pProspectCallLogs) {
						String key = p.getProspectCall().getProspect().getPhone();
						Integer c = multiMap.get(key);
						if (c == null) {
							c = 0;
						}
						if (c <= 4) {
							multiMap.put(key, c + 1);
						} else {
							isPurchase = false;
						}
					}
					campaignCriteria.setMultiprospectMap(multiMap);
				}
				if (isPurchase) {// This is for multiprospect to maintin 4 records.
					logger.debug(
							buyRowCount + "###########Buying records for campaign id##############" + prospectCall.getCampaignId());
					campaignContactRepository.saveAll(campaignContactList);
					pProspectCallLogs = prospectCallLogRepository.saveAll(prospectCallLogs);
					prospectCallInteractionRepository.saveAll(prospectCallInteractions);
					globalContactService.pciAnalysisSave(pcianList);
					globalContactService.generateGlobalContact(prospectCallLogs);
					
					if (counterIncrementer)
						buyRowCount++;

					if (campaignCriteria.getContactRequirements().equalsIgnoreCase("2")) {
						Map<String, Integer> indiMap = campaignCriteria.getCompanyIndirectMap();
						if (indiMap == null) {
							indiMap = new HashMap<String, Integer>();
						}
						Integer indcount = indiMap.get(pcLog.getProspectCall().getProspect().getSourceCompanyId());

						if (indcount != null && indcount > 0) {
							indiMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), indcount + 1);
						} else {
							indiMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), 1);
						}
						campaignCriteria.setCompanyIndirectMap(indiMap);
					} else if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
						Map<String, Integer> dirMap = campaignCriteria.getCompanyDirectMap();
						if (dirMap == null) {
							dirMap = new HashMap<String, Integer>();
						}
						Integer dircount = dirMap.get(pcLog.getProspectCall().getProspect().getSourceCompanyId());

						if (dircount != null && dircount > 0) {
							dirMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), dircount + 1);
						} else {
							dirMap.put(pcLog.getProspectCall().getProspect().getSourceCompanyId(), 1);
						}
						campaignCriteria.setCompanyDirectMap(dirMap);

					}
				}
				/*
				 * }else{ //
				 * logger.debug("ph-"+prospect.getPhone()+"-FN-"+prospect.getFirstName()+"-LN-"+
				 * prospect.getLastName()+"==SKIPPING Buy as in DNC."); }
				 */
			} catch (DataIntegrityViolationException e) {
				logger.debug("history already exist");
				e.printStackTrace();
			}
			logger.debug("Record inserted for this job : " + buyRowCount);

		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			if (exceptionAsString.contains("Current state")) {
				dbqe.setStatus(DataBuyQueueStatus.INPROCESS.toString());
			} else {
				if (dbqe.getJobretry() >= 3) {
					dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
					dbqe.setJobEndTime(new Date());
				} else {
					dbqe.setJobretry(dbqe.getJobretry() + 1);
					dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
					dbqe.setInterupted(true);
				}
			}
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());

			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}

	}
	
	private ReviewDataBought convertPCLtoReview(ProspectCallLog pcl,String reason) {
		ReviewDataBought rdb = new ReviewDataBought();
		rdb.setReason(reason);
		rdb.setDataSlice(pcl.getDataSlice());
		rdb.setProspectCall(pcl.getProspectCall());
		rdb.setStatus(pcl.getStatus().toString());
		return rdb;
		
	}
	
	
	private void persistZoomForReview( ProspectCallLog pcLog, String reason,PersonRecord personRecord,Campaign campaign) {
		try {
			if(campaign.isReviewZoomData()) {
				if(pcLog==null) {
					pcLog = new ProspectCallLog();
					pcLog.setDataSlice("dataSlice");
					pcLog.setStatus(ProspectCallStatus.QUEUED);
					Prospect prospect = new Prospect();
					if(personRecord.getFirstName()!=null && !personRecord.getFirstName().isEmpty()) {
						prospect.setFirstName(personRecord.getFirstName());
					}else {
						prospect.setFirstName("XXXX");
					}
					if(personRecord.getLastName()!=null && !personRecord.getLastName().isEmpty()) {
						prospect.setLastName(personRecord.getLastName());
					}else {
						prospect.setLastName("XXXX");
					}
					
					if (personRecord.getLocalAddress() != null
							&& personRecord.getLocalAddress().getCountry() != null) {
						if (personRecord.getLocalAddress().getStreet() != null)
							prospect.setAddressLine1(personRecord.getLocalAddress().getStreet());
						else
							prospect.setAddressLine1("");
						if (personRecord.getLocalAddress().getZip() != null)
							prospect.setZipCode(personRecord.getLocalAddress().getZip());
						else
							prospect.setZipCode("");
						if (personRecord.getLocalAddress().getCity() != null)
							prospect.setCity(personRecord.getLocalAddress().getCity());
						else
							prospect.setCity("");
						if (personRecord.getLocalAddress().getCountry() != null)
							prospect.setCountry(personRecord.getLocalAddress().getCountry());
						else
							prospect.setCountry("");
	
						// prospect.setZipCode(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getZip());
						// prospect.setCity(personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCity());
					} else if (personRecord.getCurrentEmployment().getCompany().getCompanyAddress() != null
							&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress()
									.getCountry() != null) {
						prospect.setAddressLine1(
								personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getStreet());
						prospect
								.setZipCode(personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getZip());
						prospect
								.setCity(personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCity());
						prospect.setCountry(personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCountry());
					}
					
					if(personRecord.getCurrentEmployment().getCompany().getCompanyName()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyName().isEmpty()) {
						prospect.setCompany(personRecord.getCurrentEmployment().getCompany().getCompanyName());
					}else {
						prospect.setCompany("XXXX");
					}
					if(personRecord.getCurrentEmployment().getCompany().getCompanyId()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyId().isEmpty()) {
						prospect.setCompanyId(personRecord.getCurrentEmployment().getCompany().getCompanyId());
					}else {
						prospect.setCompanyId("XXXX");
					}
					
					if(personRecord.getCurrentEmployment().getCompany().getCompanyPhone()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyPhone().isEmpty()) {
						prospect.setCompanyPhone(personRecord.getCurrentEmployment().getCompany().getCompanyPhone());
					}else {
						prospect.setCompanyPhone("XXXX");
					}
					
					if(personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange().isEmpty()) {
						prospect.setCustomAttribute("employeeCountRange", personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCountRange());
					}
					prospect.setCustomAttribute("employeecount", personRecord.getCurrentEmployment().getCompany().getCompanyEmployeeCount()+"");
					if(personRecord.getCurrentEmployment().getCompany().getCompanyRevenue()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyRevenue().isEmpty()) {
						prospect.setCustomAttribute("revenue", personRecord.getCurrentEmployment().getCompany().getCompanyRevenue());
					}
					if(personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange().isEmpty()) {
						prospect.setCustomAttribute("revenueRange", personRecord.getCurrentEmployment().getCompany().getCompanyRevenueRange());
					}
					prospect.setCustomAttribute("revnueinZero", personRecord.getCurrentEmployment().getCompany().getCompanyRevenueIn000s()+"");
					if(personRecord.getCurrentEmployment().getCompany().getCompanyWebsite()!=null && !personRecord.getCurrentEmployment().getCompany().getCompanyWebsite().isEmpty()) {
						prospect.setCustomAttribute("domain", personRecord.getCurrentEmployment().getCompany().getCompanyWebsite());
					}
					if(personRecord.getCurrentEmployment().getCompany().getZoomCompanyUrl()!=null && !personRecord.getCurrentEmployment().getCompany().getZoomCompanyUrl().isEmpty()) {
						prospect.setZoomCompanyUrl(personRecord.getCurrentEmployment().getCompany().getZoomCompanyUrl());
					}
					prospect.setDataSource("ZOOINFO");
					if (personRecord.getCurrentEmployment() != null) {
						if (personRecord.getCurrentEmployment().getJobFunction() != null) {
							String department = personRecord.getCurrentEmployment().getJobFunction();
							prospect.setDepartment(department);
						}else {
							prospect.setDepartment("XXXX");
						}
					}else {
						prospect.setDepartment("XXXX");
					}
					List<String> industryList = personRecord.getIndustry();
	
					if(industryList!=null && industryList.size()>0) {
						prospect.setIndustryList(industryList);
						prospect.setIndustry(industryList.get(0));
					}
					if(personRecord.getLastUpdatedDate()!=null && !personRecord.getLastUpdatedDate().isEmpty()) {
						String ld = personRecord.getLastUpdatedDate();
						if (ld.contains("-"))
						ld = ld.replace("-", "/");
						Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(ld);
	
						prospect.setLastUpdatedDate(date1);
					}
					if(personRecord.getManagementLevel()!=null && !personRecord.getManagementLevel().isEmpty())
						prospect.setManagementLevel(personRecord.getManagementLevel());
					
					prospect.setTimeZone(getTimeZone(prospect));
					if(personRecord.getCurrentEmployment().getJobTitle() != null)
						prospect.setTitle(personRecord.getCurrentEmployment().getJobTitle());
					
					prospect.setZoomPersonUrl(personRecord.getZoomPersonUrl());
					ProspectCall prospectCall = new ProspectCall(getUniqueProspect(), campaign.getId(), prospect);
					pcLog.setProspectCall(prospectCall);
					reviewDataBoughtRepository.save(convertPCLtoReview(pcLog, reason));
				}else {
					reviewDataBoughtRepository.save(convertPCLtoReview(pcLog, reason));
				}
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("Error occured while saving zoomReviewData");
		}
	}

	private String getUniqueProspect() {
		String uniquepcid = UUID.randomUUID().toString();
		Date d = new Date();
		// System.out.println(uniquepcid+Long.toString(d.getTime()));
		String pcidv = uniquepcid + Long.toString(d.getTime());
		return pcidv;
	}

	private ZoomInfoResponse getZoomInfoResponse(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
		// int responseCode= connection.getResponseCode();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputline;
		StringBuffer response = new StringBuffer();
		while ((inputline = bufferedReader.readLine()) != null) {
			response.append(inputline);
		}
		bufferedReader.close();

		ZoomInfoResponse zoomInfoResponse = getResponse(response, false);
		return zoomInfoResponse;
	}

	private boolean isZipCodeValid(List<String> zipCodeList, String zipCode) {
		boolean isValidZipCode = true;
		if (zipCode != null && !zipCode.isEmpty() && zipCodeList.contains(zipCode)) {
			isValidZipCode = false;
		}
		return isValidZipCode;
	}

	private boolean isSICorNAICS(List<String> industryCodeList, List<String> sicList, List<String> naicsList) {
		boolean isValidSICCode = true;
		// if(industryCodeList.get(0).contains("NAICS") ) {
		if (naicsList != null && !naicsList.isEmpty()) {
			// List<String> nacisList = abmListDetail.getCompanyNAICS();
			for (String naics : naicsList) {
				if (industryCodeList.contains("NAICS" + naics)) {
					isValidSICCode = false;
					break;
				}
			}
		}

		if (isValidSICCode && sicList != null && !sicList.isEmpty()) {
			// List<String> sicList = abmListDetail.getCompanySIC();
			for (String sic : sicList) {
				if (industryCodeList.contains("SIC" + sic)) {
					isValidSICCode = false;
					break;
				}
			}
		}

		return isValidSICCode;
	}

	private List<PersonRecord> getPersonRecordList(ZoomInfoResponse zoomInfoResponse, List<PersonRecord> personRecordList,
			CampaignCriteria campaignCriteria, String purchaseTitle, Campaign campaign, List<String> runningCampaignIds) {

		if (zoomInfoResponse.getPersonSearchResponse().getTotalResults() > 0
				&& zoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults().getPersonRecord() != null) {
			List<PersonRecord> list = zoomInfoResponse.getPersonSearchResponse().getPeopleSearchResults().getPersonRecord();
			for (int i = 0; i < list.size(); i++) {
				if (personRecordList.size() < campaignCriteria.getTotalPurchaseReq()) {
					PersonRecord personRecord = list.get(i);
					if (personRecord != null) {
						boolean isValidSICCode = false;

						try {
							if (personRecord.getCurrentEmployment() != null
									&& personRecord.getCurrentEmployment().getCompany() != null
									&& personRecord.getCurrentEmployment().getCompany().getCompanyId() != null) {
								AbmListDetail abmListDetail = abmListDetailRepository
										.findOneByCompanyId(personRecord.getCurrentEmployment().getCompany().getCompanyId());
								//List<AbmListDetail> abmdetailList = abmListDetailRepositoryImpl
								//		.getSicAndNaicsList(personRecord.getCurrentEmployment().getCompany().getCompanyId());
								if (abmListDetail != null) {
									// do nothing
									if ((campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty())) {
										List<String> industryCodeList = new ArrayList<String>();
										if (campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty())
											industryCodeList = Arrays.asList(campaignCriteria.getSicCode().split(","));

										List<String> sicList = new ArrayList<String>(1);
										List<String> nacisList = new ArrayList<String>(1);

										if (abmListDetail.getCompanySIC() != null && !abmListDetail.getCompanySIC().isEmpty()) {
											sicList = abmListDetail.getCompanySIC();
										}

										if (abmListDetail.getCompanySIC() != null && !abmListDetail.getCompanySIC().isEmpty()) {
											nacisList = abmListDetail.getCompanySIC();
										}

										isValidSICCode = isSICorNAICS(industryCodeList, sicList, nacisList);

									}

								} else {
									// Insert in abmdetaillist
									ZoomInfoResponse companyResponse = getCompanyResponse(null,
											personRecord.getCurrentEmployment().getCompany().getCompanyId(), campaign.getId());
									if (((campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty()))
											&& companyResponse != null && companyResponse.getCompanySearchResponse().getMaxResults() > 0) {
										List<String> industryCodeList = new ArrayList<String>();
										if (campaignCriteria.getSicCode() != null && !campaignCriteria.getSicCode().isEmpty())
											industryCodeList = Arrays.asList(campaignCriteria.getSicCode().split(","));

										List<String> nacisList = new ArrayList<String>(1);
										List<String> sicList = new ArrayList<String>(1);

										if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
												.getCompanyNAICS() != null) {
											nacisList = companyResponse.getCompanySearchResponse().getCompanySearchResults()
													.getCompanyRecord().get(0).getCompanyNAICS();
										}

										if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
												.getCompanySIC() != null) {
											sicList = companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
													.get(0).getCompanySIC();

										}
										isValidSICCode = isSICorNAICS(industryCodeList, sicList, nacisList);
									}
									insertAbmListDetail(companyResponse);
								}
							}
							 if (!isIndustryValid(personRecord, campaignCriteria)) { // check if person is already bought by
								// name and company
								String reason = "Search Person Industry doesn't match. ";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null)
								reason = reason + "Its " + personRecord.getIndustry();
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
								+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
								}
							else if (!isPersonIdAlreadyPurchased(personRecord, campaign.getId())) { // check if the record is purchased in
																																									// this job only
								String reason = "Search PersonId already purchased    " + personRecord.getPersonId();
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!campaign.isReviewZoomData() && !isPersonBelongstoCountry(personRecord, campaignCriteria.getCountryList())) { // check if the
																																																				// record belong
																																																				// to Country US
								String country = "";
								if (personRecord.getLocalAddress() != null) {
									if (personRecord.getLocalAddress().getCountry() != null) {
										country = personRecord.getLocalAddress().getCountry();
									}
								}
								String reason = "Search Person does not belong to" + campaignCriteria.getCountry() + ". Belongs to " + country;
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (isValidSICCode) { // check if the record belong to Country US
								String reason = "Search Company NAICS or SIC code doesnt match ";
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isFirstAndLastNameValid(personRecord)) { // check if the record belong to Country US
								String reason = "Search Person First or Last name is empty ";
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isPersonTitleKeywordExist(campaignCriteria, personRecord)) { // check if the record belong to
																																												// Country US
								String reason = "Search Person title : " + purchaseTitle + " doesnt exist in "
										+ personRecord.getCurrentEmployment().getJobTitle();
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isPersonBelongstoState(personRecord, campaignCriteria.getState())) { // check if the record
																																																// belong to Country US
								String reason = "Search Person STATE does not belong to CRITERIA" ;
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (personIdList.contains(personRecord.getPersonId())) { // check if the record is purchased in
																																							// this job only
								String reason = "Search PersonId already purchased for this job..   " + personRecord.getPersonId();
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (isPersonRecordAlreadyPurchasedPCL(personRecord, campaign.getId())) { // check if person is
																																															// already bought by name
																																															// and company
								// else if (isPersonRecordAlreadyPurchased(personRecord)) { //check if person is
								// already bought by name and company
								String reason = personRecord.getFirstName() + "_" + personRecord.getLastName() + ":"
										+ "Search First and Last name, company and title  already purchased";
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!checkManagementLevel(personRecord, campaignCriteria)) { // check if person is already bought
																																									// by name and company
								// else if (isPersonRecordAlreadyPurchased(personRecord)) { //check if person is
								// already bought by name and company
								String reason = "Search Person Management Level is missing";
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isDepartmentMatch(personRecord, campaignCriteria)) { // check if person is already bought by
																																								// name and company
								String reason = "Search Person Department doesn't match. ";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null)
									reason = reason + "Its " + personRecord.getCurrentEmployment().getJobFunction();
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!checkProspectVersion(personRecord, runningCampaignIds)) { // check if person is already bought
																																										// by name and company
								String reason = "Search Person bought more than number of versions defined. ";
								persistZoomForReview(null, reason, personRecord, campaign);
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isIndustryValid(personRecord, campaignCriteria)) { // check if person is already bought by
																																							// name and company
								String reason = "Search Person Industry doesn't match. ";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null)
									reason = reason + "Its " + personRecord.getIndustry();
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (ismultiTitle(personRecord, campaignCriteria)) { // check if person is already bought by name
																																					// and company
								String reason = "Search Person Title keyword is negated. doesn't match. ";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null)
									reason = reason + "Its " + personRecord.getCurrentEmployment().getJobTitle().toLowerCase();
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (isCompanySuppressed(personRecord, campaignCriteria)) { // check if person is already bought by
																																								// name and company
								String reason = "Search Person company is in suppression list ";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null)
									reason = reason + "in suppression list "
											+ personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
								logger.debug(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->" + campaign.getName()
										+ "-->" + reason);
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else if (!isCompanyLimitReached(personRecord, campaignCriteria)) { // check if person is already bought
																																										// by name and company
								
								String reason = "Search Per Company record buy limit reached";
								persistZoomForReview(null, reason, personRecord, campaign);
								if (personRecord.getCurrentEmployment() != null
										&& personRecord.getCurrentEmployment().getCompany() != null
										&& personRecord.getCurrentEmployment().getCompany().getCompanyName() != null) {
									reason = reason + "Already Limit reached for buying this company "
											+ personRecord.getCurrentEmployment().getCompany().getCompanyName();
								System.out.println(zoomInfoResponse.getPersonSearchResponse().getTotalResults() + "-->"
										+ campaign.getName() + "-->" + reason);
									if(campaignCriteria.getAbmCompanyIds()!=null && !campaignCriteria.getAbmCompanyIds().isEmpty()) {
										campaignCriteria.setSkipAbmCompany(true);
										break;
									}
								}
								// createXLForFailedSearchRecord(personRecord,sheetFailedRecord,reason,totalResults,companyRecord);
								personRecord = null;
								continue;
							} else {
								/*
								 * long c=1;
								 * recordsBoughtPerOrg.put(personRecord.getCurrentEmployment().getCompany().
								 * getCompanyID(), c);
								 */
								personRecordList.add(personRecord);
								personIdList.add(personRecord.getPersonId());
							}
						} catch (Exception e) {
							DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(
									campaignCriteria.getCampaignId(), DataBuyQueueStatus.INPROCESS.toString(),
									campaignCriteria.getRequestJobDate());
							if (dbqe.getPurchasedCount() != null) {
								dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
								dbqe.setZoomInfoPurchasedCount(buyRowCount);
							} else {
								dbqe.setPurchasedCount(buyRowCount);
								dbqe.setZoomInfoPurchasedCount(buyRowCount);
							}
							StringWriter sw = new StringWriter();
							e.printStackTrace(new PrintWriter(sw));
							String exceptionAsString = sw.toString();
							// logger.debug(exceptionAsString);
							dbqe.setErrorMsg(exceptionAsString);
							// dbqe.setJobEndTime(new Date());
							if (dbqe.getJobretry() >= 3) {
								dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
								dbqe.setJobEndTime(new Date());
							} else {
								dbqe.setJobretry(dbqe.getJobretry() + 1);
								dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
								dbqe.setInterupted(true);
							}
							dataBuyQueueRepository.save(dbqe);
							e.printStackTrace();
						}
					}
				} else {
					break;
				}
			}
		}

		return personRecordList;

	}

	private boolean isIndustryValid(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		List<String> removeIndustriesList = new ArrayList<String>();
		if (campaignCriteria.isIgnoreDefaultIndustries() && campaignCriteria.getIndustry() == null)
			return true;
		if (campaignCriteria.getIndustry() == null
				|| campaignCriteria.getIndustry().isEmpty() && campaignCriteria.getRemoveIndustries() != null) {
			String removeIndustries = campaignCriteria.getRemoveIndustries();
			String[] rIndArr = removeIndustries.split(",\\|");
			removeIndustriesList = Arrays.asList(rIndArr);
		}
		List<String> industryList = personRecord.getIndustry();
		List<String> searchIndList = new ArrayList<String>();
		Map<String, String> zoomIndustryMap = getIndustryMap();
		Map<String, String> zoomIndustryGrpMap = getIndustryGrpMap();
		String IndustryToMatchStr = campaignCriteria.getIndustry();
		// List<String> industryMatchList = new ArrayList<String>();
		if (IndustryToMatchStr != null && !"".equals(IndustryToMatchStr)) {
			String[] searchInd = IndustryToMatchStr.split(",");
			searchIndList = Arrays.asList(searchInd);
		}
		String industry = null;
		int industryListSize = 0;
		if (industryList != null) {
			industryListSize = industryList.size();
		}
		if (industryListSize > 0) {
			industry = industryList.get(0);
			String singind = zoomIndustryMap.get(industry);

			if (campaignCriteria.isPrimaryIndustriesOnly() && zoomIndustryMap.get(industry) != null) {

				if (!removeIndustriesList.contains(industry)) {
					if (searchIndList.isEmpty() || searchIndList.contains(singind)) {
						return true;
					} else {
						boolean indfound = false;
						for (String sind : searchIndList) {
							if (zoomIndustryGrpMap.get(sind) == null) {
								indfound = true;
							} else if (zoomIndustryGrpMap.get(sind).equalsIgnoreCase(singind)) {
								indfound = true;
							}
						}
						if (indfound)
							return true;
						else
							return false;
					}
				}
			} else if (!campaignCriteria.isPrimaryIndustriesOnly()) {
				for (String ind : industryList) {
					if (zoomIndustryMap.get(ind) != null && !removeIndustriesList.contains(ind)) {
						if (!removeIndustriesList.contains(ind)) {
							if (searchIndList == null || searchIndList.size() == 0) {
								return true;
							}
							if (searchIndList.contains(zoomIndustryMap.get(ind))) {
								return true;
							} else if (searchIndList.contains(zoomIndustryGrpMap.get(ind))) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	private boolean isCompanyLimitReached(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		if (campaignCriteria.getCompanyDirectMap() == null) {
			campaignCriteria.setCompanyDirectMap(new HashMap<String, Integer>());
		}
		if (campaignCriteria.getCompanyIndirectMap() == null) {
			campaignCriteria.setCompanyIndirectMap(new HashMap<String, Integer>());
		}
		if (personRecord.getCurrentEmployment() != null && personRecord.getCurrentEmployment().getCompany() != null
				&& personRecord.getCurrentEmployment().getCompany().getCompanyId() != null) {

			if (campaignCriteria.getContactRequirements().equalsIgnoreCase("5")) {
				Integer dcount = campaignCriteria.getCompanyDirectMap()
						.get(personRecord.getCurrentEmployment().getCompany().getCompanyId());
				if (dcount != null && dcount > maxDirectNumbers)
					return suppressed;
			} else {
				Integer icount = campaignCriteria.getCompanyIndirectMap()
						.get(personRecord.getCurrentEmployment().getCompany().getCompanyId());
				if (icount != null && icount > maxRecordsIndirect)
					return suppressed;
			}
			suppressed = true;
		}
		return suppressed;
	}

	private boolean isCompanySuppressed(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		String pdomain = personRecord.getCurrentEmployment().getCompany().getCompanyWebsite();
		if (pdomain != null && !pdomain.isEmpty()) {
			pdomain = getSuppressedHostName(pdomain);
		}
		if (pdomain != null && !pdomain.isEmpty()) {
			long domainSupCount = suppressionListNewRepository
					.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), pdomain);
			if (domainSupCount > 0) {
				suppressed = true;
			}
			if (!suppressed && personRecord.getCurrentEmployment().getCompany().getCompanyName() != null
					&& !personRecord.getCurrentEmployment().getCompany().getCompanyName().isEmpty()) {
				long compSupCount = suppressionListNewRepository.countSupressionListByCampaignDomain(
						campaignCriteria.getCampaignId(), personRecord.getCurrentEmployment().getCompany().getCompanyName());
				if (compSupCount > 0) {
					suppressed = true;
				}
			}
		}
		return suppressed;
	}

	private boolean isCompanySuppressedDetail(ProspectCallLog domain, CampaignCriteria campaignCriteria) {
		boolean suppressed = false;
		if (domain.getProspectCall() != null && domain.getProspectCall().getProspect() != null
				&& domain.getProspectCall().getProspect().getCustomAttributes() != null
				&& domain.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
				&& !domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
			String pdomain = domain.getProspectCall().getProspect().getCustomAttributeValue("domain").toString();
			if (pdomain != null && !pdomain.isEmpty()) {
				pdomain = getSuppressedHostName(pdomain);
			}
			if (domain.getProspectCall().getProspect().getEmail() != null
					&& !domain.getProspectCall().getProspect().getEmail().isEmpty()) {
				String pEmail = domain.getProspectCall().getProspect().getEmail();
				String domEmail = pEmail.substring(pEmail.indexOf("@") + 1, pEmail.length());

				long compSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), domEmail);
				if (compSupCount > 0) {
					suppressed = true;
				}
			}

			if (pdomain != null && !pdomain.isEmpty()) {
				long domainSupCount = suppressionListNewRepository
						.countSupressionListByCampaignDomain(campaignCriteria.getCampaignId(), pdomain);
				if (domainSupCount > 0) {
					suppressed = true;
				}
			}
		}
		return suppressed;
	}

	private boolean isEmailSuppressed(ProspectCallLog personRecord, CampaignCriteria campaignCriteria,
			Campaign campaign) {
		boolean suppressed = suppressionListService.isEmailSuppressed(personRecord.getProspectCall().getProspect().getEmail(),
				campaignCriteria.getCampaignId(), campaign.getOrganizationId(),
				campaign.isMdFiveSuppressionCheck());
		if (!suppressed && campaignCriteria.getRemoveIndustries() != null
				&& !campaignCriteria.getRemoveIndustries().isEmpty()
				&& personRecord.getProspectCall().getProspect().getEmail() != null
				&& !personRecord.getProspectCall().getProspect().getEmail().isEmpty()) {

			String[] indValsArr = campaignCriteria.getRemoveIndustries().split(",");
			List<String> inIndValsList = Arrays.asList(indValsArr);
			// Educatin
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".edu")) {
				if (inIndValsList.contains("1802") || inIndValsList.contains("263946") || inIndValsList.contains("132874")
						|| inIndValsList.contains("198410") || inIndValsList.contains("67338")) {
					suppressed = true;
				}
			}
			// Government
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".gov")) {
				if (inIndValsList.contains("2570")) {
					suppressed = true;
				}
			}
			// religious organization
			if (personRecord.getProspectCall().getProspect().getEmail().contains(".org")) {
				if (inIndValsList.contains("201482") || inIndValsList.contains("4874") || inIndValsList.contains("135946")
						|| inIndValsList.contains("70410") || inIndValsList.contains("267018")) {
					suppressed = true;
				}
			}
			// municipal
			// if(inIndValsList.contains("4618") || inIndValsList.contains("135690") ||
			// inIndValsList.contains("70154")) {
			// do nothing
			// }
		}
		return suppressed;
	}

	private boolean ismultiTitle(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		if (personRecord.getCurrentEmployment() != null && personRecord.getCurrentEmployment().getJobTitle() != null) {

			if (!campaignCriteria.isAndTitleIgnored()
					&& personRecord.getCurrentEmployment().getJobTitle().contains(" and ")) {
				return true;
			} else {
				if (personRecord.getCurrentEmployment().getManagementLevel().get(0).equalsIgnoreCase("Manager")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).equalsIgnoreCase("Director")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).equalsIgnoreCase("VP-Level")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).equalsIgnoreCase("C-Level")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).toLowerCase().contains("executiv")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).toLowerCase().contains("mid management")
						|| personRecord.getCurrentEmployment().getManagementLevel().get(0).toLowerCase().contains("board")) {
					List<String> negativeTitles = getNegativeTitles(campaignCriteria);
					String lowerTitle = personRecord.getCurrentEmployment().getJobTitle().toLowerCase();
					for (String neg : negativeTitles) {
						if (lowerTitle.contains(neg)) {
							return true;
						}
					}
					// return false;

				}
			}
		}

		return false;
	}

	private boolean isDepartmentMatch(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		// please set department as per criteria if not applicable then there is no need
		// to change
		// List<String> jobFunctionSearchClause = new ArrayList<String>();

		if (personRecord.getCurrentEmployment() != null) {
			if (personRecord.getCurrentEmployment().getJobFunction() != null) {
				String department = personRecord.getCurrentEmployment().getJobFunction();
				if (department.equalsIgnoreCase("Finance")) {
					// logger.debug("HERERE");
				}
				Map<String, String> zoomDepts = getTitlesMap();
				Map<String, String> zoomGrpDepts = getTitlesGroupMap();
				String sDept = zoomDepts.get(department);
				String searchDepartments = campaignCriteria.getDepartment();
				if (searchDepartments != null && !"".equals(searchDepartments)) {
					String[] searchDept = searchDepartments.split(",");

					List<String> supDeptList = Arrays.asList(searchDept);
					for (String dept : supDeptList) {
						if (sDept != null && sDept.equalsIgnoreCase(dept)) {
							return true;
						} else if (sDept != null && sDept.equalsIgnoreCase(zoomGrpDepts.get(dept))) {
							return true;
						}
					}
				} else {
					if (department != null && !department.isEmpty() /* && department.equalsIgnoreCase("NA") */
							&& campaignCriteria.getTitleKeyWord() != null && !campaignCriteria.getTitleKeyWord().isEmpty()) {
						if (department.equalsIgnoreCase("NA"))
							logger.debug("NA");
						return true;
					}

					if (department != null && !department.isEmpty() /* && !department.equalsIgnoreCase("NA") */) {
						if (department.equalsIgnoreCase("NA"))
							logger.debug("NA");
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isPersonIdAlreadyPurchased(PersonRecord personRecord, String campaignId) {
		List<ProspectCallLog> pclist = prospectCallLogRepository.getProspectByPersonId(campaignId,
				personRecord.getPersonId());
		if (pclist != null && pclist.size() > 0) {
			return false;
		}
		return true;
	}

	public boolean isPersonRecordAlreadyPurchasedPCLog(ProspectCallLog personRecord, CampaignCriteria campaignCriteria) {
		String firstName = personRecord.getProspectCall().getProspect().getFirstName();
		String lastName = personRecord.getProspectCall().getProspect().getLastName();
		String company = personRecord.getProspectCall().getProspect().getCompany();
		String title = personRecord.getProspectCall().getProspect().getTitle();
		List<String> campaignIds = new ArrayList<String>();
		// campaignIds.add(campaignId);
		List<String> campaignList = new ArrayList<String>();
		Campaign campaign = campaignService.getCampaign(campaignCriteria.getCampaignId());
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			for (String cid : campaign.getCampaignGroupIds()) {
				campaignList.add(cid);
			}
			campaignList.add(campaignCriteria.getCampaignId() + NON_CALLABLE);
			// campaignList.add(campaignCriteria.getCampaignId()+PIVOT_REMOVED);
		} else {
			campaignList.add(campaignCriteria.getCampaignId());
			campaignList.add(campaignCriteria.getCampaignId() + NON_CALLABLE);
			// campaignList.add(campaignCriteria.getCampaignId()+PIVOT_REMOVED);

		}
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecordGroup(campaignList, firstName, lastName, title,
				company);
		if (uniqueRecord == 0) {
			return false;
		} else {
			logger.debug(firstName + " " + lastName + " " + company + " | Prospect #### : Already purchased in campaign : "
					+ campaignCriteria.getCampaignId());
			return true;
		}
	}

	private boolean isPersonRecordAlreadyPurchasedPCL(PersonRecord personRecord, String campaignId) {
		String firstName = personRecord.getFirstName();
		String lastName = personRecord.getLastName();
		String company = personRecord.getCurrentEmployment().getCompany().getCompanyName();
		String title = personRecord.getCurrentEmployment().getJobTitle();
		// List<ProspectCallLog> uniqueRecord =
		// prospectCallLogRepository.findUinqueRecord(campaignId,firstName, lastName,
		// title, company);
		List<String> campaignIds = new ArrayList<String>();
		Campaign campaign = campaignService.getCampaign(campaignId);

		campaignIds.add(campaignId);
		campaignIds.add(campaignId + NON_CALLABLE);
		if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
			for (String cid : campaign.getCampaignGroupIds()) {
				campaignIds.add(cid);
			}
		} else {
			campaignIds.add(campaignId);
		}
		// TODO remove after buying ringcentral mm
		// campaignIds.add("5cedb5fee4b0023f192dd97a");
		int uniqueRecord = prospectCallLogRepository.findUniqueProspectRecord(campaignIds, firstName, lastName, title,
				company);
		// if (uniqueRecord.size() == 0) {
		if (uniqueRecord == 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean checkManagementLevel(PersonRecord personRecord, CampaignCriteria campaignCriteria) {
		if (personRecord != null && personRecord.getCurrentEmployment() != null
				&& personRecord.getCurrentEmployment().getManagementLevel() != null) {
			String mgmtLevel = campaignCriteria.getManagementLevel();

			if (mgmtLevel != null) {
				String[] mgArray = mgmtLevel.split(",");
				List<String> mgList = Arrays.asList(mgArray);
				Map<String, String> mgMap = getManagementMap();
				String value = mgMap.get(personRecord.getCurrentEmployment().getManagementLevel().get(0));
				if (mgList.contains(value)) {
					return true;
				} else {
					if (mgList.contains("Mid Management")
							&& (value.equalsIgnoreCase("Manager") || value.equalsIgnoreCase("Director"))) {
						return true;
					} else if (mgList.contains("Executives")
							|| mgList.contains("Board Members") && (value.equalsIgnoreCase("Owner")
									|| value.equalsIgnoreCase("C-Level") || value.equalsIgnoreCase("VP-Level"))) {
						return true;
					}
				}
			}

			// return true;
		}
		return false;
	}

	/*
	 * private boolean isPersonTitleKeywordExist(String purchaseTitle,PersonRecord
	 * personRecord){ if (personRecord == null) return false; if (purchaseTitle ==
	 * null || purchaseTitle.isEmpty()) return true; String apurchaseTitle =
	 * purchaseTitle.toLowerCase(); String bpurchaseTitle =
	 * personRecord.getCurrentEmployment().getJobTitle().toLowerCase(); if(
	 * bpurchaseTitle.contains(apurchaseTitle)){ return true; }
	 * 
	 * return false; }
	 */
	private boolean isPersonTitleKeywordExist(CampaignCriteria campaignCriteria, PersonRecord personRecord) {
		List<String> personTitleList = new ArrayList<String>();
		if (campaignCriteria.getTitleKeyWord() != null) {
			personTitleList = Arrays.asList(campaignCriteria.getTitleKeyWord().split(","));
		}
		if (personRecord == null)
			return false;
		if (personRecord.getCurrentEmployment() == null)
			return false;
		if (personRecord.getCurrentEmployment().getJobTitle() == null)
			return false;

		if (personTitleList == null || personTitleList.size() == 0)
			return true;

		for (String purchaseTitle : personTitleList) {
			String apurchaseTitle = purchaseTitle.toLowerCase();
			String bpurchaseTitle = personRecord.getCurrentEmployment().getJobTitle().toLowerCase();
			List<String> spacePersonTitleList = Arrays.asList(apurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));
			List<String> spaceZoomTitleList = Arrays.asList(bpurchaseTitle.replaceAll("[^a-zA-Z]+", " ").split(" "));

			boolean flag = false;

			if (spaceZoomTitleList.containsAll(spacePersonTitleList)) {
				flag = true;
			}
			// spacePersonTitleList.parallelStream().allMatch(bpurchaseTitle::contains);//.anyMatch(purchaseTitle::contains).
			if (flag) {
				// System.out.println("------------>FLAG IS TURE:
				// SOURCE-->"+apurchaseTitle+"----------->PRSPECT---->"+bpurchaseTitle);
				return flag;
			}
		}
		return false;
	}

	private boolean isFirstAndLastNameValid(PersonRecord personRecord) {
		if (personRecord == null)
			return false;
		if (personRecord.getFirstName() == null || personRecord.getFirstName().isEmpty()) {
			return false;
		}
		if (personRecord.getLastName() == null || personRecord.getLastName().isEmpty()) {
			return false;
		}
		return true;

	}

	private boolean isPersonBelongstoState(PersonRecord personRecord, String state) {
		if (personRecord == null)
			return false;
		if (state != null && !state.isEmpty()) {
			String[] stateArr = state.split(",");
			List<String> stateList = Arrays.asList(stateArr);
			if (personRecord.getLocalAddress() != null && personRecord.getLocalAddress().getState() != null) {
				String lState = personRecord.getLocalAddress().getState();
				if (stateList.contains(lState)) {
					return true;
				} else {
					logger.debug("Person does not belong to" + state + ". Belongs to " + personRecord.getLocalAddress().getState()
							+ ". Skipping to next one. ");
					// logger.debug("Person does not belong to CA. Belongs to " +
					// personRecord.getLocalAddress().getCountryCode() + ". Skipping to next one.
					// ");
					return false;
				}
			} else if (personRecord.getCurrentEmployment() != null && personRecord.getCurrentEmployment().getCompany() != null
					&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress() != null
					&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getState() != null) {
				String lState = personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getState();
				if (stateList.contains(lState)) {
					return true;
				} else {
					logger.debug("Person does not belong to" + state + ". Belongs to " + lState
							+ ". Skipping to next one. ");
					// logger.debug("Person does not belong to CA. Belongs to " +
					// personRecord.getLocalAddress().getCountryCode() + ". Skipping to next one.
					// ");
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	private boolean isPersonBelongstoCountry(PersonRecord personRecord, List<String> country) {
		if (personRecord == null)
			return false;
		List<String> countryList = new ArrayList<String>();
		if (country.contains("USA")) {
			countryList.add("united states");
			countryList.add("United States");
			countryList.add("usa");
			countryList.add("us");
			countryList.add("US");
		}
		if (country.contains("Canada")) {
			countryList.add("Canada");

			countryList.add("canada");
			countryList.add("ca");
			countryList.add("CA");
		}
		/*
		 * if(country.contains("United Kingdom")){ countryList.add("United Kingdom");
		 * 
		 * countryList.add("united kingdom"); countryList.add("uk");
		 * countryList.add("UK"); }
		 */
		//if (!country.contains("USA") && !country.contains("Canada")/* && !country.contains("United Kingdom") */) {
			countryList.addAll(country);
		//}
		boolean found = false;
		if (personRecord.getLocalAddress() != null) {
			if (personRecord.getLocalAddress().getCountry() != null) {
				String lCountry = personRecord.getLocalAddress().getCountry();
				if (personRecord.getLocalAddress().getCountry() != null && (countryList.contains(lCountry))) {
					// if
					// (personRecord.getLocalAddress().getCountryCode().equalsIgnoreCase("CANADA")
					// || personRecord.getLocalAddress().getCountryCode().equals("CA") ) {
					found = true;
				} else if (personRecord.getLocalAddress().getCountry() == null && personRecord.getCurrentEmployment() != null
						&& personRecord.getCurrentEmployment().getCompany() != null
						&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress() != null
						&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCountry() != null) {
					String cCountry = personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCountry();
					if (countryList.contains(cCountry))
						found = true;

				} else {
					logger.debug("Person does not belong to" + country + ". Belongs to "
							+ personRecord.getLocalAddress().getCountry() + ". Skipping to next one. ");
					// logger.debug("Person does not belong to CA. Belongs to " +
					// personRecord.getLocalAddress().getCountryCode() + ". Skipping to next one.
					// ");
					found = false;
				}
			}
		} else if (personRecord.getCurrentEmployment() != null && personRecord.getCurrentEmployment().getCompany() != null
				&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress() != null
				&& personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCountry() != null) {
			String cCountry = personRecord.getCurrentEmployment().getCompany().getCompanyAddress().getCountry().toLowerCase();
			if (countryList.contains(cCountry))
				found = true;

		} else {
			found = false;
		}
		return found;
	}

	private StringBuilder searchByCompanyId(String purchaseTitle, String companyId, CampaignCriteria campaignCriteria) {
		StringBuilder queryString = new StringBuilder();
		try {
			if (purchaseTitle != null)
				queryString = queryString.append("personTitle=" + URLEncoder.encode(purchaseTitle.trim(), XtaasConstants.UTF8));

			String pTitleSeniority = campaignCriteria.getManagementLevel();
			if (purchaseTitle != null && !purchaseTitle.isEmpty() && pTitleSeniority != null && !pTitleSeniority.isEmpty())
				queryString = queryString.append("&TitleSeniority=" + URLEncoder.encode(pTitleSeniority, XtaasConstants.UTF8));
			else if (purchaseTitle == null && pTitleSeniority != null) {
				queryString = queryString.append("TitleSeniority=" + URLEncoder.encode(pTitleSeniority, XtaasConstants.UTF8));
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null)
					&& campaignCriteria.getDepartment() != null) {
				queryString
						.append("&TitleClassification=" + URLEncoder.encode(campaignCriteria.getDepartment(), XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null
					&& campaignCriteria.getDepartment() != null) {
				queryString
						.append("TitleClassification=" + URLEncoder.encode(campaignCriteria.getDepartment(), XtaasConstants.UTF8));
			}

			queryString.append("&companyId=");
			queryString.append(companyId); // come from text file

			queryString.append("&Country=" + campaignCriteria.getCountry());
			// queryString.append("&locationSearchType=Person");

			// Please don't change this
			queryString.append("&companyPastOrPresent=1&ContactRequirements=" + campaignCriteria.getContactRequirements());

		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}

		return queryString;

	}

	private StringBuilder createPurchaseCriteria(String abmId, String purchaseTitle, String sicCode, String state,
			CampaignCriteria campaignCriteria, String mgmtlevel, String searchMinEmp, String searchMaxEmp,
			String searchMinRev, String searchMaxRev) {

		StringBuilder queryString = new StringBuilder();

		try {
			// if(purchaseTitle != null){
			// queryString.append("personTitle="+URLEncoder.encode(purchaseTitle,
			// XtaasConstants.UTF8));
			// }
			if (campaignCriteria.isCheckSufficiency()) {
				if (purchaseTitle != null && campaignCriteria.getManagementLevel() != null) {
					queryString.append("&TitleSeniority=" + URLEncoder.encode(mgmtlevel, XtaasConstants.UTF8));
				} else if (purchaseTitle == null && mgmtlevel != null && !mgmtlevel.isEmpty()) {
					queryString.append("TitleSeniority=" + URLEncoder.encode(mgmtlevel, XtaasConstants.UTF8));
				}
			} else {
				if (purchaseTitle != null && mgmtlevel != null && !mgmtlevel.isEmpty()) {
					queryString.append("&TitleSeniority=" + URLEncoder.encode(mgmtlevel, XtaasConstants.UTF8));
				} else if (purchaseTitle == null && mgmtlevel != null && !mgmtlevel.isEmpty()) {
					queryString.append("TitleSeniority=" + URLEncoder.encode(mgmtlevel, XtaasConstants.UTF8));
				}
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null)
					&& campaignCriteria.getDepartment() != null) {
				queryString
						.append("&TitleClassification=" + URLEncoder.encode(campaignCriteria.getDepartment(), XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null
					&& campaignCriteria.getDepartment() != null) {
				queryString
						.append("TitleClassification=" + URLEncoder.encode(campaignCriteria.getDepartment(), XtaasConstants.UTF8));
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null
					|| campaignCriteria.getDepartment() != null) && abmId != null) {
				queryString.append("&companyId=" + URLEncoder.encode(abmId, XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null && abmId != null) {
				queryString.append("companyId=" + URLEncoder.encode(abmId, XtaasConstants.UTF8));
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null
					|| campaignCriteria.getDepartment() != null || abmId != null) && campaignCriteria.getIndustry() != null) {
				queryString.append(
						"&IndustryClassification=" + URLEncoder.encode(campaignCriteria.getIndustry(), XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null
					&& campaignCriteria.getDepartment() == null && abmId == null && campaignCriteria.getIndustry() != null) {
				queryString
						.append("IndustryClassification=" + URLEncoder.encode(campaignCriteria.getIndustry(), XtaasConstants.UTF8));
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null
					|| campaignCriteria.getDepartment() != null || abmId != null || campaignCriteria.getIndustry() != null)
					&& sicCode != null) {
				queryString.append("&IndustryCode=" + URLEncoder.encode(sicCode, XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null
					&& campaignCriteria.getDepartment() == null && abmId == null && campaignCriteria.getIndustry() == null
					&& sicCode != null) {
				queryString.append("IndustryCode=" + URLEncoder.encode(sicCode, XtaasConstants.UTF8));
			}

			if (campaignCriteria.getIndustry() != null && campaignCriteria.isPrimaryIndustriesOnly()) {
				queryString.append("&primaryIndustriesOnly=" + URLEncoder.encode("true", XtaasConstants.UTF8));
			}

			if ((purchaseTitle != null || campaignCriteria.getManagementLevel() != null
					|| campaignCriteria.getDepartment() != null || abmId != null || campaignCriteria.getIndustry() != null
					|| sicCode != null) && campaignCriteria.getCountry() != null) {
				queryString.append("&Country=" + URLEncoder.encode(campaignCriteria.getCountry(), XtaasConstants.UTF8));
			} else if (purchaseTitle == null && campaignCriteria.getManagementLevel() == null
					&& campaignCriteria.getDepartment() == null && abmId == null && campaignCriteria.getIndustry() == null
					&& sicCode == null && campaignCriteria.getCountry() != null) {
				queryString.append("Country=" + URLEncoder.encode(campaignCriteria.getCountry(), XtaasConstants.UTF8));
			}
			//

			if (state != null)
				queryString.append("&location=" + URLEncoder.encode(state, XtaasConstants.UTF8));

			if (campaignCriteria.isCheckSufficiency()) {
				if (campaignCriteria.getMinRevenue() != null && !campaignCriteria.getMinRevenue().isEmpty()
						&& !campaignCriteria.getMinRevenue().equalsIgnoreCase("0"))
					queryString.append("&RevenueClassificationMin=" + URLEncoder.encode(searchMinRev, XtaasConstants.UTF8));
				if (campaignCriteria.getMaxRevenue() != null && !campaignCriteria.getMaxRevenue().isEmpty()
						&& !campaignCriteria.getMaxRevenue().equalsIgnoreCase("0"))
					queryString.append("&RevenueClassificationMax=" + URLEncoder.encode(searchMaxRev, XtaasConstants.UTF8));
				if (campaignCriteria.getMinEmployeeCount() != null && !campaignCriteria.getMinEmployeeCount().isEmpty()
						&& !campaignCriteria.getMinEmployeeCount().equalsIgnoreCase("0"))
					queryString.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(searchMinEmp, XtaasConstants.UTF8));

				if (campaignCriteria.getMaxEmployeeCount() != null && !campaignCriteria.getMaxEmployeeCount().isEmpty()
						&& !campaignCriteria.getMaxEmployeeCount().equalsIgnoreCase("0"))
					queryString.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(searchMaxEmp, XtaasConstants.UTF8));
			} else {
				if (searchMinRev != null && !searchMinRev.isEmpty() && !searchMinRev.equalsIgnoreCase("0"))
					queryString.append("&RevenueClassificationMin=" + URLEncoder.encode(searchMinRev, XtaasConstants.UTF8));
				if (searchMaxRev != null && !searchMaxRev.isEmpty() && !searchMaxRev.equalsIgnoreCase("0"))
					queryString.append("&RevenueClassificationMax=" + URLEncoder.encode(searchMaxRev, XtaasConstants.UTF8));
				if (searchMinEmp != null && !searchMinEmp.isEmpty() && !searchMinEmp.equalsIgnoreCase("0"))
					queryString.append("&EmployeeSizeClassificationMin=" + URLEncoder.encode(searchMinEmp, XtaasConstants.UTF8));

				if (searchMaxEmp != null && !searchMaxEmp.isEmpty() && !searchMaxEmp.equalsIgnoreCase("0"))
					queryString.append("&EmployeeSizeClassificationMax=" + URLEncoder.encode(searchMaxEmp, XtaasConstants.UTF8));
			}

			//
			queryString.append("&companyPastOrPresent=1");
			if (campaignCriteria.getValidDateMonthDist() != null)
				queryString.append(
						"&ValidDateMonthDist=" + URLEncoder.encode(campaignCriteria.getValidDateMonthDist(), XtaasConstants.UTF8));

			queryString.append(
					"&ContactRequirements=" + URLEncoder.encode(campaignCriteria.getContactRequirements(), XtaasConstants.UTF8));
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}
		return queryString;

	}

	private ZoomInfoResponse getResponse(StringBuffer response, boolean isCompanySearch) throws IOException {
		int countCA = 0;
		if (!response.toString().contains("personId") && !isCompanySearch) {
			return null;
		}
		String ignore = "\"companyAddress\":\"\"";
		if (response.toString().contains(ignore)) {
			countCA = StringUtils.countMatches(response.toString(), ignore);
			// logger.debug("PersonId = "+response);
			// return null;
		}

		String rep = "\"companyAddress\":{}";
		// String ignorewith = "CurrentEmployment\":[";
		for (int i = 0; i < countCA; i++) {

			int xindex = response.indexOf(ignore);
			String sub = response.substring(xindex);
			String sub1 = sub.substring(0, 19);
			int startindex = response.indexOf(sub1);
			int endindex = startindex + sub1.length();
			response.replace(xindex, endindex, rep);
			// logger.debug(response.toString());
		}
		ZoomInfoResponse zoomInfoResponse = null;
		if (response != null) {
			try {
				ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				zoomInfoResponse = mapper.readValue(response.toString(), ZoomInfoResponse.class);
				return zoomInfoResponse;
			}catch(Exception e) {
        		e.printStackTrace();
        		return null;
        	}
		} else {
			return null;
		}
	}

	private Map<String, Long> findCompanyData(String revRange, String empRange) {
		List<Long> revenueEmployesData = new ArrayList<Long>();
		Map<String, Long> companyData = new HashMap<String, Long>();
		Long minRevenue = new Long(0);
		Long maxRevenue = new Long(0);
		Long minEmployeeCount = new Long(0);
		Long maxEmployeeCount = new Long(0);
		if (revRange != null) {
			String[] revenueParts = revRange.split(" - ");

			if (revenueParts.length == 2) { // Ex response from Zoominfo:
				minRevenue = getRevenue(revenueParts[0]);
				maxRevenue = getRevenue(revenueParts[1]);

			} else if (revRange.contains("Over")) {
				String revenuePart = revRange;
				minRevenue = getRevenue(revenuePart);

			} else if (revRange.contains("Under")) {
				String revenuePart = revRange;
				maxRevenue = getRevenue(revenuePart);
			}
		} else {
			minRevenue = new Long(0);
			maxRevenue = new Long(0);
		}
		if (empRange != null) {
			String[] employeeCountParts = empRange.split(" - ");
			if (employeeCountParts.length == 2) { // Ex response from Zoominfo: 25 to less than 100
				minEmployeeCount = Long.parseLong(employeeCountParts[0].trim().replace(",", ""));
				maxEmployeeCount = Long.parseLong(employeeCountParts[1].trim().replace(",", ""));

			} else if (empRange.contains("Over")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				minEmployeeCount = Long.parseLong(employeeCountPart);

			} else if (empRange.contains("Under")) {
				String employeeCountPart = empRange.replaceAll("[^0-9]", "");
				maxEmployeeCount = Long.parseLong(employeeCountPart);
			}
		} else {
			minEmployeeCount = new Long(0);
			maxEmployeeCount = new Long(0);
		}
		revenueEmployesData.add(minRevenue);
		revenueEmployesData.add(maxRevenue);
		revenueEmployesData.add(minEmployeeCount);
		revenueEmployesData.add(maxEmployeeCount);

		if (minRevenue > 0 && maxRevenue > 0) {
			if (minRevenue > maxRevenue) {
				companyData.put("minRevenue", maxRevenue);
				companyData.put("maxRevenue", minRevenue);
			} else {
				companyData.put("minRevenue", minRevenue);
				companyData.put("maxRevenue", maxRevenue);
			}
		} else {
			companyData.put("minRevenue", minRevenue);
			companyData.put("maxRevenue", maxRevenue);
		}

		companyData.put("minRevenue", minRevenue);
		companyData.put("maxRevenue", maxRevenue);
		companyData.put("minEmployeeCount", minEmployeeCount);
		companyData.put("maxEmployeeCount", maxEmployeeCount);
		return companyData;
	}

	private Long getRevenue(String revenue) {

		double million = 1000000;
		double billion = 1000000000;
		revenue = revenue.substring(1, revenue.length() - 1);
		if (revenue.contains("mil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long y = new Double(value * million).longValue();
			return new Double(value * million).longValue();

		} else if (revenue.contains(" bil")) {
			double value = Integer.parseInt(revenue.replaceAll("[^0-9]", ""));
			// Long x = new Double(value * billion).longValue();
			return new Double(value * billion).longValue();
		}
		return new Long(0);
	}

	private String getInputDepartment(CampaignCriteria campaignCriteria, String jobFunction) {
		String searchDept = null;
		try {
			Map<String, String> zoomDepts = getTitlesMap();
			// Map<String,String> zoomGrpDepts = getTitlesGroupMap();
			String jobNumber = zoomDepts.get(jobFunction);
			// Map<String,String> zoomGrpDepts = getTitlesGroupMap();
			// String departmentList = campaignCriteria.getDepartment();
			// String[] deptArr = departmentList.split(",");
			// List<String> deptList = Arrays.asList(deptArr);
			// List<String> parentDeptList = new ArrayList<String>();
			//
			// for(String dept : deptList){
			// String dep = zoomGrpDepts.get(dept);
			// if(!parentDeptList.contains(dep))
			// parentDeptList.add(dep);
			// }
			boolean found = false;
			for (Map.Entry<String, String> entry : zoomDepts.entrySet()) {
				if (jobNumber.equalsIgnoreCase(entry.getValue())) {
					searchDept = entry.getKey();
					found = true;
					break;
				}
			}
			// if(!found){
			//
			// }

		} catch (NullPointerException e) {
			logger.error("input Department null :" + e);
			searchDept = "_";
		}
		return searchDept;

	}

	private String getJobFunction(ZoomInfoResponse zoomInfoResponseDetail) {
		String jobFunction = null;
		try {
			if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
				for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
						.getCurrentEmployment()) {
					if (currentEmployment.getJobFunction() != null) {
						jobFunction = currentEmployment.getJobFunction();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			logger.error("Department null :" + e);
			jobFunction = "_";
		}
		return jobFunction;
	}

	private String getPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			phone = phone.replaceAll("\\-", "");
			// String currentISDCode = getIsdCode(personDetailResponse);
			// return phone.replaceAll("\\s+", "");
			// phone = phone.replaceAll("[^[A-Za-z]*$]", "");
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			// phone = phone.replaceAll("\\s+", "");
			Integer isdCode = numberService.getIsdCodeFromCountryName(personDetailResponse.getLocalAddress().getCountry());
			if (isdCode != null && !phone.startsWith(Integer.toString(isdCode))) {
				logger.info("IsdCode check ====> Isdcode mismatch for Country name: " + personDetailResponse.getLocalAddress().getCountry() + " || IsdCode in DB: " + isdCode);
				phone = getCompanyPhone(personDetailResponse);
				if (phone != null) {
					phone = phone.replaceAll("[^A-Za-z0-9]", "");
					if (isdCode != null && !phone.startsWith(Integer.toString(isdCode))) {
						phone = null;
					}
				}
			} else if (isdCode == null) {
				logger.info("IsdCode check ====> No IsdCode found in DB for Country name: " + personDetailResponse.getLocalAddress().getCountry());
			}
			// phone = phone.replaceAll("\\-", "");
			// return phone.replaceAll("\\s+", "");

		} else {
			phone = getCompanyPhone(personDetailResponse);
			phone = phone.replaceAll("\\-", "");
			// String currentISDCode = getIsdCode(personDetailResponse);
			// return phone.replaceAll("\\s+", "");
			// phone = phone.replaceAll("[^[A-Za-z]*$]", "");
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			// phone = phone.replaceAll("\\s+", "");
			Integer isdCode = numberService.getIsdCodeFromCountryName(personDetailResponse.getLocalAddress().getCountry());
			if (isdCode != null && !phone.startsWith(Integer.toString(isdCode))) {
				logger.info("IsdCode check ====> Isdcode mismatch for Country name: " + personDetailResponse.getLocalAddress().getCountry() + " || IsdCode in DB: " + isdCode);
				if (phone != null) {
					phone = phone.replaceAll("[^A-Za-z0-9]", "");
					if (isdCode != null && !phone.startsWith(Integer.toString(isdCode))) {
						phone = null;
					}
				}
			} else if (isdCode == null) {
				logger.info("IsdCode check ====> No IsdCode found in DB for Country name: " + personDetailResponse.getLocalAddress().getCountry());
			}

			// phone = phone.replaceAll("\\-", "");
			// return phone.replaceAll("\\s+", "");
		}
		if ( phone != null) {
			// phone = phone.replaceAll("\\-", "");
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			// return phone.replaceAll("\\s+","");
			return phone;
		} else {
			return null;
		}
	}

	private String getIsdCode(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String isdCode = "";
		if (phone != null && phone.contains("-")) {
			isdCode = phone.substring(0, phone.indexOf("-"));
			isdCode = isdCode.replaceAll("[^[A-Za-z]*$]", "");
			isdCode = isdCode.replaceAll("\\s+", "");

			// System.out.println(ext);
		} else if (phone != null && phone.contains(" ")) {
			isdCode = phone.substring(0, phone.indexOf(" "));
			isdCode = isdCode.replaceAll("[^[A-Za-z]*$]", "");
			isdCode = isdCode.replaceAll("\\s+", "");

			// System.out.println(ext);
		}
		return isdCode;
	}

	private boolean formatPhone(String ph) {

		if (ph.contains("(")) {
			return true;
		}

		if (ph.contains(")")) {
			return true;
		}

		return false;
	}

	private String getCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		return phone;
	}

	private String getExtension(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = "";
		if (phone != null && phone.contains("ext")) {
			String phonepp = phone.substring(0, phone.indexOf("ext"));
			ext = phone.substring(phone.indexOf("ext"));
			ext = ext.replaceAll("[^[A-Za-z]*$]", "");

			// System.out.println(ext);
		}
		return ext;
	}

	private String getUSPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		if (phone != null) {
			// phone=phone.replaceAll("[^A-Za-z0-9]", "").substring(0, 10);
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			phone = getUSCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUSCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			phone = phone.replaceAll("[^A-Za-z0-9]", "");
			if (phone.length() >= 10) {
				phone = phone.substring(0, 10);
			}
			return String.valueOf(phone).replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		} else {
			return phone;
		}
	}

	private String getUKPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getDirectPhone();
		String ext = getExtension(personDetailResponse);
		if (ext != null && !ext.isEmpty()) {
			phone = phone.substring(0, phone.indexOf("ext"));
		}
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;

		} else {
			phone = getUKCompanyPhone(personDetailResponse);
			return phone;
		}
	}

	private String getUKCompanyPhone(PersonDetailResponse personDetailResponse) {
		String phone = personDetailResponse.getCompanyPhone();
		if (phone != null) {
			String ph = phone.replaceAll("\\s+", "");
			if (ph.length() >= 13) {
				ph = ph.substring(0, 13);
			}
			return ph;
		} else {
			return phone;
		}
	}

	private String getOrganizationName(ZoomInfoResponse zoomInfoResponseDetail) {
		String orgName = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					orgName = currentEmployment.getCompany().getCompanyName();
					break;
				}
			}
		}

		return orgName;
	}

	private String getStateCode(PersonDetailResponse personDetailResponse) {
		if (personDetailResponse.getLocalAddress() != null) {
			String state = personDetailResponse.getLocalAddress().getState();
			String stateCode = getState(state);
			if (personDetailResponse.getLocalAddress().getCountry() != null
					&& !personDetailResponse.getLocalAddress().getCountry().isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository
						.findByCountryName(personDetailResponse.getLocalAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		} else if (personDetailResponse.getCurrentEmployment() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress() != null
				&& personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry() != null) {
			String state = personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getState();
			String stateCode = getState(state);
			if (personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry() != null
					&& !personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry().isEmpty()
					&& (stateCode == null || stateCode.isEmpty())) {
				List<StateCallConfig> scconfigs = stateCallConfigRepository.findByCountryName(
						personDetailResponse.getCurrentEmployment().get(0).getCompany().getCompanyAddress().getCountry());
				if (scconfigs != null && scconfigs.size() > 0) {
					stateCode = scconfigs.get(0).getStateCode();
				}

			}
			return stateCode;
		}
		return null;
	}

	private String getUKStateCode(PersonDetailResponse personDetailResponse) {
		/*
		 * if(personDetailResponse.getLocalAddress() != null){ String state =
		 * personDetailResponse.getLocalAddress().getState(); String stateCode =
		 * getState(state); return stateCode; }else
		 * if(personDetailResponse.getCurrentEmployment()!=null &&
		 * personDetailResponse.getCurrentEmployment().get(0).getCompany()!=null &&
		 * personDetailResponse.getCurrentEmployment().get(0).getCompany().
		 * getCompanyAddress()!=null){ String state =
		 * personDetailResponse.getCurrentEmployment().get(0).getCompany().
		 * getCompanyAddress().getState(); String stateCode = getState(state); return
		 * stateCode; }
		 */
		return "GL";
	}

	private String getState(String state) {
		EntityMapping toMapping = getEntityMapping();
		AttributeMapping attributeMapping = getReverseAttributeMapping("state", toMapping);
		List<ValueMapping> stateCode = attributeMapping.getValues();
		Iterator<ValueMapping> iterator = stateCode.iterator();
		while (iterator.hasNext()) {
			ValueMapping valueMapping = (ValueMapping) iterator.next();
			String to = valueMapping.getTo().replaceAll("%20", " ");
			if (to.equals(state)) {
				return valueMapping.getFrom();
			}
		}
		return null;

	}

	private EntityMapping getEntityMapping() {
		List<EntityMapping> toMappings = entityMappingRepository.findByFromSystemToSystem("xtaas",
				ContactListProviderType.ZOOMINFO.name());
		if (toMappings.size() > 0) {
			return toMappings.get(0);
		}
		return null;
	}

	private AttributeMapping getReverseAttributeMapping(String netProspexAttribute, EntityMapping toMapping) {
		for (AttributeMapping attributeMapping : toMapping.getAttributes()) {
			if (attributeMapping.getTo().equals(netProspexAttribute)) {
				return attributeMapping;
			}
		}
		return null;
	}

	private String getOrganizationId(ZoomInfoResponse zoomInfoResponseDetail) {
		String companyId = null;
		if (zoomInfoResponseDetail.getPersonDetailResponse().getCurrentEmployment() != null) {
			for (CurrentEmployment currentEmployment : zoomInfoResponseDetail.getPersonDetailResponse()
					.getCurrentEmployment()) {
				if (currentEmployment.getCompany() != null) {
					companyId = currentEmployment.getCompany().getCompanyId();
					break;
				}
			}
		}

		return companyId;
	}

	public String findAreaCode(String phone) {
		int startPoint = 0;
		int endPoint = 3;
		String removeSpclChar = phone.replace("-", "");
		int length = removeSpclChar.length();
		int validatePhoneLength = length - 10;
		startPoint = startPoint + validatePhoneLength;
		endPoint = endPoint + validatePhoneLength;
		String areaCode = phone.substring(startPoint, endPoint);
		return areaCode;
	}

	public String getTimeZone(Prospect prospect) {
		// List<OutboundNumber> outboundNumberList;
		// int areaCode = Integer.parseInt(findAreaCode(prospect.getPhone()));
		if (prospect != null && prospect.getStateCode() != null) {

			StateCallConfig stateCallConfig = stateCallConfigRepository.findByStateCode(prospect.getStateCode());
			return stateCallConfig.getTimezone();
		}
		String s = prospect.getFirstName() + "" + prospect.getLastName();
		logger.debug("prospect State is null : " + s);
		// if(stateCallConfig==null){
		return null;
		// }

		// find a random number from the list

	}

	private Map<String, String> getManagementMap() {
		Map<String, String> mgmtMap = new HashMap<String, String>();
		mgmtMap.put("C-Level", "C_EXECUTIVES");
		mgmtMap.put("VP-Level", "VP_EXECUTIVES");
		mgmtMap.put("Director", "DIRECTOR");
		mgmtMap.put("Manager", "MANAGER");
		mgmtMap.put("Non-Manager", "Non Management");

		return mgmtMap;
	}

	// TODO need to handle not in
	public CampaignCriteria setCampaignCriteria(CampaignCriteria cc, List<Expression> expressions, Campaign campaign) {
		String removeIndustries = "";
		String removeIndusCodes = "70154,4618,135690,4874,135946,70410,267018,201482,2570,1802,263946,132874,198410,67338";
		Map<String, String[]> revMap = XtaasUtils.getRevenueMap();
		Map<String, String[]> empMap = XtaasUtils.getEmployeeMap();
		List<Integer> minRevList = new ArrayList<Integer>();
		List<Integer> maxRevList = new ArrayList<Integer>();
		List<Integer> minEmpList = new ArrayList<Integer>();
		List<Integer> maxEmpList = new ArrayList<Integer>();
		if (!cc.isIgnoreDefaultIndustries()) {
			removeIndustries = "Public Safety|Cities,Towns & Municipalities|"
					+ "Cities,Towns & Municipalities General|Organizations|Membership Organizations|"
					+ "Charitable Organizations & Foundations|Organizations General|"
					+ "Religious Organizations|Government|Education|Education General|K-12 Schools|Training|Colleges & Universities";
		}
		boolean isEmployeeInCriteria = false;
		for (Expression exp : expressions) {
			String attribute = exp.getAttribute();
			String value = exp.getValue();
			String operator = exp.getOperator();

			if (value.equalsIgnoreCase("All")) {
				if (attribute.equalsIgnoreCase("INDUSTRY")) {
					cc.setRemoveIndustries(removeIndusCodes);
				}
				if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
					List<String> mgmtSortedList = new ArrayList<String>();
					mgmtSortedList.add("Non Management");
					mgmtSortedList.add("MANAGER");
					mgmtSortedList.add("DIRECTOR");
					mgmtSortedList.add("Mid Management");
					mgmtSortedList.add("VP_EXECUTIVES");
					mgmtSortedList.add("C_EXECUTIVES");
					mgmtSortedList.add("Executives");
					mgmtSortedList.add("Board Members");
					cc.setSortedMgmtSearchList(mgmtSortedList);
				}
				continue;
			}

			if (operator.equalsIgnoreCase("NOT IN")) {
				boolean first = true;
				StringBuilder sb = new StringBuilder();
				List<PickListItem> pickList = exp.getPickList();
				if (attribute.equalsIgnoreCase("INDUSTRY")) {
					value = value + "," + removeIndusCodes;
					// cc.setRemoveIndustries(value);
				}
				String[] notInValsArr = value.split(",");
				List<String> notInValsList = Arrays.asList(notInValsArr);
				for (PickListItem pl : pickList) {
					if (notInValsList.contains(pl.getValue())) {
						// Do Nothing
						if (removeIndustries == null || removeIndustries.isEmpty())
							removeIndustries = pl.getLabel();
						else
							removeIndustries = removeIndustries + "," + pl.getLabel();
					} else {
						if (first) {
							if (!pl.getValue().equalsIgnoreCase("All")) {
								sb.append(pl.getValue());
								first = false;
							}
						} else {
							if (!pl.getValue().equalsIgnoreCase("All")) {
								sb.append(",");
								sb.append(pl.getValue());
							}
						}
					}
				}
				value = sb.toString();
			}

			if (attribute.equalsIgnoreCase("DEPARTMENT")) {
				cc.setDepartment(value);
			}
			if (attribute.equalsIgnoreCase("INDUSTRY")) {
				cc.setIndustry(value);
				String[] indValsArr = value.split(",");
				List<String> inIndValsList = Arrays.asList(indValsArr);
				/*
				 * String[] removeIndValsArr = removeIndusCodes.split(","); List<String>
				 * removeIndValsList = Arrays.asList(removeIndValsArr);
				 */
				List<String> newRemoveInd = new ArrayList<String>(1);
				// Educatin
				if (inIndValsList.contains("1802") || inIndValsList.contains("263946") || inIndValsList.contains("132874")
						|| inIndValsList.contains("198410") || inIndValsList.contains("67338")) {
					// do nothing
				} else {
					newRemoveInd.add("1802");
					newRemoveInd.add("263946");
					newRemoveInd.add("132874");
					newRemoveInd.add("198410");
					newRemoveInd.add("67338");
				}
				// Government
				if (inIndValsList.contains("2570")) {
					// do nothing
				} else {
					newRemoveInd.add("2570");
				}
				// religious organization
				if (inIndValsList.contains("201482") || inIndValsList.contains("4874") || inIndValsList.contains("135946")
						|| inIndValsList.contains("70410") || inIndValsList.contains("267018")) {
					// do nothing
				} else {
					newRemoveInd.add("201482");
					newRemoveInd.add("4874");
					newRemoveInd.add("135946");
					newRemoveInd.add("70410");
					newRemoveInd.add("267018");
				}
				// municipal
				if (inIndValsList.contains("4618") || inIndValsList.contains("135690") || inIndValsList.contains("70154")) {
					// do nothing
				} else {
					newRemoveInd.add("4618");
					newRemoveInd.add("135690");
					newRemoveInd.add("70154");

				}

				// Convert the List of String to String
				removeIndusCodes = String.join(", ", newRemoveInd);
				cc.setRemoveIndustries(removeIndusCodes);

				// cc.setPrimaryIndustriesOnly(true);
			}
			if (attribute.equalsIgnoreCase("MANAGEMENT_LEVEL")) {
				cc.setManagementLevel(value);
				List<String> mgmtSortedList = XtaasUtils.getSortedMgmtSearchList(value);
				cc.setSortedMgmtSearchList(mgmtSortedList);
			}
			/*
			 * if(attribute.equalsIgnoreCase("MAX_EMP")){ cc.setMaxEmployeeCount(value); }
			 * if(attribute.equalsIgnoreCase("MIN_EMP")){ cc.setMinEmployeeCount(value); }
			 * if(attribute.equalsIgnoreCase("REV_MAX")){ cc.setMaxRevenue(value); }
			 * if(attribute.equalsIgnoreCase("REV_MIN")){ cc.setMinRevenue(value); }
			 */
			//////////////////////
			if (attribute.equalsIgnoreCase("REVENUE")) {
				String[] revenueArr = value.split(",");
				cc.setSortedRevenuetSearchList(XtaasUtils.getRevenuSortedeMap(revenueArr));
				for (int i = 0; i < revenueArr.length; i++) {
					String revVal = revenueArr[i];
					String[] revArr = revMap.get(revVal);

					minRevList.add(Integer.parseInt(revArr[0]));
					maxRevList.add(Integer.parseInt(revArr[1]));
				}

				Collections.sort(minRevList);
				Collections.sort(maxRevList, Collections.reverseOrder());
				if (minRevList.contains(new Integer(0))) {

				} else if (minRevList.get(0) > 0) {
					cc.setMinRevenue(minRevList.get(0).toString());
				}
				if (maxRevList.contains(new Integer(0))) {

				} else if (maxRevList.get(0) > 0) {
					cc.setMaxRevenue(maxRevList.get(0).toString());
				}
			}

			if (attribute.equalsIgnoreCase("EMPLOYEE")) {
				isEmployeeInCriteria = true;
				String[] employeeArr = value.split(",");
				cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
				for (int i = 0; i < employeeArr.length; i++) {
					String empVal = employeeArr[i];
					String[] empArr = empMap.get(empVal);

					minEmpList.add(Integer.parseInt(empArr[0]));
					maxEmpList.add(Integer.parseInt(empArr[1]));
				}

				Collections.sort(minEmpList);
				Collections.sort(maxEmpList, Collections.reverseOrder());
				if (minEmpList.contains(new Integer(0))) {

				} else if (minEmpList.get(0) > 0) {
					cc.setMinEmployeeCount(minEmpList.get(0).toString());
				}
				if (maxEmpList.contains(new Integer(0))) {

				} else if (maxEmpList.get(0) > 0) {
					cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
				}
			}

			//////////////////////////

			if (attribute.equalsIgnoreCase("SIC_CODE")) {
				if (cc.getSicCode() != null && !cc.getSicCode().isEmpty()) {
					String nas = cc.getSicCode() + "," + value;
					cc.setSicCode(nas);
				} else {
					cc.setSicCode(value);
				}
			}
			if (attribute.equalsIgnoreCase("VALID_COUNTRY")) {
				List<String> countryList = Arrays.asList(value.split(","));
				cc.setCountryList(countryList);
				cc.setCountry(value);
			}
			if (attribute.equalsIgnoreCase("VALID_STATE")) {
				cc.setState(value);
			}
			if (attribute.equalsIgnoreCase("SEARCH_TITLE")) {
				cc.setTitleKeyWord(value);
			}
			if (attribute.equalsIgnoreCase("NEGATIVE_TITLE")) {
				cc.setNegativeKeyWord(value);
			}
			if (attribute.equalsIgnoreCase("STATE (US ONLY)")) {
				cc.setState(value);
			}

			if (attribute.equalsIgnoreCase("NAICS_CODE")) {
				if (cc.getSicCode() != null && !cc.getSicCode().isEmpty()) {
					String nas = cc.getSicCode() + "," + value;
					cc.setSicCode(nas);
				} else {
					cc.setSicCode(value);
				}

			}

			if (attribute.equalsIgnoreCase("ZIP_CODE")) {
				cc.setZipcodes(value);
			}

			if (attribute.equalsIgnoreCase("LAST_UPDATED_DATE")) {
				List<Integer> numbers = Stream.of(value.split(",")).map(Integer::parseInt).collect(Collectors.toList());

				Collections.sort(numbers, Collections.reverseOrder());
				cc.setValidDateMonthDist(numbers.get(0).toString());
			}
			/*
			 * if(attribute.equalsIgnoreCase("REMOVE_DEPT")){
			 * cc.setRemoveDepartments(value); }
			 * if(attribute.equalsIgnoreCase("REMOVE_INDUSTRIES")){
			 * cc.setRemoveIndustries(value); }
			 */
		}
		
		if(!isEmployeeInCriteria && campaign != null && !campaign.isABM()) {
			String defaultEmp = "1-5,5-10,10-20,20-50,50-100,100-250,250-500,500-1000,1000-5000,5000-10000";
			String[] employeeArr = defaultEmp.split(",");
			cc.setSortedEmpSearchList(XtaasUtils.getEmpSortedMap(employeeArr));
			for (int i = 0; i < employeeArr.length; i++) {
				String empVal = employeeArr[i];
				String[] empArr = empMap.get(empVal);

				minEmpList.add(Integer.parseInt(empArr[0]));
				maxEmpList.add(Integer.parseInt(empArr[1]));
			}

			Collections.sort(minEmpList);
			Collections.sort(maxEmpList, Collections.reverseOrder());
			if (minEmpList.contains(new Integer(0))) {

			} else if (minEmpList.get(0) > 0) {
				cc.setMinEmployeeCount(minEmpList.get(0).toString());
			}
			if (maxEmpList.contains(new Integer(0))) {

			} else if (maxEmpList.get(0) > 0) {
				cc.setMaxEmployeeCount(maxEmpList.get(0).toString());
			}
		}
		
		if (cc.getIndustry() == null || cc.getIndustry().isEmpty()) {
			if (cc.getRemoveIndustries() != null && !cc.getRemoveIndustries().isEmpty()) {
				// Dont do any thing
			} else {
				cc.setRemoveIndustries(removeIndustries);
			}
		}
		return cc;
	}

	private boolean checkProspectVersion(PersonRecord personRecord, List<String> runningCampaignIds) {
		/*
		 * String firstName = uploadedContact.getFirstName(); String lastName =
		 * uploadedContact.getLastName(); String company =
		 * uploadedContact.getOrganizationName(); String title =
		 * uploadedContact.getTitle();
		 */

		int count = prospectCallLogRepository.findProspectCountbyRunningCampaigns(runningCampaignIds,
				personRecord.getFirstName(), personRecord.getLastName(), personRecord.getCurrentEmployment().getJobTitle(),
				personRecord.getCurrentEmployment().getCompany().getCompanyName());// TODO we need to check for callable
																																						// prospects only.
		int maxProspectVersion = propertyService.getIntApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.MAX_PROSPECT_VERSION.name(), MAX_PROSPECT_VERSION);
		if (count >= maxProspectVersion) {
			// return -1;
			return false;
		} /*
			 * else { uploadedContact.setProspectVersion(count + 1); }
			 */
		// return count + 1;
		return true;
	}

	private int getprospectVersion(String firstName, String lastName, String title, String company,
			List<String> runningCampaignIds) {
		/*
		 * String firstName = uploadedContact.getFirstName(); String lastName =
		 * uploadedContact.getLastName(); String company =
		 * uploadedContact.getOrganizationName(); String title =
		 * uploadedContact.getTitle();
		 */

		int count = prospectCallLogRepository.findProspectCountbyRunningCampaigns(runningCampaignIds, firstName, lastName,
				title, company);
		// TODO we need to check for callable prospects only.
		/*
		 * int maxProspectVersion =
		 * propertyService.getIntApplicationPropertyValue(XtaasConstants.
		 * APPLICATION_PROPERTY.MAX_PROSPECT_VERSION.name(), MAX_PROSPECT_VERSION); if
		 * (count >= maxProspectVersion) { //return -1; //return false; } else {
		 * uploadedContact.setProspectVersion(count + 1); } //return count + 1;
		 */ return count + 1;
	}

	private List<String> getActiveCampaignsByOrganization(String orgId) {
		List<Campaign> campaignList = new ArrayList<Campaign>();
		List<String> runningCampaignIds = new ArrayList<String>();
		campaignList = campaignService.getActiveCampaignsByOrg(orgId);
		// List<String> campaignIds = new ArrayList<String>();
		if (campaignList != null && !campaignList.isEmpty()) {
			for (Campaign campaign : campaignList) {
				if (!runningCampaignIds.contains(campaign.getId())) {
					runningCampaignIds.add(campaign.getId());
				}
			}
		}
		return runningCampaignIds;
	}

	private ProspectCallLog massageUKPhone(ProspectCallLog p) {
		String ph = p.getProspectCall().getProspect().getPhone();
		if (!ph.startsWith("+44")) {
			p.getProspectCall().setCallRetryCount(148);
			p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			return p;
		}
		if (ph.contains("-")) {
			p.getProspectCall().setCallRetryCount(148);
			p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			return p;
		}
		if (ph.contains("(")) {
			p.getProspectCall().setCallRetryCount(148);
			p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			return p;
		}
		if ((ph.length() < 13 || ph.length() > 13)) {
			p.getProspectCall().setCallRetryCount(148);
			p.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
			return p;
		}
		p.setStatus(ProspectCallStatus.QUEUED);
		return p;
	}

	private Map<String, Integer> getSuccessCompanyDomainList(CampaignCriteria campaignCriteria) {
		Map<String, Integer> successCompanies = new HashMap<String, Integer>();
		try {
			String sDate = getStartDate(campaignCriteria);
			if (sDate == null)
				return successCompanies;

			String domain = "";

			Date startDate = XtaasDateUtils.getDate(sDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			List<ProspectCallLog> prospectCallLogListSUCCESS = prospectCallLogRepository
					.findContactsBySuccess(campaignCriteria.getCampaignId(), startDateInUTC);
			if (prospectCallLogListSUCCESS != null && prospectCallLogListSUCCESS.size() > 0)
				for (ProspectCallLog pc : prospectCallLogListSUCCESS) {
					if (pc.getProspectCall().getProspect().getCustomAttributeValue("domain") != null
							&& !pc.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
						domain = pc.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().toLowerCase();
					}
					if (domain != null && !!domain.isEmpty() && !"".equals(domain)) {
						domain = getSuppressedHostName(domain);

						if (successCompanies.containsKey(domain)) {
							int value = successCompanies.get(domain);
							successCompanies.put(domain, value + 1);
						} else {
							int value = 1;
							successCompanies.put(domain, value);
						}
					}
				}
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}

		return successCompanies;
	}

	private Map<String, Integer> getSuccessCompanyList(CampaignCriteria campaignCriteria) {
		Map<String, Integer> successCompanies = new HashMap<String, Integer>();
		try {
			String sDate = getStartDate(campaignCriteria);
			if (sDate == null)
				return successCompanies;

			String domain = "";

			Date startDate = XtaasDateUtils.getDate(sDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
			Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
			List<ProspectCallLog> prospectCallLogListSUCCESS = prospectCallLogRepository
					.findContactsBySuccess(campaignCriteria.getCampaignId(), startDateInUTC);
			if (prospectCallLogListSUCCESS != null && prospectCallLogListSUCCESS.size() > 0)
				for (ProspectCallLog pc : prospectCallLogListSUCCESS) {
					if (pc.getProspectCall().getProspect().getCustomAttributeValue("domain") == null
							|| pc.getProspectCall().getProspect().getCustomAttributeValue("domain").toString().isEmpty()) {
						// domain=pc.getProspectCall().getProspect().getCompany().toLowerCase();
					}

					if (domain != null && !domain.isEmpty() && !"".equals(domain)) {
						domain = getSuppressedHostName(domain);

						if (successCompanies.containsKey(domain)) {
							int value = successCompanies.get(domain);
							successCompanies.put(domain, value + 1);
						} else {
							int value = 1;
							successCompanies.put(domain, value);
						}
					}
				}
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatusAndReqDate(campaignCriteria.getCampaignId(),
					DataBuyQueueStatus.INPROCESS.toString(), campaignCriteria.getRequestJobDate());
			if (dbqe.getPurchasedCount() != null) {
				dbqe.setPurchasedCount(dbqe.getPurchasedCount() + buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			} else {
				dbqe.setPurchasedCount(buyRowCount);
				dbqe.setZoomInfoPurchasedCount(buyRowCount);
			}
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}

		return successCompanies;
	}

	private String getStartDate(CampaignCriteria campaignCriteria) {

		if (campaignCriteria.getDuplicateCompanyDuration() == null
				|| campaignCriteria.getDuplicateCompanyDuration().isEmpty())
			return null;

		// String toDate = "02/10/2018 00:00:00";
		// mm/dd/yyyy
		String toDate = "";
		Calendar c = Calendar.getInstance();
		logger.debug(
				"Current date : " + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));

		String periodActual = campaignCriteria.getDuplicateCompanyDuration();
		int beginIndex = periodActual.indexOf(" ");
		String periodStr = periodActual.substring(0, beginIndex);
		int period = Integer.parseInt(periodStr);
		// logger.debug(period);

		if (campaignCriteria.getDuplicateCompanyDuration().contains("Years")) {
			c.add(Calendar.YEAR, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
		} else if (campaignCriteria.getDuplicateCompanyDuration().contains("Months")) {
			c.add(Calendar.MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
		} else if (campaignCriteria.getDuplicateCompanyDuration().contains("Weeks")) {
			c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
		} else if (campaignCriteria.getDuplicateCompanyDuration().contains("Days")) {
			c.add(Calendar.DAY_OF_MONTH, -period);
			toDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
		}

		return toDate;

	}

	private Map<String, String> getTitlesGroupMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		// Consultants
		titleMap.put("4980839", "4980839");
		// Technical
		titleMap.put("4784231", "4784231");
		titleMap.put("2228226", "4784231");
		titleMap.put("3211266", "4784231");
		titleMap.put("3801090", "4784231");
		titleMap.put("4653058", "4784231");
		titleMap.put("4849666", "4784231");
		titleMap.put("1376258", "4784231");
		titleMap.put("5570562", "4784231");
		titleMap.put("5505026", "4784231");
		titleMap.put("6029314", "4784231");
		titleMap.put("4718594", "4784231");
		titleMap.put("1114114", "4784231");
		titleMap.put("5308418", "4784231");
		// Biometrics
		titleMap.put("2293762", "2293762");
		// Finance
		titleMap.put("4587623", "4587623");
		titleMap.put("262146", "4587623");
		titleMap.put("1769474", "4587623");
		titleMap.put("327682", "4587623");
		titleMap.put("2621442", "4587623");
		titleMap.put("1441794", "4587623");
		// HR
		titleMap.put("4718695", "4718695");
		titleMap.put("2162690", "4718695");
		titleMap.put("4915202", "4718695");
		titleMap.put("3473410", "4718695");
		titleMap.put("5177346", "4718695");
		titleMap.put("5242882", "4718695");
		// LAW
		titleMap.put("5832807", "5832807");
		titleMap.put("1572866", "5832807");
		titleMap.put("1507330", "5832807");
		// marketing
		titleMap.put("4456551", "4456551");
		titleMap.put("786434", "4456551");
		titleMap.put("2555906", "4456551");
		titleMap.put("3932162", "4456551");
		titleMap.put("393216", "4456551");
		titleMap.put("3276802", "4456551");
		titleMap.put("4325378", "4456551");
		// health
		titleMap.put("5701735", "5701735");
		titleMap.put("4784130", "5701735");
		titleMap.put("3670018", "5701735");
		titleMap.put("589826", "5701735");
		titleMap.put("3145730", "5701735");
		// operation
		titleMap.put("4653159", "4653159");
		titleMap.put("3014658", "4653159");
		titleMap.put("4063234", "4653159");
		titleMap.put("4390914", "4653159");
		titleMap.put("2752514", "4653159");
		titleMap.put("5373954", "4653159");
		titleMap.put("5701634", "4653159");
		titleMap.put("2686978", "4653159");
		titleMap.put("6094850", "4653159");
		// sales
		titleMap.put("4522087", "4522087");
		titleMap.put("393218", "4522087");
		titleMap.put("196610", "4522087");
		// scientist
		titleMap.put("4849767", "4849767");
		// topexec
		titleMap.put("4259942", "4259942");

		return titleMap;

	}

	private Map<String, String> getTitlesMap() {
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("Consultants", "4980839");
		titleMap.put("Consulting", "4980839");

		titleMap.put("Engineering & Technical", "4784231");
		titleMap.put("Biometrics", "2293762");
		titleMap.put("Biotechnical Engineering", "2228226");
		titleMap.put("Chemical Engineering", "3211266");
		titleMap.put("Civil Engineering", "3801090");
		titleMap.put("Database Engineering &  Design", "4653058");
		titleMap.put("Engineering", "4849666");
		titleMap.put("Hardware Engineering", "1376258");
		titleMap.put("Industrial Engineering", "5570562");
		titleMap.put("Information Technology", "5505026");
		titleMap.put("Quality Assurance", "6029314");
		titleMap.put("Software Engineering", "4718594");
		titleMap.put("Technical", "1114114");
		titleMap.put("Web Development", "5308418");
		titleMap.put("Finance", "4587623");
		titleMap.put("Accounting", "262146");
		titleMap.put("Banking", "1769474");
		titleMap.put("General Finance", "327682");
		titleMap.put("Investment Banking", "2621442");
		titleMap.put("Wealth Management", "1441794");
		titleMap.put("Human Resources", "4718695");
		titleMap.put("Benefits & Compensation", "2162690");
		titleMap.put("Diversity", "4915202");
		titleMap.put("Human Resources Generalist", "3473410");
		titleMap.put("Recruiting", "5177346");
		titleMap.put("Sourcing", "5242882");
		titleMap.put("Legal", "5832807");
		titleMap.put("Governmental Lawyers & Legal Professionals", "1572866");
		titleMap.put("Lawyers & Legal Professionals", "1507330");
		titleMap.put("Marketing", "4456551");
		titleMap.put("Advertising", "786434");
		titleMap.put("Branding", "2555906");
		titleMap.put("Communications", "3932162");
		titleMap.put("E-biz", "393216");
		titleMap.put("General Marketing", "3276802");
		titleMap.put("Product Management", "4325378");
		titleMap.put("Medical & Health", "5701735");
		titleMap.put("Dentists", "4784130");
		titleMap.put("Doctors - General Practice", "3670018");
		titleMap.put("Health Professionals", "589826");
		titleMap.put("Nurses", "3145730");
		titleMap.put("Operations", "4653159");
		titleMap.put("Change Management", "3014658");
		titleMap.put("Compliance", "4063234");
		titleMap.put("Contracts", "4390914");
		titleMap.put("Customer Relations", "2752514");
		titleMap.put("Facilities", "5373954");
		titleMap.put("Logistics", "5701634");
		titleMap.put("General Operations", "2686978");
		titleMap.put("Quality Management", "6094850");
		titleMap.put("Sales", "4522087");
		titleMap.put("Business Development", "393218");
		titleMap.put("General Sales", "196610");
		titleMap.put("Scientists", "4849767");
		titleMap.put("General Management", "4259942");

		return titleMap;

	}

	private Map<String, String> getIndustryMap() {

		Map<String, String> industryMap = new HashMap<String, String>();
		industryMap.put("Agriculture", "266");
		industryMap.put("Animals & Livestock", "65802");
		industryMap.put("Crops", "131338");
		industryMap.put("Agriculture General", "196874");
		industryMap.put("Business Services", "522");
		industryMap.put("Accounting & Accounting Services", "66058");
		industryMap.put("Auctions", "197130");
		industryMap.put("Call Centers & Business Centers", "262666");
		industryMap.put("Debt Collection", "328202");
		industryMap.put("Management Consulting", "786954");
		industryMap.put("Information & Document Management", "655882");
		industryMap.put("Multimedia & Graphic Design", "852490");
		industryMap.put("Food Service", "393738");
		industryMap.put("Business Services General", "983562");
		industryMap.put("Human Resources & Staffing", "590346");
		industryMap.put("Facilities Management & Commercial Cleaning", "459274");
		industryMap.put("Translation & Linguistic Services", "721418");
		industryMap.put("Advertising & Marketing", "131594");
		industryMap.put("Commercial Printing", "524810");
		industryMap.put("Security Products & Services", "918026");
		industryMap.put("Chambers of Commerce", "778");
		industryMap.put("Construction", "1034");
		industryMap.put("Architecture, Engineering & Design", "66570");
		industryMap.put("Commercial & Residential Construction", "132106");
		industryMap.put("Construction General", "197642");
		industryMap.put("Consumer Services", "1290");
		industryMap.put("Automotive Service & Collision Repair", "66826");
		industryMap.put("Car & Truck Rental", "132362");
		industryMap.put("Funeral Homes & Funeral Related Services", "197898");
		industryMap.put("Consumer Services General", "656650");
		industryMap.put("Hair Salons", "263434");
		industryMap.put("Laundry & Dry Cleaning Services", "328970");
		industryMap.put("Photography Studio", "394506");
		industryMap.put("Travel Agencies & Services", "460042");
		industryMap.put("Veterinary Care", "525578");
		industryMap.put("Weight & Health Management", "591114");
		industryMap.put("Cultural", "1546");
		industryMap.put("Cultural General", "198154");
		industryMap.put("Libraries", "67082");
		industryMap.put("Museums & Art Galleries", "132618");
		industryMap.put("Education", "1802");
		industryMap.put("Education General", "263946");
		industryMap.put("K-12 Schools", "132874");
		industryMap.put("Training", "198410");
		industryMap.put("Colleges & Universities", "67338");
		industryMap.put("Energy, Utilities & Waste Treatment", "2058");
		industryMap.put("Electricity, Oil & Gas", "67594");
		industryMap.put("Waste Treatment, Environmental Services & Recycling", "198666");
		industryMap.put("Energy, Utilities, & Waste Treatment General", "329738");
		industryMap.put("Oil & Gas Exploration & Services", "133130");
		industryMap.put("Water & Water Treatment", "264202");
		industryMap.put("Finance", "2314");
		industryMap.put("Banking", "67850");
		industryMap.put("Brokerage", "133386");
		industryMap.put("Credit Cards & Transaction  Processing", "198922");
		industryMap.put("Finance General", "395530");
		industryMap.put("Investment Banking", "264458");
		industryMap.put("Venture Capital & Private Equity", "329994");
		industryMap.put("Government", "2570");
		industryMap.put("Healthcare", "2826");
		industryMap.put("Emergency Medical Transportation & Services", "133898");
		industryMap.put("Healthcare General", "330506");
		industryMap.put("Hospitals & Clinics", "68362");
		industryMap.put("Medical Testing & Clinical Laboratories", "199434");
		industryMap.put("Pharmaceuticals", "264970");
		industryMap.put("Biotechnology", "17042186");
		industryMap.put("Drug Manufacturing & Research", "33819402");
		industryMap.put("Hospitality", "3082");
		industryMap.put("Hospitality General", "330762");
		industryMap.put("Lodging & Resorts", "68618");
		industryMap.put("Recreation", "134154");
		industryMap.put("Movie Theaters", "67243018");
		industryMap.put("Fitness & Dance Facilities", "33688586");
		industryMap.put("Gambling & Gaming", "50465802");
		industryMap.put("Amusement Parks, Arcades & Attractions", "16911370");
		industryMap.put("Zoos & National Parks", "84020234");
		industryMap.put("Restaurants", "199690");
		industryMap.put("Sports Teams & Leagues", "265226");
		industryMap.put("Insurance", "3338");
		industryMap.put("Law Firms & Legal Services", "3594");
		industryMap.put("Media & Internet", "4106");
		industryMap.put("Broadcasting", "69642");
		industryMap.put("Film/Video Production & Services", "50401290");
		industryMap.put("Radio Stations", "16846858");
		industryMap.put("Television Stations", "33624074");
		industryMap.put("Media & Internet General", "462858");
		industryMap.put("Information Collection & Delivery", "135178");
		industryMap.put("Search Engines & Internet Portals", "200714");
		industryMap.put("Music & Music Related Services", "266250");
		industryMap.put("Newspapers & News Services", "331786");
		industryMap.put("Publishing", "397322");
		industryMap.put("Manufacturing", "3850");
		industryMap.put("Aerospace & Defense", "69386");
		industryMap.put("Boats & Submarines", "134922");
		industryMap.put("Building Materials", "200458");
		industryMap.put("Aggregates, Concrete & Cement", "16977674");
		industryMap.put("Lumber, Wood Production & Timber Operations", "33754890");
		industryMap.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)", "67309322");
		industryMap.put("Plumbing & HVAC Equipment", "50532106");
		industryMap.put("Motor Vehicles", "790282");
		industryMap.put("Motor Vehicle Parts", "855818");
		industryMap.put("Chemicals, Petrochemicals, Glass & Gases", "265994");
		industryMap.put("Chemicals", "17043210");
		industryMap.put("Gases", "33820426");
		industryMap.put("Glass & Clay", "50597642");
		industryMap.put("Petrochemicals", "67374858");
		industryMap.put("Computer Equipment & Peripherals", "331530");
		industryMap.put("Personal Computers & Peripherals", "17108746");
		industryMap.put("Computer Networking Equipment", "50663178");
		industryMap.put("Network Security Hardware & Software", "67440394");
		industryMap.put("Computer Storage Equipment", "33885962");
		industryMap.put("Consumer Goods", "397066");
		industryMap.put("Appliances", "17174282");
		industryMap.put("Cleaning Products", "33951498");
		industryMap.put("Textiles & Apparel", "184946442");
		industryMap.put("Cosmetics, Beauty Supply & Personal Care Products", "50728714");
		industryMap.put("Consumer Electronics", "67505930");
		industryMap.put("Health & Nutrition Products", "84283146");
		industryMap.put("Household Goods", "101060362");
		industryMap.put("Pet Products", "117837578");
		industryMap.put("Photographic & Optical Equipment", "134614794");
		industryMap.put("Sporting Goods", "151392010");
		industryMap.put("Hand, Power and Lawn-care Tools", "168169226");
		industryMap.put("Jewelry & Watches", "201723658");
		industryMap.put("Electronics", "462602");
		industryMap.put("Batteries, Power Storage Equipment & Generators", "34017034");
		industryMap.put("Electronic Components", "17239818");
		industryMap.put("Power Conversion & Protection Equipment", "50794250");
		industryMap.put("Semiconductor & Semiconductor Equipment", "67571466");
		industryMap.put("Food, Beverages & Tobacco", "528138");
		industryMap.put("Food & Beverages", "17305354");
		industryMap.put("Tobacco", "34082570");
		industryMap.put("Wineries & Breweries", "50859786");
		industryMap.put("Furniture", "593674");
		industryMap.put("Manufacturing General", "1380106");
		industryMap.put("Industrial Machinery & Equipment", "659210");
		industryMap.put("Medical Devices &  Equipment", "724746");
		industryMap.put("Pulp & Paper", "921354");
		industryMap.put("Plastic, Packaging & Containers", "986890");
		industryMap.put("Tires & Rubber", "1052426");
		industryMap.put("Telecommunication  Equipment", "1117962");
		industryMap.put("Test & Measurement Equipment", "1183498");
		industryMap.put("Toys & Games", "1249034");
		industryMap.put("Wire & Cable", "1314570");
		industryMap.put("Metals & Mining", "4362");
		industryMap.put("Metals & Mining General", "200970");
		industryMap.put("Metals & Minerals", "135434");
		industryMap.put("Mining", "69898");
		industryMap.put("Cities, Towns & Municipalities", "4618");
		industryMap.put("Cities, Towns & Municipalities General", "135690");
		industryMap.put("Public Safety", "70154");
		industryMap.put("Organizations", "4874");
		industryMap.put("Membership Organizations", "135946");
		industryMap.put("Charitable Organizations & Foundations", "70410");
		industryMap.put("Organizations General", "267018");
		industryMap.put("Religious Organizations", "201482");
		industryMap.put("Real Estate", "5130");
		industryMap.put("Retail", "5386");
		industryMap.put("Motor Vehicle Dealers", "136458");
		industryMap.put("Automobile Parts Stores", "201994");
		industryMap.put("Record, Video & Book Stores", "1185034");
		industryMap.put("Apparel & Accessories Retail", "70922");
		industryMap.put("Gas Stations, Convenience & Liquor Stores", "333066");
		industryMap.put("Department Stores, Shopping Centers & Superstores", "398602");
		industryMap.put("Consumer Electronics & Computers Retail", "267530");
		industryMap.put("Furniture", "595210");
		industryMap.put("Retail General", "1381642");
		industryMap.put("Flowers, Gifts & Specialty Stores", "529674");
		industryMap.put("Grocery Retail", "660746");
		industryMap.put("Home Improvement & Hardware Retail", "791818");
		industryMap.put("Vitamins, Supplements, & Health Stores", "726282");
		industryMap.put("Jewelry & Watch Retail", "857354");
		industryMap.put("Office Products Retail & Distribution", "988426");
		industryMap.put("Pet Products", "922890");
		industryMap.put("Drug Stores & Pharmacies", "464138");
		industryMap.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)", "1316106");
		industryMap.put("Sporting & Recreational Equipment Retail", "1053962");
		industryMap.put("Toys & Games", "1119498");
		industryMap.put("Video & DVD Rental", "1250570");
		industryMap.put("Software", "5642");
		industryMap.put("Software & Technical Consulting", "136714");
		industryMap.put("Software General", "267786");
		industryMap.put("Software Development & Design", "71178");
		industryMap.put("Business Intelligence (BI) Software", "16848394");
		industryMap.put("Content & Collaboration Software", "33625610");
		industryMap.put("Customer Relationship Management (CRM) Software", "50402826");
		industryMap.put("Database & File Management Software", "67180042");
		industryMap.put("Engineering Software", "83957258");
		industryMap.put("Enterprise Resource Planning (ERP) Software", "100734474");
		industryMap.put("Financial, Legal & HR Software", "117511690");
		industryMap.put("Healthcare Software", "134288906");
		industryMap.put("Multimedia, Games and Graphics Software", "151066122");
		industryMap.put("Networking Software", "167843338");
		industryMap.put("Supply Chain Management (SCM) Software", "184620554");
		industryMap.put("Security Software", "201397770");
		industryMap.put("Storage & System Management Software", "218174986");
		industryMap.put("Retail Software", "202250");
		industryMap.put("Telecommunications", "5898");
		industryMap.put("Cable & Satellite", "71434");
		industryMap.put("Telecommunications General", "268042");
		industryMap.put("Internet Service Providers, Website Hosting & Internet-related Services", "136970");
		industryMap.put("Telephony & Wireless", "202506");
		industryMap.put("Transportation", "6154");
		industryMap.put("Airlines, Airports & Air Services", "71690");
		industryMap.put("Freight & Logistics Services", "137226");
		industryMap.put("Transportation General", "399370");
		industryMap.put("Marine Shipping & Transportation", "202762");
		industryMap.put("Trucking, Moving & Storage", "333834");
		industryMap.put("Rail, Bus & Taxi", "268298");

		return industryMap;
	}

	private Map<String, String> getIndustryGrpMap() {

		Map<String, String> industryMap = new HashMap<String, String>();
		industryMap.put("65802", "266");
		industryMap.put("131338", "266");
		industryMap.put("196874", "266");

		// industryMap.put("522","522");
		industryMap.put("197130", "522");
		industryMap.put("262666", "522");
		industryMap.put("328202", "522");
		industryMap.put("786954", "522");
		industryMap.put("655882", "522");
		industryMap.put("852490", "522");
		industryMap.put("393738", "522");
		industryMap.put("983562", "522");
		industryMap.put("590346", "522");
		industryMap.put("459274", "522");
		industryMap.put("721418", "522");
		industryMap.put("131594", "522");
		industryMap.put("524810", "522");
		industryMap.put("918026", "522");

		industryMap.put("66570", "1034");
		industryMap.put("132106", "1034");
		industryMap.put("197642", "1034");

		industryMap.put("66826", "1290");
		industryMap.put("132362", "1290");
		industryMap.put("197898", "1290");
		industryMap.put("656650", "1290");
		industryMap.put("263434", "1290");
		industryMap.put("328970", "1290");
		industryMap.put("394506", "1290");
		industryMap.put("460042", "1290");
		industryMap.put("525578", "1290");
		industryMap.put("591114", "1290");

		industryMap.put("198154", "1546");
		industryMap.put("67082", "1546");
		industryMap.put("132618", "1546");

		industryMap.put("263946", "1802");
		industryMap.put("263946", "1802");
		industryMap.put("132874", "1802");
		industryMap.put("198410", "1802");
		industryMap.put("67338", "1802");

		industryMap.put("67594", "2058");
		industryMap.put("198666", "2058");
		industryMap.put("329738", "2058");
		industryMap.put("133130", "2058");
		industryMap.put("264202", "2058");

		industryMap.put("67850", "2314");
		industryMap.put("133386", "2314");
		industryMap.put("198922", "2314");
		industryMap.put("395530", "2314");
		industryMap.put("264458", "2314");
		industryMap.put("329994", "2314");

		industryMap.put("133898", "2826");
		industryMap.put("330506", "2826");
		industryMap.put("68362", "2826");
		industryMap.put("199434", "2826");
		industryMap.put("264970", "2826");
		industryMap.put("17042186", "2826");
		industryMap.put("33819402", "2826");

		industryMap.put("330762", "3082");
		industryMap.put("68618", "3082");
		industryMap.put("134154", "3082");
		industryMap.put("67243018", "3082");
		industryMap.put("33688586", "3082");
		industryMap.put("50465802", "3082");
		industryMap.put("16911370", "3082");
		industryMap.put("84020234", "3082");
		industryMap.put("199690", "3082");
		industryMap.put("265226", "3082");

		industryMap.put("69642", "4106");
		industryMap.put("50401290", "4106");
		industryMap.put("16846858", "4106");
		industryMap.put("33624074", "4106");
		industryMap.put("462858", "4106");
		industryMap.put("135178", "4106");
		industryMap.put("200714", "4106");
		industryMap.put("266250", "4106");
		industryMap.put("331786", "4106");
		industryMap.put("397322", "4106");

		industryMap.put("69386", "3850");
		industryMap.put("134922", "3850");
		industryMap.put("200458", "3850");
		industryMap.put("16977674", "3850");
		industryMap.put("33754890", "3850");
		industryMap.put("67309322", "3850");
		industryMap.put("50532106", "3850");
		industryMap.put("790282", "3850");
		industryMap.put("855818", "3850");
		industryMap.put("265994", "3850");
		industryMap.put("17043210", "3850");
		industryMap.put("33820426", "3850");
		industryMap.put("50597642", "3850");
		industryMap.put("67374858", "3850");
		industryMap.put("331530", "3850");
		industryMap.put("17108746", "3850");
		industryMap.put("50663178", "3850");
		industryMap.put("67440394", "3850");
		industryMap.put("33885962", "3850");
		industryMap.put("397066", "3850");
		industryMap.put("17174282", "3850");
		industryMap.put("33951498", "3850");
		industryMap.put("184946442", "3850");
		industryMap.put("50728714", "3850");
		industryMap.put("67505930", "3850");
		industryMap.put("84283146", "3850");
		industryMap.put("101060362", "3850");
		industryMap.put("117837578", "3850");
		industryMap.put("134614794", "3850");
		industryMap.put("151392010", "3850");
		industryMap.put("168169226", "3850");
		industryMap.put("201723658", "3850");
		industryMap.put("462602", "3850");
		industryMap.put("34017034", "3850");
		industryMap.put("17239818", "3850");
		industryMap.put("50794250", "3850");
		industryMap.put("67571466", "3850");
		industryMap.put("528138", "3850");
		industryMap.put("17305354", "3850");
		industryMap.put("34082570", "3850");
		industryMap.put("50859786", "3850");
		industryMap.put("593674", "3850");
		industryMap.put("1380106", "3850");
		industryMap.put("659210", "3850");
		industryMap.put("724746", "3850");
		industryMap.put("921354", "3850");
		industryMap.put("986890", "3850");
		industryMap.put("1052426", "3850");
		industryMap.put("1117962", "3850");
		industryMap.put("1183498", "3850");
		industryMap.put("1249034", "3850");
		industryMap.put("1314570", "3850");

		industryMap.put("200970", "4362");
		industryMap.put("135434", "4362");
		industryMap.put("69898", "4362");

		industryMap.put("135690", "4618");
		industryMap.put("70154", "4618");

		industryMap.put("135946", "4874");
		industryMap.put("70410", "4874");
		industryMap.put("267018", "4874");
		industryMap.put("201482", "4874");

		industryMap.put("136458", "5386");
		industryMap.put("201994", "5386");
		industryMap.put("1185034", "5386");
		industryMap.put("70922", "5386");
		industryMap.put("333066", "5386");
		industryMap.put("398602", "5386");
		industryMap.put("267530", "5386");
		industryMap.put("595210", "5386");
		industryMap.put("1381642", "5386");
		industryMap.put("529674", "5386");
		industryMap.put("660746", "5386");
		industryMap.put("791818", "5386");
		industryMap.put("726282", "5386");
		industryMap.put("857354", "5386");
		industryMap.put("988426", "5386");
		industryMap.put("922890", "5386");
		industryMap.put("464138", "5386");
		industryMap.put("1316106", "5386");
		industryMap.put("1053962", "5386");
		industryMap.put("1119498", "5386");
		industryMap.put("1250570", "5386");

		industryMap.put("136714", "5642");
		industryMap.put("267786", "136714");
		industryMap.put("71178", "267786");
		industryMap.put("16848394", "71178");
		industryMap.put("33625610", "16848394");
		industryMap.put("50402826", "33625610");
		industryMap.put("67180042", "50402826");
		industryMap.put("83957258", "67180042");
		industryMap.put("100734474", "83957258");
		industryMap.put("117511690", "100734474");
		industryMap.put("134288906", "117511690");
		industryMap.put("151066122", "134288906");
		industryMap.put("167843338", "151066122");
		industryMap.put("184620554", "167843338");
		industryMap.put("201397770", "184620554");
		industryMap.put("218174986", "201397770");
		industryMap.put("202250", "218174986");

		industryMap.put("71434", "5898");
		industryMap.put("268042", "71434");
		industryMap.put("136970", "268042");
		industryMap.put("202506", "136970");

		industryMap.put("71690", "6154");
		industryMap.put("137226", "6154");
		industryMap.put("399370", "6154");
		industryMap.put("202762", "6154");
		industryMap.put("333834", "6154");
		industryMap.put("268298", "6154");

		return industryMap;
	}

	private List<String> getNegativeTitles(CampaignCriteria campaignCriteria) {
		List<String> negativeTitles = new ArrayList<String>();
		List<String> cNegativeTitles = new ArrayList<String>();
		List<String> retNegativeTitles = new ArrayList<String>();
		if (campaignCriteria.getNegativeKeyWord() != null) {
			negativeTitles = Arrays.asList(campaignCriteria.getNegativeKeyWord().split(","));
			for (String negtit : negativeTitles) {
				cNegativeTitles.add(negtit.toLowerCase());
			}
		}
		if (!campaignCriteria.isIgnoreDefaultTitles()) {
			cNegativeTitles.add("coordinator");
			cNegativeTitles.add("interim");
			cNegativeTitles.add("program");
			cNegativeTitles.add("special");
			cNegativeTitles.add("assistant");
			cNegativeTitles.add("associate");
			cNegativeTitles.add("part time");
			cNegativeTitles.add("supervisor");
			cNegativeTitles.add("program");
			cNegativeTitles.add("lead");
			cNegativeTitles.add("head");
			cNegativeTitles.add("intern");
			cNegativeTitles.add("consulting");
			cNegativeTitles.add("consultant");
			cNegativeTitles.add("temp");
		}

		/*
		 * if(negativeTitles!=null && negativeTitles.size()>0){ for(String neg :
		 * negativeTitles){ retNegativeTitles.add(neg.toLowerCase()); } }
		 */

		for (String negitive : cNegativeTitles) {
			retNegativeTitles.add(negitive.toLowerCase());
		}

		return retNegativeTitles;

	}

	/*
	 * private void replenishData(AbmList abmList,Campaign campaign ,boolean
	 * isInternalCampaign){ List<KeyValuePair<String, String>> companiesWithIds =
	 * new ArrayList<KeyValuePair<String,String>>(); try{ List<KeyValuePair<String,
	 * String>> companies = new ArrayList<KeyValuePair<String,String>>();
	 * if(abmList!=null && abmList.getCompanyList()!=null&&
	 * abmList.getCompanyList().size()>0) companies = abmList.getCompanyList();
	 * Map<String,String> existingCompanies = new HashMap<String,String>();
	 * if(companies!=null && companies.size()>0) for(KeyValuePair<String,String>
	 * keyvaluep : companies){ existingCompanies.put(keyvaluep.getKey(),
	 * keyvaluep.getValue()); }
	 * 
	 * ZoomInfoResponse compResponse = null; Map<String,String> prospectCompanies =
	 * new HashMap<String,String>(); List<Integer> callRetryList = new
	 * ArrayList<Integer>(); callRetryList.add(144); callRetryList.add(144144);
	 * List<ProspectCallLog> rProspectList =
	 * prospectCallLogRepository.findAllRecordByCampaignId(campaign.getId());
	 * 
	 * for(ProspectCallLog rpcl : rProspectList){ String companyName = "";
	 * if(rpcl.getProspectCall().getProspect().getCustomAttributes()!=null &&
	 * rpcl.getProspectCall().getProspect().getCustomAttributeValue("domain") !=null
	 * && !rpcl.getProspectCall().getProspect().getCustomAttributeValue("domain").
	 * toString().isEmpty()){ companyName =
	 * rpcl.getProspectCall().getProspect().getCustomAttributeValue("domain").
	 * toString(); }else if(rpcl.getProspectCall().getProspect().getCompany()!=null
	 * && !rpcl.getProspectCall().getProspect().getCompany().isEmpty()){ companyName
	 * = rpcl.getProspectCall().getProspect().getCompany(); }
	 * prospectCompanies.put(companyName,
	 * rpcl.getProspectCall().getProspect().getSourceCompanyId()); }
	 * for(Map.Entry<String, String> distinctComp : prospectCompanies.entrySet()){
	 * String existingCompanyId = "";
	 * 
	 * if(existingCompanies.containsKey(distinctComp.getKey())){ existingCompanyId =
	 * existingCompanies.get(distinctComp.getKey()); }
	 * if(existingCompanyId.isEmpty() && !isInternalCampaign) compResponse =
	 * getCompanyResponse(distinctComp.getKey(),distinctComp.getValue(),campaign.
	 * getId());
	 * 
	 * if(compResponse!=null &&
	 * compResponse.getCompanySearchResponse().getMaxResults()>0){ String
	 * zoomCompanyId = compResponse.getCompanySearchResponse()
	 * .getCompanySearchResults().getCompanyRecord().get(0).getCompanyId();
	 * KeyValuePair<String, String> kv = new KeyValuePair<String, String>();
	 * kv.setKey(distinctComp.getKey()); kv.setValue(zoomCompanyId);
	 * companiesWithIds.add(kv); }else{ KeyValuePair<String, String> kv = new
	 * KeyValuePair<String, String>(); kv.setKey(distinctComp.getKey());
	 * if(!existingCompanyId.isEmpty()){ kv.setValue(existingCompanyId);
	 * kv.setValue(NOT_FOUND); }else if(isInternalCampaign){
	 * kv.setValue(distinctComp.getKey()); String cVal =
	 * prospectCompanies.get(distinctComp.getKey()); kv.setValue(cVal); }else{
	 * kv.setValue(distinctComp.getKey()); kv.setValue(NOT_FOUND); }
	 * 
	 * companiesWithIds.add(kv); } }
	 * 
	 * 
	 * abmList.setCompanyList(companiesWithIds); //abmList.setReplenish(true);
	 * abmListServiceImpl.saveABMList(abmList, companiesWithIds);
	 * 
	 * logger.debug("ABM BUY COMPLETED");
	 * 
	 * }catch(Exception e){ abmList.setCompanyList(companiesWithIds);
	 * abmListServiceImpl.saveABMList(abmList, companiesWithIds);
	 * e.printStackTrace(); } }
	 */
	private ZoomInfoResponse getCompanyResponse(String companyName, String companyId, String campaignId) {
		ZoomInfoResponse companyZoomInfoResponse = new ZoomInfoResponse();

		try {
			StringBuilder queryString = createCompanyCriteria(companyName, companyId, companyId);

			String previewUrl = buildCompanyUrl(queryString.toString(), null);
			logger.debug("\n previewUrl3 : " + previewUrl);
			URL url = new URL(previewUrl);
			// Thread.sleep(100);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", "Mozilla 5.0 (Windows; U; " + "Windows NT 5.1; en-US; rv:1.8.0.11) ");
			// int responseCode= connection.getResponseCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputline;
			StringBuffer response = new StringBuffer();
			while ((inputline = bufferedReader.readLine()) != null) {
				response.append(inputline);
			}
			bufferedReader.close();
			companyZoomInfoResponse = getResponse(response, true);
			return companyZoomInfoResponse;
		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId,
					DataBuyQueueStatus.INPROCESS.toString());
			// dbqe.setPurchasedCount(buyRowCount);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			if (sw != null) {
				String exceptionAsString = sw.toString();
				// logger.debug(exceptionAsString);
				dbqe.setErrorMsg(exceptionAsString);
			}
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}
		return companyZoomInfoResponse;

	}

	private StringBuilder createCompanyCriteria(String companyName, String companyId, String campaignId) {

		StringBuilder queryString = new StringBuilder();

		try {

			if (companyId != null) {
				queryString.append("companyIds=" + URLEncoder.encode(companyId, XtaasConstants.UTF8));
			} else if (companyName != null) {
				queryString.append("companyName=" + URLEncoder.encode(companyName, XtaasConstants.UTF8));
			}

			// queryString.append("&Country="+URLEncoder.encode(country,
			// XtaasConstants.UTF8));

		} catch (Exception e) {
			DataBuyQueue dbqe = dataBuyQueueRepository.findByCampaignIdAndStatus(campaignId,
					DataBuyQueueStatus.INPROCESS.toString());
			// dbqe.setPurchasedCount(buyRowCount);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// logger.debug(exceptionAsString);
			dbqe.setErrorMsg(exceptionAsString);
			// dbqe.setJobEndTime(new Date());
			if (dbqe.getJobretry() >= 3) {
				dbqe.setStatus(DataBuyQueueStatus.ERROR.toString());
				dbqe.setJobEndTime(new Date());
			} else {
				dbqe.setJobretry(dbqe.getJobretry() + 1);
				dbqe.setStatus(DataBuyQueueStatus.PAUSEDZOOM.toString());
				dbqe.setInterupted(true);
			}
			dataBuyQueueRepository.save(dbqe);
			e.printStackTrace();
		}
		return queryString;

	}

	/*
	 * public SuppressionList getGlobalSuppressionListByCampaignId(SuppressionList
	 * supressionList, String campaignId){ Campaign campaign =
	 * campaignRepository.findOne(campaignId); SuppressionList gSupressionList =
	 * suppressionListRepository.findGlobalSupressionListByOrganization(campaign.
	 * getOrganizationId()); if(supressionList!=null){ List<String> cDomains =
	 * supressionList.getDomains(); List<String> cCompanies =
	 * supressionList.getCompanies(); List<String> cEmails =
	 * supressionList.getEmailIds(); if(gSupressionList!=null){ if(cDomains==null)
	 * cDomains = new ArrayList<String>(); if(cCompanies==null) cCompanies = new
	 * ArrayList<String>(); if(cEmails==null) cEmails = new ArrayList<String>();
	 * if(gSupressionList.getDomains()!=null &&
	 * gSupressionList.getDomains().size()>0 )
	 * cDomains.addAll(gSupressionList.getDomains());
	 * if(gSupressionList.getCompanies()!=null &&
	 * gSupressionList.getCompanies().size()>0 )
	 * cCompanies.addAll(gSupressionList.getCompanies());
	 * if(gSupressionList.getEmailIds()!=null &&
	 * gSupressionList.getEmailIds().size()>0 )
	 * cEmails.addAll(gSupressionList.getEmailIds());
	 * supressionList.setDomains(cDomains); supressionList.setCompanies(cCompanies);
	 * supressionList.setEmailIds(cEmails); } } return supressionList; }
	 */

	private void insertAbmListDetail(ZoomInfoResponse companyResponse) {
		AbmListDetail abm = new AbmListDetail();
		if (companyResponse != null && companyResponse.getCompanySearchResponse().getMaxResults() > 0) {
			String zoomCompanyId = companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
					.get(0).getCompanyId();

			// TODO add all the repsonse from company
			abm.setCompanyName(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyName());
			abm.setCompanyId(zoomCompanyId);
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyAddress() != null) {
				CompanyAddress ca = new CompanyAddress();
				ca = companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanyAddress();
				abm.setCompanyAddress(ca);
			}
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyDescription() != null)
				abm.setCompanyDescription(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyDescription());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyDetailXmlUrl() != null)
				abm.setCompanyDetailXmlUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyDetailXmlUrl());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyEmployeeCount() != null)
				abm.setCompanyEmployeeCount(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyEmployeeCount());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyEmployeeCountRange() != null)
				abm.setCompanyEmployeeCountRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyEmployeeCountRange());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyIndustry() != null)
				abm.setCompanyIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyIndustry());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyNAICS() != null)
				abm.setCompanyNAICS(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyNAICS());
			/*
			 * if(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getCompanyName()!=null)
			 * abm.setZoomCompanyName(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getCompanyName());
			 */
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyProductsAndServices() != null)
				abm.setCompanyProductsAndServices(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyProductsAndServices());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRanking() != null)
				abm.setCompanyRanking(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyRanking());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenueIn000s() != null)
				abm.setCompanyRevenueIn000s(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRevenueIn000s());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenue() != null)
				abm.setCompanyRevenue(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyRevenue());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyRevenueRange() != null)
				abm.setCompanyRevenueRange(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyRevenueRange());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanySIC() != null)
				abm.setCompanySIC(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
						.getCompanySIC());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyTicker() != null)
				abm.setCompanyTicker(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyTicker());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyTopLevelIndustry() != null)
				abm.setCompanyTopLevelIndustry(companyResponse.getCompanySearchResponse().getCompanySearchResults()
						.getCompanyRecord().get(0).getCompanyTopLevelIndustry());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyType() != null)
				abm.setCompanyType(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyType());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getCompanyWebsite() != null)
				abm.setCompanyWebsite(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getCompanyWebsite());
			abm.setDefunct(
					companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0).isDefunct());
			/*
			 * if(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getHashtags()!=null)
			 * abm.setHashtags(companyResponse.getCompanySearchResponse()
			 * .getCompanySearchResults().getCompanyRecord().get(0).getHashtags());
			 */
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getPhone() != null)
				abm.setPhone(
						companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0).getPhone());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getSimilarCompanies() != null)
				abm.setSimilarCompanies(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getSimilarCompanies());
			if (companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord().get(0)
					.getZoomCompanyUrl() != null)
				abm.setZoomCompanyUrl(companyResponse.getCompanySearchResponse().getCompanySearchResults().getCompanyRecord()
						.get(0).getZoomCompanyUrl());

			abmListDetailRepository.save(abm);
		} else {
			abm.setCompanyId("NOT_FOUND");
			// abmListDetailRepository.save(abm);
		}

	}


	public boolean validateSearchData(PersonRecord personRecord, CampaignCriteria campaignCriteria, Campaign campaign,
									  Map<String, Boolean> validationMap, Prospect prospect) {
				boolean isValid = true;
				/*if(campaign.isABM()){
					return true;
				}*/

				if (personRecord != null) {
					try {
					   if (!isFirstAndLastNameValid(personRecord)) { // check if the record belong to Country US
							isValid =false;
						} else if (validationMap.get("title") != null && validationMap.get("title")  && !isPersonTitleKeywordExist(campaignCriteria, personRecord)) { // check if the record belong to
							isValid =false;
						} else if (validationMap.get("management") != null && validationMap.get("management") && !checkManagementLevel(personRecord, campaignCriteria)) { // check if person is already bought
							isValid =false;
						} else if (validationMap.get("department") != null && validationMap.get("department") && !isDepartmentMatch(personRecord, campaignCriteria)) { // check if person is already bought by
							isValid =false;
						}else if (validationMap.get("industry") != null && validationMap.get("department") && !isIndustryValid(personRecord, campaignCriteria)) { // check if person is already bought by
						   isValid =false;
					   }  else if (isCompanySuppressed(personRecord, campaignCriteria)) { // check if person is already bought by
							isValid =false;
						} else if (!isCompanyLimitReached(personRecord, campaignCriteria)) { // check if person is already bought
							isValid =false;
						} else if (validationMap.get("company") != null && validationMap.get("company") && !isValidCompany(prospect,personRecord)) { // check if person is already bought by
						   isValid =false;
					   }
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

		return isValid;
	}

	public boolean isValidCompany(Prospect prospect, PersonRecord personRecord){
		boolean isValid = false;
		if(personRecord.getCurrentEmployment() != null && personRecord.getCurrentEmployment().getCompany() != null &&
				StringUtils.isNotBlank(personRecord.getCurrentEmployment().getCompany().getCompanyName())){
			String personRecordCompany = personRecord.getCurrentEmployment().getCompany().getCompanyName().toLowerCase();
			if(prospect != null && StringUtils.isNotBlank(prospect.getCompany())){
				String prospectCompany = prospect.getCompany().toLowerCase();
				if(personRecordCompany.contains(prospectCompany)){
					return true;
				}
			}
		}
		return  isValid;
	}

	public boolean isValidDomain(Prospect pr, CampaignCriteria cc, Campaign campaign){
		if(!campaign.isABM()){
			return true;
		}
		boolean isValid  = false;
		if(pr != null && pr.getCustomAttributes()!= null && pr.getCustomAttributes().get("domain") != null &&
				pr.getCustomAttributes().get("domain") != "" &&  pr.getCustomAttributes().get("domain").toString() != ""){
			String prospectDomain = pr.getCustomAttributes().get("domain").toString();
			List<String> abmDomains = cc.getAbmDomains();
			if(CollectionUtils.isNotEmpty(abmDomains)){
				String pDomain = getExactHostName(prospectDomain);
				for(String domain : abmDomains){
					String dbDomain = getExactHostName(domain);
					if(pDomain.equalsIgnoreCase(dbDomain)){
						return true;
					}
				}
			}
			List<String> abmCompanys = cc.getAbmCompanys();
			if(CollectionUtils.isNotEmpty(abmCompanys)){
				String pDomain = getExactHostName(prospectDomain);
				for(String companyName : abmCompanys){
					try{
						String trimCompany = removeSpace(companyName);
						String dbCompany = getExactHostName(trimCompany);
						if(pDomain.equalsIgnoreCase(dbCompany)){
							return true;
						}
					}catch (Exception e){
						e.printStackTrace();
					}

				}
			}
		}
		return isValid;
	}

	public String removeSpace(String str){
		String val = str.trim().replaceAll(" ", "");
		val = val.toLowerCase();
		return val;
	}

	public static String getExactHostName(String url) {
		String val  = getHostName(url);
		String host = val.split("\\.")[0];
		return host;
	}

	public static String getHostName(String url) {
		if (url != null && !url.isEmpty()) {
			url = url.toLowerCase();
			if (url.contains("http://") || url.contains("https://") || url.contains("HTTP://")
					|| url.contains("HTTPS://")) {

			} else {
				url = "http://" + url;
			}
			try {
				URI uri = new URI(url);
				String hostname = uri.getHost();
				if (hostname != null && !hostname.isEmpty()) {
					if (hostname.contains("www.") || hostname.contains("WWW.")) {
						return hostname = hostname.substring(4, hostname.length());
					} else {
						return hostname;
					}
				}
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}


	public boolean isValidCountry(CampaignCriteria cc, PersonRecord prospectObj) {
		boolean isValid = false;
		if(prospectObj.getLocalAddress() != null && prospectObj.getLocalAddress().getCountry() != null){
			String prospectCountry = prospectObj.getLocalAddress().getCountry();
			List<String> ccCountryList = cc.getCountryList();
			List<String> newList  = new ArrayList<>();
			newList.addAll(ccCountryList);
			boolean containsUSA = newList.stream().anyMatch("usa"::equalsIgnoreCase);
			boolean containsUnitedStates = newList.stream().anyMatch("United States"::equalsIgnoreCase);
			if(containsUSA){
				newList.add("United States");
			}else if(containsUnitedStates){
				newList.add("usa");
			}
			boolean isValidCntry = newList.stream().anyMatch(prospectCountry::equalsIgnoreCase);
			if(isValidCntry){
				isValid = true;
			}
		}

		return isValid;
	}

	public boolean isValidCountryPhone(Prospect prospectObj) {
		boolean isValid = false;
		if(prospectObj != null && prospectObj.getPhone()!= null && prospectObj.getCountry() != null){
			String phone  =  prospectObj.getPhone();
			String tempPhone = phone.replaceAll("[^A-Za-z0-9]", "");
			String prospectCountry = prospectObj.getCountry().toLowerCase();
			Integer isdCode = numberService.getIsdCodeFromCountryName(prospectCountry);
			if (isdCode != null && tempPhone.startsWith(Integer.toString(isdCode))) {
				isValid = true;
			}
		}
		return isValid;
	}

	public String getCompanyFromEmail(String email){
		if(StringUtils.isNotBlank(email)){
			String val = email.substring(email.indexOf("@") + 1);
			val =  val.substring(0, val.indexOf("."));
			return val;
		}
		return null;
	}


	public void saveGlobalContact(Prospect prospect){
		try {
			ProspectCall prospectCall = new ProspectCall();
			prospectCall.setProspect(prospect);
			GlobalContact globalContact = new GlobalContact();
			globalContact = globalContact.toGlobalContact(prospectCall);
			globalContactRepository.save(globalContact);
		}catch (DuplicateKeyException duplicateKeyException){
			GlobalContact gContact = globalContactRepository.findUniqueRecordByFLP(prospect.getFirstName(), prospect.getLastName(), prospect.getPhone());
			ProspectCall prospectCallExisting = new ProspectCall();
			prospectCallExisting.setProspect(prospect);
			gContact.toGlobalContact(prospectCallExisting);
			globalContactRepository.save(gContact);
		}
		catch (Exception e){
			//e.printStackTrace();
		}
	}

	/*
	 * Check phone DNC in local DNC list if not present then check with third-party
	 */
	private boolean checkDNC(Prospect prospect, Campaign campaign) {
		boolean isDNC = false;
		boolean isLocalDNC = internalDNCListService.checkLocalDNCList(prospect.getPhone(),
				prospect.getFirstName(), prospect.getLastName());
		String pospectCountry = !StringUtils.isEmpty(prospect.getCountry())
				? prospect.getCountry().toUpperCase()
				: "US";
		if (isLocalDNC) {
			isDNC = isLocalDNC;
		} else if (campaign.isUsPhoneDncCheck() && XtaasConstants.US_COUNTRY.contains(pospectCountry)
				&& phoneDncService.usPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		} else if (campaign.isUkPhoneDncCheck() && XtaasConstants.UK_COUNTRY.contains(pospectCountry)
				&& phoneDncService.ukPhoneDncCheck(prospect, campaign)) {
			isDNC = true;
		}
		return isDNC;
	}

}