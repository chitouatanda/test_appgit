package com.xtaas.infra.zoominfo.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.utils.XtaasDateUtils;

@Service
public class RemoveSuccessDuplicates {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RemoveSuccessDuplicates.class);
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	private static final String DUPLICATELEADS = "DUPLICATELEADS";
	private static final String DUPLICATECOMPANY = "DUPLICATECOMPANY";
	//private static final String LEADSPERCOMPANY = "LEADSPERCOMPANY";
	
	   public Long removeSuccessLeads(Campaign campaign) throws ParseException {
		   	//List<ProspectCallLog> toUpdate = new ArrayList<ProspectCallLog>();
		   	//Map<String,List<ProspectCallLog>> toUpdateMap = new HashMap<String, List<ProspectCallLog>>();
		   	//List<ProspectCallLog> cloneProspectCallLogList = new ArrayList<ProspectCallLog>();
		    List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		    Long total = new Long(0);
		    
		    String duplicateLeadDate = "";
		    String duplicateCompanyDate = "";
		    List<String> campaignIds = new ArrayList<String>();
		    String cid = campaign.getId();
		    
			if (campaign.getCampaignGroupIds() != null && campaign.getCampaignGroupIds().size() > 0) {
				campaignIds.addAll(campaign.getCampaignGroupIds());
			} else {
				campaignIds.add(cid);
			}
		    
			Long dltotal = new Long(0);
			Long dctotal = new Long(0);
		    boolean isCheck =false;
		    if(campaign.getDuplicateLeadsDuration()>0){
		    	duplicateLeadDate = getStartDate(String.valueOf(campaign.getDuplicateLeadsDuration() + " Days"));
		    	prospectCallLogList = getCallables(campaignIds);
		    	dltotal = getDuplicateDuration(campaign,campaignIds,duplicateLeadDate,prospectCallLogList,DUPLICATELEADS);
		    	isCheck =true;
		    }
		    
		    if(campaign.getDuplicateCompanyDuration()>0){
		    	duplicateCompanyDate = getStartDate(String.valueOf(campaign.getDuplicateCompanyDuration() + " Days"));
		    	if(prospectCallLogList!=null && prospectCallLogList.size()>0){
		    		//Do Nothing
		    	}else{
		    		prospectCallLogList = getCallables(campaignIds);
		    	}
		    	dctotal = getDuplicateDuration(campaign,campaignIds,duplicateLeadDate,prospectCallLogList,DUPLICATECOMPANY);
		    	isCheck =true;
		    }
		    if(!isCheck)
		    	return total;
		    
		    if(dltotal==null){
		    	dltotal = new Long(0);
		    }
		    if(dctotal==null){
		    	dctotal = new Long(0);
		    }
		    
		    total = dltotal+dctotal;
		    
		    return total;
		 }
	   
	   private String getStartDate(String periodActual){
		   
		 //String toDate = "02/10/2018 00:00:00";
           // mm/dd/yyyy
		   String toDate="";
		   Calendar c = Calendar.getInstance();
		   logger.debug("Current date : " + (c.get(Calendar.MONTH) + 1) +
				   "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));

	    	 int beginIndex = periodActual.indexOf(" ");
	    	 String periodStr = periodActual.substring(0, beginIndex);
	    	 int period = Integer.parseInt(periodStr);
	    	 //logger.info(period);
		   
		   
		   
		   if(periodActual.contains("Years")){
			   c.add(Calendar.YEAR, -period);
			   toDate = (c.get(Calendar.MONTH) + 1) +
					   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(periodActual.contains("Months")){
	    		  c.add(Calendar.MONTH, -period);
	    		  toDate = (c.get(Calendar.MONTH) + 1) +
	   				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(periodActual.contains("Weeks")){
	    		 c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(periodActual.contains("Days")){
	    		 c.add(Calendar.DAY_OF_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}
		   
		
		  
		   return toDate;
				   
				   
	   }
	   
	   private List<ProspectCallLog> getCallables(List<String> campaignIds){
		   List<ProspectCallLog> prospectCallLogList = new ArrayList<ProspectCallLog>();
		   
		   try{
			  
			   
		    //System.out.println(startDateInUTC);
			   List<ProspectCallLog> prospectCallLogList0 = prospectCallLogRepository.findContactsByQueuedByCampaigns(campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList0);
			   List<ProspectCallLog> prospectCallLogList1 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(1,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList1);
			   List<ProspectCallLog> prospectCallLogList2 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(2,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList2);
			   List<ProspectCallLog> prospectCallLogList3 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(3,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList3);
			   List<ProspectCallLog> prospectCallLogList4 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(4,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList4);
			   List<ProspectCallLog> prospectCallLogList5 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(5,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList5);
			   List<ProspectCallLog> prospectCallLogList6 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(6,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList6);
			   List<ProspectCallLog> prospectCallLogList7 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(7,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList7);
			   List<ProspectCallLog> prospectCallLogList8 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(8,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList8);
			   List<ProspectCallLog> prospectCallLogList9 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(9,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList9);
			   List<ProspectCallLog> prospectCallLogList10 = prospectCallLogRepository.findContactsByRetryCountByCampaigs(10,campaignIds);
			   prospectCallLogList.addAll(prospectCallLogList10);
		   }catch(Exception e){
			   e.printStackTrace();
		   }
		   return prospectCallLogList;

	   }
	   
	   private Long getDuplicateDuration(Campaign campaign,List<String> cids,String date,List<ProspectCallLog> prospectCallLogList,String type){
		  
		    List<ProspectCallLog> prospectCallLogSuccessList = new ArrayList<ProspectCallLog>();
		    Map<String,ProspectCallLog> prospectCallLogMap = new HashMap<String,ProspectCallLog>();
		    List<ProspectCallLog> toRemoveList = new ArrayList<ProspectCallLog>();
		    
		    try {
			   	 Date startDate = XtaasDateUtils.getDate(date, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
				 Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
				 List<ProspectCallLog> prospectCallLogListSUCCESS = prospectCallLogRepository.findContactsBySuccessByCampaigns(cids, startDateInUTC);
				 prospectCallLogSuccessList.addAll(prospectCallLogListSUCCESS);
				 Map<String, ProspectCallLog> prospectCallSuccessMap = new HashMap<String,ProspectCallLog>();
				 Map<String, List<ProspectCallLog>> prospectCallSuccessCompanyMap = new HashMap<String,List<ProspectCallLog>>();
				 for(ProspectCallLog pclSuccess : prospectCallLogSuccessList){
					 if(type.equalsIgnoreCase("DUPLICATELEADS")){
						 String key = pclSuccess.getProspectCall().getProspect().getFirstName()+"_"
								 +pclSuccess.getProspectCall().getProspect().getLastName()+"_"
								+ pclSuccess.getProspectCall().getProspect().getPhone();
						 if(prospectCallSuccessMap.containsKey(key)){
							 pclSuccess.getProspectCall().setCallRetryCount(111111);
							 pclSuccess.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							 toRemoveList.add(pclSuccess);
						 }else {
							 prospectCallSuccessMap.put(key, pclSuccess);//This is successMap
						 }
					 }
					 if(type.equalsIgnoreCase("DUPLICATECOMPANY")){
						 String key = pclSuccess.getProspectCall().getProspect().getCompany();
						 List<ProspectCallLog> successPCLs = prospectCallSuccessCompanyMap.get(key);
						 if(successPCLs!=null && successPCLs.size()>0){
							 successPCLs.add(pclSuccess);
							 prospectCallSuccessCompanyMap.put(key, successPCLs);//This is successMap
						 }else{
							 successPCLs=new ArrayList<ProspectCallLog>();
							 successPCLs.add(pclSuccess);
							 prospectCallSuccessCompanyMap.put(key, successPCLs);//This is successMap
						 }
						 
						 if(campaign.getLeadsPerCompany()>0 ){
							 if(successPCLs.size() >= campaign.getLeadsPerCompany()){
								 pclSuccess.getProspectCall().setCallRetryCount(111111);
								 pclSuccess.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
								 toRemoveList.add(pclSuccess);
							 }
						 }
					 }
				 }
				// for(ProspectCallLog pcl : prospectCallLogSuccessList){
					/* if(type.equalsIgnoreCase("DUPLICATELEADS")){
						 String key = pcl.getProspectCall().getProspect().getFirstName()+"_"
								 +pcl.getProspectCall().getProspect().getLastName()+"_"
								+ pcl.getProspectCall().getProspect().getPhone();
						 prospectCallLogMap.put(key, pcl);//This is callable map
						 if(prospectCallSuccessMap.containsKey(key)){
							 pcl.getProspectCall().setCallRetryCount(111111);
							 pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
							 toRemoveList.add(pcl);
						 }
					 }*/
					/* if(type.equalsIgnoreCase("DUPLICATECOMPANY")){
						 String key = pcl.getProspectCall().getProspect().getCompany();
						 prospectCallLogMap.put(key, pcl);//This is callable map
						 if(prospectCallSuccessCompanyMap.containsKey(key)){
							 List<ProspectCallLog> prospectCompanyList = prospectCallSuccessCompanyMap.get(key);
							 if(campaign.getLeadsPerCompany()>0 ){
								 if(prospectCompanyList.size() >= campaign.getLeadsPerCompany()){
									 pcl.getProspectCall().setCallRetryCount(111111);
									 pcl.setStatus(ProspectCallStatus.MAX_RETRY_LIMIT_REACHED);
									 toRemoveList.add(pcl);
								 }
							 }
						 }
					 }*/
				// }
				 logger.debug("Please wait we are updating Record, It will take some time ......");
		         //prospectCallLogRepository.saveAll(toRemoveList);
		         logger.debug("duplicates found :"+toRemoveList.size());
		         return new Long(toRemoveList.size());
	 
		    } catch (Exception e) {
				e.printStackTrace();
		}
	   	return  new Long(0);
    
	   }
	   	   
	   
}
