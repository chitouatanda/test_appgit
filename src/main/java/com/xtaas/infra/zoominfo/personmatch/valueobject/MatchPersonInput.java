package com.xtaas.infra.zoominfo.personmatch.valueobject;

public class MatchPersonInput {
	
	private String uniqueInputId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String companyName;
	public String getUniqueInputId() {
		return uniqueInputId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getCompanyName() {
		return companyName;
	}
}
