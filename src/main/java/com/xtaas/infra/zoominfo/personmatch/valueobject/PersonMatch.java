package com.xtaas.infra.zoominfo.personmatch.valueobject;

import java.util.List;

public class PersonMatch {
	private String personId;
	private String zoomPersonUrl;
	private String personDetailXmlUrl;
	private String lastMentioned;
	private String isUserPosted;
	private String isPast;
	private String imageUrl;
	private int referencesCount;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String fax;
	private CurrentEmployment currentEmployment;
	private LocalAddress localAddress;
	private List<String> industry;
	
	public List<String> getIndustry() {
		return industry;
	}
	public LocalAddress getLocalAddress() {
		return localAddress;
	}
	public CurrentEmployment getCurrentEmployment() {
		return currentEmployment;
	}
	
	public String getPersonId() {
		return personId;
	}
	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}
	public String getPersonDetailXmlUrl() {
		return personDetailXmlUrl;
	}
	public String getLastMentioned() {
		return lastMentioned;
	}
	public String getIsUserPosted() {
		return isUserPosted;
	}
	public String getIsPast() {
		return isPast;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public int getReferencesCount() {
		return referencesCount;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmail() {
		return email;
	}
	public String getPhone() {
		return phone;
	}
	public String getFax() {
		return fax;
	}
	
	
}
