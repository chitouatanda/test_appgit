package com.xtaas.infra.zoominfo.personmatch.valueobject;

public class PersonMatchResult {
	
	public MatchPersonInput matchPersonInput;
	
	public PersonMatches personMatches;

	public MatchPersonInput getMatchPersonInput() {
		return matchPersonInput;
	}

	public PersonMatches getPersonMatches() {
		return personMatches;
	}
}
