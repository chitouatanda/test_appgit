package com.xtaas.infra.zoominfo.personmatch.valueobject;

public class LocalAddress {

	private String street;
	private String city;
	private String state;
	private String zip;
	private String countryCode;
	public String getStreet() {
		return street;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public String getZip() {
		return zip;
	}
	public String getCountryCode() {
		return countryCode;
	}
}
