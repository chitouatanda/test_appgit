package com.xtaas.infra.zoominfo.personmatch.valueobject;

import java.util.List;

public class CurrentEmployment {
	
	private String jobTitle;
	private Company company;
	private List<String> managementLevel;
	private List<String> JobFunction;
	
	
	public String getJobTitle() {
		return jobTitle;
	}
	public Company getCompany() {
		return company;
	}
	public List<String> getManagementLevel() {
		return managementLevel;
	}
    public List<String> getJobFunction() {
        return JobFunction;
    }
    public void setJobFunction(List<String> jobFunction) {
        JobFunction = jobFunction;
    }
   
   
}
