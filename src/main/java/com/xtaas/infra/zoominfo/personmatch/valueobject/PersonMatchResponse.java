package com.xtaas.infra.zoominfo.personmatch.valueobject;

public class PersonMatchResponse {
	public InputParameters inputParameters;
	
	public MatchResults matchResults;

	public InputParameters getInputParameters() {
		return inputParameters;
	}

	public MatchResults getMatchResults() {
		return matchResults;
	}

}
