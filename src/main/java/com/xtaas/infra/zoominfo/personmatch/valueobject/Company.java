package com.xtaas.infra.zoominfo.personmatch.valueobject;


public class Company {
	
	private String companyId;
	private String zoomCompanyUrl;
	private String companyDetailXmlUrl;
	private String companyName;
	private String companyWebsite;
	private String companyPhone;
	private CompanyAddress companyAddress;
	private String companyLogo;
	private long companyRevenue;
	private long companyRevenueIn000s;
	private String companyRevenueRange;
	private long companyEmployeeCount;
	private String companyEmployeeCountRange;
	private String directPhone;
	
	public String getDirectPhone() {
		return directPhone;
	}
	public String getCompanyId() {
		return companyId;
	}
	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}
	public String getCompanyDetailXmlUrl() {
		return companyDetailXmlUrl;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public CompanyAddress getCompanyAddress() {
		return companyAddress;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	public long getCompanyRevenue() {
		return companyRevenue;
	}
	public long getCompanyRevenueIn000s() {
		return companyRevenueIn000s;
	}
	public String getCompanyRevenueRange() {
		return companyRevenueRange;
	}
	public long getCompanyEmployeeCount() {
		return companyEmployeeCount;
	}
	public String getCompanyEmployeeCountRange() {
		return companyEmployeeCountRange;
	}
	
}
