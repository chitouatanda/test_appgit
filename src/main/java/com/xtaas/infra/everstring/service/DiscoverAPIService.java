package com.xtaas.infra.everstring.service;

import com.xtaas.web.dto.EverStringRealTimeResponseDTO;

public interface DiscoverAPIService {

	public void companyDiscover(String campaignId);
	
	public EverStringRealTimeResponseDTO getCompanyDomainFromEverString(String companyName);


}
