package com.xtaas.infra.everstring.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.RateLimiter;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.AbmList;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.EverStringCompany;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.ZoomInfoEverstringMapping;
import com.xtaas.db.repository.AbmListNewRepository;
import com.xtaas.db.repository.AbmListRepository;
import com.xtaas.db.repository.EverStringCompanyRepository;
import com.xtaas.db.repository.ZoomInfoEverstringMappingRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.job.EverStringCompanyInfoJob;
import com.xtaas.service.HttpService;
import com.xtaas.service.TrafficManagerService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.Criteria;
import com.xtaas.web.dto.EverStringCompanyDiscoverDTO;
import com.xtaas.web.dto.EverStringCompanyDiscoverResponseDTO;
import com.xtaas.web.dto.EverStringDiscoverDTO;
import com.xtaas.web.dto.EverStringRealTimeRequestDTO;
import com.xtaas.web.dto.EverStringRealTimeResponseDTO;

@Service
public class DiscoverAPIServiceImpl implements DiscoverAPIService,TrafficManagerService {

	private final Logger logger = LoggerFactory.getLogger(DiscoverAPIServiceImpl.class);

	@Autowired
	private EverStringCompanyRepository everStringCompanyRepository;

	@Autowired
	private ZoomInfoEverstringMappingRepository zoomInfoEverstringMappingRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	AbmListNewRepository abmListNewRepository; 
	
	@Autowired
	EverStringCompanyInfoJob everStringCompanyInfoJob;
	
	@Autowired
	HttpService httpService;
	
	private RateLimiter rateLimiter = RateLimiter.create(0.33);

	/*
	 * This method will call when user click on discover company button. here i have
	 * create http-post request. in http-post i have use everstring url and set
	 * header and body based on everstring criteria. in response we get everstring
	 * company list.
	 * 
	 * @param campaignId
	 * 
	 * @param offset - based on offset value we get list of everstring company list
	 * (Example : offset : 2 , we get 2000 company)
	 */
	@Override
	public void companyDiscover(String campaignId) {
		Criteria criteria = getCampaignCriteria(campaignId);
		if (criteria.getEmployeeSize().isEmpty() && criteria.getIndustry().isEmpty()
				&& criteria.getRevenue().isEmpty()) {
			throw new IllegalArgumentException("Campaign criteria is not available for this campaign.");
		}
		HttpResponse response = null;
		String responseString = null;
		List<EverStringCompanyDiscoverDTO> everStringResponseCompany = null;
		List<EverStringCompanyDiscoverDTO> everStringTotalCompanies = new ArrayList<>();
		List<EverStringCompany> everStringlist = new ArrayList<>();
		int dtoOffSet = 0;
		try {
			try {
				// Create Discover API request body
				HttpPost postRequest = new HttpPost(XtaasConstants.DISCOVERURL);
				postRequest.setHeader("Content-Type", "application/json");
				postRequest.setHeader("Authorization", XtaasConstants.TOKEN);
				do {
					HttpClient httpClient = HttpClientBuilder.create().build();
					ObjectMapper objectMapper = new ObjectMapper();
					EverStringDiscoverDTO dto = new EverStringDiscoverDTO();
					dto.setCriteria(criteria);
					dto.setLimit(1000);
					dto.setOffset(dtoOffSet);
					String jsonString = objectMapper.writeValueAsString(dto);
					StringEntity entity = new StringEntity(jsonString);
					postRequest.setEntity(entity);
					response = httpClient.execute(postRequest);
					responseString = EntityUtils.toString(response.getEntity());
					EverStringCompanyDiscoverResponseDTO responseDTO = JSONUtils.toObject(responseString,
							EverStringCompanyDiscoverResponseDTO.class);
					everStringResponseCompany = responseDTO.getCompanies();
					everStringTotalCompanies.addAll(everStringResponseCompany);
					dtoOffSet = dto.getOffset() + 1000;
				} while (everStringResponseCompany != null && everStringResponseCompany.size() > 0);
			} catch (Exception e) {
				logger.error("==========> DiscoverAPIServiceImpl - Error occurred in companyDiscover <==========");
				logger.error("Expection : {} ", e.getMessage());
			}
			List<AbmListNew> addToAbmList = new ArrayList<AbmListNew>();
			List<Integer> listOfCompanyIds = new ArrayList<>();
			List<Integer> listOfDTOIds = new ArrayList<>();
			for (EverStringCompanyDiscoverDTO everStringCompany : everStringTotalCompanies) {
				AbmListNew newABM = new AbmListNew();
				newABM.setCampaignId(campaignId);
				newABM.setCompanyId("GET_COMPANY_ID");
				newABM.setCompanyName(everStringCompany.getDomain());
				newABM.setStatus("ACTIVE");
				listOfCompanyIds.add((int) everStringCompany.getId());
				listOfDTOIds.add((int) everStringCompany.getId());
				everStringlist.add(new EverStringCompany((int) everStringCompany.getId(), everStringCompany.getName(),
						everStringCompany.getDomain(), campaignId, "QUEUED"));
				addToAbmList.add(newABM);
			}

			saveEverStringList(listOfDTOIds, everStringlist, campaignId);

			addABMList(addToAbmList,campaignId);

		} catch (Exception e) {
			logger.error("==========> DiscoverAPIServiceImpl - Error occurred in companyDiscover <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
	}

	/**
	 * get existing list from db and compare and filter distinct records and insert
	 * into db.
	 * 
	 * @param listOfDTOIds
	 * @param everStringlist
	 */
	private void saveEverStringList(List<Integer> listOfDTOIds, List<EverStringCompany> everStringlist,
			String campaignId) {
		List<EverStringCompany> listOfIds = everStringCompanyRepository.findByIdIn(listOfDTOIds);
		if (listOfIds.size() > 0) {
			List<EverStringCompany> listOfCompanyTemp = listOfIds;
			everStringlist = everStringlist.stream()
					.filter(o1 -> listOfCompanyTemp.stream().noneMatch(o2 -> o2.getEcid() == o1.getEcid()))
					.collect(Collectors.toList());
		}

		everStringCompanyRepository.bulkinsert(everStringlist);
		logger.debug("inserted {} Records into EverStringList.", everStringlist.size());
		sendToMail(everStringlist.size(), campaignId);

	}

	private void sendToMail(int total, String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		String dateStr = sdf.format(date);
		String subject = "Everstring data purchase - " + dateStr + " - " + login.getId();
		String message = " You have purchased total : " + total + " company for " + campaign.getName();
		XtaasEmailUtils.sendEmail(login.getEmail(), "data@xtaascorp.com", subject, message);
	}

	/**
	 * get existing list from db and compare and filter noneMatch records and insert
	 * into db.
	 * 
	 * @param everStringlist
	 * @param campaignId
	 */
	private void addABMList(List<AbmListNew> everStringlist,String campaignId) {
		int countOfABM = abmListNewRepository.countABMListNewByCampaignIdAndStatus(campaignId);
		if (countOfABM > 0) {
			List<AbmListNew> abmList = new ArrayList<AbmListNew>();
			for(AbmListNew abmNew : everStringlist) {
				long abm = abmListNewRepository.countByCampaignCompanyName(abmNew.getCampaignId(), abmNew.getCompanyName());
				if(abm > 0) {
					//ignore
				}else {
					abmList.add(abmNew);
				}
			}
			
			saveABMList(abmList);
		}else {
			saveABMList(everStringlist);
		}
	}

	/**
	 * save ABM list if companies.size() > 0
	 * 
	 * @param abmList
	 * @param companies
	 */
	private void saveABMList(List<AbmListNew> abmList) {
		try {
			abmListNewRepository.saveAll(abmList);
			logger.debug("Inserted {} records into ABMList for campaignId {}", abmList.size());
		}catch(Exception  e) {
			e.printStackTrace();
		}
	}

	/**
	 * this method is used to read campaign criteria from campaign
	 * 
	 * @param campaignId
	 * @return
	 */
	private Criteria getCampaignCriteria(String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		QualificationCriteria cc = campaign.getQualificationCriteria();
		List<Expression> expressions = cc.getExpressions();
		List<ZoomInfoEverstringMapping> mappings = zoomInfoEverstringMappingRepository.findAll();
		Criteria criteria = new Criteria();
		for (Expression exp : expressions) {
			criteria = setCriteriaMapping(mappings, exp, criteria);
		}
		return criteria;
	}

	/**
	 * this method is used to map zoominfo criteria with everstring criteria
	 * 
	 * @param mappings
	 * @param exp
	 * @param criteria
	 * @return
	 */
	private Criteria setCriteriaMapping(List<ZoomInfoEverstringMapping> mappings, Expression exp, Criteria criteria) {
		ZoomInfoEverstringMapping mapping = null;
		List<String> listEverString = new ArrayList<>();
		List<ZoomInfoEverstringMapping> filteredMapping = mappings.stream()
				.filter(obj -> obj.getAttrubute().equalsIgnoreCase(exp.getAttribute())).collect(Collectors.toList());
		if (filteredMapping != null && filteredMapping.size() > 0) {
			mapping = filteredMapping.get(0);
			List<String> zoomInfoValues = Arrays.asList(exp.getValue().split(","));
			if (exp.getOperator().equalsIgnoreCase("IN") || exp.getOperator().equalsIgnoreCase("EQUALS")) {
				List<String> mappingValues = mapping.getMapping().stream()
						.filter(obj -> zoomInfoValues.contains(obj.getZoominfo())).map(obj -> obj.getEverstring())
						.collect(Collectors.toList());
				listEverString.addAll(mappingValues);
			} else {
				List<String> zoomInfoValuesNotIn = new ArrayList<>();
				for (PickListItem item : exp.getPickList()) {
					if (!zoomInfoValues.contains(item.getValue())) {
						zoomInfoValuesNotIn.add(item.getValue());
					}
				}
				List<String> mappingValues = mapping.getMapping().stream()
						.filter(obj -> zoomInfoValuesNotIn.contains(obj.getZoominfo())).map(obj -> obj.getEverstring())
						.collect(Collectors.toList());
				listEverString.addAll(mappingValues);
			}
		}

		if (exp.getAttribute().equalsIgnoreCase("REVENUE")) {
			criteria.setRevenue(listEverString);
		} else if (exp.getAttribute().equalsIgnoreCase("EMPLOYEE")) {
			criteria.setEmployeeSize(listEverString);
		} else if (exp.getAttribute().equalsIgnoreCase("INDUSTRY")) {
			criteria.setIndustry(listEverString);
		}
		return criteria;
	}
	
	@Override
	public void acquire() {
		rateLimiter.acquire();
	}
	
	private HttpHeaders headers() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");
		headers.add("Authorization", XtaasConstants.TOKEN);
		return headers;
	}
	
	public EverStringRealTimeResponseDTO getCompanyDomainFromEverString(String companyName) {
		acquire();
		//String domain= null;
		EverStringRealTimeRequestDTO dto = new EverStringRealTimeRequestDTO();
		dto.setName(companyName);
		try {
			EverStringRealTimeResponseDTO responseDTO = httpService.post(XtaasConstants.REALTIMEURL, headers(),
				new ParameterizedTypeReference<EverStringRealTimeResponseDTO>() {
			}, new ObjectMapper().writeValueAsString(dto));
			if(responseDTO != null && responseDTO.getData() != null) {
				//domain = responseDTO.getData().get(0).getES_PrimaryWebsite();
			}
		
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
