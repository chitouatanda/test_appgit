package com.xtaas.infra.everstring.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.db.entity.EverStringCompany;
import com.xtaas.db.entity.EverStringCompanyInfo;
import com.xtaas.db.repository.EverStringCompanyInfoRepository;
import com.xtaas.db.repository.EverStringCompanyRepository;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.EnrichMentMultipleCompanyListDTO;
import com.xtaas.web.dto.EverStringRealTimeCompanyDTO;
import com.xtaas.web.dto.EverStringRealTimeRequestDTO;
import com.xtaas.web.dto.EverStringRealTimeResponseDTO;

@Service
public class EnrichmentAPIServiceImpl {

	private final Logger logger = LoggerFactory.getLogger(EnrichmentAPIServiceImpl.class);

	@Autowired
	private EverStringCompanyRepository everStringCompanyRepository;

	@Autowired
	EverStringCompanyInfoRepository everStringCompanyInfoRepository;

	/**
	 * Get Everstring company list which status=QUEUE and campaignId. than convert
	 * into 25 batch arraylist and call multiple records API after call api get
	 * companyinfo list and save into db. then set everstring company status=SUCCESS
	 * 
	 * @param campaignId
	 */

	public void callEnrichmentAPIByCampaignIdAndStatus(String campaignId,boolean flag) {
		List<EverStringRealTimeCompanyDTO> totalResponseCompany = new ArrayList<>();
		List<EverStringCompany> everStringCompanyList =  new ArrayList<>();
		List<Long> totalSeconds = new ArrayList<>();
		List<Integer> totalcount = new ArrayList<>();
		if (flag) {
			everStringCompanyList = everStringCompanyRepository.findByStatusAndIds("QUEUED",campaignId);
		}else {
			everStringCompanyList = everStringCompanyRepository.findByStatus("QUEUED");
		}
		
		if (everStringCompanyList.size() > 0) {
			List<EverStringRealTimeRequestDTO> everStringCompanyInfoIds = new ArrayList<>();

			everStringCompanyList
					.forEach(obj -> everStringCompanyInfoIds.add(new EverStringRealTimeRequestDTO(obj.getEcid())));

			noOfBatches(everStringCompanyInfoIds, 25).forEach(
					listOfIds -> callMultipleRecordsAPIList(listOfIds, totalResponseCompany, totalSeconds, totalcount));

			boolean status = saveCompanyInfoList(totalResponseCompany);
			if (status) {
				List<Integer> ids = everStringCompanyList.stream().map(obj -> obj.getEcid())
						.collect(Collectors.toList());
				List<EverStringCompany> companies = everStringCompanyRepository.findByIdIn(ids);
				companies.forEach(obj -> obj.setStatus("SUCCESS"));
				everStringCompanyRepository.saveAll(companies);
				logger.debug("Updated {} records into everStringcompany for campaignId {}", companies.size(),
						campaignId);
			}
		}
	}

	/**
	 * call multiple records API list in 25 batch array list than add into
	 * totalResponseCompany list
	 * 
	 * @param listOfIds
	 * @param totalResponseCompany
	 */
	public void callMultipleRecordsAPIList(List<EverStringRealTimeRequestDTO> listOfIds,
			List<EverStringRealTimeCompanyDTO> totalResponseCompany, List<Long> totalSeconds,
			List<Integer> totalcount) {
		try {
			long start = System.currentTimeMillis();
			HttpPost postRequest = new HttpPost(XtaasConstants.REALTIMEURL);
			postRequest.setHeader("Content-Type", "application/json");
			postRequest.setHeader("Authorization", XtaasConstants.TOKEN);
			HttpClient httpClient = HttpClientBuilder.create().build();
			ObjectMapper objectMapper = new ObjectMapper();
			EnrichMentMultipleCompanyListDTO dto = new EnrichMentMultipleCompanyListDTO();
			dto.setCompanies(listOfIds);
			String jsonString = objectMapper.writeValueAsString(dto);
			StringEntity entity = new StringEntity(jsonString);
			postRequest.setEntity(entity);
			HttpResponse response = httpClient.execute(postRequest);
			String responseString = EntityUtils.toString(response.getEntity());
			EverStringRealTimeResponseDTO responseDTO = JSONUtils.toObject(responseString,
					EverStringRealTimeResponseDTO.class);
			List<EverStringRealTimeCompanyDTO> responseCompanyList = responseDTO.getData();
			totalResponseCompany.addAll(responseCompanyList);

			long end = System.currentTimeMillis();
			long diff = end - start;
			totalSeconds.add(diff);
			totalcount.add(1);
			long sumOfSecond = totalSeconds.stream().mapToLong(Long::longValue).sum();
			int sumOfCount = totalcount.stream().mapToInt(Integer::intValue).sum();
			System.out.println("Start : " + getDate() + " Size totalResponseCompany : " + totalResponseCompany.size()
					+ "  sumOfCount : " + sumOfCount + "  sumOfSecond : "
					+ TimeUnit.MILLISECONDS.toSeconds(sumOfSecond));
			if (sumOfCount == 20) {
				System.out.println("Sleep");
				Thread.sleep(60000 - sumOfSecond);
				totalSeconds.clear();
				totalcount.clear();
			}
		} catch (Exception e) {
			logger.error(
					"==========> EnrichmentAPIServiceImpl - Error occurred in callRealTimeEnrichMentAPI <==========");
			logger.error("Expection : {} ", e.getMessage());
		}
	}

	/**
	 * saveCompanyInfoList into db
	 * 
	 * @param companyDTO
	 * @param campaignId
	 * @return
	 */
	private boolean saveCompanyInfoList(List<EverStringRealTimeCompanyDTO> companyDTO) {
		List<EverStringCompanyInfo> everStringCompanyInfo = new ArrayList<>();
		for (EverStringRealTimeCompanyDTO realTimeCompany : companyDTO) {
			EverStringCompanyInfo company = new EverStringCompanyInfo(realTimeCompany.getES_AdvancedInsights(),
					realTimeCompany.getES_AlexaRank(), realTimeCompany.getES_City(),
					realTimeCompany.getES_CompanyListNames(), realTimeCompany.getES_CompanyPhone(),
					realTimeCompany.getES_Country(), realTimeCompany.getES_ECID(), realTimeCompany.getES_Employee(),
					realTimeCompany.getES_EmployeeBand(), realTimeCompany.getES_EstimatedAge(),
					realTimeCompany.getES_FacebookUrl(), realTimeCompany.getES_Industry(),
					realTimeCompany.getES_Intent(), realTimeCompany.getES_IntentAggregateScore(),
					realTimeCompany.getES_IntentByTier(), realTimeCompany.getES_IntentNumByTier(),
					realTimeCompany.getES_IntentStr(), realTimeCompany.getES_IntentTime(),
					realTimeCompany.getES_Keywords(), realTimeCompany.getES_LinkedInUrl(),
					realTimeCompany.getES_LocationID(), realTimeCompany.getES_MatchName(),
					realTimeCompany.getES_MatchReasonBuildingName(), realTimeCompany.getES_MatchReasonBuildingNumber(),
					realTimeCompany.getES_MatchReasonBusinessType(), realTimeCompany.getES_MatchReasonCity(),
					realTimeCompany.getES_MatchReasonCompanyPhone(), realTimeCompany.getES_MatchReasonCountry(),
					realTimeCompany.getES_MatchReasonDirectional(), realTimeCompany.getES_MatchReasonName(),
					realTimeCompany.getES_MatchReasonRoadName(), realTimeCompany.getES_MatchReasonRoadType(),
					realTimeCompany.getES_MatchReasonState(), realTimeCompany.getES_MatchReasonUnit(),
					realTimeCompany.getES_MatchReasonWebsite(), realTimeCompany.getES_MatchReasonZip(),
					realTimeCompany.getES_MatchScore(), realTimeCompany.getES_Models(), realTimeCompany.getES_NAICS2(),
					realTimeCompany.getES_NAICS2Description(), realTimeCompany.getES_NAICS4(),
					realTimeCompany.getES_NAICS4Description(), realTimeCompany.getES_NAICS6(),
					realTimeCompany.getES_NAICS6Description(), realTimeCompany.getES_Name(),
					realTimeCompany.getES_NumLocations(), realTimeCompany.getES_NumSurgingTopics(),
					realTimeCompany.getES_PrimaryWebsite(), realTimeCompany.getES_Revenue(),
					realTimeCompany.getES_RevenueBand(), realTimeCompany.getES_SIC2(),
					realTimeCompany.getES_SIC2Description(), realTimeCompany.getES_SIC3(),
					realTimeCompany.getES_SIC3Description(), realTimeCompany.getES_SIC4(),
					realTimeCompany.getES_SIC4Description(), realTimeCompany.getES_SimilarCompanies(),
					realTimeCompany.getES_State(), realTimeCompany.getES_Street(), realTimeCompany.getES_Top5NAICS(),
					realTimeCompany.getES_TwitterUrl(), realTimeCompany.getES_YearStarted(),
					realTimeCompany.getES_Zip(), realTimeCompany.getError_messages(), realTimeCompany.getInputId());
			everStringCompanyInfo.add(company);
		}
		if (everStringCompanyInfo.size() > 0) {
			everStringCompanyInfoRepository.bulkinsert(everStringCompanyInfo);
		}
		logger.debug("Inserted {} records into everStringcompanyinfo", everStringCompanyInfo.size());
		return true;
	}

	/**
	 * this method convert list into no Of Batch list
	 * 
	 * @param source
	 * @param length
	 * @return
	 */
	public static <T> Stream<List<T>> noOfBatches(List<T> source, int length) {
		if (length <= 0)
			throw new IllegalArgumentException("length = " + length);
		int size = source.size();
		if (size <= 0)
			return Stream.empty();
		int fullChunks = (size - 1) / length;
		return IntStream.range(0, fullChunks + 1)
				.mapToObj(n -> source.subList(n * length, n == fullChunks ? size : (n + 1) * length));
	}

	public String getDate() {
		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date dateobj = new Date();
		return df.format(dateobj);
	}
}
