/**
 * 
 */
package com.xtaas.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.Normalizer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.ContactRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.service.ProspectCallInteractionService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.Attribute;
import com.xtaas.web.dto.LeadformAttributes;

/**
 * @author djain
 *
 */
@Component
public class DownloadPerformanceReport {
	
	// private String campaignId = "56b4a92fe4b0c2b75250c717";
	private String startDateStr = "5 Days";//1 Months, 1 Weeks, 1 Years
	// private String endDateStr = "05/01/2016 13:30:00";
    
	@Autowired
    private ProspectCallInteractionService prospectCallInteractionService;
	
    @Autowired
    private ProspectCallLogRepository prospectCallLogRepository;
    
    @Autowired
	private ClientMappingRepository clientMappingRepository;
    
    @Autowired
    private ProspectCallInteractionRepository prospectCallInteractionRepository;
    
    @Autowired
    private CampaignService campaignService;
    
    
    private static String COMMA_DELIMITER = ",";
    
    /**
     * @param args
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonGenerationException 
     * @throws ParseException 
     */
    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException, ParseException, Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        DownloadPerformanceReport downloadLeadReport = context.getBean(DownloadPerformanceReport.class);
         List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
         authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
         SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("djain", "secret", authorities));
         downloadLeadReport.download();
    }

    private void download() throws ParseException,Exception  {
    	List<Campaign> campaignList = campaignService.getActiveCampaigns();
    	//Campaign campaignx = campaignService.getCampaign("5b45ae9ee4b0cc96c5442802");
    	//campaignList.add(campaignx);
    			
    /*	
    	List<String> staticAttribList = new ArrayList<String>();
    	 staticAttribList.add("prospect.industry");
    	 staticAttribList.add("prospect.title");
    	 staticAttribList.add("prospect.managementlevel");
    	 staticAttribList.add("prospect.department");
    	 staticAttribList.add("prospect.minemployeecount");
    	 staticAttribList.add("prospect.maxemployeecount");
    	 staticAttribList.add("prospect.minrevenue");
    	 staticAttribList.add("prospect.maxrevenue");*/
    	
    //	Campaign campaign = campaignService.getCampaign(campaignId);
    	
    	if(startDateStr!=null && !startDateStr.isEmpty() /*&& endDateStr!=null && !endDateStr.isEmpty()*/){
    		//return;
    	}
    	Date startDate = XtaasDateUtils.getDate(getStartDate(startDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
        Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
        System.out.println(startDateInUTC);
        /* Date endDate = XtaasDateUtils.getDate(endDateStr, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
        Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
        System.out.println(endDateInUTC);*/
       
     
       // FileWriter fileWriter = null;
        try {
            
            for(Campaign campaign : campaignList){
            	System.out.println("Processing Data... PLEASE WAIT!!! :)" );
            	Map<String,Map<String,Integer>> campaignAgentsDisp = new HashMap<String, Map<String,Integer>>();
            	Map<String,String> agentPerformanceMap = new HashMap<String, String>();
            	List<String> agentList = prospectCallInteractionService.getDistinctAgents(campaign.getId(),startDateInUTC);
            	int recordsAttempted1 = prospectCallInteractionRepository.getCampaignDialedCount(campaign.getId(),startDateInUTC);
     	        int recordsContacted2 = prospectCallInteractionRepository.getCampaignContactCount(campaign.getId(),startDateInUTC);
     	        int recordsConnected3 = prospectCallInteractionRepository.getCampaignConnectCount(campaign.getId(),startDateInUTC);
     	        int recordsSuccess4   = prospectCallInteractionRepository.getCampaignSuccessCount(campaign.getId(),startDateInUTC);
     	       if(agentList!=null && agentList.size()>0){
	     	        for(String agentId : agentList){
	     	        	////// Agent Wise Disposition Report.
	     	        	 Map<String, Integer> agentDispostionMap = prospectCallInteractionService.getAgentCallStats(campaign.getId(), agentId, startDateInUTC);
	     	        	 
	     	        	campaignAgentsDisp.put(agentId, agentDispostionMap);
	     	 	        /////// Agent Wise Performance Report.
	     	 	        int agentContactedCount =  prospectCallInteractionRepository.getCampaignAgentContactedCount(campaign.getId(), agentId, startDateInUTC);
	      	 	        int agentConnectedCount =  prospectCallInteractionRepository.getCampaignAgentConnectCount(campaign.getId(), agentId, startDateInUTC);
	     	 	        int agentSuccesstCount =  prospectCallInteractionRepository.getCampaignAgentSuccessCount(campaign.getId(), agentId, startDateInUTC);
	     	 	        String counts = agentContactedCount+","+agentConnectedCount+","+agentSuccesstCount;
	     	 	        agentPerformanceMap.put(agentId, counts);
	     	        	// }
	     	        }
     	       }    	       ////// Campaign Wise Performance Report.
     	        int recordsAttempted = prospectCallInteractionRepository.getCampaignDialedCount(campaign.getId(),startDateInUTC);
     	        int recordsContacted = prospectCallInteractionRepository.getCampaignContactCount(campaign.getId(),startDateInUTC);
     	        int recordsConnected = prospectCallInteractionRepository.getCampaignConnectCount(campaign.getId(),startDateInUTC);
     	        int recordsSuccess   = prospectCallInteractionRepository.getCampaignSuccessCount(campaign.getId(),startDateInUTC);

     	         createCampaignPerformance(campaign,recordsAttempted,recordsContacted,recordsConnected,recordsSuccess);
     	         createCampaignDispPerformance(campaign,campaignAgentsDisp);
     	         createCampaignAgentPerformance(campaign,agentPerformanceMap);
            	
            }
            
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } /*finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }*/
        		////////////////////////////////////
            
    }
    
    private String getStartDate(String startDate){
		   
		 //String toDate = "02/10/2018 00:00:00";
         // mm/dd/yyyy
		   String toDate="";
		   Calendar c = Calendar.getInstance();
		   System.out.println("Current date : " + (c.get(Calendar.MONTH) + 1) +
				   "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));

		   	String periodActual = startDate;
	    	 int beginIndex = periodActual.indexOf(" ");
	    	 String periodStr = periodActual.substring(0, beginIndex);
	    	 int period = Integer.parseInt(periodStr);
	    	 //logger.info(period);
		   
		   
		   
		   if(startDate.contains("Years")){
			   c.add(Calendar.YEAR, -period);
			   toDate = (c.get(Calendar.MONTH) + 1) +
					   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Months")){
	    		  c.add(Calendar.MONTH, -period);
	    		  toDate = (c.get(Calendar.MONTH) + 1) +
	   				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Weeks")){
	    		 c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Days")){
	    		 c.add(Calendar.DAY_OF_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}
		   
		
		  
		   return toDate;
				   
				   
	   }
    
    private void createCampaignPerformance(Campaign campaign,int recordsAttempted,int recordsContacted,
    		int recordsConnected,int recordsSuccess) throws Exception {
    	String[] columns = new String[4];
    	columns[0] = "Records Attempted";
    	columns[1] = "Records Contacted";
    	columns[2] = "Records Connected";
    	columns[3] = "Records Success";
    	String fileName = "C:\\ex\\campperf\\"+campaign.getName()+"Performance.csv";
    	Workbook workbook = new XSSFWorkbook();
    	CreationHelper createHelper = workbook.getCreationHelper();
    	Sheet sheet = workbook.createSheet("Performance");
        int rowNum = 1;
    	Row row = sheet.createRow(rowNum++);
    	
    	Font headerFont = workbook.createFont();
    	headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
               // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

                // Create a Row
        Row headerRow = sheet.createRow(0);
                
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

             // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
    	int col = 0;
            
    	row.createCell(col++)
               .setCellValue(recordsAttempted);
    	row.createCell(col++)
        .setCellValue(recordsContacted);
    	row.createCell(col++)
        .setCellValue(recordsConnected);
    	row.createCell(col++)
        .setCellValue(recordsSuccess);
    		
        
     // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();
        System.out.println("CSV file for campaign : "+campaign.getName()+" was created successfully." );
    }
    
    private void createCampaignAgentPerformance(Campaign campaign, Map<String,String> campAgentMap) throws Exception{
    	String[] columns = new String[4];
    	columns[0] = "Agent Id";
    	columns[1] = "Records Contacted";
    	columns[2] = "Records Connected";
    	columns[3] = "Records Success";
    	
    	String fileName = "C:\\ex\\agentperf\\"+campaign.getName()+"AgentPerformance.csv";
    	Workbook workbook = new XSSFWorkbook();
    	CreationHelper createHelper = workbook.getCreationHelper();
    	Sheet sheet = workbook.createSheet("AgentPerformance");
        int rowNum = 1;
    	
    	
    	Font headerFont = workbook.createFont();
    	headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
               // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

                // Create a Row
        Row headerRow = sheet.createRow(0);
                
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

             // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
    	int col = 0;
    	int col1 =1;
    	int col2 = 2;
    	int col3 = 3;
    	for (Map.Entry<String, String> entry : campAgentMap.entrySet())
    	{
    		Row row = sheet.createRow(rowNum++);
    		row.createCell(col)
            .setCellValue(entry.getKey());
    		String values = entry.getValue();
    		String[] valArr = values.split(",");
    		List<String> valueList = Arrays.asList(valArr);
    	   // System.out.println(entry.getKey() + "/" + entry.getValue());
    		//row = sheet.createRow(rowNum++);
    		row.createCell(col1).setCellValue(valArr[0]);
    		row.createCell(col2).setCellValue(valArr[1]);
    		row.createCell(col3).setCellValue(valArr[2]);
    	}
    	  
    	
        
     // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();
        System.out.println("CSV file for campaign : "+campaign.getName()+" was created successfully." );
    	
    }

    private void createCampaignDispPerformance(Campaign campaign,Map<String,Map<String, Integer>> campaignDispMap) throws Exception {
    	String[] columns = new String[3];
    	columns[0] = "Agent Id";
    	columns[1] = "Dispostion Status";
    	columns[2] = "Count";
    	
     	String fileName = "C:\\ex\\dispperf\\"+campaign.getName()+"DispositionPerformance.csv";
    	Workbook workbook = new XSSFWorkbook();
    	CreationHelper createHelper = workbook.getCreationHelper();
    	Sheet sheet = workbook.createSheet("AgentPerformance");
        int rowNum = 1;
    	
    	
    	Font headerFont = workbook.createFont();
    	headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
               // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

                // Create a Row
        Row headerRow = sheet.createRow(0);
                
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

             // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
    	int col = 0;
    	int col1 =1;
    	int col2 = 2;
    	for (Map.Entry<String, Map<String,Integer>> entry : campaignDispMap.entrySet())
    	{
    		Row row = sheet.createRow(rowNum++);
    		row.createCell(col)
            .setCellValue(entry.getKey());
    		Map<String, Integer> dispMap = entry.getValue();
    	   // System.out.println(entry.getKey() + "/" + entry.getValue());
    		for (Map.Entry<String, Integer> entry1 : dispMap.entrySet()){
    			row = sheet.createRow(rowNum++);
    			row.createCell(col1)
    	        .setCellValue(entry1.getKey());
    			row.createCell(col2)
    	        .setCellValue(entry1.getValue());
    		}
    	}   
    	
        
     // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();
        System.out.println("CSV file for campaign : "+campaign.getName()+" was created successfully." );
    }
   
}

