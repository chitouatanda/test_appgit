/**
 * 
 */
package com.xtaas.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.CampaignContactRepository;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.ContactRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.valueobjects.ClientMappingComparator;
import com.xtaas.web.dto.Attribute;
import com.xtaas.web.dto.LeadformAttributes;

/**
 * @author djain
 *
 */
@Component
public class DownloadClientReport {
	
	String clientRegion = "us-west-2";//This should be part of campaign object
	 String bucketName = "insideup";// This should be part of campaign object.
	 String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
	 String awsSecretKey = "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";
	 boolean flag=false;
	
	// private String campaignId = "56b4a92fe4b0c2b75250c717";
	 private String startDateStr = "31 Days";//1 Months, 1 Weeks, 1 Years
	// private String endDateStr = "05/01/2016 13:30:00";
	 private String recordingDateStr = "1 Days";
    
    @Autowired
    private ProspectCallLogRepository prospectCallLogRepository;
    
    @Autowired
	private ClientMappingRepository clientMappingRepository;
    
    @Autowired
    private ProspectCallInteractionRepository prospectCallInteractionRepository;
    
    @Autowired
    private CampaignService campaignService;
    
    String ext = ".zip";
	 String fileNameWithoutExt = null;
    
    private static String COMMA_DELIMITER = ",";
    private static final String SUFFIX = "/";
    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    /**
     * @param args
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonGenerationException 
     * @throws ParseException 
     */
    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException, ParseException, Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        DownloadClientReport downloadLeadReport = context.getBean(DownloadClientReport.class);
         List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
         authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
         SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("djain", "secret", authorities));
         downloadLeadReport.download();
        // downloadLeadReport.downloadConnectReport();
    }

    private void download() throws ParseException,Exception  {
    	List<Campaign> campaignList = campaignService.getActiveXtaasCampaigns();
    	AmazonS3 s3client = createAWSConnection(clientRegion);
    	Date date = new Date();
        //System.out.println(sdf.format(date));
        String dateFolder = sdf.format(date);
    	//Campaign campaignx = campaignService.getCampaign("5b45ae9ee4b0cc96c5442802");
    	//campaignList.add(campaignx);
        boolean exists = s3client.doesObjectExist(bucketName, dateFolder);
        if (exists) {
       	 
        }else{
       	 createFolder(bucketName,dateFolder,s3client);
        }
    			
    	
    	List<String> staticAttribList = new ArrayList<String>();
    	staticAttribList.add("prospect.zoomPersonUrl");
    	staticAttribList.add("prospect.zoomCompanyUrl");
    	staticAttribList.add("prospect.industry");
    	staticAttribList.add("prospect.industryGroup");
    	staticAttribList.add("prospect.prospectCallId");
    	 staticAttribList.add("prospect.title");
    	 staticAttribList.add("prospect.managementlevel");
    	 staticAttribList.add("prospect.department");
    	 staticAttribList.add("prospect.employeeRange");
    	 staticAttribList.add("prospect.revenueRange");
    	 staticAttribList.add("prospect.firstName");
    	 staticAttribList.add("prospect.lastName");
    	 staticAttribList.add("prospect.phone");
    	 staticAttribList.add("prospect.email");
    	 staticAttribList.add("prospect.organization");
    	 staticAttribList.add("prospect.address");
    	 staticAttribList.add("prospect.city");
    	 staticAttribList.add("prospect.state");
    	 staticAttribList.add("prospect.zip");
    	 staticAttribList.add("prospect.country");
    	 staticAttribList.add("prospect.recordingUrl");
    	 staticAttribList.add("prospect.fullName");
    	 
    	 
    	
    //	Campaign campaign = campaignService.getCampaign(campaignId);
    	 
    	
        Date startDate = XtaasDateUtils.getDate(getStartDate(startDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
        Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
        System.out.println(startDateInUTC);
        
        Date recordingDate = XtaasDateUtils.getDate(getStartDate(recordingDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
        Date recordingDateInUTC = XtaasDateUtils.convertToTimeZone(recordingDate, "UTC");
        System.out.println(recordingDateInUTC);
        
       /* Date endDate = XtaasDateUtils.getDate(endDateStr, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
        Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
        System.out.println(endDateInUTC);*/
        
        ArrayList<String> uniqueRecords = new ArrayList<String>();
     
       // List<String> sourceId = getId(prospectCallLogList);
      //  List<ProspectCallInteraction> prospectCallInteractionList = prospectCallInteractionRepository.getId(sourceId);
        
        
        
        //System.exit(0);
         Random rnd = new Random();
       // FileWriter fileWriter = null;
        try {
            int recordsCounter = 0;
	            for(Campaign campaign : campaignList){
	            	if(campaign.getId().equalsIgnoreCase("5b45ae9ee4b0cc96c5442802")){
	            		System.out.println("ABCD");
	            	}else{
	            		continue;
	            	}
	            	
	            	if(campaign.getClientName()!=null && !campaign.getClientName().isEmpty()){	
	            	System.out.println("CampaignName :" +campaign.getName());
	            	//TODO need to get the s3 bucket name of client from the campaign, this should be configured inc ampaign
	            	File dirFile = new File(campaign.getName());
    		        if (!dirFile.exists()) {
    		            if (dirFile.mkdirs()) {
    		                System.out.println("Directory is created!");
    		            } else {
    		                System.out.println("Failed to create directory!");
    		            }
    		        }
    		        
	            
	            	String folderName = campaign.getName();
               	    
               	 
        			 //////////////// Below logic to download one days success recordings to s3 storage
               	 
               	 if( campaign.getClientName()!=null && campaign.getClientName().equalsIgnoreCase("INSIDEUPTESTING1")){
               		createFolder(bucketName,dateFolder+SUFFIX+folderName,s3client);
               		List<ProspectCallLog> recordingCallLogList = prospectCallLogRepository.findContactsBySuccess(campaign.getId(), recordingDateInUTC);
	        			 for(ProspectCallLog pcl : recordingCallLogList){
	        				 URL url = new URL(pcl.getProspectCall().getRecordingUrl());
	        				 String cleanCampaignName = campaign.getName().replaceAll("[^a-zA-Z0-9\\s]", "");
	        				/* if(campaign.getId().equalsIgnoreCase("5b44c7a5e4b00400c86ba198")){
	        					 cleanCampaignName = "UC";
	        				 }
	        				 if(campaign.getId().equalsIgnoreCase("5b44c7a5e4b00400c86ba198")){
	        					 cleanCampaignName = "UC";
	        				 }*/
	         				String rfileName = pcl.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
	         						+pcl.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
	         						+cleanCampaignName+".wav";
	         				
	         				File rfile = new File(folderName+SUFFIX+rfileName);
	         				fileDownload(url, rfile);
	        			 }
	            }
        		 ////////////////////// End Logic to download one days success recordings to s3 storage
	            	
	            	System.out.println("Processing Data... PLEASE WAIT!!! :)" );
	                List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findContactsBySuccess(campaign.getId(), startDateInUTC);
	                System.out.println("Total records found : " + prospectCallLogList.size() + " for campaign : " + campaign.getName());
	            	String fileName = folderName+SUFFIX+campaign.getName()+".xlsx";
	            	File file = new File(fileName);
	            	Workbook workbook = new XSSFWorkbook();
	            	CreationHelper createHelper = workbook.getCreationHelper();
	            	Sheet sheet = workbook.createSheet("Leads");
	            	
	            	Map<String,String> colNames = new HashMap<String,String>();
	
	            	// fileWriter = new FileWriter("C:\\ex\\B2B\\"+campaign.getName()+".csv");
	                 //Write the CSV file header
	            	 
	                 //fileWriter.append("First Name","Last Name","Phone","Email","Organization Name","Address","City","State","Zip","Country","Industry","Title","ManagementLevel","Department","minEmployee","maxEmployee","minRevenue","maxRevenue");
	                 List<ClientMapping>  clientMappingList = clientMappingRepository
						.findByClientName(campaign.getClientName());
	                 
	                 List<ClientMapping> campaignCMListCampaign = clientMappingRepository
	                		 .findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
	                 
	                 
	               
	                 ////////////////////////////////////////////////////////////////////////////////////////////
	                 Map<String,List<ClientMapping>> clientMap = new HashMap<String, List<ClientMapping>>();
	                 Map<String,ClientMapping> campMap = new HashMap<String, ClientMapping>();
	                 List<ClientMapping> campaignCMList = new ArrayList<ClientMapping>();
	                 for(ClientMapping cm : clientMappingList){
	                	 if(cm.getCampaignId()==null || cm.getCampaignId().isEmpty()){
		                	 List<ClientMapping>  cmList= clientMap.get(cm.getStdAttribute());
		                	 if(cmList!=null && cmList.size()>0){
		                		 cmList.add(cm);
		                		 clientMap.put(cm.getStdAttribute(), cmList);
		                	 }else{
		                		 cmList = new ArrayList<ClientMapping>();
		                		 cmList.add(cm);
		                		 clientMap.put(cm.getStdAttribute(), cmList);
		                	 }
	                	 }
	                 }
	                 
	                 for(ClientMapping cm : campaignCMListCampaign){
                		 campMap.put(cm.getStdAttribute(), cm);
	                 }
	                 
	                 for(Map.Entry<String, ClientMapping> entry : campMap.entrySet()){
	                	 if(clientMap.containsKey(entry.getKey())){
	                		 ClientMapping cm = entry.getValue();
	                		 List<ClientMapping> tempList = clientMap.get(entry.getKey());
	                		 for(ClientMapping tcm : tempList){
	                			 tcm.setCampaignId(cm.getCampaignId());
	                			 tcm.setSequence(cm.getSequence());
	                			 tcm.setColHeader(cm.getColHeader());
	                			 campaignCMList.add(tcm);
	                		 }
	                	 }else{
	                		 campaignCMList.add(entry.getValue());
	                	 }
	                 }
	                 
	                 
	                 
	                 ///////////////////////////////////////////////////////////////////////////////////////////
	                 
	                 Collections.sort(campaignCMList, new ClientMappingComparator());
	                 
	                 int i=0;
	                 String[] columns = new String[200];
	                 for(ClientMapping ccm : campaignCMList){
	                	 if(colNames.containsKey(ccm.getColHeader())){
	                		 continue;
	                	 }else{
	                		 System.out.println(ccm.getColHeader());
	                		 colNames.put(ccm.getColHeader(),ccm.getStdAttribute());
	                		 columns[i] = ccm.getColHeader();
	                		 i++;
	                	 }
	                 }
	                 
	                 colNames.put("Status","Status");
	                 columns[i]= "Status";
	                 i++;
	                 colNames.put("Reason","Reason");
	                 columns[i]= "Reason";
	                 i++;
	                 colNames.put("Notes","Notes");
	                 columns[i]= "Notes";
	                 i++;
	                 colNames.put("DownloadDate","DownloadDate");
	                 columns[i]= "DownloadDate";
	                 i++;
	                 colNames.put("AssetName","AssetName");
	                 columns[i]= "AssetName";
	                 i++;
	                 colNames.put("DeliveryDate","DeliveryDate");
	                 columns[i]= "DeliveryDate";
	                 i++;
	                 colNames.put("PartnerId","PartnerId");
	                 columns[i]= "PartnerId";
	                 i++;
	                 colNames.put("AgentId","AgentId");
	                 columns[i]= "AgentId";
	                 i++;
	                 colNames.put("RecordingUrl","RecordingUrl");
	                 columns[i]= "RecordingUrl";
	                 i++;
	                 colNames.put("QAStatus","QAStatus");
	                 columns[i]= "QAStatus";
	                 i++;
	                 colNames.put("C1.ValidLead","C1.ValidLead");
	                 columns[i]= "C1.ValidLead";
	                 i++;
	                 colNames.put("C1.Reason","C1.Reason");
	                 columns[i]= "C1.Reason";
	                 i++;
	                 colNames.put("C1.CallNotes","C1.CallNotes");
	                 columns[i]= "C1.CallNotes";
	                 i++;
	                 colNames.put("CompanyZoomURL","CompanyZoomURL");
	                 columns[i]= "CompanyZoomURL";
	                 i++;
	                 colNames.put("PersonZoomURL","PersonZoomURL");
	                 columns[i]= "PersonZoomURL";
	
	            
	        		 //Add a new line separator after the header
	        		// Create a Font for styling header cells
	                Font headerFont = workbook.createFont();
	                headerFont.setBold(true);
	                headerFont.setFontHeightInPoints((short) 14);
	                headerFont.setColor(IndexedColors.RED.getIndex());
	
	                // Create a CellStyle with the font
	                CellStyle headerCellStyle = workbook.createCellStyle();
	                headerCellStyle.setFont(headerFont);
	
	                // Create a Row
	                Row headerRow = sheet.createRow(0);
	                
	                for(int x = 0; x < columns.length; x++) {
	                	if(columns[x] !=null && !columns[x].isEmpty()){
		                    Cell cell = headerRow.createCell(x);
		                    cell.setCellValue(columns[x]);
		                    cell.setCellStyle(headerCellStyle);
	                	}
	                }
	
	                // Create Cell Style for formatting Date
	                CellStyle dateCellStyle = workbook.createCellStyle();
	                dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		
	                 
	                 
	                 
	                 Map<String, Map<String,ClientMapping>> clientStdAttribMap = new HashMap<String, Map<String,ClientMapping>>();
	                 Map<String,Map<String, List<ClientMapping>>> titleStdAttribMap = new HashMap<String, Map<String,List<ClientMapping>>>();
	                 
	                 if(campaignCMList!=null && campaignCMList.size()>0)
	                 for(ClientMapping cm : campaignCMList){
	                 	
	                 	if(cm.getStdAttribute()!=null){
	                 		
	                 		if(cm.getStdAttribute().equalsIgnoreCase("prospect.title")){
	                 			Map<String,List<ClientMapping>> tClientMap = titleStdAttribMap.get(cm.getStdAttribute());
	                 			if(tClientMap!=null && tClientMap.size()>0){
		                 			if(cm.getKey()!=null && !cm.getKey().isEmpty()){
		                 				List<ClientMapping> tClientList = tClientMap.get(cm.getKey());
		                 				if(tClientList!=null && tClientList.size()>0){
		                 					tClientList.add(cm);
		                 				}else{
		                 					tClientList = new ArrayList<ClientMapping>();
		                 					tClientList.add(cm);
		                 				}
		                 				tClientMap.put(cm.getKey(), tClientList);
		                 			}
	                 			
		                 		}else if(cm.getKey()!=null && !cm.getKey().isEmpty()){
		                 			  tClientMap = new HashMap<String, List<ClientMapping>>();
		                 			
		                 				List<ClientMapping> tClientList = new ArrayList<ClientMapping>();
		                 				tClientList.add(cm);
		                 				tClientMap.put(cm.getKey(), tClientList);
		                 			
		                 		}else{
		                 			tClientMap = new HashMap<String, List<ClientMapping>>();
		                 			
	                 				List<ClientMapping> tClientList = new ArrayList<ClientMapping>();
	                 				tClientList.add(cm);
	                 				tClientMap.put(cm.getKey(), tClientList);
		                 		}
	                 			titleStdAttribMap.put(cm.getStdAttribute(), tClientMap);
	                 		}
	                 		
	                 		
	                 		Map<String,ClientMapping> iClientMap = clientStdAttribMap.get(cm.getStdAttribute());
	                 		if(iClientMap!=null && iClientMap.size()>0){
	                 			if(cm.getKey()!=null && !cm.getKey().isEmpty())
	                 				iClientMap.put(cm.getKey(), cm);
	                 			else
	                 				iClientMap.put(cm.getStdAttribute(), cm);
	                 		}else{
	                 			iClientMap = new HashMap<String, ClientMapping>();
	                 			if(cm.getKey()!=null && !cm.getKey().isEmpty())
	                 				iClientMap.put(cm.getKey(), cm);
	                 			else
	                 				iClientMap.put(cm.getStdAttribute(), cm);
	                 		}
	                 		clientStdAttribMap.put(cm.getStdAttribute(), iClientMap);
	                 	}
	                 }
	                 int rowNum = 1;
	                int j=0;
	                 
	                
		            for (ProspectCallLog prospectCallLog : prospectCallLogList) {
		            	Row row = sheet.createRow(rowNum++);
		            	ProspectCall prospectCall = prospectCallLog.getProspectCall();
		        		//Prospect prospect = prospectCallLog.getProspectCall().getProspect();
		        	   
		        	   for(int col = 0; col < columns.length; col++) {
		                	if(columns[col] !=null && !columns[col].isEmpty()){
		                		if(columns[col].contentEquals("Status")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue("");
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("Reason")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue("");
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("Notes")){
		                			Cell cellx = row.createCell(col);
		                			/*String notes = "";
		                			if(prospectCallLog.getProspectCall().getNotes()!=null){
		                				StringBuilder sb = new StringBuilder();
		                				for(Note note : prospectCallLog.getProspectCall().getNotes()){
		                					sb.append(note.getText());
		                					sb.append("\n");
		                				}
		                				notes = sb.toString();
		                			}*/
				                    cellx.setCellValue("");
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("DownloadDate")){
		                			Cell cellx = row.createCell(col);
		                			//Date utcDate = prospectCallLog.getProspectCall().getCallStartTime();
		                			/*DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		                			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		                			Date dateutc = utcFormat.parse("2012-08-15T22:56:02.038Z");*/

		                			DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
		                			pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		                			String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
		                			Date pstDate = pstFormat.parse(pstStr);
		                			
		                			System.out.println(pstFormat.format(pstDate));
				                    cellx.setCellValue(pstFormat.format(pstDate));
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("AssetName")){
		                			Cell cellx = row.createCell(col);
		                			String assetName = "";
		                			if(prospectCall.getDeliveredAssetId() != null && !prospectCall.getDeliveredAssetId().isEmpty()){
		                				List<Asset> assetlist = campaign.getActiveAsset();
		                				for (Asset asset : assetlist) {
											if(asset.getAssetId().equals(prospectCall.getDeliveredAssetId())) {
												assetName = asset.getName();
											}
										}
		                			}
				                    cellx.setCellValue(assetName);
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("DeliveryDate")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue("");
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("PartnerId")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("AgentId")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("RecordingUrl")){
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrl());
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("QAStatus")){
		                			Cell cellx = row.createCell(col);
		                			cellx.setCellValue(prospectCallLog.getStatus().toString());
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("C1.ValidLead")){
		                			String valid="";
		                			if(prospectCallLog.getQaFeedback()!=null && 
		                					prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
		                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
		                				for(FeedbackSectionResponse fsr : fsrList){
		                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
		                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
		                						for(FeedbackResponseAttribute fra : fraList){
		                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
		                								valid = fra.getFeedback();
		                							}
		                						}
		                					}
		                				}
		                			}
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(valid);
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("C1.Reason")){
		                			String qaReason = "";
		                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
		                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
		                				for(FeedbackSectionResponse fsr : fsrList){
		                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
		                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
		                						for(FeedbackResponseAttribute fra : fraList){
		                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
		                								if(fra.getRejectionReason()!=null){
		                									qaReason = qaReason+fra.getRejectionReason();
		                								}
		                							}
		                						}
		                					}
		                				}
		                			}
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(qaReason);
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("C1.CallNotes")){
		                			String callnotes = "";
		                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
		                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
		                				for(FeedbackSectionResponse fsr : fsrList){
		                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
		                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
		                						for(FeedbackResponseAttribute fra : fraList){
		                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
		                								if(fra.getAttributeComment()!=null){
		                									callnotes = fra.getAttributeComment();
		                								}
		                							}
		                						}
		                					}
		                				}
		                			}
		                			Cell cellx = row.createCell(col);
				                    cellx.setCellValue(callnotes);
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("CompanyZoomURL")){
		                			Cell cellx = row.createCell(col);
//				                    cellx.setCellValue(prospectCallLog.getProspectCall().getZoomCompanyUrl());
				                    cellx.setCellStyle(dateCellStyle);
		                		}else if(columns[col].contentEquals("PersonZoomURL")){
		                			Cell cellx = row.createCell(col);
//				                    cellx.setCellValue(prospectCallLog.getProspectCall().getZoomPersonUrl());
				                    cellx.setCellStyle(dateCellStyle);
		                		}
		                		else{
		                			String val = getColumnValue(campaign,clientStdAttribMap,titleStdAttribMap,prospectCall,columns[col],colNames,campaign.getName());
			                		//System.out.println(text); 
				                		row.createCell(col)
					                    .setCellValue(val);
		                		}
		                	}
		        	   }
			            
		                 recordsCounter++;
		            }
		            
		            
		            /*Industry
		            TITLE - like match
		            MANAGEMENT LEVEL
		            Department
		            employeemin
		            employeemax
		            revenuemin
		            revenuemax
		            Questions*/
		         // Write the output to a file
		            FileOutputStream fileOut = new FileOutputStream(fileName);
		            workbook.write(fileOut);
		            fileOut.close();
	
		            // Closing the workbook
		            workbook.close();
		            System.out.println("CSV file for campaign : "+campaign.getName()+" was created successfully. Records Exported : " + recordsCounter);
		            //SEnd email
		            List<File> fileNames = new ArrayList<File>();
		            fileNames.add(file);
		            String message = campaign.getName()+" Attached Lead file for the previous day";
				/*	XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com",
							campaign.getName()+" Lead Report File Attached.", message,fileNames);*/
		            
		            XtaasEmailUtils.sendEmail("hbaba@xtaascorp.com", "data@xtaascorp.com",
							campaign.getName()+" Lead Report File Attached.", message,fileNames);
					System.out.println("Lead file delivered for "+campaign.getName());
//		            makeZipFile(dirFile,campaign.getName());
					if( campaign.getClientName()!=null && campaign.getClientName().equalsIgnoreCase("INSIDEUPTESTING1")){
						List<String> fileList = new ArrayList < String > ();
						generateFileList(new File(folderName),folderName,fileList);
						String zipFile = folderName+SUFFIX+folderName+ext;
						zipIt(zipFile,folderName,fileList);
		            
						String afileName = dateFolder+ SUFFIX +folderName + SUFFIX + zipFile;
						s3client.putObject(new PutObjectRequest(bucketName, afileName, 
     						new File(zipFile))
     						.withCannedAcl(CannedAccessControlList.PublicRead));
					}
     				System.out.println("Recording File uploade");
		            
		           
					/*String efileName = dateFolder+ SUFFIX +folderName + SUFFIX + file;
     				s3client.putObject(new PutObjectRequest(bucketName, efileName, 
     						file)
     						.withCannedAcl(CannedAccessControlList.PublicRead));
     				*/System.out.println("Lead File uploaded");
     				
     				/*if(file.delete()) {
     					System.out.println("File DELETED from server successfully");
     				}else{
     					System.out.println("File NOT DELETED from server successfully");
     				}*/
	            
	           
	            deleteDir(dirFile);
	            }
	        }
            
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } /*finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }*/
        		////////////////////////////////////
            
    }
    
    
    private String getStartDate(String startDate){
		   
		 //String toDate = "02/10/2018 00:00:00";
          // mm/dd/yyyy
		   String toDate="";
		   Calendar c = Calendar.getInstance();
		   System.out.println("Current date : " + (c.get(Calendar.MONTH) + 1) +
				   "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));

		   	String periodActual = startDate;
	    	 int beginIndex = periodActual.indexOf(" ");
	    	 String periodStr = periodActual.substring(0, beginIndex);
	    	 int period = Integer.parseInt(periodStr);
	    	 //logger.info(period);
		   
		   
		   
		   if(startDate.contains("Years")){
			   c.add(Calendar.YEAR, -period);
			   toDate = (c.get(Calendar.MONTH) + 1) +
					   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Months")){
	    		  c.add(Calendar.MONTH, -period);
	    		  toDate = (c.get(Calendar.MONTH) + 1) +
	   				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Weeks")){
	    		 c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(startDate.contains("Days")){
	    		 c.add(Calendar.DAY_OF_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}
		   
		
		  
		   return toDate;
				   
				   
	   }
    
    private String getColumnValue(Campaign campaign,Map<String, Map<String, ClientMapping >> clientStdAttribMap, 
    		Map<String, Map<String, List<ClientMapping> >> titleStdAttribMap,
    		ProspectCall pc,String column,Map<String,String> colNames,String campaignName){
    	
    	 
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.zoomCompanyUrl".equalsIgnoreCase(colNames.get(column))){
    			 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zoomCompanyUrl");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
//		       		 ClientMapping fcm = clientMappingMap.get(pc.getZoomCompanyUrl());
//		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
//		       			return fcm.getValue(); 
//		       		 }
//		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
//		       			 return fcm.getDefaultValue();
//		       		 }
//		       		 return pc.getZoomCompanyUrl();
		       		 
		       	 }
    		}
      // }
    	
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.zoomPersonUrl".equalsIgnoreCase(colNames.get(column))){
	          	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zoomPersonUrl");
//		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
//		       		 ClientMapping fcm = clientMappingMap.get(pc.getZoomPersonUrl());
//		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
//		       			return fcm.getValue(); 
//		       		 }
//		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
//		       			 return fcm.getDefaultValue();
//		       		 }
//		       		 return pc.getZoomPersonUrl();
		       		 
//		       	 }
    		}
     //  }
    	
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.title".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, List<ClientMapping>> clientMappingMap = titleStdAttribMap.get("prospect.title");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		Map<String,String> titleMap = getTitlesMap();
		       		String titleCode = titleMap.get(pc.getProspect().getDepartment());
		       		 List<ClientMapping> fcmList = clientMappingMap.get(titleCode);
		       		 if(fcmList!=null){
			       		for(ClientMapping fcm : fcmList){
				       		 if(fcm!=null && fcm.getKey()!=null && !fcm.getKey().isEmpty() && fcm.getValue()!=null 
				       				 && !fcm.getValue().isEmpty() &&  fcm.getTitleMgmtLevel()!=null 
						       				 && !fcm.getTitleMgmtLevel().isEmpty()){
				       			 if(fcm.getTitleMgmtLevel().equalsIgnoreCase(pc.getProspect().getManagementLevel()))
				       				 return fcm.getValue();
				       		 }
			       		}
		       		 }else{
		       			for (Map.Entry<String,List<ClientMapping>> entry : clientMappingMap.entrySet()){
		       				List<ClientMapping> cmMap = entry.getValue();
		       				for(ClientMapping cm : cmMap){
		       					if(cm.getDefaultValue()!=null && !cm.getDefaultValue().isEmpty()){
		       						return cm.getDefaultValue();
		       					}
		       				}
		       			}
		       		 }
		       		 return pc.getProspect().getTitle();
		       		 
		       	 }
    		}
    		
    		if("prospect.prospectCallId".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.prospectCallId");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspectCallId());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspectCallId();
		       		 
		       	 }
   		}
    		
    		if("prospect.recordingUrl".equalsIgnoreCase(colNames.get(column))){
	           /*	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.recordingUrl");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getTitle());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getTitle();
		       		 
		       	 }*/
    			try{
    				//URL url = new URL(pc.getRecordingUrl());
    				//TODO here you can change the file name as needed
    				String cleanCampaignName = campaignName.replaceAll("[^a-zA-Z0-9\\s]", "");
    				String fileName = pc.getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
    						+pc.getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
    						+cleanCampaignName+".wav";
					
    			/*	File dirFile = new File("/recordings/" +cleanCampaignName);
    		        if (!dirFile.exists()) {
    		            if (dirFile.mkdirs()) {
    		                System.out.println("Directory is created!");
    		            } else {
    		                System.out.println("Failed to create directory!");
    		            }
    		        }
    				
    				File file = new File("/recordings/" +cleanCampaignName+ "/" +fileName + ".wav");
    				fileDownload(url, file);*/
    				//
    				return fileName;
    			}catch(Exception e){
    				e.printStackTrace();
    			}
   		}
     //   }
    	 
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.managementLevel".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.managementLevel");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getManagementLevel());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getManagementLevel();
		       		 
		       	 }
    		}
        //}
    	 
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.department".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.department");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getDepartment());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getDepartment();
		       		 
		       	 }
    		}
      //  }
    	 //1-5, 5-10,10-20,20-50,50-100,100-250,250-500,500-1,000,1,000-5,000, 5,000-10,000,Over 10,000
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.employeeRange".equalsIgnoreCase(colNames.get(column))){
    			//String maxempcount = pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
    			if(pc.getProspect().getCustomAttributeValue("maxEmployeeCount")==null || pc.getProspect().getCustomAttributeValue("maxEmployeeCount").equals(""))
	       			 return "";
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.employeeRange");
	           	 String maxEmp = pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
	           	 
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(maxEmp);
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getCustomAttributeValue("maxEmployeeCount").toString();
		       		 
		       	 }
    		}
     //   }
    	 
   
    		// Under $500K,$500K-$1M,$1M-$5M,$5M-$10M,$10M-$25M,$25M-$50M,$25M-$50M,$50M-$100M, $100M-$250M,$250M-$500M,$500M-$1B,$1B-$5B,Over $5B
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.revenueRange".equalsIgnoreCase(colNames.get(column))){
    			if(pc.getProspect().getCustomAttributeValue("maxRevenue")==null || pc.getProspect().getCustomAttributeValue("maxRevenue").equals(""))
	       			 return "";
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.revenueRange");
	           	 String maxRev = pc.getProspect().getCustomAttributeValue("maxRevenue").toString();
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(maxRev);
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getCustomAttributeValue("maxRevenue").toString();
		       		 
		       	 }
    		}
       // }
    	 
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.country".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.country");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCountry());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getCountry();
		       		 
		       	 }
    		}
     //   }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.zip".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.zip");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 && pc.getProspect().getZipCode()!=null ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getZipCode());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 String zip = pc.getProspect().getZipCode();
		       		 
		       		 /////
		       		//String str = "abc$def^ghi#jkl";
		       	 
		            Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		            Matcher m = p.matcher(zip);
		       
		            System.out.println(zip);
		            int count = 0;
		            while (m.find()) {
		               count =  m.start();
		               System.out.println("position "  + m.start() + ": " + zip.charAt(m.start()));
		            }
		            if(count>0){
		            	int ziplength = zip.length();
		            	String tempString = zip.substring(0,count);
		            	return tempString;
		            }
		            /////////////////////
		       		 
		       		 
		       		 return zip;
		       		 
		       	 }else {
		       		 return "";
		       	 }
    		}
     //   }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.state".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.state");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getStateCode());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getStateCode();
		       		 
		       	 }
    		}
      //  }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.city".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.city");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCity());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getCity();
		       		 
		       	 }
    		}
       // }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.address".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.address");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  && pc.getProspect().getAddressLine1() !=null ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getAddressLine1());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		return  pc.getProspect().getAddressLine1().replaceAll("[^a-zA-Z0-9\\s]", ""); 
		       		  
		       		 
		       	 }else{
		       		 return "";
		       	 }
    		}
      //  }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.organization".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.organization");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getCompany());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", ""); 
		       		 
		       	 }
    		}
      //  }
    	 
    	//if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.email".equalsIgnoreCase(colNames.get(column))){
    		
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.email");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getEmail());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getEmail();
		       		 
		       	 }
    		}
       // }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.phone".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.phone");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0  ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getPhone());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       		 return pc.getProspect().getPhone();
		       		 
		       	 }
    		}
    //    }
    		
    		if("prospect.fullName".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.fullName");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 /*ClientMapping fcm = clientMappingMap.get(pc.getProspect().getFirstName());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return StringUtils.capitalize(fcm.getValue()); 
		       		 }
		       		
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return StringUtils.capitalize(fcm.getDefaultValue());
		       		 }*/
		       		 String fn = StringUtils.capitalize(pc.getProspect().getFirstName());
		       		 String ln = StringUtils.capitalize(pc.getProspect().getLastName());
		       		 String fullName = fn+" "+ln;
		       		 return fullName;
		       		 
		       	 }
   		}
    	
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.firstName".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.firstName");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getFirstName());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return StringUtils.capitalize(fcm.getValue()); 
		       		 }
		       		
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return StringUtils.capitalize(fcm.getDefaultValue());
		       		 }
		       		 return StringUtils.capitalize(pc.getProspect().getFirstName());
		       		 
		       	 }
    		}
     //   }
    	 
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.lastName".equalsIgnoreCase(colNames.get(column))){
	           	 Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.lastName");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getProspect().getLastName());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return StringUtils.capitalize(fcm.getValue()); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return StringUtils.capitalize(fcm.getDefaultValue());
		       		 }
		       		 return StringUtils.capitalize(pc.getProspect().getLastName());
		       		 
		       	 }
    		}
    //    }
    	
    
    		if("prospect.industryGroup".equalsIgnoreCase(colNames.get(column))){
    			Map<String,String> industryGroupMap = getIndustriesGroupMap();
	       	 	Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.industryGroup");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 String key = industryGroupMap.get(pc.getProspect().getIndustry());
		       		 ClientMapping fcm = clientMappingMap.get(key);
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			 if( fcm.getValue().equalsIgnoreCase("")){
		       				System.out.println("BUG"); 
		       			 }
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			if( fcm.getDefaultValue().equalsIgnoreCase("")){
		       				System.out.println("BUG"); 
		       			 }
		       			 return fcm.getDefaultValue();
		       		 }
		       		if( pc.getProspect().getIndustry().equalsIgnoreCase("")){
	       				System.out.println("BUG"); 
	       			 }
		       		ClientMapping defaultFCM = clientMappingMap.get("UNKNOWN INDUSTRY");
		       		if(defaultFCM!=null ){
		       			return defaultFCM.getValue();
		       		}
		       		 return pc.getProspect().getIndustry();
		       		 
		       	 }
    		}
    		
    		
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.industry".equalsIgnoreCase(colNames.get(column))){
    			Map<String,String> industryMap = getIndustriesMap();
	       	 	Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.industry");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		String key = industryMap.get(pc.getProspect().getIndustry());
		       		 ClientMapping fcm = clientMappingMap.get(key);
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			 if( fcm.getValue().equalsIgnoreCase("")){
		       				System.out.println("BUG"); 
		       			 }
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			if( fcm.getDefaultValue().equalsIgnoreCase("")){
		       				System.out.println("BUG"); 
		       			 }
		       			 return fcm.getDefaultValue();
		       		 }
		       		if( pc.getProspect().getIndustry().equalsIgnoreCase("")){
	       				System.out.println("BUG"); 
	       			 }
		       		ClientMapping defaultFCM = clientMappingMap.get("UNKNOWN INDUSTRY");
		       		if(defaultFCM!=null ){
		       			return defaultFCM.getValue();
		       		}
		       		 return pc.getProspect().getIndustry();
		       		 
		       	 }
    		}
    //	}
    	
    //	if(clientStdAttribMap.containsKey(colNames.get(column))){
    		if("prospect.created".equalsIgnoreCase(colNames.get(column))){
	       	 	Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.created");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 ClientMapping fcm = clientMappingMap.get(pc.getCallStartTime());
		       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
		       			return fcm.getValue(); 
		       		 }
		       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
		       			 return fcm.getDefaultValue();
		       		 }
		       			//DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		       		 	DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
            			pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
            			String pstStr = pstFormat.format(pc.getCallStartTime());
            			//Date pstDate = pstFormat.parse(pstStr);
            			
            			//System.out.println(pstFormat.format(pstDate));
	                   
		       		 return pstStr;
		       		 
		       	 }
    		}
    		if("prospect.assetName".equalsIgnoreCase(colNames.get(column))){
	       	 	Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get("prospect.assetName");
		       	 if(clientMappingMap !=null && clientMappingMap.size()>0 ){
		       		 /*if(campaign.getActiveAsset() != null && campaign.getActiveAsset().getName()!=null){
			       		 ClientMapping fcm = clientMappingMap.get(campaign.getActiveAsset().getName());
			       		 if(fcm!=null && fcm.getValue()!=null && !fcm.getValue().isEmpty()){
			       			return fcm.getValue(); 
			       		 }
			       		 if(fcm!=null && fcm.getDefaultValue()!=null && !fcm.getDefaultValue().isEmpty()){
			       			 return fcm.getDefaultValue();
			       		 }
		       		 }
		       		String assetName = "";
					if(campaign.getActiveAsset()!=null && campaign.getActiveAsset().getName()!=null && !campaign.getActiveAsset().getName().isEmpty()){
						assetName = campaign.getActiveAsset().getName();
					}
	                   
		       		 return assetName;*/
		       		if (campaign.getActiveAsset() != null && !campaign.getActiveAsset().isEmpty()) {
						for (Asset asset : campaign.getActiveAsset()) {
							ClientMapping fcm = clientMappingMap.get(asset.getName());
							if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
								return fcm.getValue();
							}
							if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
								return fcm.getDefaultValue();
							}
						}
					}
					String assetName = "";
					if (campaign.getActiveAsset() != null && !campaign.getActiveAsset().isEmpty()) {
						assetName = campaign.getAssets().get(0).getName();
					}

					return assetName;
		       	 }
    		}
    		
    //	}
    		if (pc.getAnswers() != null)
    			for (AgentQaAnswer agentQaAnswer : pc.getAnswers()) {
    				String stdAttrib = "";
    				
    				
    				if(agentQaAnswer.getAttribute()!=null && !agentQaAnswer.getAttribute().isEmpty()) {
						  stdAttrib = agentQaAnswer.getAttribute();  	  
					}else{
						List<Expression> qObjList = campaign.getQualificationCriteria().getExpressions();
	    				for(Expression ex : qObjList){
	    					if(ex.getQuestionSkin().equalsIgnoreCase(agentQaAnswer.getQuestion()))
	    						stdAttrib = ex.getAttribute();
	    				}
					}
    				/*
    				 * if(column=="C1.ValidLead"){ System.out.println("NULL HERE"); }
    				 */
    				if (stdAttrib!=null && !stdAttrib.isEmpty() && colNames.get(column).equalsIgnoreCase(stdAttrib)
    						&& clientStdAttribMap.containsKey(stdAttrib)) {
    					Map<String, ClientMapping> crm = clientStdAttribMap.get(stdAttrib);
    					for (Map.Entry<String, ClientMapping> crmentry : crm.entrySet()) {
    						if (crmentry.getKey().equalsIgnoreCase(agentQaAnswer.getAnswer())) {
    							ClientMapping cm = crmentry.getValue();
    							if (cm.getValue() != null && !cm.getValue().isEmpty())
    								return cm.getValue();

    							if (cm.getDefaultValue() != null && !cm.getDefaultValue().isEmpty())
    								return cm.getDefaultValue();

    						}

    					}
    				}
    			}
    		if (clientStdAttribMap.containsKey(colNames.get(column))) {
    			Map<String, ClientMapping> clientMappingMap = clientStdAttribMap.get(colNames.get(column));
    			if (clientMappingMap != null && clientMappingMap.size() > 0) {
    				ClientMapping fcm = clientMappingMap.get(colNames.get(column));
    				if (fcm != null && fcm.getValue() != null && !fcm.getValue().isEmpty()) {
    					return fcm.getValue();
    				}
    				if (fcm != null && fcm.getDefaultValue() != null && !fcm.getDefaultValue().isEmpty()) {
    					return fcm.getDefaultValue();
    				}
    				return "";

    			}
    		}

    	
    	

    	
    	return "-";
    }
    
	private void fileDownload(URL url, File file) throws Exception {
		try {
			
			InputStream input = url.openStream();
			if (file.exists()) {
				if (file.isDirectory())
					throw new IOException("File '" + file + "' is a directory");

				if (!file.canWrite())
					throw new IOException("File '" + file + "' cannot be written");
			} else {
				File parent = file.getParentFile();
				if ((parent != null) && (!parent.exists()) && (!parent.mkdirs())) {
					throw new IOException("File '" + file + "' could not be created");
				}
			}

			FileOutputStream output = new FileOutputStream(file);

			byte[] buffer = new byte[4096];
			int n = 0;
			while (-1 != (n = input.read(buffer))) {
				output.write(buffer, 0, n);
			}

			input.close();
			output.close();

			System.out.println("File '" + file + "' downloaded successfully!");
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
		}
}
	
	/*
	 * This method is used for creating the connection with AWS S3 Storage
	 *  Params - AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection(String clientRegion) {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
			 s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			return s3client;
		}
		catch(Exception e){
			e.printStackTrace();
			   System.out.println("Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :" +e);
			return s3client;
		}
	}
	
	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
					folderName, emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}
	
	/**
	 * This method first deletes all the files in given folder and than the
	 * folder itself
	 */
	/*public static void deleteFolder(String bucketName, String folderName, AmazonS3 client) {
		List<S3ObjectSummary> fileList = 
				client.listObjects(bucketName, folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			client.deleteObject(bucketName, file.getKey());
		}
		client.deleteObject(bucketName, folderName);
	}*/
	
	void deleteDir(File file) {
	    File[] contents = file.listFiles();
	    if (contents != null) {
	        for (File f : contents) {
	            deleteDir(f);
	        }
	    }
	    file.delete();
	}
	
	 public void zipIt(String zipFile,String folderName,List<String> fileList) {
	        byte[] buffer = new byte[1024];
	        String source = new File(folderName).getName();
	        FileOutputStream fos = null;
	        ZipOutputStream zos = null;
	        try {
	            fos = new FileOutputStream(zipFile);
	            zos = new ZipOutputStream(fos);

	            System.out.println("Output to Zip : " + zipFile);
	            FileInputStream in = null;

	            for (String file: fileList) {
	                System.out.println("File Added : " + file);
	                ZipEntry ze = new ZipEntry(source + File.separator + file);
	                zos.putNextEntry(ze);
	                try {
	                    in = new FileInputStream(folderName + File.separator + file);
	                    int len;
	                    while ((len = in .read(buffer)) > 0) {
	                        zos.write(buffer, 0, len);
	                    }
	                } finally {
	                    in.close();
	                }
	            }

	            zos.closeEntry();
	            System.out.println("Folder successfully compressed");

	        } catch (IOException ex) {
	            ex.printStackTrace();
	        } finally {
	            try {
	                zos.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }

	    public void generateFileList(File node, String folderName,List<String> fileList) {
	        // add file only
	        if (node.isFile()) {
	            fileList.add(generateZipEntry(node.toString(),folderName));
	        }

	        if (node.isDirectory()) {
	            String[] subNote = node.list();
	            for (String filename: subNote) {
	                generateFileList(new File(node, filename),folderName,fileList);
	            }
	        }
	    }

	    private String generateZipEntry(String file,String folderName) {
	        return file.substring(folderName.length() + 1, file.length());
	    }
	    
	    private Map<String,String> getTitlesMap(){
	    	Map<String,String> titleMap = new HashMap<String, String>();
	    	titleMap.put("Consultants","TitleCode.Consultants");
	    	titleMap.put("Consulting","TitleCode.Consultants");

	    	titleMap.put("Biometrics","TitleCode.Biometrics");
	    	titleMap.put("Engineering & Technical","TitleCode.Technical");
	    	titleMap.put("Biotechnical Engineering","TitleCode.Technical");
	    	titleMap.put("Chemical Engineering","TitleCode.Technical");
	    	titleMap.put("Civil Engineering","TitleCode.Technical");
	    	titleMap.put("Database Engineering &  Design","TitleCode.Technical");
	    	titleMap.put("Hardware Engineering","TitleCode.Technical");
	    	titleMap.put("Industrial Engineering","TitleCode.Technical");
	    	titleMap.put("Information Technology","TitleCode.Technical");
	    	titleMap.put("Quality Assurance","TitleCode.Technical");
	    	titleMap.put("Software Engineering","TitleCode.Technical");
	    	titleMap.put("Technical","TitleCode.Technical.Technical");
	    	titleMap.put("Web Development","TitleCode.Technical");

	    	titleMap.put("Engineering","TitleCode.Technical.Engineer");

	    	titleMap.put("Finance","TitleCode.Finance");
	    	titleMap.put("Accounting","TitleCode.Finance");
	    	titleMap.put("Banking","TitleCode.Finance");
	    	titleMap.put("General Finance","TitleCode.Finance");
	    	titleMap.put("Investment Banking","TitleCode.Finance");
	    	titleMap.put("Wealth Management","TitleCode.Finance");

	    	titleMap.put("Human Resources","TitleCode.HR");
	    	titleMap.put("Benefits & Compensation","TitleCode.HR");
	    	titleMap.put("Diversity","TitleCode.HR");
	    	titleMap.put("Human Resources Generalist","TitleCode.HR");
	    	titleMap.put("Recruiting","TitleCode.HR");
	    	titleMap.put("Sourcing","TitleCode.HR");

	    	titleMap.put("Legal","TitleCode.Law");
	    	titleMap.put("Governmental Lawyers & Legal Professionals","TitleCode.Law");
	    	titleMap.put("Lawyers & Legal Professionals","TitleCode.Law");

	    	titleMap.put("Marketing","TitleCode.Marketing");
	    	titleMap.put("Advertising","TitleCode.Marketing");
	    	titleMap.put("Branding","TitleCode.Marketing");
	    	titleMap.put("Communications","TitleCode.Marketing");
	    	titleMap.put("E-biz","TitleCode.Marketing");
	    	titleMap.put("General Marketing","TitleCode.Marketing");
	    	titleMap.put("Product Management","TitleCode.Marketing");

	    	titleMap.put("Medical & Health","TitleCode.Health");
	    	titleMap.put("Dentists","TitleCode.Health");
	    	titleMap.put("Doctors - General Practice","TitleCode.Health");
	    	titleMap.put("Health Professionals","TitleCode.Health");
	    	titleMap.put("Nurses","TitleCode.Health");

	    	titleMap.put("Operations","TitleCode.Operations");
	    	titleMap.put("Change Management","TitleCode.Operations");
	    	titleMap.put("Compliance","TitleCode.Operations");
	    	titleMap.put("Contracts","TitleCode.Operations");
	    	titleMap.put("Customer Relations","TitleCode.Operations");
	    	titleMap.put("Facilities","TitleCode.Operations");
	    	titleMap.put("Logistics","TitleCode.Operations");
	    	titleMap.put("General Operations","TitleCode.Operations");
	    	titleMap.put("Quality Management","TitleCode.Operations");

	    	titleMap.put("Sales","TitleCode.Sales");
	    	titleMap.put("Business Development","TitleCode.Sales");
	    	titleMap.put("General Sales","TitleCode.Sales");

	    	titleMap.put("Scientists","TitleCode.Scientist");
	    	titleMap.put("General Management","TitleCode.TopExec");
	    	
	    	return titleMap;

	    }
	    
	    private Map<String,String> getIndustriesGroupMap(){
	    	Map<String,String> industriesMap = new HashMap<String, String>();
	    	
	    	industriesMap.put("Agriculture","Industry.agriculture");
	    	industriesMap.put("Animals & Livestock","Industry.agriculture");
	    	industriesMap.put("Crops","Industry.agriculture");
	    	industriesMap.put("Agriculture General","Industry.agriculture");

	    	industriesMap.put("Business Services","Industry.bizservice");
	    	industriesMap.put("Accounting & Accounting Services","Industry.bizservice");
	    	industriesMap.put("Auctions","Industry.bizservice");
	    	industriesMap.put("Call Centers & Business Centers","Industry.bizservice");
	    	industriesMap.put("Debt Collection","Industry.bizservice");
	    	industriesMap.put("Management Consulting","Industry.bizservice");
	    	industriesMap.put("Information & Document Management","Industry.bizservice");
	    	industriesMap.put("Multimedia & Graphic Design","Industry.bizservice");
	    	industriesMap.put("Food Service","Industry.bizservice");
	    	industriesMap.put("Business Services General","Industry.bizservice");
	    	industriesMap.put("Human Resources & Staffing","Industry.bizservice");
	    	industriesMap.put("Facilities Management & Commercial Cleaning","Industry.bizservice");
	    	industriesMap.put("Translation & Linguistic Services","Industry.bizservice");
	    	industriesMap.put("Advertising & Marketing","Industry.bizservice");
	    	industriesMap.put("Commercial Printing","Industry.bizservice");
	    	industriesMap.put("Security Products & Services","Industry.bizservice");

	    	industriesMap.put("Chambers of Commerce","Industry.chamber");

	    	industriesMap.put("Construction","Industry.construction");
	    	industriesMap.put("Architecture, Engineering & Design","Industry.construction");
	    	industriesMap.put("Commercial & Residential Construction","Industry.construction");
	    	industriesMap.put("Construction General","Industry.construction");

	    	industriesMap.put("Consumer Services","Industry.consumerservices");
	    	industriesMap.put("Automotive Service & Collision Repair","Industry.consumerservices");
	    	industriesMap.put("Car & Truck Rental","Industry.consumerservices");
	    	industriesMap.put("Funeral Homes & Funeral Related Services","Industry.consumerservices");
	    	industriesMap.put("Consumer Services General","Industry.consumerservices");
	    	industriesMap.put("Hair Salons","Industry.consumerservices");
	    	industriesMap.put("Laundry & Dry Cleaning Services","Industry.consumerservices");
	    	industriesMap.put("Photography Studio","Industry.consumerservices");
	    	industriesMap.put("Travel Agencies & Services","Industry.consumerservices");
	    	industriesMap.put("Veterinary Care","Industry.consumerservices");
	    	industriesMap.put("Weight & Health Management","Industry.consumerservices");

	    	industriesMap.put("Cultural","Industry.cultural");
	    	industriesMap.put("Cultural General","Industry.cultural");
	    	industriesMap.put("Libraries","Industry.cultural");
	    	industriesMap.put("Museums & Art Galleries","Industry.cultural");

	    	industriesMap.put("Education","Industry.education");
	    	industriesMap.put("Education General","Industry.education");
	    	industriesMap.put("K-12 Schools","Industry.education");
	    	industriesMap.put("Training","Industry.education");
	    	industriesMap.put("Colleges & Universities","Industry.education");

	    	industriesMap.put("Energy, Utilities & Waste Treatment","Industry.energy");
	    	industriesMap.put("Energy, Utilities & Waste Treatment General","Industry.energy");
	    	industriesMap.put("Electricity, Oil & Gas","Industry.energy");
	    	industriesMap.put("Waste Treatment, Environmental Services & Recycling","Industry.energy");
	    	industriesMap.put("Energy, Utilities, & Waste Treatment General","Industry.energy");
	    	industriesMap.put("Oil & Gas Exploration & Services","Industry.energy");
	    	industriesMap.put("Water & Water Treatment","Industry.energy");

	    	industriesMap.put("Finance","Industry.finance");
	    	industriesMap.put("Banking","Industry.finance");
	    	industriesMap.put("Brokerage","Industry.finance");
	    	industriesMap.put("Credit Cards & Transaction  Processing","Industry.finance");
	    	industriesMap.put("Finance General","Industry.finance");
	    	industriesMap.put("Investment Banking","Industry.finance");
	    	industriesMap.put("Venture Capital & Private Equity","Industry.finance");

	    	industriesMap.put("Government","Industry.government");

	    	industriesMap.put("Healthcare","Industry.healthcare");
	    	industriesMap.put("Emergency Medical Transportation & Services","Industry.healthcare");
	    	industriesMap.put("Healthcare General","Industry.healthcare");
	    	industriesMap.put("Hospitals & Clinics","Industry.healthcare");
	    	industriesMap.put("Medical Testing & Clinical Laboratories","Industry.healthcare");
	    	industriesMap.put("Pharmaceuticals","Industry.healthcare");
	    	industriesMap.put("Biotechnology","Industry.healthcare");
	    	industriesMap.put("Drug Manufacturing & Research","Industry.healthcare");

	    	industriesMap.put("Hospitality","Industry.hospitality");
	    	industriesMap.put("Hospitality General","Industry.hospitality");
	    	industriesMap.put("Lodging & Resorts","Industry.hospitality");
	    	industriesMap.put("Recreation","Industry.hospitality");
	    	industriesMap.put("Movie Theaters","Industry.hospitality");
	    	industriesMap.put("Fitness & Dance Facilities","Industry.hospitality");
	    	industriesMap.put("Gambling & Gaming","Industry.hospitality.recreation");
	    	industriesMap.put("Amusement Parks, Arcades & Attractions","Industry.hospitality");
	    	industriesMap.put("Zoos & National Parks","Industry.hospitality");
	    	industriesMap.put("Restaurants","Industry.hospitality");
	    	industriesMap.put("Sports Teams & Leagues","Industry.hospitality");

	    	industriesMap.put("Insurance","Industry.insurance");

	    	industriesMap.put("Law Firms & Legal Services","Industry.legal");

	    	industriesMap.put("Media & Internet","Industry.media");
	    	industriesMap.put("Broadcasting","Industry.media");
	    	industriesMap.put("Film/Video Production & Services","Industry.media");
	    	industriesMap.put("Radio Stations","Industry.media");
	    	industriesMap.put("Television Stations","Industry.media");
	    	industriesMap.put("Media & Internet General","Industry.media");
	    	industriesMap.put("Information Collection & Delivery","Industry.media");
	    	industriesMap.put("Search Engines & Internet Portals","Industry.media");
	    	industriesMap.put("Music & Music Related Services","Industry.media");
	    	industriesMap.put("Newspapers & News Services","Industry.media");
	    	industriesMap.put("Publishing","Industry.media");

	    	industriesMap.put("Manufacturing","Industry.mfg");
	    	industriesMap.put("Aerospace & Defense","Industry.mfg");
	    	industriesMap.put("Boats & Submarines","Industry.mfg");
	    	industriesMap.put("Building Materials","Industry.mfg");
	    	industriesMap.put("Aggregates, Concrete & Cement","Industry.mfg");
	    	industriesMap.put("Lumber, Wood Production & Timber Operations","Industry.mfg");
	    	industriesMap.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)","Industry.mfg");
	    	industriesMap.put("Plumbing & HVAC Equipment","Industry.mfg");
	    	industriesMap.put("Motor Vehicles","Industry.mfg");
	    	industriesMap.put("Motor Vehicle Parts","Industry.mfg");
	    	industriesMap.put("Chemicals, Petrochemicals, Glass & Gases","Industry.mfg");
	    	industriesMap.put("Chemicals","Industry.mfg");
	    	industriesMap.put("Gases","Industry.mfg");
	    	industriesMap.put("Glass & Clay","Industry.mfg");
	    	industriesMap.put("Petrochemicals","Industry.mfg");
	    	industriesMap.put("Computer Equipment & Peripherals","Industry.mfg");
	    	industriesMap.put("Personal Computers & Peripherals","Industry.mfg");
	    	industriesMap.put("Computer Networking Equipment","Industry.mfg");
	    	industriesMap.put("Network Security Hardware & Software","Industry.mfg");
	    	industriesMap.put("Computer Storage Equipment","Industry.mfg");
	    	industriesMap.put("Consumer Goods","Industry.mfg");
	    	industriesMap.put("Appliances","Industry.mfg");
	    	industriesMap.put("Cleaning Products","Industry.mfg");
	    	industriesMap.put("Textiles & Apparel","Industry.mfg");
	    	industriesMap.put("Cosmetics, Beauty Supply & Personal Care Products","Industry.mfg");
	    	industriesMap.put("Consumer Electronics","Industry.mfg");
	    	industriesMap.put("Health & Nutrition Products","Industry.mfg");
	    	industriesMap.put("Household Goods","Industry.mfg");
	    	industriesMap.put("Pet Products","Industry.mfg");
	    	industriesMap.put("Photographic & Optical Equipment","Industry.mfg");
	    	industriesMap.put("Sporting Goods","Industry.mfg");
	    	industriesMap.put("Hand, Power and Lawn-care Tools","Industry.mfg");
	    	industriesMap.put("Jewelry & Watches","Industry.mfg");
	    	industriesMap.put("Electronics","Industry.mfg");
	    	industriesMap.put("Batteries, Power Storage Equipment & Generators","Industry.mfg");
	    	industriesMap.put("Electronic Components","Industry.mfg");
	    	industriesMap.put("Power Conversion & Protection Equipment","Industry.mfg");
	    	industriesMap.put("Semiconductor & Semiconductor Equipment","Industry.mfg");
	    	industriesMap.put("Food, Beverages & Tobacco","Industry.mfg");
	    	industriesMap.put("Food & Beverages","Industry.mfg");
	    	industriesMap.put("Tobacco","Industry.mfg");
	    	industriesMap.put("Wineries & Breweries","Industry.mfg");
	    	industriesMap.put("Furniture","Industry.mfg");
	    	industriesMap.put("Manufacturing General","Industry.mfg");
	    	industriesMap.put("Industrial Machinery & Equipment","Industry.mfg");
	    	industriesMap.put("Medical Devices &  Equipment","Industry.mfg");
	    	industriesMap.put("Pulp & Paper","Industry.mfg");
	    	industriesMap.put("Plastic, Packaging & Containers","Industry.mfg");
	    	industriesMap.put("Tires & Rubber","Industry.mfg");
	    	industriesMap.put("Telecommunication  Equipment","Industry.mfg");
	    	industriesMap.put("Test & Measurement Equipment","Industry.mfg");
	    	industriesMap.put("Toys & Games","Industry.mfg");
	    	industriesMap.put("Wire & Cable","Industry.mfg");

	    	industriesMap.put("Metals & Mining","Industry.mm");
	    	industriesMap.put("Metals & Mining General","Industry.mm");
	    	industriesMap.put("Metals & Minerals","Industry.mm");
	    	industriesMap.put("Mining","Industry.mm");

	    	industriesMap.put("Cities, Towns & Municipalities","Industry.municipal");
	    	industriesMap.put("Cities, Towns & Municipalities General","Industry.municipal");
	    	industriesMap.put("Public Safety","Industry.municipal");

	    	industriesMap.put("Organizations","Industry.orgs");
	    	industriesMap.put("Membership Organizations","Industry.orgs");
	    	industriesMap.put("Charitable Organizations & Foundations","Industry.orgs");
	    	industriesMap.put("Organizations General","Industry.orgs");
	    	industriesMap.put("Religious Organizations","Industry.orgs");

	    	industriesMap.put("Real Estate","Industry.realestate");

	    	industriesMap.put("Retail","Industry.retail");
	    	industriesMap.put("Motor Vehicle Dealers","Industry.retail");
	    	industriesMap.put("Automobile Parts Stores","Industry.retail");
	    	industriesMap.put("Record, Video & Book Stores","Industry.retail");
	    	industriesMap.put("Apparel & Accessories Retail","Industry.retail");
	    	industriesMap.put("Gas Stations, Convenience & Liquor Stores","Industry.retail");
	    	industriesMap.put("Department Stores, Shopping Centers & Superstores","Industry.retail");
	    	industriesMap.put("Consumer Electronics & Computers Retail","Industry.retail");
	    	industriesMap.put("Furniture","Industry.retail");
	    	industriesMap.put("Retail General","Industry.retail");
	    	industriesMap.put("Flowers, Gifts & Specialty Stores","Industry.retail");
	    	industriesMap.put("Grocery Retail","Industry.retail");
	    	industriesMap.put("Home Improvement & Hardware Retail","Industry.retail");
	    	industriesMap.put("Vitamins, Supplements, & Health Stores","Industry.retail");
	    	industriesMap.put("Jewelry & Watch Retail","Industry.retail");
	    	industriesMap.put("Office Products Retail & Distribution","Industry.retail");
	    	industriesMap.put("Pet Products","Industry.retail");
	    	industriesMap.put("Drug Stores & Pharmacies","Industry.retail");
	    	industriesMap.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)","Industry.retail");
	    	industriesMap.put("Sporting & Recreational Equipment Retail","Industry.retail");
	    	industriesMap.put("Toys & Games","Industry.retail");
	    	industriesMap.put("Video & DVD Rental","Industry.retail");

	    	industriesMap.put("Software","Industry.software");
	    	industriesMap.put("Software & Technical Consulting","Industry.software");
	    	industriesMap.put("Software General","Industry.software");
	    	industriesMap.put("Software Development & Design","Industry.software");
	    	industriesMap.put("Business Intelligence (BI) Software","Industry.software");
	    	industriesMap.put("Content & Collaboration Software","Industry.software");
	    	industriesMap.put("Customer Relationship Management (CRM) Software","Industry.software");
	    	industriesMap.put("Database & File Management Software","Industry.software");
	    	industriesMap.put("Engineering Software","Industry.software");
	    	industriesMap.put("Enterprise Resource Planning (ERP) Software","Industry.software");
	    	industriesMap.put("Financial, Legal & HR Software","Industry.software");
	    	industriesMap.put("Healthcare Software","Industry.software");
	    	industriesMap.put("Multimedia, Games and Graphics Software","Industry.software");
	    	industriesMap.put("Networking Software","Industry.software");
	    	industriesMap.put("Supply Chain Management (SCM) Software","Industry.software");
	    	industriesMap.put("Security Software","Industry.software");
	    	industriesMap.put("Storage & System Management Software","Industry.software");
	    	industriesMap.put("Retail Software","Industry.software");

	    	industriesMap.put("Telecommunications","Industry.telecom");
	    	industriesMap.put("Cable & Satellite","Industry.telecom");
	    	industriesMap.put("Telecommunications General","Industry.telecom");
	    	industriesMap.put("Internet Service Providers, Website Hosting & Internet-related Services","Industry.telecom");
	    	industriesMap.put("Telephony & Wireless","Industry.telecom");

	    	industriesMap.put("Transportation","Industry.transportation");
	    	industriesMap.put("Airlines, Airports & Air Services","Industry.transportation");
	    	industriesMap.put("Freight & Logistics Services","Industry.transportation");
	    	industriesMap.put("Transportation General","Industry.transportation");
	    	industriesMap.put("Marine Shipping & Transportation","Industry.transportation");
	    	industriesMap.put("Trucking, Moving & Storage","Industry.transportation");
	    	industriesMap.put("Rail, Bus & Taxi","Industry.transportation");
	    	industriesMap.put("Default Industry","UNKNOWN INDUSTRY");

	    	
	    	return industriesMap;
	    }
	    
	    private Map<String, String> getIndustriesMap(){
	    	Map<String,String> industries = new HashMap<String, String>();
	    	industries.put("Default Industry","UNKNOWN INDUSTRY");
	    	industries.put("Agriculture","Industry.agriculture");
	    	industries.put("Animals & Livestock","Industry.agriculture.animals");
	    	industries.put("Crops","Industry.agriculture.crops");
	    	industries.put("Agriculture General","Industry.agriculture.general");
	    	industries.put("Business Services","Industry.bizservice");
	    	industries.put("Accounting & Accounting Services","Industry.bizservice.accounting");
	    	industries.put("Auctions","Industry.bizservice.auction");
	    	industries.put("Call Centers & Business Centers","Industry.bizservice.callcenter");
	    	industries.put("Debt Collection","Industry.bizservice.collection");
	    	industries.put("Management Consulting","Industry.bizservice.consulting");
	    	industries.put("Information & Document Management","Industry.bizservice.datamgmt");
	    	industries.put("Multimedia & Graphic Design","Industry.bizservice.design");
	    	industries.put("Food Service","Industry.bizservice.foodserv");
	    	industries.put("Business Services General","Industry.bizservice.general");
	    	industries.put("Human Resources & Staffing","Industry.bizservice.hr");
	    	industries.put("Facilities Management & Commercial Cleaning","Industry.bizservice.janitor");
	    	industries.put("Translation & Linguistic Services","Industry.bizservice.language");
	    	industries.put("Advertising & Marketing","Industry.bizservice.marketing");
	    	industries.put("Commercial Printing","Industry.bizservice.printing");
	    	industries.put("Security Products & Services","Industry.bizservice.security");
	    	industries.put("Chambers of Commerce","Industry.chamber");
	    	industries.put("Construction","Industry.construction");
	    	industries.put("Architecture, Engineering & Design","Industry.construction.architecture");
	    	industries.put("Commercial & Residential Construction","Industry.construction.construction");
	    	industries.put("Construction General","Industry.construction.general");
	    	industries.put("Consumer Services","Industry.consumerservices");
	    	industries.put("Automotive Service & Collision Repair","Industry.consumerservices.auto");
	    	industries.put("Car & Truck Rental","Industry.consumerservices.carrental");
	    	industries.put("Funeral Homes & Funeral Related Services","Industry.consumerservices.funeralhome");
	    	industries.put("Consumer Services General","Industry.consumerservices.general");
	    	industries.put("Hair Salons","Industry.consumerservices.hairsalon");
	    	industries.put("Laundry & Dry Cleaning Services","Industry.consumerservices.laundry");
	    	industries.put("Photography Studio","Industry.consumerservices.photo");
	    	industries.put("Travel Agencies & Services","Industry.consumerservices.travel");
	    	industries.put("Veterinary Care","Industry.consumerservices.veterinary");
	    	industries.put("Weight & Health Management","Industry.consumerservices.weight");
	    	industries.put("Cultural","Industry.cultural");
	    	industries.put("Cultural General","Industry.cultural.general");
	    	industries.put("Libraries","Industry.cultural.library");
	    	industries.put("Museums & Art Galleries","Industry.cultural.museum");
	    	industries.put("Education","Industry.education");
	    	industries.put("Education General","Industry.education.general");
	    	industries.put("K-12 Schools","Industry.education.k12");
	    	industries.put("Training","Industry.education.training");
	    	industries.put("Colleges & Universities","Industry.education.university");
	    	industries.put("Energy, Utilities & Waste Treatment General","Industry.energy");
	    	industries.put("Energy, Utilities & Waste Treatment","Industry.energy");
	    	industries.put("Electricity, Oil & Gas","Industry.energy.energy");
	    	industries.put("Waste Treatment, Environmental Services & Recycling","Industry.energy.environment");
	    	industries.put("Energy, Utilities, & Waste Treatment General","Industry.energy.general");
	    	industries.put("Oil & Gas Exploration & Services","Industry.energy.services");
	    	industries.put("Water & Water Treatment","Industry.energy.water");
	    	industries.put("Finance","Industry.finance");
	    	industries.put("Banking","Industry.finance.banking");
	    	industries.put("Brokerage","Industry.finance.brokerage");
	    	industries.put("Credit Cards & Transaction  Processing","Industry.finance.creditcards");
	    	industries.put("Finance General","Industry.finance.general");
	    	industries.put("Investment Banking","Industry.finance.investment");
	    	industries.put("Venture Capital & Private Equity","Industry.finance.venturecapital");
	    	industries.put("Government","Industry.government");
	    	industries.put("Healthcare","Industry.healthcare");
	    	industries.put("Emergency Medical Transportation & Services","Industry.healthcare.emergency");
	    	industries.put("Healthcare General","Industry.healthcare.general");
	    	industries.put("Hospitals & Clinics","Industry.healthcare.healthcare");
	    	industries.put("Medical Testing & Clinical Laboratories","Industry.healthcare.medicaltesting");
	    	industries.put("Pharmaceuticals","Industry.healthcare.pharmaceuticals");
	    	industries.put("Biotechnology","Industry.healthcare.pharmaceuticals.biotech");
	    	industries.put("Drug Manufacturing & Research","Industry.healthcare.pharmaceuticals.drugs");
	    	industries.put("Hospitality","Industry.hospitality");
	    	industries.put("Hospitality General","Industry.hospitality.general");
	    	industries.put("Lodging & Resorts","Industry.hospitality.lodging");
	    	industries.put("Recreation","Industry.hospitality.recreation");
	    	industries.put("Movie Theaters","Industry.hospitality.recreation.cinema");
	    	industries.put("Fitness & Dance Facilities","Industry.hospitality.recreation.fitness");
	    	industries.put("Gambling & Gaming","Industry.hospitality.recreation.gaming");
	    	industries.put("Amusement Parks, Arcades & Attractions","Industry.hospitality.recreation.park");
	    	industries.put("Zoos & National Parks","Industry.hospitality.recreation.zoo");
	    	industries.put("Restaurants","Industry.hospitality.restaurant");
	    	industries.put("Sports Teams & Leagues","Industry.hospitality.sports");
	    	industries.put("Insurance","Industry.insurance");
	    	industries.put("Law Firms & Legal Services","Industry.legal");
	    	industries.put("Media & Internet","Industry.media");
	    	industries.put("Broadcasting","Industry.media.broadcasting");
	    	industries.put("Film/Video Production & Services","Industry.media.broadcasting.film");
	    	industries.put("Radio Stations","Industry.media.broadcasting.radio");
	    	industries.put("Television Stations","Industry.media.broadcasting.tv");
	    	industries.put("Media & Internet General","Industry.media.general");
	    	industries.put("Information Collection & Delivery","Industry.media.information");
	    	industries.put("Search Engines & Internet Portals","Industry.media.internet");
	    	industries.put("Music & Music Related Services","Industry.media.music");
	    	industries.put("Newspapers & News Services","Industry.media.news");
	    	industries.put("Publishing","Industry.media.publishing");
	    	industries.put("Manufacturing","Industry.mfg");
	    	industries.put("Aerospace & Defense","Industry.mfg.aerospace");
	    	industries.put("Boats & Submarines","Industry.mfg.boat");
	    	industries.put("Building Materials","Industry.mfg.building");
	    	industries.put("Aggregates, Concrete & Cement","Industry.mfg.building.concrete");
	    	industries.put("Lumber, Wood Production & Timber Operations","Industry.mfg.building.lumber");
	    	industries.put("Miscellaneous Building Materials (Flooring, Cabinets, etc.)","Industry.mfg.building.other");
	    	industries.put("Plumbing & HVAC Equipment","Industry.mfg.building.plumbing");
	    	industries.put("Motor Vehicles","Industry.mfg.car");
	    	industries.put("Motor Vehicle Parts","Industry.mfg.carparts");
	    	industries.put("Chemicals, Petrochemicals, Glass & Gases","Industry.mfg.chemicals");
	    	industries.put("Chemicals","Industry.mfg.chemicals.chemicals");
	    	industries.put("Gases","Industry.mfg.chemicals.gas");
	    	industries.put("Glass & Clay","Industry.mfg.chemicals.glass");
	    	industries.put("Petrochemicals","Industry.mfg.chemicals.petrochemicals");
	    	industries.put("Computer Equipment & Peripherals","Industry.mfg.computers");
	    	industries.put("Personal Computers & Peripherals","Industry.mfg.computers.computers");
	    	industries.put("Computer Networking Equipment","Industry.mfg.computers.networking");
	    	industries.put("Network Security Hardware & Software","Industry.mfg.computers.security");
	    	industries.put("Computer Storage Equipment","Industry.mfg.computers.storage");
	    	industries.put("Consumer Goods","Industry.mfg.consumer");
	    	industries.put("Appliances","Industry.mfg.consumer.appliances");
	    	industries.put("Cleaning Products","Industry.mfg.consumer.cleaning");
	    	industries.put("Textiles & Apparel","Industry.mfg.consumer.clothes");
	    	industries.put("Cosmetics, Beauty Supply & Personal Care Products","Industry.mfg.consumer.cometics");
	    	industries.put("Consumer Electronics","Industry.mfg.consumer.electronics");
	    	industries.put("Health & Nutrition Products","Industry.mfg.consumer.health");
	    	industries.put("Household Goods","Industry.mfg.consumer.household");
	    	industries.put("Pet Products","Industry.mfg.consumer.petproducts");
	    	industries.put("Photographic & Optical Equipment","Industry.mfg.consumer.photo");
	    	industries.put("Sporting Goods","Industry.mfg.consumer.sport");
	    	industries.put("Hand, Power and Lawn-care Tools","Industry.mfg.consumer.tools");
	    	industries.put("Jewelry & Watches","Industry.mfg.consumer.watch");
	    	industries.put("Electronics","Industry.mfg.electronics");
	    	industries.put("Batteries, Power Storage Equipment & Generators","Industry.mfg.electronics.batteries");
	    	industries.put("Electronic Components","Industry.mfg.electronics.electronics");
	    	industries.put("Power Conversion & Protection Equipment","Industry.mfg.electronics.powerequip");
	    	industries.put("Semiconductor & Semiconductor Equipment","Industry.mfg.electronics.semiconductors");
	    	industries.put("Food, Beverages & Tobacco","Industry.mfg.food");
	    	industries.put("Food & Beverages","Industry.mfg.food.food");
	    	industries.put("Tobacco","Industry.mfg.food.tobacco");
	    	industries.put("Wineries & Breweries","Industry.mfg.food.winery");
	    	industries.put("Furniture","Industry.mfg.furniture");
	    	industries.put("Manufacturing General","Industry.mfg.general");
	    	industries.put("Industrial Machinery & Equipment","Industry.mfg.industrialmachinery");
	    	industries.put("Medical Devices &  Equipment","Industry.mfg.medical");
	    	industries.put("Pulp & Paper","Industry.mfg.paper");
	    	industries.put("Plastic, Packaging & Containers","Industry.mfg.plastic");
	    	industries.put("Tires & Rubber","Industry.mfg.rubber");
	    	industries.put("Telecommunication  Equipment","Industry.mfg.telecom");
	    	industries.put("Test & Measurement Equipment","Industry.mfg.testequipment");
	    	industries.put("Toys & Games","Industry.mfg.toys");
	    	industries.put("Wire & Cable","Industry.mfg.wire");
	    	industries.put("Metals & Mining","Industry.mm");
	    	industries.put("Metals & Mining General","Industry.mm.general");
	    	industries.put("Metals & Minerals","Industry.mm.metals");
	    	industries.put("Mining","Industry.mm.mining");
	    	industries.put("Cities, Towns & Municipalities","Industry.municipal");
	    	industries.put("Cities, Towns & Municipalities General","Industry.municipal.general");
	    	industries.put("Public Safety","Industry.municipal.publicsafety");
	    	industries.put("Organizations","Industry.orgs");
	    	industries.put("Membership Organizations","Industry.orgs.association");
	    	industries.put("Charitable Organizations & Foundations","Industry.orgs.foundation");
	    	industries.put("Organizations General","Industry.orgs.general");
	    	industries.put("Religious Organizations","Industry.orgs.religion");
	    	industries.put("Real Estate","Industry.realestate");
	    	industries.put("Retail","Industry.retail");
	    	industries.put("Motor Vehicle Dealers","Industry.retail.auto");
	    	industries.put("Automobile Parts Stores","Industry.retail.autoparts");
	    	industries.put("Record, Video & Book Stores","Industry.retail.book");
	    	industries.put("Apparel & Accessories Retail","Industry.retail.clothes");
	    	industries.put("Gas Stations, Convenience & Liquor Stores","Industry.retail.conveniencestore");
	    	industries.put("Department Stores, Shopping Centers & Superstores","Industry.retail.departmentstore");
	    	industries.put("Consumer Electronics & Computers Retail","Industry.retail.electronics");
	    	industries.put("Furniture","Industry.retail.furniture");
	    	industries.put("Retail General","Industry.retail.general");
	    	industries.put("Flowers, Gifts & Specialty Stores","Industry.retail.gifts");
	    	industries.put("Grocery Retail","Industry.retail.grocery");
	    	industries.put("Home Improvement & Hardware Retail","Industry.retail.hardware");
	    	industries.put("Vitamins, Supplements, & Health Stores","Industry.retail.health");
	    	industries.put("Jewelry & Watch Retail","Industry.retail.jewelry");
	    	industries.put("Office Products Retail & Distribution","Industry.retail.office");
	    	industries.put("Pet Products","Industry.retail.pet");
	    	industries.put("Drug Stores & Pharmacies","Industry.retail.pharmacy");
	    	industries.put("Other Rental Stores (Furniture, A/V, Construction & Industrial Equipment)","Industry.retail.rental");
	    	industries.put("Sporting & Recreational Equipment Retail","Industry.retail.sports");
	    	industries.put("Toys & Games","Industry.retail.toys");
	    	industries.put("Video & DVD Rental","Industry.retail.videorental");
	    	industries.put("Software","Industry.software");
	    	industries.put("Software & Technical Consulting","Industry.software.consulting");
	    	industries.put("Software General","Industry.software.general");
	    	industries.put("Software Development & Design","Industry.software.mfg");
	    	industries.put("Business Intelligence (BI) Software","Industry.software.mfg.bi");
	    	industries.put("Content & Collaboration Software","Industry.software.mfg.content_col");
	    	industries.put("Customer Relationship Management (CRM) Software","Industry.software.mfg.crm");
	    	industries.put("Database & File Management Software","Industry.software.mfg.db");
	    	industries.put("Engineering Software","Industry.software.mfg.eng");
	    	industries.put("Enterprise Resource Planning (ERP) Software","Industry.software.mfg.erp");
	    	industries.put("Financial, Legal & HR Software","Industry.software.mfg.finance");
	    	industries.put("Healthcare Software","Industry.software.mfg.health");
	    	industries.put("Multimedia, Games and Graphics Software","Industry.software.mfg.multimedia");
	    	industries.put("Networking Software","Industry.software.mfg.network");
	    	industries.put("Supply Chain Management (SCM) Software","Industry.software.mfg.scm");
	    	industries.put("Security Software","Industry.software.mfg.security");
	    	industries.put("Storage & System Management Software","Industry.software.mfg.storage");
	    	industries.put("Retail Software","Industry.software.mfg.retail");
	    	industries.put("Telecommunications","Industry.telecom");
	    	industries.put("Cable & Satellite","Industry.telecom.cable");
	    	industries.put("Telecommunications General","Industry.telecom.general");
	    	industries.put("Internet Service Providers, Website Hosting & Internet-related Services","Industry.telecom.internet");
	    	industries.put("Telephony & Wireless","Industry.telecom.telephone");
	    	industries.put("Transportation","Industry.transportation");
	    	industries.put("Airlines, Airports & Air Services","Industry.transportation.airline");
	    	industries.put("Freight & Logistics Services","Industry.transportation.freight");
	    	industries.put("Transportation General","Industry.transportation.general");
	    	industries.put("Marine Shipping & Transportation","Industry.transportation.marine");
	    	industries.put("Trucking, Moving & Storage","Industry.transportation.moving");
	    	industries.put("Rail, Bus & Taxi","Industry.transportation.railandbus");

	    	return industries;
	    }
	    
	    public void downloadConnectReport() throws ParseException,Exception{

	    	List<Campaign> campaignList = campaignService.getActiveOtherCampaigns();
	    	//AmazonS3 s3client = createAWSConnection(clientRegion);
	    	Date date = new Date();
	        //System.out.println(sdf.format(date));
	        String dateFolder = sdf.format(date);
	    	
	       /* boolean exists = s3client.doesObjectExist(bucketName, dateFolder);
	        if (exists) {
	       	 
	        }else{
	       	 createFolder(bucketName,dateFolder,s3client);
	        }*/
	    			
	    	
	    	List<String> staticAttribList = new ArrayList<String>();
	    	staticAttribList.add("prospect.zoomPersonUrl");
	    	staticAttribList.add("prospect.zoomCompanyUrl");
	    	staticAttribList.add("prospect.industry");
	    	staticAttribList.add("prospect.industryGroup");
	    	staticAttribList.add("prospect.prospectCallId");
	    	 staticAttribList.add("prospect.title");
	    	 staticAttribList.add("prospect.managementlevel");
	    	 staticAttribList.add("prospect.department");
	    	 staticAttribList.add("prospect.employeeRange");
	    	 staticAttribList.add("prospect.revenueRange");
	    	 staticAttribList.add("prospect.firstName");
	    	 staticAttribList.add("prospect.lastName");
	    	 staticAttribList.add("prospect.phone");
	    	 staticAttribList.add("prospect.email");
	    	 staticAttribList.add("prospect.organization");
	    	 staticAttribList.add("prospect.address");
	    	 staticAttribList.add("prospect.city");
	    	 staticAttribList.add("prospect.state");
	    	 staticAttribList.add("prospect.zip");
	    	 staticAttribList.add("prospect.country");
	    	 staticAttribList.add("prospect.recordingUrl");
	    	 staticAttribList.add("prospect.fullName");
	    	 
	    	 
	    	
	    //	Campaign campaign = campaignService.getCampaign(campaignId);
	    	 
	    	
	        Date startDate = XtaasDateUtils.getDate(getStartDate(startDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
	        Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
	        System.out.println(startDateInUTC);
	        
	      /*  Date recordingDate = XtaasDateUtils.getDate(getStartDate(recordingDateStr), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
	        Date recordingDateInUTC = XtaasDateUtils.convertToTimeZone(recordingDate, "UTC");
	        System.out.println(recordingDateInUTC);*/
	        
	       /* Date endDate = XtaasDateUtils.getDate(endDateStr, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
	        Date endDateInUTC = XtaasDateUtils.convertToTimeZone(endDate, "UTC");
	        System.out.println(endDateInUTC);*/
	        
	        ArrayList<String> uniqueRecords = new ArrayList<String>();
	     
	       // List<String> sourceId = getId(prospectCallLogList);
	      //  List<ProspectCallInteraction> prospectCallInteractionList = prospectCallInteractionRepository.getId(sourceId);
	        
	        
	        
	        //System.exit(0);
	         Random rnd = new Random();
	       // FileWriter fileWriter = null;
	        try {
	            int recordsCounter = 0;
		            for(Campaign campaign : campaignList){
		            	/*if(campaign.getId().equalsIgnoreCase("5bb657dee4b0d117b3afd9de")){
		            		
		            	}else{
		            		continue;
		            	}*/
		            	
		            	//if(campaign.getClientName()!=null && !campaign.getClientName().isEmpty()){	
		            		System.out.println("CampaignName :" +campaign.getName());
			            	//TODO need to get the s3 bucket name of client from the campaign, this should be configured inc ampaign
			            	File dirFile = new File(campaign.getName());
		    		        if (!dirFile.exists()) {
		    		            if (dirFile.mkdirs()) {
		    		                System.out.println("Directory is created!");
		    		            } else {
		    		                System.out.println("Failed to create directory!");
		    		            }
		    		        }
		            
			            	String folderName = campaign.getName();
		               	    
	               	 
	        			 //////////////// Below logic to download one days success recordings to s3 storage
	               	 
	               	/* if(campaign.getClientName()!=null && campaign.getClientName().equalsIgnoreCase("INSIDEUPTESTING")){
	               		createFolder(bucketName,dateFolder+SUFFIX+folderName,s3client);
	               		List<ProspectCallLog> recordingCallLogList = prospectCallLogRepository.findContactsBySuccess(campaign.getId(), recordingDateInUTC);
		        			 for(ProspectCallLog pcl : recordingCallLogList){
		        				 URL url = new URL(pcl.getProspectCall().getRecordingUrl());
		        				 String cleanCampaignName = campaign.getName().replaceAll("[^a-zA-Z0-9\\s]", "");
		        				 if(campaign.getId().equalsIgnoreCase("5b44c7a5e4b00400c86ba198")){
		        					 cleanCampaignName = "UC";
		        				 }
		        				 if(campaign.getId().equalsIgnoreCase("5b44c7a5e4b00400c86ba198")){
		        					 cleanCampaignName = "UC";
		        				 }
		         				String rfileName = pcl.getProspectCall().getProspect().getCompany().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
		         						+pcl.getProspectCall().getProspect().getLastName().replaceAll("[^a-zA-Z0-9\\s]", "")+"_"
		         						+cleanCampaignName+".wav";
		         				
		         				File rfile = new File(folderName+SUFFIX+rfileName);
		         				fileDownload(url, rfile);
		        			 }
		            }*/
	        		 ////////////////////// End Logic to download one days success recordings to s3 storage
		            	
		            	System.out.println("Processing Data... PLEASE WAIT!!! :)" );
		                List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findContactsBySuccess(campaign.getId(), startDateInUTC);
		                System.out.println("Total records found : " + prospectCallLogList.size() + " for campaign : " + campaign.getName());
		            	String fileName = folderName+SUFFIX+campaign.getName()+".xlsx";
		            	File file = new File(fileName);
		            	Workbook workbook = new XSSFWorkbook();
		            	CreationHelper createHelper = workbook.getCreationHelper();
		            	Sheet sheet = workbook.createSheet("Leads");
		            	
		            	Map<String,String> colNames = new HashMap<String,String>();
		
		            
		              /*   
		                 List<ClientMapping> campaignCMList = clientMappingRepository
		                		 .findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
		                 
		                 
		                
		                 
		                 
		                 Collections.sort(campaignCMList, new ClientMappingComparator());
		                 
		                 int i=0;
		                 String[] columns = new String[50];
		                 for(ClientMapping ccm : campaignCMList){
		                	 if(colNames.containsKey(ccm.getColHeader())){
		                		 continue;
		                	 }else{
		                		 System.out.println(ccm.getColHeader());
		                		 colNames.put(ccm.getColHeader(),ccm.getStdAttribute());
		                		 columns[i] = ccm.getColHeader();
		                		 i++;
		                	 }
		                 }*/
		            	
		            	int i=0;
		            	String[] columns = new String[50];
		                 colNames.put("First Name","First Name");
		                 columns[i]= "First Name";
		                 i++;
		                 colNames.put("Last Name","Last Name");
		                 columns[i]= "Last Name";
		                 i++;
		                 colNames.put("Job Title","Job Title");
		                 columns[i]= "Job Title";
		                 i++;
		                 colNames.put("Email ID","Email ID");
		                 columns[i]= "Email ID";
		                 i++;
		                 colNames.put("Phone No.","Phone No.");
		                 columns[i]= "Phone No.";
		                 i++;
		                 colNames.put("Company Name","Company Name");
		                 columns[i]= "Company Name";
		                 i++;
		                 colNames.put("Address Line 1","Address Line 1");
		                 columns[i]= "Address Line 1";
		                 i++;
		                 colNames.put("Address Line 2","Address Line 2");
		                 columns[i]= "Address Line 2";
		                 i++;
		                 colNames.put("City","City");
		                 columns[i]= "City";
		                 i++;
		                 colNames.put("State","State");
		                 columns[i]= "State";
		                 i++;
		                 colNames.put("Postal Code","Postal Code");
		                 columns[i]= "Postal Code";
		                 i++;
		                 colNames.put("Country","Country");
		                 columns[i]= "Country";
		                 i++;
		                 colNames.put("Company Employee Size Min","Company Employee Size Min");
		                 columns[i]= "Company Employee Size Min";
		                 i++;
		                 colNames.put("Company Employee Size Max","Company Employee Size Max");
		                 columns[i]= "Company Employee Size Max";
		                 i++;
		                 colNames.put("Company Revenue Min","Company Revenue Min");
		                 columns[i]= "Company Revenue Min";
		                 i++;
		                 colNames.put("Company Revenue Max","Company Revenue Max");
		                 columns[i]= "Company Revenue Max";
		                 i++;
		                 colNames.put("Industry Type","Industry Type");
		                 columns[i]= "Industry Type";

		                 List<String> questionKeyList = new ArrayList<String>();
		                 HashMap<String,ArrayList<PickListItem>> questionsList = campaign.getQualificationCriteria().getQuestions();
		                 for (Map.Entry<String,ArrayList<PickListItem>> entry : questionsList.entrySet())  {
		                 	questionKeyList.add(entry.getKey());
		                 	i++;
			                colNames.put(entry.getKey(),entry.getKey());
			                columns[i]= entry.getKey();
		                 }
		                 i++;
		                 colNames.put("Asset title","Asset title");
		                 columns[i]= "Asset title";
		                 i++;
		                 colNames.put("Download Date","Download Date");
		                 columns[i]= "Download Date";
		                 i++;
		                 
		                 
		                 /*i++;
		                 colNames.put("Status","Status");
		                 columns[i]= "Status";
		                 i++;
		                 colNames.put("Reason","Reason");
		                 columns[i]= "Reason";
		                 i++;
		                 colNames.put("Notes","Notes");
		                 columns[i]= "Notes";
		                 i++;
		                 colNames.put("DownloadDate","DownloadDate");
		                 columns[i]= "DownloadDate";
		                 colNames.put("PartnerId","PartnerId");
		                 columns[i]= "PartnerId";
		                 i++;
		                 colNames.put("AgentId","AgentId");
		                 columns[i]= "AgentId";
		                 */
		                 i++;
		                 colNames.put("QA Status","QA Status");
		                 columns[i]= "QA Status";
		                 i++;
		                 colNames.put("C1. Lead Valid","C1. Lead Valid");
		                 columns[i]= "C1. Lead Valid";
		                 i++;
		                 colNames.put("Reject Reason","Reject Reason");
		                 columns[i]= "Reject Reason";
		                 i++;
		                 colNames.put("C1. Notes","C1. Notes");
		                 columns[i]= "C1. Notes";
		                 i++;
		                 colNames.put("A1. Call Rcdg","A1. Call Rcdg");
		                 columns[i]= "A1. Call Rcdg";
		                 i++;
		                 colNames.put("Partner ID","Partner ID");
		                 columns[i]= "Partner ID";
		                 i++;
		                 colNames.put("Agent ID","Agent ID");
		                 columns[i]= "Agent ID";
		                 //////////
		                 i++;
		                 colNames.put("A2. Branding","A2. Branding");
		                 columns[i]= "A2. Branding";
		                 i++;
		                 colNames.put("A3. Pitch","A3. Pitch");
		                 columns[i]= "A3. Pitch";
		                 i++;
		                 colNames.put("B1. Interest","B1. Interest");
		                 columns[i]= "B1. Interest";
		                 i++;
		                 colNames.put("B2. Qual Ques","B2. Qual Ques");
		                 columns[i]= "B2. Qual Ques";
		                 i++;
		                 colNames.put("B3. Contact Dtls","B3. Contact Dtls");
		                 columns[i]= "B3. Contact Dtls";
		                 i++;
		                 colNames.put("B4. Direct Number","B4. Direct Number");
		                 columns[i]= "B4. Direct Number";
		                 i++;
		                 colNames.put("B5. Email Addy","B5. Email Addy");
		                 columns[i]= "B5. Email Addy";
		                 i++;
		                 colNames.put("B6. Consent","B6. Consent");
		                 columns[i]= "B6. Consent";
		                 //////////
		                 i++;
		                 colNames.put("Zoom Company Link","Zoom Company Link");
		                 columns[i]= "Zoom Company Link";
		                 i++;
		                 colNames.put("Zoom Employee Link","Zoom Employee Link");
		                 columns[i]= "Zoom Employee Link";
		                 
		
		            
		        		 //Add a new line separator after the header
		        		// Create a Font for styling header cells
		                Font headerFont = workbook.createFont();
		                headerFont.setBold(true);
		                headerFont.setFontHeightInPoints((short) 14);
		                headerFont.setColor(IndexedColors.RED.getIndex());
		
		                // Create a CellStyle with the font
		                CellStyle headerCellStyle = workbook.createCellStyle();
		                headerCellStyle.setFont(headerFont);
		
		                // Create a Row
		                Row headerRow = sheet.createRow(0);
		                
		                for(int x = 0; x < columns.length; x++) {
		                	if(columns[x] !=null && !columns[x].isEmpty()){
			                    Cell cell = headerRow.createCell(x);
			                    cell.setCellValue(columns[x]);
			                    cell.setCellStyle(headerCellStyle);
		                	}
		                }
		
		                // Create Cell Style for formatting Date
		                CellStyle dateCellStyle = workbook.createCellStyle();
		                dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
			
		                 
		                 
		                 int rowNum = 1;
		                int j=0;
		                
		                
			            for (ProspectCallLog prospectCallLog : prospectCallLogList) {
			            	Row row = sheet.createRow(rowNum++);
			            	ProspectCall prospectCall = prospectCallLog.getProspectCall();
			        		Prospect prospect = prospectCallLog.getProspectCall().getProspect();
			        	   
			        	   for(int col = 0; col < columns.length; col++) {
			                	if(columns[col] !=null && !columns[col].isEmpty()){
			                		
			                		if(columns[col].contentEquals("First Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getFirstName());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Last Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getLastName());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Job Title")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getTitle());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Email ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getEmail());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Phone No.")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getPhone());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Name")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCompany());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Address Line 1")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getAddressLine1());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Address Line 2")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getAddressLine2());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("City")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCity());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("State")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getStateCode());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Postal Code")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getZipCode());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Country")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCountry());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Employee Size Min")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCustomAttributeValue("minEmployeeCount").toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Employee Size Max")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCustomAttributeValue("maxEmployeeCount").toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Revenue Min")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCustomAttributeValue("minRevenue").toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Company Revenue Max")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getCustomAttributeValue("maxRevenue").toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Industry Type")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospect.getIndustry());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Asset title")){
			                			Cell cellx = row.createCell(col);
			                			String assetName = "";
			                			if(prospectCall.getDeliveredAssetId() != null && !prospectCall.getDeliveredAssetId().isEmpty()){
			                				List<Asset> assetlist = campaign.getActiveAsset();
			                				for (Asset asset : assetlist) {
												if(asset.getAssetId().equals(prospectCall.getDeliveredAssetId())) {
													assetName = asset.getName();
												}
											}
			                			}
					                    cellx.setCellValue(assetName);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Download Date")){
			                			Cell cellx = row.createCell(col);
			                			DateFormat pstFormat = new SimpleDateFormat("MM/dd/yyyy");
			                			pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));
			                			String pstStr = pstFormat.format(prospectCallLog.getProspectCall().getCallStartTime());
			                			Date pstDate = pstFormat.parse(pstStr);
			                			
			                			System.out.println(pstFormat.format(pstDate));
					                    cellx.setCellValue(pstFormat.format(pstDate));
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("QA Status")){
			                			Cell cellx = row.createCell(col);
			                			cellx.setCellValue(prospectCallLog.getStatus().toString());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("C1. Lead Valid")){
			                			String valid="";
			                			if(prospectCallLog.getQaFeedback()!=null && 
			                					prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								valid = fra.getFeedback();
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(valid);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Reject Reason")){
			                			String qaReason = "";
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								if(fra.getRejectionReason()!=null){
			                									qaReason = qaReason+fra.getRejectionReason();
			                								}
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(qaReason);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("C1. Notes")){
			                			String callnotes = "";
			                			if(prospectCallLog.getQaFeedback()!=null && prospectCallLog.getQaFeedback().getFeedbackResponseList()!=null){
			                				List<FeedbackSectionResponse> fsrList = prospectCallLog.getQaFeedback().getFeedbackResponseList();
			                				for(FeedbackSectionResponse fsr : fsrList){
			                					if(fsr.getSectionName().equalsIgnoreCase("Lead Validation")){
			                						List<FeedbackResponseAttribute> fraList = fsr.getResponseAttributes();
			                						for(FeedbackResponseAttribute fra : fraList){
			                							if(fra.getAttribute().equalsIgnoreCase("LEAD_VALIDATION_VALID")){
			                								if(fra.getAttributeComment()!=null){
			                									callnotes = fra.getAttributeComment();
			                								}
			                							}
			                						}
			                					}
			                				}
			                			}
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(callnotes);
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("A1. Call Rcdg")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getProspectCall().getRecordingUrl());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Partner ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getProspectCall().getPartnerId());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Agent ID")){
			                			Cell cellx = row.createCell(col);
					                    cellx.setCellValue(prospectCallLog.getProspectCall().getAgentId());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Zoom Company link")){
			                			Cell cellx = row.createCell(col);
//					                    cellx.setCellValue(prospectCallLog.getProspectCall().getZoomCompanyUrl());
					                    cellx.setCellStyle(dateCellStyle);
			                		}else if(columns[col].contentEquals("Zoom Employee Link")){
			                			Cell cellx = row.createCell(col);
//					                    cellx.setCellValue(prospectCallLog.getProspectCall().getZoomPersonUrl());
					                    cellx.setCellStyle(dateCellStyle);
			                		}
			                		else{
			                			for(String question :questionKeyList){
				                			if(prospectCall.getAnswers()!=null){    		
				                		    	for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
				                		   			if (agentQaAnswer.getQuestion()!=null 
				                		   					&& question.equalsIgnoreCase(agentQaAnswer.getQuestion())){
				                		   				String agentAns = agentQaAnswer.getAnswer();
				                		   				Cell cellx = row.createCell(col);
									                    cellx.setCellValue(agentAns);
									                    cellx.setCellStyle(dateCellStyle);
				                		   			}
				                		       	 }
				                			}
				                		}
			                		}
			                	}
			        	   }
				            
			                 recordsCounter++;
			            }
			            
			            
			            /*Industry
			            TITLE - like match
			            MANAGEMENT LEVEL
			            Department
			            employeemin
			            employeemax
			            revenuemin
			            revenuemax
			            Questions*/
			         // Write the output to a file
			            FileOutputStream fileOut = new FileOutputStream(fileName);
			            workbook.write(fileOut);
			            fileOut.close();
		
			            // Closing the workbook
			            workbook.close();
			            System.out.println("CSV file for campaign : "+campaign.getName()+" was created successfully. Records Exported : " + recordsCounter);
			            //SEnd email
			            List<File> fileNames = new ArrayList<File>();
			            fileNames.add(file);
			            String message = campaign.getName()+" Attached Lead file for the previous day";
					/*	XtaasEmailUtils.sendEmail("data@xtaascorp.com", "data@xtaascorp.com",
								campaign.getName()+" Lead Report File Attached.", message,fileNames);*/
			            
			            XtaasEmailUtils.sendEmail("ssalve@xtaascorp.com", "data@xtaascorp.com",
								campaign.getName()+" Lead Report File Attached.", message,fileNames);
						System.out.println("Lead file delivered for "+campaign.getName());
//			            makeZipFile(dirFile,campaign.getName());
						/*if(campaign.getClientName()!=null && campaign.getClientName().equalsIgnoreCase("INSIDEUPTESTING")){
							List<String> fileList = new ArrayList < String > ();
							generateFileList(new File(folderName),folderName,fileList);
							String zipFile = folderName+SUFFIX+folderName+ext;
							zipIt(zipFile,folderName,fileList);
			            
							String afileName = dateFolder+ SUFFIX +folderName + SUFFIX + zipFile;
							s3client.putObject(new PutObjectRequest(bucketName, afileName, 
	     						new File(zipFile))
	     						.withCannedAcl(CannedAccessControlList.PublicRead));
						}
	     				System.out.println("Recording File uploade");*/
			            
			           
						/*String efileName = dateFolder+ SUFFIX +folderName + SUFFIX + file;
	     				s3client.putObject(new PutObjectRequest(bucketName, efileName, 
	     						file)
	     						.withCannedAcl(CannedAccessControlList.PublicRead));
	     				*/System.out.println("Lead File uploaded");
	     				
	     				/*if(file.delete()) {
	     					System.out.println("File DELETED from server successfully");
	     				}else{
	     					System.out.println("File NOT DELETED from server successfully");
	     				}*/
		            
		           
		            deleteDir(dirFile);
		            //}
		        }
	            
	        } catch (Exception e) {
	            System.out.println("Error in CsvFileWriter !!!");
	            e.printStackTrace();
	        } /*finally {
	            try {
	                fileWriter.flush();
	                fileWriter.close();
	            } catch (IOException e) {
	                System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
	            }
	        }*/
	        		////////////////////////////////////
	            
	    
	    }
	    
}

