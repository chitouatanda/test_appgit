package com.xtaas.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Java program to generate MD5 checksum for files in Java. This Java example
 * uses core Java security package and Apache commons codec to generate MD5
 * checksum for a File.
 *
 * @author Javin Paul
 */
public class MD5Checksum {
    private static final Logger logger = Logger.getLogger(MD5Checksum.class.getName());
   
    public static void main(String args[]) {
        String file = "abc.txt";
      
        
        checkMd5Suppression("hbaba@xtaascorp.com");
     checkMd5Suppression("haji@xtaascorp.com");


    }
  
  
    /*
     * From Apache commons codec 1.4 md5() and md5Hex() method accepts InputStream as well.
     * If you are using lower version of Apache commons codec than you need to convert
     * InputStream to byte array before passing it to md5() or md5Hex() method.
     */
    public static void checkMd5Suppression(String prospectEmail){
    	 String suppressedEmail = "d7b4ef72a0cd08a989ec500a099ddbcd";// this one will be the value in suppression collection 
        try {
        	// prospects email should be first converted to md5Hex and compared
        	
        	String md5Hex = DigestUtils
        		      .md5Hex(prospectEmail);
        	if(md5Hex.equals(suppressedEmail)) 
        		System.out.println(prospectEmail+"--->This email is suppressed. dont sent email");
        	else
        		System.out.println(prospectEmail+"--->Send Email");


        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        
    }

}


