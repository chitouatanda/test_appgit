package com.xtaas.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import de.taimos.totp.TOTP;

/**
 * This example program shows how AES encryption and decryption can be done in Java.
 * Please note that secret key and encrypted text is unreadable binary and hence
 * in the following program we display it in hexadecimal format of the underlying bytes.
 * @author Jayson
 */

public class AESEncryption {
/**
 1. Generate a plain text for encryption
 2. Get a secret key (printed in hexadecimal form). In actual use this must
 by encrypted and kept safe. The same key is required for decryption.
 3.
 */
    public static void main(String[] args) throws Exception {
    	
        //System.out.println("TOTP XTAAS :" + generateSecretKey());
    	//MIBKWHREVO6D2HPDXOL6SWCOT5XNOPJS
    	
    	   String secretKey = "MIBKWHREVO6D2HPDXOL6SWCOT5XNOPJS";
		/* 	String email = "test@gmail.com";
		 	String companyName = "My Awesome Company";
    	 
    	String barcode = getGoogleAuthenticatorBarCode(secretKey,email , companyName);
    	createQRCode(barcode,"barcode",30,30);   	
    	*/
    	////////////////////
    	
    	String lastCode = null;
    	while (true) {
    	    String code = getTOTPCode(secretKey);
    	    if (!code.equals(lastCode)) {
    	        System.out.println(code);
    	    }
    	    lastCode = code;
    	    try {
    	        Thread.sleep(1000);
    	    } catch (InterruptedException e) {};
    	}
    	
    	////////////////////
      /*  String plainText = "Hello World";
        String sKey = "KqBJ0i0pvi5e2gZzPV4eLg==";//getSecretEncryptionKey();
        byte[] decodedKey = Base64.getDecoder().decode(sKey);
        		
        // rebuild key using SecretKeySpec
        SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
        String cipherText = encryptText(plainText, secKey);
        String decryptedText = decryptText(cipherText, secKey);
   
        System.out.println("Original Text:" + plainText);
        System.out.println("Encrypted Text :"+cipherText);
        System.out.println("Descrypted Text:"+decryptedText);*/
         
   }
    public static String generateSecretKey() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        Base32 base32 = new Base32();
        return base32.encodeToString(bytes);
    }
    
    public static String getGoogleAuthenticatorBarCode(String secretKey, String account, String issuer) {
        try {
            return "otpauth://totp/"
                    + URLEncoder.encode(issuer + ":" + account, "UTF-8").replace("+", "%20")
                    + "?secret=" + URLEncoder.encode(secretKey, "UTF-8").replace("+", "%20")
                    + "&issuer=" + URLEncoder.encode(issuer, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }
    
    public static void createQRCode(String barCodeData, String filePath, int height, int width)
            throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE,
                width, height);
        try (FileOutputStream out = new FileOutputStream(filePath)) {
            MatrixToImageWriter.writeToStream(matrix, "png", out);
        }
    }
    
    public static String getTOTPCode(String secretKey) {
        Base32 base32 = new Base32();
        byte[] bytes = base32.decode(secretKey);
        String hexKey = Hex.encodeHexString(bytes);
        return TOTP.getOTP(hexKey);
    }
     
 /**
     * gets the AES encryption key. In your actual programs, this should be safely
     * stored.
     * @return
     * @throws Exception
     */

    public static String getSecretEncryptionKey() throws Exception{
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(256); // The AES key size in number of bits
        SecretKey secKey = generator.generateKey();
        String encodedKey = Base64.getEncoder().encodeToString(secKey.getEncoded());
        return encodedKey;
    }
     
    /**
     * Encrypts plainText in AES using the secret key
     * @param plainText
     * @param secKey
     * @return
     * @throws Exception
     */
    public static String encryptText(String plainText,SecretKey secKey) throws Exception{
        // AES defaults to AES/ECB/PKCS5Padding in Java 7
        Cipher aesCipher = Cipher.getInstance("AES");

        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

      byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
     String enText =  Base64.getEncoder().encodeToString(byteCipherText);
        return enText;

   }

     
    /**
     * Decrypts encrypted byte array using the key used for encryption.
     * @param byteCipherText
     * @param secKey
     * @return
     * @throws Exception
     */
    public static String decryptText(String cipherText, SecretKey secKey) throws Exception {
        // AES defaults to AES/ECB/PKCS5Padding in Java 7
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(cipherText));
       /* byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
        return new String(original);*/
        return new String(bytePlainText);

   }

     

}
