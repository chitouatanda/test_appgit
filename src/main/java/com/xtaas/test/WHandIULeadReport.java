/**
 * 
 */
package com.xtaas.test;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.poi.ss.usermodel.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.RealTimeDelivery;
import com.xtaas.db.entity.RealTimeDeliveryIndustryMapping;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.RealTimeDeliveryRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.domain.valueobject.CallSection;
import com.xtaas.domain.valueobject.DayOfWeek;
import com.xtaas.domain.valueobject.RealTimePost;
import com.xtaas.domain.valueobject.Rebuttal;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.service.RealTimeDeliveryIndustryMappingService;
import com.xtaas.utils.XMLConverterUtil;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.web.dto.Attribute;
import com.xtaas.web.dto.CallGuideDTO;
import com.xtaas.web.dto.LeadformAttributes;
import com.xtaas.web.dto.WorkScheduleDTO;

/**
 * @author hbaba
 *
 */
@Component
public class WHandIULeadReport {
	int counter = 0;
	private List<String> getCampaignIds(){
		List<String> campaignIds = new ArrayList<String>();
	//	campaignIds.add("5a6057efe4b0e3c49755d043");
	//	campaignIds.add("5b9bbb55e4b083b7c04210fd");
		campaignIds.add("5aebbcade4b0d459d5c118a7");
	//	campaignIds.add("5b0e73ede4b075ce09aa262d");
	//	campaignIds.add("5b29e2b4e4b0532d2251e4ce");
	//	campaignIds.add("5b755900e4b066e7dba3080b");
		return campaignIds;
	}
	//private String campaignId = "5acba911e4b016cf5a9db837";// this will be input parameter
			//"5a6057efe4b0e3c49755d043"; //WOQ
	private String leadDays = "4 Days";  // this will be input parameter
	private String fileName = "leadFileUNIFIIED.xlsx"; //  this will be input parameter
	
	private String campaignName = "";
	
	
	private static String[] wheelhouseHeaders = {"auth_code","source","campaign_code","asset_name","category_name","success_email","error_email","lead_id","is_test_mode","first_name","last_name","phone","email","company_name","job_title","industry","company_size","address1","city","state","postal_code"};
	private static String[] insideupHeaders = {"catId","iusrc","firstName","lastName","phoneNumber","address","email","zip","company","title","recording_name","industry","employee","Q1","Q2","Q3","Q4","Q5","Q6","Q7","Q8","Q9","Q10"};
    

	
	@Autowired
	private ClientMappingRepository clientMappingRepository;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@Autowired
	private RealTimeDeliveryRepository realTimeDeliveryRepository;
	

	@Autowired
	private RealTimeDeliveryIndustryMappingService realTimeDeliveryIndustryMappingService;
	
	@Autowired
	private CampaignService campaignService;
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		WHandIULeadReport whandiuleadreport = context.getBean(WHandIULeadReport.class);
		
		// Login first
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("djain", "secret", authorities));
				
	
		
		whandiuleadreport.sendLeadToClient();

	//	campaignTest.getCampaign("58ac0e56e4b0669cdd2376fe");
//		campaignTest.getCampaign("560a4e11e4b07545026a250c");
	}
	
	public void sendLeadToClient() {
		
		String toDate = "";
		//////////////////////////////
		
		try{
			
			// Create a Workbook
	        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

	        /* CreationHelper helps us create instances of various things like DataFormat, 
	           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
	        CreationHelper createHelper = workbook.getCreationHelper();

	        // Create a Sheet
	        Sheet sheet = workbook.createSheet("LeadDetails");

	        // Create a Font for styling header cells
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setFontHeightInPoints((short) 14);
	        headerFont.setColor(IndexedColors.RED.getIndex());

	        // Create a CellStyle with the font
	        CellStyle headerCellStyle = workbook.createCellStyle();
	       headerCellStyle.setFont(headerFont);

	        

			
			
		
		
		
		
		
		////////////////////////////////
		
		
		  
		
		   Calendar c = Calendar.getInstance();
		   System.out.println("Current date : " + (c.get(Calendar.MONTH) + 1) +
				   "-" + c.get(Calendar.DATE) + "-" + c.get(Calendar.YEAR));

		   	String periodActual = leadDays;
	    	 int beginIndex = periodActual.indexOf(" ");
	    	 String periodStr = periodActual.substring(0, beginIndex);
	    	 int period = Integer.parseInt(periodStr);
	    	 //logger.info(period);
		   
		 if(leadDays.contains("Years")){
			   c.add(Calendar.YEAR, -period);
			   toDate = (c.get(Calendar.MONTH) + 1) +
					   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(leadDays.contains("Months")){
	    		  c.add(Calendar.MONTH, -period);
	    		  toDate = (c.get(Calendar.MONTH) + 1) +
	   				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(leadDays.contains("Weeks")){
	    		 c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}else if(leadDays.contains("Days")){
	    		 c.add(Calendar.DAY_OF_MONTH, -period);
	    		 toDate = (c.get(Calendar.MONTH) + 1) +
	  				   "/" + c.get(Calendar.DATE) + "/" + c.get(Calendar.YEAR) + " 00:00:00";
	    	}
		 Date startDate = XtaasDateUtils.getDate(toDate, XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		    Date startDateInUTC = XtaasDateUtils.convertToTimeZone(startDate, "UTC");
		 
		

		//if (realTimeDelivery != null && realTimeDelivery.getRealTimePost().equals(realTimePost)) {
			List<List<NameValuePair>> completeList = new ArrayList<List<NameValuePair>>();
			boolean isWheelHouse = false;
			boolean isInsideUp = false;
			List<String> campaignIds = getCampaignIds();
			for(String campaignId : campaignIds){
				List<ProspectCallLog> prospectCallLogList = prospectCallLogRepository.findContactsBySuccess(campaignId, startDateInUTC);
				List<String> cid = new ArrayList<String>(1);
				cid.add(campaignId);
				List<Campaign> camp = campaignService.getCampaignByIds(cid);
				Campaign can = camp.get(0);
				campaignName = can.getName();
				String prospectCampaignID = campaignId;
				RealTimeDelivery realTimeDelivery = getRealTimeDelivery(prospectCampaignID);
				for(ProspectCallLog prospectCallLogFromDB : prospectCallLogList){
						if (realTimeDelivery.getClientName().equalsIgnoreCase(XtaasConstants.WHEELHOUSE_CLIENT)) {
							isWheelHouse=true;
							counter++;
							System.out.println(counter+":Sending real time lead for [{}] client for prospectCallId [{}]."+
									realTimeDelivery.getClientName()+
									prospectCallLogFromDB.getProspectCall().getProspectCallId());
							// Create a Row
					        Row headerRow = sheet.createRow(0);
					        for(int i = 0; i < wheelhouseHeaders.length; i++) {
					            Cell cell = headerRow.createCell(i);
					            cell.setCellValue(wheelhouseHeaders[i]);
					            cell.setCellStyle(headerCellStyle);
					        }
					     // Create Cell Style for formatting Date
					        CellStyle dateCellStyle = workbook.createCellStyle();
					        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
	
					        List<NameValuePair> nameValuePair = prepareRequestBodyWheelhouse(prospectCallLogFromDB, realTimeDelivery);
					        completeList.add(nameValuePair);
						} else if (realTimeDelivery.getClientName().equalsIgnoreCase(XtaasConstants.INSIDEUP_CLIENT)) {
							isInsideUp=true;
							System.out.println(counter+":Sending real time lead for [{}] client for prospectCallId [{}]."+
									realTimeDelivery.getClientName()+
									prospectCallLogFromDB.getProspectCall().getProspectCallId());
							// Create a Row
					        Row headerRow = sheet.createRow(0);
					        for(int i = 0; i < insideupHeaders.length; i++) {
					            Cell cell = headerRow.createCell(i);
					            cell.setCellValue(insideupHeaders[i]);
					            cell.setCellStyle(headerCellStyle);
					        }
					     // Create Cell Style for formatting Date
					        CellStyle dateCellStyle = workbook.createCellStyle();
					        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
	
					        List<NameValuePair> nameValuePair =  prepareRequestBodyInsideUp(prospectCallLogFromDB, realTimeDelivery);
					        completeList.add(nameValuePair);
						}
					}
			}
				
				int rowNum = 1;
				//Inserting to EXcel sheet
				for(List<NameValuePair> nvPair: completeList) {
					Row row = sheet.createRow(rowNum++);
					int colNum = 0;
					Map<String,String> nvMap = new HashMap<String, String>();
					for(NameValuePair nv : nvPair){
						nvMap.put(nv.getName(), nv.getValue());
					}
					if(isWheelHouse){
						row.createCell(0)
	                    .setCellValue(nvMap.get("auth_code"));

						row.createCell(1)
	                    .setCellValue(nvMap.get("source"));
						
						row.createCell(2)
	                    .setCellValue(nvMap.get("campaign_code"));
						
						row.createCell(3)
	                    .setCellValue(nvMap.get("asset_name"));
						

						row.createCell(4)
	                    .setCellValue(nvMap.get("category_name"));
						
						row.createCell(5)
	                    .setCellValue(nvMap.get("success_email"));
						
						row.createCell(6)
	                    .setCellValue(nvMap.get("error_email"));
						
						row.createCell(7)
	                    .setCellValue(nvMap.get("lead_id"));
						
						row.createCell(8)
	                    .setCellValue(nvMap.get("is_test_mode"));
						
						row.createCell(9)
	                    .setCellValue(nvMap.get("first_name"));
						
						row.createCell(10)
	                    .setCellValue(nvMap.get("last_name"));
						
						row.createCell(11)
	                    .setCellValue(nvMap.get("phone"));
						
						row.createCell(12)
	                    .setCellValue(nvMap.get("email"));
						
						row.createCell(13)
	                    .setCellValue(nvMap.get("company_name"));
						
						row.createCell(14)
	                    .setCellValue(nvMap.get("job_title"));
						
						row.createCell(15)
	                    .setCellValue(nvMap.get("industry"));
						
						row.createCell(16)
	                    .setCellValue(nvMap.get("company_size"));
						
						row.createCell(17)
	                    .setCellValue(nvMap.get("address1"));
						
						row.createCell(18)
	                    .setCellValue(nvMap.get("city"));
						
						row.createCell(19)
	                    .setCellValue(nvMap.get("state"));
						
						row.createCell(20)
	                    .setCellValue(nvMap.get("postal_code"));
						

						/*Cell dateOfBirthCell = row.createCell(2);
						dateOfBirthCell.setCellValue(employee.getDateOfBirth());
						dateOfBirthCell.setCellStyle(dateCellStyle);*/

						/*row.createCell(3)
	                    .setCellValue(employee.getSalary());
						"auth_code","source","campaign_code","asset_name","category_name","success_email","error_email","lead_id","is_test_mode",
						"first_name","last_name","phone","email","company_name","job_title","industry","company_size","address1","city","state",
						"postal_code"
*/
					}else if(isInsideUp){
						/*"catId","iusrc","firstName","firstName","lastName","phoneNumber","address","email","zip","company","title",
						"recording_name","industry","employee","Q1","Q2","Q3","Q4","Q5","Q6","Q7","Q8","Q9","Q10"*/
						row.createCell(0)
	                    .setCellValue(nvMap.get("catId"));

						row.createCell(1)
	                    .setCellValue(nvMap.get("iusrc"));
						
						row.createCell(2)
	                    .setCellValue(nvMap.get("firstName"));
						
						row.createCell(3)
	                    .setCellValue(nvMap.get("lastName"));
						
						row.createCell(4)
	                    .setCellValue(nvMap.get("phoneNumber"));
						
						row.createCell(5)
	                    .setCellValue(nvMap.get("address"));
						
						row.createCell(6)
	                    .setCellValue(nvMap.get("email"));
						
						row.createCell(7)
	                    .setCellValue(nvMap.get("zip"));
						
						row.createCell(8)
	                    .setCellValue(nvMap.get("company"));
						
						row.createCell(9)
	                    .setCellValue(nvMap.get("title"));
						
						row.createCell(10)
	                    .setCellValue(nvMap.get("recording_name"));
						
						row.createCell(11)
	                    .setCellValue(nvMap.get("industry"));
						
						row.createCell(12)
	                    .setCellValue(nvMap.get("employee"));
						
						row.createCell(13)
	                    .setCellValue(nvMap.get("Q1"));
						
						row.createCell(14)
	                    .setCellValue(nvMap.get("Q2"));
						
						row.createCell(15)
	                    .setCellValue(nvMap.get("Q3"));
						
						row.createCell(16)
	                    .setCellValue(nvMap.get("Q4"));
						
						row.createCell(17)
	                    .setCellValue(nvMap.get("Q5"));
						
						row.createCell(18)
	                    .setCellValue(nvMap.get("Q6"));
						
						row.createCell(19)
	                    .setCellValue(nvMap.get("Q7"));
						
						row.createCell(20)
	                    .setCellValue(nvMap.get("Q8"));
						
						row.createCell(21)
	                    .setCellValue(nvMap.get("Q9"));
						
						row.createCell(22)
	                    .setCellValue(nvMap.get("Q10"));
					}
					
					
		        }
				// Resize all columns to fit the content size
				if(isWheelHouse){
			        for(int i = 0; i < wheelhouseHeaders.length; i++) {
			            sheet.autoSizeColumn(i);
			        }
				}else if(isInsideUp){
					for(int i = 0; i < insideupHeaders.length; i++) {
			            sheet.autoSizeColumn(i);
			        }
				}

		        // Write the output to a file
		        FileOutputStream fileOut = new FileOutputStream(fileName);
		        workbook.write(fileOut);
		        fileOut.close();
		        
		     // Closing the workbook
		        workbook.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		//}
	}
	
	private RealTimeDelivery getRealTimeDelivery(String xtaasCampaignId) {
		return realTimeDeliveryRepository.findByXtaasCampaignId(xtaasCampaignId);
	}

	@SuppressWarnings("deprecation")
	private List<NameValuePair> prepareRequestBodyWheelhouse(ProspectCallLog prospectCallLogFromDB, RealTimeDelivery realTimeDelivery)
			throws Exception {
		String prospectCampaignID = prospectCallLogFromDB.getProspectCall().getCampaignId();

	//	System.out.println("Sending real time lead [{}] "+realTimeDelivery.toString());
		String postUrl = realTimeDelivery.getPostUrl();

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair(realTimeDelivery.getAuthCodeKey(), realTimeDelivery.getAuthCodeValue()));
		nameValuePairs.add(new BasicNameValuePair("source", realTimeDelivery.getSource()));
		nameValuePairs.add(new BasicNameValuePair("campaign_code", realTimeDelivery.getCampaignCode()));
		nameValuePairs.add(new BasicNameValuePair("asset_name", realTimeDelivery.getAssetName()));
		nameValuePairs.add(new BasicNameValuePair("category_name", realTimeDelivery.getCategoryName()));

		nameValuePairs.add(new BasicNameValuePair("success_email", realTimeDelivery.getSuccessMailId()));
		nameValuePairs.add(new BasicNameValuePair("error_email", realTimeDelivery.getErrorMailId()));
		nameValuePairs
				.add(new BasicNameValuePair("lead_id", prospectCallLogFromDB.getProspectCall().getProspectCallId()));

		if (realTimeDelivery.getTestMode() != null && !realTimeDelivery.getTestMode().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("is_test_mode", realTimeDelivery.getTestMode()));
		}

		String first_name = prospectCallLogFromDB.getProspectCall().getProspect().getFirstName();
		if (first_name != null && !first_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("first_name", first_name));
		}

		String last_name = prospectCallLogFromDB.getProspectCall().getProspect().getLastName();
		if (last_name != null && !last_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("last_name", last_name));
		}

		String phone = prospectCallLogFromDB.getProspectCall().getProspect().getPhone();
		if (phone != null && !phone.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("phone", phone));
		}

		String email = prospectCallLogFromDB.getProspectCall().getProspect().getEmail();
		if (email != null && !email.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("email", email));
		}

		String company_name = prospectCallLogFromDB.getProspectCall().getProspect().getCompany();
		if (company_name != null && !company_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("company_name", company_name));
		}

		String job_title = prospectCallLogFromDB.getProspectCall().getProspect().getTitle();
		if (job_title != null && !job_title.equals("")) {
			int maxJobTitleLength = (job_title.length() < 100) ? job_title.length() : 100;
			job_title = job_title.substring(0, maxJobTitleLength);
			nameValuePairs.add(new BasicNameValuePair("job_title", job_title));
		}

		String industry = mapXtaasRealTimeDeliveryIndustry(
				prospectCallLogFromDB.getProspectCall().getProspect().getIndustry());
		nameValuePairs.add(new BasicNameValuePair("industry", industry));

		String company_size = findCompanySize(prospectCallLogFromDB.getProspectCall().getProspect());
		nameValuePairs.add(new BasicNameValuePair("company_size", company_size));

		String addressLine1 = prospectCallLogFromDB.getProspectCall().getProspect().getAddressLine1();
		String addressLine2 = prospectCallLogFromDB.getProspectCall().getProspect().getAddressLine2();
		String address1 = "";
		if (addressLine1 != null && !addressLine1.equals("")) {
			address1 = address1 + addressLine1;
		} else if (addressLine2 != null && !addressLine2.equals("")) {
			address1 = address1 + addressLine2;
		}
		if (!address1.equals("")) {
			int maxAddressLength = (address1.length() < 80) ? address1.length() : 80;
			address1 = address1.substring(0, maxAddressLength);
			nameValuePairs.add(new BasicNameValuePair("address1", address1));
		}

		String city = prospectCallLogFromDB.getProspectCall().getProspect().getCity();
		if (city != null && !city.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("city", city));
		}

		String state = prospectCallLogFromDB.getProspectCall().getProspect().getStateCode();
		if (state != null && !state.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("state", state));
		}
		
		String postal_code = prospectCallLogFromDB.getProspectCall().getProspect().getZipCode();
		if (postal_code != null && !postal_code.equals("")) {
			if (postal_code.length() > 5) {
				postal_code = postal_code.substring(0, 5);
				nameValuePairs.add(new BasicNameValuePair("postal_code", postal_code));
			} else if (postal_code.length() == 5) {
				nameValuePairs.add(new BasicNameValuePair("postal_code", postal_code));
			}
		}

		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(postUrl);
			System.out.println("Real time post lead URL is: [{}] and Form Data Enitty is: {}"+ postUrl+
					nameValuePairs.toString());
			//TODO here excel sheet should be prepared
			httpClient.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return nameValuePairs;
	}

	private String findCompanySize(Prospect prospect) {
		String companySize = "Unknown";
		Object minEmployeeCount = prospect.getCustomAttributeValue("minEmployeeCount");
		int minEmployeeCountInt = (Integer) minEmployeeCount;
		Object maxEmployeeCount = prospect.getCustomAttributeValue("maxEmployeeCount");
		int maxEmployeeCountInt = (Integer) maxEmployeeCount;

		/*
		 * if (minEmployeeCountInt != 0) { companySize =
		 * compareMinEmployeeCount(minEmployeeCountInt); } else
		 */if (maxEmployeeCountInt != 0) {
			companySize = compareMaxEmployeeCount(maxEmployeeCountInt);
		}
		return companySize;
	}

	private String compareMaxEmployeeCount(int maxEmployeeCountInt) {
		String companySize = "Unknown";
		if (maxEmployeeCountInt <= 3) {
			companySize = "1 to 3";
		} else if (maxEmployeeCountInt <= 10) {
			companySize = "4 to 10";
		} else if (maxEmployeeCountInt <= 25) {
			companySize = "11 to 25";
		} else if (maxEmployeeCountInt <= 50) {
			companySize = "26 to 50";
		} else if (maxEmployeeCountInt <= 100) {
			companySize = "51 to 100";
		} else if (maxEmployeeCountInt <= 200) {
			companySize = "101 to 200";
		} else if (maxEmployeeCountInt <= 500) {
			companySize = "201 to 500";
		} else if (maxEmployeeCountInt <= 1000) {
			companySize = "501 to 1000";
		} else {
			companySize = "1001 or more";
		}
		return companySize;
	}

	private String compareMinEmployeeCount(int minEmployeeCountInt) {
		String companySize = "Unknown";
		if (minEmployeeCountInt < 4) {
			companySize = "1 to 3";
		} else if (minEmployeeCountInt < 11) {
			companySize = "4 to 10";
		} else if (minEmployeeCountInt < 26) {
			companySize = "11 to 25";
		} else if (minEmployeeCountInt < 51) {
			companySize = "26 to 50";
		} else if (minEmployeeCountInt < 101) {
			companySize = "51 to 100";
		} else if (minEmployeeCountInt < 201) {
			companySize = "101 to 200";
		} else if (minEmployeeCountInt < 501) {
			companySize = "201 to 500";
		} else if (minEmployeeCountInt < 1001) {
			companySize = "501 to 1000";
		} else {
			companySize = "1001 or more";
		}
		return companySize;
	}

	private String mapXtaasRealTimeDeliveryIndustry(String xtaasIndustry) {
		String realTimeDeliveryIndustry = "Unknown";
		RealTimeDeliveryIndustryMapping realTimeDeliveryIndustryMapping = realTimeDeliveryIndustryMappingService
				.getRealTimeDeliveryIndustryMapping(xtaasIndustry);
		if (realTimeDeliveryIndustryMapping != null) {
			realTimeDeliveryIndustry = realTimeDeliveryIndustryMapping.getRealTimeDeliveryIndustry();
		}
		return realTimeDeliveryIndustry;
	}

	@SuppressWarnings("deprecation")
	private List<NameValuePair> prepareRequestBodyInsideUp(ProspectCallLog prospectCallLog, RealTimeDelivery realTimeDelivery)
			throws Exception {
		String prospectCampaignID = prospectCallLog.getProspectCall().getCampaignId();
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		Prospect prospect = prospectCallLog.getProspectCall().getProspect();

		String postUrl = realTimeDelivery.getPostUrl();

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		
		nameValuePairs.add(new BasicNameValuePair("catId", realTimeDelivery.getCategoryId()));
		nameValuePairs.add(new BasicNameValuePair("iusrc", realTimeDelivery.getIusrc()));

		String first_name = prospectCallLog.getProspectCall().getProspect().getFirstName();
		if (first_name != null && !first_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("firstName", first_name));
		}

		String last_name = prospectCallLog.getProspectCall().getProspect().getLastName();
		if (last_name != null && !last_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("lastName", last_name));
		}

		String phone = prospectCallLog.getProspectCall().getProspect().getPhone();
		if (phone != null && !phone.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("phoneNumber", phone));
		}

		String addressLine1 = prospectCallLog.getProspectCall().getProspect().getAddressLine1();
		String addressLine2 = prospectCallLog.getProspectCall().getProspect().getAddressLine2();
		String address = "";
		if (addressLine1 != null && !addressLine1.equals("")) {
			address = address + addressLine1 + " ";
		}
		if (addressLine2 != null && !addressLine2.equals("")) {
			address = address + addressLine2 + " ";
		}
		String city = prospectCallLog.getProspectCall().getProspect().getCity();
		if (city != null && !city.isEmpty()) {
			address = address + city + " ";
		}
		String state = prospectCallLog.getProspectCall().getProspect().getStateCode();
		if (state != null && !state.isEmpty()) {
			address = address + state;
		}
		
		nameValuePairs.add(new BasicNameValuePair("address", address));

		String email = prospectCallLog.getProspectCall().getProspect().getEmail();
		if (email != null && !email.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("email", email));
		}

		String postal_code = prospectCallLog.getProspectCall().getProspect().getZipCode();
		if (postal_code != null && !postal_code.equals("")) {
			if (postal_code.length() > 5) {
				postal_code = postal_code.substring(0, 5);
				nameValuePairs.add(new BasicNameValuePair("zip", postal_code));
			} else if (postal_code.length() == 5) {
				nameValuePairs.add(new BasicNameValuePair("zip", postal_code));
			}
		}

		String company_name = prospectCallLog.getProspectCall().getProspect().getCompany();
		if (company_name != null && !company_name.equals("")) {
			nameValuePairs.add(new BasicNameValuePair("company", company_name));
		}
		
		String title = prospectCallLog.getProspectCall().getProspect().getTitle();
		ClientMapping clientTitleMapping = clientMappingRepository
				.findByClientNameAndByKey(XtaasConstants.INSIDEUP_CLIENT, title);
		if (clientTitleMapping != null) {
			nameValuePairs.add(new BasicNameValuePair("title", clientTitleMapping.getKey()));
		} else {
			ClientMapping clientDefaultTitleMapping = clientMappingRepository
					.findByCampaignIdAndByKey(prospectCampaignID, "defaultTitle");
			if (clientDefaultTitleMapping != null) {
				nameValuePairs.add(new BasicNameValuePair("title", clientDefaultTitleMapping.getKey()));
			}
		}
		
		// TODO Sending recording file name which is sending manually
		//Campaign campaign = campaignService.getCampaign(campaignId);
		String fileName = prospectCallLog.getProspectCall().getProspect().getFirstName() + "_" +
				campaignName + ".wav";
		
		ClientMapping clientDefaultRecordingMapping = clientMappingRepository
				.findByCampaignIdAndByKey(prospectCampaignID, "recordingName");
		if (clientDefaultRecordingMapping != null ) {
			nameValuePairs.add(new BasicNameValuePair("recording_name", fileName));
		}
		
		String industry = prospectCallLog.getProspectCall().getProspect().getIndustry();
		ClientMapping clientIndustryMapping = clientMappingRepository
				.findByClientNameAndByKey(XtaasConstants.INSIDEUP_CLIENT, industry.trim());
		List<String> industryList = getIUIndustryMapping();
		String mappedIndus = "";
		for(String ind : industryList){
			if(ind.contains(industry.trim())){
				mappedIndus = ind;
			}
		}
		if (mappedIndus != null && !mappedIndus.isEmpty()) {
			nameValuePairs.add(new BasicNameValuePair("industry", mappedIndus));
		} else {
			ClientMapping clientDefaultIndustryMapping = clientMappingRepository
					.findByCampaignIdAndByKey(prospectCampaignID, "defaultIndustry");
			if (clientDefaultIndustryMapping != null) {
				nameValuePairs.add(new BasicNameValuePair("industry", clientDefaultIndustryMapping.getKey()));
			}
		}
		
		Object maxEmployeeCount = prospect.getCustomAttributeValue("maxEmployeeCount");
		int maxEmployeeCountInt = (Integer) maxEmployeeCount;
		String employeeSize = InsideUpEmployeeMapping(maxEmployeeCountInt);
		nameValuePairs.add(new BasicNameValuePair("employee", employeeSize));

		try {
			LeadformAttributes leadformAttributes = getInsideUpCategoryAttributes(realTimeDelivery.getCategoryId());
			System.out.println("LeadformAttributes : " + leadformAttributes.toString());

			if (leadformAttributes.getAttributes() != null && leadformAttributes.getAttributes().size() > 0) {
				for (int i = 0; i < leadformAttributes.getAttributes().size(); i++) {
					Attribute attribute = leadformAttributes.getAttributes().get(i);
					ClientMapping clientQuestionMapping = clientMappingRepository
							.findByCampaignIdAndByValue(prospectCampaignID, attribute.getAttributeName().trim());
					String questionSequenceNumber = "Q";
					int queNumber = i + 1;
					questionSequenceNumber = questionSequenceNumber + queNumber;
					String answerId = "";
					if (clientQuestionMapping != null && prospectCall.getAnswers() != null
							&& prospectCall.getAnswers().size() > 0) {
						for (AgentQaAnswer agentQaAnswer : prospectCall.getAnswers()) {
							if (agentQaAnswer.getQuestion().equalsIgnoreCase(clientQuestionMapping.getKey())) {
								ClientMapping clientAnswerMapping = clientMappingRepository
										.findByCampaignIdAndByKey(prospectCampaignID, agentQaAnswer.getAnswer().trim());
								if (clientAnswerMapping != null) {
									answerId = clientAnswerMapping.getValue();
									nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber, agentQaAnswer.getAnswer().trim()));
								}
							}
						}
					} else {
						ClientMapping clientQuestionDefaultMapping = clientMappingRepository
								.findByCampaignIdAndByKey(prospectCampaignID, attribute.getAttributeName().trim());
						if (clientQuestionDefaultMapping != null) {
							nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber,
									clientQuestionDefaultMapping.getValue()));
						} else {
							nameValuePairs.add(new BasicNameValuePair(questionSequenceNumber, "2040"));
						}
					}
				}
			}

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(postUrl);
			System.out.println("Real time post lead URL - [{}] with reuquest data - [{}]"+postUrl+nameValuePairs.toString());
			// TODO here excel sheet
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nameValuePairs;
	}

	private String InsideUpEmployeeMapping(int maxEmployeeCount) {
		String employeeCountCode = "1-9";
		if (maxEmployeeCount <= 9) {
			employeeCountCode = "1-9";
		} else if (maxEmployeeCount <= 19) {
			employeeCountCode = "10-19";
		} else if (maxEmployeeCount <= 49) {
			employeeCountCode = "20-49";
		} else if (maxEmployeeCount <= 99) {
			employeeCountCode = "50-99";
		} else if (maxEmployeeCount <= 499) {
			employeeCountCode = "100-499";
		} else if (maxEmployeeCount <= 999) {
			employeeCountCode = "500-999";
		} else {
			employeeCountCode = "1000+";
		}
		return employeeCountCode;
	}

	private LeadformAttributes getInsideUpCategoryAttributes(String categoryId) {
		String xmlResponseString = "";
		LeadformAttributes leadformAttributes = new LeadformAttributes();
		String getCategoryAttributesUrl = "http://www.insideup.com/categoryattributes.html?catId=" + categoryId;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(getCategoryAttributesUrl);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			xmlResponseString = EntityUtils.toString(entity, "UTF-8");
			leadformAttributes = XMLConverterUtil.convertXMLToLeadformAttributes(xmlResponseString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		httpClient.close();
		return leadformAttributes;
	}
	
	private List<String> getIUIndustryMapping(){
		List<String> iuIndustryList = new ArrayList<String>();
		iuIndustryList.add("Advertising/Marketing/PR");
		iuIndustryList.add("Agriculture");
		iuIndustryList.add("Biotech/Pharmaceuticals");
		iuIndustryList.add("Computers - Hardware");
		iuIndustryList.add("Computers - Software");
		iuIndustryList.add("Construction/General Contracting");
		iuIndustryList.add("Consulting");
		iuIndustryList.add("Education");
		iuIndustryList.add("Equipment Sales & Service");
		iuIndustryList.add("Financial Services");
		iuIndustryList.add("Government");
		iuIndustryList.add("Healthcare");
		iuIndustryList.add("Information Services");
		iuIndustryList.add("Insurance");
		iuIndustryList.add("Legal");
		iuIndustryList.add("Manufacturing Service");
		iuIndustryList.add("Media/Entertainment/Publishing");
		iuIndustryList.add("Membership Organization");
		iuIndustryList.add("Non-Profit");
		iuIndustryList.add("Other Services");
		iuIndustryList.add("Real Estate");
		iuIndustryList.add("Restaurant");
		iuIndustryList.add("Retail");
		iuIndustryList.add("Telecom/Utilities");
		iuIndustryList.add("Transportation/Logistics");
		iuIndustryList.add("Travel/Hospitality");
		iuIndustryList.add("Wholesale");
		
		return iuIndustryList;

	}


}
