package com.xtaas.test;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.model.Bucket;
import com.xtaas.aws.AwsS3Service;

@Component
public class AwsS3ServiceTest {

	public static final Logger logger = LoggerFactory.getLogger(AwsS3ServiceTest.class);

	@Autowired
	private AwsS3Service awsS3Service;

	/**
	 * @param args
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		AwsS3ServiceTest awsS3ServiceTest = context.getBean(AwsS3ServiceTest.class);
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		SecurityContextHolder.getContext()
				.setAuthentication(new UsernamePasswordAuthenticationToken("djain", "secret", authorities));

		awsS3ServiceTest.getBuckets();
	}

	private void getBuckets() {
		List<Bucket> buckets = awsS3Service.GetBuckets();
		if (buckets != null && buckets.size() > 0) {
			for (Bucket bucket : buckets) {
				logger.info("Bucket Name: " + bucket.getName());
			}
		}
	}

}
