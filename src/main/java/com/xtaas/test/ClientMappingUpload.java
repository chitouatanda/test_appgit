package com.xtaas.test;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.repository.ClientMappingRepository;

@Component
public class ClientMappingUpload {

	@Autowired
	ClientMappingRepository clientMappingRepository;
	
	 
    /**
     * @param args
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonGenerationException 
     * @throws ParseException 
     */
    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException, ParseException {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        ClientMappingUpload clientMappingUpload = context.getBean(ClientMappingUpload.class);
         List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
         authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
         SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("djain", "secret", authorities));
         clientMappingUpload.upload();
    }

    
    private void upload() {
    	
		//////////////////////////////  Excel File Reading //////////////////////////////
		
		try{
			// Creating a Workbook from an Excel file (.xls or .xlsx)
			Workbook workbook = WorkbookFactory.create(new File("C:\\ex\\clientmapping\\gina\\voipnew.xlsx"));
	
			// Retrieving the number of sheets in the Workbook
			//System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
			
			 // Getting the Sheet at index zero
	        Sheet sheet = workbook.getSheetAt(0);
	        
	        // Create a DataFormatter to format and get each cell's value as String
	        DataFormatter dataFormatter = new DataFormatter();
	        int rowCounter=0;
	       // System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
	        for (Row row: sheet) {
	        	if(rowCounter==0){
	        		rowCounter++;
	        		continue;
	        	}
	        	ClientMapping clientMapping = new ClientMapping();
	        	
	        	 Cell c = row.getCell(0);
	        	 if (c == null || c.getCellType() == CellType.BLANK) {
		        	// System.out.println(rowCounter+"-->ClientName Can't Blank");
		        	 break;
		         }else{
		        	 String clientname = dataFormatter.formatCellValue(c).trim();
		        	 clientMapping.setClientName(clientname.trim());
		         }
	        	 Cell c2 = row.getCell(1);
	        	 if (c2 == null || c2.getCellType() == CellType.BLANK) {
	        		 clientMapping.setCampaignId("");
	        	 }else{
	        		 String cid = dataFormatter.formatCellValue(c2).trim();
	        		 clientMapping.setCampaignId(cid.trim());
	        	 }
	        	 Cell c3 = row.getCell(2);
	        	 if (c3 == null || c3.getCellType() == CellType.BLANK) {
	        		 //System.out.println(rowCounter+"-->Excel column sequence Can't Blank");
	        	 }else{
	        		 String seq = dataFormatter.formatCellValue(c3).trim();
	        		 clientMapping.setSequence(Integer.parseInt(seq.trim()));
	        	 }
	        	 Cell c4 = row.getCell(3);
	        	 if (c4 == null || c4.getCellType() == CellType.BLANK) {
	        		 //System.out.println(rowCounter+"-->Excel Header Name Can't Blank");
	        	 }else{
	        		 String colhead = dataFormatter.formatCellValue(c4).trim();
	        		 clientMapping.setColHeader(colhead.trim());
	        	 }
	        	 
	        	 
	        	 ///////////////////////////////////////
	        	 Cell c5 = row.getCell(4);
	        	 if (c5 == null || c5.getCellType() == CellType.BLANK) {
	        		 //System.out.println(rowCounter+"-->StdAttribute Can't Blank");
	        	 }else{
	        		 String stdattrib = dataFormatter.formatCellValue(c5).trim();
	        		 clientMapping.setStdAttribute(stdattrib.trim());
	        	 }
	        	 Cell c6 = row.getCell(5);
	        	 if (c6 == null || c6.getCellType() == CellType.BLANK) {
	        		 //System.out.println(rowCounter+"-->Key Can't Blank");
		         }else{
		        	 String xkey = dataFormatter.formatCellValue(c6).trim();
		        	 
		        	 String tempStdAttrib = dataFormatter.formatCellValue(c5).trim();
	        		 if(tempStdAttrib.equalsIgnoreCase("prospect.revenueRange")){
	        			 Map<String,String[]> revMap = getRevenueMap();
	        			 for (Map.Entry<String, String[]> entry : revMap.entrySet()) {
	        				 if(entry.getKey().equalsIgnoreCase(xkey)){
	        					 String[] keyArrrev = entry.getValue();
	        					 clientMapping.setKey(keyArrrev[1]);
	        				 }
	        			 }
	        			 
	        		 }else if(tempStdAttrib.equalsIgnoreCase("prospect.employeeRange")){
	        			 Map<String,String[]> empMap = getEmployeeMap();
	        			 for (Map.Entry<String, String[]> entry : empMap.entrySet()) {
	        				 if(entry.getKey().equalsIgnoreCase(xkey)){
	        					 String[] keyArremp = entry.getValue();
	        					 clientMapping.setKey(keyArremp[1]);
	        				 }
	        			 }
	        		 }else{
	        			 clientMapping.setKey(xkey.trim());
	        		 }
		         }
	        	 Cell c7 = row.getCell(6);
	        	 if (c7== null || c7.getCellType() == CellType.BLANK) {
	        		 clientMapping.setTitleMgmtLevel("");
		         }else {
		        	 String val = dataFormatter.formatCellValue(c7).trim();
		        	 //val = val.replaceFirst("\\s", "");
		        	 clientMapping.setTitleMgmtLevel(val.trim());
		         }
	        	 
	        	 Cell c8 = row.getCell(7);
	        	 if (c8== null || c8.getCellType() == CellType.BLANK) {
	        		 clientMapping.setValue("");
		         }else{
		        	 String val = dataFormatter.formatCellValue(c8).trim();
		        	 //val = val.replaceFirst("\\s", "");
		        	 clientMapping.setValue(val.trim());
		         }
	        	 Cell c9 = row.getCell(8);
	        	 if (c9 == null || c9.getCellType() == CellType.BLANK) {
	        		 clientMapping.setDefaultValue("");
	        	 }else{
	        		 String defval = dataFormatter.formatCellValue(c9).trim();
	        		 clientMapping.setDefaultValue(defval.trim());
	        	 }
	        	
	        	 clientMappingRepository.save(clientMapping);
	        	 
	        	
	            //System.out.println();
	        }
	
	        // Closing the workbook
	        workbook.close();    
		        
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	
		////////////////////////////// End of Excel File Reading ///////////////////////
    }
    
    private Map<String, String[]> getRevenueMap(){
		Map<String,String[]> revMap = new HashMap<String, String[]>();
		/////
		
		
		
		String[] revArray21 = new String[2];
		 revArray21[0]="0";
		 revArray21[1]="5000000";
		 revMap.put("Under $500K", revArray21);
		 
		 String[] revArray22 = new String[2];
		 revArray22[0]="0";
		 revArray22[1]="10000000";
		 revMap.put("$500K-$1M", revArray22);
		 
		 String[] revArray23 = new String[2];
		 revArray23[0]="0";
		 revArray23[1]="10000000";
		 revMap.put("$1M-$5M", revArray23);
		 
		
		 String[] revArray2 = new String[2];
		 revArray2[0]="5000000";
		 revArray2[1]="10000000";
		 revMap.put("$5M-$10M", revArray2);
		 
		 String[] revArray3 = new String[2];
		 revArray3[0]="10000000";
		 revArray3[1]="25000000";
		 revMap.put("$10M-$25M", revArray3);
		 
		 String[] revArray4 = new String[2];
		 revArray4[0]="25000000";
		 revArray4[1]="50000000";
		 revMap.put("$25M-$50M", revArray4);
		 
		 String[] revArray5 = new String[2];
		 revArray5[0]="50000000";
		 revArray5[1]="100000000";
		 revMap.put("$50M-$100M", revArray5);
		 
		 String[] revArray6 = new String[2];
		 revArray6[0]="100000000";
		 revArray6[1]="250000000";
		 revMap.put("$100M-$250M", revArray6);
		 
		 String[] revArray7 = new String[2];
		 revArray7[0]="250000000";
		 revArray7[1]="500000000";
		 revMap.put("$250M-$500M", revArray7);
		 
		 String[] revArray8 = new String[2];
		 revArray8[0]="500000000";
		 revArray8[1]="1000000000";
		 revMap.put("$500M-$1B", revArray8);
		 
		 String[] revArray9 = new String[2];
		 revArray9[0]="1000000000";
		 revArray9[1]="5000000000";
		 revMap.put("$1B-$5B", revArray9);
		 
		 String[] revArray10 = new String[2];
		 revArray10[0]="5000000000";
		 revArray10[1]="0";
		 revMap.put("Over $5B", revArray10);
		
		
		return revMap;
	}
	
	private Map<String, String[]> getEmployeeMap(){
		Map<String,String[]> empMap = new HashMap<String, String[]>();
		/////
		 String[] empArray1 = new String[2];
		 empArray1[0]="0";
		 empArray1[1]="5";
		 empMap.put("1-5", empArray1);
		
		String[] empArray2 = new String[2];
		empArray2[0]="5";
		empArray2[1]="10";
		empMap.put("5-10", empArray2);
		
		String[] empArray3 = new String[2];
		empArray3[0]="10";
		empArray3[1]="20";
		empMap.put("10-20", empArray3);
		
		String[] empArray4 = new String[2];
		empArray4[0]="20";
		empArray4[1]="50";
		empMap.put("20-50", empArray4);
		
		String[] empArray5 = new String[2];
		empArray5[0]="50";
		empArray5[1]="100";
		empMap.put("50-100", empArray5);
		
		String[] empArray6 = new String[2];
		empArray6[0]="100";
		empArray6[1]="250";
		empMap.put("100-250", empArray6);
		
		String[] empArray7 = new String[2];
		empArray7[0]="250";
		empArray7[1]="500";
		empMap.put("250-500", empArray7);
		
		String[] empArray8 = new String[2];
		empArray8[0]="500";
		empArray8[1]="1000";
		empMap.put("500-1,000", empArray8);
		
		String[] empArray9 = new String[2];
		empArray9[0]="1000";
		empArray9[1]="5000";
		empMap.put("1,000-5,000", empArray9);
		
		String[] empArray10 = new String[2];
		empArray10[0]="5000";
		empArray10[1]="10000";
		empMap.put("5,000-10,000", empArray10);
		
		String[] empArray11 = new String[2];
		empArray11[0]="10000";
		empArray11[1]="0";
		empMap.put("Over 10,000", empArray11);
		
		
		
		return empMap;
	}

}
