package com.xtaas.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.xtaas.db.repository.CampaignContactRepositoryImpl;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.web.dto.AgentDTO;

@Component
public class UpdateTitle {
	
	@Autowired
	private ProspectCallLogRepositoryImpl prospectCallLogRepositoryImpl;
	
	@Autowired
	private CampaignContactRepositoryImpl campaignContactRepositoryImpl;
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		UpdateTitle updateTitle = context.getBean(UpdateTitle.class);

		// Login first
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SUPERVISOR"));
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("jsmith", "secret", authorities));
		
		updateTitle.updateRecordsTitle();
	}

	private void updateRecordsTitle() {
		try
        {
            FileInputStream file = new FileInputStream(new File("Purchased.xlsx"));
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
            	String sourceId = "";
            	String title = "";
            	String firstName = "";
            	String lastName = "";
            	String email = "";
            	
                Row row = rowIterator.next();
                if (row.getRowNum() == 0) {
                	continue;
                } 
                
              //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                 
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    if (cell.getColumnIndex() == 0) {
                    	if(cell.getCellType() == CellType.STRING) {
                    		 sourceId = cell.getStringCellValue();
                    	}
//                    	switch (cell.getCellType()) {
//                           case Cell.CELL_TYPE_STRING:
//                                sourceId = cell.getStringCellValue();
//                                break;
//                        }
                    } 
                    if (cell.getColumnIndex() == 9) {
                    	if(cell.getCellType() == CellType.STRING) {
                    		title = cell.getStringCellValue();
                   	}
//                    	switch (cell.getCellType()) {
//                            case Cell.CELL_TYPE_STRING:
//                                title = cell.getStringCellValue();
//                                break;
//                        }
                    } 
                    if (cell.getColumnIndex() == 1) {
                    	if(cell.getCellType() == CellType.STRING) {
                    		firstName = cell.getStringCellValue();
                   	}
//                    	switch (cell.getCellType()) {
//                            case Cell.CELL_TYPE_STRING:
//                                firstName = cell.getStringCellValue();
//                                break;
//                        }
                    } 
                    if (cell.getColumnIndex() == 2) {
                    	if(cell.getCellType() == CellType.STRING) {
                    		lastName = cell.getStringCellValue();
                   	}
//                    	switch (cell.getCellType()) {
//                            case Cell.CELL_TYPE_STRING:
//                                lastName = cell.getStringCellValue();
//                                break;
//                        }
                    } 
                    if (cell.getColumnIndex() == 5) {
                    	if(cell.getCellType() == CellType.STRING) {
                    		email = cell.getStringCellValue();
                   	}
//                    	switch (cell.getCellType()) {
//                            case Cell.CELL_TYPE_STRING:
//                                email = cell.getStringCellValue();
//                                break;
//                        }
                    } 
                }
                System.out.println("updating....."+"sourceId="+sourceId+" firstName="+firstName+" lastName="+lastName+" email="+email+" Title"+title);
                prospectCallLogRepositoryImpl.updateProspectTitle(sourceId, title);
                campaignContactRepositoryImpl.updateCampaignContactTitle(email, title);
                System.out.println("Done");
                System.out.println("\n");
               
            }
            System.out.println("Finally Done");
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
