package com.xtaas.test;

import java.util.Arrays;
import java.util.List;

/**
 * @author Haji Baba
 */
public class TFIDFCalculator {
    /**
     * @param doc  list of strings
     * @param term String represents a term
     * @return term frequency of term in document
     */
    public double tf(List<String> doc, String term) {
        double result = 0;
        for (String word : doc) {
            if (term.equalsIgnoreCase(word))
                result++;
        }
        return result / doc.size();
    }

    /**
     * @param docs list of list of strings represents the dataset
     * @param term String represents a term
     * @return the inverse term frequency of term in documents
     */
    public double idf(List<List<String>> docs, String term) {
        double n = 0;
        for (List<String> doc : docs) {
            for (String word : doc) {
                if (term.equalsIgnoreCase(word)) {
                    n++;
                    break;
                }
            }
        }
        return Math.log(docs.size() / n);
    }

    /**
     * @param doc  a text document
     * @param docs all documents
     * @param term term
     * @return the TF-IDF of term
     */
    public double tfIdf(List<String> doc, List<List<String>> docs, String term) {
        return tf(doc, term) * idf(docs, term);

    }

    public static void main(String[] args) {

        List<String> doc1 = Arrays.asList("Lorem", "ipsum", "google", "ipsum", "india", "ipsum");
        List<String> doc2 = Arrays.asList("Vituperata", "incorrupte", "at", "ipsum", "pro", "quo");
        List<String> doc3 = Arrays.asList("Has", "persius", "disputationi", "google", "usa");
        List<String> doc4 = Arrays.asList("google", "india", "disputationi", "google", "usa");
        List<String> doc5 = Arrays.asList("google", "india", "disputationi", "google", "usa");

        String s = "google india disputationi google usa";
        
        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        TFIDFCalculator calculator = new TFIDFCalculator();
        double tfidf = calculator.tfIdf(doc1, documents, "ipsum");
        double tfidf1 = calculator.tfIdf(doc2, documents, "ipsum");
        double tfidf2 = calculator.tfIdf(doc3, documents, "google");
        System.out.println("TF-IDF (ipsum) = " + tfidf);
        System.out.println("TF-IDF (ipsum) = " + tfidf1);
        System.out.println("TF-IDF (ipsum) = " + tfidf2);
        double tfidf4 = calculator.tfIdf(doc4, documents, "google");
        double tfidf5 = calculator.tfIdf(doc5, documents, "india");
        System.out.println("TF-IDF (ipsum) = " + tfidf4);
        System.out.println("TF-IDF (ipsum) = " + tfidf5);




    }


}