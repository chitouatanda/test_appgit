package com.xtaas.logging;

import com.splunk.logging.SplunkCimLogEvent;

public class SplunkLoggingUtils {

	private SplunkCimLogEvent event;

	public SplunkLoggingUtils(String eventName, String eventID) {
		event = new SplunkCimLogEvent(eventName, eventID);
	}

	public String build() {
		return event.toString();
	}

	public SplunkLoggingUtils addField(String key, Object value) {
		event.addField(key, value);
		return this;
	}

	public SplunkLoggingUtils addThrowableWithStacktrace(Throwable throwable) {
		event.addThrowableWithStacktrace(throwable);
		return this;
	}

	public SplunkLoggingUtils addThrowableWithStacktrace(Throwable throwable, int stacktraceDepth) {
		event.addThrowableWithStacktrace(throwable, stacktraceDepth);
		return this;
	}

	public SplunkLoggingUtils eventDescription(String eventDescription) {
		event.addField(SplunkEvent.EVENT_DESCRIPTION.getValue(), eventDescription);
		return this;
	}

	public SplunkLoggingUtils processName(String processName) {
		event.addField(SplunkEvent.PROCESS_NAME.getValue(), processName);
		return this;
	}

	public SplunkLoggingUtils methodName(String methodName) {
		event.addField(SplunkEvent.METHOD_NAME.getValue(), methodName);
		return this;
	}

	public SplunkLoggingUtils campaignId(String campaignId) {
		event.addField(SplunkEvent.CAMPAIGN_ID.getValue(), campaignId);
		return this;
	}

	public SplunkLoggingUtils campaignName(String cmpName) {
		event.addField(SplunkEvent.CAMPAIGN_NAME.getValue(), cmpName);
		return this;
	}

	public SplunkLoggingUtils agentId(String agentId) {
		event.addField(SplunkEvent.AGENT_ID.getValue(), agentId);
		return this;
	}

	public SplunkLoggingUtils agentName(String agentName) {
		event.addField(SplunkEvent.AGENT_NAME.getValue(), agentName);
		return this;
	}

	public SplunkLoggingUtils agentType(String dialerMode) {
		event.addField(SplunkEvent.AGENT_TYPE.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils organizationId(String orgId) {
		event.addField(SplunkEvent.ORGANIZATION_ID.getValue(), orgId);
		return this;
	}

	public SplunkLoggingUtils prospectCallId(String pcid) {
		event.addField(SplunkEvent.PROSPECT_CALL_ID.getValue(), pcid);
		return this;
	}

	public SplunkLoggingUtils prospectInteractionSessionId(String prospectInteractionSessionId) {
		event.addField(SplunkEvent.PROSPECT_INTERACTION_SESSION_ID.getValue(), prospectInteractionSessionId);
		return this;
	}

	public SplunkLoggingUtils callSid(String callSid) {
		event.addField(SplunkEvent.CALL_SID.getValue(), callSid);
		return this;
	}

	public SplunkLoggingUtils callState(String callState) {
		event.addField(SplunkEvent.CALL_STATE.getValue(), callState);
		return this;
	}

	public SplunkLoggingUtils callDurationTime(String callDurationTime) {
		event.addField(SplunkEvent.CALL_DURATION_TIME.getValue(), callDurationTime);
		return this;
	}

	public SplunkLoggingUtils callableDataTimezone(String callableDataTimezone) {
		event.addField(SplunkEvent.CALLABLE_DATA_TIMEZONE.getValue(), callableDataTimezone);
		return this;
	}

	public SplunkLoggingUtils dataSource(String dataSource) {
		event.addField(SplunkEvent.DATA_SOURCE.getValue(), dataSource);
		return this;
	}

	public SplunkLoggingUtils outboundNumber(String outboundNumber) {
		event.addField(SplunkEvent.OUTBOUND_NUMBER.getValue(), outboundNumber);
		return this;
	}

	public SplunkLoggingUtils toNumber(String toNumber) {
		event.addField(SplunkEvent.TO_NUMBER.getValue(), toNumber);
		return this;
	}

	public SplunkLoggingUtils voiceProvider(String voiceProvider) {
		event.addField(SplunkEvent.VOICE_PROVIDER.getValue(), voiceProvider);
		return this;
	}

	public SplunkLoggingUtils dataSlice(String dataSlice) {
		event.addField(SplunkEvent.DATA_SLICE.getValue(), dataSlice);
		return this;
	}

	public SplunkLoggingUtils callRetryCount(String callRetryCount) {
		event.addField(SplunkEvent.CALL_RETRY_COUNT.getValue(), callRetryCount);
		return this;
	}

	public SplunkLoggingUtils dailyCallRetryCount(String dailyCallRetryCount) {
		event.addField(SplunkEvent.DAILY_CALL_RETRY_COUNT.getValue(), dailyCallRetryCount);
		return this;
	}

	public SplunkLoggingUtils status(String status) {
		event.addField(SplunkEvent.STATUS.getValue(), status);
		return this;
	}

	public SplunkLoggingUtils dispositionStatus(String dispositionStatus) {
		event.addField(SplunkEvent.DISPOSITION_STATUS.getValue(), dispositionStatus);
		return this;
	}

	public SplunkLoggingUtils subStatus(String dispositionStatus) {
		event.addField(SplunkEvent.SUB_STATUS.getValue(), dispositionStatus);
		return this;
	}

	public SplunkLoggingUtils dialerMode(String dialerMode) {
		event.addField(SplunkEvent.DIALER_MODE.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils agentWaitTime(String dialerMode) {
		event.addField(SplunkEvent.AGENT_WAIT_TIME.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils telcoDuration(String dialerMode) {
		event.addField(SplunkEvent.TELCO_DURATION.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils dispositionTime(String dialerMode) {
		event.addField(SplunkEvent.DISPOSITION_TIME.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils prospectHandleTime(String dialerMode) {
		event.addField(SplunkEvent.PROSPECT_HANDLE_TIME.getValue(), dialerMode);
		return this;
	}

	public SplunkLoggingUtils timeTaken(String timeTaken) {
		event.addField(SplunkEvent.TIME_TAKEN.getValue(), timeTaken);
		return this;
	}

	public SplunkLoggingUtils dataRetrived(String dataRetrived) {
		event.addField(SplunkEvent.DATA_RETRIVED.getValue(), dataRetrived);
		return this;
	}

	public SplunkLoggingUtils sipProvider(String sipProvider) {
		event.addField(SplunkEvent.SIP_PROVIDER.getValue(), sipProvider);
		return this;
	}

}
