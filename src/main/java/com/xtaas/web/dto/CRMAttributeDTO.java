package com.xtaas.web.dto;

import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.PickListItem;

public class CRMAttributeDTO {
	@JsonProperty
	private String label;
	@JsonProperty
	private ArrayList<PickListItem> pickList;
	@JsonProperty
	private String questionSkin;
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the pickList
	 */
	public ArrayList<PickListItem> getPickList() {
		return pickList;
	}
	/**
	 * @param pickList the pickList to set
	 */
	public void setPickList(ArrayList<PickListItem> pickList) {
		this.pickList = pickList;
	}
	/**
	 * @return the questionSkin
	 */
	public String getQuestionSkin() {
		return questionSkin;
	}
	/**
	 * @param questionSkin the questionSkin to set
	 */
	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}
	
	public CRMAttribute toCRMAttribute() {
		CRMAttribute crmAttribute = new CRMAttribute();
		crmAttribute.setSystemName(convertLabelToAttributeName(getLabel()));
		crmAttribute.setLabel(getLabel());
		crmAttribute.setLength(512); //hardcoded. not used anywhere
		crmAttribute.setSystemDataType("string");
		crmAttribute.setPickList(getPickList());
		crmAttribute.setQuestionSkin(getQuestionSkin());
		crmAttribute.setAttributeType("CUSTOM");
		crmAttribute.setAgentValidationRequired(false);
		crmAttribute.setOperator("IN");
		crmAttribute.setOrganizationQuestion(false);
		crmAttribute.setDefaultQuestion(false);
		crmAttribute.setDefaultValues(new ArrayList<PickListItem>());
		return crmAttribute;
	}
	
	private String convertLabelToAttributeName(String label) {
		return StringUtils.upperCase(label).replace(" ", "_");
	}
}
