package com.xtaas.web.dto;

public class BillingGroupOutputDTO {

	private String connectionId;

	private String billingGroupId;

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getBillingGroupId() {
		return billingGroupId;
	}

	public void setBillingGroupId(String billingGroupId) {
		this.billingGroupId = billingGroupId;
	}

	@Override
	public String toString() {
		return "BillingGroupOutputDTO [connectionId=" + connectionId + ", billingGroupId=" + billingGroupId + "]";
	}

}