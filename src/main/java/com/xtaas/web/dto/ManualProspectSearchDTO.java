package com.xtaas.web.dto;

import com.xtaas.db.entity.Prospect;

import java.util.List;

public class ManualProspectSearchDTO {

	List<LeadIQProspectDTO> manualSearchProspect;

	List<LeadIQProspectDTO> similarSearchProspect;

	private boolean similarProspect;

	public List<LeadIQProspectDTO> getManualSearchProspect() {
		return manualSearchProspect;
	}

	public void setManualSearchProspect(List<LeadIQProspectDTO> manualSearchProspect) {
		this.manualSearchProspect = manualSearchProspect;
	}

	public List<LeadIQProspectDTO> getSimilarSearchProspect() {
		return similarSearchProspect;
	}

	public void setSimilarSearchProspect(List<LeadIQProspectDTO> similarSearchProspect) {
		this.similarSearchProspect = similarSearchProspect;
	}

	public boolean isSimilarProspect() {
		return similarProspect;
	}

	public void setSimilarProspect(boolean similarProspect) {
		this.similarProspect = similarProspect;
	}
}