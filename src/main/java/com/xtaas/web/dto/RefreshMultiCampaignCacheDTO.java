package com.xtaas.web.dto;

import java.util.List;

public class RefreshMultiCampaignCacheDTO {

	private List<String> campaignIds;

	public RefreshMultiCampaignCacheDTO() {
		// default constructor
	}

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	@Override
	public String toString() {
		return "RefreshMultiCampaignCacheDTO [campaignIds=" + campaignIds + "]";
	}

}
