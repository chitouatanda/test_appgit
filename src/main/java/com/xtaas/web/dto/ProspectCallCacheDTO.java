
package com.xtaas.web.dto;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.ProspectCallLog;

public class ProspectCallCacheDTO implements Comparable<ProspectCallCacheDTO> {
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private ProspectCacheDTO prospect;
	@JsonProperty
	private String twilioCallSid;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String outboundNumber;
	@JsonProperty
	private int callRetryCount;
	@JsonProperty
	private Long dailyCallRetryCount;
	@JsonProperty
	private String dataSlice;
	@JsonProperty
	private List<IndirectProspectsCacheDTO> indirectProspects;
	@JsonProperty
	private Date createdDate;
	@JsonProperty
	private Date updatedDate;
	@JsonProperty
	private int qualityBucketSort;

	public ProspectCallCacheDTO() {
	}

//	public ProspectCallCacheDTO(ProspectCall prospectCall) {
//		this.prospectCallId = prospectCall.getProspectCallId();
//		this.prospect = prospectCall.getProspectCacheDTO(prospectCall);
//		this.twilioCallSid = prospectCall.getTwilioCallSid();
//		this.campaignId = prospectCall.getCampaignId();
//		this.outboundNumber = prospectCall.getOutboundNumber();
//		this.callRetryCount = prospectCall.getCallRetryCount();
//		this.dailyCallRetryCount = prospectCall.getDailyCallRetryCount();
//		this.dataSlice = prospectCall.getDataSlice();
//		this.indirectProspects = prospectCall.getIndirectProspectCacheDTO(prospectCall.getIndirectProspects());
//		this.createdDate = prospectCall.getCreatedDate();
//		this.updatedDate = prospectCall.getUpdatedDate();
//	}

	public ProspectCallCacheDTO(ProspectCallLog prospectCallLog) {
		this.prospectCallId = prospectCallLog.getProspectCall().getProspectCallId();
		this.prospect = prospectCallLog.getProspectCall().getProspectCacheDTO(prospectCallLog.getProspectCall());
		this.twilioCallSid = prospectCallLog.getProspectCall().getTwilioCallSid();
		this.campaignId = prospectCallLog.getProspectCall().getCampaignId();
		this.outboundNumber = prospectCallLog.getProspectCall().getOutboundNumber();
		this.callRetryCount = prospectCallLog.getProspectCall().getCallRetryCount();
		this.dailyCallRetryCount = prospectCallLog.getProspectCall().getDailyCallRetryCount();
		this.dataSlice = prospectCallLog.getProspectCall().getDataSlice();
		this.indirectProspects = prospectCallLog.getProspectCall()
				.getIndirectProspectCacheDTO(prospectCallLog.getProspectCall().getIndirectProspects());
		this.createdDate = prospectCallLog.getCreatedDate();
		this.updatedDate = prospectCallLog.getUpdatedDate();
	}

	public ProspectCallCacheDTO(ProspectCallDTO prospectCallDTO) {
		this.prospectCallId = prospectCallDTO.getProspectCallId();
		this.prospect = prospectCallDTO.getProspectCacheDTO(prospectCallDTO);
		this.twilioCallSid = prospectCallDTO.getTwilioCallSid();
		this.campaignId = prospectCallDTO.getCampaignId();
		this.outboundNumber = prospectCallDTO.getOutboundNumber();
		this.callRetryCount = prospectCallDTO.getCallRetryCount();
		this.dailyCallRetryCount = prospectCallDTO.getDailyCallRetryCount();
		this.dataSlice = prospectCallDTO.getDataSlice();
		this.indirectProspects = prospectCallDTO.getIndirectProspectCacheDTO(prospectCallDTO.getIndirectProspects());
		this.createdDate = prospectCallDTO.getCreatedDate();
		this.updatedDate = prospectCallDTO.getUpdatedDate();
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public ProspectCacheDTO getProspect() {
		return prospect;
	}

	public void setProspect(ProspectCacheDTO prospect) {
		this.prospect = prospect;
	}

	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public List<IndirectProspectsCacheDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsCacheDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	@Override
	public String toString() {
		return "ProspectCallCacheDTO [prospectCallId=" + prospectCallId + ", prospect=" + prospect + ", twilioCallSid="
				+ twilioCallSid + ", campaignId=" + campaignId + ", outboundNumber=" + outboundNumber
				+ ", callRetryCount=" + callRetryCount + ", dailyCallRetryCount=" + dailyCallRetryCount + ", dataSlice="
				+ dataSlice + ", indirectProspects=" + indirectProspects + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + ", qualityBucketSort=" + qualityBucketSort + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + callRetryCount;
		result = prime * result + ((campaignId == null) ? 0 : campaignId.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((dailyCallRetryCount == null) ? 0 : dailyCallRetryCount.hashCode());
		result = prime * result + ((dataSlice == null) ? 0 : dataSlice.hashCode());
		result = prime * result + ((indirectProspects == null) ? 0 : indirectProspects.hashCode());
		result = prime * result + ((outboundNumber == null) ? 0 : outboundNumber.hashCode());
		result = prime * result + ((prospect == null) ? 0 : prospect.hashCode());
		result = prime * result + ((prospectCallId == null) ? 0 : prospectCallId.hashCode());
		result = prime * result + qualityBucketSort;
		result = prime * result + ((twilioCallSid == null) ? 0 : twilioCallSid.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProspectCallCacheDTO other = (ProspectCallCacheDTO) obj;
		if (callRetryCount != other.callRetryCount)
			return false;
		if (campaignId == null) {
			if (other.campaignId != null)
				return false;
		} else if (!campaignId.equals(other.campaignId))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (dailyCallRetryCount == null) {
			if (other.dailyCallRetryCount != null)
				return false;
		} else if (!dailyCallRetryCount.equals(other.dailyCallRetryCount))
			return false;
		if (dataSlice == null) {
			if (other.dataSlice != null)
				return false;
		} else if (!dataSlice.equals(other.dataSlice))
			return false;
		if (indirectProspects == null) {
			if (other.indirectProspects != null)
				return false;
		} else if (!indirectProspects.equals(other.indirectProspects))
			return false;
		if (outboundNumber == null) {
			if (other.outboundNumber != null)
				return false;
		} else if (!outboundNumber.equals(other.outboundNumber))
			return false;
		if (prospect == null) {
			if (other.prospect != null)
				return false;
		} else if (!prospect.equals(other.prospect))
			return false;
		if (prospectCallId == null) {
			if (other.prospectCallId != null)
				return false;
		} else if (!prospectCallId.equals(other.prospectCallId))
			return false;
		if (qualityBucketSort != other.qualityBucketSort)
			return false;
		if (twilioCallSid == null) {
			if (other.twilioCallSid != null)
				return false;
		} else if (!twilioCallSid.equals(other.twilioCallSid))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}

//	@Override
//	public int compareTo(ProspectCallCacheDTO o) {
//		Comparator<ProspectCallCacheDTO> result = null;
//		LeadSortOrder campaignLeadSortOrder = leadSortOrder;
//		switch (campaignLeadSortOrder) {
//		case FIFO:
//			Comparator<ProspectCallCacheDTO> fifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
//					.thenComparing(ProspectCallCacheDTO::getCreatedDate);
//			result = fifoOrder;
//			break;
//		case LIFO:
//			Comparator<ProspectCallCacheDTO> lifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
//					.thenComparing(ProspectCallCacheDTO::getCreatedDate, Comparator.reverseOrder());
//			result = lifoOrder;
//			break;
//		case QUALITY_FIFO:
//			Comparator<ProspectCallCacheDTO> qualityFifoOrder = Comparator.comparing(ProspectCallCacheDTO::getQualityBucketSort)
//					.thenComparing(ProspectCallCacheDTO::getCallRetryCount).thenComparing(ProspectCallCacheDTO::getUpdatedDate);
//			result = qualityFifoOrder;
//			break;
//		case QUALITY_LIFO:
//			Comparator<ProspectCallCacheDTO> qualityLifoOrder = Comparator.comparing(ProspectCallCacheDTO::getQualityBucketSort)
//					.thenComparing(ProspectCallCacheDTO::getCallRetryCount)
//					.thenComparing(ProspectCallCacheDTO::getUpdatedDate, Comparator.reverseOrder());
//			result = qualityLifoOrder;
//			break;
//		case RETRY_FIFO:
//			Comparator<ProspectCallCacheDTO> retryFifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
//					.thenComparing(ProspectCallCacheDTO::getQualityBucketSort)
//					.thenComparing(ProspectCallCacheDTO::getUpdatedDate);
//			result = retryFifoOrder;
//			break;
//		case RETRY_LIFO:
//			Comparator<ProspectCallCacheDTO> retryLifoOrder = Comparator.comparing(ProspectCallCacheDTO::getCallRetryCount)
//					.thenComparing(ProspectCallCacheDTO::getQualityBucketSort)
//					.thenComparing(ProspectCallCacheDTO::getUpdatedDate, Comparator.reverseOrder());
//			result = retryLifoOrder;
//			break;
//		}
//		return result;
//		return 0;
//	}
	
	
	public int compareTo(ProspectCallCacheDTO o) {
		return Comparator.comparing(ProspectCallCacheDTO::getQualityBucketSort)
				.thenComparing(ProspectCallCacheDTO::getCallRetryCount)
				.thenComparing(ProspectCallCacheDTO::getUpdatedDate, Comparator.reverseOrder()).compare(this, o);
	}

}