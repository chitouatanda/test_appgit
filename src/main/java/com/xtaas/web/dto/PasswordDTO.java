package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PasswordDTO {
	@JsonProperty
	private String newPassword;
	
	@JsonProperty
	private String currentPassword;
			
	public PasswordDTO() {
		
	}
	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/**
	 * @return the currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}
	/**
	 * @param currentPassword the currentPassword to set
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	
	
	
}
