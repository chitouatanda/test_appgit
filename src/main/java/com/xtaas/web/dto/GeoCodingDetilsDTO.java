package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class GeoCodingDetilsDTO {

	private List<AddressComponent> address_components;

	private String formatted_address;

	@JsonIgnore
	private String geometry;
	@JsonIgnore
	private String place_id;
	@JsonIgnore
	private List<String> types;
	@JsonIgnore
	private List<String> postcode_localities;
	
	public GeoCodingDetilsDTO() {
	}

	public String getFormatted_address() {
		return formatted_address;
	}

	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}

	public List<AddressComponent> getAddress_components() {
		return address_components;
	}

	public void setAddress_components(List<AddressComponent> address_components) {
		this.address_components = address_components;
	}

	public String getGeometry() {
		return geometry;
	}

	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

	public List<String> getPostcode_localities() {
		return postcode_localities;
	}

	public void setPostcode_localities(List<String> postcode_localities) {
		this.postcode_localities = postcode_localities;
	}
	

}
