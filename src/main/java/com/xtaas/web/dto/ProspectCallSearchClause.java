package com.xtaas.web.dto;

public enum ProspectCallSearchClause {

	ByQa, ByAgent, ByDispositionStatus, ByProspectCallStatus, ByQa2, ByQaStat, BySecondaryQaStat;
}
