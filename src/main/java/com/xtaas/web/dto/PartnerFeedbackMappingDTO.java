package com.xtaas.web.dto;

import java.util.LinkedHashMap;
import java.util.List;

import com.xtaas.db.entity.PartnerFeedbackMapping;

/**
 * DTO for PartnerFeedbackMapping
 * 
 * @author pranay
 *
 */
public class PartnerFeedbackMappingDTO {
	private String name;
	private boolean enableDataFeedback;
	private int dataFeedbackInDays;
	private String fromEmail;
	private List<String> toEmails;
	private String emailSubject;
	private String emailMessageBody;
	private List<String> errorToEmails;
	private String s3BucketName;
	private LinkedHashMap<String, String> headers;
	private LinkedHashMap<String, String> statusMapping;

	public PartnerFeedbackMappingDTO(PartnerFeedbackMapping partnerFeedbackMapping) {
		this.name = partnerFeedbackMapping.getName();
		this.enableDataFeedback = partnerFeedbackMapping.isEnableDataFeedback();
		this.dataFeedbackInDays = partnerFeedbackMapping.getDataFeedbackInDays();
		this.fromEmail = partnerFeedbackMapping.getFromEmail();
		this.toEmails = partnerFeedbackMapping.getToEmails();
		this.emailSubject = partnerFeedbackMapping.getEmailSubject();
		this.emailMessageBody = partnerFeedbackMapping.getEmailMessageBody();
		this.errorToEmails = partnerFeedbackMapping.getErrorToEmails();
		this.s3BucketName = partnerFeedbackMapping.getS3BucketName();
		this.headers = partnerFeedbackMapping.getHeaders();
		this.statusMapping = partnerFeedbackMapping.getStatusMapping();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnableDataFeedback() {
		return enableDataFeedback;
	}

	public void setEnableDataFeedback(boolean enableDataFeedback) {
		this.enableDataFeedback = enableDataFeedback;
	}

	public int getDataFeedbackInDays() {
		return dataFeedbackInDays;
	}

	public void setDataFeedbackInDays(int dataFeedbackInDays) {
		this.dataFeedbackInDays = dataFeedbackInDays;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public List<String> getToEmails() {
		return toEmails;
	}

	public void setToEmails(List<String> toEmails) {
		this.toEmails = toEmails;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailMessageBody() {
		return emailMessageBody;
	}

	public void setEmailMessageBody(String emailMessageBody) {
		this.emailMessageBody = emailMessageBody;
	}

	public List<String> getErrorToEmails() {
		return errorToEmails;
	}

	public void setErrorToEmails(List<String> errorToEmails) {
		this.errorToEmails = errorToEmails;
	}

	public String getS3BucketName() {
		return s3BucketName;
	}

	public void setS3BucketName(String s3BucketName) {
		this.s3BucketName = s3BucketName;
	}

	public LinkedHashMap<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(LinkedHashMap<String, String> headers) {
		this.headers = headers;
	}

	public LinkedHashMap<String, String> getStatusMapping() {
		return statusMapping;
	}

	public void setStatusMapping(LinkedHashMap<String, String> statusMapping) {
		this.statusMapping = statusMapping;
	}

	@Override
	public String toString() {
		return "PartnerFeedbackMappingDTO [name=" + name + ", enableDataFeedback=" + enableDataFeedback
				+ ", dataFeedbackInDays=" + dataFeedbackInDays + ", fromEmail=" + fromEmail + ", toEmails=" + toEmails
				+ ", emailSubject=" + emailSubject + ", emailMessageBody=" + emailMessageBody + ", errorToEmails="
				+ errorToEmails + ", s3BucketName=" + s3BucketName + ", headers=" + headers + ", statusMapping="
				+ statusMapping + "]";
	}

}
