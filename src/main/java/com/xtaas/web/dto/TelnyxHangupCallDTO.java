package com.xtaas.web.dto;

public class TelnyxHangupCallDTO {

  private String hangup_cause;

  private String hangup_source;

  private String state;

  public String getHangup_cause() {
    return hangup_cause;
  }

  public void setHangup_cause(String hangup_cause) {
    this.hangup_cause = hangup_cause;
  }

  public String getHangup_source() {
    return hangup_source;
  }

  public void setHangup_source(String hangup_source) {
    this.hangup_source = hangup_source;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

}