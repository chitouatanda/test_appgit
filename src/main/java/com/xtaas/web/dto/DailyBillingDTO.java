package com.xtaas.web.dto;

import java.util.List;

public class DailyBillingDTO {

	private List<ConnectDetailsDTO> connectDetailsDTOs;
	
	private List<VoiceUsageDetailsDTO> voiceUsageDetailsDTO;
	
	private int totalAttempts;
	
	private int totalContacts;
	
	private int totalConnects;
	
	private int totalSuccess;
	
	private double totalCost;
	
	public int getTotalAttempts() {
		return totalAttempts;
	}

	public void setTotalAttempts(int totalAttempts) {
		this.totalAttempts = totalAttempts;
	}

	public int getTotalContacts() {
		return totalContacts;
	}

	public void setTotalContacts(int totalContacts) {
		this.totalContacts = totalContacts;
	}

	public int getTotalConnects() {
		return totalConnects;
	}

	public void setTotalConnects(int totalConnects) {
		this.totalConnects = totalConnects;
	}

	public int getTotalSuccess() {
		return totalSuccess;
	}

	public void setTotalSuccess(int totalSuccess) {
		this.totalSuccess = totalSuccess;
	}

	public List<ConnectDetailsDTO> getConnectDetailsDTOs() {
		return connectDetailsDTOs;
	}

	public void setConnectDetailsDTOs(List<ConnectDetailsDTO> connectDetailsDTOs) {
		this.connectDetailsDTOs = connectDetailsDTOs;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public List<VoiceUsageDetailsDTO> getVoiceUsageDetailsDTO() {
		return voiceUsageDetailsDTO;
	}

	public void setVoiceUsageDetailsDTO(List<VoiceUsageDetailsDTO> voiceUsageDetailsDTO) {
		this.voiceUsageDetailsDTO = voiceUsageDetailsDTO;
	}

	

}
