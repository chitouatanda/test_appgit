package com.xtaas.web.dto;

import java.util.List;

public class RestCdpCampaignContactDTO {

	public String campaignName;
	public String organizationId;
	public List<RestCampaignContactDTO> campaignContacts;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public List<RestCampaignContactDTO> getCampaignContacts() {
		return campaignContacts;
	}

	public void setCampaignContacts(List<RestCampaignContactDTO> campaignContacts) {
		this.campaignContacts = campaignContacts;
	}

	@Override
	public String toString() {
		return "RestCdpCampaignContactDTO [campaignName=" + campaignName + ", organizationId=" + organizationId
				+ ", campaignContacts=" + campaignContacts + "]";
	}

}