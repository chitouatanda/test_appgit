package com.xtaas.web.dto;



import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"areaLevel1",
"city",
"country",
"formattedAddress",
"postalCode",
"street1",
"street2"
})
public class LeadIQLocationInfo {

@JsonProperty("areaLevel1")
private String areaLevel1;

@JsonProperty("city")
private String city;

@JsonProperty("country")
private String country;

@JsonProperty("formattedAddress")
private String formattedAddress;

@JsonProperty("postalCode")
private String postalCode;

@JsonProperty("street1")
private String street1;

@JsonProperty("street2")
private String street2;

@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("areaLevel1")
public String getAreaLevel1() {
return areaLevel1;
}

@JsonProperty("areaLevel1")
public void setAreaLevel1(String areaLevel1) {
this.areaLevel1 = areaLevel1;
}

@JsonProperty("city")
public String getCity() {
return city;
}

@JsonProperty("city")
public void setCity(String city) {
this.city = city;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonProperty("formattedAddress")
public String getFormattedAddress() {
return formattedAddress;
}

@JsonProperty("formattedAddress")
public void setFormattedAddress(String formattedAddress) {
this.formattedAddress = formattedAddress;
}

@JsonProperty("postalCode")
public String getPostalCode() {
return postalCode;
}

@JsonProperty("postalCode")
public void setPostalCode(String postalCode) {
this.postalCode = postalCode;
}

@JsonProperty("street1")
public String getStreet1() {
return street1;
}

@JsonProperty("street1")
public void setStreet1(String street1) {
this.street1 = street1;
}

@JsonProperty("street2")
public String getStreet2() {
return street2;
}

@JsonProperty("street2")
public void setStreet2(String street2) {
this.street2 = street2;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
return new ToStringBuilder(this).append("areaLevel1", areaLevel1).append("city", city).append("country", country).append("formattedAddress", formattedAddress).append("postalCode", postalCode).append("street1", street1).append("street2", street2).append("additionalProperties", additionalProperties).toString();
}

}