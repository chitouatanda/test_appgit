package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.EmailMessage;

public class EmailMessageDTO {
	@JsonProperty
	@NotNull
	private String subject;
	
	@JsonProperty
	@NotNull
	private String message;
	
	@JsonProperty
	private String fromEmail; //optional
	
	@JsonProperty
	private String fromName; //optional
			
	@JsonProperty
	private String replyTo; //optional
	
	@JsonProperty
	private String unSubscribeUrl;//optional
	
	@JsonProperty
	private String clickUrl;//optional
	
	public EmailMessageDTO() {
	
	}
	
	public EmailMessageDTO(EmailMessage emailMessage) {
		this.fromEmail = emailMessage.getFromEmail();
		this.fromName = emailMessage.getFromName();
		this.subject = emailMessage.getSubject();
		//this.message = emailMessage.getMessage().replaceAll("<br>", "\n");
		this.message = emailMessage.getMessage();
		this.replyTo = emailMessage.getReplyTo();
		this.clickUrl = emailMessage.getClickUrl();
		this.unSubscribeUrl = emailMessage.getUnSubscribeUrl();
	}
	
	public EmailMessage toEmailMessage() {
		//EmailMessage emailMessage = new EmailMessage(subject, message.replaceAll("\n", "<br>"));
		EmailMessage emailMessage = new EmailMessage(subject, message);
		emailMessage.setFromEmail(getFromEmail());
		emailMessage.setFromName(getFromName());
		emailMessage.setReplyTo(getReplyTo());
		emailMessage.setUnSubscribeUrl(getUnSubscribeUrl());
		emailMessage.setClickUrl(getClickUrl());
		return emailMessage;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}
	
	public String getFromEmail() {
		return fromEmail;
	}

	public String getFromName() {
		return fromName;
	}
	
	public String getReplyTo() {
		return replyTo;
	}

	public String getUnSubscribeUrl() {
		return unSubscribeUrl;
	}

	public void setUnSubscribeUrl(String unSubscribeUrl) {
		this.unSubscribeUrl = unSubscribeUrl;
	}

	public String getClickUrl() {
		return clickUrl;
	}

	public void setClickUrl(String clickUrl) {
		this.clickUrl = clickUrl;
	}
}
