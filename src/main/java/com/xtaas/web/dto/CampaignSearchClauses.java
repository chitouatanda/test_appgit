package com.xtaas.web.dto;

public enum CampaignSearchClauses {
	ByCampaignManager, BySupervisor, ByTeamIds, ByQa, ByQa2 ;
}
