package com.xtaas.web.dto;

public enum AgentMetricsSearchClauses {
	BySupervisor, ByCampaign, BySupervisorLiveQueue
}
