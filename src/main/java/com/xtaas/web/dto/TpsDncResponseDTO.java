package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TpsDncResponseDTO {

	@JsonProperty("phone_format_status")
	private String phoneFormatStatus;

	@JsonProperty("error")
	private String error;

	@JsonProperty("data")
	private Data data;

	private boolean isDNC;

	public String getPhoneFormatStatus() {
		return phoneFormatStatus;
	}

	public void setPhoneFormatStatus(String phoneFormatStatus) {
		this.phoneFormatStatus = phoneFormatStatus;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public boolean isDNC() {
		return isDNC;
	}

	public void setDNC(boolean isDNC) {
		this.isDNC = isDNC;
	}

	public class Data {
		@JsonProperty("tel_no")
		private String telNo;

		@JsonProperty("national")
		private String national;

		@JsonProperty("international")
		private String international;

		@JsonProperty("line_type")
		private String lineType;

		@JsonProperty("location")
		private String location;

		@JsonProperty("original_carrier")
		private String originalCarrier;

		@JsonProperty("on_tps")
		private String onTps;

		@JsonProperty("on_ctps")
		private String onCtps;

		@JsonProperty("check_date")
		private String checkDate;

		@JsonProperty("original_carrier_allocation_date")
		private String originalCarrierAllocationDate;

		@JsonProperty("remaining_credits")
		private String remainingCredits;

		public String getTelNo() {
			return telNo;
		}

		public void setTelNo(String telNo) {
			this.telNo = telNo;
		}

		public String getNational() {
			return national;
		}

		public void setNational(String national) {
			this.national = national;
		}

		public String getInternational() {
			return international;
		}

		public void setInternational(String international) {
			this.international = international;
		}

		public String getLineType() {
			return lineType;
		}

		public void setLineType(String lineType) {
			this.lineType = lineType;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getOriginalCarrier() {
			return originalCarrier;
		}

		public void setOriginalCarrier(String originalCarrier) {
			this.originalCarrier = originalCarrier;
		}

		public String getOnTps() {
			return onTps;
		}

		public void setOnTps(String onTps) {
			this.onTps = onTps;
		}

		public String getOnCtps() {
			return onCtps;
		}

		public void setOnCtps(String onCtps) {
			this.onCtps = onCtps;
		}

		public String getCheckDate() {
			return checkDate;
		}

		public void setCheckDate(String checkDate) {
			this.checkDate = checkDate;
		}

		public String getOriginalCarrierAllocationDate() {
			return originalCarrierAllocationDate;
		}

		public void setOriginalCarrierAllocationDate(String originalCarrierAllocationDate) {
			this.originalCarrierAllocationDate = originalCarrierAllocationDate;
		}

		public String getRemainingCredits() {
			return remainingCredits;
		}

		public void setRemainingCredits(String remainingCredits) {
			this.remainingCredits = remainingCredits;
		}

	}

}
