package com.xtaas.web.dto;

import java.util.List;

public class UserImpersonateDTO {
	private String username;
	private List<String> roles;
	private String organization;
	private String firstName;
	private String lastName;
	private String email;

	protected UserImpersonateDTO() {
	}

	public UserImpersonateDTO(LoginDTO loginDTO) {
		setUsername(loginDTO.getId());
		setRoles(loginDTO.getRoles());
		setOrganization(loginDTO.getOrganization());
		setFirstName(loginDTO.getFirstName());
		setLastName(loginDTO.getLastName());
		setEmail(loginDTO.getEmail());
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserImpersonateDTO [username=" + username + ", roles=" + roles + ", organization=" + organization
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}

}
