package com.xtaas.web.dto;

public class TelnyxHangupRequest {
  
  private String client_state;

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }
}
