package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SupervisorDTO {

	@JsonProperty
	private String id;

	@JsonProperty
	private String firstName;

	@JsonProperty
	private String lastName;

	@JsonProperty
	private String adminRole;

	public SupervisorDTO() {

	}

	public SupervisorDTO(LoginDTO login) {
		this.id = login.getId();
		this.firstName = login.getFirstName();
		this.lastName = login.getLastName();
		this.adminRole = login.getAdminRole();
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	public String getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	@Override
	public String toString() {
		return "SupervisorDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", adminRole="
				+ adminRole + "]";
	}
	
}
