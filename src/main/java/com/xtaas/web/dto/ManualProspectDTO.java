package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.infra.zoominfo.personsearch.valueobject.PersonRecord;
import com.xtaas.insideview.Contacts;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;

public class ManualProspectDTO {

	private String personId;

	private String source;

	private PersonRecord personRecord;

	private boolean isDirectSearch;

	private Contacts contactsRecord;

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public PersonRecord getPersonRecord() {
		return personRecord;
	}

	public void setPersonRecord(PersonRecord personRecord) {
		this.personRecord = personRecord;
	}

	public boolean isDirectSearch() {
		return isDirectSearch;
	}

	public void setDirectSearch(boolean directSearch) {
		isDirectSearch = directSearch;
	}

	public Contacts getContactsRecord() {
		return contactsRecord;
	}

	public void setContactsRecord(Contacts contactsRecord) {
		this.contactsRecord = contactsRecord;
	}
}