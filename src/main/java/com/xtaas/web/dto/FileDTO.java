package com.xtaas.web.dto;

public class FileDTO {

	private String name;
	private String content;
	private String type;

	public FileDTO(String name, String content, String type) {
		super();
		setName(name);
		setContent(content);
		setType(type);
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	private void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	private void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "FileDTO [name=" + name + ", content=" + content + ", type=" + type + "]";
	}

}
