package com.xtaas.web.dto;

import java.util.List;

public class QueuedProspectDTO {

	private List<ProspectCallCacheDTO> queuedProspects;
	private List<ProspectCallDTO> queuedProspectCalls;
	private int delta;

	protected QueuedProspectDTO() {
	}

	public QueuedProspectDTO(List<ProspectCallCacheDTO> queuedProspects, int delta) {
		super();
		this.queuedProspects = queuedProspects;
		this.delta = delta;
	}
	
	public QueuedProspectDTO(List<ProspectCallDTO> queuedProspectCalls, int delta , int val) {
		super();
		this.queuedProspectCalls = queuedProspectCalls;
		this.delta = delta;
	}

	public List<ProspectCallCacheDTO> getQueuedProspects() {
		return queuedProspects;
	}

	public void setQueuedProspects(List<ProspectCallCacheDTO> queuedProspects) {
		this.queuedProspects = queuedProspects;
	}

	public int getDelta() {
		return delta;
	}

	public void setDelta(int delta) {
		this.delta = delta;
	}

	public List<ProspectCallDTO> getQueuedProspectCalls() {
		return queuedProspectCalls;
	}

	public void setQueuedProspectCalls(List<ProspectCallDTO> queuedProspectCalls) {
		this.queuedProspectCalls = queuedProspectCalls;
	}

}
