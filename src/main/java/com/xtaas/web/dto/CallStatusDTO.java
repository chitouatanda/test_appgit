/**
 * 
 */
package com.xtaas.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author djain
 *
 */
public class CallStatusDTO {
	
	public enum CallState {
		CONNECTED, DISCONNECTED, PREVIEW, DIALED, MUTE, UNMUTE, HOLD, UNHOLD;
	}
	
	@JsonProperty
	private CallState callState;
	
	@JsonProperty
	private Date callStartTime;
	
	@JsonProperty
	private int callStateTransitionDuration; //calculated in UI by subtracting current call state time - previous call state time
	
	@JsonProperty
	private String twilioCallSid; //Twilio Call Sid sent in case of Preview, Preview_System mode
	
	@JsonProperty
	private String outboundNumber;
	
	/**
	 * @return the callStatus
	 */
	public CallState getCallState() {
		return callState;
	}

	/**
	 * @param callStatus the callStatus to set
	 */
	public void setCallStatus(CallState callState) {
		this.callState = callState;
	}

	/**
	 * @return the callStartTime
	 */
	public Date getCallStartTime() {
		return callStartTime;
	}

	/**
	 * @param callStartTime the callStartTime to set
	 */
	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}

	/**
	 * @return the callStateTransitionDuration
	 */
	public int getCallStateTransitionDuration() {
		return callStateTransitionDuration;
	}

	/**
	 * @param callStateTransitionTime the callStateTransitionDuration to set
	 */
	public void setCallStateTransitionDuration(int callStateTransitionDuration) {
		this.callStateTransitionDuration = callStateTransitionDuration;
	}

	/**
	 * @return the twilioCallSid
	 */
	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	/**
	 * @param twilioCallSid the twilioCallSid to set
	 */
	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}
}
