package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.Expression;

import java.util.List;

public class DataBuyQueueExpDTO {

	@JsonProperty
	private List<Expression> expressions;

	public List<Expression> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<Expression> expressions) {
		this.expressions = expressions;
	}

	@Override
	public String toString() {
		return "DataBuyQueueExpDTO{" +
				"expressions=" + expressions +
				'}';
	}




}
