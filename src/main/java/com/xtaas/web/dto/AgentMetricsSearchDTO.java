package com.xtaas.web.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Raghava
 * 
 */
public class AgentMetricsSearchDTO {
	@JsonProperty
	private AgentMetricsSearchClauses clause;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private Date fromDate;
	@JsonProperty
	private Date toDate;
	@JsonProperty
	private String timeZone;
	
	/**
	 * @return the clause
	 */
	public AgentMetricsSearchClauses getClause() {
		return clause;
	}
	/**
	 * @param clause the clause to set
	 */
	public void setClause(AgentMetricsSearchClauses clause) {
		this.clause = clause;
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}
	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
}
