package com.xtaas.web.dto;

public class TelnyxCallWebhookDTOData {

    private String occurred_at;

    private String event_type;

    private TelnyxCallWebhookDTOPayload payload;

    private String id;

    private String record_type;

    public String getOccurred_at() {
        return occurred_at;
    }

    public void setOccurred_at(String occurred_at) {
        this.occurred_at = occurred_at;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public TelnyxCallWebhookDTOPayload getPayload() {
        return payload;
    }

    public void setPayload(TelnyxCallWebhookDTOPayload payload) {
        this.payload = payload;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecord_type() {
        return record_type;
    }

    public void setRecord_type(String record_type) {
        this.record_type = record_type;
    }

}