package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignCommandDTO {
	@JsonProperty
	@NotNull
	private CampaignCommands campaignCommand;

	public CampaignCommands getCampaignCommand() {
		return campaignCommand;
	}
}
