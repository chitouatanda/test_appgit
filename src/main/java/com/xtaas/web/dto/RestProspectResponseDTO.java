package com.xtaas.web.dto;

import java.util.List;

public class RestProspectResponseDTO {

	private String contactId;
	
	private String sfdcCampaignId;
	
	private String status;
	
	private List<String> errorList;

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
	
	
}
