package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hasMore",
"results",
"totalResults"
})
public class LeadIQSearchPeople {

@JsonProperty("hasMore")
private Boolean hasMore;
@JsonProperty("results")
private List<LeadIQResult> results = null;
@JsonProperty("totalResults")
private Integer totalResults;

@JsonProperty("hasMore")
public Boolean getHasMore() {
return hasMore;
}

@JsonProperty("hasMore")
public void setHasMore(Boolean hasMore) {
this.hasMore = hasMore;
}

@JsonProperty("results")
public List<LeadIQResult> getResults() {
return results;
}

@JsonProperty("results")
public void setResults(List<LeadIQResult> results) {
this.results = results;
}

@JsonProperty("totalResults")
public Integer getTotalResults() {
return totalResults;
}

@JsonProperty("totalResults")
public void setTotalResults(Integer totalResults) {
this.totalResults = totalResults;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("hasMore", hasMore).append("results", results).append("totalResults", totalResults).toString();
}

}