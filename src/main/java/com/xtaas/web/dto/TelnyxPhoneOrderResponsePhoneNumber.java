package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"status",
"regulatory_requirements",
"id",
"requirements_met",
"phone_number",
"record_type"
})
public class TelnyxPhoneOrderResponsePhoneNumber {

@JsonProperty("status")
private String status;
@JsonProperty("regulatory_requirements")
private List<Object> regulatoryRequirements = null;
@JsonProperty("id")
private String id;
@JsonProperty("requirements_met")
private Boolean requirementsMet;
@JsonProperty("phone_number")
private String phoneNumber;
@JsonProperty("record_type")
private String recordType;

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonProperty("regulatory_requirements")
public List<Object> getRegulatoryRequirements() {
return regulatoryRequirements;
}

@JsonProperty("regulatory_requirements")
public void setRegulatoryRequirements(List<Object> regulatoryRequirements) {
this.regulatoryRequirements = regulatoryRequirements;
}

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("requirements_met")
public Boolean getRequirementsMet() {
return requirementsMet;
}

@JsonProperty("requirements_met")
public void setRequirementsMet(Boolean requirementsMet) {
this.requirementsMet = requirementsMet;
}

@JsonProperty("phone_number")
public String getPhoneNumber() {
return phoneNumber;
}

@JsonProperty("phone_number")
public void setPhoneNumber(String phoneNumber) {
this.phoneNumber = phoneNumber;
}

@JsonProperty("record_type")
public String getRecordType() {
return recordType;
}

@JsonProperty("record_type")
public void setRecordType(String recordType) {
this.recordType = recordType;
}

}