package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.repository.DataBuyQueue;

public class ZoomBuySearchDTO {
	
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private long totalCount;
	@JsonProperty
	private String contactRequirement;
	@JsonProperty
	private boolean primaryIndustries;
	@JsonProperty
	private String duplicatePeriod;//Years,Months,Weeks,Days
	@JsonProperty
	private boolean isSufficiency;//Years,Months,Weeks,Days
	@JsonProperty
	private long prospectsPerSuccess;//Years,Months,Weeks,Days
	@JsonProperty
	private long directDialsToSuccess;
	@JsonProperty
	private long inDirectDialsToSuccess;
	@JsonProperty
	private boolean ignoreDefaultTitles;
	@JsonProperty
	private boolean ignoreANDTitles;
	@JsonProperty
	private boolean ignorePrimaryIndustries;
	@JsonProperty
	private boolean replenishData;
	@JsonProperty
	private boolean internalCampaign;
	@JsonProperty
	private boolean parallelBuy;
	@JsonProperty
	private boolean fetchSuccessData;
	@JsonProperty
	private String organizationId;
	@JsonProperty
	private List<HealthChecksCriteriaDTO> criteria;

	public String getOrganizationId() {
		return organizationId;
	}



	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}



	public boolean isFetchSuccessData() {
		return fetchSuccessData;
	}



	public void setFetchSuccessData(boolean fetchSuccessData) {
		this.fetchSuccessData = fetchSuccessData;
	}



	public boolean isParallelBuy() {
		return parallelBuy;
	}



	public void setParallelBuy(boolean parallelBuy) {
		this.parallelBuy = parallelBuy;
	}



	public boolean isInternalCampaign() {
		return internalCampaign;
	}



	public void setInternalCampaign(boolean internalCampaign) {
		this.internalCampaign = internalCampaign;
	}



	public boolean isReplenishData() {
		return replenishData;
	}



	public void setReplenishData(boolean replenishData) {
		this.replenishData = replenishData;
	}



	public boolean isIgnorePrimaryIndustries() {
		return ignorePrimaryIndustries;
	}



	public void setIgnorePrimaryIndustries(boolean ignorePrimaryIndustries) {
		this.ignorePrimaryIndustries = ignorePrimaryIndustries;
	}



	public boolean isIgnoreANDTitles() {
		return ignoreANDTitles;
	}



	public void setIgnoreANDTitles(boolean ignoreANDTitles) {
		this.ignoreANDTitles = ignoreANDTitles;
	}



	public boolean isIgnoreDefaultTitles() {
		return ignoreDefaultTitles;
	}



	public void setIgnoreDefaultTitles(boolean ignoreDefaultTitles) {
		this.ignoreDefaultTitles = ignoreDefaultTitles;
	}



	@JsonProperty
	private boolean ignoreDefaultIndustries;
	
	public boolean isIgnoreDefaultIndustries() {
		return ignoreDefaultIndustries;
	}



	public void setIgnoreDefaultIndustries(boolean ignoreDefaultIndustries) {
		this.ignoreDefaultIndustries = ignoreDefaultIndustries;
	}



	@JsonProperty
	private DataBuyQueue dataBuyQueue;
	
	public DataBuyQueue getDataBuyQueue() {
		return dataBuyQueue;
	}



	public void setDataBuyQueue(DataBuyQueue dataBuyQueue) {
		this.dataBuyQueue = dataBuyQueue;
	}



	/*
	 * 
	 */
	public long getDirectDialsToSuccess() {
		return directDialsToSuccess;
	}



	public void setDirectDialsToSuccess(long directDialsToSuccess) {
		this.directDialsToSuccess = directDialsToSuccess;
	}



	public long getInDirectDialsToSuccess() {
		return inDirectDialsToSuccess;
	}



	public void setInDirectDialsToSuccess(long inDirectDialsToSuccess) {
		this.inDirectDialsToSuccess = inDirectDialsToSuccess;
	}



	public long getProspectsPerSuccess() {
		return prospectsPerSuccess;
	}



	public void setProspectsPerSuccess(long prospectsPerSuccess) {
		this.prospectsPerSuccess = prospectsPerSuccess;
	}



	public boolean isSufficiency() {
		return isSufficiency;
	}



	public void setSufficiency(boolean isSufficiency) {
		this.isSufficiency = isSufficiency;
	}



	public String getContactRequirement() {
		return contactRequirement;
	}



	public void setContactRequirement(String contactRequirement) {
		this.contactRequirement = contactRequirement;
	}



	public boolean isPrimaryIndustries() {
		return primaryIndustries;
	}



	public void setPrimaryIndustries(boolean primaryIndustries) {
		this.primaryIndustries = primaryIndustries;
	}



	/*
	 * @return the campaign
	 */
	public String getCampaignId() {
		return campaignId;
	}



	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	
	public String getDuplicatePeriod() {
		return duplicatePeriod;
	}



	public void setDuplicatePeriod(String duplicatePeriod) {
		this.duplicatePeriod = duplicatePeriod;
	}



	public List<HealthChecksCriteriaDTO> getCriteria() {
		return criteria;
	}



	public void setCriteria(List<HealthChecksCriteriaDTO> criteria) {
		this.criteria = criteria;
	}


}
