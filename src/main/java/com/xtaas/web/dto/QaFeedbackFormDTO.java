package com.xtaas.web.dto;

import java.util.LinkedHashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.FeedbackFormAttribute;
import com.xtaas.db.entity.QaFeedbackForm;

public class QaFeedbackFormDTO {
	
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private LinkedHashMap<String, List<FeedbackFormAttribute>> formAtrributes;
	
	public QaFeedbackFormDTO() {
		
	}
	public QaFeedbackFormDTO(QaFeedbackForm qaFeedbackForm) {
		this.campaignId = qaFeedbackForm.getCampaignId();
		this.formAtrributes = qaFeedbackForm.getFormAtrributes();
	}
	
	public  QaFeedbackForm toQaFeedbackForm() {
		return new  QaFeedbackForm(campaignId, formAtrributes);
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the formAtrributes
	 */
	public LinkedHashMap<String, List<FeedbackFormAttribute>> getFormAtrributes() {
		return formAtrributes;
	}
	/**
	 * @param formAtrributes the formAtrributes to set
	 */
	public void setFormAtrributes(
			LinkedHashMap<String, List<FeedbackFormAttribute>> formAtrributes) {
		this.formAtrributes = formAtrributes;
	}
	
	
}
