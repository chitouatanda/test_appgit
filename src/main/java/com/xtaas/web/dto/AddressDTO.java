package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.Address;


public class AddressDTO {
	
	@JsonProperty
	private String line1;
	
	@JsonProperty
	private String line2;
	
	@JsonProperty
	private String city;
	
	@JsonProperty
	private String state;
	
	@JsonProperty
	private String postalCode;
	
	@JsonProperty
	private String country;
	
	@JsonProperty
	private String timeZone;
	
	public AddressDTO() {
	
	}

	public AddressDTO(Address address) {
		this.line1 = address.getLine1();
		this.line2 = address.getLine2();
		this.city = address.getCity();
		this.state = address.getState();
		this.postalCode = address.getPostalCode();
		this.country = address.getCountry();
		this.timeZone = address.getTimeZone();
	}
	
	public Address toAddress() {
		return new Address(line1, line2, city, state, country, postalCode, timeZone);
	}

	public String getLine1() {
		return line1;
	}

	public String getLine2() {
		return line2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCountry() {
		return country;
	}

	public String getTimeZone() {
		return timeZone;
	}
}
