package com.xtaas.web.dto;

public class TelnyxSpeakTextRequestDTO {

  private String language;

  private String payload;

  private String voice;

  private String client_state;

  private String command_id;

  private String payload_type;

  public TelnyxSpeakTextRequestDTO() {
    this.client_state = "";
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public String getVoice() {
    return voice;
  }

  public void setVoice(String voice) {
    this.voice = voice;
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }

  public String getCommand_id() {
    return command_id;
  }

  public void setCommand_id(String command_id) {
    this.command_id = command_id;
  }

  public String getPayload_type() {
    return payload_type;
  }

  public void setPayload_type(String payload_type) {
    this.payload_type = payload_type;
  }

}