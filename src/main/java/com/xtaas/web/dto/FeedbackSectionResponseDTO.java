package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.FeedbackResponseAttribute;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;

public class FeedbackSectionResponseDTO {
	
	@JsonProperty
	private String sectionName;
	
	@JsonProperty
	private String comments;
	
	@JsonProperty
	private List<FeedbackResponseAttribute> responseAttributes;
	/**
	 * 
	 */
	public FeedbackSectionResponseDTO() {
	
	}
	
	/**
	 * @param sectionName
	 * @param responseAttributes
	 */
	public FeedbackSectionResponseDTO(FeedbackSectionResponse feedbackSectionResponse) {
		this.sectionName = feedbackSectionResponse.getSectionName();
		this.responseAttributes = feedbackSectionResponse.getResponseAttributes();
	}


	public FeedbackSectionResponse toFeedbackSectionResponse () {
		return new FeedbackSectionResponse(sectionName, responseAttributes);
	}
	
	/**
	 * @return the sectionName
	 */
	public String getSectionName() {
		return sectionName;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * @param sectionName the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the responseAttributes
	 */
	public List<FeedbackResponseAttribute> getResponseAttributes() {
		return responseAttributes;
	}

	/**
	 * @param responseAttributes the responseAttributes to set
	 */
	public void setResponseAttributes(
			List<FeedbackResponseAttribute> responseAttributes) {
		this.responseAttributes = responseAttributes;
	}
	
	
}
