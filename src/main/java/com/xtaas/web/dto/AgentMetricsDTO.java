package com.xtaas.web.dto;

import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.application.service.AgentMetricsServiceImpl.AgentMetrics;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.domain.valueobject.AgentStatus;

public class AgentMetricsDTO {
	@JsonProperty
	private String campaignId;
	
	@JsonProperty
	private String campaignName;
	
	@JsonProperty
	private String agentId;
	
	@JsonProperty
	private String agentName;
	
	@JsonProperty
	private AgentStatus agentStatus;
	
	@JsonProperty
	private HashMap<AgentMetrics, String> metrics;
	
	@JsonProperty
    private boolean readOnlyCampaign;	// adding to give read and write access to partner supervisors

	@JsonProperty
	private String partnerId;	// added for "partner" column in agent & livequeue tab in supervisor screen.
	
	@JsonProperty
	private agentSkill agentType;

	@JsonProperty
	private boolean agentStatusChangeApprove;

	private boolean isSupervisorApproved;

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the agentStatus
	 */
	public AgentStatus getAgentStatus() {
		return agentStatus;
	}

	/**
	 * @param agentStatus the agentStatus to set
	 */
	public void setAgentStatus(AgentStatus agentStatus) {
		this.agentStatus = agentStatus;
	}

	/**
	 * @return the metrics
	 */
	public HashMap<AgentMetrics, String> getMetrics() {
		return metrics;
	}

	/**
	 * @param metrics the metrics to set
	 */
	public void setMetrics(HashMap<AgentMetrics, String> metrics) {
		this.metrics = metrics;
	}

	/* START READ/WRITE ACCESS TO SUPERVISOR
	 * DATE : 07/11/2017
	 * REASON : setting readOnlyCampaign flag to give read and write access to partner supervisors depending upon whether he is team's supervisor or partner supervisor */
	public boolean isReadOnlyCampaign() {
		return readOnlyCampaign;
	}

	public void setReadOnlyCampaign(boolean readOnlyCampaign) {
		this.readOnlyCampaign = readOnlyCampaign;
	}
	/* END READ/WRITE ACCESS TO SUPERVISOR */
	
	/* START
	 * DATE : 24/11/2017
	 * REASON : "partnerId" getter/setter used for agent & livequeue tab of supervisor screen. 
	 *  */
	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/* END */

	public agentSkill getAgentType() {
		return agentType;
	}

	public void setAgentType(agentSkill agentType) {
		this.agentType = agentType;
	}

	public boolean isAgentStatusChangeApprove() {
		return agentStatusChangeApprove;
	}

	public void setAgentStatusChangeApprove(boolean agentStatusChangeApprove) {
		this.agentStatusChangeApprove = agentStatusChangeApprove;
	}

	public boolean isSupervisorApproved() {
		return isSupervisorApproved;
	}

	public void setSupervisorApproved(boolean isSupervisorApproved) {
		this.isSupervisorApproved = isSupervisorApproved;
	}
}