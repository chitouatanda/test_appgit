package com.xtaas.web.dto;

public class Companie {

	private int id;
	private String name;
	private String domain;

	public Companie(int id, String name, String domain) {
		super();
		this.id = id;
		this.name = name;
		this.domain = domain;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Override
	public String toString() {
		return "Companie [id=" + id + ", name=" + name + ", domain=" + domain + "]";
	}

}
