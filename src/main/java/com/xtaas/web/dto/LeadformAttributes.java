package com.xtaas.web.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LeadformAttributes {

	private String subCategoryName;
	private String subCategoryId;
	private List<Attribute> attributes;

	public String getSubCategoryName() {
		return subCategoryName;
	}

	@XmlElement(name = "subcategoryname")
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	@XmlElement(name = "subcategoryid")
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	@XmlElementWrapper(name = "attributes")
	@XmlElement(name = "attribute")
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "LeadformAttributes [subCategoryName=" + subCategoryName + ", subCategoryId=" + subCategoryId
				+ ", attributes=" + attributes + "]";
	}

}
