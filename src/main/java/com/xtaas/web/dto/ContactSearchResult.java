package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.xtaas.domain.entity.Contact;

public class ContactSearchResult {
	private int count;
	private int MaxResults;
	private List<Contact> contacts;
	private double costAssociated;
	private String searchQuery;

	public ContactSearchResult() {
		contacts = new ArrayList<Contact>();
	}

	public void addCount(int count) {
		this.count += count;
	}

	public void addCostAssociated(double costAssociated) {
		this.costAssociated += costAssociated;
	}

	public void addContact(Contact contact) {
		this.contacts.add(contact);
	}

	/**
	 * @return the searchQuery
	 */
	public String getSearchQuery() {
		return searchQuery;
	}

	/**
	 * @param searchQuery the searchQuery to set
	 */
	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public int getCount() {
		return count;
	}

	public double getCostAssociated() {
		return costAssociated;
	}

	public List<Contact> getContacts() {
		return contacts;
	}
	
	public int getMaxResults() {
		return MaxResults;
	}
	
	public void setMaxResults(int maxResults) {
		MaxResults = maxResults;
	}
}
