package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"connection_id",
"phone_numbers"
})
public class TelnyxPhoneOrderRequestDTO {

@JsonProperty("connection_id")
private String connectionId;
@JsonProperty("phone_numbers")
private List<TelnyxPhoneOrderRequestPhoneNumber> phoneNumbers = null;

@JsonProperty("connection_id")
public String getConnectionId() {
return connectionId;
}

@JsonProperty("connection_id")
public void setConnectionId(String connectionId) {
this.connectionId = connectionId;
}

@JsonProperty("phone_numbers")
public List<TelnyxPhoneOrderRequestPhoneNumber> getPhoneNumbers() {
return phoneNumbers;
}

@JsonProperty("phone_numbers")
public void setPhoneNumbers(List<TelnyxPhoneOrderRequestPhoneNumber> phoneNumbers) {
this.phoneNumbers = phoneNumbers;
}

}