/**
 * 
 */
package com.xtaas.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * @author djain
 *
 */
public class AgentScheduleSearchDTO {
	
	@NotNull
	private Date startDate; // as ms from epoch
	
	@NotNull
	private Date endDate; // as ms from epoch
	
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
}
