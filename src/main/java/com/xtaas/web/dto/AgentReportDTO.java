package com.xtaas.web.dto;

public class AgentReportDTO {
	public int srNo;
	public String agentName;
	public String campaignName;
	public int recordsAttempted;
	public int contacts;
	public int connects;
	public int success;
	public float dialsToSuccess;
	public float connectPercentage;
	
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public int getRecordsAttempted() {
		return recordsAttempted;
	}
	public void setRecordsAttempted(int recordsAttempted) {
		this.recordsAttempted = recordsAttempted;
	}
	public int getContacts() {
		return contacts;
	}
	public void setContacts(int contacts) {
		this.contacts = contacts;
	}
	public int getConnects() {
		return connects;
	}
	public void setConnects(int connects) {
		this.connects = connects;
	}
	public int getSuccess() {
		return success;
	}
	public void setSuccess(int success) {
		this.success = success;
	}
	public float getDialsToSuccess() {
		return dialsToSuccess;
	}
	public void setDialsToSuccess(float dialsToSuccess) {
		this.dialsToSuccess = dialsToSuccess;
	}
	public float getConnectPercentage() {
		return connectPercentage;
	}
	public void setConnectPercentage(float connectPercentage) {
		this.connectPercentage = connectPercentage;
	}
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
}
