package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentQaAnswer;

public class CampaignDetailsForAgentDTO {

	@JsonProperty
	private boolean isEmailSuppressed;

	@JsonProperty
	private List<AssetDTO> assets;

	@JsonProperty
	private List<String> multiCurrentAssetId;

	@JsonProperty
	private List<AgentQaAnswer> customFields;

	public CampaignDetailsForAgentDTO(CampaignDTO campaignDTO) {
		this.setEmailSuppressed(campaignDTO.isEmailSuppressed());
		this.setAssets(campaignDTO.getAssets());
		this.setMultiCurrentAssetId(campaignDTO.getMultiCurrentAssetId());
		this.setCustomFields(null);
	}

	public CampaignDetailsForAgentDTO(CampaignDTO campaignDTO, List<AgentQaAnswer> defCustomFields) {
		this.setEmailSuppressed(campaignDTO.isEmailSuppressed());
		this.setAssets(campaignDTO.getAssets());
		this.setMultiCurrentAssetId(campaignDTO.getMultiCurrentAssetId());
		if (campaignDTO.isEnableCustomFields()) {
			this.setCustomFields(defCustomFields);
		} else {
			this.setCustomFields(null);
		}
	}
	

	public boolean isEmailSuppressed() {
		return isEmailSuppressed;
	}

	public void setEmailSuppressed(boolean isEmailSuppressed) {
		this.isEmailSuppressed = isEmailSuppressed;
	}

	public List<AssetDTO> getAssets() {
		return assets;
	}

	public void setAssets(List<AssetDTO> assets) {
		this.assets = assets;
	}

	public List<String> getMultiCurrentAssetId() {
		return multiCurrentAssetId;
	}

	public void setMultiCurrentAssetId(List<String> multiCurrentAssetId) {
		this.multiCurrentAssetId = multiCurrentAssetId;
	}

	public List<AgentQaAnswer> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<AgentQaAnswer> customFields) {
		this.customFields = customFields;
	}

}