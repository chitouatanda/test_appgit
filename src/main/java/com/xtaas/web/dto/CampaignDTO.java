package com.xtaas.web.dto;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.application.service.CampaignServiceImpl;
import com.xtaas.db.entity.Login;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.utils.XtaasDateUtils;

@JsonInclude(Include.NON_NULL)
public class CampaignDTO {
	private static final Logger logger = LoggerFactory.getLogger(CampaignDTO.class);
	
	@JsonProperty
	private String sfdcCampaignId;

	@JsonProperty
	private String id;

	@JsonProperty
	private String organizationId;

	@JsonProperty
	private String campaignManagerId;

	@JsonProperty
	@NotNull
	private CampaignStatus status;

	@JsonProperty
	@NotNull
	private String name;

	@JsonProperty
	private String startDate; // as selected by user in MM/dd/yyyy format

	@JsonProperty
	private String endDate; // as selected by user in MM/dd/yyyy format

	@JsonProperty
	private CampaignTypes type; // Lead Verification, Lead Generation,...

	@JsonProperty
	private int deliveryTarget;

	@JsonProperty
	private String domain;

	@JsonProperty
	private int dailyCap;

	@JsonProperty
	private ExpressionGroupDTO qualificationCriteria;

	@JsonProperty
	private String qualificationClause;

	@JsonProperty
	private List<String> restrictions;

	@JsonProperty
	private Set<String> informationCriteria;

	@JsonProperty
	private ExpressionGroupDTO leadFilteringCriteria;

	@JsonProperty
	private String leadFilteringClause;

	@JsonProperty
	private List<AssetDTO> assets;

	@JsonProperty
	private boolean selfManageTeam;

	@JsonProperty
	private int agentCount;

	@JsonProperty
	private String currentAssetId;

	@JsonProperty
	private List<String> multiCurrentAssetId;
	
	private CallGuide callGuide;

	@JsonProperty
	private String brand;

	@JsonProperty
	private CallSpeed callSpeedPerMinPerAgent;

	@JsonProperty
	private CallSpeed callSpeedRetryPerMinPerAgent;

	@JsonProperty
	private LeadSortOrder leadSortOrder;

	@JsonProperty
	private Date updatedDate;

	@JsonProperty
	private int deliveryTargetAchieved;

	@JsonProperty
    private List<Login> supervisorList;

    @JsonProperty
    private Login login;

    @JsonProperty
    private String supervisorId;

    @JsonProperty
    private String timezone;

    @JsonProperty
    private String campaignRequirements;

    @JsonProperty
    private boolean readOnlyCampaign;

    @JsonProperty
	private boolean hidePhone;	// Added to hide phone number on agent screen from third party center

    @JsonProperty
   	private boolean hideEmail;	// Added to hide email number on agent screen from third party center

    @JsonProperty
    private List<String> partnerSupervisors;	// Added to give show/hide access to supervisor screen for partner supervisor

    @JsonProperty
    private EmailMessage assetEmailTemplate;

    @JsonProperty
	private boolean qaRequired; // added to check if leads go to qa or not for a campaign

    @JsonProperty
    private boolean enableMachineAnsweredDetection;

    @JsonProperty
    private boolean disableAMDOnQueued;

    @JsonProperty
    private boolean simpleDisposition;

    @JsonProperty
    private boolean useSlice;

    @JsonProperty
    private Map<String, Float> retrySpeed;

    @JsonProperty
	private boolean retrySpeedEnabled;

    @JsonProperty
    private float retrySpeedFactor;

    @JsonProperty
    private int dailyCallMaxRetries;

	@JsonProperty
	private int callMaxRetries;

	@JsonProperty
	private String clientName;

	@JsonProperty
	private boolean localOutboundCalling;

	@JsonProperty
	private int duplicateLeadsDuration;

	@JsonProperty
	private int duplicateCompanyDuration;

	@JsonProperty
	private int leadsPerCompany;

	@JsonProperty
	private List<String> campaignGroupIds;

	@JsonProperty
	private int costPerLead;

	@JsonProperty
	private DialerMode dialerMode;

	@JsonProperty
	private boolean multiProspectCalling;

	@JsonProperty
	private boolean isEmailSuppressed;

	@JsonProperty
	private boolean isNotConference;

	@JsonProperty
	private List<String> adminSupervisorIds;

	@JsonProperty
	private boolean isABM;

	@JsonProperty
	private String callControlProvider;

	@JsonProperty
	private String recordProspect;
	
	@JsonProperty
	private boolean isLeadIQ;
	
	@JsonProperty
	private boolean networkCheck;

	@JsonProperty
	private boolean bandwidthCheck;
	
	@JsonProperty
	private int telnyxCallTimeLimitSeconds;
	
	@JsonProperty
	private boolean autoMachineHangup;
	
	private long plivoAMDTime;
	
	@JsonProperty
	private boolean callAbandon;

	@JsonProperty
	private boolean enableProspectCaching;

	@JsonProperty
	private boolean removeMobilePhones;
	
	@JsonProperty
	private String autoModeCallProvider;
	
	@JsonProperty
	private String manualModeCallProvider;


	@JsonProperty
	private boolean hideNonSuccessPII;

	@JsonProperty
	private boolean enableCustomFields;

	@JsonProperty
    private boolean autoIVR;

	@JsonProperty
	private boolean enableInternetCheck;

	@JsonProperty
	private boolean enableStateCodeDropdown;
	
	@JsonProperty
	private boolean callableVanished;

	@JsonProperty
	private String sipProvider;
	
	@JsonProperty
	private boolean enableCallbackFeature;

	@JsonProperty
	private boolean autoCallbackStatusMapCleanup;

  //DATE: 18/12/2017
  	// Added script clone functionality
    /*@JsonProperty
	private CampaignTeam team;

	public CampaignTeam getTeam() {
		return team;
	}

	public void setTeam(CampaignTeam team) {
		this.team = team;
	}*/

	public boolean isEmailSuppressed() {
		return isEmailSuppressed;
	}

	public void setEmailSuppressed(boolean isEmailSuppressed) {
		this.isEmailSuppressed = isEmailSuppressed;
	}

	public CampaignDTO() {
	}

	public CampaignDTO(Campaign campaign) {
		this.sfdcCampaignId = campaign.getSfdcCampaignId();
		this.id = campaign.getId();
		this.organizationId = campaign.getOrganizationId();
		this.campaignManagerId = campaign.getCampaignManagerId();
		this.status = campaign.getStatus();
		this.name = campaign.getName();
		if(campaign.getEndDate() != null && campaign.getStartDate() != null){
		    this.startDate = XtaasDateUtils.convertDateToString(campaign.getStartDate(), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
	        this.endDate = XtaasDateUtils.convertDateToString(campaign.getEndDate(), XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
		}
		this.type = campaign.getType();
		this.deliveryTarget = campaign.getDeliveryTarget();
		//this.domain = campaign.getDomain();
		this.dailyCap = campaign.getDailyCap();
		if (campaign.getQualificationCriteria() != null) {
			this.qualificationCriteria = new ExpressionGroupDTO(campaign.getQualificationCriteria());
		}
		this.qualificationClause = campaign.getQualificationClause();
		this.restrictions = campaign.getRestrictions();
		this.informationCriteria = campaign.getInformationCriteria();
		if (campaign.getLeadFilteringCriteria() != null) {
			this.leadFilteringCriteria = new ExpressionGroupDTO(campaign.getLeadFilteringCriteria());
		}
		this.leadFilteringClause = campaign.getLeadFilteringClause();
		if (campaign.getAssets() != null) {
			this.assets = new ArrayList<AssetDTO>();
			for (Asset asset : campaign.getAssets()) {
				this.assets.add(new AssetDTO(asset));
			}
		}
		this.selfManageTeam = campaign.isSelfManageTeam();
		if( campaign.getTeam() != null && campaign.getTeam().getAgents() != null) {
			this.agentCount = campaign.getTeam().getAgents().size();
			this.supervisorId = campaign.getTeam().getSupervisorId();
		}
		if(campaign.getMultiCurrentAssetId() != null && campaign.getMultiCurrentAssetId().size() > 0) {
			this.multiCurrentAssetId = campaign.getMultiCurrentAssetId();
		}
		if (campaign.getCurrentAssetId() != null) {
			this.currentAssetId = campaign.getCurrentAssetId();
		}
		if (campaign.getBrand() == null || campaign.getBrand().isEmpty()) {
			this.brand = campaign.getOrganizationId();
		} else {
			this.brand = campaign.getBrand();
		}
		this.setUpdatedDate(campaign.getUpdatedDate());
		this.timezone = campaign.getTimezone();
		this.campaignRequirements = campaign.getCampaignRequirements();
		this.hidePhone = campaign.isHidePhone();	// Added to hide phone number on agent screen from third party center
		this.hideEmail = campaign.isHideEmail();	// Added to hide email number on agent screen from third party center
		this.partnerSupervisors = campaign.getPartnerSupervisors();
		this.assetEmailTemplate = campaign.getAssetEmailTemplate();
		this.qaRequired = campaign.getQaRequired();
		this.enableMachineAnsweredDetection = campaign.isEnableMachineAnsweredDetection();
		this.disableAMDOnQueued = campaign.isDisableAMDOnQueued();
		//this.team = campaign.getTeam();
		this.simpleDisposition = campaign.isSimpleDisposition();
		this.sipProvider = campaign.getSipProvider() != null? campaign.getSipProvider(): "";
		this.useSlice = campaign.isUseSlice();
		this.retrySpeed = campaign.getRetrySpeed();
		this.retrySpeedEnabled = campaign.isRetrySpeedEnabled();
		this.retrySpeedFactor = campaign.getRetrySpeedFactor();
		this.dailyCallMaxRetries = campaign.getDailyCallMaxRetries();
		this.callMaxRetries = campaign.getCallMaxRetries();
		this.clientName = campaign.getClientName();
		this.localOutboundCalling = campaign.isLocalOutboundCalling();
		this.duplicateLeadsDuration = campaign.getDuplicateLeadsDuration();
		this.duplicateCompanyDuration = campaign.getDuplicateCompanyDuration();
		this.leadsPerCompany = campaign.getLeadsPerCompany();
		this.campaignGroupIds = campaign.getCampaignGroupIds();
		this.costPerLead = campaign.getCostPerLead();
		this.dialerMode = campaign.getDialerMode();
		this.isEmailSuppressed = campaign.isEmailSuppressed();
		this.isNotConference = campaign.isNotConference();
		this.adminSupervisorIds = campaign.getAdminSupervisorIds();
		this.isABM = campaign.isABM();
		this.callControlProvider= campaign.getCallControlProvider();
		this.recordProspect = campaign.getRecordProspect();
		if (campaign.getTeam() != null && campaign.getTeam().getCallGuide() != null) {
			this.callGuide = campaign.getTeam().getCallGuide();
		}
		this.isLeadIQ = campaign.isLeadIQ();
		this.bandwidthCheck = campaign.isBandwidthCheck();
		this.telnyxCallTimeLimitSeconds = campaign.getTelnyxCallTimeLimitSeconds();
		this.autoMachineHangup = campaign.isAutoMachineHangup();
		this.plivoAMDTime = campaign.getPlivoAMDTime();
		this.callAbandon = campaign.isCallAbandon();
		this.enableProspectCaching = campaign.isEnableProspectCaching();
		this.autoModeCallProvider = campaign.getAutoModeCallProvider();
		this.manualModeCallProvider = campaign.getManualModeCallProvider();
		this.hideNonSuccessPII = campaign.isHideNonSuccessPII();
		this.enableCustomFields = campaign.isEnableCustomFields();
		this.autoIVR = campaign.isAutoIVR();
		this.enableInternetCheck = campaign.isEnableInternetCheck();
		this.enableStateCodeDropdown = campaign.isEnableStateCodeDropdown();
		this.callableVanished = campaign.isCallableVanished();
		this.autoCallbackStatusMapCleanup = campaign.isAutoCallbackStatusMapCleanup();
	}

	public EmailMessage getAssetEmailTemplate() {
		return assetEmailTemplate;
	}

	public void setAssetEmailTemplate(EmailMessage assetEmailTemplate) {
		this.assetEmailTemplate = assetEmailTemplate;
	}

	/**
	 * @return the callSpeedPerMinPerAgent
	 */
	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	/**
	 * @param callSpeedPerMinPerAgent the callSpeedPerMinPerAgent to set
	 */
	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	/**
	 * @return the callSpeedRetryPerMinPerAgent
	 */
	public CallSpeed getCallSpeedRetryPerMinPerAgent() {
		return callSpeedRetryPerMinPerAgent;
	}

	/**
	 * @param callSpeedPerMinPerAgent the callSpeedPerMinPerAgent to set
	 */
	public void setCallSpeedRetryPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	/**
	 * @return the leadSortOrder
	 */
	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	/**
	 * @param leadSortOrder the leadSortOrder to set
	 */
	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	/**
	 * @return the sfdcCampaignId
	 */
	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	/**
	 * @param id the sfdcCampaignId to set
	 */
	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the organizationId
	 */
	public String getOrganizationId() {
		return organizationId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the campaignManagerId
	 */
	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	/**
	 * @param campaignManagerId the campaignManagerId to set
	 */
	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}

	/**
	 * @return the status
	 */
	public CampaignStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(CampaignStatus status) {
		this.status = status;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		try {
		    if(this.startDate != null){
		        return  XtaasDateUtils.getDate(this.startDate+" 00:00:00", XtaasDateUtils.DATETIME_FORMAT_DEFAULT); //defaulting time to 12 PM in UTC
		    }
        } catch (ParseException e) {
            e.printStackTrace();
        }
		return null;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
	    try {
	        if(this.endDate != null){
	            return XtaasDateUtils.getDate(this.endDate+" 15:59:59", XtaasDateUtils.DATETIME_FORMAT_DEFAULT); //defaulting time to 12 PM in UTC
	        }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the type
	 */
	public CampaignTypes getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(CampaignTypes type) {
		this.type = type;
	}

	/**
	 * @return the deliveryTarget
	 */
	public int getDeliveryTarget() {
		return deliveryTarget;
	}

	/**
	 * @param deliveryTarget the deliveryTarget to set
	 */
	public void setDeliveryTarget(int deliveryTarget) {
		this.deliveryTarget = deliveryTarget;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return the dailyCap
	 */
	public int getDailyCap() {
		return dailyCap;
	}

	/**
	 * @param dailyCap the dailyCap to set
	 */
	public void setDailyCap(int dailyCap) {
		this.dailyCap = dailyCap;
	}

	/**
	 * @return the qualificationCriteria
	 */
	public ExpressionGroupDTO getQualificationCriteria() {
		return qualificationCriteria;
	}

	/**
	 * @param qualificationCriteria the qualificationCriteria to set
	 */
	public void setQualificationCriteria(ExpressionGroupDTO qualificationCriteria) {
		this.qualificationCriteria = qualificationCriteria;
	}

	/**
	 * @return the qualificationClause
	 */
	public String getQualificationClause() {
		return qualificationClause;
	}

	/**
	 * @param qualificationClause the qualificationClause to set
	 */
	public void setQualificationClause(String qualificationClause) {
		this.qualificationClause = qualificationClause;
	}

	/**
	 * @return the restrictions
	 */
	public List<String> getRestrictions() {
		return restrictions;
	}

	/**
	 * @param restrictions the restrictions to set
	 */
	public void setRestrictions(List<String> restrictions) {
		this.restrictions = restrictions;
	}

	/**
	 * @return the informationCriteria
	 */
	public Set<String> getInformationCriteria() {
		return informationCriteria;
	}

	/**
	 * @param informationCriteria the informationCriteria to set
	 */
	public void setInformationCriteria(Set<String> informationCriteria) {
		this.informationCriteria = informationCriteria;
	}

	/**
	 * @return the leadFilteringCriteria
	 */
	public ExpressionGroupDTO getLeadFilteringCriteria() {
		return leadFilteringCriteria;
	}

	/**
	 * @param leadFilteringCriteria the leadFilteringCriteria to set
	 */
	public void setLeadFilteringCriteria(ExpressionGroupDTO leadFilteringCriteria) {
		this.leadFilteringCriteria = leadFilteringCriteria;
	}

	/**
	 * @return the leadFilteringClause
	 */
	public String getLeadFilteringClause() {
		return leadFilteringClause;
	}

	/**
	 * @param leadFilteringClause the leadFilteringClause to set
	 */
	public void setLeadFilteringClause(String leadFilteringClause) {
		this.leadFilteringClause = leadFilteringClause;
	}

	/**
	 * @return the assets
	 */
	public List<AssetDTO> getAssets() {
		return assets;
	}

	/**
	 * @param assets the assets to set
	 */
	public void setAssets(List<AssetDTO> assets) {
		this.assets = assets;
	}

	/**
	 * @return the selfManageTeam
	 */
	public boolean isSelfManageTeam() {
		return selfManageTeam;
	}

	/**
	 * @param selfManageTeam the selfManageTeam to set
	 */
	public void setSelfManageTeam(boolean selfManageTeam) {
		this.selfManageTeam = selfManageTeam;
	}

	/**
	 * @return the agentCount
	 */
	public int getAgentCount() {
		return agentCount;
	}

	/**
	 * @param agentCount the agentCount to set
	 */
	public void setAgentCount(int agentCount) {
		this.agentCount = agentCount;
	}

	/**
	 * @return the currentAssetId
	 */
	public String getCurrentAssetId() {
		return currentAssetId;
	}

	/**
	 * @param currentAssetId the currentAssetId to set
	 */
	public void setCurrentAssetId(String currentAssetId) {
		this.currentAssetId = currentAssetId;
	}

	/**
	 * @return the callGuide
	 */
	public CallGuide getCallGuide() {
		return callGuide;
	}

	/**
	 * @param callGuide the callGuide to set
	 */
	public void setCallGuide(CallGuide callGuide) {
		this.callGuide = callGuide;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the deliveryTargetAchieved
	 */
	public int getDeliveryTargetAchieved() {
		return deliveryTargetAchieved;
	}

	/**
	 * @param deliveryTargetAchieved the deliveryTargetAchieved to set
	 */
	public void setDeliveryTargetAchieved(int deliveryTargetAchieved) {
		this.deliveryTargetAchieved = deliveryTargetAchieved;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	private void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

    public List<Login> getSupervisorList() {
        return supervisorList;
    }

    public void setSupervisorList(List<Login> supervisorList) {
        this.supervisorList = supervisorList;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    /* START
     * DATE : 03/05/2017
     * REASON : Added to set & get campaign requirements */
    public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public int getCostPerLead() {
		return costPerLead;
	}

	public void setCostPerLead(int costPerLead) {
		this.costPerLead = costPerLead;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}
	/* END */

	/* START
	* DATE : 28/06/2017
	* REASON : Added for partner supervisor */
	public boolean isReadOnlyCampaign() {
		return readOnlyCampaign;
	}

	public void setReadOnlyCampaign(boolean readOnlyCampaign) {
		this.readOnlyCampaign = readOnlyCampaign;
	}
	/* END */

	/* START HIDE PHONE
     * DATE : 03/11/2017
     * REASON : Added to hide phone number on agent screen from third party center */
	public boolean isHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}
	/* END HIDE PHONE */

	/* START PARTNER SUPERVISOR
     * DATE : 03/11/2017
     * REASON : Added to give show/hide access to supervisor screen for partner supervisor */

	/*START QA REQUIRED
	  DATE : 18/12/2017*/
    // Added to check if leads goes to qa or not for a campaign
	public boolean getQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(boolean qaRequired) {
		this.qaRequired = qaRequired;
	}

	public List<String> getPartnerSupervisors() {
		return partnerSupervisors;
	}

	public void setPartnerSupervisors(List<String> partnerSupervisors) {
		this.partnerSupervisors = partnerSupervisors;
	}
	/* END PARTNER SUPERVISOR */

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean isDisableAMDOnQueued() {
		return disableAMDOnQueued;
	}

	public void setDisableAMDOnQueued(boolean disableAMDOnQueued) {
		this.disableAMDOnQueued = disableAMDOnQueued;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public boolean isUseSlice() {
		return useSlice;
	}

	public void setUseSlice(boolean useSlice) {
		this.useSlice = useSlice;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public boolean isRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(boolean retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public float getRetrySpeedFactor() {
		return retrySpeedFactor;
	}

	public void setRetrySpeedFactor(float retrySpeedFactor) {
		this.retrySpeedFactor = retrySpeedFactor;
	}

	public int getDailyCallMaxRetries() {
		return dailyCallMaxRetries;
	}

	public void setDailyCallMaxRetries(int dailyCallMaxRetries) {
		this.dailyCallMaxRetries = dailyCallMaxRetries;
	}

	public int getCallMaxRetries() {
		return callMaxRetries;
	}

	public void setCallMaxRetries(int callMaxRetries) {
		this.callMaxRetries = callMaxRetries;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public boolean isLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public int getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}

	public void setDuplicateLeadsDuration(int duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}

	public int getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}

	public void setDuplicateCompanyDuration(int duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}

	public int getLeadsPerCompany() {
		return leadsPerCompany;
	}

	public void setLeadsPerCompany(int leadsPerCompany) {
		this.leadsPerCompany = leadsPerCompany;
	}

	public List<String> getCampaignGroupIds() {
		return campaignGroupIds;
	}

	public void setCampaignGroupIds(List<String> campaignGroupIds) {
		this.campaignGroupIds = campaignGroupIds;
	}

	public DialerMode getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public boolean isMultiProspectCalling() {
		return multiProspectCalling;
	}

	public void setMultiProspectCalling(boolean multiProspectCalling) {
		this.multiProspectCalling = multiProspectCalling;
	}

	public boolean isHideEmail() {
		return hideEmail;
	}

	public void setHideEmail(boolean hideEmail) {
		this.hideEmail = hideEmail;
	}

	public boolean isNotConference() {
		return isNotConference;
	}

	public void setNotConference(boolean isNotConference) {
		this.isNotConference = isNotConference;
	}

	public List<String> getAdminSupervisorIds() {
		return adminSupervisorIds;
	}

	public void setAdminSupervisorIds(List<String> adminSupervisorIds) {
		this.adminSupervisorIds = adminSupervisorIds;
	}

	public boolean isABM() {
		return isABM;
	}

	public void setABM(boolean isABM) {
		this.isABM = isABM;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public String getRecordProspect() {
		return recordProspect;
	}

	public void setRecordProspect(String recordProspect) {
		this.recordProspect = recordProspect;
	}

	public boolean isLeadIQ() {
		return isLeadIQ;
	}

	public void setLeadIQ(boolean isLeadIQ) {
		this.isLeadIQ = isLeadIQ;
	}

	public boolean isNetworkCheck() {
		return networkCheck;
	}

	public void setNetworkCheck(boolean networkCheck) {
		this.networkCheck = networkCheck;
	}

	public boolean isBandwidthCheck() {
		return bandwidthCheck;
	}

	public void setBandwidthCheck(boolean bandwidthCheck) {
		this.bandwidthCheck = bandwidthCheck;
	}

	public int getTelnyxCallTimeLimitSeconds() {
		return telnyxCallTimeLimitSeconds;
	}

	public void setTelnyxCallTimeLimitSeconds(int telnyxCallTimeLimitSeconds) {
		this.telnyxCallTimeLimitSeconds = telnyxCallTimeLimitSeconds;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}
	
	public long getPlivoAMDTime() {
		return plivoAMDTime;
	}

	public void setPlivoAMDTime(long plivoAMDTime) {
		this.plivoAMDTime = plivoAMDTime;
	}

	public List<String> getMultiCurrentAssetId() {
		return multiCurrentAssetId;
	}

	public void setMultiCurrentAssetId(List<String> multiCurrentAssetId) {
		this.multiCurrentAssetId = multiCurrentAssetId;
	}

	public boolean isCallAbandon() {
		return callAbandon;
	}

	public void setCallAbandon(boolean callAbandon) {
		this.callAbandon = callAbandon;
	}
	public String getAutoModeCallProvider() {
		return autoModeCallProvider;
	}

	public boolean isEnableProspectCaching() {
		return enableProspectCaching;
	}

	public void setEnableProspectCaching(boolean enableProspectCaching) {
		this.enableProspectCaching = enableProspectCaching;
	}

	/*public boolean isRemoveMobilePhones() {
		return removeMobilePhones;
	}

	public void setRemoveMobilePhones(boolean removeMobilePhones) {
		this.removeMobilePhones = removeMobilePhones;
	}*/

	public void setAutoModeCallProvider(String autoModeCallProvider) {
		this.autoModeCallProvider = autoModeCallProvider;
	}

	public String getManualModeCallProvider() {
		return manualModeCallProvider;
	}

	public void setManualModeCallProvider(String manualModeCallProvider) {
		this.manualModeCallProvider = manualModeCallProvider;
	}
	
	public boolean isHideNonSuccessPII() {
		return hideNonSuccessPII;
	}

	public void setHideNonSuccessPII(boolean hideNonSuccessPII) {
		this.hideNonSuccessPII = hideNonSuccessPII;
	}

	public boolean isEnableCustomFields() {
		return enableCustomFields;
	}

	public void setEnableCustomFields(boolean enableCustomFields) {
		this.enableCustomFields = enableCustomFields;
	}

	public boolean isAutoIVR() {
		return autoIVR;
	}

	public void setAutoIVR(boolean autoIVR) {
		this.autoIVR = autoIVR;
	}
	
	public boolean isEnableInternetCheck() {
		return enableInternetCheck;
	}

	public void setEnableInternetCheck(boolean enableInternetCheck) {
		this.enableInternetCheck = enableInternetCheck;
	}

	public boolean isEnableStateCodeDropdown() {
		return enableStateCodeDropdown;
	}

	public void setEnableStateCodeDropdown(boolean enableStateCodeDropdown) {
		this.enableStateCodeDropdown = enableStateCodeDropdown;
	}

	public boolean isCallableVanished() {
		return callableVanished;
	}

	public void setCallableVanished(boolean callableVanished) {
		this.callableVanished = callableVanished;
	}
	
	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	public boolean isAutoCallbackStatusMapCleanup() {
		return autoCallbackStatusMapCleanup;
	}

	public void setAutoCallbackStatusMapCleanup(boolean autoCallbackStatusMapCleanup) {
		this.autoCallbackStatusMapCleanup = autoCallbackStatusMapCleanup;
	}
	
	public boolean isEnableCallbackFeature() {
		return enableCallbackFeature;
	}

	public void setEnableCallbackFeature(boolean enableCallbackFeature) {
		this.enableCallbackFeature = enableCallbackFeature;
	}

}