package com.xtaas.web.dto;

import java.util.List;

public class RefreshCampaignsCacheDTO {

	private List<String> campaignIds;

	private String environment;
	
	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	@Override
	public String toString() {
		return "RefreshCampaignsCacheDTO [campaignIds=" + campaignIds + ", environment=" + environment + "]";
	}
}
