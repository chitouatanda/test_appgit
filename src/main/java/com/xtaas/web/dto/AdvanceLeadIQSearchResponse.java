package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"data"
})

public class AdvanceLeadIQSearchResponse {
  @JsonProperty("data")
  private AdvanceLeadIQData data;


  @JsonProperty("data")
  public AdvanceLeadIQData getData() {
    return data;
  }

  @JsonProperty("data")
  public void setData(AdvanceLeadIQData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "AdvanceLeadIQSearchResponse{" +
            "data=" + data +
            '}';
  }

}