package com.xtaas.web.dto;

public class TelnyxCallWebhookDTO {

  private TelnyxCallWebhookDTOData data;

    private TelnyxCallWebhookDTOMeta meta;

    public TelnyxCallWebhookDTOData getData ()
    {
        return data;
    }

    public void setData (TelnyxCallWebhookDTOData data)
    {
        this.data = data;
    }

    public TelnyxCallWebhookDTOMeta getMeta ()
    {
        return meta;
    }

    public void setMeta (TelnyxCallWebhookDTOMeta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }

}