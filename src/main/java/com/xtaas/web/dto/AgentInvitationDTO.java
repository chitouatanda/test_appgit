package com.xtaas.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;

public class AgentInvitationDTO {
	@JsonProperty
	@NotNull
	private String agentId;

	@JsonProperty
	private double maximumPayment;

	@JsonProperty
	@NotNull
	private int minimumHours;
	
	@JsonProperty
	private Date invitationDate;
	
	public AgentInvitationDTO() {
	}
	
	public AgentInvitationDTO(CampaignAgentInvitation agentInviation) {
		this.agentId = agentInviation.getAgentId();
		this.maximumPayment = agentInviation.getMaximumPayment();
		this.minimumHours = agentInviation.getMinimumHours();
		this.invitationDate = agentInviation.getInvitationDate();
	}
	
	public CampaignAgentInvitation toCampaignAgentInvitation() {
		return new CampaignAgentInvitation(agentId, maximumPayment, minimumHours);
	}
}
