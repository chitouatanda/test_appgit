package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentQaAnswer;

public class IndirectProspectsDTO {
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	@JsonProperty
	private String title;
	@JsonProperty
	private String department;
	@JsonProperty
	private String company;
	@JsonProperty
	private String email;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private String qualityBucket;
	@JsonProperty
	private int qualityBucketSort;
	@JsonProperty
	private String multiProspect;
	@JsonProperty
	private String status;
	@JsonProperty
	private List<AgentQaAnswer> customFields;

	public IndirectProspectsDTO() {
	}

	public IndirectProspectsDTO(String firstName, String lastName, String title, String department, String company,
			String email, String campaignId, String prospectCallId, String qualityBucket, int qualityBucketSort,
			String multiProspect, String status, List<AgentQaAnswer> customFields ) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.department = department;
		this.company = company;
		this.email = email;
		this.campaignId = campaignId;
		this.prospectCallId = prospectCallId;
		this.qualityBucket = qualityBucket;
		this.qualityBucketSort = qualityBucketSort;
		this.multiProspect = multiProspect;
		this.status = status;
		this.customFields = customFields;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getQualityBucket() {
		return qualityBucket;
	}

	public void setQualityBucket(String qualityBucket) {
		this.qualityBucket = qualityBucket;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	public String getMultiProspect() {
		return multiProspect;
	}

	public void setMultiProspect(String multiProspect) {
		this.multiProspect = multiProspect;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public List<AgentQaAnswer> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<AgentQaAnswer> customFields) {
		this.customFields = customFields;
	}
	
	

}
