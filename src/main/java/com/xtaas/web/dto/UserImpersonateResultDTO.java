package com.xtaas.web.dto;

import java.util.List;

public class UserImpersonateResultDTO {

	private long userCount;
	private List<UserImpersonateDTO> userList;

	protected UserImpersonateResultDTO() {
	}

	public UserImpersonateResultDTO(long userCount, List<UserImpersonateDTO> userList) {
		this.userCount = userCount;
		this.userList = userList;
	}

	public long getUserCount() {
		return userCount;
	}

	public void setUserCount(long userCount) {
		this.userCount = userCount;
	}

	public List<UserImpersonateDTO> getUserList() {
		return userList;
	}

	public void setUserList(List<UserImpersonateDTO> userList) {
		this.userList = userList;
	}

	@Override
	public String toString() {
		return "UserImpersonateListDTO [userCount=" + userCount + ", userList=" + userList + "]";
	}

}
