package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ES_SimilarCompaniesDTO {

	@JsonProperty("domain")
	private String domain;
	@JsonProperty("ecid")
	private int ecid;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getEcid() {
		return ecid;
	}

	public void setEcid(int ecid) {
		this.ecid = ecid;
	}

}
