package com.xtaas.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.ScheduleRecurrence;
import com.xtaas.domain.valueobject.WorkSchedule;

public class WorkScheduleDTO {

	@JsonProperty
	private String scheduleId;
	
	@JsonProperty
	private Date scheduleDate;
	
	@JsonProperty
	private int startHour;
	
	@JsonProperty
	private int endHour;
	
	@JsonProperty
	private ScheduleRecurrenceDTO recurrence;

	public WorkScheduleDTO() {
		
	}

	public WorkScheduleDTO(WorkSchedule workSchedule) {
		this.scheduleId = workSchedule.getScheduleId();
		this.scheduleDate = workSchedule.getScheduleDate();
		this.startHour = workSchedule.getStartHour();
		this.endHour = workSchedule.getEndHour();
		if (workSchedule.getRecurrence() != null) {
			this.recurrence = new ScheduleRecurrenceDTO(workSchedule.getRecurrence());
		}
	}
	
	public WorkSchedule toWorkSchedule() {
		if (recurrence != null && recurrence.getStartDate() != null && recurrence.getEndDate() != null) {
			return new WorkSchedule(scheduleDate, startHour, endHour, recurrence.toScheduleRecurrence());
		} else {
			return new WorkSchedule(scheduleDate, startHour, endHour, null);
		}
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public int getStartHour() {
		return startHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public ScheduleRecurrenceDTO getRecurrence() {
		return recurrence;
	}
}
