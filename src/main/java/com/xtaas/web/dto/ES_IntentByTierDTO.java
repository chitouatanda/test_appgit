package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ES_IntentByTierDTO {

	@JsonProperty("Tier 1")
	List<Object> Tier1;
	@JsonProperty("Tier 2")
	List<Object> Tier2;
	@JsonProperty("Tier 3")
	List<Object> Tier3;
	
}
