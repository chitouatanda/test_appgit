package com.xtaas.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DemandShoreDNCDTO {
	
	@JsonProperty
	private String dnc_call_time;
	@JsonProperty
	private String company_board_line;
	@JsonProperty
	private String contact_id;
	@JsonProperty
	private String email_id;
	@JsonProperty
	private String first_name;
	@JsonProperty
	private String last_name;
	@JsonProperty
	private String flc;
	@JsonProperty
	private String company;
	@JsonProperty
	private String status;
	@JsonProperty
	private String campaign_id;
	@JsonProperty
	private String list_name;
	@JsonProperty
	private String domain;
	@JsonProperty
	private String partnerId;
	
	
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDnc_call_time() {
		return dnc_call_time;
	}
	public void setDnc_call_time(String dnc_call_time) {
		this.dnc_call_time = dnc_call_time;
	}
	public String getCompany_board_line() {
		return company_board_line;
	}
	public void setCompany_board_line(String company_board_line) {
		this.company_board_line = company_board_line;
	}
	public String getContact_id() {
		return contact_id;
	}
	public void setContact_id(String contact_id) {
		this.contact_id = contact_id;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getFlc() {
		return flc;
	}
	public void setFlc(String flc) {
		this.flc = flc;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}
	public String getList_name() {
		return list_name;
	}
	public void setList_name(String list_name) {
		this.list_name = list_name;
	}

}
