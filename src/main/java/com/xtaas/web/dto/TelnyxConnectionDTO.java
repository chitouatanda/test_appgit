package com.xtaas.web.dto;

public class TelnyxConnectionDTO {
    private TelnyxConnectionData data;

    public TelnyxConnectionData getData () {
        return data;
    }

    public void setData (TelnyxConnectionData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [data = "+data+"]";
    }
}