package com.xtaas.web.dto;
import com.opencsv.bean.CsvBindByName;

public class AdhocTranscribeJobIO {
	@CsvBindByName(column = "_id")
	private String _id;
	@CsvBindByName(column = "prospectCallId")
	private String prospectCallId;
	@CsvBindByName(column = "fromNumber")
	private String fromNumber;
	@CsvBindByName(column = "toNumber")
	private String toNumber;
	@CsvBindByName(column = "callSid")
	private String callSid;
	@CsvBindByName(column = "messageAudioUrl")
	private String messageAudioUrl;
	@CsvBindByName(column = "awsAudioUrl")
	private String awsAudioUrl;
	@CsvBindByName(column = "callParams__RecordingDuration")
	private String callParams__RecordingDuration;
	@CsvBindByName(column = "transcript")
	private String transcript;

	public AdhocTranscribeJobIO() {
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getFromNumber() {
		return fromNumber;
	}

	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}

	public String getToNumber() {
		return toNumber;
	}

	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public String getMessageAudioUrl() {
		return messageAudioUrl;
	}

	public void setMessageAudioUrl(String messageAudioUrl) {
		this.messageAudioUrl = messageAudioUrl;
	}

	public String getAwsAudioUrl() {
		return awsAudioUrl;
	}

	public void setAwsAudioUrl(String awsAudioUrl) {
		this.awsAudioUrl = awsAudioUrl;
	}

	public String getCallParams__RecordingDuration() {
		return callParams__RecordingDuration;
	}

	public void setCallParams__RecordingDuration(String callParams__RecordingDuration) {
		this.callParams__RecordingDuration = callParams__RecordingDuration;
	}

	public String getTranscript() {
		return transcript;
	}

	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	@Override
	public String toString() {
		return "AdhocTranscribeJobIO [_id=" + _id + ", prospectCallId=" + prospectCallId + ", fromNumber=" + fromNumber
				+ ", toNumber=" + toNumber + ", callSid=" + callSid + ", messageAudioUrl=" + messageAudioUrl
				+ ", awsAudioUrl=" + awsAudioUrl + ", callParams__RecordingDuration=" + callParams__RecordingDuration
				+ ", transcript=" + transcript + "]";
	}

}

