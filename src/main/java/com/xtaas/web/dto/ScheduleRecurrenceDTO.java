package com.xtaas.web.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.DayOfWeek;
import com.xtaas.domain.valueobject.RecurrenceTypes;
import com.xtaas.domain.valueobject.ScheduleRecurrence;

public class ScheduleRecurrenceDTO {

	@JsonProperty
	private Date startDate;
	
	@JsonProperty
	private Date endDate;
	
	@JsonProperty
	private RecurrenceTypes type;
	
	@JsonProperty
	private List<DayOfWeek> days;

	public ScheduleRecurrenceDTO() {
	
	}

	public ScheduleRecurrenceDTO(ScheduleRecurrence scheduleRecurrence) {
		this.startDate = scheduleRecurrence.getStartDate();
		this.endDate = scheduleRecurrence.getEndDate();
		this.type = scheduleRecurrence.getType();
		this.days = scheduleRecurrence.getDays();
	}
	
	public ScheduleRecurrence toScheduleRecurrence() {
		return new ScheduleRecurrence(startDate, endDate, type, days);
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public RecurrenceTypes getType() {
		return type;
	}

	public List<DayOfWeek> getDays() {
		return days;
	}
	
}
