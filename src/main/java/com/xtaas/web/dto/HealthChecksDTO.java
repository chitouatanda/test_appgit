package com.xtaas.web.dto;

import java.util.List;

public class HealthChecksDTO {

	private String campaignId;

	private List<HealthChecksCriteriaDTO> criteria;

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public List<HealthChecksCriteriaDTO> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<HealthChecksCriteriaDTO> criteria) {
		this.criteria = criteria;
	}

	@Override
	public String toString() {
		return "HealthChecksDTO [campaignId=" + campaignId + ", criteria=" + criteria + "]";
	}

}
