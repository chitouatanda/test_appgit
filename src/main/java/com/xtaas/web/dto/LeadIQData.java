package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"searchPeople"
})
public class LeadIQData {

@JsonProperty("searchPeople")
private LeadIQSearchPeople searchPeople;

@JsonProperty("errors")
private LeadIQSearchPeople errors;


@JsonProperty("searchPeople")
public LeadIQSearchPeople getSearchPeople() {
return searchPeople;
}

@JsonProperty("searchPeople")
public void setSearchPeople(LeadIQSearchPeople searchPeople) {
this.searchPeople = searchPeople;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("searchPeople", searchPeople).toString();
}

}