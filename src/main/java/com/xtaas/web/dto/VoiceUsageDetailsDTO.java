package com.xtaas.web.dto;

import java.math.BigDecimal;

public class VoiceUsageDetailsDTO {
	private String category;
	private String usage;
	private BigDecimal price;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal bigDecimal) {
		this.price = bigDecimal;
	}

}
