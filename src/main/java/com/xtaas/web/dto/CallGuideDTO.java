package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.domain.valueobject.CallSection;

public class CallGuideDTO {
	@JsonProperty
	private CallSectionDTO confirmSection;
	@JsonProperty
	private CallSectionDTO questionSection;
	@JsonProperty
	private CallSectionDTO consentSection;
	@JsonProperty
	private CallSectionDTO noteSection;
	/**
	 * 
	 */
	public CallGuideDTO() {
	
	}
	/**
	 * @param confirmSection
	 * @param questionSection
	 * @param consentSection
	 * @param noteSection
	 */
	public CallGuideDTO(CallGuide callGuide) {
		CallSection confirmSection = callGuide.getConfirmSection();
		if (confirmSection != null) {
			CallSectionDTO confirmSectionDTO = new CallSectionDTO();
			confirmSectionDTO.setScript(confirmSection.getScript());
			confirmSectionDTO.setRebuttals(confirmSection.getRebuttals());
			this.confirmSection = confirmSectionDTO;
		}
		CallSection questionSection = callGuide.getQuestionSection();
		if (questionSection != null) {
			CallSectionDTO questionSectionDTO = new CallSectionDTO();
			questionSectionDTO.setScript(questionSection.getScript());
			questionSectionDTO.setRebuttals(questionSection.getRebuttals());
			this.questionSection = questionSectionDTO;
		}
		CallSection consentSection = callGuide.getConsentSection();
		if (consentSection != null) {
			CallSectionDTO consentSectionDTO = new CallSectionDTO();
			consentSectionDTO.setScript(consentSection.getScript());
			consentSectionDTO.setRebuttals(consentSection.getRebuttals());
			this.consentSection = consentSectionDTO;
		}
		CallSection noteSection = callGuide.getNoteSection();
		if (noteSection != null) {
			CallSectionDTO noteSectionDTO = new CallSectionDTO();
			noteSectionDTO.setScript(noteSection.getScript());
			noteSectionDTO.setRebuttals(noteSection.getRebuttals());
			this.noteSection = noteSectionDTO;
		}
	}
	
	public CallGuide toCallGuide() {
		return new CallGuide(confirmSection == null ? null : confirmSection.toCallSection(),questionSection == null ? null : questionSection.toCallSection(), 
							consentSection == null ? null : consentSection.toCallSection(), noteSection == null ? null :  noteSection.toCallSection());
	}
	/**
	 * @return the confirmSection
	 */
	public CallSectionDTO getConfirmSection() {
		return confirmSection;
	}
	/**
	 * @return the questionSection
	 */
	public CallSectionDTO getQuestionSection() {
		return questionSection;
	}
	/**
	 * @return the consentSection
	 */
	public CallSectionDTO getConsentSection() {
		return consentSection;
	}
	/**
	 * @return the noteSection
	 */
	public CallSectionDTO getNoteSection() {
		return noteSection;
	}
	
}
