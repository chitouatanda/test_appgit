package com.xtaas.web.dto;

public class DailySummaryPCIDTO {
	
	private String campaignName;
	
	private int recordsAttempted = 0;
	
	private int answerMachineRecords = 0;
	
	private int recordsContacted = 0;
	
	private int recordsConnected = 0;
	
	private int abandonedRecords = 0;
	
	private int successRecords = 0;
	
	private String timeSpan;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public int getRecordsAttempted() {
		return recordsAttempted;
	}

	public void setRecordsAttempted(int recordsAttempted) {
		this.recordsAttempted = recordsAttempted;
	}

	public int getAnswerMachineRecords() {
		return answerMachineRecords;
	}

	public void setAnswerMachineRecords(int answerMachineRecords) {
		this.answerMachineRecords = answerMachineRecords;
	}

	public int getRecordsContacted() {
		return recordsContacted;
	}

	public void setRecordsContacted(int recordsContacted) {
		this.recordsContacted = recordsContacted;
	}

	public int getRecordsConnected() {
		return recordsConnected;
	}

	public void setRecordsConnected(int recordsConnected) {
		this.recordsConnected = recordsConnected;
	}

	public int getAbandonedRecords() {
		return abandonedRecords;
	}

	public void setAbandonedRecords(int abandonedRecords) {
		this.abandonedRecords = abandonedRecords;
	}

	public int getSuccessRecords() {
		return successRecords;
	}

	public void setSuccessRecords(int successRecords) {
		this.successRecords = successRecords;
	}

	public String getTimeSpan() {
		return timeSpan;
	}

	public void setTimeSpan(String timeSpan) {
		this.timeSpan = timeSpan;
	}
	
	
}
