package com.xtaas.web.dto;

import org.springframework.data.domain.Sort.Direction;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignNameSearchDTO {
	@JsonProperty
	private String campaignName;
	@JsonProperty
	private String campaignManagerId;
	@JsonProperty
	private Direction direction = Direction.DESC;
	@JsonProperty
	private int page;
	@JsonProperty
	private int size = 10;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
