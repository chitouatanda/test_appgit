package com.xtaas.web.dto;

public class CountryWiseDetailsDTO {

	private String countryCode;
	private String countryName;
	private String timeZone;
	private int isdCode;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	@Override
	public String toString() {
		return "CountryWiseDetailsDTO [countryCode=" + countryCode + ", countryName=" + countryName + ", isdCode="
				+ isdCode + ", timeZone=" + timeZone + "]";
	}
	
}
