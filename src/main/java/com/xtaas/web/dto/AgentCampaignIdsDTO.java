package com.xtaas.web.dto;

import java.util.List;

public class AgentCampaignIdsDTO {

	private List<String> agentIds;
	private List<String> campaignIds;

	public List<String> getAgentIds() {
		return agentIds;
	}

	public void setAgentIds(List<String> agentIds) {
		this.agentIds = agentIds;
	}

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

}
