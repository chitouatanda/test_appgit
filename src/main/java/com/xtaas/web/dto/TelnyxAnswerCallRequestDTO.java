package com.xtaas.web.dto;

public class TelnyxAnswerCallRequestDTO {

  private String client_state;

  private String command_id;

  public TelnyxAnswerCallRequestDTO() {
    this.client_state = "";
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }

  public String getCommand_id() {
    return command_id;
  }

  public void setCommand_id(String command_id) {
    this.command_id = command_id;
  }

}