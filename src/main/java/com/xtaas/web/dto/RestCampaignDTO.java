package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.web.dto.CampaignDTO;

public class RestCampaignDTO {
	
	public String sfdcCampaignId;
	public String campaignId;
	public String name;
	public String type;
	public String status;
	private Date startDate;
	private Date endDate;
	private int deliveryTarget;
	private int dailyCap;
	private List<String> assets;
	private String campaignRequirements;
	private int leadsPerCompany;
	private int duplicateLeadsDuration;
	private int duplicateCompanyDuration;
	private String campaignManagerId;
	private String organizationId;
	private List<RestClientQuestions> questions;
	private String teamSupervisorId;
	private List<String> partnerSupervisors;
	
	public RestCampaignDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public RestCampaignDTO(CampaignDTO campaignDTO) {
		this.setSfdcCampaignId(campaignDTO.getSfdcCampaignId());
		this.setCampaignId(campaignDTO.getId());
		this.setName(campaignDTO.getName());
		this.setType(campaignDTO.getType().name());
		this.setStatus(campaignDTO.getStatus().name());
		this.setStartDate(campaignDTO.getStartDate());
		this.setEndDate(campaignDTO.getEndDate());
		this.setDeliveryTarget(campaignDTO.getDeliveryTarget());
		this.setDailyCap(campaignDTO.getDailyCap());
		List<String> assets = new ArrayList<String>();
		if (campaignDTO.getAssets() != null) {
			assets = campaignDTO.getAssets().stream().map(asset -> asset.getUrl()).collect(Collectors.toList());
		}
		this.setAssetsFromCampaign(assets);
		this.setCampaignRequirements(campaignDTO.getCampaignRequirements());
		this.setLeadsPerCompany(campaignDTO.getLeadsPerCompany());
		this.setDuplicateLeadsDuration(campaignDTO.getDuplicateLeadsDuration());
		this.setDuplicateCompanyDuration(campaignDTO.getDuplicateCompanyDuration());
		this.setCampaignManagerId(campaignDTO.getCampaignManagerId());
		this.setOrganizationId(campaignDTO.getOrganizationId());
	}

	
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDeliveryTarget() {
		return deliveryTarget;
	}

	public void setDeliveryTarget(int deliveryTarget) {
		this.deliveryTarget = deliveryTarget;
	}

	public int getDailyCap() {
		return dailyCap;
	}

	public void setDailyCap(int dailyCap) {
		this.dailyCap = dailyCap;
	}

	public List<String> getAssets() {
		return assets;
	}

	public void setAssets(List<String> assets) {
		this.assets = assets;
	}

	public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}

	public int getLeadsPerCompany() {
		return leadsPerCompany;
	}

	public void setLeadsPerCompany(int leadsPerCompany) {
		this.leadsPerCompany = leadsPerCompany;
	}

	public int getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}

	public void setDuplicateLeadsDuration(int duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}

	public int getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}

	public void setDuplicateCompanyDuration(int duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}

	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}

	public void setAssetsFromCampaign(List<String> assets) {
		List<String> assetList = new ArrayList<>();
		if (assets != null) {
			for (String assetDTO : assets) {
				assetList.add(assetDTO);
			}
		}
		this.assets = assetList;
	}

	public List<RestClientQuestions> getQuestions() {
		return questions;
	}

	public void setQuestions(List<RestClientQuestions> questions) {
		this.questions = questions;
	}

	public String getTeamSupervisorId() {
		return teamSupervisorId;
	}

	public void setTeamSupervisorId(String teamSupervisorId) {
		this.teamSupervisorId = teamSupervisorId;
	}

	public List<String> getPartnerSupervisors() {
		return partnerSupervisors;
	}

	public void setPartnerSupervisors(List<String> partnerSupervisors) {
		this.partnerSupervisors = partnerSupervisors;
	}

	
}
