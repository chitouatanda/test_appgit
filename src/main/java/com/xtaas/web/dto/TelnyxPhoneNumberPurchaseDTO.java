package com.xtaas.web.dto;

import java.util.List;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

public class TelnyxPhoneNumberPurchaseDTO {

	@NotEmpty(message = "countryISO can not be null or empty")
	private String countryISO;

	@NotEmpty(message = "Can not be null or empty")
	private List<String> features;

	@NotEmpty(message = "Can not be null or empty")
	private String connectionId;

	@NotEmpty(message = "Can not be null or empty")
	private String billingAccountId;

//	@NotEmpty(message = "Can not be null or empty")
	private List<String> states;

	@NotEmpty(message = "Can not be null or empty")
	private String timezone;

	@NotEmpty(message = "Can not be null or empty")
	private String partnerId;

	@Min(value = 1)
	private int limit;

	@Min(value = 1)
	private int isdCode;

	@Min(value = 1)
	private int pool;
	
	private String region;
	
	private boolean addNumberInOtherCountries;

	public String getCountryISO() {
		return countryISO;
	}

	public void setCountryISO(String countryISO) {
		this.countryISO = countryISO;
	}

	public List<String> getFeatures() {
		return features;
	}

	public void setFeatures(List<String> features) {
		this.features = features;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(String billingAccountId) {
		this.billingAccountId = billingAccountId;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean isAddNumberInOtherCountries() {
		return addNumberInOtherCountries;
	}

	public void setAddNumberInOtherCountries(boolean addNumberInOtherCountries) {
		this.addNumberInOtherCountries = addNumberInOtherCountries;
	}

}