package com.xtaas.web.dto;

public class TelnyxConnectionRequestDTOOutbound {

  private String outbound_voice_profile_id;

  public String getOutbound_voice_profile_id() {
    return outbound_voice_profile_id;
  }

  public void setOutbound_voice_profile_id(String outbound_voice_profile_id) {
    this.outbound_voice_profile_id = outbound_voice_profile_id;
  }

}