package com.xtaas.web.dto;

public class ZoomInfoEverstringKeyValueDTO {

	private String zoominfo;
	private String everstring;

	public String getZoominfo() {
		return zoominfo;
	}

	public void setZoominfo(String zoominfo) {
		this.zoominfo = zoominfo;
	}

	public String getEverstring() {
		return everstring;
	}

	public void setEverstring(String everstring) {
		this.everstring = everstring;
	}

	@Override
	public String toString() {
		return "ZoomInfoEverstringKeyValueDTO [zoominfo=" + zoominfo + ", everstring=" + everstring + "]";
	}

}
