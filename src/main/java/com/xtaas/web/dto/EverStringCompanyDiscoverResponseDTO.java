package com.xtaas.web.dto;

import java.io.Serializable;
import java.util.List;

public class EverStringCompanyDiscoverResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<EverStringCompanyDiscoverDTO> companies;

	public List<EverStringCompanyDiscoverDTO> getCompanies() {
		return companies;
	}

	public void setCompanies(List<EverStringCompanyDiscoverDTO> companies) {
		this.companies = companies;
	}

}
