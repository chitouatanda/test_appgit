package com.xtaas.web.dto;

public class DncSearchDTO {

	private String firstName;
	private String lastName;
	private String phone;
	private String partnerId;
	private String companyName;
	private String companyDomain;
	private String companyPhone;
	private boolean isUSorCanada;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getCompanyDomain() {
		return companyDomain;
	}

	public void setCompanyDomain(String companyDomain) {
		this.companyDomain = companyDomain;
	}

	public boolean isUSorCanada() {
		return isUSorCanada;
	}

	public void setIsUSorCanada(boolean isUSorCanada) {
		this.isUSorCanada = isUSorCanada;
	}

}
