package com.xtaas.web.dto;

import org.springframework.data.domain.Sort.Direction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserImpersonateSearchDTO {

	@JsonProperty
	private String searchType;
	@JsonProperty
	private String searchText;
	@JsonProperty
	private String sort = "id";
	@JsonProperty
	private Direction direction = Direction.ASC;
	@JsonProperty
	private int page;
	@JsonProperty
	private int size = 80;

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "UserImpersonateSearchDTO [searchType=" + searchType + ", searchText=" + searchText + ", sort=" + sort
				+ ", direction=" + direction + ", page=" + page + ", size=" + size + "]";
	}

}
