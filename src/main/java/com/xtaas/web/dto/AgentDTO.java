package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Agent.agentSkill;
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentDTO {
	
	@JsonProperty
	private String id;
	
	@JsonProperty
	private String partnerId;
	
	@JsonProperty
	private String firstName;
	
	@JsonProperty
	private String lastName;
	
	@JsonProperty
	private String username;
	
	@JsonProperty
	private String overview;
	
	@JsonProperty
	private AddressDTO address;
	
	@JsonProperty
	private List<String> languages;
	
	@JsonProperty
	private List<String> domains;
	
	@JsonProperty
	private double perHourPrice;

	@JsonProperty
	private agentSkill agentType;
	
	@JsonProperty
	private String supervisorId;	

	public AgentDTO() {
		
	}

	public AgentDTO(Agent agent) {
		this.id = agent.getId();
		this.partnerId = agent.getPartnerId();
		this.firstName = agent.getFirstName();
		this.lastName = agent.getLastName();
		this.username = agent.getUsername();
		this.overview = agent.getOverview();
		if (agent.getAddress() != null) {
			this.address = new AddressDTO(agent.getAddress());
		}
		this.languages = agent.getLanguages();
		this.domains = agent.getDomains();
		this.perHourPrice = agent.getPerHourPrice();
		this.agentType = agent.getAgentType();
		this.supervisorId = agent.getSupervisorId();
	}
	
	public String getId() {
		return id;
	}
	
	public String getPartnerId() {
		return partnerId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	public String getName() {
		String name = this.firstName;
    	if (this.lastName != null) {
    		name = name + " " + this.lastName;
    	}
    	return name;
	}

	public String getUsername() {
		return username;
	}

	public String getOverview() {
		return overview;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public List<String> getDomains() {
		return domains;
	}

	public double getPerHourPrice() {
		return perHourPrice;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	public void setDomains(List<String> domains) {
		this.domains = domains;
	}

	public void setPerHourPrice(double perHourPrice) {
		this.perHourPrice = perHourPrice;
	}

	public agentSkill getAgentType() {
		return agentType;
	}

	public void setAgentType(agentSkill agentType) {
		this.agentType = agentType;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

}
