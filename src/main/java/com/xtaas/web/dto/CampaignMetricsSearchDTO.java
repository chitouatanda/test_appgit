package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sagar
 * 
 */
public class CampaignMetricsSearchDTO {
	@JsonProperty
	private CampaignMetricsSearchClauses clause;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String fromDate;
	@JsonProperty
	private String toDate;
	@JsonProperty
	private String timeZone;
	@JsonProperty
    private int noOfDays;
	/**
	 * @return the clause
	 */
	public CampaignMetricsSearchClauses getClause() {
		return clause;
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}
    public int getNoOfDays() {
        return noOfDays;
    }
}
