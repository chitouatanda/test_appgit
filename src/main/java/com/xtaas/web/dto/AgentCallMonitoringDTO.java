package com.xtaas.web.dto;

public class AgentCallMonitoringDTO {

	private String agentId;
	private String agentName;
	private String prospectCallId;
	private String callSid;
	private String organization;
	private String campaignId;
	private String campaignName;
	private String dialerMode;
	private String agentType;
	private float agentWaitTime;
	private float telcoDuration;
	private float dispositionTime;
	private float prospectHandleTime;
	private String dispositionStatus;
	private String subStatus;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(String dialerMode) {
		this.dialerMode = dialerMode;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public float getAgentWaitTime() {
		return agentWaitTime;
	}

	public void setAgentWaitTime(float agentWaitTime) {
		this.agentWaitTime = agentWaitTime;
	}

	public float getTelcoDuration() {
		return telcoDuration;
	}

	public void setTelcoDuration(float telcoDuration) {
		this.telcoDuration = telcoDuration;
	}

	public float getDispositionTime() {
		return dispositionTime;
	}

	public void setDispositionTime(float dispositionTime) {
		this.dispositionTime = dispositionTime;
	}

	public float getProspectHandleTime() {
		return prospectHandleTime;
	}

	public void setProspectHandleTime(float prospectHandleTime) {
		this.prospectHandleTime = prospectHandleTime;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	@Override
	public String toString() {
		return "AgentCallMonitoringDTO [agentId=" + agentId + ", agentName=" + agentName + ", prospectCallId="
				+ prospectCallId + ", callSid=" + callSid + ", organization=" + organization + ", campaignId="
				+ campaignId + ", campaignName=" + campaignName + ", dialerMode=" + dialerMode + ", agentType="
				+ agentType + ", agentWaitTime=" + agentWaitTime + ", telcoDuration=" + telcoDuration
				+ ", dispositionTime=" + dispositionTime + ", prospectHandleTime=" + prospectHandleTime
				+ ", dispositionStatus=" + dispositionStatus + ", subStatus=" + subStatus + "]";
	}

}
