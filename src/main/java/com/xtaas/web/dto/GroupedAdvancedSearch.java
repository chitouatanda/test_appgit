package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupedAdvancedSearch {

    @JsonProperty("companies")
    private List<Companies> companies = null;

    @JsonProperty("totalCompanies")
    private Long totalCompanies;


    public List<Companies> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Companies> companies) {
        this.companies = companies;
    }

    public Long getTotalCompanies() {
        return totalCompanies;
    }

    public void setTotalCompanies(Long totalCompanies) {
        this.totalCompanies = totalCompanies;
    }

    @Override
    public String toString() {
        return "GroupedAdvancedSearch{" +
                "companies=" + companies +
                ", totalCompanies=" + totalCompanies +
                '}';
    }
}
