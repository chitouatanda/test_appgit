package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.Contact;

public class ContactSearchResultDTO {
	@JsonProperty
	private int count;
	
	@JsonProperty
	private List<ContactDTO> contacts;
	
	public ContactSearchResultDTO(ContactSearchResult contactSearchResult) {
		this.count = contactSearchResult.getCount();
		this.contacts = new ArrayList<ContactDTO>();
		
		for (Contact contact : contactSearchResult.getContacts()) {
			contacts.add(new ContactDTO(contact));
		}
	}
}
