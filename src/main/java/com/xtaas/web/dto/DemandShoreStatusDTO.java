package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DemandShoreStatusDTO {

	@JsonProperty
	private String sfdcCampaignId;

	@JsonProperty
	private String organizationName;

	@JsonProperty
	private String phone;

	@JsonProperty
	private String contactId;

	@JsonProperty
	private String callStartDateandTime;

	@JsonProperty
	private String email;

	@JsonProperty
	private String firstName;

	@JsonProperty
	private String lastName;

	@JsonProperty
	private String source;// (For Manual mode - the value will be RESEARCH)

	@JsonProperty
	private String partnerID;// Partner ID/Organization ID

	@JsonProperty
	private String subStatus;

	@JsonProperty
	private String title;

	@JsonProperty
	private String department;

	@JsonProperty
	private String domain;

	@JsonProperty
	private String stateCode;

	@JsonProperty
	private String country;

	@JsonProperty
	private String directPhone;

	@JsonProperty
	private String dispositionStatus;

	@JsonProperty
	private String callDuration;

	@JsonProperty
	private String agentID;

	@JsonProperty
	private String recordingURL;

	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getCallStartDateandTime() {
		return callStartDateandTime;
	}

	public void setCallStartDateandTime(String callStartDateandTime) {
		this.callStartDateandTime = callStartDateandTime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(String directPhone) {
		this.directPhone = directPhone;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public String getRecordingURL() {
		return recordingURL;
	}

	public void setRecordingURL(String recordingURL) {
		this.recordingURL = recordingURL;
	}

}
