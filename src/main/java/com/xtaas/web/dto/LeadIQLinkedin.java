package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"linkedinId",
"linkedinUrl"
})
public class LeadIQLinkedin {

@JsonProperty("linkedinId")
private String linkedinId;
@JsonProperty("linkedinUrl")
private String linkedinUrl;

@JsonProperty("linkedinId")
public String getLinkedinId() {
return linkedinId;
}

@JsonProperty("linkedinId")
public void setLinkedinId(String linkedinId) {
this.linkedinId = linkedinId;
}

@JsonProperty("linkedinUrl")
public String getLinkedinUrl() {
return linkedinUrl;
}

@JsonProperty("linkedinUrl")
public void setLinkedinUrl(String linkedinUrl) {
this.linkedinUrl = linkedinUrl;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("linkedinId", linkedinId).append("linkedinUrl", linkedinUrl).toString();
}

}