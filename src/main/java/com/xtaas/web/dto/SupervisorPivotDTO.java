package com.xtaas.web.dto;

import java.util.List;
import com.xtaas.domain.valueobject.Pivot;

public class SupervisorPivotDTO {

	public int totalRecords;
	private List<Pivot> department;
	private List<Pivot> managementLevel;
	private List<Pivot> employeeCount;
	private List<Pivot> industry;
	private List<Pivot> revenue;
	private List<Pivot> geography;
	private List<Pivot> title;
	private List<Pivot> country;
	private List<Pivot> company;
	private List<Pivot> source;
	private List<Pivot> industryList;
	private List<Pivot> stateCode;
	private List<Pivot> isEu;
	private List<Pivot> sicList;
	private List<Pivot> naicsList;
	private List<Pivot> titleCategory;

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<Pivot> getDepartment() {
		return department;
	}

	public void setDepartment(List<Pivot> department) {
		this.department = department;
	}

	public List<Pivot> getManagementLevel() {
		return managementLevel;
	}

	public void setManagementLevel(List<Pivot> managementLevel) {
		this.managementLevel = managementLevel;
	}

	public List<Pivot> getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(List<Pivot> employeeCount) {
		this.employeeCount = employeeCount;
	}

	public List<Pivot> getIndustry() {
		return industry;
	}

	public void setIndustry(List<Pivot> industry) {
		this.industry = industry;
	}

	public List<Pivot> getRevenue() {
		return revenue;
	}

	public void setRevenue(List<Pivot> revenue) {
		this.revenue = revenue;
	}

	public List<Pivot> getGeography() {
		return geography;
	}

	public void setGeography(List<Pivot> geography) {
		this.geography = geography;
	}

	public List<Pivot> getTitle() {
		return title;
	}

	public void setTitle(List<Pivot> title) {
		this.title = title;
	}

	public List<Pivot> getCountry() {
		return country;
	}

	public void setCountry(List<Pivot> country) {
		this.country = country;
	}

	public List<Pivot> getCompany() {
		return company;
	}

	public void setCompany(List<Pivot> company) {
		this.company = company;
	}

	public List<Pivot> getSource() {
		return source;
	}

	public void setSource(List<Pivot> source) {
		this.source = source;
	}

	public List<Pivot> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<Pivot> industryList) {
		this.industryList = industryList;
	}

	
	public List<Pivot> getStateCode() {
		return stateCode;
	}

	public void setStateCode(List<Pivot> stateCode) {
		this.stateCode = stateCode;
	}

	public List<Pivot> getIsEu() {
		return isEu;
	}

	public void setIsEu(List<Pivot> isEu) {
		this.isEu = isEu;
	}

	public List<Pivot> getSicList() {
		return sicList;
	}

	public void setSicList(List<Pivot> sicList) {
		this.sicList = sicList;
	}

	public List<Pivot> getNaicsList() {
		return naicsList;
	}

	public void setNaicsList(List<Pivot> naicsList) {
		this.naicsList = naicsList;
	}

	public List<Pivot> getTitleCategory() {
		return titleCategory;
	}

	public void setTitleCategory(List<Pivot> titleCategory) {
		this.titleCategory = titleCategory;
	}

	@Override
	public String toString() {
		return "SupervisorPivotDTO{" +
				"totalRecords=" + totalRecords +
				", department=" + department +
				", managementLevel=" + managementLevel +
				", employeeCount=" + employeeCount +
				", industry=" + industry +
				", revenue=" + revenue +
				", geography=" + geography +
				", title=" + title +
				", country=" + country +
				", company=" + company +
				", source=" + source +
				", industryList=" + industryList +
				", stateCode=" + stateCode +
				", isEu=" + isEu +
				", sicList=" + sicList +
				", naicsList=" + naicsList +
				", titleCategory=" + titleCategory +
				'}';
	}
}
