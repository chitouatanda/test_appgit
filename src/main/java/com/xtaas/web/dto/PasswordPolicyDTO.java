package com.xtaas.web.dto;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordPolicyDTO {

	@JsonProperty
	private int noOfAttempt;

	@JsonProperty
	private boolean userLock;

	@JsonProperty
	private Date lastPasswordUpdated;

	@JsonProperty
	private boolean activateUser;

	@JsonProperty
	private boolean passwordPolicy;

	@JsonProperty
	private boolean multiFactorRequired;

	@JsonProperty
	private boolean activateMFAUser;

	@JsonProperty
	private boolean enforeMFA;

	@JsonProperty
	private boolean allowMFAUser;

	public int getNoOfAttempt() {
		return noOfAttempt;
	}

	public void setNoOfAttempt(int noOfAttempt) {
		this.noOfAttempt = noOfAttempt;
	}

	public boolean isUserLock() {
		return userLock;
	}

	public void setUserLock(boolean userLock) {
		this.userLock = userLock;
	}

	public Date getLastPasswordUpdated() {
		return lastPasswordUpdated;
	}

	public void setLastPasswordUpdated(Date lastPasswordUpdated) {
		this.lastPasswordUpdated = lastPasswordUpdated;
	}

	public boolean isActivateUser() {
		return activateUser;
	}

	public void setActivateUser(boolean activateUser) {
		this.activateUser = activateUser;
	}

	public boolean isPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(boolean passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public boolean isMultiFactorRequired() {
		return multiFactorRequired;
	}

	public void setMultiFactorRequired(boolean multiFactorRequired) {
		this.multiFactorRequired = multiFactorRequired;
	}

	public boolean isActivateMFAUser() {
		return activateMFAUser;
	}

	public void setActivateMFAUser(boolean activateMFAUser) {
		this.activateMFAUser = activateMFAUser;
	}

	public boolean isEnforeMFA() {
		return enforeMFA;
	}

	public void setEnforeMFA(boolean enforeMFA) {
		this.enforeMFA = enforeMFA;
	}

	public boolean isAllowMFAUser() {
		return allowMFAUser;
	}

	public void setAllowMFAUser(boolean allowMFAUser) {
		this.allowMFAUser = allowMFAUser;
	}
}
