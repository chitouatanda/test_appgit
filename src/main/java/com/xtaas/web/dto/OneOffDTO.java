package com.xtaas.web.dto;

import java.util.List;
import java.util.Map;

public class OneOffDTO {

	public List<String> campaignIdList;
	public String primaryCampaignId;
	public String fromDate;
	public String toDate;
	public int totalPurchaseCount;
	public List<String> emailIds;
	public String hashtagField;
	public String  industryClassification;
	public String industryCode;
	public String minEmp;
	public String maxEmp;
	public String minRev;
	public String maxRev;
	public String zip;
	public String country;
	public String primaryIndustriesOnly;
	public String  contactRequirements;
	public List<String> prospectCallIds;
	public String assetId;
	public List<String> deleteGroupCampaignIds;// this is for cleaning prospect call interactions from group of camapigns
	public boolean useTempPCI;
	public List<String> dispositionStatus;
	public Map<String,Integer> dataSourceMap;
	public UpdateCampaignSettings updateCampaignSettings;
	public UpdateProspectInfo updateProspectInfo;
	public String campaignName;// to get prospect
	public String organizationId;// to get prospect
	public String firstName;// to get prospect
	public String lastName;// to get prospect
	public List<String> moveProspects;
	//public List<String> getBackProspects;
	public String dataBuyId;
	public String toBuyStatus;
	public int noOfProspectsToBeCopiedFromCache;
	public String updateType;
	private String prospectUploadEnabled;
	public int upperThreshold;
	public int lowerThreshold;
	public String supervisorStatsType;
	public String supervisorStatsStatus;

	
	public String getDataBuyId() {
		return dataBuyId;
	}

	public void setDataBuyId(String dataBuyId) {
		this.dataBuyId = dataBuyId;
	}

	public String getToBuyStatus() {
		return toBuyStatus;
	}

	public void setToBuyStatus(String toBuyStatus) {
		this.toBuyStatus = toBuyStatus;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public List<String> getMoveProspects() {
		return moveProspects;
	}

	public void setMoveProspects(List<String> moveProspects) {
		this.moveProspects = moveProspects;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UpdateProspectInfo getUpdateProspectInfo() {
		return updateProspectInfo;
	}

	public void setUpdateProspectInfo(UpdateProspectInfo updateProspectInfo) {
		this.updateProspectInfo = updateProspectInfo;
	}

	public List<String> getDeleteGroupCampaignIds() {
		return deleteGroupCampaignIds;
	}

	public void setDeleteGroupCampaignIds(List<String> deleteGroupCampaignIds) {
		this.deleteGroupCampaignIds = deleteGroupCampaignIds;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public List<String> getProspectCallIds() {
		return prospectCallIds;
	}

	public void setProspectCallIds(List<String> prospectCallIds) {
		this.prospectCallIds = prospectCallIds;
	}

	public String getContactRequirements() {
		return contactRequirements;
	}

	public void setContactRequirements(String contactRequirements) {
		this.contactRequirements = contactRequirements;
	}

	public String getPrimaryIndustriesOnly() {
		return primaryIndustriesOnly;
	}

	public void setPrimaryIndustriesOnly(String primaryIndustriesOnly) {
		this.primaryIndustriesOnly = primaryIndustriesOnly;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHashtagField() {
		return hashtagField;
	}

	public void setHashtagField(String hashtagField) {
		this.hashtagField = hashtagField;
	}

	public String getIndustryClassification() {
		return industryClassification;
	}

	public void setIndustryClassification(String industryClassification) {
		this.industryClassification = industryClassification;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMinEmp() {
		return minEmp;
	}

	public void setMinEmp(String minEmp) {
		this.minEmp = minEmp;
	}

	public String getMaxEmp() {
		return maxEmp;
	}

	public void setMaxEmp(String maxEmp) {
		this.maxEmp = maxEmp;
	}

	public String getMinRev() {
		return minRev;
	}

	public void setMinRev(String minRev) {
		this.minRev = minRev;
	}

	public String getMaxRev() {
		return maxRev;
	}

	public void setMaxRev(String maxRev) {
		this.maxRev = maxRev;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<String> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(List<String> emailIds) {
		this.emailIds = emailIds;
	}

	public int getTotalPurchaseCount() {
		return totalPurchaseCount;
	}

	public void setTotalPurchaseCount(int totalPurchaseCount) {
		this.totalPurchaseCount = totalPurchaseCount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getPrimaryCampaignId() {
		return primaryCampaignId;
	}

	public void setPrimaryCampaignId(String primaryCampaignId) {
		this.primaryCampaignId = primaryCampaignId;
	}

	public List<String> getCampaignIdList() {
		return campaignIdList;
	}

	public void setCampaignIdList(List<String> campaignIdList) {
		this.campaignIdList = campaignIdList;
	}

	public boolean isUseTempPCI() {
		return useTempPCI;
	}

	public void setUseTempPCI(boolean useTempPCI) {
		this.useTempPCI = useTempPCI;
	}

	public List<String> getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(List<String> dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public Map<String, Integer> getDataSourceMap() {
		return dataSourceMap;
	}

	public void setDataSourceMap(Map<String, Integer> dataSourceMap) {
		this.dataSourceMap = dataSourceMap;
	}

	public UpdateCampaignSettings getUpdateCampaignSettings() {
		return updateCampaignSettings;
	}

	public void setUpdateCampaignSettings(UpdateCampaignSettings updateCampaignSettings) {
		this.updateCampaignSettings = updateCampaignSettings;
	}

	public int getNoOfProspectsToBeCopiedFromCache() {
		return noOfProspectsToBeCopiedFromCache;
	}

	public void setNoOfProspectsToBeCopiedFromCache(int noOfProspectsToBeCopiedFromCache) {
		this.noOfProspectsToBeCopiedFromCache = noOfProspectsToBeCopiedFromCache;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}
	
	public String getProspectUploadEnabled() {
		return prospectUploadEnabled;
	}

	public void setProspectUploadEnabled(String prospectUploadEnabled) {
		this.prospectUploadEnabled = prospectUploadEnabled;
	}

	public int getUpperThreshold() {
		return upperThreshold;
	}

	public void setUpperThreshold(int upperThreshold) {
		this.upperThreshold = upperThreshold;
	}

	public int getLowerThreshold() {
		return lowerThreshold;
	}

	public void setLowerThreshold(int lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}

	public String getSupervisorStatsType() {
		return supervisorStatsType;
	}

	public void setSupervisorStatsType(String supervisorStatsType) {
		this.supervisorStatsType = supervisorStatsType;
	}

	public String getSupervisorStatsStatus() {
		return supervisorStatsStatus;
	}

	public void setSupervisorStatsStatus(String supervisorStatsStatus) {
		this.supervisorStatsStatus = supervisorStatsStatus;
	}
	
	
}
