package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CallSection;
import com.xtaas.domain.valueobject.Rebuttal;

public class CallSectionDTO {
	@JsonProperty
	private String script;
	@JsonProperty
	private List<Rebuttal> rebuttals;
	/**
	 * 
	 */
	public CallSectionDTO() {
	
	}
	
	
	public CallSection toCallSection() {
		CallSection callSection = new CallSection();
		callSection.setScript(script);
		callSection.setRebuttals(rebuttals);
		return callSection;
	}
	
	/**
	 * @return the script
	 */
	public String getScript() {
		return script;
	}
	/**
	 * @return the rebuttals
	 */
	public List<Rebuttal> getRebuttals() {
		return rebuttals;
	}

	/**
	 * @param script the script to set
	 */
	public void setScript(String script) {
		this.script = script;
	}

	/**
	 * @param rebuttals the rebuttals to set
	 */
	public void setRebuttals(List<Rebuttal> rebuttals) {
		this.rebuttals = rebuttals;
	}
	
	
}
