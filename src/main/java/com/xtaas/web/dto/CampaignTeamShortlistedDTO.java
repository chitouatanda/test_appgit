package com.xtaas.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CampaignTeamShortlisted;

public class CampaignTeamShortlistedDTO {
	@JsonProperty
	@NotNull
	private String teamId;

	@JsonProperty
	private Date shortlistedDate;
	
	public CampaignTeamShortlistedDTO() {
		
	}
	
	public CampaignTeamShortlistedDTO(CampaignTeamShortlisted campaignTeamShortlisted) {
		this.teamId = campaignTeamShortlisted.getTeamId();
		this.shortlistedDate = campaignTeamShortlisted.getShortlistDate();
	}
	
	public CampaignTeamShortlisted toCampaignTeamShortlisted() {
		return new CampaignTeamShortlisted(teamId);
	}
}
