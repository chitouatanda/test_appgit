package com.xtaas.web.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;


public class RestCampaignContactDTO {

	
	private String contactId;
	
	private String sfdcCampaignId;
	
	private String firstName;
	
	private String lastName;
	
	private String title;
	
	private String department;

	private String email;	
	
	private String phone;
	
	private String organizationName;
	
	private List<String> industryList;

	private boolean directPhone;

	private String managementLevel;
	
	private String domain;
	
	private double minRevenue;
	
	private double maxRevenue;
	
	private double minEmployeeCount;
	
	private double maxEmployeeCount;
	
	private String addressLine1;
	
	private String addressLine2;
	
	private String city;
	
	private String stateCode;
	
	private String country;
	
	private String postalCode;
	
	private String companyValidationLink;
	
	private String titleValidationLink;
	
	private String source;
	
	private String extension;
	

	public String getExtension() {
		return extension;
	}


	public void setExtension(String extension) {
		this.extension = extension;
	}


	public RestCampaignContactDTO() {
		
	}


	public String getContactId() {
		return contactId;
	}


	public void setContactId(String contactId) {
		this.contactId = contactId;
	}


	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}


	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getOrganizationName() {
		return organizationName;
	}


	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}


	public List<String> getIndustryList() {
		return industryList;
	}


	public void setIndustryList(List<String> industryList) {
		this.industryList = industryList;
	}


	public boolean isDirectPhone() {
		return directPhone;
	}


	public void setDirectPhone(boolean directPhone) {
		this.directPhone = directPhone;
	}


	public String getManagementLevel() {
		return managementLevel;
	}


	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}


	public String getDomain() {
		return domain;
	}


	public void setDomain(String domain) {
		this.domain = domain;
	}


	public double getMinRevenue() {
		return minRevenue;
	}


	public void setMinRevenue(double minRevenue) {
		this.minRevenue = minRevenue;
	}


	public double getMaxRevenue() {
		return maxRevenue;
	}


	public void setMaxRevenue(double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}


	public double getMinEmployeeCount() {
		return minEmployeeCount;
	}


	public void setMinEmployeeCount(double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}


	public double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}


	public void setMaxEmployeeCount(double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}


	public String getAddressLine1() {
		return addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getStateCode() {
		return stateCode;
	}


	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getCompanyValidationLink() {
		return companyValidationLink;
	}


	public void setCompanyValidationLink(String companyValidationLink) {
		this.companyValidationLink = companyValidationLink;
	}


	public String getTitleValidationLink() {
		return titleValidationLink;
	}


	public void setTitleValidationLink(String titleValidationLink) {
		this.titleValidationLink = titleValidationLink;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}

	
}
