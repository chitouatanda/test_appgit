package com.xtaas.web.dto;

public class TelnyxStartRecordingRequestDTO {

  private String channels;

  private String format;

  private String client_state;

  private String command_id;

  private boolean play_beep;

  public TelnyxStartRecordingRequestDTO() {
    this.client_state = "";
  }

  public String getChannels() {
    return channels;
  }

  public void setChannels(String channels) {
    this.channels = channels;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }

  public String getCommand_id() {
    return command_id;
  }

  public void setCommand_id(String command_id) {
    this.command_id = command_id;
  }

  public boolean isPlay_beep() {
    return play_beep;
  }

  public void setPlay_beep(boolean play_beep) {
    this.play_beep = play_beep;
  }

}