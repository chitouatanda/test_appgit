package com.xtaas.web.dto;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Address;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.Phone;

public class OrganizationContactDTO {
	@JsonProperty
	private String type;
	
	@JsonProperty
	private String status;
	
	@JsonProperty
	private String salutation;
	
	@JsonProperty
	@NotNull
	private String firstName;
	
	@JsonProperty
	@NotNull
	private String lastName;
	private String title;
	private Address address;
	
	@JsonProperty
	@NotNull
	private ArrayList<PhoneDTO> phones;
	
	@JsonProperty
	@NotNull
	@Email
	private String email;
	
	public OrganizationContactDTO() {
		
	}

	public OrganizationContactDTO(Contact contact) {
		this.type = contact.getType();
		this.status = contact.getStatus();
		this.salutation = contact.getSalutation();
		this.firstName = contact.getFirstName();
		this.lastName = contact.getLastName();
		this.title = contact.getTitle();
		this.address = contact.getAddress();
		
		if (contact.getPhones() != null) {
			this.phones = new ArrayList<PhoneDTO>();
			for (Phone phone : contact.getPhones()) {
				this.phones.add(new PhoneDTO(phone));
			}
		}
		this.email = contact.getEmail();
	}
	
	public Contact toContact() {
		ArrayList<Phone> phoneList = new ArrayList<Phone>();
		for (PhoneDTO phoneDTO : this.phones) {
				phoneList.add(phoneDTO.toPhone());
			}
		return new Contact(type, status, salutation, firstName, lastName, title, address, phoneList, email);
	}

	public String getType() {
		return type;
	}

	public String getStatus() {
		return status;
	}

	public String getSalutation() {
		return salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getTitle() {
		return title;
	}

	public Address getAddress() {
		return address;
	}

	public ArrayList<PhoneDTO> getPhones() {
		return phones;
	}

	public String getEmail() {
		return email;
	}

}
