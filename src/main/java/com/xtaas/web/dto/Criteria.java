package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

public class Criteria {

	private List<String> revenue = new ArrayList<>(0);

	private List<String> employeeSize = new ArrayList<>(0);

	private List<String> industry = new ArrayList<>(0);

	public List<String> getRevenue() {
		return revenue;
	}

	public void setRevenue(List<String> revenue) {
		this.revenue = revenue;
	}

	public List<String> getEmployeeSize() {
		return employeeSize;
	}

	public void setEmployeeSize(List<String> employeeSize) {
		this.employeeSize = employeeSize;
	}

	public List<String> getIndustry() {
		return industry;
	}

	public void setIndustry(List<String> industry) {
		this.industry = industry;
	}

}
