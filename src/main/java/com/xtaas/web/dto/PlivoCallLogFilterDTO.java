package com.xtaas.web.dto;

import java.util.Date;
import java.util.List;

public class PlivoCallLogFilterDTO {

	private String toNumber;
	private String fromNumber;
	private String status;
	private String callSid;
	private Date startDate;
	private Date endDate;
	private String direction;
	private String profile;
	private List<String> toEmails;
	private String fromEmail;

	public String getToNumber() {
		return toNumber;
	}

	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}

	public String getFromNumber() {
		return fromNumber;
	}

	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public List<String> getToEmails() {
		return toEmails;
	}

	public void setToEmails(List<String> toEmails) {
		this.toEmails = toEmails;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	@Override
	public String toString() {
		return "PlivoCallLogFilterDTO [toNumber=" + toNumber + ", fromNumber=" + fromNumber + ", status=" + status
				+ ", callSid=" + callSid + ", startDate=" + startDate + ", endDate=" + endDate + ", direction="
				+ direction + ", profile=" + profile + ", toEmails=" + toEmails + ", fromEmail=" + fromEmail + "]";
	}

}
