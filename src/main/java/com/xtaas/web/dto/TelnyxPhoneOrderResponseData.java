package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"connection_id",
"status",
"customer_reference",
"phone_numbers",
"phone_numbers_count",
"created_at",
"id",
"requirements_met",
"updated_at",
"messaging_profile_id",
"record_type"
})

public class TelnyxPhoneOrderResponseData {

@JsonProperty("connection_id")
private String connectionId;
@JsonProperty("status")
private String status;
@JsonProperty("customer_reference")
private Object customerReference;
@JsonProperty("phone_numbers")
private List<TelnyxPhoneOrderResponsePhoneNumber> phoneNumbers = null;
@JsonProperty("phone_numbers_count")
private Integer phoneNumbersCount;
@JsonProperty("created_at")
private String createdAt;
@JsonProperty("id")
private String id;
@JsonProperty("requirements_met")
private Boolean requirementsMet;
@JsonProperty("updated_at")
private String updatedAt;
@JsonProperty("messaging_profile_id")
private Object messagingProfileId;
@JsonProperty("record_type")
private String recordType;

@JsonProperty("connection_id")
public String getConnectionId() {
return connectionId;
}

@JsonProperty("connection_id")
public void setConnectionId(String connectionId) {
this.connectionId = connectionId;
}

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonProperty("customer_reference")
public Object getCustomerReference() {
return customerReference;
}

@JsonProperty("customer_reference")
public void setCustomerReference(Object customerReference) {
this.customerReference = customerReference;
}

@JsonProperty("phone_numbers")
public List<TelnyxPhoneOrderResponsePhoneNumber> getPhoneNumbers() {
return phoneNumbers;
}

@JsonProperty("phone_numbers")
public void setPhoneNumbers(List<TelnyxPhoneOrderResponsePhoneNumber> phoneNumbers) {
this.phoneNumbers = phoneNumbers;
}

@JsonProperty("phone_numbers_count")
public Integer getPhoneNumbersCount() {
return phoneNumbersCount;
}

@JsonProperty("phone_numbers_count")
public void setPhoneNumbersCount(Integer phoneNumbersCount) {
this.phoneNumbersCount = phoneNumbersCount;
}

@JsonProperty("created_at")
public String getCreatedAt() {
return createdAt;
}

@JsonProperty("created_at")
public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("requirements_met")
public Boolean getRequirementsMet() {
return requirementsMet;
}

@JsonProperty("requirements_met")
public void setRequirementsMet(Boolean requirementsMet) {
this.requirementsMet = requirementsMet;
}

@JsonProperty("updated_at")
public String getUpdatedAt() {
return updatedAt;
}

@JsonProperty("updated_at")
public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

@JsonProperty("messaging_profile_id")
public Object getMessagingProfileId() {
return messagingProfileId;
}

@JsonProperty("messaging_profile_id")
public void setMessagingProfileId(Object messagingProfileId) {
this.messagingProfileId = messagingProfileId;
}

@JsonProperty("record_type")
public String getRecordType() {
return recordType;
}

@JsonProperty("record_type")
public void setRecordType(String recordType) {
this.recordType = recordType;
}

}