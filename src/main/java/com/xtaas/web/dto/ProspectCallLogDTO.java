package com.xtaas.web.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;

public class ProspectCallLogDTO {
	@JsonProperty
	private ProspectCallDTO prospectCall;
	
	@JsonProperty
	private ProspectCallStatus status;
	
	@JsonProperty
	private QaFeedbackDTO qaFeedback;
    
	private Map<String, String> companyAddress;


	public ProspectCallLogDTO(ProspectCallLog prospectCallLog) {
		this.prospectCall = new ProspectCallDTO(prospectCallLog.getProspectCall());
		this.status = prospectCallLog.getStatus();
//		this.qaFeedback = new QaFeedbackDTO(prospectCallLog.getQaFeedback());
	}
	
	public Map<String, String> getcompanyAddress() {
		return companyAddress;
	}

	public void setcompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	public ProspectCallLogDTO() {
		
	}

	/**
	 * @return the prospectCall
	 */
	public ProspectCallDTO getProspectCall() {
		return prospectCall;
	}

	/**
	 * @param prospectCall the prospectCall to set
	 */
	public void setProspectCall(ProspectCallDTO prospectCall) {
		this.prospectCall = prospectCall;
	}

	/**
	 * @return the status
	 */
	public ProspectCallStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ProspectCallStatus status) {
		this.status = status;
	}

	/**
	 * @return the qaFeedback
	 */
	public QaFeedbackDTO getQaFeedback() {
		return qaFeedback;
	}

	/**
	 * @param qaFeedback the qaFeedback to set
	 */
	public void setQaFeedback(QaFeedbackDTO qaFeedback) {
		this.qaFeedback = qaFeedback;
	}
	
	
}
