package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"column",
"line"
})
public class LeadIQErrorLocation {

@JsonProperty("column")
private Integer column;
@JsonProperty("line")
private Integer line;

@JsonProperty("column")
public Integer getColumn() {
return column;
}

@JsonProperty("column")
public void setColumn(Integer column) {
this.column = column;
}

@JsonProperty("line")
public Integer getLine() {
return line;
}

@JsonProperty("line")
public void setLine(Integer line) {
this.line = line;
}

}