package com.xtaas.web.dto;

public class TelnyxCreateConferenceDTO {

  private String call_control_id;

  private boolean start_conference_on_create;

  private String name;

  private String client_state;

  private String beep_enabled;

  public TelnyxCreateConferenceDTO() {
    this.client_state = "";
    this.beep_enabled = "never";
  }
  
  public String getCall_control_id ()
  {
      return call_control_id;
  }

  public void setCall_control_id (String call_control_id)
  {
      this.call_control_id = call_control_id;
  }

  public boolean getStart_conference_on_create ()
  {
      return start_conference_on_create;
  }

  public void setStart_conference_on_create (boolean start_conference_on_create)
  {
      this.start_conference_on_create = start_conference_on_create;
  }

  public String getName ()
  {
      return name;
  }

  public void setName (String name)
  {
      this.name = name;
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }
  
  public String getBeep_enabled() {
    return beep_enabled;
  }

  public void setBeep_enabled(String beep_enabled) {
    this.beep_enabled = beep_enabled;
  }
}