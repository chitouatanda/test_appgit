package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.CRMSystem;
import com.xtaas.db.entity.Contact;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.entity.Organization.ClientType;
import com.xtaas.db.entity.Organization.OrganizationLevel;
import com.xtaas.domain.valueobject.OrganizationTypes;

public class OrganizationDTO {
	@JsonProperty
	@NotNull
	private String name;
	
	@JsonProperty
	private boolean agency;
	
	@JsonProperty
	private String description;
	
	@JsonProperty
	private String companySize;
	
	@JsonProperty
	private String url;
	
	@JsonProperty
	private String organizationEmail;
	
	@JsonProperty
	private String organizationLoginId;
	
	@JsonProperty
	private String accountOwnerLoginId;
	
	@JsonProperty
	private String status;
	
	@JsonProperty
	private String vertical;
	
	@JsonProperty
	private int marketCap;
	
	@JsonProperty
	private CRMSystem crm;
	
	@JsonProperty
	private OrganizationTypes organizationType;
	
	@JsonProperty
	@Valid
	private ArrayList<OrganizationContactDTO> contacts;
	
	@JsonProperty
	private OrganizationLevel organizationLevel;
	
	@JsonProperty
	private List<String> partnerSupervisors;
	
	@JsonProperty
	private List<String> reportingEmail;
	
	@JsonProperty
	private ClientType clientType;
	
	@JsonProperty
	private String plivoAuthId;
	
	@JsonProperty
	private String plivoAuthToken;
	
	@JsonProperty
	private String plivoAppId;
	
	@JsonProperty
	private String telnyxConnectionId;

	public OrganizationDTO() {
		
	}

	public OrganizationDTO(Organization organization) {
		this.name = organization.getName();
		this.agency = organization.isAgency();
		this.description = organization.getDescription();
		this.companySize = organization.getCompanySize();
		this.url = organization.getUrl();
		this.organizationLoginId = organization.getOrganizationLoginId();
		this.accountOwnerLoginId = organization.getAccountOwnerLoginId();
		this.status = organization.getStatus();
		this.vertical = organization.getVertical();
		this.marketCap = organization.getMarketCap();
		this.crm = organization.getCrm();
		if (organization.getContacts() != null) {
			this.contacts = new ArrayList<OrganizationContactDTO>();
			for (Contact contact : organization.getContacts()) {
				this.contacts.add(new OrganizationContactDTO(contact));
			}
		}
		this.organizationType = organization.getOrganizationType();
		this.organizationLevel = organization.getOrganizationLevel();
		this.partnerSupervisors = organization.getPartnerSupervisors();
		this.organizationEmail = organization.getOrganizationEmail();
		this.reportingEmail = organization.getReportingEmail();
		this.clientType = organization.getClientType();
		this.telnyxConnectionId = organization.getTelnyxConnectionId();
	}
	
	public Organization toOrganization() {
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		for (OrganizationContactDTO contact : this.getContacts()) {
			contacts.add(contact.toContact());
		}
		return new Organization(name, agency, description, companySize, url, organizationLoginId, accountOwnerLoginId, vertical, marketCap, crm, contacts, organizationLevel, partnerSupervisors);
	}

	public String getName() {
		return name;
	}

	public boolean isAgency() {
		return agency;
	}

	public String getDescription() {
		return description;
	}

	public String getCompanySize() {
		return companySize;
	}

	public String getUrl() {
		return url;
	}

	public String getOrganizationLoginId() {
		return organizationLoginId;
	}

	public String getAccountOwnerLoginId() {
		return accountOwnerLoginId;
	}

	public String getStatus() {
		return status;
	}

	public String getVertical() {
		return vertical;
	}

	public int getMarketCap() {
		return marketCap;
	}

	public CRMSystem getCrm() {
		return crm;
	}

	public ArrayList<OrganizationContactDTO> getContacts() {
		return contacts;
	}

	public OrganizationTypes getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(OrganizationTypes organizationType) {
		this.organizationType = organizationType;
	}

	public OrganizationLevel getOrganizationLevel() {
		return organizationLevel;
	}

	public void setOrganizationLevel(OrganizationLevel organizationLevel) {
		this.organizationLevel = organizationLevel;
	}

	public String getOrganizationEmail() {
		return organizationEmail;
	}

	public void setOrganizationEmail(String organizationEmail) {
		this.organizationEmail = organizationEmail;
	}

	public List<String> getReportingEmail() {
		return reportingEmail;
	}

	public void setReportingEmail(List<String> reportingEmail) {
		this.reportingEmail = reportingEmail;
	}

	public ClientType getClientType() {
		return clientType;
	}

	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}

	public String getPlivoAuthId() {
		return plivoAuthId;
	}

	public void setPlivoAuthId(String plivoAuthId) {
		this.plivoAuthId = plivoAuthId;
	}

	public String getPlivoAuthToken() {
		return plivoAuthToken;
	}

	public void setPlivoAuthToken(String plivoAuthToken) {
		this.plivoAuthToken = plivoAuthToken;
	}

	public String getPlivoAppId() {
		return plivoAppId;
	}

	public void setPlivoAppId(String plivoAppId) {
		this.plivoAppId = plivoAppId;
	}

	public String getTelnyxConnectionId() {
		return telnyxConnectionId;
	}

	public void setTelnyxConnectionId(String telnyxConnectionId) {
		this.telnyxConnectionId = telnyxConnectionId;
	}
}
