package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.domain.valueobject.WorkSlot;

public class AgentCampaignDTO {

	@JsonProperty
	private String id;

	@JsonProperty
	@NotNull
	private String name;

	@JsonProperty
	@NotNull
	private CampaignTypes type; // Lead Verification, Lead Generation,...

	@JsonProperty
	private String vertical;

	@JsonProperty
	private String campaignRequirements;

	@JsonProperty
	private List<AssetDTO> assets;

	@SuppressWarnings("unused")
	@JsonProperty
	private String currentAssetId;

	@SuppressWarnings("unused")
	@JsonProperty
	private List<String> multiCurrentAssetId;

	@JsonProperty
	private CallGuide callGuide;

	@JsonProperty
	private LinkedHashMap<String, CRMAttribute> questionAttributeMap;

	@JsonProperty
	private DialerMode dialerMode;

	@JsonProperty
	private boolean tempAllocation;

	@JsonProperty
	private WorkSlot nextWorkSlot;

	@JsonProperty
	private String brand;

	@JsonProperty
	private ExpressionGroupDTO qualificationCriteria;

	@JsonProperty
	private boolean hidePhone; // Added to hide phone number on agent screen from third party center

	@JsonProperty
	private boolean hideEmail;

	@JsonProperty
	private boolean simpleDisposition;

	@JsonProperty
	private boolean isEncrypted;

	@JsonProperty
	private String hostingServer;

	@JsonProperty
	private String organizationId;

	@JsonProperty
	private boolean isNotConference;

	@JsonProperty
	private boolean isABM;

	@JsonProperty
	private boolean supervisorAgentStatusApprove;

	@JsonProperty
	private String callControlProvider;

	@JsonProperty
	private boolean isLeadIQ;

	@JsonProperty
	private boolean enableMachineAnsweredDetection;

	@JsonProperty
	private boolean networkCheck;

	@JsonProperty
	private boolean bandwidthCheck;

	@JsonProperty
	private boolean ringingIcon;

	@JsonProperty
	private boolean unHideSocialLink;

	@JsonProperty
	private boolean autoMachineHangup;
	
	@JsonProperty
	private boolean revealEmail;

	@JsonProperty
	private String autoModeCallProvider;
	
	@JsonProperty
	private String manualModeCallProvider;

	@JsonProperty
	private boolean autoGoldenPass;
	
	@JsonProperty
	private boolean liteMode;

	@JsonProperty
	private boolean enableCustomFields;

	@JsonProperty
	private boolean enableInternetCheck;

	@JsonProperty
	private boolean enableStateCodeDropdown;

	@JsonProperty
	private boolean enableCallbackFeature;
	
	public AgentCampaignDTO() {
		// default constructor
	}

	// public AgentCampaignDTO(Campaign campaign, HashMap<String, CRMAttribute>
	// orgCRMMapping) {
	public AgentCampaignDTO(Campaign campaign) {
		this.id = campaign.getId();
		this.name = campaign.getName();
		this.type = campaign.getType();
		if (campaign.getAssets() != null) {
			this.assets = new ArrayList<AssetDTO>();
			for (Asset asset : campaign.getAssets()) {
				this.assets.add(new AssetDTO(asset));
			}
		}
		if (campaign.getMultiCurrentAssetId() != null && campaign.getMultiCurrentAssetId().size() > 0) {
			this.multiCurrentAssetId = campaign.getMultiCurrentAssetId();
		}
		if (campaign.getCurrentAssetId() != null) {
			this.currentAssetId = campaign.getCurrentAssetId();
		}
		this.callGuide = campaign.getTeam().getCallGuide(); // set call guide

		LinkedHashSet<String> uniqueAttributeMap = new LinkedHashSet<String>();
		HashMap<String, ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		HashMap<String, String> questionAttribMap = new HashMap<String, String>();
		HashMap<String, String> sequenceNumberMap = new HashMap<String, String>();
		// set the questions attributes
		if (campaign.getQualificationClause() != null) {
			uniqueAttributeMap.addAll(campaign.getAgentQualificationCriteria().getUniqueAttributeMap());
			/*
			 * DATE:16/05/2017 REASON:fetching questions which has "agentValidationRequired"
			 * (Expression.java) flag true.
			 */
			questions.putAll(campaign.getAgentQualificationCriteria().getQuestions());
			questionAttribMap.putAll(campaign.getAgentQualificationCriteria().getQuestionSkins());
			sequenceNumberMap.putAll(campaign.getQualificationCriteria().getSequenceNumbers());
		}
		// add info attr as well
		if (campaign.getInformationCriteria() != null) {
			uniqueAttributeMap.addAll(campaign.getInformationCriteria());
		}
		// TODO hack to bypass crmmapping collection
		this.questionAttributeMap = new LinkedHashMap<String, CRMAttribute>();
		for (String attrName : uniqueAttributeMap) {
			// this.questionAttributeMap.put(attrName, orgCRMMapping.get(attrName));
			// this.questionAttributeMap.put(attrName, orgCRMMapping.get(attrName));
			// TODO below code should be removed after discussion, i am hacking this and
			// creating a virtual orgCRMMapping object
			String questionSkin = questionAttribMap.get(attrName);
			String sequenceNumber = sequenceNumberMap.get(attrName);
			ArrayList<PickListItem> answers = questions.get(questionSkin);
			CRMAttribute crmAttrib = new CRMAttribute();
			crmAttrib.setSystemName(attrName);
			crmAttrib.setCrmDataType("string");
			crmAttrib.setCrmName(attrName);
			// crmAttrib.setExtraAttributes("");
			crmAttrib.setLabel(attrName);
			crmAttrib.setLength(512);
			crmAttrib.setPickList(answers);
			// CRMAttributes
			// [crmName=IS_YOUR_COMPANY_EVALUATING_MARKETING_AUTOMATION_PLATFORM?,
			// systemName=IS_YOUR_COMPANY_EVALUATING_MARKETING_AUTOMATION_PLATFORM?,
			// crmDataType=string, systemDataType=string, length=512,
			// pickList=[com.xtaas.db.entity.PickListItem@7ea465a3,
			// com.xtaas.db.entity.PickListItem@5b0cf149,
			// com.xtaas.db.entity.PickListItem@2eb51e52,
			// com.xtaas.db.entity.PickListItem@74d9b172], extraAttributes=null]
			crmAttrib.setProspectBeanPath("Prospect.CustomAttributes['" + attrName + "']");
			crmAttrib.setQuestionSkin(questionSkin);
			crmAttrib.setSystemDataType("string");
			crmAttrib.setSequence(sequenceNumber);

			crmAttrib.setAttributeType("CUSTOM");

			if (attrName != null && (attrName.equalsIgnoreCase("CUSTOMFIELD_01")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_02")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_03")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_04")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_05")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_06")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_07")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_08")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_09")
					|| attrName.equalsIgnoreCase("CUSTOMFIELD_10"))) {
				/*if (campaign != null && campaign.getQualificationCriteria() != null
						&& campaign.getQualificationCriteria().getExpressions() != null) {
					crmAttrib.setValue(campaign.getCustomFieldAnswer(attrName, campaign.getQualificationCriteria().getExpressions()));	
				} else {
					crmAttrib.setValue("");
				}*/
				continue;
			}
			this.questionAttributeMap.put(attrName, crmAttrib);
		}

		// set the dialer mode
		this.dialerMode = campaign.getDialerMode();
		if (campaign.getBrand() == null || campaign.getBrand().isEmpty()) {
			this.brand = campaign.getOrganizationId();
		} else {
			this.brand = campaign.getBrand();
		}
		// set qualification criteria - used to evaluate criteria on client side
		if (campaign.getQualificationCriteria() != null) {
			this.qualificationCriteria = new ExpressionGroupDTO(campaign.getQualificationCriteria());
		}
		// Added to hide phone number on agent screen from third party center
		this.hidePhone = campaign.isHidePhone();
		this.hideEmail = campaign.isHideEmail();
		this.simpleDisposition = campaign.isSimpleDisposition();
		this.hostingServer = campaign.getHostingServer();
		this.isEncrypted = campaign.isEncrypted();
		this.campaignRequirements = campaign.getCampaignRequirements();
		this.organizationId = campaign.getOrganizationId();
		this.isNotConference = campaign.isNotConference();
		this.isABM = campaign.isABM();
		this.supervisorAgentStatusApprove = campaign.isSupervisorAgentStatusApprove();
		this.callControlProvider = campaign.getCallControlProvider();
		this.isLeadIQ = campaign.isLeadIQ();
		this.enableMachineAnsweredDetection = campaign.isEnableMachineAnsweredDetection();
		this.networkCheck = campaign.isNetworkCheck();
		this.bandwidthCheck = campaign.isBandwidthCheck();
		this.ringingIcon = campaign.isRingingIcon();
		this.unHideSocialLink = campaign.isUnHideSocialLink();
		this.autoMachineHangup = campaign.isAutoMachineHangup();
		this.autoModeCallProvider = campaign.getAutoModeCallProvider();
		this.manualModeCallProvider = campaign.getManualModeCallProvider();
		this.revealEmail = campaign.isRevealEmail();
		this.autoGoldenPass =campaign.isAutoGoldenPass();
		this.enableCustomFields = campaign.isEnableCustomFields();
		this.enableInternetCheck = campaign.isEnableInternetCheck();
		this.enableStateCodeDropdown = campaign.isEnableStateCodeDropdown();
		this.enableCallbackFeature = campaign.isEnableCallbackFeature();
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the type
	 */
	public CampaignTypes getType() {
		return type;
	}

	/**
	 * @return the vertical
	 */
	public String getVertical() {
		return vertical;
	}

	/**
	 * @param vertical the vertical to set
	 */
	public void setVertical(String vertical) {
		this.vertical = vertical;
	}

	/**
	 * @return the assets
	 */
	public List<AssetDTO> getAssets() {
		return assets;
	}

	public CallGuide getCallGuide() {
		return this.callGuide;
	}

	/**
	 * @return the tempAllocation
	 */
	public boolean isTempAllocation() {
		return tempAllocation;
	}

	/**
	 * @param tempAllocation the tempAllocation to set
	 */
	public void setTempAllocation(boolean tempAllocation) {
		this.tempAllocation = tempAllocation;
	}

	/**
	 * @return the nextWorkSlot
	 */
	public WorkSlot getNextWorkSlot() {
		return nextWorkSlot;
	}

	/**
	 * @param nextWorkSlot the nextWorkSlot to set
	 */
	public void setNextWorkSlot(WorkSlot nextWorkSlot) {
		this.nextWorkSlot = nextWorkSlot;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the dialerMode
	 */
	public DialerMode getDialerMode() {
		return dialerMode;
	}

	public ExpressionGroupDTO getQualificationCriteria() {
		return qualificationCriteria;
	}

	public void setQualificationCriteria(ExpressionGroupDTO qualificationCriteria) {
		this.qualificationCriteria = qualificationCriteria;
	}

	/*
	 * START DATE : 09/10/2017 REASON : Added below methods to handle agent campaign
	 * change issue when campaign has large text size. PUSHER has 10 KB size limit
	 * that's why reducing campaign text size to pass through PUSHER
	 */
	public void setCallGuide(CallGuide callGuide) {
		this.callGuide = callGuide;
	}

	public void setQuestionAttributeMap(LinkedHashMap<String, CRMAttribute> questionAttributeMap) {
		this.questionAttributeMap = questionAttributeMap;
	}
	/* END */

	/*
	 * START HIDE PHONE DATE : 02/11/2017 REASON : Added to hide phone number on
	 * agent screen from third party center
	 */
	public boolean isHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}
	/* END HIDE PHONE */

	public boolean isHideEmail() {
		return hideEmail;
	}

	public void setHideEmail(boolean hideEmail) {
		this.hideEmail = hideEmail;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public boolean isNotConference() {
		return isNotConference;
	}

	public void setNotConference(boolean isNotConference) {
		this.isNotConference = isNotConference;
	}

	public boolean isABM() {
		return isABM;
	}

	public void setABM(boolean isABM) {
		this.isABM = isABM;
	}

	public boolean isSupervisorAgentStatusApprove() {
		return supervisorAgentStatusApprove;
	}

	public void setSupervisorAgentStatusApprove(boolean supervisorAgentStatusApprove) {
		this.supervisorAgentStatusApprove = supervisorAgentStatusApprove;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public boolean isLeadIQ() {
		return isLeadIQ;
	}

	public void setLeadIQ(boolean isLeadIQ) {
		this.isLeadIQ = isLeadIQ;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean isNetworkCheck() {
		return networkCheck;
	}

	public void setNetworkCheck(boolean networkCheck) {
		this.networkCheck = networkCheck;
	}

	public boolean isBandwidthCheck() {
		return bandwidthCheck;
	}

	public void setBandwidthCheck(boolean bandwidthCheck) {
		this.bandwidthCheck = bandwidthCheck;
	}

	public boolean isRingingIcon() {
		return ringingIcon;
	}

	public void setRingingIcon(boolean ringingIcon) {
		this.ringingIcon = ringingIcon;
	}

	public boolean isUnHideSocialLink() {
		return unHideSocialLink;
	}

	public void setUnHideSocialLink(boolean unHideSocialLink) {
		this.unHideSocialLink = unHideSocialLink;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}

	public String getAutoModeCallProvider() {
		return autoModeCallProvider;
	}

	public void setAutoModeCallProvider(String autoModeCallProvider) {
		this.autoModeCallProvider = autoModeCallProvider;
	}

	public String getManualModeCallProvider() {
		return manualModeCallProvider;
	}

	public void setManualModeCallProvider(String manualModeCallProvider) {
		this.manualModeCallProvider = manualModeCallProvider;
	}
	
	public boolean isRevealEmail() {
		return revealEmail;
	}

	public void setRevealEmail(boolean revealEmail) {
		this.revealEmail = revealEmail;
	}
	
	public boolean isAutoGoldenPass() {
		return autoGoldenPass;
	}

	public void setAutoGoldenPass(boolean autoGoldenPass) {
		this.autoGoldenPass = autoGoldenPass;
	}

	public boolean isLiteMode() {
		return liteMode;
	}

	public void setLiteMode(boolean liteMode) {
		this.liteMode = liteMode;
	}

	public boolean isEnableCustomFields() {
		return enableCustomFields;
	}

	public void setEnableCustomFields(boolean enableCustomFields) {
		this.enableCustomFields = enableCustomFields;
	}
	
	public boolean isEnableInternetCheck() {
		return enableInternetCheck;
	}

	public void setEnableInternetCheck(boolean enableInternetCheck) {
		this.enableInternetCheck = enableInternetCheck;
	}

	public boolean isEnableStateCodeDropdown() {
		return enableStateCodeDropdown;
	}

	public void setEnableStateCodeDropdown(boolean enableStateCodeDropdown) {
		this.enableStateCodeDropdown = enableStateCodeDropdown;
	}

	public boolean isEnableCallbackFeature() {
		return enableCallbackFeature;
	}

	public void setEnableCallbackFeature(boolean enableCallbackFeature) {
		this.enableCallbackFeature = enableCallbackFeature;
	}
	
}
