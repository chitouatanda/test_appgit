package com.xtaas.web.dto;

public enum TeamSearchClauses {
	ByAll, ByPreviousCampaign, BySearch, ByAutoSuggest, ByCurrentInvitation, ByCurrentSelection, ByQa, ByRating, ByShortlist ,ByQa2
}
