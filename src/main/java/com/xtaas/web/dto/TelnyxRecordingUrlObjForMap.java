package com.xtaas.web.dto;

public class TelnyxRecordingUrlObjForMap {

  private String recordingUrl;

  private String awsRecordingUrl;

  public String getRecordingUrl() {
    return recordingUrl;
  }

  public void setRecordingUrl(String recordingUrl) {
    this.recordingUrl = recordingUrl;
  }

  public String getAwsRecordingUrl() {
    return awsRecordingUrl;
  }

  public void setAwsRecordingUrl(String awsRecordingUrl) {
    this.awsRecordingUrl = awsRecordingUrl;
  }


}