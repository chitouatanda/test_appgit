package com.xtaas.web.dto;

import java.util.List;

public class RestProspectResponseStatusDTO {

	private int success;
	
	private int failed;
	
	//private String status;
	
	private List<RestProspectResponseDTO> errorList;

	

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public int getFailed() {
		return failed;
	}

	public void setFailed(int failed) {
		this.failed = failed;
	}

	public List<RestProspectResponseDTO> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<RestProspectResponseDTO> errorList) {
		this.errorList = errorList;
	}
	
	
}
