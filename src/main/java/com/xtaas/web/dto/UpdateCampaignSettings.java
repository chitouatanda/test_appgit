package com.xtaas.web.dto;

import java.util.List;
import java.util.Map;

import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.valueobjects.KeyValuePair;

public class UpdateCampaignSettings {

	private LeadSortOrder leadSortOrder;
	private CallSpeed callSpeedPerMinPerAgent;
	private float campaignSpeed;
	private DialerMode dialerMode;
	private String hostingServer;
	private Map<String, Float> retrySpeed;
	private String callControlProvider;
	private List<KeyValuePair<String, Integer>> dataSourcePriority;
	private Map<String, Integer> sourceCallingPriority;
	private String enableMachineAnsweredDetection;
	private String disableAMDOnQueued;
	private String useSlice;
	private String hidePhone;
	private String hideEmail;
	private String qaRequired;
	private String simpleDisposition;
	private String retrySpeedEnabled;
	private String indirectBuyStarted;
	private String abm;
	private String leadIQ;
	private List<String> campaignGroupIds;
	private String callableEvent;
	private String type;
	private int numberOfTopRecords;
	private String sipProvider;

	
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCallableEvent() {
		return callableEvent;
	}

	public void setCallableEvent(String callableEvent) {
		this.callableEvent = callableEvent;
	}

	public List<String> getCampaignGroupIds() {
		return campaignGroupIds;
	}

	public void setCampaignGroupIds(List<String> campaignGroupIds) {
		this.campaignGroupIds = campaignGroupIds;
	}

	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public float getCampaignSpeed() {
		return campaignSpeed;
	}

	public void setCampaignSpeed(float campaignSpeed) {
		this.campaignSpeed = campaignSpeed;
	}

	public DialerMode getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public List<KeyValuePair<String, Integer>> getDataSourcePriority() {
		return dataSourcePriority;
	}

	public void setDataSourcePriority(List<KeyValuePair<String, Integer>> dataSourcePriority) {
		this.dataSourcePriority = dataSourcePriority;
	}

	public String getEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(String enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public String getDisableAMDOnQueued() {
		return disableAMDOnQueued;
	}

	public void setDisableAMDOnQueued(String disableAMDOnQueued) {
		this.disableAMDOnQueued = disableAMDOnQueued;
	}

	public String getUseSlice() {
		return useSlice;
	}

	public void setUseSlice(String useSlice) {
		this.useSlice = useSlice;
	}

	public String getHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(String hidePhone) {
		this.hidePhone = hidePhone;
	}

	public String getHideEmail() {
		return hideEmail;
	}

	public void setHideEmail(String hideEmail) {
		this.hideEmail = hideEmail;
	}

	public String getQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(String qaRequired) {
		this.qaRequired = qaRequired;
	}

	public String getSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(String simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(String retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public String getIndirectBuyStarted() {
		return indirectBuyStarted;
	}

	public void setIndirectBuyStarted(String indirectBuyStarted) {
		this.indirectBuyStarted = indirectBuyStarted;
	}

	public String getAbm() {
		return abm;
	}

	public void setAbm(String abm) {
		this.abm = abm;
	}

	public String getLeadIQ() {
		return leadIQ;
	}

	public void setLeadIQ(String leadIQ) {
		this.leadIQ = leadIQ;
	}

	public Map<String, Integer> getSourceCallingPriority() {
		return sourceCallingPriority;
	}

	public void setSourceCallingPriority(Map<String, Integer> sourceCallingPriority) {
		this.sourceCallingPriority = sourceCallingPriority;
	}

	public int getNumberOfTopRecords() {
		return numberOfTopRecords;
	}

	public void setNumberOfTopRecords(int numberOfTopRecords) {
		this.numberOfTopRecords = numberOfTopRecords;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

}
