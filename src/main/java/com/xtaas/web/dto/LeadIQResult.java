package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"_id",
"personalPhones",
"currentPositions",
"linkedin",
"name"
})
public class LeadIQResult {

@JsonProperty("_id")
private String id;
@JsonProperty("personalPhones")
private LeadIQPersonalPhones personalPhones;
@JsonProperty("currentPositions")
private List<LeadIQCurrentPosition> currentPositions = null;
@JsonProperty("linkedin")
private LeadIQLinkedin linkedin;
@JsonProperty("name")
private LeadIQName name;

@JsonProperty("_id")
public String getId() {
return id;
}

@JsonProperty("_id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("currentPositions")
public List<LeadIQCurrentPosition> getCurrentPositions() {
return currentPositions;
}

@JsonProperty("currentPositions")
public void setCurrentPositions(List<LeadIQCurrentPosition> currentPositions) {
this.currentPositions = currentPositions;
}

@JsonProperty("linkedin")
public LeadIQLinkedin getLinkedin() {
return linkedin;
}

@JsonProperty("linkedin")
public void setLinkedin(LeadIQLinkedin linkedin) {
this.linkedin = linkedin;
}

@JsonProperty("name")
public LeadIQName getName() {
return name;
}

@JsonProperty("name")
public void setName(LeadIQName name) {
this.name = name;
}

@JsonProperty("personalPhones")
public LeadIQPersonalPhones getPersonalPhones() {
	return personalPhones;
}

@JsonProperty("personalPhones")
public void setPersonalPhones(LeadIQPersonalPhones personalPhones) {
	this.personalPhones = personalPhones;
}

@Override
public String toString() {
	return "LeadIQResult [id=" + id + ", personalPhones=" + personalPhones + ", currentPositions=" + currentPositions
			+ ", linkedin=" + linkedin + ", name=" + name + "]";
}

}