package com.xtaas.web.dto;

import java.util.ArrayList;

import com.xtaas.db.entity.PickListItem;

public class HealthChecksCriteriaDTO {

	private String attribute;

	private String operator;

	private String value;
	
	private ArrayList<PickListItem> pickList;

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

	public ArrayList<PickListItem> getPickList() {
		return pickList;
	}

	public void setPickList(ArrayList<PickListItem> pickList) {
		this.pickList = pickList;
	}

	@Override
	public String toString() {
		return "HealthChecksCriteriaDTO [attribute=" + attribute + ", operator=" + operator + ", value=" + value
				+ ", pickList=" + pickList + "]";
	}

}
