/**
 * 
 */
package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.utils.XtaasDateUtils;

/**
 * @author djain
 *
 */
public class ProspectCallDTO {
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private Prospect prospect;
	@JsonProperty
	private String twilioCallSid;
	@JsonProperty
	private ArrayList<NoteDTO> notes;
	@JsonProperty
	private List<IndirectProspectsDTO> indirectProspects;
	@JsonProperty
	private List<AgentQaAnswer> answers;
	@JsonProperty
	private String deliveredAssetId;
	@JsonProperty
	private String dispositionStatus;
	@JsonProperty
	private String leadStatus;
	@JsonProperty
	private String subStatus;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String agentId;
	@JsonProperty
	private long callbackTime; // set the callback in MS
	@JsonProperty
	private String callbackTimezone; // callbackTime comes in UTC
	@JsonProperty
	private String callbackSelectedDate; // callback date selected by user in MM/dd/yyyy format. Taking as string as it
											// needs to maintained in Timezone specified
	@JsonProperty
	private int callbackSelectedHours; // callback hour time in 24 hr format Ex: 13 for 1 PM
	@JsonProperty
	private int callbackSelectedMin; // callback min
	@JsonProperty
	private Date callStartTime; // time when call started in UTC
	@JsonProperty
	private int callDuration; // duration of calls in secs
	@JsonProperty
	private String callbackAmPm;
	@JsonProperty
	private String recordingUrl; // call recording url
	@JsonProperty
	private String recordingUrlAws; // call recording url
	@JsonProperty
	private String outboundNumber; // outbound number used to call this prospect
	@JsonProperty
	private int wrapupDuration; // indicates duration in sec spent in wrap up state
	@JsonProperty
	private int prospectHandleDuration; // indicate total time spent by agent in handling this prospect from begin to
										// end
	@JsonProperty
	private int callRetryCount; // Number of times a prospect has been retried
	@JsonProperty
	private Date createdDate; // Date when prospect was created in system
	@JsonProperty
	private String prospectInteractionSessionId; // unique session id for the prospect call interaction
	@JsonProperty
	private Long dailyCallRetryCount; // Number of calls made to a prospect in a day
	@JsonProperty
	private String partnerId;
	@JsonProperty
	private boolean emailBounce;
	@JsonProperty
	private int prospectVersion;
	@JsonProperty
	private boolean isFinalSubmit;
	@JsonProperty
	private String qualityBucket;
	@JsonProperty
	private int qualityBucketSort;
	@JsonProperty
	private String multiProspect;
	@JsonProperty
	private String zoomValidated;
	@JsonProperty
	private String zoomCompanyUrl;
	@JsonProperty
	private String zoomPersonUrl;
	@JsonProperty
	private boolean leadDelivered;
	@JsonProperty
	private String clientDelivered;
	@JsonProperty
	private String dataSlice;
	@JsonProperty
	private int machineDetectedIncrementer;
	@JsonProperty
	private String callDetectedAs;
	@JsonProperty
	private long amdTimeDiffInSec;
	@JsonProperty
	private boolean autoMachineDetected;
	@JsonProperty
	private String voiceProvider;
	@JsonProperty
	private Date updatedDate;
	private long amdTimeDiffInMilliSec;

	@JsonProperty
	private boolean emailRevealed;
	@JsonProperty
	private String sipProvider;
	
	public ProspectCallDTO() {
	}

	public ProspectCallDTO(ProspectCall prospectCall) {
		this.prospectCallId = prospectCall.getProspectCallId();
		this.prospect = prospectCall.getProspect();
		this.twilioCallSid = prospectCall.getTwilioCallSid();
		ArrayList<NoteDTO> notesDTO = new ArrayList<NoteDTO>();
		if (prospectCall.getNotes() != null) {
			for (Note note : prospectCall.getNotes()) {
				notesDTO.add(new NoteDTO(note));
			}
		}
		this.notes = notesDTO;
		/*
		 * ArrayList<IndirectProspectsDTO> indirectProspectsDTO = new
		 * ArrayList<IndirectProspectsDTO>(); if(prospectCallLogList!=null ) {
		 * for(ProspectCallLog pcl : prospectCallLogList){ indirectProspectsDTO.add(new
		 * IndirectProspectsDTO(pcl.getProspectCall().getProspect().getFirstName(),
		 * pcl.getProspectCall().getProspect().getLastName(),
		 * pcl.getProspectCall().getProspect().getTitle(),
		 * pcl.getProspectCall().getProspect().getDepartment(),
		 * pcl.getProspectCall().getProspect().getEmail(),
		 * pcl.getProspectCall().getCampaignId(),
		 * pcl.getProspectCall().getProspectCallId())); } }
		 */
		this.qualityBucket = prospectCall.getQualityBucket();
		this.qualityBucketSort = prospectCall.getQualityBucketSort();
		this.multiProspect = prospectCall.getMultiProspect();
		this.indirectProspects = prospectCall.getIndirectProspects();
//		this.answers = prospectCall.getAnswers();
		this.deliveredAssetId = prospectCall.getDeliveredAssetId();
		this.dispositionStatus = prospectCall.getDispositionStatus();
		this.leadStatus = prospectCall.getLeadStatus();
		this.subStatus = prospectCall.getSubStatus();
		this.campaignId = prospectCall.getCampaignId();
		this.agentId = prospectCall.getAgentId();
		this.callbackTime = prospectCall.getCallbackTime();
		this.callStartTime = prospectCall.getCallStartTime();
		this.callDuration = prospectCall.getCallDuration();
		this.callRetryCount = prospectCall.getCallRetryCount();
		this.setOutboundNumber(prospectCall.getOutboundNumber());
		this.setCreatedDate(prospectCall.getCreatedDate());
		this.setProspectInteractionSessionId(prospectCall.getProspectInteractionSessionId());
		this.setRecordingUrl(prospectCall.getRecordingUrl());
		this.setRecordingUrlAws(prospectCall.getRecordingUrlAws());
		this.dailyCallRetryCount = prospectCall.getDailyCallRetryCount();
		List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
		if (prospectCall.getAnswers() != null) {
			for (AgentQaAnswer answer : prospectCall.getAnswers()) {
				qaAnswers.add(answer);
			}
		}
		this.answers = qaAnswers;
		this.partnerId = prospectCall.getPartnerId();
		this.emailBounce = prospectCall.isEmailBounce();
		this.prospectVersion = prospectCall.getProspectVersion();
		this.zoomValidated = prospectCall.getZoomValidated();
		this.zoomCompanyUrl = prospectCall.getProspect().getZoomCompanyUrl();
		this.zoomPersonUrl = prospectCall.getProspect().getZoomPersonUrl();
		this.leadDelivered = prospectCall.isLeadDelivered();
		this.clientDelivered = prospectCall.getClientDelivered();
		this.dataSlice = prospectCall.getDataSlice();
		this.machineDetectedIncrementer = prospectCall.getMachineDetectedIncrementer();
		this.autoMachineDetected = prospectCall.isAutoMachineDetected();
		this.callDetectedAs = prospectCall.getCallDetectedAs();
		this.amdTimeDiffInSec = prospectCall.getAmdTimeDiffInSec();
		this.voiceProvider = prospectCall.getVoiceProvider();
		this.amdTimeDiffInMilliSec = prospectCall.getAmdTimeDiffInMilliSec();
		this.sipProvider = prospectCall.getSipProvider();
	}
	
	public ProspectCallDTO(ProspectCallLog prospectCallLog) {
		this.prospectCallId = prospectCallLog.getProspectCall().getProspectCallId();
		this.prospect = prospectCallLog.getProspectCall().getProspect();
		this.twilioCallSid = prospectCallLog.getProspectCall().getTwilioCallSid();
		ArrayList<NoteDTO> notesDTO = new ArrayList<NoteDTO>();
		if (prospectCallLog.getProspectCall().getNotes() != null) {
			for (Note note : prospectCallLog.getProspectCall().getNotes()) {
				notesDTO.add(new NoteDTO(note));
			}
		}
		this.notes = notesDTO;
		this.qualityBucket = prospectCallLog.getProspectCall().getQualityBucket();
		this.qualityBucketSort = prospectCallLog.getProspectCall().getQualityBucketSort();
		this.multiProspect = prospectCallLog.getProspectCall().getMultiProspect();
		this.indirectProspects = prospectCallLog.getProspectCall().getIndirectProspects();
		this.deliveredAssetId = prospectCallLog.getProspectCall().getDeliveredAssetId();
		this.dispositionStatus = prospectCallLog.getProspectCall().getDispositionStatus();
		this.leadStatus = prospectCallLog.getProspectCall().getLeadStatus();
		this.subStatus = prospectCallLog.getProspectCall().getSubStatus();
		this.campaignId = prospectCallLog.getProspectCall().getCampaignId();
		this.agentId = prospectCallLog.getProspectCall().getAgentId();
		this.callbackTime = prospectCallLog.getProspectCall().getCallbackTime();
		this.callStartTime = prospectCallLog.getProspectCall().getCallStartTime();
		this.callDuration = prospectCallLog.getProspectCall().getCallDuration();
		this.callRetryCount = prospectCallLog.getProspectCall().getCallRetryCount();
		this.setOutboundNumber(prospectCallLog.getProspectCall().getOutboundNumber());
		this.setCreatedDate(prospectCallLog.getProspectCall().getCreatedDate());
		this.setProspectInteractionSessionId(prospectCallLog.getProspectCall().getProspectInteractionSessionId());
		this.setRecordingUrl(prospectCallLog.getProspectCall().getRecordingUrl());
		this.setRecordingUrlAws(prospectCallLog.getProspectCall().getRecordingUrlAws());
		this.dailyCallRetryCount = prospectCallLog.getProspectCall().getDailyCallRetryCount();
		List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
		if (prospectCallLog.getProspectCall().getAnswers() != null) {
			for (AgentQaAnswer answer : prospectCallLog.getProspectCall().getAnswers()) {
				qaAnswers.add(answer);
			}
		}
		this.answers = qaAnswers;
		this.partnerId = prospectCallLog.getProspectCall().getPartnerId();
		this.emailBounce = prospectCallLog.getProspectCall().isEmailBounce();
		this.prospectVersion = prospectCallLog.getProspectCall().getProspectVersion();
		this.zoomValidated = prospectCallLog.getProspectCall().getZoomValidated();
		this.zoomCompanyUrl = prospectCallLog.getProspectCall().getProspect().getZoomCompanyUrl();
		this.zoomPersonUrl = prospectCallLog.getProspectCall().getProspect().getZoomPersonUrl();
		this.leadDelivered = prospectCallLog.getProspectCall().isLeadDelivered();
		this.clientDelivered = prospectCallLog.getProspectCall().getClientDelivered();
		this.dataSlice = prospectCallLog.getProspectCall().getDataSlice();
		this.machineDetectedIncrementer = prospectCallLog.getProspectCall().getMachineDetectedIncrementer();
		this.autoMachineDetected = prospectCallLog.getProspectCall().isAutoMachineDetected();
		this.callDetectedAs = prospectCallLog.getProspectCall().getCallDetectedAs();
		this.amdTimeDiffInSec = prospectCallLog.getProspectCall().getAmdTimeDiffInSec();
		this.voiceProvider = prospectCallLog.getProspectCall().getVoiceProvider();
		this.createdDate = prospectCallLog.getCreatedDate();
		this.updatedDate = prospectCallLog.getUpdatedDate();
		this.sipProvider = prospectCallLog.getProspectCall().getSipProvider();
	}

	public ProspectCall toProspectCall() {
		ProspectCall prospectCall = new ProspectCall(this.prospectCallId, this.campaignId, this.prospect);
		prospectCall.setTwilioCallSid(this.twilioCallSid);
		ArrayList<Note> notes = new ArrayList<Note>();
		if (this.notes != null) {
			for (NoteDTO noteDTO : this.notes) {
				notes.add(noteDTO.toNote());
			}
		}

//		// callback Time calculation -- converting in specified TZ
//		if (this.callbackSelectedDate != null && !this.callbackSelectedDate.isEmpty()) {
//			// create date object in callbackTZ and then convert it into UTC and set it into
//			// callbackTime variable
//			try {
//				String[] dt = StringUtils.split(this.callbackSelectedDate, "/"); // date is in format MM/dd/yyyy
//				DateTime callbackDateTime = new DateTime(Integer.valueOf(dt[2]), Integer.valueOf(dt[0]),
//						Integer.valueOf(dt[1]), this.callbackSelectedHours, this.callbackSelectedMin,
//						DateTimeZone.forID(this.callbackTimezone));
//				this.callbackTime = XtaasDateUtils.changeTimeZone(callbackDateTime, DateTimeZone.UTC.getID())
//						.getMillis();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}

		prospectCall.setNotes(notes);
		prospectCall.setCallbackTime(this.callbackTime);
		List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
		if (this.answers != null) {
			for (AgentQaAnswer answer : this.answers) {
				qaAnswers.add(answer);
			}
		}
		List<IndirectProspectsDTO> indirectProspects = new ArrayList<IndirectProspectsDTO>();
		if (this.indirectProspects != null) {
			for (IndirectProspectsDTO ipd : this.indirectProspects) {
				indirectProspects.add(ipd);
			}
		}
		prospectCall.setAnswers(qaAnswers);
		prospectCall.setIndirectProspects(indirectProspects);
		prospectCall.setDeliveredAssetId(this.deliveredAssetId);
		prospectCall.setDispositionStatus(this.dispositionStatus);
		prospectCall.setSubStatus(this.subStatus);
		prospectCall.setAgentId(this.agentId);
		prospectCall.setCallbackTime(this.callbackTime);
		prospectCall.setCallStartTime(this.callStartTime);
		prospectCall.setCallDuration(this.callDuration);
		prospectCall.setOutboundNumber(this.outboundNumber);
		prospectCall.setCallRetryCount(this.callRetryCount);
		prospectCall.setCreatedDate(this.createdDate);
		prospectCall.setProspectInteractionSessionId(this.prospectInteractionSessionId);
		prospectCall.setRecordingUrl(this.recordingUrl);
		prospectCall.setRecordingUrlAws(this.recordingUrlAws);
		prospectCall.setDailyCallRetryCount(this.dailyCallRetryCount);
		prospectCall.setPartnerId(this.partnerId);
		prospectCall.setEmailBounce(this.emailBounce);
		prospectCall.setProspectVersion(this.prospectVersion);
		prospectCall.setQualityBucket(this.qualityBucket);
		prospectCall.setQualityBucketSort(this.qualityBucketSort);
		prospectCall.setMultiProspect(this.multiProspect);
		prospectCall.setZoomValidated(this.zoomValidated);
		prospectCall.setLeadDelivered(this.leadDelivered);
		prospectCall.setClientDelivered(this.clientDelivered);
		prospectCall.setDataSlice(this.dataSlice);
		prospectCall.setMachineDetectedIncrementer(this.machineDetectedIncrementer);
		prospectCall.setAutoMachineDetected(this.autoMachineDetected);
		prospectCall.setCallDetectedAs(this.callDetectedAs);
		prospectCall.setAmdTimeDiffInSec(this.amdTimeDiffInSec);
		prospectCall.setVoiceProvider(this.voiceProvider);
		prospectCall.setAmdTimeDiffInMilliSec(this.amdTimeDiffInMilliSec);
		prospectCall.setSipProvider(this.sipProvider);
		return prospectCall;
	}

	public ProspectCall toNewProspectCall(Prospect newProspect) {
		Prospect prospect = new Prospect();
		prospect.setEu(newProspect.getIsEu());
		prospect.setSource(newProspect.getSource());
		prospect.setSourceId(newProspect.getSourceId());
		prospect.setEncrypted(newProspect.getIsEncrypted());
		prospect.setEmail(newProspect.getEmail());
		prospect.setPrefix(newProspect.getPrefix());
		prospect.setFirstName(newProspect.getFirstName());
		prospect.setLastName(newProspect.getLastName());
		prospect.setCompany(newProspect.getCompany());
		prospect.setAddressLine1(newProspect.getAddressLine1());
		prospect.setAddressLine2(newProspect.getAddressLine2());
		prospect.setZipCode(newProspect.getZipCode());
		prospect.setCity(newProspect.getCity());
		prospect.setPhone(newProspect.getPhone());
		prospect.setCompanyPhone(newProspect.getCompanyPhone());
		prospect.setDirectPhone(newProspect.isDirectPhone());
		prospect.setStateCode(newProspect.getStateCode());
		prospect.setCountry(newProspect.getCountry());
		prospect.setTitle(newProspect.getTitle());
		prospect.setDepartment(newProspect.getDepartment());
		prospect.setIndustry(newProspect.getIndustry());
		prospect.setZoomCompanyUrl(newProspect.getZoomCompanyUrl());
		prospect.setZoomPersonUrl(newProspect.getZoomPersonUrl());
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		customAttributes.put("minRevenue", newProspect.getCustomAttributes().get("minRevenue"));
		customAttributes.put("maxRevenue", newProspect.getCustomAttributes().get("maxRevenue"));
		customAttributes.put("maxEmployeeCount", newProspect.getCustomAttributes().get("maxEmployeeCount"));
		customAttributes.put("minEmployeeCount", newProspect.getCustomAttributes().get("minEmployeeCount"));
		customAttributes.put("domain", newProspect.getCustomAttributes().get("domain"));
		prospect.setCustomAttributes(customAttributes);
		prospect.setSourceCompanyId(newProspect.getSourceCompanyId());
		prospect.setManagementLevel(newProspect.getManagementLevel());
		prospect.setTimeZone(newProspect.getTimeZone());
		if (newProspect.getCustomFields() != null && newProspect.getCustomFields().size() > 0) {
			prospect.setCustomFields(newProspect.getCustomFields());
		}

		ProspectCall prospectCall = new ProspectCall(this.prospectCallId, this.campaignId, prospect);
		prospectCall.setTwilioCallSid(this.twilioCallSid);
		ArrayList<Note> notes = new ArrayList<Note>();
		if (this.notes != null) {
			for (NoteDTO noteDTO : this.notes) {
				notes.add(noteDTO.toNote());
			}
		}

		// callback Time calculation -- converting in specified TZ
//		if (this.callbackSelectedDate != null && !this.callbackSelectedDate.isEmpty()) {
//			// create date object in callbackTZ and then convert it into UTC and set it into
//			// callbackTime variable
//			try {
//				String[] dt = StringUtils.split(this.callbackSelectedDate, "/"); // date is in format MM/dd/yyyy
//				DateTime callbackDateTime = new DateTime(Integer.valueOf(dt[2]), Integer.valueOf(dt[0]),
//						Integer.valueOf(dt[1]), this.callbackSelectedHours, this.callbackSelectedMin,
//						DateTimeZone.forID(this.callbackTimezone));
//				this.callbackTime = XtaasDateUtils.changeTimeZone(callbackDateTime, DateTimeZone.UTC.getID())
//						.getMillis();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
		prospectCall.setCallbackTime(this.callbackTime);

		prospectCall.setNotes(notes);
		List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
		if (this.answers != null) {
			for (AgentQaAnswer answer : this.answers) {
				qaAnswers.add(answer);
			}
		}
		List<IndirectProspectsDTO> indirectProspects = new ArrayList<IndirectProspectsDTO>();
		if (this.indirectProspects != null) {
			for (IndirectProspectsDTO ipd : this.indirectProspects) {
				indirectProspects.add(ipd);
			}
		}
		prospectCall.setAnswers(qaAnswers);
		prospectCall.setIndirectProspects(indirectProspects);
		prospectCall.setDeliveredAssetId(this.deliveredAssetId);
		prospectCall.setDispositionStatus(this.dispositionStatus);
		prospectCall.setSubStatus(this.subStatus);
		prospectCall.setAgentId(this.agentId);
		prospectCall.setCallbackTime(this.callbackTime);
		prospectCall.setCallStartTime(this.callStartTime);
		prospectCall.setCallDuration(this.callDuration);
		prospectCall.setOutboundNumber(this.outboundNumber);
		prospectCall.setCallRetryCount(this.callRetryCount);
		prospectCall.setCreatedDate(this.createdDate);
		prospectCall.setProspectInteractionSessionId(this.prospectInteractionSessionId);
		prospectCall.setRecordingUrl(this.recordingUrl);
		prospectCall.setRecordingUrlAws(this.recordingUrlAws);
		prospectCall.setDailyCallRetryCount(this.dailyCallRetryCount);
		prospectCall.setPartnerId(this.partnerId);
		prospectCall.setEmailBounce(this.emailBounce);
		prospectCall.setProspectVersion(this.prospectVersion);
		prospectCall.setQualityBucket(this.qualityBucket);
		prospectCall.setQualityBucketSort(this.qualityBucketSort);
		prospectCall.setMultiProspect(this.multiProspect);
		prospectCall.setZoomValidated(this.zoomValidated);
		prospectCall.setLeadDelivered(this.leadDelivered);
		prospectCall.setClientDelivered(this.clientDelivered);
		prospectCall.setAutoMachineDetected(this.autoMachineDetected);
		prospectCall.setCallDetectedAs(this.callDetectedAs);
		prospectCall.setAmdTimeDiffInSec(this.amdTimeDiffInSec);
		prospectCall.setVoiceProvider(this.voiceProvider);
		prospectCall.setAmdTimeDiffInMilliSec(this.amdTimeDiffInMilliSec);
		prospectCall.setSipProvider(this.sipProvider);
		return prospectCall;
	}
	
	public ProspectCacheDTO getProspectCacheDTO(ProspectCallDTO prospectCallDTO) {
		ProspectCacheDTO prospectCacheDTO = new ProspectCacheDTO();
		if (prospectCallDTO != null && prospectCallDTO.getProspect() != null) {
			prospectCacheDTO.setCountry(prospectCallDTO.getProspect().getCountry());
			prospectCacheDTO.setFirstName(prospectCallDTO.getProspect().getFirstName());
			prospectCacheDTO.setLastName(prospectCallDTO.getProspect().getLastName());
			prospectCacheDTO.setPhone(prospectCallDTO.getProspect().getPhone());
			prospectCacheDTO.setTimeZone(prospectCallDTO.getProspect().getTimeZone());
			prospectCacheDTO.setProspectType(prospectCallDTO.getProspect().getProspectType());
			prospectCacheDTO.setCustomAttributes(prospectCallDTO.getProspect().getCustomAttributes());
			prospectCacheDTO.setCompany(prospectCallDTO.getProspect().getCompany());
			prospectCacheDTO.setEmail(prospectCallDTO.getProspect().getEmail());
			prospectCacheDTO.setStateCode(prospectCallDTO.getProspect().getStateCode());
			prospectCacheDTO.setExtension(prospectCallDTO.getProspect().getExtension());
		}
		return prospectCacheDTO;
	}
	
	public List<IndirectProspectsCacheDTO> getIndirectProspectCacheDTO(List<IndirectProspectsDTO> indirectProspects) {
		List<IndirectProspectsCacheDTO> indirectList = new ArrayList<IndirectProspectsCacheDTO>();
		if (indirectProspects != null && indirectProspects.size() > 0) {
			for (IndirectProspectsDTO indirectProspectDTO : indirectProspects) {
				if (indirectProspectDTO != null) {
					IndirectProspectsCacheDTO indirectProspectsCacheDTO = new IndirectProspectsCacheDTO();
					indirectProspectsCacheDTO.setProspectCallId(indirectProspectDTO.getProspectCallId());
					indirectProspectsCacheDTO.setFirstName(indirectProspectDTO.getFirstName());
					indirectProspectsCacheDTO.setLastName(indirectProspectDTO.getLastName());
					indirectProspectsCacheDTO.setDepartment(indirectProspectDTO.getDepartment());
					indirectProspectsCacheDTO.setTitle(indirectProspectDTO.getTitle());
					indirectProspectsCacheDTO.setCompany(indirectProspectDTO.getCompany());
					indirectList.add(indirectProspectsCacheDTO);
				}
			}
		}
		return indirectList;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @return the prospect
	 */
	public Prospect getProspect() {
		return prospect;
	}

	/**
	 * @return the twilioCallSid
	 */
	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	/**
	 * @return the notes
	 */
	public ArrayList<NoteDTO> getNotes() {
		return notes;
	}

	/**
	 * @return the answers
	 */
	/*
	 * public HashMap<String, String> getAnswers() { return answers; }
	 */
	/**
	 * @return the deliveredAssetId
	 */
	public String getDeliveredAssetId() {
		return deliveredAssetId;
	}

	/**
	 * @return the dispositionStatus
	 */
	public String getDispositionStatus() {
		return dispositionStatus;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	public String getAgentId() {
		return this.agentId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @param prospect the prospect to set
	 */
	public void setProspect(Prospect prospect) {
		this.prospect = prospect;
	}

	/**
	 * @param twilioCallSids the twilioCallSids to set
	 */
	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(ArrayList<NoteDTO> notes) {
		this.notes = notes;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public String getRecordingUrlAws() {
		return recordingUrlAws;
	}

	public void setRecordingUrlAws(String recordingUrlAws) {
		this.recordingUrlAws = recordingUrlAws;
	}

	/**
	 * @param answers the answers to set
	 */
	/*
	 * public void setAnswers(HashMap<String, String> answers) { this.answers =
	 * answers; }
	 */

	/**
	 * @param deliveredAssetId the deliveredAssetId to set
	 */
	public void setDeliveredAssetId(String deliveredAssetId) {
		this.deliveredAssetId = deliveredAssetId;
	}

	/**
	 * @param dispositionStatus the dispositionStatus to set
	 */
	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	/**
	 * @param callbackHours the callbackHours to set
	 */
	public void setCallbackTime(long callbackTime) {
		this.callbackTime = callbackTime;
	}

	public Date getCallStartTime() {
		return callStartTime;
	}

	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}

	public int getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	/**
	 * @return the callbackTimezone
	 */
	public String getCallbackTimezone() {
		return callbackTimezone;
	}

	/**
	 * @param callbackTimezone the callbackTimezone to set
	 */
	public void setCallbackTimezone(String callbackTimezone) {
		this.callbackTimezone = callbackTimezone;
	}

	/**
	 * @return the callbackSelectedDate
	 */
	public String getCallbackSelectedDate() {
		return callbackSelectedDate;
	}

	/**
	 * @param callbackSelectedDate the callbackSelectedDate to set
	 */
	public void setCallbackSelectedDate(String callbackSelectedDate) {
		this.callbackSelectedDate = callbackSelectedDate;
	}

	/**
	 * @return the callbackSelectedHours
	 */
	public int getCallbackSelectedHours() {
		return callbackSelectedHours;
	}

	/**
	 * @param callbackSelectedHours the callbackSelectedHours to set
	 */
	public void setCallbackSelectedHours(int callbackSelectedHours) {
		this.callbackSelectedHours = callbackSelectedHours;
	}

	/**
	 * @return the callbackSelectedMin
	 */
	public int getCallbackSelectedMin() {
		return callbackSelectedMin;
	}

	/**
	 * @param callbackSelectedMin the callbackSelectedMin to set
	 */
	public void setCallbackSelectedMin(int callbackSelectedMin) {
		this.callbackSelectedMin = callbackSelectedMin;
	}

	/**
	 * @return the outboundNumber
	 */
	public String getOutboundNumber() {
		return outboundNumber;
	}

	/**
	 * @param outboundNumber the outboundNumber to set
	 */
	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	/**
	 * @return the wrapupDuration
	 */
	public int getWrapupDuration() {
		return wrapupDuration;
	}

	/**
	 * @param wrapupDuration the wrapupDuration to set
	 */
	public void setWrapupDuration(int wrapupDuration) {
		this.wrapupDuration = wrapupDuration;
	}

	/**
	 * @return the prospectHandleDuration
	 */
	public int getProspectHandleDuration() {
		return prospectHandleDuration;
	}

	/**
	 * @param prospectHandleDuration the prospectHandleDuration to set
	 */
	public void setProspectHandleDuration(int prospectHandleDuration) {
		this.prospectHandleDuration = prospectHandleDuration;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the prospectInteractionSessionId
	 */
	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	/**
	 * @param prospectInteractionSessionId the prospectInteractionSessionId to set
	 */
	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public boolean isEmailBounce() {
		return emailBounce;
	}

	public void setEmailBounce(boolean emailBounce) {
		this.emailBounce = emailBounce;
	}

	public int getProspectVersion() {
		return prospectVersion;
	}

	public void setProspectVersion(int prospectVersion) {
		this.prospectVersion = prospectVersion;
	}

	public List<IndirectProspectsDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	public boolean isFinalSubmit() {
		return isFinalSubmit;
	}

	public void setFinalSubmit(boolean isFinalSubmit) {
		this.isFinalSubmit = isFinalSubmit;
	}

	public String getQualityBucket() {
		return qualityBucket;
	}

	public void setQualityBucket(String qualityBucket) {
		this.qualityBucket = qualityBucket;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	public String getMultiProspect() {
		return multiProspect;
	}

	public void setMultiProspect(String multiProspect) {
		this.multiProspect = multiProspect;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public String getZoomValidated() {
		return zoomValidated;
	}

	public void setZoomValidated(String zoomValidated) {
		this.zoomValidated = zoomValidated;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public boolean isLeadDelivered() {
		return leadDelivered;
	}

	public void setLeadDelivered(boolean leadDelivered) {
		this.leadDelivered = leadDelivered;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getClientDelivered() {
		return clientDelivered;
	}

	public void setClientDelivered(String clientDelivered) {
		this.clientDelivered = clientDelivered;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public int getMachineDetectedIncrementer() {
		return machineDetectedIncrementer;
	}

	public void setMachineDetectedIncrementer(int machineDetectedIncrementer) {
		this.machineDetectedIncrementer = machineDetectedIncrementer;
	}

	public List<AgentQaAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AgentQaAnswer> answers) {
		this.answers = answers;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public long getCallbackTime() {
		return callbackTime;
	}

	public String getVoiceProvider() {
		return voiceProvider;
	}

	public void setVoiceProvider(String voiceProvider) {
		this.voiceProvider = voiceProvider;
	}

	public long getAmdTimeDiffInSec() {
		return amdTimeDiffInSec;
	}

	public void setAmdTimeDiffInSec(long amdTimeDiffInSec) {
		this.amdTimeDiffInSec = amdTimeDiffInSec;
	}

	public boolean isAutoMachineDetected() {
		return autoMachineDetected;
	}

	public void setAutoMachineDetected(boolean autoMachineDetected) {
		this.autoMachineDetected = autoMachineDetected;
	}

	public String getCallDetectedAs() {
		return callDetectedAs;
	}

	public void setCallDetectedAs(String callDetectedAs) {
		this.callDetectedAs = callDetectedAs;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public long getAmdTimeDiffInMilliSec() {
		return amdTimeDiffInMilliSec;
	}

	public void setAmdTimeDiffInMilliSec(long amdTimeDiffInMilliSec) {
		this.amdTimeDiffInMilliSec = amdTimeDiffInMilliSec;
	}

	public boolean isEmailRevealed() {
		return emailRevealed;
	}

	public void setEmailRevealed(boolean emailRevealed) {
		this.emailRevealed = emailRevealed;
	}

	public String getCallbackAmPm() {
		return callbackAmPm;
	}

	public void setCallbackAmPm(String callbackAmPm) {
		this.callbackAmPm = callbackAmPm;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}
}
