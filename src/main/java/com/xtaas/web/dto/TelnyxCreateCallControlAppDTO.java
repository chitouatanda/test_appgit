package com.xtaas.web.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class TelnyxCreateCallControlAppDTO {

	@JsonProperty("application_name")
	private String application_name;

	@JsonProperty("webhook_event_url")
	private String webhook_event_url;

	@JsonProperty("outbound")
	private TelnyxOutboundVoiceProfileDTO outbound;

	@JsonProperty("webhook_api_version")
	private String webhook_api_version;
	
	public TelnyxCreateCallControlAppDTO() {
		this.outbound = new TelnyxOutboundVoiceProfileDTO();
	}

	public String getApplication_name() {
		return application_name;
	}

	public void setApplication_name(String application_name) {
		this.application_name = application_name;
	}

	public String getWebhook_event_url() {
		return webhook_event_url;
	}

	public void setWebhook_event_url(String webhook_event_url) {
		this.webhook_event_url = webhook_event_url;
	}

	public TelnyxOutboundVoiceProfileDTO getOutbound() {
		return outbound;
	}

	public void setOutbound(TelnyxOutboundVoiceProfileDTO outbound) {
		this.outbound = outbound;
	}

	public String getWebhook_api_version() {
		return webhook_api_version;
	}

	public void setWebhook_api_version(String webhook_api_version) {
		this.webhook_api_version = webhook_api_version;
	}

	@Override
	public String toString() {
		return "TelnyxCreateCallControlAppDTO [application_name=" + application_name + ", webhook_event_url="
				+ webhook_event_url + ", outbound=" + outbound + ", webhook_api_version=" + webhook_api_version + "]";
	}

}