package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.ExpressionGroup;
import com.xtaas.domain.valueobject.QualificationCriteria;

public class ExpressionGroupDTO {
	@JsonProperty
	@NotNull
	private String groupClause; // AND, OR
	
	@JsonProperty
	private List<ExpressionDTO> expressions;

	@JsonProperty
	private List<ExpressionGroupDTO> expressionGroups;
	
	public ExpressionGroupDTO() {
	}
	
	public ExpressionGroupDTO(ExpressionGroup expressionGroup) {
		this.groupClause = expressionGroup.getGroupClause();
		if (expressionGroup.getExpressions() != null) {
			this.expressions = new ArrayList<ExpressionDTO>();
			for (Expression expression : expressionGroup.getExpressions()) {
				this.expressions.add(new ExpressionDTO(expression));
			}
		}
		if (expressionGroup.getExpressionGroups() != null) {
			this.expressionGroups = new ArrayList<ExpressionGroupDTO>();
			for (ExpressionGroup tmpExpressionGroup : expressionGroup.getExpressionGroups()) {
				this.expressionGroups.add(new ExpressionGroupDTO(tmpExpressionGroup));
			}
		}
	}
	
	public ExpressionGroup toExpressionGroup() {
		List<Expression> tmpExpressions = null;
		List<ExpressionGroup> tmpExpressionGroups = null;
		
		if (expressions != null) {
			tmpExpressions = new ArrayList<Expression>();
			for (ExpressionDTO expressionDTO : expressions) {
				tmpExpressions.add(expressionDTO.toExpression());
			}
		}
		if (expressionGroups != null) {
			tmpExpressionGroups = new ArrayList<ExpressionGroup>();
			for (ExpressionGroupDTO expressionGroupDTO : expressionGroups) {
				tmpExpressionGroups.add(expressionGroupDTO.toExpressionGroup());
			}
		}
		return new ExpressionGroup(groupClause, tmpExpressions, tmpExpressionGroups);
	}
	
	// TODO This is really a bad way of implementing single responsibility principle
	public QualificationCriteria toQualificationCriteria() {
		List<Expression> tmpExpressions = null;
		List<ExpressionGroup> tmpExpressionGroups = null;
		
		if (expressions != null) {
			tmpExpressions = new ArrayList<Expression>();
			for (ExpressionDTO expressionDTO : expressions) {
				tmpExpressions.add(expressionDTO.toExpression());
			}
		}
		if (expressionGroups != null) {
			tmpExpressionGroups = new ArrayList<ExpressionGroup>();
			for (ExpressionGroupDTO expressionGroupDTO : expressionGroups) {
				tmpExpressionGroups.add(expressionGroupDTO.toExpressionGroup());
			}
		}
		return new QualificationCriteria(groupClause, tmpExpressions, tmpExpressionGroups);
	}
}
