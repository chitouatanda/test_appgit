package com.xtaas.web.dto;

public enum AgentSearchClauses {
	ByAll, ByPreviousCampaign, BySearch, ByAutoSuggest, ByCurrentInvitation, ByCurrentSelection, BySupervisor, ByQa, ByCampaign, ByQa2
}
