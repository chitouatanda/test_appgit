/**
 * 
 */
package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.HashMap;

import com.xtaas.domain.valueobject.AgentSearchResult;

/**
 * @author djain
 *
 */
public class AgentAllocationDTO {
	public HashMap<String, ArrayList<AgentSearchResult>> agentAllocationMap;
	
	public AgentAllocationDTO() {
		this.agentAllocationMap = new HashMap<String, ArrayList<AgentSearchResult>>();
	}
	
	public void addAllocation(String campaignId, AgentSearchResult agent) {
		ArrayList<AgentSearchResult> agentList = this.agentAllocationMap.get(campaignId);
		if (agentList == null) {
			agentList = new ArrayList<AgentSearchResult>();
			this.agentAllocationMap.put(campaignId, agentList);
		}
		agentList.add(agent);
	}
}
