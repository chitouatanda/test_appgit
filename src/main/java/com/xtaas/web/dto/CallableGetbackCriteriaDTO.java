package com.xtaas.web.dto;

import java.util.List;

public class CallableGetbackCriteriaDTO {

	private List<String> campaignIds;
	private String dispositionStatus;
	private String subStatus;
	private String status;
	private int minutes;

	@Override
	public String toString() {
		return "CallableGetbackCriteriaDTO [campaignIds=" + campaignIds + ", dispositionStatus=" + dispositionStatus
				+ ", subStatus=" + subStatus + ", status=" + status + ", minutes=" + minutes + "]";
	}

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

}
