package com.xtaas.web.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.TeamNotice;
import com.xtaas.domain.valueobject.NoticePriority;
import com.xtaas.domain.valueobject.NoticeType;

@JsonIgnoreProperties
public class TeamNoticeDTO {
	@JsonProperty
	private NoticeType type;
	@JsonProperty
	private String notice;
	@JsonProperty
	private NoticePriority priority;
	@JsonProperty
	private Date startDate;
	@JsonProperty
	private Date endDate;
	@JsonProperty
	private List<String> forTeams;  //list of teamIds, in case it needs to be published to entire team
	@JsonProperty
	private List<String> forResources;  //list of agentIds or qaIds, in case it needs to be published to a bunch of resources 
	@JsonProperty
	private String timeZone;
	@JsonProperty
	private String requestorId;
	@JsonProperty
	private String link;
	
	/**
	 * 
	 */
	public TeamNoticeDTO() {
		
	}
	
	/**
	 * 
	 */
	public TeamNoticeDTO(TeamNotice teamNotice) {
		this.setNotice(teamNotice.getNotice());
		this.setType(teamNotice.getType());
		this.setPriority(teamNotice.getPriority());
		this.setStartDate(teamNotice.getStartDate());
		this.setEndDate(teamNotice.getEndDate());
		this.setRequestorId(teamNotice.getRequestorId());
	}
	
	public TeamNotice toTeamNotice() {
		TeamNotice teamNotice = new TeamNotice();
		teamNotice.setNotice(notice);
		teamNotice.setType(type);
		teamNotice.setPriority(priority);
		teamNotice.setStartDate(startDate);
		teamNotice.setEndDate(endDate);
		teamNotice.setRequestorId(requestorId);
		return teamNotice;
	}

	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the type
	 */
	public NoticeType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(NoticeType type) {
		this.type = type;
	}

	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/**
	 * @return the priority
	 */
	public NoticePriority getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(NoticePriority priority) {
		this.priority = priority;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the forTeams
	 */
	public List<String> getForTeams() {
		return forTeams;
	}

	/**
	 * @param forTeams the forTeams to set
	 */
	public void setForTeams(List<String> forTeams) {
		this.forTeams = forTeams;
	}

	/**
	 * @return the forResources
	 */
	public List<String> getForResources() {
		return forResources;
	}

	/**
	 * @param forResources the forResources to set
	 */
	public void setForResources(List<String> forResources) {
		this.forResources = forResources;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	
	
}
