package com.xtaas.web.dto;

public class CallStatsDTO {
	private String hours;
	private String campaignId;
	private String campaignName;
	private String type;
	private int leads;
	private double dialsPerSuccess;
	private int attempt;
	private int contact;
	private int answerMachine;
	private int connect;
	private int abandoned;
	private double abandonedPercent;
	private double contactPercent;
	private double connectPercent;
	private double onlineTimeHoursVal;
	private int handleTimeHoursVal;
	private double leadPerHandleTimeHour;
	private double successPerDials;
	private double successPerContacts;
	private double successPerConnects;
	private double contactPerDial;
	private double connectsPerDial;
	private double connectsPerContacts;
	private int callableCount;
	
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getLeads() {
		return leads;
	}
	public void setLeads(int leads) {
		this.leads = leads;
	}
	public double getDialsPerSuccess() {
		return dialsPerSuccess;
	}
	public void setDialsPerSuccess(double dialsPerSuccess) {
		this.dialsPerSuccess = dialsPerSuccess;
	}
	public int getAttempt() {
		return attempt;
	}
	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}
	public int getContact() {
		return contact;
	}
	public void setContact(int contact) {
		this.contact = contact;
	}
	public int getAnswerMachine() {
		return answerMachine;
	}
	public void setAnswerMachine(int answerMachine) {
		this.answerMachine = answerMachine;
	}
	public int getConnect() {
		return connect;
	}
	public void setConnect(int connect) {
		this.connect = connect;
	}
	public int getAbandoned() {
		return abandoned;
	}
	public void setAbandoned(int abandoned) {
		this.abandoned = abandoned;
	}
	public double getAbandonedPercent() {
		return abandonedPercent;
	}
	public void setAbandonedPercent(double abandonedPercent) {
		this.abandonedPercent = abandonedPercent;
	}
	public double getContactPercent() {
		return contactPercent;
	}
	public void setContactPercent(double contactPercent) {
		this.contactPercent = contactPercent;
	}
	public double getConnectPercent() {
		return connectPercent;
	}
	public void setConnectPercent(double connectPercent) {
		this.connectPercent = connectPercent;
	}
	
	public int getHandleTimeHoursVal() {
		return handleTimeHoursVal;
	}
	public void setHandleTimeHoursVal(int handleTimeHoursVal) {
		this.handleTimeHoursVal = handleTimeHoursVal;
	}
	public double getLeadPerHandleTimeHour() {
		return leadPerHandleTimeHour;
	}
	public void setLeadPerHandleTimeHour(double leadPerHandleTimeHour) {
		this.leadPerHandleTimeHour = leadPerHandleTimeHour;
	}
	public double getSuccessPerDials() {
		return successPerDials;
	}
	public void setSuccessPerDials(double successPerDials) {
		this.successPerDials = successPerDials;
	}
	public double getSuccessPerContacts() {
		return successPerContacts;
	}
	public void setSuccessPerContacts(double successPerContacts) {
		this.successPerContacts = successPerContacts;
	}
	public double getSuccessPerConnects() {
		return successPerConnects;
	}
	public void setSuccessPerConnects(double successPerConnects) {
		this.successPerConnects = successPerConnects;
	}
	public double getContactPerDial() {
		return contactPerDial;
	}
	public void setContactPerDial(double contactPerDial) {
		this.contactPerDial = contactPerDial;
	}
	public double getConnectsPerDial() {
		return connectsPerDial;
	}
	public void setConnectsPerDial(double connectsPerDial) {
		this.connectsPerDial = connectsPerDial;
	}
	public double getConnectsPerContacts() {
		return connectsPerContacts;
	}
	public void setConnectsPerContacts(double connectsPerContacts) {
		this.connectsPerContacts = connectsPerContacts;
	}
	public int getCallableCount() {
		return callableCount;
	}
	public void setCallableCount(int callableCount) {
		this.callableCount = callableCount;
	}
	public double getOnlineTimeHoursVal() {
		return onlineTimeHoursVal;
	}
	public void setOnlineTimeHoursVal(double onlineTimeHoursVal) {
		this.onlineTimeHoursVal = onlineTimeHoursVal;
	}

}
