package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"phone_number"
})
public class TelnyxPhoneOrderRequestPhoneNumber {

@JsonProperty("phone_number")
private String phoneNumber;

@JsonProperty("phone_number")
public String getPhoneNumber() {
return phoneNumber;
}

@JsonProperty("phone_number")
public void setPhoneNumber(String phoneNumber) {
this.phoneNumber = phoneNumber;
}

}