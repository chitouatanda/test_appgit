package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Phone;

public class PhoneDTO {

	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String number;
	
	@JsonProperty
	private boolean isPreferred;

	public PhoneDTO() {
	
	}

	public PhoneDTO(Phone phone) {
		this.name = phone.getName();
		this.number = phone.getNumber();
		this.isPreferred = phone.isPreferred();
	}
	
	public Phone toPhone() {
		return new Phone(name, number, isPreferred);
	}

	public String getName() {
		return name;
	}

	public String getNumber() {
		return number;
	}

	public boolean isPreferred() {
		return isPreferred;
	}
	
	
	
	
	
}
