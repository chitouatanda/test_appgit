package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"companyInfo",
"emails",
"phones",
"title"
})
public class LeadIQCurrentPosition {

@JsonProperty("companyInfo")
private LeadIQCompanyInfo companyInfo;
@JsonProperty("emails")
private List<LeadIQEmail> emails = null;
@JsonProperty("phones")
private List<LeadIQPhone> phones = null;
@JsonProperty("title")
private String title;

@JsonProperty("companyInfo")
public LeadIQCompanyInfo getCompanyInfo() {
return companyInfo;
}

@JsonProperty("companyInfo")
public void setCompanyInfo(LeadIQCompanyInfo companyInfo) {
this.companyInfo = companyInfo;
}

@JsonProperty("emails")
public List<LeadIQEmail> getEmails() {
return emails;
}

@JsonProperty("emails")
public void setEmails(List<LeadIQEmail> emails) {
this.emails = emails;
}

@JsonProperty("phones")
public List<LeadIQPhone> getPhones() {
return phones;
}

@JsonProperty("phones")
public void setPhones(List<LeadIQPhone> phones) {
this.phones = phones;
}

@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("companyInfo", companyInfo).append("emails", emails).append("phones", phones).append("title", title).toString();
}

}