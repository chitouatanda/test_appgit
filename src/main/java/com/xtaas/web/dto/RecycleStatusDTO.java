package com.xtaas.web.dto;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.RecycleStatusEnum;

public class RecycleStatusDTO {
	
	@JsonProperty
	private RecycleStatusEnum status;
	
	@JsonProperty
	private int withinCampaignCount;
	
	@JsonProperty
	private int acrossCampaignCount;
	
	@JsonProperty
	private boolean withinCampaignSelected;
	
	@JsonProperty
	private boolean acrossCampaignSelected;

	public RecycleStatusEnum getStatus() {
		return status;
	}

	public void setStatus(RecycleStatusEnum status) {
		this.status = status;
	}

	public int getWithinCampaignCount() {
		return withinCampaignCount;
	}

	public void setWithinCampaignCount(int withinCampaignCount) {
		this.withinCampaignCount = withinCampaignCount;
	}

	public int getAcrossCampaignCount() {
		return acrossCampaignCount;
	}

	public void setAcrossCampaignCount(int acrossCampaignCount) {
		this.acrossCampaignCount = acrossCampaignCount;
	}

	public boolean isWithinCampaignSelected() {
		return withinCampaignSelected;
	}

	public void setWithinCampaignSelected(boolean withinCampaignSelected) {
		this.withinCampaignSelected = withinCampaignSelected;
	}

	public boolean isAcrossCampaignSelected() {
		return acrossCampaignSelected;
	}

	public void setAcrossCampaignSelected(boolean acrossCampaignSelected) {
		this.acrossCampaignSelected = acrossCampaignSelected;
	}
}
