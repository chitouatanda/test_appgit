package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IndirectProspectsCacheDTO {
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	@JsonProperty
	private String title;
	@JsonProperty
	private String department;
	@JsonProperty
	private String company;
	@JsonProperty
	private String prospectCallId;

	public IndirectProspectsCacheDTO(String firstName, String lastName, String title, String department, String company,
			String prospectCallId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.department = department;
		this.company = company;
		this.prospectCallId = prospectCallId;
	}

	public IndirectProspectsCacheDTO() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

}
