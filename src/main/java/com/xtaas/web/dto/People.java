package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class People {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("linkedinId")
    private String linkedinId;

    @JsonProperty("linkedinUrl")
    private String linkedinUrl;

    @JsonProperty("title")
    private String title;

    @JsonProperty("personalEmails")
    private Object personalEmails;

    @JsonProperty("personalPhones")
    private Object personalPhones;

    @JsonProperty("workEmails")
    private List<Object> workEmails;

    @JsonProperty("workPhones")
    private List<Object>  workPhones;

    @JsonProperty("role")
    private String role;

    @JsonProperty("seniority")
    private String seniority;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getLinkedinUrl() {
        return linkedinUrl;
    }

    public void setLinkedinUrl(String linkedinUrl) {
        this.linkedinUrl = linkedinUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getPersonalEmails() {
        return personalEmails;
    }

    public void setPersonalEmails(Object personalEmails) {
        this.personalEmails = personalEmails;
    }

    public Object getPersonalPhones() {
        return personalPhones;
    }

    public void setPersonalPhones(Object personalPhones) {
        this.personalPhones = personalPhones;
    }

    public List<Object> getWorkEmails() {
        return workEmails;
    }

    public void setWorkEmails(List<Object> workEmails) {
        this.workEmails = workEmails;
    }

    public List<Object> getWorkPhones() {
        return workPhones;
    }

    public void setWorkPhones(List<Object> workPhones) {
        this.workPhones = workPhones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSeniority() {
        return seniority;
    }

    public void setSeniority(String seniority) {
        this.seniority = seniority;
    }

    @Override
    public String toString() {
        return "People{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", linkedinId='" + linkedinId + '\'' +
                ", linkedinUrl='" + linkedinUrl + '\'' +
                ", title='" + title + '\'' +
                ", personalEmails=" + personalEmails +
                ", personalPhones=" + personalPhones +
                ", workEmails=" + workEmails +
                ", workPhones=" + workPhones +
                ", role='" + role + '\'' +
                ", seniority='" + seniority + '\'' +
                '}';
    }
}
