package com.xtaas.web.dto;

import java.util.List;

public class GeoCodingDTO {

	private List<GeoCodingDetilsDTO> results;

	private String status;

	public GeoCodingDTO() {
	}

	public List<GeoCodingDetilsDTO> getResults() {
		return results;
	}

	public void setResults(List<GeoCodingDetilsDTO> results) {
		this.results = results;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

}
