package com.xtaas.web.dto;

import java.util.List;

public class EverStringMultipleCompanyRequest {

	private List<EverStringDomainDTO> companies;

	public List<EverStringDomainDTO> getCompanies() {
		return companies;
	}

	public void setCompanies(List<EverStringDomainDTO> companies) {
		this.companies = companies;
	}
	
}
