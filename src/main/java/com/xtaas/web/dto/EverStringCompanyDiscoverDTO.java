package com.xtaas.web.dto;

import java.io.Serializable;

public class EverStringCompanyDiscoverDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private String domain;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
