package com.xtaas.web.dto;

import java.util.List;
import java.util.Map;

public class CustomBuyDTO {

    public String campaignId;
    public String country;
    public String department;
    public String industry;
    public String managementLevel;
    public String title;
    public String phoneType;
    public String minEmp;
    public String maxEmp;
    public String minRev;
    public String maxRev;
    public int dataBuyRequest;
    public boolean restFullAPI;
    public String source;

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getManagementLevel() {
        return managementLevel;
    }

    public void setManagementLevel(String managementLevel) {
        this.managementLevel = managementLevel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getMinEmp() {
        return minEmp;
    }

    public void setMinEmp(String minEmp) {
        this.minEmp = minEmp;
    }

    public String getMaxEmp() {
        return maxEmp;
    }

    public void setMaxEmp(String maxEmp) {
        this.maxEmp = maxEmp;
    }

    public String getMinRev() {
        return minRev;
    }

    public void setMinRev(String minRev) {
        this.minRev = minRev;
    }

    public String getMaxRev() {
        return maxRev;
    }

    public void setMaxRev(String maxRev) {
        this.maxRev = maxRev;
    }

    public int getDataBuyRequest() {
        return dataBuyRequest;
    }

    public void setDataBuyRequest(int dataBuyRequest) {
        this.dataBuyRequest = dataBuyRequest;
    }

    public boolean isRestFullAPI() {
        return restFullAPI;
    }

    public void setRestFullAPI(boolean restFullAPI) {
        this.restFullAPI = restFullAPI;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
