package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.FeedbackSectionResponse;
import com.xtaas.domain.valueobject.QaFeedback;

public class QaFeedbackDTO {
	@JsonProperty
	private String qaId;
	
	@JsonProperty
	private String secQaId;
	
	@JsonProperty
	private Double overallScore;
	
	@JsonProperty
	private List<FeedbackSectionResponseDTO> feedbackResponseList;
	
	@JsonProperty
	private Date feedbackTime;
	
	@JsonProperty
	private String agentNotes;
	
	/* START 
	 * DATE : 16/05/2017
	 * REASON : Below properties are added for accessing "isAnswerValid" and "answerCertification" fields on QADetails page*/
	/*@JsonProperty
	private HashMap<String, String> isAnswerValid;
	
	@JsonProperty
	private HashMap<String, String> answerCertification;*/
	/* END */
	
	@JsonProperty
	private List<AgentQaAnswer> qaAnswers;
			
	public QaFeedbackDTO() {
		
	}
	
	/**
	 * @param qaId
	 * 
	 * @param overallScore
	 * @param feedbackSectionResponse
	 * @param feedbackTime
	 */
	public QaFeedbackDTO(QaFeedback qaFeedback) {
		this.qaId = qaFeedback.getQaId();
		this.secQaId = qaFeedback.getSecQaId();
		this.overallScore = qaFeedback.getOverallScore();
		
		if (qaFeedback.getFeedbackResponseList() != null) {
			this.feedbackResponseList = new ArrayList<FeedbackSectionResponseDTO>();
			for (FeedbackSectionResponse feedbackSectionResponse : qaFeedback.getFeedbackResponseList()) {
				FeedbackSectionResponseDTO feedbackSectionResponseDTO = new FeedbackSectionResponseDTO(feedbackSectionResponse);
				feedbackSectionResponseDTO.setComments(feedbackSectionResponse.getComments());
				this.feedbackResponseList.add(feedbackSectionResponseDTO);
			}
		}
		
		this.feedbackTime = qaFeedback.getFeedbackTime();
		/*this.isAnswerValid = qaFeedback.getIsAnswerValid();
		this.answerCertification = qaFeedback.getAnswerCertification();*/
		this.qaAnswers = qaFeedback.getQaAnswers();
		this.agentNotes = qaFeedback.getAgentNotes();
	}
	
	public QaFeedback toQaFeedbackResponse () {
		List<FeedbackSectionResponse>  feedbackSectionResponseList = null;
		if (this.feedbackResponseList != null) {
			feedbackSectionResponseList = new ArrayList<FeedbackSectionResponse>();
			for (FeedbackSectionResponseDTO feedbackSectionResponseDTO : this.feedbackResponseList) {
				FeedbackSectionResponse feedbackSectionResponse = feedbackSectionResponseDTO.toFeedbackSectionResponse();
				feedbackSectionResponse.setComments(feedbackSectionResponseDTO.getComments());
				feedbackSectionResponseList.add(feedbackSectionResponse);
			}
		}
		return new QaFeedback(qaId, secQaId, feedbackSectionResponseList, overallScore, feedbackTime, qaAnswers, agentNotes/*, isAnswerValid, answerCertification*/);
	}

	/**
	 * @return the qaId
	 */
	public String getQaId() {
		return qaId;
	}

	/**
	 * @return the overallScore
	 */
	public Double getOverallScore() {
		return overallScore;
	}
	/**
	 * @return the feedbackResponseList
	 */
	public List<FeedbackSectionResponseDTO> getFeedbackResponseList() {
		return feedbackResponseList;
	}

	/**
	 * @return the feedbackTime
	 */
	public Date getFeedbackTime() {
		return feedbackTime;
	}
	
	/* START
	 * DATE : 16/05/2017
	 * REASON: Below getters are added to get values of "isAnswerValid" & "answerCertification" */
	/*public HashMap<String, String> getIsAnswerValid() {
		return isAnswerValid;
	}

	public HashMap<String, String> getAnswerCertification() {
		return answerCertification;
	}*/
	/* END */

	public List<AgentQaAnswer> getQaAnswers() {
		return qaAnswers;
	}

	public void setQaAnswers(List<AgentQaAnswer> qaAnswers) {
		this.qaAnswers = qaAnswers;
	}

	public String getSecQaId() {
		return secQaId;
	}

	public String getAgentNotes() {
		return agentNotes;
	}

	public void setAgentNotes(String agentNotes) {
		this.agentNotes = agentNotes;
	}
	
}
