package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"data"
})

public class LeadIQSearchResponse {

@JsonProperty("data")
private LeadIQData data;

@JsonProperty("errors")
private List<LeadIQError> errors = null;



@JsonProperty("data")
public LeadIQData getData() {
return data;
}

@JsonProperty("data")
public void setData(LeadIQData data) {
this.data = data;
}

@JsonProperty("errors")
public List<LeadIQError> getErrors() {
return errors;
}

@JsonProperty("errors")
public void setErrors(List<LeadIQError> errors) {
this.errors = errors;
}

@Override
public String toString() {
  return "LeadIQSearchResponse [data=" + data + ", errors=" + errors + "]";
}

}