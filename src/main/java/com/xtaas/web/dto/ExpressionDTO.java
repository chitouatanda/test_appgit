package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.domain.valueobject.Expression;

/**
 * @author djain
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpressionDTO {
	@JsonProperty
	@NotNull
	private String attribute;
	
	@JsonProperty
	@NotNull
	private String operator;
	
	@JsonProperty
	@NotNull
	private List<String> value;
	
	@JsonProperty
	@NotNull
	private String questionSkin;
	
	@JsonProperty
	@NotNull
	private  ArrayList<PickListItem> pickList;
	
	/* START
	 * DATE : 10/05/2017
	 * REASON : Below property is added for handling CheckBox value on "edit campaign" page. Which will decide whether question will be shown on agent screen or not */
	@JsonProperty
	@NotNull
	private Boolean agentValidationRequired;
	/*END*/
	
	@JsonProperty
	private String sequenceNumber;
		
	public ExpressionDTO() {
	}
	
	public ExpressionDTO(Expression expression) {
		  attribute = expression.getAttribute();
		  operator = expression.getOperator();
		  if (expression.getValue() != null) {
			value = Arrays.asList(expression.getValue().split(","));
		  }
		  agentValidationRequired = expression.getAgentValidationRequired();
		  questionSkin = expression.getQuestionSkin();
		  pickList = expression.getPickList();
		  sequenceNumber = expression.getSequenceNumber();
		 }
	
	public Expression toExpression() {
		return new Expression(attribute, operator, StringUtils.join(value,","), agentValidationRequired, questionSkin, pickList, sequenceNumber);
	}
}
