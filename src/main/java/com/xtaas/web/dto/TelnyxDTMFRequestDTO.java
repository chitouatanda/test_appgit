package com.xtaas.web.dto;

public class TelnyxDTMFRequestDTO {
  
  private String callLegId;
  
  private String digits;

  public String getCallLegId() {
    return callLegId;
  }

  public void setCallLegId(String callLegId) {
    this.callLegId = callLegId;
  }

  public String getDigits() {
    return digits;
  }

  public void setDigits(String digits) {
    this.digits = digits;
  }
  
  
}