package com.xtaas.web.dto;

public class RedialOutboundNumberDTO {

	private String phone;

	private int callRetryCount;

	private String country;

	private String organizationId;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@Override
	public String toString() {
		return "RedialOutboundNumberDTO [phone=" + phone + ", callRetryCount=" + callRetryCount + ", country=" + country
				+ ", organizationId=" + organizationId + "]";
	}

}