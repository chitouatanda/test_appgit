package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PivotStatusDTO {
	
	@JsonProperty
	private boolean activateStatus;
	
	@JsonProperty
	private boolean downloadStatus;
	
	@JsonProperty
	private boolean uploadStatus;

	public boolean isActivateStatus() {
		return activateStatus;
	}

	public void setActivateStatus(boolean activateStatus) {
		this.activateStatus = activateStatus;
	}

	public boolean isDownloadStatus() {
		return downloadStatus;
	}

	public void setDownloadStatus(boolean downloadStatus) {
		this.downloadStatus = downloadStatus;
	}

	public boolean isUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(boolean uploadStatus) {
		this.uploadStatus = uploadStatus;
	}
}
