package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignManagerMetricsDTO {

	@JsonProperty
	private String id;
	
	@JsonProperty
	private int createdCampaigns;
	
	@JsonProperty
	private int completedCampaigns;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the createdCampaigns
	 */
	public int getCreatedCampaigns() {
		return createdCampaigns;
	}

	/**
	 * @param createdCampaigns the createdCampaigns to set
	 */
	public void setCreatedCampaigns(int createdCampaigns) {
		this.createdCampaigns = createdCampaigns;
	}

	/**
	 * @return the completedCampaigns
	 */
	public int getCompletedCampaigns() {
		return completedCampaigns;
	}

	/**
	 * @param completedCampaigns the completedCampaigns to set
	 */
	public void setCompletedCampaigns(int completedCampaigns) {
		this.completedCampaigns = completedCampaigns;
	}

	
}
