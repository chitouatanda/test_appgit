package com.xtaas.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.TeamResource;

public class TeamDTO {

	@JsonProperty
	private String id;

	@JsonProperty
	private String partnerId;

	@JsonProperty
	private String name;

	@JsonProperty
	private String overview;

	@JsonProperty
	private double perLeadPrice;

	@JsonProperty
	private AddressDTO address;

	@JsonProperty
	private List<String> adminSupervisorIds;

	@JsonProperty
	private TeamResourceDTO supervisor;

	@JsonProperty
	private List<TeamResourceDTO> agents;

	@JsonProperty
	private List<TeamResourceDTO> qas;

	public TeamDTO() {

	}

	public TeamDTO(Team team) {
		this.id = team.getId();
		this.partnerId = team.getPartnerId();
		this.name = team.getName();
		this.overview = team.getOverview();
		this.perLeadPrice = team.getPerLeadPrice();
		if (team.getAddress() != null) {
			this.address = new AddressDTO(team.getAddress());
		}
		this.adminSupervisorIds = team.getAdminSupervisorIds();
		this.supervisor = new TeamResourceDTO(team.getSupervisor());
		if (team.getAgents() != null) {
			this.agents = new ArrayList<TeamResourceDTO>();
			for (TeamResource teamResource : team.getAgents()) {
				this.agents.add(new TeamResourceDTO(teamResource));
			}
		}
		if (team.getQas() != null) {
			this.qas = new ArrayList<TeamResourceDTO>();
			for (TeamResource teamResource : team.getQas()) {
				this.qas.add(new TeamResourceDTO(teamResource));
			}
		}
	}

	public String getId() {
		return id;
	}

	public List<TeamResourceDTO> getAgents() {
		return agents;
	}

	public List<TeamResourceDTO> getQas() {
		return qas;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getName() {
		return name;
	}

	public String getOverview() {
		return overview;
	}

	public double getPerLeadPrice() {
		return perLeadPrice;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public TeamResourceDTO getSupervisor() {
		return supervisor;
	}

	public List<String> getAdminSupervisorIds() {
		return adminSupervisorIds;
	}

}
