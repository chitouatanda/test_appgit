package com.xtaas.web.dto;

public class HealthSearchDTO {

	private long departmentHealthCount;

	private long titleHealthCount;

	private long industryHealthCount;

	private long employeeHealthCount;

	private long revenueHealthCount;
	
	private long countryHealthCount;
	
	private long sicCodeHealthCount;
	
	private long stateHealthCount;
	
	private long zipHealthCount;

	private long totalRecords;

	public long getDepartmentHealthCount() {
		return departmentHealthCount;
	}

	public void setDepartmentHealthCount(long departmentHealthCount) {
		this.departmentHealthCount = departmentHealthCount;
	}

	public long getTitleHealthCount() {
		return titleHealthCount;
	}

	public void setTitleHealthCount(long titleHealthCount) {
		this.titleHealthCount = titleHealthCount;
	}

	public long getIndustryHealthCount() {
		return industryHealthCount;
	}

	public void setIndustryHealthCount(long industryHealthCount) {
		this.industryHealthCount = industryHealthCount;
	}

	public long getEmployeeHealthCount() {
		return employeeHealthCount;
	}

	public void setEmployeeHealthCount(long employeeHealthCount) {
		this.employeeHealthCount = employeeHealthCount;
	}

	public long getRevenueHealthCount() {
		return revenueHealthCount;
	}

	public void setRevenueHealthCount(long revenueHealthCount) {
		this.revenueHealthCount = revenueHealthCount;
	}

	public long getCountryHealthCount() {
		return countryHealthCount;
	}

	public void setCountryHealthCount(long countryHealthCount) {
		this.countryHealthCount = countryHealthCount;
	}

	public long getSicCodeHealthCount() {
		return sicCodeHealthCount;
	}

	public void setSicCodeHealthCount(long sicCodeHealthCount) {
		this.sicCodeHealthCount = sicCodeHealthCount;
	}

	public long getStateHealthCount() {
		return stateHealthCount;
	}

	public void setStateHealthCount(long stateHealthCount) {
		this.stateHealthCount = stateHealthCount;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public long getZipHealthCount() {
		return zipHealthCount;
	}

	public void setZipHealthCount(long zipHealthCount) {
		this.zipHealthCount = zipHealthCount;
	}
	
	
}
