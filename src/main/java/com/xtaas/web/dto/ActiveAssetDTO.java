package com.xtaas.web.dto;

import java.util.HashMap;
import java.util.List;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActiveAssetDTO {

	@JsonProperty
	private List<HashMap<String, Integer>> assetTargetMap;

	@JsonProperty
	@NotNull
	private String campaignId;

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public List<HashMap<String, Integer>> getAssetTargetMap() {
		return assetTargetMap;
	}

	public void setAssetTargetMap(List<HashMap<String, Integer>> assetTargetMap) {
		this.assetTargetMap = assetTargetMap;
	}

}