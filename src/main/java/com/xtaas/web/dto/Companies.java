package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Companies {

    @JsonProperty("company")
    private Company company;

    @JsonProperty("people")
    private List<People> people;


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<People> getPeople() {
        return people;
    }

    public void setPeople(List<People> people) {
        this.people = people;
    }


    @Override
    public String toString() {
        return "Companies{" +
                "company=" + company +
                ", people=" + people +
                '}';
    }
}
