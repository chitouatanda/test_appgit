package com.xtaas.web.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.TeamSearchResult;
@JsonInclude(Include.NON_NULL)
public class TeamSearchResultDTO {
	@Id
	@JsonProperty
	private String id;
	@JsonProperty
	private String teamId;
	@JsonProperty
	private String name;
	@JsonProperty
	private String overview;
	@JsonProperty
	private Integer teamSize;
	@JsonProperty
	private Double perLeadPrice;
	@JsonProperty
	private Integer totalCompletedCampaigns;
	@JsonProperty
	private Double averageRating;
	@JsonProperty
	private Integer totalVolume;
	@JsonProperty
	private Double averageConversion;
	@JsonProperty
	private Double averageQuality;
	@JsonProperty
	private Integer averageAvailableHours;
	@JsonProperty
	private Date invitationDate;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String campaignName;
	@JsonProperty
	private Date campaignCreatedDate;
	
	
	/*
	public TeamSearchResultDTO() {
		
	}
	*/
	
	
	/*public TeamSearchResultDTO(TeamSearchResult teamSearchResult) {
		this.id = teamSearchResult.getId();
		this.teamId = teamSearchResult.getTeamId();
		this.name = teamSearchResult.getName();
		this.overview = teamSearchResult.getOverview();
		this.teamSize = teamSearchResult.getTeamSize();
		this.perLeadPrice = teamSearchResult.getPerLeadPrice();
		this.totalCompletedCampaigns = teamSearchResult.getTotalCompletedCampaigns();
		this.averageRating = teamSearchResult.getAverageRating();
		this.totalVolume = teamSearchResult.getTotalVolume();
		this.averageConversion = teamSearchResult.getAverageConversion();
		this.averageQuality = teamSearchResult.getAverageQuality();
		this.averageAvailableHours = teamSearchResult.getAverageAvailableHours();
		this.invitationDate = teamSearchResult.getInvitationDate();
	}
*/


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @param teamId the teamId to set
	 */
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}



	/**
	 * @param teamSize the teamSize to set
	 */
	public void setTeamSize(Integer teamSize) {
		this.teamSize = teamSize;
	}



	/**
	 * @param perLeadPrice the perLeadPrice to set
	 */
	public void setPerLeadPrice(Double perLeadPrice) {
		this.perLeadPrice = perLeadPrice;
	}



	/**
	 * @param totalCompletedCampaigns the totalCompletedCampaigns to set
	 */
	public void setTotalCompletedCampaigns(Integer totalCompletedCampaigns) {
		this.totalCompletedCampaigns = totalCompletedCampaigns;
	}



	/**
	 * @param averageRating the averageRating to set
	 */
	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}



	/**
	 * @param totalVolume the totalVolume to set
	 */
	public void setTotalVolume(Integer totalVolume) {
		this.totalVolume = totalVolume;
	}



	/**
	 * @param averageConversion the averageConversion to set
	 */
	public void setAverageConversion(Double averageConversion) {
		this.averageConversion = averageConversion;
	}



	/**
	 * @param averageQuality the averageQuality to set
	 */
	public void setAverageQuality(Double averageQuality) {
		this.averageQuality = averageQuality;
	}



	/**
	 * @param averageAvailableHours the averageAvailableHours to set
	 */
	public void setAverageAvailableHours(Integer averageAvailableHours) {
		this.averageAvailableHours = averageAvailableHours;
	}



	/**
	 * @param invitationDate the invitationDate to set
	 */
	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}



	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}



	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}



	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}



	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}






	/**
	 * @return the campaignCreatedDate
	 */
	public Date getCampaignCreatedDate() {
		return campaignCreatedDate;
	}



	/**
	 * @param campaignCreatedDate the campaignCreatedDate to set
	 */
	public void setCampaignCreatedDate(Date campaignCreatedDate) {
		this.campaignCreatedDate = campaignCreatedDate;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @return the teamId
	 */
	public String getTeamId() {
		return teamId == null ? id : teamId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}
	/**
	 * @return the teamSize
	 */
	public Integer getTeamSize() {
		return teamSize;
	}
	/**
	 * @return the perLeadPrice
	 */
	public Double getPerLeadPrice() {
		return perLeadPrice;
	}
	/**
	 * @return the totalCompletedCampaigns
	 */
	public Integer getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}
	/**
	 * @return the averageRating
	 */
	public Double getAverageRating() {
		return averageRating;
	}
	/**
	 * @return the totalVolume
	 */
	public Integer getTotalVolume() {
		return totalVolume;
	}
	/**
	 * @return the averageConversion
	 */
	public Double getAverageConversion() {
		return averageConversion;
	}
	/**
	 * @return the averageQuality
	 */
	public Double getAverageQuality() {
		return averageQuality;
	}
	/**
	 * @return the averageAvailableHours
	 */
	public Integer getAverageAvailableHours() {
		return averageAvailableHours;
	}
	/**
	 * @return the invitationDate
	 */
	public Date getInvitationDate() {
		return invitationDate;
	}
	
	
	
}
