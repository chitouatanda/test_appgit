package com.xtaas.web.dto;

import java.util.Date;
import java.util.List;

public class UpdateProspectInfo {

	private String prospectCallId;
	private String status;
	private String dispositionStatus;
	private List<String> prospectCallIds;
	private String leadStatus;
	private String recordingUrl;
	private String recordingUrlAws;
	private String agentId;
	private String supervisorId;
	private String subStatus;
	private String partnerId;
	private int callDuration;
	private Date callStartTime;
	private Integer prospectHandleDuration;

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(String dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public String getRecordingUrlAws() {
		return recordingUrlAws;
	}

	public void setRecordingUrlAws(String recordingUrlAws) {
		this.recordingUrlAws = recordingUrlAws;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public int getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	public Date getCallStartTime() {
		return callStartTime;
	}

	public void setCallStartTime(Date callStartTime) {
		this.callStartTime = callStartTime;
	}

	public Integer getProspectHandleDuration() {
		return prospectHandleDuration;
	}

	public void setProspectHandleDuration(Integer prospectHandleDuration) {
		this.prospectHandleDuration = prospectHandleDuration;
	}

	public List<String> getProspectCallIds() {
        return prospectCallIds;
    }

    public void setProspectCallIds(List<String> prospectCallIds) {
        this.prospectCallIds = prospectCallIds;
    }

	@Override
	public String toString() {
		return "UpdateProspectInfo [prospectCallId=" + prospectCallId + ", status=" + status + ", dispositionStatus="
				+ dispositionStatus + ", leadStatus=" + leadStatus + ", recordingUrl=" + recordingUrl
				+ ", recordingUrlAws=" + recordingUrlAws + ", agentId=" + agentId + ", supervisorId=" + supervisorId
				+ ", subStatus=" + subStatus + ", partnerId=" + partnerId + ", callDuration=" + callDuration
				+ ", callStartTime=" + callStartTime + ", prospectHandleDuration=" + prospectHandleDuration + "]";
	}

}