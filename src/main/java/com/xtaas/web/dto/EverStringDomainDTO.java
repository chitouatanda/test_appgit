package com.xtaas.web.dto;

public class EverStringDomainDTO {

	private String website;

	public EverStringDomainDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public EverStringDomainDTO(String website) {
		super();
		this.website = website;
	}


	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public String toString() {
		return "EverStringDomainDTO [website=" + website + "]";
	}
}
