package com.xtaas.web.dto;

import java.util.Date;
import java.util.List;

public class AgentOnlineReportAPIDTO {

	public List<String> organizations;
	public Date startDate;
	public Date endDate;
	public List<String> emails;

	public List<String> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<String> organizations) {
		this.organizations = organizations;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	@Override
	public String toString() {
		return "AgentOnlineReportAPIDTO [organizations=" + organizations + ", startDate=" + startDate + ", endDate="
				+ endDate + ", emails=" + emails + "]";
	}

}