package com.xtaas.web.dto;

import javax.xml.bind.annotation.XmlElement;

public class Option {

	private String optionId;
	private String optionName;

	public String getOptionId() {
		return optionId;
	}

	@XmlElement(name = "optionid")
	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}

	public String getOptionName() {
		return optionName;
	}

	@XmlElement(name = "optionname")
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	@Override
	public String toString() {
		return "Option [optionId=" + optionId + ", optionName=" + optionName + "]";
	}

}
