package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignWorkroomDTO {
	@JsonProperty
	private String campaignId; // CAMPAIGN_ID
	@JsonProperty
	private String campaignManagerId; // USERNAME
	@JsonProperty
	private String campaignType; // AGENT / TEAM
	@JsonProperty
	private String invitee; // TEAMID / AGENT USERNAME
	
	public CampaignWorkroomDTO() {
		
	}
	
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the campaignManagerId
	 */
	public String getCampaignManagerId() {
		return campaignManagerId;
	}
	/**
	 * @param campaignManagerId the campaignManagerId to set
	 */
	public void setCampaignManagerId(String campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}
	/**
	 * @return the campaignType
	 */
	public String getCampaignType() {
		return campaignType;
	}
	/**
	 * @param campaignType the campaignType to set
	 */
	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}
	/**
	 * @return the invitee
	 */
	public String getInvitee() {
		return invitee;
	}
	/**
	 * @param invitee the invitee to set
	 */
	public void setInvitee(String invitee) {
		this.invitee = invitee;
	}
}
