package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.TeamResource;

public class TeamResourceDTO {

	@JsonProperty
	private String resourceId;
	
	@JsonProperty
	private String supervisorOverrideCampaignId;
	
	@JsonProperty
	private AddressDTO address;
	
	@JsonProperty
	private List<String> languages;
	
	@JsonProperty
	private List<String> domains;
	
	public TeamResourceDTO() {
	}

	public TeamResourceDTO(TeamResource teamResource) {
		this.resourceId = teamResource.getResourceId();
		this.supervisorOverrideCampaignId = teamResource.getSupervisorOverrideCampaignId();
		if (teamResource.getAddress() != null) {
			this.address = new AddressDTO(teamResource.getAddress());
		}
		this.languages = teamResource.getLanguages();
		this.domains = teamResource.getDomains();
	}
	
	public TeamResource toTeamResource() {
		return new TeamResource(resourceId);
	}

	public String getResourceId() {
		return resourceId;
	}

	public String getSupervisorOverrideCampaignId() {
		return supervisorOverrideCampaignId;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public List<String> getDomains() {
		return domains;
	}
}
