package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactCommandDTO {
	@JsonProperty
	@NotNull
	private int count;
	
	@JsonProperty
	@NotNull
	private String campaignId;
	
	@JsonProperty
	@NotNull
	private ContactCommand contactCommand;
	
	@JsonProperty
	private ExpressionGroupDTO expressionGroup;
	
	@JsonProperty
	private String listPurchaseTransactionId;
	

	public int getCount() {
		return count;
	}
	
	public String getCampaignId() {
		return campaignId;
	}

	public ContactCommand getContactCommand() {
		return contactCommand;
	}
	
	public ExpressionGroupDTO getExpressionGroup() {
		return expressionGroup;
	}

	public String getListPurchaseTransactionId() {
		return listPurchaseTransactionId;
	}

	
	
	
}