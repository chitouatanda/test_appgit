/**
 * 
 */
package com.xtaas.web.dto;

/**
 * @author djain
 *
 */
public class CallDTO {
	
	private String twilioCallSid;

	/**
	 * @return the twilioCallSid
	 */
	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	/**
	 * @param twilioCallSid the twilioCallSid to set
	 */
	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

}
