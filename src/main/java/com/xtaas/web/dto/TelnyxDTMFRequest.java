package com.xtaas.web.dto;

public class TelnyxDTMFRequest {
  
  private String digits;

  public String getDigits() {
    return digits;
  }

  public void setDigits(String digits) {
    this.digits = digits;
  }
}