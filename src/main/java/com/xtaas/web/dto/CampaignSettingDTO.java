package com.xtaas.web.dto;

import java.util.Map;

import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.LeadSortOrder;

public class CampaignSettingDTO {

	private boolean hidePhone;
	private boolean qaRequired;
	private LeadSortOrder leadSortOrder;
	private CallSpeed callSpeedPerMinPerAgent;
	private boolean localOutboundCalling;
	private int dailyCallMaxRetries;
	private int callMaxRetries;
	private boolean enableMachineAnsweredDetection;
	private boolean retrySpeedEnabled;
	private boolean simpleDisposition;
	private DialerMode dialerMode;
	private float retrySpeedFactor;
	private Map<String, Float> retrySpeed;
	private boolean emailSuppressed;
	private String campaignType;
	private String sipProvider;

	public boolean getHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}

	public boolean getQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(boolean qaRequired) {
		this.qaRequired = qaRequired;
	}

	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public boolean getLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public int getDailyCallMaxRetries() {
		return dailyCallMaxRetries;
	}

	public void setDailyCallMaxRetries(int dailyCallMaxRetries) {
		this.dailyCallMaxRetries = dailyCallMaxRetries;
	}

	public int getCallMaxRetries() {
		return callMaxRetries;
	}

	public void setCallMaxRetries(int callMaxRetries) {
		this.callMaxRetries = callMaxRetries;
	}

	public boolean getEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean getRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(boolean retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public boolean getSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public DialerMode getDialerMode() {
		return dialerMode;
	}

	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public float getRetrySpeedFactor() {
		return retrySpeedFactor;
	}

	public void setRetrySpeedFactor(float retrySpeedFactor) {
		this.retrySpeedFactor = retrySpeedFactor;
	}

	public boolean isEmailSuppressed() {
		return emailSuppressed;
	}

	public void setEmailSuppressed(boolean emailSuppressed) {
		this.emailSuppressed = emailSuppressed;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	@Override
	public String toString() {
		return "CampaignSettingDTO [callMaxRetries=" + callMaxRetries + ", callSpeedPerMinPerAgent="
				+ callSpeedPerMinPerAgent + ", campaignType=" + campaignType + ", dailyCallMaxRetries="
				+ dailyCallMaxRetries + ", dialerMode=" + dialerMode + ", emailSuppressed=" + emailSuppressed
				+ ", enableMachineAnsweredDetection=" + enableMachineAnsweredDetection + ", hidePhone=" + hidePhone
				+ ", leadSortOrder=" + leadSortOrder + ", localOutboundCalling=" + localOutboundCalling
				+ ", qaRequired=" + qaRequired + ", retrySpeed=" + retrySpeed + ", retrySpeedEnabled="
				+ retrySpeedEnabled + ", retrySpeedFactor=" + retrySpeedFactor + ", simpleDisposition="
				+ simpleDisposition + ", sipProvider=" + sipProvider + "]";
	}
	
}
