package com.xtaas.web.dto;

public enum CampaignCommands {
	Plan, Run, Stop, Pause, Resume, Complete, Delist,RunReady, Running
}
