package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.Contact;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.ContactPurchaseError;
import com.xtaas.domain.valueobject.ContactSicCode;
import com.xtaas.domain.valueobject.ContactSocialMedia;

public class ContactDTO {
	
	@JsonProperty
	private String source;
	@JsonProperty
	private String sourceId;
	
	@JsonProperty
	private String prefix;
	
	@JsonProperty
	private String firstName;
	
	@JsonProperty
	private String lastName;
	
	@JsonProperty
	private String suffix;
	
	@JsonProperty
	private String title;
	
	@JsonProperty
	private String email;
	
	@JsonProperty
	private String phone;
	
	@JsonProperty
	private List<ContactFunction> functions;
	
	@JsonProperty
	private String organizationName;
	
	@JsonProperty
	private String domain;
	
	@JsonProperty
	private List<ContactIndustry> industries;
	
	@JsonProperty
	private double minRevenue;
	
	@JsonProperty
	private double maxRevenue;
	
	@JsonProperty
	private double minEmployeeCount;
	
	@JsonProperty
	private double maxEmployeeCount;
	
	@JsonProperty
	private String addressLine1;
	
	@JsonProperty
	private String addressLine2;
	
	@JsonProperty
	private String city;
	
	@JsonProperty
	private String state;
	
	@JsonProperty
	private String country;
	
	@JsonProperty
	private String postalCode;
	
	@JsonProperty
	private String purchaseError;
	
	@JsonProperty
	private List<ContactSicCode> sicCodes;
	
	@JsonProperty
	private String phoneAccuracy;
	
	@JsonProperty
	private ContactSocialMedia socialMedia;
	
	
	
	public ContactDTO(Contact contact) {
		this.source = contact.getSource();
		this.sourceId = contact.getSourceId();
		this.prefix = contact.getPrefix();
		this.firstName = contact.getFirstName();
		this.lastName = contact.getLastName();
		this.suffix = contact.getSuffix();
		this.title = contact.getTitle();
		this.email = contact.getEmail();
		this.phone = contact.getDirectPhone() != null ? contact.getDirectPhone() : contact.getOfficePhone();
		this.functions = contact.getFunctions();
		this.organizationName = contact.getOrganizationName();
		this.domain = contact.getDomain();
		this.industries = contact.getIndustries();
		this.minRevenue = contact.getMinRevenue();
		this.maxRevenue = contact.getMaxRevenue();
		this.minEmployeeCount = contact.getMinEmployeeCount();
		this.maxEmployeeCount = contact.getMaxEmployeeCount();
		this.addressLine1 = contact.getAddressLine1();
		this.addressLine2 = contact.getAddressLine2();
		this.city = contact.getCity();
		this.state = contact.getState();
		this.country = contact.getCountry();
		this.postalCode = contact.getPostalCode();
		this.sicCodes = contact.getSicCodes();
		this.socialMedia = contact.getSocialMedia();
		this.phoneAccuracy = contact.getPhoneAccuracy();
		this.socialMedia = contact.getSocialMedia();
	}


	/**
	 * @return the purchaseError
	 */
	public String getPurchaseError() {
		return purchaseError;
	}


	/**
	 * @param purchaseError the purchaseError to set
	 */
	public void setPurchaseError(String purchaseError) {
		this.purchaseError = purchaseError;
	}


	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}


	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}


	
	
}

