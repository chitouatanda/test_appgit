package com.xtaas.web.dto;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.TeamSortClauses;

/**
 * @author Raghava
 * 
 */
public class TeamSearchDTO {
	@JsonProperty
	private TeamSearchClauses clause;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private List<String> countries;
	@JsonProperty
	private boolean excludeCountries;
	@JsonProperty
	private List<String> states;
	@JsonProperty
	private boolean excludeStates;
	@JsonProperty
	private List<String> cities;
	@JsonProperty
	private boolean excludeCities;
	@JsonProperty
	private List<String> timeZones;
	@JsonProperty
	private List<String> languages;
	@JsonProperty
	private List<Double> ratings;
	@JsonProperty
	private boolean includeDomain;
	@JsonProperty
	private double maximumPerLeadPrice;
	@JsonProperty
	private int minimumCompletedCampaigns;
	@JsonProperty
	private int minimumTotalVolume;
	@JsonProperty
	private double minimumAverageConversion;
	@JsonProperty
	private double minimumAverageQuality;
	@JsonProperty
	private boolean includeTeamsWithBandwidth;
	@JsonProperty
	private TeamSortClauses sort = TeamSortClauses.PerLeadPrice;
	@JsonProperty
	private Direction direction = Direction.ASC;
	@JsonProperty
	private int page;
	@JsonProperty
	private int size = 10;
	//Added for qafilter change
	@JsonProperty
	private String prospectCallStatus;
	@JsonProperty
	private String timeZone;
	
	
	public TeamSearchClauses getClause() {
		return clause;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public List<String> getCountries() {
		return countries;
	}

	public boolean isExcludeCountries() {
		return excludeCountries;
	}

	public List<String> getStates() {
		return states;
	}

	public boolean isExcludeStates() {
		return excludeStates;
	}

	public List<String> getCities() {
		return cities;
	}

	public boolean isExcludeCities() {
		return excludeCities;
	}

	public List<String> getTimeZones() {
		return timeZones;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public List<Double> getRatings() {
		return ratings;
	}

	public boolean isIncludeTeamsWithBandwidth() {
		return includeTeamsWithBandwidth;
	}

	public boolean isIncludeDomain() {
		return includeDomain;
	}

	public int getMinimumCompletedCampaigns() {
		return minimumCompletedCampaigns;
	}

	public double getMaximumPerLeadPrice() {
		return maximumPerLeadPrice;
	}

	public int getMinimumTotalVolume() {
		return minimumTotalVolume;
	}

	public double getMinimumAverageConversion() {
		return minimumAverageConversion;
	}

	public double getMinimumAverageQuality() {
		return minimumAverageQuality;
	}

	public TeamSortClauses getSort() {
		return sort;
	}

	public Direction getDirection() {
		return direction;
	}

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}

/*<<<<<<< HEAD
=======*/
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getProspectCallStatus() {
		return prospectCallStatus;
	}

	public void setProspectCallStatus(String prospectCallStatus) {
		this.prospectCallStatus = prospectCallStatus;
	}
/*>>>>>>> stash*/
}
