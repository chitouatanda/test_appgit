package com.xtaas.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Note;

public class NoteDTO {
	@JsonProperty
	private Date creationDate;
	@JsonProperty
	private String text;
	@JsonProperty
	private String agentId;
	@JsonProperty
	private String agentName;
	@JsonProperty
	private String dispositionStatus;
	@JsonProperty
	private String subStatus;
	@JsonProperty
	private String qaId;

	public NoteDTO() {

	}

	public NoteDTO(Note note) {
		this.creationDate = note.getCreationDate();
		this.text = note.getText();
		this.agentId = note.getAgentId();
		this.agentName = note.getAgentName();
		this.dispositionStatus = note.getDispositionStatus();
		this.subStatus = note.getSubStatus();
		this.qaId = note.getQaId();
	}

	public Note toNote() {
		Note note = new Note(this.creationDate, this.text, this.agentId);
		note.setAgentName(this.agentName);
		note.setDispositionStatus(this.dispositionStatus);
		note.setSubStatus(this.subStatus);
		note.setQaId(this.qaId);
		return note;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getQaId() {
		return qaId;
	}

	public void setQaId(String qaId) {
		this.qaId = qaId;
	}

}
