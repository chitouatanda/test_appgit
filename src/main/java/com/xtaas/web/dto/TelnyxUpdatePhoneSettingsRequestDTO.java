package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "billing_group_id" })
public class TelnyxUpdatePhoneSettingsRequestDTO {

  @JsonProperty("billing_group_id")
  private String billingGroupId;

  @JsonProperty("billing_group_id")
  public String getBillingGroupId() {
    return billingGroupId;
  }

  @JsonProperty("billing_group_id")
  public void setBillingGroupId(String billingGroupId) {
    this.billingGroupId = billingGroupId;
  }

}