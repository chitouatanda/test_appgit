package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CampaignSearchResult;
import com.xtaas.mvc.model.ProspectCallSearch;

public class ProspectCallQaSearchResult {

	@JsonProperty
	private long prospectCallCount;

	@JsonProperty
	private List<ProspectCallSearch> prospectCallSearchList;

	@JsonProperty
	private List<AgentSearchResult> agentSearchResult;

	@JsonProperty
	private CampaignSearchResult campaignSearchResult;
	
	@JsonProperty
	List<String> partnerIds;

	@JsonProperty
	List<String> teamIds;
	/**
	 * @param prospectCallCount
	 * @param prospectCallSearchList
	 */
	public ProspectCallQaSearchResult(long prospectCallCount, List<ProspectCallSearch> prospectCallSearchList,
			CampaignSearchResult campaignSearchResult, List<AgentSearchResult> agentSearchResult, List<String> partnerIds, List<String> teamIds) {
		this.prospectCallCount = prospectCallCount;
		this.prospectCallSearchList = prospectCallSearchList;
		this.campaignSearchResult = campaignSearchResult;
		this.agentSearchResult = agentSearchResult;
		this.partnerIds = partnerIds;
		this.teamIds = teamIds;
	}

}
