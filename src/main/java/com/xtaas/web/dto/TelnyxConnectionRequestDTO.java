package com.xtaas.web.dto;

public class TelnyxConnectionRequestDTO {

    private String user_name;

    private boolean active;

    private String connection_name;

    private String password;

    private String sip_uri_calling_preference;

    private TelnyxConnectionRequestDTOOutbound outbound;

    public String getUser_name() {
      return user_name;
    }

    public void setUser_name(String user_name) {
      this.user_name = user_name;
    }

    public boolean getActive() {
      return active;
    }

    public void setActive(boolean active) {
      this.active = active;
    }

    public String getConnection_name() {
      return connection_name;
    }

    public void setConnection_name(String connection_name) {
      this.connection_name = connection_name;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getSip_uri_calling_preference() {
      return sip_uri_calling_preference;
    }

    public void setSip_uri_calling_preference(String sip_uri_calling_preference) {
      this.sip_uri_calling_preference = sip_uri_calling_preference;
    }

    public TelnyxConnectionRequestDTOOutbound getOutbound() {
      return outbound;
    }

    public void setOutbound(TelnyxConnectionRequestDTOOutbound outbound) {
      this.outbound = outbound;
    }


}