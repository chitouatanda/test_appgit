package com.xtaas.web.dto;

import java.util.List;

public class GlobalContactCriteriaDTO {

	private List<String> campaignIds;
	private List<String> statuses;

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	@Override
	public String toString() {
		return "GlobalContactCriteriaDTO [campaignIds=" + campaignIds + ", statuses=" + statuses + "]";
	}

}
