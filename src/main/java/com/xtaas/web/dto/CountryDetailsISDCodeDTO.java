package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryDetailsISDCodeDTO {

    @JsonProperty("name")
    private String name;
    @JsonProperty("callingCodes")
    private List<String> callingCodes = null;
    @JsonProperty("nativeName")
    private String nativeName;
    @JsonProperty("altSpellings")
    private List<String> altSpellings = null;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("callingCodes")
    public List<String> getCallingCodes() {
        return callingCodes;
    }

    @JsonProperty("callingCodes")
    public void setCallingCodes(List<String> callingCodes) {
        this.callingCodes = callingCodes;
    }

    @JsonProperty("nativeName")
    public String getNativeName() {
        return nativeName;
    }

    @JsonProperty("nativeName")
    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    @JsonProperty("altSpellings")
    public List<String> getAltSpellings() {
        return altSpellings;
    }

    @JsonProperty("altSpellings")
    public void setAltSpellings(List<String> altSpellings) {
        this.altSpellings = altSpellings;
    }

}