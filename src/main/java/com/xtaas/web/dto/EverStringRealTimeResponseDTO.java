package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EverStringRealTimeResponseDTO {

	@JsonProperty("data")
	private List<EverStringRealTimeCompanyDTO> data;

	@JsonProperty("time_taken")
	private String time_taken;

	public List<EverStringRealTimeCompanyDTO> getData() {
		return data;
	}

	public void setData(List<EverStringRealTimeCompanyDTO> data) {
		this.data = data;
	}

	public String getTime_taken() {
		return time_taken;
	}

	public void setTime_taken(String time_taken) {
		this.time_taken = time_taken;
	}

}
