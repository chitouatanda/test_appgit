package com.xtaas.web.dto;

import java.util.List;

public class TwilioNumberBuyDTO {

	private String profile;
	private String isoCountry;
	private Boolean voiceEnabled;
	private Boolean smsEnabled;
	private Boolean mmsEnabled;
	private Boolean faxEnabled;
	private List<String> areaCodes;
	private int buyCountPerAreaCode;
	private Boolean excludeAllAddressRequired;
	private Boolean excludeForeignAddressRequired;
	private Boolean excludeLocalAddressRequired;

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getIsoCountry() {
		return isoCountry;
	}

	public void setIsoCountry(String isoCountry) {
		this.isoCountry = isoCountry;
	}

	public Boolean getVoiceEnabled() {
		return voiceEnabled;
	}

	public void setVoiceEnabled(Boolean voiceEnabled) {
		this.voiceEnabled = voiceEnabled;
	}

	public Boolean getSmsEnabled() {
		return smsEnabled;
	}

	public void setSmsEnabled(Boolean smsEnabled) {
		this.smsEnabled = smsEnabled;
	}

	public Boolean getMmsEnabled() {
		return mmsEnabled;
	}

	public void setMmsEnabled(Boolean mmsEnabled) {
		this.mmsEnabled = mmsEnabled;
	}

	public Boolean getFaxEnabled() {
		return faxEnabled;
	}

	public void setFaxEnabled(Boolean faxEnabled) {
		this.faxEnabled = faxEnabled;
	}

	public List<String> getAreaCodes() {
		return areaCodes;
	}

	public void setAreaCodes(List<String> areaCodes) {
		this.areaCodes = areaCodes;
	}

	public int getBuyCountPerAreaCode() {
		return buyCountPerAreaCode;
	}

	public void setBuyCountPerAreaCode(int buyCountPerAreaCode) {
		this.buyCountPerAreaCode = buyCountPerAreaCode;
	}

	public Boolean getExcludeAllAddressRequired() {
		return excludeAllAddressRequired;
	}

	public void setExcludeAllAddressRequired(Boolean excludeAllAddressRequired) {
		this.excludeAllAddressRequired = excludeAllAddressRequired;
	}

	public Boolean getExcludeForeignAddressRequired() {
		return excludeForeignAddressRequired;
	}

	public void setExcludeForeignAddressRequired(Boolean excludeForeignAddressRequired) {
		this.excludeForeignAddressRequired = excludeForeignAddressRequired;
	}

	public Boolean getExcludeLocalAddressRequired() {
		return excludeLocalAddressRequired;
	}

	public void setExcludeLocalAddressRequired(Boolean excludeLocalAddressRequired) {
		this.excludeLocalAddressRequired = excludeLocalAddressRequired;
	}

	@Override
	public String toString() {
		return "TwilioNumberBuyDTO [profile=" + profile + ", isoCountry=" + isoCountry + ", voiceEnabled="
				+ voiceEnabled + ", smsEnabled=" + smsEnabled + ", mmsEnabled=" + mmsEnabled + ", faxEnabled="
				+ faxEnabled + ", areaCodes=" + areaCodes + ", buyCountPerAreaCode=" + buyCountPerAreaCode
				+ ", excludeAllAddressRequired=" + excludeAllAddressRequired + ", excludeForeignAddressRequired="
				+ excludeForeignAddressRequired + ", excludeLocalAddressRequired=" + excludeLocalAddressRequired + "]";
	}

}
