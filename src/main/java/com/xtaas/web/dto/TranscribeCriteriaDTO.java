package com.xtaas.web.dto;

import java.util.List;

public class TranscribeCriteriaDTO {

	private List<String> campaignIds;
	private String minDate;
	private String maxDate;
	private int minCallDuration;
	private int maxCallDuration;
	private List<String> dispositionStatus;
	private List<String> subStatus;
	private String directPhone;
	private String emailRevealed;

	public String getMinDate() {
		return minDate;
	}

	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public int getMinCallDuration() {
		return minCallDuration;
	}

	public void setMinCallDuration(int minCallDuration) {
		this.minCallDuration = minCallDuration;
	}

	public int getMaxCallDuration() {
		return maxCallDuration;
	}

	public void setMaxCallDuration(int maxCallDuration) {
		this.maxCallDuration = maxCallDuration;
	}

	public String getDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(String directPhone) {
		this.directPhone = directPhone;
	}

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public List<String> getDispositionStatus() {
		return dispositionStatus;
	}

	public void setDispositionStatus(List<String> dispositionStatus) {
		this.dispositionStatus = dispositionStatus;
	}

	public List<String> getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(List<String> subStatus) {
		this.subStatus = subStatus;
	}

	public String getEmailRevealed() {
		return emailRevealed;
	}

	public void setEmailRevealed(String emailRevealed) {
		this.emailRevealed = emailRevealed;
	}

	@Override
	public String toString() {
		return "TranscribeCriteriaDTO [campaignIds=" + campaignIds + ", minDate=" + minDate + ", maxDate=" + maxDate
				+ ", minCallDuration=" + minCallDuration + ", maxCallDuration=" + maxCallDuration
				+ ", dispositionStatus=" + dispositionStatus + ", subStatus=" + subStatus + ", directPhone="
				+ directPhone + ", emailRevealed=" + emailRevealed + "]";
	}

}
