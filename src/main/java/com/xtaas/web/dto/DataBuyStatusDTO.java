package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataBuyStatusDTO {

	@JsonProperty
	private String dataBuyQueueId;

	@JsonProperty
	private String status;

	@JsonProperty
	private boolean isBuyProspect;
	
	@JsonProperty
	private boolean isSearchCompany;

	public String getDataBuyQueueId() {
		return dataBuyQueueId;
	}

	public void setDataBuyQueueId(String dataBuyQueueId) {
		this.dataBuyQueueId = dataBuyQueueId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isBuyProspect() {
		return isBuyProspect;
	}

	public void setBuyProspect(boolean isBuyProspect) {
		this.isBuyProspect = isBuyProspect;
	}

	public boolean isSearchCompany() {
		return isSearchCompany;
	}

	public void setSearchCompany(boolean isSearchCompany) {
		this.isSearchCompany = isSearchCompany;
	}

}
