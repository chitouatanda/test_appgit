package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"data"
})
public class TelnyxPhoneOrderResponseDTO {

@JsonProperty("data")
private TelnyxPhoneOrderResponseData data;

@JsonProperty("data")
public TelnyxPhoneOrderResponseData getData() {
return data;
}

@JsonProperty("data")
public void setData(TelnyxPhoneOrderResponseData data) {
this.data = data;
}

}