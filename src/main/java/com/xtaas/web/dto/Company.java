package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Company {

    @JsonProperty("id")
    private String id;

    @JsonProperty("country")
    private String country;

    @JsonProperty("domain")
    private String domain;

    @JsonProperty("industry")
    private String industry;

    @JsonProperty("linkedinId")
    private String linkedinId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("state")
    private String state;

    @JsonProperty("employeeCount")
    private Long employeeCount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Long employeeCount) {
        this.employeeCount = employeeCount;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id='" + id + '\'' +
                ", country='" + country + '\'' +
                ", domain='" + domain + '\'' +
                ", industry='" + industry + '\'' +
                ", linkedinId='" + linkedinId + '\'' +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", employeeCount=" + employeeCount +
                '}';
    }
}
