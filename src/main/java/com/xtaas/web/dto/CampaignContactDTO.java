package com.xtaas.web.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.CampaignContact;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;

public class CampaignContactDTO {

	@JsonProperty
	private String campaignId;

	@JsonProperty
	private String source;

	@JsonProperty
	private String sourceId;

	@JsonProperty
	private String prefix;

	@JsonProperty
	private String firstName;

	@JsonProperty
	private String lastName;

	@JsonProperty
	private String suffix;

	@JsonProperty
	private String title;

	@JsonProperty
	private String department;

	@JsonProperty
	private List<ContactFunction> functions;

	@JsonProperty
	private String organizationName;

	@JsonProperty
	private String domain;

	@JsonProperty
	private List<ContactIndustry> industries;

	@JsonProperty
	private double minRevenue;

	@JsonProperty
	private double maxRevenue;

	@JsonProperty
	private double minEmployeeCount;

	@JsonProperty
	private double maxEmployeeCount;

	@JsonProperty
	private String email;

	@JsonProperty
	private String addressLine1;

	@JsonProperty
	private String addressLine2;

	@JsonProperty
	private String city;

	@JsonProperty
	private String stateCode;

	@JsonProperty
	private String country;

	@JsonProperty
	private String postalCode;

	@JsonProperty
	private String phone;

	@JsonProperty
	private String extension;

	@JsonProperty
	private String listPurchaseTransactionId;

	@JsonProperty
	private Map<String, String> companyAddress;

	/**
	 * @return the purchaseError
	 */
	public String getPurchaseError() {
		return purchaseError;
	}

	/**
	 * @param purchaseError the purchaseError to set
	 */
	public void setPurchaseError(String purchaseError) {
		this.purchaseError = purchaseError;
	}

	public Map<String, String> getcompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	@JsonProperty
	private String purchaseError;

	public CampaignContactDTO(CampaignContact campaignContact) {
		this.campaignId = campaignContact.getCampaignId();
		this.source = campaignContact.getSource();
		this.sourceId = campaignContact.getSourceId();
		this.prefix = campaignContact.getPrefix();
		this.firstName = campaignContact.getFirstName();
		this.lastName = campaignContact.getLastName();
		this.suffix = campaignContact.getSuffix();
		this.title = campaignContact.getTitle();
		this.department = campaignContact.getDepartment();
		this.functions = campaignContact.getFunctions();
		this.organizationName = campaignContact.getOrganizationName();
		this.domain = campaignContact.getDomain();
		this.industries = campaignContact.getIndustries();
		this.minRevenue = campaignContact.getMinRevenue();
		this.maxRevenue = campaignContact.getMaxRevenue();
		this.minEmployeeCount = campaignContact.getMinEmployeeCount();
		this.maxEmployeeCount = campaignContact.getMaxEmployeeCount();
		this.email = campaignContact.getEmail();
		this.addressLine1 = campaignContact.getAddressLine1();
		this.addressLine2 = campaignContact.getAddressLine2();
		this.city = campaignContact.getCity();
		this.stateCode = campaignContact.getStateCode();
		this.country = campaignContact.getCountry();
		this.postalCode = campaignContact.getPostalCode();
		this.phone = campaignContact.getPhone();
		this.extension = campaignContact.getExtension();
		this.listPurchaseTransactionId = campaignContact.getListPurchaseTransactionId();
	}

}
