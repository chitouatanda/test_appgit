package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.Asset;

public class AssetDTO {
	
	
	private String assetId;
	
	private String name;
	
	private String description;
	
	private String url;
	
	private int assetTarget;
	
	private int assetActualTarget;

	private Boolean isRemoved;
	
	public AssetDTO() {
		
	}
	
	public AssetDTO(Asset asset) {
		this.assetId = asset.getAssetId();
		this.name = asset.getName();
		this.description = asset.getDescription();
		this.url = asset.getUrl();
		this.assetTarget = asset.getAssetTarget();
		if (asset.getIsRemoved() != null) {
			this.isRemoved = asset.getIsRemoved();	
		} else {
			this.isRemoved = false;
		}
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Asset toAsset() {
		return new Asset(assetId, name, description, url, isRemoved);
	}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

	public int getAssetTarget() {
		return assetTarget;
	}

	public void setAssetTarget(int assetTarget) {
		this.assetTarget = assetTarget;
	}

	public int getAssetActualTarget() {
		return assetActualTarget;
	}

	public void setAssetActualTarget(int assetActualTarget) {
		this.assetActualTarget = assetActualTarget;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

}
