package com.xtaas.web.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentQaAnswer;

public class ProspectDTO {
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private String source;
	@JsonProperty
	private String sourceId;
	@JsonProperty
	private String campaignContactId;
	@JsonProperty
	private String status;
	@JsonProperty
	private String prefix;
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	@JsonProperty
	private String suffix;
	@JsonProperty
	private String company;
	@JsonProperty
	private String title; // C, VP, DIR, MGR, OTHER
	@JsonProperty
	private String department; // Marketing, Finance, Legal
	@JsonProperty
	private String inputDepartment;
	@JsonProperty
	private String phone;
	@JsonProperty
	private String companyPhone;
	@JsonProperty
	private boolean directPhone;
	@JsonProperty
	private String timeZone;
	@JsonProperty
	private String industry;
	@JsonProperty
	private List<String> industryList;
	@JsonProperty
	private String email;
	@JsonProperty
	private String addressLine1;
	@JsonProperty
	private String addressLine2;
	@JsonProperty
	private String city;
	@JsonProperty
	private String zipCode;
	@JsonProperty
	private String country;
	@JsonProperty
	private String extension;
	@JsonProperty
	private String stateCode; // 2 letter code -- must field
	@JsonProperty
	private boolean optedIn;// default false
	@JsonProperty
	private boolean isEu;
	@JsonProperty
	private Map<String, Object> customAttributes = new HashMap<String, Object>();
	@JsonProperty
	private String sourceCompanyId;
	@JsonProperty
	private String name;
	@JsonProperty
	private String managementLevel;
	@JsonProperty
	private Long callbackTimeInMS;
	@JsonProperty
	private List<String> topLevelIndustries;
	@JsonProperty
	private Map<String, String> companyAddress;
	@JsonProperty
	private boolean isEncrypted;
	@JsonProperty
	private String zoomCompanyUrl;
	@JsonProperty
	private String zoomPersonUrl;
	@JsonProperty
	private String prospectType;
	@JsonProperty
	private List<String> companySIC;
	@JsonProperty
    private List<String> companyNAICS;
	@JsonProperty
	private String sourceType;
	@JsonProperty
	private String phoneType;
	private boolean emailSent;
    private boolean emailValidated;
    @JsonProperty
    private List<AgentQaAnswer> cfAnswers;
	

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getCampaignContactId() {
		return campaignContactId;
	}

	public void setCampaignContactId(String campaignContactId) {
		this.campaignContactId = campaignContactId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getInputDepartment() {
		return inputDepartment;
	}

	public void setInputDepartment(String inputDepartment) {
		this.inputDepartment = inputDepartment;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public boolean isDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(boolean directPhone) {
		this.directPhone = directPhone;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public List<String> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<String> industryList) {
		this.industryList = industryList;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public boolean isOptedIn() {
		return optedIn;
	}

	public void setOptedIn(boolean optedIn) {
		this.optedIn = optedIn;
	}

	public boolean isEu() {
		return isEu;
	}

	public void setEu(boolean isEu) {
		this.isEu = isEu;
	}

	public Map<String, Object> getCustomAttributes() {
		return customAttributes;
	}

	public void setCustomAttributes(Map<String, Object> customAttributes) {
		this.customAttributes = customAttributes;
	}

	public String getSourceCompanyId() {
		return sourceCompanyId;
	}

	public void setSourceCompanyId(String sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManagementLevel() {
		return managementLevel;
	}

	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}

	public Long getCallbackTimeInMS() {
		return callbackTimeInMS;
	}

	public void setCallbackTimeInMS(Long callbackTimeInMS) {
		this.callbackTimeInMS = callbackTimeInMS;
	}

	public List<String> getTopLevelIndustries() {
		return topLevelIndustries;
	}

	public void setTopLevelIndustries(List<String> topLevelIndustries) {
		this.topLevelIndustries = topLevelIndustries;
	}

	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public String getProspectType() {
		return prospectType;
	}

	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	public List<String> getCompanySIC() {
		return companySIC;
	}

	public void setCompanySIC(List<String> companySIC) {
		this.companySIC = companySIC;
	}

	public List<String> getCompanyNAICS() {
		return companyNAICS;
	}

	public void setCompanyNAICS(List<String> companyNAICS) {
		this.companyNAICS = companyNAICS;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	public boolean isEmailValidated() {
		return emailValidated;
	}

	public void setEmailValidated(boolean emailValidated) {
		this.emailValidated = emailValidated;
	}

	public List<AgentQaAnswer> getCfAnswers() {
		return cfAnswers;
	}

	public void setCfAnswers(List<AgentQaAnswer> cfAnswers) {
		this.cfAnswers = cfAnswers;
	}
	
}
