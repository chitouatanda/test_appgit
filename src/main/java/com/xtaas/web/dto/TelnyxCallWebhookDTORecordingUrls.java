package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"mp3",
"wav"
})
public class TelnyxCallWebhookDTORecordingUrls {

@JsonProperty("mp3")
private String mp3;
@JsonProperty("wav")
private String wav;

@JsonProperty("mp3")
public String getMp3() {
return mp3;
}

@JsonProperty("mp3")
public void setMp3(String mp3) {
this.mp3 = mp3;
}

@JsonProperty("wav")
public String getWav() {
return wav;
}

@JsonProperty("wav")
public void setWav(String wav) {
this.wav = wav;
}

}