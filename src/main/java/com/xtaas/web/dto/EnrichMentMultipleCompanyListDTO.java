package com.xtaas.web.dto;

import java.util.List;

public class EnrichMentMultipleCompanyListDTO {

	private List<EverStringRealTimeRequestDTO> companies;

	public List<EverStringRealTimeRequestDTO> getCompanies() {
		return companies;
	}

	public void setCompanies(List<EverStringRealTimeRequestDTO> companies) {
		this.companies = companies;
	}

}
