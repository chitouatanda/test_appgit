package com.xtaas.web.dto;

import java.util.List;

public class TelnyxTransferCallRequest {

  private String to;

  private String from;

  private String client_state;
  
  private String sip_auth_password;
  
  private String sip_auth_username;
  
  private Integer time_limit_secs;
  
  public TelnyxTransferCallRequest() {
    this.client_state = "";
    this.time_limit_secs = 14400;
  }
  
//  private List<TelnyxSIPInviteObject> custom_headers;

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }

  public String getSip_auth_password() {
    return sip_auth_password;
  }

  public void setSip_auth_password(String sip_auth_password) {
    this.sip_auth_password = sip_auth_password;
  }

  public String getSip_auth_username() {
    return sip_auth_username;
  }

  public void setSip_auth_username(String sip_auth_username) {
    this.sip_auth_username = sip_auth_username;
  }

  public Integer getTime_limit_secs() {
    return time_limit_secs;
  }

  public void setTime_limit_secs(Integer time_limit_secs) {
    this.time_limit_secs = time_limit_secs;
  }

//  public List<TelnyxSIPInviteObject> getCustom_headers() {
//    return custom_headers;
//  }
//
//  public void setCustom_headers(List<TelnyxSIPInviteObject> custom_headers) {
//    this.custom_headers = custom_headers;
//  }

}