package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataBuyTracker {

	@JsonProperty
	private String dataBuyStatus;
	
	@JsonProperty
	private int percentage;

	@JsonProperty
	private int totalRequestCount;
	
	@JsonProperty
	private int totalPurchasedCount;
	
	public String getDataBuyStatus() {
		return dataBuyStatus;
	}

	public void setDataBuyStatus(String dataBuyStatus) {
		this.dataBuyStatus = dataBuyStatus;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getTotalRequestCount() {
		return totalRequestCount;
	}

	public void setTotalRequestCount(int totalRequestCount) {
		this.totalRequestCount = totalRequestCount;
	}

	public int getTotalPurchasedCount() {
		return totalPurchasedCount;
	}

	public void setTotalPurchasedCount(int totalPurchasedCount) {
		this.totalPurchasedCount = totalPurchasedCount;
	}

	@Override
	public String toString() {
		return "DataBuyTracker [dataBuyStatus=" + dataBuyStatus + ", percentage=" + percentage + ", totalRequestCount="
				+ totalRequestCount + ", totalPurchasedCount=" + totalPurchasedCount + "]";
	}
	
}
