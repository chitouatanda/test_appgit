package com.xtaas.web.dto;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.AgentSortClauses;

public class AgentSearchDTO {
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private AgentSearchClauses clause;
	@JsonProperty
	private List<String> countries;
	@JsonProperty
	private boolean excludeCountries;
	@JsonProperty
	private List<String> states;
	@JsonProperty
	private boolean excludeStates;
	@JsonProperty
	private List<String> cities;
	@JsonProperty
	private boolean excludeCities;
	@JsonProperty
	private List<String> timeZones;
	@JsonProperty
	private List<String> languages;
	@JsonProperty
	private List<Double> ratings;
	@JsonProperty
	private boolean includeDomain;
	@JsonProperty
	private int minimumAvailableHours;
	@JsonProperty
	private int minimumCompletedCampaigns;
	@JsonProperty
	private double maximumPerHourPrice;
	@JsonProperty
	private int minimumTotalVolume;
	@JsonProperty
	private double minimumAverageConversion;
	@JsonProperty
	private double minimumAverageQuality;
	@JsonProperty
	private AgentSortClauses sort = AgentSortClauses.PerHourPrice;
	@JsonProperty
	private Direction direction = Direction.ASC;
	@JsonProperty
	private int page;
	@JsonProperty
	private int size = 10;
	@JsonProperty
	private String timeZone;
	@JsonProperty
	private String prospectCallStatus;	

	public String getCampaignId() {
		return campaignId;
	}

	public AgentSearchClauses getClause() {
		return clause;
	}

	public List<String> getCountries() {
		return countries;
	}

	public boolean isExcludeCountries() {
		return excludeCountries;
	}

	public List<String> getStates() {
		return states;
	}

	public boolean isExcludeStates() {
		return excludeStates;
	}

	public List<String> getCities() {
		return cities;
	}

	public boolean isExcludeCities() {
		return excludeCities;
	}

	public List<String> getTimeZones() {
		return timeZones;
	}

	public List<String> getLanguages() {
		return languages;
	}
	
	public List<Double> getRatings() {
		return ratings;
	}
	
	public boolean isIncludeDomain() {
		return includeDomain;
	}

	public int getMinimumAvailableHours() {
		return minimumAvailableHours;
	}

	public int getMinimumCompletedCampaigns() {
		return minimumCompletedCampaigns;
	}

	public double getMaximumPerHourPrice() {
		return maximumPerHourPrice;
	}

	public int getMinimumTotalVolume() {
		return minimumTotalVolume;
	}

	public double getMinimumAverageConversion() {
		return minimumAverageConversion;
	}

	public double getMinimumAverageQuality() {
		return minimumAverageQuality;
	}

	public AgentSortClauses getSort() {
		return sort;
	}

	public Direction getDirection() {
		return direction;
	}

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getProspectCallStatus() {
		return prospectCallStatus;
	}

	public void setProspectCallStatus(String prospectCallStatus) {
		this.prospectCallStatus = prospectCallStatus;
	}
}
