package com.xtaas.web.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"extensions",
"locations",
"message",
"path"
})
public class LeadIQError {

@JsonProperty("extensions")
private LeadIQErrorExtensions extensions;
@JsonProperty("locations")
private List<LeadIQErrorLocation> locations = null;
@JsonProperty("message")
private String message;
@JsonProperty("path")
private List<String> path = null;

@JsonProperty("extensions")
public LeadIQErrorExtensions getExtensions() {
return extensions;
}

@JsonProperty("extensions")
public void setExtensions(LeadIQErrorExtensions extensions) {
this.extensions = extensions;
}

@JsonProperty("locations")
public List<LeadIQErrorLocation> getLocations() {
return locations;
}

@JsonProperty("locations")
public void setLocations(List<LeadIQErrorLocation> locations) {
this.locations = locations;
}

@JsonProperty("message")
public String getMessage() {
return message;
}

@JsonProperty("message")
public void setMessage(String message) {
this.message = message;
}

@JsonProperty("path")
public List<String> getPath() {
return path;
}

@JsonProperty("path")
public void setPath(List<String> path) {
this.path = path;
}

}