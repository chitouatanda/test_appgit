package com.xtaas.web.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RestClientQuestions {

	@JsonProperty
	private boolean agentValidationRequired;
	
	@JsonProperty
	@NotNull
	private String questionSkin;
	
	@JsonProperty
	@NotNull
	private String operator;
	
	@JsonProperty
	@NotNull
	private String  selectedOptions;
	
	@JsonProperty
	private List<String>  options;

	public boolean isAgentValidationRequired() {
		return agentValidationRequired;
	}

	public void setAgentValidationRequired(boolean agentValidationRequired) {
		this.agentValidationRequired = agentValidationRequired;
	}

	public String getQuestionSkin() {
		return questionSkin;
	}

	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(String selctedOptions) {
		this.selectedOptions = selctedOptions;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}
	
	
	
}
