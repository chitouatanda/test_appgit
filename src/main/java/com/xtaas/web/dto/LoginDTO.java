package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Login;
import com.xtaas.domain.valueobject.LoginStatus;

public class LoginDTO {
	
	@JsonProperty
    private String id;

	@JsonProperty
	private LoginStatus status;

	@JsonProperty
	private List<String> roles;
	
	// Name of the Organization to which this user belongs
	@JsonProperty
    private String organization;
    
    //profile pic url
	@JsonProperty
    private String profilePicUrl;
    
	@JsonProperty
    private String firstName;
    
	@JsonProperty
    private String lastName;
	
	@JsonProperty
    private String email;

	private String adminRole;
	
	private List<String> reportEmails;

	@JsonProperty
	private boolean networkStatus;
	
	@JsonProperty
	private boolean dialerSetting;
	
	@JsonProperty
	private boolean showManageAgent;
	
	
	/**
	 * 
	 */
	public LoginDTO() {
		
	}

	/**
	 * @param roles
	 * @param organization
	 * @param profilePicUrl
	 * @param firstName
	 * @param lastName
	 */
	public LoginDTO(Login login) {
		this.id = login.getId();
		this.status = login.getStatus();
		this.roles = login.getRoles();
		this.organization = login.getOrganization();
		this.profilePicUrl = login.getProfilePicUrl();
		this.firstName = login.getFirstName();
		this.lastName = login.getLastName();
		this.email = login.getEmail();
		this.adminRole = login.getAdminRole();
		this.reportEmails = login.getReportEmails();
		this.networkStatus = login.isNetworkStatus();
		this.dialerSetting = login.isDialerSetting();
		this.showManageAgent = login.isShowManageAgent();
	}

	/**
	 * 
	 * @return the status
	 */
	public LoginStatus getStatus() {
		return status;
	}

	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @return the profilePicUrl
	 */
	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	public String getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	public List<String> getReportEmails() {
		return reportEmails;
	}

	public void setReportEmails(List<String> reportEmails) {
		this.reportEmails = reportEmails;
	}

	public boolean isNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(boolean networkStatus) {
		this.networkStatus = networkStatus;
	}

	public boolean isDialerSetting() {
		return dialerSetting;
	}

	public void setDialerSetting(boolean dialerSetting) {
		this.dialerSetting = dialerSetting;
	}

	public boolean isShowManageAgent() {
		return showManageAgent;
	}

	public void setShowManageAgent(boolean showManageAgent) {
		this.showManageAgent = showManageAgent;
	}
	
}
