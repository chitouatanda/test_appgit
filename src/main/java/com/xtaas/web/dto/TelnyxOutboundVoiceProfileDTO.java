package com.xtaas.web.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class TelnyxOutboundVoiceProfileDTO {

	@JsonProperty("outbound_voice_profile_id")
	private String outbound_voice_profile_id;

	public String getOutbound_voice_profile_id() {
		return outbound_voice_profile_id;
	}

	public void setOutbound_voice_profile_id(String outbound_voice_profile_id) {
		this.outbound_voice_profile_id = outbound_voice_profile_id;
	}

	@Override
	public String toString() {
		return "TelnyxOutboundVoiceProfileDTO [outbound_voice_profile_id=" + outbound_voice_profile_id + "]";
	}

}