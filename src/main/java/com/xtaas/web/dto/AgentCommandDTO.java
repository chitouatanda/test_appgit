package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.xtaas.domain.valueobject.AgentStatus;

public class AgentCommandDTO {

	@NotNull
	private AgentCommands agentCommand;

	private AgentStatus agentStatus;

	private String campaignId;

	private String agentType;

	private String agentId;

	public AgentCommands getAgentCommand() {
		return agentCommand;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public AgentStatus getAgentStatus() {
		return agentStatus;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

}
