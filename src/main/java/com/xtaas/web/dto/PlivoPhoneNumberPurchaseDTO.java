package com.xtaas.web.dto;

import java.util.List;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class PlivoPhoneNumberPurchaseDTO {

	@NotEmpty(message = "countryISO can not be null or empty")
	private String countryISO;

	@NotEmpty(message = "Can not be null or empty")
	private String type;

	@NotEmpty(message = "Can not be null or empty")
	private String region;

	@NotEmpty(message = "Can not be null or empty")
	private String services;

	@NotEmpty(message = "Can not be null or empty")
	private String appId;

//	@NotEmpty(message = "Can not be null or empty")
	private String subAccountId;

//	@NotEmpty(message = "Can not be null or empty")
	private String timezone;

//	@NotEmpty(message = "Can not be null or empty")
	private String partnerId;

//	@NotEmpty(message = "Can not be null or empty")
	private List<String> states;

//	@Min(value = 1)
	private int isdCode;

//	@Min(value = 1)
	private int pool;

//	@Min(value = 1)
	private int limit;

//	@Min(value = 1)
	private int offset;

	private String lata;

	private String rateCenter;
	
	private String userName;
	
	private String password;
	
	private List<String> emails;
	
	private List<String> pattern;
	
	private boolean addNumberInOtherCountries;

	public String getCountryISO() {
		return countryISO;
	}

	public void setCountryISO(String countryISO) {
		this.countryISO = countryISO;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSubAccountId() {
		return subAccountId;
	}

	public void setSubAccountId(String subAccountId) {
		this.subAccountId = subAccountId;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<String> getPattern() {
		return pattern;
	}

	public void setPattern(List<String> pattern) {
		this.pattern = pattern;
	}

	public String getLata() {
		return lata;
	}

	public void setLata(String lata) {
		this.lata = lata;
	}

	public String getRateCenter() {
		return rateCenter;
	}

	public void setRateCenter(String rateCenter) {
		this.rateCenter = rateCenter;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public boolean isAddNumberInOtherCountries() {
		return addNumberInOtherCountries;
	}

	public void setAddNumberInOtherCountries(boolean addNumberInOtherCountries) {
		this.addNumberInOtherCountries = addNumberInOtherCountries;
	}

}