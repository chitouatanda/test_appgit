package com.xtaas.web.dto;

public class EverStringRealTimeRequestDTO {

	private int ecid;
	private String name;

	public EverStringRealTimeRequestDTO(int ecid) {
		super();
		this.ecid = ecid;
	}

	public EverStringRealTimeRequestDTO() {
		// TODO Auto-generated constructor stub
	}

	public int getEcid() {
		return ecid;
	}

	public void setEcid(int ecid) {
		this.ecid = ecid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "EverStringRealTimeRequestDTO [ecid=" + ecid + ", name=" + name + "]";
	}
}
