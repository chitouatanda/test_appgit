package com.xtaas.web.dto;

import java.util.List;

public class TelnyxHoldConferenceDTO {
	
	private String audio_url;

	private List<String> call_control_ids;

	public List<String> getCall_control_ids() {
		return call_control_ids;
	}
	
	public void setCall_control_ids(List<String> call_control_ids) {
		this.call_control_ids = call_control_ids;
	}

	public String getAudio_url() {
		return audio_url;
	}

	public void setAudio_url(String audio_url) {
		this.audio_url = audio_url;
	}
  
}