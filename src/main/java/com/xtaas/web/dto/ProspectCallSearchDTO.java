package com.xtaas.web.dto;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProspectCallSearchDTO {

	@JsonProperty
	private ProspectCallSearchClause clause;

	@JsonProperty
	private List<String> teamIds;

	@JsonProperty
	private List<String> campaignIds;

	@JsonProperty
	private List<String> agentIds;

	// @JsonProperty
	// private List<String> qaIds;

	@JsonProperty
	private String clientId;

	@JsonProperty
	private String prospectCallStatus;

	@JsonProperty
	private List<String> disposition;

	@JsonProperty
	private List<String> subStatus;

	@JsonProperty
	private List<String> leadStatus;

	@JsonProperty
	private List<String> prospectCallIds;

	@JsonProperty
	private String fromDate;

	@JsonProperty
	private String toDate;

	@JsonProperty
	private String timeZone;

	@JsonProperty
	private Integer startHour;

	@JsonProperty
	private Integer endHour;

	@JsonProperty
	private int startMinute;

	@JsonProperty
	private int endMinute;

	@JsonProperty
	private Integer callDurationRangeStart;

	@JsonProperty
	private Integer callDurationRangeEnd;

	@JsonProperty
	private String sort = "prospectCall.leadStatus";

	@JsonProperty
	private Direction direction = Direction.DESC;

	@JsonProperty
	private int page;

	@JsonProperty
	private int size = 10;

	@JsonProperty
	private List<String> partnerIds;

	@JsonProperty
	private List<String> supervisorIds;

	@JsonProperty
	private String qaSearchString;

	/**
	 * @return the clause
	 */
	public ProspectCallSearchClause getClause() {
		return clause;
	}

	/**
	 * @return the teamIds
	 */
	public List<String> getTeamIds() {
		return teamIds;
	}

	/**
	 * @return the agentId
	 */
	public List<String> getAgentIds() {
		return agentIds;
	}

	// /**
	// * @return the qaId
	// */
	// public List<String> getQaIds() {
	// return qaIds;
	// }

	/**
	 * @return the campaignIds
	 */
	public List<String> getCampaignIds() {
		return campaignIds;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return the disposition
	 */
	public List<String> getDisposition() {
		return disposition;
	}

	/**
	 * @return the subStatus
	 */
	public List<String> getSubStatus() {
		return subStatus;
	}

	public List<String> getLeadStatus() {
		return leadStatus;
	}

	/**
	 * @return the prospectCallIds
	 */
	public List<String> getProspectCallIds() {
		return prospectCallIds;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @return the startHour
	 */
	public Integer getStartHour() {
		return startHour;
	}

	/**
	 * @return the endHour
	 */
	public Integer getEndHour() {
		return endHour;
	}

	/**
	 * @return the startMinute
	 */
	public int getStartMinute() {
		return startMinute;
	}

	/**
	 * @return the endMinute
	 */
	public int getEndMinute() {
		return endMinute;
	}

	/**
	 * @return the callDurationRangeStart
	 */
	public Integer getCallDurationRangeStart() {
		return callDurationRangeStart;
	}

	/**
	 * @return the callDurationRangeEnd
	 */
	public Integer getCallDurationRangeEnd() {
		return callDurationRangeEnd;
	}

	/**
	 * @return the sort
	 */
	public String getSort() {
		return sort;
	}

	/**
	 * @return the direction
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @return the prospectCallStatus
	 */
	public String getProspectCallStatus() {
		return prospectCallStatus;
	}

	/**
	 * @return the partnerIds
	 */
	public List<String> getPartnerIds() {
		return partnerIds;
	}

	public void setClause(ProspectCallSearchClause clause) {
		this.clause = clause;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public void setTeamIds(List<String> teamIds) {
		this.teamIds = teamIds;
	}

	public void setAgentIds(List<String> agentIds) {
		this.agentIds = agentIds;
	}

	// public void setQaIds(List<String> qaIds) {
	// this.qaIds = qaIds;
	// }

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public void setProspectCallStatus(String prospectCallStatus) {
		this.prospectCallStatus = prospectCallStatus;
	}

	public void setDisposition(List<String> disposition) {
		this.disposition = disposition;
	}

	public void setSubStatus(List<String> subStatus) {
		this.subStatus = subStatus;
	}

	public void setLeadStatus(List<String> leadStatus) {
		this.leadStatus = leadStatus;
	}

	public void setProspectCallIds(List<String> prospectCallIds) {
		this.prospectCallIds = prospectCallIds;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}

	public void setStartMinute(int startMinute) {
		this.startMinute = startMinute;
	}

	public void setEndMinute(int endMinute) {
		this.endMinute = endMinute;
	}

	public void setCallDurationRangeStart(Integer callDurationRangeStart) {
		this.callDurationRangeStart = callDurationRangeStart;
	}

	public void setCallDurationRangeEnd(Integer callDurationRangeEnd) {
		this.callDurationRangeEnd = callDurationRangeEnd;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setPartnerIds(List<String> partnerIds) {
		this.partnerIds = partnerIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public List<String> getSupervisorIds() {
		return supervisorIds;
	}

	public void setSupervisorIds(List<String> supervisorIds) {
		this.supervisorIds = supervisorIds;
	}

	public String getQaSearchString() {
		return qaSearchString;
	}

	public void setQaSearchString(String qaSearchString) {
		this.qaSearchString = qaSearchString;
	}
}
