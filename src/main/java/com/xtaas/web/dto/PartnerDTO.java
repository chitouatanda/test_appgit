package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.Address;
import com.xtaas.db.entity.Partner;

public class PartnerDTO {
	@JsonProperty
	@NotNull
	private String firstName;
	
	@JsonProperty
	@NotNull
	private String lastName;
	
	@JsonProperty
	@NotNull
	private String emailId;
	
	@JsonProperty
	@NotNull
	private String userName;
	
	@JsonProperty
	@NotNull
	private String companyName;
	
	@JsonProperty
	private String companySize;
	
	@JsonProperty
	private String industry;
	
	@JsonProperty
	@NotNull
	private String phoneNumber;
	
	@JsonProperty
	private Address address;

	public PartnerDTO() {
		
	}
	
	public PartnerDTO(Partner partner) {
		this.firstName = partner.getFirstName();
		this.lastName = partner.getLastName();
		this.emailId = partner.getEmailId();
		this.userName = partner.getUserName();
		this.companyName = partner.getCompanyName();
		this.companySize = getCompanySize();
		this.industry = partner.getIndustry();
		this.phoneNumber = partner.getPhoneNumber();
		this.address = partner.getAddress();
	}
	
	
	public Partner toPartner() {
		return new Partner(firstName, lastName, emailId, userName, companyName, companySize, industry, phoneNumber, address);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getUserName() {
		return userName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getCompanySize() {
		return companySize;
	}

	public String getIndustry() {
		return industry;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	
}
