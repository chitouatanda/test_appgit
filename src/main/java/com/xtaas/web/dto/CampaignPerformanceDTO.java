package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignPerformanceDTO {
	
	@JsonProperty
	private String id;
	
	@JsonProperty
	private String type;
	
	@JsonProperty
	private String team;
	
	@JsonProperty
	private String campaignName;
	
	@JsonProperty
	private String startDate;
	
	@JsonProperty
	private String endDate;
	
	@JsonProperty
	private String pacing;
	
	@JsonProperty
	private int budget;
	
	@JsonProperty
	private int generated;
	
	@JsonProperty
	private int rejected;
	
	@JsonProperty
	private int inQA;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPacing() {
		return pacing;
	}

	public void setPacing(String pacing) {
		this.pacing = pacing;
	}

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

	public int getGenerated() {
		return generated;
	}

	public void setGenerated(int generated) {
		this.generated = generated;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}

	public int getInQA() {
		return inQA;
	}

	public void setInQA(int inQA) {
		this.inQA = inQA;
	}
}
