package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InterpreterInputDTO {

    public InterpreterInputDTO (String text) {
        setInput(text);
    }

    @JsonProperty("input")
    private Input input;

    @JsonProperty("input")
    public Input getInput() {
        return input;
    }

    @JsonProperty("input")
    public void setInput(String text) {
    	this.input = new Input();
        this.input.setText(text);
    }

}

class Input {

    @JsonProperty("text")
    private String text;

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

}
