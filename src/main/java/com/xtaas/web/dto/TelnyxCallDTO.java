package com.xtaas.web.dto;

public class TelnyxCallDTO {

  private String connection_id;

  private String to;

  private String from;

  private String call_control_id;

  private String call_leg_id;

  private String call_session_id;

  private String client_state;

  private Integer time_limit_secs;

  public TelnyxCallDTO() {
    this.client_state = "";
    this.time_limit_secs = 14400;
  }

  public String getConnection_id() {
    return connection_id;
  }

  public void setConnection_id(String connection_id) {
    this.connection_id = connection_id;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getCall_control_id() {
    return call_control_id;
  }

  public void setCall_control_id(String call_control_id) {
    this.call_control_id = call_control_id;
  }

  public String getCall_leg_id() {
    return call_leg_id;
  }

  public void setCall_leg_id(String call_leg_id) {
    this.call_leg_id = call_leg_id;
  }

  public String getCall_session_id() {
    return call_session_id;
  }

  public void setCall_session_id(String call_session_id) {
    this.call_session_id = call_session_id;
  }

  public String getClient_state() {
    return client_state;
  }

  public void setClient_state(String client_state) {
    this.client_state = client_state;
  }

  public Integer getTime_limit_secs() {
    return time_limit_secs;
  }

  public void setTime_limit_secs(Integer time_limit_secs) {
    this.time_limit_secs = time_limit_secs;
  }

}