package com.xtaas.web.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Attribute {

	private String attributeName;
	private String attributeId;
	private String defaultSelected;
	private String attributeType;
	private List<Option> options;

	public String getAttributeName() {
		return attributeName;
	}

	@XmlElement(name = "attributename")
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeId() {
		return attributeId;
	}

	@XmlElement(name = "attributeId")
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getDefaultSelected() {
		return defaultSelected;
	}

	@XmlElement(name = "defaultSelected")
	public void setDefaultSelected(String defaultSelected) {
		this.defaultSelected = defaultSelected;
	}

	public String getAttributeType() {
		return attributeType;
	}

	@XmlElement(name = "attributeType")
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public List<Option> getOptions() {
		return options;
	}

	@XmlElementWrapper(name = "options")
	@XmlElement(name = "option")
	public void setOptions(List<Option> options) {
		this.options = options;
	}

	@Override
	public String toString() {
		return "Attribute [attributeName=" + attributeName + ", attributeId=" + attributeId + ", defaultSelected="
				+ defaultSelected + ", attributeType=" + attributeType + ", options=" + options + "]";
	}

}
