package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TelnyxCallWebhookDTOPayload {

    private String overlay;

    private String call_leg_id;

    private String start_time;

    private String client_state;

    private String from;

    private String to;

    private String reason;

    private String conference_id;

    private String fs_channel_id;

    private String connection_id;

    private String call_control_id;

    private String call_session_id;

    private String direction;

    private String state;

    private String caller_id_name;

    private String hangup_cause;

    private String hangup_source;

    private String recording_id;

    private String channels;

    private String format;

    private String end_time;

    private String status;

    private String sip_hangup_cause;

    private String recording_started_at;

    private String recording_ended_at;

    private TelnyxCallWebhookDTORecordingUrls recording_urls;

    private TelnyxCallWebhookDTORecordingUrls public_recording_urls;

    private String creator_call_session_id;

    private String conference_started;
    
    private String result;
    
    private String media_url;
    
    private String digit;

    public String getConference_started() {
		return conference_started;
	}

	public void setConference_started(String conference_started) {
		this.conference_started = conference_started;
	}

	public String getCreator_call_session_id() {
		return creator_call_session_id;
	}

	public void setCreator_call_session_id(String creator_call_session_id) {
		this.creator_call_session_id = creator_call_session_id;
	}

	public TelnyxCallWebhookDTOPayload() {
        this.client_state = "";
    }

    public String getCall_leg_id() {
        return call_leg_id;
    }

    public void setCall_leg_id(String call_leg_id) {
        this.call_leg_id = call_leg_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getClient_state() {
        return client_state;
    }

    public void setClient_state(String client_state) {
        this.client_state = client_state;
    }

    public String getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(String connection_id) {
        this.connection_id = connection_id;
    }

    public String getCall_control_id() {
        return call_control_id;
    }

    public void setCall_control_id(String call_control_id) {
        this.call_control_id = call_control_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCall_session_id() {
        return call_session_id;
    }

    public void setCall_session_id(String call_session_id) {
        this.call_session_id = call_session_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getConference_id() {
        return conference_id;
    }

    public void setConference_id(String conference_id) {
        this.conference_id = conference_id;
    }

    public String getFs_channel_id() {
        return fs_channel_id;
    }

    public void setFs_channel_id(String fs_channel_id) {
        this.fs_channel_id = fs_channel_id;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCaller_id_name() {
        return caller_id_name;
    }

    public String getHangup_cause() {
        return hangup_cause;
    }

    public void setHangup_cause(String hangup_cause) {
        this.hangup_cause = hangup_cause;
    }

    public String getHangup_source() {
        return hangup_source;
    }

    public void setHangup_source(String hangup_source) {
        this.hangup_source = hangup_source;
    }

    public String getRecording_id() {
        return recording_id;
    }

    public void setRecording_id(String recording_id) {
        this.recording_id = recording_id;
    }

    public String getChannels() {
        return channels;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public void setCaller_id_name(String caller_id_name) {
        this.caller_id_name = caller_id_name;
    }

    public TelnyxCallWebhookDTORecordingUrls getRecording_urls() {
        return recording_urls;
    }

    public void setRecording_urls(TelnyxCallWebhookDTORecordingUrls recording_urls) {
        this.recording_urls = recording_urls;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSip_hangup_cause() {
        return sip_hangup_cause;
    }

    public void setSip_hangup_cause(String sip_hangup_cause) {
        this.sip_hangup_cause = sip_hangup_cause;
    }

    public TelnyxCallWebhookDTORecordingUrls getPublic_recording_urls() {
        return public_recording_urls;
    }

    public void setPublic_recording_urls(TelnyxCallWebhookDTORecordingUrls public_recording_urls) {
        this.public_recording_urls = public_recording_urls;
    }

    public String getRecording_started_at() {
        return recording_started_at;
    }

    public void setRecording_started_at(String recording_started_at) {
        this.recording_started_at = recording_started_at;
    }

    public String getRecording_ended_at() {
        return recording_ended_at;
    }

    public void setRecording_ended_at(String recording_ended_at) {
        this.recording_ended_at = recording_ended_at;
    }

    public String getOverlay() {
        return overlay;
    }

    public void setOverlay(String overlay) {
        this.overlay = overlay;
    }

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMedia_url() {
		return media_url;
	}

	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }
	
    
}