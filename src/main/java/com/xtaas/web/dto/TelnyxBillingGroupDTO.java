package com.xtaas.web.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class TelnyxBillingGroupDTO {

	@JsonProperty("name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TelnyxBillingGroupDTO [name=" + name + "]";
	}

}