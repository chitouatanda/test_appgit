package com.xtaas.web.dto;

public class ConnectDetailsDTO {

	private String campaignName;
	
	private int recordsAttempted;
	
	private int connected;
	
	private int contacted;
	
	private int successRecords;
	
	private double costPerConnect;
	
	private double connectCost;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public int getRecordsAttempted() {
		return recordsAttempted;
	}

	public void setRecordsAttempted(int recordsAttempted) {
		this.recordsAttempted = recordsAttempted;
	}

	public int getConnected() {
		return connected;
	}

	public void setConnected(int connected) {
		this.connected = connected;
	}

	public int getContacted() {
		return contacted;
	}

	public void setContacted(int contacted) {
		this.contacted = contacted;
	}

	public int getSuccessRecords() {
		return successRecords;
	}

	public void setSuccessRecords(int successRecords) {
		this.successRecords = successRecords;
	}


	public double getConnectCost() {
		return connectCost;
	}

	public void setConnectCost(double connectCost) {
		this.connectCost = connectCost;
	}

	public double getCostPerConnect() {
		return costPerConnect;
	}

	public void setCostPerConnect(double costPerConnect) {
		this.costPerConnect = costPerConnect;
	}
	
	
}
