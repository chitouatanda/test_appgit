package com.xtaas.web.dto;

import javax.validation.constraints.NotNull;

import com.xtaas.domain.valueobject.AgentStatus;

public class AgentCommandOfflineDTO {
  
  @NotNull
	private AgentCommands agentCommand;

	private AgentStatus agentStatus;

	private String campaignId;

	private String agentType;

  private String agentId;
  
  private String callSid;
  
  private boolean usePlivo;
  
  private boolean isConferenceCall;

  public AgentCommands getAgentCommand() {
    return agentCommand;
  }

  public void setAgentCommand(AgentCommands agentCommand) {
    this.agentCommand = agentCommand;
  }

  public AgentStatus getAgentStatus() {
    return agentStatus;
  }

  public void setAgentStatus(AgentStatus agentStatus) {
    this.agentStatus = agentStatus;
  }

  public String getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(String campaignId) {
    this.campaignId = campaignId;
  }

  public String getAgentType() {
    return agentType;
  }

  public void setAgentType(String agentType) {
    this.agentType = agentType;
  }

  public String getAgentId() {
    return agentId;
  }

  public void setAgentId(String agentId) {
    this.agentId = agentId;
  }

  public String getCallSid() {
    return callSid;
  }

  public void setCallSid(String callSid) {
    this.callSid = callSid;
  }

  public boolean isUsePlivo() {
    return usePlivo;
  }

  public void setUsePlivo(boolean usePlivo) {
    this.usePlivo = usePlivo;
  }

  public boolean isConferenceCall() {
    return isConferenceCall;
  }

  public void setConferenceCall(boolean isConferenceCall) {
    this.isConferenceCall = isConferenceCall;
  }
  
}