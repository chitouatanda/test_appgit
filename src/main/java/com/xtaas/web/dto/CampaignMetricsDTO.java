package com.xtaas.web.dto;

import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.application.service.CampaignMetricsServiceImpl.CampaignMetrics;

public class CampaignMetricsDTO {
	@JsonProperty
	private String campaignId;
	
	@JsonProperty
	private HashMap<CampaignMetrics, String> metrics;

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the metrics
	 */
	public HashMap<CampaignMetrics, String> getMetrics() {
		return metrics;
	}

	/**
	 * @param metrics the metrics to set
	 */
	public void setMetrics(HashMap<CampaignMetrics, String> metrics) {
		this.metrics = metrics;
	}

	
	
}
