package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookEventDTO {

	@JsonProperty("email")
	private String email;
	@JsonProperty("timestamp")
	private int timestamp;
	@JsonProperty("smtp-id")
	private String smtp_id;
	@JsonProperty("event")
	private String event;
	@JsonProperty("sg_event_id")
	private String sg_event_id;
	@JsonProperty("sg_message_id")
	private String sg_message_id;
	@JsonProperty("response")
	private String response;
	@JsonProperty("attempt")
	private String attempt;
	@JsonProperty("useragent")
	private String useragent;
	@JsonProperty("ip")
	private String ip;
	@JsonProperty("url")
	private String url;
	@JsonProperty("reason")
	private String reason;
	@JsonProperty("status")
	private String status;
	@JsonProperty("asm_group_id")
	private int asm_group_id;
	@JsonProperty("cert_err")
	private int cert_err;
	@JsonProperty("tls")
	private int tls;
	@JsonProperty("prospectInteractionSessionId")
	private String prospectInteractionSessionId;
	@JsonProperty("campaignId")
	private String campaignId;
	@JsonProperty("prospectCallId")
	private String prospectCallId;
	@JsonProperty("callableEvent")
	private String callableEvent;
	@JsonProperty("clickUrl")
	private String clickUrl;
	@JsonProperty("unSubscribeUrl")
	private String unSubscribeUrl;
	@JsonProperty("campaignType")
	private String campaignType;
	@JsonProperty("supervisorId")
	private String supervisorId;
	
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public String getSmtp_id() {
		return smtp_id;
	}

	public void setSmtp_id(String smtp_id) {
		this.smtp_id = smtp_id;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getSg_event_id() {
		return sg_event_id;
	}

	public void setSg_event_id(String sg_event_id) {
		this.sg_event_id = sg_event_id;
	}

	public String getSg_message_id() {
		return sg_message_id;
	}

	public void setSg_message_id(String sg_message_id) {
		this.sg_message_id = sg_message_id;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getAttempt() {
		return attempt;
	}

	public void setAttempt(String attempt) {
		this.attempt = attempt;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAsm_group_id() {
		return asm_group_id;
	}

	public int getCert_err() {
		return cert_err;
	}

	public void setCert_err(int cert_err) {
		this.cert_err = cert_err;
	}

	public int getTls() {
		return tls;
	}

	public void setTls(int tls) {
		this.tls = tls;
	}

	public String getProspectInteractionSessionId() {
		return prospectInteractionSessionId;
	}

	public void setProspectInteractionSessionId(String prospectInteractionSessionId) {
		this.prospectInteractionSessionId = prospectInteractionSessionId;
	}

	public void setAsm_group_id(int asm_group_id) {
		this.asm_group_id = asm_group_id;
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public String getCallableEvent() {
		return callableEvent;
	}

	public void setCallableEvent(String callableEvent) {
		this.callableEvent = callableEvent;
	}

	public String getClickUrl() {
		return clickUrl;
	}

	public void setClickUrl(String clickUrl) {
		this.clickUrl = clickUrl;
	}

	public String getUnSubscribeUrl() {
		return unSubscribeUrl;
	}

	public void setUnSubscribeUrl(String unSubscribeUrl) {
		this.unSubscribeUrl = unSubscribeUrl;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	@Override
	public String toString() {
		return "WebHookEventDTO [email=" + email + ", timestamp=" + timestamp + ", smtp_id=" + smtp_id + ", event="
				+ event + ", sg_event_id=" + sg_event_id + ", sg_message_id=" + sg_message_id + ", response=" + response
				+ ", attempt=" + attempt + ", useragent=" + useragent + ", ip=" + ip + ", url=" + url + ", reason="
				+ reason + ", status=" + status + ", asm_group_id=" + asm_group_id + ", cert_err=" + cert_err + ", tls="
				+ tls + ", prospectInteractionSessionId=" + prospectInteractionSessionId + ", campaignId=" + campaignId
				+ ", prospectCallId=" + prospectCallId + ", callableEvent=" + callableEvent + ", clickUrl=" + clickUrl
				+ ", unSubscribeUrl=" + unSubscribeUrl + ", campaignType=" + campaignType + ", supervisorId="
				+ supervisorId + ", additionalProperties=" + additionalProperties + "]";
	}
}
