package com.xtaas.web.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.entity.Qa;

public class QaDTO {
	@JsonProperty
	private String id; 
	
	@JsonProperty
	private String partnerId;
	
	@JsonProperty
	private String firstName;
	
	@JsonProperty
	private String lastName;
	
	@JsonProperty
	private String username;
	
	@JsonProperty
	private String overview;
	
	@JsonProperty
	private AddressDTO address;
	
	@JsonProperty
	private List<String> languages;
	
	@JsonProperty
	private List<String> domains;
	
	@JsonProperty
	private double perHourPrice;
	
	public QaDTO() {
		
	}

	public QaDTO(Qa qa) {
		this.id = qa.getId();
		this.partnerId = qa.getPartnerId();
		this.firstName = qa.getFirstName();
		this.lastName = qa.getLastName();
		this.username = qa.getUsername();
		this.overview = qa.getOverview();
		if (qa.getAddress() != null) {
			this.address = new AddressDTO(qa.getAddress());
		}
		this.languages = qa.getLanguages();
		this.domains = qa.getDomains();
		this.perHourPrice = qa.getPerHourPrice();
	}
	
	public String getId() {
		return id;
	}
	
	public String getPartnerId() {
		return partnerId;
	}

	public String getName() {
		String name = this.firstName;
    	if (this.lastName != null) {
    		name = name + " " + this.lastName;
    	}
    	return name;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	public String getUsername() {
		return username;
	}

	public String getOverview() {
		return overview;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public List<String> getDomains() {
		return domains;
	}

	public double getPerHourPrice() {
		return perHourPrice;
	}

}
