package com.xtaas.web.dto;

public class TelnyxCallWebhookDTOMeta {

  private String delivered_to;

    private String attempt;

    public String getDelivered_to ()
    {
        return delivered_to;
    }

    public void setDelivered_to (String delivered_to)
    {
        this.delivered_to = delivered_to;
    }

    public String getAttempt ()
    {
        return attempt;
    }

    public void setAttempt (String attempt)
    {
        this.attempt = attempt;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [delivered_to = "+delivered_to+", attempt = "+attempt+"]";
    }

}