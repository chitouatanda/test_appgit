package com.xtaas.web.dto;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CampaignStatus;

public class CampaignSearchDTO {
	@JsonProperty
	private CampaignSearchClauses clause;
	@JsonProperty
	private String name;
	@JsonProperty
	private List<String> ids;
	@JsonProperty
	private CampaignStatus status;
	@JsonProperty
	private String sort = "name";
	@JsonProperty
	private Direction direction = Direction.ASC;
	@JsonProperty
	private int page;
	@JsonProperty
	private int size = 80;

	//Added for qafilter change
	@JsonProperty
	private String timeZone;
	@JsonProperty
	private String prospectCallStatus;
	
	/* START
	 * DATE : 19/04/2017
	 * REASON : Added to handle campaigns on supervisor screen, when flag is true will show campaigns with statuses RUNNING, PAUSED */
	@JsonProperty
	private boolean flag = false;	

	public boolean isFlag() {
		return flag;
	}
	/* END */

	public CampaignSearchClauses getClause() {
		return clause;
	}

	public String getName() {
		return name;
	}

	public List<String> getIds() {
		return ids;
	}

	public CampaignStatus getStatus() {
		return status;
	}

	public String getSort() {
		return sort;
	}

	public Direction getDirection() {
		return direction;
	}

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getProspectCallStatus() {
		return prospectCallStatus;
	}

	public void setProspectCallStatus(String prospectCallStatus) {
		this.prospectCallStatus = prospectCallStatus;
	}
}
