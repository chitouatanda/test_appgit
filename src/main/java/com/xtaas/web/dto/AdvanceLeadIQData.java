package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdvanceLeadIQData {

    @JsonProperty("groupedAdvancedSearch")
    private GroupedAdvancedSearch groupedAdvancedSearch;

    public GroupedAdvancedSearch getGroupedAdvancedSearch() {
        return groupedAdvancedSearch;
    }

    public void setGroupedAdvancedSearch(GroupedAdvancedSearch groupedAdvancedSearch) {
        this.groupedAdvancedSearch = groupedAdvancedSearch;
    }

    @Override
    public String toString() {
        return "AdvanceLeadIQData{" +
                "groupedAdvancedSearch=" + groupedAdvancedSearch +
                '}';
    }
}