package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"code",
"status",
"trackingRef"
})
public class LeadIQErrorExtensions {

@JsonProperty("code")
private String code;
@JsonProperty("status")
private Integer status;
@JsonProperty("trackingRef")
private Object trackingRef;

@JsonProperty("code")
public String getCode() {
return code;
}

@JsonProperty("code")
public void setCode(String code) {
this.code = code;
}

@JsonProperty("status")
public Integer getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(Integer status) {
this.status = status;
}

@JsonProperty("trackingRef")
public Object getTrackingRef() {
return trackingRef;
}

@JsonProperty("trackingRef")
public void setTrackingRef(Object trackingRef) {
this.trackingRef = trackingRef;
}

}