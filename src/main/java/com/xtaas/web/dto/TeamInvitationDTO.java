package com.xtaas.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.domain.valueobject.CampaignTeamInvitation;

public class TeamInvitationDTO {
	@JsonProperty
	@NotNull
	private String teamId;

	@JsonProperty
	private Date invitationDate;
	
	public TeamInvitationDTO() {
		
	}
	
	public TeamInvitationDTO(CampaignTeamInvitation campaignTeamInvitation) {
		this.teamId = campaignTeamInvitation.getTeamId();
		this.invitationDate = campaignTeamInvitation.getInvitationDate();
	}
	
	public CampaignTeamInvitation toCampaignTeamInvitation() {
		return new CampaignTeamInvitation(teamId);
	}
	
}
