package com.xtaas.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.BeanLocator;
import com.xtaas.db.entity.DNCList;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.DNCList.DNCTrigger;
import com.xtaas.service.UserService;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IDNCNumberDTO {
	@JsonProperty
	private String id;
	@JsonProperty
	private String phoneNumber;
	@JsonProperty
	private String note;
	@JsonProperty
	private Date createdDate;
	@JsonProperty
	private String createdByName; //for display only
	@JsonIgnore
	private UserService userService;
	@JsonProperty
	private DNCTrigger trigger; //what event triggered addition to DNC List
	@JsonProperty
	private String partnerId;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private String recordingUrl;
	@JsonProperty
	private String recordingUrlAws;
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	@JsonProperty
	private String domain;
	@JsonProperty
	private String company;
	@JsonProperty
	private String dncLevel;
	
	public IDNCNumberDTO() {
	}

	public IDNCNumberDTO(DNCList dncNumber) {
		this.id = dncNumber.getId();
		this.phoneNumber = dncNumber.getPhoneNumber();
		this.note = dncNumber.getNote();
		this.firstName = dncNumber.getFirstName();
		this.lastName = dncNumber.getLastName();
		this.setCreatedDate(dncNumber.getCreatedDate());
		this.userService = BeanLocator.getBean("userServiceImpl", UserService.class);
		this.partnerId = dncNumber.getPartnerId();
		this.trigger = dncNumber.getTrigger();
		if (dncNumber.getCreatedBy() != null) {
			if (dncNumber.getCreatedBy().equals("anonymousUser")) {
				this.setCreatedByName(dncNumber.getCreatedBy());
			}else {
				Login user = userService.getUser(dncNumber.getCreatedBy());
				if (user != null) {
					this.setCreatedByName(user.getName());
				}
			}
		}
	}
	
	public DNCList toDNCList() {
		DNCList dncNumber = new DNCList(phoneNumber, firstName, lastName, trigger);
		if (id != null) {
			dncNumber.setId(id);
		}
		dncNumber.setNote(note);
		return dncNumber;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	private void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the createdByname
	 */
	public String getCreatedByName() {
		return createdByName;
	}

	/**
	 * @param createdByname the createdByname to set
	 */
	private void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the trigger
	 */
	public DNCTrigger getTrigger() {
		return trigger;
	}

	/**
	 * @param trigger the trigger to set
	 */
	public void setTrigger(DNCTrigger trigger) {
		this.trigger = trigger;
	}

	/**
	 * @return the partnerId
	 */
	public String getPartnerId() {
		return partnerId;
	}

	/**
	 * @param partnerId the partnerId to set
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the prospectCallId
	 */
	public String getProspectCallId() {
		return prospectCallId;
	}

	/**
	 * @param prospectCallId the prospectCallId to set
	 */
	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	/**
	 * @return the recordingUrl
	 */
	public String getRecordingUrl() {
		return recordingUrl;
	}

	/**
	 * @param recordingUrl the recordingUrl to set
	 */
	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}
	/**
	 * @return the recordingUrlAws
	 */
	public String getRecordingUrlAws() {
		return recordingUrlAws;
	}

	/**
	 * @param recordingUrlAws the recordingUrlAws to set
	 */
	public void setRecordingUrlAws(String recordingUrlAws) {
		this.recordingUrlAws = recordingUrlAws;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDncLevel() {
		return dncLevel;
	}

	public void setDncLevel(String dncLevel) {
		this.dncLevel = dncLevel;
	}
}
