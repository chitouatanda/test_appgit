package com.xtaas.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"industry",
"name",
"locationInfo",
"address",
"country",
"domain",
"numberOfEmployees",
"phones"
})
public class LeadIQCompanyInfo {

@JsonProperty("industry")
private String industry;

@JsonProperty("name")
private String name;

@JsonProperty("locationInfo")
private LeadIQLocationInfo locationInfo;

@JsonProperty("address")
private String address;

@JsonProperty("country")
private String country;

@JsonProperty("domain")
private String domain;

@JsonProperty("numberOfEmployees")
private Integer numberOfEmployees;

@JsonProperty("phones")
private List<String> phones = null;

public LeadIQLocationInfo getLocationInfo() {
	return locationInfo;
}

public void setLocationInfo(LeadIQLocationInfo locationInfo) {
	this.locationInfo = locationInfo;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getDomain() {
	return domain;
}

public void setDomain(String domain) {
	this.domain = domain;
}

public Integer getNumberOfEmployees() {
	return numberOfEmployees;
}

public void setNumberOfEmployees(Integer numberOfEmployees) {
	this.numberOfEmployees = numberOfEmployees;
}

@JsonProperty("industry")
public String getIndustry() {
return industry;
}

@JsonProperty("industry")
public void setIndustry(String industry) {
this.industry = industry;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("phones")
public List<String> getPhones() {
return phones;
}

@JsonProperty("phones")
public void setPhones(List<String> phones) {
this.phones = phones;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("industry", industry).append("name", name).toString();
}

}