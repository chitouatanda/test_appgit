package com.xtaas.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialerStartupServlet extends HttpServlet {

	/**
	 * AutoGenerated Serial version UID
	 */
	private static final long serialVersionUID = 929187361532878594L;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Thread dialerMainThread = null;
	
	private Thread callbackQueueMonitorThread = null;
	
	private Thread activeConferencesSyncThread = null;
	
	private static boolean isServletInitialized = false;
	
	public void init(ServletConfig config) throws ServletException 
	{
		if (!isServletInitialized) //Some web-container creates instance pool of a servlet
		{
			logger.debug("Initalizing StartupServlet");
			
			//start ActiveConferences sync thread
			//startActiveConferencesSyncThread();
			
			logger.debug("Background Threads started");
			
			isServletInitialized = true;
		}
	}
	
	/*private void startActiveConferencesSyncThread() {
		logger.debug("startActiveConferencesSyncThread(): Starting ActiveConferencesSync thread.");
		ActiveConferencesSyncThread activeConferencesSyncTask = BeanLocator.getBean("activeConferencesSyncThread", ActiveConferencesSyncThread.class);
		activeConferencesSyncThread = new Thread(activeConferencesSyncTask, "ActiveConferencesSyncThread");
		activeConferencesSyncThread.start();
		logger.debug("startActiveConferencesSyncThread(): Started ActiveConferencesSync thread.");
	}*/

	public void destroy() {
		if (dialerMainThread != null) {
			logger.debug("destroy() : Interrupting the DialerMainThread");
			dialerMainThread.interrupt();
		}
		
		if (callbackQueueMonitorThread != null)	{
			logger.debug("destroy() : Interrupting the CallbackQueueMonitorThread");
			callbackQueueMonitorThread.interrupt();
		}
		
		if (activeConferencesSyncThread != null)	{
			logger.debug("destroy() : Interrupting the ActiveConferencesSyncThread");
			activeConferencesSyncThread.interrupt();
		}
	}
}
