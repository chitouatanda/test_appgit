package com.xtaas.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.xtaas.service.XtaasUserDetailsServiceImpl;

@Configuration
public class AppBeanConfiguration {

	@Autowired
	private XtaasUserDetailsServiceImpl xtaasUserDetailsServiceImpl;

	@Autowired
	private RoleHierarchyImpl roleHierarchy;

	@Bean
	public RoleHierarchyVoter roleVoter() {
		RoleHierarchyVoter roleHierarchyVoter = new RoleHierarchyVoter(roleHierarchy);
		return roleHierarchyVoter;
	}

	@Bean
	public DefaultWebSecurityExpressionHandler expressionHandler() {
		DefaultWebSecurityExpressionHandler defaultWebSecurityExpressionHandler = new DefaultWebSecurityExpressionHandler();
		defaultWebSecurityExpressionHandler.setRoleHierarchy(roleHierarchy);
		return defaultWebSecurityExpressionHandler;
	}

	@Bean
	public WebExpressionVoter expressionVoter() {
		WebExpressionVoter webExpressionVoter = new WebExpressionVoter();
		webExpressionVoter.setExpressionHandler(expressionHandler());
		return webExpressionVoter;
	}

	@Bean
	public AuthenticatedVoter authenticatedVoter() {
		AuthenticatedVoter authenticatedVoter = new AuthenticatedVoter();
		return authenticatedVoter;
	}

	/**
	 * used for WebSecurityConfiguration
	 * 
	 * @return
	 */
	@Bean
	public AffirmativeBased accessDecisionManager() {
		List<AccessDecisionVoter<?>> decisionVoters = new ArrayList<AccessDecisionVoter<?>>();
		decisionVoters.add(roleVoter());
		decisionVoters.add(authenticatedVoter());
		decisionVoters.add(expressionVoter());
		AffirmativeBased affirmativeBased = new AffirmativeBased(decisionVoters);
		return affirmativeBased;
	}

	/**
	 * used for WebSecurityConfiguration
	 * 
	 * @return AuthenticationFailureHandler
	 */
	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		return new SimpleUrlAuthenticationFailureHandler();
	}

	/**
	 * used for WebSecurityConfiguration to handle impersonate user
	 * 
	 * @return
	 */
	@Bean
	public SwitchUserFilter switchUserFilter() {
		SwitchUserFilter filter = new SwitchUserFilter();
		filter.setUserDetailsService(xtaasUserDetailsServiceImpl);
		filter.setSwitchUserUrl("/spr/impersonate");
		filter.setExitUserUrl("/j_spring_security_logout");
		filter.setTargetUrl("/spr/landingpage");
		return filter;
	}

	@Bean
	public MappingJackson2JsonView mappingJackson2JsonView() {
		return new MappingJackson2JsonView();
	}

	@Bean
	public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
		ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
		List<View> defaultViews = new ArrayList<View>();
		defaultViews.add(mappingJackson2JsonView());
		contentNegotiatingViewResolver.setDefaultViews(defaultViews);
		return contentNegotiatingViewResolver;
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver("/WEB-INF/jsp/",
				".jsp");
		internalResourceViewResolver.setExposeContextBeansAsAttributes(false);
		String[] beans = { "netProspexService" };
		internalResourceViewResolver.setExposedContextBeanNames(beans);
		return internalResourceViewResolver;
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
		return factoryBean;
	}

	@Bean
	public InternalResourceViewResolver jspViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver("/WEB-INF/jsp/", ".jsp");
		viewResolver.setViewClass(JstlView.class);
		return viewResolver;
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(Long.MAX_VALUE);
		multipartResolver.setMaxInMemorySize(10240000);
		return multipartResolver;
	}

}
