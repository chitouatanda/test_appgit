package com.xtaas.web.security;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.xtaas.infra.security.Roles;
import com.xtaas.infra.security.XTaaSPasswordEncoder;
import com.xtaas.service.XtaasUserDetailsServiceImpl;
import com.xtaas.web.filter.TokenAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private XtaasUserDetailsServiceImpl xtaasUserDetailsServiceImpl;

	@Autowired
	private XTaaSPasswordEncoder xTaaSPasswordEncoder;

	@Autowired
	private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;

	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Autowired
	private NoRedirectLogoutSuccessHandler noRedirectLogoutSuccessHandler;

	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	@Autowired
	private AffirmativeBased accessDecisionManager;

	@Autowired
	public TokenAuthenticationFilter authenticationTokenProcessingFilter;

	@Autowired
	private SwitchUserFilter switchUserFilter;

	@Override
	protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
		authManagerBuilder.userDetailsService(xtaasUserDetailsServiceImpl).passwordEncoder(xTaaSPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		login(http);
		logout(http);
		csrf(http);
		rememberMe(http);
		sessionManagement(http);
		exceptionHandling(http);
		authorizeRequests(http);
		otherConfigurations(http);
	}

	/**
	 * Configuring authentication.
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void login(HttpSecurity http) throws Exception {
		http.formLogin().loginProcessingUrl("/j_spring_security_check").usernameParameter("j_username")
				.passwordParameter("j_password").successHandler(restAuthenticationSuccessHandler)
				.failureHandler(authenticationFailureHandler);
	}

	/**
	 * Logout related configuration
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void logout(HttpSecurity http) throws Exception {
		http.logout().logoutSuccessHandler(noRedirectLogoutSuccessHandler).logoutUrl("/j_spring_security_logout");
	}

	/**
	 * Configures CSRF
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void csrf(HttpSecurity http) throws Exception {
		http.csrf().disable();
	}

	/**
	 * Configures remember-me
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void rememberMe(HttpSecurity http) throws Exception {
		http.rememberMe().key("mySecurityKey").userDetailsService(xtaasUserDetailsServiceImpl);
	}

	/**
	 * Configures session-management
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void sessionManagement(HttpSecurity http) throws Exception {
		http.sessionManagement().maximumSessions(2).expiredUrl("/spr/loginexpired");
	}

	/**
	 * Configures exception-handling with multiple authenticationEntryPoint
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void exceptionHandling(HttpSecurity http) throws Exception {
		http.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint)

				// authenticationEntryPoint for REST API access
				.defaultAuthenticationEntryPointFor(restAuthenticationEntryPoint,
						new AntPathRequestMatcher("/spr/api/**"));
	}

	/**
	 * URL based authorization configuration. Override this if needed.
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void authorizeRequests(HttpSecurity http) throws Exception {
		http.authorizeRequests()

				// Token based authentication used for REST API access
				.antMatchers("/spr/api/**").authenticated()

				.antMatchers("/spr/impersonate").hasRole(Roles.ROLE_SUPPORT_ADMIN.toString())

				.antMatchers(HttpMethod.GET, "/spr/loginexpired").hasRole(Roles.ROLE_ANONYMOUS.toString())
				.antMatchers(HttpMethod.POST, "/spr/login").hasRole(Roles.ROLE_ANONYMOUS.toString())
				.antMatchers("/spr/rest/passwordattempt/**").hasRole(Roles.ROLE_ANONYMOUS.toString())
				.antMatchers("/spr/rest/user/resetpassword").hasRole(Roles.ROLE_ANONYMOUS.toString())
				.antMatchers("/spr/rest/user/changeforgotpassword").hasRole(Roles.ROLE_ANONYMOUS.toString())

				.antMatchers("/spr/twilio/**").hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/plivo/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/telnyx/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/signalwire/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString())

				.antMatchers("/spr/testapi/**").hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/campaign/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/login/upload")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dialer/confendevent")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/email/bounceevent")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/email/notification/event")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/encryptProspect/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/recordingurl/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/email/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/outboundnumbers")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/agent/allocate/dialer/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/cacherefresh/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/stamp/sourceType")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/zoombuy")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/onlineagentreport")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/everstring/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dnc")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/insideview/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/agent/offline")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/cachecount/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/applicationproperties")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/countrydetails/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/agentonlinereport")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/ds/campaign/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dnc")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/client/token")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/campaignsuppression/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/buildcache")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/admin/campaignsettings/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/campaignmetadata/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/suppress/callable/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/leadreport/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dialer/savecallmetrics")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/call/twilio/status")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dialer/handleincoming")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dialer/saveincomingcallmsg")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/dialer/callanswered")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/cas/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/abmlist/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/transcription/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/abmlist/count/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/globalcontact/generate")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/prospectcalllog/getbackcallable")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/transcript")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/remove/logincache/**")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/changeloglevel")
				.hasRole(Roles.ROLE_ANONYMOUS.toString()).antMatchers("/spr/globalemailsuppression")
				.hasRole(Roles.ROLE_ANONYMOUS.toString())
				.antMatchers(HttpMethod.GET, "/spr/**").hasRole(Roles.ROLE_USER.toString())
				.antMatchers(HttpMethod.POST, "/spr/**").hasRole(Roles.ROLE_USER.toString())

				.antMatchers("/**").hasRole(Roles.ROLE_ANONYMOUS.toString())

				.accessDecisionManager(accessDecisionManager);
	}

	/**
	 * Override this to add more http configurations, such as more authentication
	 * methods, switchUserFilter, etc.
	 * 
	 * @param http
	 * @throws Exception
	 */
	protected void otherConfigurations(HttpSecurity http) throws Exception {
		// Token based authentication used for REST API access
		http.addFilterBefore(authenticationTokenProcessingFilter, UsernamePasswordAuthenticationFilter.class);

		// Adds switch-user filter
		http.addFilterAt(switchUserFilter, SwitchUserFilter.class);
	}

}
