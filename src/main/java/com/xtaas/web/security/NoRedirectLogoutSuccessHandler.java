package com.xtaas.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority;
import org.springframework.stereotype.Component;

import com.xtaas.db.repository.ImpersonateAuditLogRepository;
import com.xtaas.valueobjects.UserPrincipal;

@Component(value = "noRedirectLogoutSuccessHandler")
public class NoRedirectLogoutSuccessHandler implements LogoutSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(NoRedirectLogoutSuccessHandler.class);

	@Autowired
	private ImpersonateAuditLogRepository impersonateAuditLogRepository;

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		// no redirect !! (unlike @SimpleUrlLogoutSuccessHandler, that redirects after
		// logout)
		if (authentication != null) {
			userImpersonateAuditLog(authentication);
			logger.debug("onLogoutSuccess() : User logged out : " + authentication.getName());
		} else {
			logger.debug("onLogoutSuccess() : User not found");
		}
	}

	/**
	 * if user was impersonated user then update ImpersonateAuditLog entry
	 */
	private void userImpersonateAuditLog(Authentication authentication) {
		if (authentication == null || authentication.getAuthorities() == null) {
			logger.error("user logout - got null for authorities");
			return;
		}
		for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
			if (grantedAuthority instanceof SwitchUserGrantedAuthority) {
				SwitchUserGrantedAuthority switchUserGrantedAuthority = (SwitchUserGrantedAuthority) grantedAuthority;
				UserPrincipal userPrincipal = (UserPrincipal) switchUserGrantedAuthority.getSource().getPrincipal();
				String impersonatedUserId = authentication.getName();
				logger.debug("User Impersonate Logout - Admin:{} user:{}", userPrincipal.getUsername(),
						impersonatedUserId);
				impersonateAuditLogRepository.findLatest(userPrincipal.getUsername(), impersonatedUserId).ifPresent(
						impersonateAuditLog -> impersonateAuditLogRepository.save(impersonateAuditLog.withLogout()));
				break;
			}
		}
	}

}
