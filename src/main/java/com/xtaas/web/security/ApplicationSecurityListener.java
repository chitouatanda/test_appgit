package com.xtaas.web.security;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import com.xtaas.db.entity.LoginActivityLog;
import com.xtaas.db.entity.LoginActivityLog.LoginActivityType;
import com.xtaas.db.repository.LoginActivityLogRepository;
import com.xtaas.infra.security.Roles;

@Component
public class ApplicationSecurityListener implements ApplicationListener<ApplicationEvent> {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationSecurityListener.class);
	
	@Autowired
	private LoginActivityLogRepository loginActivityLogRepository;

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		if (event instanceof InteractiveAuthenticationSuccessEvent) { // User log in
            InteractiveAuthenticationSuccessEvent iasEvent = (InteractiveAuthenticationSuccessEvent) event;
            String userName = iasEvent.getAuthentication().getName();
            @SuppressWarnings("unchecked")
			List<GrantedAuthority> authorities = (List<GrantedAuthority>) iasEvent.getAuthentication().getAuthorities();
            if (authorities != null && !authorities.isEmpty()) {
	            String userRole = authorities.get(0).getAuthority();
	            if (userName != null && userRole != null && !Roles.ROLE_SYSTEM.name().equals(userRole)) {
	                LoginActivityLog loginActivityLog = new LoginActivityLog(userName, LoginActivityType.LOGIN);
					loginActivityLogRepository.save(loginActivityLog);
					logger.debug("User Logged In: " + userName);
	            }
            }
        } else if (event instanceof SessionDestroyedEvent) { // User log out / session timeout
            SessionDestroyedEvent sdEvent = (SessionDestroyedEvent) event;
            List<SecurityContext> lstSecurityContext = sdEvent.getSecurityContexts();
            for (SecurityContext securityContext : lstSecurityContext)
            {
                String userName = securityContext.getAuthentication().getName();
                if (userName != null) {
                	LoginActivityLog loginActivityLog = new LoginActivityLog(userName, LoginActivityType.LOGOUT);
					loginActivityLogRepository.save(loginActivityLog);
					logger.debug("User Logged Out: " + userName);
                }
            }
        }
	}
}
