package com.xtaas.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.xtaas.job.DailyDataBuyJob;
import com.xtaas.worker.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.plivo.api.Plivo;
import com.xtaas.BeanLocator;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;

@SuppressWarnings("serial")
public class ApplicationStartupServlet extends HttpServlet {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationStartupServlet.class);

	private Thread agentCampaignChangeMonitorThread = null;

	private Thread prospectCallDetailsUpdateThread = null;

	private Thread failureRecordingsDownloadThread = null;

	private Thread zoomOAuthTokenThread = null;
	
	private Thread catchOSSignalThread = null;
	
	private Thread speechToTextThread = null;

	private Thread dataBuyThread = null;

	private Thread dailyDataBuyThread = null;

	private static boolean isServletInitialized = false;

	public void init(ServletConfig config) throws ServletException {
		if (!isServletInitialized) // Some web-container creates instance pool of a servlet
		{
			logger.debug("Initalizing ApplicationStartupServlet");

			// start Agent's Campaign Change Monitor thread
			startAgentCampaignChangeMonitorThread();

			// startProspectCallDetailsUpdateThread();

			startFailureRecordingsDownloadThread();

			startDataBuyThread();

			startZoomTokenThread();
			
			startCatchOSSignalThread();
			
			startSpeechTotTextThread();

			//startDailyDataBuyThread();

			plivoInit(); 

			logger.debug("Background Threads started");

			isServletInitialized = true;
		}
	}

	private void startAgentCampaignChangeMonitorThread() {
		logger.debug("startAgentCampaignChangeMonitorThread(): Starting AgentCampaignChangeMonitor thread.");
		AgentCampaignChangeMonitorThread agentCampaignChangeMonitorTask = BeanLocator
				.getBean("agentCampaignChangeMonitorThread", AgentCampaignChangeMonitorThread.class);
		agentCampaignChangeMonitorThread = new Thread(agentCampaignChangeMonitorTask,
				"AgentCampaignChangeMonitorThread");
		agentCampaignChangeMonitorThread.start();
		logger.debug("startAgentCampaignChangeMonitorThread(): Started AgentCampaignChangeMonitor thread.");
	}

	private void startProspectCallDetailsUpdateThread() {
		logger.debug("startProspectCallDetailsUpdateThread(): Starting ProspectCallDetailsUpdateThread.");
		ProspectCallDetailsUpdateThread prospectCallDetailsUpdateTask = BeanLocator
				.getBean("prospectCallDetailsUpdateThread", ProspectCallDetailsUpdateThread.class);
		prospectCallDetailsUpdateThread = new Thread(prospectCallDetailsUpdateTask, "ProspectCallDetailsUpdateThread");
		prospectCallDetailsUpdateThread.start();
		logger.debug("startProspectCallDetailsUpdateThread(): Started ProspectCallDetailsUpdateThread.");
	}

	private void startFailureRecordingsDownloadThread() {
		logger.debug("startFailureRecordingsDownloadThread(): Starting FailureRecordingsDownloadThread.");
		FailureRecordingsDownloadThread failureRecordingsDownloadTask = BeanLocator
				.getBean("failureRecordingsDownloadThread", FailureRecordingsDownloadThread.class);
		failureRecordingsDownloadThread = new Thread(failureRecordingsDownloadTask, "FailureRecordingsDownloadThread");
		failureRecordingsDownloadThread.start();
		logger.debug("startFailureRecordingsDownloadThread(): Started FailureRecordingsDownloadThread.");
	}

	private void startZoomTokenThread() {
		logger.debug("startZoomTokenThread(): Starting zoomTokenThread.");
		ZoomOAuthTokenThread zoomOAuthTokenTask = BeanLocator.getBean("zoomOAuthTokenThread",
				ZoomOAuthTokenThread.class);
		zoomOAuthTokenThread = new Thread(zoomOAuthTokenTask, "zoomOAuthTokenThread");
		zoomOAuthTokenThread.start();
		logger.debug("startZoomTokenThread(): Started zoomtokenthread.");
	}
	
	private void startCatchOSSignalThread() {
		logger.debug("startCatchOSSignal(): Starting catch OS Signal.");
		CatchOSSignalThread catchOSSignalTask = BeanLocator.getBean("catchOSSignalThread",
				CatchOSSignalThread.class);
		catchOSSignalThread = new Thread(catchOSSignalTask, "catchOSSignalThread");
		catchOSSignalThread.start();
		logger.debug("startCatchOSSignalThread(): Started startCatchOSSignalThread.");
	}
	
	private void startSpeechTotTextThread() {
		logger.debug("startSpeechTotTextThread(): Starting Speech to Text.");
//		SpeechToTextThread speechToTextTask = BeanLocator.getBean("speechToTextThread",
//				SpeechToTextThread.class);
//		speechToTextThread = new Thread(speechToTextTask, "SpeechToTextThread");
//		speechToTextThread.start();
		logger.debug("startSpeechTotTextThread(): Started startSpeechTotTextThread.");
	}

	private void startDataBuyThread() {
		logger.debug("startDataBuyThread(): Starting startDataBuyThread.");
		DataBuyThread dataBuyThreadTask = BeanLocator.getBean("dataBuyThread", DataBuyThread.class);
		dataBuyThreadTask.setServerGettingStarted(true);
		dataBuyThread = new Thread(dataBuyThreadTask, "DataBuyThread");
		dataBuyThread.start();
		logger.debug("startDataBuyThread(): Started DataBuyThread.");
	}

	private void plivoInit() {
		Plivo.init(ApplicationEnvironmentPropertyUtils.getPlivoAuthId(),
				ApplicationEnvironmentPropertyUtils.getPlivoAuthToken());
	}

	private void startDailyDataBuyThread() {
		logger.debug("startDailyDataBuyThread(): Starting dailyDataBuyThread.");
		DailyDataBuyThread dailyDataBuyThreadTask = BeanLocator.getBean("dailyDataBuyThread", DailyDataBuyThread.class);
		dailyDataBuyThreadTask.setServerGettingStarted(true);
		dailyDataBuyThread = new Thread(dailyDataBuyThreadTask, "dailyDataBuyThread");
		dailyDataBuyThread.start();
		logger.debug("startDailyDataBuyThread(): Started DailyDataBuyThread.");
	}

	public void destroy() {
		if (agentCampaignChangeMonitorThread != null) {
			logger.debug("destroy() : Interrupting the AgentCampaignChangeMonitorThread");
			agentCampaignChangeMonitorThread.interrupt();
		}
		if (prospectCallDetailsUpdateThread != null) {
			logger.debug("destroy() : Interrupting the ProspectCallDetailsUpdateThread");
			prospectCallDetailsUpdateThread.interrupt();
		}
	}

}
