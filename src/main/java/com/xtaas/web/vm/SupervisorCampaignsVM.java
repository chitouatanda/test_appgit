
package com.xtaas.web.vm;

import java.util.Date;
import java.util.Map;

import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.domain.valueobject.QualificationCriteria;

public class SupervisorCampaignsVM {

	public String campaignId;

	public String name;

	public CampaignStatus status;

	public String timeZone;

	public CallSpeed callSpeedPerMinPerAgent;

	public LeadSortOrder leadSortOrder;

	public Date startDate;

	private boolean readOnlyCampaign;

	private Map<String, Float> retrySpeed;
	
	private QualificationCriteria qualificationCriteria;
	
	private String campaignRequirements;
	
	private EmailMessage assetEmailTemplate;
	
	private String type;

	private boolean abm;

	private boolean maxEmployeeAllow;

	public SupervisorCampaignsVM() {

	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CampaignStatus getStatus() {
		return status;
	}

	public void setStatus(CampaignStatus status) {
		this.status = status;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public boolean isReadOnlyCampaign() {
		return readOnlyCampaign;
	}

	public void setReadOnlyCampaign(boolean readOnlyCampaign) {
		this.readOnlyCampaign = readOnlyCampaign;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public QualificationCriteria getQualificationCriteria() {
		return qualificationCriteria;
	}

	public void setQualificationCriteria(QualificationCriteria qualificationCriteria) {
		this.qualificationCriteria = qualificationCriteria;
	}

	public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}

	public EmailMessage getAssetEmailTemplate() {
		return assetEmailTemplate;
	}

	public void setAssetEmailTemplate(EmailMessage assetEmailTemplate) {
		this.assetEmailTemplate = assetEmailTemplate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAbm() {
		return abm;
	}

	public void setAbm(boolean abm) {
		this.abm = abm;
	}

	public boolean isMaxEmployeeAllow() {
		return maxEmployeeAllow;
	}

	public void setMaxEmployeeAllow(boolean maxEmployeeAllow) {
		this.maxEmployeeAllow = maxEmployeeAllow;
	}

	@Override
	public String toString() {
		return "SupervisorCampaignsVM{" +
				"campaignId='" + campaignId + '\'' +
				", name='" + name + '\'' +
				", status=" + status +
				", timeZone='" + timeZone + '\'' +
				", callSpeedPerMinPerAgent=" + callSpeedPerMinPerAgent +
				", leadSortOrder=" + leadSortOrder +
				", startDate=" + startDate +
				", readOnlyCampaign=" + readOnlyCampaign +
				", retrySpeed=" + retrySpeed +
				", qualificationCriteria=" + qualificationCriteria +
				", campaignRequirements='" + campaignRequirements + '\'' +
				", assetEmailTemplate=" + assetEmailTemplate +
				", type='" + type + '\'' +
				", abm=" + abm +
				", maxEmployeeAllow=" + maxEmployeeAllow +
				'}';
	}


}
