/**
 * 
 */
package com.xtaas.web.filter;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;

/**
 * Simple CORS filter for setting CORS headers
 * @author djain
 *
 */
@Component
public class SimpleCORSFilter implements Filter {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SimpleCORSFilter.class);

	private static final String XTAASCORP_URL = "https://xtaascorp.herokuapp.com"; 
	private static final String XTAASUPGRADE_URL = "https://xtaasupgrade.herokuapp.com";
	private static final String DEMANDUPGRADE_URL = "https://demandupgrade.herokuapp.com";
	private static final String DEMANDSHORE_URL = "https://demandshore.herokuapp.com";
	private static final String DEMANDSHOREQA_URL = "https://demandshoreqa.herokuapp.com";
	private static final String LOCALHOST_URL = "http://localhost:8080";

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		//String uri = ((HttpServletRequest) req).getRequestURI();
		//logger.debug("SimpleCORSFilter filter invoked. Url : {}", uri);
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		if (XTAASCORP_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", XTAASCORP_URL);
		} else if (XTAASUPGRADE_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", XTAASUPGRADE_URL);
		} else if (LOCALHOST_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", LOCALHOST_URL);
		} else if (DEMANDUPGRADE_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", DEMANDUPGRADE_URL);
		} else if (DEMANDSHORE_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", DEMANDSHORE_URL);
		} else if (DEMANDSHOREQA_URL.equals(request.getHeader("Origin"))) {
			response.setHeader("Access-Control-Allow-Origin", DEMANDSHOREQA_URL);
		} else {
			response.setHeader("Access-Control-Allow-Origin", ApplicationEnvironmentPropertyUtils.getBssServerBaseUrl());
		}
		String serverRestrictUrl = System.getenv("server.restrict.url");
		String refererUrl = request.getHeader("referer");
		if (serverRestrictUrl != null && !serverRestrictUrl.isEmpty() && refererUrl != null && !refererUrl.isEmpty()
				&& refererUrl.contains(serverRestrictUrl)) {
			return;
		}
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "OPTIONS, POST, GET, PUT");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,X-Prototype-Version,Content-Type,Cache-Control,Pragma,Origin");
		response.setHeader("Access-Control-Max-Age", "600");
		response.setHeader("Access-Control-Expose-Headers","Access-Control-Allow-Origin");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Cache-Control","max-age=0");
	    chain.doFilter(req, res);
	}

	public void init(FilterConfig filterConfig) {}

	public void destroy() {}
}
