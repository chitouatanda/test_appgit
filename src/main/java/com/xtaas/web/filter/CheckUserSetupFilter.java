/**
 * 
 */
package com.xtaas.web.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.xtaas.db.entity.Organization;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.UserPrincipal;

/**
 * @author djain
 *
 */
public class CheckUserSetupFilter extends OncePerRequestFilter {
	
	private static final Logger logger = LoggerFactory.getLogger(CheckUserSetupFilter.class);
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String uri = ((HttpServletRequest) request).getRequestURI();
		logger.debug("URI = " + uri);
		
		String primaryRole = XtaasUserUtils.getPrimaryRole();
        
        if (primaryRole != null && primaryRole.equalsIgnoreCase(Roles.ROLE_CLIENT.name())) {
        	UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	String username = principal.getUsername();
        	//check if Organization is already setup for this user, if not then redirect to organization setup page
        	Organization org = principal.getOrganization();
        	if (null == org) {
        		XtaasUserUtils.refreshSecurityContext(); //Fix for BUG XP-277. Rebuilding the security context after org has been setup
        		principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); //rechecking the principal to get Org
        		org = principal.getOrganization();
        		if (null == org) {
					logger.debug("User [{}] DOES NOT have the Organization [{}] setup. Redirecting to Org Setup page... ", username, principal.getLogin().getOrganization());
					//angular cannot intercept 302 response, hence sending the Location header with 200 code for browser to redirect
					String redirUrl = ApplicationEnvironmentPropertyUtils.getBssServerBaseUrl()+"/client/add"; //http://gory-web-2586.herokuapp.com/client/add
					response.addHeader("Location", redirUrl);
					return;
        		}
        	} 
        }
        filterChain.doFilter(request, response);
	}
}
