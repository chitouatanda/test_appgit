/**
 * 
 */
package com.xtaas.web.filter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * A simple pre-authenticated filter which obtains the username from a request paramater, for pre-authenticated requests coming 
 * from systems like Salesforce .
 * <p>
 * As with most pre-authenticated scenarios, it is essential that the external authentication system is set up
 * correctly as this filter does no authentication whatsoever. All the protection is assumed to be provided externally
 * and if this filter is included inappropriately in a configuration, it would be possible  to assume the
 * identity of a user merely by setting the request parameter with the secret key passed on by the external system. 
 * This secret key is like password which is mapped to XTaaS user. XTaaS user is retrieved based on this secret key.
 * <p>
 * The property {@code principalRequestHeader} is the name of the request parameter that contains the encrypted secret key. 
 * <p>
 * If the parameter is missing from the request, it will return Null.
 * 
 * @author djain
 *
 */
public class RequestParamPreAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

	private static final Logger logger = LoggerFactory.getLogger(RequestParamPreAuthenticationFilter.class);
	private String principalRequestParameter = "PREAUTH_PRINCIPAL";
    private String credentialsRequestHeader;
    
	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedPrincipal(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		String principal = request.getParameter(principalRequestParameter);
		
		if (logger.isDebugEnabled()) {
            logger.debug("RequestParam PreAuthenticated principal: " + principal);
        }
		return principal;
	}

	/* 
	 * Usually Not Applicable.
	 * (non-Javadoc)
	 * @see org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedCredentials(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		if (credentialsRequestHeader != null) {
            return request.getParameter(credentialsRequestHeader);
        }

        return "N/A";
	}

	/**
	 * @param principalRequestParameter the principalRequestParameter to set
	 */
	public void setPrincipalRequestParameter(String principalRequestParameter) {
		this.principalRequestParameter = principalRequestParameter;
	}

	public void setCredentialsRequestHeader(String credentialsRequestHeader) {
        this.credentialsRequestHeader = credentialsRequestHeader;
    }
}
