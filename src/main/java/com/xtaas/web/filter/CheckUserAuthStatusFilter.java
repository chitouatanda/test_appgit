/**
 * 
 */
package com.xtaas.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.utils.XtaasUserUtils;

/**
 * @author djain
 *
 */
@Component
public class CheckUserAuthStatusFilter extends OncePerRequestFilter {
	
	private static final Logger logger = LoggerFactory.getLogger(CheckUserAuthStatusFilter.class);
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String uri = ((HttpServletRequest) request).getRequestURI();
		logger.debug("CheckUserAuthStatusFilter. Requested URI = " + uri);
		if (uri.startsWith("/j_spring_security_check")) {
			String uname = request.getParameter("j_username");
			//check if user is already logged in and throw Exception if now logging in with a different user 
			String currentLoginUsername = XtaasUserUtils.getCurrentLoggedInUsername(); 
			if (currentLoginUsername != null && !currentLoginUsername.equals("anonymousUser") && !currentLoginUsername.equals(uname)) {
				logger.error("User already logged in as " + currentLoginUsername);
				String redirUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl()+"/#!/login?err=CONFLICT";
				response.addHeader("Location", redirUrl);
				return;
			} 
		}
		filterChain.doFilter(request, response);
	}
}
