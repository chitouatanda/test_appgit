/**
 * 
 */
package com.xtaas.web.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.resource.list.CallList;
import com.xtaas.BeanLocator;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.ActivePreviewCall;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.ActivePreviewCallQueueRepository;
import com.xtaas.domain.valueobject.AgentCall;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.pusher.Pusher;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUtils;

/**
 * This filter intercepts the requests to dial.xml and if the request is to ConnectToAgent, then spawns a separate thread to 
 * process whisper message for an agent 
 * 
 * @author djain
 *
 */
public class DialTwimlFilter implements Filter {
	
	private static final Logger logger = LoggerFactory.getLogger(DialTwimlFilter.class);
	
	private AgentRegistrationService agentRegistrationService;
	
	private ProspectCallLogService prospectCallLogService;
	
	private ActivePreviewCallQueueRepository activePreviewCallQueueRepository;
	
	private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("[\\?:(0-9-+\\?:)]*");
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		setAgentRegistrationService(BeanLocator.getBean("agentRegistrationServiceImpl", AgentRegistrationService.class));
		setProspectCallLogService(BeanLocator.getBean("prospectCallLogServiceImpl", ProspectCallLogService.class));
		setActivePreviewCallQueueRepository(BeanLocator.getBean("activePreviewCallQueueRepository", ActivePreviewCallQueueRepository.class));
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {/*
		String uri = ((HttpServletRequest) request).getRequestURI();
		logger.debug("DialTwimlFilter Filter invoked. URI = " + uri);
		
		final String callSid = request.getParameter("CallSid");
		final String caller = request.getParameter("Caller");

		DialerMode dialerMode = DialerMode.PREVIEW;
		if (callSid.startsWith("CA") && !caller.startsWith("client:")) { //twilio callsid always starts with CA while for preview mode dummy callsid is sent by dialer AND caller is a outbound number in case of POWER 
			dialerMode = DialerMode.POWER;
		}
		final DialerMode finalDialerMode = dialerMode;
		
		String phone=request.getParameter("pn");
		String pcid=request.getParameter("pcid");
		String callerId=request.getParameter("callerId");
		String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
		String answeredBy = request.getParameter("AnsweredBy");
		
		String isAgent=request.getParameter("agent");
       	if (null == isAgent) isAgent = "false";
		if (isAgent.equals("true") && phone != null) {
			AgentCall agentCall = agentRegistrationService.getAgentCall(phone);
			if (agentCall != null && agentCall.getCurrentCampaign() != null) {
				request.setAttribute("campaignId", agentCall.getCurrentCampaign().getId()); //setting campaignId for Calls initiated by agent in POWER mode. Needed for callbilling
			}
		}
		
		String machineAnsweredDetection = request.getParameter("MachineAnsweredDetection");
		if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
			AgentCall agentCall = agentRegistrationService.getAgentCall(phone); //phone is agentId in case of supervisorJoinRequest
			DialerMode agentsCampaignDialerMode = agentCall.getCurrentCampaign().getDialerMode();
			request.setAttribute("agentsCampaignDialerMode", agentsCampaignDialerMode.name());
			// if request initiated by supervisor to join the call then Pass request back down the filter chain
			chain.doFilter(request, response);
		} else if (answeredBy != null && answeredBy.equalsIgnoreCase("machine_start")
				&& machineAnsweredDetection != null && machineAnsweredDetection.equalsIgnoreCase("true")) {
			// Pass request back down the filter chain
			chain.doFilter(request, response);
		} else if (DialerMode.PREVIEW.equals(dialerMode) && phone != null && PHONE_NUMBER_PATTERN.matcher(phone).matches() && pcid != null && callerId != null && caller != null) { 
			String callerAgentId = StringUtils.replace(caller, "client:", "");
			
			String previewCallSid = makePreviewCall(phone, callerId, pcid, callerAgentId);
			if (previewCallSid != null) {
				//put this in preview queue for monitoring
				ActivePreviewCall activePreviewCall = new ActivePreviewCall(previewCallSid, XtaasConstants.TWILIO_CALL_STATUS_QUEUED, callerAgentId, pcid, callSid);
				this.activePreviewCallQueueRepository.save(activePreviewCall);
			}
			chain.doFilter(request, response); // Pass request back down the filter chain
		} else {
			//if answered by human OR dialer is in PREVIEW mode (answeredby is NULL)
			String requestFromDialer = request.getParameter("dialer");
			AgentCall allocatedAgent = null;
			String prospectCallAsJson = null;
			ProspectCall prospectCall = null;
			if (null != requestFromDialer) { //picked by human, so connect to an agent 
				//find the available agent to connect with the prospect
				logger.debug("===========> Finding available agent for CallSid [{}]", callSid);
				allocatedAgent = agentRegistrationService.allocateAgent(callSid);
				if (null != allocatedAgent) {
					request.setAttribute("connectToAgent", allocatedAgent.getAgentId());
					prospectCall = allocatedAgent.getProspectCall();
					prospectCallAsJson = JSONUtils.toJson(prospectCall);
					logger.debug("##### doFilter() : Connecting PhoneNumber: [{}] to agent [{}] #####", prospectCall.getProspect().getPhone(), allocatedAgent.getAgentId());
					logger.debug("============> Allocated Agent [{}] for ProspectCall  [{}]", allocatedAgent.getAgentId(), prospectCallAsJson);
				} else { //if no agent available
					try {
						Thread.sleep(1000); //wait for a sec and attempt again to find an agent
					} catch (InterruptedException e) { }
					allocatedAgent = agentRegistrationService.allocateAgent(callSid); //reattempting to find the available agent
					if (null == allocatedAgent) { //still no agent available then play the abandon msg
						request.setAttribute("connectToAgent", "ivr");
						logger.debug("============> No Agent available for CallSid  [{}]", callSid);
					} else {
						request.setAttribute("connectToAgent", allocatedAgent.getAgentId());
						prospectCall = allocatedAgent.getProspectCall();
						prospectCallAsJson = JSONUtils.toJson(prospectCall);
						logger.debug("##### doFilter() : Connecting PhoneNumber: [{}] to agent [{}] #####", prospectCall.getProspect().getPhone(), allocatedAgent.getAgentId());
						logger.debug("============> Allocated Agent [{}] for ProspectCall  [{}]", allocatedAgent.getAgentId(), prospectCallAsJson);
					}
				}
			}
			
			final ProspectCall finalProspectCall = prospectCall;
			final String finalProspectCallAsJson = prospectCallAsJson;
			final String whisperMsg = request.getParameter("whisperMsg");
			final AgentCall finalAllocatedAgent = allocatedAgent;
			if (uri.contains("dial.xml.jsp")) {
				Thread thread = new Thread() {
					public void run() {
						logger.debug("run() : PlayWhisperThread started");
						if (finalAllocatedAgent != null) { //applicable for POWER and PREVIEW mode
							try {
								//Push Agent object on to allocated agent's screen. This agent object contains call specific details like CallSid, Prospect object
								if (finalProspectCallAsJson != null) {
									pushMessageToAgent(finalAllocatedAgent.getAgentId(), "prospect_call", finalProspectCallAsJson);
								}
							} catch (Exception ex) {
								logger.error("run() : Error while pushing Lead details to Agent screen : " + finalAllocatedAgent.getAgentId(), ex);
								logger.debug("run() : Retrying to send prospect_call notification onto Agent screen : " + finalAllocatedAgent.getAgentId());
								if (finalProspectCallAsJson != null) {
									pushMessageToAgent(finalAllocatedAgent.getAgentId(), "prospect_call", finalProspectCallAsJson);
								} 
							}
							
							//Play Whisper Msg only in POWER mode
							if (finalDialerMode.equals(DialerMode.POWER)) {
								String agentWhisperConfName =  finalAllocatedAgent.getAgentId() + "WHISPER";
								Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
								Account account = channelTwilio.getChannelConnection();
								Map<String, String> filter = new HashMap<String, String>();
								filter.put("From", "client:"+agentWhisperConfName);
								filter.put("Status", "in-progress");
								CallList callList = account.getCalls(filter); 
								Call agentsCall = null;
								for (Call inProgressCall : callList) {
								    agentsCall = inProgressCall;
								    break; //return the first one only..there shouldn't be more
								}
								if (agentsCall != null) {
									Map<String, String> agentsCallParams = new HashMap<String, String>();
									try {
										agentsCallParams.put("Url", ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/twiml/whisper.xml.jsp?wmsg="+URLEncoder.encode(whisperMsg,"UTF-8")+"&cn="+agentWhisperConfName);
									} catch (UnsupportedEncodingException e) {
										logger.error("run() : Exception occurred while requesting whisper.xml.jsp", e);
									}
									agentsCallParams.put("Method", "GET");
									try {
										logger.debug("run() : Calling whisper.xml for conf name: " + agentWhisperConfName);
										agentsCall.update(agentsCallParams);
									} catch (Exception e) {
										logger.error("run() : Exception occurred", e);
									}
								}
								
								//In power mode, change the prospect call status as INPROGRESS
								if (finalProspectCall != null) {
									// 25-10-2018 => removed CALL_INPROGRESS, CALL_COMPLETE entry on the request of Manish
									// prospectCallLogService.updateCallDetails(finalProspectCall.getProspectCallId(), ProspectCallStatus.CALL_INPROGRESS, callSid, null, finalAllocatedAgent.getAgentId(), null);
								}
							} else { // end if DialerMode == POWER
								if (finalProspectCall != null) { //if PREVIEW
									prospectCallLogService.updateCallDetails(finalProspectCall.getProspectCallId(), ProspectCallStatus.CALL_PREVIEW, null, finalProspectCall.getCallRetryCount(), finalAllocatedAgent.getAgentId(), finalProspectCall.getDailyCallRetryCount());
								}
							} //end if DialerMode == PREVIEW
						} //end if (finalAllocatedAgent != null)
						logger.debug("run() : PlayWhisperThread stopped");
					} //end run()
				};
				thread.start();
			} //end if uri.contains("dial.xml.jsp")
			chain.doFilter(request, response); // Pass request back down the filter chain
		} //end answeredBy check
	*/}
	
	private void pushMessageToAgent(String agentId, String event, String message) {
		logger.debug("pushMessageToAgent() : Event = [{}], Message = [{}], Agent = " + agentId , event, message);
		try {
			String responseCode = Pusher.triggerPush(agentId, event, XtaasUtils.removeNonAsciiChars(message));
			logger.debug("pushMessageToAgent() : Response Code [{}]", responseCode);
		} catch (IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing message to agent: " + agentId + " Msg:" + message, e);
		}
	}

	/**
	 * @param agentRegistrationService the agentRegistrationService to set
	 */
	public void setAgentRegistrationService(AgentRegistrationService agentRegistrationService) {
		this.agentRegistrationService = agentRegistrationService;
	}

	/**
	 * @param prospectCallLogService the prospectCallLogService to set
	 */
	public void setProspectCallLogService(ProspectCallLogService prospectCallLogService) {
		this.prospectCallLogService = prospectCallLogService;
	}
	
	private String makePreviewCall(String phoneNumber, String callerId, String pcid, String agentId) {
		logger.debug("makePreviewCall() : Making Call to PhoneNumber: " + phoneNumber);
		Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
    	Account account = channelTwilio.getChannelConnection();
    	CallFactory callFactory = account.getCallFactory(); //callfactory to make outbound call.
    	
    	Map<String, String> callParams = new HashMap<String, String>();
        callParams.put("To", phoneNumber); // lead's phone number
        callParams.put("From", callerId); // Twilio number
        //callParams.put("IfMachine", "Continue"); // to detect if phone is picked by human or machine
        
        //Commented out - StatusCallback not reliable
        //String statusCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/dialer/statuscallback?pcid="+pcid+"&agentId="+agentId;
        /*callParams.put("StatusCallback", statusCallbackUrl);
        callParams.put("StatusCallbackMethod", "POST");
        callParams.put("StatusCallbackEvent", "initiated");
        callParams.put("StatusCallbackEvent", "ringing");
        //callParams.put("StatusCallbackEvent", "answered");
        callParams.put("StatusCallbackEvent", "completed");*/
        
        try {
        	StringBuilder dialXMLUrl = new StringBuilder(ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/dialer/callanswered");
            dialXMLUrl.append("?agentId=").append(URLEncoder.encode(agentId, "UTF-8"));
            dialXMLUrl.append("&pcid=").append(pcid);
            
			callParams.put("Url", dialXMLUrl.toString());
		} catch (UnsupportedEncodingException e) {
			logger.error("makePreviewCall() : Exception while encoding url parameters.", e);
		}
		
        // Make the call
        Call call;
		try {
			call = callFactory.create(callParams);
			return call.getSid();
		} catch (Exception e) {
			logger.error("Error in calling " + phoneNumber, e);
		} 
        return null;
	}

	/**
	 * @param activePreviewCallQueueRepository the activePreviewCallQueueRepository to set
	 */
	public void setActivePreviewCallQueueRepository(
			ActivePreviewCallQueueRepository activePreviewCallQueueRepository) {
		this.activePreviewCallQueueRepository = activePreviewCallQueueRepository;
	}
	
}
