/**
 * 
 */
package com.xtaas.web.filter;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.xtaas.utils.EncryptionUtils;
import com.xtaas.infra.security.AppManager;
import com.xtaas.infra.security.AppManager.App;

/**
 * A spring custom filter to do Token based authentication. Mainly used for REST API access.
 * 
 * Token is in format appname:data:hash where, 
 * 		appname is the name of the application which is generating the token
 * 		data is two-way encrypted data using the app's secret key. After decryption data will be in format username|org|role|time in MS
 * 		hash is one-way encryption of data using the app's secret key
 * 
 * @author djain
 *
 */
public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	protected TokenAuthenticationFilter(String defaultFilterProcessesUrl) {
		super(defaultFilterProcessesUrl); //defaultFilterProcessesUrl - specified in applicationContext.xml.  
		super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(defaultFilterProcessesUrl)); //Authentication will only be initiated for the request url matching this pattern
        setAuthenticationManager(new NoOpAuthenticationManager());
        setAuthenticationSuccessHandler(new TokenSimpleUrlAuthenticationSuccessHandler());
	}

	/**
     * Attempt to authenticate request - basically just pass over to another method to authenticate request headers 
     */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException,
			IOException, ServletException {
		String token = request.getHeader("Authorization");
        //logger.info("token found:"+token);
        
        AbstractAuthenticationToken userAuthenticationToken = authUserByToken(token);
        if(userAuthenticationToken == null) throw new AuthenticationServiceException(MessageFormat.format("Error | {0}", "Bad Token"));
        return userAuthenticationToken;
	}
	
	/**
     * authenticate the user based on token
     * @return
     */
    private AbstractAuthenticationToken authUserByToken(String token) {
        if(token==null) return null;

        /* Token is in format appname:data:hash where,
         * 		appname is the name of the application which is generating the token
         * 		data is two-way encrypted data using the app's secret key. After decryption data will be in format username|org|role|time in MS 
         * 		hash is one-way encryption of data using the app's secret key
         */
        
        //get the app name, based on which get the secret key
        String[] splittedTokens = StringUtils.split(token, ":");
        String appName = splittedTokens[0];
        String data = splittedTokens[1];
        String hash = splittedTokens[2];
        
        //get the app's secret key
        String secretKey = AppManager.getSecurityKey(App.valueOf(appName));
        
        //decrypt the data using the secret key
        String decryptedData = EncryptionUtils.decrypt(data, secretKey);
        
        //check if data has not been compromised in transit
        String generatedHash = EncryptionUtils.generateHash(decryptedData, secretKey);
        if (!hash.equals(generatedHash)) return null;
        
        String[] splittedData = StringUtils.split(decryptedData, "|");
        
        String username = splittedData[0];
        //String org = splittedData[1];
        String role = splittedData[2];
        Long tokenGenTimeInMillis = Long.valueOf(splittedData[3]);
        
        //token must not be older than 5 mins (5 * 60 * 1000) Millis
        if ((System.currentTimeMillis() - tokenGenTimeInMillis) > (5*60*1000))  return null;
        
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(role));
        
        User principal = new User(username, "", authorities); 
        AbstractAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(principal, "", principal.getAuthorities());
        
        return authToken;
    }
 
 
    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
        super.doFilter(req, res, chain);
    }
}
