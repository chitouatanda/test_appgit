package com.xtaas.ml.transcribe.dto;

import java.net.URI;

/**
 * <p>
 */
public class AwsS3PersistenceResult implements TranscriptPersistenceResult {
    final private URI transcriptUri;
    /**
     * Default constructor
     */
    public AwsS3PersistenceResult(URI uri) {
        this.transcriptUri = uri;
    }

    public URI getTranscriptUri() {
        return transcriptUri;
    }

    @Override
    public boolean hasTranscriptUri() {
        return true;
    }

    @Override
    public String getTranscript() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return "AwsS3PersistenceResult [transcriptUri=" + transcriptUri + "]";
    }
}
