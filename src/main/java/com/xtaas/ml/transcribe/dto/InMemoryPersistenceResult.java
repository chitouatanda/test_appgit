package com.xtaas.ml.transcribe.dto;

import java.net.URI;

/**
 * <p>
 */
public class InMemoryPersistenceResult implements TranscriptPersistenceResult {
    final private String transcript;
	/**
     * Default constructor
	 */
    public InMemoryPersistenceResult(String transcriptIn) {
        this.transcript = transcriptIn;
    }

    @Override
    public boolean hasTranscriptUri() {
        return false;
    }

    @Override
    public URI getTranscriptUri() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getTranscript() {
        return transcript;
    }

    @Override
    public String toString() {
        return "InMemoryPersistenceResult [transcript=" + transcript + "]";
    }
}
