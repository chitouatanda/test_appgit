package com.xtaas.ml.transcribe.dto;

import java.io.InputStream;

public class JobResult {
    // Required attributes
    private final JobTracker jobTracker;
    private final boolean success;
    private InputStream transcriptDataInputStream;

    // optional
    private String trUriStr;

    public JobResult(final JobTracker jobTracker, final boolean success) {
        this.jobTracker = jobTracker;
        this.success = success;
    }

    public JobTracker getJobTracker() {
        return jobTracker;
    }

    public boolean isSuccess() {
        return success;
    }

    public JobResult withUri(final String uri) {
        this.trUriStr = uri;
        return this;
    }

    public String getTrancriptUri() {
        return trUriStr;
    }

    public JobResult withInputStream(final InputStream transcriptDataInputStream) {
        this.transcriptDataInputStream = transcriptDataInputStream;
        return this;
    }

    public InputStream getTrancriptDataInputStream() {
        return transcriptDataInputStream;
    }
}
