package com.xtaas.ml.transcribe.dto;

import java.util.UUID;

/**
 * <p>
 * Transcribe job tracking object
 *
 * @author sushant
 * @version 1.0
 */
public class JobTracker {
    private S3AudioMediaLink mediaLink;
	private String region;
	private String jobId;
	private UUID jobTrailId;
	private String transcriptionServiceName;

	public JobTracker(S3AudioMediaLink mediaLink) {
		this.mediaLink = mediaLink;
	}

	public JobTracker withJobId(final String jobId) {
		this.jobId = jobId;
		return this;
	}

	public JobTracker withRegion(final String region) {
		this.region = region;
		return this;
	}

	public JobTracker withTranscriptionService(String transcriptionServiceName) {
		this.transcriptionServiceName = transcriptionServiceName;
		return this;
	}

	public String getRegion() {
		return region;
	}

	public String getJobId() {
		return jobId;
	}

	public S3AudioMediaLink getMediaLink() {
		return mediaLink;
	}

	public JobTracker withJobTrailId(UUID jobTrailId) {
		this.jobTrailId = jobTrailId;
		return this;
	}	

	public UUID getJobTrailId() {
		return jobTrailId;
	}

	public String getTranscriptionServiceName() {
		return transcriptionServiceName;
	}

	@Override
	public String toString() {
		return "JobTracker [jobId=" + jobId + ", jobTrailId=" + jobTrailId + ", mediaLink=" + mediaLink
				+ ", transcriptionServiceName=" + transcriptionServiceName + "]";
	}

	
}
