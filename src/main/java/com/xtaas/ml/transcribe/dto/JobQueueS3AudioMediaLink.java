package com.xtaas.ml.transcribe.dto;

import java.net.URI;

public class JobQueueS3AudioMediaLink extends S3AudioMediaLink {
	// Should I just make it a wrapper around CallClassificationJobQueue?
	// No. CallClassificatonJobQueue has mutable state, whereas
	// the mediaLink object is immutable conprising of its invariants
	final private String messageId;
	final private String prospectCallLogId;
	final private String prospectCallId;
	final private String phone;
	final private int callDuration;

	public JobQueueS3AudioMediaLink(URI mediaUri, String messageId, String prospectCallLogId, String phone,
			int callDuration, String prospectCallId) {
		super(mediaUri);
		this.messageId = messageId;
		this.prospectCallLogId = prospectCallLogId;
		this.phone = phone;
		this.callDuration = callDuration;
		this.prospectCallId = prospectCallId;
	}

	public String getMessageId() {
		return messageId;

	}

	public String getProspectCallLogId() {
		return prospectCallLogId;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public String getPhone() {
		return phone;
	}

	public int getCallDuration() {
		return callDuration;
	}

	static public Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private String mediaUri;
		private String prospectCallLogId;
		private String phone;
		private int callDuration;
		private String messageId;
		private String prospectCallId;

		private Builder() {
		}

		public Builder withMediaUri(String mediaUri) {
			this.mediaUri = mediaUri;
			return this;
		}

		public Builder withProspectCallLogId(String prospectCallId) {
			this.prospectCallLogId = prospectCallId;
			return this;
		}

		public Builder withProspectCallId(String prospectCallId) {
			this.prospectCallId = prospectCallId;
			return this;
		}

		public Builder withPhone(String phone) {
			this.phone = phone;
			return this;
		}

		public Builder withCallDuration(int callDuration) {
			this.callDuration = callDuration;
			return this;
		}

		public Builder withMessageId(String messageId) {
			this.messageId = messageId;
			return this;
		}

		public JobQueueS3AudioMediaLink build() {
			mediaUri = mediaUri.replaceAll("\\s", ""); // Remove all white spaces.
			return new JobQueueS3AudioMediaLink(URI.create(mediaUri), messageId, prospectCallLogId, phone, callDuration,
					prospectCallId);
		}
	}
}