package com.xtaas.ml.transcribe.dto;

import java.net.URI;

/**
 * <p>
 * Marker interface to be used for chaining BiConsumer<JobResult,
 * TranscriptPersistenceResult> postProcessConsumer
 */
public interface TranscriptPersistenceResult {
    boolean hasTranscriptUri();

    URI getTranscriptUri();

    String getTranscript();
}
