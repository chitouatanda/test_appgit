package com.xtaas.ml.transcribe.dto;

public class AppTekSubmitResponse {
    private boolean success;
    private String requestId;
    private String error;

    public boolean getSuccess() {
        return this.success;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public String getError() {
        return this.error;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AppTekSubmitResponse{");
        sb.append("success=").append(this.success);
        sb.append(", ").append("requestId=\'").append(this.requestId).append("\'");
        sb.append(", ").append("error=\'").append(this.error).append("\'");
        sb.append('}');
        return sb.toString();
    }
}
