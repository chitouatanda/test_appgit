package com.xtaas.ml.transcribe.dto;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.services.s3.AmazonS3URI;

/**
 * <p> Generalize to an AudioMediaLink super class when more input types
 * are encountered
 *
 * @author sushant
 * @version 1.0
 */
public class S3AudioMediaLink {

    // recordings/fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav
    private static final Pattern S3_OBJ_KEY_PATTERN = Pattern.compile("^(.*/)(.*)\\.(.*)$");
    
    final private URI mediaUri;

    public S3AudioMediaLink(URI uri) {
        this.mediaUri = uri;
    }
    
    public URI getMediaUri() {
        return mediaUri;
    }

    public String getMediaObjectName() {
        final AmazonS3URI s3MediaUri = new AmazonS3URI(mediaUri);
        Matcher matcher = S3_OBJ_KEY_PATTERN.matcher(s3MediaUri.getKey());
        if (!matcher.find()) {
            throw new IllegalArgumentException(
                    "Invalid S3 Object Key: " + s3MediaUri.getKey() +
                    ", s3 uri: " + mediaUri);
        }

        String objName = matcher.group(2);
        // group 1 is namespace or prefix
        // group 3 is file type
        return objName;
    }

    @Override
    public String toString() {
        return "S3AudioMediaLink [mediaUri=" + mediaUri + "]";
    }
}
