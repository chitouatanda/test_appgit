package com.xtaas.ml.transcribe.service;

import java.net.URI;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.xtaas.ml.transcribe.dto.AwsS3PersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AwsS3PersistenceService {
    final static private Logger logger = LoggerFactory.getLogger(AwsS3PersistenceService.class);
    private String transcriptBucket;
    private String transcriptNamespace;
    private int trServNameLimit;
    private AmazonS3 s3client;
    
    
    public AwsS3PersistenceService(
            @Value("${aws.access.key.id}") String awsAccessKeyId, 
            @Value("${aws.secret.access.key}") String awsSecretKey, 
            @Value("${ml.transcript.bucket_name}") String transcriptBucket,
            @Value("${ml.transcript.namespace}") String transcriptNamespace, 
            @Value("${ml.transcript.region}") String transcriptRegion, 
            @Value("${transcribe.service.name.limit:6}") int trServNameLimit) {
        this.transcriptBucket = transcriptBucket;
        this.transcriptNamespace = transcriptNamespace;
        this.trServNameLimit = trServNameLimit;
        this.s3client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(
                        new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials(awsAccessKeyId, awsSecretKey)))
                    .withRegion(transcriptRegion)
                    .build();
    }

    /*
        The bucket and region where the transcript is saved is static based on env variable.
        Can be extended to be picked up dynamically.

        Currently the transcript has public access
        can create presigned url for transcript with access life of 7 days
        s3client.copyObject(new CopyObjectRequest(transcriptUri.getBucket(), 
                                transcriptUri.getKey(), transcriptBucket, transcriptNamespace + recordingObjName)
                                .withCannedAccessControlList(CannedAccessControlList.PublicRead));
        GeneratePresignedUrlRequest url = new GeneratePresignedUrlRequest(transcriptBucket, 
                        transcriptNamespace + recordingObjName)
                        .withMethod(HttpMethod.GET)
                        .withExpiration(expiration)
        return s3client.generatePresignedUrl(url).toString();
    */
    public AwsS3PersistenceResult saveTranscript(JobResult jobResult) {
        String recordingObjName = jobResult.getJobTracker().getMediaLink().getMediaObjectName();
        String objKey = createObjectKey(
                            transcriptNamespace, 
                            recordingObjName, 
                            jobResult.getJobTracker().getTranscriptionServiceName());

        ObjectMetadata metadata = new ObjectMetadata();
        // if (jobResult.getJobTracker().getLabels() != null) {
        //     metadata.addUserMetadata("labels", 
        //                 jobResult.getJobTracker().getLabels()
        //                             .stream()
        //                             .collect(Collectors.joining(",")));
        // }

        s3client.putObject(new PutObjectRequest(
                    transcriptBucket, 
                    objKey,
                    jobResult.getTrancriptDataInputStream(), 
                    metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));
        URI s3Uri = getS3Uri(transcriptBucket, objKey);
        logger.debug("Saved transcript for recording {} at {}", objKey, s3Uri);
        return new AwsS3PersistenceResult(s3Uri);
    }

    private URI getS3Uri(String transcriptBucket, String objKey) {
        return URI.create(String.format(
                    "https://%s.s3.amazonaws.com/%s", 
                    transcriptBucket, 
                    objKey));
    }
    
    private String createObjectKey(String namespace, String objName, 
            String transcriptionServiceName) {
        return String.format("%s%s.%s.%s.json", 
                namespace, 
                objName, 
                transcriptionServiceName.substring(0, 
                    Math.min(transcriptionServiceName.length(), trServNameLimit)),
                ZonedDateTime.now(ZoneOffset.UTC)
                    .format(DateTimeFormatter.ISO_INSTANT)
                    .replaceAll("([.\\-:])", ""));
    }
}
