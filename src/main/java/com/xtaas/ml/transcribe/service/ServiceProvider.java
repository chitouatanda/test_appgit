package com.xtaas.ml.transcribe.service;

/**
 * <p>Identifies the class as a transcription service provider
 */
public interface ServiceProvider {
    
    /**
     * <p>
     * 
     * @return String
    */
    public String getServiceProvider();
}
