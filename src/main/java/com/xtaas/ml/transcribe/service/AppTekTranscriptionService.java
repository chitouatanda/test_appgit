package com.xtaas.ml.transcribe.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Optional;

import com.amazonaws.util.StringInputStream;
import com.xtaas.ml.transcribe.dto.AppTekSubmitResponse;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.JobTracker;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/**
 * <p>
 */
@Service
public class AppTekTranscriptionService implements TranscriptionService, TrafficManager {
    final static private Logger logger = LoggerFactory.getLogger(AppTekTranscriptionService.class);
    final static private String TRANSCRIPTION_SERVICE_NAME = "APPTEK";
    final static private ExchangeFilterFunction logRequestFilter = (request, next) -> {
        logger.debug("{} {}, \nAttributes: {}, \nHeaders: {}", 
                request.method().toString().toUpperCase(), request.url(), 
                request.attributes(), request.headers());
        return next.exchange(request);
    };

    final private WebClient client;
    final private TranscriberSemaphore transcriberSemaphore;

    /**
     * Default constructor
     */
    public AppTekTranscriptionService(
            @Value("${apptek.base.uri}") final String endpointBaseUri,
            @Value("${apptek.access.token}") final String accessToken,
            @Value("${apptek.rate.concurrent.max:10}") int maxConcurrent) {
        this.client = WebClient.builder()
                            .baseUrl(endpointBaseUri)
                            .defaultHeader("x-token", accessToken)
                            .filter(logRequestFilter)
                            .build();
        this.transcriberSemaphore = new TranscriberSemaphore(maxConcurrent, 
            AppTekTranscriptionService.TRANSCRIPTION_SERVICE_NAME);
    }

    public JobTracker submitTranscribeJob(final S3AudioMediaLink mediaLink) {
        Mono<AppTekSubmitResponse> respMono;
        try {
            respMono = client.post()
                    .uri("/en.tel?punc=1&diar=1")
                    .accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.CONTENT_TYPE, "audio/wav")
                    .body(BodyInserters.fromResource(
                        // new InputStreamResource(getInputStream(mediaLink.getMediaUri()))
                        new UrlResource(mediaLink.getMediaUri())
                     ))
                    .retrieve()
                    .bodyToMono(AppTekSubmitResponse.class);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        AppTekSubmitResponse resp = respMono.block();
        return new JobTracker(mediaLink).withJobId(resp.getRequestId());
    }

    private static class PendingException extends RuntimeException{
        private static final long serialVersionUID = 1L;
    }

    // alternate flow instead of PendingException?
    @Override
    public Optional<JobResult> checkStatus(final JobTracker jobTracker) {
        try {
            Mono<String> transcriptMono = client.get()
                    .uri("/" + jobTracker.getJobId() + "?format=json")  
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .onRawStatus(i -> i == 204, r -> Mono.error(new PendingException()))
                    .bodyToMono(String.class);
            String transcript = transcriptMono.block();
            JobResult jobResult = new JobResult(jobTracker, true).withInputStream(new StringInputStream(transcript));
            return Optional.of(jobResult);
        } catch (PendingException e) {
            System.out.println("pending: " + jobTracker.getJobId());
            return Optional.empty();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    // TODO add implementation
    @Override
    public String getTranscriptText(InputStream in) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getServiceProvider() {
        return TRANSCRIPTION_SERVICE_NAME;
    }

    @Override
    public void acquire() {
        transcriberSemaphore.acquire();
    }

    @Override
    public void release() {
        transcriberSemaphore.release(); 
    }

    @Override
    public void acquire(String method) {

    }
}
