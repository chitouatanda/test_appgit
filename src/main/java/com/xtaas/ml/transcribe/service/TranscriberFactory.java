package com.xtaas.ml.transcribe.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Creates a Transcriber instance based on TranscriberContext. It will provide a
 * default method
 *
 * @author sushant
 * @version 1.0
 */
@Component
public class TranscriberFactory {
    final private Map<String, TranscriptionService> transcriptionServiceCache;
    final private Map<String, TrafficManager> trafficManagerCache;
    final private AwsS3PersistenceService persistenceService;
    final private int pollWaitTime;
    final private int statusCheckPollMax;
    final private ExecutorService transcriberExecutor;
    final private ExecutorService transcriberCheckStatusExecutor;
    final private ExecutorService transcriberPersistenceExecutor;
    final private ExecutorService transcriberPostProcessExecutor;
    final private ScheduledExecutorService transcriberStatusPollExecutor;
    final private ExecutorService transcriberExceptionHandlingExecutor;

    /**
     * Default constructor
     */
    public TranscriberFactory(@Autowired List<TranscriptionService> transcriptionServices,
            @Autowired List<TrafficManager> trafficManagers,
            @Autowired AwsS3PersistenceService persistenceService,
            @Autowired @Qualifier("transcriberExecutor") ExecutorService transcriberExecutor,
            @Autowired @Qualifier("transcriberCheckStatusExecutor") ExecutorService transcriberCheckStatusExecutor,
            @Autowired @Qualifier("transcriberPersistenceExecutor") ExecutorService transcriberPersistenceExecutor,
            @Autowired @Qualifier("transcriberPostProcessExecutor") ExecutorService transcriberPostProcessExecutor,
            @Autowired @Qualifier("transcriberStatusPollExecutor") ScheduledExecutorService transcriberStatusPollExecutor,
            @Autowired @Qualifier("transcriberExceptionHandlingExecutor") ExecutorService transcriberExceptionHandlingExecutor,
            @Value("${transcribe.status_check.wait_time_in_ms:100}") int pollWaitTime,
            @Value("${transcribe.status_check.poll_max:10}") int statusCheckPollMax) { // failsafe to avoid busy polling

        this.transcriptionServiceCache = transcriptionServices.stream().collect(
            Collectors.toMap(TranscriptionService::getServiceProvider, service -> service));
        this.trafficManagerCache = trafficManagers.stream().collect(
            Collectors.toMap(TrafficManager::getServiceProvider, mgr -> mgr));
        this.persistenceService = persistenceService;
        this.pollWaitTime = pollWaitTime;
        this.statusCheckPollMax = statusCheckPollMax;
        this.transcriberExecutor = transcriberExecutor;
        this.transcriberCheckStatusExecutor = transcriberCheckStatusExecutor;
        this.transcriberPersistenceExecutor = transcriberPersistenceExecutor;
        this.transcriberPostProcessExecutor = transcriberPostProcessExecutor;
        this.transcriberStatusPollExecutor = transcriberStatusPollExecutor;
        this.transcriberExceptionHandlingExecutor = transcriberExceptionHandlingExecutor;
    }

    public Transcriber getTrancriber(Supplier<TranscriberConfig> configSupplier) {
        TranscriberConfig config = configSupplier.get();
        return new Transcriber(
            transcriptionServiceCache.get(config.getTranscriptionServiceProvider()),
            config.getTrafficManager()
                    .orElseGet(() -> 
                        trafficManagerCache.get(config.getTranscriptionServiceProvider())),
            config.getPersistenceService(), 
            config.getPostProcessConsumer(), 
            config.getExceptionHandler(),
            config.getMediaLinkValidator(),
            this.pollWaitTime,
            this.statusCheckPollMax,
            transcriberExecutor, 
            transcriberCheckStatusExecutor,
            transcriberPersistenceExecutor,
            transcriberPostProcessExecutor,
            transcriberStatusPollExecutor,
            transcriberExceptionHandlingExecutor);
    }

    /**
     * defaults to using AWS transcriber
     */
    public Transcriber getTranscriber(BiConsumer<JobResult, TranscriptPersistenceResult> callback) {
        return getTrancriber(() -> TranscriberConfig.builder()
                    .withPersistenceService((j) -> persistenceService.saveTranscript(j))
                    .withTranscriptPostProcessConsumer(callback)
                );
    }

    public Transcriber getTranscriber(String transcriptionServiceProvider, 
                BiConsumer<JobResult, TranscriptPersistenceResult> callback) {
        return getTrancriber(() -> TranscriberConfig.builder()
                    .withPersistenceService((j) -> persistenceService.saveTranscript(j))
                    .withTranscriptionServiceProvider(transcriptionServiceProvider)
                    .withTranscriptPostProcessConsumer(callback)
                );
    }
}
