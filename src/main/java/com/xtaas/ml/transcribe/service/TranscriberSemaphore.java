package com.xtaas.ml.transcribe.service;

import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Control max number of concurrent jobs
 *
 * @author sushant
 * @version 1.0
 */
public class TranscriberSemaphore {
    final static private Logger logger = LoggerFactory.getLogger(TranscriberSemaphore.class);
    final private Semaphore semaphore;
    final private String id;

    /**
     * Default constructor
     */
    public TranscriberSemaphore(int permitSize, String id) {
        this.semaphore = new Semaphore(permitSize);
        this.id = id;
    }

    // blocking
    public void acquire() {
        logger.debug("TranscribeSemaphore {} acquire, available: {}", id, semaphore.availablePermits());
        try {
            semaphore.acquire();
            // return;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public void release() {
        semaphore.release();
        logger.debug("TranscribeSemaphore {} release, available: {}", id, semaphore.availablePermits());
    }
}
