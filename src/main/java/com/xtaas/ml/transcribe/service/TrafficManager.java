package com.xtaas.ml.transcribe.service;

public interface TrafficManager extends ServiceProvider {
    void acquire();

    void release();

    void acquire(String method);
}
