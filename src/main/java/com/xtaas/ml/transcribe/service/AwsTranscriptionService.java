package com.xtaas.ml.transcribe.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.transcribe.AmazonTranscribe;
import com.amazonaws.services.transcribe.AmazonTranscribeClient;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobResult;
import com.amazonaws.services.transcribe.model.LanguageCode;
import com.amazonaws.services.transcribe.model.Media;
import com.amazonaws.services.transcribe.model.Settings;
import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.TranscriptionJob;
import com.amazonaws.services.transcribe.model.TranscriptionJobStatus;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.JobTracker;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
@Service
public class AwsTranscriptionService implements TranscriptionService, TrafficManager {
	final private static String TRANSCRIPTION_SERVICE_NAME = "AWS";
	final private String awsAccessKeyId;
	final private String awsSecretKey;
	final private TranscriberSemaphore transcribeSemaphore;

	/**
	 * Default constructor
	 */
	public AwsTranscriptionService(@Value("${aws.access.key.id}") String awsAccessKeyId,
								   @Value("${aws.secret.access.key}") String awsSecretKey,
								   @Value("${aws.transcribe.region:us-east-1}") String awsTranscribeRegion,
								   @Value("${aws.rate.concurrent.max:10}") int maxConcurrent) {
		this.awsAccessKeyId = awsAccessKeyId;
		this.awsSecretKey = awsSecretKey;
		this.transcribeSemaphore = new TranscriberSemaphore(maxConcurrent,
				AwsTranscriptionService.TRANSCRIPTION_SERVICE_NAME);
	}

	@Override
	public JobTracker submitTranscribeJob(S3AudioMediaLink mediaLink) {
		final AmazonS3URI s3UriWestRegion = new AmazonS3URI(mediaLink.getMediaUri());
		String eastRecordingUrl = mediaLink.getMediaUri().toString();
		if (s3UriWestRegion != null && s3UriWestRegion.getRegion() != null
				&& s3UriWestRegion.getRegion().equalsIgnoreCase("us-west-2")) {
			try {
				eastRecordingUrl = moveRecordingUrl(createAWSConnection("us-west-2"), createAWSConnection("us-east-1"),
						"xtaasrecordings", s3UriWestRegion.getKey(), "xtaaseastbucket", s3UriWestRegion.getKey());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		AmazonTranscribe trClient = getTranscribeClient("us-east-1");
		Instant now = Instant.now();
		String jobId = s3UriWestRegion.getKey().split("/")[1] + "." + now.toEpochMilli();
		TranscriptionJob job = trClient.startTranscriptionJob(
						new StartTranscriptionJobRequest().withMedia(new Media().withMediaFileUri(eastRecordingUrl))
								.withMediaFormat("wav").withLanguageCode(LanguageCode.EnUS).withTranscriptionJobName(jobId)
								.withSettings(new Settings().withChannelIdentification(true)))
				.getTranscriptionJob();
		String status = job.getTranscriptionJobStatus();
		if (TranscriptionJobStatus.FAILED == TranscriptionJobStatus.fromValue(status)) {
			throw new RuntimeException(job.getFailureReason());
		}
		return new JobTracker(mediaLink).withJobId(jobId).withRegion("us-east-1");
	}

	public static String moveRecordingUrl(AmazonS3 sourceClient, AmazonS3 destClient, String sourceBucket,
										  String sourceKey, String destBucket, String destKey) throws IOException {

		S3ObjectInputStream inStream = null;
		String awsRecordingUrl = null;
		try {
			GetObjectRequest request = new GetObjectRequest(sourceBucket, sourceKey);
			S3Object object = sourceClient.getObject(request);
			inStream = object.getObjectContent();
			destClient.putObject(new PutObjectRequest(destBucket, destKey, inStream, object.getObjectMetadata())
					.withCannedAcl(CannedAccessControlList.PublicRead));
			awsRecordingUrl = "https://s3.us-east-1.amazonaws.com/xtaaseastbucket/" + destKey;
		} catch (SdkClientException e) {
			e.printStackTrace();
		} finally {
			if (inStream != null) {
				inStream.close();
			}
		}
		return awsRecordingUrl;
	}

	/*
	 * This method is used for creating the connection with AWS S3 Storage Params -
	 * AWS Access key, AWS secret key
	 */
	private static AmazonS3 createAWSConnection(String clientRegion) {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_ACCESS_KEY_ID,
					XtaasConstants.AWS_SECRET_KEY);
			s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return s3client;
	}

	@Override
	public Optional<JobResult> checkStatus(final JobTracker jobTracker) {
		String region = jobTracker.getRegion();
		AmazonTranscribe trClient = getTranscribeClient("us-east-1");
		GetTranscriptionJobResult transcriptionJob = trClient
				.getTranscriptionJob(new GetTranscriptionJobRequest().withTranscriptionJobName(jobTracker.getJobId()));
		TranscriptionJob job = transcriptionJob.getTranscriptionJob();
		TranscriptionJobStatus status = TranscriptionJobStatus.fromValue(job.getTranscriptionJobStatus());
		switch (status) {
			case FAILED:
				throw new RuntimeException(job.getFailureReason());
			case IN_PROGRESS:
				return Optional.empty();
			case COMPLETED:
				try {
					return Optional.of(new JobResult(jobTracker, true).withUri(job.getTranscript().getTranscriptFileUri())
							.withInputStream(new URL(job.getTranscript().getTranscriptFileUri()).openStream()));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			default: // unreachable
				throw new RuntimeException("Unrecognized status: " + status);
		}
	}

	@Override
	public String getTranscriptText(InputStream in) {
		try {
			String transcriptPath = "$['results']['transcripts'][0]['transcript']";
			String trasncriptJson = IOUtils.toString(in, "UTF-8");
			DocumentContext jsonContext = JsonPath.parse(trasncriptJson);
			String transcriptText = jsonContext.read(transcriptPath);
			// System.out.println(transcriptText);
			return transcriptText;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getServiceProvider() {
		return TRANSCRIPTION_SERVICE_NAME;
	}

	@Override
	public void acquire() {
		transcribeSemaphore.acquire();
	}

	@Override
	public void release() {
		transcribeSemaphore.release();
	}

	@Override
	public void acquire(String method) {

	}

	private AmazonTranscribe getTranscribeClient(String region) {
		if (region.equals("us-east-1")) {
			return AmazonTranscribeClient.builder().withRegion(region)
					.withCredentials(
							new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKeyId, awsSecretKey)))
					.build();
		} else {
			throw new IllegalArgumentException("AWS region is not valid for transcription. Required : us-east-1");
		}
	}
}