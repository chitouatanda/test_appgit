package com.xtaas.ml.transcribe.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.xtaas.ml.transcribe.dto.InMemoryPersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class InMemoryPersistenceService {
    final static private Logger logger = LoggerFactory.getLogger(InMemoryPersistenceService.class);
    final private Map<String, TranscriptionService> transcriptionServiceCache;

    public InMemoryPersistenceService(
            @Autowired List<TranscriptionService> transcriptionServices,
            @Value("${aws.access.key.id}") String awsAccessKeyId,
            @Value("${aws.secret.access.key}") String awsSecretKey,
            @Value("${ml.transcript.bucket_name}") String transcriptBucket,
            @Value("${ml.transcript.namespace}") String transcriptNamespace,
            @Value("${ml.transcript.region}") String transcriptRegion,
            @Value("${transcribe.service.name.limit:6}") int trServNameLimit) {
        this.transcriptionServiceCache = transcriptionServices.stream().collect(
                    Collectors.toMap(TranscriptionService::getServiceProvider, service -> service));
    }

    public InMemoryPersistenceResult saveTranscript(JobResult jobResult) {
        TranscriptionService transcriptionService = transcriptionServiceCache.get(jobResult.getJobTracker().getTranscriptionServiceName());
        String transcriptText = transcriptionService.getTranscriptText(jobResult.getTrancriptDataInputStream());
        logger.debug("Transcription result: recording id: {}, transcription service: {}, transcription text: {}",
            jobResult.getJobTracker().getMediaLink().getMediaObjectName(),
            jobResult.getJobTracker().getTranscriptionServiceName(),
            transcriptText);
        return new InMemoryPersistenceResult(transcriptText);
    }
}
