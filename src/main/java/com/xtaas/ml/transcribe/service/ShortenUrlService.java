package com.xtaas.ml.transcribe.service;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;

@Service
public class ShortenUrlService {

    @Value("${bitly.access.token}")
    private String bitlyAccessToken;

    // why URI? because we dont want to send out shotening request in case of format
    // error
    // forgiving. in case of error return full uri
    public String shortenUrl(URI inputUri) {
        String shortUri = inputUri.toString();
        BitlyClient client = new BitlyClient(bitlyAccessToken);
        final Response<ShortenResponse> resp = client.shorten()
            .setLongUrl(inputUri.toString())
            .call();
        if (resp.status_code == 200) {     
            shortUri = resp.data.url;
        } else {
            System.out.println(resp.toString());
            // log error
        }
        return shortUri;
    }
}
