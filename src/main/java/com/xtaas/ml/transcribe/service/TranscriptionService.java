package com.xtaas.ml.transcribe.service;

import java.io.InputStream;
import java.util.Optional;

import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.JobTracker;

public interface TranscriptionService extends ServiceProvider {
    JobTracker submitTranscribeJob(S3AudioMediaLink mediaLink);

    Optional<JobResult> checkStatus(JobTracker jobTracker);

    String getTranscriptText(InputStream in);
}
