package com.xtaas.ml.transcribe.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.JobTracker;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 */
public class Transcriber {

    final static private Logger logger = LoggerFactory.getLogger(Transcriber.class);
    final private TranscriptionService transcriptionService;
	final private TrafficManager trafficManager;
    final private Function<JobResult, TranscriptPersistenceResult> persistenceService;
    final private BiConsumer<JobResult, TranscriptPersistenceResult> postProcessConsumer;
    final private BiConsumer<S3AudioMediaLink, Throwable> exceptionHandler;
    final private Predicate<S3AudioMediaLink> mediaLinkValidator;
    final int pollWaitTimeInMs;
    final int statusCheckPollMax;
    final private ExecutorService transcriberExecutor;
    final private ExecutorService transcriberCheckStatusExecutor;
    final private ExecutorService transcriberPersistenceExecutor;
    final private ExecutorService transcriberPostProcessExecutor;
    final private ScheduledExecutorService transcriberStatusPollExecutor;
    final private ExecutorService transcriberExceptionHandlingExecutor;

    public Transcriber(TranscriptionService transcriptionService,
            TrafficManager trafficManager, 
            Function<JobResult, TranscriptPersistenceResult> persistenceService,
            BiConsumer<JobResult, TranscriptPersistenceResult> postProcessConsumer,
            BiConsumer<S3AudioMediaLink, Throwable> exceptionHandler, 
            Predicate<S3AudioMediaLink> mediaLinkValidator, 
            int pollWaitTimeInMs,
            int statusCheckPollMax,
            ExecutorService transcriberExecutor,
            ExecutorService transcriberCheckStatusExecutor,
            ExecutorService transcriberPersistenceExecutor,
            ExecutorService transcriberPostProcessExecutor,
            ScheduledExecutorService transcriberStatusPollExecutor,
            ExecutorService transcriberExceptionHandlingExecutor
            ) {
        this.transcriptionService = transcriptionService;
        this.trafficManager = trafficManager;
        this.persistenceService = persistenceService;
        this.postProcessConsumer = postProcessConsumer;
        this.exceptionHandler = exceptionHandler;
        this.mediaLinkValidator = mediaLinkValidator;
        this.pollWaitTimeInMs = pollWaitTimeInMs;
        this.statusCheckPollMax = statusCheckPollMax;
        this.transcriberExecutor = transcriberExecutor;
        this.transcriberCheckStatusExecutor = transcriberCheckStatusExecutor;
        this.transcriberPersistenceExecutor = transcriberPersistenceExecutor;
        this.transcriberPostProcessExecutor = transcriberPostProcessExecutor;
        this.transcriberStatusPollExecutor = transcriberStatusPollExecutor;
        this.transcriberExceptionHandlingExecutor = transcriberExceptionHandlingExecutor;
    
    }

    // can delete this delegate method?
    public CompletableFuture<List<String>> transcribe(Supplier<List<? extends S3AudioMediaLink>> audioMediaSupplier) {
        final UUID jobTrailId = UUID.randomUUID();
        return transcribe(audioMediaSupplier, jobTrailId);
    }

    public CompletableFuture<List<String>> transcribe(Supplier<List<? extends S3AudioMediaLink>> audioMediaSupplier, UUID jobTrailId) {
        List<CompletableFuture<String>> statusFutures = audioMediaSupplier.get().stream()
            .filter(mediaLinkValidator)
            .map((mediaLink) -> processAudioMediaLink(jobTrailId, mediaLink))
            .collect(Collectors.toList());

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(
            statusFutures.toArray(new CompletableFuture[0]));
        // since join is called only after allFutures is complete implying all the
        // individual statusFuture is complete, this code will not block
        CompletableFuture<List<String>> allStatusFutures = allFutures.thenApplyAsync(v -> {
            return statusFutures.stream()
                    .map(statusFuture -> statusFuture.join())
                    .collect(Collectors.toList());
            });
        return allStatusFutures;
    }

    private CompletableFuture<String> processAudioMediaLink(UUID jobTrailId, S3AudioMediaLink mediaLink) {
        logger.debug("About to schedule transcription job for media:{} with trailId:{}", mediaLink, jobTrailId);
        CompletableFuture<String> statusFuture = 
            CompletableFuture.supplyAsync(() -> submitTrancriptionJob(jobTrailId, mediaLink), transcriberExecutor)
                .thenComposeAsync((jobTracker) -> pollForCompletion(jobTracker), transcriberCheckStatusExecutor)
                .thenComposeAsync((jobResult) -> persistTranscript(jobResult), transcriberPersistenceExecutor)
                .thenComposeAsync((compositeResult) -> postProcess(compositeResult), transcriberPostProcessExecutor)
                // .exceptionally((ex) -> {
                //     logger.error("Error transcribing {}", mediaLink, ex);
                //     trafficManager.release();
                //     exceptionHandler.accept(mediaLink, ex);
                //     // throw new RuntimeException(ex.getCause());
                //     return "FAILURE";
                // });
                .handleAsync((result, ex) -> {
                    if (ex != null) {
                            logger.error("Error transcribing {}", mediaLink, ex);
                            trafficManager.release();
                            exceptionHandler.accept(mediaLink, ex);
                            return "FAILURE";
                    } else {
                        return result;
                    }
                }, transcriberExceptionHandlingExecutor);
        logger.debug("Scheduled transcription job for media:{} with trailId:{}", mediaLink, jobTrailId);
        return statusFuture;
    }

    private JobTracker submitTrancriptionJob(final UUID jobTrailId, S3AudioMediaLink v) {
        trafficManager.acquire();
        JobTracker jobTracker = transcriptionService.submitTranscribeJob(v).withJobTrailId(jobTrailId)
                .withTranscriptionService(transcriptionService.getServiceProvider());
        logger.debug("Submitted transcription job {}", jobTracker);
        return jobTracker;
    }


    private CompletableFuture<CompositeResult> persistTranscript(JobResult jobResult) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                TranscriptPersistenceResult persistenceResult = persistenceService.apply(jobResult);
                logger.debug("Transcription persisted for {} with result {}", jobResult.getJobTracker(), persistenceResult);
                return new CompositeResult(jobResult, persistenceResult);
            } finally {
                InputStream is = jobResult.getTrancriptDataInputStream();
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            }});
    }

    private CompletableFuture<String> postProcess(CompositeResult compositeResult) {
        return CompletableFuture.supplyAsync(
            () -> {
                postProcessConsumer.accept(compositeResult.jobResult, compositeResult.persistenceResult);
                logger.debug("Transcription job complete for {}",
                                    compositeResult.jobResult.getJobTracker());    
                return "SUCCESS";
            });
    }

    private class CompositeResult {
        private JobResult jobResult;
        private TranscriptPersistenceResult persistenceResult;
        CompositeResult(JobResult jobResult, TranscriptPersistenceResult persistenceResult) {
            this.jobResult = jobResult;
            this.persistenceResult = persistenceResult;
        }
    }

    private CompletableFuture<JobResult> pollForCompletion(JobTracker jobTracker) {
        final CompletableFuture<JobResult> completionFuture = new CompletableFuture<>();
        final ScheduledFuture<?> checkFuture = new FixedExecutionRunnable(() -> {
            try {
                Optional<JobResult> jobResultOptional = null;
                if ((jobResultOptional = transcriptionService.checkStatus(jobTracker)).isPresent()) {
                    trafficManager.release();
                    completionFuture.complete(jobResultOptional.get());
                } 
                logger.debug("Polled check status for transcription job {} with result {}", jobTracker,
                        jobResultOptional.isPresent() ? (jobResultOptional.get().isSuccess() ? "SUCCESS" : "FAILURE")
                                : "PENDING");
            } catch (Throwable t) {
                // trafficManager.release();
                completionFuture.completeExceptionally(t);
            }
        }, () -> {
            // trafficManager.release();
            completionFuture.completeExceptionally(new RuntimeException("Max number of times to poll exceeded for job: " + jobTracker));
        }, statusCheckPollMax).runNTimes(transcriberStatusPollExecutor, pollWaitTimeInMs, TimeUnit.MILLISECONDS);
        
        completionFuture.whenComplete((result, thrown) -> {
            if (!checkFuture.isCancelled()) {
                checkFuture.cancel(true);
            }
        });
        return completionFuture;
    }
    private class FixedExecutionRunnable implements Runnable {
        private final AtomicInteger runCount = new AtomicInteger();
        private final Runnable delegate;
        private final Runnable runtimesExceededHandler;
        private final int maxRunCount;

        private volatile ScheduledFuture<?> self;
    
        public FixedExecutionRunnable(Runnable delegate, Runnable runtimesExceededHandler, int maxRunCount) {
            this.delegate = delegate;
            this.runtimesExceededHandler = runtimesExceededHandler;
            this.maxRunCount = maxRunCount;
        }
    
        @Override
        public void run() {
            delegate.run();
            if(runCount.incrementAndGet() == maxRunCount) {
                boolean interrupted = false;
                try {
                    while(self == null) {
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            interrupted = true;
                        }
                    }
                    self.cancel(true); 
                    runtimesExceededHandler.run();
                } finally {
                    if(interrupted) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    
        // https://stackoverflow.com/questions/7269294/how-to-stop-a-runnable-scheduled-for-repeated-execution-after-a-certain-number-o
        public ScheduledFuture<?> runNTimes(ScheduledExecutorService executor, long period, TimeUnit unit) {
            // self = executor.scheduleAtFixedRate(this, 0, period, unit);
            self = executor.scheduleWithFixedDelay(this, period, period, unit);
            return self;
        }
    }
}
