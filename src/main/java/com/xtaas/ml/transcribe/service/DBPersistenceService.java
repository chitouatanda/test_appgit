package com.xtaas.ml.transcribe.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.ml.classifier.dto.DBPersistenceResult;
import com.xtaas.ml.transcribe.dto.InMemoryPersistenceResult;
import com.xtaas.ml.transcribe.dto.JobQueueS3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.JobResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DBPersistenceService {
	final static private Logger logger = LoggerFactory.getLogger(DBPersistenceService.class);
	final private Map<String, TranscriptionService> transcriptionServiceCache;
	private CallLogRepository callLogRepository;
	private CallClassificationJobQueueRepository callClassificationJobQueueRepository;

	public DBPersistenceService(@Autowired List<TranscriptionService> transcriptionServices,
			@Autowired CallLogRepository callLogRepository,
			@Autowired CallClassificationJobQueueRepository callClassificationJobQueueRepository) {
		this.transcriptionServiceCache = transcriptionServices.stream()
				.collect(Collectors.toMap(TranscriptionService::getServiceProvider, service -> service));
		this.callLogRepository = callLogRepository;
		this.callClassificationJobQueueRepository = callClassificationJobQueueRepository;
	}

	public DBPersistenceResult saveTranscript(JobResult jobResult) {
		TranscriptionService transcriptionService = transcriptionServiceCache
				.get(jobResult.getJobTracker().getTranscriptionServiceName());
		String transcriptText = transcriptionService.getTranscriptText(jobResult.getTrancriptDataInputStream());
		logger.debug("Transcription result: recording id: {}, transcription service: {}, transcription text: {}",
				jobResult.getJobTracker().getMediaLink().getMediaObjectName(),
				jobResult.getJobTracker().getTranscriptionServiceName(), transcriptText);

		JobQueueS3AudioMediaLink mediaLink = (JobQueueS3AudioMediaLink) jobResult.getJobTracker().getMediaLink();
		List<CallLog> callLogFromDB = callLogRepository.findCallLogByPcid(mediaLink.getProspectCallId());
		if (callLogFromDB != null) {
			callLogFromDB.get(0).getCallLogMap().put("transcript", transcriptText);
			callLogRepository.save(callLogFromDB.get(0));
		}
		return new DBPersistenceResult(callLogRepository).withProspectCallId(mediaLink.getProspectCallId());
	}
}