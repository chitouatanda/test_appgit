package com.xtaas.ml.transcribe.service;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.InMemoryPersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

import org.apache.commons.io.IOUtils;

import java.io.IOException;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
public class TranscriberConfig {
    private Function<JobResult, TranscriptPersistenceResult> persistenceService;
    private String transcriptionServiceProvider;
    private BiConsumer<JobResult, TranscriptPersistenceResult> postProcessConsumer;
    private BiConsumer<S3AudioMediaLink, Throwable> exceptionHandler;
    private Predicate<S3AudioMediaLink> mediaLinkValidator;
    private TrafficManager trafficManager;

    private TranscriberConfig() {
    }

    public static TranscriberConfig builder() {
        return new TranscriberConfig();
    }

    public TranscriberConfig withPersistenceService(
            Function<JobResult, TranscriptPersistenceResult> persistenceService) {
        this.persistenceService = persistenceService;
        return this;
    }

    public TranscriberConfig withTranscriptionServiceProvider(String transcriptionServiceProvider) {
        this.transcriptionServiceProvider = transcriptionServiceProvider;
        return this;
    }

    public TranscriberConfig withTranscriptPostProcessConsumer(
            BiConsumer<JobResult, TranscriptPersistenceResult> postProcessConsumerIn) {
        if (this.postProcessConsumer == null) {
            this.postProcessConsumer = postProcessConsumerIn;
        } else {
            this.postProcessConsumer = this.postProcessConsumer.andThen(postProcessConsumerIn);
        }
        return this;
    }

    public TranscriberConfig withExceptionHandler(
            BiConsumer<S3AudioMediaLink, Throwable> exceptionHandlerIn) {
        this.exceptionHandler = exceptionHandlerIn;
        return this;
    }

    public TranscriberConfig withMediaLinkValidator(Predicate<S3AudioMediaLink> mediaLinkValidator) {
        this.mediaLinkValidator = mediaLinkValidator;
        return this;
    }

    public TranscriberConfig withTrafficManager(TrafficManager trafficManager) {
        this.trafficManager = trafficManager;
        return this;
    }
    public Function<JobResult, TranscriptPersistenceResult> getPersistenceService() {
        if (this.persistenceService == null) {
            return (j) -> {
                try {
                    return new InMemoryPersistenceResult(IOUtils.toString(j.getTrancriptDataInputStream(), "UTF-8"));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            };
        }
        return this.persistenceService;
    }

    public String getTranscriptionServiceProvider() {
        if (this.transcriptionServiceProvider == null) {
            return "AWS";
        }
        return this.transcriptionServiceProvider;
    }

    public BiConsumer<JobResult, TranscriptPersistenceResult>  getPostProcessConsumer() {
        if (this.postProcessConsumer == null) {
            return (j, r) -> {};
        }
        return this.postProcessConsumer;
    }

    public BiConsumer<S3AudioMediaLink, Throwable> getExceptionHandler() {
        if (this.exceptionHandler == null) {
            return (j, r) -> {};
        }
        return this.exceptionHandler;
    }

    public Predicate<S3AudioMediaLink> getMediaLinkValidator() {
        if (this.mediaLinkValidator == null) {
            return (o) -> true;
        }
        return this.mediaLinkValidator;
    }

    public Optional<TrafficManager> getTrafficManager() {
        return Optional.ofNullable(trafficManager);
    }
}
