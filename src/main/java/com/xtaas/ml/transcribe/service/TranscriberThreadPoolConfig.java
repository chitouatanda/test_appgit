package com.xtaas.ml.transcribe.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * Simple approach to restrict number of trancription requests fetching and
 * processing trancribed content. @50KB size of each transcription, will take up
 * 50MB space if 100 tokens are available allowing 1000 concurrent fetch and
 * processing
 *
 * @author sushant
 * @version 1.0
 */
@Configuration
public class TranscriberThreadPoolConfig {

    final private int threadPoolSize;
    final private int statusPollThreadPoolSize;

    public TranscriberThreadPoolConfig(
        @Value("${transcribe.thread_pool.size:10}") int threadPoolSize,
        @Value("${transcribe.status_poll.thread_pool.size:10}") int statusPollThreadPoolSize) {
        this.threadPoolSize = threadPoolSize;
        this.statusPollThreadPoolSize = statusPollThreadPoolSize;
    }

    // TODO change from fixed thread pool to custom
    @Bean
    public ExecutorService transcriberExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    @Bean
    public ExecutorService transcriberCheckStatusExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    @Bean
    public ExecutorService transcriberPersistenceExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    @Bean
    public ExecutorService transcriberPostProcessExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    @Bean
    public ScheduledExecutorService transcriberStatusPollExecutor() {
        return Executors.newScheduledThreadPool(statusPollThreadPoolSize);
    }

    @Bean
    public ExecutorService transcriberExceptionHandlingExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }
}
