package com.xtaas.ml.transcribe.service;

import java.net.URI;
import java.util.ArrayList;
import static com.xtaas.db.entity.CallClassificationJobQueue.Status.INPROCESS;
import static com.xtaas.db.entity.CallClassificationJobQueue.Status.QUEUED;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.ml.transcribe.dto.JobQueueS3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

@Service
public class TranscriptionAPIService {

	@Autowired
	TranscriberFactory transcriberFactory;

	@Autowired
	CallClassificationJobQueueRepository queueRepository;

	@Autowired
	DBAPIPersistenceService dBAPIPersistenceService;

	@Value("${classifier.job.chunk_size:1000}")
	int chunkSize;

	@Value("${server.base.url}")
	String serverUrl;

	public void transcribeRecordings() {
		try {
			Pageable pageable = PageRequest.of(0, 2000);
			List<CallClassificationJobQueue> queues = queueRepository.findByStatusAndProducer(
					CallClassificationJobQueue.Status.TRANSCRIBE_QUEUED, pageable);
			if (queues != null && queues.size() > 0) {
//				for (CallClassificationJobQueue callClassificationJobQueue : queues) {
//					callClassificationJobQueue.setStatus(CallClassificationJobQueue.Status.INPROCESS);
//				}
//				List<CallClassificationJobQueue> recordsFromDB = queueRepository.saveAll(queues);
//				List<S3AudioMediaLink> audioMediaLinks = new ArrayList<S3AudioMediaLink>();
//				if (recordsFromDB != null && recordsFromDB.size() > 0) {
//					for (CallClassificationJobQueue callClassificationJobQueue : recordsFromDB) {
//						JobQueueS3AudioMediaLink jobQueueS3AudioMediaLink = new JobQueueS3AudioMediaLink(
//								URI.create(callClassificationJobQueue.getRecordingUrl()),
//								callClassificationJobQueue.getMessageLifecycleId(), null, null,
//								callClassificationJobQueue.getCallDuration(),
//								callClassificationJobQueue.getProspectCallId());
//						audioMediaLinks.add(jobQueueS3AudioMediaLink);
//					}
//				}
//				AtomicBoolean transcriptionComplete = new AtomicBoolean(false);
				Transcriber service = transcriberFactory
						.getTrancriber(() -> TranscriberConfig.builder().withTranscriptionServiceProvider("AWS")
								.withPersistenceService((j) -> dBAPIPersistenceService.saveTranscript(j))
								.withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r))
								.withExceptionHandler((l, t) -> handleException(l, t)));
//				CompletableFuture<List<String>> statusFuture = service.transcribe(() -> audioMediaLinks);
//				statusFuture.join();
//				transcriptionComplete.set(true);
				while (true) {
					int succCnt = 0;
					int total = 0;
					// this thread is going to block
					List<String> result = service.transcribe(() -> {
						return getNextChunk(LocalDateTime.now());
					}).join();

					succCnt += result.stream().filter(status -> "SUCCESS".equals(status)).count();
					total += result.size();
					if (result.size() < chunkSize) {
						break;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<CallClassificationJobQueue> scheduleNextChunk(int chunkSize) {
		Pageable pageable = PageRequest.of(0, chunkSize, Direction.ASC, "jobCreationTime");
		return queueRepository.saveAll(queueRepository.findByStatus(CallClassificationJobQueue.Status.TRANSCRIBE_QUEUED, pageable).stream()
				.map(entry -> entry.withStatus(INPROCESS).withJobStartTime(new Date())
						.withConsumer("TranscriptionAPIService").withConsumerServerId(serverUrl).decrementTtl())
				.collect(Collectors.toList()));
	}

	private void postProcessTranscript(JobResult jobResult, TranscriptPersistenceResult persistenceResult) {
		JobQueueS3AudioMediaLink mediaLink = (JobQueueS3AudioMediaLink) jobResult.getJobTracker().getMediaLink();
		List<CallClassificationJobQueue> jobsFromDB = queueRepository
				.findByProspectCallId(mediaLink.getProspectCallId());
		if (jobsFromDB != null && jobsFromDB.size() > 0) {
			jobsFromDB.get(0).setStatus(CallClassificationJobQueue.Status.COMPLETED);
			jobsFromDB.get(0).setJobEndTime(new Date());
			queueRepository.save(jobsFromDB.get(0));
		}
	}

	private void handleException(S3AudioMediaLink mediaLink, final Throwable t) {
		Throwable cause = t;
		while (cause != null && cause.getCause() != null) {
			if (cause.getMessage() != null) {
				break;
			}
			cause = cause.getCause();
		}
		String errorMsg = cause != null ? cause.getMessage() : null;
		JobQueueS3AudioMediaLink link = (JobQueueS3AudioMediaLink) mediaLink;
		List<CallClassificationJobQueue> jobsFromDB = queueRepository.findByProspectCallId(link.getProspectCallId());
		if (jobsFromDB != null && jobsFromDB.size() > 0) {
			jobsFromDB.get(0).setStatus(CallClassificationJobQueue.Status.ERROR);
			jobsFromDB.get(0).setJobEndTime(new Date());
			jobsFromDB.get(0).setErrorMsg(errorMsg);
			queueRepository.save(jobsFromDB.get(0));
		}
	}

	private List<S3AudioMediaLink> getNextChunk(LocalDateTime time) {
		List<S3AudioMediaLink> links = new ArrayList<S3AudioMediaLink>();
		List<CallClassificationJobQueue> jobsFromDB = scheduleNextChunk(chunkSize);
		if (jobsFromDB != null && jobsFromDB.size() > 0) {
			for (CallClassificationJobQueue callClassificationJobQueue : jobsFromDB) {
				try {
					links.add(JobQueueS3AudioMediaLink.builder()
							.withMediaUri(callClassificationJobQueue.getRecordingUrl())
							.withPhone(callClassificationJobQueue.getPhone())
							.withProspectCallLogId(callClassificationJobQueue.getProspectCallLogId())
							.withProspectCallId(callClassificationJobQueue.getProspectCallId())
							.withCallDuration(callClassificationJobQueue.getCallDuration())
							.withMessageId(callClassificationJobQueue.getId()).build());
				} catch (Exception e) {
					List<CallClassificationJobQueue> jobs = queueRepository
							.findByProspectCallId(callClassificationJobQueue.getProspectCallId());
					if (jobs != null && jobs.size() > 0) {
						jobs.get(0).setStatus(CallClassificationJobQueue.Status.ERROR);
						jobs.get(0).setJobEndTime(new Date());
						jobs.get(0).setErrorMsg(e.toString());
						queueRepository.save(jobs.get(0));
					}
					e.printStackTrace();
				}
			}
		}
		return links;
	}

}