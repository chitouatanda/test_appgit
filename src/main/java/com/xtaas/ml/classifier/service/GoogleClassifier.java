package com.xtaas.ml.classifier.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.cloud.automl.v1.AnnotationPayload;
import com.google.cloud.automl.v1.ExamplePayload;
import com.google.cloud.automl.v1.ModelName;
import com.google.cloud.automl.v1.PredictRequest;
import com.google.cloud.automl.v1.PredictResponse;
import com.google.cloud.automl.v1.PredictionServiceClient;
import com.google.cloud.automl.v1.TextSnippet;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 *<p>Current default classifier client
 *
 * @author sushant
 * @version 1.0
 */
@Primary
@Service
public class GoogleClassifier {
    final static private Logger logger = LoggerFactory.getLogger(GoogleClassifier.class);

    final private String projectId;
    final private String modelId;
    final private String location;
    final private float threshold;

    public enum PhoneNumberType {
        Direct, Indirect;

        private static Map<String, PhoneNumberType> valueMap;
        static { 
            valueMap = Arrays.stream(PhoneNumberType.values()).collect(
                Collectors.toMap(type -> type.toString().toUpperCase(), type -> type));
         }

        public static  Optional<PhoneNumberType> valueOfCaseInsensitive(String str) {
            return Optional.ofNullable(valueMap.get(str != null ? str.toUpperCase() : null));
        }
    }

    /**
     * Default constructor
     */
    public GoogleClassifier(@Value("${classifier.project_id}") String projectId,
            @Value("${classifier.phone_number_type.model.id}") String modelId,
            @Value("${classifier.location}") String location,
            @Value("${classifier.phone_number_type.threshold:65}") float threshold) {
        this.projectId = projectId;
        this.modelId = modelId;
        this.location = location;
        this.threshold = threshold/100;
    }

    /*
     *  
     */
    public Optional<PhoneNumberType> classify(ClassifyContentInput contentInput) {
        logger.debug("Classify content: {}", contentInput);
        try (PredictionServiceClient client = PredictionServiceClient.create()) {
            // Get the full path of the model.
            ModelName modelName = ModelName.of(this.projectId, this.location, this.modelId);
            TextSnippet textSnippet = TextSnippet.newBuilder()
                .setContent(contentInput.getTextContent())
                .setMimeType(contentInput.getMimeType()) // Types: text/plain, text/html
                .build();
            ExamplePayload requestPayload = ExamplePayload.newBuilder().setTextSnippet(textSnippet).build();
            PredictRequest predictRequest = PredictRequest.newBuilder().setName(
                    modelName.toString()).setPayload(requestPayload).build();
            PredictResponse response = client.predict(predictRequest);

            // if no label has score greater than threshold return empty
            Optional<PhoneNumberType> phoneNumberType = Optional.empty();
            for (AnnotationPayload payload : response.getPayloadList()) {
                Optional<PhoneNumberType> tempType = 
                    process(contentInput, payload.getDisplayName(), payload.getClassification().getScore(), this.threshold);
                if (tempType.isPresent() && !phoneNumberType.isPresent()) {
                    phoneNumberType = tempType;
                }
            }
            return phoneNumberType;
        } catch (IOException e) {
            logger.error("Error classifying content: {} {}", new Object[] {contentInput, e});
            throw new RuntimeException(e);
        }
    }

    protected Optional<PhoneNumberType> process(ClassifyContentInput contentInput, String className, float score, float thresholdUsed) {
        logger.debug("Content for claassification: {}, class: {}, score: {}, threshold: {}", contentInput, className, score, thresholdUsed);
        Optional<PhoneNumberType> phoneNumberType;
            if (score > thresholdUsed && 
                (phoneNumberType = PhoneNumberType.valueOfCaseInsensitive(
                    className)).isPresent()) {
                    return phoneNumberType;
            }
        return Optional.empty();
    }
}
