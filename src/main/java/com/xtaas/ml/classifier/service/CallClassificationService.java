package com.xtaas.ml.classifier.service;

import static com.xtaas.db.entity.CallClassificationJobQueue.Status.*;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.stream.Collectors;

import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.repository.CallClassificationJobQueueRepository;
import com.xtaas.db.repository.GlobalContactRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.job.CallClassificationJob;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;
import com.xtaas.ml.transcribe.dto.JobQueueS3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;
import com.xtaas.ml.transcribe.service.InMemoryPersistenceService;
import com.xtaas.ml.transcribe.service.Transcriber;
import com.xtaas.ml.transcribe.service.TranscriberConfig;
import com.xtaas.ml.transcribe.service.TranscriberFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.xtaas.ml.transcribe.service.DBPersistenceService;

@Service
public class CallClassificationService {
    public static final Logger logger = LoggerFactory.getLogger(CallClassificationJob.class);

    final private ProspectCallLogRepository prospectCallLogRepository;
    final private GlobalContactRepository globalContactRepository;
    final private CallClassificationJobQueueRepository queueRepository;
    final private TranscriberFactory transcriberFactory;
    final private InMemoryPersistenceService persistenceService;
    final private DBPersistenceService dbPersistenceService;
    final private GoogleClassifierStatistics classifierStatistics;
    final private GoogleClassifierStatistics statistics;
    final private String transcriptionServiceProvider;
    final private int chunkSize;
    final private String serverUrl;

    public CallClassificationService(@Autowired CallClassificationJobQueueRepository queueRepository,
            @Autowired ProspectCallLogRepository prospectCallLogRepository,
            @Autowired GlobalContactRepository globalContactRepository,
            @Autowired TranscriberFactory transcriberFactory, @Autowired InMemoryPersistenceService persistenceService,
            @Autowired GoogleClassifierStatistics statistics,
            @Autowired DBPersistenceService dbPersistenceService,
            @Autowired GoogleClassifierStatistics classifierStatistics,
            @Value("${classifier.job.transcription_service_provider:AWS}") String transcriptionServiceProvider,
            @Value("${classifier.job.chunk_size:1000}") int chunkSize, 
            @Value("${server.base.url}") String serverUrl) {
        this.queueRepository = queueRepository;
        this.prospectCallLogRepository = prospectCallLogRepository;
        this.globalContactRepository = globalContactRepository;
        this.transcriberFactory = transcriberFactory;
        this.persistenceService = persistenceService;
        this.dbPersistenceService = dbPersistenceService;
        this.classifierStatistics = classifierStatistics;
        this.statistics = statistics;
        this.transcriptionServiceProvider = transcriptionServiceProvider;
        this.chunkSize = chunkSize;
        this.serverUrl = serverUrl;
    }

    public void classifyCallRecordings() {
        LocalDateTime currentTime = LocalDateTime.now();
        String jobTrailId = UUID.randomUUID().toString(); 

        logger.info("<========== Running CallClassificationJob for call recordings with trail id {} ==========>",
                jobTrailId);
        Transcriber service = transcriberFactory.getTrancriber(() -> TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true).withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withPersistenceService((j) -> dbPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r))
                .withExceptionHandler((l, t) -> handleException(l, t)));

        int succCnt = 0;
        int total = 0;
        try {
            while (true) {
                // this thread is going to block
                List<String> result = service.transcribe(() -> {
                    return getNextChunk(jobTrailId, currentTime);
                }).join();

                succCnt += result.stream().filter(status -> "SUCCESS".equals(status)).count();
                total += result.size();
                if (result.size() < chunkSize) {
                    break;
                }
                CallClassificationJob.logger.info(
                        "<========== Completed CallClassificationJob for call recordings with trail id {} with in {}ms, successful {}, failed {} and total {} ==========>",
                        jobTrailId, Duration.between(currentTime.atZone(ZoneId.systemDefault()).toInstant(), Instant.now())
                                .toMillis(),
                        succCnt, total - succCnt, total);
            }
            //InterruptedException | ExecutionException | 
        } catch (CancellationException e) {
            CallClassificationJob.logger.error(
                    "<========== Failed CallClassificationJob with trail id {} ended with exception  ==========>",
                    jobTrailId, e);
        }
    }

    private List<CallClassificationJobQueue> scheduleNextChunk(String jobTrailId, int chunkSize) {
        Pageable pageable = PageRequest.of(0, chunkSize, Direction.ASC, "jobCreationTime");
        return queueRepository.saveAll(queueRepository.findByStatus(QUEUED, pageable)
                .stream()
                .map(entry -> entry.withStatus(INPROCESS).withJobStartTime(new Date())
                        .withJobTrailId(jobTrailId).withConsumer("CallClassificationService")
                        .withConsumerServerId(serverUrl).decrementTtl())
                .collect(Collectors.toList()));
    }


    // push to archive table should happen in this class after success/failure
    // need jobtrail table to recover from stuck in_process jobs or retry on
    // surpassed time
    // will also need a job trail table to track result in terms of asynchronous
    //      service with callback

    private void postProcessTranscript(JobResult jobResult, TranscriptPersistenceResult persistenceResult) {
        JobQueueS3AudioMediaLink mediaLink = 
            (JobQueueS3AudioMediaLink) jobResult.getJobTracker().getMediaLink();

        ClassifyContentInput contentInput = new ClassifyContentInput(
            UUID.fromString(mediaLink.getMediaObjectName()),
                persistenceResult.getTranscript(), "text/plain");
        Optional<PhoneNumberType> classification = classifierStatistics.classify(contentInput);
        classification.ifPresent(type -> {
            boolean directPhone = type == PhoneNumberType.Direct;
            // update global contact
            globalContactRepository
                    .saveAll(globalContactRepository.findByPhone(mediaLink.getPhone()).stream().map(contact -> {
                        contact.setDirectClassificationML(true);
                        contact.setDirectPhone(directPhone);
                        return contact;
                    }).collect(Collectors.toList()));

            // update prospect call log
            prospectCallLogRepository.findById(mediaLink.getProspectCallLogId()).ifPresent(prospectCallLog -> {
                prospectCallLog.getProspectCall().getProspect().setDirectPhone(directPhone);
                prospectCallLogRepository.save(prospectCallLog);
            });
            // archive resutls
        });

        // update job status to completed
        String errorMsg = !classification.isPresent() ? "NON_DETERMINISTIC" : null;
        queueRepository.findById(mediaLink.getMessageId())
                .ifPresent(job -> queueRepository.save(job.withStatus(COMPLETED)
                        .withJobEndTime(new Date()).withErrorMsg(errorMsg)));
    }

    private void handleException(S3AudioMediaLink mediaLink, final Throwable t) {
        Throwable cause = t;
        while(cause != null && cause.getCause() != null) {
            if (cause.getMessage() != null) {
                break;
            }
            cause = cause.getCause();
        }
        String errorMsg = cause != null ? cause.getMessage() : null;
        
        queueRepository.findById(((JobQueueS3AudioMediaLink) mediaLink).getMessageId())
            .ifPresent(job ->
                queueRepository.save(
                    job.withStatus(job.getTtl() > 0 ? QUEUED : ERROR)
                        .withJobEndTime(new Date())
                        .withErrorMsg(errorMsg)));
    }

    private List<S3AudioMediaLink> getNextChunk(String jobTrailId, LocalDateTime time) {
        return scheduleNextChunk(jobTrailId, chunkSize)
            .stream()
            .map(item -> JobQueueS3AudioMediaLink.builder()
                            .withMediaUri(item.getRecordingUrl())
                            .withPhone(item.getPhone())
                            .withProspectCallLogId(item.getProspectCallLogId())
                            .withProspectCallId(item.getProspectCallId())
                            .withCallDuration(item.getCallDuration())
                            .withMessageId(item.getId())
                            .build())
            .collect(Collectors.toList());
    }
}
