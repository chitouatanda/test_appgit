package com.xtaas.ml.classifier.service;

import static com.xtaas.db.entity.ClassifierStatisticsLog.ScoreType.*;
import static com.xtaas.db.entity.ClassifierSampleLog.SampleType.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.google.common.math.Stats;
import com.google.common.math.StatsAccumulator;
import com.xtaas.db.entity.ClassifierSampleLog;
import com.xtaas.db.entity.ClassifierStatisticsLog;
import com.xtaas.db.entity.ClassifierStatisticsLog.ScoreType;
import com.xtaas.db.repository.ClassifierSampleLogRepository;
import com.xtaas.db.repository.ClassifierStatisticsLogRepository;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GoogleClassifierStatistics extends GoogleClassifier {

	final private ClassifierStatisticsLogRepository statsRepo;
	final private ClassifierSampleLogRepository sampleRepo;
	private Map<String, EnumMap<ScoreType, ThreadSafeStatsAccumulator>> scores;
	private Map<String, SampleBucket> failedSamples;
	private CopyOnWriteArrayList<Sample> randomSamples;

	private Float testThreshold;
	final private int sampleSize;
	final private int randomSampleSize;
	final private int bufferSize;
	final private Random random;

	/**
	 * Default constructor
	 */
	public GoogleClassifierStatistics(@Autowired ClassifierStatisticsLogRepository statsRepo,
			@Autowired ClassifierSampleLogRepository sampleRepo, @Value("${classifier.project_id}") String projectId,
			@Value("${classifier.phone_number_type.model.id}") String modelId,
			@Value("${classifier.location}") String location,
			@Value("${classifier.phone_number_type.threshold:90}") float threshold,
			@Value("${classifier.statistics.sample_size:5}") int sampleSize,
			@Value("${classifier.statistics.random_sample_size:10}") int randomSampleSize,
			@Value("${classifier.statistics.buffer_size:5}") int bufferSize) {
		super(projectId, modelId, location, threshold);
		this.sampleRepo = sampleRepo;
		this.statsRepo = statsRepo;
		this.sampleSize = sampleSize;
		this.scores = new ConcurrentHashMap<>();
		this.failedSamples = new ConcurrentHashMap<>();
		this.randomSamples = new CopyOnWriteArrayList<Sample>();
		this.randomSampleSize = randomSampleSize;
		this.bufferSize = bufferSize;
		this.random = new Random();
	}

	protected Optional<PhoneNumberType> process(ClassifyContentInput contentInput, String className, float score,
			float threshold) {
		float thresholdUsed = this.testThreshold != null ? this.testThreshold : threshold;
		Optional<PhoneNumberType> phoneNumberType = super.process(contentInput, className, score, thresholdUsed);
		// log(contentInput, className, score, thresholdUsed);
		scores.computeIfAbsent(className, key -> new EnumMap<>(ScoreType.class))
				.computeIfAbsent(getScoreType(score), key -> new ThreadSafeStatsAccumulator()).add(score);
		if (!phoneNumberType.isPresent() && isHigh(getScoreType(score))) { // Only positive less than threshold score
																			// class makes the cut
			failedSamples.computeIfAbsent(className, key -> new SampleBucket()).add(contentInput, className, score);
		}
		// Approach 1 :- Use Synchronized block.
		if (random.nextInt(100) < this.randomSampleSize) {
			synchronized (randomSamples) {
				randomSamples.add(new Sample(contentInput, className, score));
				if (randomSamples.size() == this.bufferSize) {
					randomSnapshot();
				}
			}
		}
		return phoneNumberType;
	}

	public List<ClassifierStatisticsLog> statsSapshot() {
		List<ClassifierStatisticsLog> statLogs = new ArrayList<>();
		scores.entrySet().stream().forEach(classType -> {
			classType.getValue().entrySet().stream().forEach(e -> {
				Stats stats = e.getValue().snapshot();
				statLogs.add(ClassifierStatisticsLog.create(classType.getKey(), e.getKey(), stats));
			});
		});
		scores = new ConcurrentHashMap<>();
		return statsRepo.saveAll(statLogs);
	}

	public List<ClassifierSampleLog> sampleSnapshot() {
		List<ClassifierSampleLog> sampleLogs = new ArrayList<>();
		failedSamples.entrySet().stream().forEach(e -> {
			List<Sample> bucket = e.getValue().snapshot();
			bucket.stream().forEach(s -> {
				sampleLogs.add(new ClassifierSampleLog().withClassName(s.getClassName()).withSampleType(FAILED_SAMPLE)
						.withScore(s.getScore()).withRecordingId(s.getId().toString())
						.withTranscript(s.getTextContent()).withMimeType(s.getMimeType()));
			});
		});
		failedSamples = new ConcurrentHashMap<>();
		return sampleRepo.saveAll(sampleLogs);
	}

	public List<ClassifierSampleLog> randomSnapshot() {
		List<ClassifierSampleLog> randomSampleLogs = new ArrayList<>();
		randomSamples.stream().forEach(random -> {
			randomSampleLogs
					.add(new ClassifierSampleLog().withClassName(random.getClassName()).withSampleType(RANDOM_SAMPLE)
							.withScore(random.getScore()).withRecordingId(random.getId().toString())
							.withTranscript(random.getTextContent()).withMimeType(random.getMimeType()));

		});
		randomSamples = new CopyOnWriteArrayList<Sample>();
		return sampleRepo.saveAll(randomSampleLogs);
	}

	private class ThreadSafeStatsAccumulator {
		final private ReadWriteLock rwLock;
		final private StatsAccumulator statsAccumulator;

		public ThreadSafeStatsAccumulator() {
			this.rwLock = new ReentrantReadWriteLock();
			this.statsAccumulator = new StatsAccumulator();
		}

		private void add(double value) {
			rwLock.writeLock().lock();
			statsAccumulator.add(value);
			rwLock.writeLock().unlock();
		}

		private Stats snapshot() {
			rwLock.readLock().lock();
			Stats stats = statsAccumulator.snapshot();
			rwLock.readLock().unlock();
			return stats;
		}
	}

	private class SampleBucket {
		final private ReadWriteLock rwLock;
		final private PriorityQueue<Sample> maxHeap;

		public SampleBucket() {
			this.rwLock = new ReentrantReadWriteLock();
			this.maxHeap = new PriorityQueue<>(Comparator.comparing(Sample::getScore).reversed());
		}

		private void add(ClassifyContentInput contentInput, String className, float score) {
			rwLock.writeLock().lock();
			Sample sample = new Sample(contentInput, className, score);
			if (maxHeap.size() < sampleSize) {
				maxHeap.add(sample);
			} else {
				Sample top = maxHeap.remove();
				maxHeap.add(Comparator.comparing(Sample::getScore).compare(sample, top) <= 0 ? sample : top);
			}
			rwLock.writeLock().unlock();
		}

		private List<Sample> snapshot() {
			rwLock.readLock().lock();
			List<Sample> immutableBucket = Arrays.asList(maxHeap.toArray(new Sample[0]));
			rwLock.readLock().unlock();
			return immutableBucket;
		}
	}

	private class Sample extends ClassifyContentInput {
		final private String className;
		final private float score;

		public Sample(ClassifyContentInput contentInput, String className, float score) {
			super(contentInput.getId(), contentInput.getTextContent(), contentInput.getMimeType());
			this.score = score;
			this.className = className;
		}

		public String getClassName() {
			return className;
		}

		public float getScore() {
			return score;
		}
	}

	public void setTestThreshold(Float testThreshold) {
		this.testThreshold = testThreshold != null ? testThreshold / 100 : null;
	}
}