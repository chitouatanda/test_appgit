package com.xtaas.ml.classifier.dto;

import java.util.UUID;

public class ClassifyContentInput {
    private UUID id;
    private String textContent;
    private String mimeType;

    public ClassifyContentInput(UUID idIn, String textContentIn, String mimeTypeIn) {
        this.id = idIn;
        this.textContent = textContentIn;
        this.mimeType = mimeTypeIn;
    }

    public UUID getId() {
        return this.id;
    }

    public String getTextContent() {
        return this.textContent;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public void setId(UUID idIn) {
        this.id = idIn;
    }

    public void setTextContent(String textContentIn) {
        this.textContent = textContentIn;
    }

    public void setMimeType(String mimeTypeIn) {
        this.mimeType = mimeTypeIn;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClassifyContentInput{");
        sb.append("id=").append(this.id);
        sb.append(", ").append("textContent=\'").append(this.textContent).append("\'");
        sb.append(", ").append("mimeType=\'").append(this.mimeType).append("\'");
        sb.append('}');
        return sb.toString();
    }
}
