package com.xtaas.ml.classifier.dto;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

/**
 * <p>
 */
public class DBPersistenceResult implements TranscriptPersistenceResult {

	private String prospectCallId;

	final private CallLogRepository callLogRepository;

	public DBPersistenceResult(@Autowired CallLogRepository callLogRepository) {
		this.callLogRepository = callLogRepository;
	}

	@Override
	public boolean hasTranscriptUri() {
		return false;
	}

	@Override
	public URI getTranscriptUri() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "InDBPersistenceResult [prospectCallId=" + prospectCallId + "]";
	}

	@Override
	public String getTranscript() {
		String transcript = "";
		List<CallLog> callLogFromDB = callLogRepository.findCallLogByPcid(this.prospectCallId);
		if (callLogFromDB != null && callLogFromDB.size() > 0) {
			transcript = callLogFromDB.get(0).getCallLogMap().get("transcript");
		}
		return transcript;
	}

	public DBPersistenceResult withProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
		return this;
	}

}