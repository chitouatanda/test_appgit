package com.xtaas.mvc.controller;

import com.xtaas.db.entity.Prospect;
import com.xtaas.service.LeadIQSearchService;

import com.xtaas.service.ManualProspectSearchServiceImpl;
import com.xtaas.web.dto.ManualProspectSearchDTO;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LeadIQSearchController {

//	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LeadIQSearchController.class);

	@Autowired
	private LeadIQSearchService leadIQSearchService;

	@Autowired
	private ManualProspectSearchServiceImpl manualProspectSearchServiceImpl;

	@RequestMapping(value = "/leadiq/search", method = RequestMethod.POST)
	public Prospect searchContactDetails(@RequestBody Prospect prospectInfo) {
		Prospect responseProspect = null;
		responseProspect = leadIQSearchService.searchContactDetails(prospectInfo);
		return responseProspect;
	}

	// LeadIQ search and return prospects with multiple titles
	@RequestMapping(value = "/leadiq/searchmultiple/{campaignId}/{agentId}", method = RequestMethod.POST)
	public ManualProspectSearchDTO searchMultipleContactDetails(@RequestBody Prospect prospectInfo,
			@PathVariable String campaignId, @PathVariable String agentId) {
		return leadIQSearchService.manualProspectSearch(prospectInfo, campaignId, agentId);
	}

	@RequestMapping(value = "/leadiq/fetchprospect/{sourceId}", method = RequestMethod.GET)
	public ResponseEntity<Prospect> fetchLeadIQProspect(@PathVariable String sourceId) {
		Prospect p = manualProspectSearchServiceImpl.searchProspectFromGlobalContact(sourceId);
		return new ResponseEntity<Prospect>(p, HttpStatus.OK);
	}

}