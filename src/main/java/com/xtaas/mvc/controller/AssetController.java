package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.web.dto.ActiveAssetDTO;
import com.xtaas.web.dto.EmailMessageDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/activeasset")
public class AssetController {

	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(value="/assetId", method = RequestMethod.POST)
	public void activateAsset(@RequestBody ActiveAssetDTO activeAssetDTO) {
		campaignService.activateAsset(activeAssetDTO);
	}
	
	/*@RequestMapping(value="/deliver", method = RequestMethod.POST)
	public void deliverActiveAsset(@PathVariable("campaignId") String campaignId, @RequestBody String emailAddress) {
		campaignService.deliverActiveAsset(campaignId, emailAddress);
	}*/
	
	@RequestMapping(value="/emailtemplate", method = RequestMethod.POST)
	public void setAssetEmailTemplate(@PathVariable("campaignId") String campaignId, @RequestBody EmailMessageDTO emailMessageDTO) {
		 campaignService.setAssetEmailTemplate(campaignId, emailMessageDTO);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public EmailMessageDTO retriveAssetEmailTemplate(@PathVariable("campaignId") String campaignId) {
		 return campaignService.retrivetAssetEmailTemplate(campaignId);
	}
	
	/* START
	 * REASON : Added to test email template on supervisor screen */
	@RequestMapping(value="/testemailtemplate", method = RequestMethod.POST)
	public void testAssetEmailTemplate(@PathVariable("campaignId") String campaignId, @RequestBody EmailMessageDTO emailMessageDTO) {
		campaignService.testAssetEmailTemplate(campaignId, emailMessageDTO);
	}
	/* END */
	
	
}
