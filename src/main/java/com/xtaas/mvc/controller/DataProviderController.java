package com.xtaas.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.DataProviderService;

@RestController
public class DataProviderController {

	@Autowired
	private DataProviderService dataProviderService;

	@RequestMapping(value = "/campaign/xtaas/data/provider", method = RequestMethod.GET)
	public List<String> getXtaasDataProvider() {
		return dataProviderService.getXtaasDataProvider();
	}

}