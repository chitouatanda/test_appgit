/**
 * 
 */
package com.xtaas.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.ImpersonateAuditLog;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.AgentRepository;
import com.xtaas.db.repository.ImpersonateAuditLogRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.AgentStatus;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.UserPrincipal;

/**
 * @author djain
 *
 */
@RestController
@RequestMapping("/landingpage")
public class LandingPageController {

	private static final Logger logger = LoggerFactory.getLogger(LandingPageController.class);

	@Autowired
	private PropertyService propertyService;

	@Autowired
	AgentRepository agentrepository;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	private ImpersonateAuditLogRepository impersonateAuditLogRepository;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getLandingPage() {
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
		String redirectUrl = "";
		List<String> fromInstances = new ArrayList<String>();
		if (organization.getRedirectUrl() != null && !organization.getRedirectUrl().isEmpty()) {
			redirectUrl = organization.getRedirectUrl();
		}
		if (organization.getFromInstance() != null && !organization.getFromInstance().isEmpty()) {
			fromInstances = organization.getFromInstance();
		}
		String pageName = "/listcampaigns";
		boolean qaPortalAccess = propertyService
				.getBooleanApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.QA_PORTAL_ACCESS.name(), true);
		String primaryRole = XtaasUserUtils.getPrimaryRole();

		userImpersonateAuditLog();

		if (primaryRole.equalsIgnoreCase(Roles.ROLE_CLIENT.name())
				|| primaryRole.equalsIgnoreCase(Roles.ROLE_CAMPAIGN_MANAGER.name())) {
			pageName = "/listcampaigns";
		} else if (primaryRole.equalsIgnoreCase(Roles.ROLE_SUPERVISOR.name())) {
			pageName = "/supervisor";
		} else if (primaryRole.equalsIgnoreCase(Roles.ROLE_AGENT.name())) {
			if (isActiveAgentFound()) {
				pageName = "/agentsplash";
			} else {
				return new ResponseEntity<String>("", HttpStatus.UNAUTHORIZED);
			}
		} else if (qaPortalAccess && primaryRole.equalsIgnoreCase(Roles.ROLE_QA.name())) {
			pageName = "/qa";
		} else if (qaPortalAccess && primaryRole.equalsIgnoreCase(Roles.ROLE_SECONDARYQA.name())) {
			pageName = "/secondaryqa";
		} else if (primaryRole.equalsIgnoreCase(Roles.ROLE_ADMIN.name())) {
			pageName = "/adminhome";
		} else if (primaryRole.equalsIgnoreCase(Roles.ROLE_SUPPORT_ADMIN.name())) {
			pageName = "/supportadmin";
		} else if (primaryRole.equalsIgnoreCase(Roles.ROLE_PARTNER.name())) {
			throw new IllegalArgumentException("Partner dashboard is under development. Please come back later.");
		} else {
			throw new IllegalArgumentException(
					"You don't have privilage to this page. Please contact your administrator.");
		}

		String responseMsg = "{\"page\" : \"" + pageName + "\", \"redirectUrl\" : \"" + redirectUrl
				+ "\",  \"fromInstance\" : \"" + fromInstances + "\"}";
		return new ResponseEntity<String>(responseMsg, HttpStatus.OK);
	}

	/**
	 * Check agent status is ACTIVE or INACTIVE in agent collection.
	 * 
	 * @return true for active agent and false for inactive agent.
	 */
	@SuppressWarnings("unlikely-arg-type")
	private boolean isActiveAgentFound() {
		boolean isAgentActive = false;
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (login != null) {
			Agent agent = agentrepository.findOneById(login.getId());
			if (agent != null && agent.getStatus().equals(AgentStatus.ACTIVE)) {
				isAgentActive = true;
			} else {
				isAgentActive = false;
			}
		}
		return isAgentActive;
	}

	/**
	 * if user is impersonated user then add ImpersonateAuditLog entry
	 */
	private void userImpersonateAuditLog() {
		List<GrantedAuthority> authorities = XtaasUserUtils.getGrantedRoles();
		if (authorities == null) {
			logger.error("landing page - got null for authorities");
			return;
		}
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority instanceof SwitchUserGrantedAuthority) {
				SwitchUserGrantedAuthority switchUserGrantedAuthority = (SwitchUserGrantedAuthority) grantedAuthority;
				UserPrincipal userPrincipal = (UserPrincipal) switchUserGrantedAuthority.getSource().getPrincipal();
				String impersonatedUserId = XtaasUserUtils.getCurrentLoggedInUsername();
				logger.debug("User Impersonate Login - Admin:{} user:{}", userPrincipal.getUsername(),
						impersonatedUserId);
				impersonateAuditLogRepository
						.save(new ImpersonateAuditLog(userPrincipal.getUsername(), impersonatedUserId));
				break;
			}
		}
	}

}
