package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.service.CampaignMetaDataService;

@RestController
public class CampaignMetaDataController {

	@Autowired
	private CampaignMetaDataService campaignMetaDataService;

	@RequestMapping(value = { "/campaignmetadata" }, method = RequestMethod.GET)
	public CampaignMetaData retrieve() {
		return campaignMetaDataService.getCampaignMetaData();
	}

	@RequestMapping(value = { "/campaignmetadata/reset" }, method = RequestMethod.GET)
	public String resetData() {
		campaignMetaDataService.resetCampaignMetaData();
		return "CampaignMetaData Refreshed Successfully";
	}
}
