package com.xtaas.mvc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.application.service.QaRejectReasonService;
import com.xtaas.db.entity.QaRejectReason;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value = "/rest/qarejectreason")
public class QaRejectReasonController {

	@Autowired
	private QaRejectReasonService qaRejectReasonService;

	@RequestMapping(method = RequestMethod.GET)
	public List<KeyValuePair<String, String>> retrieveAll() {
		/*
		 * DATE:22/11/2017 
		 * REASON:modified to access QaRejectReason from "qarejectreason" collection in DB
		 */
		QaRejectReason qaRejectReason = qaRejectReasonService.getQaRejectReason("QaRejectReason");	// sending QaRejectReason as an id
		return qaRejectReason.getQaRejectReason();
	}
}
