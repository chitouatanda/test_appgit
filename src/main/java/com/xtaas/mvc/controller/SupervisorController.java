/**
 * 
 */
package com.xtaas.mvc.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPubSub;

import com.xtaas.application.service.HealthCheckskService;
import com.xtaas.application.service.SupervisorService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentAllocationDTO;
import com.xtaas.web.dto.CampaignSettingDTO;
import com.xtaas.web.dto.HealthChecksCriteriaDTO;
import com.xtaas.web.dto.HealthChecksDTO;
import com.xtaas.web.dto.HealthSearchDTO;
import com.xtaas.web.dto.QaFeedbackFormDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;
import com.xtaas.worker.AgentReportThread;
import com.xtaas.worker.CampaignReportThread;

/**
 * @author djain
 *
 */
@RestController
@RequestMapping(value = "/supervisor")
public class SupervisorController {
	private static final Logger logger = LoggerFactory.getLogger(SupervisorController.class);
	
//	public static final Jedis jedis = new Jedis("zimetrics-001.0lrl8n.0001.usw2.cache.amazonaws.com", 6379);
	
	@Autowired
	private SupervisorService supervisorService;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private HealthCheckskService healthCheckskService;
	
	@RequestMapping(value="/agentallocation", method = RequestMethod.GET)
	@ResponseBody
	public AgentAllocationDTO getAgentAllocation() {
		return supervisorService.getAgentAllocation();
	}
	
	@RequestMapping(value = "/feedbackform", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createFeedbackForm(@Valid @RequestBody QaFeedbackFormDTO qaFeedbackFormDTO) {
		try {
			supervisorService.createFeedbackForm(qaFeedbackFormDTO);
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.qaFeedbackFormDTO.id"); 
		}
	}
	
	@RequestMapping(value="/rc", method = RequestMethod.GET)
	@ResponseBody
	public String testRedisClient() {
		return null;
//		jedis.set("testKey", "testValue");
//		String value = jedis.get("testKey");
//		System.out.println("VAL FROM REDIS : " + value);
//		
//		jedis.subscribe( new JedisSubscriber(), "AgentStatus");
//		return value;
	}
	
	@RequestMapping(value = "/campaignReport", method = RequestMethod.GET)
	public String downloadCampaignReport(HttpServletRequest request, HttpServletResponse response)
			throws ParseException, Exception {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		String message = supervisorService.downloadCampaignReportCheckAndSave(login);
		return message;
	}

	@RequestMapping(value = "/agentReport", method = RequestMethod.GET)
	public String downloadAgentReport(HttpServletRequest request, HttpServletResponse response)
			throws ParseException, Exception {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		String message = supervisorService.downloadAgentReportCheckAndSave(login);
		return message;
	}
	
	@RequestMapping(value="/healthcheck", method = RequestMethod.POST)
	public HealthSearchDTO getHealthChecks(@RequestBody HealthChecksDTO healthChecksDTO) {
		ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
		zoomBuySearchDTO.setCampaignId(healthChecksDTO.getCampaignId());
		zoomBuySearchDTO.setCriteria(healthChecksDTO.getCriteria());
		return healthCheckskService.getHealthCheckcount(zoomBuySearchDTO);
	}
	
	@RequestMapping(value = "/data/callables/{campaignId}", method = RequestMethod.PUT)
	public int update(@PathVariable("campaignId") String campaignId) {
		SupervisorStats stats = supervisorStatsRepository.findByCampaignIdTypeAndStatusList("PivotStatus",campaignId,Arrays.asList("PIVOT_FILE_DOWNLOAD","PIVOT_FILE_DOWNLOAD_UPLOADED"));
		if(stats != null) {
			stats.setStatus(stats.getStatus()+"_ACTIVATED");
			supervisorStatsRepository.save(stats);
		}
		return supervisorService.moveDataToCallables(campaignId);
	}
}

