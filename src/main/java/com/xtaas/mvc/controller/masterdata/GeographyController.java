package com.xtaas.mvc.controller.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.domain.valueobject.Location;
import com.xtaas.service.GeographyService;

@RestController
@RequestMapping(value="/geography")
public class GeographyController {

	@Autowired
	private GeographyService geographyService;
	
	@RequestMapping(value="/areas",  method = RequestMethod.GET)
	public List<Location> getAreas() {
		return geographyService.getAreas();
	}
	
	@RequestMapping(value="/countries",  method = RequestMethod.GET)
	public List<Location> getCountries() {
		return geographyService.getCountries();
		
	}
	
	@RequestMapping(value="/states",  method = RequestMethod.GET)
	public List<Location> getStates() {
		return geographyService.getStates();
	}
	
	
}
