package com.xtaas.mvc.controller.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.MasterData;
import com.xtaas.service.MasterDataService;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value="/masterdata/common")
public class MasterDataController {
	@Autowired
	private MasterDataService masterDataService;
	
	@RequestMapping(value="/{id}",  method = RequestMethod.GET)
	public List<KeyValuePair<String, String>> get(@PathVariable("id") String id) {
		MasterData data = masterDataService.getMasterData(id);
		return data.getData();
	}
}
