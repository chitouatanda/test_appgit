package com.xtaas.mvc.controller.masterdata;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.domain.valueobject.DialerCodeDispositionStatus;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.SuccessDispositionStatus;
import com.xtaas.domain.valueobject.FailureDispositionStatus;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value="/masterdata/dispositionstatus")
public class DispositionStatusController {
	
	@RequestMapping(value="/{dispositionType}",  method = RequestMethod.GET)
	public List<KeyValuePair<String, String>> get(@PathVariable("dispositionType") String dispositionType) {
		List<KeyValuePair<String, String>> dispositionStatusList = new ArrayList<KeyValuePair<String, String>>();
		if (dispositionType.equalsIgnoreCase(DispositionType.DIALERCODE.name())) {
			for (DialerCodeDispositionStatus dialerCodeDispositionStatus : DialerCodeDispositionStatus.values()) {
				KeyValuePair<String, String> dispositionStatusPair = new KeyValuePair<String, String>();
				dispositionStatusPair.setKey(dialerCodeDispositionStatus.toString());
				dispositionStatusPair.setValue(dialerCodeDispositionStatus.name());
				dispositionStatusList.add(dispositionStatusPair);
			}
		} else if (dispositionType.equalsIgnoreCase(DispositionType.SUCCESS.name())) {
			for (SuccessDispositionStatus successDispositionStatus : SuccessDispositionStatus.values()) {
				KeyValuePair<String, String> dispositionStatusPair = new KeyValuePair<String, String>();
				dispositionStatusPair.setKey(successDispositionStatus.toString());
				dispositionStatusPair.setValue(successDispositionStatus.name());
				dispositionStatusList.add(dispositionStatusPair);
			}
		} else if (dispositionType.equalsIgnoreCase(DispositionType.FAILURE.name())){
			for (FailureDispositionStatus failureDispositionStatus : FailureDispositionStatus.values()) {
				KeyValuePair<String, String> dispositionStatusPair = new KeyValuePair<String, String>();
				dispositionStatusPair.setKey(failureDispositionStatus.toString());
				dispositionStatusPair.setValue(failureDispositionStatus.name());
				dispositionStatusList.add(dispositionStatusPair);
			}
		} else {
			throw new IllegalArgumentException("Disposition type does not exist");
		}
		return dispositionStatusList;
		
	}
}
