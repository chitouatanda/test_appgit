package com.xtaas.mvc.controller.masterdata;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Domain;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.service.OrganizationService;
import com.xtaas.service.UserService;
import com.xtaas.service.VerticalService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value = "/masterdata/domain")
public class DomainController {
	@Autowired
	private VerticalService verticalService;
	
	@Autowired
	private UserService userService;

	@Autowired
	private OrganizationService organizationService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<KeyValuePair<String, ArrayList<Domain>>> get(@RequestParam(value = "verticals", required = false) List<String> verticals) {
		if (verticals == null || verticals.isEmpty()) {
			List<String> organizationVerticals = new ArrayList<String>();
			Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
			Organization organization = organizationService.findOne(userLogin.getOrganization());
			organizationVerticals.add(organization.getVertical());
			return verticalService.getDomains(organizationVerticals);
		} else {
			return verticalService.getDomains(verticals);
		}
	}
}
