package com.xtaas.mvc.controller.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Vertical;
import com.xtaas.service.VerticalService;

@RestController
@RequestMapping(value = "/masterdata/vertical")
public class VerticalController {
	@Autowired
	private VerticalService verticalService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Vertical> get() {
		return verticalService.getVericals();
	}
}
