package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.web.dto.CampaignManagerMetricsDTO;

@RestController
public class CampaignManagerController {
	
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(value = "/api/campaignmanager/metrics/{campaignManagerId}", method = RequestMethod.GET)
	public CampaignManagerMetricsDTO getCampaignManagerMetrics(@PathVariable("campaignManagerId") String campaignManagerId) {
		return campaignService.getCampaignManagerMetrics(campaignManagerId);
	}

}
