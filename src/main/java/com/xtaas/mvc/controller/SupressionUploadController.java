package com.xtaas.mvc.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.domain.entity.SupervisorStats;
//import com.xtaas.db.entity.SuppressionList;
import com.xtaas.service.CampaignSuppressionService;
import com.xtaas.service.GlobalSuppressionService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.worker.SuppressCallableThread;

@RestController
public class SupressionUploadController {

	@Autowired
	private CampaignSuppressionService campaignSuppressionService;

	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;

	@Autowired
	private GlobalSuppressionService globalSuppressionService;

	@RequestMapping(value = "/campaignsuppression/downloadTemplate", method = RequestMethod.GET)
	public ResponseEntity<?> downloadTemplate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("campaignId") String campaignId, @RequestParam("suppressionType") String suppressionType) throws IOException {
		campaignSuppressionService.downloadSuppressionListTemplate(request, response, campaignId, suppressionType);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/campaignsuppression/upload/{campaignId}/{suppressionType}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadCampaignSupressionFile(@PathVariable("campaignId") String campaignId,
			@PathVariable("suppressionType") String suppressionType, @RequestParam("file") MultipartFile file)
			throws IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,suppressionType, "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType(suppressionType);
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("An upload process is in progress, please try later.");
		}
		return campaignSuppressionService.createCampaignSuppressionList(campaignId, suppressionType, file);
	}
	
	
	/**
	 * DATE :- 08/01/2020 
	 * Added below API to suppress the callable (removing from
	 * calling queue) when we upload the contacts through supervisor screen. 
	 * @param campaignId
	 */
	@RequestMapping(value = "/suppress/callable/{campaignId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> uploadLeadReportFile(@PathVariable("campaignId") String campaignId) {
		SuppressCallableThread suppressCallableThread = new SuppressCallableThread(campaignId);
		Thread thread = new Thread(suppressCallableThread);
		thread.start();
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	/**
	 * DATE :- 13/03/2020 
	 * Added below API to get supression list of campaign.
	 * @param campaignId
	 */
	@RequestMapping(value = "/campaignsuppression/{campaignId}", method = RequestMethod.GET)
	public @ResponseBody Map<String,Integer> retriveSupressionlist(@PathVariable("campaignId") String campaignId) {
		return campaignSuppressionService.getSuppressionListByCampaignId(campaignId);
	}
	
	@RequestMapping(value = "/campaignsuppression/{campaignId}/{suppressionType}", method = RequestMethod.GET)
	public @ResponseBody String multiDownloadSupressionlist(@PathVariable("campaignId") String campaignId,
			@PathVariable("suppressionType") String suppressionType) throws FileNotFoundException, IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,suppressionType, "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType(suppressionType);
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("Download Suppression Already in Process. Please Check After Sometime.");
		}
		return campaignSuppressionService.multiDownloadSupressionlist(campaignId, suppressionType,login.getId());
	}
	
	@RequestMapping(value = "/campaignsuppression/removedublicate", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> removeDublicate() {
		return new ResponseEntity<>(campaignSuppressionService.removeDublicateRecords(), HttpStatus.OK);
	}

	@RequestMapping(value = "/globalemailsuppression", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadGlobalEmailSupression(@RequestParam("file") MultipartFile file) throws Exception {
		return globalSuppressionService.uploadEmailSuppressionList(file);
	}

}
