package com.xtaas.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignServiceImpl;
import com.xtaas.db.entity.EmailBounces;
import com.xtaas.service.EmailBouncesService;
import com.xtaas.web.dto.WebHookEventDTO;

@RestController
public class EmailNotificationController {

	private static final Logger logger = LoggerFactory.getLogger(CampaignServiceImpl.class);

	@Autowired
	private EmailBouncesService emailBouncesService;

	@RequestMapping(value = "/email/bounceevent", method = RequestMethod.POST)
	public ResponseEntity<Boolean> getRequest(HttpServletRequest request,
			@RequestBody List<WebHookEventDTO> webHookEvents) {
		logger.debug("Webhook is calling for Bouncing Emails........ Webhook response is = " + webHookEvents);
		emailBouncesService.handleBounceNotification(webHookEvents);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/email/status/{prospectCallId}", method = RequestMethod.GET)
	public EmailBounces getEmailStatus(@PathVariable String prospectCallId) throws Exception {
		logger.debug("Request to fetch email status for prospectCallId [{}]", prospectCallId);
		return emailBouncesService.getEmailStatus(prospectCallId);
	}

}
