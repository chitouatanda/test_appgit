package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.zxing.WriterException;
import com.xtaas.db.entity.Login;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.UserService;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.LoginDTO;
import com.xtaas.web.dto.PasswordPolicyDTO;
import com.xtaas.web.dto.SupervisorDTO;
import com.xtaas.web.dto.UserImpersonateResultDTO;
import com.xtaas.web.dto.UserImpersonateSearchDTO;


@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET, value="/user/reporttoken")
	public ResponseEntity<String> getReportToken() {
		Login userLogin = userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		String token = "";
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(DateTimeZone.UTC.getID()));
    	cal.add(GregorianCalendar.MINUTE, 5);
    	String expTime = XtaasDateUtils.convertDateToString(cal.getTime(),"yyyyMMddHHmmssZ");
		
    	//for(String role : userLogin.getRoles()) {
		token = EncryptionUtils.encrypt("u="+userLogin.getUsername()+"|r=ROLE_USER|exp="+expTime+"|t="+userLogin.getOrganization()); //ex: u=jsmith|r=XTAAS_ROLE_SUPERVISOR|exp=20150831172506+0530|t=QED
		//}
		return new ResponseEntity<String>(token, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/{id}/supervisor")
    public List<SupervisorDTO> getSupervisorList(@PathVariable("id") String organizationId){
        return userService.getsupervisorList(organizationId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/agent/getnetworkstatus/{username}")
	public Login getAgentNetworkStatus(@PathVariable("username") String username) {
		return userService.getAgentNetworkStatus(username);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/agent/postnetworkstatus/{username}/{networkstatus}")
	public Login postAgentNetworkStatus(@PathVariable("username") String username,@PathVariable("networkstatus") boolean networkstatus) {
		return userService.postAgentNetworkStatus(username,networkstatus);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/passwordattempt/getuserfromdb/{username}")
	public PasswordPolicyDTO getUserFromDB(@PathVariable("username") String username) {
		return userService.getUserFromDB(username);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/passwordattempt/resetuser/{username}")
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR, Roles.ROLE_AGENT, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA })
	public void resetUserAfterFiveAttempts(@PathVariable("username") String username) {
		userService.resetUserAfterFiveAttempts(username);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/passwordattempt/createqrcode/{username}")
	public String createQRCodeMFA(@PathVariable("username") String username) throws WriterException, IOException {
		return userService.createQRCodeMFA(username);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/rest/passwordattempt/totp/{username}/{topt}")
	public boolean getTOTPCode(@PathVariable("username") String username,@PathVariable("topt") int topt) {
		return userService.getTOTPCode(username,topt);
	}

	@PreAuthorize(value = "hasAnyRole('ROLE_SUPPORT_ADMIN')")
	@RequestMapping(method = RequestMethod.GET, value = "/rest/users")
	public UserImpersonateResultDTO getUserList(@RequestParam("searchstring") String searchstring) throws Exception {
		UserImpersonateSearchDTO userImpersonateSearchDTO = JSONUtils.toObject(searchstring,
				UserImpersonateSearchDTO.class);
		return userService.getUserList(userImpersonateSearchDTO);
	}

}
