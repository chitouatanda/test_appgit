package com.xtaas.mvc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.domain.valueobject.CampaignTeamDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/campaignteam")
public class CampaignTeamController {
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.GET)
	public CampaignTeamDTO get(@PathVariable("campaignId") String campaignId) {
		return campaignService.getTeam(campaignId);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void chooseTeam(@PathVariable("campaignId") String campaignId,
			@RequestBody @Valid CampaignTeamDTO campaignTeamDTO) {
		campaignService.chooseTeam(campaignId, campaignTeamDTO);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public void updateTeam(@PathVariable("campaignId") String campaignId,
			@RequestBody @Valid CampaignTeamDTO campaignTeamDTO) {
		campaignService.updateTeam(campaignId, campaignTeamDTO);
	}
}
