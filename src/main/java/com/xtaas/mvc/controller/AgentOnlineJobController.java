package com.xtaas.mvc.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.AgentOnlineJobReportServiceImpl;

@RestController
public class AgentOnlineJobController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AgentOnlineJobController.class);



	@Autowired
	public AgentOnlineJobReportServiceImpl agentOnlineJobReportServiceImpl;

	

	@RequestMapping(method = RequestMethod.GET, value = "/onlineagentreport")
	public ResponseEntity<Boolean> runAgentOnlineReport() {
		logger.debug("Request to AgentOnlineJobReport.");
		
		agentOnlineJobReportServiceImpl.onlineAgentReport();;
		logger.debug("End of AgentOnlineJobReport.");

		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

}
