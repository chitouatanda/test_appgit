package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.TeamService;

@RestController
@RequestMapping(value = "/team/agent/{agentId}/override")
public class SupervisorOverrideAllocationController {
	@Autowired
	private TeamService teamService;
	
	@RequestMapping(value="/{campaignId}", method = RequestMethod.GET)
	public String get(@PathVariable("agentId") String agentId, @PathVariable("campaignId") String campaignId) {
		return "test";
	}
	
	@RequestMapping(value="/{campaignId}", method = RequestMethod.POST)
	public void override(@PathVariable("agentId") String agentId, @PathVariable("campaignId") String campaignId) {
		teamService.temporaryAllocateCampaign(agentId, campaignId);
	}
	
	@RequestMapping(value="/{campaignId}", method = RequestMethod.DELETE)
	public void cancelOverride(@PathVariable("agentId") String agentId, @PathVariable("campaignId") String campaignId) {
		teamService.undoTemporaryAllocation(agentId, campaignId);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public void cancelOverride(@PathVariable("agentId") String agentId) {
		teamService.undoTemporaryAllocation(agentId);
	}
}
