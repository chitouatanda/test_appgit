package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.web.dto.CallGuideDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/callguide")
public class CallGuideController {
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.GET)
	public CallGuideDTO getCallGuide(@PathVariable("campaignId") String campaignId) {
		return campaignService.getCallGuide(campaignId);
	}
	
	@RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
	public void setCallGuide(@PathVariable("campaignId") String campaignId, @RequestBody CallGuideDTO callGuide) {
		campaignService.setCallGuide(campaignId, callGuide);
	}
}
