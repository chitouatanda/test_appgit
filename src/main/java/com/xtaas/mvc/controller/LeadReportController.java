package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ClientMapping;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.ClientMappingRepository;
import com.xtaas.db.repository.NewClientMappingRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.service.LeadReportService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.worker.DemandshoreUploadLeadReportThread;
import com.xtaas.worker.LeadReportThread;
import com.xtaas.worker.NewLeadReportThread;

@RestController
public class LeadReportController {

	@Autowired
	private LeadReportService leadReportService;

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ClientMappingRepository clientMappingRepository;
	
	@Autowired
	private NewClientMappingRepository newClientMappingRepository;
	
	/*@RequestMapping(method = RequestMethod.GET, value = "/leadreport/leadreportbycid/{campaignId}")
	public ResponseEntity<Boolean> getLeadReportByCampaignId(@PathVariable("campaignId") String campaignid)
			throws ParseException, Exception {
		leadReportService.downloadLeadReportByCampaignId(campaignid);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/leadreport/leadreport", method = RequestMethod.GET)
	public ResponseEntity<Boolean> getLeadReport() throws ParseException, Exception {
		leadReportService.downloadLeadReport();
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/leadreport/leadreportinternalbycid/{campaignId}")
	public ResponseEntity<Boolean> downloadLeadInternalReportByCid(@PathVariable("campaignId") String campaignid)
			throws ParseException, Exception {
		leadReportService.downloadLeadInternalReportByCid(campaignid);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/leadreport/leadreportinternal", method = RequestMethod.GET)
	public ResponseEntity<Boolean> downloadLeadInternalReport() throws ParseException, Exception {
		leadReportService.downloadLeadInternalReport();
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

*/	@RequestMapping(method = RequestMethod.GET, value = "/leadreport/zoomreport/{data}")
	public ResponseEntity<Boolean> downloadZoomReport(@PathVariable("data") String date)
			throws ParseException, Exception {
		leadReportService.downloadZoomReport(date);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

/*	@RequestMapping(value = "/leadreport/clientreport", method = RequestMethod.GET)
	public ResponseEntity<?> downloadClientReport() throws ParseException, Exception {
		leadReportService.downloadClientReport();
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/leadreport/clientreportbycid/{campaignId}")
	public ResponseEntity<?> downloadClientReportByCampaignId(@PathVariable("campaignId") String campaignId)
			throws ParseException, Exception {
		leadReportService.downloadClientReportByCampaignId(campaignId);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
*/
	@RequestMapping(value = "/leadreport/filter/client", method = RequestMethod.POST)
	public ResponseEntity<String> downloadClientReportByFilter(@RequestBody ProspectCallSearchDTO prospectCallSearchDTO,
			HttpServletRequest request, HttpServletResponse response) throws ParseException, Exception {
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
			Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
			List<ClientMapping> campaignCMList = clientMappingRepository.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
			if(campaignCMList==null || campaignCMList.size()==0) {
				return new ResponseEntity<String>("No Lead Template Defined to download Report.", HttpStatus.CREATED);
			}
		}
		
		// leadReportService.downloadClientReportByFilter(prospectCallSearchDTO,
		// request, response);
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		LeadReportThread lrt = new LeadReportThread(prospectCallSearchDTO,login, "");
		Thread t1 = new Thread(lrt, "t" + Math.random());
		t1.start();
		return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
	}

//	@RequestMapping(value = "/leadreport/filter/client", method = RequestMethod.GET)
//	public ResponseEntity<String> downloadClientReportByFilter(@RequestParam("searchstring") String searchstring,
//			HttpServletRequest request, HttpServletResponse response) throws Exception {
//		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
//		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
//			List<String> statuses = new ArrayList<>();
//			statuses.add("SUCCESS");
//			prospectCallSearchDTO.setDisposition(statuses);
//		}
//		if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
//			Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
//			List<ClientMapping> campaignCMList = clientMappingRepository.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
//			if(campaignCMList==null || campaignCMList.size()==0) {
//				return new ResponseEntity<String>("No Lead Template Defined to download Report.", HttpStatus.CREATED);
//			}
//		}
//		LeadReportThread lrt = new LeadReportThread(prospectCallSearchDTO, XtaasUserUtils.getCurrentLoggedInUserObject());
//		Thread t1 = new Thread(lrt, "t" + Math.random());
//		t1.start();
//		return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
//	}
	
	
	@RequestMapping(value = "/leadreport/filter/client/{version}", method = RequestMethod.GET)
	public ResponseEntity<String> downloadClientReportByFilter(@RequestParam("searchstring") String searchstring,@PathVariable("version") String version,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		if (prospectCallSearchDTO.getCampaignIds() != null && prospectCallSearchDTO.getCampaignIds().size() == 1) {
			Campaign campaign = campaignService.getCampaign(prospectCallSearchDTO.getCampaignIds().get(0));
			if (!version.isEmpty() && version.equalsIgnoreCase("old")) {
				List<ClientMapping> campaignCMList = clientMappingRepository.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
				if(campaignCMList==null || campaignCMList.size()==0) {
					return new ResponseEntity<String>("No Lead Template Defined to download Report.", HttpStatus.CREATED);
				}
			} else {
				List<NewClientMapping> campaignCMList = newClientMappingRepository.findByCampaignIdAndClientName(campaign.getId(), campaign.getClientName());
				if(campaignCMList==null || campaignCMList.size()==0) {
					return new ResponseEntity<String>("No Lead Template Defined to download Report.", HttpStatus.CREATED);
				}
			}
			
		}
		LeadReportThread lrt = new LeadReportThread(prospectCallSearchDTO, XtaasUserUtils.getCurrentLoggedInUserObject(), version);
		Thread t1 = new Thread(lrt, "t" + Math.random());
		t1.start();
		return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
	}

	@RequestMapping(value = "/leadreport/filter/connect", method = RequestMethod.POST)
	public ResponseEntity<?> downloadConnectReportByFilter(@RequestBody ProspectCallSearchDTO prospectCallSearchDTO,
			HttpServletRequest request, HttpServletResponse response) throws ParseException, Exception {
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		leadReportService.downloadConnectReportByFilter(prospectCallSearchDTO, request, response,XtaasUserUtils.getCurrentLoggedInUserObject());
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/leadreport/filter/connect", method = RequestMethod.GET)
	public ResponseEntity<?> downloadConnectReportByFilter(@RequestParam("searchstring") String searchstring,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		leadReportService.downloadConnectReportByFilter(prospectCallSearchDTO, request, response,XtaasUserUtils.getCurrentLoggedInUserObject());
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/leadreport/upload/{campaignId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadLeadReportFile(@PathVariable("campaignId") String campaignId,
			@RequestParam("file") MultipartFile file) throws IOException {
		return leadReportService.updateLeadDetails(campaignId, file);
	}
	
	@RequestMapping(value = "/leadreport/upload/multicampaign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadMultiCampaignLeadReportFile(@RequestParam("file") MultipartFile file) throws IOException {
		return leadReportService.updateMultiCampaignLeadDetails(file);
	}
	
	@RequestMapping(value = "/leadreport/demandshore", method = RequestMethod.GET)
	public ResponseEntity<?> downloadDemandshoreReportByFilter(@RequestParam("searchstring") String searchstring,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		leadReportService.downloadDemandshoreReportByFilter(prospectCallSearchDTO, login);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/leadreport/upload/demandshore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody ResponseEntity<?> uploadLeadReportFile(@RequestParam("file") MultipartFile file) throws IOException {
		DemandshoreUploadLeadReportThread demandshoreUploadLeadReportThread = new DemandshoreUploadLeadReportThread(file);
		Thread thread = new Thread(demandshoreUploadLeadReportThread);
		thread.start();
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/leadreport/filter/newclient/{version}", method = RequestMethod.GET)
	public ResponseEntity<String> downloadNewClientReportByFilter(@RequestParam("searchstring") String searchstring,
			@PathVariable("version") String version, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		if (prospectCallSearchDTO.getDisposition() == null || prospectCallSearchDTO.getDisposition().isEmpty()) {
			List<String> statuses = new ArrayList<>();
			statuses.add("SUCCESS");
			prospectCallSearchDTO.setDisposition(statuses);
		}
		Organization org = XtaasUserUtils.getCurrentLoggedInUsersOrganization();
		NewLeadReportThread nlrt = new NewLeadReportThread(prospectCallSearchDTO,
				XtaasUserUtils.getCurrentLoggedInUserObject(), org , version, request, response);
		Thread t1 = new Thread(nlrt, "t" + Math.random());
		t1.start();
		return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
	}

}
