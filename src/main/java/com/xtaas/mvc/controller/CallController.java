package com.xtaas.mvc.controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CallService;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.TelnyxCallStatusDTO;

@RestController
public class CallController {
	private static final Logger logger = LoggerFactory.getLogger(CallController.class);

	@Autowired
	private AgentRegistrationService agentRegistrationService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private CallService callService;

	@Autowired
	private ProspectCallLogService prospectCallLogService;

	@RequestMapping(value = "/call/{callsid}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> setCallSid(@PathVariable("callsid") String callSid,
			@RequestBody ProspectCallDTO prospectCallDTO) {
		ProspectCall prospectCall = prospectCallDTO.toProspectCall();
		logger.info("setCallSid() : Setting CallSid - '{}' to ProspectCallId - '{}' ", callSid,
				prospectCall.getProspectCallId());
		agentRegistrationService.setProspectByCallSid(callSid, prospectCall);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = "/api/call/{callsid}/prospectCall", method =
	 * RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE) public
	 * ResponseEntity<Boolean> setCallSidToMap(@PathVariable("callsid") String
	 * callSid, @RequestParam(value = "prospectCallId") String prospectCallId) {
	 * ProspectCallLog prospectCallLog =
	 * prospectCallLogService.findByProspectCallId(prospectCallId); ProspectCall
	 * prospectCall = prospectCallLog.getProspectCall();
	 * logger.info("setCallSid() : Setting CallSid - '{}' to ProspectCallId - '{}' "
	 * , callSid, prospectCall.getProspectCallId());
	 * agentRegistrationService.setProspectByCallSid(callSid, prospectCall); return
	 * new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK); }
	 */

	// added for new dialer
	@RequestMapping(value = "/api/call/{callsid}/prospectCall", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> setCallSidToMap(@PathVariable("callsid") String callSid,
			@RequestBody ProspectCallDTO prospectCallDTO) {
		ProspectCallLog prospectCallLog = prospectCallLogService
				.findByProspectCallId(prospectCallDTO.getProspectCallId());
		ProspectCall prospectCall = prospectCallLog.getProspectCall();
		prospectCall.setIndirectProspects(prospectCallDTO.getIndirectProspects());
		prospectCall.getProspect().setProspectType(prospectCallDTO.getProspect().getProspectType());
		prospectCall.setCampaignId(prospectCallDTO.getCampaignId());
		logger.info("setCallSid() : Setting CallSid - '{}' to ProspectCallId - '{}' ", callSid,
				prospectCall.getProspectCallId());
		agentRegistrationService.setProspectByCallSid(callSid, prospectCall);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	// added for dialer telnyx implementation
	// callsid is call leg id for telnyx  which is used as key in all maps as Call control Id was unavailable
	@RequestMapping(value = "/api/call/{callsid}/prospectCall", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public TelnyxCallStatusDTO getCallStatus(@PathVariable("callsid") String callSid) {
		return callService.getTelnyxCallStatus(callSid);
	}

	@RequestMapping(value = { "/api/call/{callsid}/{callControlProvider}/{isConferenceCall}",
			"/call/{callsid}/{callControlProvider}/{isConferenceCall}" }, method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> unsetCallSid(@PathVariable("callsid") String callSid, @PathVariable String callControlProvider,
			@PathVariable boolean isConferenceCall) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		logger.info("unsetCallSid() : Unsetting CallSid - '{}' ", callSid);
		ProspectCall prospectCall = agentRegistrationService.getProspectByCallSid(callSid);
		if (prospectCall != null) {
			agentRegistrationService.unsetProspectByCallSid(callSid);
		}
		callService.setCallAsCompleteNew(agentId, callSid, callControlProvider, isConferenceCall);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	/**
	 * Api called by AGENT
	 *
	 * @param callSid
	 * @return
	 */
	@RequestMapping(value = { "/call/status" }, method = RequestMethod.POST)
	public ResponseEntity<Boolean> changeCallStatus(@RequestBody CallStatusDTO callStatusDTO) throws IOException {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		logger.info("changeCallStatus() : Update Call Status for AgentId [{}] to [{}] ", agentId,
				callStatusDTO.getCallState());
		// refreshes the agent metrics for supervisor and pushes the new metrics on
		// supervisor screen
		// supervisorService.pushAgentMetricsToSupervisor(agentId, callStatusDTO);
		// log the agent activity for call change event
		agentService.changeCallStatus(callStatusDTO);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	/**
	 * This url will be requested by AGENT whenever a call is to be placed on HOLD
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/call/hold/{callsid}/{usePlivo}/{isConferenceCall}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> holdCall(@PathVariable("callsid") String callSid, @PathVariable boolean usePlivo,
			@PathVariable boolean isConferenceCall, @RequestBody CallStatusDTO callStatusDTO) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		boolean success = callService.placeCallOnHold(agentId, callSid, usePlivo, isConferenceCall);
		if (success) {
			// log the agent activity for call change event
			agentService.changeCallStatus(callStatusDTO);
			logger.info("holdCall() : Call placed on HOLD for callsid [{}] by Agent [{}]", callSid, agentId);
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		} else {
			logger.info("holdCall() : Request to place Call on HOLD FAILED. CallSid [{}] by Agent [{}]", callSid,
					agentId);
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This url will be requested by AGENT whenever a call is to be UNHOLD
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/call/unhold/{callsid}/{usePlivo}/{isConferenceCall}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> unholdCall(@PathVariable("callsid") String callSid, @PathVariable boolean usePlivo,
			@PathVariable boolean isConferenceCall, @RequestBody CallStatusDTO callStatusDTO) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		boolean success = callService.unHoldCall(agentId, callSid, usePlivo, isConferenceCall);
		if (success) {
			// log the agent activity for call change event
			agentService.changeCallStatus(callStatusDTO);
			logger.info("unholdCall() : Call UNHOLD for callsid [{}] by Agent [{}]", callSid, agentId);
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		} else {
			logger.info("unholdCall() : Request to UNHOLD call FAILED. CallSid [{}] by Agent [{}]", callSid, agentId);
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This url will be requested by AGENT whenever a digit needs to be sent
	 *
	 * NOTE - This method is not used anymore because agent can send digits directly
	 * to Twilio/Plivo
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/call/dtmf/{callsid}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> playDTMF(@PathVariable("callsid") String callSid, @RequestBody String digit) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		boolean success = callService.playDTMF(callSid, digit);
		if (success) {
			// log the agent activity for call change event
			logger.info("playDTMF() : Playing DTMF [{}] by Agent [{}]", digit, agentId);
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		} else {
			logger.info("playDTMF() : Request to play DTMF failed. CallSid [{}] by Agent [{}]", callSid, agentId);
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/api/call/{callSid}/call_disconnect", method = RequestMethod.GET)
	public void notify(@PathVariable("callSid") String callSid) {
		callService.notifyAgent(callSid);
	}

}
