package com.xtaas.mvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.LogManagementService;

@RestController
public class LogManagementController {

	private final LogManagementService logLevelService;

	public LogManagementController(LogManagementService logLevelService) {
		this.logLevelService = logLevelService;
	}

	@RequestMapping("/changeloglevel")
	public ResponseEntity<String> changeLogLevel(@RequestParam String classpath, @RequestParam String level) {
		return new ResponseEntity<>(logLevelService.changeLoglevel(classpath, level), HttpStatus.OK);
	}

}
