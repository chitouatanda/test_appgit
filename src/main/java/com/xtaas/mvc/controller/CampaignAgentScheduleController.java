package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.web.dto.WorkScheduleDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/campaignagent/{agentId}/schedule")
public class CampaignAgentScheduleController {
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.GET, value="/{scheduleId}")
	public WorkScheduleDTO get(@PathVariable("campaignId") String campaignId, @PathVariable("agentId") String agentId, @PathVariable("scheduleId") String scheduleId) {
		return campaignService.getWorkSchedule(campaignId, agentId, scheduleId);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void create(@PathVariable("campaignId") String campaignId, @PathVariable("agentId") String agentId,
			 @RequestBody WorkScheduleDTO workScheduleDTO) {
		campaignService.addWorkSchedule(campaignId, agentId, workScheduleDTO);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/{scheduleId}")
	public void update(@PathVariable("campaignId") String campaignId, @PathVariable("agentId") String agentId,
			@PathVariable(value = "scheduleId") String scheduleId, @RequestBody WorkScheduleDTO workScheduleDTO) {
		campaignService.updateWorkSchedule(campaignId, agentId, scheduleId, workScheduleDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/{scheduleId}")
	public void delete(@PathVariable("campaignId") String campaignId, @PathVariable("agentId") String agentId, @PathVariable(value = "scheduleId") String scheduleId) {
		campaignService.deleteWorkSchedule(campaignId, agentId, scheduleId);
	}
}
