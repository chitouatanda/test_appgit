package com.xtaas.mvc.controller;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.service.AgentOnlineReportAPIServiceImpl;
import com.xtaas.web.dto.AgentOnlineReportAPIDTO;

@RestController
public class AgentOnlineReportAPIController {

	private static final Logger logger = LoggerFactory.getLogger(AgentOnlineReportAPIController.class);

	@Autowired
	private AgentOnlineReportAPIServiceImpl agentOnlineReportAPIServiceImpl;

	@RequestMapping(value = "/agentonlinereport", method = RequestMethod.POST)
	public void create(@Valid @RequestBody AgentOnlineReportAPIDTO agentOnlineReportAPIDTO) {
		logger.debug("Request to send Agent online report through POSTMEN API.");
		agentOnlineReportAPIServiceImpl.downloadAgentReport(agentOnlineReportAPIDTO);
	}

}
