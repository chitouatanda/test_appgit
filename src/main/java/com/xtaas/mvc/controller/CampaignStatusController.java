package com.xtaas.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value = "/rest/campaignstatus")
public class CampaignStatusController {
	
	@RequestMapping(method = RequestMethod.GET)
	public List<KeyValuePair<String, String>> retrieveAll() {
		List<KeyValuePair<String, String>> campaignStatusList = new ArrayList<KeyValuePair<String, String>>();
		for (CampaignStatus campaignStatus : CampaignStatus.values()) {
			if(!(campaignStatus.name().equals(campaignStatus.RUNREADY.name()) || campaignStatus.name().equals(campaignStatus.DELISTED.name()))) {
				KeyValuePair<String, String> campaignStatusPair = new KeyValuePair<String, String>();
				campaignStatusPair.setKey(campaignStatus.toString());
				campaignStatusPair.setValue(campaignStatus.name());
				campaignStatusList.add(campaignStatusPair);
			}
		}
		return campaignStatusList;
	}
}
