package com.xtaas.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.AgentService;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentCommandDTO;

@RestController
@RequestMapping(value = "/agent/agentcommand")
public class AgentCommandController {
	
	@Autowired
	private AgentRegistrationService agentRegistrationService;
	
	@Autowired
	private AgentService agentService;
	
	@RequestMapping(method = RequestMethod.POST)
	public void command(@RequestBody @Valid AgentCommandDTO command, HttpSession session) {
		switch (command.getAgentCommand()) {
		case STATUS_CHANGE:
			agentService.changeStatus(command.getAgentStatus(), command.getAgentType(), command.getCampaignId());
			break;
		case HEARTBEAT:
			agentRegistrationService.registerHeartbeat(XtaasUserUtils.getCurrentLoggedInUsername(), command.getAgentStatus());
			break;
		case BROWSER_CLOSED:
			agentService.changeStatusOnBrowserClosed(command.getCampaignId(), command.getAgentType(), session);
			break;
		default:
			throw new NotImplementedException("Support command is [STATUS_CHANGE, HEARTBEAT].");
		}
	}

	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public void approveRequest(@RequestBody @Valid AgentCommandDTO command, HttpSession session) {
		switch (command.getAgentCommand()) {
		case STATUS_CHANGE:
			agentService.approveAgentStatusChangeRequestBySupervisor(command.getAgentId(), command.getAgentStatus(),
					command.getAgentType(), command.getCampaignId(), false);
			break;
		}
	}
	
	@RequestMapping(value = "/approveAll", method = RequestMethod.POST)
	public void approveRequest(@RequestBody @Valid List<AgentCommandDTO> commands, HttpSession session) {
		if (commands != null && commands.size() > 0) {
			for (AgentCommandDTO agentCommandDTO : commands) {
				agentService.approveAgentStatusChangeRequestBySupervisor(agentCommandDTO.getAgentId(), agentCommandDTO.getAgentStatus(),
						agentCommandDTO.getAgentType(), agentCommandDTO.getCampaignId(), false);
			}
		}
	}
}
