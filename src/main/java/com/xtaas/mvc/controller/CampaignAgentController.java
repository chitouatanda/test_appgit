package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/campaignagent/{agentId}")
public class CampaignAgentController {
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.POST)
	public void allocateAgent(@PathVariable("campaignId") String campaignId,
			@PathVariable("agentId") String agentId) {
		campaignService.allocateAgent(campaignId, agentId);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public void deallocateAgent(@PathVariable("campaignId") String campaignId,
			@PathVariable("agentId") String agentId) {
		campaignService.deallocateAgent(campaignId, agentId);
	}
}
