package com.xtaas.mvc.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.CountryStateService;

@RestController
public class CountryStateController {

	@Autowired
	private CountryStateService countryStateService;

	@RequestMapping(value = "/countryState/{countryName:.+}", method = RequestMethod.GET)
	public List<String> getCountryDetails(@PathVariable("countryName") String countryName) {
		return countryStateService.getStatesOfCountry(countryName);
	}
	
	@RequestMapping(value = "/countryState", method = RequestMethod.GET)
	public List<String> getCountry() {
		return countryStateService.getCountries();
	}
	
	

}
