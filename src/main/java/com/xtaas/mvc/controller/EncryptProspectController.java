package com.xtaas.mvc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.EncryptProspectService;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLogEnc;
import com.xtaas.domain.entity.PcLogNew;

@RestController
public class EncryptProspectController {

	private static final Logger logger = LoggerFactory.getLogger(EncryptProspectController.class);

	@Autowired
	private EncryptProspectService encryptProspectService;

	@RequestMapping(value = "/encryptProspect", method = RequestMethod.POST)
	public void create(@RequestBody PcLogNew encryptProspect) {
		encryptProspectService.createProspect(encryptProspect);
	}

	@RequestMapping(value = "/encryptProspect/{campaignId}", method = RequestMethod.GET)
	public List<ProspectCallLogEnc> getABMListByPagination(@PathVariable("campaignId") String campaignId) {
		return encryptProspectService.getAllProspects(campaignId);
	}

}