/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xtaas.application.service.AuthTokenGeneratorService;
import com.xtaas.infra.security.AppManager.App;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;

/**
 * Controller responsible for checking if a user is authenticated. If not then redirect to login page. 
 * If authenticated and requests from support system (like BSS, Analytics etc) then generate token and return them.
 * 
 * Token is in format appname:data:hash where, 
 * 		appname is the name of the application which is generating the token
 * 		data is two-way encrypted data using the app's secret key. After decryption data will be in format username|org|role|time in MS
 * 		hash is one-way encryption of data using the app's secret key
 * 
 * Each support system knows there secret key and would be able to decrypt the token using the secret key they have.
 * 
 * @author djain
 *
 */
@Controller
@RequestMapping(value = "/cas")
public class CentralAuthenticationController {
	
	private static final Logger logger = LoggerFactory.getLogger(CentralAuthenticationController.class);
	
	@Autowired
	private AuthTokenGeneratorService authTokenGeneratorService;
	
	@RequestMapping(value="/authenticate", method=RequestMethod.GET)
    public String authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String appName = request.getParameter("app");
		String redirectUrl = request.getParameter("redir");
		String currentLoggedInUsername = XtaasUserUtils.getCurrentLoggedInUsername();
		logger.debug("authenticate() : Authentication request from APP [{}]", appName);	
		
		//check if AppName is a valid AppName
		boolean appNameValid = EnumUtils.isValidEnum(App.class, appName);
		if (!appNameValid) {
			logger.error("authenticate() : INVALID APP : [{}]", appName);
			response.sendError( HttpServletResponse.SC_UNAUTHORIZED, "Unknown App - " + appName);
		} else if (currentLoggedInUsername == null || currentLoggedInUsername.equalsIgnoreCase("anonymousUser")) { //if user session is not already established
			//then redirect to login page
			logger.error("authenticate() : Request Unauthenticated. Redirecting to Login page.");
			return "redirect:/#!/login?redir="+URLEncoder.encode(redirectUrl, XtaasConstants.UTF8);
		} else {
			String token = URLEncoder.encode(authTokenGeneratorService.generateUserToken(appName, currentLoggedInUsername), XtaasConstants.UTF8);

			StringBuilder redirUrlBuilder = new StringBuilder(redirectUrl);
			if (redirectUrl.contains("&")) {
				redirUrlBuilder.append("&token=").append(token);
			} else {
				redirUrlBuilder.append("?token=").append(token);
			}
			
			String redirUrl = redirUrlBuilder.toString();
			logger.debug("authenticate() : SecurityContext is available for user [{}]. Redirecting to [{}]", XtaasUserUtils.getCurrentLoggedInUsername(), redirUrl);
			return "redirect:"+redirUrl;
		}
		
		return null;
	}
}
