package com.xtaas.mvc.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import com.xtaas.BeanLocator;
import com.xtaas.service.PropertyServiceImpl;
import com.xtaas.web.dto.ZoomTokenDTO;
import com.xtaas.worker.DataBuyThread;
import org.apache.commons.collections.CollectionUtils;
import com.xtaas.web.dto.CustomBuyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.infra.zoominfo.service.ZoomInfoResponse;
import com.xtaas.service.ApiTestingService;
import com.xtaas.web.dto.CampaignPerformanceDTO;
import com.xtaas.web.dto.OneOffDTO;

@RestController
public class ApiTestingController {

	@Autowired
	public ApiTestingService apiTestingService;

	@Autowired
	public PropertyServiceImpl propertyServiceImpl;




	//// This is interaction cleanup program.
	@RequestMapping(value = "/campaign/getinteractions", method = RequestMethod.POST)
	public ResponseEntity<String> getCallLogRecordsForCampaign(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.getCallLogRecordsForCampaign(oneOffDTO);///$$$$$$$$$$$$$$$
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	// this is  phone number directand indirect tagging  program.
	@RequestMapping(value = "/campaign/tagdirectindirect", method = RequestMethod.POST)
	public ResponseEntity<String> tagDirectIndirectData(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.tagdirectindirect(oneOffDTO);///$$$$$$$$$$$$$$$
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
	//  this is setting direct,indirect and quality bucket program.
	@RequestMapping(value = "/campaign/testqualitybuckeet", method = RequestMethod.POST)
	public ResponseEntity<String> testQualityBuckeet(@Valid @RequestBody OneOffDTO oneOffDTO) {
		String str = apiTestingService.testQualityBuckeet(oneOffDTO);///$$$$$$$$$$$$$$$		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/campaign/testsalutaryqualitybucket", method = RequestMethod.POST)
	public ResponseEntity<String> testSalutaryQualityBucket(@Valid @RequestBody OneOffDTO oneOffDTO) {
		String result = apiTestingService.testQualityBucketForProspectSalutary(oneOffDTO);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/campaign/testsalutaryqualitybuckettemp", method = RequestMethod.POST)
	public ResponseEntity<String> testQualityBucketForProspectSalutaryTemp(@Valid @RequestBody OneOffDTO oneOffDTO) {
		String result = apiTestingService.testQualityBucketForProspectSalutaryTemp(oneOffDTO);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/campaign/salutarytimezone", method = RequestMethod.POST)
	public ResponseEntity<String> setSalutaryTimeZone(@Valid @RequestBody OneOffDTO oneOffDTO) {
		String result = apiTestingService.setSalutaryTimeZone(oneOffDTO);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

    // this is the  program to get companies by  criteria
	@RequestMapping(value = "/campaign/buildquery/bycriteria", method = RequestMethod.POST)
	public ResponseEntity<List<ZoomInfoResponse>> buildQueryByCriteria(@Valid @RequestBody OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException{
		List<ZoomInfoResponse> list = apiTestingService.buildQueryByCriteria(oneOffDTO);///$$$$$$$$$$$$$$$		
		return new ResponseEntity<List<ZoomInfoResponse>>(list, HttpStatus.OK);
	}
	
	

	// this is the program to get companies by industries
	@RequestMapping(value = "/campaign/buildquery/byindustries", method = RequestMethod.POST)
	public ResponseEntity<List<ZoomInfoResponse>> buildQueryByIndustries(@Valid @RequestBody OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException{

		List<ZoomInfoResponse> list = apiTestingService.buildQueryByIndustries(oneOffDTO);///$$$$$$$$$$$$$$$
		
		return new ResponseEntity<List<ZoomInfoResponse>>(list, HttpStatus.OK);
	}
	
	
	
	//######################
	// this is the program to purchase success records by email id.
	@RequestMapping(value = "/campaign/purchasesuccess", method = RequestMethod.POST)
	public ResponseEntity<String> buildQueryPurchaseSuccess(@Valid @RequestBody OneOffDTO oneOffDTO) throws NoSuchAlgorithmException, IOException  {

		String str = apiTestingService.buildQueryPurchaseSuccess(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
	// This program is used to remove  duplcate prospects
	@RequestMapping(value = "/campaign/removeduplicateprospects", method = RequestMethod.POST)
	public ResponseEntity<String> removeDuplicateProspects(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.removeDuplicateProspects(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	// 	this program is  used to remove unwanted characters from  phone numbers.
	@RequestMapping(value = "/campaign/removespacesfromphone", method = RequestMethod.POST)
	public ResponseEntity<String> removeSpacesFromPhone(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.removeSpacesFromPhone(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	// this program is used to remove success duplicates
	@RequestMapping(value = "/campaign/removesuccessduplicates", method = RequestMethod.POST)
	public ResponseEntity<String> removeSuccessDuplicates(@Valid @RequestBody OneOffDTO oneOffDTO) {
		String str = null;
		try {
			str = apiTestingService.removeSuccessDuplicates(oneOffDTO);///$$$$$$$$$4
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	

	// this program is  used to keep 4 prospects per company
	@RequestMapping(value = "/campaign/gettenprospect", method = RequestMethod.POST)
	public ResponseEntity<String> getTenProspectsForACompany(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.getTenProspectsForACompany(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/campaign/pcianalysistemp", method = RequestMethod.POST)
	public ResponseEntity<String> getPcianalysisTemp(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.getPCIAnalysisTemp(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
	/*@RequestMapping(value = "/campaign/pciextraction", method = RequestMethod.POST)
	public ResponseEntity<String> getPCIExtraction(@Valid @RequestBody OneOffDTO oneOffDTO) {

		String str = apiTestingService.getPCIExtraction(oneOffDTO);///$$$$$$$$$4
		
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}*/

	
	@RequestMapping(value="/campaign/upload/{campaignId}", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity<List<ProspectCallLog>> uploadCampaignContactFile(@PathVariable("campaignId") String campaignId, @RequestParam("file") MultipartFile file){	
		List<ProspectCallLog> list = apiTestingService.readBooksFromCSV(campaignId, file);//$$$$$$$$$
		return new ResponseEntity<List<ProspectCallLog>>(list, HttpStatus.OK);

	}
	
	@RequestMapping(value="/campaign/zoomtoken", method=RequestMethod.POST)
    public ResponseEntity<String> createZoomToken(){	
		String token = apiTestingService.createZoomToken();//$$$$$$$$$
		return new ResponseEntity<String>(token, HttpStatus.OK);

	}
	
	// Deliver assets to campaign.
		@RequestMapping(value = "/campaign/deliverAsset", method = RequestMethod.POST)
		public ResponseEntity<String> deliverCampaignAsset(@Valid @RequestBody OneOffDTO oneOffDTO) {

			String str = apiTestingService.deliverCampaignAsset(oneOffDTO);///$$$$$$$$$$$$$$$
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
	

		@RequestMapping(value = "/campaign/upload/scltopcl/{campaignId}/{source}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> copySCLToPCL(@PathVariable String campaignId,@PathVariable String source,
				@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.copySCLToPCL(campaignId,source, file);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}

		@RequestMapping(value = "/campaign/upload/pcltopcl/{campaignId}/{source}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> copyPCLToPCL(@PathVariable String campaignId,@PathVariable String source,
				@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.copyPCLToPCL(campaignId,source, file);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}

		@RequestMapping(value = "/campaign/upload/pcitopcl/{campaignId}/{source}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> copyPCIToPCL(@PathVariable String campaignId,@PathVariable String source,
				@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.copyPCIToPCL(campaignId, source,file);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}
		
//	 	this program is  used to remove unwanted characters from  phone numbers.
		@RequestMapping(value = "/campaign/movedatatonewsuppression", method = RequestMethod.POST)
		public ResponseEntity<String> movedatatonewsuppression(@Valid @RequestBody OneOffDTO oneOffDTO) {

			String str = apiTestingService.movedatatonewsuppression(oneOffDTO);///$$$$$$$$$4
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
//	 	this program is  used to remove unwanted characters from  phone numbers.
		@RequestMapping(value = "/campaign/downloadsubprospect", method = RequestMethod.POST)
		public ResponseEntity<String> downloadDataSubProspect(@Valid @RequestBody OneOffDTO oneOffDTO) {

			String str = apiTestingService.downloadDataSubProspect(oneOffDTO);///$$$$$$$$$4
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		// this program is  used to keep 4 prospects per company
		@RequestMapping(value = "/campaign/movetonewabm", method = RequestMethod.POST)
		public ResponseEntity<String> moveDataFromAbmListToAbmListNew(@Valid @RequestBody OneOffDTO oneOffDTO) {

			String str = apiTestingService.moveDataToNewAbm(oneOffDTO);///$$$$$$$$$4
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}

		@RequestMapping(value = "/campaign/upload/uploadOldSuppression/{campaignId}/{type}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> uploadOldSuppression(@PathVariable String campaignId,@PathVariable String type,
				@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.uploadOldSuppression(campaignId,type, file);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}
		
		@RequestMapping(value = "/campaign/salttopcl/{counter}", method = RequestMethod.POST)
		public ResponseEntity<String> moveSalutaryData(@PathVariable String counter) {

			String str = apiTestingService.moveSalutaryData(counter);///$$$$$$$$$4
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/upload/copytopcl/{campaignId}/{source}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> copyDATAToPCL(@PathVariable String campaignId,@PathVariable String source,
				@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.copyDATAToPCL(campaignId,source, file);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}
		
		@RequestMapping(value = "/campaign/telnyxrecording/{days}", method = RequestMethod.POST)
		public ResponseEntity<String> updateMissingRecording(@PathVariable String days) {

			String str = apiTestingService.updateMissingRecording(days);///$$$$$$$$$4
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/campaignSettings", method = RequestMethod.POST)
		public ResponseEntity<String> changeCampaignSettings(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.changeCampaignSettings(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}	
		
		@RequestMapping(value = "/campaign/campaigncache", method = RequestMethod.POST)
		public ResponseEntity<String> getCampaignCache(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.getCampaignCache(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}	
		
		@RequestMapping(value = "/campaign/create/prospect/{campaignId}/{number}", method = RequestMethod.GET)
		public ResponseEntity<String> createProspectUsingFaker(@PathVariable String campaignId,@PathVariable int number) {
			String str = apiTestingService.createProspectUsingFaker(campaignId,number);///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}
		
		@RequestMapping(value = "/campaign/invalidsalt", method = RequestMethod.GET)
		public ResponseEntity<String> moveInvalidSalutary() {
			String str = apiTestingService.moveInvalidSalutary();///$$$$$$$$$$$$$$$
			return new ResponseEntity<String>(str, HttpStatus.OK);

		}
		
		@RequestMapping(value = "/campaign/telnyx/missing/recordings", method = RequestMethod.POST)
		public ResponseEntity<String> telnyxMissingRecordings(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.telnyxMissingRecordings(oneOffDTO);///$$$$$$$$$  campaignId && dispositionStatus 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/encrypt/prospectcalllog", method = RequestMethod.POST)
		public ResponseEntity<String> convertProspectcalllogToEncryptedProspectcalllog(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.convertProspectcalllogToEncryptedProspectcalllog(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/prospect/prospectupdate", method = RequestMethod.POST)
		public ResponseEntity<String> updateProspect(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.updateProspect(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}

		@RequestMapping(value = "/campaign/prospect/prospectupdate/multiple", method = RequestMethod.POST)
        public ResponseEntity<String> updateManyProspect(@Valid @RequestBody OneOffDTO oneOffDTO) {
            String str = apiTestingService.updateMultipleProspects(oneOffDTO);
            return new ResponseEntity<String>(str, HttpStatus.OK);
        }

		@RequestMapping(value = "/campaign/prospect/getprospect", method = RequestMethod.POST)
		public ResponseEntity<String> getProspect(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.getProspect(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/prospect/getencryptprospect", method = RequestMethod.POST)
		public ResponseEntity<String> getEncryptProspect(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.getEncryptProspect(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/prospect/removecallables", method = RequestMethod.POST)
		public ResponseEntity<String> removeCallables(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.removeCallables(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/prospect/getbackcallables", method = RequestMethod.POST)
		public ResponseEntity<String> getBackCallabes(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.getBackCallabes(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/getcampaign", method = RequestMethod.POST)
		public ResponseEntity<String> getCampaign(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.getCampaign(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/databuykill", method = RequestMethod.POST)
		public ResponseEntity<String> killdatabuyqueue(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.killdatabuyqueue(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/changebuyStatus", method = RequestMethod.POST)
		public ResponseEntity<String> changeBuyStatus(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.changeBuyStatus(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/startemail", method = RequestMethod.POST)
		public ResponseEntity<String> startEmail(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.startEmail(oneOffDTO);///$$$$$$$$$  campaignId 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/startbuy", method = RequestMethod.POST)
		public ResponseEntity<String> startDataBuy(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.startCampaignDataBuy(oneOffDTO); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/emailToMD5", method = RequestMethod.POST)
		public ResponseEntity<String> emailToMD5(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.emailToMD5(oneOffDTO); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/updatelateststatus", method = RequestMethod.POST)
		public ResponseEntity<String> updateLatestStatus(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.updateLatestStatus(oneOffDTO); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/campaign/salutory/everstring", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> getCompanyInfoFromEverstring(@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.getCompanyInfoFromEverstring(file); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/create/salutary/company", method = RequestMethod.GET)
		public ResponseEntity<String> createSalutaryCompany() {
			String str = apiTestingService.createSalutaryCompany();
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/salutory/zoominfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
		@Consumes(MediaType.MULTIPART_FORM_DATA)
		public ResponseEntity<String> getCompanyInfoFromZoomInfo(@RequestParam("file") MultipartFile file) {
			String str = apiTestingService.getCompanyInfoFromZoomInfo(file); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}

		@RequestMapping(value = "/campaign/convert/everstringcompany/mastercompany", method = RequestMethod.GET)
		public ResponseEntity<String> convertEverstringCompanyToMasterCompay() {
			String str = apiTestingService.convertEverstringCompanyToMasterCompay();
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/update/prospect/salutary/company/details", method = RequestMethod.GET)
		public ResponseEntity<String> updateProspectSalutaryCompanyDetails() {
			String str = apiTestingService.updateProspectSalutaryCompanyDetails();
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/fixcrmmapping", method = RequestMethod.POST)
		public ResponseEntity<String> fixCrmMapping(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.fixCrmMapping(oneOffDTO); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/find/prospectcallid", method = RequestMethod.POST)
		public ResponseEntity<String> findByProspectCallId(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.findByProspectCallId(oneOffDTO); 
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/downloadaberdeen", method = RequestMethod.POST)
		public ResponseEntity<String> downloadData(@Valid @RequestBody OneOffDTO oneOffDTO) {

			String str = apiTestingService.downloadData(oneOffDTO);///$$$$$$$$$$$$$$$
			
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/performance", method = RequestMethod.POST)
		public ResponseEntity<List<CampaignPerformanceDTO>> getCampaignPerformance(@Valid @RequestBody OneOffDTO oneOffDTO) {
			List<CampaignPerformanceDTO> list = apiTestingService.getCampaignPerformance(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<List<CampaignPerformanceDTO>>(list, HttpStatus.OK);
		}


		@RequestMapping(value = "/signalwire/recordings/{callSID}", method = RequestMethod.GET)
		public ResponseEntity<String> fetchSignalwireRecordingUrl (@PathVariable String callSID) {
			String str = apiTestingService.fetchSignalwireRecordingUrl(callSID);
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/recordingurl/stamp/{callSID}/{email:.+}", method = RequestMethod.GET)
		public ResponseEntity<?> stampRecordingUrl (@PathVariable String callSID, @PathVariable String email) {
			apiTestingService.fetchAndSaveRecordingUrl(callSID, email, true);
			return new ResponseEntity<String>("", HttpStatus.OK);
		}

		@RequestMapping(value = "/countrydetails/fetch", method = RequestMethod.POST)
		public ResponseEntity<String> fetchAndAddCountryDetailsInDB() {
			String str = apiTestingService.fetchAndAddCountryDetailsInDB();
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
	
		
		@RequestMapping(value = "/campaign/suppressMD5Emails", method = RequestMethod.POST)
		public ResponseEntity<String> suppressMD5Emails(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.suppressMD5Emails(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/supervisorstatskill", method = RequestMethod.POST)
		public ResponseEntity<String> supstatschange(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.supstatschange(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/campaign/extractCID", method = RequestMethod.POST)
		public ResponseEntity<String> extractCID(@Valid @RequestBody OneOffDTO oneOffDTO) {
			String str = apiTestingService.extractCID(oneOffDTO);///$$$$$$$$$4
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}

	@RequestMapping(value = "/campaign/salutary/data", method = RequestMethod.POST)
	public ResponseEntity<String> moveSalutaryProspect() {
		System.out.print("Request received for Salutary Data");
		String str = apiTestingService.moveSalutaryProspect();///$$$$$$$$$4

		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@RequestMapping(value = "/campaign/salutary/datalive/{pageNo}", method = RequestMethod.GET)
	public ResponseEntity<String> moveSalutaryDataLive(@PathVariable int pageNo) {
		String result = apiTestingService.moveSalutaryDataLive(pageNo);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/campaign/upload/source/{source}", method = RequestMethod.GET)
	public ResponseEntity<String> uploadSource(@PathVariable String source) {
		String str = apiTestingService.uploadSource(source);
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@RequestMapping(value = "/campaign/clear/ratelimiter/cache", method = RequestMethod.GET)
	public ResponseEntity<String> clearRateLimitCache() {
		String str = "Failed";
		DataBuyThread dataBuyThreadTask = BeanLocator.getBean("dataBuyThread", DataBuyThread.class);
		if (CollectionUtils.isNotEmpty(propertyServiceImpl.rateLimitPriorityList)) {
			propertyServiceImpl.rateLimitPriorityList = null;
			dataBuyThreadTask.rateLimiter = null;
			str = "Clear";
		}
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@RequestMapping(value = "/campaign/api/databuy", method = RequestMethod.POST)
	public ResponseEntity<String> apiDataBuy(@Valid @RequestBody CustomBuyDTO createCustomBuyDTO) {
		String str = apiTestingService.apiDataBuy(createCustomBuyDTO);///$$$$$$$$$4
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@RequestMapping(value = "/campaign/update/zoom/token", method = RequestMethod.POST)
	public ResponseEntity<String> updateZoomToken(@Valid @RequestBody ZoomTokenDTO zoomTokenDTO) {
		String str = apiTestingService.updateZoomToken(zoomTokenDTO);///$$$$$$$$$4
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}


	@RequestMapping(value = "/campaign/read/s3/{startIndex}", method = RequestMethod.GET)
	public ResponseEntity<String> readSalutaryData(@PathVariable int startIndex) {
		String str = apiTestingService.readSalutaryData(startIndex);
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

}
