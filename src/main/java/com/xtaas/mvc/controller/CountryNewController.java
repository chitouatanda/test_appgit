package com.xtaas.mvc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.service.CountryNewService;

@RestController
public class CountryNewController {

	@Autowired
	private CountryNewService countryNewService;

	@RequestMapping(value = "/countrynew/{countryName}", method = RequestMethod.GET)
	public List<String> getCountryDetails(@PathVariable("countryName") String countryName) {
		return countryNewService.getStatesOfCountry(countryName);
	}

}
