package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.TeamService;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.TeamSortClauses;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.TeamDTO;
import com.xtaas.web.dto.TeamSearchDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;

@RestController
public class TeamController {
	@Autowired
	private TeamService teamService;
	
	@Autowired 
    private CampaignService campaignService;

	@RequestMapping(value = "/team", method = RequestMethod.GET)
	public List<TeamSearchResultDTO> retrieve(@RequestParam("searchstring") String searchstring) throws IOException, ParseException {
		TeamSearchDTO teamSearchDTO = JSONUtils.toObject(searchstring, TeamSearchDTO.class);
		switch (teamSearchDTO.getClause()) {
		case BySearch:
			return teamService.searchByCriteria(teamSearchDTO, PageRequest.of(teamSearchDTO.getPage(), teamSearchDTO
					.getSize(), Sort.by(teamSearchDTO.getDirection(), teamSearchDTO.getSort().getFieldName())));
		case ByPreviousCampaign:
			return teamService.searchByPreviousWorkedCampaigns(teamSearchDTO.getCampaignId(), teamSearchDTO.getSize());
		case ByAutoSuggest:
			return teamService.searchByAutosuggest(teamSearchDTO.getCampaignId(), PageRequest.of(teamSearchDTO.getPage(), teamSearchDTO
					.getSize(), Sort.by(Direction.ASC, TeamSortClauses.PerLeadPrice.getFieldName())));
		case ByRating:
			return teamService.searchByRating(teamSearchDTO.getCampaignId(), PageRequest.of(teamSearchDTO.getPage(), teamSearchDTO
					.getSize(), Sort.by(Direction.DESC, TeamSortClauses.AverageRating.getFieldName())));
		case ByCurrentInvitation:
			return teamService.searchByCurrentInvitations(teamSearchDTO.getCampaignId(), PageRequest.of(teamSearchDTO.getPage(), teamSearchDTO
					.getSize(), Sort.by(Direction.ASC, TeamSortClauses.PerLeadPrice.getFieldName())));
		case ByCurrentSelection:
			return teamService.searchByCurrentSelection(teamSearchDTO.getCampaignId());
		case ByQa:
			return teamService.searchByQa();
		case ByQa2:
			return teamService.searchByQa();
		case ByShortlist:
		    return campaignService.listTeamsShortlisted(teamSearchDTO.getCampaignId());
		default:
			throw new NotImplementedException();
		}
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET, value="/team/counter")
	public List<Object> count(@RequestParam("searchstring") String searchstring) throws IOException {
		TeamSearchDTO teamSearchDTO = JSONUtils.toObject(searchstring, TeamSearchDTO.class);
		switch (teamSearchDTO.getClause()) {
		case BySearch:
			return teamService.countByCriteria(teamSearchDTO);
		default:
			throw new NotImplementedException();
		}
	}
	
	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.GET)
	public TeamDTO update(@PathVariable("teamId") String teamId) {
		Team team = teamService.getTeam(teamId);
		return new TeamDTO(team);
	}
	
	@RequestMapping(value="/api/team", method = RequestMethod.POST)
	public TeamDTO create(@Valid @RequestBody TeamDTO teamDTO) {
		return teamService.createTeam(teamDTO);
	}
	
	@RequestMapping(value = "/api/team/{teamId}", method = RequestMethod.PUT)
	public TeamDTO update(@PathVariable("teamId") String teamId, @Valid @RequestBody TeamDTO teamDTO) {
		return teamService.updateTeam(teamId, teamDTO);
	}
	
	@RequestMapping(value = "/api/team/{teamId}/agent/{agentId:.+}", method = RequestMethod.POST)
	public void addAgent(@PathVariable("teamId") String teamId, @PathVariable("agentId") String agentId) {
		teamService.addAgent(teamId, agentId);
	}
	
	@RequestMapping(value = "/api/team/{teamId}/agent/{agentId:.+}", method = RequestMethod.DELETE)
	public void removeAgent(@PathVariable("teamId") String teamId, @PathVariable("agentId") String agentId) {
		teamService.removeAgent(teamId, agentId);
	}
	
	@RequestMapping(value = "/api/team/{teamId}/qa/{qaId:.+}", method = RequestMethod.POST)
	public void addQa(@PathVariable("teamId") String teamId, @PathVariable("qaId") String qaId) {
		teamService.addQa(teamId, qaId);
	}
	
	@RequestMapping(value = "/api/team/{teamId}/qa/{qaId:.+}", method = RequestMethod.DELETE)
	public void removeQa(@PathVariable("teamId") String teamId, @PathVariable("qaId") String qaId) {
		teamService.removeQa(teamId, qaId);
	}
}
