package com.xtaas.mvc.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.Login;
import com.xtaas.service.CRMMappingService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.CRMAttributeDTO;

@RestController
@RequestMapping(value = "/rest/crmmapping")
public class CRMMappingController {
	@Autowired
	private CRMMappingService crmMappingService;

	@RequestMapping(method = RequestMethod.GET)
	public HashMap<String, CRMAttribute> get() {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		return crmMappingService.getCRMAttributeMap(userLogin.getOrganization());
	}

	@RequestMapping(value = "/crmattribute/{campaignId}", method = RequestMethod.GET)
	public HashMap<String, CRMAttribute> getByCampaignId(@PathVariable(value = "campaignId") String campaignId) {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		return crmMappingService.getCRMAttributeMap(userLogin.getOrganization(), campaignId);
	}
	
	@RequestMapping(value = "/crmattribute/system", method = RequestMethod.GET)
	public HashMap<String, CRMAttribute> getSystemCRMAttrubuteMap() {
		return crmMappingService.getSystemCRMAttrubuteMap();
	}

	@RequestMapping(value = "/crmattribute/{campaignId}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> create(@Valid @RequestBody CRMAttributeDTO crmAttributeDTO,
			@PathVariable(value = "campaignId") String campaignId) {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		CRMAttribute crmAttribute = crmAttributeDTO.toCRMAttribute();
		crmAttribute.setCampaignId(campaignId);
		crmMappingService.addCRMAttribute(userLogin.getOrganization(), crmAttribute);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	// @RequestMapping(value = "/crmattribute", method = RequestMethod.POST)
	// public ResponseEntity<Boolean> create(@Valid @RequestBody CRMAttributeDTO
	// crmAttributeDTO) {
	// Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
	// crmMappingService.addCRMAttribute(userLogin.getOrganization(),
	// crmAttributeDTO.toCRMAttribute());
	// return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	// }

	@RequestMapping(value = "/crmattribute/questions", method = RequestMethod.POST)
	public HashMap<String, CRMAttribute> getAllQuestions(@Valid @RequestBody List<CRMAttributeDTO> crmAttributeDTOs) {
		Login userLogin = XtaasUserUtils.getCurrentLoggedInUserObject();
		return crmMappingService.getAllQuestions(userLogin.getOrganization(), crmAttributeDTOs);
	}

}
