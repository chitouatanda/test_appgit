package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.infra.everstring.service.DiscoverAPIService;
import com.xtaas.infra.everstring.service.EnrichmentAPIServiceImpl;

@RestController
@RequestMapping("/everstring")
public class EverStringController {

	@Autowired
	DiscoverAPIService discoverAPIService;

	@Autowired
	EnrichmentAPIServiceImpl enrichmentAPIServiceImpl;

	@RequestMapping(method = RequestMethod.GET, value = "/company/discover/{campaignId}")
	public ResponseEntity<Boolean> companyDiscover(@PathVariable String campaignId) {
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		discoverAPIService.companyDiscover(campaignId);
		return new ResponseEntity<Boolean>(Boolean.TRUE, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/company/multiplerecordsapi/{campaignId}")
	public ResponseEntity<Boolean> callEnrichmentAPIByCampaignIdAndStatus(@PathVariable String campaignId) {
		enrichmentAPIServiceImpl.callEnrichmentAPIByCampaignIdAndStatus(campaignId, true);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/company/multiplerecordsapi")
	public ResponseEntity<Boolean> callEnrichmentAPIByStatus() {
		enrichmentAPIServiceImpl.callEnrichmentAPIByCampaignIdAndStatus("", false);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

}
