package com.xtaas.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.application.service.AuthTokenGeneratorService;
import com.xtaas.application.service.RestCampaignContactService;
import com.xtaas.service.ClientRestService;
import com.xtaas.web.dto.ClientAuthDTO;
import com.xtaas.web.dto.ProspectCallLogDTO;
import com.xtaas.web.dto.RestCampaignContactDTO;
import com.xtaas.web.dto.RestCampaignDTO;
import com.xtaas.web.dto.RestCdpCampaignContactDTO;
import com.xtaas.web.dto.RestProspectResponseDTO;
import com.xtaas.web.dto.RestProspectResponseStatusDTO;

@RestController
public class ClientRestController {

	private static final Logger logger = LoggerFactory.getLogger(ClientRestController.class);

	@Autowired
	private AuthTokenGeneratorService authTokenGeneratorService;

	@Autowired
	private RestCampaignContactService restCampaignContactService;

	@Autowired
	private ClientRestService clientRestService;

	@RequestMapping(value = "/client/token", method = RequestMethod.POST)
	public String update(@Valid @RequestBody ClientAuthDTO clientAuthDTO) {
		logger.debug("Request to generate token.");
		return authTokenGeneratorService.generateClientToken(clientAuthDTO.getUsername().toLowerCase(),
				clientAuthDTO.getPassword());
	}

	@RequestMapping(value = "/api/client/campaign/{sfdcCampaignId}", method = RequestMethod.GET)
	public RestCampaignDTO getCampaign(@PathVariable String sfdcCampaignId) {
		logger.debug("Request to fetch campaign.");
		return clientRestService.getCampaign(sfdcCampaignId);
	}

	@RequestMapping(value = "/api/client/campaign", method = RequestMethod.POST)
	public RestCampaignDTO create(@Valid @RequestBody RestCampaignDTO restCampaignDTO) {
		logger.debug("Request to create campaign.");
		return clientRestService.createCampaign(restCampaignDTO);
	}

	@RequestMapping(value = "/api/client/campaignmanagers", method = RequestMethod.GET)
	public List<String> getCampaignManagerId() {
		logger.debug("Request to fetch campaignmanagerid");
		return clientRestService.getCampaignManagers();
	}

	@RequestMapping(value = "/api/client/campaign", method = RequestMethod.PUT)
	public RestCampaignDTO update(@RequestBody RestCampaignDTO restCampaignDTO) {
		logger.debug("Request to update campaign.");
		return clientRestService.updateCampaign(restCampaignDTO);
	}

	@RequestMapping(value = "/api/client/prospect/{sfdcCampaignId}", method = RequestMethod.POST)
	public RestProspectResponseStatusDTO UploadCampaignContactList(
			@Valid @RequestBody List<RestCampaignContactDTO> campaignContacts, @PathVariable String sfdcCampaignId) {
		logger.debug("UploadCampaignContactList() : Uploading campaign contact records...");
		// return restCampaignContactService.uploadRestData(campaignContacts);
		return restCampaignContactService.uploadData(campaignContacts, sfdcCampaignId);
	}

	@RequestMapping(value = "/api/client/prospect", method = RequestMethod.PUT)
	public ProspectCallLogDTO update(@RequestBody RestCampaignContactDTO restCampaignDto) {
		return restCampaignContactService.updateCampaignAudience(restCampaignDto);
	}
	
	@RequestMapping(value = "/api/client/prospect/internalcampaign", method = RequestMethod.POST)
	public ResponseEntity<String> UploadContactList(
			@Valid @RequestBody RestCdpCampaignContactDTO restCdpCampaignContactDTO) {
		logger.debug("UploadContactList() : Uploading campaign contact records.");
		return restCampaignContactService.uploadInternalData(restCdpCampaignContactDTO);
	}

}
