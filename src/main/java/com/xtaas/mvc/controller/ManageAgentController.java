package com.xtaas.mvc.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.xtaas.service.ManageAgentService;

@RestController
public class ManageAgentController {

	private static final Logger logger = LoggerFactory.getLogger(ManageAgentController.class);

	@Autowired
	private ManageAgentService manageAgentService;

	@RequestMapping(value = "/agent/manage/downloadTemplate", method = RequestMethod.GET)
	public ResponseEntity<?> downloadTemplate(HttpServletRequest request, HttpServletResponse response) throws IOException {
		manageAgentService.downloadTemplate(request, response);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	@RequestMapping(value="/agent/manage/upload", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public @ResponseBody String uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException{
		return manageAgentService.uploadDetails(file, request.getSession());
	}

}



