package com.xtaas.mvc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.GlobalContactService;

@RestController
public class GlobalContactController {

	private static final Logger logger = LoggerFactory.getLogger(GlobalContactController.class);

	@Autowired
	private GlobalContactService globalContactService;

	@RequestMapping(method = RequestMethod.GET, value = "/globalcontact/generate")
	public void generateGlobalContact() {
		logger.debug("Request to generate GlobalContact records.");
		globalContactService.generateGlobalContact();
		logger.debug("Request to generate GlobalContact records.");
	}

}
