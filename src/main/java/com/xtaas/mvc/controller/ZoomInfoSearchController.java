package com.xtaas.mvc.controller;



import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Prospect;
import com.xtaas.service.ZoomInfoSearchService;




//@RestController
public class ZoomInfoSearchController {
	
	  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ZoomInfoSearchController.class);

	
	  @Autowired
	  private ZoomInfoSearchService zoomInfoSearchService;
	  
	  @RequestMapping(value = "/zoominfo/search", method = RequestMethod.POST)
	  public Prospect getZoomProspect(@RequestBody Prospect prospectInfo) {
	    Prospect responseProspect = null;
	    responseProspect = zoomInfoSearchService.getZoomProspect(prospectInfo);
	    return responseProspect;
	  }
	  
	  

}
