package com.xtaas.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.ProspectAttributeService;
import com.xtaas.domain.entity.ProspectAttribute;

@RestController
@RequestMapping(value="/masterdata/prospectattribute")
public class ProspectAttributeController {
	@Autowired
	private ProspectAttributeService prospectAttributeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<ProspectAttribute> get() {
		return prospectAttributeService.getProspectAttributes();
	}
}
