/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.IOException;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.IDNCSearchResult;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.IDNCNumberDTO;
import com.xtaas.web.dto.IDNCSearchDTO;

/**
 * @author djain
 *
 */
@RestController
public class IDNCController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(IDNCController.class);

	@Autowired
	private InternalDNCListService internalDNCListService;

	@Autowired
	private CampaignService campaignService;

	@RequestMapping(value = "/idnc", method = RequestMethod.GET)
	public IDNCSearchResult retrieveAll(@RequestParam("searchstring") String searchstring) throws IOException {
		IDNCSearchDTO idncSearchDTO = JSONUtils.toObject(searchstring, IDNCSearchDTO.class);
		return internalDNCListService.getDNCNumbers(idncSearchDTO);
	}

	@RequestMapping(value = "/idnc/{campaignId}", method = RequestMethod.POST)
	public void create(@Valid @RequestBody IDNCNumberDTO idncNumberDTO, @PathVariable String campaignId) {
		Campaign campaign = campaignService.getCampaign(campaignId);
		idncNumberDTO.setPartnerId(campaign.getOrganizationId());
		idncNumberDTO.setCampaignId(campaignId);
		internalDNCListService.addNumber(idncNumberDTO);
	}

	@RequestMapping(value = "/idnc/bulkupload/{campaignId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadDNCFile(@RequestParam("file") MultipartFile file, @PathVariable String campaignId) throws IOException {
		Campaign campaign = campaignService.getCampaign(campaignId);
		return internalDNCListService.createDNCList(file, campaignId, campaign.getOrganizationId());
	}

}
