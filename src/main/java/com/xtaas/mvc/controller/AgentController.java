package com.xtaas.mvc.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.CallService;
import com.xtaas.application.service.SupervisorService;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.StateCallConfig;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.AgentSortClauses;
//import com.xtaas.infra.aws.service.S3ClientService;
import com.xtaas.infra.aws.service.S3File;
import com.xtaas.mvc.model.CallbackProspectCacheDTO;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.ConfigService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.AgentCampaignDTO;
import com.xtaas.web.dto.AgentCommandDTO;
import com.xtaas.web.dto.AgentCommandOfflineDTO;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.AgentSearchDTO;
import com.xtaas.web.dto.AgentCallMonitoringDTO;
import com.xtaas.web.dto.LoginDTO;
import com.xtaas.web.dto.ProspectCallDTO;

@RestController
public class AgentController {
	private static final Logger logger = LoggerFactory.getLogger(AgentController.class);
	
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private AgentRegistrationService agentRegistrationService;
	
	@Autowired
	private SupervisorService supervisorService;
	
	@Autowired
	private ConfigService configService;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private CallService callService;
	
	//@Autowired
	//private S3ClientService s3clientService;
	
	@RequestMapping(value = "/agent", method = RequestMethod.GET)
	public List<AgentSearchResult> retrieve(@RequestParam("searchstring") String searchstring) throws IOException {
		AgentSearchDTO agentSearchDTO = JSONUtils.toObject(searchstring, AgentSearchDTO.class);
		switch (agentSearchDTO.getClause()) {
		case BySearch:
			return agentService.searchByCriteria(agentSearchDTO, PageRequest.of(agentSearchDTO.getPage(),
					agentSearchDTO.getSize(), agentSearchDTO.getDirection(), agentSearchDTO.getSort().toString()));
		case ByPreviousCampaign:
			return agentService.searchByPreviousCampaigns(agentSearchDTO.getCampaignId());
		case ByAutoSuggest:
			return agentService.searchByAutosuggest(agentSearchDTO.getCampaignId(), PageRequest.of(agentSearchDTO.getPage(), agentSearchDTO
					.getSize(), Sort.by(Direction.ASC, AgentSortClauses.PerHourPrice.getFieldName())));
		case ByCurrentInvitation:
			return agentService.searchByCurrentInvitations(agentSearchDTO.getCampaignId(), PageRequest.of(agentSearchDTO.getPage(), agentSearchDTO
					.getSize(), Sort.by(Direction.ASC, AgentSortClauses.PerHourPrice.getFieldName())));
		case ByCurrentSelection:
			return agentService.searchByCurrentSelection(agentSearchDTO.getCampaignId());
		case BySupervisor:
			return agentService.searchBySupervisor();
		case ByQa:
			try {
				return agentService.searchByQa(agentSearchDTO);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		case ByQa2:		
			try {
				return agentService.searchBySecQa(agentSearchDTO);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		case ByCampaign:
			return agentService.searchByCampaign(agentSearchDTO.getCampaignId());
		default:
			throw new NotImplementedException();
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agent/counter")
	public List<Object> count(@RequestParam("searchstring") String searchstring) throws IOException {
		AgentSearchDTO agentSearchDTO = JSONUtils.toObject(searchstring, AgentSearchDTO.class);
		switch (agentSearchDTO.getClause()) {
		case BySearch:
			return agentService.countByCriteria(agentSearchDTO);
		default:
			throw new NotImplementedException();
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agent/currentcampaign")
	public AgentCampaignDTO getCurrentCampaign(HttpServletRequest httpServletRequest) {
		// String url = httpServletRequest.getRequestURL().toString();
		String url = httpServletRequest.getHeader("referer");
		if (url == null || url.isEmpty()) {
			url = httpServletRequest.getRequestURL().toString();
		}
		AgentCampaignDTO agentCampaignDTO = agentService.getCurrentAllocatedCampaign();
		if (agentCampaignDTO != null && agentCampaignDTO.getHostingServer() != null
				&& url.contains(agentCampaignDTO.getHostingServer())) {
			return agentCampaignDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agent/currentprospect")
	public ResponseEntity<Boolean> getCurrentAllocatedProspect() {
		agentService.getCurrentAllocatedProspect();
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/agent", method = RequestMethod.POST)
	public AgentDTO create(@Valid @RequestBody AgentDTO agentDTO) {
		try {
			return agentService.createAgent(agentDTO);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.agent.username"); 
		}
	}
	
	@RequestMapping(value = "/api/agent/{agentId:.+}", method = RequestMethod.PUT)
	public AgentDTO update(@PathVariable("agentId") String agentId, @Valid @RequestBody AgentDTO agentDTO) {
		return agentService.updateAgent(agentId, agentDTO);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agent/twiliotoken")
	public ResponseEntity<String> getAgentTwilioCapabilityToken() {
		String token = agentService.getTwilioCapabilityToken(XtaasUserUtils.getCurrentLoggedInUsername(), false);
		return new ResponseEntity<String>(token, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agentwhisper/twiliotoken")
	public ResponseEntity<String> getAgentWhisperTwilioCapabilityToken() {
		String token = agentService.getTwilioCapabilityToken(XtaasUserUtils.getCurrentLoggedInUsername(), true);
		return new ResponseEntity<String>(token, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/agent/prospectcall")
	public ResponseEntity<Boolean> submitProspectCall(@RequestBody ProspectCallDTO prospectCallDTO) {
		agentService.submitProspectCall(prospectCallDTO);
		//supervisorService.pushAgentMetricsToSupervisor(prospectCallDTO.getAgentId(), null);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/agent/supervisor/invite/call")
	public ResponseEntity<Boolean> inviteSupervisorToJoinCall() {
		supervisorService.inviteSupervisorToJoinCall(XtaasUserUtils.getCurrentLoggedInUsername());
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/agentqueue/{campaignId}/counter")
	public ResponseEntity<Integer> getAvailableAgentsCount(@PathVariable("campaignId") String campaignId) {
		int availableAgentsCount = agentRegistrationService.getAvailableAgentsCount(campaignId);
		return new ResponseEntity<Integer>(availableAgentsCount, HttpStatus.OK);
	}
	
	/**
	 * Returns the list of campaigns which have agents active
	 * @return list of campaignIds
	 */
	@RequestMapping(method = RequestMethod.GET, value="/api/agentqueue/campaigns")
	@ResponseBody
	public List<String> getActiveAgentsCampaigns() {
		return new ArrayList<String>(agentRegistrationService.getCampaignActiveAgentCount().keySet());
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/agentqueue/active")
	public HashMap<String, Integer> getCampaignActiveAgentCount() {
		return agentRegistrationService.getCampaignActiveAgentCount();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/agentqueue/busy")
	public HashMap<String, Integer> getCampaignBusyAgentCount() {
		return agentRegistrationService.getCampaignBusyAgentCount();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/agent/callbackconfig/{stateCode}")
	@ResponseBody
	public HashMap<String, Object> getCallbackConfig(@PathVariable("stateCode") String stateCode) {
		HashMap<String, Object> callbackConfigMap = new HashMap<String, Object>();
		StateCallConfig stateCallConfig = configService.getStateCallConfig(stateCode);
		callbackConfigMap.put("StateCode", stateCode);
		callbackConfigMap.put("StateTimeZone", stateCallConfig.getTimezone());
		
		DateTime dtCurrDateTimeInStateTZ = DateTime.now(DateTimeZone.forID(stateCallConfig.getTimezone()));
		DateTimeFormatter fmt = DateTimeFormat.forPattern(XtaasDateUtils.DATETIME_12HRFORMAT);
		callbackConfigMap.put("CurrentTimeInStateTZ", dtCurrDateTimeInStateTZ.toString(fmt));
		callbackConfigMap.put("StateHolidays", stateCallConfig.getHolidayDates());
		
		//get team's TZ to which this agent belongs. if team is NULL then take agent's TZ
		Team teamOfAgent = teamRepository.findByAgent(XtaasUserUtils.getCurrentLoggedInUsername());
		String agentTZ = null;
		if (teamOfAgent != null) {
			agentTZ = teamOfAgent.getAddress().getTimeZone();
		} else {
			Agent agent = agentService.getAgent(XtaasUserUtils.getCurrentLoggedInUsername());
			agentTZ = agent.getAddress().getTimeZone();
		}
		
		//Calculating work hour range
		DateTime currentDateInAgentTz = new DateTime(DateTimeZone.forID(agentTZ));
		//ToDo: For now assuming agents work from 9 AM to 5 PM, this does not handle the scenario when agents are working in Night to match the state's TZ (Ex: If agents working in India in night), then this will not work
		int agentWorkStartHrInAgentTz = 9; int agentWorkEndHrInAgentTz = 17;
		DateTime agentWorkStartTime = new DateTime(currentDateInAgentTz.getYear(), currentDateInAgentTz.getMonthOfYear(), currentDateInAgentTz.getDayOfMonth(), agentWorkStartHrInAgentTz, 0, DateTimeZone.forID(agentTZ));
		DateTime agentWorkEndTime = new DateTime(currentDateInAgentTz.getYear(), currentDateInAgentTz.getMonthOfYear(), currentDateInAgentTz.getDayOfMonth(), agentWorkEndHrInAgentTz, 0, DateTimeZone.forID(agentTZ));
		DateTime agentWorkStartTimeInStateTz = agentWorkStartTime.withZone(DateTimeZone.forID(stateCallConfig.getTimezone()));
		DateTime agentWorkEndTimeInStateTz = agentWorkEndTime.withZone(DateTimeZone.forID(stateCallConfig.getTimezone()));
		
		int agentWorkStartHr = agentWorkStartHrInAgentTz;
		int agentWorkEndHr = agentWorkEndHrInAgentTz;
		int agentWorkStartHrInStateTz = agentWorkStartTimeInStateTz.getHourOfDay();
		if (agentWorkStartHrInStateTz > agentWorkStartHrInAgentTz) { //if StateTZ (Ex: NY-12PM) is ahead of AgentsTZ (Ex: CA-9AM) then show start work hr from State TZ (12PM to 5PM) 
			agentWorkStartHr = agentWorkStartHrInStateTz; 
		}
		
		int agentWorkEndHrInStateTz = agentWorkEndTimeInStateTz.getHourOfDay();
		if (agentWorkEndHrInStateTz < agentWorkEndHrInAgentTz) { //if StateTZ (Ex: CA-2PM) is behind AgentsTZ (Ex: NY-5PM) then show end hr till StateTZ as agents are available till that time
			agentWorkEndHr = agentWorkEndHrInStateTz;
		}	
		
		callbackConfigMap.put("AgentWorkStartHrInStateTZ", agentWorkStartHr);
		callbackConfigMap.put("AgentWorkEndHrInStateTZ", agentWorkEndHr);
		return callbackConfigMap;
	}
	
/*	@RequestMapping(value = "/agent/training/docs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<S3File> getTrainingDocs(@RequestParam("folder") String filePath) {
		String bucketName = "xtaas-docs";
		/*String filePath = "training/agent/";
		
		if (subfolder != null && !subfolder.isEmpty()) {
			filePath = filePath + subfolder;
		}*/
		
		/*List<S3File> files = s3clientService.getTopLevelFiles(bucketName, filePath);
		for (S3File file : files) {
			String fileUrl = "";
			try {
				if (file.getFileSize() > 0) { //its a file
					fileUrl = "/agent/training/docs/download?filepath=";
					file.setUrl(fileUrl+URLEncoder.encode(file.getFilepath(), XtaasConstants.UTF8));
				} else {
					fileUrl = "/agent/training/docs?folder=";
					file.setUrl(fileUrl+URLEncoder.encode(file.getFilepath(), XtaasConstants.UTF8));
				}
			} catch (UnsupportedEncodingException e) {
				logger.error("getTrainingDocs() : Error occurred in Url encoding. Setting the file url without encoding", e);
				file.setUrl(fileUrl+file.getFilepath());
			}
		}
		return files;
	}*/
	
	/*@RequestMapping(value = "/agent/training/docs/download", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public UrlResource downloadTrainingDoc(@RequestParam("filepath") String filePath, HttpServletRequest request, HttpServletResponse response) throws MalformedURLException {
		String bucketName = "xtaas-docs";
		String[] splittedFilePath = StringUtils.split(filePath, "/");
		String filename = splittedFilePath[splittedFilePath.length-1];
		
		String signedUrl = s3clientService.getSignedUrl(bucketName, filePath);
		response.setHeader("Content-Disposition", "attachment; filename="+filename);
		return new UrlResource(signedUrl); 
	}*/

	@RequestMapping(method = RequestMethod.GET, value = "/agent/allocate/{callsid}/{dialerMode}/{agentType}")
	public ResponseEntity<Boolean> allocateAgentFromUI(@PathVariable String callsid, @PathVariable String dialerMode,
			@PathVariable String agentType) {
		String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
		agentRegistrationService.allocateAgentFromUI(callsid, agentId, dialerMode, agentType);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/agent/allocate/dialer/{callsid}/{callAbandon}")
	public ResponseEntity<Boolean> allocateAgentFromDialer(@PathVariable String callsid, @PathVariable boolean callAbandon) {
		agentService.allocateAgentFromDialer(callsid, callAbandon);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/agent/details", method = RequestMethod.GET)
	@ResponseBody
	public AgentDTO getAgentDetails() {
		return new AgentDTO(agentService.getAgent(XtaasUserUtils.getCurrentLoggedInUsername()));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/agentMode/{agentId}/{agentMode}")
	public ResponseEntity<Boolean> changeAgentMode(@PathVariable String agentId, @PathVariable String agentMode) {
		agentService.updateAgentMode(agentId, agentMode);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/agent/offline", method = RequestMethod.POST)
	public void offlineCommandRequest(@RequestBody AgentCommandOfflineDTO commandRequestObj, HttpSession session) {
		logger.info("Browser OFFLINE status Agent");
		//kick participant from call
		if (commandRequestObj.getCallSid() != null && !commandRequestObj.getCallSid().isEmpty()) {
			String callSid = commandRequestObj.getCallSid();
			String agentId = XtaasUserUtils.getCurrentLoggedInUsername();
			ProspectCall prospectCall = agentRegistrationService.getProspectByCallSid(callSid);
			if (prospectCall != null) {
				agentRegistrationService.unsetProspectByCallSid(callSid);
			}
			callService.setCallAsComplete(agentId, callSid, commandRequestObj.isUsePlivo(), commandRequestObj.isConferenceCall());
		}
		
		// // update browser closed command
		agentService.changeStatusOnBrowserClosed(commandRequestObj.getCampaignId(), commandRequestObj.getAgentType(), session);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/agent/callmonitoring")
	public ResponseEntity<Boolean> agentCallLog(@RequestBody AgentCallMonitoringDTO agentCallMonitoringDTO) {
		agentService.agentCallMonitoring(agentCallMonitoringDTO);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/callback/{agentId}")
	public CallbackProspectCacheDTO getCallback(@PathVariable String agentId) {
		return agentService.getMissedCallbackProspect(agentId);
	}

}
