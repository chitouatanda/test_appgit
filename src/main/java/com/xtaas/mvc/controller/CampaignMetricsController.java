package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignMetricsService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.CampaignMetricsDTO;
import com.xtaas.web.dto.CampaignMetricsSearchDTO;

@RestController
@RequestMapping(value = "/campaign/metrics")
public class CampaignMetricsController {
	
	@Autowired
	private CampaignMetricsService campaignMetricsService;
	
	@RequestMapping(method = RequestMethod.GET)
	public CampaignMetricsDTO get(@RequestParam("searchstring") String searchstring) throws IOException, ParseException {
		CampaignMetricsSearchDTO campaignMetricsSearchDTO = JSONUtils.toObject(searchstring, CampaignMetricsSearchDTO.class);
		
		switch (campaignMetricsSearchDTO.getClause()) {
			case ByCampaign:
				if (campaignMetricsSearchDTO.getTimeZone() == null || campaignMetricsSearchDTO.getTimeZone().isEmpty()) {
					throw new IllegalArgumentException("Timezone is mandatory");
				}
				return campaignMetricsService.getCampaignMetricsByCampaign(campaignMetricsSearchDTO.getCampaignId(), campaignMetricsSearchDTO.getFromDate(), 
						campaignMetricsSearchDTO.getToDate(), campaignMetricsSearchDTO.getTimeZone(), campaignMetricsSearchDTO.getNoOfDays());
			default:
				throw new NotImplementedException("Invalid Search Clause specified: " + campaignMetricsSearchDTO.getClause());
		}
	}
	
	// This method will give total number of bounce mails per campaign
    @RequestMapping(value = "/emailbounces/count", method = RequestMethod.GET)
    public HashMap<String, Integer> getEmailBouncesCount(@RequestParam("searchstring") String searchstring) throws IOException,ParseException {
        CampaignMetricsSearchDTO campaignMetricsSearchDTO = JSONUtils.toObject(searchstring, CampaignMetricsSearchDTO.class);
        return campaignMetricsService.getEmailBounceCount(campaignMetricsSearchDTO.getCampaignId(),campaignMetricsSearchDTO.getFromDate(),
                campaignMetricsSearchDTO.getToDate(), campaignMetricsSearchDTO.getTimeZone(), campaignMetricsSearchDTO.getNoOfDays());
    }
}
