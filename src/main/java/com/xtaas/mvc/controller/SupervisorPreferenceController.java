package com.xtaas.mvc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.SupervisorPreference;
import com.xtaas.domain.valueobject.PreferenceId;
import com.xtaas.service.SupervisorPreferenceService;
import com.xtaas.service.UserService;

@RestController
@RequestMapping(value="/preference")
public class SupervisorPreferenceController {
	
	@Autowired
	private SupervisorPreferenceService supervisorPreferenceService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/{preferenceId}", method = RequestMethod.GET)
	public SupervisorPreference retrieve(@PathVariable("preferenceId") PreferenceId preferenceId) {
			return supervisorPreferenceService.getPreference(preferenceId);
		
	}
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Boolean> update(@Valid @RequestBody SupervisorPreference supervisorPreference) {
		
		try {
			supervisorPreferenceService.updateSupervisorPreference(supervisorPreference);
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.supervisor.supervisorPreference"); 
		}
	}

	
}
