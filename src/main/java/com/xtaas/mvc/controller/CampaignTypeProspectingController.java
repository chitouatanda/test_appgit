package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.CampaignTypeProspectingService;
import com.xtaas.web.dto.ProspectCallDTO;

@RestController
public class CampaignTypeProspectingController {

	@Autowired
	private CampaignTypeProspectingService campaignTypeProspectingService;

	@RequestMapping(method = RequestMethod.POST, value = "/prospectcalllog/prospecting/create")
	public ProspectCallDTO createProspect(@RequestBody ProspectCallDTO prospectCallDTO) {
		return campaignTypeProspectingService.createProspectingLead(prospectCallDTO);
	}
}
