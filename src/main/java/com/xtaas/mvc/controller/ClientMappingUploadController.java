package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.NewClientMapping;
import com.xtaas.service.ClientMappingUploadService;
import com.xtaas.service.NewClientMappingUploadService;
import com.xtaas.web.dto.CampaignDTO;

@RestController
public class ClientMappingUploadController {

	@Autowired
	private ClientMappingUploadService clientMappingUploadService;
	
	@Autowired
	private NewClientMappingUploadService newClientMappingUploadService;

	@RequestMapping(value = "/clientMapping/downloadTemplate/{campaignId}", method = RequestMethod.GET)
	public ResponseEntity<?> downloadTemplate(@PathVariable("campaignId") String campaignId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		clientMappingUploadService.downloadTemplate(campaignId, request, response);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/clientMapping/upload/{campaignId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadClientMappingFile(@PathVariable("campaignId") String campaignId,
			@RequestParam("file") MultipartFile file) throws IOException {
		return clientMappingUploadService.createClientMappingEntries(campaignId, file);
	}

	@RequestMapping(value = "/clientMapping/download/new/{campaignId}", method = RequestMethod.GET)
	public ResponseEntity<?> downloadNewTemplate(@PathVariable("campaignId") String campaignId,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		newClientMappingUploadService.downloadNewTemplate(campaignId, request, response);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/clientMapping/upload/new/{campaignId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadNewClientMappingFile(@PathVariable("campaignId") String campaignId,
			@RequestParam("file") MultipartFile file) throws IOException {
		return newClientMappingUploadService.createNewClientMappings(campaignId, file);
	}
	
	@RequestMapping(value = "/clientMapping/{campaignId}", method = RequestMethod.GET)
	public List<NewClientMapping> getMappings(@PathVariable("campaignId") String campaignId) {
		return newClientMappingUploadService.getNewCampaignClientMappings(campaignId);
	}

}
