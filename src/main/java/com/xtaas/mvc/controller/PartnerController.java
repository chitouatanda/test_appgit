package com.xtaas.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Partner;
import com.xtaas.service.PartnerService;
import com.xtaas.web.dto.PartnerDTO;

@RestController
public class PartnerController {
	
	@Autowired
	private PartnerService partnerService;
	
	@RequestMapping(value = "/api/partner", method = RequestMethod.POST)
	public Partner create(@Valid @RequestBody PartnerDTO partnerDTO) {
		try {
			return partnerService.createPartner(partnerDTO);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.partner.partnerName"); 
		}
	}
	
	@RequestMapping(value = "/partner/all", method = RequestMethod.GET)
	public List<String> retrive() {
		return partnerService.retriveAll();
	}
	
	@RequestMapping(value = "/api/partner/{partnerId:.+}", method = RequestMethod.PUT)
	public PartnerDTO update(@PathVariable("partnerId") String partnerId, @Valid @RequestBody PartnerDTO partnerDTO) {
		return partnerService.updatePartner(partnerId, partnerDTO);
	}
}




