package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.AgentService;
import com.xtaas.domain.service.AgentCalendarService;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.domain.valueobject.WorkSlot;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.AgentScheduleSearchDTO;

@RestController
@RequestMapping(value = "/agent/{agentId}/schedule")
public class AgentScheduleController {
	@Autowired
	private AgentCalendarService agentCalendarService;
	
	@Autowired
	private AgentService agentService;
	
	@RequestMapping(method = RequestMethod.POST)
	public void create(@PathVariable("agentId") String agentId, @RequestBody WorkSchedule workSchedule) {
		agentService.addWorkSchedule(agentId, workSchedule);
	}
	
	@RequestMapping(value="/{scheduleId}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("agentId") String agentId, @PathVariable("scheduleId") String scheduleId) {
		agentService.deleteWorkSchedule(agentId, scheduleId);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public Map<LocalDate, TreeSet<WorkSlot>> getSchedule(@PathVariable("agentId") String agentId, @RequestParam("searchstring") String searchstring) throws IOException {
		AgentScheduleSearchDTO agentScheduleSearchDTO = JSONUtils.toObject(searchstring, AgentScheduleSearchDTO.class);
		LocalDate startDate = LocalDate.fromDateFields(agentScheduleSearchDTO.getStartDate());
		LocalDate endDate = LocalDate.fromDateFields(agentScheduleSearchDTO.getEndDate());
		return agentCalendarService.getCalendar(startDate, endDate, agentService.getAgent(agentId));
	}
}
