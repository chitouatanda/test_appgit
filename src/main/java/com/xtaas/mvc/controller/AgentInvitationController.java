package com.xtaas.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.web.dto.AgentInvitationDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/agentinvitation")
public class AgentInvitationController {
	
	@Autowired
	private CampaignService campaignService;

	@RequestMapping(method = RequestMethod.POST)
	public void invite(@PathVariable("campaignId") String campaignId,
			@RequestBody @Valid AgentInvitationDTO agentInvitation) {
		campaignService.inviteAgent(campaignId, agentInvitation);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<AgentInvitationDTO> list(@PathVariable("campaignId") String campaignId) {
        return campaignService.listAgentInvitations(campaignId);
	}
}
