/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.IOException;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.InternalDNCListService;
import com.xtaas.web.dto.DemandShoreDNCDTO;


/**
 * @author hbaba
 *
 */
@RestController
public class DNCController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DNCController.class);

	@Autowired
	private InternalDNCListService internalDNCListService;

/*

	@RequestMapping(value = "/dnc", method = RequestMethod.POST)
	public @ResponseBody String create(@Valid @RequestBody DemandShoreDNCDTO dsDNCNumberDTO) {
		internalDNCListService.addNumber(dsDNCNumberDTO);
	}*/

	@RequestMapping(value = "/dnc", method = RequestMethod.POST)
	
	public @ResponseBody String uploadCampaignSupressionFile(@Valid @RequestBody DemandShoreDNCDTO dsDNCNumberDTO) throws IOException {
		return internalDNCListService.addNumber(dsDNCNumberDTO);
	}

}
