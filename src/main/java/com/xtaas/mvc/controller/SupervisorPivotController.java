package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.service.SupervisorPivotService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.PivotStatusDTO;
import com.xtaas.web.dto.SupervisorPivotDTO;

@RestController
public class SupervisorPivotController {

	@Autowired
	private SupervisorPivotService supervisorPivotService;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;

	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
	
	@RequestMapping(value = { "/supervisorpivot/{campaignId}/{pivotSelectedTab}" }, method = RequestMethod.GET)
	public SupervisorPivotDTO retrieve(@PathVariable("campaignId") String campaignId, @PathVariable String pivotSelectedTab) {
		return supervisorPivotService.getSupervisorPivotsForUI(campaignId, false, pivotSelectedTab);
	}

	@RequestMapping(value = { "/supervisorpivot/{campaignId}/download" }, method = RequestMethod.GET)
	public ResponseEntity<?> download(@PathVariable("campaignId") String campaignId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		SupervisorStats dbStats = supervisorStatsRepository.findByCampaignIdTypeAndStatusList("PivotStatus",
				campaignId, Arrays.asList("PIVOT_FILE_DOWNLOAD", "PIVOT_FILE_DOWNLOAD_UPLOADED"));
		if(dbStats == null) {
			SupervisorStats stats = new SupervisorStats();
			stats.setCampaignId(campaignId);
			stats.setStatus("PIVOT_FILE_DOWNLOAD");
			stats.setType("PivotStatus");
			supervisorStatsRepository.save(stats);
		}
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"PIVOT_DOWNLOAD", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("PIVOT_DOWNLOAD");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("download process is in prgoress, please try later.");
		}
		supervisorPivotService.downloadPivots(campaignId, request, response, true);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = {
			"/supervisorpivot/{campaignId}/upload" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public ResponseEntity<Boolean> nukeCallableProspect(@PathVariable("campaignId") String campaignId,
			@RequestParam("file") MultipartFile file) throws IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		SupervisorStats stats = supervisorStatsRepository.findByCampaignIdTypeAndStatus("PivotStatus",campaignId,"PIVOT_FILE_DOWNLOAD");
		if(stats != null) {
			stats.setStatus("PIVOT_FILE_DOWNLOAD_UPLOADED");
			supervisorStatsRepository.save(stats);
		}
		
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"PIVOT_UPLOAD", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("PIVOT_UPLOAD");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("An upload process is in prgoress, please try later.");
		}
		
		supervisorPivotService.nukeCallableProspect(file, campaignId);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	@RequestMapping(value = { "/supervisorpivot/mail/{campaignId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> sendPivotStats(@PathVariable("campaignId") String campaignId) throws IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"PIVOT_DOWNLOAD", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("PIVOT_DOWNLOAD");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("download process is in prgoress, please try later.");
		}
		supervisorPivotService.sendPivotStats(campaignId, login);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
	@RequestMapping(value = { "/supervisorpivot/status/{campaignId}" }, method = RequestMethod.GET)
	public PivotStatusDTO checkPivotStatus(@PathVariable("campaignId") String campaignId) {
		PivotStatusDTO dto = new PivotStatusDTO();
		int count = prospectCallLogRepository.getCallableCount(campaignId);
		if (count > 0) {
			dto.setActivateStatus(true);
			SupervisorStats stats = supervisorStatsRepository.findByCampaignIdTypeAndStatusList("PivotStatus",
					campaignId, Arrays.asList("PIVOT_FILE_DOWNLOAD", "PIVOT_FILE_DOWNLOAD_UPLOADED"));
			if (stats != null) {
				if (stats.getStatus().equals("PIVOT_FILE_DOWNLOAD")) {
					dto.setDownloadStatus(true);
				}
				if (stats.getStatus().equals("PIVOT_FILE_DOWNLOAD_UPLOADED")) {
					dto.setUploadStatus(true);
					dto.setDownloadStatus(true);
				}
			}else {
				dto.setUploadStatus(true);
			}
		} 
		return dto;
	}

}
