package com.xtaas.mvc.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.TwilioCallLogAndDataBuyService;
import com.xtaas.service.TwilioService;
import com.xtaas.web.dto.TwilioCallLogFilterDTO;
import com.xtaas.web.dto.TwilioCallLogFilterRequestDTO;
import com.xtaas.web.dto.TwilioNumberBuyDTO;

@RestController
public class TwilioController {

	private final Logger logger = LoggerFactory.getLogger(TwilioController.class);

	@Autowired
	TwilioCallLogAndDataBuyService twilioCallLogAndDataBuyService;

	@Autowired
	private TwilioService twilioService;

	@RequestMapping(method = RequestMethod.POST, value = "/twilio/voice", produces = "application/xml")
	public ResponseEntity<String> handleVoice(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to handle twilio voice call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.handleVoiceCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/twilio/dial/wait", produces = "application/xml")
	public ResponseEntity<String> handleConferenceWait(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to handle conference wait.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.handleConferenceWait(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/twilio/dial/prospect", produces = "application/xml")
	public ResponseEntity<String> handleDialedProspect(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to handle dialed prospects call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.handleProspectCallPreviewMode(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/twilio/call/status", produces = "application/xml")
	public ResponseEntity<String> handleCallStatus(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to disconnect prospect call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.handleCallStatus(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/twilio/call/hold", method = RequestMethod.POST)
	public ResponseEntity<String> holdCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to hold twilio call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.holdCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/twilio/call/unhold", method = RequestMethod.POST)
	public ResponseEntity<String> unHoldCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Request to unhold twilio.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = twilioService.unHoldCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/twilio/dial/{prospectCallId}")
	public String dialProspectPhone(@PathVariable("prospectCallId") String prospectCallId) {
		logger.debug("Request to dial prospects phone.");
		return twilioService.dialProspectPhone(prospectCallId);
	}

	@RequestMapping(value = "/twilio/calllog", method = RequestMethod.POST)
	public ResponseEntity<Boolean> generateCallLog(@RequestBody TwilioCallLogFilterDTO twilioCallLogFilterDTO) {
		logger.debug("Request to download twilio call log.");
		twilioCallLogAndDataBuyService.generateCallLog(twilioCallLogFilterDTO, true);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/twilio/purchase/number", method = RequestMethod.POST)
	public ResponseEntity<Boolean> purchaseNumbers(@RequestBody TwilioNumberBuyDTO twilioNumberBuyDTO) {
		logger.debug("Request to purchase twilio phone numbers.");
		twilioCallLogAndDataBuyService.purchaseNumbers(twilioNumberBuyDTO);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/twilio/taglead", method = RequestMethod.POST)
	public String tagLeadForNumber(@RequestBody TwilioCallLogFilterRequestDTO twilioCallLogFilterDTO) {
		logger.debug("Request to tag lead.");
		return twilioService.tagLeadForNumber(twilioCallLogFilterDTO);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/twilio/call/stats")
	public Map<String, Integer> getCallStats(@RequestBody TwilioCallLogFilterDTO twilioCallLogFilterDTO) {
		return twilioCallLogAndDataBuyService.getCallStats(twilioCallLogFilterDTO);
	}

}
