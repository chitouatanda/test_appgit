package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.AgentMetricsService;
import com.xtaas.application.service.AgentMetricsServiceImpl.AgentMetrics;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.AgentMetricsDTO;
import com.xtaas.web.dto.AgentMetricsSearchDTO;

@RestController
@RequestMapping(value = "/agent/metrics")
public class AgentMetricsController {
	@Autowired
	private AgentMetricsService agentMetricsService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<AgentMetricsDTO> get(@RequestParam("searchstring") String searchstring) throws IOException {
		AgentMetricsSearchDTO agentMetricsSearchDTO = JSONUtils.toObject(searchstring, AgentMetricsSearchDTO.class);
		switch (agentMetricsSearchDTO.getClause()) {
			case BySupervisor:
				return agentMetricsService.getAgentMetricsBySupervisor(agentMetricsSearchDTO.getTimeZone());
			case BySupervisorLiveQueue:
				return agentMetricsService.getAgentMetricsBySupervisorLiveQueue();
			case ByCampaign: //TODO : Remove this part, as used in old supervisor screen (campaign detail section)
				HashMap<AgentMetrics, String> campaignMetrics = new HashMap<AgentMetrics, String>();
				campaignMetrics.put(AgentMetrics.AVG_TALK_TIME, null);
				campaignMetrics.put(AgentMetrics.CONTACT_RATE, null);
				campaignMetrics.put(AgentMetrics.LEADS_DELIVERED, null);
				campaignMetrics.put(AgentMetrics.SUCCESS_RATE, null);
				campaignMetrics.put(AgentMetrics.LAST_LOGIN_TIME, null);
				campaignMetrics.put(AgentMetrics.AWAY_DURATION, null);
				campaignMetrics.put(AgentMetrics.GEARING_DURATION, null);
				campaignMetrics.put(AgentMetrics.IDLE_DURATION, null);
				campaignMetrics.put(AgentMetrics.OFFLINE_DURATION, null);
				campaignMetrics.put(AgentMetrics.ONBREAK_DURATION, null);
				campaignMetrics.put(AgentMetrics.ONLINE_DURATION, null);
				return agentMetricsService.getAgentMetricsByCampaign(agentMetricsSearchDTO.getCampaignId(), agentMetricsSearchDTO.getFromDate(), agentMetricsSearchDTO.getToDate(), campaignMetrics);
			default:
				throw new NotImplementedException("Invalid Search Clause specified: " + agentMetricsSearchDTO.getClause());
		}
	}
	
}
