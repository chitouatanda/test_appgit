package com.xtaas.mvc.controller;

import com.xtaas.web.dto.RefreshCampaignsCacheDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.CountryNew;
import com.xtaas.service.CacheRefreshService;
import com.xtaas.web.dto.CampaignSettingDTO;
import com.xtaas.web.dto.RefreshMultiCampaignCacheDTO;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CacheRefreshController {

	private static final Logger logger = LoggerFactory.getLogger(CacheRefreshController.class);

	@Autowired
	private CacheRefreshService cacheRefreshService;

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/campaign/{campaignId}")
	public ResponseEntity<Boolean> updateProspectCall(@PathVariable("campaignId") String campaignid) {
		logger.debug("Request to refresh [{}] campaign cache.", campaignid);
		cacheRefreshService.refreshCache(campaignid);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/activecampaigns")
	public ResponseEntity<Boolean> updateAllProspectCall() {
		logger.debug("Request to refresh all active campaigns cache.");
		cacheRefreshService.refreshAllCache();
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/applicationproperty")
	public ResponseEntity<Boolean> loadApplicationPropertyCacheForcefully() {
		logger.debug("Request to refresh application property cache.");
		Boolean retVal = cacheRefreshService.loadApplicationPropertyCacheForcefully();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/twilio/outboundnumbers")
	public ResponseEntity<Boolean> refreshTwilioOutboundNumbers() {
		logger.debug("Request to refresh Twilio outbound numbers cache.");
		Boolean retVal = cacheRefreshService.refreshTwilioOutboundNumbers();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/plivo/outboundnumbers")
	public ResponseEntity<Boolean> refreshPlivoOutboundNumbers() {
		logger.debug("Request to refresh Plivo outbound numbers cache.");
		Boolean retVal = cacheRefreshService.refreshPlivoOutboundNumbers();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/telnyx/outboundnumbers")
	public ResponseEntity<Boolean> refreshTelnyxOutboundNumbers() {
		logger.debug("Request to refresh Telnyx outbound numbers cache.");
		Boolean retVal = cacheRefreshService.refreshTelnyxOutboundNumbers();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/signalwire/outboundnumbers")
	public ResponseEntity<Boolean> refreshSignalwireOutboundNumbers() {
		logger.info("Request to refresh Signalwire outbound numbers cache.");
		Boolean retVal = cacheRefreshService.refreshSignalwireOutboundNumbers();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/tencompanies/{campaignId}")
	public ResponseEntity<Boolean> updateTenCompanies(@PathVariable("campaignId") String campaignid) {
		Boolean retVal = cacheRefreshService.updateTenCompanies(campaignid);
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/resetcampaign/campaignid/{campaignId}")
	public ResponseEntity<String> resetSupervisorOverrideCampaignId(@PathVariable("campaignId") String campaignid) {
		logger.debug("Request to reset agents campaign by campaignId.");
		String retVal = cacheRefreshService.resetSupervisorOverrideCampaignId(campaignid);
		return new ResponseEntity<String>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/resetcampaign/supervisorid/{supervisorId}")
	public ResponseEntity<String> resetSupervisorOverrideCampaignIdBySupervisorId(
			@PathVariable("supervisorId") String supervisorId) {
		logger.debug("Request to reset agents campaign by supervisorId.");
		String retVal = cacheRefreshService.resetSupervisorOverrideCampaignIdBySupervisorId(supervisorId);
		return new ResponseEntity<String>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/resetagentcampaign/{campaignId}")
	public ResponseEntity<Boolean> resetAgentCampaignByCampaignId(@PathVariable("campaignId") String campaignid) {
		Boolean retVal = cacheRefreshService.resetAgentCampaignByCampaignId(campaignid);
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/cacherefresh/multi/resetagentcampaign")
	public ResponseEntity<Boolean> resetAgentCampaign(
			@RequestBody RefreshMultiCampaignCacheDTO refreshMultiCampaignCacheDTO) {
		if (refreshMultiCampaignCacheDTO.getCampaignIds() == null
				|| refreshMultiCampaignCacheDTO.getCampaignIds().size() == 0) {
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		Boolean retVal = cacheRefreshService
				.resetAgentCampaignForMultipleCampaigns(refreshMultiCampaignCacheDTO.getCampaignIds());
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/organization")
	public ResponseEntity<Boolean> refreshOrg() {
		logger.debug("Request to refresh organizations cache.");
		Boolean retVal = cacheRefreshService.refreshOrganizations();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/agents")
	public ResponseEntity<Boolean> refreshAgent() {
		logger.debug("Request to refresh agents cache.");
		Boolean retVal = cacheRefreshService.refreshAgents();
		return new ResponseEntity<Boolean>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/campaign/count/{email:.+}")
	public ResponseEntity<String> refreshCampaign(@PathVariable("email") String email) {
		logger.debug("CacheRefeshController() :: Request to refresh campaign cache.");
		cacheRefreshService.getCountOfProspectsLoadedInMemory(email);
		return new ResponseEntity<String>("You will receive email in sometime.", HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/statecode")
	public ResponseEntity<Boolean> refreshStateCodeCache() {
		logger.debug("CacheRefeshController() :: Request to refresh state code cache.");
		Boolean val = cacheRefreshService.refreshStateCodeCache();
		return new ResponseEntity<Boolean>(val, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/cacherefresh/campaigns")
	public ResponseEntity<String> refreshCampaignIdsCache(
			@RequestBody RefreshCampaignsCacheDTO refreshCampaignsCacheDTO) {
		String environment = refreshCampaignsCacheDTO.getEnvironment();
		List<String> campaignIds = refreshCampaignsCacheDTO.getCampaignIds();
		logger.debug("CacheRefeshController() :: Request to refresh cache for campaignIds [{}] on [{}] environment.", campaignIds, environment);
		if (CollectionUtils.isEmpty(campaignIds) || StringUtils.isEmpty(environment)) {
			return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
		}
		String retVal = cacheRefreshService.refreshCacheForCampaignIds(campaignIds,environment);
		return new ResponseEntity<String>(retVal, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cacherefresh/databuy/isdcodes")
	public ResponseEntity<String> hardCacheRefreshCountryIsdCOdeMapForDatabuy () {
		logger.debug("CacheRefeshController() :: Request to refresh cache for Databuy isdcodes map");
		cacheRefreshService.hardCacheRefreshCountryIsdCOdeMapForDatabuy();
		return new ResponseEntity<String>("Success", HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cacherefresh/inactivecampaigns")
	public ResponseEntity<List<String>> removeProspectCacheInactiveCampaigns() {
		return new ResponseEntity<List<String>>(cacheRefreshService.removeProspectCacheInactiveCampaigns(),
				HttpStatus.OK);
	}

}