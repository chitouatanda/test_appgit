package com.xtaas.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.domain.valueobject.TeamSearchResult;
import com.xtaas.web.dto.TeamInvitationDTO;
import com.xtaas.web.dto.TeamSearchResultDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/teaminvitation")
public class TeamInvitationController {
	@Autowired 
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.POST)
	public void invite(@PathVariable("campaignId") String campaignId, @RequestBody List<String> teamIds) {
        campaignService.inviteTeam(campaignId, teamIds);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TeamSearchResultDTO> list(@PathVariable("campaignId") String campaignId) {
        return campaignService.listTeamInvitations(campaignId);
	}
	
	@RequestMapping(value = "/{teamId}", method = RequestMethod.DELETE)
	public void removeInvite(@PathVariable("campaignId") String campaignId, @PathVariable("teamId") String teamId) {
        campaignService.removeInviteTeam(campaignId, teamId);
	}

}
