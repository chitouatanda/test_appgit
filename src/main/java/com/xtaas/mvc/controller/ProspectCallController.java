package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.ProspectCallService;
import com.xtaas.web.dto.ProspectDTO;

@RestController
public class ProspectCallController {
    
    @Autowired
    private ProspectCallService prospectCallService;
    
    /* START UPDATE PROSPECT DETAILS
     * DATE : 26/05/2017
     * REASON : Below method is used to update only Prospect/Lead details from QA detail page */
    @RequestMapping(method = RequestMethod.PUT, value="/prospect/{prospectCallId}")
    public ResponseEntity<Boolean> updateProspectCall(@RequestBody ProspectDTO prospectDTO) {
        prospectCallService.updateProspectCall(prospectDTO);
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }
    /* END UPDATE PROSPECT DETAILS */
    
    @RequestMapping(method = RequestMethod.POST, value="/prospect/update/{prospectCallId}/{leadStatus}")
    public ResponseEntity<Boolean> updateProspectCallStatus(@PathVariable("prospectCallId") String prospectCallId, @PathVariable("leadStatus") String leadStatus) {
        prospectCallService.updateProspectStatus(prospectCallId,leadStatus);
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }
}
