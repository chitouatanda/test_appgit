/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.ProspectCallLogService;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.ProspectCallCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.ProspectSearchDTO;
import com.xtaas.web.dto.QueuedProspectDTO;

/**
 * @author djain
 *
 */
@RestController
public class ProspectController {
	
	@Autowired
	private ProspectCallLogService prospectLogService;
	
	@RequestMapping(value = "/api/prospect", method = RequestMethod.GET)
	public QueuedProspectDTO retrieve(@RequestParam("searchstring") String searchstring) throws IOException {
		ProspectSearchDTO prospectSearchDTO = JSONUtils.toObject(searchstring, ProspectSearchDTO.class);
		// List<ProspectCallDTO> prospectCallList = new ArrayList<ProspectCallDTO>(0);
		QueuedProspectDTO queuedProspectDTO = new QueuedProspectDTO(new ArrayList<ProspectCallCacheDTO>(0), 0);
		switch (prospectSearchDTO.getClause()) {
			case ByCampaign :
			queuedProspectDTO = prospectLogService.getQueuedProspects(prospectSearchDTO.getCampaignId(),
					prospectSearchDTO.getQueuedCallCount());
				break;
		}
		return queuedProspectDTO;
	}
	
	@RequestMapping(value = "/prospect/{campaignid}/callable/count", method = RequestMethod.GET)
	public HashMap<String, Integer> getCallableProspectCount(@PathVariable("campaignid") String campaignId) throws IOException {
		return prospectLogService.getCallableRecordsCount(campaignId);
	}
	
	@RequestMapping(value = "/cachecount/{campaignid}", method = RequestMethod.GET)
	public int getProspectCount(@PathVariable("campaignid") String campaignId) throws IOException {
		return prospectLogService.getProspectCountFromCache(campaignId);
	}
}
