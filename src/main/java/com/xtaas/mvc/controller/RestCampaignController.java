package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.valueobject.CampaignNameSearchResult;
import com.xtaas.domain.valueobject.CampaignSearchResult;
import com.xtaas.domain.valueobject.SupervisorCampaignResult;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.AssetDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.CampaignDetailsForAgentDTO;
import com.xtaas.web.dto.CampaignNameSearchDTO;
import com.xtaas.web.dto.CampaignSearchDTO;

@RestController
public class RestCampaignController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RestCampaignController.class);
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@RequestMapping(value = {"/rest/campaign/{id}", "/api/campaign/{id}"}, method = RequestMethod.GET)
	@Authorize(roles = {Roles.ROLE_CAMPAIGN_MANAGER})
	public CampaignDTO retrieve(@PathVariable("id") String id) {
		return campaignService.returnCampaigns(id);
	}
	
	@RequestMapping(value = {"/rest/campaigndetails/agent/{id}"}, method = RequestMethod.GET)
	public CampaignDetailsForAgentDTO getCampaignForAgent(@PathVariable("id") String id) {
		return campaignService.getCampaignInfoForAgent(id);
	}

	@RequestMapping(value = "/rest/campaign", method = RequestMethod.GET)
	public CampaignSearchResult retrieveAll(@RequestParam("searchstring") String searchstring) throws IOException {
		CampaignSearchDTO campaignSearchDTO = JSONUtils.toObject(searchstring, CampaignSearchDTO.class);
		switch (campaignSearchDTO.getClause()) {
		case ByCampaignManager:
			return campaignService.listByCampaignManager(campaignSearchDTO);
		case BySupervisor:
			return campaignService.listBySupervisor(campaignSearchDTO);
		case ByTeamIds:
			return campaignService.listByteamIds(campaignSearchDTO);
		case ByQa:
			try {
				return campaignService.listByQa(campaignSearchDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Added for qafilter change
		case ByQa2:
			try {
				return campaignService.listBySecQa(campaignSearchDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		default:
			throw new NotImplementedException();
		}
	}
	
	@RequestMapping(value = "/rest/campaign/supervisor", method = RequestMethod.GET)
	public SupervisorCampaignResult retrieveCampaigns(@RequestParam("searchstring") String searchstring)
			throws IOException {
		CampaignSearchDTO campaignSearchDTO = JSONUtils.toObject(searchstring, CampaignSearchDTO.class);
		return campaignService.getSupervisorCampaigns(campaignSearchDTO);
	}

	@RequestMapping(value = "/rest/campaign", method = RequestMethod.POST)
	public CampaignDTO create(@Valid @RequestBody CampaignDTO campaignDTO, HttpServletRequest request) {
		return campaignService.createCampaign(campaignDTO, request);
	}

	@RequestMapping(value = "/rest/campaign/{id}", method = RequestMethod.PUT)
	public CampaignDTO update(@PathVariable("id") String id,
			@RequestBody CampaignDTO campaignDTO) {
		return campaignService.editCampaign(campaignDTO);
	}
	
	@RequestMapping(value = "/rest/campaign/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("id") String id) {
		campaignService.deleteCampaign(id);
	}
	
	@RequestMapping(value = {"/rest/plancampaign/{id}"}, method = RequestMethod.GET)
	public void planCampaign(@PathVariable("id") String campaignId) {
		campaignService.sendEmail(campaignId);
	} 
	
	@RequestMapping(value = "/rest/searchcampaign", method = RequestMethod.GET)
	public CampaignNameSearchResult searchCampaign(@RequestParam("searchstring") String searchstring)
			throws IOException {
		CampaignNameSearchDTO campaignNameSearchDTO = JSONUtils.toObject(searchstring, CampaignNameSearchDTO.class);
		return campaignService.searchCampaigns(campaignNameSearchDTO);
	}
	
	@RequestMapping(value = "/rest/campaign/clone/{id}", method = RequestMethod.GET)
	public CampaignDTO cloneCampaign(@PathVariable("id") String campaignId) {
		return campaignService.cloneCampaign(campaignId);
	}

	@RequestMapping(value = "/rest/supervisor/searchCampaigns/{name}", method = RequestMethod.GET)
	public SupervisorCampaignResult searchCampaigns(@PathVariable("name") String name) throws IOException {
		return campaignService.searchSupervisorCampaigns(name);
	}
	
}
