package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CallServiceStatus;

@RestController
public class CallResource {

	@Autowired
	private CallServiceStatus callService;

	@RequestMapping(value = "/call/twilio/status", method = RequestMethod.POST)
	public ResponseEntity<Boolean> callStatus(@RequestParam("CallSid") String callSid,
			@RequestParam("CallStatus") String callStatus) {
		// twilio callback to update status in DB.
		callService.processCallStatus(callSid, callStatus);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
}
