package com.xtaas.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.SendGridService;
import com.xtaas.web.dto.WebHookEventDTO;

@RestController
public class SendGridController {

	private static final Logger logger = LoggerFactory.getLogger(SendGridController.class);

	@Autowired
	private SendGridService sendGridService;

	@RequestMapping(value = "/email/notification/event", method = RequestMethod.POST)
	public ResponseEntity<Boolean> handleEmailNotificationEvent(HttpServletRequest request,
			@RequestBody List<WebHookEventDTO> webHookEvents) {
		logger.debug("Webhook response is = " + webHookEvents);
		sendGridService.handleEmailNotificationEvent(webHookEvents);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

}
