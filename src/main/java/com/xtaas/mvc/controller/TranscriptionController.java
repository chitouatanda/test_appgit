package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.db.entity.Login;
import com.xtaas.exception.UnauthorizedAccessException;
import com.xtaas.service.TranscriptionServiceImpl;
import com.xtaas.service.UserService;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.web.dto.TranscribeCriteriaDTO;
import com.xtaas.worker.TranscriptApiThread;

@RestController
public class TranscriptionController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private TranscriptionServiceImpl transcriptionServiceImpl;

	/**
	 * DATE :- 28/01/2021 Added below API to transcript the given recordings and
	 * send in email.
	 */
	@RequestMapping(value = "/transcript", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public ResponseEntity<String> transcriptRecording(@RequestParam("file") @NotBlank MultipartFile file,
			@RequestParam("userName") @NotBlank String userName, @RequestParam("password") @NotBlank String password,@RequestParam("emails") @NotBlank ArrayList<String> emails) throws IOException {
		final HttpHeaders httpHeaders = new HttpHeaders();
		if (file.isEmpty() || file == null || userName.isEmpty() || userName == "" || password.isEmpty() || password == "" || emails.isEmpty() || emails.size() == 0) {
			throw new UnauthorizedAccessException("Please provide all the required fields.");
		}
		Login user = userService.getUser(userName);
		if (user != null) {
			String salt = password + "{" + userName + "}";
			String passwordHash = EncryptionUtils.generateMd5Hash(salt);
			if (passwordHash.equalsIgnoreCase(user.getPassword())) {
				TranscriptApiThread transcriptApiThread = new TranscriptApiThread(file, emails);
				Thread thread = new Thread(transcriptApiThread);
				thread.start();
				return new ResponseEntity<String>("Please check the transcription status in callclassificationjobqueue collection.", httpHeaders,
						HttpStatus.OK);
			} else {
				throw new UnauthorizedAccessException("Username or password is not valid.");
			}
		} else {
			throw new UnauthorizedAccessException("Username or password is not valid.");
		}
	}
	
	@RequestMapping(value = "/transcription/criteria", method = RequestMethod.POST)
	public ResponseEntity<String> transcript(@Valid @RequestBody TranscribeCriteriaDTO transcribeCriteriaDTO)
			throws ParseException {
		String resultMessage = transcriptionServiceImpl.transcribeBasedOnCriteria(transcribeCriteriaDTO);
		return new ResponseEntity<String>(resultMessage, HttpStatus.OK);
	}
}
