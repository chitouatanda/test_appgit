package com.xtaas.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.OrgTypeExperienceMap;
import com.xtaas.db.entity.Organization;
import com.xtaas.service.AgentRegistrationServiceImpl;
import com.xtaas.service.ConfigService;
import com.xtaas.service.LoginUploadService;
import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.LoginDTO;
import com.xtaas.web.dto.PasswordDTO;
@Controller
public class RestUserController {
	
	private static final Logger logger = LoggerFactory.getLogger(RestUserController.class);
	
	@Autowired
    private LoginUploadService loginUploadService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConfigService configService;
	
	@Autowired
	private	AgentRegistrationServiceImpl agentRegistrationServiceImpl;
	
	@RequestMapping(value = "/rest/user", method = RequestMethod.GET)
	@ResponseBody
	public LoginDTO getLoggedInUser(){
		agentRegistrationServiceImpl.removePreviousProspect();
		return new LoginDTO(userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername()));
	}
	
	@RequestMapping(value = {"/api/user/{username}","/user/{username}"}, method = RequestMethod.GET)
	@ResponseBody
	public LoginDTO getUser(@PathVariable("username") String username){
		return new LoginDTO(userService.getUser(username));
	}
	
	@RequestMapping(value = "/rest/user/{id}/attribute/password", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Boolean> updatePassword(@PathVariable("id") String userId,@RequestBody PasswordDTO passwordDTO) {
		if (!userId.equals(XtaasUserUtils.getCurrentLoggedInUsername())) { //user must be logged in and he should be able to change his own pwd only 
			logger.error("Bad Request. Logged-In user is [{}] but changing Password for [{}] ", XtaasUserUtils.getCurrentLoggedInUsername(), userId);
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
		if (passwordDTO.getNewPassword() == null || passwordDTO.getNewPassword().isEmpty()) {
			logger.error("Bad Request. New Password is blank");
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
		if (userService.updatePassword(userId, passwordDTO)) {		
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		} else {
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/api/user", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Boolean> create(@Valid @RequestBody Login user) {
		try {
			userService.createUser(user); 
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.user.username"); 
		}
	}
	
	/**
     * Api for sending a reset password link to emailId of user
     * @return
     */
	@RequestMapping(value = "/rest/user/resetpassword", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> sendResetPasswordLink(@RequestParam("username") String username) {
		userService.sendResetPasswordLink(username);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		
	}
	
	/**
     * Api for validate a forgot password link 
     * if link is valid, redirect user to change password page
     * if link is not valid, redirect user to login page  
     * @return
     */
	@RequestMapping(value = "/rest/user/changeforgotpassword", method = RequestMethod.GET)
	public final String showChangePasswordPage(@RequestParam("token") String token) {
		if (userService.validateResetPasswordLink(token)) {
			return "redirect:/#!/resetpassword";
		} else {
			return "redirect:/#!/login";
		}
	}
	
	/**
     * Api for change forgot password 
     * @return
     */
	@RequestMapping(value = "/rest/user/changeforgotpassword", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Boolean> changeForgotPassword(@RequestBody String newPassword) {
		if (newPassword.isEmpty()) {
			logger.error("Bad Request. New Password is blank");
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
		if (userService.updateForgotPassword(newPassword)) {		
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		} else {
			return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/rest/user/orgexperience", method = RequestMethod.GET)
	@ResponseBody
    public OrgTypeExperienceMap getOrgTypeExperienceMap(){
       Organization organization =  XtaasUserUtils.getCurrentLoggedInUsersOrganization();
       return configService.getOrgTypeExperienceMap(organization.getOrganizationType().toString());
    }
	
	@RequestMapping(value="/login/upload", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public @ResponseBody String uploadLoginFile(@RequestParam("file") MultipartFile file, HttpServletRequest request){
		return loginUploadService.uploadLoginDetails(file, request.getSession());
	}
	
	@RequestMapping(value = "/remove/logincache/{username}", method = RequestMethod.GET)
	public ResponseEntity<String> removeUserFromCache(@PathVariable("username") String username){
		String str = userService.removeUserFromCache(username);
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
	
}
