package com.xtaas.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.CampaignSettingService;
import com.xtaas.web.dto.CampaignSettingDTO;

@RestController
public class CampaignSettingController {

	@Autowired
	private CampaignSettingService campaignSettingService;

	@RequestMapping(value = "/admin/campaignsettings/{campaignId}", method = RequestMethod.PUT)
	public void update(@PathVariable("campaignId") String campaignId,
			@RequestBody CampaignSettingDTO campaignSettingDTO, HttpServletRequest request) {
		campaignSettingService.updateCampaignSettings(campaignId, campaignSettingDTO, request);
	}

}
