package com.xtaas.mvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.service.DataFeedbackReportService;
import com.xtaas.service.ProspectFeedbackService;

@RestController
public class DataFeedbackController {

	private final DataFeedbackReportService dataFeedbackReportService;
	private final ProspectFeedbackService prospectFeedbackService;

	public DataFeedbackController(DataFeedbackReportService dataFeedbackReportService,
			ProspectFeedbackService prospectFeedbackService) {
		this.dataFeedbackReportService = dataFeedbackReportService;
		this.prospectFeedbackService = prospectFeedbackService;
	}

	@RequestMapping(value = "/datafeedback/report/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> dataFeedbackReport(@PathVariable("name") String name) throws Exception {
		String jobId = dataFeedbackReportService.manualDataFeedbackReport(name);
		return new ResponseEntity<String>(String.format(
				"Report link will be sent on email. If job is failed then mail will be sent with details or you can check FeedbackJobTracker collection for JobId:[%s].",
				jobId), HttpStatus.OK);
	}

	@RequestMapping(value = "/datafeedback/backup/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> dataFeedbackBackup(@PathVariable("name") String name) throws Exception {
		String jobId = prospectFeedbackService.manualDataBackup(name);
		return new ResponseEntity<String>(String.format(
				"If job is failed then mail will be sent with details or you can check FeedbackJobTracker collection for JobId [%s].",
				jobId), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/datafeedback/backupandreport/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> dataFeedbackBackupAndReport(@PathVariable("name") String name) throws Exception {
		String jobId = prospectFeedbackService.manualDataBackupAndReport(name);
		return new ResponseEntity<String>(String.format(
				"Report link will be sent on email. If job is failed then mail will be sent with details or you can check FeedbackJobTracker collection for JobId [%s].",
				jobId), HttpStatus.OK);
	}

}
