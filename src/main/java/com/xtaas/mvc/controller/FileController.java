/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.UUID;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasUserUtils;

/**
 * Controller which handles file upload request and uploads them to Amazon S3
 * 
 * @author djain
 *
 */
@Controller
public class FileController {
	
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	private AmazonS3 s3Client;
	private AmazonS3 s3IAMClient;

	
	@Autowired
	private LoginRepository loginRepository;
	
	@RequestMapping(value="/profilepic/upload", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON)
    public @ResponseBody String uploadProfilePic(@RequestParam("file") MultipartFile file){
		String bucketName = "xtaas-user-profiles";
		String fileName = file.getOriginalFilename();
		
		@SuppressWarnings("serial")
		final HashSet<String> supportedContentTypes = new HashSet<String>() {{
			add("image/gif"); 
			add("image/jpeg"); 
			add("image/png"); 
		}};
		String responseMsg = "";
		if (!supportedContentTypes.contains(file.getContentType())) { //if unsupported content type uploaded return with an error
			responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"UNSUPPORTED FILE TYPE : Only jpg, png and gif Filetypes are supported.\", \"file\" : \"" + fileName + "\"}";
			logger.error("uploadFile() : Error: UNSUPPORTED FILE TYPE " + fileName);
			return responseMsg;
		}
		
		String userId = XtaasUserUtils.getCurrentLoggedInUsername();
		String folderName;
		String folderNameInUrl;
		try {
			folderName = URLEncoder.encode(EncryptionUtils.encrypt(userId),XtaasConstants.UTF8); //this is the name of the folder created in S3
			folderNameInUrl = URLEncoder.encode(folderName,XtaasConstants.UTF8); //this is the name using which the folder can be access in url 
		} catch (UnsupportedEncodingException e1) {
			folderName = UUID.randomUUID().toString(); //should never occur
			folderNameInUrl = folderName;
		}
		
		String[] fileNameSplittedOnDot = StringUtils.split(fileName, ".");
		String fileExtension = fileNameSplittedOnDot[fileNameSplittedOnDot.length-1];
		
		String uploadedFileName = "/pic."+fileExtension;
		String filePath = folderName + uploadedFileName;
		if (!file.isEmpty()) {
            try {
            	if (s3Client == null) {
            		s3Client = new AmazonS3Client();
            	}
            	
            	//upload file
            	ObjectMetadata metaData = new ObjectMetadata();
            	metaData.setContentLength(file.getBytes().length);
            	metaData.setContentType(file.getContentType());
            	
            	PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, filePath, file.getInputStream(), metaData);
                putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
                s3Client.putObject(putObjectRequest); // upload file
            	
                String url = "https://s3.amazonaws.com/"+bucketName+"/"+folderNameInUrl+uploadedFileName;
                responseMsg = "{\"code\": \"SUCCESS\", \"url\" : \"" + url +"\", \"file\" : \"" + filePath + "\"}";
                
                //update url in login record
                Login user = loginRepository.findOneById(userId);
                user.setProfilePicUrl(url);
                
                loginRepository.save(user);
            } catch (Exception e) {
            	responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"" + e.getMessage() +"\", \"file\" : \"" + filePath + "\"}";
            	logger.error("uploadProfilePic() : Error occurred while uploading file " + filePath, e);
            }
        } else {
        	responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"EMPTY FILE\", \"file\" : \"" + filePath + "\"}";
        }
        logger.debug("uploadProfilePic() : Upload complete for [{}] file. Response : [{}]", filePath, responseMsg);
        return responseMsg;
	}

	@RequestMapping(value = "/file/upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER })
	public @ResponseBody String uploadFile(@RequestParam("folderName") String folder,
			@RequestParam("file") MultipartFile file) {
		String returnStatement = "";
		Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (loginUser != null && loginUser.getRoles().contains(Roles.ROLE_CAMPAIGN_MANAGER.toString())) {
			returnStatement = uploadFileToS3Bucket(folder, file);
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
		return returnStatement;
	}
	
	private String uploadFileToS3Bucket(String folder, MultipartFile file) {
		String fileName = file.getOriginalFilename();
		String filePath = folder + "/" + fileName;
		@SuppressWarnings("serial")
		final HashSet<String> supportedContentTypes = new HashSet<String>() {
			{
				add("application/pdf"); // .pdf
				add("application/msword"); // .doc, .dot
				add("application/vnd.openxmlformats-officedocument.wordprocessingml.document"); // .docx
			}
		};
		String responseMsg = "";
		if (!supportedContentTypes.contains(file.getContentType())) { // if unsupported content type uploaded return
																		// with an error
			responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"UNSUPPORTED FILE TYPE : Only pdf, doc, docx Filetypes are supported.\", \"file\" : \""
					+ filePath + "\"}";
			logger.error("uploadFile() : Error: UNSUPPORTED FILE TYPE " + filePath);
			return responseMsg;
		}
		logger.debug("uploadFile() : Uploading [{}] file to folder [{}]", fileName, folder);
		if (!file.isEmpty()) {
			try {
				if (s3IAMClient == null) {
					s3IAMClient = createIAMAWSConnection();
				}
				// upload file
				ObjectMetadata metaData = new ObjectMetadata();
				metaData.setContentLength(file.getBytes().length);
				metaData.setContentType(file.getContentType());
				PutObjectRequest putObjectRequest = new PutObjectRequest("xtaas-assets-bucket", filePath,
						file.getInputStream(), metaData);
				putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
				s3IAMClient.putObject(putObjectRequest); // upload file
				String url = "https://xtaas-assets-bucket.s3.amazonaws.com/" + filePath;
				responseMsg = "{\"code\": \"SUCCESS\", \"url\" : \"" + url + "\", \"file\" : \"" + filePath + "\"}";
				// s3IAMClient.shutdown();
			} catch (Exception e) {
				responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"" + e.getMessage() + "\", \"file\" : \"" + filePath
						+ "\"}";
				logger.error("uploadFile() : Error occurred while uploading file " + filePath, e);
			}
		} else {
			responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"EMPTY FILE\", \"file\" : \"" + filePath + "\"}";
		}
		logger.debug("uploadFile() : Upload complete for [{}] file. Response : [{}]", filePath, responseMsg);
		return responseMsg;
	}
	
	private AmazonS3 createIAMAWSConnection() {
		try {
			if (s3IAMClient == null) {
				BasicAWSCredentials creds = new BasicAWSCredentials(XtaasConstants.AWS_IAM_ACCESS_KEY_ID,
						XtaasConstants.AWS_IAM_SECRET_KEY);
				s3IAMClient = AmazonS3ClientBuilder.standard().withRegion(XtaasConstants.CLIENT_REGION)
						.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			}
		} catch (Exception ex) {
			logger.error("Error while creating the connection with AWS stroage : {}", ex);
		}
		return s3IAMClient;
	}
}
