package com.xtaas.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.util.json.Jackson;
import com.xtaas.application.service.PlivoOutboundNumberService;
import com.xtaas.db.entity.Login;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;
import com.xtaas.exception.UnauthorizedAccessException;
import com.xtaas.infra.security.AppManager;
import com.xtaas.service.PlivoService;
import com.xtaas.service.UserService;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.service.PlivoCallLogService;
import com.xtaas.web.dto.PlivoCallLogFilterDTO;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;

@RestController
public class PlivoController {

	private final Logger logger = LoggerFactory.getLogger(PlivoController.class);

	@Autowired
	private PlivoService plivoService;

	@Autowired
	private PlivoCallLogService plivoCallLogService;

	@Autowired
	private PlivoOutboundNumberService plivoOutboundNumberService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/voice", produces = "application/xml")
	public ResponseEntity<String> handleVoiceCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle voice call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleVoiceCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/manualcall/callback", produces = "application/xml")
	public ResponseEntity<String> handleManualcallCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle prospect hangup cause.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleManualcallCallback(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/hangup", produces = "application/xml")
	public ResponseEntity<String> handleProspectCallHangup(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle call hangup.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleProspectCallHangup(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/fallback", produces = "application/xml")
	public ResponseEntity<String> handleFallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle fallback.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleFallback(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/machinedetection")
	public ResponseEntity<String> handleSyncMachineDetection(@RequestBody HashMap<String, String> paramMap) {
		logger.debug("Plivo request to handle sync machine detection call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleSyncMachineDetection(paramMap);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/incoming/call", produces = "application/xml")
	public ResponseEntity<String> handleIncomingCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle incoming call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleIncomingCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/incoming/message", produces = "application/xml")
	public ResponseEntity<String> handleIncomingMessage(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle incoming message.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleIncomingMessage(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/voicemail", produces = "application/xml")
	public ResponseEntity<String> handleVoicemail(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle voicemail.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleVoicemail(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/recording/callback", produces = "application/xml")
	public ResponseEntity<String> handleRecordingCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle recording callback.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.handleRecordingCallback(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/plivo/endpoint/{username}")
	public ResponseEntity<String> createEndpoint(@PathVariable String username) {
		logger.debug("Plivo request to create endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Map<String, String> plivoEndpointCredentials = plivoService.createEndpoint(username);
		String jsonString = Jackson.toJsonString(plivoEndpointCredentials); // TODO::Hack, as the Hashmap conversion is
// not working, manually converted
		return new ResponseEntity<>(jsonString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/plivo/endpoint/{endpointId}")
	public ResponseEntity<String> deleteEndpoint(@PathVariable String endpointId) {
		logger.debug("Plivo request to delete endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Boolean isDeleted = plivoService.deleteEndpoint(endpointId);
		String jsonString = Jackson.toJsonString(isDeleted); 
		return new ResponseEntity<>(jsonString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/plivo/endpoint/deleteall")
	public ResponseEntity<Boolean> deleteAllEndpoint() {
		logger.debug("Plivo request to delete endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Boolean isDeleted = plivoService.deleteAllEndpoints();
		return new ResponseEntity<>(isDeleted, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/savecallmetrics", produces = "application/xml")
	public ResponseEntity<String> saveCallMetrics(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Plivo request to handle call metrics.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = plivoService.saveCallMetrics(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/phone/purchase")
	public ResponseEntity<List<PlivoOutboundNumber>> purchasePhoneNumbers(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.debug("Plivo request to purchase phone number for {}.", plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		List<PlivoOutboundNumber> plivoOutboundNumbers = plivoService.purchasePhoneNumbers(plivoPhoneNumberPurchaseDTO);
		return new ResponseEntity<>(plivoOutboundNumbers, httpHeaders, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/plivo/phone/purchase/other")
	public ResponseEntity<List<PlivoOutboundNumber>> purchasePhoneNumbersForOther(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.debug("Plivo request to purchase phone number for other countries {}.", plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		List<PlivoOutboundNumber> plivoOutboundNumbers = plivoService.purchaseUSPhoneNumbersForOtherCountry(plivoPhoneNumberPurchaseDTO);
		return new ResponseEntity<>(plivoOutboundNumbers, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plivo/voicemail/report")
	public ResponseEntity<?> sendIncomingVoiceMail(@RequestBody PlivoCallLogFilterDTO filterDTO) {
		logger.debug("Plivo request to send Plivo call log report.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Boolean isSent = plivoCallLogService.sendIncomingVoiceMails(filterDTO);
		return new ResponseEntity<>(isSent, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/plivo/outboundnumbers")
	public List<PlivoOutboundNumber> getAllOutboundNumbers() {
		return plivoOutboundNumberService.getAllOutboundNumbers();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/plivo/outboundnumbers/countrydetails")
	public List<CountryDetails> getCountryInformation() {
		return plivoOutboundNumberService.getDistinctCountryInformation();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/plivo/call/stats")
	public Map<String, Integer> getCallStats() {
		return plivoService.getCallStats();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/plivo/recording/{callUUID}")
	public Map<String, List<String>> getRecordingUrlsByCallUUID(@PathVariable String callUUID) {
		return plivoService.getRecordingUrlsByCallUUID(callUUID);
	}
	
	/**
	 * DATE :- 15/10/2020
	 * Added below API to fetch new outboundnumber foreach redial in case of manual dial.
	 * @param redialNumberDTO
	 * @return Map<String, String>
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/plivo/redial/outboundnumber")
	public Map<String, String> getOutboundnumber(@RequestBody RedialOutboundNumberDTO redialNumberDTO) {
		logger.debug("Plivo request to get new outboundnumber for each redial.");
		return plivoService.getPlivoOutboundNumberForRedial(redialNumberDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/plivo/phonenumber/refresh")
	public ResponseEntity<String> refreshPhoneNumbers(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.debug("Plivo request to refresh phone number for {}.", plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Login user = userService.getUser(plivoPhoneNumberPurchaseDTO.getUserName());
		if (user != null) {
			String salt = plivoPhoneNumberPurchaseDTO.getPassword() + "{" + plivoPhoneNumberPurchaseDTO.getUserName() + "}";
			String passwordHash = EncryptionUtils.generateMd5Hash(salt);
			if (passwordHash.equalsIgnoreCase(user.getPassword())) {
				plivoOutboundNumberService.refreshPhoneNumbers(plivoPhoneNumberPurchaseDTO);
				return new ResponseEntity<>("Please check youe email for plivonumber refresh result.", httpHeaders, HttpStatus.OK);
			} else {
				throw new UnauthorizedAccessException("Username or password is not valid.");
			}
		} else {
			throw new UnauthorizedAccessException("Username or password is not valid.");
		}
	}

}
