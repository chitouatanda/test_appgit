package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xtaas.application.service.TelnyxOutboundNumberService;
import com.xtaas.domain.entity.PlivoOutboundNumber;
import com.xtaas.domain.entity.TelnyxOutboundNumber;
import com.xtaas.service.TelnyxService;
import com.xtaas.web.dto.CallInfoDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;
import com.xtaas.web.dto.TelnyxCallWebhookDTO;
import com.xtaas.web.dto.TelnyxDTMFRequestDTO;
import com.xtaas.web.dto.TelnyxPhoneNumberPurchaseDTO;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TelnyxController {

	private final Logger logger = LoggerFactory.getLogger(TelnyxController.class);

	@Autowired
	private TelnyxService telnyxService;
	
	@Autowired
	private TelnyxOutboundNumberService telnyxOutboundNumberService;

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/voice")
	public ResponseEntity<String> handleVoiceCall(@RequestBody Map<String, Object> telnyxCallWebhookMap) {
		logger.debug("Telnyx request to handle voice call.");
		JSONObject webhookJson = new JSONObject(telnyxCallWebhookMap);
		TelnyxCallWebhookDTO telnyxCallWebhookDTO = new TelnyxCallWebhookDTO();
		try {
			telnyxCallWebhookDTO = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(webhookJson.toString(), TelnyxCallWebhookDTO.class);
		} catch (IOException e) {
			logger.error("TelnyxController======>Mapping error with Exception : " + e.getStackTrace());
		}
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		String response = "";
		if (telnyxCallWebhookDTO != null && telnyxCallWebhookDTO.getData() != null) {
			response = telnyxService.handleVoiceCall(telnyxCallWebhookDTO);
		} else {
			logger.error("TelnyxController======>TelnyxCallWebhookDTO object with null data with Webhook Input Map : " + telnyxCallWebhookMap.toString());
		}
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/telnyx/phone/purchase")
	public ResponseEntity<String> purchasePhoneNumbers(
			@Valid @RequestBody TelnyxPhoneNumberPurchaseDTO telnyxPhoneNumberPurchaseDTO) {
		logger.debug("Telnyx request to purchase phone number for {}.", telnyxPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
	//	httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String telnyxOutboundNumbers = telnyxService.purchasePhoneNumbers(telnyxPhoneNumberPurchaseDTO);
		return new ResponseEntity<>(telnyxOutboundNumbers, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/telnyx/connection/{username}")
	public ResponseEntity<Map<String, String>> createConnectionForAgent(@PathVariable String username) {
		logger.debug("Telnyx request to create endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Map<String, String> plivoEndpointCredentials = telnyxService.createConnectionForAgent(username);
		return new ResponseEntity<>(plivoEndpointCredentials, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/telnyx/connection/{connectionId}")
	public ResponseEntity<String> deleteConnectionForAgent(@PathVariable String connectionId) {
		logger.debug("Telnyx request to create endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		String xmlString = telnyxService.deleteConnectionForAgent(connectionId);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/call/incoming", produces = "application/xml")
	public ResponseEntity<String> handleIncomingCall(@RequestBody TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		logger.debug("Telnyx request to handle incoming call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = telnyxService.handleIncomingCall(telnyxCallWebhookDTO);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/recording/save", produces = "application/xml")
	public ResponseEntity<String> saveRecording(@RequestBody TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
		logger.debug("Telnyx request to handle incoming call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
	//	httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = telnyxService.saveRecording(telnyxCallWebhookDTO);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/callagent/auto")
	public ResponseEntity<Map<String, Object>> callAgentForConferenceCreation(@RequestBody CallInfoDTO callInfo) {
		logger.debug("Telnyx request to call Agent from server");
		final HttpHeaders httpHeaders = new HttpHeaders();
	//	httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Map<String, Object> callDetails = telnyxService.callAgentForConferenceCreation(callInfo);
		return new ResponseEntity<>(callDetails, httpHeaders, HttpStatus.OK);
	}

	// @RequestMapping(method = RequestMethod.POST, value = "/telnyx/record/start")
	// public ResponseEntity<String> startRecording(@RequestBody TelnyxCallWebhookDTO telnyxCallWebhookDTO) {
	// 	logger.debug("Telnyx request to call Agent from server");
	// 	final HttpHeaders httpHeaders = new HttpHeaders();
	// 	httpHeaders.setContentType(MediaType.APPLICATION_XML);
	// 	String xmlString = telnyxService.startRecording(telnyxCallWebhookDTO);
	// 	return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	// }

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/callagent/manual")
	public ResponseEntity<Map<String, Object>> callAgentForManualDial(@RequestBody CallInfoDTO callInfo) {
		logger.debug("Telnyx request to call Agent from server");
		final HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Map<String, Object> callDetails = telnyxService.callAgentForManualDial(callInfo);
		return new ResponseEntity<>(callDetails, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/call/hangup")
	public ResponseEntity<String> hangupCall(@RequestBody String callLegId) {
		logger.debug("Telnyx request to call Agent from server");
		final HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		telnyxService.handleCallHangup(callLegId);
		return new ResponseEntity<>("ok", httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/telnyx/call/control/{callLegId}")
	public ResponseEntity<String> getCallControlId(@PathVariable String callLegId) {
		logger.debug("Telnyx request to create endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String callControlId = telnyxService.getCallControlId(callLegId);
		return new ResponseEntity<>(callControlId, httpHeaders, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/call/senddtmf")
	public ResponseEntity<String> sendDtmf(@RequestBody TelnyxDTMFRequestDTO dtmfObj) {
		logger.debug("Telnyx request to send dtmf digit");
		final HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		telnyxService.sendDtmf(dtmfObj);
		return new ResponseEntity<>("ok", httpHeaders, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/telnyx/outboundnumbers")
	public List<TelnyxOutboundNumber> getAllOutboundNumbers() {
		return telnyxOutboundNumberService.getAllOutboundNumbers();
	}
	
	/**
	 * DATE :- 06/11/2020
	 * Added below API to fetch new outboundnumber foreach redial in case of manual dial.
	 * @param redialNumberDTO
	 * @return Map<String, String>
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/telnyx/redial/outboundnumber")
	public Map<String, String> getOutboundnumber(@RequestBody RedialOutboundNumberDTO redialNumberDTO) {
		logger.debug("Telnyx request to get new outboundnumber for each redial.");
		return telnyxService.getTelnyxOutboundNumberForRedial(redialNumberDTO);
	}
	
	/**
	 * DATE :- 18/11/2020
	 * Added below API to delete outboundnumber from telnyx portal and telnyxoutboundnumber collection.
	 * @param phoneNumberIdList
	 * @return List<String>
	 */	
	@RequestMapping(method = RequestMethod.DELETE, value = "/telnyx/delete/outboundnumber")
	public List<String> getOutboundnumber(@RequestBody List<String> phoneNumberList) {
		logger.debug("Telnyx request to delete outboundnumber");
		// return not deleted phoneNumbers
		return telnyxService.deleteTelnyxOutboundNumber(phoneNumberList);
	}

}