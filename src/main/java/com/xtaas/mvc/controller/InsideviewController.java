package com.xtaas.mvc.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xtaas.service.InsideviewService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InsideviewController {
  
  private final Logger logger = LoggerFactory.getLogger(InsideviewController.class);
  
  @Autowired
  InsideviewService insideviewService;
  
  @RequestMapping(method = RequestMethod.HEAD, value = "/insideview/jobstatus")
  public void verifyJob(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("Insideview job status HEAD webhook called.");
    response.setHeader("X-InsideViewAPI-ValidationCode", request.getHeader("X-InsideViewAPI-ValidationCode"));
  }
  
  @RequestMapping(method = RequestMethod.POST, value = "/insideview/jobstatus")
	public ResponseEntity<String> getJobStatus(@RequestBody Map<String, Object> insideviewJobStatusMap) {
    logger.debug("Insideview job status POST webhook called.");
    //{alertId=433275, jobId=sh7dae35eejeiv1js8hg, jobStatus=finished, jobType=targetContactDetails, recordsProcessed=5}
    if (insideviewJobStatusMap.containsKey("jobStatus") && insideviewJobStatusMap.containsKey("jobId")) {
      insideviewService.getJobResults((String) insideviewJobStatusMap.get("jobId"));
    } else {
      logger.error("No Job status or JobId field present in Request map from Insideview webhook");
    }
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>("OK", httpHeaders, HttpStatus.OK);
  }
  
}
