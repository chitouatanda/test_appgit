package com.xtaas.mvc.controller;

import java.util.HashMap;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.domain.valueobject.CallbackDispositionStatus;
import com.xtaas.domain.valueobject.DialerCodeDispositionStatus;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.DispositionType;
import com.xtaas.domain.valueobject.FailureDispositionStatus;
import com.xtaas.domain.valueobject.LabelEntityEnum;
import com.xtaas.domain.valueobject.NotDisposeDispositionStatus;
import com.xtaas.domain.valueobject.SuccessDispositionStatus;

@RestController
@RequestMapping(value = "/label")
public class DisplayLabelController {
	
	@RequestMapping(value="/dispositions", method = RequestMethod.GET)
	public HashMap<String, String> getDispositionLabels() {
		HashMap<String, String> dispositionsKeyLabelMap = new HashMap<String, String>();
		for (DispositionType dispositionType : DispositionType.values()) {
			dispositionsKeyLabelMap.put(dispositionType.name(), dispositionType.toString());
		}
		
		for (SuccessDispositionStatus successDispositionStatus : SuccessDispositionStatus.values()) {
			dispositionsKeyLabelMap.put(successDispositionStatus.name(), successDispositionStatus.toString());
		}

		for (NotDisposeDispositionStatus notDisposedDispositionStatus : NotDisposeDispositionStatus.values()) {
			dispositionsKeyLabelMap.put(notDisposedDispositionStatus.name(), notDisposedDispositionStatus.toString());
		}
		
		for (CallbackDispositionStatus callbackDispositionStatus : CallbackDispositionStatus.values()) {
			dispositionsKeyLabelMap.put(callbackDispositionStatus.name(), callbackDispositionStatus.toString());
		}
		
		for (DialerCodeDispositionStatus dialerCodeDispositionStatus : DialerCodeDispositionStatus.values()) {
			dispositionsKeyLabelMap.put(dialerCodeDispositionStatus.name(), dialerCodeDispositionStatus.toString());
		}
		
		for (FailureDispositionStatus failureDispositionStatus : FailureDispositionStatus.values()) {
			dispositionsKeyLabelMap.put(failureDispositionStatus.name(), failureDispositionStatus.toString());
		}
		
		return dispositionsKeyLabelMap;
	}
	
	
	
	@RequestMapping(value="/{labelentity}/{key}", method = RequestMethod.GET)
	public HashMap<String, String> getDisplayLabel(@PathVariable("labelentity") String labelEntity, @PathVariable("key") String key) {
		LabelEntityEnum labelEntityEnum = EnumUtils.getEnum(LabelEntityEnum.class, labelEntity.toUpperCase());
		if (labelEntityEnum == null) throw new IllegalArgumentException("Invalid Label Entity specified: " + labelEntity);
		
		HashMap<String, String> subDispositionsKeyLabelMap = new HashMap<String, String>();
		String displayLabel = key;
		switch (labelEntityEnum) {
			case DISPOSITIONTYPE:
				DispositionType dispositionType = EnumUtils.getEnum(DispositionType.class, key.toUpperCase());
				if (dispositionType == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = dispositionType.toString();
				subDispositionsKeyLabelMap.put(dispositionType.name(), displayLabel);
				break;
			case SUCCESSDISPOSITIONSTATUS:
				SuccessDispositionStatus successDispositionStatus = EnumUtils.getEnum(SuccessDispositionStatus.class, key.toUpperCase());
				if (successDispositionStatus == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = successDispositionStatus.toString();
				subDispositionsKeyLabelMap.put(successDispositionStatus.name(), displayLabel);
				break;
			case NOT_DISPOSEDDISPOSITIONSTATUS:
				NotDisposeDispositionStatus notDisposedDispositionStatus = EnumUtils.getEnum(NotDisposeDispositionStatus.class, key.toUpperCase());
				if (notDisposedDispositionStatus == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = notDisposedDispositionStatus.toString();
				subDispositionsKeyLabelMap.put(notDisposedDispositionStatus.name(), displayLabel);
				break;		
			case CALLBACKDISPOSITIONSTATUS:
				CallbackDispositionStatus callbackDispositionStatus = EnumUtils.getEnum(CallbackDispositionStatus.class, key.toUpperCase());
				if (callbackDispositionStatus == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = callbackDispositionStatus.toString();
				subDispositionsKeyLabelMap.put(callbackDispositionStatus.name(), displayLabel);
				break;
			case DIALERCODEDISPOSITIONSTATUS:
				DialerCodeDispositionStatus dialerCodeDispositionStatus = EnumUtils.getEnum(DialerCodeDispositionStatus.class, key.toUpperCase());
				if (dialerCodeDispositionStatus == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = dialerCodeDispositionStatus.toString();
				subDispositionsKeyLabelMap.put(dialerCodeDispositionStatus.name(), displayLabel);
				break;
			case FAILUREDISPOSITIONSTATUS:
				FailureDispositionStatus failureDispositionStatus = EnumUtils.getEnum(FailureDispositionStatus.class, key.toUpperCase());
				if (failureDispositionStatus == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = failureDispositionStatus.toString();
				subDispositionsKeyLabelMap.put(failureDispositionStatus.name(), displayLabel);
				break;
			case DIALERMODE:
				DialerMode dialerMode = EnumUtils.getEnum(DialerMode.class, key.toUpperCase());
				if (dialerMode == null) throw new IllegalArgumentException("Invalid label key specified: " + key);
				displayLabel = dialerMode.toString();
				subDispositionsKeyLabelMap.put(dialerMode.name(), displayLabel);
				break;
		}
		return subDispositionsKeyLabelMap;
	}

}
