package com.xtaas.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.ApplicationProperty;
import com.xtaas.service.ApplicationPropertyService;

@RestController
public class ApplicationPropertyController {

	@Autowired
	private ApplicationPropertyService applicationPropertyService;

	@RequestMapping(value = "/applicationproperties", method = RequestMethod.GET)
	public List<ApplicationProperty> getAllApplicationProperties() {
		return applicationPropertyService.getAllApplicationProperties();
	}
	
	@RequestMapping(value = "/applicationproperties/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ApplicationProperty getAgentDetails(@PathVariable("name") String name) {
		return applicationPropertyService.getSingleApplicationProperty(name);
	}

}
