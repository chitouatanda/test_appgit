package com.xtaas.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.service.UserService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
@RequestMapping(value = "/rest/campaigntypes")
public class CampaignTypesController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<KeyValuePair<String, String>> retrieveAll() {
		Login loginUser =  userService.getUser(XtaasUserUtils.getCurrentLoggedInUsername());
		Organization organization = organizationRepository.findOneById(loginUser.getOrganization());
		List<KeyValuePair<String, String>> campaignTypesList = new ArrayList<KeyValuePair<String, String>>();
		//if organization isAgency then add LeadGeneration into list
		//else
		//Loop through
		if(organization != null && organization.isAgency() ) {
			KeyValuePair<String, String> campaignTypesPair = new KeyValuePair<String, String>();
			campaignTypesPair.setKey(CampaignTypes.LeadGeneration.toString());
			campaignTypesPair.setValue(CampaignTypes.LeadGeneration.name());
			campaignTypesList.add(campaignTypesPair);
		} else {
			for (CampaignTypes campaignTypes : CampaignTypes.values()) {
				KeyValuePair<String, String> campaignTypesPair = new KeyValuePair<String, String>();
				campaignTypesPair.setKey(campaignTypes.toString());
				campaignTypesPair.setValue(campaignTypes.name());
				campaignTypesList.add(campaignTypesPair);
			}
		}
		return campaignTypesList;
	}
	
}
