package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xtaas.application.service.CampaignContactService;
import com.xtaas.application.service.CampaignContactServiceImpl.FileUploadTracker;
import com.xtaas.application.service.ContactDownloadServiceImpl;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.domain.entity.ListProviderUsage;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.web.dto.CampaignContactDTO;
import com.xtaas.web.dto.ContactDTO;


@RestController
@RequestMapping(value = "/campaigncontact/{campaignId}")
public class CampaignContactController {

	private static final Logger logger = LoggerFactory.getLogger(CampaignContactController.class);
	
	@Autowired
	private CampaignContactService campaignContactService;
	
	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private ContactDownloadServiceImpl contactDownloadServiceImpl;
	
	@RequestMapping(value="/upload", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public @ResponseBody String uploadCampaignContactFile(@PathVariable("campaignId") String campaignId, @RequestParam("file") MultipartFile file, HttpServletRequest request){
		SupervisorStats dbStats = supervisorStatsRepository.findByCampaignIdTypeAndStatusList("PivotStatus",
				campaignId, Arrays.asList("PIVOT_FILE_DOWNLOAD", "PIVOT_FILE_DOWNLOAD_UPLOADED"));
		if(dbStats != null) {
			dbStats.setStatus("PIVOT_FILE_DOWNLOAD_UPLOADED_DATA_FILE_UPLOAD");
			supervisorStatsRepository.save(dbStats);
		}
		logger.debug("uploadCampaignContactFile() : Uploading of campaign contact file is started...");
		return campaignContactService.createCampaignContact(campaignId, file, request.getSession());
	}
	
	@RequestMapping(value="/upload/status", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	public @ResponseBody FileUploadTracker getFileUploadStatus(HttpServletRequest request) throws IOException{
		HttpSession httpSession = request.getSession();
		FileUploadTracker fileUploaderTracker = (FileUploadTracker) httpSession.getAttribute("fileUploadStatus");//get filUploadStatus object from session
		if (fileUploaderTracker != null) {
			return fileUploaderTracker;
		}
		return null;
	}
	
	@RequestMapping(value="/upload/errors", method=RequestMethod.GET, produces=MediaType.TEXT_HTML)
	public @ResponseBody String getFileUploadErrors(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession httpSession = request.getSession();
		FileUploadTracker fileUploaderTracker = (FileUploadTracker) httpSession.getAttribute("fileUploadStatus");
		if (fileUploaderTracker != null) {
			response.setContentType("text/html");
			ServletOutputStream out = response.getOutputStream();
			if (fileUploaderTracker.getInvalidFileError() != null && !fileUploaderTracker.getInvalidFileError().isEmpty()) {//check if file is in Incorrect format else records have errors
				out.println(StringUtils.join(fileUploaderTracker.getInvalidFileError(),"<br>"));
			} else {
				List<String> errorList = fileUploaderTracker.getErrorList();
				if (errorList != null && !errorList.isEmpty()) {
					out.println(StringUtils.join(errorList,"<br>"));
				}
			}
			out.flush();
		    out.close();
		}
		return null;
	}
	
	@RequestMapping(value= "/listpurchasehistory", method = RequestMethod.GET)
	public List<ListProviderUsage> getListPurchaseHistory(@PathVariable("campaignId") String campaignId) {
		return campaignContactService.getListPurchaseHistory(campaignId);
	}
	
	@RequestMapping(value= "/listpurchasehistory/{listPurchaseTransactionId}", method = RequestMethod.GET)
	public List<CampaignContactDTO> getListPurchaseRecords(@PathVariable("campaignId") String campaignId, @PathVariable("listPurchaseTransactionId") String listPurchaseTransactionId) {
		return campaignContactService.getListPurchasedRecords(listPurchaseTransactionId);
	}
	
	@RequestMapping(value = "/downloadTemplate", method = RequestMethod.GET)
	public ResponseEntity<?> downloadTemplate(HttpServletRequest request, HttpServletResponse response,@PathVariable("campaignId") String campaignId) throws IOException {
		contactDownloadServiceImpl.downloadContactUploadTemplate(request, response, campaignId);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
}
