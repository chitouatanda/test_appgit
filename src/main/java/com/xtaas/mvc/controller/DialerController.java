package com.xtaas.mvc.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Recording;
import com.twilio.sdk.resource.list.RecordingList;
import com.twilio.sdk.verbs.Conference;
import com.twilio.sdk.verbs.Dial;
import com.twilio.sdk.verbs.Hangup;
import com.twilio.sdk.verbs.Record;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;
import com.xtaas.application.service.AgentService;
import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.channel.Channel;
import com.xtaas.channel.ChannelFactory;
import com.xtaas.channel.ChannelType;
import com.xtaas.db.entity.AgentActivityLog;
import com.xtaas.db.entity.AgentActivityLog.AgentActivityCallStatus;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.entity.ProspectCallInteraction;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.db.repository.AgentActivityLogRepository;
import com.xtaas.db.repository.OutboundNumberRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.entity.VoiceMail;
import com.xtaas.domain.entity.VoiceMail.VoiceMessageStatus;
import com.xtaas.pusher.Pusher;
import com.xtaas.service.CallLogService;
import com.xtaas.service.ProspectCallInteractionService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.VoiceMailService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.CallStatusDTO;
import com.xtaas.web.dto.CallStatusDTO.CallState;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/dialer")
public class DialerController {
	private static final Logger logger = LoggerFactory.getLogger(DialerController.class);

	@Autowired
	private CallLogService callLogService;
	
    @Autowired
    private ProspectCallLogService prospectCallLogService;
    
    @Autowired
    private ProspectCallInteractionService prospectCallInteractionService;
    
    @Autowired
	private VoiceMailService voicemailService;
    
    @Autowired
	private AgentService agentService;
    
    @Autowired
	private OutboundNumberRepository outboundNumberRepository;
    
    @Autowired
   	private AgentActivityLogRepository agentActivityLogRepository;
    
    @Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;
    
    @RequestMapping(value = "/confendevent", method = RequestMethod.POST)
    public ResponseEntity<Boolean> handleConferenceEndEvent(HttpServletRequest request) {
    	String confSid = request.getParameter("ConferenceSid");
    	logger.debug("handleConferenceEndEvent() : Conference end event received. ConferenceSid: [{}] ", confSid );
    	
    	HashMap<String, String> paramMap = new HashMap<String, String>();
    	for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
    		String param = params.nextElement();
    		logger.debug(param + " = " + request.getParameter(param));
    		paramMap.put(param, request.getParameter(param));
    	}
    	String prospectCallId = paramMap.get("pcid");
    	String recordingUrl = paramMap.get("RecordingUrl");
    	try {
    		if (prospectCallId != null && recordingUrl != null) {
    			downloadRecordingsToAws.downloadRecording(recordingUrl, prospectCallId, "", false);
    		}
		} catch (Exception ex) {
			logger.error("handleConferenceEndEvent : Error occurred while downloading AWS Recording URL.");
		}
    	logger.debug("handleConferenceEndEvent() : Conference end event handled. ConferenceSid: [{}] ", confSid);
    	return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }
    
    /**
     * This url will be requested by Twilio on call completion
     * @param request
     * @param response
     */
    @RequestMapping(value = "/savecallmetrics", method = RequestMethod.POST, produces = "text/xml")
    public @ResponseBody String saveCallMetrics(HttpServletRequest request) {
    	logger.debug("saveCallMetrics() : Get Call Metrics from Twilio and persist in DB.");
    	
    	HashMap<String, String> paramMap = new HashMap<String, String>();
    	for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
    		String param = params.nextElement();
    		logger.debug(param + " = " + request.getParameter(param));
    		paramMap.put(param, request.getParameter(param));
    	}
    	
    	String response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say></Say></Response>";
    	try {
    		CallLog callLog = new CallLog();
    		callLog.setCallLogMap(paramMap);
    		try {
    			//if recording url is not part of the request parameter than fetch it using Twilio API
				if (!paramMap.containsKey("RecordingUrl")) {
					Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
					Account account = channelTwilio.getChannelConnection();
					Map<String, String> filter = new HashMap<String, String>();
					filter.put("CallSid", paramMap.get("CallSid"));
					RecordingList recordings = account.getRecordings(filter);
					String awsRecordingUrl = "";
					for (Recording recording : recordings) {
						/*
						 * api returns with extension as .json..removing it so that it becomes playable.
						 * Ex: /2010-04-01/Accounts/ACab4d611aa1465a246cb58290ba930a5c/Recordings/
						 * REb64d147f6a4008ae4e308486e4029a5a.json
						 */
						String recordingUrl = "https://api.twilio.com"
								+ StringUtils.replace(recording.getProperty("uri"), ".json", "");
						paramMap.put("RecordingUrl", recordingUrl);
						paramMap.put("RecordingDuration", recording.getProperty("duration"));
						int lastIndex = recordingUrl.lastIndexOf("/");
						String fileName = recordingUrl.substring(lastIndex + 1, recordingUrl.length());
						String rfileName = fileName + ".wav";
						awsRecordingUrl = XtaasConstants.CALL_RECORDING_AWS_URL + rfileName;
						paramMap.put("awsRecordingUrl", awsRecordingUrl);
						// String prospectCallId = paramMap.get("pcid");
						// downloadRecordingsToAws.downloadRecording(recordingUrl, prospectCallId, "", true);
						break; // return the first recording only. support for multiple call recordings
					}
				}
    			callLogService.saveCallLog(callLog);
    		} catch (DuplicateKeyException e) {
    			callLog = callLogService.findCallLog(paramMap.get("CallSid"));
    			HashMap<String, String> callLogMap = callLog.getCallLogMap();
    			Iterator<Map.Entry<String, String>> paramIterator = paramMap.entrySet().iterator();
    			
    			while(paramIterator.hasNext()) {
    	            Map.Entry<String, String> entry = paramIterator.next();
    	            if (!callLogMap.containsKey(entry.getKey())) {
    	            	callLogMap.put(entry.getKey(), entry.getValue());
    	            }
    	        }
    	        callLogService.saveCallLog(callLog);
    		}
    		String prospectCallId = request.getParameter("pcid");
    		String isAbandoned = request.getParameter("abandoned");
    		String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
    		
    		if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
    			return response; //just return, no further processing needed
    		} else if (isAbandoned != null) { //prospect was abandoned
    			prospectCallLogService.updateCallStatus(prospectCallId, ProspectCallStatus.CALL_ABANDONED, paramMap.get("CallSid"), null, null);
    		} else if (paramMap.get("AnsweredBy") != null && paramMap.get("AnsweredBy").equalsIgnoreCase("machine")) {
    			prospectCallLogService.updateCallStatus(prospectCallId, ProspectCallStatus.CALL_MACHINE_ANSWERED, paramMap.get("CallSid"), null, null);
    			//response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>"+XtaasConstants.TWILIO_VM_LEFT_BY_DIALER+"</Say></Response>" ; //It is illegal to leave a VOICEMAIL in case picked up by MACHINE, decided after discussion with Gina
    		} else {
    			String caller = paramMap.get("Caller");
    			if (!caller.startsWith("client:")) { //twilio caller is a outbound number for outgoing calls. Caller is client: for calls initiated by agent.
    				String agentId = paramMap.get("agentid"); //hangup agent call if prospect has disconnected
    				
    				if (agentId != null) {
	    				//Fixed the issue where hold was disconnecting the call
	    				List<AgentActivityLog> agentLastCallStatusList = agentActivityLogRepository.getLastCallStatus(agentId, prospectCallId, PageRequest.of(0, 1, Direction.DESC, "createdDate"));
	    				AgentActivityLog agentLastCallStatus = null;
	    				if (agentLastCallStatusList != null && agentLastCallStatusList.size() > 0) {
	    					agentLastCallStatus = agentLastCallStatusList.get(0);
	    				}
	    				if (agentLastCallStatus != null && !agentLastCallStatus.getStatus().equals(AgentActivityCallStatus.CALL_ONHOLD.name())) { //disconnect only when call is not on hold
	    					logger.debug("saveCallMetrics() : Call is ended by prospect.");
	    					pushMessageToAgent(agentId, "call_disconnect", ""); 
	    					// 25-10-2018 => removed CALL_INPROGRESS, CALL_COMPLETE entry on the request of Manish
	    					// prospectCallLogService.updateCallStatus(prospectCallId, ProspectCallStatus.CALL_COMPLETE, paramMap.get("CallSid"), null, null);
	    					// prospectCallLogService.updateRecordingUrlByCallSid(paramMap.get("CallSid"));
	    				}
    				}
    			}
    		}
    	} catch (Exception ex) {
    		logger.error("saveCallMetrics() : Exception occurred. Call Logs could not be saved. ", ex);
    	}
    	
    	return response;
    }
    
    /**
     * This url requested by Twilio on receipt of incoming call to XTaaS number
     */
    @RequestMapping(value = "/handleincoming", method = RequestMethod.POST, produces = "text/xml")
    public @ResponseBody String handleIncoming(HttpServletRequest request) {
    	//print all the params recd from TWILIO
    	HashMap<String, String> paramMap = new HashMap<String, String>();
    	for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
    		String param = params.nextElement();
    		logger.debug(param + " = " + request.getParameter(param));
    		paramMap.put(param, request.getParameter(param));
    	}
    	
    	String toNumber = request.getParameter("To");
    	toNumber = StringUtils.replace(toNumber, "+", "");
    	String message = "Hi, You have reached the sales associate voicemail at " + toNumber + ". We are terribly sorry we missed your call, please leave us a message and we will return your call as soon as we can.  We hope you enjoy the rest of your day!";
    	
    	OutboundNumber outboundNumber = outboundNumberRepository.findOneById(toNumber);
    	if (outboundNumber != null && outboundNumber.getVoiceMessage() != null && !outboundNumber.getVoiceMessage().isEmpty()) {
    		message = outboundNumber.getVoiceMessage();
    	}
    	
    	// Create a TwiML response and add our friendly message.
        TwiMLResponse twiml = new TwiMLResponse();
        
        Say say = new Say(message);
        //say.setVoice("alice");
        
        // Record the caller's voice.
        Record record = new Record();
        record.setMaxLength(120);
        record.setAction("/spr/dialer/saveincomingcallmsg");
        try {
            twiml.append(say);
            twiml.append(record);
        } catch (TwiMLException e) {
            e.printStackTrace();
        }
 
    	return twiml.toXML();
    }
    
	@RequestMapping(value = "/saveincomingcallmsg", method = RequestMethod.POST, produces = "text/xml")
	public @ResponseBody String saveIncomingCallMsg(HttpServletRequest request) {
		// log all the params recd from TWILIO
		HashMap<String, String> paramMap = new HashMap<String, String>();
		for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
			String param = params.nextElement();
			logger.debug(param + " = " + request.getParameter(param));
			paramMap.put(param, request.getParameter(param));
		}

		String recordingUrl = request.getParameter("RecordingUrl");
		String callSid = request.getParameter("CallSid");
		TwiMLResponse twiml = new TwiMLResponse();
		try {
			if (recordingUrl != null) {
				String fromNumber = request.getParameter("From");
				String tempToNumber = request.getParameter("To");
				String toNumber = tempToNumber.substring(1); // removing + sing from toNumber
				OutboundNumber outboundNumber = outboundNumberRepository.findOneById(toNumber);
				String partnerId = null;
				if (outboundNumber != null) {
					partnerId = outboundNumber.getPartnerId();
				}
				logger.debug("parternerId == " + partnerId + "  toNumber = " + toNumber);
				/* Below code is to upload recordings to AWS S3 bucket. */
				InputStream inputStream = null;
				inputStream = new URL(recordingUrl).openStream();
				String fileName = request.getParameter("RecordingID");
				String fileFormat = recordingUrl.substring(recordingUrl.length() - 4, recordingUrl.length());
				fileName = fileName + fileFormat;
				downloadRecordingsToAws.createFile(XtaasConstants.XTAAS_RECORDING_BUCKET_NAME, fileName, inputStream,
						true);
				logger.debug("saveIncomingCallMsg() : Successfully uploaded recording url [{}] to AWS S3 bucket.",
						recordingUrl);
				VoiceMail voicemail = new VoiceMail(fromNumber, toNumber, callSid, recordingUrl, partnerId,
						XtaasConstants.TWILIO);
				voicemail.setCallParams(paramMap);
				voicemail.setMessageStatus(VoiceMessageStatus.NEW);
				voicemailService.add(voicemail);
				logger.debug("saveIncomingCallMsg() : Saved recording url [{}] in DB.", recordingUrl);
				twiml.append(new Say("Thanks for your message. Goodbye"));
			} else {
				twiml.append(new Hangup());
			}
		} catch (TwiMLException e) {
			e.printStackTrace();
			logger.error("saveIncomingCallMsg() : Error occurred: ", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return twiml.toXML();
	}
    
    @RequestMapping(value = "/callanswered", method = RequestMethod.POST, produces = "text/xml")
    public @ResponseBody String handleCallAnswered(HttpServletRequest request) {
    	final HashMap<String, String> paramMap = new HashMap<String, String>();
    	for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements();) {
    		String param = params.nextElement();
    		logger.debug(param + " = " + request.getParameter(param));
    		paramMap.put(param, request.getParameter(param));
    	}
    	
    	final String finalAgentId = paramMap.get("agentId");
    	final String finalPcid = paramMap.get("pcid");
    	Thread thread = new Thread() {
            public void run() {
                logger.debug("run() : CallAnsweredStatusUpdater Thread started");
                
                try {
        	    	CallStatusDTO callStatusDTO = new CallStatusDTO();
        			callStatusDTO.setCallStatus(CallState.CONNECTED);
        			callStatusDTO.setCallStartTime(new Date());
        			agentService.changeCallStatus(finalAgentId, callStatusDTO);
        			
        			//In preview mode, change the prospect call status as INPROGRESS when call is picked by prospect 
        			prospectCallLogService.updateCallDetails(finalPcid, ProspectCallStatus.CALL_INPROGRESS, paramMap.get("CallSid"), null, finalAgentId, null);
            	} catch (Exception ex) {
            		logger.error("handleCallAnswered(): Error occurred while updating call details in prospectcalllog and prospectcallinteraction for pcid: " + finalPcid , ex);
            	}
                
                logger.debug("run() : CallAnsweredStatusUpdater Thread stopped");
            } //end run()
        };
        thread.start();
    	
    	//Prepare the response
        TwiMLResponse twiml = new TwiMLResponse();
    	try {
    		String msg = request.getParameter("msg");
    		if (msg != null) {
    			twiml.append(new Say(msg));
    		}
    		
    		String actionUrl = "/spr/dialer/savecallmetrics?pcid="+paramMap.get("pcid")+"&amp;agentid="+finalAgentId;
    		Dial dial = new Dial();
    		dial.setAction(actionUrl);
    		
    		String conferenceName = finalAgentId + "_" + finalPcid;
    		Conference conf = new Conference(conferenceName);
    		conf.setBeep(false);
    		conf.setEndConferenceOnExit(false);
    		conf.setWaitUrl("");
    		
    		dial.append(conf);
    		twiml.append(dial);
		} catch (TwiMLException e) {
			logger.error("handleCallAnswered() : Error occurred: ", e);
		}
    	
    	String responseXML = twiml.toXML();
    	logger.debug("handleCallAnswered() : Returning Twiml : {} ", responseXML);
    	return responseXML;
    }
    
    @RequestMapping(value = "/campaignrunstatus/{id}", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, String> getRunStatus(@PathVariable("id") String campaignId) {
    	ProspectCallInteraction prospectCallInteraction = prospectCallInteractionService.getRecentDialerProcessed(campaignId);
    	HashMap<String, String> responseMap = new HashMap<String, String>();
    	if (prospectCallInteraction != null) {
    		responseMap.put("RECENT_PROCESSED_PROSPECT_STATUS", prospectCallInteraction.getStatus().toString());
    		responseMap.put("RECENT_PROCESSED_PROSPECT_TIME", prospectCallInteraction.getUpdatedDate().getTime()+"");
    	}
    	return responseMap;
    }
    
    private void pushMessageToAgent(String agentId, String event, String message) {
		logger.debug("pushMessageToAgent() : Event = [{}], Message = [{}], Agent = " + agentId , event, message);
		try {
			Pusher.triggerPush(agentId, event, message);
		} catch (IOException e) {
			logger.error("pushMessageToAgent() : Error occurred while pushing message to agent: " + agentId + " Msg:" + message, e);
		}
	}
}
