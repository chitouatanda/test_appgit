package com.xtaas.mvc.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.SignalwireOutboundNumberService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.entity.SignalwireOutboundNumber;
import com.xtaas.exception.UnauthorizedAccessException;
import com.xtaas.service.AgentRegistrationService;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.DialerService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.service.SignalwireService;
import com.xtaas.service.TranscriptionInterpretationService;
import com.xtaas.service.UserService;
import com.xtaas.utils.EncryptionUtils;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.PlivoPhoneNumberPurchaseDTO;
import com.xtaas.web.dto.RedialOutboundNumberDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SignalwireController {

	private final Logger logger = LoggerFactory.getLogger(SignalwireController.class);

	private final static String endpointUrl = "https://xtaascorp.signalwire.com/api/relay/rest/endpoints/sip";

	@Autowired
	SignalwireService signalwireService;

	@Autowired
	private SignalwireOutboundNumberService signalwireOutboundNumberService;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/voice", produces = "application/xml")
	public ResponseEntity<String> handleVoiceCall(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("SignalwireController ==> request to handle voice call.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String xmlString = signalwireService.handleVoiceCall(request, response);
		return new ResponseEntity<>(xmlString, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/voicestatus", produces = "application/xml")
	public void handleVoiceStatusCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("SignalwireController request to handle voice statuscallback.");
		signalwireService.handleVoiceStatusCallback(request, response);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/signalwire/endpoint/{username}")
	public ResponseEntity<Map<String, String>> createEndpointForAgent(@PathVariable String username) {
		logger.debug("Signalwire request to create endpoint.");
		final HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Map<String, String> endpointCredentials = signalwireService.createEndpointForAgent(username);
		return new ResponseEntity<>(endpointCredentials, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/recordingstatus")
	public void handleRecordingCallback(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Signalwire request to handle recording statuscallback.");
		signalwireService.handleRecordingCallback(request, response);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/signalwire/endpoint/{endpointId}")
	public ResponseEntity<String> deleteEndpointForAgent(@PathVariable String endpointId) {
		logger.info("Signalwire request to delete endpoint: " + endpointId);
		final HttpHeaders httpHeaders = new HttpHeaders();
		signalwireService.deleteEndpoint(endpointId);
		return new ResponseEntity<>("", httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/signalwire/outboundnumbers")
	public List<SignalwireOutboundNumber> getAllOutboundNumbers() {
		return signalwireOutboundNumberService.getAllOutboundNumbers();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/phonenumber/refresh")
	public ResponseEntity<String> refreshPhoneNumbers(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.info("Signalwire request to refresh phone number for {}.", plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		Login user = userService.getUser(plivoPhoneNumberPurchaseDTO.getUserName());
		if (user != null) {
			String salt = plivoPhoneNumberPurchaseDTO.getPassword() + "{" + plivoPhoneNumberPurchaseDTO.getUserName()
					+ "}";
			String passwordHash = EncryptionUtils.generateMd5Hash(salt);
			if (passwordHash.equalsIgnoreCase(user.getPassword())) {
				signalwireOutboundNumberService.refreshPhoneNumbers(plivoPhoneNumberPurchaseDTO);
				return new ResponseEntity<>("Please check youe email for signalwirenumber refresh result.", httpHeaders,
						HttpStatus.OK);
			} else {
				throw new UnauthorizedAccessException("Username or password is not valid.");
			}
		} else {
			throw new UnauthorizedAccessException("Username or password is not valid.");
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/phone/purchase")
	public ResponseEntity<String> purchasePhoneNumbers(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.debug("Signalwire request to purchase phone number for {}.", plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String signalwireOutboundNumbers = signalwireService.purchasePhoneNumbers(plivoPhoneNumberPurchaseDTO);
		return new ResponseEntity<>(signalwireOutboundNumbers, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/phone/purchase/other")
	public ResponseEntity<String> purchasePhoneNumbersForOther(
			@Valid @RequestBody PlivoPhoneNumberPurchaseDTO plivoPhoneNumberPurchaseDTO) {
		logger.debug("Signalwire request to purchase phone number for other countries {}.",
				plivoPhoneNumberPurchaseDTO);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		String signalwireOutboundNumbers = signalwireService
				.purchaseUSPhoneNumbersForOtherCountryUsingLimit(plivoPhoneNumberPurchaseDTO);
		return new ResponseEntity<>(signalwireOutboundNumbers, httpHeaders, HttpStatus.OK);
	}

	/**
	 * Added below API to fetch new outboundnumber foreach redial in case of manual
	 * dial.
	 * 
	 * @param redialNumberDTO
	 * @return Map<String, String>
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/signalwire/redial/outboundnumber")
	public Map<String, String> getOutboundnumber(@RequestBody RedialOutboundNumberDTO redialNumberDTO) {
		logger.debug("Plivo request to get new outboundnumber for each redial.");
		return signalwireService.getSignalwireOutboundNumberForRedial(redialNumberDTO);
	}

}
