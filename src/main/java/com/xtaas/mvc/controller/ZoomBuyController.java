package com.xtaas.mvc.controller;

import java.io.IOException;
import java.util.*;

import com.xtaas.web.dto.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xtaas.application.service.CampaignService;
import com.xtaas.application.service.SourceTypeService;
import com.xtaas.application.service.CampaignContactServiceImpl.FileUploadTracker;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.dialer.DialerThreadPool;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.valueobjects.KeyValuePair;

@RestController
public class ZoomBuyController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ZoomBuyController.class);

	@Autowired
	private DialerThreadPool dialerThreadPool;

	@Autowired
	public ZoomInfoContactListProviderNewImpl zoomInfoContactListProviderNewImpl;

	@Autowired
	SourceTypeService sourceTypeService;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private CampaignService campaignService;

	@RequestMapping(value = "/zoombuy", method = RequestMethod.POST)
	public ResponseEntity<String> purchaseZoomData(@RequestBody ZoomBuySearchDTO zoomBuySearchDTO) {
		zoomBuySearchDTO.setIgnoreDefaultIndustries(true); // Ignore Default Industries
		zoomBuySearchDTO.setIgnoreDefaultTitles(true); // Ignore Default titles
		zoomBuySearchDTO.setIgnoreANDTitles(true); // Ignore AND titles
		zoomBuySearchDTO.setParallelBuy(true);

		DataBuyQueue dbInProcess = dataBuyQueueRepository.findByCampaignIdAndStatus(zoomBuySearchDTO.getCampaignId(),
				DataBuyQueueStatus.INPROCESS.toString());
		DataBuyQueue dbInQueued = dataBuyQueueRepository.findByCampaignIdAndStatus(zoomBuySearchDTO.getCampaignId(),
				DataBuyQueueStatus.QUEUED.toString());
		if (dbInProcess != null || dbInQueued != null) {
			return new ResponseEntity<>("Data Buy Already in Process Please Check After Sometime.", HttpStatus.BAD_REQUEST);
		}

		Campaign campaign = campaignService.getCampaign(zoomBuySearchDTO.getCampaignId());
		zoomBuySearchDTO.setIgnorePrimaryIndustries(campaign.isIgnorePrimaryIndustries());

		DataBuyQueue dbq = new DataBuyQueue();
		dbq.setCampaignId(campaign.getId());
		dbq.setCampaignName(campaign.getName());
		dbq.setStatus(DataBuyQueueStatus.QUEUED.toString());
		dbq.setBuyProspect(true);
		dbq.setJobRequestTime(new Date());
		dbq.setRequestCount(zoomBuySearchDTO.getTotalCount());
		dbq.setOrganizationId(zoomBuySearchDTO.getOrganizationId());
		// dbq.setSalutaryRequestCount(Long.parseLong("2"));

		// ****************************************************************
		List<Expression> expression = new ArrayList<Expression>();
		List<HealthChecksCriteriaDTO> criteria = zoomBuySearchDTO.getCriteria();
		for (HealthChecksCriteriaDTO dto : criteria) {
			//Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), dto.getValue(), false, "ABC",
			List<String> listValue = new ArrayList<String>(Arrays.asList(dto.getValue().split(",")));
			if(listValue.size() > 1 && listValue.contains("All")) {
				listValue.remove("All");
			}
			Expression exp = new Expression(dto.getAttribute(), dto.getOperator(), StringUtils.join(listValue, ','), false, "ABC",
					dto.getPickList(), "");
			// Expression exp = new Expression(attribute, operator, value,
			// agentValidationRequired, questionSkin, pickList, sequenceNumber);
			expression.add(exp);
		}

		dbq.setCampaignCriteriaDTO(expression);
		dataBuyQueueRepository.save(dbq);

		// ZoomInfoContactListProviderNewImpl zilp = new
		// ZoomInfoContactListProviderNewImpl(zoomBuySearchDTO);
		// zoomInfoContactListProviderNewImpl.PurchaseRecords(zoomBuySearchDTO);
		// dialerThreadPool.scheduleJob(zilp, 5);
		// Thread t1 = new Thread(zilp, "t" + Math.random());
		logger.info("Starting");
		// t1.start();

		logger.info("started");
		return null;
	}


	@RequestMapping(value = "/sourceType/{campaignId}", method = RequestMethod.GET)
	public void updateDate(@PathVariable("campaignId") String campaignId) {
		sourceTypeService.stampSourceTypeInProspect(campaignId);
	}

	@RequestMapping(value = "/campaign/databuy/status/{campaignId}", method = RequestMethod.GET)
	public DataBuyStatusDTO getCampaignDatabuyStatus(@PathVariable("campaignId") String campaignId) {
		DataBuyStatusDTO dto = new DataBuyStatusDTO();
		List<String> status = new ArrayList<String>();
		status.add(DataBuyQueueStatus.QUEUED.toString());
		status.add(DataBuyQueueStatus.INPROCESS.toString());
		List<DataBuyQueue> dbList = dataBuyQueueRepository.findByStatusAndCampaignId(campaignId, status);
		if (dbList != null && dbList.size() > 0) {
			dto.setDataBuyQueueId(dbList.get(0).getId());
			dto.setStatus("Cancel Buy");
			dto.setBuyProspect(dbList.get(0).isBuyProspect());
			dto.setSearchCompany(dbList.get(0).isSearchCompany());
			return dto;
		} else {
			try{
				List<String> completeStatus = new ArrayList<String>();
				completeStatus.add(DataBuyQueueStatus.COMPLETED.toString());
				List<DataBuyQueue> completeDBQList = dataBuyQueueRepository.findByStatusAndCampaignId(campaignId, completeStatus);
				if (completeDBQList != null && completeDBQList.size() > 0) {
					dto.setDataBuyQueueId(completeDBQList.get(completeDBQList.size()-1).getId());
					dto.setStatus("Data Buy");
					dto.setBuyProspect(completeDBQList.get(completeDBQList.size()-1).isBuyProspect());
					dto.setSearchCompany(completeDBQList.get(completeDBQList.size()-1).isSearchCompany());
				} else{
					dto.setDataBuyQueueId("NA");
					dto.setStatus("Data Buy");
					dto.setBuyProspect(completeDBQList.get(completeDBQList.size()-1).isBuyProspect());
					dto.setSearchCompany(completeDBQList.get(completeDBQList.size()-1).isSearchCompany());
				}
			}catch (Exception e){

			}
			return dto;
		}
	}

	@RequestMapping(value = "/campaign/databuy/stop/{campaignId}", method = RequestMethod.GET)
	public DataBuyStatusDTO getCampaignDatabuyStop(@PathVariable("campaignId") String campaignId) {
		DataBuyStatusDTO dto = new DataBuyStatusDTO();
		List<String> status = new ArrayList<String>();
		status.add(DataBuyQueueStatus.QUEUED.toString());
		status.add(DataBuyQueueStatus.INPROCESS.toString());
		List<DataBuyQueue> dbList = dataBuyQueueRepository.findByStatusAndCampaignId(campaignId, status);
		if (dbList != null && dbList.size() > 0) {
			for (DataBuyQueue dbq : dbList) {
				dbq.setStatus(DataBuyQueueStatus.KILL.toString());
				dataBuyQueueRepository.save(dbq);
			}
		}
		dto.setDataBuyQueueId("NA");
		dto.setStatus("Data Buy");
		return dto;
	}

	@RequestMapping(value = "/campaign/databuy/process/{campaignId}/{id}", method = RequestMethod.GET)
	public @ResponseBody
	DataBuyTracker getDataBuyStatus(@PathVariable("campaignId") String campaignId,
									@PathVariable("id") String id) throws IOException {
		int totalRequestCount = 0;
		int totalPurchasedCount = 0;
		int percentage = 0;
		DataBuyTracker dto = new DataBuyTracker();
		DataBuyQueue dbQueue = dataBuyQueueRepository.findByCampaignIdAndId(campaignId, id);
		if(dbQueue == null) {
			List<DataBuyQueue> dbqList = dataBuyQueueRepository.findDbqBasedOnCampaign(campaignId);
			if (CollectionUtils.isNotEmpty(dbqList)) {
				dbQueue= dbqList.get(dbqList.size()-1);
				dto.setDataBuyStatus(DataBuyQueueStatus.COMPLETED.toString());
			}
		} else {
			dto.setDataBuyStatus(dbQueue.getStatus());
		}
		if (dbQueue != null && dbQueue.isBuyProspect()) {

			if (dbQueue.getRequestCount() != null) {
				totalRequestCount = totalRequestCount + dbQueue.getRequestCount().intValue();
			}
			if (dbQueue.getTotalBought() != null) {
				totalPurchasedCount = totalPurchasedCount + dbQueue.getTotalBought().intValue();
			}
			try {
				percentage = ((totalPurchasedCount * 100) / totalRequestCount);
			} catch (ArithmeticException e) {

			}
			if (percentage == 0) {
				dto.setPercentage(2);
			} else {
				dto.setPercentage(percentage);
			}
			if (dbQueue.getRequestCount() != null) {
				dto.setTotalRequestCount(dbQueue.getRequestCount().intValue());
			}
			dto.setTotalPurchasedCount(totalPurchasedCount);
		}
		return dto;
	}


	@RequestMapping(value = "/campaign/databuy/dbq/{campaignId}", method = RequestMethod.GET)
	public DataBuyQueueExpDTO getCampaignDbq(@PathVariable("campaignId") String campaignId) {
		DataBuyQueueExpDTO dto = new DataBuyQueueExpDTO();
		List<Expression> expressions = null;
		DataBuyQueue dbq = dataBuyQueueRepository.findByCampaignAndSortByJobRequestTime(campaignId);
		if (dbq != null && dbq.getCampaignCriteriaDTO() != null) {
			expressions = dbq.getCampaignCriteriaDTO();
		}
		dto.setExpressions(expressions);
		return dto;
	}

}
