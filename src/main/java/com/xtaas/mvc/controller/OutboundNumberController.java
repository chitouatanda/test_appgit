package com.xtaas.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.OutboundNumberService;
import com.xtaas.domain.entity.OutboundNumber;
import com.xtaas.domain.valueobject.CountryDetails;

@RestController
public class OutboundNumberController {

	@Autowired
	private OutboundNumberService outboundNumberService;

	@RequestMapping(value = "/outboundnumbers", method = RequestMethod.GET)
	public List<OutboundNumber> getAllOutboundNumbers() {
		return outboundNumberService.getAllOutboundNumbers();
	}

	@RequestMapping(value = "/outboundnumbers/countrydetails", method = RequestMethod.GET)
	public List<CountryDetails> getCountryInformation() {
		return outboundNumberService.getDistinctCountryInformation();
	}

}