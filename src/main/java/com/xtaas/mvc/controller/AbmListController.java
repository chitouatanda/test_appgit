package com.xtaas.mvc.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.util.Sleeper;
import com.xtaas.db.entity.AbmListNew;
import com.xtaas.db.entity.Login;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.SupervisorStatsRepository;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.SupervisorStats;
import com.xtaas.service.AbmListService;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.EmailMessageDTO;

@RestController
public class AbmListController {

	@Autowired
	private AbmListService abmListService;

	@Autowired
	private SupervisorStatsRepository supervisorStatsRepository;
	
	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;
	
	@RequestMapping(value = "/abmlist/downloadTemplate", method = RequestMethod.GET)
	public ResponseEntity<?> downloadTemplate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("campaignId") String campaignId) throws IOException {
		abmListService.downloadABMListTemplate(request, response, campaignId);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/abmlist/upload/{campaignId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public @ResponseBody String uploadCampaignABMFile(@PathVariable("campaignId") String campaignId,
			@RequestParam("file") MultipartFile file) throws IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		String message = abmListService.uploadCampaignABMFile(campaignId, file, login);
		return message;
	}
	
	@RequestMapping(value = "/abmlist/count/{campaignId}", method = RequestMethod.GET)
	public String retriveABMListCount(@PathVariable("campaignId") String campaignId) {
		return abmListService.getABMListCountOfCampaign(campaignId);
	}
	
	@RequestMapping(value = "/abmlist/remove/{campaignId}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> removeABMList(@PathVariable("campaignId") String campaignId) {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		List<String> status = new ArrayList<String>();
		status.add(DataBuyQueueStatus.QUEUED.toString());
		status.add(DataBuyQueueStatus.INPROCESS.toString());
		List<DataBuyQueue> dbList = dataBuyQueueRepository.findByStatusAndCampaignId(campaignId,status);
		if(dbList != null && dbList.size() > 0) {
			//throw new IllegalArgumentException("ABM remove already in progress, please try later.");
			
			DataBuyQueue dbq = dbList.get(0);
			dbq.setStatus(DataBuyQueueStatus.KILL.toString());
			dataBuyQueueRepository.save(dbq);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		SupervisorStats supervisorStats = supervisorStatsRepository.findBycampaignManagerIdAndcampaignId(login.getId(),campaignId,"AbmDelete", "INPROCESS");
		if(supervisorStats == null) {
			SupervisorStats tempSupervisorStats = new SupervisorStats();
			tempSupervisorStats.setCampaignManagerId(login.getId());
			tempSupervisorStats.setCampaignId(campaignId);
			tempSupervisorStats.setStatus("INPROCESS");
			tempSupervisorStats.setDate(new Date());
			tempSupervisorStats.setType("AbmDelete");
			supervisorStatsRepository.save(tempSupervisorStats);
		} else {
			throw new IllegalArgumentException("ABM remove already in progress, please try later.");
		}
		abmListService.removeABMListByCampaignId(campaignId);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@RequestMapping(value = "/abmlist/getlist/{campaignId}/{companyName}", method = RequestMethod.GET)
	public List<AbmListNew> searchABMListByCompanyName(@PathVariable("campaignId") String campaignId,
			@PathVariable("companyName") String companyName) {
		return abmListService.searchABMListByCompanyName(campaignId,companyName);
	}
	
	@RequestMapping(value = "/abmlist/bypagination/{campaignId}/{pageSize}", method = RequestMethod.GET)
	public List<AbmListNew> getABMListByPagination(@PathVariable("campaignId") String campaignId,
			@PathVariable("pageSize") int pageSize) {
		return abmListService.getABMListByPagination(campaignId, pageSize);
	}
	
	@RequestMapping(value = "/abmlist/{campaignId}", method = RequestMethod.GET)
	public @ResponseBody String multiDownloadABMlist(@PathVariable("campaignId") String campaignId) throws FileNotFoundException, IOException {
		Login login = XtaasUserUtils.getCurrentLoggedInUserObject();
		return abmListService.multiDownloadABMlistCheckAndSave(campaignId, login);
	}
	
}
