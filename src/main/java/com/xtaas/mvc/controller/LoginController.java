/**
 * 
 */
package com.xtaas.mvc.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author djain
 *
 */
@RestController
public class LoginController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	/*@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String uname = request.getParameter("j_username");
		//check if user is already logged in and throw Exception if now logging in with a different user 
		String currentLoginUsername = XtaasUserUtils.getCurrentLoggedInUsername(); 
		if (currentLoginUsername != null && !currentLoginUsername.equals("anonymousUser") && !uname.equals(currentLoginUsername)) {
			logger.error("You are already logged as user " + currentLoginUsername);
			return new ResponseEntity<String>("{\"Error\":\"You are already logged in as user "+ currentLoginUsername + "\"}", HttpStatus.CONFLICT);
		} else {
			//return "forward:/j_spring_security_check";
			request.getRequestDispatcher("/j_spring_security_check").forward(request, response);
			return null;
		}
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/j_spring_security_logout").forward(request, response);
		//return "forward:/j_spring_security_logout";
	}*/
	
	@RequestMapping(value = "/loginexpired", method = RequestMethod.GET)
    public ResponseEntity<String> handleConcurrentSessions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return new ResponseEntity<String>("CONCURRENT_SESSION", HttpStatus.UNAUTHORIZED);
	}
}
