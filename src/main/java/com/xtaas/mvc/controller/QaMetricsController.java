package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.application.service.QaMetricsService;
import com.xtaas.application.service.QaMetricsServiceImpl.QaMetrics;
import com.xtaas.utils.JSONUtils;
import com.xtaas.web.dto.ProspectCallSearchDTO;

@RestController
@RequestMapping(value = "/qa/metrics")
public class QaMetricsController {

	@Autowired
	private QaMetricsService qaMetricsService;
	
	@RequestMapping(method = RequestMethod.GET)
	public HashMap<QaMetrics, Integer> getQaMetrics(@RequestParam("searchstring") String searchstring) throws JsonParseException, JsonMappingException, IOException, ParseException {
		
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		switch (prospectCallSearchDTO.getClause()) {
			case ByDispositionStatus:
				return qaMetricsService.getQaMetricsByDispositionStatus(prospectCallSearchDTO);
			case ByProspectCallStatus:
				return qaMetricsService.getQaMetricsByProspectCallStatus(prospectCallSearchDTO);
			case ByQaStat:
				return qaMetricsService.getQaMetrics(prospectCallSearchDTO);
			case BySecondaryQaStat:
				return qaMetricsService.getSecondaryQaMetrics(prospectCallSearchDTO);
			default:
				throw new NotImplementedException("Invalid Search Clause specified: " + prospectCallSearchDTO.getClause());
		} 
	}
	
	@RequestMapping(value = "/scoredrate", method = RequestMethod.GET)
	public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate(){
		return qaMetricsService.getQaMetricsByScoreRate();
	}
	
}
