package com.xtaas.mvc.controller;

import javax.validation.Valid;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.CampaignService;
import com.xtaas.web.dto.CampaignCommandDTO;

@RestController
@RequestMapping(value = "/campaign/{campaignId}/campaigncommand")
public class CampaignCommandController {
	@Autowired
	private CampaignService campaignService;
	
	@RequestMapping(method = RequestMethod.POST)
	public void command(@PathVariable("campaignId") String campaignId, @RequestBody @Valid CampaignCommandDTO command) {
		switch (command.getCampaignCommand()) {
		case Plan: 
			campaignService.planCampaign(campaignId);
			break;
		case Run: 
			campaignService.runCampaign(campaignId);
			break;
		case Pause: 
			campaignService.pauseCamaign(campaignId);
			break;
		case Stop:
			campaignService.stopCampaign(campaignId);
			break;
		case Resume:
			campaignService.resumeCampaign(campaignId);
			break;
		case Complete:
            campaignService.completeCampaign(campaignId);
            break;
        case Delist:
            campaignService.delistCampaign(campaignId);
            break;
        case RunReady:
            campaignService.runReadyCampaign(campaignId);
            break;
        // 23/11/2018 - added for supervisor screen. 
        case Running:
        	campaignService.runningCampaign(campaignId);
        	break;
		default:
			throw new NotImplementedException("Campaign cannot be ordered for any other command other than run, resume, pause, stop");

		}
	}
}
