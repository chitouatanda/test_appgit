package com.xtaas.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.service.PlivoService;
import com.xtaas.service.ProspectCallLogService;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.DncSearchDTO;
import com.xtaas.web.dto.ProspectCallDTO;
import com.xtaas.web.dto.ProspectCallLogUpdateDTO;

@RestController
public class ProspectCallLogController {

	@Autowired
	private ProspectCallLogService prospectLogService;
	
	@Autowired
	private PlivoService plivoService;

	@RequestMapping(method = RequestMethod.PUT, value = "/api/prospectcalllog/update")
	public void updateProspectCallLog(@RequestBody ProspectCallLogUpdateDTO prospectCallLogUpdateDTO) {
		if (prospectCallLogUpdateDTO.getCallbackDate() != null) {
			prospectLogService.updateCallbackStatusSystemGenerated(prospectCallLogUpdateDTO.getProspectCallId(),
					prospectCallLogUpdateDTO.getCallbackDate());
		} else {
			if (prospectCallLogUpdateDTO.getProspectInteractionSessionId() != null) {
				prospectLogService.updateInteractionSessionIdAndOutboundNumber(
						prospectCallLogUpdateDTO.getProspectCallId(),
						prospectCallLogUpdateDTO.getProspectInteractionSessionId(),
						prospectCallLogUpdateDTO.getOutboundNumber());
			}
			prospectLogService.updateCallStatus(prospectCallLogUpdateDTO.getProspectCallId(),
					prospectCallLogUpdateDTO.getProspectCallStatus(), prospectCallLogUpdateDTO.getTwilioCallSid(),
					prospectCallLogUpdateDTO.getCallRetryCount(), prospectCallLogUpdateDTO.getDailyCallRetryCount());
			// plivoService.removePcidFromMap(prospectCallLogUpdateDTO.getProspectCallId());
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/prospectcalllog/create")
	public ProspectCallDTO createProspect(@RequestBody ProspectCallDTO prospectCallDTO) {
		return prospectLogService.createProspect(prospectCallDTO);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/prospectcalllog/dnc")
	public boolean checkLocalDNC(@RequestBody DncSearchDTO dncSearchDTO) {
		return prospectLogService.checkLocalDNCList(dncSearchDTO.getFirstName(), dncSearchDTO.getLastName(),
				dncSearchDTO.getPhone(), dncSearchDTO.getCompanyDomain(), dncSearchDTO.isUSorCanada(),
				dncSearchDTO.getPartnerId(), null, null);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/prospectcalllog/getbackcallable")
	public int getbackCallableProspects(@RequestBody CallableGetbackCriteriaDTO criteriaDTO) {
		return prospectLogService.getbackCallableProspects(criteriaDTO);
	}

}
