package com.xtaas.mvc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.Organization;
import com.xtaas.service.OrganizationService;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.OrganizationDTO;

@RestController 
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;
	
	@RequestMapping(value = "/api/organization", method = RequestMethod.POST)
	public Organization create(@Valid @RequestBody OrganizationDTO organizationDTO) {
		try {
			return organizationService.createOrganization(organizationDTO);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.organization.organizationName"); 
		}
	}
	
	@RequestMapping(value = {"/api/organization/{id}", "/organization/{id}"}, method = RequestMethod.GET)
	public Organization retrieve(@PathVariable("id") String organizationId) {
		return organizationService.findOne(organizationId);
	}
}
