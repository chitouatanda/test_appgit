package com.xtaas.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.application.service.QaService;
import com.xtaas.db.entity.Login;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.QaDetails;
import com.xtaas.utils.JSONUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.ProspectCallQaSearchResult;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.QaDTO;
import com.xtaas.web.dto.QaFeedbackDTO;

@RestController
public class QaController {
	@Autowired
	private QaService qaService;
	
	@RequestMapping(value = "/qa/prospectcall/search", method = RequestMethod.GET)
	public ProspectCallQaSearchResult retrieveAll(@RequestParam("searchstring") String searchstring) throws IOException, ParseException {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		switch (prospectCallSearchDTO.getClause()) {
		case ByQa:
			return qaService.searchProspectCallsByQa (prospectCallSearchDTO);
		
		case ByQa2:
			return qaService.searchProspectCallsBySecQa (prospectCallSearchDTO);
			
		case ByAgent:
			return qaService.searchProspectCallsByAgent (prospectCallSearchDTO);
		
		default:
			throw new NotImplementedException();
		}
	}

	@RequestMapping(value = "/api/qa", method = RequestMethod.POST)
	public QaDTO create(@Valid @RequestBody QaDTO qaDTO) {
		System.out.println("QA Controller: " + qaDTO);
		try {
			return qaService.createQa(qaDTO);
		} catch (DuplicateKeyException e) {
			throw new IllegalArgumentException("Duplicate.Qa.username"); 
		}
	}
	@RequestMapping(value = "/api/qa/{qaId:.+}", method = RequestMethod.PUT)
	public QaDTO update(@PathVariable("qaId") String qaId, @Valid @RequestBody QaDTO qaDTO) {
		return qaService.updateQa(qaId, qaDTO);
	}

	@RequestMapping(value = "/qa/{prospectCallId}", method = RequestMethod.GET)
	public QaDetails getDetails(@PathVariable("prospectCallId") String prospectCallId) {
		return qaService.getProspectCallForQA(prospectCallId);
	}
	
	/* DATE : 05/06/2017	REASON : added secondaryQaReview flag to handle list of records for review  */
	@RequestMapping(value = "/qa/{prospectCallId}/{secondaryQaReview}/savefeedback", method = RequestMethod.POST)
	public ResponseEntity<Boolean> saveFeedback(@PathVariable("prospectCallId") String prospectCallId, @PathVariable("secondaryQaReview") String secondaryQaReview, @Valid @RequestBody QaFeedbackDTO qaFeedbackDTO) {
		qaService.createFeedback(prospectCallId, secondaryQaReview, qaFeedbackDTO, false);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
		
	}
	
	/* DATE : 05/06/2017	REASON : added secondaryQaReview flag to handle list of records for review  */
	@RequestMapping(value = "/qa/{prospectCallId}/{secondaryQaReview}/submitfeedback", method = RequestMethod.POST)
	public ResponseEntity<Boolean> submitFeedback(@PathVariable("prospectCallId") String prospectCallId, @PathVariable("secondaryQaReview") String secondaryQaReview, @Valid @RequestBody QaFeedbackDTO qaFeedbackDTO) {
		qaService.createFeedback(prospectCallId, secondaryQaReview, qaFeedbackDTO, true);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
	}
	
	// added to send record to primary qa from secondary qa.
	@RequestMapping(value = "/qa/{prospectCallId}/returntoprimary", method = RequestMethod.POST)
	public ResponseEntity<Boolean> returnToPrimary(@PathVariable("prospectCallId") String prospectCallId, @Valid @RequestBody QaFeedbackDTO qaFeedbackDTO) {
		qaService.returnToPrimary(prospectCallId, qaFeedbackDTO);
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/secondaryqa/{organizationId}", method = RequestMethod.GET)
	public List<Team> getTeams(@PathVariable("organizationId") String organizationId) {
		return qaService.getOrganizationTeams(organizationId);
	}
	
	@RequestMapping(value = "/qa/search", method = RequestMethod.GET)
	public ProspectCallQaSearchResult searchProspect(@RequestParam("searchstring") String searchstring) throws IOException, ParseException {
		ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
		Login loggedInUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		if (loggedInUser.getRoles().contains("QA")) {
			return qaService.searchProspectCallsByQa(prospectCallSearchDTO);
		} else {
			return qaService.searchProspectCallsBySecQa(prospectCallSearchDTO);
		}
	}

	@RequestMapping(value = "/qa/login/firstTime", method = RequestMethod.GET)
	 public ProspectCallQaSearchResult getQADetails(@RequestParam("searchstring") String searchstring) throws IOException, ParseException {
	 	ProspectCallSearchDTO prospectCallSearchDTO = JSONUtils.toObject(searchstring, ProspectCallSearchDTO.class);
	 	Login loggedInUser = XtaasUserUtils.getCurrentLoggedInUserObject();
	 	if (loggedInUser.getRoles().contains("QA")) {
	 		return qaService.getQADetails(prospectCallSearchDTO);
	 	}else {
	 		return qaService.getSecQADetails(prospectCallSearchDTO);
	 	}
	 }
	
}
