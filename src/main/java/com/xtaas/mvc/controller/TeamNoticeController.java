package com.xtaas.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xtaas.db.entity.TeamNotice;
import com.xtaas.domain.service.TeamNoticeService;
import com.xtaas.web.dto.TeamNoticeDTO;

@RestController
public class TeamNoticeController {

	@Autowired
	private TeamNoticeService teamNoticeService;

	@RequestMapping(value = "/teamnotice/{userId}", method = RequestMethod.GET)
	public List<TeamNoticeDTO> getTeamNotices(@PathVariable("userId") String userId) {
        return teamNoticeService.getNotices(userId);
	}
	
	@RequestMapping(value = "/teamnotice", method = RequestMethod.GET)
	public List<TeamNotice> getTeamNotices() {
        return teamNoticeService.getTeamNotices();
	}
	
	@RequestMapping(value = "/teamnotice", method = RequestMethod.POST)
	public ResponseEntity<Boolean> saveTeamNotices(@RequestBody TeamNoticeDTO teamNoticeDTO) {
        teamNoticeService.saveNotices(teamNoticeDTO);
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/teamnotice/markAsRead", method = RequestMethod.POST)
	public ResponseEntity<Boolean> markNoticesAsRead(@RequestBody String userId) {
        teamNoticeService.markNoticesAsRead(userId);
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/teamnotice/{userId}/unreadcount", method = RequestMethod.GET)
	public String getTeamNoticesUnreadCount(@PathVariable("userId") String userId) {
        return teamNoticeService.getUnreadCount(userId);
	}
	
	@RequestMapping(value = "/teamnotice/{agentId}/supervisor/{mode}", method = RequestMethod.GET)
	public String notifySupervisor(@PathVariable("agentId") String agentId, @PathVariable("mode") String mode) {
        return teamNoticeService.notifySupervisor(agentId, mode);
	}
}
