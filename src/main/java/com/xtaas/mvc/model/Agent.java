package com.xtaas.mvc.model;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.entity.Campaign;

public class Agent {
    private Integer id;
    private String screenName;
    //list of campaigns assigned to this agent 
    private List<Campaign> assignedCampaigns;
    //Campaign currently being worked upon
    private Campaign currentCampaign;
    //Prospect to whom Agent has spoken 
    private Prospect prospect;
    //SID of the Call with the Prospect
    private String callSid;
    //SID of the Call initiated by Agent from their browser
    private String agentCallSid;
    //Closing Notes
    private String closingNote;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean isEqual = false;
		if (object == null || object.getClass() != getClass()) {
			isEqual = false;
		} else {
			Agent agent = (Agent) object;
			if (this.getScreenName().equalsIgnoreCase(agent.getScreenName())) {
				isEqual = true;
			}
		}
		return isEqual;
	}
	
	
	@Override
	public int hashCode() {
		return this.getScreenName().toLowerCase().hashCode();
	}
	
	public void cleanupCallDetails() {
		setCallSid(null);
		setProspect(null);
	}

	/**
	 * @return the assignedCampaigns
	 */
	public List<Campaign> getAssignedCampaigns() {
		return assignedCampaigns;
	}

	/**
	 * @param assignedCampaigns the assignedCampaigns to set
	 */
	public void setAssignedCampaigns(List<Campaign> assignedCampaigns) {
		this.assignedCampaigns = assignedCampaigns;
	}

	/**
	 * @return the currentCampaign
	 */
	public Campaign getCurrentCampaign() {
		return currentCampaign;
	}

	/**
	 * @param currentCampaign the currentCampaign to set
	 */
	public void setCurrentCampaign(Campaign currentCampaign) {
		this.currentCampaign = currentCampaign;
	}
	
	public String getCurrentCampaignQualificationCriteriaAsJson() throws IOException {
		return  new ObjectMapper().writeValueAsString(this.currentCampaign.getQualificationCriteria());
	}
	
	public String getCurrentCampaignInformationCriteriaAsJson() throws IOException {
		return  new ObjectMapper().writeValueAsString(this.currentCampaign.getInformationCriteria());
	}

	/**
	 * @return the prospect
	 */
	public Prospect getProspect() {
		return prospect;
	}

	/**
	 * @param prospect the prospect to set
	 */
	public void setProspect(Prospect prospect) {
		this.prospect = prospect;
	}

	/**
	 * @return the callSid
	 */
	public String getCallSid() {
		return callSid;
	}

	/**
	 * @param callSid the callSid to set
	 */
	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	/**
	 * @return the closingNote
	 */
	public String getClosingNote() {
		return closingNote;
	}

	/**
	 * @param closingNote the closingNote to set
	 */
	public void setClosingNote(String closingNote) {
		this.closingNote = closingNote;
	}

	/**
	 * @return the agentCallSid
	 */
	public String getAgentCallSid() {
		return agentCallSid;
	}

	/**
	 * @param agentCallSid the agentCallSid to set
	 */
	public void setAgentCallSid(String agentCallSid) {
		this.agentCallSid = agentCallSid;
	}
}
