package com.xtaas.mvc.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;

public class ProspectCallSearch {

	@JsonProperty
	private String agentName;
	
	@JsonProperty
	private String campaignName;
	
	@JsonProperty
	private String clientId;
	
	@JsonProperty
	private String teamName;
	
	@JsonProperty
	private ProspectCall prospectCall;
	
	@JsonProperty
	private ProspectCallStatus status;
	
	@JsonProperty
	private List<String> recordingUrl;
	
	@JsonProperty
	private Double overallScore;
	
	@JsonProperty
	private String qaId;
	
	@JsonProperty
	private String secQaId;

	/**
	 * @param agentName
	 * @param campaignName
	 * @param clientId
	 * @param teamName
	 * @param prospectCall
	 * @param status
	 * @param recordingUrl
	 * @param overallScore
	 */
	public ProspectCallSearch(String agentName, String campaignName,
			String clientId, String teamName, ProspectCall prospectCall,
			ProspectCallStatus status, List<String> recordingUrl,
			Double overallScore, String qaId, String secQaId) {
		this.agentName = agentName;
		this.campaignName = campaignName;
		this.clientId = clientId;
		this.teamName = teamName;
		this.prospectCall = prospectCall;
		this.status = status;
		this.recordingUrl = recordingUrl;
		this.overallScore = overallScore;
		this.qaId = qaId;
		this.secQaId = secQaId;
	}


	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
}
