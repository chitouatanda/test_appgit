package com.xtaas.mvc.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 5/25/14
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class ResultsPage<T> {
  private int pageSize;
  private int offset;
  private int foundRows;
  private List<T> results;

  @Override
  public String toString() {
    return "ResultsPage{" +
        ", pageSize=" + pageSize +
        ", offset=" + offset +
        ", foundRows=" + foundRows +
        ", results=" + results +
        '}';
  }

  public int getPageCount() {
    return pageSize==0 ? 0: (int)Math.ceil((double)foundRows/pageSize);
  }

  public int getCurrentPage() {
    return pageSize==0 ? 1 : 1+offset/pageSize;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public int getFoundRows() {
    return foundRows;
  }

  public void setFoundRows(int foundRows) {
    this.foundRows = foundRows;
  }

  public List<T> getResults() {
    return results;
  }

  public void setResults(List<T> results) {
    this.results = results;
  }
}
