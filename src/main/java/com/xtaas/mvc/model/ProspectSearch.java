package com.xtaas.mvc.model;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 4/30/14
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProspectSearch {
    private String campaignId;
    private String companies;
    private String[] industries;
    private String titles;
    private String[] categories;
    private String[] mgmtLevels;
    private String[] personIds;
    private String cities;
    private String states;
    private String zips;
    private String areacodes;
    private String revMin;
    private String revMax;
    private String empMin;
    private String empMax;
    private int preview = 1;
    private int offset = 0; //for pagination. begin at
    private int pageSize = 100; //for pagination.

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCompanies() {
        return companies;
    }

    public void setCompanies(String companies) {
        this.companies = companies;
    }

    public String[] getIndustries() {
        return industries;
    }

    public void setIndustries(String[] industries) {
        this.industries = industries;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String[] getMgmtLevels() {
        return mgmtLevels;
    }

    public void setMgmtLevels(String[] mgmtLevels) {
        this.mgmtLevels = mgmtLevels;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getZips() {
        return zips;
    }

    public void setZips(String zips) {
        this.zips = zips;
    }

    public String getAreacodes() {
        return areacodes;
    }

    public void setAreacodes(String areacodes) {
        this.areacodes = areacodes;
    }

    public String getRevMin() {
        return revMin;
    }

    public void setRevMin(String revMin) {
        this.revMin = revMin;
    }

    public String getRevMax() {
        return revMax;
    }

    public void setRevMax(String revMax) {
        this.revMax = revMax;
    }

    public String getEmpMin() {
        return empMin;
    }

    public void setEmpMin(String empMin) {
        this.empMin = empMin;
    }

    public String getEmpMax() {
        return empMax;
    }

    public void setEmpMax(String empMax) {
        this.empMax = empMax;
    }

    public String[] getPersonIds() {
        return personIds;
    }

    public void setPersonIds(String[] personIds) {
        this.personIds = personIds;
    }

    public int getPreview() {
        return preview;
    }

    public void setPreview(int preview) {
        this.preview = preview;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "ProspectSearch{" +
          "campaignId='" + campaignId + '\'' +
          ", companies='" + companies + '\'' +
          ", industries=" + Arrays.toString(industries) +
          ", titles='" + titles + '\'' +
          ", categories=" + Arrays.toString(categories) +
          ", mgmtLevels=" + Arrays.toString(mgmtLevels) +
          ", personIds=" + Arrays.toString(personIds) +
          ", cities='" + cities + '\'' +
          ", states='" + states + '\'' +
          ", zips='" + zips + '\'' +
          ", areacodes='" + areacodes + '\'' +
          ", revMin='" + revMin + '\'' +
          ", revMax='" + revMax + '\'' +
          ", empMin='" + empMin + '\'' +
          ", empMax='" + empMax + '\'' +
          ", preview=" + preview +
          ", offset=" + offset +
          ", pageSize=" + pageSize +
          "} " + super.toString();
    }
}
