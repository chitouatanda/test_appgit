package com.xtaas.mvc.model;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 4/30/14
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class IdLabel {
  private String id;
  private String label;

  @Override
  public String toString() {
    return "IdLabel{" +
      "id='" + id + '\'' +
      ", label='" + label + '\'' +
      "} " + super.toString();
  }

  public IdLabel(String id, String value) {
    this.id = id;
    this.label = value;
  }

  public String getId() {

    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
