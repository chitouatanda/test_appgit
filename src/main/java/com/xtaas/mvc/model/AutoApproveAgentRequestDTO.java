
package com.xtaas.mvc.model;

import java.util.Date;

import com.xtaas.domain.valueobject.AgentStatus;

public class AutoApproveAgentRequestDTO {

	private String campaignId;

	private String agentType;

	private AgentStatus agentStatus;

	private Date requestedTime;

	public AutoApproveAgentRequestDTO(String campaignId, String agentType, AgentStatus agentStatus,
			Date requestedTime) {
		super();
		this.campaignId = campaignId;
		this.agentType = agentType;
		this.agentStatus = agentStatus;
		this.requestedTime = requestedTime;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public AgentStatus getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(AgentStatus agentStatus) {
		this.agentStatus = agentStatus;
	}

	public Date getRequestedTime() {
		return requestedTime;
	}

	public void setRequestedTime(Date requestedTime) {
		this.requestedTime = requestedTime;
	}

	@Override
	public String toString() {
		return "AutoApproveAgentRequestDTO [campaignId=" + campaignId + ", agentType=" + agentType + ", agentStatus="
				+ agentStatus + ", requestedTime=" + requestedTime + "]";
	}

}