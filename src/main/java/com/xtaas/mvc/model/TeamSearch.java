package com.xtaas.mvc.model;

import com.xtaas.db.entity.Address;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hashimca
 * Date: 3/23/14
 * Time: 10:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TeamSearch {
  private String campaignId;
  private String campaignName;
  private String clientName;
  private int rating;
  private Address address;

  private List<String> skills;

  public String getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(String campaignId) {
    this.campaignId = campaignId;
  }

  public String getCampaignName() {
    return campaignName;
  }

  public void setCampaignName(String campaignName) {
    this.campaignName = campaignName;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public int getRating() {
    return rating;
  }

  public void setRating(int rating) {
    this.rating = rating;
  }

  public List<String> getSkills() {
    return skills;
  }

  public void setSkills(List<String> skills) {
    this.skills = skills;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "TeamSearch{" +
      "campaignId='" + campaignId + '\'' +
      ", campaignName='" + campaignName + '\'' +
      ", clientName='" + clientName + '\'' +
      ", rating=" + rating +
      ", address=" + address +
      ", skills=" + skills +
      "} " + super.toString();
  }
}
