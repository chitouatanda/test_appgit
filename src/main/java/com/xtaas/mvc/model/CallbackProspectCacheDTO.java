
package com.xtaas.mvc.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.primitives.Ints;
import com.xtaas.db.entity.Note;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.web.dto.IndirectProspectsCacheDTO;
import com.xtaas.web.dto.NoteDTO;
import com.xtaas.web.dto.ProspectCacheDTO;
import com.xtaas.web.dto.ProspectCallDTO;

public class CallbackProspectCacheDTO implements Delayed {
	@JsonProperty
	private String prospectCallId;
	@JsonProperty
	private ProspectCacheDTO prospect;
	@JsonProperty
	private String twilioCallSid;
	@JsonProperty
	private String campaignId;
	@JsonProperty
	private String outboundNumber;
	@JsonProperty
	private int callRetryCount;
	@JsonProperty
	private Long dailyCallRetryCount;
	@JsonProperty
	private String dataSlice;
	@JsonProperty
	private List<IndirectProspectsCacheDTO> indirectProspects;
	@JsonProperty
	private Date createdDate;
	@JsonProperty
	private Date updatedDate;
	@JsonProperty
	private int qualityBucketSort;
	@JsonProperty
	private Date callbackDate;
	@JsonProperty
	private long delayTime;
	@JsonProperty
	private String agentId;
	@JsonProperty
	private ArrayList<NoteDTO> notes;
	@JsonProperty
	private List<AgentQaAnswer> answers;

	public CallbackProspectCacheDTO() {
	}

	public CallbackProspectCacheDTO(CallbackProspectCacheDTO callbackProspectCacheDTO, long delayTime) {
		this.setProspectCallId(callbackProspectCacheDTO.getProspectCallId());
		this.setProspect(callbackProspectCacheDTO.getProspect());
		this.setTwilioCallSid(callbackProspectCacheDTO.getTwilioCallSid());
		this.setCampaignId(callbackProspectCacheDTO.getCampaignId());
		this.setOutboundNumber(callbackProspectCacheDTO.getOutboundNumber());
		this.setCallRetryCount(callbackProspectCacheDTO.getCallRetryCount());
		this.setDataSlice(callbackProspectCacheDTO.getDataSlice());
		this.setIndirectProspects(callbackProspectCacheDTO.getIndirectProspects());
		this.setCreatedDate(callbackProspectCacheDTO.getCreatedDate());
		this.setUpdatedDate(callbackProspectCacheDTO.getUpdatedDate());
		this.setQualityBucketSort(callbackProspectCacheDTO.getQualityBucketSort());
		this.setCallbackDate(callbackProspectCacheDTO.getCallbackDate());
		this.setAgentId(callbackProspectCacheDTO.getAgentId());
		this.setNotes(callbackProspectCacheDTO.getNotes());
		this.setAnswers(callbackProspectCacheDTO.getAnswers());
		this.delayTime = System.currentTimeMillis() + delayTime;
	}

	public CallbackProspectCacheDTO(ProspectCallLog prospectCallLog) {
		if (prospectCallLog != null && prospectCallLog.getProspectCall() != null) {
			this.prospectCallId = prospectCallLog.getProspectCall().getProspectCallId();
			this.prospect = prospectCallLog.getProspectCall().getProspectCacheDTO(prospectCallLog.getProspectCall());
			this.twilioCallSid = prospectCallLog.getProspectCall().getTwilioCallSid();
			this.campaignId = prospectCallLog.getProspectCall().getCampaignId();
			this.outboundNumber = prospectCallLog.getProspectCall().getOutboundNumber();
			this.callRetryCount = prospectCallLog.getProspectCall().getCallRetryCount();
			this.dailyCallRetryCount = prospectCallLog.getProspectCall().getDailyCallRetryCount();
			this.dataSlice = prospectCallLog.getProspectCall().getDataSlice();
			if (prospectCallLog.getProspectCall().getIndirectProspects() != null) {
				this.indirectProspects = prospectCallLog.getProspectCall()
						.getIndirectProspectCacheDTO(prospectCallLog.getProspectCall().getIndirectProspects());
			}
			this.createdDate = prospectCallLog.getCreatedDate();
			this.updatedDate = prospectCallLog.getUpdatedDate();
			this.agentId = prospectCallLog.getProspectCall().getAgentId();
			this.callbackDate = prospectCallLog.getCallbackDate();
			ArrayList<NoteDTO> notesDTO = new ArrayList<NoteDTO>();
			if (prospectCallLog.getProspectCall().getNotes() != null
					&& prospectCallLog.getProspectCall().getNotes().size() > 0) {
				for (Note note : prospectCallLog.getProspectCall().getNotes()) {
					notesDTO.add(new NoteDTO(note));
				}
			}
			this.notes = notesDTO;
			List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
			if (prospectCallLog.getProspectCall().getAnswers() != null) {
				for (AgentQaAnswer answer : prospectCallLog.getProspectCall().getAnswers()) {
					qaAnswers.add(answer);
				}
			}
			this.answers = qaAnswers;
		}
	}

	public CallbackProspectCacheDTO(ProspectCallDTO prospectCallDTO) {
		this.prospectCallId = prospectCallDTO.getProspectCallId();
		this.prospect = prospectCallDTO.getProspectCacheDTO(prospectCallDTO);
		this.twilioCallSid = prospectCallDTO.getTwilioCallSid();
		this.campaignId = prospectCallDTO.getCampaignId();
		this.outboundNumber = prospectCallDTO.getOutboundNumber();
		this.callRetryCount = prospectCallDTO.getCallRetryCount();
		this.dailyCallRetryCount = prospectCallDTO.getDailyCallRetryCount();
		this.dataSlice = prospectCallDTO.getDataSlice();
		this.indirectProspects = prospectCallDTO.getIndirectProspectCacheDTO(prospectCallDTO.getIndirectProspects());
		this.createdDate = prospectCallDTO.getCreatedDate();
		this.updatedDate = prospectCallDTO.getUpdatedDate();
		this.agentId = prospectCallDTO.getAgentId();
		this.notes = prospectCallDTO.getNotes();
		List<AgentQaAnswer> qaAnswers = new ArrayList<AgentQaAnswer>();
		if (prospectCallDTO.getAnswers() != null) {
			for (AgentQaAnswer answer : prospectCallDTO.getAnswers()) {
				qaAnswers.add(answer);
			}
		}
		this.answers = qaAnswers;
	}

	public String getProspectCallId() {
		return prospectCallId;
	}

	public void setProspectCallId(String prospectCallId) {
		this.prospectCallId = prospectCallId;
	}

	public ProspectCacheDTO getProspect() {
		return prospect;
	}

	public void setProspect(ProspectCacheDTO prospect) {
		this.prospect = prospect;
	}

	public String getTwilioCallSid() {
		return twilioCallSid;
	}

	public void setTwilioCallSid(String twilioCallSid) {
		this.twilioCallSid = twilioCallSid;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	public int getCallRetryCount() {
		return callRetryCount;
	}

	public void setCallRetryCount(int callRetryCount) {
		this.callRetryCount = callRetryCount;
	}

	public Long getDailyCallRetryCount() {
		return dailyCallRetryCount;
	}

	public void setDailyCallRetryCount(Long dailyCallRetryCount) {
		this.dailyCallRetryCount = dailyCallRetryCount;
	}

	public String getDataSlice() {
		return dataSlice;
	}

	public void setDataSlice(String dataSlice) {
		this.dataSlice = dataSlice;
	}

	public List<IndirectProspectsCacheDTO> getIndirectProspects() {
		return indirectProspects;
	}

	public void setIndirectProspects(List<IndirectProspectsCacheDTO> indirectProspects) {
		this.indirectProspects = indirectProspects;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getQualityBucketSort() {
		return qualityBucketSort;
	}

	public void setQualityBucketSort(int qualityBucketSort) {
		this.qualityBucketSort = qualityBucketSort;
	}

	public Date getCallbackDate() {
		return callbackDate;
	}

	public void setCallbackDate(Date callbackDate) {
		this.callbackDate = callbackDate;
	}

	public long getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(long delayTime) {
		this.delayTime = delayTime;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public ArrayList<NoteDTO> getNotes() {
		return notes;
	}

	public void setNotes(ArrayList<NoteDTO> notes) {
		this.notes = notes;
	}

	public List<AgentQaAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AgentQaAnswer> answers) {
		this.answers = answers;
	}

	@Override
	public int compareTo(Delayed o) {
		if (this.delayTime < (((CallbackProspectCacheDTO) o).getDelayTime())) {
			return -1;
		}
		if (this.delayTime > (((CallbackProspectCacheDTO) o).getDelayTime())) {
			return 1;
		}
		return 0;
	}
	
	
	// @Override
	// public int compareTo(Delayed o) {
	//     return Ints.saturatedCast(
	//       this.delayTime - ((CallbackProspectCacheDTO) o).delayTime);
	// }

	@Override
	public long getDelay(TimeUnit unit) {
		long diff = this.delayTime - System.currentTimeMillis();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

//	@Override
//	public boolean equals(Object anObject) {
//		logger.info(
//						"Inside equals method :: this.prospectcallid [{}], objectProspectcallid [{}]" ,
//						this.prospectCallId, ((CallbackProspectCacheDTO) anObject).getProspectCallId());
//		if (anObject == null) {
//			return false;
//		}
//		if ((this.prospectCallId.equalsIgnoreCase(((CallbackProspectCacheDTO) anObject).getProspectCallId()))) {
//			return true;
//		}
//		return false;
//	}

}