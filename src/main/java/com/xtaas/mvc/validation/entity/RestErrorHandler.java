package com.xtaas.mvc.validation.entity;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xtaas.exception.ResourceNotFoundException;
import com.xtaas.exception.UnauthorizedAccessException;

@ControllerAdvice
public class RestErrorHandler {
	private MessageSource messageSource;
	 
    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    @ExceptionHandler(UnauthorizedAccessException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public String processUnauthorizedAccessError(UnauthorizedAccessException ex) {
    	String responseMsg = "{\"code\": \"ERROR\", \"msg\" : \"" + ex.getMessage() + "\"}";
    	return responseMsg;
    }
    
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ValidationError processResourceNotFoundError(ResourceNotFoundException ex) {
    	ValidationError dto = new ValidationError();
        String localizedErrorMessage = resolveLocalizedErrorMessage(ex.getMessage(), ex.getMessage()); //if messageKey is not found then return the messageKey as default message
        dto.addFieldError("", localizedErrorMessage);
        return dto;
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationError processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
 
        return processFieldErrors(fieldErrors);
    }
    
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationError processValidationError(IllegalArgumentException ex) {
        ValidationError dto = new ValidationError();
        String localizedErrorMessage = resolveLocalizedErrorMessage(ex.getMessage(), ex.getMessage()); //if messageKey is not found then return the messageKey as default message
        dto.addFieldError("", localizedErrorMessage);
        return dto;
    }
 
    private ValidationError processFieldErrors(List<FieldError> fieldErrors) {
        ValidationError dto = new ValidationError();
 
        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }
 
        return dto;
    }
 
    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
 
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }
 
        return localizedErrorMessage;
    }
    
    private String resolveLocalizedErrorMessage(String messageKey, String defaultMessage) {
    	Locale currentLocale =  LocaleContextHolder.getLocale();
        return messageSource.getMessage(messageKey, null, defaultMessage, currentLocale);
    }
}
