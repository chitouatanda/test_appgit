package com.xtaas.mvc.validation.entity;

import java.util.ArrayList;
import java.util.List;

public class ValidationError {
	private List<FieldError> fieldErrors = new ArrayList<FieldError>();
	 
    public void addFieldError(String path, String message) {
        FieldError error = new FieldError(path, message);
        fieldErrors.add(error);
    }

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
    
    
}
