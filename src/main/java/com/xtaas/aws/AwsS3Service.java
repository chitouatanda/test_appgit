package com.xtaas.aws;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * AWS S3 service to list/create/delete buckets & files. This will be useful for
 * only default region (us-west-2) passed in environment variable. If you want
 * to create bucket or upload file into different region then create new
 * s3Client object locally.
 * 
 * @author pranay
 */
public interface AwsS3Service {

	public Bucket CreateBucket(String bucketName);

	public void DeleteBucket(String bucketName);

	public List<Bucket> GetBuckets();

	/**
	 * @param bucketName   The name of an existing bucket to which the new object
	 *                     will be uploaded.
	 * @param key          The key under which to store the new object.
	 * @param file         The file containing the data to be uploaded to Amazon S3.
	 * @param metadata     The object metadata. At minimum this specifies the
	 *                     content length for the stream of data being uploaded.
	 * @param isPublicRead Specify the object has public read access or not.
	 * 
	 * @return {@link URI} link to the file uploaded to the Amazon S3 bucket.
	 * 
	 * @throws Exception It can throws exceptions like FileNotFoundException,
	 *                   SdkClientException, AmazonServiceException
	 */
	public URI UploadFile(String bucketName, String key, File file, ObjectMetadata metadata, Boolean isPublicRead)
			throws Exception;

	/**
	 * @param bucketName     The name of an existing bucket to which the new object
	 *                       will be uploaded.
	 * @param key            The key under which to store the new object.
	 * @param input          The stream of data to upload to Amazon S3.
	 * @param objectMetadata The object metadata. At minimum this specifies the
	 *                       content length for the stream of data being uploaded.
	 * @param isPublicRead   Specify the object has public read access or not.
	 * 
	 * @return {@link URI} link to the file uploaded to the Amazon S3 bucket.
	 * 
	 * @throws Exception It can throws exceptions like FileNotFoundException,
	 *                   SdkClientException, AmazonServiceException
	 */
	public URI UploadFile(String bucketName, String key, InputStream input, ObjectMetadata metadata,
			Boolean isPublicRead) throws Exception;

	public void DeleteFile(String bucketName, String fileName);

	public S3Object GetFileByBucketAndFileName(String bucketName, String fileName);

	public List<S3ObjectSummary> GetFilesByBucketName(String bucketName);

}
