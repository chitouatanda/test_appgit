package com.xtaas.aws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.xtaas.utils.XtaasEmailUtils;

@Component
public class UploadFileToAWSS3Storage {
	
	 String clientRegion = "us-west-2";
	 String bucketName = "xtaasrecordings";
	 String awsAccessKeyId = "AKIAJZ5EWMT6OAKGOGNQ";
	 String awsSecretKey = "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";
	
	 String filePath = "MyData.xlsx";   //  this is file name which is you want to upload
	 
	 String ext = ".zip";
	 String fileNameWithoutExt = null;
	 
	// Please mention the file which is download from AWS
	 String key_name = "MyData.xlsx";
	
	private static final Logger logger = LoggerFactory.getLogger(UploadFileToAWSS3Storage.class);
	
	
	public void uploadFileToAWS() {
		try {
			// First make the ZIP file
			makeZipFile();
			
			// Upload the zip file to AWS
			AmazonS3 s3client = createAWSConnection();
			String fileName = fileNameWithoutExt + ext;
			logger.debug("The file"+ fileName +"is uploading to AWS S3 storage.....");
			s3client.putObject(new PutObjectRequest(bucketName, fileName, new File(fileName)).withCannedAcl(CannedAccessControlList.PublicRead));
			
			logger.debug("The file"+ fileName +"is uploaded to AWS S3 storage !!!");
		}
		 catch(AmazonServiceException e) {
	            e.printStackTrace();
	            logger.error("The file was transmitted successfully, but Amazon S3 couldn't process :" +e);
	        }
	    catch(SdkClientException e) {
	        e.printStackTrace();
	        logger.error("Amazon S3 couldn't be contacted for a response, or the client :" +e);
	    }
		catch(Exception e) {
			  e.printStackTrace();
		      logger.error("uploadFileToAWS() : Could not upload the file to Amozone S3 Storage :" +e);
		}
	}
	
	
	public void makeZipFile() throws IOException {
    	 FileOutputStream fos =null;
		 ZipOutputStream zos = null; 
    	try {
    		File f = new File(filePath);
    		String fileName = f.getName();
    		fileNameWithoutExt = fileName.split("\\.")[0];
    		
    		String zipFile = fileNameWithoutExt+ext;
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(fos);
			addToZipFile(filePath, zos);
			logger.debug("Wrote '" + fileName + "' to zip file");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			 logger.error("File not found at the location :" +e);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			zos.close();
			fos.close();
		}
	}

	public  void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		logger.debug("Writing '" + fileName + "' to zip file");

		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}
	
	/*
	 * This method is used for creating the connection with AWS S3 Storage
	 *  Params - AWS Access key, AWS secret key
	 */
	private AmazonS3 createAWSConnection() {
		AmazonS3 s3client = null;
		try {
			BasicAWSCredentials creds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
			 s3client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			return s3client;
		}
		catch(Exception e){
			e.printStackTrace();
			   logger.error("Error while creating the connection with AWS stroage. It  may happnens when awsAccessKeyId, awsSecretKey expired :" +e);
			return s3client;
		}
	}
	
	
	// this method is used for download the file from AWS
	public void download() throws ParseException,Exception  {
			AmazonS3 s3client = createAWSConnection();
			
			File file = null;
			try {
				file = new File(key_name);
				file.createNewFile();
			    S3Object o = s3client.getObject(bucketName, key_name);
			    S3ObjectInputStream s3is = o.getObjectContent();
			    FileOutputStream fos = new FileOutputStream(file);
			    byte[] read_buf = new byte[1024];
			    int read_len = 0;
				logger.debug("download() " +key_name + "  is downloading deom AWS  ");
			    while ((read_len = s3is.read(read_buf)) > 0) {
			        fos.write(read_buf, 0, read_len);
			    }
			    s3is.close();
			    fos.close();
			} catch (AmazonServiceException e) {
			    System.err.println(e.getErrorMessage());
			    System.exit(1);
			} catch (FileNotFoundException e) {
			    System.err.println(e.getMessage());
			    System.exit(1);
			} catch (IOException e) {
			    System.err.println(e.getMessage());
			    System.exit(1);
			}
			
			 List<File> fileNames = new ArrayList<File>();
	            fileNames.add(file);
			XtaasEmailUtils.sendEmail("kailashsaini7@gmail.com", "kailashsaini7@yahoo.com"," Lead Report File Attached.", "Lead Report",fileNames);
			logger.debug("Email sent successfully tokailashsaini7@gmail.com  ");
			
	}

	
	
}
