package com.xtaas.aws;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@Service
public class AwsS3ServiceImpl implements AwsS3Service {

	public static final Logger logger = LoggerFactory.getLogger(AwsS3ServiceImpl.class);

	private final AmazonS3 s3client;

	public AwsS3ServiceImpl(@Value("${aws.access.key.id}") String awsAccessKeyId,
			@Value("${aws.secret.access.key}") String awsSecretKey,
			@Value("${aws.s3.region:us-west-2}") String region) {
		this.s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(
						new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKeyId, awsSecretKey)))
				.withRegion(region).build();
	}

	@Override
	public Bucket CreateBucket(String bucketName) throws SdkClientException, AmazonServiceException {
		return s3client.createBucket(bucketName);
	}

	@Override
	public void DeleteBucket(String bucketName) {
		s3client.deleteBucket(bucketName);
	}

	@Override
	public List<Bucket> GetBuckets() {
		return s3client.listBuckets();
	}

	@Override
	public URI UploadFile(String bucketName, String key, File file, ObjectMetadata metadata, Boolean isPublicRead)
			throws Exception {
		CannedAccessControlList accessControl = CannedAccessControlList.PublicRead;
		if (isPublicRead == null || !isPublicRead) {
			accessControl = CannedAccessControlList.Private;
		}
		if (metadata == null) {
			metadata = new ObjectMetadata();
			metadata.setContentLength(file.length());
		}
		InputStream inputStream = new FileInputStream(file);
		s3client.putObject(new PutObjectRequest(bucketName, key, inputStream, metadata).withCannedAcl(accessControl));

		return URI.create(String.format("https://%s.s3.amazonaws.com/%s", bucketName, key));
	}

	@Override
	public URI UploadFile(String bucketName, String key, InputStream input, ObjectMetadata metadata,
			Boolean isPublicRead) throws Exception {
		CannedAccessControlList accessControl = CannedAccessControlList.PublicRead;
		if (isPublicRead == null || !isPublicRead) {
			accessControl = CannedAccessControlList.Private;
		}
		if (metadata == null) {
			metadata = new ObjectMetadata();
		}
		s3client.putObject(new PutObjectRequest(bucketName, key, input, metadata).withCannedAcl(accessControl));

		return URI.create(String.format("https://%s.s3.amazonaws.com/%s", bucketName, key));
	}

	@Override
	public void DeleteFile(String bucketName, String fileName) {
		s3client.deleteObject(bucketName, fileName);
	}

	@Override
	public S3Object GetFileByBucketAndFileName(String bucketName, String fileName) {
		return s3client.getObject(bucketName, fileName);
	}

	@Override
	public List<S3ObjectSummary> GetFilesByBucketName(String bucketName) {
		return s3client.listObjects(bucketName).getObjectSummaries();
	}

}
