package com.xtaas.job;

import com.xtaas.application.service.CampaignService;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.ProspectCallInteractionRepository;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.utils.XtaasDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service
public class DailyDataBuyJob{

    private static final Logger logger = LoggerFactory.getLogger(DailyDataBuyJob.class);

    @Autowired
    private DataBuyQueueRepository dataBuyQueueRepository;

    @Autowired
    private ProspectCallLogRepository prospectCallLogRepository;

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private ProspectCallInteractionRepository prospectCallInteractionRepository;


   // @Scheduled(cron = "0 */2 * * * *", zone = "GMT+5:30")
    public synchronized void dailyDataBuy(){
        Set<String> campaignSet = new HashSet<>();
        List<DataBuyQueue> dataBuyQueueList = getRecordPurchasedLastDay();
        for(DataBuyQueue dataBuyQueue : dataBuyQueueList){
            String campaignId  = dataBuyQueue.getCampaignId();
            long lastDayAttemptedCount = getLastDayAttemptedCount(campaignId);
            if(lastDayAttemptedCount > 0){
                if(campaignSet.contains(campaignId)){
                    dataBuyQueue.setDailyBuyProspect(true);
                    dataBuyQueueRepository.save(dataBuyQueue);
                }else{
                    DataBuyQueue dbq = new DataBuyQueue();
                    dbq.setCampaignId(campaignId);
                    dbq.setCampaignName(dataBuyQueue.getCampaignName());
                    dbq.setStatus(DataBuyQueue.DataBuyQueueStatus.QUEUED.toString());
                    dbq.setBuyProspect(dataBuyQueue.isBuyProspect());
                    dbq.setJobRequestTime(new Date());
                    dbq.setRequestCount(lastDayAttemptedCount);
                    dbq.setOrganizationId(dataBuyQueue.getOrganizationId());
                    dbq.setCampaignCriteriaDTO(dataBuyQueue.getCampaignCriteriaDTO());
                    dbq.setSearchCompany(dataBuyQueue.isSearchCompany());
                    dbq.setSearchCompany(dataBuyQueue.getSearchCompany());
                    dbq.setDailyBuyProspect(false);
                    dataBuyQueue.setDailyBuyProspect(true);
                    dataBuyQueueRepository.save(dataBuyQueue);
                    dataBuyQueueRepository.save(dbq);
                }
            }
            campaignSet.add(campaignId);
        }
    }

    private List<DataBuyQueue> getRecordPurchasedLastDay(){
        List<String> notInStatusList = getDatabuyQueueStatuses();
        List<Boolean> dailyDataBuyVal = getDailyDataBuyValue();
        List<DataBuyQueue> dataBuyQueueList = new ArrayList<>();
        try {
            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE, -1440); // last day date
            Date nowDate = now.getTime();
            String strCurrDateInStateTZ = XtaasDateUtils.convertToTimeZone(nowDate, XtaasDateUtils.TZ_UTC,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
            Date endDate = XtaasDateUtils.convertStringToDate(strCurrDateInStateTZ,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);

            now = Calendar.getInstance();
            now.add(Calendar.MINUTE, -2880); // last to last day date

            Date beforeDate = now.getTime();
            String strBeforeDateInStateTZ = XtaasDateUtils.convertToTimeZone(beforeDate, XtaasDateUtils.TZ_UTC,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
            Date startDate = XtaasDateUtils.convertStringToDate(strBeforeDateInStateTZ,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
            dataBuyQueueList = dataBuyQueueRepository.findByStatusAndJobStartTime(notInStatusList, startDate, endDate, dailyDataBuyVal );
        } catch (ParseException e) {
            logger.error("Error Occurred while fetching the record from DataBuyQueue ",  e);
        }

        return dataBuyQueueList;
    }

    private Integer getLastDayAttemptedCount(String campaignId ){
        Integer attemptedCount = 0;
        try {
            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE, -2880); // last to last day date

            Date beforeDate = now.getTime();
            String strBeforeDateInStateTZ = XtaasDateUtils.convertToTimeZone(beforeDate, XtaasDateUtils.TZ_UTC,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);
            Date startDate = XtaasDateUtils.convertStringToDate(strBeforeDateInStateTZ,
                    XtaasDateUtils.DATETIME_FORMAT_DEFAULT);

            attemptedCount=  prospectCallInteractionRepository.getCampaignRecordsAttemptedCount(campaignId,startDate);

        } catch (ParseException e) {
            logger.error("Error Occurred while fetching the record from DataBuyQueue for CampaignId " + campaignId, e);
        }
        return attemptedCount;
    }

    private List<String> getDatabuyQueueStatuses(){
        List<String> statusList = new ArrayList<>();
        statusList.add(DataBuyQueue.DataBuyQueueStatus.PAUSED.toString());
        statusList.add(DataBuyQueue.DataBuyQueueStatus.INPROCESS.toString());
        return statusList;
    }

    private List<Boolean> getDailyDataBuyValue(){
        List<Boolean> list = new ArrayList<>();
        list.add(true);
        return list;
    }


}
