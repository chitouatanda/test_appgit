package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.AgentOnlineReportServiceImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/***
 * The job calculates agent online time of DEMANDSHORE,DEMANDSHOREAG,SALSIFY
 * organizations.
 * 
 * Purpose :- To analyze agent online time.
 * 
 * The job will run at 8AM IST from Monday to Friday.
 */
@Service
public class AgentOnlineReportJob {

	private static final Logger logger = LoggerFactory.getLogger(AgentOnlineReportJob.class);

	@Autowired
	private AgentOnlineReportServiceImpl agentOnlineReportServiceImpl;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 0 8 * * MON-FRI", zone = "Asia/Kolkata")
	public void getAgentOnlineReport() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.AGENT_ONLINE_REPORT_JOB_INSTANCE.name(), "xtaasupgrade");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.debug("Cron job started to send agent online report.");
				agentOnlineReportServiceImpl.downloadAgentReport();
				logger.debug("Cron job ended to send agent online report.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}