package com.xtaas.job;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.ProspectFeedbackService;
import com.xtaas.utils.XtaasConstants;

/**
 * CronJob to take backup of prospects into prospectfeedback collection &
 * generate report.
 * 
 * @author pranay
 *
 */
@Service
public class DataFeedbackJob {
	public static final Logger logger = LoggerFactory.getLogger(DataFeedbackJob.class);

	private final PropertyService propertyService;
	private final ProspectFeedbackService prospectFeedbackService;

	public DataFeedbackJob(PropertyService propertyService, ProspectFeedbackService prospectFeedbackService) {
		this.propertyService = propertyService;
		this.prospectFeedbackService = prospectFeedbackService;
	}

	@Scheduled(cron = "${data.feedback.job.expr:0 0 12 * * SUN}", zone = "Asia/Kolkata")
	public void runDataFeedbackJob() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.DATA_FEEDBACK_JOB_INSTANCE.name(), "xtaascorp");
		if (StringUtils.isEmpty(serverUrl) || StringUtils.isEmpty(instance)
				|| !serverUrl.toLowerCase().contains(instance.toLowerCase())) {
			return;
		}
		logger.info("Started DataFeedbackJob");
		prospectFeedbackService.autoDataBackupAndReport();
	}

}
