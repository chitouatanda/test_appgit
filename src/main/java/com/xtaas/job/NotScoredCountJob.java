package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.NotScoredCountServiceImpl;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/**
 * The job counts NOT_SCORED leads of partners from 1st day of every month 
 * till todays date and sends lead count of each partner in the email.
 * 
 * Purpose :- To check whether partners doing their QA of NOT_SCORED leads on daily basis or not.
 * 
 * Job will run @4:30 PM IST from Monday to Friday.
 */

@Service
public class NotScoredCountJob {

	private static final Logger logger = LoggerFactory.getLogger(NotScoredCountJob.class);

	@Autowired
	private NotScoredCountServiceImpl notScoredCountServiceImpl;

	@Autowired
	private PropertyService propertyService;

//	@Scheduled(cron = "0 0 5 * * MON-FRI", zone = "PST")
	public void notScoredJobs() {
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			String instance = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.PARTNER_NOT_SCORED_JOB_INSTANCE.name(), "xtaasupgrade");
			logger.debug("Cron job started to count partner NOT_SCORED leads.");
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				notScoredCountServiceImpl.countPartnerNotScoredLeads();
			}
			logger.debug("Cron job ended to count partner NOT_SCORED leads.");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
