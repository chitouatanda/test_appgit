package com.xtaas.job;

import com.xtaas.service.TelnyxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The Job prints the size of telnyxCallLegAndControlIdMap used in
 * telnyxserviceimpl.java every minute to track the percentage of requests
 * processed from telnyx.
 */
@Service
public class TelnyxMapSizePrintJob {

	private static final Logger logger = LoggerFactory.getLogger(TelnyxMapSizePrintJob.class);

	@Autowired
	private TelnyxService telnyxService;

	@Scheduled(cron = "0 */1 * * * *")
	public void logSizeOfCallLegAndControlIdMap() {
		logger.info("logSizeOfCallLegAndControlIdMap() : Size of telnyxCallLegAndControlIdMap  : [{}]",
				telnyxService.getSizeOfCallLegAndControlIdMap());
	}

}