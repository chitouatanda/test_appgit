package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.DeadCallsReportServiceImpl;
import com.xtaas.service.ProspectCallLogServiceImpl;

@Service
public class DeadCallsReportJob {

	private static final Logger logger = LoggerFactory.getLogger(DeadCallsReportJob.class);

	@Autowired
	private DeadCallsReportServiceImpl deadCallsReportServiceImpl;

	@Scheduled(cron = "0 59 23 * * MON-FRI", zone = "PST")
	public void sendAgentOnlineReport() {
		logger.debug("Cron job Started for dead air calls report.");
		
		try {
			deadCallsReportServiceImpl.sendDeadCallsReport();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug("Cron job End for dead air calls report.");
	}
}
