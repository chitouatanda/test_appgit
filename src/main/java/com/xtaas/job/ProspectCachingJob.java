package com.xtaas.job;

import com.xtaas.service.ProspectInMemoryCacheServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The job checks the onlineagent count in the campaign and prospects stoted in the cache for the campaign if the prospects in the cache goes 
 * below threshold value then it fetched more prospects from DB based on ONLINE AGENTS in the campaign and RANGE defined in application property and stores 
 * in cache.
 * 
 * Purpose :- AUTO load the cache in the campaign from DB and to improve the existing caching performance.
 * 
 * Job will run in every 15 mins.
 */
@Service
public class ProspectCachingJob {
	public static final Logger logger = LoggerFactory.getLogger(ProspectCachingJob.class);

	final private ProspectInMemoryCacheServiceImpl prospectInMemoryCacheServiceImpl;

	public ProspectCachingJob(@Autowired ProspectInMemoryCacheServiceImpl prospectInMemoryCacheServiceImpl) {
		this.prospectInMemoryCacheServiceImpl = prospectInMemoryCacheServiceImpl;
	}

	@Scheduled(cron = "0 */5 * * * *", zone = "Asia/Kolkata")
	public void cacheProspects() {
		logger.info("Cron job started to cache the prospects.");
		prospectInMemoryCacheServiceImpl.cacheProspectsInMemory();
		logger.info("Cron job ended of prospect caching.");
	}

}