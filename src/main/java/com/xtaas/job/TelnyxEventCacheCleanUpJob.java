package com.xtaas.job;

import com.xtaas.service.ProspectCacheServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class TelnyxEventCacheCleanUpJob {
  
  private static final Logger logger = LoggerFactory.getLogger(TelnyxEventCacheCleanUpJob.class);
  
  @Autowired
  private ProspectCacheServiceImpl prospectCacheServiceImpl;
  
  // job configured for every 5 minutes
  @Scheduled(cron = "0 */5 * * * *")
  public void cleanTelnyxEventCache() {
    prospectCacheServiceImpl.cleanObsoleteTelnyxEventEntries();
  }
  
}