package com.xtaas.job;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ProspectCallLogRepository;
import com.xtaas.domain.valueobject.Data;
import com.xtaas.domain.valueobject.TelnyxRecordingObject;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;


@Service
public class TelnyxMissingRecordingsJob {

	private static final Logger logger = LoggerFactory.getLogger(TelnyxMissingRecordingsJob.class);

	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;
	
	@Autowired
	private ProspectCallLogRepository prospectCallLogRepository;

	@Autowired
	private PropertyService propertyService;

	// job configured for every 10 minutes
	  @Scheduled(cron = "0 */10 * * * *")
	  public void telnyxRecordingJob() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.TELNYX_MISSING_RECORDING.name(), "xtaascorp.herokuapp.com");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.info("Cron job started to to find missing recordings from telnyx.");
				getMissingRecordings();
				logger.info("Cron job ended to end for missing recordings from telnyx.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getMissingRecordings() {
		Map<String,ProspectCallLog> telnyxUidMap = new HashMap<String, ProspectCallLog>();
		ZoneId defaultZoneId = ZoneId.systemDefault();
		LocalDate todaydate = LocalDate.now();
		LocalDate fromDate = todaydate.minusDays(1);
		Date date = Date.from(fromDate.atStartOfDay(defaultZoneId).toInstant());
		List<String> dispositionsList = new ArrayList<String>();
		dispositionsList.add("SUCCESS");
		
		List<ProspectCallLog> pclogList =  prospectCallLogRepository.findSuccessWithMissingRecordings(dispositionsList, date);
		
		if(pclogList!=null) {
			for(ProspectCallLog pclog : pclogList) {
				if(pclog.getProspectCall().getTwilioCallSid()!=null && !pclog.getProspectCall().getTwilioCallSid().isEmpty()
						&& pclog.getProspectCall().getVoiceProvider()!=null && pclog.getProspectCall().getVoiceProvider().equalsIgnoreCase("Telnyx")) {
					telnyxUidMap.put(pclog.getProspectCall().getTwilioCallSid(),pclog);
				}
			}
			if(telnyxUidMap.size()>0)
				getRecordingsFromTelnyx(telnyxUidMap);
		}
		
	}
	
	public void getForPostMan(int days) {
		Map<String,ProspectCallLog> telnyxUidMap = new HashMap<String, ProspectCallLog>();
		ZoneId defaultZoneId = ZoneId.systemDefault();
		LocalDate todaydate = LocalDate.now();
		LocalDate fromDate = todaydate.minusDays(days);
		Date date = Date.from(fromDate.atStartOfDay(defaultZoneId).toInstant());
		List<String> dispositionsList = new ArrayList<String>();
		dispositionsList.add("SUCCESS");
		
		List<ProspectCallLog> pclogList =  prospectCallLogRepository.findSuccessWithMissingRecordings(dispositionsList, date);
		
		if(pclogList!=null) {
			for(ProspectCallLog pclog : pclogList) {
				if(pclog.getProspectCall().getTwilioCallSid()!=null && !pclog.getProspectCall().getTwilioCallSid().isEmpty()) {
					telnyxUidMap.put(pclog.getProspectCall().getTwilioCallSid(),pclog);
				}
			}
			if(telnyxUidMap.size()>0)
				getRecordingsFromTelnyx(telnyxUidMap);
		}
		
	}
	
	public void getRecordingsFromTelnyx(Map<String,ProspectCallLog> telnyxUidMap) {
		
		TelnyxRecordingObject telnyxObject = null;
		String url = "https://api.telnyx.com/recordings";
		try {
			for (Map.Entry<String,ProspectCallLog> entry : telnyxUidMap.entrySet()) {				
				//url  =url+"?call_id="+entry.getKey();
				StringBuilder response = new StringBuilder();
				String urlWithParams = url+"?call_id="+entry.getKey();;
				ProspectCallLog pclog = entry.getValue();
		        HttpsURLConnection httpClient = (HttpsURLConnection) new URL(urlWithParams).openConnection();
		        //add request header
		        httpClient.setRequestMethod("GET");
		        httpClient.setRequestProperty("x-api-user", "kchugh@xtaascorp.com");
		        httpClient.setRequestProperty("x-api-token", "p4o-wVSBSoy-EmjsOdE2Fg");
	
		        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
		        httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		        httpClient.setRequestProperty("Accept", "application/json");
		        int responseCode = httpClient.getResponseCode();
		        logger.debug("\nSending 'POST' request to URL : " + urlWithParams);
		        logger.debug("Post parameters : " + urlWithParams);
		        logger.debug("Response Code : " + responseCode);
		        try (BufferedReader in = new BufferedReader(
		                new InputStreamReader(httpClient.getInputStream()))) {
		            String line;
		
		            while ((line = in.readLine()) != null) {
		                response.append(line);
		            }
		            logger.debug(response.toString());
		        }
		        
            	ProspectCallLog prospect =  prospectCallLogRepository.findByProspectCallId(pclog.getProspectCall().getProspectCallId());

		        if (response != null) {
		            ObjectMapper mapper= new ObjectMapper().setVisibility(JsonMethod.FIELD,Visibility.ANY);
		            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
		            telnyxObject=mapper.readValue(response.toString(), TelnyxRecordingObject.class);
		            if(telnyxObject.getData()!=null && telnyxObject.getData().size()>0) {
		            	Data data = telnyxObject.getData().get(0);
		            	if(data.getDownload_urls()!=null && data.getDownload_urls().getWav()!=null) {
		            		String awsUrl = downloadRecordingsToAws.downloadTelnyxRecording(data.getDownload_urls().getWav(), 
		            				pclog.getProspectCall().getProspectCallId(), pclog.getProspectCall().getCampaignId(), true);
		            		prospect.getProspectCall().setRecordingUrlAws(awsUrl);
		            	}else {
		            		prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
		            	}
		            }else {
	            		prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
		            }
		        }else {
            		prospect.getProspectCall().setRecordingUrlAws("NOT_FOUND");
		        }
        		prospectCallLogRepository.save(prospect);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		//return response;
	}
}