package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.AgentStatusUpdateServiceImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/***
 * The job updates the agent status change entries in agentstatuslog collection.
 * 
 * Purpose :- To analyze the agentstatuschange data in DOMO and calculate the
 * actual ONLINE time of agent.
 * 
 * The job will run every 15 min from Monday to Friday.
 */
@Service
public class AgentStatusLogUpdateJob {

	private static final Logger logger = LoggerFactory.getLogger(AgentStatusLogUpdateJob.class);

	@Autowired
	private AgentStatusUpdateServiceImpl agentStatusUpdateServiceImpl;

	@Autowired
	private PropertyService propertyService;

//	@Scheduled(cron = "0 */15 * * * MON-FRI", zone = "Asia/Kolkata")
	public void inActiveAgents() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.AGENT_STATUS_LOG_UPDATE_JOB_INSTANCE.name(), "xtaasupgrade");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.info("Cron job started to update entries in agentstatuslog collection.");
//				agentStatusUpdateServiceImpl.updateAgentStatusLogCollection();
				logger.info("Cron job ended to update entries in agentstatuslog collection.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}