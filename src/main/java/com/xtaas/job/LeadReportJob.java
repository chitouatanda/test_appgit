package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.LeadReportServiceImpl;

@Service
public class LeadReportJob {
	private static final Logger logger = LoggerFactory.getLogger(LeadReportJob.class);
    
    @Autowired
    private LeadReportServiceImpl leadReportServiceImpl;

//	@Scheduled(cron = "30 23 * * *  MON-FRI", zone = "UTC")
	public void sendLeadReport()
    {
        logger.debug("Cron job Started for lead report.");
        System.out.println("Starting");
        try{
        	/*leadReportServiceImpl.downloadLeadReport();
        	leadReportServiceImpl.downloadLeadInternalReport();*/

        }catch(Exception e){
        	e.printStackTrace();
        }
        System.out.println("Ending");
        logger.debug("Cron job End for lead report.");
    }
}
