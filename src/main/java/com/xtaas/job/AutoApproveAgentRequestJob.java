package com.xtaas.job;

import com.xtaas.service.AutoApproveAgentRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The job checks the time difference between current time and agent break
 * requested time. If this difference is greater than or equal to configurable
 * time then auto approve agent requests.
 * 
 * Purpose :- AUTO approve agent requests after configurable time if supervisor
 * doesn't approves it.
 * 
 * 
 * Job will run in every 30 seconds.
 */
@Service
public class AutoApproveAgentRequestJob {
	public static final Logger logger = LoggerFactory.getLogger(AutoApproveAgentRequestJob.class);

	final private AutoApproveAgentRequestService autoApproveAgentRequestService;

	public AutoApproveAgentRequestJob(@Autowired AutoApproveAgentRequestService autoApproveAgentRequestService) {
		this.autoApproveAgentRequestService = autoApproveAgentRequestService;
	}

	@Scheduled(cron = "*/30 * * * * *", zone = "Asia/Kolkata")
	public void approveRequest() {
		logger.info("Cron job started to auto approve agent requests.");
		autoApproveAgentRequestService.autoApproveAgentRequest();
		logger.info("Cron job ended to auto approve agent requests.");
	}

}