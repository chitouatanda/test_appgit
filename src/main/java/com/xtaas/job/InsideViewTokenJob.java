package com.xtaas.job;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import com.xtaas.db.repository.InsideViewTokenRepository;
import com.xtaas.domain.entity.InsideViewToken;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

public class InsideViewTokenJob {


	private static final Logger logger = LoggerFactory.getLogger(InsideViewTokenJob.class);

	@Autowired
	private InsideViewTokenRepository insideViewTokenRepository;


	@Autowired
	private PropertyService propertyService;
	
	int retry = 0;

//TODO uncomment when completely move to upgrade instanE
	//@Scheduled(cron = "59 * * * * *")
	//TODO this method should be never called, i have kept it to run it manually.
	public void getZoomOAuthTokenJob() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.ZOOM_TOKEN_THREAD_INSTANCE.name(), "xtaascorplive");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.debug("Cron job started to to check InsideView Token.");
				generateInsideViewToken();
				logger.debug("Cron job ended to check InsideView Token.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// this  token validity is for one year, will need to handle every year later
	  private void generateInsideViewToken() throws Exception {

			// url is missing?
	        //String url = "https://selfsolve.apple.com/wcResults.do";
			String url = "https://login.insideview.com/Auth/login/v1/token.json";

			//String jsonInputString = "{\"clientId\": \"nva86ilb4c1mvo72gokl\", \"clientSecret\": \"cpgk0s3ctou8spbjcrqargvibu0frk5naguo7n4qienlpeaj7ivriu1pfhuc\"}";

	        HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();

	        //add reuqest header
	        httpClient.setRequestMethod("POST");
	        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient.setRequestProperty("Accept", "application/json");

	        String urlParameters = "";
	        		
	        		//"clientId=nva86ilb4c1mvo72gokl&clientSecret=cpgk0s3ctou8spbjcrqargvibu0frk5naguo7n4qienlpeaj7ivriu1pfhuc&grantType=cred";

	        // Send post request
	        httpClient.setDoOutput(true);
	        try (DataOutputStream wr = new DataOutputStream(httpClient.getOutputStream())) {
	            wr.writeBytes(urlParameters);
	            wr.flush();
	        }

	        int responseCode = httpClient.getResponseCode();
	        logger.debug("\nSending 'POST' request to URL : " + url);
	        logger.debug("Post parameters : " + urlParameters);
	        logger.debug("Response Code : " + responseCode);

	        try (BufferedReader in = new BufferedReader(
	                new InputStreamReader(httpClient.getInputStream()))) {

	            String line;
	            StringBuilder response = new StringBuilder();

	            while ((line = in.readLine()) != null) {
	                response.append(line);
	            }

	            //print result
	            logger.debug(response.toString());

	        }

	    }
	
}
