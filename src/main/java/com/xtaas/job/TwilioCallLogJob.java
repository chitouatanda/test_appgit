package com.xtaas.job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.TwilioCallLogAndDataBuyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.web.dto.TwilioCallLogFilterDTO;

@Service
public class TwilioCallLogJob {

	private static final Logger logger = LoggerFactory.getLogger(LeadReportJob.class);

	@Autowired
	TwilioCallLogAndDataBuyService twilioCallLogAndDataBuyService;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 0 18 * * MON-FRI", zone = "PST")
	public void generateTwilioCallLogs() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.TWILIO_CALL_LOG_JOB_INSTANCE.name(), "xtaasupgrade");
		if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
			logger.debug("<========== Running TwilioCallLogJob for incoming voicemails. ==========>");
			Calendar startCalendar = Calendar.getInstance(TimeZone.getTimeZone("US/Pacific"));
			Calendar endCalendar = Calendar.getInstance(TimeZone.getTimeZone("US/Pacific"));
			endCalendar.add(Calendar.DAY_OF_MONTH, 1);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
			List<TwilioCallLogFilterDTO> filterDTOs = new ArrayList<>();

			TwilioCallLogFilterDTO masterFilterDTO = new TwilioCallLogFilterDTO();
			masterFilterDTO.setProfile("MASTER");
			masterFilterDTO.setDirection("inbound");
			masterFilterDTO.setStatus("completed");
			masterFilterDTO.setToEmails(Arrays.asList("reports@xtaascorp.com"));
			masterFilterDTO.setFromEmail("data@xtaascorp.com");
			masterFilterDTO.setStartDate(simpleDateFormat.format(startCalendar.getTime()));
			masterFilterDTO.setEndDate(simpleDateFormat.format(endCalendar.getTime()));
//			filterDTOs.add(masterFilterDTO);

			TwilioCallLogFilterDTO prodFilterDTO = new TwilioCallLogFilterDTO();
			prodFilterDTO.setProfile("PROD");
			prodFilterDTO.setDirection("inbound");
			prodFilterDTO.setStatus("completed");
			prodFilterDTO.setToEmails(Arrays.asList("reports@xtaascorp.com"));
			prodFilterDTO.setFromEmail("data@xtaascorp.com");
			prodFilterDTO.setStartDate(simpleDateFormat.format(startCalendar.getTime()));
			prodFilterDTO.setEndDate(simpleDateFormat.format(endCalendar.getTime()));
			filterDTOs.add(prodFilterDTO);

			TwilioCallLogFilterDTO demandshoreFilterDTO = new TwilioCallLogFilterDTO();
			demandshoreFilterDTO.setProfile("DEMANDSHORE");
			demandshoreFilterDTO.setDirection("inbound");
			demandshoreFilterDTO.setStatus("completed");
			demandshoreFilterDTO.setToEmails(Arrays.asList("dnc@demandshore.com"));
			demandshoreFilterDTO.setFromEmail("data@xtaascorp.com");
			demandshoreFilterDTO.setStartDate(simpleDateFormat.format(startCalendar.getTime()));
			demandshoreFilterDTO.setEndDate(simpleDateFormat.format(endCalendar.getTime()));
			filterDTOs.add(demandshoreFilterDTO);

			TwilioCallLogFilterDTO qaFilterDTO = new TwilioCallLogFilterDTO();
			qaFilterDTO.setProfile("QA");
			qaFilterDTO.setDirection("inbound");
			qaFilterDTO.setStatus("completed");
			qaFilterDTO.setToEmails(Arrays.asList("kchugh@xtaascorp.com"));
			qaFilterDTO.setFromEmail("data@xtaascorp.com");
			qaFilterDTO.setStartDate(simpleDateFormat.format(startCalendar.getTime()));
			qaFilterDTO.setEndDate(simpleDateFormat.format(endCalendar.getTime()));
//			filterDTOs.add(qaFilterDTO);
			for (TwilioCallLogFilterDTO twilioCallLogFilterDTO : filterDTOs) {
				twilioCallLogAndDataBuyService.generateCallLog(twilioCallLogFilterDTO, false);
			}
		}
	}

}
