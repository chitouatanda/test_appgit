package com.xtaas.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PlivoCallLogService;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.web.dto.PlivoCallLogFilterDTO;

@Service
public class PlivoCallLogJob {

	private static final Logger logger = LoggerFactory.getLogger(PlivoCallLogJob.class);

	@Autowired
	PlivoCallLogService plivoCallLogService;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 0 18 * * MON-FRI", zone = "PST")
	public void generatePlivoCallLogs() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.TWILIO_CALL_LOG_JOB_INSTANCE.name(), "xtaasupgrade");
		if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
			logger.debug("<========== Running PlivoCallLogJob for incoming voicemails. ==========>");
			Calendar startCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			Calendar endCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			endCalendar.add(Calendar.DAY_OF_MONTH, 1);
			Date startDate = XtaasDateUtils.getDateAtTime(startCalendar.getTime(), 0, 0, 0, 0);
			Date endDate = XtaasDateUtils.getDateAtTime(endCalendar.getTime(), 23, 59, 59, 0);
			List<PlivoCallLogFilterDTO> filterDTOs = new ArrayList<>();

			PlivoCallLogFilterDTO masterFilterDTO = new PlivoCallLogFilterDTO();
			masterFilterDTO.setProfile("MASTER");
			masterFilterDTO.setToEmails(Arrays.asList("reports@xtaascorp.com"));
			masterFilterDTO.setFromEmail("data@xtaascorp.com");
			masterFilterDTO.setStartDate(startDate);
			masterFilterDTO.setEndDate(endDate);
//			filterDTOs.add(masterFilterDTO);

			PlivoCallLogFilterDTO prodFilterDTO = new PlivoCallLogFilterDTO();
			prodFilterDTO.setProfile("XTAAS CALL CENTER");
			prodFilterDTO.setToEmails(Arrays.asList("reports@xtaascorp.com"));
			prodFilterDTO.setFromEmail("data@xtaascorp.com");
			prodFilterDTO.setStartDate(startDate);
			prodFilterDTO.setEndDate(endDate);
			filterDTOs.add(prodFilterDTO);

			PlivoCallLogFilterDTO demandshoreFilterDTO = new PlivoCallLogFilterDTO();
			demandshoreFilterDTO.setProfile("DEMANDSHORE");
			demandshoreFilterDTO.setToEmails(Arrays.asList("dnc@demandshore.com"));
			demandshoreFilterDTO.setFromEmail("data@xtaascorp.com");
			demandshoreFilterDTO.setStartDate(startDate);
			demandshoreFilterDTO.setEndDate(endDate);
			filterDTOs.add(demandshoreFilterDTO);

			PlivoCallLogFilterDTO qaFilterDTO = new PlivoCallLogFilterDTO();
			qaFilterDTO.setProfile("QA");
			qaFilterDTO.setToEmails(Arrays.asList("kchugh@xtaascorp.com"));
			qaFilterDTO.setFromEmail("data@xtaascorp.com");
			qaFilterDTO.setStartDate(startDate);
			qaFilterDTO.setEndDate(endDate);
//			filterDTOs.add(qaFilterDTO);
			for (PlivoCallLogFilterDTO plivoCallLogFilterDTO : filterDTOs) {
				plivoCallLogService.sendIncomingVoiceMails(plivoCallLogFilterDTO);
			}
		}
	}

}
