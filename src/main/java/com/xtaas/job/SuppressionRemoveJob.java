package com.xtaas.job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.CampaignSuppressionServiceImpl;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;
@Service
public class SuppressionRemoveJob {

	private final Logger logger = LoggerFactory.getLogger(SuppressionRemoveJob.class);

	@Autowired
	CampaignSuppressionServiceImpl campaignSuppressionServiceImpl;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "* * 6 * * *", zone = "GMT+5:30")
	public void removeDublicateSuppressionList() {

		logger.debug("Cron job Started for remove Dublicate Suppression List.");
		try{
       	String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
   		String instance = propertyService.getApplicationPropertyValue(
   				XtaasConstants.APPLICATION_PROPERTY.SUPPRESSION_REMOVE.name(), "xtaasupgrade");
   		if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
    			campaignSuppressionServiceImpl.removeDublicateRecords();
   		}

        }catch(Exception e){
        	e.printStackTrace();
        }
        logger.debug("Cron job End for remove Dublicate Suppression List.");
	}
}