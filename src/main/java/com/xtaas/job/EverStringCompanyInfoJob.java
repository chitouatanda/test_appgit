package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.infra.everstring.service.EnrichmentAPIServiceImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

@Service
public class EverStringCompanyInfoJob {

	private final Logger logger = LoggerFactory.getLogger(EverStringCompanyInfoJob.class);

	@Autowired
	EnrichmentAPIServiceImpl enrichmentAPIServiceImpl;
	
	@Autowired
	private PropertyService propertyService;
	
	@Scheduled(cron = "0 40 2 * * MON-FRI", zone = "PST")
	public void getCompanyInfo() {

		logger.debug("Cron job Started for get company info.");
		System.out.println("Starting");
		try{
        	String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
    		String instance = propertyService.getApplicationPropertyValue(
    				XtaasConstants.APPLICATION_PROPERTY.EVERSTRING_API.name(), "xtaasupgrade");
    		if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
    			enrichmentAPIServiceImpl.callEnrichmentAPIByCampaignIdAndStatus("", false);
    		}
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        System.out.println("Ending");
        logger.debug("Cron job End for get company info.");
	}

}
