package com.xtaas.job;

import com.xtaas.service.CallbackNotificationServiceImpl;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The job notifies the agent that callback is scheduled with the prospect
 * before 10 minutes and creates entry in team notice collection and maintains
 * the agent and prospect association.
 * 
 * Job runs every minute.
 */
@Service
public class CallbackNotificationJob {
	public static final Logger logger = LoggerFactory.getLogger(CallbackNotificationJob.class);

	final private CallbackNotificationServiceImpl callbackNotificationServiceImpl;

	public CallbackNotificationJob(@Autowired CallbackNotificationServiceImpl callbackNotificationServiceImpl) {
		this.callbackNotificationServiceImpl = callbackNotificationServiceImpl;
	}

	@Scheduled(cron = "0 */1 * * * *", zone = "Asia/Kolkata")
	public void sendNotification() {
		logger.info("Cron job started to send callbacknotification to agent.");
		try {
			callbackNotificationServiceImpl.sendCallbackNotificationToAgentBefore10Minutes();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Cron job ended to send callbacknotification to agent.");
	}

}