package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.TranscriptionServiceImpl;
import com.xtaas.utils.XtaasConstants;

/**
 * The job transcribes the voicemails after every 2 hours.
 * 
 * 
 * Purpose :- To convert the voicemail to speech so that we can read voicemails
 * instead of hearing.
 * 
 * Job will run after every 2 hours from Monday to Friday.
 */

@Service
public class VMTranscriptionJob {

	private static final Logger logger = LoggerFactory.getLogger(VMTranscriptionJob.class);

	@Autowired
	private TranscriptionServiceImpl transcriptionServiceImpl;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 0 */2 * * MON-FRI", zone = "PST")
	public void transcribeVoiceMails() {
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			String instance = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.RECORDING_TRANSCRIPTION_JOB_INSTANCE.name(), "xtaasupgrade");
			logger.debug("Cron job started for voice mail transcription.");
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				transcriptionServiceImpl.transcriptVoiceMails();
			}
			logger.debug("Cron job ended for voice mail transcription.");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
