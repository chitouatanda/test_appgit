package com.xtaas.job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.xtaas.BeanLocator;
import com.xtaas.application.service.CampaignService;
import com.xtaas.db.entity.CampaignMetaData;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.Organization;
import com.xtaas.db.repository.CampaignMetaDataRepository;
import com.xtaas.db.repository.DataBuyQueue;
import com.xtaas.db.repository.DataBuyQueueRepository;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.OrganizationRepository;
import com.xtaas.db.repository.DataBuyQueue.DataBuyQueueStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.infra.zoominfo.service.ZoomInfoContactListProviderNewImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.SalutaryServiceImpl;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.ZoomBuySearchDTO;

@Service
public class DataBuyJob {


	private static final Logger logger = LoggerFactory.getLogger(DataBuyJob.class);

	@Autowired
	private ZoomInfoContactListProviderNewImpl zoomInfoContactListProviderNewImpl;

	@Autowired
	private DataBuyQueueRepository dataBuyQueueRepository;

	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private SalutaryServiceImpl salutaryServiceImpl;
	
	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private CampaignMetaDataRepository campaignMetaDataRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	

	//@Scheduled(cron = "0 */2 * * * *", zone = "GMT+5:30")
	public void buyData() {
		logger.debug("run() : DataBuyThread STARTED");
		
		//while (!Thread.interrupted()) {
			try {
				String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
				String instance = propertyService.getApplicationPropertyValue(
						XtaasConstants.APPLICATION_PROPERTY.DATA_BUY_THREAD_INSTANCE.name(), "xtaascorp.herokuapp.com");
				if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
					purchaseRecordsJob();
				}
				//this.setServerGettingStarted(false);

				//Thread.sleep(12000);
			} catch (Exception e) {
				try {
					logger.error("Error Occurred: " + Thread.currentThread().getName(), e);
					//Thread.sleep(12000);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		//}
	}

	public String purchaseRecordsJob() {
		String statusMessage = "";
		int qcounter = 0;
		int pcounter = 0;
		logger.debug("In PurchaseRecrodJob");
		List<DataBuyQueue> queuedList = new ArrayList<DataBuyQueue>();
		try {
			List<DataBuyQueue> dataBuyQueueList = getCampaignsFromQueue();
			
			for (DataBuyQueue dbq : dataBuyQueueList) {
				
				if (dbq.getStatus().equals(DataBuyQueueStatus.PAUSED.toString())) {
					queuedList.add(dbq);
					qcounter++;
				}/*else if (dbq.getStatus().equals("PASS")) {
					queuedList.add(dbq);
					qcounter++;
				}*/else if (dbq.getStatus().equals(DataBuyQueueStatus.PAUSEDZOOM.toString())) {
					queuedList.add(dbq);
					qcounter++;
				}else if (dbq.getStatus().equals(DataBuyQueueStatus.QUEUED.toString())) {
					queuedList.add(dbq);
					qcounter++;
				}else if (dbq.getStatus().equals(DataBuyQueueStatus.INPROCESS.toString())) {
					pcounter++;
				}
			}
			int maxjobbuys = propertyService
					.getIntApplicationPropertyValue(XtaasConstants.APPLICATION_PROPERTY.MAX_BUY_JOBS.name(), 10);
			if (pcounter < maxjobbuys) {
				if (qcounter > 0) {
					DataBuyQueue dbq = queuedList.get(0);				 
					if (dbq.isSearchCompany()) {
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
						dataBuyQueueRepository.save(dbq);
						ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
						logger.debug("Starting Search Company");
						zoomBuySearchDTO.setDataBuyQueue(dbq);
						ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(
								zoomBuySearchDTO);
						Thread t1 = new Thread(zilp, "t" + Math.random());
						t1.start();
					} else if(dbq.getStatus().equals(DataBuyQueueStatus.PAUSEDZOOM.toString())){
						//This i have kept if the server restart or stuck, to start from where it has been stopped
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
							dataBuyQueueRepository.save(dbq);
							logger.debug("Starting ZoomInfo Buy");
							ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
							zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
							zoomBuySearchDTO.setTotalCount(dbq.getRequestCount());
							zoomBuySearchDTO.setDataBuyQueue(dbq);
							ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(
									zoomBuySearchDTO);
							Thread t1 = new Thread(zilp, "t" + Math.random());
							t1.start();
					}else {
						dbq.setStatus(DataBuyQueueStatus.INPROCESS.toString());
						dbq.setJobStartTime(new Date());
						dataBuyQueueRepository.save(dbq);
						buyFromOtherSources(dbq);
					}	
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusMessage;
	}

	public List<DataBuyQueue> getCampaignsFromError() {

		List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>();
		List<String> dataBuyStatusList = new ArrayList<String>();
		try {
			dataBuyStatusList.add(DataBuyQueueStatus.ERROR.toString());
			Pageable pageable = PageRequest.of(0, 100, Direction.ASC, "jobRequestTime");
			dataBuyList = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataBuyList;
	}

	public List<DataBuyQueue> getCampaignsFromQueue() {
		List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>();
		List<DataBuyQueue> dataBuyListStarting = new ArrayList<DataBuyQueue>();
		List<String> dataBuyStatusList = new ArrayList<String>();

		dataBuyStatusList.add(DataBuyQueueStatus.INPROCESS.toString());

		try {
			dataBuyStatusList.add(DataBuyQueueStatus.QUEUED.toString());
			dataBuyStatusList.add(DataBuyQueueStatus.PAUSED.toString());
			// dataBuyStatusList.add("PASS");
			dataBuyStatusList.add(DataBuyQueueStatus.PAUSEDZOOM.toString());

			Pageable pageable = PageRequest.of(0, 500, Direction.ASC, "jobRequestTime");
			dataBuyList = dataBuyQueueRepository.findByStatus(dataBuyStatusList, pageable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataBuyList;
	}
	
	private void buyFromOtherSources(DataBuyQueue dbq) {
		long totalRequestCount = dbq.getRequestCount();
		Campaign campaign = campaignService.getCampaign(dbq.getCampaignId());
		List<KeyValuePair<String, Integer>> dataSourcePriorityMap = new ArrayList<>();
		Long salutaryBought = 0L;
		Long insideViewBought = 0L;
		Long zoomBought = 0L;

		
		if (campaign.getDataSourcePriority() != null) {
			dataSourcePriorityMap = campaign.getDataSourcePriority();	
		} else {
			Organization org = organizationRepository.findOneById(campaign.getOrganizationId());
			if (org.getCampaignDefaultSetting() != null && org.getCampaignDefaultSetting().getDataSourcePriority() != null) {
				dataSourcePriorityMap = org.getCampaignDefaultSetting().getDataSourcePriority();
			} else {
				CampaignMetaData campaignMetaData = campaignMetaDataRepository.findOneById("CAMPAIGN_DEFAULT");
				dataSourcePriorityMap = campaignMetaData.getDataSourcePriority();
			}
		}
		/*if(campaign.isABM()) {
			// TODO for now ABM is only happening through zoominfo.
			dbq.setZoomInfoRequestCount(dbq.getRequestCount());
			dbq = dataBuyQueueRepository.save(dbq);
			ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
			zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
			zoomBuySearchDTO.setTotalCount(dbq.getRequestCount());
			zoomBuySearchDTO.setDataBuyQueue(dbq);
			ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(
			zoomBuySearchDTO);
			Thread t1 = new Thread(zilp, "t" + Math.random());
			t1.start();
		}else*/ if(dataSourcePriorityMap!=null && dataSourcePriorityMap.size()>0) {
			for(KeyValuePair<String,Integer> keyValuePair : dataSourcePriorityMap) {
				if(keyValuePair.getKey().equalsIgnoreCase("Salutary")) {
					if(keyValuePair.getValue()>0) {
						Long salutaryBuyRequest = Double.valueOf((keyValuePair.getValue()/100.00) * dbq.getRequestCount()).longValue();
						dbq.setSalutaryRequestCount(salutaryBuyRequest);
						dbq = dataBuyQueueRepository.save(dbq);
						dbq = buyDataFromSaluatary(campaign,dbq,salutaryBuyRequest);
						if(dbq.getSalutaryPurchasedCount()!=null)
							salutaryBought =dbq.getSalutaryPurchasedCount();
					}
				} else if(keyValuePair.getKey().equalsIgnoreCase("InsideView")) {
					//TODO commented for now.
					/*if(keyValuePair.getValue()>0) {
						Long insideViewBuyRequest = Double.valueOf((keyValuePair.getValue()/100.00) * dbq.getRequestCount()).longValue();
						dbq.setInsideViewRequestCount(insideViewBuyRequest);
						dbq = dataBuyQueueRepository.save(dbq);
						dbq =  buyDataFrominsideView(campaign,dbq,insideViewBuyRequest);
						if(dbq.getInsideViewPurchasedCount()!=null)
							insideViewBought = dbq.getInsideViewPurchasedCount();
					}*/
				}
				
			}

			///Default, what ever is remaining should be bought from zoom
			// latch
				
			Long zoomBuyRequest = totalRequestCount-(salutaryBought+insideViewBought);
			//zoomBuyRequest = Double.valueOf((keyValuePair.getValue()/100.00) * dbq.getRequestCount()).longValue();
			//Initiate a thread to buy data from zoominfo
			//dataBuyQueueRepository.save(dbq);
			logger.info("Starting ZoomInfo Buy");
			//commented to test salutary databuy
			if(zoomBuyRequest!=null && zoomBuyRequest>0) {	
				dbq.setZoomInfoRequestCount(zoomBuyRequest);
				dbq = dataBuyQueueRepository.save(dbq);
				ZoomBuySearchDTO zoomBuySearchDTO = new ZoomBuySearchDTO();
				zoomBuySearchDTO.setCampaignId(dbq.getCampaignId());
				zoomBuySearchDTO.setTotalCount(zoomBuyRequest);
				zoomBuySearchDTO.setDataBuyQueue(dbq);
				ZoomInfoContactListProviderNewImpl zilp = new ZoomInfoContactListProviderNewImpl(
				zoomBuySearchDTO);
				Thread t1 = new Thread(zilp, "t" + Math.random());
				t1.start();
			}else {
				dbq.setStatus(DataBuyQueueStatus.COMPLETED.toString());
		        dbq.setJobEndTime(new Date());
		        dataBuyQueueRepository.save(dbq);
		        List<String> toemails =  new ArrayList<String>();
		        toemails.add("data@xtaascorp.com");
		        if(dbq.getRequestorId()!=null && !dbq.getRequestorId().isEmpty()) {
		        	Login l = loginRepository.findOneById(dbq.getRequestorId());
		        	if(l.getEmail()!=null  && !l.getEmail().isEmpty())
		        	toemails.add(l.getEmail());
		        }
			  XtaasEmailUtils.sendEmail(toemails, "data@xtaascorp.com",
						campaign.getName()+" Data purchase job completed", dbq.getTotalBought()+" records purchased for this job");
			}
		}
	}
	
	private DataBuyQueue buyDataFromSaluatary(Campaign campaign,DataBuyQueue dbq, Long salutaryBuyRequest) {
		Long salutaryPurchaseCount = salutaryServiceImpl.databuySaltary(dbq.getCampaignCriteriaDTO(), salutaryBuyRequest, dbq.getCampaignId(),true);
		/*if(salutaryBuyRequest>salutaryPurchaseCount) {
       	 salutaryPurchaseCount = salutaryServiceImpl.databuySaltary(dbq.getCampaignCriteriaDTO(), salutaryBuyRequest-salutaryPurchaseCount, dbq.getCampaignId(),false);
		}*/
		logger.info("Salutary databuy purchase count : " + salutaryPurchaseCount);
		if(salutaryBuyRequest!=null && salutaryBuyRequest >  0 && salutaryPurchaseCount!=null && salutaryPurchaseCount >  0) {
			dbq.setSalutaryPurchasedCount(salutaryPurchaseCount);
			if(dbq.getPurchasedCount()!=null) {
				dbq.setPurchasedCount(dbq.getPurchasedCount()-salutaryPurchaseCount);
			}else {
				dbq.setPurchasedCount(salutaryPurchaseCount);
			}
			
			dbq = dataBuyQueueRepository.save(dbq);
		}else {
			salutaryPurchaseCount = 0L;
		}
		return dbq;
	}
	
	private DataBuyQueue buyDataFrominsideView(Campaign campaign,DataBuyQueue dbq, Long insideViewBuyRequest) {
		Long ivPurchaseCount = 0L;//TODO  actual call to insideview apis should be called from here.
		logger.info("Inside View databuy purchase count : " + ivPurchaseCount);
		if(insideViewBuyRequest!=null && insideViewBuyRequest >  0 && ivPurchaseCount!=null && ivPurchaseCount >  0) {
			dbq.setInsideViewPurchasedCount(ivPurchaseCount);
			if(dbq.getPurchasedCount()!=null) {
				dbq.setPurchasedCount(dbq.getPurchasedCount()-ivPurchaseCount);
			}else {
				dbq.setPurchasedCount(0L);
			}
			dbq = dataBuyQueueRepository.save(dbq);
		}else {
			ivPurchaseCount = 0L;
		}
		return dbq;
	}
	
	
	/*
	 * public List<DataBuyQueue> getCampaignsFromProcess(){
	 * 
	 * List<DataBuyQueue> dataBuyList = new ArrayList<DataBuyQueue>(); List<String>
	 * dataBuyStatusList = new ArrayList<String>(); try{
	 * //dataBuyStatusList.add(DataBuyQueueStatus.QUEUED.toString());
	 * dataBuyStatusList.add(DataBuyQueueStatus.INPROCESS.toString());
	 * //dataBuyStatusList.add(DataBuyQueueStatus.KILL.toString()); Pageable
	 * pageable = PageRequest.of(0, 100, Direction.ASC, "jobRequestTime");
	 * dataBuyList =
	 * dataBuyQueueRepository.findByStatus(dataBuyStatusList,pageable);
	 * }catch(Exception e){ e.printStackTrace(); } return dataBuyList; }
	 */


}
