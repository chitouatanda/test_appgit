package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.DeactivateAgentServiceImpl;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/***
 * The job deactivates the agents who are absent since last given configurable days.
 * and sends list of deactivated agents in the email.
 * 
 * Purpose :- To identify the agents are taking more leaves.
 * 
 * The job will run @5:00PM IST Monday to Friday.
 */
@Service
public class DeactivateAgentJob {

	private static final Logger logger = LoggerFactory.getLogger(DeactivateAgentJob.class);

	@Autowired
	private DeactivateAgentServiceImpl deactivateAgentServiceImpl;

	@Autowired
	private PropertyService propertyService;

//	@Scheduled(cron = "0 0 17 * * MON-FRI", zone = "Asia/Kolkata")
	public void inActiveAgents() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.AGENT_DEACTIVATE_JOB_INSTANCE.name(), "xtaasupgrade");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.info("Cron job started to inactive agents absent for last two days.");
				deactivateAgentServiceImpl.deActivateAgents();
				logger.info("Cron job ended to inactive agents absent for last two days.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}