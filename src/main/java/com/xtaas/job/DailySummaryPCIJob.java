package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.DailySummaryPCIServiceImpl;

@Service
public class DailySummaryPCIJob {

	private static final Logger logger = LoggerFactory.getLogger(DailySummaryPCIJob.class);
	
	@Autowired
	private DailySummaryPCIServiceImpl dailySummaryPCIServiceImpl;
	//At every 15th minute on every day-of-week from Monday through Friday.
//	 @Scheduled(cron = "0 */15 * * * MON-FRI", zone = "PST")
		public void saveCallStats() {
			logger.info("Cron job Started for save summery pci.");
			System.out.println("Starting");
			try {
				dailySummaryPCIServiceImpl.saveCallstats();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Ending");
			logger.debug("Cron job End for save summery pci.");
		}
	 // At minute 5 past every hour on every day-of-week from Monday through Friday.
//	 @Scheduled(cron = "0 5 */1 * * MON-FRI", zone = "PST")
		public void sendDailySummaryPCIReport() {
			logger.info("Cron job Started for summery pci report.");
			System.out.println("Starting");
			try {
				dailySummaryPCIServiceImpl.createDailySummaryPCIReport();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Ending");
			logger.debug("Cron job End for summery pci report.");
		}
}
