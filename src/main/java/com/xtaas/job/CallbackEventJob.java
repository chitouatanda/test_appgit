package com.xtaas.job;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.xtaas.service.CallbackEventServiceImpl;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The job sends the prospect to agent after 10 minutes when notified the agent.
 * 
 * Job runs every minute.
 */
@Service
public class CallbackEventJob {
	public static final Logger logger = LoggerFactory.getLogger(CallbackEventJob.class);

	final private CallbackEventServiceImpl callbackEventServiceImpl;

	public CallbackEventJob(@Autowired CallbackEventServiceImpl callbackEventServiceImpl) {
		this.callbackEventServiceImpl = callbackEventServiceImpl;
	}

	@Scheduled(cron = "0 */1 * * * *", zone = "Asia/Kolkata")
	public void sendCallbackEvent() {
		logger.info("Cron job started to send callback prospect to agent after 10 min.");
		try {
			callbackEventServiceImpl.sendCallbackProspectToAgent();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Cron job ended to send callback prospect to agent after 10 min.");
	}

}