package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.DailyBillingReportServiceImpl;

@Service
public class DailyBillingReportJob {
	private static final Logger logger = LoggerFactory.getLogger(DailyBillingReportJob.class);
	
	@Autowired
	private DailyBillingReportServiceImpl dailyBillingReportServiceImpl;
	
	// @Scheduled(cron = "0 */1 * * * MON-FRI", zone = "IST")
	public void DailyBillingReport() {
		logger.debug("Cron job Started for daily billing report.");
		System.out.println("Starting");
		try {
			dailyBillingReportServiceImpl.createBillingReports();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Ending");
		logger.debug("Cron job End for daily billing report.");
	}
}
