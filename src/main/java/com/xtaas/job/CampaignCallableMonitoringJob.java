package com.xtaas.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.CampaignCallableMonitoringService;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

/**
 * The job will send notification to the all agents where callables in the
 * campaign becomes zero. The agent will log out of the system when this
 * notification comes.
 *
 * Purpose :- When there are no callables in the campaign agent and supervisor
 * should get alert of the same.
 * 
 * Job will run after every 5 minutes.
 */

@Service
public class CampaignCallableMonitoringJob {

	private static final Logger logger = LoggerFactory.getLogger(CampaignCallableMonitoringJob.class);

	@Autowired
	private CampaignCallableMonitoringService campaignCallableMonitoringService;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 */3 * * * *", zone = "Asia/Kolkata")
	public void monitorCampaigns() {
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			String instance = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.CAMPAIGN_CALLABLE_MONITOR_JOB_INSTANCE.name(), "xtaascorp");
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.info("Cron job started to monitor callables in the campaign.");
				campaignCallableMonitoringService.monitorCallablesInCampaign();
				logger.info("Cron job ended to monitor callables in the campaign.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}