package com.xtaas.job;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.DownloadRecordingsToAws;
import com.xtaas.db.repository.ZoomTokenRepository;
import com.xtaas.domain.entity.ZoomToken;
import com.xtaas.infra.zoominfo.service.ZoomOAuth;
import com.xtaas.service.AgentOnlineJobReportServiceImpl;
import com.xtaas.service.AgentOnlineReportServiceImpl;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;


@Service
public class ZoomOAuthTokenJob {

	private static final Logger logger = LoggerFactory.getLogger(ZoomOAuthTokenJob.class);

	@Autowired
	private ZoomTokenRepository zoomTokenRepository;
	
	@Autowired
	private ZoomOAuth zoomOAuth;
	
	@Autowired
	private DownloadRecordingsToAws downloadRecordingsToAws;


	@Autowired
	private PropertyService propertyService;
	
	int retry = 0;

//TODO uncomment when completely move to upgrade instanE
	//@Scheduled(cron = "59 * * * * *")
	public void getZoomOAuthTokenJob() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.ZOOM_TOKEN_THREAD_INSTANCE.name(), "xtaascorplive");
		try {
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.debug("Cron job started to to check zoomtoken.");
				generateZoomToken(retry);
				logger.debug("Cron job ended to check zoomtoken.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void generateZoomToken(int retry) throws Exception {
		String oAuth = "";
		ZoomToken zt = new ZoomToken();
		Pageable pageable = PageRequest.of(0, 1, Direction.DESC, "updatedDate");
		Date currDate = new Date();
		List<ZoomToken> zoomTokenList = zoomTokenRepository.findLatestToken(pageable);
		ZoomToken ezt = zoomTokenList.get(0);
		long duration = currDate.getTime() - ezt.getCreatedDate().getTime();
		long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		if (diffInMinutes >= 350) {
			oAuth = zoomOAuth.createJWT();
			if (oAuth != null && !oAuth.isEmpty()) {
				// tokenKey =oAuth;
				ezt.setStatus("INACTIVE");
				zoomTokenRepository.save(ezt);
				ZoomToken newZoomToken = new ZoomToken();
				newZoomToken.setOauthToken(oAuth);
				newZoomToken.setStatus("ACTIVE");
				zoomTokenRepository.save(newZoomToken);
				downloadRecordingsToAws.updateZoomTokenToS3(newZoomToken.getOauthToken());
				// cachedDate = null;
			} else {
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
				retry = 1;
				System.out.println("Retrying");
				Thread.sleep(6000);
			}
		} /*
			 * else{ // oAuth = zt.getOauthToken(); // tokenKey = oAuth; }
			 */
		// return oAuth;

	}
}