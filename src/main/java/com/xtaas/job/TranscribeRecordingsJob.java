package com.xtaas.job;

import com.xtaas.ml.transcribe.service.TranscriptionAPIService;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.utils.XtaasConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class TranscribeRecordingsJob {
	public static final Logger logger = LoggerFactory.getLogger(TranscribeRecordingsJob.class);

	final private PropertyService propertyService;

	final private TranscriptionAPIService transcriptionAPIService;

	public TranscribeRecordingsJob(@Autowired PropertyService propertyService,
			@Autowired TranscriptionAPIService transcriptionAPIService) {
		this.propertyService = propertyService;
		this.transcriptionAPIService = transcriptionAPIService;
	}

	/**
	 * Any scheduled transcription job will be picked up in the corresponding 5 min
	 * slot for transcription.
	 */

	@Scheduled(cron = "0 */3 * * * *", zone = "Asia/Kolkata")
	public void transcribe() {
		String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
		String instance = propertyService.getApplicationPropertyValue(
				XtaasConstants.APPLICATION_PROPERTY.TRANSCRIPTION_JOB_INSTANCE.name(), "xtaascorp.herokuapp.com");
		if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
			logger.info("Cron job started for recording transcription.");
			transcriptionAPIService.transcribeRecordings();
			logger.info("Cron job ended for recording transcription.");
		}

	}

}