package com.xtaas.job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.SendGridServiceImpl;
import com.xtaas.utils.XtaasConstants;
@Service
public class SendGridEmailJob {

	private final Logger logger = LoggerFactory.getLogger(SendGridEmailJob.class);

	@Autowired
	SendGridServiceImpl sendGridServiceImpl;

	@Autowired
	private PropertyService propertyService;

	@Scheduled(cron = "0 */30 * * * *", zone = "GMT+5:30")
	public void sendSendGridEmail() {

		logger.info("Cron job Started for send sendgrid email.");
		try {
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			String instance = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.SENDGRID_EMAIL_SEND.name(), "xtaascorplive");
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				sendGridServiceImpl.sendSendGridEmail();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Cron job End for send sendgrid email.");
	}
}