package com.xtaas.job;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.xtaas.service.ApplicationEnvironmentPropertyUtils;
import com.xtaas.service.PropertyService;
import com.xtaas.service.UpdateCallDialedLeadsServiceImpl;
import com.xtaas.utils.XtaasConstants;

/**
 * The job updates the leads which are stuck in CALL_DIALED to disposition
 * status AUTO.
 * 
 * Purpose :- To move the prospects which are stuck in CALL_DIALED to AUTO so
 * that they can be callable for the campaign.
 * 
 * Job will run after every 30 minutes IST from Monday to Friday.
 */

@Service
public class UpdateCallDialedLeadsJob {

	private static final Logger logger = LoggerFactory.getLogger(UpdateCallDialedLeadsJob.class);

	@Autowired
	private UpdateCallDialedLeadsServiceImpl updateCallDialedLeadsServiceImpl;

	@Autowired
	private PropertyService propertyService;

	 @Scheduled(cron = "0 */30 * * * *", zone = "Asia/Kolkata")
	public void updateLeads() {
		try {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));	
				SecurityContextHolder.getContext().setAuthentication(
					new UsernamePasswordAuthenticationToken("call_dialed_to_auto_updatejob", "secret", authorities));
			String serverUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl();
			String instance = propertyService.getApplicationPropertyValue(
					XtaasConstants.APPLICATION_PROPERTY.UPDATE_CALL_DIALED_TO_AUTO_THREAD_INSTANCE.name(),
					"xtaascorp");
			if (serverUrl != null && instance != null && serverUrl.toLowerCase().contains(instance.toLowerCase())) {
				logger.info("Cron job started to update CALL_DIALED leads.");
				updateCallDialedLeadsServiceImpl.moveLeadsToAuto();
				logger.info("Cron job ended for update CALL_DIALED leads.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
