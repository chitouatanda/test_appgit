package com.xtaas.job;

import com.xtaas.ml.classifier.service.CallClassificationService;
import com.xtaas.ml.classifier.service.GoogleClassifierStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CallClassificationJob {
    public static final Logger logger = LoggerFactory.getLogger(CallClassificationJob.class);
    
    final private CallClassificationService callClassificationService;
    final private GoogleClassifierStatistics classifierStatistics;
    final private String serverUrl;
    final private String jobInstance;

    public CallClassificationJob(
            @Autowired CallClassificationService queueService,
            @Autowired GoogleClassifierStatistics classifierStatistics,
            @Value("${server.base.url}") String serverUrl,
            @Value("${classifier.job.instance:peaceful}") String jobInstance) {

        this.callClassificationService = queueService;
        this.classifierStatistics = classifierStatistics;
        this.serverUrl = serverUrl;
        this.jobInstance = jobInstance;
    }
  /**
     * Any scheduled classification job will be picked up in the corresponding 30 min slot
     * with a max delay of 30 mins subject to downstream application throttling and delay
     */
    @Scheduled(cron = "${classifier.job.cron_expr:0 */1 * * * *}",
               zone = "${classifier.job.cron_zone:Asia/Kolkata}")
//    @Scheduled(cron = "0 */1 * * * *", zone = "Asia/Kolkata")	
    public void classifyCallRecordings() {
         if (serverUrl == null && jobInstance == null || !serverUrl.toLowerCase().contains(jobInstance.toLowerCase())) {
            return;
        }
        logger.info("Cron job started for call classification.");
        callClassificationService.classifyCallRecordings();
        saveStats();
        logger.info("Cron job ended for call classification.");
    }


	/**
	 * Saves the stats received from google for each recording in
	 * "ClassifierStatisticsLog", "ClassifierSampleLog" collections in db respectively.
	 */
	private void saveStats() {
		classifierStatistics.sampleSnapshot();
		classifierStatistics.statsSapshot();
	}
}