package com.xtaas.domain.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection="supervisorstats")
public class SupervisorStats extends AbstractEntity{

	 	String supervisorId;
	 	Date date;
	 	String campaignManagerId;
	 	String campaignId;
	 	String status;
		String type;
		String error; 
	 	
		public String getSupervisorId() {
			return supervisorId;
		}
		public void setSupervisorId(String supervisorId) {
			this.supervisorId = supervisorId;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public String getCampaignId() {
			return campaignId;
		}
		public void setCampaignId(String campaignId) {
			this.campaignId = campaignId;
		}
		public String getCampaignManagerId() {
			return campaignManagerId;
		}
		public void setCampaignManagerId(String campaignManagerId) {
			this.campaignManagerId = campaignManagerId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}
	 	
		 
}
