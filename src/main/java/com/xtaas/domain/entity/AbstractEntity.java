package com.xtaas.domain.entity;

import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author djain
 *
 */
public abstract class AbstractEntity {
	
	@Id
	private String id;
	
	@CreatedDate
	private Date createdDate;
	
	@CreatedBy
	private String createdBy;
	
	@LastModifiedDate
	private Date updatedDate;
	
	@LastModifiedBy
	private String updatedBy;
	
	@LastModifiedDate
	private long updatedDateNum;
	
	@CreatedDate
	private long createdDateNum;

	
	@Version
	private Integer version;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	protected void setId(String id)  {
		if (id == null) {
			throw new IllegalArgumentException("Id is required");
		}
		this.id = id;
	}
	
	/*
	 *
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	
	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}
	
	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}
	
	/**
	 * @param version the version to set
	 */
	protected void setVersion(Integer version) {
		this.version = version;
	}

	
	protected long getUpdatedDateNum() {
		Date qlikDate = updatedDate;
		updatedDateNum = qlikDate.getTime();
		return updatedDateNum;
	}
	
	protected long getCreatedDateNum() {
		Date qlikDate = createdDate;
		createdDateNum = qlikDate.getTime();
		return createdDateNum;
	}

	
	
	
}
