package com.xtaas.domain.entity;

import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="fakeaddress")
public class FakeAddress extends AbstractEntity {
	private String title;
	private String givenName;
	private String surname;
	private String name;
	private String line1;
	private String city;
	private String state;
	private String stateCode;
	private String postalCode;
	private String country;
	private String countryCode;
	private String timeZone;
	private String languages;

	public String getName() {
		return name;
	}

	public String getLine1() {
		return line1;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}
	
	public String getStateCode() {
		return stateCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCountry() {
		return country;
	}

	public String getCountryCode() {
		return countryCode;
	}
	
	
	
	public String getTimeZone() {
		return timeZone;
	}

	public String getLanguages() {
		return languages;
	}

	public String getByKey(String key) {
		if (key.equals("countryCode")) {
			return countryCode;
		} else if (key.equals("stateCode")) {
			return stateCode;
		} else if (key.equals("city")) {
			return city;
		}
		return "";
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surname;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surname = surName;
	}

}
