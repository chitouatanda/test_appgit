package com.xtaas.domain.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pclog_enc_new")
public class PcLogNew extends AbstractEntity {

//	private ProspectName prospectInfo;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String company;
	private String comment;

	public PcLogNew() {
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PcLogNew(String firstName, String lastName, String email, String phone, String company, String comment) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.company = company;
		this.comment = comment;
	}

}