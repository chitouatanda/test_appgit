/**
 * 
 */
package com.xtaas.domain.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * A read-only configuration table used by dialer. Not intended to be changed thru application
 * 
 * @author djain
 *
 */
@Document(collection="outboundnumber")
public class OutboundNumber extends AbstractEntity {
	private String phoneNumber;
	private String displayPhoneNumber;
	private int areaCode;
	private int isdCode;
	private String provider;
	private String partnerId;
	private String bucketName;
	private String voiceMessage;
	private String timeZone;
	private boolean isRedialNumber;
	private String countryCode;
	private int pool;
	private String sid;
	private String countryName;
	
	public OutboundNumber(String phoneNumber, String displayPhoneNumber, int areaCode, String timeZone, int isdCode, String provider, String partnerId, String bucketName, String countryName) {
		setPhoneNumber(phoneNumber);
		setDisplayPhoneNumber(displayPhoneNumber);
		setAreaCode(areaCode);
		setIsdCode(isdCode);
		setProvider(provider);
		setPartnerId(partnerId);
		setBucketName(bucketName);
		setRedialNumber(false);
		setTimeZone(timeZone);
		setCountryName(countryName);
	}
	

	@Override
	public String toString() {
		return "OutboundNumber [phoneNumber=" + phoneNumber + ", displayPhoneNumber=" + displayPhoneNumber
				+ ", areaCode=" + areaCode + ", isdCode=" + isdCode + ", provider=" + provider + ", partnerId="
				+ partnerId + ", bucketName=" + bucketName + ", voiceMessage=" + voiceMessage + ", timeZone=" + timeZone
				+ ", isRedialNumber=" + isRedialNumber + ", countryCode=" + countryCode + ", pool=" + pool + ", sid="
				+ sid + ", countryName=" + countryName + "]";
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	private void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the displayPhoneNumber
	 */
	public String getDisplayPhoneNumber() {
		return displayPhoneNumber;
	}
	/**
	 * @param displayPhoneNumber the displayPhoneNumber to set
	 */
	private void setDisplayPhoneNumber(String displayPhoneNumber) {
		this.displayPhoneNumber = displayPhoneNumber;
	}
	/**
	 * @return the areaCode
	 */
	public int getAreaCode() {
		return areaCode;
	}
	/*
	 * @param timeZone the timeZone to set
	 */
	private void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}
	/**
	 * @param areaCode the areaCode to set
	 */
	private void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}
	/**
	 * @return the isdCode
	 */
	public int getIsdCode() {
		return isdCode;
	}
	/**
	 * @param isdCode the isdCode to set
	 */
	private void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}
	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}
	/**
	 * @param provider the provider to set
	 */
	private void setProvider(String provider) {
		this.provider = provider;
	}


	/**
	 * @return the partnerId
	 */
	public String getPartnerId() {
		return partnerId;
	}


	/**
	 * @param partnerId the partnerId to set
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}


	/**
	 * @return the bucketName
	 */
	public String getBucketName() {
		return bucketName;
	}


	/**
	 * @param bucketName the bucketName to set
	 */
	private void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}


	/**
	 * @return the voiceMessage
	 */
	public String getVoiceMessage() {
		return voiceMessage;
	}


	/**
	 * @param voiceMessage the voiceMessage to set
	 */
	public void setVoiceMessage(String voiceMessage) {
		this.voiceMessage = voiceMessage;
	}


	/**
	 * @return the isRedialNumber
	 */
	public boolean isRedialNumber() {
		return isRedialNumber;
	}


	/**
	 * @param isRedialNumber the isRedialNumber to set
	 */
	public void setRedialNumber(boolean isRedialNumber) {
		this.isRedialNumber = isRedialNumber;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}
	
	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}
	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
