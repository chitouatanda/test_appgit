package com.xtaas.domain.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.AttributeMapping;

@Document(collection="entitymapping")
public class EntityMapping extends AbstractEntity {
	private String fromSystem;
	private String toSystem;
	private String from;
	private String to;
	private List<AttributeMapping> attributes;

	public String getFromSystem() {
		return fromSystem;
	}

	public String getToSystem() {
		return toSystem;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public List<AttributeMapping> getAttributes() {
		return attributes;
	}

}
