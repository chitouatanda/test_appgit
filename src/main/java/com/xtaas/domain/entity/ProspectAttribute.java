package com.xtaas.domain.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.valueobjects.KeyValuePair;

@Document(collection="prospectattribute")
public class ProspectAttribute extends AbstractEntity {
	private String label;
	private String dataType;
	private List<KeyValuePair<String, String>> data;

	public String getLabel() {
		return label;
	}

	public String getDataType() {
		return dataType;
	}

	public List<KeyValuePair<String, String>> getData() {
		return data;
	}

}
