package com.xtaas.domain.entity;

import java.util.HashMap;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * A collection which stores all the recorded incoming messages
 * 
 */
@Document(collection = "voicemail")
public class VoiceMail extends AbstractEntity {

	public enum VoiceMessageStatus {
		NEW, READ, DELETED;
	}

	private String fromNumber;
	private String toNumber;
	private String callSid;
	private String messageAudioUrl;
	private String awsAudioUrl;
	private VoiceMessageStatus messageStatus;
	private String partnerId;
	private String provider;
	private HashMap<String, String> callParams; // key value pair for all the request parameters sent by Twilio/Plivo
	private Boolean isTranscribed;
	private String speech;
	
	public VoiceMail(String fromNumber, String toNumber, String callSid, String messageAudioUrl, String partnerId,
			String provider) {
		setFromNumber(fromNumber);
		setToNumber(toNumber);
		setCallSid(callSid);
		setMessageAudioUrl(messageAudioUrl);
		setPartnerId(partnerId);
		setProvider(provider);
		setIsTranscribed(false);
	}

	public String getFromNumber() {
		return fromNumber;
	}

	private void setFromNumber(String fromNumber) {
		if (fromNumber == null || fromNumber.isEmpty()) {
			throw new IllegalArgumentException("Caller number is required");
		}
		this.fromNumber = fromNumber;
	}

	public String getToNumber() {
		return toNumber;
	}

	private void setToNumber(String toNumber) {
		if (toNumber == null || toNumber.isEmpty()) {
			throw new IllegalArgumentException("Called number is required");
		}
		this.toNumber = toNumber;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		if (callSid == null || callSid.isEmpty()) {
			throw new IllegalArgumentException("CallSid is required");
		}
		this.callSid = callSid;
	}

	public String getMessageAudioUrl() {
		return messageAudioUrl;
	}

	private void setMessageAudioUrl(String messageAudioUrl) {
		if (messageAudioUrl == null || messageAudioUrl.isEmpty()) {
			throw new IllegalArgumentException("Message audio url is required");
		}

		String[] schemes = { "http", "https" };
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (!urlValidator.isValid(messageAudioUrl)) {
			throw new IllegalArgumentException("Message Audio Url is not a valid url. Url = " + messageAudioUrl);
		}
		this.messageAudioUrl = messageAudioUrl;
	}

	public VoiceMessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(VoiceMessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public HashMap<String, String> getCallParams() {
		return callParams;
	}

	public void setCallParams(HashMap<String, String> callParams) {
		this.callParams = callParams;
	}

	public Boolean getIsTranscribed() {
		return isTranscribed;
	}

	public void setIsTranscribed(Boolean isTranscribed) {
		this.isTranscribed = isTranscribed;
	}

	public String getSpeech() {
		return speech;
	}

	public void setSpeech(String speech) {
		this.speech = speech;
	}

	public String getAwsAudioUrl() {
		return awsAudioUrl;
	}

	public void setAwsAudioUrl(String awsAudioUrl) {
		this.awsAudioUrl = awsAudioUrl;
	}

}