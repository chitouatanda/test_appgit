package com.xtaas.domain.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentConference")
public class AgentConference extends AbstractEntity {

	private String agentId;
	private String conferenceId;
	private String callControlId;
	private String callLegId;

	public AgentConference(String agentId, String conferenceId, String callControlId, String callLegId) {
		setAgentId(agentId);
		setConferenceId(conferenceId);
		setCallControlId(callControlId);
		setCallLegId(callLegId);
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getConferenceId() {
		return conferenceId;
	}

	public void setConferenceId(String conferenceId) {
		this.conferenceId = conferenceId;
	}

	public String getCallControlId() {
		return callControlId;
	}

	public void setCallControlId(String callControlId) {
		this.callControlId = callControlId;
	}

	public String getCallLegId() {
		return callLegId;
	}

	public void setCallLegId(String callLegId) {
		this.callLegId = callLegId;
	}

}