package com.xtaas.domain.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.CompletedCampaign;
import com.xtaas.domain.valueobject.EntityHelper;
import com.xtaas.domain.valueobject.TeamResource;
import com.xtaas.domain.valueobject.TeamStatus;
import com.xtaas.domain.valueobject.WorkSchedule;

@Document(collection="team")
public class Team extends AbstractEntity {
	private String partnerId;
	private String name;
	private String overview;
	private double perLeadPrice;
	private TeamStatus status;
	private Address address;
	private List<String> adminSupervisorIds;
	private TeamResource supervisor;
	private List<TeamResource> agents;
	private int teamSize;
	private List<TeamResource> qas;
	private List<CompletedCampaign> completedCampaigns;
	private int totalCompletedCampaigns;
	private double averageRating;
	private int totalRatings;
	private int totalVolume;
	private double averageConversion;
	private double averageQuality;
	private double averageAvailableHours;
	
	public Team(String partnerId, TeamResource supervisor, String name, Address address) {
		setPartnerId(partnerId);
		setSupervisor(supervisor);
		setName(name);
		setAddress(address);
		agents = new ArrayList<TeamResource>();
		qas = new ArrayList<TeamResource>();
		completedCampaigns = new ArrayList<CompletedCampaign>();
		setStatus(TeamStatus.ACTIVE);
	}

	private void setPartnerId(String partnerId) {
		if (partnerId == null || partnerId.isEmpty()) {
			throw new IllegalArgumentException("Partner id of team is required");
		}
		this.partnerId = partnerId;
	}

	public void setPerLeadPrice(double perLeadPrice) {
		this.perLeadPrice = perLeadPrice;
	}

	public List<String> getAdminSupervisorIds() {
		return adminSupervisorIds;
	}

	public void setAdminSupervisorIds(List<String> adminSupervisorIds) {
		this.adminSupervisorIds = adminSupervisorIds;
	}

	public void setSupervisor(TeamResource supervisor) {
		if (supervisor == null) {
			throw new IllegalArgumentException("Supervisor of team is required");
		}
		this.supervisor = supervisor;
	}

	private void setName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Name of team is required");
		}
		this.name = name;
	}

	public void setAddress(Address address) {
		if (address == null) {
			throw new IllegalArgumentException("Address of team is required");
		}
		this.address = address;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setStatus(TeamStatus status) {
		this.status = status;
	}

	public void addCompletedCampaign(CompletedCampaign completedCampaign) {
		EntityHelper.addItem(completedCampaigns, completedCampaign);
		totalCompletedCampaigns = completedCampaigns.size();
		if (completedCampaign.getRating() != 0) {
			totalRatings++;
		}
	}

	public void removeCompletedCampaign(String campaignId) {
		if (campaignId == null || campaignId.isEmpty()) {
			throw new IllegalArgumentException("Campaign id of campaign to be removed is required");
		}
		int index = completedCampaigns.size();
		for (; index > 0; index--) {
			if (completedCampaigns.get(index - 1).getCampaignId().equals(campaignId)) {
				break;
			}
		}
		if (index != 0) {
			if (completedCampaigns.get(index - 1).getRating() != 0) {
				totalRatings--;
			}
			completedCampaigns.remove(index - 1);
		} else {
			throw new IllegalArgumentException("Campaign is not part of the team");
		}
		totalCompletedCampaigns = completedCampaigns.size();
	}

	public void addAgent(TeamResource agent) {
		EntityHelper.addItem(agents, agent);
		teamSize = agents.size();
	}
	
	public void temporaryAllocateCampaign(String agentId, String campaignId) {
		TeamResource agent = getTeamResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.setSupervisorOverrideCampaignId(campaignId);
		}
	}

	// TODO:: This is a possible copy paste from previous method. Refactoring should be possible
	public void undoTemporaryAllocation(String agentId) {
		TeamResource agent = getTeamResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.setSupervisorOverrideCampaignId(null);
		}
	}
	
	public void removeAgent(String agentId) {
		RemoveResource(agentId, agents);
		teamSize = agents.size();
	}
	
	public void addWorkSchedule(String agentId, WorkSchedule workSchedule) {
		TeamResource agent = getTeamResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.addWorkSchedule(workSchedule);
		}
	}

	public void removeWorkSchedule(String agentId, String scheduleId) {
		TeamResource agent = getTeamResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.removeWorkSchedule(scheduleId);
		}
	}

	private TeamResource getTeamResource(String agentId) {
		int index = agents.size();
		for (; index > 0; index--) {
			if (agents.get(index - 1).getResourceId().equals(agentId)) {
				return agents.get(index - 1);
			}
		}
		return null;
	}

	public void addQA(TeamResource qa) {
		EntityHelper.addItem(qas, qa);
	}

	public void removeQA(String qaId) {
		RemoveResource(qaId, qas);
	}

	private static void RemoveResource(String resourceId, List<TeamResource> resources) {
		if (resourceId == null || resourceId.isEmpty()) {
			throw new IllegalArgumentException("Resource id of team is required");
		}
		int index = resources.size();
		for (; index > 0; index--) {
			if (resources.get(index - 1).getResourceId().equals(resourceId)) {
				break;
			}
		}
		if (index != 0) {
			resources.remove(index - 1);
		} else {
			throw new IllegalArgumentException("Resource is not part of the team");
		}
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getName() {
		return name;
	}

	public String getOverview() {
		return overview;
	}

	public double getPerLeadPrice() {
		return perLeadPrice;
	}

	public TeamStatus getStatus() {
		return status;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public double getAverageAvailableHours() {
		return averageAvailableHours;
	}

	public Address getAddress() {
		return address;
	}

	public TeamResource getSupervisor() {
		return supervisor;
	}

	public List<TeamResource> getAgents() {
		// TODO:: This should return a clonable list so that the agent are not updatable
		return Collections.unmodifiableList(agents);
	}

	public List<TeamResource> getQas() {
		return Collections.unmodifiableList(qas);
	}

	public List<CompletedCampaign> getCompletedCampaigns() {
		return Collections.unmodifiableList(completedCampaigns);
	}

	public int getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}

	public int getTotalVolume() {
		return totalVolume;
	}

	public double getAverageConversion() {
		return averageConversion;
	}

	public double getAverageQuality() {
		return averageQuality;
	}

	public int getTeamSize() {
		return teamSize;
	}

	public int getTotalRatings() {
		return totalRatings;
	}
	
	// TODO:: Remove the below setters as there are supposed to done by batch job
	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public void setTotalVolume(int totalVolume) {
		this.totalVolume = totalVolume;
	}

	public void setAverageConversion(double averageConversion) {
		this.averageConversion = averageConversion;
	}

	public void setAverageQuality(double averageQuality) {
		this.averageQuality = averageQuality;
	}

	public void setAverageAvailableHours(double teamAvgAvailableHrs) {
		this.averageAvailableHours = teamAvgAvailableHrs;
	}
	// TODO:: Remove the above setters as there are supposed to done by batch job
}
