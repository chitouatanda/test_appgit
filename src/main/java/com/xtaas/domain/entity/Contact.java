package com.xtaas.domain.entity;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;
import com.xtaas.domain.valueobject.ContactSicCode;
import com.xtaas.domain.valueobject.ContactSocialMedia;
import com.xtaas.domain.valueobject.ContactStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact extends AbstractEntity {
	private String source;
	private String sourceId;
	private ContactStatus status;
	private String prefix;
	private String firstName;
	private String lastName;
	private String suffix;
	private String title;
	private String department;
	private List<String> hierarchyTitle;
	private List<ContactFunction> functions;
	private String organizationId;
	private String organizationName;
	private String domain;
	private List<ContactIndustry> industries;
	private List<String> topLevelIndustries;
	private double minRevenue;
	private double maxRevenue;
	private double minEmployeeCount;
	private double maxEmployeeCount;
	private String email;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private String directPhone;
	private String officePhone;
	private int usageCount;
	private Map<String, String> extraAttributes;
	private String listPurchaseTransactionId;
	private List<ContactSicCode> sicCodes;
	private String phoneAccuracy;
	private ContactSocialMedia socialMedia;
	private String sourceCompanyId;
	private String managementLevel;
	

	//Contact Constructor for Netprospex
	 public Contact(String source, String sourceId, String prefix, String firstName, String lastName, String suffix,
			String title, List<String> hierarchyTitle, String department, List<ContactFunction> functions, String organizationId,
			String organizationName, String domain, List<ContactIndustry> industries, List<String> topLevelIndustries, double minRevenue, double maxRevenue,
			double minEmployeeCount, double maxEmployeeCount, String email, String addressLine1, String addressLine2,
			String city, String state, String country, String postalCode, String directPhone, String officePhone,
			int usageCount,Map<String, String> extraAttributes, String sourceCompanyId,String managementLevel) { 
		this.setStatus(ContactStatus.ACTIVE);
		this.setSource(source);
		this.setSourceId(sourceId);
		this.setPrefix(prefix);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setSuffix(suffix);
		this.setTitle(title);
		this.setDepartment(department);
		this.setHierarchyTitle(hierarchyTitle);
		this.setFunctions(functions);
		this.setOrganizationId(organizationId);
		this.setOrganizationName(organizationName);
		this.setDomain(domain);
		this.setIndustries(industries);
		this.setTopLevelIndustries(topLevelIndustries);
		this.setMinRevenue(minRevenue);
		this.setMaxRevenue(maxRevenue);
		this.setMinEmployeeCount(minEmployeeCount);
		this.setMaxEmployeeCount(maxEmployeeCount);
		this.setEmail(email);
		this.setAddressLine1(addressLine1);
		this.setAddressLine2(addressLine2);
		this.setCity(city);
		this.setState(state);
		this.setCountry(country);
		this.setPostalCode(postalCode);
		this.setDirectPhone(directPhone);
		this.setOfficePhone(officePhone);
		this.setExtraAttributes(extraAttributes);
		this.extraAttributes = extraAttributes;	
		this.setSourceCompanyId(sourceCompanyId);
		this.setManagementLevel(managementLevel);
	}
	
	public CampaignContact toCampaignContact(String campaignId,boolean isEncrypted) {
		return new CampaignContact(campaignId, source, sourceId, prefix, firstName, lastName, suffix, title, department, functions, organizationName,
				domain, industries, topLevelIndustries, minRevenue, maxRevenue, minEmployeeCount, maxEmployeeCount, email, 
				addressLine1,addressLine2, city, state, country, postalCode, directPhone != null ? directPhone : officePhone, officePhone != null ? officePhone : directPhone, sourceCompanyId, managementLevel, 
						extraAttributes,isEncrypted);
	}
	
	public void useProspect() {
		usageCount++;
	}

	private void setStatus(ContactStatus status) {
		this.status = status;
	}

	private void setSource(String source) {
		this.source = source;
	}

	private void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	private void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	private void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	private void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	private void setHierarchyTitle(List<String> hierarchyTitle) {
		this.hierarchyTitle = hierarchyTitle;
		
	}
	
	public List<ContactFunction> getFunctions() {
		return functions;
	}

	public void setFunctions(List<ContactFunction> functions) {
		this.functions = functions;
	}

	public List<ContactIndustry> getIndustries() {
		return industries;
	}

	public void setIndustries(List<ContactIndustry> industries) {
		this.industries = industries;
	}

	public List<ContactSicCode> getSicCodes() {
		return sicCodes;
	}

	public void setSicCodes(List<ContactSicCode> sicCodes) {
		this.sicCodes = sicCodes;
	}

	public String getPhoneAccuracy() {
		return phoneAccuracy;
	}

	public void setPhoneAccuracy(String phoneAccuracy) {
		this.phoneAccuracy = phoneAccuracy;
	}

	public ContactSocialMedia getSocialMedia() {
		return socialMedia;
	}

	public void setSocialMedia(ContactSocialMedia socialMedia) {
		this.socialMedia = socialMedia;
	}

	private void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	private void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	private void setDomain(String domain) {
		this.domain = domain;
	}

	private void setMinRevenue(double minRevenue) {
		this.minRevenue = minRevenue;
	}

	private void setMaxRevenue(double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	private void setMinEmployeeCount(double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	private void setMaxEmployeeCount(double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}
	

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setEmail(String email) { // TODO:: Hack to setup dummy email
											// addresses instead of purchasing
											// from providers
		this.email = email;
	}

	private void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	private void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	private void setCity(String city) {
		this.city = city;
	}

	private void setState(String state) {
		this.state = state;
	}

	private void setCountry(String country) {
		this.country = country;
	}

	private void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setDirectPhone(String directPhone) { // TODO:: Hack to setup
														// dummy email addresses
														// instead of purchasing
														// from providers
		this.directPhone = directPhone;
	}

	private void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	private void setExtraAttributes(Map<String, String> extraAttributes) {
		this.extraAttributes = extraAttributes;
	}

	public String getSource() {
		return source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public ContactStatus getStatus() {
		return status;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getHierarchyTitle() {
		return hierarchyTitle;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public String getDomain() {
		return domain;
	}

	
	public double getMinRevenue() {
		return minRevenue;
	}

	public double getMaxRevenue() {
		return maxRevenue;
	}

	public double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public String getEmail() {
		return email;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getDirectPhone() {
		return directPhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public int getUsageCount() {
		return usageCount;
	}

	public Map<String, String> getExtraAttributes() {
		return extraAttributes;
	}

	/**
	 * @return the listPurchaseTransactionId
	 */
	public String getListPurchaseTransactionId() {
		return listPurchaseTransactionId;
	}

	/**
	 * @param listPurchaseTransactionId the listPurchaseTransactionId to set
	 */
	public void setListPurchaseTransactionId(String listPurchaseTransactionId) {
		this.listPurchaseTransactionId = listPurchaseTransactionId;
	}

    public String getSourceCompanyId() {
        return sourceCompanyId;
    }

    public void setSourceCompanyId(String sourceCompanyId) {
        this.sourceCompanyId = sourceCompanyId;
    }

    public String getManagementLevel() {
        return managementLevel;
    }

    public void setManagementLevel(String managementLevel) {
        this.managementLevel = managementLevel;
    }
    public List<String> getTopLevelIndustries() {
		return topLevelIndustries;
	}

	public void setTopLevelIndustries(List<String> topLevelIndustries) {
		this.topLevelIndustries = topLevelIndustries;
	}
}
