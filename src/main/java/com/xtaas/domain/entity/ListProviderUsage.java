package com.xtaas.domain.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.ContactPurchaseError;
import com.xtaas.domain.valueobject.PurchaseStatus;
import com.xtaas.domain.valueobject.PurchaseTransaction;
import com.xtaas.domain.valueobject.TransactionStatus;

@Document(collection = "listproviderusage")
public class ListProviderUsage extends AbstractEntity {
	private String providerName;
	private String supervisorId;
	private String campaignId;
	private int recordPurchaseRequestedByUser;
	private int uniqueRecordsPurchased;
	private int recordPurchaseOutstanding;
	private PurchaseStatus purchaseStatus;
	private String searchQueryToProvider;
	private String searchExpressionByUser;
	private int pageOffset;
	private int pageSize;
	private String listPurchaseTransactionId;
	private Map<String, PurchaseTransaction> purchaseTransactions;
	private TransactionStatus transactionStatus;
	private List<ContactPurchaseError> purchaseErrors;
	
	/**
	 * @param providerName
	 * @param supervisorId
	 * @param campaignId
	 * @param recordPurchasedRequestedByUser
	 * @param uniqueRecordsPurchased
	 * @param purchaseStatus
	 * @param searchQueryToProvider
	 * @param searchExpressionByUser
	 * @param pageOffset
	 * @param pageSize
	 * @param listPurchasedTransactionId
	 */
	public ListProviderUsage(String providerName, String supervisorId,
			String campaignId, int recordPurchaseRequestedByUser,
			int uniqueRecordsPurchased, int recordPurchaseOutstanding, PurchaseStatus purchaseStatus, String searchQueryToProvider,
			String searchExpressionByUser, int pageOffset, int pageSize,
			String listPurchaseTransactionId) {
		this.setProviderName(providerName);
		this.setCampaignId(campaignId);
		this.setSupervisorId(supervisorId);
		this.setRecordPurchasedRequestedByUser(recordPurchaseRequestedByUser);
		this.setUniqueRecordsPurchased(uniqueRecordsPurchased);
		this.setRecordPurchaseOutstanding(recordPurchaseOutstanding);
		this.setPurchaseStatus(purchaseStatus);
		this.setSearchQueryToProvider(searchQueryToProvider);
		this.setSearchExpressionByUser(searchExpressionByUser);
		this.setPageOffset(pageOffset);
		this.setPageSize(pageSize);
		this.setListPurchasedTransactionId(listPurchaseTransactionId);
	}

	/**
	 * @return the recordPurchaseOutstanding
	 */
	public int getRecordPurchaseOutstanding() {
		return recordPurchaseOutstanding;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	


	/**
	 * @return the errors
	 */
	public List<ContactPurchaseError> getErrors() {
		return purchaseErrors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(List<ContactPurchaseError> purchaseErrors) {
		this.purchaseErrors = purchaseErrors;
	}

	/**
	 * @param recordPurchaseOutstanding the recordPurchaseOutstanding to set
	 */
	public void setRecordPurchaseOutstanding(int recordPurchaseOutstanding) {
		this.recordPurchaseOutstanding = recordPurchaseOutstanding;
	}

	/**
	 * @return the purchaseStatus
	 */
	public PurchaseStatus getPurchaseStatus() {
		return purchaseStatus;
	}

	/**
	 * @param purchaseStatus the purchaseStatus to set
	 */
	public void setPurchaseStatus(PurchaseStatus purchaseStatus) {
		this.purchaseStatus = purchaseStatus;
	}

	private void setProviderName(String providerName) {
		if (providerName == null || providerName.isEmpty()) {
			throw new IllegalArgumentException("Provider name of List provider usage is required");
		}
		this.providerName = providerName;
	}

	private void setSupervisorId(String supervisorId) {
		if (supervisorId == null || supervisorId.isEmpty()) {
			throw new IllegalArgumentException("Supervisor name of List provider usage is required");
		}
		this.supervisorId = supervisorId;
	}

	private void setCampaignId(String campaignId) {
		if (campaignId == null || campaignId.isEmpty()) {
			throw new IllegalArgumentException("Campaign id of List provider usage is required");
		}
		this.campaignId = campaignId;
	}

	private void setSearchExpressionByUser(String searchExpressionByUser) {
		if (!providerName.equals("NONE") && (searchExpressionByUser == null || searchExpressionByUser.isEmpty())) {
			throw new IllegalArgumentException("Search expression of List provider usage is required");
		}
		this.searchExpressionByUser = searchExpressionByUser;
	}

	public String getProviderName() {
		return providerName;
	}

	public String getSupervisorName() {
		return supervisorId;
	}

	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @return the recordPurchasedRequestedByUser
	 */
	public int getRecordPurchaseRequestedByUser() {
		return recordPurchaseRequestedByUser;
	}

	/**
	 * @param recordPurchasedRequestedByUser the recordPurchasedRequestedByUser to set
	 */
	public void setRecordPurchasedRequestedByUser(int recordPurchaseRequestedByUser) {
		this.recordPurchaseRequestedByUser = recordPurchaseRequestedByUser;
	}

	/**
	 * @return the uniqueRecordsPurchased
	 */
	public int getUniqueRecordsPurchased() {
		return uniqueRecordsPurchased;
	}

	/**
	 * @param uniqueRecordsPurchased the uniqueRecordsPurchased to set
	 */
	public void setUniqueRecordsPurchased(int uniqueRecordsPurchased) {
		this.uniqueRecordsPurchased = uniqueRecordsPurchased;
	}

	/**
	 * @return the searchQueryToProvider
	 */
	public String getSearchQueryToProvider() {
		return searchQueryToProvider;
	}

	/**
	 * @param searchQueryToProvider the searchQueryToProvider to set
	 */
	public void setSearchQueryToProvider(String searchQueryToProvider) {
		this.searchQueryToProvider = searchQueryToProvider;
	}

	/**
	 * @return the supervisorId
	 */
	public String getSupervisorId() {
		return supervisorId;
	}

	/**
	 * @return the pageOffset
	 */
	public int getPageOffset() {
		return pageOffset;
	}

	/**
	 * @param pageOffset the pageOffset to set
	 */
	public void setPageOffset(int pageOffset) {
		this.pageOffset = pageOffset;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the listPurchasedTransactionId
	 */
	public String getListPurchasedTransactionId() {
		return listPurchaseTransactionId;
	}

	/**
	 * @param listPurchasedTransactionId the listPurchasedTransactionId to set
	 */
	public void setListPurchasedTransactionId(String listPurchasedTransactionId) {
		this.listPurchaseTransactionId = listPurchasedTransactionId;
	}

	/**
	 * @return the purchaseSummary
	 */
	public Map<String, PurchaseTransaction> getPurchaseTransactions() {
		return purchaseTransactions;
	}

	/**
	 * @param purchaseSummary the purchaseSummary to set
	 */
	public void setPurchaseTransactions(Map<String, PurchaseTransaction> purchaseTransactions) {
		this.purchaseTransactions = purchaseTransactions;
	}

	/**
	 * @return the searchExpressionByUser
	 */
	public String getSearchExpressionByUser() {
		return searchExpressionByUser;
	}
	
	public void addPurchaseTransaction(PurchaseTransaction purchaseTransaction) {
		purchaseTransactions.put(purchaseTransaction.getProviderName(), purchaseTransaction);
	}
	
	public PurchaseTransaction getPurchaseTransaction(String providerName) {
		return purchaseTransactions.get(providerName);
	}
}
