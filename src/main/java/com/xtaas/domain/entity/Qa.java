package com.xtaas.domain.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xtaas.domain.valueobject.Address;
import com.xtaas.domain.valueobject.CompletedCampaign;
import com.xtaas.domain.valueobject.EntityHelper;
import com.xtaas.domain.valueobject.Status;
import com.xtaas.domain.valueobject.WorkSchedule;

@Document(collection="qa")
public class Qa extends AbstractEntity{
	private String partnerId;
	private String firstName;
	private String lastName;
	private String overview;
	private Status status;
	private Address address;
	private List<String> languages;
	private List<String> domains;
	private double perHourPrice;
	private List<CompletedCampaign> completedCampaigns;
	private int totalCompletedCampaigns;
	private double averageRating;
	private int totalRatings;
	private int totalVolume;
	private double averageConversion;
	private double averageQuality;
	private double averageAvailableHours;
	private List<WorkSchedule> workSchedules;
	
	public Qa(String partnerId, String firstName, String lastName, Address address) {
		setPartnerId(partnerId);
		setFirstName(firstName);
		setLastName(lastName);
		setAddress(address);
		setStatus(Status.ACTIVE);
		languages = new ArrayList<String>();
		domains = new ArrayList<String>();
		completedCampaigns = new ArrayList<CompletedCampaign>();
		workSchedules = new ArrayList<WorkSchedule>();
	}
	
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	
	
	public String getUsername() {
		return getId();
	}

	public void setUsername(String username) {
		if (username == null || username.isEmpty()) {
			throw new IllegalArgumentException("Username is required");
		}
		setId(username.toLowerCase());
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		if (firstName == null || firstName.isEmpty()) {
			throw new IllegalArgumentException("First Name is required");
		}
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		if (lastName == null || lastName.isEmpty()) {
			throw new IllegalArgumentException("Last Name is required");
		}
		this.lastName = lastName;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}
	
	public void setPerHourPrice(double perHourPrice) {
		this.perHourPrice = perHourPrice;
	}

	public void addCompletedCampaign(CompletedCampaign completedCampaign) {
		EntityHelper.addItem(completedCampaigns, completedCampaign);
		totalCompletedCampaigns = completedCampaigns.size();
		if (completedCampaign.getRating() != 0) {
			totalRatings++;
		}
	}

	public void removeCompletedCampaign(String campaignId) {
		if (campaignId == null || campaignId.isEmpty()) {
			throw new IllegalArgumentException("Campaign id of campaign to be removed is required");
		}
		int index = completedCampaigns.size();
		for (; index > 0; index--) {
			if (completedCampaigns.get(index - 1).getCampaignId().equals(campaignId)) {
				break;
			}
		}
		if (index != 0) {
			if (completedCampaigns.get(index - 1).getRating() != 0) {
				totalRatings--;
			}
			completedCampaigns.remove(index - 1);
		} else {
			throw new IllegalArgumentException("Campaign is not part of the team");
		}
		totalCompletedCampaigns = completedCampaigns.size();
	}
	
	public void addWorkSchedule(WorkSchedule workSchedule) {
		EntityHelper.addItem(workSchedules, workSchedule);
	}

	public void removeWorkSchedule(String scheduleId) {
		if (scheduleId == null || scheduleId.isEmpty()) {
			throw new IllegalArgumentException("Schedule id of schedule to be removed is required");
		}
		int index = workSchedules.size();
		for (; index > 0; index--) {
			if (workSchedules.get(index - 1).getScheduleId().equals(scheduleId)) {
				break;
			}
		}
		if (index != 0) {
			workSchedules.remove(index - 1);
		} else {
			throw new IllegalArgumentException("Work schedule is not available");
		}
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}

	public void setAddress(Address address) {
		if (address == null) {
			throw new IllegalArgumentException("Address of qa is required");
		}
		this.address = address;
	}
	
	public void addLanguage(String language) {
		EntityHelper.addItem(languages, language);
	}

	public void addDomain(String domain) {
		EntityHelper.addItem(domains, domain);
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getName() {
		String name = this.firstName;
    	if (this.lastName != null) {
    		name = name + " " + this.lastName;
    	}
    	return name;
	}

	public String getOverview() {
		return overview;
	}
	
	public double getAverageRating() {
		return averageRating;
	}

	public List<CompletedCampaign> getCompletedCampaigns() {
		return completedCampaigns;
	}

	public int getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}

	public int getTotalRatings() {
		return totalRatings;
	}

	public Status getStatus() {
		return status;
	}

	public Address getAddress() {
		return address;
	}

	public List<String> getLanguages() {
		return Collections.unmodifiableList(languages);
	}

	public List<String> getDomains() {
		return Collections.unmodifiableList(domains);
	}

	public double getPerHourPrice() {
		return perHourPrice;
	}

	public double getAverageAvailableHours() {
		return averageAvailableHours;
	}

	public int getTotalVolume() {
		return totalVolume;
	}

	public double getAverageConversion() {
		return averageConversion;
	}

	public double getAverageQuality() {
		return averageQuality;
	}
	
	public List<WorkSchedule> getWorkSchedules() {
		return Collections.unmodifiableList(workSchedules);
	}

	public boolean isIndependent() {
		return (partnerId == null);
	}
	// TODO:: Remove the below setters as there are supposed to done by batch job
	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public void setTotalVolume(int totalVolume) {
		this.totalVolume = totalVolume;
	}

	public void setAverageConversion(double averageConversion) {
		this.averageConversion = averageConversion;
	}

	public void setAverageQuality(double averageQuality) {
		this.averageQuality = averageQuality;
	}

	public void setAverageAvailableHours(double averageAvailableHours) {
		this.averageAvailableHours = averageAvailableHours;
	}
	// TODO:: Remove the above setters as there are supposed to done by batch job
}
