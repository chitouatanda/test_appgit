package com.xtaas.domain.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.Asset;
import com.xtaas.domain.valueobject.CallGuide;
import com.xtaas.domain.valueobject.CallSpeed;
import com.xtaas.domain.valueobject.CampaignAgentInvitation;
import com.xtaas.domain.valueobject.CampaignStatus;
import com.xtaas.domain.valueobject.CampaignTeam;
import com.xtaas.domain.valueobject.CampaignTeamDTO;
import com.xtaas.domain.valueobject.CampaignTeamInvitation;
import com.xtaas.domain.valueobject.CampaignTeamShortlisted;
import com.xtaas.domain.valueobject.CampaignTypes;
import com.xtaas.domain.valueobject.DialerMode;
import com.xtaas.domain.valueobject.EmailMessage;
import com.xtaas.domain.valueobject.Expression;
import com.xtaas.domain.valueobject.ExpressionGroup;
import com.xtaas.domain.valueobject.LeadSortOrder;
import com.xtaas.domain.valueobject.QualificationCriteria;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.valueobjects.KeyValuePair;
import com.xtaas.web.dto.TranscribeCriteriaDTO;

public class Campaign extends AbstractEntity {
	private String sfdcCampaignId;
	private String organizationId; // linked to the organization/agency for
	private String campaignManagerId; // linked to user who owns this campaign
	private CampaignStatus status;
	private String name;
	private boolean cacheRefresh;
	private Date startDate; // as ms from epoch
	private Date endDate; // as ms from epoch
	private CampaignTypes type; // Lead Verification, Lead Generation,...
	private int deliveryTarget;
	private String domain;
	private int dailyCap;
	private QualificationCriteria qualificationCriteria;
	private String qualificationClause;
	private List<String> restrictions;
	private Set<String> informationCriteria;
	private ExpressionGroup leadFilteringCriteria;
	private String leadFilteringClause;
	private List<Asset> assets;
	private List<CampaignTeamInvitation> teamInvitations;
	private List<CampaignTeamShortlisted> teamsShortlisted;
	private List<CampaignAgentInvitation> agentInvitations;
	private boolean selfManageTeam; // Whether the team is managed self/by
	// partner
	private CampaignTeam team;
	private String currentAssetId;
	private List<String> multiCurrentAssetId;
	private EmailMessage assetEmailTemplate;
	private DialerMode dialerMode;
	private String brand;
	private String timezone;
	private boolean enableMachineAnsweredDetection;
	private boolean disableAMDOnQueued;
	private boolean useSlice;
	private boolean useDirectClassificationML;
	private boolean multiProspectCalling;
	private String campaignRequirements;
	private boolean hidePhone;	// Added to hide phone number on agent screen from third party center
	private boolean hideEmail;	// Added to hide email number on agent screen from third party center
	private List<String> partnerSupervisors;	// Added to give show/hide access to supervisor screen for partner supervisor
	private List<String> adminSupervisorIds;
	private boolean qaRequired;	//Added to check if lead goes to qa or not
	//private boolean callSpeedRetryPerMinPerAgent;	// added to enable call speed changes to be configured in retry count >0
	private boolean simpleDisposition;
	private String hostingServer;
	private Map<String, Float> retrySpeed;
	private boolean retrySpeedEnabled;
	private float retrySpeedFactor;
	private int dailyCallMaxRetries;
	private int callMaxRetries;
	private String clientName;
	private boolean localOutboundCalling;
	private int duplicateLeadsDuration;
	private int duplicateCompanyDuration;
	private int leadsPerCompany;
	private List<String> campaignGroupIds;
	private int costPerLead;
	private boolean isIndirectBuyStarted;
	private boolean isEncrypted;
	private boolean isANDTitleIgnored;
	private String clientCampaignName;
	private String deliverAssetOnQA;
	private boolean coldEmails;
	private String coldEmailSubject;
	private String coldEmailMessage;
	private boolean sendMailToAllCallables;
	private boolean isEmailSuppressed;
	private boolean isABM;
	private String callControlProvider;
	private boolean isNotConference;
	private boolean supervisorAgentStatusApprove;
	private int emailSuppressionCount;
	private int domainSuppressionCount;
	private String recordProspect;
	private boolean isLeadIQ;
	private boolean networkCheck;
	private boolean bandwidthCheck;
	private boolean telnyxHold;
	private String holdAudioUrl;
	private boolean ringingIcon;
	private boolean unHideSocialLink;
	private boolean mdFiveSuppressionCheck;
	private String callableEvent;
	private int telnyxCallTimeLimitSeconds;
	private boolean autoMachineHangup;
	private boolean manaulBroadSearch;//This flag to search insideview if true will fetch by last name, else by both first and last name.

	private List<KeyValuePair<String, Integer>> dataSourcePriority;
	private List<KeyValuePair<String, Integer>> manualDataSourcePriority;
	private List<KeyValuePair<String, Integer>> similarDataSourcePriority;
	private Map<String, Integer> sourceCallingPriority;
	private boolean insideViewDataBuy;
	private long plivoAMDTime;
	private boolean ignorePrimaryIndustries;
	private boolean primaryIndustriesOnly;
	private int numberOfTopRecords;
	private boolean callAbandon;
	private int prospectCacheAgentRange;
	private int prospectCacheNoOfProspectsPerAgent;
	private boolean enableProspectCaching;
	private boolean removeMobilePhones;
	private String autoModeCallProvider;
	private String manualModeCallProvider;
	private boolean revealEmail;
	private boolean hideNonSuccessPII;
	private boolean enableCustomFields;
	private boolean reviewZoomData;

	private boolean autoGoldenPass;
	private int upperThreshold;
	private int lowerThreshold;
	private boolean similarProspect;
	private boolean tagSuccess;
	private boolean autoIVR;
	private boolean enableInternetCheck;
	private boolean enableStateCodeDropdown;
	private boolean enableCallbackFeature;
	private boolean isMaxEmployeeAllow;
	private boolean useInsideviewSmartCache;
	private boolean ukPhoneDncCheck;
	private boolean usPhoneDncCheck;
	private boolean callableVanished;
	private boolean useLeadIQSmartCache;
	private TranscribeCriteriaDTO transcribeCriteria;
	private boolean transcribeFilter;
	private String sipProvider;
	private boolean autoCallbackStatusMapCleanup;

	public Campaign() {}

	public Campaign(String organizationId, String campaignManagerId, String name, Date startDate, Date endDate,
			CampaignTypes type, QualificationCriteria qualificationCriteria, int deliveryTarget, String domain,
			boolean selfManageTeam) {
		//this.clientName = clientName;
		this.setOrganizationId(organizationId);
		this.setCampaignManagerId(campaignManagerId);
		this.setName(name);
		this.setStatus(CampaignStatus.CREATED);
		this.setRunDates(startDate, endDate);
		this.setType(type);
		this.setQualificationCriteria(qualificationCriteria);
		this.setDeliveryTarget(deliveryTarget);
		this.setDomain(domain);
		//this.setRecordProspect("yes");
		this.setSelfManageTeam(selfManageTeam);
		if (selfManageTeam) {
			team = new CampaignTeam("self", campaignManagerId, 0);
			agentInvitations = new ArrayList<CampaignAgentInvitation>();
		} else {
			teamInvitations = new ArrayList<CampaignTeamInvitation>();
			teamsShortlisted = new ArrayList<CampaignTeamShortlisted>();
		}
		this.setAdminSupervisorIds(new ArrayList<String>());
	}

	/**
	 * Used to clone the campaign. When you add any new field in the Campaign then
	 * set it here as well
	 * 
	 * @param campaign
	 * @return
	 */
	public Campaign getCampaignClone(Campaign campaign) {
		if (!StringUtils.isEmpty(campaign.getSfdcCampaignId())) {
			this.setSfdcCampaignId(campaign.getSfdcCampaignId());
		}
		this.setOrganizationId(campaign.getOrganizationId());
		this.setCampaignManagerId(campaign.getCampaignManagerId());
		this.setStatus(CampaignStatus.CREATED);
		this.setName("Clone of " + campaign.getName());
		this.setCacheRefresh(campaign.getCacheRefresh());
//		this.setStartDate(campaign.getStartDate());
//		this.setEndDate(campaign.getEndDate());
		this.setType(campaign.getType());
		this.setDeliveryTarget(campaign.getDeliveryTarget());
		this.setDomain(campaign.getDomain());
		this.setDailyCap(campaign.getDailyCap());
		this.setQualificationCriteria(campaign.getQualificationCriteria());
		this.setQualificationClause(campaign.getQualificationClause());
		this.setRestrictions(campaign.getRestrictions());
		this.setInformationCriteria(campaign.getInformationCriteria());
		this.setLeadFilteringCriteria(
				campaign.getLeadFilteringCriteria() == null ? null : campaign.getLeadFilteringCriteria());
		this.setLeadFilteringClause(campaign.getLeadFilteringClause());
		this.setAssets(campaign.getAssets());
//		this.setTeamInvitations(campaign.getTeamInvitations());
//		this.setTeamsShortlisted(campaign.getTeamsShortlisted());
//		this.setAgentInvitations(campaign.getAgentInvitations());
//		this.setSelfManageTeam(campaign.isSelfManageTeam());
//		this.setTeam(campaign.getTeam());
		this.setCurrentAssetId(campaign.getCurrentAssetId());
		this.setMultiCurrentAssetId(campaign.getMultiCurrentAssetId());
		this.setAssetEmailTemplate(campaign.getAssetEmailTemplate());
		this.setDialerMode(campaign.getDialerMode());
		this.setBrand(campaign.getBrand());
		this.setTimezone(campaign.getTimezone());
		this.setEnableMachineAnsweredDetection(campaign.isEnableMachineAnsweredDetection());
		this.setDisableAMDOnQueued(campaign.isDisableAMDOnQueued());
		this.setUseSlice(campaign.isUseSlice());
		this.setUseDirectClassificationML(campaign.isUseDirectClassificationML());
		this.setMultiProspectCalling(campaign.isMultiProspectCalling());
		this.setCampaignRequirements(campaign.getCampaignRequirements());
		this.setHidePhone(campaign.isHidePhone());
		this.setHideEmail(campaign.isHideEmail());
		this.setPartnerSupervisors(campaign.getPartnerSupervisors());
//		this.setAdminSupervisorIds(campaign.getAdminSupervisorIds());
		this.setQaRequired(campaign.getQaRequired());
		this.setSimpleDisposition(campaign.isSimpleDisposition());
		this.setHostingServer(campaign.getHostingServer());
		this.setRetrySpeed(campaign.getRetrySpeed());
		this.setRetrySpeedEnabled(campaign.isRetrySpeedEnabled());
		this.setRetrySpeedFactor(campaign.getRetrySpeedFactor());
		this.setDailyCallMaxRetries(campaign.getDailyCallMaxRetries());
		this.setCallMaxRetries(campaign.getCallMaxRetries());
		this.setClientName(campaign.getClientName());
		this.setLocalOutboundCalling(campaign.isLocalOutboundCalling());
		this.setDuplicateLeadsDuration(campaign.getDuplicateLeadsDuration());
		this.setDuplicateCompanyDuration(campaign.getDuplicateCompanyDuration());
		this.setLeadsPerCompany(campaign.getLeadsPerCompany());
		this.setCampaignGroupIds(campaign.getCampaignGroupIds());
		this.setCostPerLead(campaign.getCostPerLead());
		this.setIndirectBuyStarted(campaign.isIndirectBuyStarted());
		this.setIsEncrypted(campaign.isEncrypted());
		this.setANDTitleIgnored(campaign.isANDTitleIgnored());
		this.setClientCampaignName(campaign.getClientCampaignName());
		this.setDeliverAssetOnQA(campaign.getDeliverAssetOnQA());
		this.setColdEmails(campaign.isColdEmails());
		this.setColdEmailSubject(campaign.getColdEmailSubject());
		this.setColdEmailMessage(campaign.getColdEmailMessage());
		this.setSendMailToAllCallables(campaign.isSendMailToAllCallables());
		this.setEmailSuppressed(campaign.isEmailSuppressed());
		this.setABM(campaign.isABM());
		this.setCallControlProvider(campaign.getCallControlProvider());
		this.setNotConference(campaign.isNotConference());
		this.setSupervisorAgentStatusApprove(campaign.isSupervisorAgentStatusApprove());
		this.setEmailSuppressionCount(campaign.getEmailSuppressionCount());
		this.setDomainSuppressionCount(campaign.getDomainSuppressionCount());
		this.setRecordProspect(campaign.getRecordProspect());
		this.setLeadIQ(campaign.isLeadIQ());
		this.setNetworkCheck(campaign.isNetworkCheck());
		this.setBandwidthCheck(campaign.isBandwidthCheck());
		this.setTelnyxHold(campaign.isTelnyxHold());
		this.setHoldAudioUrl(campaign.getHoldAudioUrl());
		this.setRingingIcon(campaign.isRingingIcon());
		this.setUnHideSocialLink(campaign.isUnHideSocialLink());
		this.setMdFiveSuppressionCheck(campaign.isMdFiveSuppressionCheck());
		this.setCallableEvent(campaign.getCallableEvent());
		this.setTelnyxCallTimeLimitSeconds(campaign.getTelnyxCallTimeLimitSeconds());
		this.setAutoMachineHangup(campaign.isAutoMachineHangup());
		this.setManaulBroadSearch(campaign.isManaulBroadSearch());
		this.setDataSourcePriority(campaign.getDataSourcePriority());
		this.setManualDataSourcePriority(campaign.getManualDataSourcePriority());
		this.setSimilarDataSourcePriority(campaign.getSimilarDataSourcePriority());
		this.setSourceCallingPriority(campaign.getSourceCallingPriority());
		this.setInsideViewDataBuy(campaign.isInsideViewDataBuy());
		this.setPlivoAMDTime(campaign.getPlivoAMDTime());
		this.setIgnorePrimaryIndustries(campaign.isIgnorePrimaryIndustries());
		this.setPrimaryIndustriesOnly(campaign.isPrimaryIndustriesOnly());
		this.setNumberOfTopRecords(campaign.getNumberOfTopRecords());
		this.setCallAbandon(campaign.isCallAbandon());
		this.setProspectCacheAgentRange(campaign.getProspectCacheAgentRange());
		this.setProspectCacheNoOfProspectsPerAgent(campaign.getProspectCacheNoOfProspectsPerAgent());
		this.setEnableProspectCaching(campaign.isEnableProspectCaching());
		this.setRemoveMobilePhones(campaign.isRemoveMobilePhones());
		this.setAutoModeCallProvider(campaign.getAutoModeCallProvider());
		this.setManualModeCallProvider(campaign.getManualModeCallProvider());
		this.setRevealEmail(campaign.isRevealEmail());
		this.setHideNonSuccessPII(campaign.isHideNonSuccessPII());
		this.setEnableCustomFields(campaign.isEnableCustomFields());
		this.setReviewZoomData(campaign.isReviewZoomData());
		this.setAutoGoldenPass(campaign.isAutoGoldenPass());
		this.setUpperThreshold(campaign.getUpperThreshold());
		this.setLowerThreshold(campaign.getLowerThreshold());
		this.setSimilarProspect(campaign.isSimilarProspect());
		this.setTagSuccess(campaign.isTagSuccess());
		this.setEnableInternetCheck(campaign.isEnableInternetCheck());
		this.setEnableStateCodeDropdown(campaign.isEnableStateCodeDropdown());
		this.setEnableCallbackFeature(campaign.isEnableCallbackFeature());
		this.setMaxEmployeeAllow(campaign.isMaxEmployeeAllow());
		this.setUseInsideviewSmartCache(campaign.isUseInsideviewSmartCache());
		this.setUkPhoneDncCheck(campaign.isUkPhoneDncCheck());
		this.setUsPhoneDncCheck(campaign.isUsPhoneDncCheck());
		this.setCallableVanished(campaign.isCallableVanished());
		this.setUseLeadIQSmartCache(campaign.isUseLeadIQSmartCache());
		this.setTranscribeCriteria(campaign.getTranscribeCriteria());
		this.setTranscribeFilter(campaign.isTranscribeFilter());
		this.setSipProvider(campaign.getSipProvider());
		return this;
	}

	/**
	 * @return the costPerLead
	 */
	public int getCostPerLead() {
		return costPerLead;
	}

	/**
	 * @param costPerLead the costPerLead to set
	 */
	public void setCostPerLead(int costPerLead) {
		this.costPerLead = costPerLead;
	}

	public boolean isIndirectBuyStarted() {
		return isIndirectBuyStarted;
	}

	public void setIndirectBuyStarted(boolean isIndirectBuyStarted) {
		this.isIndirectBuyStarted = isIndirectBuyStarted;
	}



	public String getDeliverAssetOnQA() {
		return deliverAssetOnQA;
	}

	public void setDeliverAssetOnQA(String deliverAssetOnQA) {
		this.deliverAssetOnQA = deliverAssetOnQA;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setIsEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public boolean isANDTitleIgnored() {
		return isANDTitleIgnored;
	}

	public void setANDTitleIgnored(boolean isANDTitleIgnored) {
		this.isANDTitleIgnored = isANDTitleIgnored;
	}

	public int getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}


	public void setDuplicateLeadsDuration(int duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}


	public int getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}


	public void setDuplicateCompanyDuration(int duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}


	public int getLeadsPerCompany() {
		return leadsPerCompany;
	}


	public void setLeadsPerCompany(int leadsPerCompany) {
		this.leadsPerCompany = leadsPerCompany;
	}



	/**
	 * @param clientName
	 *            the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @param sfdcCampaignId
	 * 			the sfdcCampaignId to set
	 */
	public void setSfdcCampaignId(String sfdcCampaignId) {
		if ((sfdcCampaignId == null || sfdcCampaignId.isEmpty())) {
			throw new IllegalArgumentException("Sfdc Campaign id is required");
		}
		this.sfdcCampaignId = sfdcCampaignId;
	}

	/**
	 * @param organizationId
	 *            the organizationId to set
	 */
	private void setOrganizationId(String organizationId) {
		if ((organizationId == null || organizationId.isEmpty())) {
			throw new IllegalArgumentException("Organization id is required");
		}
		this.organizationId = organizationId;
	}


	/**
	 * @param campaignManagerId
	 *            the campaignManagerId to set
	 */
	private void setCampaignManagerId(String campaignManagerId) {
		if ((campaignManagerId == null || campaignManagerId.isEmpty())) {
			throw new IllegalArgumentException("Campaign manager id is required");
		}
		this.campaignManagerId = campaignManagerId;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Name is required");
		}
		if (!name.equals(this.name)) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Name cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}
		this.name = name;
	}

	public void planCampaign() {
		// remove Bidding
		if (!this.selfManageTeam) {
			throw new IllegalArgumentException(
					"Only self-managed campaigns that are in Bidding state can be Planned");
		}
		if (this.team.getAgents().isEmpty()) {
			throw new IllegalArgumentException(
					"Campaign cannot be changed to Planning as Team is not yet formed");
		}
		setStatus(CampaignStatus.PLANNING);
	}

	public void runCampaign() {
		Date today = new Date();
		//Date:7/12/2017   To run campaign on same day
		today.setHours(12);
		today.setMinutes(00);
		today.setSeconds(00);
		if (this.startDate.after(today) || this.endDate.before(today)) {
			throw new IllegalArgumentException("Campaigns can only be run in the between start and end dates");
		} else {
			setStatus(CampaignStatus.RUNNING);
		}
		// TODO:: The notification system to be intimated about this change
		// TODO:: Make sure at least one agent is allocated before running the
		// campaign

	}

	public void resumeCampaign() {
		Date today = new Date();
		if (this.status != CampaignStatus.PAUSED) {
			throw new IllegalArgumentException("Campaigns that are in paused status only can be resumed");
		} else if (this.endDate.before(today)) {
			throw new IllegalArgumentException("Campaigns can only be resumed before end date");
		} else {
			setStatus(CampaignStatus.RUNNING);
		}
		// TODO:: The notification system to be intimated about this change
	}

	public void pauseCampaign() {
		if (this.status == CampaignStatus.RUNNING) {
			setStatus(CampaignStatus.PAUSED);
		} else {
			throw new IllegalArgumentException("Campaigns that are in running status only can be paused");
		}
		// TODO:: The notification system to be intimated about this change
	}

	public void stopCampaign() {
		// remove Bidding
		if (this.status == CampaignStatus.RUNNING || this.status == CampaignStatus.PAUSED) {
			setStatus(CampaignStatus.STOPPED);
		} else {
			throw new IllegalArgumentException("Only Running and Paused campaigns can be marked as Stopped");
		}
		// TODO:: The notification system to be intimated about this change
	}

	public void completeCampaign() {
		if (this.status == CampaignStatus.RUNNING || this.status == CampaignStatus.PAUSED) {
			setStatus(CampaignStatus.COMPLETED);
		} else {
			throw new IllegalArgumentException("Only Running and Paused campaigns can be marked as Completed");
		}
	}

	public void delistCampaign() {
		if (this.status == CampaignStatus.STOPPED || this.status == CampaignStatus.COMPLETED) {
			setStatus(CampaignStatus.DELISTED);
		} else {
			throw new IllegalArgumentException("Only Stopped and Completed campaigns can be marked as Delisted");
		}
	}

	public void runReadyCampaign() {
		if (this.status == CampaignStatus.PLANNING) {
			setStatus(CampaignStatus.RUNREADY);
		} else {
			throw new IllegalArgumentException("Only campaigns in Planning state can be marked as RunReady");
		}
	}

	public void runningCampaign() {
		Date today = new Date();
		// Date:7/12/2017 To run campaign on same day
		today.setHours(12);
		today.setMinutes(00);
		today.setSeconds(00);
		if (this.status != CampaignStatus.PLANNING) {
			throw new IllegalArgumentException("Only Planning campaigns can be run");
		} else if (this.startDate.after(today) || this.endDate.before(today)) {
			throw new IllegalArgumentException("Campaigns can only be run in the between start and end dates");
		} else {
			setStatus(CampaignStatus.RUNNING);
		}
	}

	public void setStatus(CampaignStatus status) {
		this.status = status;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setQualificationClause(String qualificationClause) {
		this.qualificationClause = qualificationClause;
	}

	public void setLeadFilteringClause(String leadFilteringClause) {
		this.leadFilteringClause = leadFilteringClause;
	}

	public void setTeamInvitations(List<CampaignTeamInvitation> teamInvitations) {
		this.teamInvitations = teamInvitations;
	}

	public void setAgentInvitations(List<CampaignAgentInvitation> agentInvitations) {
		this.agentInvitations = agentInvitations;
	}

	public void setTeam(CampaignTeam team) {
		this.team = team;
	}

	public boolean isMaxEmployeeAllow() {
		return isMaxEmployeeAllow;
	}

	public void setMaxEmployeeAllow(boolean maxEmployeeAllow) {
		isMaxEmployeeAllow = maxEmployeeAllow;
	}


	public void setRunDates(Date startDate, Date endDate) {
		if (startDate == null && this.status != CampaignStatus.CREATED) { //Startdate and Enddate can be NULL when campaign is being CREATED
			throw new IllegalArgumentException("Start date is required");
		}
		if (endDate == null && this.status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException("End date is required");
		}
		if (startDate != null && endDate != null) {
			if (!startDate.equals(this.startDate)) {
				if (isIOSigned() || isFinished()) {
					throw new IllegalArgumentException(
							"Start date cannot be changed once IO is signed or delisted/stopped or completed");
				}
			}
			if (!endDate.equals(this.endDate)) {
				if (isFinished()) {
					throw new IllegalArgumentException(
							"End date cannot be changed once a campaign is delisted/stopped or completed");
				}
			}
			if (endDate.compareTo(startDate) <= 0) {
				throw new IllegalArgumentException("End date should be greater than start date");
			}
		}
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(CampaignTypes type) {
		if (type == null && this.status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException("Type is required");
		}
		// if(this.status == CampaignStatus.CREATED) {
			this.type = type;
		// } else {
		// 	throw new IllegalArgumentException("Status can only be changed if campaign is in created status");
		// }
	}

	/**
	 * @param deliveryTarget
	 *            the deliveryTarget to set
	 */
	public void setDeliveryTarget(int deliveryTarget) {
		if (isFinished()) {
			throw new IllegalArgumentException(
					"Delivery target cannot be changed once a campaign is delisted/stopped or completed");
		}
		if (deliveryTarget <= 0 && this.status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException("Invalid delivery target");
		}
		this.deliveryTarget = deliveryTarget;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(String domain) {
		/*if (domain == null) {
			throw new IllegalArgumentException("Domain is required");
		}
		if (!domain.equals(this.domain)) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Domain cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}*/
		this.domain = domain;
	}

	/**
	 * @param dailyCap
	 *            the dailyCap to set
	 */
	public void setDailyCap(int dailyCap) {
		if (isFinished()) {
			throw new IllegalArgumentException(
					"Daily cap cannot be changed once a campaign is delisted/stopped or completed");
		}
		if (dailyCap < 0 && this.status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException("Invalid daily cap");
		}
		this.dailyCap = dailyCap;
	}

	/**
	 * @param qualificationCriteria
	 *            the qualificationCriteria to set
	 */
	public void setQualificationCriteria(QualificationCriteria qualificationCriteria) {
		if (this.qualificationCriteria != null && !this.qualificationCriteria.toString().equals(qualificationCriteria.toString())) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Qualification criteria cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}
		/*if (type != CampaignTypes.LeadQualification) {
			throw new IllegalArgumentException(
					"Qualification criteria is allowed only for lead qualification campaigns");
		}*/
		if (qualificationCriteria == null && this.status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException("Qualification criteria is required");
		}
		if (qualificationCriteria != null) {
			this.qualificationCriteria = qualificationCriteria;
			this.qualificationClause = qualificationCriteria.toString();
		}
	}

	/**
	 * @param restrictions
	 *            the restrictions to set
	 */
	public void setRestrictions(List<String> restrictions) {
		if (this.restrictions != null && (this.restrictions.size() != restrictions.size() || !this.restrictions.containsAll(restrictions) )) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Restrictions cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}
		this.restrictions = restrictions;
	}

	/**
	 * @param informationCriteria
	 *            the informationCriteria to set
	 */
	public void setInformationCriteria(Set<String> informationCriteria) {
		if (this.informationCriteria != null && (this.informationCriteria.size() != informationCriteria.size() || !this.informationCriteria.containsAll(informationCriteria) )) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Information criteria cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}
		this.informationCriteria = informationCriteria;
	}

	/**
	 * @param leadFilteringCriteria
	 *            the leadFilteringCriteria to set
	 */
	public void setLeadFilteringCriteria(ExpressionGroup leadFilteringCriteria) {
		if (this.leadFilteringCriteria != null && !this.leadFilteringCriteria.toString().equals(leadFilteringCriteria.toString())) {
			if (isIOSigned() || isFinished()) {
				throw new IllegalArgumentException(
						"Lead filtering criteria cannot be changed once IO is signed or delisted/stopped or completed");
			}
		}
		this.leadFilteringCriteria = leadFilteringCriteria;
		if (leadFilteringCriteria != null) {
			this.leadFilteringClause = leadFilteringCriteria.toString();
		} else {
			this.leadFilteringClause = null;
		}
	}

	/**
	 * @param assets
	 *            the assets to set
	 */
	public void setAssets(List<Asset> assets) {
		if (isFinished()) {
			throw new IllegalArgumentException(
					"Assets cannot be changed once a campaign is delisted/stopped or completed");
		}
		this.assets = assets;
	}

	/**
	 * @param selfManageTeam
	 *            the selfManageTeam to set
	 */
	public void setSelfManageTeam(boolean selfManageTeam) {
		if(this.status == CampaignStatus.CREATED) {
			this.selfManageTeam = selfManageTeam;
		} else {
			throw new IllegalArgumentException("Self manage team can only be changed if campaign is in created status");
		}
	}

	/**
	 * @param teamInvitations
	 *            the teamInvitations to set
	 */
	public void addTeamInvitations(CampaignTeamInvitation teamInvitation) {
		if (selfManageTeam) {
			throw new IllegalArgumentException("Team invitations are not allowed for self managed teams");
		}
		if (isIOSigned() || isFinished()) {
			throw new IllegalArgumentException(
					"Teams cannot be invited once IO is signed or delisted/stopped or completed");
		}
		boolean found = false;
		for (CampaignTeamInvitation tmpTeamInvitatation : teamInvitations) {
			if (tmpTeamInvitatation.getTeamId().equals(teamInvitation.getTeamId())) {
				found = true;
				break;
			}
		}
		if (!found) {
			this.teamInvitations.add(teamInvitation);
			// remove Bidding
			//this.setStatus(CampaignStatus.BIDDING);
		}
	}

	/**
	 * @param teamshortlisted
	 *            the teamInvitations to set
	 */
	public void addTeamShortlisted(CampaignTeamShortlisted campaignTeamShortlisted) {
		if (selfManageTeam) {
			throw new IllegalArgumentException("Team shortlisted are not allowed for self managed teams");
		}
		if (isIOSigned() || isFinished()) {
			throw new IllegalArgumentException(
					"Teams cannot be shortlisted once IO is signed or delisted/stopped or completed");
		}
		boolean found = false;
		for (CampaignTeamShortlisted tmpCampaignTeamShortlisted : teamsShortlisted) {
			if (tmpCampaignTeamShortlisted.getTeamId().equals(campaignTeamShortlisted.getTeamId())) {
				found = true;
				break;
			}
		}
		if (!found) {
			this.teamsShortlisted.add(campaignTeamShortlisted);
		}
	}

	/**
	 * @param teamshortlisted
	 *            the teamInvitations to set
	 */
	public void removeTeamShortlisted(String teamId) {
		if (selfManageTeam) {
			throw new IllegalArgumentException("Remove Team shortlisted are not allowed for self managed teams");
		}
		if (isIOSigned() || isFinished()) {
			throw new IllegalArgumentException(
					"Shortlisted team cannot be remove once IO is signed or delisted/stopped or completed");
		}
		CampaignTeamShortlisted teamToBeRemoveShortlisted = null;
		for (CampaignTeamShortlisted tmpCampaignTeamShortlisted : teamsShortlisted) {
			if (tmpCampaignTeamShortlisted.getTeamId().equals(teamId)) {
				teamToBeRemoveShortlisted = tmpCampaignTeamShortlisted;
				break;
			}
		}
		if (teamToBeRemoveShortlisted != null) {
			this.teamsShortlisted.remove(teamToBeRemoveShortlisted);
		}
	}

	/**
	 * @param teamInvited
	 *            the teamInvitations to set
	 */
	public void removeTeamInvited(String teamId) {
		if (selfManageTeam) {
			throw new IllegalArgumentException("Remove Team Invited are not allowed for self managed teams");
		}
		if (isIOSigned() || isFinished()) {
			throw new IllegalArgumentException(
					"Invited team cannot be remove once IO is signed or delisted/stopped or completed");
		}
		CampaignTeamInvitation teamToBeRemoveInvited = null;
		for (CampaignTeamInvitation tmpCampaignTeamInvitation : teamInvitations) {
			if (tmpCampaignTeamInvitation.getTeamId().equals(teamId)) {
				teamToBeRemoveInvited = tmpCampaignTeamInvitation;
				break;
			}
		}
		if (teamToBeRemoveInvited != null) {
			this.teamInvitations.remove(teamToBeRemoveInvited);
		}
	}

	/**
	 * @param agentInvitations
	 *            the agentInvitations to set
	 */
	public void addAgentInvitations(CampaignAgentInvitation agentInvitation) {
		if (!selfManageTeam) {
			throw new IllegalArgumentException("Agent invitations are not allowed for supervisor managed teams");
		}
		if (isIOSigned() || isFinished()) {
			throw new IllegalArgumentException(
					"Agents cannot be invited once IO is signed or delisted/stopped or completed");
		}
		boolean found = false;
		for (CampaignAgentInvitation tmpAgentInvitatation : agentInvitations) {
			if (tmpAgentInvitatation.getAgentId().equals(agentInvitation.getAgentId())) {
				found = true;
				break;
			}
		}
		if (found) {
			throw new IllegalArgumentException("The agent is already been invited");
		}
		// remove Bidding
		if (status != CampaignStatus.CREATED) {
			throw new IllegalArgumentException(
					"Agent invitations are allowed only if the campaign status is bidding or created");
		}
		this.agentInvitations.add(agentInvitation);
		//this.setStatus(CampaignStatus.BIDDING);
	}

	/**
	 * @param team
	 *            the team to set
	 */
	public void chooseTeam(String teamId, String supervisorId, CampaignTeamDTO campaignTeamDTO) {
		if (this.selfManageTeam) {
			throw new IllegalArgumentException("Team cannot be explicitly set for self managed campaigns");
		}
		if (this.team != null) {
			// throw new IllegalArgumentException("Team already assigned to campaign");
		}
		//DATE:18/12/21017	supervisor can be changed except if campaign is in running state
		/*if (this.status == CampaignStatus.RUNNING) {
				throw new IllegalArgumentException("Cannot assign team. Campaign is in running mode.");
			} else {
				CampaignTeam oldCampaignTeam = this.team;
				this.team = new CampaignTeam(teamId, supervisorId, campaignTeamDTO.getBid());
				this.team.setCallGuide(oldCampaignTeam.getCallGuide());
				this.team.setLeadSortOrder(oldCampaignTeam.getLeadSortOrder());
			}*/
		else {
			//this.team = new CampaignTeam(teamId, supervisorId, bid);
			//this.team.setCallSpeedPerMinPerAgent(CallSpeed.CONSERVATIVE);
			/* START
			 * DATE : 12/06/2017
			 * REASON : setting call speed (INTERMEDIATE by default. sending default value from front-end). Initially it was CONSERVATIVE by default */
			this.team = new CampaignTeam(teamId, supervisorId, campaignTeamDTO.getBid());
			this.team.setCallSpeedPerMinPerAgent(campaignTeamDTO.getCallSpeedPerMinPerAgent());
			this.team.setCallSpeedRetryPerMinPerAgent(campaignTeamDTO.getCallSpeedRetryPerMinPerAgent());
			this.team.setCampaignSpeed(campaignTeamDTO.getCampaignSpeed());
			//this.team.setCampaignThreadCap(campaignTeamDTO.getCampaignThreadCap());
			/* END */
			this.team.setLeadSortOrder(campaignTeamDTO.getLeadSortOrder());
		}
	}

	/**
	 * @param callGuide
	 *            the callGuide to set
	 */
	public void setCallGuide(CallGuide callGuide) {
		this.team.setCallGuide(callGuide);
	}


	public CallGuide getCallGuide() {
		return this.team.getCallGuide();
	}

	/**
	 * @param leadSortOrder
	 *            the leadSortOrder to set
	 */
	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.team.setLeadSortOrder(leadSortOrder);
	}

	/**
	 * @param callSpeedPerMinPerAgent
	 *            the callSpeedPerMinPerAgent to set
	 */
	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.team.setCallSpeedPerMinPerAgent(callSpeedPerMinPerAgent);
	}

	public void setCallSpeedRetryPerMinPerAgent(CallSpeed callSpeedRetryPerMinPerAgent) {
		this.team.setCallSpeedRetryPerMinPerAgent(callSpeedRetryPerMinPerAgent);
	}

	/**
	 * @param agents
	 *            the agents to set
	 */
	public void allocateAgent(String agentId) {
		this.team.allocateAgent(agentId);
	}

	public void deallocateAgent(String agentId) {
		this.team.deallocateAgent(agentId);
		// TODO:: Don't let the agent deallocated if the number of agents are
		// only one in running campaigns
	}

	public void addWorkSchedule(String agentId, WorkSchedule workSchedule) {
		this.team.addWorkSchedule(agentId, workSchedule);
	}

	public void updateWorkSchedule(String agentId, String scheduleId, WorkSchedule workSchedule) {
		this.team.updateWorkSchedule(agentId, scheduleId, workSchedule);
	}

	public void removeWorkSchedule(String agentId, String scheduleId) {
		this.team.removeWorkSchedule(agentId, scheduleId);
	}

	public WorkSchedule getWorkSchedule(String agentId, String scheduleId) {
		return this.team.getWorkSchedule(agentId, scheduleId);
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @return the sfdcCampaignId
	 */
	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	/**
	 * @return the organizationId
	 */
	public String getOrganizationId() {
		return organizationId;
	}

	/**
	 * @return the campaignManagerId
	 */
	public String getCampaignManagerId() {
		return campaignManagerId;
	}

	/**
	 * @return the status
	 */
	public CampaignStatus getStatus() {
		return status;
	}

	public String getCurrentAssetId() {
		return currentAssetId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return the type
	 */
	public CampaignTypes getType() {
		return type;
	}

	/**
	 * @return the deliveryTarget
	 */
	public int getDeliveryTarget() {
		return deliveryTarget;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @return the dailyCap
	 */
	public int getDailyCap() {
		return dailyCap;
	}

	/**
	 * @return the qualificationCriteria
	 */
	public QualificationCriteria getQualificationCriteria() {
		return qualificationCriteria;
	}

	/**
	 * @return the qualificationClause
	 */
	public String getQualificationClause() {
		return qualificationClause;
	}

	/**
	 * @return the restrictions
	 */
	public List<String> getRestrictions() {
		return restrictions;
	}

	/**
	 * @return the informationCriteria
	 */
	public Set<String> getInformationCriteria() {
		return informationCriteria;
	}

	/**
	 * @return the leadFilteringCriteria
	 */
	public ExpressionGroup getLeadFilteringCriteria() {
		return leadFilteringCriteria;
	}

	/**
	 * @return the leadFilteringClause
	 */
	public String getLeadFilteringClause() {
		return leadFilteringClause;
	}

	/**
	 * @return the assets
	 */
	public List<Asset> getAssets() {
		return assets;
	}

	//	public Asset getActiveAsset() {
	//		// Date:11/10/2017	Reason:Agent able to submit(SUCCESS) prospect even there is no ASSET
	//		if(assets != null) {
	//			for (Asset asset : assets) {
	//				if (asset.getAssetId().equals(currentAssetId)) {
	//					return asset;
	//				}
	//			}
	//		}
	//		return null;
	//	}

	public List<Asset> getActiveAsset() {
		// Date:11/10/2017
		// Reason:Agent able to submit(SUCCESS) prospect even there is no ASSET
		List<Asset> assetList = new ArrayList<>();
		if (assets != null) {
			if (multiCurrentAssetId != null) {
				for (String currentAsset : multiCurrentAssetId) {
					for (Asset asset : assets) {
						if (asset.getAssetId().equals(currentAsset)) {
							assetList.add(asset);
						}
					}
				}
			} else if (currentAssetId != null) {
				for (Asset asset : assets) {
					if (asset.getAssetId().equals(currentAssetId)) {
						assetList.add(asset);
					}
				}
			}
			return assetList;
		}
		return null;
	}


	/**
	 * @return the teamInvitations
	 */
	public List<CampaignTeamInvitation> getTeamInvitations() {
		return Collections.unmodifiableList(teamInvitations);
	}

	/**
	 * @return the agentInvitations
	 */
	public List<CampaignAgentInvitation> getAgentInvitations() {
		return Collections.unmodifiableList(agentInvitations);
	}

	/**
	 * @return the teamsShortlisted
	 */
	public List<CampaignTeamShortlisted> getTeamsShortlisted() {
		return Collections.unmodifiableList(teamsShortlisted);
	}

	/**
	 * @param teamsShortlisted the teamsShortlisted to set
	 */
	public void setTeamsShortlisted(List<CampaignTeamShortlisted> teamsShortlisted) {
		this.teamsShortlisted = teamsShortlisted;
	}

	/**
	 * @return the selfManageTeam
	 */
	public boolean isSelfManageTeam() {
		return selfManageTeam;
	}

	/**
	 * @return the team
	 */
	public CampaignTeam getTeam() {
		// TODO:: Return a cloned object so that no one can change this outside
		return team;
	}

	public int getRequiredHours() {
		if (startDate != null && endDate != null) {
			long duration = (endDate.getTime() - startDate.getTime()) / XtaasConstants.ONE_DAY_IN_MILLIS;
			// The logic to calculate required hours is one hour per lead generated.
			// This logic need to be updated based on strategy pattern
			int perDayLeads = deliveryTarget / (int) duration;
			return perDayLeads * 30;
		}
		return 0;

	}

	private boolean isIOSigned() {
		//		return status == CampaignStatus.RUNNING || status == CampaignStatus.PAUSED;
		return false;	// DATE:09/10/20187	REASON:returning false to allow CM to modify campaign details in all statuses.
	}

	private boolean isFinished() {
		return status == CampaignStatus.STOPPED || status == CampaignStatus.DELISTED
				|| status == CampaignStatus.COMPLETED;
	}

	public void setAssetEmailTemplate(EmailMessage assetEmailTemplate) {
		this.assetEmailTemplate = assetEmailTemplate;
	}

	public void setDialerMode(DialerMode dialerMode) {
		this.dialerMode = dialerMode;
	}

	public EmailMessage getAssetEmailTemplate() {
		return assetEmailTemplate;
	}

	public DialerMode getDialerMode() {
		return dialerMode;
	}

	//function to activate specific asset in campaign
	//	public void activateAsset(String currentAssetId) {
	//		if (this.assets == null || this.assets.isEmpty()) {
	//			throw new IllegalArgumentException("No assets present in campaign");
	//		}
	//		if ( !checkAssetPresence(currentAssetId) ) {
	//			throw new IllegalArgumentException("No such asset present in campaign");
	//		}
	//		if(this.currentAssetId != null && this.currentAssetId.equals(currentAssetId)) {
	//			throw new IllegalArgumentException("Given asset already activated in campaign");
	//		}
	//		this.currentAssetId = currentAssetId;
	//	}

	//function to activate multiple asset in campaign
	public void activateAssets(List<String> currentAssetIds) {
		if (this.assets == null || this.assets.isEmpty()) {
			throw new IllegalArgumentException("No assets present in campaign");
		}
		//			if ( !checkAssetPresence(currentAssetIds) ) {
		//				throw new IllegalArgumentException("No such asset present in campaign");
		//			}
		//			if(this.currentAssetId != null && this.currentAssetId.equals(currentAssetId)) {
		//				throw new IllegalArgumentException("Given asset already activated in campaign");
		//			}
		if (currentAssetIds != null && currentAssetIds.size() > 0 ) {
			this.currentAssetId = currentAssetIds.get(0);
		} else {
			this.currentAssetId = null;
		}

		this.multiCurrentAssetId = currentAssetIds;
	}

	//logic to check given asset is available or not
	private boolean checkAssetPresence(List<String> assetIds) {
		for(Asset assets : this.assets) {
			for (String assetId : assetIds) {
				if ( assets.getAssetId().equals(assetId) ) {
					return true;
				}
			}
		}
		return false;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public boolean isEnableMachineAnsweredDetection() {
		return enableMachineAnsweredDetection;
	}

	public void setEnableMachineAnsweredDetection(boolean enableMachineAnsweredDetection) {
		this.enableMachineAnsweredDetection = enableMachineAnsweredDetection;
	}

	public boolean isDisableAMDOnQueued() {
		return disableAMDOnQueued;
	}

	public void setDisableAMDOnQueued(boolean disableAMDOnQueued) {
		this.disableAMDOnQueued = disableAMDOnQueued;
	}

	public boolean isUseSlice() {
		return useSlice;
	}

	public void setUseSlice(boolean useSlice) {
		this.useSlice = useSlice;
	}

	public boolean isMultiProspectCalling() {
		return multiProspectCalling;
	}

	public void setMultiProspectCalling(boolean multiProspectCalling) {
		this.multiProspectCalling = multiProspectCalling;
	}

	/* START
	 * DATE : 03/05/2017
	 * REASON : Added to set & get campaign requirements */
	public String getCampaignRequirements() {
		return campaignRequirements;
	}

	public void setCampaignRequirements(String campaignRequirements) {
		this.campaignRequirements = campaignRequirements;
	}
	/* END */

	/* START
	 * DATE : 16/05/2017
	 * REASON : Added to get selected questions for agent (which has "agentValidationRequired" (Expression.java) flag true) */
	public QualificationCriteria getAgentQualificationCriteria() {

		List<Expression> agentExpressions = new ArrayList<Expression>();
		for (Expression expression : qualificationCriteria.getExpressions()) {
			if (expression.getAgentValidationRequired()) {
				if(!expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
						|| !expression.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10"))
				agentExpressions.add(expression);
			}
		}
		return new QualificationCriteria(qualificationCriteria.getGroupClause(), agentExpressions, qualificationCriteria.getExpressionGroups());
	}
	/* END */

	/* START HIDE PHONE
	 * DATE : 03/11/2017
	 * REASON : Added to hide phone number on agent screen from third party center */
	public boolean isHidePhone() {
		return hidePhone;
	}

	public void setHidePhone(boolean hidePhone) {
		this.hidePhone = hidePhone;
	}
	/* END HIDE PHONE */


	/* START PARTNER SUPERVISOR
	 * DATE : 03/11/2017
	 * REASON : Added to give show/hide access to supervisor screen for partner supervisor */
	public List<String> getPartnerSupervisors() {
		return partnerSupervisors;
	}

	public void setPartnerSupervisors(List<String> partnerSupervisors) {
		this.partnerSupervisors = partnerSupervisors;
	}
	/* END PARTNER SUPERVISOR */

	/*START QA REQUIRED
	  DATE : 18/12/2017
	  Added to check if leads goes to qa or not for a campaign*/
	public boolean getQaRequired() {
		return qaRequired;
	}

	public void setQaRequired(boolean qaRequired) {
		this.qaRequired = qaRequired;
	}

	public boolean isSimpleDisposition() {
		return simpleDisposition;
	}

	public void setSimpleDisposition(boolean simpleDisposition) {
		this.simpleDisposition = simpleDisposition;
	}

	public String getHostingServer() {
		return hostingServer;
	}

	public void setHostingServer(String hostingServer) {
		this.hostingServer = hostingServer;
	}

	public boolean getCacheRefresh() {
		return cacheRefresh;
	}

	public void setCacheRefresh(boolean cacheRefresh) {
		this.cacheRefresh = cacheRefresh;
	}

	public Map<String, Float> getRetrySpeed() {
		return retrySpeed;
	}

	public void setRetrySpeed(Map<String, Float> retrySpeed) {
		this.retrySpeed = retrySpeed;
	}

	public boolean isRetrySpeedEnabled() {
		return retrySpeedEnabled;
	}

	public void setRetrySpeedEnabled(boolean retrySpeedEnabled) {
		this.retrySpeedEnabled = retrySpeedEnabled;
	}

	public float getRetrySpeedFactor() {
		return retrySpeedFactor;
	}

	public void setRetrySpeedFactor(float retrySpeedFactor) {
		this.retrySpeedFactor = retrySpeedFactor;
	}

	public int getDailyCallMaxRetries() {
		return dailyCallMaxRetries;
	}

	public void setDailyCallMaxRetries(int dailyCallMaxRetries) {
		this.dailyCallMaxRetries = dailyCallMaxRetries;
	}

	public int getCallMaxRetries() {
		return callMaxRetries;
	}

	public void setCallMaxRetries(int callMaxRetries) {
		this.callMaxRetries = callMaxRetries;
	}

	public boolean isLocalOutboundCalling() {
		return localOutboundCalling;
	}

	public void setLocalOutboundCalling(boolean localOutboundCalling) {
		this.localOutboundCalling = localOutboundCalling;
	}

	public List<String> getCampaignGroupIds() {
		return campaignGroupIds;
	}

	public void setCampaignGroupIds(List<String> campaignGroupIds) {
		this.campaignGroupIds = campaignGroupIds;
	}

	public String getClientCampaignName() {
		return clientCampaignName;
	}

	public void setClientCampaignName(String clientCampaignName) {
		this.clientCampaignName = clientCampaignName;
	}

	public List<String> getMultiCurrentAssetId() {
		return multiCurrentAssetId;
	}

	public void setMultiCurrentAssetId(List<String> multiCurrentAssetId) {
		this.multiCurrentAssetId = multiCurrentAssetId;
	}

	public void setCurrentAssetId(String currentAssetId) {
		this.currentAssetId = currentAssetId;
	}

	public boolean isColdEmails() {
		return coldEmails;
	}

	public void setColdEmails(boolean coldEmails) {
		this.coldEmails = coldEmails;
	}

	public String getColdEmailSubject() {
		return coldEmailSubject;
	}

	public void setColdEmailSubject(String coldEmailSubject) {
		this.coldEmailSubject = coldEmailSubject;
	}

	public String getColdEmailMessage() {
		return coldEmailMessage;
	}

	public void setColdEmailMessage(String coldEmailMessage) {
		this.coldEmailMessage = coldEmailMessage;
	}

	public boolean isSendMailToAllCallables() {
		return sendMailToAllCallables;
	}

	public void setSendMailToAllCallables(boolean sendMailToAllCallables) {
		this.sendMailToAllCallables = sendMailToAllCallables;
	}


	public boolean isEmailSuppressed() {
		return isEmailSuppressed;
	}

	public void setEmailSuppressed(boolean isEmailSuppressed) {
		this.isEmailSuppressed = isEmailSuppressed;
	}

	public String getCallControlProvider() {
		return callControlProvider;
	}

	public void setCallControlProvider(String callControlProvider) {
		this.callControlProvider = callControlProvider;
	}

	public boolean isHideEmail() {
		return hideEmail;
	}

	public void setHideEmail(boolean hideEmail) {
		this.hideEmail = hideEmail;
	}

	public List<String> getAdminSupervisorIds() {
		return adminSupervisorIds;
	}

	public void setAdminSupervisorIds(List<String> adminSupervisorIds) {
		this.adminSupervisorIds = adminSupervisorIds;
	}

	public boolean isABM() {
		return isABM;
	}

	public void setABM(boolean isABM) {
		this.isABM = isABM;
	}
	public boolean isNotConference() {
		return isNotConference;
	}

	public void setNotConference(boolean isNotConference) {
		this.isNotConference = isNotConference;
	}
	public boolean isSupervisorAgentStatusApprove() {
		return supervisorAgentStatusApprove;
	}

	public void setSupervisorAgentStatusApprove(boolean supervisorAgentStatusApprove) {
		this.supervisorAgentStatusApprove = supervisorAgentStatusApprove;
	}

	public int getEmailSuppressionCount() {
		return emailSuppressionCount;
	}

	public void setEmailSuppressionCount(int emailSuppressionCount) {
		this.emailSuppressionCount = emailSuppressionCount;
	}

	public int getDomainSuppressionCount() {
		return domainSuppressionCount;
	}

	public void setDomainSuppressionCount(int domainSuppressionCount) {
		this.domainSuppressionCount = domainSuppressionCount;
	}

	public String getRecordProspect() {
		return recordProspect;
	}

	public void setRecordProspect(String recordProspect) {
		this.recordProspect = recordProspect;
	}

	public boolean isLeadIQ() {
		return isLeadIQ;
	}

	public void setLeadIQ(boolean isLeadIQ) {
		this.isLeadIQ = isLeadIQ;
	}

	public boolean isNetworkCheck() {
		return networkCheck;
	}

	public void setNetworkCheck(boolean networkCheck) {
		this.networkCheck = networkCheck;
	}

	public boolean isBandwidthCheck() {
		return bandwidthCheck;
	}

	public void setBandwidthCheck(boolean bandwidthCheck) {
		this.bandwidthCheck = bandwidthCheck;
	}

	public boolean isUseDirectClassificationML() {
		return useDirectClassificationML;
	}

	public void setUseDirectClassificationML(boolean useDirectClassificationML) {
		this.useDirectClassificationML = useDirectClassificationML;
	}

	public String getHoldAudioUrl() {
		return holdAudioUrl;
	}

	public void setHoldAudioUrl(String holdAudioUrl) {
		this.holdAudioUrl = holdAudioUrl;
	}

	public boolean isTelnyxHold() {
		return telnyxHold;
	}

	public void setTelnyxHold(boolean telnyxHold) {
		this.telnyxHold = telnyxHold;
	}

	public List<KeyValuePair<String, Integer>> getDataSourcePriority() {
		return dataSourcePriority;
	}

	public void setDataSourcePriority(List<KeyValuePair<String, Integer>> dataSourcePriority) {
		this.dataSourcePriority = dataSourcePriority;
	}

	public List<KeyValuePair<String, Integer>> getManualDataSourcePriority() {
		return manualDataSourcePriority;
	}

	public void setManualDataSourcePriority(List<KeyValuePair<String, Integer>> manualDataSourcePriority) {
		this.manualDataSourcePriority = manualDataSourcePriority;
	}

	public boolean isRingingIcon() {
		return ringingIcon;
	}

	public void setRingingIcon(boolean ringingIcon) {
		this.ringingIcon = ringingIcon;
	}

	public boolean isUnHideSocialLink() {
		return unHideSocialLink;
	}

	public void setUnHideSocialLink(boolean unHideSocialLink) {
		this.unHideSocialLink = unHideSocialLink;
	}

	public boolean isMdFiveSuppressionCheck() {
		return mdFiveSuppressionCheck;
	}

	public void setMdFiveSuppressionCheck(boolean mdFiveSuppressionCheck) {
		this.mdFiveSuppressionCheck = mdFiveSuppressionCheck;
	}

	public String getCallableEvent() {
		return callableEvent;
	}

	public void setCallableEvent(String callableEvent) {
		this.callableEvent = callableEvent;
	}

	public boolean isInsideViewDataBuy() {
		return insideViewDataBuy;
	}

	public void setInsideViewDataBuy(boolean insideViewDataBuy) {
		this.insideViewDataBuy = insideViewDataBuy;
	}
	public int getTelnyxCallTimeLimitSeconds() {
		return telnyxCallTimeLimitSeconds;
	}

	public void setTelnyxCallTimeLimitSeconds(int telnyxCallTimeLimitSeconds) {
		this.telnyxCallTimeLimitSeconds = telnyxCallTimeLimitSeconds;
	}

	public boolean isAutoMachineHangup() {
		return autoMachineHangup;
	}

	public void setAutoMachineHangup(boolean autoMachineHangup) {
		this.autoMachineHangup = autoMachineHangup;
	}

	public Map<String, Integer> getSourceCallingPriority() {
		return sourceCallingPriority;
	}

	public void setSourceCallingPriority(Map<String, Integer> sourceCallingPriority) {
		this.sourceCallingPriority = sourceCallingPriority;
	}

	public long getPlivoAMDTime() {
		return plivoAMDTime;
	}

	public void setPlivoAMDTime(long plivoAMDTime) {
		this.plivoAMDTime = plivoAMDTime;
	}

	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

	public boolean isManaulBroadSearch() {
		return manaulBroadSearch;
	}

	public void setManaulBroadSearch(boolean manaulBroadSearch) {
		this.manaulBroadSearch = manaulBroadSearch;
	}

	public boolean isIgnorePrimaryIndustries() {
		return ignorePrimaryIndustries;
	}

	public void setIgnorePrimaryIndustries(boolean ignorePrimaryIndustries) {
		this.ignorePrimaryIndustries = ignorePrimaryIndustries;
	}

	public boolean isPrimaryIndustriesOnly() {
		return primaryIndustriesOnly;
	}

	public void setPrimaryIndustriesOnly(boolean primaryIndustriesOnly) {
		this.primaryIndustriesOnly = primaryIndustriesOnly;
	}
	public int getNumberOfTopRecords() {
		return numberOfTopRecords;
	}

	public void setNumberOfTopRecords(int numberOfTopRecords) {
		this.numberOfTopRecords = numberOfTopRecords;
	}

	public boolean isCallAbandon() {
		return callAbandon;
	}

	public void setCallAbandon(boolean callAbandon) {
		this.callAbandon = callAbandon;
	}

	public int getProspectCacheAgentRange() {
		return prospectCacheAgentRange;
	}

	public void setProspectCacheAgentRange(int prospectCacheAgentRange) {
		this.prospectCacheAgentRange = prospectCacheAgentRange;
	}

	public int getProspectCacheNoOfProspectsPerAgent() {
		return prospectCacheNoOfProspectsPerAgent;
	}

	public void setProspectCacheNoOfProspectsPerAgent(int prospectCacheNoOfProspectsPerAgent) {
		this.prospectCacheNoOfProspectsPerAgent = prospectCacheNoOfProspectsPerAgent;
	}

	public boolean isEnableProspectCaching() {
		return enableProspectCaching;
	}

	public void setEnableProspectCaching(boolean enableProspectCaching) {
		this.enableProspectCaching = enableProspectCaching;
	}

	public boolean isRemoveMobilePhones() {
		return removeMobilePhones;
	}

	public void setRemoveMobilePhones(boolean removeMobilePhones) {
		this.removeMobilePhones = removeMobilePhones;
	}
	
	

	public String getAutoModeCallProvider() {
		return autoModeCallProvider;
	}

	public void setAutoModeCallProvider(String autoModeCallProvider) {
		this.autoModeCallProvider = autoModeCallProvider;
	}

	public String getManualModeCallProvider() {
		return manualModeCallProvider;
	}

	public void setManualModeCallProvider(String manualModeCallProvider) {
		this.manualModeCallProvider = manualModeCallProvider;
	}
	
	public boolean isRevealEmail() {
		return revealEmail;
	}

	public void setRevealEmail(boolean revealEmail) {
		this.revealEmail = revealEmail;
	}
	
	public boolean isHideNonSuccessPII() {
		return hideNonSuccessPII;
	}

	public void setHideNonSuccessPII(boolean hideNonSuccessPII) {
		this.hideNonSuccessPII = hideNonSuccessPII;
	}
	
	public boolean isAutoGoldenPass() {
		return autoGoldenPass;
	}

	public void setAutoGoldenPass(boolean autoGoldenPass) {
		this.autoGoldenPass = autoGoldenPass;
	}

	public int getUpperThreshold() {
		return upperThreshold;
	}

	public void setUpperThreshold(int upperThreshold) {
		this.upperThreshold = upperThreshold;
	}

	public int getLowerThreshold() {
		return lowerThreshold;
	}

	public void setLowerThreshold(int lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}

	public boolean isTagSuccess() {
		return tagSuccess;
	}

	public void setTagSuccess(boolean tagSuccess) {
		this.tagSuccess = tagSuccess;
	}

	public boolean isEnableCustomFields() {
		return enableCustomFields;
	}

	public boolean isSimilarProspect() {
		return similarProspect;
	}

	public void setSimilarProspect(boolean similarProspect) {
		this.similarProspect = similarProspect;
	}

	public void setEnableCustomFields(boolean enableCustomFields) {
		this.enableCustomFields = enableCustomFields;
	}

	public String getCustomFieldAnswer(String attributeName, List<Expression> expressions) {
		String customFieldAnswer = "";
		if (expressions == null)
			return "";
		for (Expression expr : expressions) {
			if (expr.getAttribute() != null && (expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10")) && attributeName.equalsIgnoreCase(expr.getAttribute())) {
				customFieldAnswer = expr.getValue();
			}
		}
		return customFieldAnswer;
	}

	public boolean isAutoIVR() {
		return autoIVR;
	}

	public void setAutoIVR(boolean autoIVR) {
		this.autoIVR = autoIVR;
	}
	
	public Map<String, AgentQaAnswer> getAllDefaultCustomFieldQuestionAndAnswerMap(List<Expression> expressions) {
		Map<String, AgentQaAnswer> customFieldsMap = new HashMap<>(); 
		if (expressions == null)
			return null;
		for (Expression expr : expressions) {
			if (expr.getAttribute() != null && (expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_01")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_02")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_03")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_04")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_05")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_06")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_07")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_08")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_09")
					|| expr.getAttribute().equalsIgnoreCase("CUSTOMFIELD_10"))) {
				AgentQaAnswer qaAnswer = new AgentQaAnswer(expr.getQuestionSkin(), expr.getValue(), expr.getAgentValidationRequired());
				qaAnswer.setAttribute(expr.getAttribute());
				customFieldsMap.put(expr.getAttribute(), qaAnswer);
			}
		}
		return customFieldsMap;
	}

	public boolean isEnableInternetCheck() {
		return enableInternetCheck;
	}

	public void setEnableInternetCheck(boolean enableInternetCheck) {
		this.enableInternetCheck = enableInternetCheck;
	}

	public boolean isEnableStateCodeDropdown() {
		return enableStateCodeDropdown;
	}

	public void setEnableStateCodeDropdown(boolean enableStateCodeDropdown) {
		this.enableStateCodeDropdown = enableStateCodeDropdown;
	}

	public boolean isEnableCallbackFeature() {
		return enableCallbackFeature;
	}

	public void setEnableCallbackFeature(boolean enableCallbackFeature) {
		this.enableCallbackFeature = enableCallbackFeature;
	}

	public String getCustomFieldAttribute(String questionSkin, List<Expression> expressions) {
		String customFieldAttribute = "";
		if (expressions == null)
			return "";
		for (Expression expr : expressions) {
			if (expr.getQuestionSkin() != null &&  !expr.getQuestionSkin().isEmpty() && expr.getQuestionSkin().equalsIgnoreCase(questionSkin)) {
				customFieldAttribute = expr.getAttribute();
			}
		}
		return customFieldAttribute;
	}

	public List<KeyValuePair<String, Integer>> getSimilarDataSourcePriority() {
		return similarDataSourcePriority;
	}

	public void setSimilarDataSourcePriority(List<KeyValuePair<String, Integer>> similarDataSourcePriority) {
		this.similarDataSourcePriority = similarDataSourcePriority;
	}

	public boolean isUseInsideviewSmartCache() {
		return useInsideviewSmartCache;
	}

	public void setUseInsideviewSmartCache(boolean useInsideviewSmartCache) {
		this.useInsideviewSmartCache = useInsideviewSmartCache;

	}
	public boolean isReviewZoomData() {
		return reviewZoomData;
	}

	public void setReviewZoomData(boolean reviewZoomData) {
		this.reviewZoomData = reviewZoomData;
	}
	
	
	public boolean isUkPhoneDncCheck() {
		return ukPhoneDncCheck;

	}
	public void setUkPhoneDncCheck(boolean ukPhoneDncCheck) {
		this.ukPhoneDncCheck = ukPhoneDncCheck;
	}

	public boolean isUsPhoneDncCheck() {
		return usPhoneDncCheck;
	}

	public void setUsPhoneDncCheck(boolean usPhoneDncCheck) {
		this.usPhoneDncCheck = usPhoneDncCheck;
	}

	public boolean isCallableVanished() {
		return callableVanished;
	}

	public void setCallableVanished(boolean callableVanished) {
		this.callableVanished = callableVanished;
	}

	public boolean isUseLeadIQSmartCache() {
		return useLeadIQSmartCache;
	}

	public void setUseLeadIQSmartCache(boolean useLeadIQSmartCache) {
		this.useLeadIQSmartCache = useLeadIQSmartCache;
	}
	
	public TranscribeCriteriaDTO getTranscribeCriteria() {
		return transcribeCriteria;
	}

	public void setTranscribeCriteria(TranscribeCriteriaDTO transcribeCriteria) {
		this.transcribeCriteria = transcribeCriteria;
	}

	public boolean isTranscribeFilter() {
		return transcribeFilter;
	}

	public void setTranscribeFilter(boolean transcribeFilter) {
		this.transcribeFilter = transcribeFilter;
	}
	
	public String getSipProvider() {
		return sipProvider;
	}

	public void setSipProvider(String sipProvider) {
		this.sipProvider = sipProvider;
	}

	public boolean isAutoCallbackStatusMapCleanup() {
		return autoCallbackStatusMapCleanup;
	}

	public void setAutoCallbackStatusMapCleanup(boolean autoCallbackStatusMapCleanup) {
		this.autoCallbackStatusMapCleanup = autoCallbackStatusMapCleanup;
	}
	
}
