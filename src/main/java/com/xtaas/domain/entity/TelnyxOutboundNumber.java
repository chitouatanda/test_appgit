package com.xtaas.domain.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "telnyxoutboundnumber")
public class TelnyxOutboundNumber extends AbstractEntity {

	private String phoneNumber;
	private String displayPhoneNumber;
	private int areaCode;
	private int isdCode;
	private String provider;
	private String partnerId;
	private String bucketName;
	private String voiceMessage;
	private int pool;
	private String timeZone;
	private String countryCode;
	private String countryName;
	private String connectionId;
	private String telnyxPhoneNumberId;

	public TelnyxOutboundNumber(String phoneNumber, String displayPhoneNumber, int areaCode, int isdCode,
			String provider, String partnerId, String bucketName, String voiceMessage, int pool, String timeZone,
			String countryCode, String countryName, String connectionId, String telnyxPhoneNumberId) {
		setPhoneNumber(phoneNumber);
		setDisplayPhoneNumber(displayPhoneNumber);
		setAreaCode(areaCode);
		setIsdCode(isdCode);
		setProvider(provider);
		setPartnerId(partnerId);
		setBucketName(bucketName);
		setVoiceMessage(voiceMessage);
		setPool(pool);
		setTimeZone(timeZone);
		setCountryCode(countryCode);
		setCountryName(countryName);
		setConnectionId(connectionId);
		setTelnyxPhoneNumberId(telnyxPhoneNumberId);
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDisplayPhoneNumber() {
		return displayPhoneNumber;
	}

	public void setDisplayPhoneNumber(String displayPhoneNumber) {
		this.displayPhoneNumber = displayPhoneNumber;
	}

	public int getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getVoiceMessage() {
		return voiceMessage;
	}

	public void setVoiceMessage(String voiceMessage) {
		this.voiceMessage = voiceMessage;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getTelnyxPhoneNumberId() {
		return telnyxPhoneNumberId;
	}

	public void setTelnyxPhoneNumberId(String telnyxPhoneNumberId) {
		this.telnyxPhoneNumberId = telnyxPhoneNumberId;
	}

	@Override
	public String toString() {
		return "TelnyxOutboundNumber [phoneNumber=" + phoneNumber + ", displayPhoneNumber=" + displayPhoneNumber
				+ ", areaCode=" + areaCode + ", isdCode=" + isdCode + ", provider=" + provider + ", partnerId="
				+ partnerId + ", bucketName=" + bucketName + ", voiceMessage=" + voiceMessage + ", pool=" + pool
				+ ", timeZone=" + timeZone + ", countryCode=" + countryCode + ", countryName=" + countryName
				+ ", connectionId=" + connectionId + ", telnyxPhoneNumberId=" + telnyxPhoneNumberId + "]";
	}

}