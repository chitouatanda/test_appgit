package com.xtaas.domain.entity;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xtaas.db.entity.Prospect;
import com.xtaas.domain.valueobject.AgentQaAnswer;
import com.xtaas.domain.valueobject.ContactFunction;
import com.xtaas.domain.valueobject.ContactIndustry;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection="campaigncontact")
public class CampaignContact extends AbstractEntity {
	
	private static final String SECRET_KEY = "KqBJ0i0pvi5e2gZzPV4eLg==";
	private static final byte[] decodedKey = Base64.getDecoder().decode(SECRET_KEY);
	// rebuild key using SecretKeySpec
    private static final SecretKey secKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
	
    private String contactId;
	private String sfdcCampaignId;
	private String campaignId;
	private String source;
	private String sourceId;
	private String prefix;
	private String firstName;
	private String lastName;
	private String suffix;
	private String title;
	private String department;
	private List<ContactFunction> functions;
	private String organizationName;
	private String domain;
	private List<ContactIndustry> industries;
	private List<String> topLevelIndustries;
	private double minRevenue;
	private double maxRevenue;
	private double minEmployeeCount;
	private double maxEmployeeCount;
	private String email;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String stateCode;
	private String country;
	private String postalCode;
	private String phone;
	private String companyPhone;
	private String extension;
	private String listPurchaseTransactionId;
	private String sourceCompanyId;
	private String managementLevel;
	private Map<String, String> companyAddress;
	private boolean isEncrypted;
	private boolean isDirectPhone;
	private String zoomCompanyUrl;
	private String zoomPersonUrl;
	private boolean isEu;
	private String sourceType;
	private String dataSource;
	private Integer sortOrder;
	private List<AgentQaAnswer> answers;
	
	protected CampaignContact() {
		// TODO Auto-generated constructor stub
	}
	
	public CampaignContact(String campaignId, String source, String sourceId, String prefix, String firstName, String lastName, String suffix,
			String title, String department, List<ContactFunction> functions, String organizationName, String domain, List<ContactIndustry> industries,
			List<String> topLevelIndustries,double minRevenue, double maxRevenue, double minEmployeeCount, double maxEmployeeCount, String email,
			String addressLine1, String addressLine2, String city, String stateCode, String country, String postalCode,
			String phone,String companyPhone, String sourceCompanyId, String managementLevel, Map<String, String> companyAddress,boolean isEncrypted) {
		this.setEncrypted(isEncrypted);
		this.setCampaignId(campaignId);
		this.setSource(source);
		this.setSourceId(sourceId);
		this.setPrefix(prefix);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setSuffix(suffix);
		this.setTitle(title);
		this.setFunctions(functions);
		this.setOrganizationName(organizationName);
		this.setDomain(domain);
		this.setIndustries(industries);
		this.setTopLevelIndustries(topLevelIndustries);
		this.setMinRevenue(minRevenue);
		this.setMaxRevenue(maxRevenue);
		this.setMinEmployeeCount(minEmployeeCount);
		this.setMaxEmployeeCount(maxEmployeeCount);
		this.setEmail(email);
		this.setAddressLine1(addressLine1);
		this.setAddressLine2(addressLine2);
		this.setCity(city);
		this.setStateCode(stateCode);
		this.setCountry(country);
		this.setPostalCode(postalCode);
		this.setPhone(phone);
		this.setCompanyPhone(companyPhone);
		this.setDepartment(department);
		this.setSourceCompanyId(sourceCompanyId);
		this.setManagementLevel(managementLevel);
		this.setCompanyAddress(companyAddress);
	}
	
	public boolean isEncrypted() {
		return isEncrypted;
	}



	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}



	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}



	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}



	/**
	 * @return the sourceId
	 */
	public String getSourceId() {
		return sourceId;
	}



	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}



	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	private void setCampaignId(String campaignId) {
		if (campaignId == null || campaignId.isEmpty()) {
			throw new IllegalArgumentException("Campaign id in campaign contact is required");
		}
		this.campaignId = campaignId;
	}

	private void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	private void setFirstName(String firstName) {
		if (firstName == null || firstName.isEmpty()) {
			throw new IllegalArgumentException("First name in campaign contact is required");
		}
		if(this.isEncrypted)
			this.firstName = encryptText(firstName);
		else
			this.firstName = firstName;
	}

	private void setLastName(String lastName) {
		if (lastName == null || lastName.isEmpty()) {
			throw new IllegalArgumentException("Last name in campaign contact is required");
		}
		if(this.isEncrypted)
			this.lastName = encryptText(lastName);
		else
			this.lastName = lastName;
	}

	private void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	private void setOrganizationName(String organizationName) {
		if (organizationName == null || organizationName.isEmpty()) {
			//throw new IllegalArgumentException("Organization name in campaign contact is required");
			organizationName ="NOT_FOUND_BY_PROVIDER";
		}
		this.organizationName = organizationName;
	}

	private void setDomain(String domain) {
		this.domain = domain;
	}

	private void setMinRevenue(double minRevenue) {
		this.minRevenue = minRevenue;
	}

	private void setMaxRevenue(double maxRevenue) {
		this.maxRevenue = maxRevenue;
	}

	private void setMinEmployeeCount(double minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}

	private void setMaxEmployeeCount(double maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}

	private void setEmail(String email) {
		if(this.isEncrypted)
			this.email = encryptText(email);
		else
			this.email = email;
	}

	private void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	private void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	private void setCity(String city) {
		this.city = city;
	}

	private void setStateCode(String stateCode) {
		if(this.stateCode==null && stateCode!=null)
			this.stateCode = stateCode.toUpperCase();
	}

	private void setCountry(String country) {
		this.country = country;
	}

	private void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	private void setPhone(String phone) {
		if (phone == null || phone.isEmpty()) {
			throw new IllegalArgumentException("Phone in campaign contact is required");
		}
		if(this.isEncrypted)
			this.phone = encryptText(phone);
		else
			this.phone = phone;
	}
	

	private void setCompanyPhone(String companyPhone) {
		if(this.isEncrypted)
			this.companyPhone = encryptText(companyPhone);
		else
			this.companyPhone = companyPhone;
	}
	
	public String getCampaignId() {
		return campaignId;
	}


	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getFirstName() {
		if(this.isEncrypted)
			return decryptText(firstName);
		return firstName;
	}

	public String getLastName() {
		if(this.isEncrypted)
			return decryptText(lastName);
		return lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getTitle() {
		return title;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public String getDomain() {
		return domain;
	}

	public double getMinRevenue() {
		return minRevenue;
	}

	public double getMaxRevenue() {
		return maxRevenue;
	}

	public double getMinEmployeeCount() {
		return minEmployeeCount;
	}

	public double getMaxEmployeeCount() {
		return maxEmployeeCount;
	}

	public String getEmail() {
		if(this.isEncrypted)
			return decryptText(email);
		return email;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getCity() {
		return city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public String getCountry() {
		return country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getPhone() {
		if(this.isEncrypted)
			return decryptText(phone);
		return phone;
	}
	
	public String getCompanyPhone() {
		if(this.isEncrypted)
			return decryptText(companyPhone);
		return companyPhone;
	}



	public List<ContactFunction> getFunctions() {
		return functions;
	}



	public void setFunctions(List<ContactFunction> functions) {
		this.functions = functions;
	}



	public List<ContactIndustry> getIndustries() {
		return industries;
	}



	public void setIndustries(List<ContactIndustry> industries) {
		this.industries = industries;
	}



	/**
	 * @return the listPurchaseTransactionId
	 */
	public String getListPurchaseTransactionId() {
		return listPurchaseTransactionId;
	}

	/**
	 * @param listPurchaseTransactionId the listPurchaseTransactionId to set
	 */
	public void setListPurchaseTransactionId(String listPurchaseTransactionId) {
		this.listPurchaseTransactionId = listPurchaseTransactionId;
	}
	
	
	
	public Prospect toProspect(String industry) {
		Prospect prospect = new Prospect();
		prospect.setSourceId(this.sourceId);
		prospect.setEncrypted(this.isEncrypted);
		prospect.setCampaignContactId(this.getId());
		prospect.setEmail(this.getEmail());
		prospect.setPrefix(this.getPrefix());
		prospect.setFirstName(this.getFirstName());
		prospect.setLastName(this.getLastName());
		prospect.setSuffix(this.getSuffix());
		prospect.setCompany(this.getOrganizationName());
		prospect.setAddressLine1(this.getAddressLine1());
		prospect.setAddressLine2(this.getAddressLine2());
		prospect.setZipCode(this.getPostalCode());
		prospect.setCity(this.getCity());
		prospect.setPhone(this.getPhone());
		prospect.setCompanyPhone(this.getCompanyPhone());
		if (this.getPhone() != null && !this.getPhone().isEmpty() && this.getCompanyPhone() != null
				&& !this.getCompanyPhone().isEmpty()) {
			if (this.phone.equalsIgnoreCase(this.companyPhone)) {
				prospect.setDirectPhone(false);
			} else {
				prospect.setDirectPhone(true);
			}
		} else {
			prospect.setDirectPhone(true);
		}
		prospect.setExtension(this.extension);
		prospect.setStateCode(this.getStateCode());
		prospect.setCountry(this.getCountry());
		prospect.setTitle(this.getTitle());
		prospect.setDepartment(this.getDepartment());
		ContactIndustry contactIndustry = getPrimaryIndustry();
		if(industry==null)
			prospect.setIndustry(contactIndustry != null ? contactIndustry.getName() : null);
		else
			prospect.setIndustry(industry);
		prospect.setIndustryList(getAllIndustries());
		prospect.setTopLevelIndustries(this.topLevelIndustries);
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		customAttributes.put("minRevenue", this.getMinRevenue());
		customAttributes.put("maxRevenue", this.getMaxRevenue());
		customAttributes.put("maxEmployeeCount", this.getMaxEmployeeCount());
		customAttributes.put("minEmployeeCount", this.getMinEmployeeCount());
		customAttributes.put("domain", this.getDomain());
		prospect.setCustomAttributes(customAttributes);
		prospect.setSourceCompanyId(this.getSourceCompanyId());
		prospect.setManagementLevel(this.getManagementLevel());
		return prospect;
	}
	
	public Prospect toProspectFromExcel(String industry) {
		Prospect prospect = new Prospect();
		prospect.setSourceId(this.sourceId);
		prospect.setEncrypted(this.isEncrypted);
		prospect.setCampaignContactId(this.getId());
		prospect.setEmail(this.getEmail());
		prospect.setPrefix(this.getPrefix());
		prospect.setFirstName(this.getFirstName());
		prospect.setLastName(this.getLastName());
		prospect.setSuffix(this.getSuffix());
		prospect.setCompany(this.getOrganizationName());
		prospect.setAddressLine1(this.getAddressLine1());
		prospect.setAddressLine2(this.getAddressLine2());
		prospect.setZipCode(this.getPostalCode());
		prospect.setCity(this.getCity());
		prospect.setPhone(this.getPhone());
		prospect.setCompanyPhone(this.getCompanyPhone());
		prospect.setDirectPhone(this.isDirectPhone());
		prospect.setExtension(this.extension);
		prospect.setStateCode(this.getStateCode());
		prospect.setCountry(this.getCountry());
		prospect.setTitle(this.getTitle());
		prospect.setDepartment(this.getDepartment());
		ContactIndustry contactIndustry = getPrimaryIndustry();
		if(industry==null)
			prospect.setIndustry(contactIndustry != null ? contactIndustry.getName() : null);
		else
			prospect.setIndustry(industry);
		prospect.setIndustryList(getAllIndustries());
		prospect.setTopLevelIndustries(this.topLevelIndustries);
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		customAttributes.put("minRevenue", this.getMinRevenue());
		customAttributes.put("maxRevenue", this.getMaxRevenue());
		customAttributes.put("maxEmployeeCount", this.getMaxEmployeeCount());
		customAttributes.put("minEmployeeCount", this.getMinEmployeeCount());
		customAttributes.put("domain", this.getDomain());
		prospect.setCustomAttributes(customAttributes);
		prospect.setSourceCompanyId(this.getSourceCompanyId());
		prospect.setManagementLevel(this.getManagementLevel());
		prospect.setZoomCompanyUrl(this.getZoomCompanyUrl());
		prospect.setZoomPersonUrl(this.getZoomPersonUrl());
		return prospect;
	}
	
	private ContactIndustry getPrimaryIndustry() {
		boolean primaryIndustryAbsent = false;
		if (industries == null) return null;
		for (ContactIndustry contactIndustry : industries) {
			if (contactIndustry.getPrimary() != null) {
				return contactIndustry;
			} else {
				primaryIndustryAbsent = true;
			}
		}
		if (primaryIndustryAbsent) {
			return industries.get(0);
		}
		return null;
		
	}
	
	private List<String> getAllIndustries(){
	    if (industries == null) return null;
	    List<String> allIndustries=new ArrayList<String>();
	    ListIterator<ContactIndustry> itr = industries.listIterator();
        while(itr.hasNext()){
            allIndustries.add(itr.next().getName());
        }
        return allIndustries;
	}



    public String getSourceCompanyId() {
        return sourceCompanyId;
    }



    public void setSourceCompanyId(String sourceCompanyId) {
        this.sourceCompanyId = sourceCompanyId;
    }



    public String getManagementLevel() {
        return managementLevel;
    }



    public void setManagementLevel(String managementLevel) {
        this.managementLevel = managementLevel;
    }
    
    public List<String> getTopLevelIndustries() {
		return topLevelIndustries;
	}

	public void setTopLevelIndustries(List<String> topLevelIndustries) {
		this.topLevelIndustries = topLevelIndustries;
	}
	
	public Map<String, String> getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Map<String, String> companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	 /**
     * Encrypts plainText in AES using the secret key
     * @param plainText
     * @param secKey
     * @return
     * @throws Exception
     */
    public String encryptText(String plainText) {
    	if(plainText==null || plainText.isEmpty()){
    		return plainText;
    	}
    	String enText="";
    	try{
	        // AES defaults to AES/ECB/PKCS5Padding in Java 7
	        Cipher aesCipher = Cipher.getInstance("AES");
	
	        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
	
	        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
	        enText =  Base64.getEncoder().encodeToString(byteCipherText);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
        return "XTAAS-"+enText;

   }
    
    /**
     * Decrypts encrypted byte array using the key used for encryption.
     * @param byteCipherText
     * @param secKey
     * @return
     * @throws Exception
     */
    public String decryptText(String cipherText) {
    	if(cipherText==null || cipherText.isEmpty()){
    		return cipherText;
    	}
    	String deText = "";
    	try{
    		if(cipherText.startsWith("XTAAS-")){
    			cipherText = cipherText.substring(6);
    			// AES defaults to AES/ECB/PKCS5Padding in Java 7
    			Cipher aesCipher = Cipher.getInstance("AES");
    			aesCipher.init(Cipher.DECRYPT_MODE, secKey);
    			byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(cipherText));
    			deText = new String(bytePlainText);
    		}else{
    			deText = cipherText;
    		}
	       /* byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
	        return new String(original);*/
    	}catch(Exception e){
    		e.printStackTrace();
    	}
        return deText;

   }

	public boolean isDirectPhone() {
		return isDirectPhone;
	}

	public void setDirectPhone(boolean isDirectPhone) {
		this.isDirectPhone = isDirectPhone;
	}

	public String getZoomCompanyUrl() {
		return zoomCompanyUrl;
	}

	public void setZoomCompanyUrl(String zoomCompanyUrl) {
		this.zoomCompanyUrl = zoomCompanyUrl;
	}

	public String getZoomPersonUrl() {
		return zoomPersonUrl;
	}

	public void setZoomPersonUrl(String zoomPersonUrl) {
		this.zoomPersonUrl = zoomPersonUrl;
	}

	public boolean isEu() {
		return isEu;
	}

	public void setEu(boolean isEu) {
		this.isEu = isEu;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getSfdcCampaignId() {
		return sfdcCampaignId;
	}

	public void setSfdcCampaignId(String sfdcCampaignId) {
		this.sfdcCampaignId = sfdcCampaignId;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public List<AgentQaAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AgentQaAnswer> answers) {
		this.answers = answers;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
}
