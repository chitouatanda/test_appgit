package com.xtaas.domain.service;

import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalDate;

import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.valueobject.WorkSlot;

public interface AgentCalendarService {
	
	Map<LocalDate, TreeSet<WorkSlot>> getCalendar(int numOfDays, Agent agent);

	Map<LocalDate, TreeSet<WorkSlot>> getCalendar(LocalDate startDate, LocalDate endDate, Agent agent);

}
