package com.xtaas.domain.service;

import java.util.List;

import com.xtaas.db.entity.TeamNotice;
import com.xtaas.web.dto.TeamNoticeDTO;

public interface TeamNoticeService {

	List<TeamNoticeDTO> getNotices(String userId);
	
	List<TeamNotice> getTeamNotices();
	
	void saveNotices(TeamNoticeDTO teamNoticeDTO);
	
	void saveUserNotices(String userId, String notice);
	
	void markNoticesAsRead(String userId);

	String getUnreadCount(String userId);
	
	String notifySupervisor(String agentId, String mode);
	
}
