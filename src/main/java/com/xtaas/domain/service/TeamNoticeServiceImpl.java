package com.xtaas.domain.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.application.service.AgentService;
import com.xtaas.db.entity.Login;
import com.xtaas.db.entity.TeamNotice;
import com.xtaas.db.repository.LoginRepository;
import com.xtaas.db.repository.TeamNoticeRepository;
import com.xtaas.db.repository.TeamRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Team;
import com.xtaas.domain.valueobject.NoticePriority;
import com.xtaas.domain.valueobject.NoticeType;
import com.xtaas.infra.security.Authorize;
import com.xtaas.infra.security.Roles;
import com.xtaas.service.UserService;
import com.xtaas.utils.PusherUtils;
import com.xtaas.utils.XtaasConstants;
import com.xtaas.utils.XtaasDateUtils;
import com.xtaas.utils.XtaasEmailUtils;
import com.xtaas.utils.XtaasUserUtils;
import com.xtaas.web.dto.TeamNoticeDTO;

@Service
public class TeamNoticeServiceImpl implements TeamNoticeService{
	
	@Autowired
	private TeamNoticeRepository teamNoticeRepository;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AgentService agentService;

	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR, Roles.ROLE_AGENT, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA })
	public List<TeamNoticeDTO> getNotices(String userId) {
		String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
		List<TeamNoticeDTO> teamNoticeDTOList = new ArrayList<TeamNoticeDTO>();
		if (loggedInUserName != null && !loggedInUserName.isEmpty() && loggedInUserName.equalsIgnoreCase(userId)) {
			List<Team> teams = teamRepository.findTeamsByUserId(userId);
			List<String> teamIds = new ArrayList<String>();
			for (Team team : teams) {
				teamIds.add(team.getId());
			}
			List<TeamNotice> teamNotices = teamNoticeRepository.getNotices(new Date(), userId, teamIds);
			List<TeamNotice> UserNotices = teamNoticeRepository.getNoticesByRequestorId(new Date(), userId);
			for (TeamNotice resourceNotice : teamNotices) {
				teamNoticeDTOList.add(new TeamNoticeDTO(resourceNotice));
			}
			for (TeamNotice resourceNotice : UserNotices) {
				TeamNoticeDTO teamNoticeDto = new TeamNoticeDTO(resourceNotice);
				int start = teamNoticeDto.getNotice().lastIndexOf("<");
				int end = teamNoticeDto.getNotice().lastIndexOf(">");
				if (start != -1 && start != -1) {
					teamNoticeDto.setLink(teamNoticeDto.getNotice().substring(start + 1, end));
					teamNoticeDto.setNotice(teamNoticeDto.getNotice().substring(0, start));
				}
				teamNoticeDTOList.add(teamNoticeDto);
			}
			return teamNoticeDTOList;
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
	}

	@Override
	public void saveNotices(TeamNoticeDTO teamNoticeDTO) {
		//get Interval having from date starting at 12 A.M and to date ending at 11.59 P.M
		Interval interval = XtaasDateUtils.getDateTimeInterval(teamNoticeDTO.getStartDate(), teamNoticeDTO.getEndDate(), teamNoticeDTO.getTimeZone());
		teamNoticeDTO.setStartDate(new Date(interval.getStartMillis()));
		teamNoticeDTO.setEndDate(new Date(interval.getEndMillis()));
		TeamNotice teamNotice = teamNoticeDTO.toTeamNotice();
		
		List<Team> teams = teamRepository.findBySupervisorId(XtaasUserUtils.getCurrentLoggedInUsername());
		List<String> teamIds = new ArrayList<String>();
		for (Team team : teams) {
			teamIds.add(team.getId());
		}
		teamNotice.setForTeams(teamIds);
		teamNoticeRepository.save(teamNotice);
	}

	@Override
	public List<TeamNotice> getTeamNotices() {
		return teamNoticeRepository.getNoticesBySupervisorId(new Date(), XtaasUserUtils.getCurrentLoggedInUsername());
	}
	
	@Override
	public void saveUserNotices(String userId, String notice) {
		TeamNotice teamNotice = new TeamNotice();
		Interval interval = XtaasDateUtils.getDateTimeIntervalForTeamNotice(new Date(), "UTC");
		teamNotice.setStartDate(new Date(interval.getStartMillis()));
		teamNotice.setEndDate(new Date(interval.getEndMillis()));
		teamNotice.setRequestorId(userId);
		teamNotice.setNotice(notice);
		teamNotice.setType(NoticeType.STATUS);
		teamNotice.setPriority(NoticePriority.MEDIUM);
		teamNotice.setRead(false);
		teamNoticeRepository.save(teamNotice);
	}
	
	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR, Roles.ROLE_AGENT, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA })
	public void markNoticesAsRead(String userId) {
		String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
		if (loggedInUserName != null && !loggedInUserName.isEmpty() && loggedInUserName.equalsIgnoreCase(userId)) {
			List<TeamNotice> UserNotices = teamNoticeRepository.getNoticesByRequestorId(new Date(), userId);
			for (TeamNotice teamNotice : UserNotices) {
				if (!teamNotice.isRead()) {
					teamNotice.setRead(true);
					teamNoticeRepository.save(teamNotice);
				}
			}
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
	}
	
	@Override
	@Authorize(roles = { Roles.ROLE_CAMPAIGN_MANAGER, Roles.ROLE_SUPERVISOR, Roles.ROLE_AGENT, Roles.ROLE_QA, Roles.ROLE_SECONDARYQA })
	public String getUnreadCount(String userId) {
		int unreadCount = 0;
		String loggedInUserName = XtaasUserUtils.getCurrentLoggedInUsername();
		if (loggedInUserName != null && !loggedInUserName.isEmpty() && loggedInUserName.equalsIgnoreCase(userId)) {
			List<TeamNotice> UserNotices = teamNoticeRepository.getNoticesByRequestorId(new Date(), userId);
			for (TeamNotice teamNotice : UserNotices) {
				if (!teamNotice.isRead()) {
					unreadCount++;
				}
			}
			return Integer.toString(unreadCount);
		} else {
			throw new IllegalArgumentException("You are not authorized.");
		}
	}

	@Override
	public String notifySupervisor(String agentId, String mode) {
		List<Team> teamsFromDB = teamRepository.findAllTeamsByAgentId(agentId);
		Agent agentFromDB = agentService.getAgent(agentId);
		if (teamsFromDB != null && teamsFromDB.size() > 0) {
			List<String> supervisors = teamsFromDB.stream().map(team -> team.getSupervisor().getResourceId())
					.collect(Collectors.toList());
			if (supervisors != null && supervisors.size() > 0) {
				String notice = "";
				for (String supervisor : supervisors) {
					PusherUtils.pushMessageToUser(supervisor, XtaasConstants.TEAM_NOTIFICATION_EVENT, "");
					if (mode.equalsIgnoreCase("AUTO")) {
						notice = " Agent Name " + agentFromDB.getFirstName() + " " + agentFromDB.getLastName() + " Agent ID " + agentId + " from organization " + agentFromDB.getPartnerId()
								+ " was logged out of the system automatically for sequential auto-dispositions on his calls.";
					} else {
						notice = " Agent Name " + agentFromDB.getFirstName() + " " + agentFromDB.getLastName() + " Agent ID " + agentId + " from organization " + agentFromDB.getPartnerId()
								+ " was logged out of the system automatically as he is idle on the system.";
					}
					saveUserNotices(supervisor, notice);
					sendNotificationEmail(supervisor, notice);
				}
				
			}
		}
		return null;
	}
	
	private void sendNotificationEmail(String supervisor, String notice) {
		// Login loginUser = XtaasUserUtils.getCurrentLoggedInUserObject();
		Login loginUser = loginRepository.findOneById(supervisor);
		if (loginUser != null) {
			List<String> reportingEmail = loginUser.getReportEmails();
			if (reportingEmail != null && reportingEmail.size() > 0) {
				for (String email : reportingEmail) {
					XtaasEmailUtils.sendEmail(email, "data@xtaascorp.com", "Agent Inactivity", notice);
				}
			} else {
				if (loginUser.getEmail() != null && !loginUser.getEmail().isEmpty()) {
					XtaasEmailUtils.sendEmail(loginUser.getEmail(), "data@xtaascorp.com", "Agent Inactivity", notice);
				}
			}
			XtaasEmailUtils.sendEmail("supportteam@xtaascorp.com", "data@xtaascorp.com", "Agent Inactivity", notice);
		}
	}
}