/**
 * 
 */
package com.xtaas.domain.service;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtaas.db.repository.CampaignRepository;
import com.xtaas.domain.entity.Agent;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentCalendar;
import com.xtaas.domain.valueobject.CampaignResource;
import com.xtaas.domain.valueobject.WorkSchedule;
import com.xtaas.domain.valueobject.WorkSlot;

/**
 * @author djain
 *
 */
@Service
public class AgentCalendarServiceImpl implements AgentCalendarService {
	
	@Autowired
	private CampaignRepository campaignRepository;

	/**
	 * Prepare calendar for num of days from current date 
	 */
	@Override
	public Map<LocalDate, TreeSet<WorkSlot>> getCalendar(int numOfDays, Agent agent) {
		//build Calendar for Agent
		LocalDate startDate = new LocalDate();
		LocalDate endDate = startDate.plusDays(numOfDays);
		
		return getCalendar(startDate, endDate, agent);
	}
	
	/**
	 * Prepare calendar for date range specified 
	 */
	@Override
	public Map<LocalDate, TreeSet<WorkSlot>> getCalendar(LocalDate startDate, LocalDate endDate, Agent agent) {
		//build Calendar for Agent
		AgentCalendar agentCalendar = new AgentCalendar(startDate, endDate, agent.getId());
		for (WorkSchedule ws : agent.getWorkSchedules()) {
			agentCalendar.setAvailableSchedule(ws);
		}
				
		//find out the campaigns to which this Agent is allocated
		List<Campaign> allocatedToCampaigns = campaignRepository.findByAllocatedAgent(agent.getId(), startDate.toDate(), endDate.toDate());
		for (Campaign campaign : allocatedToCampaigns) {
			CampaignResource campaignResource = campaign.getTeam().getCampaignResource(agent.getId()); //find out the current agent and mark the allocated schedule as busy in Agent Calendar
			for (WorkSchedule ws : campaignResource.getWorkSchedules()) {
				agentCalendar.setBusySchedule(ws, campaign.getId(), campaign.getName());
			}
		}
		return agentCalendar.getAgentCalendar();
	}

}
