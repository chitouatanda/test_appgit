package com.xtaas.domain.valueobject;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.web.dto.CampaignDTO;

public class CampaignSearchResult {
	@JsonProperty
	private long campaignCount; //total count
	
	@JsonProperty
	List<CampaignDTO> campaignsList;

	public CampaignSearchResult(long campaignCount,
			List<CampaignDTO> campaignsList) {
		this.setCampaignCount(campaignCount);
		this.campaignsList = campaignsList;
	}

	/**
	 * @return the campaignCount
	 */
	public long getCampaignCount() {
		return campaignCount;
	}

	/**
	 * @param campaignCount the campaignCount to set
	 */
	private void setCampaignCount(long campaignCount) {
		this.campaignCount = campaignCount;
	}
}
