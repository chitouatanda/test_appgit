package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.List;

public class CampaignTeam implements Cloneable {
	private String teamId;
	private String supervisorId;
	private double bid;
	private CallGuide callGuide;
	private LeadSortOrder leadSortOrder;
	private CallSpeed callSpeedPerMinPerAgent;
	private CallSpeed callSpeedRetryPerMinPerAgent;
	private float campaignSpeed;
	private List<CampaignResource> agents;
	private List<ProspectCustomSortField> prospectCustomSortFieldList;
	private List<String> supervisorIds;

	//DATE: 18/12/2017
	// Added script clone functionality
	private CampaignTeam() {}

	public CampaignTeam(String teamId, String supervisorId, double bid) {
		setTeamId(teamId);
		setSupervisorId(supervisorId);
		setBid(bid);
		agents = new ArrayList<CampaignResource>();
	}

	/**
	 * @param teamRef
	 *            the teamRef to set
	 */
	private void setTeamId(String teamId) {
		if (teamId == null || teamId.isEmpty()) {
			throw new IllegalArgumentException("Team id is required");
		}
		this.teamId = teamId;
	}

	/**
	 * @param teamRef
	 *            the teamRef to set
	 */
	private void setSupervisorId(String supervisorId) {
		if (supervisorId == null || supervisorId.isEmpty()) {
			throw new IllegalArgumentException("Supervior id is required");
		}
		this.supervisorId = supervisorId;
	}

	/**
	 * @param bid
	 *            the bid to set
	 */
	private void setBid(double bid) {
		this.bid = bid;
	}

	/**
	 * @param callGuide
	 *            the callGuide to set
	 */
	public void setCallGuide(CallGuide callGuide) {
		this.callGuide = callGuide;
	}

	/**
	 * @param leadSortOrder
	 *            the leadSortOrder to set
	 */
	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}

	/**
	 * @param callSpeedPerMinPerAgent the callSpeedPerMinPerAgent to set
	 */
	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public void setCallSpeedRetryPerMinPerAgent(CallSpeed callSpeedRetryPerMinPerAgent) {
		this.callSpeedRetryPerMinPerAgent = callSpeedRetryPerMinPerAgent;
	}

	public void setCampaignSpeed(float campaignSpeed) {
		this.campaignSpeed = campaignSpeed;
	}
	/**
	 * @param agents
	 *            the agents to set
	 */
	public void allocateAgent(String agentId) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent != null) {
			throw new IllegalArgumentException("Agent already part of team");
		} else {
			agents.add(new CampaignResource(agentId));
		}
	}

	public void deallocateAgent(String agentId) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agents.remove(agent);
		}
	}

	public void addWorkSchedule(String agentId, WorkSchedule workSchedule) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent == null) {
			allocateAgent(agentId);
			agent = getCampaignResource(agentId);
		}
		agent.addWorkSchedule(workSchedule);
	}

	public WorkSchedule getWorkSchedule(String agentId, String scheduleId) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			return agent.getWorkSchedule(scheduleId);
		}
	}

	public void updateWorkSchedule(String agentId, String scheduleId, WorkSchedule workSchedule) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.updateWorkSchedule(scheduleId, workSchedule);
		}
	}

	public void removeWorkSchedule(String agentId, String scheduleId) {
		CampaignResource agent = getCampaignResource(agentId);
		if (agent == null) {
			throw new IllegalArgumentException("Agent is not a part of the team");
		} else {
			agent.removeWorkSchedule(scheduleId);
		}
	}

	public CampaignResource getCampaignResource(String agentId) {
		int index = agents.size();
		for (; index > 0; index--) {
			if (agents.get(index - 1).getResourceId().equals(agentId)) {
				return agents.get(index - 1);
			}
		}
		return null;
	}

	/**
	 * @return the teamRef
	 */
	public String getTeamId() {
		return teamId;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	/**
	 * @return the bid
	 */
	public double getBid() {
		return bid;
	}

	/**
	 * @return the callGuide
	 */
	public CallGuide getCallGuide() {
		return callGuide;
	}

	/**
	 * @return the leadSortOrder
	 */
	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}


	/**
	 * @return the callSpeedPerMinPerAgent
	 */
	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}

	/**
	 * @return the callSpeedPerMinPerAgent
	 */
	public CallSpeed getCallSpeedRetryPerMinPerAgent() {
		return callSpeedRetryPerMinPerAgent;
	}

	public float getCampaignSpeed(){
		return campaignSpeed;
	}


	/**
	 * @return the agents
	 */
	public List<CampaignResource> getAgents() {
		return agents;
	}

	/**
	 * @return the prospectCustomSortFieldList
	 */
	public List<ProspectCustomSortField> getProspectCustomSortFieldList() {
		return prospectCustomSortFieldList;
	}

	/**
	 * @param prospectCustomSortFieldList the prospectCustomSortFieldList to set
	 */
	public void setProspectCustomSortFieldList(
			List<ProspectCustomSortField> prospectCustomSortFieldList) {
		this.prospectCustomSortFieldList = prospectCustomSortFieldList;
	}

	public List<String> getSupervisorIds() {
		return supervisorIds;
	}

	public void setSupervisorIds(List<String> supervisorIds) {
		this.supervisorIds = supervisorIds;
	}
}