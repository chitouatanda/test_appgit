package com.xtaas.domain.valueobject;

public class CountryDetails {
	public String countryName;
	public String countryCode;
	public int isdCode;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}

	@Override
	public String toString() {
		return "CountryDetails [countryName=" + countryName + ", countryCode=" + countryCode + ", isdCode=" + isdCode
				+ "]";
	}
}
