package com.xtaas.domain.valueobject;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class TeamSearchResult {
	@Id
	private String id;
	private String teamId;
	private String name;
	private String overview;
	private Integer teamSize;
	private Double perLeadPrice;
	private Integer totalCompletedCampaigns;
	private Double averageRating;
	private Integer totalVolume;
	private Double averageConversion;
	private Double averageQuality;
	private Integer averageAvailableHours;
	private Date invitationDate;

	public String getTeamId() {
		return teamId == null ? id : teamId; // TODO::Raghava, This is a hack,
												// change the find query to
												// aggregate instead
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}

	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}

	/**
	 * @return the teamSize
	 */
	public Integer getTeamSize() {
		return teamSize;
	}

	/**
	 * @param teamSize the teamSize to set
	 */
	public void setTeamSize(Integer teamSize) {
		this.teamSize = teamSize;
	}

	/**
	 * @return the perLeadPrice
	 */
	public Double getPerLeadPrice() {
		return perLeadPrice;
	}

	/**
	 * @param perLeadPrice the perLeadPrice to set
	 */
	public void setPerLeadPrice(Double perLeadPrice) {
		this.perLeadPrice = perLeadPrice;
	}

	/**
	 * @return the totalCompletedCampaigns
	 */
	public Integer getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}

	/**
	 * @param totalCompletedCampaigns the totalCompletedCampaigns to set
	 */
	public void setTotalCompletedCampaigns(Integer totalCompletedCampaigns) {
		this.totalCompletedCampaigns = totalCompletedCampaigns;
	}

	/**
	 * @return the averageRating
	 */
	public Double getAverageRating() {
		return averageRating;
	}

	/**
	 * @param averageRating the averageRating to set
	 */
	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	/**
	 * @return the totalVolume
	 */
	public Integer getTotalVolume() {
		return totalVolume;
	}

	/**
	 * @param totalVolume the totalVolume to set
	 */
	public void setTotalVolume(Integer totalVolume) {
		this.totalVolume = totalVolume;
	}

	/**
	 * @return the averageConversion
	 */
	public Double getAverageConversion() {
		return averageConversion;
	}

	/**
	 * @param averageConversion the averageConversion to set
	 */
	public void setAverageConversion(Double averageConversion) {
		this.averageConversion = averageConversion;
	}

	/**
	 * @return the averageQuality
	 */
	public Double getAverageQuality() {
		return averageQuality;
	}

	/**
	 * @param averageQuality the averageQuality to set
	 */
	public void setAverageQuality(Double averageQuality) {
		this.averageQuality = averageQuality;
	}

	/**
	 * @return the averageAvailableHours
	 */
	public Integer getAverageAvailableHours() {
		return averageAvailableHours;
	}

	/**
	 * @param averageAvailableHours the averageAvailableHours to set
	 */
	public void setAverageAvailableHours(Integer averageAvailableHours) {
		this.averageAvailableHours = averageAvailableHours;
	}

	/**
	 * @return the invitationDate
	 */
	public Date getInvitationDate() {
		return invitationDate;
	}

	/**
	 * @param invitationDate the invitationDate to set
	 */
	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	/**
	 * @param teamId the teamId to set
	 */
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	
}
