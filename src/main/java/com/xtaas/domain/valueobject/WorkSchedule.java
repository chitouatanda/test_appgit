package com.xtaas.domain.valueobject;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkSchedule {
	@JsonProperty
	private String scheduleId;
	@JsonProperty
	private Date scheduleDate;
	@JsonProperty
	private int startHour;
	@JsonProperty
	private int endHour;
	@JsonProperty
	private ScheduleRecurrence recurrence;
	
	public WorkSchedule() {
		
	}

	public WorkSchedule(Date scheduleDate, int startHour, int endHour, ScheduleRecurrence recurrence) {
		scheduleId = UUID.randomUUID().toString();
		setScheduleDate(scheduleDate);
		setTimeSlot(startHour, endHour);;
		setRecurrence(recurrence);
	}

	private void setScheduleDate(Date scheduleDate) {
		if (scheduleDate == null) {
			throw new IllegalArgumentException("Schedule date of work schedule is required");
		}
		this.scheduleDate = scheduleDate;
	}

	private void setTimeSlot(int startHour, int endHour) {
		if (endHour > 23 || startHour < 0) {
			throw new IllegalArgumentException("Start hour cannot be less than 0 and end hour cannot be more than 23");
		} else if (endHour <= startHour) {
			throw new IllegalArgumentException("Start hour should be less than end hour");
		}
		this.startHour = startHour;
		this.endHour = endHour;
	}
	private void setRecurrence(ScheduleRecurrence recurrence) {
		this.recurrence = recurrence;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public int getStartHour() {
		return startHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public ScheduleRecurrence getRecurrence() {
		return recurrence;
	}

}
