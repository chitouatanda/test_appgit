package com.xtaas.domain.valueobject;

import java.util.List;

public class EntityHelper {
	public static <T> void addItem(List<T> collection, T item) {
		if (item == null) {
			throw new IllegalArgumentException("Item to be added cannot be null");
		}
		int index = collection.size();
		for (; index > 0; index--) {
			if (collection.get(index - 1).equals(item)) {
				throw new IllegalArgumentException("Item already present in collection");
			}
		}
		collection.add(item);
	}
}
