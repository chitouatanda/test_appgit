package com.xtaas.domain.valueobject;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignTeamDTO {
	@JsonProperty
	private String teamId;
	@JsonProperty
	private double bid;
	@JsonProperty
	private LeadSortOrder leadSortOrder;
	@JsonProperty
	private CallSpeed callSpeedPerMinPerAgent;
	@JsonProperty
	private CallSpeed callSpeedRetryPerMinPerAgent;
	@JsonProperty
	private String supervisorId;
	@JsonProperty
	private List<String> supervisorIds;
	@JsonProperty
	private float campaignSpeed;
	
	public CampaignTeamDTO() {
	}
	
	public CampaignTeamDTO(CampaignTeam campaignTeam) {
		this.teamId = campaignTeam.getTeamId();
		this.bid = campaignTeam.getBid();
		this.leadSortOrder = campaignTeam.getLeadSortOrder();
		this.callSpeedPerMinPerAgent = campaignTeam.getCallSpeedPerMinPerAgent();
		this.callSpeedRetryPerMinPerAgent = campaignTeam.getCallSpeedRetryPerMinPerAgent();
		this.campaignSpeed = campaignTeam.getCampaignSpeed();
	}

	public String getTeamId() {
		return teamId;
	}

	public double getBid() {
		return bid;
	}

	public LeadSortOrder getLeadSortOrder() {
		return leadSortOrder;
	}

	public CallSpeed getCallSpeedPerMinPerAgent() {
		return callSpeedPerMinPerAgent;
	}
	
	public CallSpeed getCallSpeedRetryPerMinPerAgent() {
		return callSpeedRetryPerMinPerAgent;
	}

	public List<String> getSupervisorIds() {
		return supervisorIds;
	}

	public void setSupervisorIds(List<String> supervisorIds) {
		this.supervisorIds = supervisorIds;
	}

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

	public void setCallSpeedPerMinPerAgent(CallSpeed callSpeedPerMinPerAgent) {
		this.callSpeedPerMinPerAgent = callSpeedPerMinPerAgent;
	}

	public float getCampaignSpeed() {
		return campaignSpeed;
	}

	public void setCampaignSpeed(float campaignSpeed) {
		this.campaignSpeed = campaignSpeed;
	}

	public void setLeadSortOrder(LeadSortOrder leadSortOrder) {
		this.leadSortOrder = leadSortOrder;
	}
    
}
