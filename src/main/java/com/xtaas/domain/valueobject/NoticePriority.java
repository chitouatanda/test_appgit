package com.xtaas.domain.valueobject;

public enum NoticePriority {
	
	HIGH, MEDIUM, LOW

}
