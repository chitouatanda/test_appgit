package com.xtaas.domain.valueobject;

public enum RealTimePost {
	AGENTREALTIMEPOST, QAREALTIMEPOST, SECQAREALTIMEPOST, MANUAL;

	@Override
	public String toString() {
		switch (this) {
			case AGENTREALTIMEPOST:
				return "AGENT REAL TIME POST";
			case QAREALTIMEPOST:
				return "QA REAL TIME POST";
			case SECQAREALTIMEPOST:
				return "SECONDARY QA REAL TIME POST";
			case MANUAL:
				return "MANUAL";
			default:
				throw new IllegalArgumentException();
		}
	}
}
