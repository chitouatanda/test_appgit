package com.xtaas.domain.valueobject;

import java.util.Date;

public class CampaignAgentInvitation {
	private String agentId;
	private double maximumPayment;
	private int minimumHours;
	private Date invitationDate;
	
	public CampaignAgentInvitation(String agentId, double maximumPayment, int minimumHours) {
		setInvitationDate(new Date());
		setAgentId(agentId);
		setMaximumPayment(maximumPayment);
		setMinimumHours(minimumHours);
	}
	
	private void setAgentId(String agentId) {
		if (agentId == null || agentId.isEmpty()) {
			throw new IllegalArgumentException("Agent id is required");
		}
		this.agentId = agentId;
	}
	
	private void setMaximumPayment(double maximumPayment) {
		if (maximumPayment < 0) {
			throw new IllegalArgumentException("Maximum payment should not be a negitive number");
		}
		this.maximumPayment = maximumPayment;
	}

	private void setMinimumHours(int minimumHours) {
		if (minimumHours <= 0) {
			throw new IllegalArgumentException("Minimum hours should be a positive number");
		}
		this.minimumHours = minimumHours;
	}

	private void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	public String getAgentId() {
		return agentId;
	}
	
	public double getMaximumPayment() {
		return maximumPayment;
	}

	public int getMinimumHours() {
		return minimumHours;
	}

	public Date getInvitationDate() {
		return invitationDate;
	}
}
