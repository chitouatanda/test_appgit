package com.xtaas.domain.valueobject;

public enum DialerCodeDispositionStatus {

	BUSY, NOANSWER, ANSWERMACHINE, DEADAIR, FAXMACHINE, GATEKEEPER_ANSWERMACHINE, DIALERMISDETECT, ABANDONED, UNKNOWN, CALL_LATER, AUTO;
	
	@Override
    public String toString() {
		switch(this) {
	        case BUSY: return "Busy";
	        case NOANSWER: return "No Answer";
	        case ANSWERMACHINE: return "Answering Machine";
	        case DEADAIR: return "Dead Air";
	        case FAXMACHINE: return "Fax Machine";
	        case GATEKEEPER_ANSWERMACHINE: return "Gate Keeper - Answering Machine";
	        case DIALERMISDETECT: return "Dialer Misdetect";
	        case ABANDONED: return "Abandoned";
	        case UNKNOWN: return "Unknown";
			case CALL_LATER: return "Call Later";
			case AUTO: return "AUTO";
	        default: throw new IllegalArgumentException("Invalid DialerCodeDispositionStatus: " + this);
		}
    }
}
