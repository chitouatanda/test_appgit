package com.xtaas.domain.valueobject;

public enum SuccessDispositionStatus {

	SUBMITLEAD; 
	
	@Override
    public String toString() {
		switch(this) {
        	//case VERBALCOMPLETION: return "Verbal Completion";
        	//case NUMBERCOMPLETION: return "Number Completion";
        	case SUBMITLEAD: return "Submit Lead";
        	default: throw new IllegalArgumentException();
		}
    }
}
