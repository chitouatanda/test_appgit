package com.xtaas.domain.valueobject;

import java.util.List;

public class FakeAddressGroup {
	private String country;
	private String countryCode;
	private String state;
	private String city;
	private String postalCode;
	private int count;
	private List<RemainingData> remainingList;

	public String getCountry() {
		return country;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getState() {
		return state;
	}

	public String getCity() {
		return city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public int getCount() {
		return remainingList.size();
	}

	public List<RemainingData> getRemainingList() {
		return remainingList;
	}
	
	public RemainingData popRemainingData() {
		RemainingData data = remainingList.get(remainingList.size() - 1);
		remainingList.remove(remainingList.size() - 1);
		return data;
	}

	public class RemainingData {
		private String name;
		private String line1;

		public String getName() {
			return name;
		}

		public String getLine1() {
			return line1;
		}

	}
}
