package com.xtaas.domain.valueobject;

public class SendGirdEmailEvents {

	boolean open;
	boolean click;
	boolean unsubscribe;
	boolean bounce;
	boolean spam_report;
	boolean processed;
	boolean delivered;
	boolean deferred;
	boolean dropped;
	
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public boolean isClick() {
		return click;
	}
	public void setClick(boolean click) {
		this.click = click;
	}
	public boolean isUnsubscribe() {
		return unsubscribe;
	}
	public void setUnsubscribe(boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}
	public boolean isBounce() {
		return bounce;
	}
	public void setBounce(boolean bounce) {
		this.bounce = bounce;
	}
	public boolean isSpam_report() {
		return spam_report;
	}
	public void setSpam_report(boolean spam_report) {
		this.spam_report = spam_report;
	}
	public boolean isProcessed() {
		return processed;
	}
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
	public boolean isDelivered() {
		return delivered;
	}
	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}
	public boolean isDeferred() {
		return deferred;
	}
	public void setDeferred(boolean deferred) {
		this.deferred = deferred;
	}
	public boolean isDropped() {
		return dropped;
	}
	public void setDropped(boolean dropped) {
		this.dropped = dropped;
	}
	@Override
	public String toString() {
		return "SendGirdEmailEvents [open=" + open + ", click=" + click + ", unsubscribe=" + unsubscribe + ", bounce="
				+ bounce + ", spam_report=" + spam_report + ", processed=" + processed + ", delivered=" + delivered
				+ ", deferred=" + deferred + ", dropped=" + dropped + "]";
	}
}
