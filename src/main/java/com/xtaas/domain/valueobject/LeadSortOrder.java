package com.xtaas.domain.valueobject;

public enum LeadSortOrder {
		FIFO, LIFO, CUSTOM, QUALITY_LIFO, QUALITY_FIFO, RETRY_LIFO, RETRY_FIFO
	}