package com.xtaas.domain.valueobject;

public enum TeamStatus {
	ACTIVE, INACTIVE;
}
