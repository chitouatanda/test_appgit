package com.xtaas.domain.valueobject;

public enum DayOfWeek {
	Mon, Tue, Wed, Thu, Fri, Sat, Sun
}
