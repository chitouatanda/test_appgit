package com.xtaas.domain.valueobject;

public enum CallbackDispositionStatus {
	CALLBACK, CALLBACK_SYSTEM_GENERATED, CALLBACK_USER_GENERATED, CALLBACK_GATEKEEPER;
	
	@Override
    public String toString() {
		switch(this) {
	        case CALLBACK_SYSTEM_GENERATED: return "Callback - System Generated";
	        case CALLBACK_USER_GENERATED: return "Callback";
	        case CALLBACK_GATEKEEPER: return "Callback - Gate Keeper";
			case CALLBACK: return "CALLBACK";
	        default: throw new IllegalArgumentException();
		}
    }
}
