/**
 * 
 */
package com.xtaas.domain.valueobject;

import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.Range;

/**
 * A time slot allocated for work in agent's calendar on a specific day. Hours are specified in UTC timezone. 
 * 
 * @author djain
 *
 */
public class WorkSlot implements Comparable<WorkSlot> {
	
	private Range hourRange;
	private String workUnitIdfier; //identifier for the workunit (ex: campaignId for now)
	private String workUnitName; //name for the workunit (ex: campaign name for now)
	private String workScheduleId; //reference to scheduleId due to which this slot exist
	private String recurrenceDescription; //text description for work schedule's recurrence
	
	/**
	 * 
	 * @param startHour Hours in 0-23 range  
	 * @param endHour Hours in 0-23 range 
	 */
	public WorkSlot (int startHour, int endHour) {
		if (startHour < 0 || startHour > 23 || endHour < 0 || endHour > 23) {
			throw new IllegalArgumentException("Hours must be in the range of 0-23");
		} else if (endHour <= startHour) {
			throw new IllegalArgumentException("Start hour should be less than end hour");
		}
		
		this.hourRange = new IntRange(startHour, endHour);
	}
	
	public int getStartHour() {
		return this.hourRange.getMinimumInteger();
	}
	
	public int getEndHour() {
		return this.hourRange.getMaximumInteger();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WorkSlot [hourRange=");
		builder.append(hourRange);
		builder.append(", workUnitIdfier=");
		builder.append(workUnitIdfier);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the workUnitIdfier
	 */
	public String getWorkUnitIdfier() {
		return workUnitIdfier;
	}

	/**
	 * @param workUnitIdfier the workUnitIdfier to set
	 */
	public void setWorkUnitIdfier(String workUnitIdfier) {
		this.workUnitIdfier = workUnitIdfier;
	}
	
	public int getLength() {
		return this.hourRange.getMaximumInteger() - this.hourRange.getMinimumInteger();
	}
	
	/**
	 * Check if this Slot contains the other slot.
	 * @param otherSlot
	 * @return
	 */
	public boolean contains(WorkSlot otherSlot) {
		return this.hourRange.containsRange(otherSlot.hourRange);
	}
	
	public boolean containsHour(int hour) {
		return this.hourRange.containsInteger(hour);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hourRange == null) ? 0 : hourRange.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkSlot other = (WorkSlot) obj;
		if (hourRange == null) {
			if (other.hourRange != null)
				return false;
		} else if (!hourRange.equals(other.hourRange))
			return false;
		return true;
	}

	@Override
	public int compareTo(WorkSlot otherSlot) {
		if (this.equals(otherSlot)) {
			return 0;
		} else if (this.hourRange.getMinimumInteger() == otherSlot.hourRange.getMinimumInteger()) { //same start hour but difft end hour then slot with earlier end hour is small
			return this.hourRange.getMaximumInteger() > otherSlot.hourRange.getMaximumInteger() ? 1 : -1;
		} else { //comparison based on Slot's start hour. Earliest start hour is lowest
			return this.hourRange.getMinimumInteger() > otherSlot.hourRange.getMinimumInteger() ? 1 : -1; 
		}
	}

	public boolean isAvailable() {
		return (this.workUnitIdfier == null);
	}

	public void setWorkUnitName(String workUnitName) {
		this.workUnitName = workUnitName;
	}

	public void setWorkScheduleId(String scheduleId) {
		this.workScheduleId = scheduleId;
	}

	public void setRecurrenceDescription(String recurrenceDescription) {
		this.recurrenceDescription = recurrenceDescription;
	}

	/**
	 * @return the workUnitName
	 */
	public String getWorkUnitName() {
		return workUnitName;
	}

	/**
	 * @return the workScheduleId
	 */
	public String getWorkScheduleId() {
		return workScheduleId;
	}

	/**
	 * @return the recurrenceDescription
	 */
	public String getRecurrenceDescription() {
		return recurrenceDescription;
	}
}
