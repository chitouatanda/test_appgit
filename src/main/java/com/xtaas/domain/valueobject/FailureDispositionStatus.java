package com.xtaas.domain.valueobject;

public enum FailureDispositionStatus {

	BADNUMBER, DNCL, DNRM, FAKENUMBER, LANGUAGEBARRIER, NOTINTERESTED, UNSERVICEABLE, OTHER, NO_LONGER_EMPLOYED, NO_CONSENT, PROSPECT_UNREACHABLE, FAILED_QUALIFICATION, OUT_OF_COUNTRY, INVALID_DATA;
	
	@Override
    public String toString() {
		switch(this) {
	        case BADNUMBER: return "Bad Number";
	        case DNCL: return "Do Not Call List";
	        case DNRM: return "Do Not Record Me";
	        case FAKENUMBER: return "Fake Number";
	        case NO_LONGER_EMPLOYED: return "No Longer Employed";
	        case LANGUAGEBARRIER: return "Language Barrier";
	        case NOTINTERESTED: return "Not Interested";
	        case UNSERVICEABLE: return "Unserviceable";
	        case NO_CONSENT: return "No Consent";
	        case OTHER: return "Other";
	        case PROSPECT_UNREACHABLE: return "Prospect Unreachable";
	        case FAILED_QUALIFICATION: return "Failed Qualification";
	        case OUT_OF_COUNTRY: return "Out of Country";
			case INVALID_DATA: return "Data Invalid";
	        default: throw new IllegalArgumentException();
		}
    }
}
