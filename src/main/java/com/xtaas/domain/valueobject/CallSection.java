package com.xtaas.domain.valueobject;

import java.util.List;

public class CallSection {
	private String script;
	private List<Rebuttal> rebuttals;
	
	/**
	 * @return the script
	 */
	public String getScript() {
		return script;
	}
	/**
	 * @param script the script to set
	 */
	public void setScript(String script) {
		if (script != null && script.length() > 10000) 
			throw new IllegalArgumentException("Script cannot be more than 10000 characters");
		this.script = script;
	}
	/**
	 * @return the rebuttals
	 */
	public List<Rebuttal> getRebuttals() {
		return rebuttals;
	}
	/**
	 * @param rebuttals the rebuttals to set
	 */
	public void setRebuttals(List<Rebuttal> rebuttals) {
		if (rebuttals != null && rebuttals.size() > 12) {
			throw new IllegalArgumentException("Max 12 rebuttals can be added to a section");
		}
		this.rebuttals = rebuttals;
	}
}
