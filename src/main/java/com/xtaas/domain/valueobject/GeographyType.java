package com.xtaas.domain.valueobject;

public enum GeographyType {
	AREA, COUNTRY, STATE, CITY
}
