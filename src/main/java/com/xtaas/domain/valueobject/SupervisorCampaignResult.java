package com.xtaas.domain.valueobject;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.web.vm.SupervisorCampaignsVM;

public class SupervisorCampaignResult {
	@JsonProperty
	private long campaignCount;

	@JsonProperty
	List<SupervisorCampaignsVM> campaignsList;
	
	@JsonProperty
	private boolean prospectUploadEnabled;

	public SupervisorCampaignResult(long campaignCount, List<SupervisorCampaignsVM> campaignsList,boolean prospectUploadEnabled) {
		this.setCampaignCount(campaignCount);
		this.campaignsList = campaignsList;
		this.prospectUploadEnabled = prospectUploadEnabled;
	}

	/**
	 * @return the campaignCount
	 */
	public long getCampaignCount() {
		return campaignCount;
	}

	/**
	 * @param campaignCount
	 *            the campaignCount to set
	 */
	private void setCampaignCount(long campaignCount) {
		this.campaignCount = campaignCount;
	}

	public boolean isProspectUploadEnabled() {
		return prospectUploadEnabled;
	}

	public void setProspectUploadEnabled(boolean prospectUploadEnabled) {
		this.prospectUploadEnabled = prospectUploadEnabled;
	}
}
