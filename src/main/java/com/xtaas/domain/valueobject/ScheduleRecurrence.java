package com.xtaas.domain.valueobject;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScheduleRecurrence {
	@JsonProperty
	private Date startDate;
	@JsonProperty
	private Date endDate;
	@JsonProperty
	private RecurrenceTypes type;
	@JsonProperty
	private List<DayOfWeek> days;
	
	public ScheduleRecurrence() {
		
	}

	public ScheduleRecurrence(Date startDate, Date endDate, RecurrenceTypes type, List<DayOfWeek> days) {
		setStartDate(startDate);
		setEndDate(endDate);
		setRecurrenceAndDays(type, days);
	}

	private void setStartDate(Date startDate) {
		if (startDate == null) {
			throw new IllegalArgumentException("Start date of schedule recurrence is required");
		}
		this.startDate = startDate;
	}

	private void setEndDate(Date endDate) {
		if (endDate == null) {
			throw new IllegalArgumentException("End date of schedule recurrence is required");
		}
		this.endDate = endDate;
	}

	private void setRecurrenceAndDays(RecurrenceTypes type, List<DayOfWeek> days) {
		if (type == RecurrenceTypes.Weekly) {
			if (days == null || days.size() == 0) {
				throw new IllegalArgumentException("At least one day is required for for Weekly recurrance");
			}
			this.days = days;
		}
		this.type = type;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public RecurrenceTypes getType() {
		return type;
	}

	public List<DayOfWeek> getDays() {
		return days;
	}

}
