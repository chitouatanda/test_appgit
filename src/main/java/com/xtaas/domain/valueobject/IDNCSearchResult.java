package com.xtaas.domain.valueobject;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.web.dto.IDNCNumberDTO;

public class IDNCSearchResult {
	@SuppressWarnings("unused")
	@JsonProperty
	private long dncNumberCount;
	
	@JsonProperty
	List<IDNCNumberDTO> dncNumbers;

	public IDNCSearchResult(long dncNumberCount, List<IDNCNumberDTO> dncNumbers) {
		this.dncNumberCount = dncNumberCount;
		this.dncNumbers = dncNumbers;
	}
}
