package com.xtaas.domain.valueobject;

public enum DispositionType {

	SUCCESS,
	DIALERCODE,
	FAILURE, 
	CALLBACK;
	
	@Override
    public String toString() {
		switch(this) {
        	case SUCCESS: return "Success";
        	case DIALERCODE: return "Dialer Code";
        	case FAILURE: return "Failure";
        	case CALLBACK: return "Callback";
        	default: throw new IllegalArgumentException();
		}
    }
}
