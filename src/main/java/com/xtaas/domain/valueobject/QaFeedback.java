package com.xtaas.domain.valueobject;

import java.util.Date;
import java.util.List;


public class QaFeedback {
	private String qaId;
	private String secQaId;
	private List<FeedbackSectionResponse> feedbackResponseList;
	private Double overallScore;
	private Date feedbackTime;
	private List<AgentQaAnswer> qaAnswers;
	private String agentNotes;
	/*@JsonProperty
	private HashMap<String, String> isAnswerValid;
	@JsonProperty
	private HashMap<String, String> answerCertification;*/

	/**
	 * @param qaId
	 * @param feedbackResponseList
	 * @param overallScore
	 * @param feedbackTime
	 */
	public QaFeedback(String qaId, String secQaId, List<FeedbackSectionResponse> feedbackResponseList, Double overallScore,
			Date feedbackTime, List<AgentQaAnswer> qaAnswers, String agentNotes/*, HashMap<String, String> isAnswerValid,
			HashMap<String, String> answerCertification*/) {
		this.qaId = qaId;
		this.secQaId = secQaId;
		this.feedbackResponseList = feedbackResponseList;
		this.overallScore = overallScore;
		this.feedbackTime = feedbackTime;
		/*this.isAnswerValid = isAnswerValid;
		this.answerCertification = answerCertification;*/
		this.qaAnswers = qaAnswers;
		this.agentNotes = agentNotes;
	}

	/**
	 * @return the feedbackResponseList
	 */
	public List<FeedbackSectionResponse> getFeedbackResponseList() {
		return feedbackResponseList;
	}

	/**
	 * @param feedbackResponseList
	 *            the feedbackResponseList to set
	 */
	public void setFeedbackResponseList(List<FeedbackSectionResponse> feedbackResponseList) {
		this.feedbackResponseList = feedbackResponseList;
	}

	/**
	 * @return the qaId
	 */
	public String getQaId() {
		return qaId;
	}

	/**
	 * @param qaId
	 *            the qaId to set
	 */
	public void setQaId(String qaId) {
		this.qaId = qaId;
	}
	
	/**
	 * @return the secQaId
	 */
	public String getSecQaId() {
		return secQaId;
	}
	
	/**
	 * @param secQaId
	 *            the secQaId to set
	 */
	public void setSecQaId(String secQaId) {
		this.secQaId = secQaId;
	}

	/**
	 * @return the overallScore
	 */
	public Double getOverallScore() {
		return overallScore;
	}

	/**
	 * @param overallScore
	 *            the overallScore to set
	 */
	public void setOverallScore(Double overallScore) {
		this.overallScore = overallScore;
	}

	/**
	 * @return the feedbackTime
	 */
	public Date getFeedbackTime() {
		return feedbackTime;
	}

	/**
	 * @param feedbackTime
	 *            the feedbackTime to set
	 */
	public void setFeedbackTime(Date feedbackTime) {
		this.feedbackTime = feedbackTime;
	}

/*	public HashMap<String, String> getIsAnswerValid() {
		return isAnswerValid;
	}

	public void setIsAnswerValid(HashMap<String, String> isAnswerValid) {
		this.isAnswerValid = isAnswerValid;
	}

	public HashMap<String, String> getAnswerCertification() {
		return answerCertification;
	}

	public void setAnswerCertification(HashMap<String, String> answerCertification) {
		this.answerCertification = answerCertification;
	}*/

	public List<AgentQaAnswer> getQaAnswers() {
		return qaAnswers;
	}

	public void setQaAnswers(List<AgentQaAnswer> qaAnswers) {
		this.qaAnswers = qaAnswers;
	}

	public String getAgentNotes() {
		return agentNotes;
	}

	public void setAgentNotes(String agentNotes) {
		this.agentNotes = agentNotes;
	}
}
