package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.List;

public class PurchaseTransaction {
	
	public static enum TransactionResult {
		PURCHASED
	}
	
	private List<String> listProviderUsageIds;
	private String providerName;
	private int purchaseCount;
	private TransactionResult transactionResult;
	
	/**
	 * @param usageIds
	 * @param providerName
	 * @param purchaseCount
	 */
	public PurchaseTransaction(String providerName) {
		this.setProviderName(providerName);
		listProviderUsageIds = new ArrayList<String>();
	}
	/**
	 * @return the usageIds
	 */
	public List<String> getUsageIds() {
		return listProviderUsageIds;
	}
	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}
	/**
	 * @param providerName the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	/**
	 * @return the purchaseCount
	 */
	public int getPurchaseCount() {
		return purchaseCount;
	}
	/**
	 * @return the transactionResult
	 */
	public TransactionResult getTransactionResult() {
		return transactionResult;
	}
	/**
	 * @param transactionResult the transactionResult to set
	 */
	public void setTransactionResult(TransactionResult transactionResult) {
		this.transactionResult = transactionResult;
	}
	/**
	 * @param purchaseCount the purchaseCount to set
	 */
	public void setPurchaseCount(int purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	
	public void addUsageId(String usageId) {
		listProviderUsageIds.add(usageId);
	}
	
}
