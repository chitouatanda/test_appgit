/**
 * 
 */
package com.xtaas.domain.valueobject;

import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.data.annotation.PersistenceConstructor;

/**
 * @author djain
 *
 */
public class EmailMessage {
	private String subject;
	private String message;
	private List<String> to; //optional
	private String fromEmail; //optional
	private String fromName; //optional
	private String replyTo; //optional
	private String clickUrl;
	private String unSubscribeUrl;
		
	private EmailMessage() {}
	
	//Constructor to create Email Message with the receivers
	public EmailMessage(List<String> to, String subject, String message) {
		this(subject, message);
		setTo(to);
	}
	
	//Constructor to create Email Message Template
	@PersistenceConstructor
	public EmailMessage(String subject, String message) {
		setSubject(subject);
		setMessage(message);
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(List<String> to) {
		EmailValidator emailValidator = EmailValidator.getInstance();
		if (to != null && !to.isEmpty()) {
			for (String toEmail : to) {
				if (!emailValidator.isValid(toEmail)) {
					throw new IllegalArgumentException(toEmail + " must be a valid Email address");
				}
			}
		}
		this.to = to;
	}

	/**
	 * @param from the from to set
	 */
	public void setFromEmail(String fromEmail) {
		EmailValidator emailValidator = EmailValidator.getInstance();
		if (fromEmail != null && !emailValidator.isValid(fromEmail)) {
			throw new IllegalArgumentException(fromEmail + " must be a valid Email address");
		}
		
		this.fromEmail = fromEmail;
	}

	/**
	 * @param subject the subject to set
	 */
	private void setSubject(String subject) {
		if (subject == null || subject.isEmpty()) {
			throw new IllegalArgumentException("Subject field is required");
		}
		
		this.subject = subject;
	}

	/**
	 * @param body the body to set
	 */
	private void setMessage(String message) {
		if (message == null || message.isEmpty()) {
			throw new IllegalArgumentException("Message field is required");
		}
		
		this.message = message;
	}

	/**
	 * @return the to
	 */
	public List<String> getTo() {
		return to;
	}

	/**
	 * @return the from
	 */
	public String getFromEmail() {
		return fromEmail;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @return the body
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the fromName
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @param fromName the fromName to set
	 */
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	/**
	 * @return the replyTo
	 */
	public String getReplyTo() {
		return replyTo;
	}

	/**
	 * @param replyTo the replyTo to set
	 */
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public String getClickUrl() {
		return clickUrl;
	}

	public void setClickUrl(String clickUrl) {
		this.clickUrl = clickUrl;
	}

	public String getUnSubscribeUrl() {
		return unSubscribeUrl;
	}

	public void setUnSubscribeUrl(String unSubscribeUrl) {
		this.unSubscribeUrl = unSubscribeUrl;
	}
	
}
