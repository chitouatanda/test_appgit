package com.xtaas.domain.valueobject;

public enum ContactStatus {
	ACTIVE, INACTIVE
}
