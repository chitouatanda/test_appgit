package com.xtaas.domain.valueobject;

import java.util.UUID;

public class Asset {
	private String assetId;
	private String name;
	private String description;
	private String url;
	private int assetTarget;
	private Boolean isRemoved;
	
	public Asset(String assetId, String name, String description, String url, Boolean isRemoved) {
	    if(assetId == null){
            this.assetId = UUID.randomUUID().toString();
        } else {
            this.assetId = assetId;
        }
        setName(name);
        setDescription(description);
        setUrl(url);
		if (isRemoved != null) {
			setIsRemoved(isRemoved);
		} else {
			isRemoved = false;
		}
	}
	
	/*public Asset(String name, String description, String url) {
	    this(null, name, description, url);
	}*/
	
	public String getAssetId() {
		return assetId;
	}

	/**
	 * @param name the name to set
	 */
	private void setName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Name is required for asset");
		}
		this.name = name;
	}

	/**
	 * @param description the description to set
	 */
	private void setDescription(String description) {
		if (description == null || description.isEmpty()) {
			throw new IllegalArgumentException("Description is required for asset");
		}
		this.description = description;
	}

	/**
	 * @param url the url to set
	 */
	private void setUrl(String url) {
		if (url == null || url.isEmpty()) {
			throw new IllegalArgumentException("URL is required for asset");
		}
		this.url = url;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	public int getAssetTarget() {
		return assetTarget;
	}

	public void setAssetTarget(int assetTarget) {
		this.assetTarget = assetTarget;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

}
