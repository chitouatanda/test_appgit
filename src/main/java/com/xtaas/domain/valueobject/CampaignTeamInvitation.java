package com.xtaas.domain.valueobject;

import java.util.Date;

public class CampaignTeamInvitation {
	private String teamId;
	private Date invitationDate;

	public CampaignTeamInvitation(String teamId) {
		setTeamId(teamId);
		setInvitationDate(new Date());
	}
	
	private void setTeamId(String teamId) {
		if (teamId == null) {
			throw new IllegalArgumentException("Team id is required");
		}
		this.teamId = teamId;
	}
	
	private void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	public String getTeamId() {
		return teamId;
	}

	public Date getInvitationDate() {
		return invitationDate;
	}
}
