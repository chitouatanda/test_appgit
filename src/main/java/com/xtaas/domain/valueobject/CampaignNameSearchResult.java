package com.xtaas.domain.valueobject;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xtaas.web.dto.CampaignDTO;

public class CampaignNameSearchResult {

	@JsonProperty
	private long campaignCount;

	@JsonProperty
	List<CampaignDTO> campaigns;

	public CampaignNameSearchResult(long campaignCount, List<CampaignDTO> campaigns) {
		super();
		this.campaignCount = campaignCount;
		this.campaigns = campaigns;
	}

}
