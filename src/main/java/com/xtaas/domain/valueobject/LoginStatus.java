package com.xtaas.domain.valueobject;

public enum LoginStatus {
	ACTIVE, INACTIVE;

	@Override
	public String toString() {
		switch (this) {
		case ACTIVE:
			return "ACTIVE";
		case INACTIVE:
			return "INACTIVE";
		default:
			throw new IllegalArgumentException("Invalid Login Status: " + this);
		}
	}

}
