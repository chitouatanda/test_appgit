package com.xtaas.domain.valueobject;

public enum QaRejectReason {
	ADDRESS, COMPANY, CONSENT, DUPLICATECOMPANY, DUPLICATELEAD, EMAIL, EMPLOYEECOUNT, EXTRALEAD, GEOGRAPHY, INDUSTRY, INTEREST, JOBTITLE, LOCATION, MULITPLEREASONS, OTHER, QUALIFICATION, QUALITYASSURANCE, REVENUE;
	
    @Override
    public String toString() {
      switch(this) {
      	case ADDRESS:
      		return "Address - Unable to Find";
      	case COMPANY:
    		return "Company Change - Mismatched PII";
      	case CONSENT:
    		return "Consent - Improper or Lacking";
    	case DUPLICATECOMPANY:
    		return "Duplicate - Company Name";
    	case DUPLICATELEAD:
    		return "Duplicate - Lead Name";
    	case EMAIL:
    		return "Email Bounce - Unresolved Still";
    	case EMPLOYEECOUNT:
    		return "Employee Count - Not Qualified";
    	case EXTRALEAD:
    		return "Extra Lead - Holding for Returns";
    	case GEOGRAPHY:
    		return "Geography - Out of State or Country ";
    	case INDUSTRY:
    		return "Industry - Not Qualified";
    	case INTEREST:
    		return "Interest - Lack of Genuine";
    	case JOBTITLE:
    		return "Job Title - Not Qualified";
    	case LOCATION:
      		return "Locations - Not Qualified";
    	case MULITPLEREASONS:
      		return "Multiple Reasons - Explain in Notes";
    	case OTHER:
    		return "Other - Explain in Notes";
    	case QUALIFICATION:
      		return "Qualification - Failed Questions";
      	case QUALITYASSURANCE:
      		return "Quality Assurance - Failed";
    	case REVENUE:
    		return "Revenue - Not Qualified";   		
        default: throw new IllegalArgumentException();
      }
    }
}