package com.xtaas.domain.valueobject;

public enum RecycleStatusEnum {
	SUCCESS,
	QUEUED,
	BUSY,
	CALL_ABANDONED,
	CALLBACK,
	NOANSWER,
	DEADAIR,
	UNKNOWN_ERROR,
	ANSWERMACHINE,
	DIALERMISDETECT,
	MAX_RETRY_LIMIT_REACHED,
	GATEKEEPER_ANSWERMACHINE,
	FAILURE;
	
    @Override
    public String toString() {
      switch(this) {
        case SUCCESS: return "Success";
        case QUEUED: return "Queued";
        case BUSY: return "Busy";
        case CALL_ABANDONED: return "Call Abandoned";
        case CALLBACK: return "Callback";
        case NOANSWER: return "No Answer";
        case DEADAIR: return "Dead Air";
        case UNKNOWN_ERROR: return "Unknown Error";
        case ANSWERMACHINE: return "Answering Machine";
        case DIALERMISDETECT: return "Dialer Misdetect";
        case MAX_RETRY_LIMIT_REACHED: return "Max Retry Limit Reached";
        case GATEKEEPER_ANSWERMACHINE: return "Gate Keeper - Answering Machine";
        case FAILURE: return "Failure";
        default: throw new IllegalArgumentException();
      }
    }
}
