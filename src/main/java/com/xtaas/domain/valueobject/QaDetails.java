package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.web.dto.AgentDTO;
import com.xtaas.web.dto.CampaignDTO;
import com.xtaas.web.dto.ProspectCallLogDTO;
import com.xtaas.web.dto.QaFeedbackFormDTO;

public class QaDetails {
	private CampaignDTO  campaign;
	private String agentName;
	private String teamName;
	private String agentProfilePic;
	private ProspectCallLogDTO prospectCallLog;
    private List<HashMap<String, String>>  callLogMap;
    private QaFeedbackFormDTO qaFeedbackForm;
    private LinkedHashMap<String, CRMAttribute> questionAttributeMap;	// which will contain all questions for particular campaign
    
    public QaDetails(AgentDTO agentDTO, CampaignDTO  campaignDTO, ProspectCallLogDTO prospectCallLogDTO, QaFeedbackFormDTO qaFeedbackFormDTO, LinkedHashMap<String, CRMAttribute> questionAttributeMap) {
    	this.setCampaign(campaignDTO);
    	this.setAgentName(agentDTO.getName());
    	this.setProspectCallLog(prospectCallLogDTO);
    	this.callLogMap = new ArrayList<HashMap<String, String>>();
    	this.setQaFeedbackForm(qaFeedbackFormDTO);
    	this.setQuestionAttributeMap(questionAttributeMap);
    }
   
    public CampaignDTO getCampaign() {
		return campaign;
	}

	public void setCampaign(CampaignDTO campaign) {
		this.campaign = campaign;
	}

	/**
	 * @return the agentProfilePic
	 */
	public String getAgentProfilePic() {
		return agentProfilePic;
	}

	/**
	 * @param agentProfilePic the agentProfilePic to set
	 */
	public void setAgentProfilePic(String agentProfilePic) {
		this.agentProfilePic = agentProfilePic;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public List<HashMap<String, String>> getCallLogMap() {
		return callLogMap;
	}

	public void setCallLogMap(List<HashMap<String, String>> callLogMap) {
		this.callLogMap = callLogMap;
	}

	public QaFeedbackFormDTO getQaFeedbackForm() {
		return qaFeedbackForm;
	}

	public void setQaFeedbackForm(QaFeedbackFormDTO qaFeedbackForm) {
		this.qaFeedbackForm = qaFeedbackForm;
	}

	/**
	 * @return the prospectCallLog
	 */
	public ProspectCallLogDTO getProspectCallLog() {
		return prospectCallLog;
	}

	/**
	 * @param prospectCallLog the prospectCallLog to set
	 */
	public void setProspectCallLog(ProspectCallLogDTO prospectCallLog) {
		this.prospectCallLog = prospectCallLog;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/* START
	 * DATE : 11/05/2017
	 * REASON : getter/setter for questionAttributeMap which will contain all questions for particular campaign */
	public LinkedHashMap<String, CRMAttribute> getQuestionAttributeMap() {
		return questionAttributeMap;
	}

	public void setQuestionAttributeMap(LinkedHashMap<String, CRMAttribute> questionAttributeMap) {
		this.questionAttributeMap = questionAttributeMap;
	}
	/* END */
	
}
