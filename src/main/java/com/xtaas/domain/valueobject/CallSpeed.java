package com.xtaas.domain.valueobject;

public enum CallSpeed {
	CONSERVATIVE(1), INTERMEDIATE(2), AGGRESSIVE(3), CUSTOM4(4), CUSTOM5(5), CUSTOM6(6), CUSTOM(5);
	
	private final int value;

    private CallSpeed(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}