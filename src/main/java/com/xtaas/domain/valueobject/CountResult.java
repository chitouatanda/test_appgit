package com.xtaas.domain.valueobject;

import org.springframework.data.annotation.Id;

public class CountResult {
	@Id
	private String id;
	private int count;

	public int getCount() {
		return count;
	}

}
