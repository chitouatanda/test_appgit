package com.xtaas.domain.valueobject;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import com.xtaas.db.entity.PickListItem;

/**
 * @author djain
 * 
 */
public class Expression {
	@NotNull
	private String attribute;
	@NotNull
	private String operator;
	@NotNull
	private String value;
	@NotNull
	private String questionSkin;
	@NotNull
	private ArrayList<PickListItem> pickList;
	
	/*START 
	 * DATE : 10/05/2017 
	 * REASON : Below property is added for handling CheckBox value on "edit campaign" page. Which will decide whether question will be shown on agent screen or not */
	@NotNull
	private Boolean agentValidationRequired;
	/*END*/
	
	private String sequenceNumber;

	public String getSequenceNumber() {
		return sequenceNumber;
	}
	

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Expression(String attribute, String operator, String value, Boolean agentValidationRequired, String questionSkin, ArrayList<PickListItem> pickList, String sequenceNumber) {
		  setAttribute(attribute);
		  setOperator(operator);
		  setValue(attribute, value);
		  setAgentValidationRequired(agentValidationRequired);
		  if(questionSkin!=null)
		   setQuestionSkin(questionSkin);
		  if(pickList!=null)
		   setPickList(pickList);
		  setSequenceNumber(sequenceNumber);
		 }
	
	/**
	 * @param questionSkin the questionSkin to set
	 */
	private void setQuestionSkin(String questionSkin) {
		if (questionSkin == null || questionSkin.isEmpty()) {
			throw new IllegalArgumentException("QuestionSkin is required");
		}
		this.questionSkin = questionSkin;
	}
	
	/**
	 * @return the questionSkin
	 */
	public String getQuestionSkin() {
		return questionSkin;
	}
	
	/**
	 * @return the pickList
	 */
	public ArrayList<PickListItem> getPickList() {
		return pickList;
	}

	/**
	 * @param pickList
	 *            the pickList to set
	 */
	public void setPickList(ArrayList<PickListItem> pickList) {
		this.pickList = pickList;
	}
	
	/**
	 * @param attribute the attribute to set
	 */
	private void setAttribute(String attribute) {
		if (attribute == null || attribute.isEmpty()) {
			throw new IllegalArgumentException("Attribute is required");
		}
		this.attribute = attribute;
	}

	/**
	 * @param operator the operator to set
	 */
	private void setOperator(String operator) {
		if (operator == null || operator.isEmpty()) {
			throw new IllegalArgumentException("Operator is required");
		}
		this.operator = operator;
	}

	/**
	 * @param value the value to set
	 */
	private void setValue(String attribute, String value) {
		if (attribute.equalsIgnoreCase("CUSTOMFIELD_01") || attribute.equalsIgnoreCase("CUSTOMFIELD_02")
				|| attribute.equalsIgnoreCase("CUSTOMFIELD_03") || attribute.equalsIgnoreCase("CUSTOMFIELD_04")
				|| attribute.equalsIgnoreCase("CUSTOMFIELD_05") || attribute.equalsIgnoreCase("CUSTOMFIELD_06")
				|| attribute.equalsIgnoreCase("CUSTOMFIELD_07") || attribute.equalsIgnoreCase("CUSTOMFIELD_08")
				|| attribute.equalsIgnoreCase("CUSTOMFIELD_09") || attribute.equalsIgnoreCase("CUSTOMFIELD_10")) {
			
		} else {
			if (value == null || value.isEmpty()) {
				 throw new IllegalArgumentException("Value is required");
			}
		}
		this.value = value;
	}

	/**
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/* START 
	 * DATE : 10/05/2017 
	 * REASON : Below property is added for handling CheckBox value on "edit campaign" page. Which will decide whether question will be shown on agent screen or not */
	public Boolean getAgentValidationRequired() {
		if (agentValidationRequired == null) {
			return false;
		} else {
			return agentValidationRequired;
		}
		//return agentValidationRequired;
	}

	public void setAgentValidationRequired(Boolean agentValidationRequired) {
		if (agentValidationRequired == null) {
			this.agentValidationRequired = false;
		} else {
			this.agentValidationRequired = agentValidationRequired;
		}
		
	}
	/* END */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(attribute);
		builder.append(" ");
		builder.append(operator);
		builder.append(" ");
		builder.append(value);
		builder.append(")");
		return builder.toString();
	}
}
