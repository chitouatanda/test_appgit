package com.xtaas.domain.valueobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CallGuide {
/*	private String introductionGuide;
	private String questionGuide;
	private String assetGuide;
	private String closingGuide;
	private List<Rebuttal> rebuttalGuide;
*/	
	private CallSection confirmSection;
	private CallSection questionSection;
	private CallSection consentSection;
	private CallSection noteSection;
	//DATE: 18/12/2017
	// Added script clone functionality
	private CallGuide() {}
	
	@JsonCreator
	public CallGuide(@JsonProperty("confirmSection") CallSection confirmSection,
			@JsonProperty("questionSection") CallSection questionSection, @JsonProperty("consentSection") CallSection consentSection,
			@JsonProperty("noteSection") CallSection noteSection) {
		setConfirmSection(confirmSection);
		setQuestionSection(questionSection);
		setConsentSection(consentSection);
		setNoteSection(noteSection);
	}

	/*@JsonCreator
	public CallGuide(@JsonProperty("introductionGuide") String introductionGuide,
			@JsonProperty("questionGuide") String questionGuide, @JsonProperty("assetGuide") String assetGuide,
			@JsonProperty("closingGuide") String closingGuide,
			@JsonProperty("rebuttalGuide") List<Rebuttal> rebuttalGuide) {
		setIntroductionGuide(introductionGuide);
		setQuestionGuide(questionGuide);
		setAssetGuide(assetGuide);
		setClosingGuide(closingGuide);
		setRebuttalGuide(rebuttalGuide);
	}*/

	/*private void setIntroductionGuide(String introductionGuide) {
		this.introductionGuide = introductionGuide;
	}

	private void setQuestionGuide(String questionGuide) {
		this.questionGuide = questionGuide;
	}

	private void setAssetGuide(String assetGuide) {
		this.assetGuide = assetGuide;
	}

	private void setClosingGuide(String closingGuide) {
		this.closingGuide = closingGuide;
	}

	private void setRebuttalGuide(List<Rebuttal> rebuttalGuide) {
		this.rebuttalGuide = rebuttalGuide;
	}

	public String getIntroductionGuide() {
		return introductionGuide;
	}

	public String getQuestionGuide() {
		return questionGuide;
	}

	public String getAssetGuide() {
		return assetGuide;
	}

	public String getClosingGuide() {
		return closingGuide;
	}

	public List<Rebuttal> getRebuttalGuide() {
		return rebuttalGuide;
	}*/

	/**
	 * @return the confirmSection
	 */
	public CallSection getConfirmSection() {
		return confirmSection;
	}

	/**
	 * @return the questionSection
	 */
	public CallSection getQuestionSection() {
		return questionSection;
	}

	/**
	 * @return the consentSection
	 */
	public CallSection getConsentSection() {
		return consentSection;
	}

	/**
	 * @return the noteSection
	 */
	public CallSection getNoteSection() {
		return noteSection;
	}

	/**
	 * @param confirmSection the confirmSection to set
	 */
	private void setConfirmSection(CallSection confirmSection) {
		this.confirmSection = confirmSection;
	}

	/**
	 * @param questionSection the questionSection to set
	 */
	private void setQuestionSection(CallSection questionSection) {
		this.questionSection = questionSection;
	}

	/**
	 * @param consentSection the consentSection to set
	 */
	private void setConsentSection(CallSection consentSection) {
		this.consentSection = consentSection;
	}

	/**
	 * @param noteSection the noteSection to set
	 */
	private void setNoteSection(CallSection noteSection) {
		this.noteSection = noteSection;
	}
}