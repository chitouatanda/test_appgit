package com.xtaas.domain.valueobject;

import java.util.Date;

public class CampaignTeamShortlisted {
	private String teamId;
	private Date shortlistedDate;

	public CampaignTeamShortlisted(String teamId) {
		setTeamId(teamId);
		setShortlistedDate(new Date());
	}
	
	private void setTeamId(String teamId) {
		if (teamId == null) {
			throw new IllegalArgumentException("Team id is required");
		}
		this.teamId = teamId;
	}
	
	public String getTeamId() {
		return teamId;
	}

	public Date getShortlistDate() {
		return shortlistedDate;
	}

	public void setShortlistedDate(Date shortlistDate) {
		this.shortlistedDate = shortlistDate;
	}

	
}
