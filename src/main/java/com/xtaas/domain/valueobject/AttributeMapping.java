package com.xtaas.domain.valueobject;

import java.util.List;

public class AttributeMapping {
	private String from;
	private String to;
	private List<ValueMapping> values;

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public List<ValueMapping> getValues() {
		return values;
	}

}
