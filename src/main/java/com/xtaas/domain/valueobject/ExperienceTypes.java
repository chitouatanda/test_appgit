package com.xtaas.domain.valueobject;

public enum ExperienceTypes {
    SAAS, MARKETPLACE, NETWORK;
    
    @Override
    public String toString() {
        switch(this) {
            case SAAS: return "SAAS";
            case MARKETPLACE: return "MARKETPLACE";
            case NETWORK: return "NETWORK";
            default: throw new IllegalArgumentException();
        }
    }
}
