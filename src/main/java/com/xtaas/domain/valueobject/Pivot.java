package com.xtaas.domain.valueobject;

public class Pivot {
	public String id;
	public String name;
	public int count;
	public double percentage;
	public String category;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Pivot{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", count=" + count +
				", percentage=" + percentage +
				", category='" + category + '\'' +
				'}';
	}
}
