package com.xtaas.domain.valueobject;

public enum ProspectCustomSortField {
	EMPLOYEE_COUNT("employeeCountIndex"), 
	COMPANY_REVENUE("companyRevenueIndex"), 
	TITLE("titleIndex"), 
	STATE("stateIndex"), 
	INDUSTRY("industryIndex"), 
	LIFO("lifoIndex"), 
	FIFO("fifoIndex"), 
	ML("mlIndex");
	
	private final String prospectSortIndexFieldName;
	
	ProspectCustomSortField(String prospectSortIndexFieldName) {
		this.prospectSortIndexFieldName = prospectSortIndexFieldName;
	}
	
	public String getFieldName() {
		return prospectSortIndexFieldName;
	}
	
	public String getProspectSortIndexField() {
        return this.prospectSortIndexFieldName;
    }
}