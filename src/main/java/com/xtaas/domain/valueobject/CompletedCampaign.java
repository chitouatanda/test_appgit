package com.xtaas.domain.valueobject;

import java.util.Date;

public class CompletedCampaign {
	private String campaignId;
	private double rating;
	private Date completedDate;
	
	public CompletedCampaign(String campaignId, double rating) {
		setCampaignId(campaignId);
		setRating(rating);
		setCompletedDate(new Date());
	}

	private void setCampaignId(String campaignId) {
		if (campaignId == null || campaignId.isEmpty()) {
			throw new IllegalArgumentException("Campaign id of completed campaign is required");
		}
		this.campaignId = campaignId;
	}

	private void setRating(double rating) {
		this.rating = rating;
	}
	
	private void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public double getRating() {
		return rating;
	}
	
	public Date getCompleteDate() {
		return completedDate;
	}
}
