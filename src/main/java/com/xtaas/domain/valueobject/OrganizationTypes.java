package com.xtaas.domain.valueobject;

public enum OrganizationTypes {
    CONTACTCENTER, BIZ, AGENCY;
    
    @Override
    public String toString() {
        switch(this) {
            case CONTACTCENTER: return "CONTACTCENTER";
            case BIZ: return "BIZ";
            case AGENCY: return "AGENCY";
            default: throw new IllegalArgumentException();
        }
    }
}
