package com.xtaas.domain.valueobject;

import java.util.List;

import com.xtaas.db.entity.FeedbackResponseAttribute;

public class FeedbackSectionResponse {
	
	private String sectionName;
	private String comments;
	private List<FeedbackResponseAttribute> responseAttributes;
	
	
	/**
	 * @param sectionName
	 * @param responseAttributes
	 */
	public FeedbackSectionResponse(String sectionName,
			List<FeedbackResponseAttribute> responseAttributes) {
		this.sectionName = sectionName;
		this.responseAttributes = responseAttributes;
	}
	/**
	 * @return the sectionName
	 */
	public String getSectionName() {
		return sectionName;
	}
	/**
	 * @param sectionName the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the responseAttributes
	 */
	public List<FeedbackResponseAttribute> getResponseAttributes() {
		return responseAttributes;
	}
	/**
	 * @param responseAttributes the responseAttributes to set
	 */
	public void setResponseAttributes(
			List<FeedbackResponseAttribute> responseAttributes) {
		this.responseAttributes = responseAttributes;
	}
	
}
