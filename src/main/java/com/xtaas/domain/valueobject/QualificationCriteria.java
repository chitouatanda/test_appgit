package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.xtaas.db.entity.CRMAttribute;
import com.xtaas.db.entity.PickListItem;
import com.xtaas.db.entity.Prospect;

/**
 * @author djain
 *
 */
public class QualificationCriteria extends ExpressionGroup {
	private static final Logger logger = LoggerFactory.getLogger(QualificationCriteria.class);
	
	private LinkedHashSet<String> uniqueAttributeMap;
	
	public QualificationCriteria(String groupClause, List<Expression> expressions, List<ExpressionGroup> expressionGroups) {
		super(groupClause, expressions, expressionGroups);
	}
	
	//TODO remove this method after discussion
	
	public HashMap<String,ArrayList<PickListItem>> getQuestions() {
		  HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		  //LinkedHashSet<String> hashset = new LinkedHashSet<String>();
		  questions.putAll(getExpressionValues(getExpressions()));
		  //hashset.addAll(getExpressionGroupAttributes(getExpressionGroups()));
		  questions.putAll(getExpressionGroupValues(getExpressionGroups()));
		  return questions;
		 }
	
	public HashMap<String,ArrayList<PickListItem>> getStdQuestions() {
		  HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		  //LinkedHashSet<String> hashset = new LinkedHashSet<String>();
		  questions.putAll(getStdExpressionValues(getExpressions()));
		  //hashset.addAll(getExpressionGroupAttributes(getExpressionGroups()));
		  questions.putAll(getStdExpressionGroupValues(getExpressionGroups()));
		  return questions;
		 }
	public HashMap<String,String> getStdAttribAndSkins() {
		  HashMap<String,String> questions = new HashMap<String, String>();
		  //LinkedHashSet<String> hashset = new LinkedHashSet<String>();
		  questions.putAll(getStdAttribAndSkin(getExpressions()));
		  //hashset.addAll(getExpressionGroupAttributes(getExpressionGroups()));
		  questions.putAll(getStdAtrribAndSkins(getExpressionGroups()));
		  return questions;
		 }
	
	public HashMap<String,String> getQuestionSkins() {
		HashMap<String,String> questionSkins = new HashMap<String, String>();
		//LinkedHashSet<String> hashset = new LinkedHashSet<String>();
		questionSkins.putAll(getQuestionValueSkin(getExpressions()));
		//hashset.addAll(getExpressionGroupAttributes(getExpressionGroups()));
		//questions.putAll(getExpressionGroupValues(getExpressionGroups()));
		return questionSkins;
	}
	
	public HashMap<String,String> getSequenceNumbers() {
		HashMap<String,String> sequenceNumber = new HashMap<String, String>();
		sequenceNumber.putAll(getSequenceNumber(getExpressions()));
		return sequenceNumber;
	}
	
	private HashMap<String,String> getSequenceNumber(List<Expression> expressions) {
		HashMap<String,String> sequenceNumber = new HashMap<String, String>();
		if (expressions == null) return sequenceNumber;
		for (Expression expr : expressions) {
			String sequence = expr.getSequenceNumber();
			sequenceNumber.put(expr.getAttribute(), sequence);
		}
		return sequenceNumber;
	}
	
	public LinkedHashSet<String> getUniqueAttributeMap() {
		this.uniqueAttributeMap = new LinkedHashSet<String>();
		//traverse expressions list/expression Groups and add attributes to set
		this.uniqueAttributeMap.addAll(getExpressionAttributes(getExpressions()));
		this.uniqueAttributeMap.addAll(getExpressionGroupAttributes(getExpressionGroups()));
		
		return this.uniqueAttributeMap;
	}

	private LinkedHashSet<String> getExpressionAttributes(List<Expression> expressions) {
		LinkedHashSet<String> attributeMap = new LinkedHashSet<String>();
		if (expressions == null) return attributeMap;
		for (Expression expr : expressions) {
			attributeMap.add(expr.getAttribute());
		}
		return attributeMap;
	}

	private HashMap<String,String> getQuestionValueSkin(List<Expression> expressions) {
		HashMap<String,String> questions = new HashMap<String, String>();
	//	LinkedHashSet<String> attributeMap = new LinkedHashSet<String>();
		if (expressions == null) return questions;
		//HashMap<String,List<String>> question = new HashMap<String, List<String>>();
		for (Expression expr : expressions) {
			//attributeMap.add(expr.getValue());
			
			String questionSkin = expr.getQuestionSkin();
			//String[] arr = answers.split(",");
			//List<String> list = Arrays.asList(arr);
			questions.put(expr.getAttribute(), questionSkin);
		}
		return questions;
	}
	
	private HashMap<String,ArrayList<PickListItem>> getExpressionValues(List<Expression> expressions) {
		HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		LinkedHashSet<String> attributeMap = new LinkedHashSet<String>();
		if (expressions == null) return questions;
		//HashMap<String,List<String>> question = new HashMap<String, List<String>>();
		for (Expression expr : expressions) {
			//attributeMap.add(expr.getValue());
			
			ArrayList<PickListItem> answers = expr.getPickList();
			//String[] arr = answers.split(",");
			//List<String> list = Arrays.asList(arr);
			questions.put(expr.getQuestionSkin(), answers);
		}
		return questions;
	}
	
	private HashMap<String,String> getStdAttribAndSkin(List<Expression> expressions) {
		HashMap<String,String> questions = new HashMap<String, String>();
		LinkedHashSet<String> attributeMap = new LinkedHashSet<String>();
		if (expressions == null) return questions;
		//HashMap<String,List<String>> question = new HashMap<String, List<String>>();
		for (Expression expr : expressions) {
			//attributeMap.add(expr.getValue());
			
			
			//String[] arr = answers.split(",");
			//List<String> list = Arrays.asList(arr);
			questions.put(expr.getAttribute(), expr.getQuestionSkin());
		}
		return questions;
	}
	
	private HashMap<String,ArrayList<PickListItem>> getStdExpressionValues(List<Expression> expressions) {
		HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		LinkedHashSet<String> attributeMap = new LinkedHashSet<String>();
		if (expressions == null) return questions;
		//HashMap<String,List<String>> question = new HashMap<String, List<String>>();
		for (Expression expr : expressions) {
			//attributeMap.add(expr.getValue());
			
			ArrayList<PickListItem> answers = expr.getPickList();
			//String[] arr = answers.split(",");
			//List<String> list = Arrays.asList(arr);
			questions.put(expr.getAttribute(), answers);
		}
		return questions;
	}
	
	private HashSet<String> getExpressionGroupAttributes(List<ExpressionGroup> expressionGroups) {
		HashSet<String> attributeMap = new HashSet<String>();
		if (null == expressionGroups) return attributeMap;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.addAll(getExpressionAttributes(exprGroup.getExpressions()));
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.addAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return attributeMap;
	}
	
	private HashMap<String,ArrayList<PickListItem>> getExpressionGroupValues(List<ExpressionGroup> expressionGroups) {
		HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		HashSet<String> attributeMap = new HashSet<String>();
		if (null == expressionGroups) return questions;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.addAll(getExpressionAttributes(exprGroup.getExpressions()));
			questions = getExpressionValues(exprGroup.getExpressions());
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.addAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return questions;
	}
	
	private HashMap<String,ArrayList<PickListItem>> getStdExpressionGroupValues(List<ExpressionGroup> expressionGroups) {
		HashMap<String,ArrayList<PickListItem>> questions = new HashMap<String, ArrayList<PickListItem>>();
		HashSet<String> attributeMap = new HashSet<String>();
		if (null == expressionGroups) return questions;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.addAll(getExpressionAttributes(exprGroup.getExpressions()));
			questions = getStdExpressionValues(exprGroup.getExpressions());
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.addAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return questions;
	}
	
	private HashMap<String,String> getStdAtrribAndSkins(List<ExpressionGroup> expressionGroups) {
		HashMap<String,String> questions = new HashMap<String, String>();
		HashSet<String> attributeMap = new HashSet<String>();
		if (null == expressionGroups) return questions;
		for (ExpressionGroup exprGroup : expressionGroups) {
			attributeMap.addAll(getExpressionAttributes(exprGroup.getExpressions()));
			questions = getStdAttribAndSkin(exprGroup.getExpressions());
			if (exprGroup.getExpressionGroups() != null && exprGroup.getExpressionGroups().size() > 0) {
				attributeMap.addAll(getExpressionGroupAttributes(exprGroup.getExpressionGroups()));
			}
		}
		return questions;
	}
	
	public boolean isProspectQualify(Prospect prospect, HashMap<String, CRMAttribute> prospectAttributeMap) {
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext simpleContext = new StandardEvaluationContext(prospect);
		
		String springExpression = toSpringExpression(prospectAttributeMap);
		boolean result = parser.parseExpression(springExpression).getValue(simpleContext, Boolean.class);
			
		logger.debug("evaluate() : " + springExpression + " Result : " + result);
		return result;
	}
	
	public String toSpringExpression(HashMap<String, CRMAttribute> prospectAttributeMap) {
		//build Spel expression from Qualification Expression
        StringBuilder springExpression = buildSpringExpression(getExpressions(), prospectAttributeMap, getGroupClause());
        StringBuilder springExpressionFromGroup = buildSpringExpressionFromGroup(getExpressionGroups(), prospectAttributeMap, getGroupClause());
		
        StringBuilder completeSpringExpression = new StringBuilder();
        if (springExpression != null) {
        	completeSpringExpression.append(springExpression);
        	if (springExpressionFromGroup != null) {
        		completeSpringExpression.append(" ").append(getGroupClause()).append(" ").append(springExpressionFromGroup).append(" ");
        	}
        } else {
        	if (springExpressionFromGroup != null) {
        		completeSpringExpression.append(springExpressionFromGroup);
        	}
        }
        
		String spelExpression = StringUtils.replace(completeSpringExpression.toString(), "Prospect.", "");
		logger.debug("toSpringExpression() : " + spelExpression);
		return spelExpression;
	}
	
	private StringBuilder buildSpringExpressionFromGroup(List<ExpressionGroup> expressionGroupList, HashMap<String, CRMAttribute> prospectAttributeMap, String groupClause) {
		if (expressionGroupList == null) return null;
		StringBuilder springGroupExpr = new StringBuilder();
		
		for (ExpressionGroup expressionGroup : expressionGroupList) {
			if (springGroupExpr.length() > 0) springGroupExpr.append(" ").append(groupClause).append(" "); //join multiple expressionGroups with the group clause
			springGroupExpr.append("("); //enclose entire expression group into a pair of brackets
			springGroupExpr.append(buildSpringExpression(expressionGroup.getExpressions(), prospectAttributeMap, expressionGroup.getGroupClause()));
			if (expressionGroup.getExpressionGroups() != null && expressionGroup.getExpressionGroups().size() > 0) {
				springGroupExpr.append(" ").append(expressionGroup.getGroupClause()).append(" ");
				springGroupExpr.append(buildSpringExpressionFromGroup(expressionGroup.getExpressionGroups(), prospectAttributeMap, expressionGroup.getGroupClause()));
			}
			springGroupExpr.append(")");
		}
		
		return springGroupExpr;
	}
	
	private StringBuilder buildSpringExpression(List<Expression> expressionList, HashMap<String, CRMAttribute> prospectAttributeMap, String groupClause) {
		if (expressionList == null) return null;
		StringBuilder springExpr = new StringBuilder();
		
		for (Expression qualificationExpression : expressionList) {
			//if not the first condition then append group clause
			if (springExpr.length() > 0) springExpr.append(" ").append(groupClause).append(" ");
			
			//find if attribute is a standard attribute or custom attribute
			CRMAttribute prospectAttribute = prospectAttributeMap.get(qualificationExpression.getAttribute());
			if (prospectAttribute.getProspectBeanPath() != null && !prospectAttribute.getProspectBeanPath().toLowerCase().contains("customattribute")) {
				//it is a standard attribute
				springExpr.append(prospectAttribute.getProspectBeanPath());
			} else {
				//it is a custom attribute
				//check the attribute data type. 
				//if int or any sort of numeric data type then cast the object to Java Integer object
				if (prospectAttribute.getSystemDataType().equals("double")) { 
					springExpr.append(" new Double(CustomAttributes['" + prospectAttribute.getCrmName() + "'])");
				} else { //else "string" data type
					springExpr.append(" CustomAttributes['" + prospectAttribute.getCrmName() + "']");
				}
			}
			
			if (qualificationExpression.getOperator().equalsIgnoreCase("IN")) {
				String inOperatorValues = qualificationExpression.getValue().replace("(", "").replace(")", "").replace(",", "|"); //converting values to regex format
				springExpr.append(" matches '").append("(?i:").append(inOperatorValues).append(")").append("'"); //case insensitive
			} else if(qualificationExpression.getOperator().equalsIgnoreCase("LIKE")) {
				String likeOperatorValues = qualificationExpression.getValue().replace("%", "[A-Za-z0-9]*");
				springExpr.append(" matches ").append(likeOperatorValues);
			} else if (qualificationExpression.getOperator().matches("=|!=|>|>=|<|<=")) {
				if (qualificationExpression.getOperator().equals("=")) {
					if (prospectAttribute.getSystemDataType().equals("string")) {
						springExpr.append(" matches '").append("(?i:").append(qualificationExpression.getValue()).append(")").append("'"); //case insenstive regex
					} else {
						springExpr.append(" == ");
					}
				} else {
					springExpr.append(" ").append(qualificationExpression.getOperator()).append(" ");
					springExpr.append(qualificationExpression.getValue());
				}
				
			}
		} //end for
		return springExpr;
	}
}