package com.xtaas.domain.valueobject;

public class AgentQaAnswer {
	private String question;
	private String answer;
	private String sequence;
	private String isAnswerValid;
	private String answerCertification;
	private String attribute;
	private String clientValue;
	private boolean isAgentQuestion;

	public AgentQaAnswer() {
	}
	
	public AgentQaAnswer(String question, String answer, boolean isAgentQuestion) {
		this.question = question;
		this.answer = answer;
		this.isAgentQuestion = isAgentQuestion;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getIsAnswerValid() {
		return isAnswerValid;
	}

	public void setIsAnswerValid(String isAnswerValid) {
		this.isAnswerValid = isAnswerValid;
	}

	public String getAnswerCertification() {
		return answerCertification;
	}

	public void setAnswerCertification(String answerCertification) {
		this.answerCertification = answerCertification;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getClientValue() {
		return clientValue;
	}

	public void setClientValue(String clientValue) {
		this.clientValue = clientValue;
	}

	public boolean isAgentQuestion() {
		return isAgentQuestion;
	}

	public void setAgentQuestion(boolean isAgentQuestion) {
		this.isAgentQuestion = isAgentQuestion;
	}
	
	

}
