 package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.List;


public class TeamResource {
	private String resourceId;
	private String supervisorOverrideCampaignId;
	private List<WorkSchedule> workSchedules;
	private Address address;
	private List<String> languages;
	private List<String> domains;
	private double averageAvailableHours;
	
	public TeamResource(String resourceId) {
		setResourceId(resourceId);
		workSchedules = new ArrayList<WorkSchedule>();
	}

	private void setResourceId(String resourceId) {
		if (resourceId == null || resourceId.isEmpty()) {
			throw new IllegalArgumentException("Resource id in resource is required");
		}
		this.resourceId = resourceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public Address getAddress() {
		return address;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public List<String> getDomains() {
		return domains;
	}

	public double getAverageAvailableHours() {
		return averageAvailableHours;
	}
	
	public String getSupervisorOverrideCampaignId() {
		return supervisorOverrideCampaignId;
	}

	public void setSupervisorOverrideCampaignId(String supervisorOverrideCampaignId) {
		this.supervisorOverrideCampaignId = supervisorOverrideCampaignId;
	}

	public void addWorkSchedule(WorkSchedule workSchedule) {
		EntityHelper.addItem(workSchedules, workSchedule);
	}

	public void removeWorkSchedule(String scheduleId) {
		if (scheduleId == null || scheduleId.isEmpty()) {
			throw new IllegalArgumentException("Schedule id of schedule to be removed is required");
		}
		int index = workSchedules.size();
		for (; index > 0; index--) {
			if (workSchedules.get(index - 1).getScheduleId().equals(scheduleId)) {
				break;
			}
		}
		if (index != 0) {
			workSchedules.remove(index - 1);
		} else {
			throw new IllegalArgumentException("Work schedule is not available");
		}
	}
	
	// TODO:: Remove the below setters as there are supposed to done by batch
	// job

	public void setAverageAvailableHours(double monthlyAvgAvailableHrs) {
		this.averageAvailableHours = monthlyAvgAvailableHrs;
	}

	public void addLanguage(String language) {
		if (languages == null) {
			languages = new ArrayList<String>();
		}
		EntityHelper.addItem(languages, language);
	}

	public void addDomain(String domain) {
		if (domains == null) {
			domains = new ArrayList<String>();
		}
		EntityHelper.addItem(domains, domain);
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	// TODO:: Remove the above setters as there are supposed to done by batch
	// job

	public List<WorkSchedule> getWorkSchedules() {
		return this.workSchedules;
	}
}
