package com.xtaas.domain.valueobject;

public enum PurchaseStatus {

	PURCHASE_COMPLETE, PURCHASE_SUMMARY
}
