package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignResource {
	private String resourceId;
	private List<WorkSchedule> workSchedules;
	
	@JsonCreator
	public CampaignResource(@JsonProperty("resourceId") String resourceId) {
		setResourceId(resourceId);
		workSchedules = new ArrayList<WorkSchedule>();
	}

	private void setResourceId(String resourceId) {
		if (resourceId == null || resourceId.isEmpty()) {
			throw new IllegalArgumentException("Resource id in resource is required");
		}
		this.resourceId = resourceId;
	}

	public void addWorkSchedule(WorkSchedule workSchedule) {
		EntityHelper.addItem(workSchedules, workSchedule);
	}
	
	public WorkSchedule getWorkSchedule(String scheduleId) {
		if (scheduleId == null || scheduleId.isEmpty()) {
			throw new IllegalArgumentException("Schedule id of schedule to be removed is required");
		}
		int index = workSchedules.size();
		for (; index > 0; index--) {
			if (workSchedules.get(index - 1).getScheduleId().equals(scheduleId)) {
				return workSchedules.get(index - 1);
			}
		}
		throw new IllegalArgumentException("Work schedule is not available");
	}
	
	public void updateWorkSchedule(String scheduleId, WorkSchedule workSchedule) {
		workSchedules.add(workSchedule);
	}

	public void removeWorkSchedule(String scheduleId) {
		if (scheduleId == null || scheduleId.isEmpty()) {
			throw new IllegalArgumentException("Schedule id of schedule to be removed is required");
		}
		int index = workSchedules.size();
		for (; index > 0; index--) {
			if (workSchedules.get(index - 1).getScheduleId().equals(scheduleId)) {
				break;
			}
		}
		if (index != 0) {
			workSchedules.remove(index - 1);
		}
	}
	
	public String getResourceId() {
		return resourceId;
	}

	public List<WorkSchedule> getWorkSchedules() {
		return workSchedules;
	}
}
