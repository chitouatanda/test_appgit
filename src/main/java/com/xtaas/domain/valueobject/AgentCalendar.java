/**
 * 
 */
package com.xtaas.domain.valueobject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.utils.XtaasDateUtils;

/**
 * Represents a Work Calendar of an Agent
 * 
 * @author djain
 */
public class AgentCalendar {
	
	private static final Logger logger = LoggerFactory.getLogger(AgentCalendar.class);
	
	private String agentId; //Agent for which this calendar belongs to
	private Map<LocalDate, TreeSet<WorkSlot>> agentCalendar = new TreeMap<LocalDate, TreeSet<WorkSlot>>(); //map of time in millis (of specific date) to the work slots
	private LocalDate startDate;
	private LocalDate endDate;
	
	/**
	 * Prepare calendar for agent for the specified date range
	 */
	public AgentCalendar(LocalDate startDate, LocalDate endDate, String agentId) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.agentId = agentId;
	}
	
	public void setAvailableSchedule(WorkSchedule workSchedule) {
		LocalDate calendarStartDate = getStartDate(); //date from which calendar needs to be generated
		LocalDate calendarEndDate = getEndDate(); //end date till which calendar needs to be generated (exclusive of last day)
		
		if (workSchedule.getRecurrence() == null) { //its a single day schedule
			LocalDate scheduleDate = new LocalDate(workSchedule.getScheduleDate());
			WorkSlot availableSlot = new WorkSlot(workSchedule.getStartHour(), workSchedule.getEndHour());
			createAvailableSlot(scheduleDate, availableSlot, calendarStartDate, calendarEndDate);
		} else { //if recurrence is set
			LocalDate recurrenceEndDate = new LocalDate(workSchedule.getRecurrence().getEndDate());
			LocalDate scheduleEndDate = (recurrenceEndDate.isBefore(calendarEndDate)) ? recurrenceEndDate : calendarEndDate; //recurrenceEndDate or calendarEndDate, whichever is earlier
			List<DayOfWeek> days = workSchedule.getRecurrence().getDays();
			if (null == days && workSchedule.getRecurrence().getType().equals(RecurrenceTypes.Daily)) {
				days = Arrays.asList(DayOfWeek.values());
			}
			
			LocalDate currentDateProcessed = calendarStartDate; //date for which calendar is being processed / generated.
			//for each of the day create a recurring schedule till the schedule End Date is reached
			while (currentDateProcessed.isBefore(scheduleEndDate) || currentDateProcessed.isEqual(scheduleEndDate)) {
				String day = currentDateProcessed.dayOfWeek().getAsShortText();
				if (days.contains(Enum.valueOf(DayOfWeek.class, day))) {
					WorkSlot availableSlot = new WorkSlot(workSchedule.getStartHour(), workSchedule.getEndHour());
					createAvailableSlot(currentDateProcessed, availableSlot, calendarStartDate, scheduleEndDate);
				}
				currentDateProcessed = currentDateProcessed.plusDays(1); //move to next day
			}
		}
	}
	
	private void createAvailableSlot(LocalDate forDate, WorkSlot availableSlot, LocalDate scheduleStartDate, LocalDate scheduleEndDate) {
		if (forDate.isBefore(scheduleStartDate) || forDate.isAfter(scheduleEndDate)) return;

    	TreeSet<WorkSlot> wsList = agentCalendar.get(forDate);
    	if (wsList == null) {
    		wsList = new TreeSet<WorkSlot>();
    		agentCalendar.put(forDate, wsList);
    	}
    	wsList.add(availableSlot);
	}
	
	public void setBusySchedule(WorkSchedule workSchedule, String workUnitIdfier) {
		setBusySchedule(workSchedule, workUnitIdfier, "");
	}
	
	public void setBusySchedule(WorkSchedule workSchedule, String workUnitIdfier, String workUnitName) {
		LocalDate calendarStartDate = getStartDate(); //date from which calendar needs to be generated
		LocalDate calendarEndDate = getEndDate(); //end date till which calendar needs to be generated 
    	
		if (workSchedule.getRecurrence() == null) { //its a single day schedule
			LocalDate scheduleDate = new LocalDate(workSchedule.getScheduleDate());
			WorkSlot busySlot = new WorkSlot(workSchedule.getStartHour(), workSchedule.getEndHour());
	    	busySlot.setWorkUnitIdfier(workUnitIdfier);
	    	busySlot.setWorkUnitName(workUnitName);
	    	busySlot.setWorkScheduleId(workSchedule.getScheduleId());
	    	createBusySlot(scheduleDate, busySlot, calendarStartDate, calendarEndDate);
		} else {  //if recurrence is set
			LocalDate recurrenceStartDate = new LocalDate(workSchedule.getRecurrence().getStartDate());
			LocalDate recurrenceEndDate = new LocalDate(workSchedule.getRecurrence().getEndDate());
			LocalDate scheduleEndDate = (recurrenceEndDate.isBefore(calendarEndDate)) ? recurrenceEndDate : calendarEndDate; //recurrenceEndDate or calendarEndDate, whichever is earlier
			List<DayOfWeek> days = workSchedule.getRecurrence().getDays();
			if (null == days && workSchedule.getRecurrence().getType().equals(RecurrenceTypes.Daily)) {
				days = Arrays.asList(DayOfWeek.values());
			}
			
			LocalDate currentDateProcessed = calendarStartDate; //date for which calendar is being processed / generated.
			//for each day create a recurring schedule till the schedule End Date is reached
			while (currentDateProcessed.isBefore(scheduleEndDate) || currentDateProcessed.isEqual(scheduleEndDate)) {
				if (currentDateProcessed.isBefore(recurrenceStartDate) || currentDateProcessed.isAfter(recurrenceEndDate)) {
					currentDateProcessed = currentDateProcessed.plusDays(1); //move to next day
					continue; //don't create busy slot if date is beyond recurrence date 
				}
				
				String day = currentDateProcessed.dayOfWeek().getAsShortText();
				if (days.contains(Enum.valueOf(DayOfWeek.class, day))) {
					WorkSlot busySlot = new WorkSlot(workSchedule.getStartHour(), workSchedule.getEndHour());
			    	busySlot.setWorkUnitIdfier(workUnitIdfier);
			    	busySlot.setWorkUnitName(workUnitName);
			    	busySlot.setWorkScheduleId(workSchedule.getScheduleId());
			    	busySlot.setRecurrenceDescription(generateWorkScheduleDescription(workSchedule));
					createBusySlot(currentDateProcessed, busySlot, calendarStartDate, scheduleEndDate);
				}
				currentDateProcessed = currentDateProcessed.plusDays(1); //move to next day
			}
		}
	}
	
	private String generateWorkScheduleDescription(WorkSchedule workSchedule) {
		StringBuilder wsDescription = new StringBuilder();
		if (workSchedule.getRecurrence() != null) { //if recurrence is set
			if (RecurrenceTypes.Daily.equals(workSchedule.getRecurrence().getType())) {
				wsDescription.append("Daily from ")
					.append(XtaasDateUtils.convertDateToString(workSchedule.getRecurrence().getStartDate()))
					.append(" to ")
					.append(XtaasDateUtils.convertDateToString(workSchedule.getRecurrence().getEndDate()));
			} else if (RecurrenceTypes.Weekly.equals(workSchedule.getRecurrence().getType())) { //if Weekly Recurrence
				wsDescription.append("Weekly on ")
					.append(StringUtils.join(workSchedule.getRecurrence().getDays(), ','))
					.append(" from ")
					.append(XtaasDateUtils.convertDateToString(workSchedule.getRecurrence().getStartDate()))
					.append(" to ")
					.append(XtaasDateUtils.convertDateToString(workSchedule.getRecurrence().getEndDate()));
			}
		}
		return wsDescription.toString();
	}
	
	private void createBusySlot(LocalDate forDate, WorkSlot busySlot, LocalDate scheduleStartDate, LocalDate scheduleEndDate) {
		if (forDate.isBefore(scheduleStartDate) || forDate.isAfter(scheduleEndDate)) return;
		
		Set<WorkSlot> allSlots = agentCalendar.get(forDate);
		
		if (allSlots == null) {
    		logger.error("createBusySlot() : There is no slot made available by agent [{}] for the Date [{}] ", getAgentId(), forDate);
    		return;
    	} 
    	for (WorkSlot slot : allSlots) { //traverse the work schedules list and find the slot. Slot may be of same size or may be a subset of available slot
    		if (slot.equals(busySlot)) { //if slot found, then mark it as busy
    			//logger.debug("createBusySlot() : Date: " + forDate + " -> Workslot [{}] marked as busy with WorkUnitIdfier [{}]", slot, busySlot.getWorkUnitIdfier());
    			allSlots.remove(slot);
    			allSlots.add(busySlot); //adds the busy slot
    			return;
    		} else if (slot.contains(busySlot)) { //check if it is a subset of the available slot, then split the slot Ex: Slot=10-15, BusySlot=11-14
    			//TreeSet<WorkSlot> updatedSlots = new TreeSet<WorkSlot>(allSlots); //copy of allSlots to update this one. This is needed to avoid ConcurrentModificationException
    			//logger.debug("createBusySlot() : Date: " + forDate + " -> Workslot [{}] contains slot [{}]. Splitting the container slot.", slot, busySlot);
    			allSlots.remove(slot); //remove the bigger slot as now its been split into a smaller slot
    			//split the slot
    			if (slot.getStartHour() == busySlot.getStartHour()) { //1. if start hour is same but busySlot's end hour is below slot's end hour Ex: Slot=10-15, BusySlot=10-14
    				WorkSlot availableSlot = new WorkSlot(busySlot.getEndHour(), slot.getEndHour()); //creating new available slot as 14-15
    				allSlots.add(availableSlot);
    			} else if (slot.getEndHour() == busySlot.getEndHour()) { //2. if end hour is same but busySlot's start hour is after slot's start hour Ex: Slot=10-15, BusySlot=11-15
    				WorkSlot availableSlot = new WorkSlot(slot.getStartHour(), busySlot.getStartHour());
    				allSlots.add(availableSlot); //creating new available slot as 10-11
    			} else { //3. busySlot is a complete subset i.e slot.startHour < busySlot.startHour and slot.endHour > busySlot.endHour Ex: Slot=10-15, BusySlot=11-14
    				WorkSlot availableSlot1 = new WorkSlot(slot.getStartHour(), busySlot.getStartHour()); //creating new available slot as 10-11
    				WorkSlot availableSlot2 = new WorkSlot(busySlot.getEndHour(), slot.getEndHour()); //creating new available slot as 14-15
    				allSlots.add(availableSlot1);
    				allSlots.add(availableSlot2);
    			}
    			allSlots.add(busySlot); //adds the busy slot
    			//agentCalendar.put(forDate, updatedSlots);
    			return;
    		} //end if
    	} //end for
    	
	}
	
	public String getAgentId() {
		return this.agentId;
	}
	
	public LocalDate getStartDate() {
		return this.startDate;
	}
	
	public LocalDate getEndDate() {
		return this.endDate;
	}

	/**
	 * @return the aCalendar
	 */
	public Map<LocalDate, TreeSet<WorkSlot>> getAgentCalendar() {
		return Collections.unmodifiableMap(agentCalendar);
	}

	public int getTotalAvailableHrs() {
		int totalAvailableHrs = 0;
		for (LocalDate date : agentCalendar.keySet()) {
			TreeSet<WorkSlot> workSlots = agentCalendar.get(date);
			for (WorkSlot slot : workSlots) {
				if (slot.isAvailable()) {
					totalAvailableHrs = totalAvailableHrs + slot.getLength();
				}
			}
		}
		return totalAvailableHrs;
	}
}
