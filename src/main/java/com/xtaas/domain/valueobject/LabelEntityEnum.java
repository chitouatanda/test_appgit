/**
 * 
 */
package com.xtaas.domain.valueobject;

/**
 * Enum of entities for which DisplayLabelController will return labels to display
 *   
 * @author djain
 */
public enum LabelEntityEnum {
	DISPOSITIONTYPE, FAILUREDISPOSITIONSTATUS, SUCCESSDISPOSITIONSTATUS, DIALERCODEDISPOSITIONSTATUS, CALLBACKDISPOSITIONSTATUS, DIALERMODE,NOT_DISPOSEDDISPOSITIONSTATUS;
}
