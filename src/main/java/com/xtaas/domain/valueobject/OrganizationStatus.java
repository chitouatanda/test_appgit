package com.xtaas.domain.valueobject;

public enum OrganizationStatus {
	ACTIVE,INACTIVE;

	@Override
    public String toString() {
		switch(this) {
        	case ACTIVE: return "Active";
        	case INACTIVE: return "InActive";
        	default: throw new IllegalArgumentException();
		}
    }
}
