package com.xtaas.domain.valueobject;

public class RestQuestionDTO {
	private String questionAttribute;
	private String questionSkin;
	private String operator;
	private String answers;
	private boolean agentValidationRequired;

	public String getQuestionAttribute() {
		return questionAttribute;
	}

	public void setQuestionAttribute(String questionAttribute) {
		this.questionAttribute = questionAttribute;
	}

	public String getQuestionSkin() {
		return questionSkin;
	}

	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

	public boolean isAgentValidationRequired() {
		return agentValidationRequired;
	}

	public void setAgentValidationRequired(boolean agentValidationRequired) {
		this.agentValidationRequired = agentValidationRequired;
	}

}