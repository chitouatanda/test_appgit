package com.xtaas.domain.valueobject;

public class ValueMapping {
	private String from;
	private String to;

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

}
