package com.xtaas.domain.valueobject;

import java.util.List;

public class WorkDays {
	private List<WorkHours> mondayHours;
	private List<WorkHours> tuesdayHours;
	private List<WorkHours> wednesdayHours;
	private List<WorkHours> thursdayHours;
	private List<WorkHours> fridayHours;

	public WorkDays(List<WorkHours> mondayHours, List<WorkHours> tuesdayHours, List<WorkHours> wednesdayHours,
			List<WorkHours> thursdayHours, List<WorkHours> fridayHours) {
		setMondayHours(mondayHours);
		setTuesdayHours(tuesdayHours);
		setWednesdayHours(wednesdayHours);
		setThursdayHours(thursdayHours);
		setFridayHours(fridayHours);
	}

	/**
	 * @param mondayHours
	 *            the mondayHours to set
	 */
	private void setMondayHours(List<WorkHours> mondayHours) {
		this.mondayHours = mondayHours;
	}

	/**
	 * @param tuesdayHours
	 *            the tuesdayHours to set
	 */
	private void setTuesdayHours(List<WorkHours> tuesdayHours) {
		this.tuesdayHours = tuesdayHours;
	}

	/**
	 * @param wednesdayHours
	 *            the wednesdayHours to set
	 */
	private void setWednesdayHours(List<WorkHours> wednesdayHours) {
		this.wednesdayHours = wednesdayHours;
	}

	/**
	 * @param thursdayHours
	 *            the thursdayHours to set
	 */
	private void setThursdayHours(List<WorkHours> thursdayHours) {
		this.thursdayHours = thursdayHours;
	}

	/**
	 * @param fridayHours
	 *            the fridayHours to set
	 */
	private void setFridayHours(List<WorkHours> fridayHours) {
		this.fridayHours = fridayHours;
	}

	/**
	 * @return the mondayHours
	 */
	public List<WorkHours> getMondayHours() {
		return mondayHours;
	}

	/**
	 * @return the tuesdayHours
	 */
	public List<WorkHours> getTuesdayHours() {
		return tuesdayHours;
	}

	/**
	 * @return the wednesdayHours
	 */
	public List<WorkHours> getWednesdayHours() {
		return wednesdayHours;
	}

	/**
	 * @return the thursdayHours
	 */
	public List<WorkHours> getThursdayHours() {
		return thursdayHours;
	}

	/**
	 * @return the fridayHours
	 */
	public List<WorkHours> getFridayHours() {
		return fridayHours;
	}

	public int getTotalWorkHours() {
		return getDayWorkHours(mondayHours) + getDayWorkHours(tuesdayHours) + getDayWorkHours(wednesdayHours)
				+ getDayWorkHours(thursdayHours) + getDayWorkHours(fridayHours);
	}

	private int getDayWorkHours(List<WorkHours> workHoursList) {
		int totalWorkHours = 0;
		if (workHoursList != null) {
			for (WorkHours workHours : workHoursList) {
				totalWorkHours += (workHours.getEndHour() - workHours.getStartHour());
			}
		}
		return totalWorkHours;
	}

}
