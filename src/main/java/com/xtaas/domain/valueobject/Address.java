package com.xtaas.domain.valueobject;

public class Address {
	private String line1;
	private String line2;
	private String city;
	private String state;
	private String postalCode;
	private String country;
	private String timeZone;

	public Address(String line1, String line2, String city, String state, String country, String postalCode, String timeZone) {
		setLine1(line1);
		setLine2(line2);
		setCity(city);
		setState(state);
		setCountry(country);
		setPostalCode(postalCode);
		setTimeZone(timeZone);
	}

	private void setLine1(String line1) {
		if (line1 == null || line1.isEmpty()) {
			throw new IllegalArgumentException("Line1 in address is required");
		}
		this.line1 = line1;
	}

	private void setLine2(String line2) {
		this.line2 = line2;
	}

	private void setCity(String city) {
		if (city == null || city.isEmpty()) {
			throw new IllegalArgumentException("City in address is required");
		}
		this.city = city;
	}

	private void setState(String state) {
		if (state == null || state.isEmpty()) {
			throw new IllegalArgumentException("State in address is required");
		}
		this.state = state;
	}

	private void setPostalCode(String postalCode) {
		if (postalCode == null || postalCode.isEmpty()) {
			throw new IllegalArgumentException("Postal code in address is required");
		}
		this.postalCode = postalCode;
	}

	private void setCountry(String country) {
		if (country == null || country.isEmpty()) {
			throw new IllegalArgumentException("Country in address is required");
		}
		this.country = country;
	}

	private void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLine1() {
		return line1;
	}

	public String getLine2() {
		return line2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCountry() {
		return country;
	}

	public String getTimeZone() {
		return timeZone;
	}

}
