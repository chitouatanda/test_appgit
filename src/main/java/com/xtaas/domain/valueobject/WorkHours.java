package com.xtaas.domain.valueobject;

public class WorkHours {
	private int startHour;
	private int endHour;

	public WorkHours(int startHour, int endHour) {
		if (endHour <= startHour) {
			throw new IllegalArgumentException("End hour cannot be greater than start hour");
		}
		this.setStartHour(startHour);
		this.setEndHour(endHour);
	}
	
	/**
	 * @param startHour the startHour to set
	 */
	private void setStartHour(int startHour) {
		this.startHour = startHour;
	}
	
	/**
	 * @param endHour the endHour to set
	 */
	private void setEndHour(int endHour) {
		this.endHour = endHour;
	}
	
	/**
	 * @return the startHour
	 */
	public int getStartHour() {
		return startHour;
	}
	
	/**
	 * @return the endHour
	 */
	public int getEndHour() {
		return endHour;
	}
}
