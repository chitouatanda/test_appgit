package com.xtaas.domain.valueobject;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class AgentSearchResult {
	@Id
	private String id;
	private String firstName;
	private String lastName;
	private String overview;
	private Address address;
	private Double perLeadHour;
	private Integer totalCompletedCampaigns;
	private Double averageRating;
	private Integer totalVolume;
	private Double averageConversion;
	private Double averageQuality;
	private Integer averageAvailableHours;
	private Date invitationDate;
	private Integer contactsMade;
	private Integer leadsDelivered;
	private Double averageTalkTime;
	private Boolean campaignTemporaryAllocated;
	private AgentStatus currentStatus;
	private String agentProfilePicUrl;
	private String partnerId;

	public String getId() {
		return id;
	}

	public String getName() {
		return getFirstName() + " " + getLastName();
	}

	public String getOverview() {
		return overview;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the perLeadHour
	 */
	public Double getPerLeadHour() {
		return perLeadHour;
	}

	/**
	 * @param perLeadHour the perLeadHour to set
	 */
	public void setPerLeadHour(Double perLeadHour) {
		this.perLeadHour = perLeadHour;
	}

	/**
	 * @return the totalCompletedCampaigns
	 */
	public Integer getTotalCompletedCampaigns() {
		return totalCompletedCampaigns;
	}

	/**
	 * @param totalCompletedCampaigns the totalCompletedCampaigns to set
	 */
	public void setTotalCompletedCampaigns(Integer totalCompletedCampaigns) {
		this.totalCompletedCampaigns = totalCompletedCampaigns;
	}

	/**
	 * @return the averageRating
	 */
	public Double getAverageRating() {
		return averageRating;
	}

	/**
	 * @param averageRating the averageRating to set
	 */
	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	/**
	 * @return the totalVolume
	 */
	public Integer getTotalVolume() {
		return totalVolume;
	}

	/**
	 * @param totalVolume the totalVolume to set
	 */
	public void setTotalVolume(Integer totalVolume) {
		this.totalVolume = totalVolume;
	}

	/**
	 * @return the averageConversion
	 */
	public Double getAverageConversion() {
		return averageConversion;
	}

	/**
	 * @param averageConversion the averageConversion to set
	 */
	public void setAverageConversion(Double averageConversion) {
		this.averageConversion = averageConversion;
	}

	/**
	 * @return the averageQuality
	 */
	public Double getAverageQuality() {
		return averageQuality;
	}

	/**
	 * @param averageQuality the averageQuality to set
	 */
	public void setAverageQuality(Double averageQuality) {
		this.averageQuality = averageQuality;
	}

	/**
	 * @return the averageAvailableHours
	 */
	public Integer getAverageAvailableHours() {
		return averageAvailableHours;
	}

	/**
	 * @param averageAvailableHours the averageAvailableHours to set
	 */
	public void setAverageAvailableHours(Integer averageAvailableHours) {
		this.averageAvailableHours = averageAvailableHours;
	}

	/**
	 * @return the invitationDate
	 */
	public Date getInvitationDate() {
		return invitationDate;
	}

	/**
	 * @param invitationDate the invitationDate to set
	 */
	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	/**
	 * @return the contactsMade
	 */
	public Integer getContactsMade() {
		return contactsMade;
	}

	/**
	 * @param contactsMade the contactsMade to set
	 */
	public void setContactsMade(Integer contactsMade) {
		this.contactsMade = contactsMade;
	}

	/**
	 * @return the leadsDelivered
	 */
	public Integer getLeadsDelivered() {
		return leadsDelivered;
	}

	/**
	 * @param leadsDelivered the leadsDelivered to set
	 */
	public void setLeadsDelivered(Integer leadsDelivered) {
		this.leadsDelivered = leadsDelivered;
	}

	/**
	 * @return the averageTalkTime
	 */
	public Double getAverageTalkTime() {
		return averageTalkTime;
	}

	/**
	 * @param averageTalkTime the averageTalkTime to set
	 */
	public void setAverageTalkTime(Double averageTalkTime) {
		this.averageTalkTime = averageTalkTime;
	}

	/**
	 * @return the campaignTemporaryAllocated
	 */
	public Boolean getCampaignTemporaryAllocated() {
		return campaignTemporaryAllocated;
	}

	/**
	 * @param campaignTemporaryAllocated the campaignTemporaryAllocated to set
	 */
	public void setCampaignTemporaryAllocated(Boolean campaignTemporaryAllocated) {
		this.campaignTemporaryAllocated = campaignTemporaryAllocated;
	}

	/**
	 * @return the currentStatus
	 */
	public AgentStatus getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * @param currentStatus the currentStatus to set
	 */
	public void setCurrentStatus(AgentStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	/**
	 * @return the agentProfilePicUrl
	 */
	public String getAgentProfilePicUrl() {
		return agentProfilePicUrl;
	}

	/**
	 * @param agentProfilePicUrl the agentProfilePicUrl to set
	 */
	public void setAgentProfilePicUrl(String agentProfilePicUrl) {
		this.agentProfilePicUrl = agentProfilePicUrl;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
}
