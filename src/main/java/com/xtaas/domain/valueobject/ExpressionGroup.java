package com.xtaas.domain.valueobject;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author djain
 * 
 */
public class ExpressionGroup {
	@NotNull
	private String groupClause; // AND, OR
	private List<Expression> expressions;
	private List<ExpressionGroup> expressionGroups;
	
	public ExpressionGroup(String groupClause, List<Expression> expressions, List<ExpressionGroup> expressionGroups) {
		/*if ((expressions == null || expressions.size() == 0) && (expressionGroups == null || expressionGroups.size() == 0)) {
			throw new IllegalArgumentException("At least one expression or expression group must be present");
		}*/
		setGroupClause(groupClause);
		setExpressions(expressions);
		setExpressionGroups(expressionGroups);
	}

	/**
	 * @param groupClause the groupClause to set
	 */
	private void setGroupClause(String groupClause) {
		/*if (groupClause == null) {
			throw new IllegalArgumentException("Group clause is required");
		}*/
		this.groupClause = groupClause;
	}
	
	/**
	 * @param expressions the expressions to set
	 */
	private void setExpressions(List<Expression> expressions) {
		this.expressions = expressions;
	}

	/**
	 * @param expressionGroups the expressionGroups to set
	 */
	private void setExpressionGroups(List<ExpressionGroup> expressionGroups) {
		this.expressionGroups = expressionGroups;
	}

	/**
	 * @return the groupClause
	 */
	public String getGroupClause() {
		return groupClause;
	}

	/**
	 * @return the expressions
	 */
	public List<Expression> getExpressions() {
		return expressions;
	}

	/**
	 * @return the expressionGroups
	 */
	public List<ExpressionGroup> getExpressionGroups() {
		return expressionGroups;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		builder.append("(");
		if (null != expressions) {
			for (Expression expression : expressions) {
				if (first) {
					first = false;
				} else {
					builder.append(' ');
					builder.append(groupClause);
					builder.append(' ');
				}
				builder.append(expression.toString());
			}
		}
		if (null != expressionGroups) {
			for (ExpressionGroup expressionGroup : expressionGroups) {
				if (first) {
					first = false;
				} else {
					builder.append(' ');
					builder.append(groupClause);
					builder.append(' ');
				}
				builder.append(expressionGroup.toString());
			}
		}
		builder.append(")");
		return builder.toString();
	}
}
