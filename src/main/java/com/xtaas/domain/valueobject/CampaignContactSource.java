package com.xtaas.domain.valueobject;

public enum CampaignContactSource {
	
	FILE, NETPROSPEX,ZOOMINFO;
	
	 @Override
	    public String toString() {
	      switch(this) {
	        case FILE: return "file";
	        case NETPROSPEX: return "NetProspex";
	        case ZOOMINFO: return "ZoomInfo";
	       
	        default: throw new IllegalArgumentException();
	      }
	 }
}
