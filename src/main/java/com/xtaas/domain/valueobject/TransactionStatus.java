package com.xtaas.domain.valueobject;

public enum TransactionStatus {

	SUCCESS, FAILURE, IN_PROGRESS
}
