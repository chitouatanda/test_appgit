package com.xtaas.domain.valueobject;

public enum NotDisposeDispositionStatus {

	NOT_DISPOSED; 
	
	@Override
    public String toString() {
		switch(this) {
        	case NOT_DISPOSED: return "NOT_DISPOSED";
        	default: throw new IllegalArgumentException();
		}
    }
}
