package com.xtaas.domain.valueobject;

public enum CampaignTypes {
	LeadGeneration, LeadVerification, LeadQualification, EmailMarketing, TeleMailCampaign, Prospecting;
		
	@Override
    public String toString() {
		switch(this) {
        	case LeadGeneration: return "Lead Generation";
        	case LeadVerification: return "Lead Verification";
			case LeadQualification: return "Lead Qualification";
			case EmailMarketing: return "Email Marketing";
			case TeleMailCampaign: return "TeleMail Campaign";
			case Prospecting: return "Prospecting";
        	default: throw new IllegalArgumentException();
		}
    }
}