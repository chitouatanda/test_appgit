/**
 * 
 */
package com.xtaas.domain.valueobject;

import java.util.Date;


/**
 * A change log class which maintain the history of changed prospect identity info with the date and changeId. 
 * 
 * @author djain
 */
public class ProspectIdentityChangeLog {
	private int changeLogId;
    private String oldFirstName;
    private String oldLastName;
    private String oldEmail;
    private String oldDepartment;
    private String oldCompany;
    private String oldRole;
	private Date dateChangeLogged;
	private String oldDomain;
	private String questionSkin;
	private String oldAnswer;
	private String answerFromAgent;
	private String oldExtension;
	private String oldAgentDialedExtension;
	
	public ProspectIdentityChangeLog() {
		// TODO Auto-generated constructor stub
	}
    
	public ProspectIdentityChangeLog(int changeLogId, String oldFirstName, String oldLastName, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setFirstName(oldFirstName);
		setLastName(oldLastName);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogByEmail(int changeLogId, String oldEmail, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldEmail(oldEmail);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogByDepartment(int changeLogId, String oldDepartment, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldDepartment(oldDepartment);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogByCompany(int changeLogId, String oldCompany, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldCompany(oldCompany);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogByRole(int changeLogId, String oldRole, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldRole(oldRole);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogByDomain(int changeLogId, String oldDomain, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldDomain(oldDomain);
		setDateChangeLogged(dateChangeLogged);
	}
	
	public void setProspectIdentityChangeLogAnswer(int changeLogId, String questionSkin, String oldAnswer, String answer, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setQuestionSkin(questionSkin);
		setOldAnswer(oldAnswer);
		setAnswerFromAgent(answer);
		setDateChangeLogged(dateChangeLogged);
	}

	public void setProspectIdentityChangeLogByExtension(int changeLogId, String oldExtension, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldExtension(oldExtension);
		setDateChangeLogged(dateChangeLogged);
	}

	public void setProspectIdentityChangeLogByAgentDialedExtension(int changeLogId, String oldAgentDialedExtension, Date dateChangeLogged) {
		setChangeLogId(changeLogId);
		setOldAgentDialedExtension(oldAgentDialedExtension);
		setDateChangeLogged(dateChangeLogged);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return oldFirstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	private void setFirstName(String oldFirstName) {
		this.oldFirstName = oldFirstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return oldLastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	private void setLastName(String oldLastName) {
		this.oldLastName = oldLastName;
	}

	/**
	 * @return the changeLogId
	 */
	public int getChangeLogId() {
		return changeLogId;
	}

	/**
	 * @param changeLogId the changeLogId to set
	 */
	private void setChangeLogId(int changeLogId) {
		this.changeLogId = changeLogId;
	}

	/**
	 * @return the dateChangeLogged
	 */
	public Date getDateChangeLogged() {
		return dateChangeLogged;
	}

	/**
	 * @param dateChangeLogged the dateChangeLogged to set
	 */
	private void setDateChangeLogged(Date dateChangeLogged) {
		this.dateChangeLogged = dateChangeLogged;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public String getOldDomain() {
		return oldDomain;
	}

	public void setOldDomain(String oldDomain) {
		this.oldDomain = oldDomain;
	}

	public String getOldDepartment() {
		return oldDepartment;
	}

	public void setOldDepartment(String oldDepartment) {
		this.oldDepartment = oldDepartment;
	}

	public String getOldCompany() {
		return oldCompany;
	}

	public void setOldCompany(String oldCompany) {
		this.oldCompany = oldCompany;
	}

	public String getOldRole() {
		return oldRole;
	}

	public void setOldRole(String oldRole) {
		this.oldRole = oldRole;
	}
	
	public String getQuestionSkin() {
		return questionSkin;
	}

	public void setQuestionSkin(String questionSkin) {
		this.questionSkin = questionSkin;
	}
	
	public String getOldAnswer() {
		return oldAnswer;
	}

	public void setOldAnswer(String oldAnswer) {
		this.oldAnswer = oldAnswer;
	}

	public String getAnswerFromAgent() {
		return answerFromAgent;
	}

	public void setAnswerFromAgent(String answerFromAgent) {
		this.answerFromAgent = answerFromAgent;
	}

	public String getOldExtension() {
		return oldExtension;
	}

	public void setOldExtension(String oldExtension) {
		this.oldExtension = oldExtension;
	}

	public String getOldAgentDialedExtension() {
		return oldAgentDialedExtension;
	}

	public void setOldAgentDialedExtension(String oldAgentDialedExtension) {
		this.oldAgentDialedExtension = oldAgentDialedExtension;
	}
}
