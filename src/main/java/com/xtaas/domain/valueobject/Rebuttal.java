package com.xtaas.domain.valueobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Rebuttal {
	private String scenario; // rebuttal scenario Ex: NO_TIME
	private String label; // displayable label for scenario Ex: "No Time"
	private String solution; //solution to handle the rebuttal. EX: "State that you’ll keep the brief and try to continue pitching prior to setting a callback"

	@JsonCreator
	public Rebuttal(@JsonProperty("scenario") String scenario, @JsonProperty("label") String label, @JsonProperty("solution") String solution) {
		this.setScenario(scenario);
		this.setSolution(solution);
		this.setLabel(label);
	}

	private void setScenario(String scenario) {
		if (scenario == null) {
			throw new IllegalArgumentException("Scenario is required");
		}
		this.scenario = scenario;
	}

	private void setSolution(String solution) {
		if (solution == null) {
			throw new IllegalArgumentException("Solution is required");
		}
		this.solution = solution;
	}

	public String getScenario() {
		return scenario;
	}

	public String getSolution() {
		return solution;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		if (label == null) {
			throw new IllegalArgumentException("Label is required");
		}
		if (label.length() > 30) {
			throw new IllegalArgumentException("Rebuttal label cannot be more than 30 characters");
		}
		this.label = label;
	}
}
