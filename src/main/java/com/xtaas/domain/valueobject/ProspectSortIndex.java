/**
 * 
 */
package com.xtaas.domain.valueobject;

/**
 * Maintains the sort index for Prospect. This SortIndex is used by Dialer to pick a ProspectCall records based on 
 * these sort indexes
 * 
 * @author djain
 */
public class ProspectSortIndex {
	private int employeeCountIndex;
	private int companyRevenueIndex;
    private int titleIndex;
    private int stateIndex;
    private int industryIndex;
    private int lifoIndex;
    private int fifoIndex;
    private int mlIndex; //machine learning index
    
    public ProspectSortIndex() {
    	//set default value for all Index
    	int defaultIndexValue = 999; //keeping default value as high value, as sort will be always in Ascending order of the index values
    	employeeCountIndex = defaultIndexValue;
    	companyRevenueIndex = defaultIndexValue;
        titleIndex = defaultIndexValue;
        stateIndex = defaultIndexValue;
        industryIndex = defaultIndexValue;
        lifoIndex = defaultIndexValue;
        fifoIndex = defaultIndexValue;
        mlIndex = defaultIndexValue;
    }
    
	/**
	 * @return the employeeCountIndex
	 */
	public int getEmployeeCountIndex() {
		return employeeCountIndex;
	}
	/**
	 * @return the companyRevenueIndex
	 */
	public int getCompanyRevenueIndex() {
		return companyRevenueIndex;
	}
	/**
	 * @return the titleIndex
	 */
	public int getTitleIndex() {
		return titleIndex;
	}
	/**
	 * @return the stateIndex
	 */
	public int getStateIndex() {
		return stateIndex;
	}
	/**
	 * @return the industryIndex
	 */
	public int getIndustryIndex() {
		return industryIndex;
	}
	/**
	 * @return the lifoIndex
	 */
	public int getLifoIndex() {
		return lifoIndex;
	}
	/**
	 * @return the fifoIndex
	 */
	public int getFifoIndex() {
		return fifoIndex;
	}
	/**
	 * @return the mlIndex
	 */
	public int getMlIndex() {
		return mlIndex;
	}
}
