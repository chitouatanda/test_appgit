package com.xtaas.domain.valueobject;

public class RestCampaignTeamDTO {
	private String supervisorId;
	private CallGuide callGuide;

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public CallGuide getCallGuide() {
		return callGuide;
	}

	public void setCallGuide(CallGuide callGuide) {
		this.callGuide = callGuide;
	}

}