package com.xtaas.domain.valueobject;

public enum AgentStatus {
	ACTIVE, //Internal state representation
	INACTIVE, //Internal state representation
	ONLINE, // Taking the Calls
	REQUEST_BREAK, //Request to go ONBREAK
	ONBREAK, // On Break. Not taking the calls
	REQUEST_MEETING, //Request to go in MEETING
	MEETING, //Agent is in Meeting
	REQUEST_TRAINING, //Request to go on TRAINING
	TRAINING, //Agent is in Training
	REQUEST_OFFLINE, //Request to go OFFLINE
	OFFLINE, //Cannot take any more calls
	REQUEST_OTHER, //Request to  go away due to any other reason not listed
	OTHER, //Away for some reason
	OFFLINE_AUTO_LOGOUT;
	
	
	public boolean isRequestStatus() {
		switch (this) {
			case REQUEST_BREAK:
			case REQUEST_MEETING:
			case REQUEST_TRAINING:
			case REQUEST_OFFLINE:
			case REQUEST_OTHER:
				return true;
			default:
				return false;
		}
	}
	
	public boolean isStatusAway() {
		switch (this) {
			case ONBREAK:
			case MEETING:
			case TRAINING:
			case OFFLINE:
			case OTHER:
				return true;
			default:
				return false;
		}
	}
	
	@Override
    public String toString() {
		switch(this) {
			case ACTIVE : return "Active";
			case INACTIVE : return "Inactive";
			case ONLINE : return "Online";
			case REQUEST_BREAK : return "Request Break";
			case ONBREAK: return "On-Break";
			case REQUEST_MEETING : return "Request Meeting";
			case MEETING: return "In-Meeting";
			case REQUEST_TRAINING : return "Request Training";
			case TRAINING: return "In-Training";
			case REQUEST_OFFLINE : return "Request Offline";
        	case OFFLINE: return "Offline";
        	case REQUEST_OTHER : return "Request Other";
        	case OTHER: return "Other";
        	case OFFLINE_AUTO_LOGOUT: return "Offline Auto Logout";
        	default: throw new IllegalArgumentException("Invalid Agent Status: " + this);
		}
    }
}
