/**
 * 
 */
package com.xtaas.domain.valueobject;

import java.util.Date;

import com.xtaas.db.entity.ProspectCall;
import com.xtaas.domain.entity.Agent.agentSkill;
import com.xtaas.web.dto.AgentCampaignDTO;

/**
 * Call which represents Agents who are ONLINE and taking calls
 * 
 * @author djain
 */
public class AgentCall {
	private String agentId;
	private ProspectCall prospectCall;
	private AgentCampaignDTO currentCampaign; //campaign on which this agent is working currently
	private AgentStatus currentStatus; // current status of the Agent
	private Date agentHeartbeatTime; //Time when heartbeat recd from agent's client
	private agentSkill agentType;
	private DialerMode campaignType;
	private boolean isSupervisorApproved;
	
	public AgentCall(String agentId, String agentType,DialerMode campaignType, AgentStatus currentStatus) {
		setAgentId(agentId);
		agentSkill agentskill = agentSkill.AUTO;
		if(agentType!=null && !agentType.isEmpty()){
			if(campaignType.equals(DialerMode.HYBRID) && agentType.equalsIgnoreCase(agentSkill.MANUAL.toString())){
				agentskill = agentSkill.MANUAL;
			}
		}
		setAgentType(agentskill);
		setCurrentStatus(currentStatus);
	}

	public DialerMode getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(DialerMode campaignType) {
		this.campaignType = campaignType;
	}

	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId the agentId to set
	 */
	private void setAgentId(String agentId) {
		if (agentId == null) {
			throw new IllegalArgumentException("AgentId is required");
		}
		this.agentId = agentId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentId == null) ? 0 : agentId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentCall other = (AgentCall) obj;
		if (agentId == null) {
			if (other.agentId != null)
				return false;
		} else if (!agentId.equals(other.agentId))
			return false;
		return true;
	}

	/**
	 * @return the prospectCall
	 */
	public ProspectCall getProspectCall() {
		return prospectCall;
	}

	/**
	 * @param prospectCall the prospectCall to set
	 */
	public void setProspectCall(ProspectCall prospectCall) {
		this.prospectCall = prospectCall;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		if (currentCampaign != null) {
			return currentCampaign.getId();
		} else {
			return null;
		}
	}

	/**
	 * @return the currentCampaign
	 */
	public AgentCampaignDTO getCurrentCampaign() {
		return currentCampaign;
	}

	/**
	 * @param currentCampaign the currentCampaign to set
	 */
	public void setCurrentCampaign(AgentCampaignDTO currentCampaign) {
		this.currentCampaign = currentCampaign;
	}

	/**
	 * @return the currentStatus
	 */
	public AgentStatus getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * @param currentStatus the currentStatus to set
	 */
	public void setCurrentStatus(AgentStatus currentStatus) {
		if (currentStatus == null) {
			throw new IllegalArgumentException("Agent current status is required");
		}
		this.currentStatus = currentStatus;
	}

	/**
	 * @return the agentHeartbeatTime
	 */
	public Date getAgentHeartbeatTime() {
		return agentHeartbeatTime;
	}

	/**
	 * @param agentHeartbeatTime the agentHeartbeatTime to set
	 */
	public void setAgentHeartbeatTime(Date agentHeartbeatTime) {
		this.agentHeartbeatTime = agentHeartbeatTime;
	}

	public agentSkill getAgentType() {
		return agentType;
	}

	public void setAgentType(agentSkill agentType) {
		if (agentType == null) {
			agentType = agentSkill.AUTO;
		}
		this.agentType = agentType;
	}

	public boolean isSupervisorApproved() {
		return isSupervisorApproved;
	}

	public void setSupervisorApproved(boolean isSupervisorApproved) {
		this.isSupervisorApproved = isSupervisorApproved;
	}

	@Override
	public String toString() {
		return "AgentCall [agentId=" + agentId + ", prospectCall=" + prospectCall + ", currentCampaign="
				+ currentCampaign + ", currentStatus=" + currentStatus + ", agentHeartbeatTime=" + agentHeartbeatTime
				+ ", agentType=" + agentType + ", campaignType=" + campaignType + ", isSupervisorApproved="
				+ isSupervisorApproved + "]";
	}
	
}
