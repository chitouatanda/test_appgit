package com.xtaas.domain.valueobject;

public enum CampaignStatus {
	CREATED, // When the campaign is created
	// remove Bidding
	//BIDDING, // When the teams/agents are being searched
	PLANNING, // When the workroom workflow is being executed
	RUNREADY, // Campaign has been planned by Supervisor and now ready to be launched
	RUNNING, // When the campaign is being executed
	PAUSED, // When the campaign is paused
	DELISTED, // When the campaign is stoped before the IO is signed
	STOPPED, // When the campaign is stopped after the IO is signed
	COMPLETED; // When the campaign is completed either because the endDate
				// elapsed or quantity delivered
    @Override
    public String toString() {
      switch(this) {
        case CREATED: return "Created";
     // remove Bidding
       // case BIDDING: return "Bidding";
        case PLANNING: return "Planning";
        case RUNNING: return "Running";
        case PAUSED: return "Paused";
        case DELISTED: return "Delisted";
        case STOPPED: return "Stopped";
        case COMPLETED: return "Completed";
        case RUNREADY: return "RunReady";
        default: throw new IllegalArgumentException();
      }
    }
}