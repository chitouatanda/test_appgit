package com.xtaas.domain.valueobject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xtaas.db.entity.PickListItem;

public class CampaignCriteria  {
	
	private static final Logger logger = LoggerFactory.getLogger(CampaignCriteria.class);
	
	private String managementLevel;
	private String state;
	private String titleKeyWord;
	private String maxRevenue;
	private String minRevenue;
	private String maxEmployeeCount;
	private String minEmployeeCount;
	private String industry;
	private String sicCode;
	private String department;
	private String country;
	private String contactRequirements;
	private Long totalPurchaseReq;
	private boolean primaryIndustriesOnly;
	private int purchaseperOrg;
	private String removeIndustries;
	//private String removeDepartments;
	private String duplicateCompanyDuration;
	private String duplicateLeadsDuration;
	//private String ValidDateMonthDist="3";
	private String ValidDateMonthDist;
	private Long estBuy;
	private Long cstBuy;
	private Long mstBuy;
	private Long pstBuy;
	private int leadPerCompany;
	private List<String> suppressEmail;
	private List<String> suppressComapnies;
	private List<String> suppressDomains;
	private List<String> sortedMgmtSearchList;
	private Date requestJobDate;
	private boolean apiCheck;
	private String campaignId;
	private boolean aborted;
	Map<String,Integer> multiprospectMap;
	private Map<String,String> multiCompanyIdsMap;
	private boolean checkSufficiency;
	private List<String> abmCompanyIds;
	private List<String> abmDomains;
	private List<String> abmCompanys;
	private String negativeKeyWord;
	private boolean isAndTitleIgnored;
	private boolean ignoreDefaultTitles;
	private boolean ignoreDefaultIndustries;
	//private boolean parallelBuy;
	private List<String> countryList;
	private String source;
	private boolean fetchSuccessData;
	private LinkedHashMap<String, String[]> sortedEmpSearchList;
	private LinkedHashMap<String, String[]> sortedRevenuetSearchList;
	private String organizationId;
	private Map<String,Integer> companyDirectMap;
	private Map<String, Integer> companyIndirectMap;
	private String naicsCode;
	private String usStates;
	private String zipcodes;
	private String subIndustries;
	private boolean skipAbmCompany;
	List<Expression> expressions;
	private String phoneType;
	public boolean restFullAPI;
	private List<String> abmCompanyDomain;

	public List<Expression> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<Expression> expressions) {
		this.expressions = expressions;
	}
	
	
	
	public Map<String, Integer> getCompanyDirectMap() {
		return companyDirectMap;
	}
	public void setCompanyDirectMap(Map<String, Integer> companyDirectMap) {
		this.companyDirectMap = companyDirectMap;
	}
	public Map<String, Integer> getCompanyIndirectMap() {
		return companyIndirectMap;
	}
	public void setCompanyIndirectMap(Map<String, Integer> companyIndirectMap) {
		this.companyIndirectMap = companyIndirectMap;
	}
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public LinkedHashMap<String, String[]> getSortedEmpSearchList() {
		return sortedEmpSearchList;
	}
	public void setSortedEmpSearchList(LinkedHashMap<String, String[]> sortedEmpSearchList) {
		this.sortedEmpSearchList = sortedEmpSearchList;
	}
	public LinkedHashMap<String, String[]> getSortedRevenuetSearchList() {
		return sortedRevenuetSearchList;
	}
	public void setSortedRevenuetSearchList(LinkedHashMap<String, String[]> sortedRevenuetSearchList) {
		this.sortedRevenuetSearchList = sortedRevenuetSearchList;
	}
	public boolean isFetchSuccessData() {
		return fetchSuccessData;
	}
	public void setFetchSuccessData(boolean fetchSuccessData) {
		this.fetchSuccessData = fetchSuccessData;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public List<String> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<String> countryList) {
		this.countryList = countryList;
	}
	/*public boolean isParallelBuy() {
		return parallelBuy;
	}
	public void setParallelBuy(boolean parallelBuy) {
		this.parallelBuy = parallelBuy;
	}*/
	public boolean isIgnoreDefaultTitles() {
		return ignoreDefaultTitles;
	}
	public void setIgnoreDefaultTitles(boolean ignoreDefaultTitles) {
		this.ignoreDefaultTitles = ignoreDefaultTitles;
	}
	public boolean isIgnoreDefaultIndustries() {
		return ignoreDefaultIndustries;
	}
	public void setIgnoreDefaultIndustries(boolean ignoreDefaultIndustries) {
		this.ignoreDefaultIndustries = ignoreDefaultIndustries;
	}
	
	
	
	
	public boolean isAndTitleIgnored() {
		return isAndTitleIgnored;
	}
	public void setAndTitleIgnored(boolean isAndTitleIgnored) {
		this.isAndTitleIgnored = isAndTitleIgnored;
	}
	public String getNegativeKeyWord() {
		return negativeKeyWord;
	}
	public void setNegativeKeyWord(String negativeKeyWord) {
		this.negativeKeyWord = negativeKeyWord;
	}
	public List<String> getAbmCompanyIds() {
		return abmCompanyIds;
	}
	public void setAbmCompanyIds(List<String> abmCompanyIds) {
		this.abmCompanyIds = abmCompanyIds;
	}
	public boolean isCheckSufficiency() {
		return checkSufficiency;
	}
	public void setCheckSufficiency(boolean checkSufficiency) {
		this.checkSufficiency = checkSufficiency;
	}
	public Map<String,String> getMultiCompanyIdsMap() {
		return multiCompanyIdsMap;
	}
	public void setMultiCompanyIdsMap(Map<String,String> multiCompanyIdsMap) {
		this.multiCompanyIdsMap = multiCompanyIdsMap;
	}
	public Map<String, Integer> getMultiprospectMap() {
		return multiprospectMap;
	}
	public void setMultiprospectMap(Map<String, Integer> multiprospectMap) {
		this.multiprospectMap = multiprospectMap;
	}
	public boolean isAborted() {
		return aborted;
	}
	public void setAborted(boolean aborted) {
		this.aborted = aborted;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public boolean isApiCheck() {
		return apiCheck;
	}
	public void setApiCheck(boolean apiCheck) {
		this.apiCheck = apiCheck;
	}
	public Date getRequestJobDate() {
		return requestJobDate;
	}
	public void setRequestJobDate(Date requestJobDate) {
		this.requestJobDate = requestJobDate;
	}
	public List<String> getSortedMgmtSearchList() {
		return sortedMgmtSearchList;
	}
	public void setSortedMgmtSearchList(List<String> sortedMgmtSearchList) {
		this.sortedMgmtSearchList = sortedMgmtSearchList;
	}
	public List<String> getSuppressEmail() {
		return suppressEmail;
	}
	public void setSuppressEmail(List<String> suppressEmail) {
		this.suppressEmail = suppressEmail;
	}
	public List<String> getSuppressComapnies() {
		return suppressComapnies;
	}
	public void setSuppressComapnies(List<String> suppressComapnies) {
		this.suppressComapnies = suppressComapnies;
	}
	public List<String> getSuppressDomains() {
		return suppressDomains;
	}
	public void setSuppressDomains(List<String> suppressDomains) {
		this.suppressDomains = suppressDomains;
	}
	public int getLeadPerCompany() {
		return leadPerCompany;
	}
	public void setLeadPerCompany(int leadPerCompany) {
		this.leadPerCompany = leadPerCompany;
	}
	public Long getEstBuy() {
		return estBuy;
	}
	public void setEstBuy(Long estBuy) {
		this.estBuy = estBuy;
	}
	public Long getCstBuy() {
		return cstBuy;
	}
	public void setCstBuy(Long cstBuy) {
		this.cstBuy = cstBuy;
	}
	public Long getMstBuy() {
		return mstBuy;
	}
	public void setMstBuy(Long mstBuy) {
		this.mstBuy = mstBuy;
	}
	public Long getPstBuy() {
		return pstBuy;
	}
	public void setPstBuy(Long pstBuy) {
		this.pstBuy = pstBuy;
	}
	public String getValidDateMonthDist() {
		return ValidDateMonthDist;
	}
	public void setValidDateMonthDist(String validDateMonthDist) {
		this.ValidDateMonthDist = validDateMonthDist;
	}
	public String getDuplicateCompanyDuration() {
		return duplicateCompanyDuration;
	}
	public void setDuplicateCompanyDuration(String duplicateCompanyDuration) {
		this.duplicateCompanyDuration = duplicateCompanyDuration;
	}
	public String getDuplicateLeadsDuration() {
		return duplicateLeadsDuration;
	}
	public void setDuplicateLeadsDuration(String duplicateLeadsDuration) {
		this.duplicateLeadsDuration = duplicateLeadsDuration;
	}
	public String getRemoveIndustries() {
		return removeIndustries;
	}
	public void setRemoveIndustries(String removeIndustries) {
		this.removeIndustries = removeIndustries;
	}
	/*public String getRemoveDepartments() {
		return removeDepartments;
	}
	public void setRemoveDepartments(String removeDepartments) {
		this.removeDepartments = removeDepartments;
	}*/
	public int getPurchaseperOrg() {
		return purchaseperOrg;
	}
	public void setPurchaseperOrg(int purchaseperOrg) {
		this.purchaseperOrg = purchaseperOrg;
	}
	public boolean isPrimaryIndustriesOnly() {
		return primaryIndustriesOnly;
	}
	public void setPrimaryIndustriesOnly(boolean primaryIndustriesOnly) {
		this.primaryIndustriesOnly = primaryIndustriesOnly;
	}
	public String getManagementLevel() {
		return managementLevel;
	}
	public void setManagementLevel(String managementLevel) {
		this.managementLevel = managementLevel;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTitleKeyWord() {
		return titleKeyWord;
	}
	public void setTitleKeyWord(String titleKeyWord) {
		this.titleKeyWord = titleKeyWord;
	}
	public String getMaxRevenue() {
		return maxRevenue;
	}
	public void setMaxRevenue(String maxRevenue) {
		this.maxRevenue = maxRevenue;
	}
	public String getMinRevenue() {
		return minRevenue;
	}
	public void setMinRevenue(String minRevenue) {
		this.minRevenue = minRevenue;
	}
	public String getMaxEmployeeCount() {
		return maxEmployeeCount;
	}
	public void setMaxEmployeeCount(String maxEmployeeCount) {
		this.maxEmployeeCount = maxEmployeeCount;
	}
	public String getMinEmployeeCount() {
		return minEmployeeCount;
	}
	public void setMinEmployeeCount(String minEmployeeCount) {
		this.minEmployeeCount = minEmployeeCount;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getSicCode() {
		return sicCode;
	}
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getContactRequirements() {
		return contactRequirements;
	}
	public void setContactRequirements(String contactRequirements) {
		this.contactRequirements = contactRequirements;
	}
	public Long getTotalPurchaseReq() {
		return totalPurchaseReq;
	}
	public void setTotalPurchaseReq(Long totalPurchaseReq) {
		this.totalPurchaseReq = totalPurchaseReq;
	}
	public String getNaicsCode() {
		return naicsCode;
	}
	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}
	public String getUsStates() {
		return usStates;
	}
	public void setUsStates(String usStates) {
		this.usStates = usStates;
	}
	public String getZipcodes() {
		return zipcodes;
	}
	public void setZipcodes(String zipcodes) {
		this.zipcodes = zipcodes;
	}
	public String getSubIndustries() {
		return subIndustries;
	}
	public void setSubIndustries(String subIndustries) {
		this.subIndustries = subIndustries;
	}
	public boolean isSkipAbmCompany() {
		return skipAbmCompany;
	}
	public void setSkipAbmCompany(boolean skipAbmCompany) {
		this.skipAbmCompany = skipAbmCompany;
	}

	public List<String> getAbmDomains() {
		return abmDomains;
	}

	public void setAbmDomains(List<String> abmDomains) {
		this.abmDomains = abmDomains;
	}

	public List<String> getAbmCompanys() {
		return abmCompanys;
	}

	public void setAbmCompanys(List<String> abmCompanys) {
		this.abmCompanys = abmCompanys;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public boolean isRestFullAPI() {
		return restFullAPI;
	}

	public void setRestFullAPI(boolean restFullAPI) {
		this.restFullAPI = restFullAPI;
	}

	public List<String> getAbmCompanyDomain() {
		return abmCompanyDomain;
	}

	public void setAbmCompanyDomain(List<String> abmCompanyDomain) {
		this.abmCompanyDomain = abmCompanyDomain;
	}
}

