package com.xtaas.domain.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactSocialMedia {

	private String twitterUrl;
	
	private String facebookUrl;
	
	private String linkedinUrl;

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}
	
	
}
