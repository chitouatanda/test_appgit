package com.xtaas.domain.valueobject;

public enum TeamSortClauses {
	AverageRating("averageRating"), 
	PerLeadPrice("perLeadPrice"), 
	TotalCompletedCampaigns("totalCompletedCampaigns"), 
	AverageAvailableHours("averageAvailableHours"), 
	TeamSize("teamSize");
	
	private final String fieldName;
	
	TeamSortClauses(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}
}
