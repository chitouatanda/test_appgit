package com.xtaas.domain.valueobject;

public enum DialerMode {
	PREVIEW, POWER, HYBRID, PREVIEW_SYSTEM, RESEARCH_AND_DIAL, RESEARCH, MANUALDIAL;

	@Override
	public String toString() {
		switch (this) {
		case PREVIEW:
			return "Agent Preview";
		case PREVIEW_SYSTEM:
			return "System Preview";
		case POWER:
			return "Power";
		case HYBRID:
			return "Hybrid";
		case RESEARCH_AND_DIAL:
			return "Research and Dial";
		case RESEARCH:
			return "Research";
		case MANUALDIAL:
			return "Manualdial";
		default:
			throw new IllegalArgumentException();
		}
	}

}
