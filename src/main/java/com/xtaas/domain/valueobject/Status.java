package com.xtaas.domain.valueobject;

public enum Status {
	ACTIVE, //Internal state representation
	INACTIVE, //Internal state representation
	IDLE, // When no campaign is allocated to an agent,qa
	GEARING, // Getting ready for the call. Preparing for the campaign
	ONLINE, // Taking the Calls 
	AWAY, // Away in some official work. Not taking the calls
	ONBREAK, // On Break. Not taking the calls
	OFFLINE; // Signed out 
	
	@Override
    public String toString() {
		switch(this) {
			case ACTIVE : return "Active";
			case INACTIVE : return "Inactive";
			case IDLE : return "Idle";
			case GEARING : return "Gearing";
			case ONLINE : return "Online";
        	case AWAY: return "Away";
        	case ONBREAK: return "On-Break";
        	case OFFLINE: return "Offline";
        	default: throw new IllegalArgumentException("Invalid Status: " + this);
		}
    }
}
