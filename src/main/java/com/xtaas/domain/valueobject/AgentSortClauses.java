package com.xtaas.domain.valueobject;

public enum AgentSortClauses {
	PerHourPrice("perHourPrice"),
	TotalCompletedCampaigns("totalCompletedCampaigns"), 
	AverageAvailableHours("averageAvailableHours"), 
	AverageRating("averageRating");
	
	private final String fieldName;
	
	AgentSortClauses(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}
}
