<%@ page contentType="text/xml" %><%@ page import="java.util.*" %><%@ page import="com.xtaas.channel.*" %><%@ page import="com.xtaas.utils.*" %><%@ page import="com.twilio.sdk.resource.instance.*"%><%@ page import="com.twilio.sdk.resource.list.*"%><%@ page import="com.twilio.sdk.resource.factory.*"%><%@ page import="org.apache.commons.lang.StringUtils"%><?xml version="1.0" encoding="UTF-8"?>

<Response>
    <Dial>
        <Conference beep="false" endConferenceOnExit="true" startConferenceOnEnter="true">TESTCONF</Conference>
    </Dial>
</Response>
<%
	Channel channelTwilio = ChannelFactory.createChannel(ChannelType.TWILIO);
	Account account = channelTwilio.getChannelConnection();
	CallFactory callFactory = account.getCallFactory(); //callfactory to make outbound call.
	
	String phone=request.getParameter("pn");
	
	Map<String, String> callParams = new HashMap<String, String>();
	callParams.put("To", phone); // lead's phone number
	callParams.put("From", "415-403-2426"); // Twilio number
	callParams.put("IfMachine", "Continue"); // to detect if phone is picked by human or machine
	
	//Commented out - StatusCallback not reliable
	//String statusCallbackUrl = ApplicationEnvironmentPropertyUtils.getServerBaseUrl() + "/spr/dialer/statuscallback?pcid="+pcid+"&agentId="+agentId;
	/*callParams.put("StatusCallback", statusCallbackUrl);
	callParams.put("StatusCallbackMethod", "POST");
	callParams.put("StatusCallbackEvent", "initiated");
	callParams.put("StatusCallbackEvent", "ringing");
	//callParams.put("StatusCallbackEvent", "answered");
	callParams.put("StatusCallbackEvent", "completed");*/
	
	StringBuilder dialXMLUrl = new StringBuilder("http://shrouded-fortress-2842.herokuapp.com/twiml/testdialtoconf.xml.jsp");
	callParams.put("Url", dialXMLUrl.toString());
	
	// Make the call
	Call call;
	try {
		call = callFactory.create(callParams);
		System.out.println("***CALL SID: " + call.getSid());
	} catch (Exception e) {
		e.printStackTrace();
	} 

%>