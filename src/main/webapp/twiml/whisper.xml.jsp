<%@ page contentType="text/xml" %><%@ page import="java.util.*" %><?xml version="1.0" encoding="UTF-8"?>
<Response>
	<%
		String conferenceName=request.getParameter("cn");
		String wmsg = request.getParameter("wmsg");
		if (wmsg != null) {
	%>
			<Play>/sounds/whisper_beep.mp3</Play>
	<%  } %>
			
		<Dial>
   			<Conference muted="true" beep="false" waitUrl="" endConferenceOnExit="false"><%=conferenceName%></Conference>
		</Dial>
</Response>