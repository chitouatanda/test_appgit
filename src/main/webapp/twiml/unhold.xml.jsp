<%@ page contentType="text/xml" %><?xml version="1.0" encoding="UTF-8" ?>
<%
   	String agentId = request.getParameter("agentId");
	String prospectCallId = request.getParameter("pcid");
	String conferenceName = agentId + "_" + prospectCallId;
	String actionUrl = "/spr/dialer/savecallmetrics?pcid="+prospectCallId+"&amp;agentid="+agentId;
%>
<Response>
    <Dial action="<%=actionUrl%>">
       	<Conference beep="false" endConferenceOnExit="false" waitUrl=""><%=conferenceName%></Conference>
     </Dial>
</Response>