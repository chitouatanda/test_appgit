<%-- <%@ page contentType="text/xml" %><%@ page import="java.util.*" %><%@ page import="com.xtaas.channel.*" %><%@ page import="com.xtaas.utils.*" %><%@ page import="com.twilio.sdk.resource.instance.*"%><%@ page import="com.twilio.sdk.resource.list.*"%><%@ page import="com.twilio.sdk.resource.factory.*"%><%@ page import="org.apache.commons.lang.StringUtils"%><%@ page import="com.xtaas.domain.valueobject.*"%><?xml version="1.0" encoding="UTF-8"?>
<Response>
    <%
    	String caller = request.getParameter("Caller");
		String callerAgentId = null;
		if (caller != null && caller.startsWith("client:")) { //sent when PREVIEW call is initiated
			callerAgentId = StringUtils.replace(caller, "client:", "");
		}
		
		String connectToAgent = (String) request.getAttribute("connectToAgent");
		if (callerAgentId == null && connectToAgent != null) { //sent in POWER mode. Caller is an OUTGOING number 
			callerAgentId = connectToAgent;
		}
    	String prospectCallId = request.getParameter("pcid");
   		String actionUrl = "/spr/dialer/savecallmetrics?pcid="+prospectCallId+"&amp;agentid="+callerAgentId;
   		String confEndEventCallbackUrl = "/spr/dialer/confendevent?pcid="+prospectCallId;
    	
   		String campaignId = (String) request.getAttribute("campaignId");
   		if (campaignId != null) {
   			actionUrl = actionUrl + "&amp;campaignId="+campaignId;
   		}
   		
   		String answeredBy = request.getParameter("AnsweredBy");
		System.out.println("prospectCallId = " + prospectCallId + " AnsweredBy = " + answeredBy);
		if (answeredBy != null && answeredBy.equals("machine")) { //picked by Answering Machine, leave a VM
	%>
			<Redirect method="POST"><%=actionUrl%></Redirect>
	<%		
		} else {
			String isSupervisorJoinRequest = request.getParameter("supervisorjoin");
           	String phone=request.getParameter("pn");
           	if (phone == null) phone = connectToAgent;
           	String isAgent=request.getParameter("agent");
           	if (null == isAgent) isAgent = "false";
           	
           	if (phone != null) {
       			if (phone.equals("ivr")) { //<Say voice="woman">We are sorry. Currently, all agents are busy attending other customers.</Say>
	%>
				    <Gather timeout="10" numDigits="1" action="<%=actionUrl%>&amp;abandoned=1" method="POST">
	        			<Say><%=XtaasConstants.TWILIO_ABANDON_OPTOUT_MSG%></Say>
	    			</Gather>
	    			<Redirect method="POST"><%=actionUrl%>&amp;abandoned=1</Redirect>
	<% 			} else { 
       				if(phone.matches("[\\?:(0-9-+\\?:)]*")){
       					String callerId = request.getParameter("callerId"); //for preview call, it is sent by the agent screen
       					String confName = callerAgentId + "_" + prospectCallId;
	%>
	        			<Dial callerId="<%= callerId%>" action="<%=actionUrl%>">
       						<Conference record="record-from-start" eventCallbackUrl="<%=confEndEventCallbackUrl%>" beep="false" endConferenceOnExit="true" waitUrl="/twiml/ringing_phone.xml.jsp"><%= confName%></Conference>
       					</Dial>
	<%
           			} else if (isSupervisorJoinRequest != null && isSupervisorJoinRequest.equals("true")) {
           				String prospectInteractionSessionId = request.getParameter("pisid");
           				actionUrl = "/spr/dialer/savecallmetrics?supervisorjoin="+isSupervisorJoinRequest+"&amp;pcid="+prospectCallId+"&amp;pisid="+prospectInteractionSessionId;
           				String confName = phone + "_" + prospectCallId; //phone is agentId in this case
           				String dialerMode = (String) request.getAttribute("agentsCampaignDialerMode"); //only set in case of supervisorJoinRequest
           				if (dialerMode.equalsIgnoreCase(DialerMode.POWER.name())) {
       						confName = phone;
       					}
    %>           				
           				<Dial action="<%=actionUrl%>">
    	    				<Conference muted="true" beep="false" endConferenceOnExit="false" waitUrl=""><%= confName%></Conference>
	        			</Dial>
    <%	        			
           			} else {
           				String callerId = request.getParameter("callerId"); //for power call, it is configured in CallJob
           				/*	START
           					DATE: 17-10-2017
           					REASON: Added to disable recording for conference and record only dialed numbers.
           							Initially below record field was true due to which it was recording conference. */
           				boolean recordDial = false;
						if (callerId != null) {
							recordDial = true;
						}
						String recordingStatusCallbackUrl = "/spr/dialer/confendevent?pcid=" + prospectCallId;
	%>
	        			<Dial callerId="<%= callerId%>" record="<%=recordDial%>" recordingStatusCallback="<%=recordingStatusCallbackUrl%>" action="<%=actionUrl%>">
	        				<Conference muted="false" beep="false" endConferenceOnExit="<%=isAgent%>" waitUrl=""><%= phone%></Conference>
	        			</Dial>
	<%
           			} // end if - phone matches "\\d*"
       			} //end if - phone.equals("ivr")
           	} //end if - phone != null
   		} //end if - answeredBy 
   	%>
</Response> --%>