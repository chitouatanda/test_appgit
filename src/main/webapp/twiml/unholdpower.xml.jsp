<%@ page contentType="text/xml" %><?xml version="1.0" encoding="UTF-8"?>
<%
   	String conferenceName = request.getParameter("agentId");
	String prospectCallId = request.getParameter("pcid");
	String actionUrl = "/spr/dialer/savecallmetrics?pcid="+prospectCallId+"&amp;agentid="+conferenceName;
%>
<Response>
    <Dial record="true" action="<%=actionUrl%>">
        <Conference beep="false" endConferenceOnExit="false" waitUrl=""><%=conferenceName%></Conference>
    </Dial>
</Response>