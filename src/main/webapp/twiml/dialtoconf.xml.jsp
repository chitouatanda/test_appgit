<%@ page contentType="text/xml" %><?xml version="1.0" encoding="UTF-8"?>
<%
   	String conferenceName = request.getParameter("cn");
	String pcid = request.getParameter("pcid");
	String actionUrl = "/spr/dialer/savecallmetrics?pcid="+pcid;
	String confEndEventCallbackUrl = "/spr/dialer/confendevent?pcid="+pcid;
	String msg = request.getParameter("msg");
	String digit = request.getParameter("digit");
%>
<Response>
<%
	if (msg != null) {
%>
		<Say><%=msg%></Say>
<%  } %>
<% if (digit != null) {%>
		<Play digits="<%=digit%>"/>
<% } %>

    <Dial action="<%=actionUrl%>">
        <Conference beep="false" endConferenceOnExit="false" waitUrl=""><%=conferenceName%></Conference>
    </Dial>
</Response>