<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="com.twilio.sdk.client.*" %>
<%
final String ACCOUNT_SID = "ACab4d611aa1465a246cb58290ba930a5c";
final String AUTH_TOKEN = "8096913a4808bc8287d5a7b0b8332d22";

TwilioCapability capability = new TwilioCapability(ACCOUNT_SID, AUTH_TOKEN);
capability.allowClientOutgoing("AP63ad67962147bcdf4f2bccdc795504f3");
capability.allowClientIncoming("alice");
String token = null;
try {
    token = capability.generateToken();
} catch (Exception e) {
    e.printStackTrace();
}
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Screen</title>
<script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<link href="//static0.twilio.com/packages/quickstart/client.css" type="text/css" rel="stylesheet" />
    
<script type="text/javascript"><!--
    $(document).ready(function(){
	    /* Create the Client with a Capability Token */
	    Twilio.Device.setup("<%=token%>", {debug: true});
	
	    var connection=null;
		$("#call").click(function() {  
			params = { "pn" : $('#tocall').val()};
			connection = Twilio.Device.connect(params);
		});
	
		$("#hangup").click(function() {  
			Twilio.Device.disconnectAll();
		});
	 
	    /* Let us know when the client is ready. */
	    Twilio.Device.ready(function (device) {
	        $("#status").text("Ready");
	    });
	
	    /* Listen for incoming connections */
	    Twilio.Device.incoming(function (conn) {
	      if (confirm('Accept incoming call from ' + conn.parameters.From + '?')){
				connection=conn;
			    conn.accept(); // accept the incoming connection and start two-way audio
		  }
	    });
	
	    Twilio.Device.offline(function (device) {
			$('#status').text('Offline');
		});
		    
	    /* Report any errors on the screen */
	    Twilio.Device.error(function (error) {
	        $("#status").text("Error: " + error.message);
	    });
	 
	    Twilio.Device.connect(function (conn) {
	        $("#status").text("Successfully established call");
	        toggleCallStatus();
	    });
	
	    Twilio.Device.disconnect(function (conn) {
	        $("#status").text("Call ended");
	        toggleCallStatus();
	    });
	 
	    function toggleCallStatus(){
			$('#call').toggle();
			$('#hangup').toggle();
			$('#dialpad').toggle();
		}
    
    	$.each(['0','1','2','3','4','5','6','7','8','9','star','pound'], function(index, value) { 
    		$('#button' + value).click(function(){ 
				if(connection) {
					if (value=='star')
						connection.sendDigits('*')
					else if (value=='pound')
						connection.sendDigits('#')
					else
						connection.sendDigits(value)
					return false;
				} 
			});
		});
    });
</script>
</head>
<body>
	<div align="center">
		<!-- @start snippet -->
		
		Number to call: <input type="text" id="tocall" value="">
		<input type="button" id="call" value="Start Call"/>
		<input type="button" id="hangup" value="Hangup Call" style="display:none;"/>
		<div id="status">
			Offline
		</div>
		<!-- @end snippet -->
	</div>
</body>
</html>