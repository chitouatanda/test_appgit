define(['angular', 'angular-resource'], function(angular) {
	angular.module('partner-service', ['ngResource'])
		.factory("PartnerService",function($resource) {
			return $resource("/spr/partner/all",{},{
				query: {
		            method: 'GET',
		            isArray:true
				}
			});
		})
});