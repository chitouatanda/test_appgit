define(['angular', 'agent/agent-service','campaign/campaign-service', 'angular-localStorage','synchronous-service','notification-service'], function(angular) {
	angular.module('AgentSplashController', ['agent-service', 'campaign-service', 'ngStorage','synchronous-service','notification-service'])
	.controller('AgentSplashController', [
	                                      '$scope',
	                                      '$rootScope',
	                                      '$location',
	                                      '$q',
	                                      '$log',
	                                      '$http',
	                                      '$window',
	                                      'currentCampaignService',
	                                      'agentStatusService',
	                                      'GlobalCampaignTypes',
	                                      'getDisplayLabelService',
	                                      '$modal',
	                                      'getTrainingDocs',
	                                      '$localStorage',
	                                      'getDisplayLabelService',
										  'synchronousService',
										  'LoginService',
										  'NotificationService',
										  'agentDetails',
										  '$sce',
										  'PlivoService',
										  'user',
										  '$timeout',
										  'GetMissedCallbackService',
										  '$sessionStorage',
	                                      function ($scope, $rootScope, $location, $q, $log, $http, $window, currentCampaignService, agentStatusService, GlobalCampaignTypes, getDisplayLabelService, $modal, getTrainingDocs, $localStorage, getDisplayLabelService, synchronousService, LoginService, NotificationService,agentDetails,$sce, PlivoService,user,$timeout, GetMissedCallbackService, $sessionStorage) {
											  $scope.loading = true;
											  $scope.isOnlineButtonDisable = false;
											  $scope.agentDetails = agentDetails;
											  $scope.showAlert = false;
												$scope.scriptSelectedTab="questions";
												
												$rootScope.$on('onCallBackEventReceived', function (event, prospectName) {
													$rootScope.callbackMessage = "There is callback scheduled with " + prospectName;
												})

												if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("AGENT")) {
													$state.go('login');
													return;
												}
												if($localStorage.validUser == null || $localStorage.validUser == undefined || $localStorage.validUser == '') {
													var temp = LoginService.logout();
													temp.then(function() {
														$rootScope.isUserLoggedIn = false;				
														$localStorage.$reset();	
														$location.path('/login');
													});
												}	
											  currentCampaignService.get({}).$promise.then(function(data) {
	                                    		  $rootScope.agent = {
	                                    				  "currentCampaign":data
	                                    		  };
	                                    		  $scope.campaignTypes = GlobalCampaignTypes.getGlobalCampaignTypes();
	                                        	  for(i=0; i<$scope.campaignTypes.length;i++){
	                                        		  if($scope.campaignTypes[i].value == $rootScope.agent.currentCampaign.type) {
	                                        			  $rootScope.agent.currentCampaign.type = $scope.campaignTypes[i].key;
	                                        		  }
												  }
												  
												  $scope.isMobileTablet = function() {
													var check = false;
													(function(a){
														if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) 
															check = true;
													})(navigator.userAgent||navigator.vendor||window.opera);
													return check;
												}
												
												$scope.createEndPoint = function() {
													if ($scope.getCallControlProvider() == 'Plivo') {
														if ($scope.plivoWebSdk == null || !$scope.plivoWebSdk.client.isLoggedIn){
															PlivoService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
																if (endpoint.PLIVO_ENDPOINT1 && endpoint.PLIVO_ENDPOINT2) {
																	$scope.plivoEndpointId = endpoint.PLIVO_ENDPOINT_ID;
																	$scope.plivoEndpointUsername = endpoint.PLIVO_ENDPOINT1;
																	$scope.plivoInitSplashPhone();
																	var toNumber = user.id;
																	var extraHeaders = {
																		'X-PH-from': 'client:' + user.id,
																		'X-PH-agentId' : user.id,
																		'X-PH-agentType' : '',
																		'X-PH-campaignId' : $rootScope.agent.currentCampaign.id
																	};
																	$scope.setSplashPlivoToNumberAndHeaders(toNumber, extraHeaders);
																	$scope.splashPlivoLogin(endpoint.PLIVO_ENDPOINT1, endpoint.PLIVO_ENDPOINT2);
																}
															}, function (error) {
																NotificationService.log('error', 'An error occurred while creating plivo endpoint.');
															});
														}
													}
												}


	                                        	  getDisplayLabelService.get({labelentity: "DIALERMODE" ,key:$rootScope.agent.currentCampaign.dialerMode}).$promise.then( function(data) {
	  												angular.forEach(data,  function(value, key){
	  													if(key == $rootScope.agent.currentCampaign.dialerMode) {
	  														$rootScope.agent.currentCampaign.dialerModeLabel  = value;	
	  													}												
	  												});												 	
	                                        	  });	
												  if ($rootScope.agent.currentCampaign != null && $rootScope.agent.currentCampaign != undefined && $rootScope.agent.currentCampaign.callGuide != null && $rootScope.agent.currentCampaign.callGuide != undefined) {
														$scope.confirmSectionScript =  $rootScope.agent.currentCampaign.callGuide.confirmSection != null ? $sce.trustAsHtml($rootScope.agent.currentCampaign.callGuide.confirmSection.script) : null;
														$scope.questionSectionScript = $rootScope.agent.currentCampaign.callGuide.questionSection != null ? $sce.trustAsHtml($rootScope.agent.currentCampaign.callGuide.questionSection.script) : null;
														$scope.consentSectionScript = $rootScope.agent.currentCampaign.callGuide.consentSection != null ? $sce.trustAsHtml($rootScope.agent.currentCampaign.callGuide.consentSection.script) : null;
														$scope.noteSectionScript = $rootScope.agent.currentCampaign.callGuide.noteSection != null ? $sce.trustAsHtml($rootScope.agent.currentCampaign.callGuide.noteSection.script) : null;
												  }
												  if ($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
													&& $scope.agent.currentCampaign.networkCheck) {
														$scope.createEndPoint();
														$scope.showGoOnlineButton = false;
													} else {
														$scope.showGoOnlineButton = true;
													}

	                                        	  $scope.loading = false;
											  });
											  
											  $scope.plivoRefreshSettings = function() {
												var plivoSettings = {
													"debug" : "ERROR",
													"permOnClick" : true,
													"audioConstraints" : {
														"optional" : [ {
															"googAutoGainControl" : false
														}, {
															"googEchoCancellation" : false
														} ]
													},
													"enableTracking" : true,
													"dscp" : true,
													"clientRegion" : "asia",
													"codecs" : [ "OPUS", "PCMU" ],
													"enableIPV6" : false
												}
												return plivoSettings;
											}
							
											$scope.plivoInitSplashPhone = function() {
												var plivoSettings = $scope.plivoRefreshSettings();
												$scope.plivoWebSdk = new window.Plivo(plivoSettings);
												$scope.plivoWebSdk = new window.Plivo(plivoSettings);
												$scope.plivoWebSdk.client.on('onWebrtcNotSupported', $scope.onSplashWebrtcNotSupported);
												$scope.plivoWebSdk.client.on('onLogin', $scope.onSplashLogin);
												$scope.plivoWebSdk.client.on('onLogout', $scope.onSplashLogout);
												$scope.plivoWebSdk.client.on('onLoginFailed', $scope.onSplashLoginFailed);
												$scope.plivoWebSdk.client.on('onCallFailed', $scope.onSplashCallFailed);
												$scope.plivoWebSdk.client.on('mediaMetrics', $scope.splashMediaMetrics);
												$scope.plivoWebSdk.client.on('onConnectionChange', $scope.onSplashConnectionChange);
												$scope.plivoWebSdk.client.setRingTone(true);
												$scope.plivoWebSdk.client.setRingToneBack(true);
												$scope.plivoWebSdk.client.setConnectTone(true);
												$scope.plivoWebSdk.client.setDebug("ERROR");
												$scope.checkBrowserComplaince($scope.plivoWebSdk.client);
												console.log('PLIVO - phone initialized.');
											}

											$scope.getCallControlProvider = function() {
												return $scope.agent.currentCampaign.callControlProvider;
											}

											$scope.checkBrowserComplaince = function(client) {
												if (client.browserDetails.browser != "chrome") {
													$scope.customAlert('error', 'Please use latest version of chrome.');
												}
											}
							
											$scope.splashPlivoLogin = function(username, password) {
												console.info('PLIVO - login initiated.');
												$scope.plivoWebSdk.client.login(username, password);
											}
							
											$scope.plivoLogout = function() {
												console.info('PLIVO - logout initiated.');
												if ($scope.plivoWebSdk && $scope.plivoWebSdk.client && $scope.plivoWebSdk.client.isLoggedIn) {
													$scope.plivoWebSdk.client.logout();
												}
											}

											$scope.splashPlivoCall = function(to, extraHeaders) {
												console.info('PLIVO - call initiated.');
												if (!$scope.plivoWebSdk.client.isLoggedIn) {
													$scope.customAlert('error', 'PLIVO - You are not Logged in. Please try again.');
													return;
												}
												$scope.plivoWebSdk.client.call(to, extraHeaders);
											}

											$scope.onSplashWebrtcNotSupported = function() {
												$scope.customAlert('error', 'Webrtc is not supported in this broswer, Please use latest version of chrome/firefox.');
											}
							
											$scope.onSplashLogin = function() {
												console.info('PLIVO - ' + $scope.plivoWebSdk.client.userName + ' logged in.');
												$scope.splashPlivoCall($scope.plivoToNumber, $scope.plivoExtraHeaders);
											}
							
											$scope.onSplashLogout = function() {
												console.info('PLIVO - user logout successfully.');
											}
							
											$scope.onSplashLoginFailed = function(reason) {
												var loginFailedReason = 'PLIVO - user login failed. Reason - ' + reason;
												$scope.customAlert('error', loginFailedReason);
											}
							
											$scope.onSplashCallFailed = function(cause, callInfo) {
												var callFailedReason = 'PLIVO - call failed. Reason - ' + cause;
												if (cause && /Denied Media/i.test(cause)) {
													$scope.customAlert('error', 'PLIVO - Media access blocked.');
												} else {
													// $scope.customAlert('error', callFailedReason);
													if (callInfo.callUUID == null && callInfo.state == 'failed') {
														$scope.invalidCall('invalid-phone',callInfo);
														console.info('PLIVO - onCallFailed : ', callInfo);
														console.info('PLIVO - onCallFailed : ', cause);
													}
												}
											}

											$scope.logOutAgent = function() {
												var loggedOut = LoginService.logout();
													loggedOut.then(function() {	
														$location.path('/login');
													});
											}

											var averageBandWidthCheck = 0;
											var lowBandWidthCheck = 0;
											var mediaMethodCall = 0;
											$scope.splashMediaMetrics = function(event) {
												mediaMethodCall++;
												console.log('PLIVO - mediaMetrics : ' + JSON.stringify(event));
												if ((event.type == "high_jitter" && event.value >= 30) || 
                        							(event.type == "high_packetloss" && event.value >= 0.9) || 
                        							(event.type == "high_rtt" && event.value >= 300)) {
													lowBandWidthCheck++;
													if (lowBandWidthCheck > 3) {
														NotificationService.log('error', 'Your network is unsuitable to be working with XTaaS, please check network and relogin.');
														$scope.refreshPlivoForDesktop();
														$scope.logOutAgent();
													}
                    						}

											if ((event.type == "high_jitter" && (event.value >= 10 && event.value <= 30)) || 
												(event.type == "high_packetloss" && (event.value >= 0.5 && event.value <= 0.9)) || 
												(event.type == "high_rtt" && (event.value >= 200 && event.value <= 300))) {
													averageBandWidthCheck++;
													if (averageBandWidthCheck > 3) {
														$scope.showGoOnlineButton = true;
														NotificationService.log('error', 'Unstable network detected - Please check network.');
													}
												}

												if (lowBandWidthCheck == 0 && mediaMethodCall > 3) {
													// NotificationService.log('success', 'Network test pass.');
													$scope.showGoOnlineButton = true;
												}

												// if (lowBandWidthCheck > 1 && mediaMethodCall > 3) {
												// 	NotificationService.log('success', 'Network test pass.');
												// 	$scope.showGoOnlineButton = true;
												// }
											}
							
											$scope.onSplashConnectionChange = function(obj) {
												console.info('PLIVO - onConnectionChange: ', obj);
												if (obj.state === "connected") {
													console.log('PLIVO - onConnectionChange : ' + obj);
												} else if (obj.state === "disconnected") {
													var connectionDisconnectError = obj.state + " " + obj.eventCode + " " + obj.eventReason;
													$scope.customAlert('error', connectionDisconnectError);
												} else {
													$scope.customAlert('error', 'PLIVO - Unknown connection state.');
												}
											}
							
											$scope.customAlert = function(type, alertMessage) {
												NotificationService.clear();
												NotificationService.log(type, alertMessage);
											}
							
											$scope.setSplashPlivoToNumberAndHeaders = function(toNumber, extraHeaders) {
												$scope.plivoToNumber = toNumber;
												$scope.plivoExtraHeaders = extraHeaders;
											}
							
											$scope.resetPlivoToNumberAndHeaders = function() {
												$scope.plivoToNumber = '';
												$scope.plivoExtraHeaders = {};
											}
							
											$scope.refreshPlivoForDesktop = function() {
												$scope.plivoLogout();
												// var deletePlivoEndpointUrl = "/spr/plivo/endpoint/" + $scope.plivoEndpointId;
												// synchronousService(deletePlivoEndpointUrl, "DELETE", null);
												// 	$location.path( "/agent");
												PlivoService.deleteEndpoint({endpointId : $scope.plivoEndpointId}).$promise.then(function (data) {
													$location.path( "/agent");
												}, function (error) {
													console.log("Error occured in deleting endpoint.");
												});
											}

											$scope.refreshPlivoForMobile = function() {
												$scope.plivoLogout();
												PlivoService.deleteEndpoint({endpointId : $scope.plivoEndpointId}).$promise.then(function (data) {
													$location.path( "/mobileagent");
												}, function (error) {
													console.log("Error occured in deleting endpoint.");
												});
											}
											  
											$scope.goOnline = function() {
												if ($scope.plivoBrowserCheck()) {
													if ($scope.isMobileTablet()) {
														if ($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
															&& $scope.agent.currentCampaign.networkCheck) {
																$scope.refreshPlivoForMobile();
															} else {
																$location.path( "/mobileagent");
															}
													}
												}else {
													$scope.navigateAgentToLoginPage();
												}
											  }
											  
	                                    	  $scope.goOnlineDesktop = function() {
												$rootScope.callbackMessage = "";
												if ($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
													&& $scope.agent.currentCampaign.enableCallbackFeature) {
														if ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined) {
															GetMissedCallbackService.query({agentId : user.id}).$promise.then(function(callbackProspect) {
																if (callbackProspect.prospect != null && callbackProspect.prospect != undefined) {
																	$sessionStorage.callbackProspect = callbackProspect;
																	$sessionStorage.missedCallback = true;
																} else {
																	$sessionStorage.callbackProspect = null;
																	$sessionStorage.missedCallback = false;
																}
																$scope.checkLiteModeAndNetworkCheck();
															});
														} else {
															$scope.checkLiteModeAndNetworkCheck();
														}
													} else {
														$scope.checkLiteModeAndNetworkCheck();
													}
											  }

											  $scope.checkLiteModeAndNetworkCheck = function() {
												if ($scope.plivoBrowserCheck()) {
													if ($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
														&& $scope.agent.currentCampaign.networkCheck) {
															$scope.refreshPlivoForDesktop();
														} else {
															if($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined &&  
																$scope.agent.currentCampaign.dialerMode != null && $scope.agent.currentCampaign.dialerMode != undefined && $scope.agent.currentCampaign.dialerMode != "RESEARCH"
																&& $scope.agent.currentCampaign.liteMode && !$scope.agent.currentCampaign.autoGoldenPass && $scope.agentDetails.agentType == 'AUTO' ) {
																	$scope.showAlert = true;
																	$timeout(function(){
																		var temp = LoginService.logout();
																		temp.then(function() {	
																			$location.path('/login');
																		});
																	},10000);
															} else {
																$scope.isOnlineButtonDisable = true;
																$location.path( "/agent");
															}
														}
												} else {
													$scope.navigateAgentToLoginPage();
												}
											  }
											  
											  $scope.goOnlineMobileDesktop = function() {
												$rootScope.callbackMessage = "";
												if ($scope.plivoBrowserCheck()) {
													if ($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
														&& $scope.agent.currentCampaign.networkCheck) {
															$scope.refreshPlivoForMobile();
														} else {
															if($scope.agent != null && $scope.agent != undefined && $scope.agent.currentCampaign != null && $scope.agent.currentCampaign != undefined 
																&& $scope.agent.currentCampaign.liteMode && !$scope.agent.currentCampaign.autoGoldenPass && $scope.agentDetails.agentType == 'AUTO') {
																	$scope.showAlert = true;
																	$timeout(function(){
																		var temp = LoginService.logout();
																		temp.then(function() {	
																			$location.path('/login');
																		});
																	},10000);
															}else {
																$scope.isOnlineButtonDisable = true;
																$location.path( "/mobileagent");
															}
														}
												}else {
													$scope.navigateAgentToLoginPage();
												}
											  }
											  
											  $scope.navigateAgentToLoginPage = function() {
												alert("Your browser is not supported for dialing Please download the latest version.");
												var temp = LoginService.logout();
												temp.then(function() {	
													$location.path('/login');
												});
											  }
											 
											  $scope.plivoBrowserCheck = function() {
												(function () {
													var plivocheck;
													plivocheck = (function () {
														var flexApp = null;
														function plivocheck() {
														}
														plivocheck.prototype.checkWebSocket = function () {
															if (typeof(WebSocket) != "function")
																return false;
															return true;
														};
														plivocheck.prototype.checkFlash = function () {
															if (navigator.mimeTypes ["application/x-shockwave-flash"] !== undefined) {
																return true;
															}
															if (window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash')) {
																return true;
															}
															return false;
														};
														plivocheck.prototype.getFlashVer = function () {
															var ver = swfobject.getFlashPlayerVersion();
															return ver.major + '.' + ver.minor + '.' + ver.release;
														};
														plivocheck.prototype.checkWebRTC = function () {
															return BrowserDetect.isWebrtcSupported();
														};
														plivocheck.prototype.checkBrowser = function () {
															var browser = BrowserDetect.browser;
															var ver = BrowserDetect.fullVersion;
															return [browser, ver];
														};
												
														plivocheck.prototype.checkOS = function () {
															return BrowserDetect.fullOS;
														};
														return plivocheck;
													})();
													window.PlivoCheck = plivocheck;
												}).call(this);
												var p = new window.PlivoCheck();
												var os_name = p.checkOS(); // OS Check
												console.info('PLIVO - OS : ' + os_name);
												var info = p.checkBrowser();// Browser Check
												var browserStatus = 'Browser Version : ' + info[0] + ' ' + info[1];
												console.info('PLIVO - ' + browserStatus);
												var statusWebSocket = p.checkWebSocket(); // WebSocket
												if (!statusWebSocket) {
													console.info('PLIVO - WebSocket is not available.');
												}else {
													console.info('PLIVO - WebSocket is available.');
												}
												var statusWebRTC = p.checkWebRTC(); // WebRTC Check
												if (!statusWebRTC) {
													console.info('PLIVO - WebRTC is not supported.');
												}else {
													console.info('PLIVO - WebRTC is supported.');
												}
												var statusFlash = p.checkFlash(); // Flash Check
												if (!statusFlash) {
													if (!statusWebRTC) {
														console.info('PLIVO - Flash is not supported.');
													}else {
														console.info('PLIVO - Flash is not supported, we will use WebRTC.');
													}
												}
												if (browserStatus == null && browserStatus == undefined &&  !statusWebSocket && !statusWebRTC) {
													return false;
												}else {
													return true;
												}
											}

	                                    	  $scope.goToScoredCalls = function() {
	                                    		  delete $localStorage.prospectCallList;
	                                    		  delete $localStorage.filterCriteriaObj;
	                                    		  $location.path( "/qa" );
	                                    	  }

	                                    	 $rootScope.breadcrumbs = [];
	                                    		 
	                                    	//  getTrainingDocs.get({folder: 'training/agent/'}).$promise.then(function(data){
                                			// 	  $rootScope.files = data;
                                			// 	 $scope.breadcrumbs.push({key:'Home', value:'training/agent/'});
	                                    	//  });
	                                    	  
	                                    	 $scope.getTrainingDocuments = function(path) {
	                                    		  var modalInstance = $modal.open({
	  		                                		templateUrl: '/partials/agent/trainingfiles.html',
	  		                                		controller: AgentTrainingController,
	  		                                		resolve: {
	  		                                			filepath: function(){
		  		                    		                return path;
		  		                    		            }
	  		                                		}
	  		                                	});
	                                    	  };
	                                    	  var sliceBreadcrumbs = [];
	                                    	  var AgentTrainingController = function ($scope, $rootScope, $state, $modalInstance, filepath,  getTrainingDocs) {
	                                    		  $scope.cancel = function () {
	                                    			  $modalInstance.dismiss('cancel');
	                                    		  };
	                                    		//   $scope.getDocs = function(fileName, path, index) {
	                                    		// 	   getTrainingDocs.get({folder: path}).$promise.then(function(data){
			                                    // 				  $rootScope.files = data;
			                                    // 				  if (index == null) {
			                                    // 					  $scope.breadcrumbs.push({key:fileName, value:path});
			                                    // 				  } else {
			                                    // 					  var sliceBreadcrumbs = $scope.breadcrumbs.slice(0, index+1);
			                                    // 					  $scope.breadcrumbs = sliceBreadcrumbs;
			                                    // 				  }
			                                    // 			});
		                                    	// 	}
	                                    		};
	                                    		
	                                    		$window.onbeforeunload = function (evt) {
	                                         	   $log.debug("Submitting status change to server with status offline");
	                                         	   var agentCommand = {
	                                         			   agentCommand: "BROWSER_CLOSED",
	                                         			   agentStatus: "OFFLINE",
	                                         			   agentType: $scope.agentDetails.agentType,
	                                         			   campaignId: $rootScope.agent.currentCampaign.id
	                                         	   }
	                                         	   var data = angular.toJson(agentCommand);
	                                         	   synchronousService("/spr/agent/agentcommand", "POST", data);
	                                            }
	                                      }]);
});

