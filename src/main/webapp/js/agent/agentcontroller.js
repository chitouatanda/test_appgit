define(['angular', 'telnyx', 'signalwire', 'angular-wizard', 'angular-ui-bootstrap', 'lodash', 'xtaas-constants', 'user-service', 'agent/agent-service', 'agent/twilio-service', 'agent/ui-helper-service', 'notification-service', 'teamnotice-service', 'angular-timer', 'common/xtaas-directives', 'common/xtaas-filters', 'angular-perfect-scrollbar','synchronous-service', 'phoneNumberUtils','phoneController', 'intlTelMin','chosen-jquery', 'angular-localStorage', 'login-service', 'angular-x-editable'], function (angular, telnyx, signalwire) {
    angular.module('AgentController', ['user-service', 'agent-service', 'twilio-service', 'ui-helper-service', 'mgo-angular-wizard', 'ui.bootstrap', 'timer', 'xtaas.constants', 'xtaas-directives', 'xtaas-filters', 'notification-service', 'teamnotice-service', 'perfect_scrollbar','synchronous-service', 'ngStorage', 'login-service', 'xeditable'])
        .controller('AgentController', [
            '$scope',
            '$filter',
            '$rootScope',
            '$q',
            '$compile',
            '$log',
            '$timeout',
            '$interval',
            '$modal',
            '$state',
            '$window',
            'XTAAS_CONSTANTS',
            'user',
            'twilioService',
            'uiHelperService',
            'dispositionStatusService',
            'agentStatusService',
            'agentOfflineStatusService',
            'assetSenderService',
            'prospectCallService',
            'conferenceService',
            'callStatusService',
            'supervisorInvitationService',
            'twilioTokenService',
            'NotificationService',
            'Pusher',
            'getStateService',
            'getCallbackTimezoneService',
            'NoticeService',
            'callHoldService',
            'callUnholdService',
            'callDTMFService',
            'getDisplayLabelService',
            'getDialerStatus',
            'currentProspectService',
            'synchronousService',
            'researchAnDialService',
            'attributeMappings',
            'manualDialingService',
            'getCountryDetailsService',
            'CampaignService',
            'agentDetails',
            'currentCampaignService',
            'PlivoService',
            'TelnyxService',
            'GetOrganizationService',
            'PropertyService',
            'applicationProperties',
            'AgentNetworkStatusUpdateService',
            'LoginService',
            '$sessionStorage',
            'ProviderDetailService',
            'getCampaignDetailsForAgentService',
            '$location',
            '$localStorage',
            'NotifySupervisorService',
            'SignalwireService',
            'getStateDetailService',
            'getCountryDetailService',
            'agentMonitoringService',
            function ($scope, $filter, $rootScope, $q, $compile, $log, $timeout, $interval, $modal, $state, $window, XTAAS_CONSTANTS, user, twilioService, uiHelperService,
                      dispositionStatusService, agentStatusService, agentOfflineStatusService, assetSenderService, prospectCallService, conferenceService, callStatusService,
                      supervisorInvitationService, twilioTokenService, NotificationService, Pusher, getStateService, getCallbackTimezoneService,
                      NoticeService, callHoldService, callUnholdService, callDTMFService, getDisplayLabelService, getDialerStatus, currentProspectService, synchronousService,
                      researchAnDialService, attributeMappings, manualDialingService, getCountryDetailsService, CampaignService, agentDetails, currentCampaignService, PlivoService, 
                      TelnyxService, GetOrganizationService, PropertyService,applicationProperties,AgentNetworkStatusUpdateService,LoginService, $sessionStorage,ProviderDetailService, 
                      getCampaignDetailsForAgentService, $location, $localStorage, NotifySupervisorService, SignalwireService, getStateDetailService, getCountryDetailService, agentMonitoringService) {

                $scope.loggedInUser = user;

                /*Splunk RUM Instrumentation*/
                $scope.agentTracer = null;
                $scope.prospectReceivedTime = null;
                $scope.prospectSubmittedTime = null;
                $scope.prospectSubmittedTimeOld = null;
                $scope.prospectDisconnectedTime = null;
                var token = "ig3tbHcrHmiBqHgpbQWfAg";
        		var appName = "localhost";
        		var enableSplunkRum = false;
        		var host = $window.location.href;
        		if (host.indexOf('demandshore.2xconnect.com') > 0 || host.indexOf('demandshore.herokuapp.com') > 0) {
        			appName = "demandshore";
        			enableSplunkRum = true;
        		} else if (host.indexOf('demandshoreqa.2xconnect.com') > 0 || host.indexOf('demandshoreqa.herokuapp.com') > 0) {
        			appName = "demandshoreqa";
        			enableSplunkRum = true;
        		} else if (host.indexOf('app.2xconnect.com') > 0 || host.indexOf('xtaasupgrade.herokuapp.com') > 0) {
        			appName = "xtaasupgrade";
        			enableSplunkRum = true;
        		} else if (host.indexOf('live.2xconnect.com') > 0 || host.indexOf('xtaascorplive.herokuapp.com') > 0) {
        			appName = "xtaascorplive";
        			enableSplunkRum = true;
        		} else if (host.indexOf('beta.2xconnect.com') > 0 || host.indexOf('xtaascorp.herokuapp.com') > 0) {
        			appName = "xtaascorp";
        			enableSplunkRum = true;
        		} else if (host.indexOf('peaceful-castle-6000.herokuapp.com') > 0) {
        			appName = "peaceful-castle-6000";
        			enableSplunkRum = true;
        		}
        		// initiate SplunkRum & set user id & role in globalAttributes
                if (enableSplunkRum && SplunkRum && !SplunkRum.inited) {
            		SplunkRum.init({
            			beaconUrl: "https://rum-ingest.us1.signalfx.com/v1/rum",
            			rumAuth: token,
            			app: appName,
            			environment: appName
            		});
            		console.log('Initiated Splunk RUM');
            		// set user id & role for SplunkRum 
            		SplunkRum.setGlobalAttributes({
            			'enduser.id': user.id,
            			'enduser.role': user.roles[0]
            		});
            		$scope.agentTracer = SplunkRum.provider.getTracer('agentTracer');
            	}

                $scope.agentCallMonitoring = function(prospectCall) {
                	if (!$scope.isAutoCalling()) {
                		return;
                	}
                	var agentWaitTime = 0;
                	if ($scope.prospectSubmittedTimeOld) {
                		agentWaitTime = ($scope.prospectReceivedTime.getTime() - $scope.prospectSubmittedTimeOld.getTime()) / 1000;
                	}
                	if (agentWaitTime < 0) {
                		// agentWaitTime will be negative for first call submitted
                		agentWaitTime = 0;
                	}
                	var telcoDuration = ($scope.prospectDisconnectedTime.getTime() - $scope.prospectReceivedTime.getTime()) / 1000;
                	var dispositionTime = ($scope.prospectSubmittedTime.getTime() - $scope.prospectDisconnectedTime.getTime()) / 1000;
                	var prospectHandleTime = telcoDuration + dispositionTime;
                	var agentName = user.firstName + "-" + user.lastName;

                	// send splunk spans
                	$scope.splunkOrgNameSpan(prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName);
                	$scope.splunkOrgNameCampIdSpan(prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName);
                	$scope.splunkOrgNameCampIdAgentIdSpan(prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName);
                	
                	// send details to server to log details for agent call monitoring
                	var agentMonitoringLog = {
                		'agentId' : user.id,
                		'agentName' : agentName.trim(),
                		'prospectCallId' : $scope.prospectCall.prospectCallId,
                		'callSid' : $scope.prospectCall.twilioCallSid,
                		'organization' : $scope.loggedInUser.organization,
                		'campaignId' : $scope.campaign.id,
                		'campaignName' : $scope.campaign.name,
                		'dialerMode' : $scope.campaign.dialerMode,
                		'agentType' : $scope.agentType,
                		'agentWaitTime' : agentWaitTime,
                		'telcoDuration' : telcoDuration,
                		'dispositionTime' : dispositionTime,
                		'prospectHandleTime' : prospectHandleTime,
                		'dispositionStatus' : prospectCall.dispositionStatus,
                		'subStatus' : prospectCall.subStatus
                	}
                	agentMonitoringService.printLog(agentMonitoringLog).$promise.then(function() {
                		// console.log('log sent to server for agent call monitoring');
                	}, function (error) {
                		console.error('Failed to send logs to server. Error: ' + error);
                    });
                }

                $scope.splunkOrgNameSpan = function (prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName) {
                	if (SplunkRum && $scope.agentTracer) {
                		var orgNameSpan = $scope.agentTracer.startSpan('orgNameSpan');
                		orgNameSpan.setAttribute('workflow.name', 'orgName');
                		orgNameSpan.setAttribute('agentId', $scope.loggedInUser.id);
                		orgNameSpan.setAttribute('agentName', agentName.trim());
                		orgNameSpan.setAttribute('prospectCallId', prospectCall.prospectCallId);
                		orgNameSpan.setAttribute('callSid', prospectCall.twilioCallSid);
                        orgNameSpan.setAttribute('organization', $scope.loggedInUser.organization);
                		orgNameSpan.setAttribute('campaignId', $scope.campaign.id);
                        orgNameSpan.setAttribute('campaignName', $scope.campaign.name);
                		orgNameSpan.setAttribute('dialerMode', $scope.campaign.dialerMode);
                		orgNameSpan.setAttribute('agentType', $scope.agentType);
                		orgNameSpan.setAttribute('agentWaitTime', agentWaitTime.toFixed(2));
                		orgNameSpan.setAttribute('telcoDuration', telcoDuration.toFixed(2));
                		orgNameSpan.setAttribute('dispositionTime', dispositionTime.toFixed(2));
                		orgNameSpan.setAttribute('prospectHandleTime', prospectHandleTime.toFixed(2));
                		orgNameSpan.setAttribute('dispositionStatus', prospectCall.dispositionStatus);
                		orgNameSpan.setAttribute('subStatus', prospectCall.subStatus);
                		orgNameSpan.end();
					}
				}

                $scope.splunkOrgNameCampIdSpan = function (prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName) {
                	if (SplunkRum && $scope.agentTracer) {
                		var orgNameCampIdSpan = $scope.agentTracer.startSpan('orgNameCampIdSpan');
                		orgNameCampIdSpan.setAttribute('workflow.name', 'orgNameCampId');
                		orgNameCampIdSpan.setAttribute('agentId', $scope.loggedInUser.id);
                		orgNameCampIdSpan.setAttribute('agentName', agentName.trim());
                		orgNameCampIdSpan.setAttribute('prospectCallId', prospectCall.prospectCallId);
                		orgNameCampIdSpan.setAttribute('callSid', prospectCall.twilioCallSid);
                        orgNameCampIdSpan.setAttribute('organization', $scope.loggedInUser.organization);
                		orgNameCampIdSpan.setAttribute('campaignId', $scope.campaign.id);
                        orgNameCampIdSpan.setAttribute('campaignName', $scope.campaign.name);
                		orgNameCampIdSpan.setAttribute('dialerMode', $scope.campaign.dialerMode);
                		orgNameCampIdSpan.setAttribute('agentType', $scope.agentType);
                		orgNameCampIdSpan.setAttribute('agentWaitTime', agentWaitTime.toFixed(2));
                		orgNameCampIdSpan.setAttribute('telcoDuration', telcoDuration.toFixed(2));
                		orgNameCampIdSpan.setAttribute('dispositionTime', dispositionTime.toFixed(2));
                		orgNameCampIdSpan.setAttribute('prospectHandleTime', prospectHandleTime.toFixed(2));
                		orgNameCampIdSpan.setAttribute('dispositionStatus', prospectCall.dispositionStatus);
                		orgNameCampIdSpan.setAttribute('subStatus', prospectCall.subStatus);
                		orgNameCampIdSpan.end();
					}
                }

                $scope.splunkOrgNameCampIdAgentIdSpan = function (prospectCall, agentWaitTime, telcoDuration, dispositionTime, prospectHandleTime, agentName) {
                	if (SplunkRum && $scope.agentTracer) {
                		var orgNameCampIdAgentIdSpan = $scope.agentTracer.startSpan('orgNameCampIdAgentIdSpan');
                		orgNameCampIdAgentIdSpan.setAttribute('workflow.name', 'orgNameCampIdAgentId');
                		orgNameCampIdAgentIdSpan.setAttribute('agentId', $scope.loggedInUser.id);
                		orgNameCampIdAgentIdSpan.setAttribute('agentName', agentName.trim());
                		orgNameCampIdAgentIdSpan.setAttribute('prospectCallId', prospectCall.prospectCallId);
                		orgNameCampIdAgentIdSpan.setAttribute('callSid', prospectCall.twilioCallSid);
                        orgNameCampIdAgentIdSpan.setAttribute('organization', $scope.loggedInUser.organization);
                		orgNameCampIdAgentIdSpan.setAttribute('campaignId', $scope.campaign.id);
                        orgNameCampIdAgentIdSpan.setAttribute('campaignName', $scope.campaign.name);
                		orgNameCampIdAgentIdSpan.setAttribute('dialerMode', $scope.campaign.dialerMode);
                		orgNameCampIdAgentIdSpan.setAttribute('agentType', $scope.agentType);
                		orgNameCampIdAgentIdSpan.setAttribute('agentWaitTime', agentWaitTime.toFixed(2));
                		orgNameCampIdAgentIdSpan.setAttribute('telcoDuration', telcoDuration.toFixed(2));
                		orgNameCampIdAgentIdSpan.setAttribute('dispositionTime', dispositionTime.toFixed(2));
                		orgNameCampIdAgentIdSpan.setAttribute('prospectHandleTime', prospectHandleTime.toFixed(2));
                		orgNameCampIdAgentIdSpan.setAttribute('dispositionStatus', prospectCall.dispositionStatus);
                		orgNameCampIdAgentIdSpan.setAttribute('subStatus', prospectCall.subStatus);
                		orgNameCampIdAgentIdSpan.end();
					}
                }

                $scope.isAutoCalling = function () {
                	if ($sessionStorage.callbackProspect != null && $sessionStorage.callbackProspect != undefined
                        && $sessionStorage.callbackProspect.prospect != null && $sessionStorage.callbackProspect.prospect != undefined) {
                        return false;
                    } else if ($scope.campaign.dialerMode == 'POWER') {
                    	return true;
                    }  else if ($scope.agentType && $scope.agentType == 'AUTO'
                    		&& $scope.campaign.dialerMode == 'HYBRID') {
                    	return true;
                    } else {
                        return false;
                    }
                }

                $scope.isProspectInDNC = false;
                if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("AGENT")) {
                    $state.go('login');
                    return;
                }
                
                if (applicationProperties != null && applicationProperties != undefined) {
                    angular.forEach(applicationProperties, function(value, key) {
                        if (value.name == "DISCONNECT_BUTTON_AUTO_TIMER_IN_MS") {
                            $scope.disconnectButtonAutoTimer = value.value;
                        }
                        if (value.name == "DISCONNECT_BUTTON_MANUAL_TIMER_IN_MS") {
                            $scope.disconnectButtonManualTimer = value.value;
                        }
                        if (value.name == "IS_FIFTEEN_SEC_DISCONNECT_TIMER") {
                            $scope.isFifteenSecDisconnectTimer = value.value;
                        }
                    });
                }

                if($localStorage.validUser == null || $localStorage.validUser == undefined || $localStorage.validUser == '') {
                    var temp = LoginService.logout();
                    temp.then(function() {
                        $rootScope.isUserLoggedIn = false;				
                        $localStorage.$reset();	
                        $location.path('/login');
                    });
                }	
                $scope.checkAgentIdleTime = function() {
                    if ($scope.campaign.supervisorAgentStatusApprove) {
                        if ($scope.parent.status != undefined && $scope.parent.status == 'ONLINE') {
                            $scope.notifyAndLogOutManualAgent();
                        } 
                    } else {
                        if ($scope.parent.status != undefined && $scope.parent.status == 'ONLINE') {
                            $scope.notifyAndLogOutManualAgent();
                        }
                    }
                }

                $scope.notifyAndLogOutManualAgent = function() {
                    if ($scope.campaign != null && $scope.campaign != undefined && $scope.agentOnlineCheckInTime != null && $scope.agentOnlineCheckInTime != undefined && 
                        $scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization != undefined && 
                        $scope.loggedInUserOrganization.manualAgentIdleTime != 0 && $scope.campaign.type != 'Prospecting') {
                            var diff =(new Date().getTime() - $scope.agentOnlineCheckInTime.getTime()) / 1000;
                            diff /= 60;
                            var minutes = Math.abs(diff);
                            if (minutes >= $scope.loggedInUserOrganization.manualAgentIdleTime) {
                                NotifySupervisorService.query({agentId : $scope.loggedInUser.id, mode: "MANUAL"}).$promise.then(function(organization) {
                                    $scope.refreshPlivo();
                                    $scope.logOutAgent();
                                });   
                            }
                        }
                }

                $scope.startAgentActivityCheckTimer = function() {
                    $scope.agentIdleTimeInterval =   $interval(function() {
                        $scope.checkAgentIdleTime();
                    }, 60000);
                }

                $scope.loggedInUserOrganization = {};
                GetOrganizationService.query({id : $scope.loggedInUser.organization}).$promise.then(function(organization) {  
                    $scope.loggedInUserOrganization = organization;
                    if ($scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization.showAlternateLink != null 
                        && $scope.loggedInUserOrganization.showAlternateLink != undefined 
                        && $scope.loggedInUserOrganization.showAlternateLink && 
                        (($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'AUTO') || 
                          $scope.campaign.dialerMode == 'PREVIEW' || $scope.campaign.dialerMode == 'POWER')) {
                        $scope.showAlternateLink = true;
                    }
                    if (!$scope.isAutoDialingEnabled()) {
                        if ($scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization.enableAgentInactivity != null 
                            && $scope.loggedInUserOrganization.enableAgentInactivity != undefined 
                            && $scope.loggedInUserOrganization.enableAgentInactivity) {
                            $scope.agentOnlineCheckInTime = new Date();
                            $scope.startAgentActivityCheckTimer();
                        }
                    } 
                });

                $scope.dataProvider = [];
                ProviderDetailService.query().$promise.then(function(data) {
                    $scope.dataProvider = data;
                },function(error) {
                    console.log(error);
                });
                $scope.autoAlertDisposition = '';
                $scope.autoDisposeTimeInterval = undefined;
                $scope.machineDetectionTimeInterval = undefined;
                $scope.isMobileTablet = function() {
                    var check = false;
                    (function(a){
                        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
                            check = true;
                    })(navigator.userAgent||navigator.vendor||window.opera);
                    return check;
                }

                $scope.splitBandwidthValues = function() {
                    angular.forEach(applicationProperties, function(value, key) {
                        if (value.name == "JITTER_RANGE") {
                            $scope.jitterRange = value.value;
                        }
                        if (value.name == "PACKETLOSS_RANGE") {
                            $scope.packetLossRange = value.value;
                        }
                        if (value.name == "RTT_RANGE") {
                            $scope.rttRange = value.value;
                        }
                    });
                    if ($scope.jitterRange != null && $scope.jitterRange != undefined) {
                        $scope.jitterList = [];
                        $scope.jitterList = $scope.jitterRange.split('-');
                    }
                    if ($scope.packetLossRange != null && $scope.packetLossRange != undefined) {
                        $scope.packetLossList = [];
                        $scope.packetLossList = $scope.packetLossRange.split('-');
                    }
                    if ($scope.rttRange != null && $scope.rttRange != undefined) {
                        $scope.rttList = [];
                        $scope.rttList = $scope.rttRange.split('-');
                    }
                }
                $scope.splitBandwidthValues();
            	/* PLIVO CODE */
            	$scope.getCallControlProvider = function() {
            		if($scope.isAutoDialingEnabled()){
            			 if ( $scope.campaign.autoModeCallProvider != null && $scope.campaign.autoModeCallProvider != undefined) {
                             return $scope.campaign.autoModeCallProvider;
                         }
            		}else{
                         if ($scope.campaign.manualModeCallProvider != null && $scope.campaign.manualModeCallProvider != undefined) {
                             return $scope.campaign.manualModeCallProvider;
                         }
            		}
                    return $scope.campaign.callControlProvider;
                    // return 'Telnyx';
            	}
                $scope.plivoWebSdk = null;
                $scope.telnyxClient = null;
            	$scope.plivoEndpointId = '';
            	$scope.plivoEndpointUsername = '';
            	$scope.plivoToNumber = '';
    			$scope.plivoExtraHeaders = {};
                $scope.telnyxConnectionId = "";
                $scope.telnyxConnectionUsername = "";
                $scope.telnyxCall = null;
                $scope.telnyxCallLegId = "";
                $scope.selectedFromTableLeadIQ = false;
                $scope.isLeadIQProspect = false;
                $scope.leadIQEmail = false;
                $scope.hidePhoneLeadIQ = false;
                $scope.isAutoAMD = false;
                $scope.isCustomFieldCollapsed = true;
                $scope.defEmailUnhide = false;
                $scope.isNewExtensionDialed = false;
                // // $scope.prospectCall = {};
                // $scope.cfAnswers = [];
                // $scope.cfAnswers[0] = {"question": "CUSTOMFIELD_01gvjjhbasdkj 124143", "answer": "1.00000000000000000sdsdahbhbsdcvbsmnc mnc00000", "agentQuestion" : true};
                // $scope.cfAnswers[1] = {"question": "CUSTOMFIELDjkjnkasdasasdasdasadasdasdn xcvx_02", "answer": "2.jhgnmkzc0", "agentQuestion" : true};
                // $scope.cfAnswers[2] = {"question": "CUSTOMFIELDzfbxbxzz_02", "answer": "2.0787uhnmx", "agentQuestion" : false};
                // $scope.cfAnswers[3] = {"question": "CUSTOMFIELD_02", "answer": "2.0jjhhms90", "agentQuestion" : true};
                // $scope.cfAnswers[4] = {"question": "CUSTOMFIELzcvdzD_02", "answer": "2.kjhsbjkacnj0", "agentQuestion" : true};
                // $scope.cfAnswers[5] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : false};
                // $scope.cfAnswers[6] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};
                // $scope.cfAnswers[7] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : false};
                // $scope.cfAnswers[8] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};
                // $scope.cfAnswers[9] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};
                // $scope.cfAnswers[10] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : false};
                // $scope.cfAnswers[11] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};
                // $scope.cfAnswers[12] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};
                // $scope.cfAnswers[13] = {"question": "CUSTOMFIELD_02", "answer": "2.0", "agentQuestion" : true};


                // $scope.printcfAnswers = function() {
                //     console.log($scope.cfAnswers[0]);
                // };
                $scope.signalwireClient = null;
                
            	$scope.plivoRefreshSettings = function() {
            		var plivoSettings = {
            			"debug" : "ERROR",
            			"permOnClick" : true,
            			"audioConstraints" : {
            				"optional" : [ {
            					"googAutoGainControl" : false
            				}, {
            					"googEchoCancellation" : false
            				} ]
            			},
            			"enableTracking" : true,
            			"dscp" : true,
            			"clientRegion" : "asia",
            			"codecs" : [ "OPUS", "PCMU" ],
            			"enableIPV6" : false
            		}
            		return plivoSettings;
            	}

            	$scope.plivoInitPhone = function() {
            		var plivoSettings = $scope.plivoRefreshSettings();
            		$scope.plivoWebSdk = new window.Plivo(plivoSettings);
            		$scope.plivoWebSdk = new window.Plivo(plivoSettings);
            		$scope.plivoWebSdk.client.on('onWebrtcNotSupported', $scope.onWebrtcNotSupported);
        			$scope.plivoWebSdk.client.on('onLogin', $scope.onLogin);
        			$scope.plivoWebSdk.client.on('onLogout', $scope.onLogout);
        			$scope.plivoWebSdk.client.on('onLoginFailed', $scope.onLoginFailed);
        			$scope.plivoWebSdk.client.on('onCallRemoteRinging', $scope.onCallRemoteRinging);
        			$scope.plivoWebSdk.client.on('onIncomingCallCanceled', $scope.onIncomingCallCanceled);
        			$scope.plivoWebSdk.client.on('onCallFailed', $scope.onCallFailed);
        			$scope.plivoWebSdk.client.on('onCallAnswered', $scope.onCallAnswered);
        			$scope.plivoWebSdk.client.on('onMediaConnected', $scope.onMediaConnected);
        			$scope.plivoWebSdk.client.on('onCallTerminated', $scope.onCallTerminated);
        			$scope.plivoWebSdk.client.on('onCalling', $scope.onCalling);
        			$scope.plivoWebSdk.client.on('onIncomingCall', $scope.onIncomingCall);
        			$scope.plivoWebSdk.client.on('onMediaPermission', $scope.onMediaPermission);
        			$scope.plivoWebSdk.client.on('mediaMetrics', $scope.mediaMetrics);
        			$scope.plivoWebSdk.client.on('onConnectionChange', $scope.onConnectionChange);
        			$scope.plivoWebSdk.client.on('audioDeviceChange', $scope.audioDeviceChange);
            		$scope.plivoWebSdk.client.setRingTone(true);
            		$scope.plivoWebSdk.client.setRingToneBack(true);
            		$scope.plivoWebSdk.client.setConnectTone(true);
                    $scope.plivoWebSdk.client.setDebug("ERROR");
            		$scope.checkBrowserComplaince($scope.plivoWebSdk.client);
            		console.log('PLIVO - phone initialized.');
                }

                $scope.telnyxInit = function (username, password) {
                    var audioElement = document.getElementById("audioElement");
                    // Initialize the client
                    $scope.telnyxClient = new telnyx.TelnyxRTC({
                      // Required credentials
                      credentials: {
                        // Telnyx Connection Username
                        username: username,
                        // Telnyx Connection Password
                        password: password
                        // username: "ixagent2yljIT7bhFP",
                        // password: "yljIT7bhFP"
                      },
                      // Other options
                      //
                      // This can be a DOM element, DOM selector, or a function that returns an element.
                      remoteElement: audioElement,
                      useMic: true,
                      useSpeaker: true,
                      useCamera: false,
                    });

                    // Create a variable to track the current call
                    let activeCall;
                    // Attach event listeners
                    $scope.telnyxClient
                      .on("socket.connect", () => console.log("socket connected"))
                      .on("socket.close", () => console.log("socket closed"))
                      .on("registered", () => {
                        console.log("registered");
                        console.log($scope.telnyxClient);

                        if ($scope.isAutoDialingEnabled()) {
                            var agentCallInfo = {
                                // from: "+18883021231",
                                to: $scope.telnyxConnectionUsername,
                                campaignId: $scope.campaign.id,
                                dialerMode: $scope.campaign.dialerMode,
                                agentId: $scope.parent.userId
                              };
                               TelnyxService.createIncomingCallForAgent(agentCallInfo).$promise.then(function (callDetails) {
                                $scope.telnyxCallLegId = callDetails.call_leg_id;
                               });

                        }
                      })
                      .on("unregistered", () => console.log("unregistered"))
                      // Event fired on call updates, e.g. when there is an incoming call
                      .on("callUpdate", (call) => {

                        switch (call.state) {
                          // Connecting to a call
                          case "connecting":
                            $scope.telnyxCall = call;
                            return;
                          // Receiving an inbound call
                          case "ringing":
                            return;
                          // An established and active call
                          case "active":
                            $scope.telnyxCall = call;
                            // $scope.startRecording(call);
                            return;
                          // Call is active but on hold
                          case "held":
                            return;
                          // Call is over and can be removed
                          case "done":
                            $scope.endCall();
                            return;
                          // New calls that haven't started connecting yet
                          case "new":
                            $scope.telnyxCall = call;
                            call.answer();
                            $scope.onCallRemoteRingingTelnyx(call);
                            return;
                          default:
                            return;
                        }
                      });

                    // Connect and login
                    $scope.telnyxClient.connect();

                  };
                
                $scope.signalwireInit =  function (username, pass) {
                    // var domain = document.getElementById('sipUrl').value;
                    var domain = "xtaascorp-test.sip.signalwire.com";
                    var sip_url = username + '@' + domain;
                    // console.log(sip_url);
                    // console.log(pass);
              
                    var options = {
                      /*
                      media: {
                        local: {
                          video: document.getElementById('localVideo')
                        },
                        remote: {
                          video: document.getElementById('remoteVideo'),
                          audio: document.getElementById('remoteVideo')
                        }
                      },
                      */
                      // use this instead for audio only:
                      media: {
                        remote: {
                          audio: document.getElementById('audioElement')
                        }
                      },
                      wsServers: domain,
                      ua: {
                        wsServers: "wss://" + domain,
                        uri: sip_url,
                        password: pass,
                        traceSip: true
                      }
                    };
              
                    // this sets up the main object
                    $scope.signalwireClient = new signalwire.Web.Simple(options);
              
                    // set up event handlers 
                    $scope.signalwireClient.on('registered', function() {
                      console.log("registered")
                      if ($scope.isAutoDialingEnabled()) {
                        // Do not change sequence, check SignalwireServiceImpl ===> agentId--campaignId
                        var confCreationCallDapp = user.id + "--" + $scope.campaign.id + "@" + $scope.signalwireDomainApp;
                        $scope.signalwireClient.call(confCreationCallDapp);
                    //   } else {
                        // Do not change sequence, check SignalwireServiceImpl ===> agentId==campaignId==pcid==toNumber==fromNumber
                    //     var confCreationCallDapp = user.id + "==" + $scope.prospectCall.prospectCallId + "==" + $scope.signalwireToNumber.replace("+", "") + "==" + $scope.signalwireExtraHeaders.callerId.replace("+", "") + "@" + $scope.signalwireDomainApp;
                    //     $scope.signalwireClient.call(confCreationCallDapp, $scope.signalwireExtraHeaders);
                      }
                    }).on('connecting', function() {
                        console.log("signalwire call connecting")
                    }).on('connected', function() {
                        console.log("signalwire call connected")
                    }).on('ringing', function() {
                      setStatus('Signalwire Incoming call');
                      $scope.signalwireClient.answer();
                    });
                }

                $scope.signalwireManualCall = function(to, extraHeaders, userId) {
        			console.info('Signalwire - call initiated.');
        			if ($scope.signalwireClient == null) {
                        SignalwireService.createEndpoint({username: userId}).$promise.then(function (endpoint) {
                            if (endpoint.SIGNALWIRE_ENDPOINT_USERNAME && endpoint.SIGNALWIRE_ENDPOINT_PASSWORD) {
                                $scope.signalwireEndpointId = endpoint.SIGNALWIRE_ENDPOINT_ID;
                                $scope.signalwireEndpointUsername = endpoint.SIGNALWIRE_ENDPOINT_USERNAME;
                                $scope.signalwireDomainApp = endpoint.SIGNALWIRE_DOMAINAPP;
                                $scope.signalwireInit($scope.signalwireEndpointUsername, endpoint.SIGNALWIRE_ENDPOINT_PASSWORD);
                            }
                        }, function (error) {
                            NotificationService.log('error', 'An error occurred while creating Signalwire endpoint.');
                        });
                        // $scope.customAlert('error', 'PLIVO - You are not Logged in. Please try again.');
        				// return;
                    }
                    if ($scope.signalwireClient != null) {
                        // Do not change sequence, check SignalwireServiceImpl ===> agentId==campaignId==pcid==toNumber==fromNumber
                        var confCreationCallDapp = user.id + "==" + $scope.campaign.id + "==" + $scope.prospectCall.prospectCallId + "==" + to.replace("+", "") + "==" + extraHeaders.callerId.replace("+", "") + "@" + $scope.signalwireDomainApp;
                        $scope.signalwireClient.call(confCreationCallDapp, extraHeaders);
                        // $scope.signalwireClient.call(to, extraHeaders);
                    }
        		}

                $scope.startRecording = function(callInfo) {
                    var webhookData = {
                        data: {
                            payload: {
                                call_control_id: callInfo.call.callID,
                                to: $scope.parent.numberToDial
                            }
                        }
                    };
                    TelnyxService.startRecording(webhookData);
                }

                $scope.saveRecording = function(callInfo) {
                    var webhookData = {
                        data: {
                            payload: {
                                call_control_id: callInfo.call.callID,
                                to: $scope.parent.numberToDial
                            }
                        }
                    };
                    TelnyxService.saveRecording(webhookData);
                }

            	$scope.checkBrowserComplaince = function(client) {
            		if (client.browserDetails.browser != "chrome") {
            			$scope.customAlert('error', 'Please use latest version of chrome.');
            		}
            	}

            	$scope.plivoLogin = function(username, password) {
        			console.info('PLIVO - login initiated.');
        			$scope.plivoWebSdk.client.login(username, password);
                }

        		$scope.plivoLogout = function() {
        			console.info('PLIVO - logout initiated.');
        			if ($scope.plivoWebSdk && $scope.plivoWebSdk.client && $scope.plivoWebSdk.client.isLoggedIn) {
        				$scope.plivoWebSdk.client.logout();
        			}
                }

                $scope.telnyxLogout = function() {
                    console.info('Telnyx - disconnect initiated');
                    if ($scope.telnyxClient ) {
                        $scope.telnyxConnectionId = "";
                        $scope.telnyxConnectionUsername = "";
                        $scope.telnyxCall = null;
                        // You can disconnect when you're done
                        $scope.telnyxClient.disconnect();
                        $scope.telnyxClient = null;
                    }
                }

        		$scope.plivoCall = function(to, extraHeaders) {
        			console.info('PLIVO - call initiated.');
        			if (!$scope.plivoWebSdk.client.isLoggedIn) {
        				$scope.customAlert('error', 'PLIVO - You are not Logged in. Please try again.');
        				return;
                    }
                    if ($scope.campaign.isNotConference) {
                        return;
                    }
                    if ($sessionStorage.callbackProspect == null && $sessionStorage.callbackProspect == undefined) {
        			    $scope.plivoWebSdk.client.call(to, extraHeaders);
                    }
                }

                $scope.telnyxCreateCall = function (to, from) {
                  const call = $scope.telnyxClient.newCall({
                    // Destination is required and can be a phone number or SIP URI
                    // destination: "+13322161699",
                    destination: to,
                    callerName: "",
                    // Caller ID number is optional.
                    // You can only specify a phone number that you own and have assigned
                    // to your Connection in the Telnyx Portal
                    callerNumber: from
                    // callerNumber: "‬+18883021231",
                  });
                };

                $scope.telnyxCreateCallForManual = function(agentCallInfo) {
                    TelnyxService.createCallForManualDial(agentCallInfo).$promise.then(function (callDetails) {
                        $scope.telnyxCallLegId = callDetails.call_leg_id;
                        $scope.prospectCall.twilioCallSid = $scope.telnyxCallLegId;
                        if ($scope.campaign.ringingIcon) {
                            addBlinkerToDialButton();   
                        }
                       });
                }
                
                addBlinkerToDialButton = function() {
                    var iconElement, element, name, arr;       
                    element = document.getElementById("dialButton");
                    element.innerHTML = "Ringing";
                    name = "dialButtonBlink";
                    arr = element.className.split(" ");
                    if (arr.indexOf(name) == -1) {
                        element.className += " " + name;
                    }                
                }
                
                removeBlinkerFromDialButton = function() {
                    var element = document.getElementById("dialButton");   
                    if(element != undefined){
                    	element.innerHTML = "<i class=\"fa fa-phone\"></i> Dial";
                    	element.className = element.className.replace(/\bdialButtonBlink\b/g, "");
                	}
                }

                $scope.plivoManualCall = function(to, extraHeaders) {
        			console.info('PLIVO - call initiated.');
        			if (!$scope.plivoWebSdk.client.isLoggedIn) {
                        $scope.createEndPointOnPlivo();
                        // $scope.customAlert('error', 'PLIVO - You are not Logged in. Please try again.');
        				// return;
                    }
                    if ($scope.plivoWebSdk.client.isLoggedIn) {
                        $scope.plivoWebSdk.client.call(to, extraHeaders);
                    }
        		}

        		$scope.plivoHangupCall = function() {
                    console.info('PLIVO - call hangup initiated.');

        			if ($scope.plivoWebSdk.client && $scope.plivoWebSdk.client.callSession) {
        				$scope.plivoWebSdk.client.hangup();
        			}
                }

                $scope.telnyxHangupCall = function() {
        			console.info('TELNYX - call hangup initiated.');
        			if ($scope.telnyxClient && $scope.telnyxCall) {
                        // $scope.telnyxCall.hangup();
                        // var callLegId = {
                        //     callLegId: $scope.telnyxCallLegId
                        // };
                        TelnyxService.hangupCall($scope.telnyxCallLegId);
        			}
                }

                $scope.appendExtension = function(key) {
                    if ($scope.parent.state > 2) {
                        if ($scope.prospectCall.prospect.agentDialedExtension != null && $scope.prospectCall.prospect.agentDialedExtension != undefined) {
                            if ($scope.isNewExtensionDialed) {
                                $scope.prospectCall.prospect.agentDialedExtension += key;
                            } else {
                                $scope.prospectCall.prospect.agentDialedExtension = key;
                            }
                        } else {
                            $scope.prospectCall.prospect.agentDialedExtension = key;
                        }
                        $scope.isNewExtensionDialed = true;
                    }
                }

        		$scope.plivoSendDtmf = function(key) {
        			console.info('PLIVO - sending DTMF key ' + key);
        			if ($scope.plivoWebSdk.client && $scope.plivoWebSdk.client.callSession) {
                        $scope.appendExtension(key);
        				$scope.plivoWebSdk.client.sendDtmf(key);
        			}
                }

                $scope.telnyxSendDtmf = function(key) {
                    console.info('TELNYX - sending DTMF key ' + key);
                    $scope.playBeepSound();
                    if ($scope.isAutoDialingEnabled()) {
                        var dtmfObj = {
                            callLegId: $scope.telnyxCallLegId,
                            digits: key
                        };
                        $scope.appendExtension(key);
                        TelnyxService.sendDtmf(dtmfObj);
                    } else {
                        if ($scope.telnyxClient && $scope.telnyxCall) {
                            $scope.appendExtension(key);
                            $scope.telnyxCall.dtmf(key);
                        }
                    }
        		}

                $scope.signalwireSendDtmf = function(key) {
        			console.info('SIGNALWIRE - sending DTMF key ' + key);
        			// if ($scope.signalwireClient) {
                    $scope.appendExtension(key);
                    $scope.playBeepSound();
                    $scope.signalwireClient.sendDTMF(key);
        			// }
                }

        		$scope.plivoCallMuteUnmute = function(action) {
        			if (!$scope.muteStatus) {
        				// console.info('PLIVO - Call mute.');
        				$scope.plivoWebSdk.client.mute();
        			} else {
        				// console.info('PLIVO - Call unmute.');
        				$scope.plivoWebSdk.client.unmute();
        			}
                }

                $scope.telnyxCallMuteUnmute = function(action) {
        			if (!$scope.muteStatus) {
        				console.info('Telnyx - Call mute.');
        				$scope.telnyxCall.mute();
        			} else {
        				console.info('Telnyx - Call unmute.');
        				$scope.telnyxCall.unmute();
        			}
                }

                $scope.signalwireCallMuteUnmute = function(action) {
        			if (!$scope.muteStatus) {
        				console.info('Signalwire - Call mute.');
        				$scope.signalwireClient.mute();
        			} else {
        				console.info('Signalwire - Call unmute.');
        				$scope.signalwireClient.unmute();
        			}
                }

        		/* PLIVO CALLBACK METHODS */
        		$scope.onWebrtcNotSupported = function() {
        			$scope.customAlert('error', 'Webrtc is not supported in this broswer, Please use latest version of chrome/firefox.');
        		}

        		$scope.onLogin = function() {
        			console.info('PLIVO - ' + $scope.plivoWebSdk.client.userName + ' logged in.');
        			if ($scope.isAutoDialingEnabled()) {
        				$scope.plivoCall($scope.plivoToNumber, $scope.plivoExtraHeaders);
					}
        		}

        		$scope.onLogout = function() {
        			console.info('PLIVO - user logout successfully.');
        		}

        		$scope.onLoginFailed = function(reason) {
                    var loginFailedReason = 'PLIVO - user login failed. Reason - ' + reason;
                    console.error(loginFailedReason);
        			// $scope.customAlert('error', loginFailedReason);
        		}

        		$scope.onCallRemoteRinging = function(callInfo) {
                    // console.info('PLIVO - onCallRemoteRinging : ', callInfo);
                    if (!$scope.isAutoDialingEnabled()) {
                        if ($scope.getCallControlProvider() == 'Plivo') {
                            $scope.updateCallStatusManualDial(callInfo.callUUID);
                       } else if ($scope.getCallControlProvider() == 'Telnyx') {
                            $scope.updateCallStatusManualDial($scope.telnyxCallLegId);
                        } else if ($scope.getCallControlProvider() == 'Signalwire') {
                            $scope.updateCallStatusManualDial($scope.signalwireCallSid);
                        }
                    } else {
                        $log.debug("Prev state " + $scope.parent.state);
                        console.log($scope.$id);
                        if ($sessionStorage.callbackProspect == null && $sessionStorage.callbackProspect == undefined) {
                            $scope.parent.state = 1;   
                        } else {
                            $scope.updateCallStatusManualDial(callInfo.callUUID); 
                        }
                        $log.debug("New state " + $scope.parent.state);
                    }
                }

                $scope.onCallRemoteRingingTelnyx = function(callInfo) {

                   // Telnyx update call status
        			 if (!$scope.isAutoDialingEnabled()) {
                         if ($scope.getCallControlProvider() == 'Plivo') {
                             $scope.updateCallStatusManualDial(callInfo.callUUID);
                        } else if ($scope.getCallControlProvider() == 'Telnyx') {
                         $scope.updateCallStatusManualDial($scope.telnyxCallLegId);
                         } else if ($scope.getCallControlProvider() == 'Signalwire') {
                            $scope.updateCallStatusManualDial($scope.signalwireCallSid);
                        }
        			 } else {
                         $log.debug("Prev state " + $scope.parent.state);
                         console.log($scope.$id);
                         $scope.parent.state = 1;
                         $log.debug("New state " + $scope.parent.state);
                     }
        		}

        		$scope.onIncomingCallCanceled = function(callInfo) {
                    // console.info('PLIVO - onIncomingCallCanceled : ', callInfo);
        		}

        		$scope.onCallFailed = function(cause, callInfo) {
        			var callFailedReason = 'PLIVO - call failed. Reason - ' + cause;
        			if (cause && /Denied Media/i.test(cause)) {
        				$scope.customAlert('error', 'PLIVO - Media access blocked.');
        			} else {
        				// $scope.customAlert('error', callFailedReason);
        				if (callInfo.callUUID == null && callInfo.state == 'failed') {
        					$scope.invalidCall('invalid-phone',callInfo);
        					console.info('PLIVO - onCallFailed : ', callInfo);
            				console.info('PLIVO - onCallFailed : ', cause);
        				}
        			}
        		}

                $scope.enableDialSettingsOnUI = function() {
                    if ($scope.callInfo == undefined) {
                        $scope.callInfo = {};
                        $scope.callInfo.callUUID = "";
                    }
                    if (!$scope.isAutoDialingEnabled()) {
                        if ($scope.getCallControlProvider() == 'Plivo') {
                            $scope.updateCallStatusManualDial($scope.callInfo.callUUID);
                        } else if ($scope.getCallControlProvider() == 'Telnyx') {
                            $scope.updateCallStatusManualDial($scope.telnyxCallLegId);
                        } else if ($scope.getCallControlProvider() == 'Signalwire') {
                            $scope.updateCallStatusManualDial($scope.signalwireCallSid);
                        }
                    } else {
                        $log.debug("Prev state " + $scope.parent.state);
                        console.log($scope.$id);
                        $scope.parent.state = 1;
                        $log.debug("New state " + $scope.parent.state);
                    }
            }

            $scope.onCallAnswered = function(callInfo) {
                console.info('PLIVO - onCallAnswered : ', callInfo);
                // if (!$scope.isAutoDialingEnabled()) {
                //     $scope.enableDialSettingsOnUI();
                // }
            }

        		$scope.onMediaConnected = function(callInfo) {
        			// console.info('PLIVO - onMediaConnected : ', callInfo);
        		}

        		$scope.onCallTerminated = function(evt) {
        			// console.info('PLIVO - onCallTerminated');
        		}

        		$scope.onCalling = function() {
        			// console.info('PLIVO - onCalling');
        		}

        		$scope.onIncomingCall = function(callerID, extraHeaders, callInfo) {
        			console.info('PLIVO - onIncomingCall : callerID - ' + callerID);
                    // console.log('PLIVO - onIncomingCall : extraHeaders - ' + extraHeaders + ' callInfo - ' + callInfo);
                    $scope.plivoWebSdk.client.answer(callInfo.callUUID);
        		}

        		$scope.onMediaPermission = function(evt) {
        			console.info('PLIVO - onMediaPermission', evt);
        			if (evt.error) {
        				$scope.customAlert('error', 'PLIVO - Media permission error');
        			}
        		}

        		var averageBandWidthCheck = 0;
                var lowBandWidthCheck = 0;
        		$scope.mediaMetrics = function(event) {
                    //console.log('PLIVO - mediaMetrics : ' + JSON.stringify(event));
                    //$scope.resetNotices();
                    var msg;
                    if (event.type == "high_jitter") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - high_jitter";
                    } else if (event.type == "high_rtt") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - high_rtt";
                    }else if (event.type == "high_packetloss") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - high_packetloss";
                    }else if (event.type == "low_mos") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - low_mos";
                    }
                    // else if (event.type == "no_audio_received") {
                    //     msg = "Please check microphone- Error Code - no_audio_received";
                    // }
                    else if (event.type == "ice_timeout") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - ice_timeout";
                    }else if (event.type == "no_microphone_acesss") {
                        msg = "Microphone access blocked, please provide Microphone Access or contact administrator- Error Code - no_microphone_acesss";
                    }else if (event.type == "ice_connection") {
                        msg = "Unstable Network Detected, Please check internet connection- Error Code - ice_connection";
                    }
                    //$scope.notices.push(msg);
                    //$timeout(function() {
                    //	$scope.resetNotices();
                    //}, 5000);
                    console.log(event);
                    if ($scope.campaign.bandwidthCheck != null && $scope.campaign.bandwidthCheck != undefined && $scope.campaign.bandwidthCheck) {
                        if ((event.type == "high_jitter" && event.value >= $scope.jitterList[1]) || 
                        (event.type == "high_packetloss" && event.value >= $scope.packetLossList[1]) || 
                        (event.type == "high_rtt" && event.value >= $scope.rttList[1])) {
                            lowBandWidthCheck++;
                            if (lowBandWidthCheck > 3) {
                                NotificationService.log('error', 'Your network is unsuitable to be working with XTaaS, please check network and relogin.');
                                $scope.endCall();
                                $scope.refreshPlivo();
                                $scope.updateAgentNetworkStatus(user.id, false);
                                $scope.logOutAgent();
                            }
                    }

                    if ((event.type == "high_jitter" && (event.value >= $scope.jitterList[0] && event.value <= $scope.jitterList[1])) || 
                        (event.type == "high_packetloss" && (event.value >= $scope.packetLossList[0] && event.value <= $scope.packetLossList[1])) || 
                        (event.type == "high_rtt" && (event.value >= $scope.rttList[0] && event.value <= $scope.rttList[1]))) {
                            averageBandWidthCheck++;
                            if (averageBandWidthCheck > 3) {
                                $scope.updateAgentNetworkStatus(user.id, true);
                                NotificationService.log('error', 'Unstable network detected - Please check network.');
                            }
                        }
                    }
                }
                
                $scope.logOutAgent = function(msg) {
                    var agentType = "";
                	if ($scope.agentType != undefined && $scope.agentType != null) {
                		agentType = $scope.agentType;
                	}
                    agentStatusService.save({}, {
                        agentCommand: "STATUS_CHANGE",
                        agentStatus: "OFFLINE_AUTO_LOGOUT",
                        campaignId: $scope.campaign.id,
                        agentType: agentType
                    }).$promise.then(function (data) {
                            var loggedOut = LoginService.logout();
                            loggedOut.then(function() {
                            $scope.navigateToLoginScreen(msg);   
                        }, function (error) {
                            var loggedOut = LoginService.logout();
                            loggedOut.then(function() {
                            $scope.navigateToLoginScreen(msg);
                            })
                        });
                    });
                }

                $scope.navigateToLoginScreen = function(msg) {
                    if ($scope.agentIdleTimeInterval != null && $scope.agentIdleTimeInterval != undefined) {
                        $interval.cancel($scope.agentIdleTimeInterval);	
                    }
                    if ($scope.loggedInUserOrganization.enableAgentInactivity != null && $scope.loggedInUserOrganization.enableAgentInactivity != undefined && $scope.loggedInUserOrganization.enableAgentInactivity) {
                        var status;
                        if ($scope.isAutoDialingEnabled()) {
                            status = "You were logged out of the system automatically at " + $filter('date')(new Date(), 'HH:mm') + " on " + $filter('date')(new Date(), 'dd MMM yyyy') + " because the system detected sequencial auto dispositions on your calls.";
                            $rootScope.$emit("onAgentLoggedOut", status);
                        } else {
                            status = "You were logged out of the system automatically at " + $filter('date')(new Date(), 'HH:mm') + " on " + $filter('date')(new Date(), 'dd MMM yyyy') + " because the system detected you are idle since last " +  $scope.loggedInUserOrganization.manualAgentIdleTime + " minutes.";
                            $rootScope.$emit("onAgentLoggedOut", status);
                        }
                    }
                    if (msg != null && msg != undefined && msg == "noData") {
                        status = "You were logged out of the system automatically at " + $filter('date')(new Date(), 'HH:mm') + " on " + $filter('date')(new Date(), 'dd MMM yyyy') + " because there are zero callables in the campaign.";
                        $rootScope.$emit("onAgentLoggedOut", status);    
                    }
                    $location.path('/login');
                }
                
                $scope.updateAgentNetworkStatus = function(userId, networkstatus) {
                    AgentNetworkStatusUpdateService.postAgent({username:userId,networkstatus:networkstatus}).$promise.then(function(data){
                        console.log('Post-Agent Status : ' + data.networkstatus);
                    });
                }

        		$scope.onConnectionChange = function(obj) {
        			// console.info('PLIVO - onConnectionChange: ', obj);
        			if (obj.state === "connected") {
        				console.log('PLIVO - onConnectionChange : ' + obj);
        			} else if (obj.state === "disconnected") {
        				var connectionDisconnectError = obj.state + " " + obj.eventCode + " " + obj.eventReason;
        				// $scope.customAlert('error', connectionDisconnectError);
        			} else {
        				$scope.customAlert('error', 'PLIVO - Unknown connection state.');
        			}
        		}

        		$scope.submitCallQualityFeedback = function(callUUID, starRating, issues, note, sendConsoleLogs) {
        			console.log('PLIVO - Sending call quality feedback...');
        			$scope.plivoWebSdk.client.submitCallQualityFeedback(callUUID, starRating,issues, note,sendConsoleLogs).then((result) => {
        				console.log('PLIVO - feedback sent');
        			}).catch((error) => {
        				console.log('PLIVO - could not send feedback');
        			});
        		}

        		$scope.audioDeviceChange = function(e) {
        			console.info('audioDeviceChange', e);
        			if (e.change) {
        				if (e.change == "added") {
        					console.log(e.change + ' - ' + e.device.kind + ' - ' + e.device.label);
        				} else {
        					console.log(e.change + ' - ' + e.device.kind + ' - ' + e.device.label);
        				}
        			} else {
        				$scope.customAlert('error', 'There is an audioDeviceChange but mediaPermission is not allowed yet.');
        			}
        		}

        		$scope.customAlert = function(type, alertMessage) {
        			NotificationService.clear();
    				NotificationService.log(type, alertMessage);
        		}

                $scope.setSignalwireToNumberAndHeaders = function(toNumber, extraHeaders) {
        			$scope.signalwireToNumber = toNumber;
        			$scope.signalwireExtraHeaders = extraHeaders;
        		}

                $scope.resetSignalwireToNumberAndHeaders = function() {
        			$scope.signalwireToNumber = '';
        			$scope.signalwireExtraHeaders = {};
        		}

        		$scope.setPlivoToNumberAndHeaders = function(toNumber, extraHeaders) {
        			$scope.plivoToNumber = toNumber;
        			$scope.plivoExtraHeaders = extraHeaders;
        		}

        		$scope.resetPlivoToNumberAndHeaders = function() {
        			$scope.plivoToNumber = '';
        			$scope.plivoExtraHeaders = {};
        		}

        		$scope.refreshPlivo = function() {
                	$scope.plivoLogout();
                	var deletePlivoEndpointUrl = "/spr/plivo/endpoint/" + $scope.plivoEndpointId;
             	   	synchronousService(deletePlivoEndpointUrl, "DELETE", null);
                }

                $scope.refreshTelnyx = function() {
                	var deleteTelnyxConnectionUrl = "/spr/telnyx/connection/" + $scope.telnyxConnectionId.toString();
                    synchronousService(deleteTelnyxConnectionUrl, "DELETE", null);
                    if ($scope.telnyxCall) {
                        $scope.telnyxCall.hangup();
                    }
                    $scope.telnyxLogout();
                }

                $scope.refreshSignalwire = function() {
                    $scope.signalwireClient.hangup();
                	$scope.signalwireClient = null;
                	var deleteSignalwireEndpointUrl = "/spr/signalwire/endpoint/" + $scope.signalwireEndpointId;
             	   	synchronousService(deleteSignalwireEndpointUrl, "DELETE", null);
                }

        		/* PLIVO CODE */

            	$scope.agentType = agentDetails.agentType;
                $scope.parent = {};
                $scope.showCreateLink = false;
                $scope.forceSubmitInterval = undefined;
                $scope.newIndirectProspect = {};
                $scope.parent.state = 0;
                $scope.campaignCountryCode = "1";
                $scope.phoneNumber = "";
                $scope.hideCallStatusTimer = false;
                $scope.disableDialButtonInPreviewMode = false;
                $scope.oldStstus ="";
                $scope.assetName = "";
                $scope.submitLeadFlag = true;
                $scope.agentOnlineCount = 0;
                $scope.callLaterStatus = "";
                localStorage.setItem('agentOnlineCount', $scope.agentOnlineCount);
                var successDispositions = dispositionStatusService.get({dispositiontype: 'SUCCESS'}); // TODO::
                // Move
                // them
                // to
                // resolve
                var dialerCodeDispositions = dispositionStatusService.get({dispositiontype: 'DIALERCODE'});
                var failureDispositions = dispositionStatusService.get({dispositiontype: 'FAILURE'});
                $scope.allDispositionStatus = getDisplayLabelService.get({labelentity: 'dispositions'}); // TODO::
                $scope.getDispositionLabel = function(key){
                	var value = '';
                	angular
                    .forEach(
                    $scope.allDispositionStatus,
                    function (v, k) {
                        if(k == key.toUpperCase()){
                        	value = v.toUpperCase();
                        }
                    });
                	return value;
                }

                $scope.getDispositionLabelDispositionStatus = function(key){
                	var value = '';
                    if (key == "ANSWERMACHINE") {
                        key = "DIALERCODE";
                    }
                	angular
                    .forEach(
                    $scope.allDispositionStatus,
                    function (v, k) {
                        if(k == key.toUpperCase()){
                        	value = v.toUpperCase();
                        }
                    });
                	return value;
                }

                /* DATE :- 16/09/2019
                 * Added below to show campaign requirements and target segments to agents.
                 * START
                 */
                // $scope.getLabelAttributeSpecificValue = function(attributeName,value) {
                //     var pickAttributeData = $scope.getAttributePickList(attributeName);
                //     var specificPicklist = $filter('filter')(pickAttributeData, value)[0];
                //     if (!specificPicklist) {
                //         return value;
                //     }
                //     return specificPicklist.label;
                // };

                // $scope.getSymbolLabel = function(symbol) {
                //     switch (symbol) {
                //     case "=":
                //         return "is";
                //     case "!=":
                //         return "is not Equal to";
                //     case ">":
                //         return "greater Than";
                //     case "<":
                //         return "less Than";
                //     case ">=":
                //         return "greater than or equals";
                //     case "<=":
                //         return "less than or equals";
                //     default:
                //         return symbol;
                //     }
                // }

                $scope.getAttributePickList = function(attributeName) {
                    $scope.pickData = [];
                    if (attributeMappings[attributeName] != undefined) {
                        angular.forEach(attributeMappings[attributeName].pickList, function(v, k) {
                            $scope.pickData[k]=v;
                        });
                    }
                    return $scope.pickData;
                };
                 /*
                 * END
                 */

                $scope.getAssetsAndDefaultCustomFields = function() {
                    $scope.assetList = [];
                    // $scope.defaultCustomFields = [];
                    var tempAsset = [];
                    getCampaignDetailsForAgentService.get({id:$rootScope.agent.currentCampaign.id}).$promise.then(function(data){
                        $scope.campaignDetails = data;
                        $scope.defaultCustomFields = angular.copy($scope.campaignDetails.customFields);
                        if($scope.campaignDetails.multiCurrentAssetId != undefined && $scope.campaignDetails.multiCurrentAssetId.length > 0) {
                            tempAsset = $scope.campaignDetails.multiCurrentAssetId;
                        }
                        // if($scope.campaignDetails.currentAssetId) {
                        //     if(!tempAsset.includes($scope.campaignDetails.currentAssetId)) {
                        //         tempAsset.push($scope.campaignDetails.currentAssetId);
                        //     }
                        // }
                        angular.forEach(tempAsset, function(value, key) {
                            angular.forEach($scope.campaignDetails.assets, function(value1, key1) {
                                if (value == value1.assetId && (value1.isRemoved == null || value1.isRemoved == undefined || !value1.isRemoved)) {
                                    $scope.assetList.push(value1);
                                }
                            });
                        });
                        if($scope.assetList.length == 1) {
                            $scope.assetName = $scope.assetList[0].assetId;
                        }
                    }
                )};
                $scope.getAssetsAndDefaultCustomFields();

                $scope.getCurrentCampaign = function() {
                    currentCampaignService.get({}).$promise.then(function(data) {
                        $scope.campaign = data;
                    }
                )}

                $scope.assetSelected = function(id) {
                    $scope.assetName = id;
                    if ($scope.prospectCall != undefined && $scope.prospectCall != null) {
                        $scope.prospectCall.deliveredAssetId = id;
                    }
                    $scope.setSelectedAssetInfo(id);
                }

                /**
                 *  Date :- 24/09/2019
                 *  Set the selected asset info to display on UI screen.
                 *  START
                 */
                $scope.setSelectedAssetInfo = function(id) {
                    if ($scope.campaign.assets != null && $scope.campaign.assets != undefined) {
                        var asset = {};
                        asset = $scope.campaign.assets.find(asset => asset.assetId == id);
                        if (asset.name != null && asset.name != undefined) {
                            $scope.assetInfo.name = asset.name;
                            $scope.assetInfo.description = asset.description;
                            $scope.assetInfo.url = asset.url;
                        }
                    }
                }
                /* END  */

                $scope.loadCountryDetails = function() {
                    getCountryDetailsService.get().$promise.then(function(countryData) {
                        $scope.countryDetails = countryData;
                        $scope.checkCampaignCountry();
                    },function(error) {

                    });
                }
                $scope.loadCountryDetails();

                uiHelperService.apply($scope);

                $scope.dispositionStatuses = XTAAS_CONSTANTS.dispositionStatus;
                $scope.getDialerStatus = function() {
                	if ($rootScope.agent != null && $rootScope.agent.currentCampaign != null) {
                		getDialerStatus.get({campaignid: $rootScope.agent.currentCampaign.id}).$promise.then(function(data) {
                    		$scope.dialerStatus = data;
                    	});
                	}
                }

                $scope.getDialerStatus();

                $scope.dialerTimer =   $interval(function() {
                	$scope.getDialerStatus();
				}, 10000);

                $scope.currentProspectTimer =   $interval(function() {
                	if ($scope.this.parent.status == "ONLINE" && $scope.prospectCall == null) {
                		currentProspectService.get();
                	}
                }, 10000);

/////////////////////////start by pt////////////////
                $scope.disposition = "";
                $scope.guide = {};
                $scope.rebuttals = {};
                $scope.getState = {selectedState: ""}
                $scope.hrsList = [];
                $scope.hrsList[9] = "09";
                $scope.hrsList[10] = "10";
                $scope.hrsList[11] = "11";
                $scope.hrsList[12] = "12";
                $scope.hrsList[13] = "01";
                $scope.hrsList[14] = "02";
                $scope.hrsList[15] = "03";
                $scope.hrsList[16] = "04";
                $scope.hrsList[17] = "05";
                $scope.failureUnreachableProspectMessage = 'This is a key disposition used for determing Call List quality.  Use this disposition when:  We will never be able to talk directly to the prospect for ANY reason. Examples include:  disconnected phone, beeping noises, inability to get through IVR to a person no matter what we try, prospect doesn\'t work at the company any more, the phone number dialed didn\'t reach the correct company, the person you are talking to says "wrong number", the gatekeeper/receptionist/assistant will not allow us to talk to the prospect, etc.';
                var changeStatus = function (status, campaignId) {
                    var deferred = $q.defer();
                    var tmpStatus = status;
                    $log.debug("Submitting status change to server with status: " + status);
                    var agentType = "";
                	if ($scope.agentType != undefined && $scope.agentType != null) {
                		agentType = $scope.agentType;
                	}
                    agentStatusService.save({}, {
                        agentCommand: "STATUS_CHANGE",
                        agentStatus: tmpStatus,
                        campaignId: campaignId,
                        agentType: agentType
                    }).$promise.then(function () {
                            // $log.debug("Successfully submitted status change to server");
                            $scope.parent.status = status;
                            if ($scope.campaign.supervisorAgentStatusApprove) {
                                if ($scope.parent.status == 'REQUEST_BREAK' || $scope.parent.status == 'REQUEST_MEETING' || $scope.parent.status == 'REQUEST_TRAINING' || $scope.parent.status == 'REQUEST_OFFLINE' || $scope.parent.status == 'REQUEST_OTHER') {
                                    $scope.notices = [];
                                    var requestStatus = "";
                                    if ($scope.parent.status == 'REQUEST_BREAK') {
                                        requestStatus = "break"
                                    } else if ($scope.parent.status == 'REQUEST_MEETING') {
                                        requestStatus = "meeting"
                                    } else if ($scope.parent.status == 'REQUEST_TRAINING') {
                                        requestStatus = "training"
                                    } else if ($scope.parent.status == 'REQUEST_OFFLINE') {
                                        requestStatus = "offline"
                                    } else if ($scope.parent.status == 'REQUEST_OTHER') {
                                        requestStatus = "other"
                                    }
                                    var message = "Your " + requestStatus + " request is being reviewed by your Supervisor.";
                                    $scope.notices.push(message);
                                }
                            }
                            if ($scope.parent.status == 'ONLINE') {
                                $scope.agentOnlineCount = localStorage.getItem('agentOnlineCount');
                                if ($scope.agentOnlineCount > 0) {
                                    $scope.agentOnlineCount = parseInt($scope.agentOnlineCount) + 1;
                                    localStorage.setItem('agentOnlineCount', $scope.agentOnlineCount);
                                    if (!$scope.isModalOpened) {
                                        if ($sessionStorage.showStatusChangeModal == null || $sessionStorage.showStatusChangeModal == undefined || $sessionStorage.showStatusChangeModal == true) {
                                            $scope.showStateChangeModal();
                                        }
									}
                                    $scope.getCurrentCampaign();
                                }
                            }
                            if ($scope.parent.status == 'OFFLINE' || $scope.parent.status == 'ONBREAK' || $scope.parent.status == 'MEETING' || $scope.parent.status == 'TRAINING' || $scope.parent.status == 'OTHER') {
                                $scope.agentOnlineCount = parseInt($scope.agentOnlineCount) + 1;
                                localStorage.setItem('agentOnlineCount', $scope.agentOnlineCount);
                                if (!$scope.isModalOpened) {
                                	if ($sessionStorage.showStatusChangeModal == null || $sessionStorage.showStatusChangeModal == undefined || $sessionStorage.showStatusChangeModal == true) {
                                        $scope.showStateChangeModal();
                                    }
								}
                            }

                            if ($scope.parent.status == 'OFFLINE' && $scope.agentIdleTimeInterval != null && $scope.agentIdleTimeInterval != undefined) {
                                $interval.cancel($scope.agentIdleTimeInterval);
                            }

                            $rootScope.$broadcast('onStatusApproved', status, user.id);
                            deferred.resolve();
                            $rootScope.$broadcast(
									"checkSt",  $scope.oldStstus ,status);
                            if (status == 'ONBREAK' || status == 'MEETING' || status == 'TRAINING' || status == 'OTHER' || status == 'OFFLINE') {
                            		// set parameters for agent wait time monitoring
                            		$scope.prospectSubmittedTime = null;
                            		$scope.prospectSubmittedTimeOld = null;
                            		if ($scope.getCallControlProvider() == 'Plivo') {
                                        $scope.refreshPlivo();
                                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                                        $scope.telnyxHangupCall();
                                        $scope.refreshTelnyx();
                                       } else if ($scope.getCallControlProvider() == 'Signalwire') {
                                        $scope.refreshSignalwire();
                                    }
							}
                        }, function () {
                            $log.debug("Server rejected status change");
                            $rootScope.$broadcast('onStatusRejected', status, user.id);
                            deferred.reject();
                            if ($scope.getCallControlProvider() == 'Plivo') {
                                $scope.refreshPlivo();
                               } else if ($scope.getCallControlProvider() == 'Telnyx') {
                                $scope.telnyxHangupCall();
                                $scope.refreshTelnyx();
                               } else if ($scope.getCallControlProvider() == 'Signalwire') {
                                $scope.refreshSignalwire();
                            }
                        });
                    return deferred.promise;
                }

                $scope.loadAutoMachineDetectedValue = function() {
                        $scope.autoMachineHangup = '';
                        PropertyService.get({name : 'AUTO_MACHINE_HANGUP'}).$promise.then(function (data) {
                            if (data != undefined && data != null) {
                                $scope.autoMachineHangup = data.value;
                            }
                        }, function (error) {
                            console.log("Error occured while fetching application property.");
                        });
                }

                var nextCampaign = null;
                var setCampaign = function (campaign) {
                    if (campaign == null) {
                        $log.debug("Changing the state to idle state");
                        $scope.variable.campaign = campaignVariable;
                        $log.debug("Prev state " + $scope.parent.state);
                        console.log($scope.$id);
                        $scope.parent.state = 0;
                        $log.debug("New state " + $scope.parent.state);
                    } else {
                        $log.debug("Compiling campaign guides");

                        /* START
                         * DATE : 18/04/2017
                         * REASON : Added to handle errors if scripts are not present for campaign */
                        if (campaign.callGuide == null) {
                        	campaign.callGuide = {};
                        	campaign.callGuide.confirmSection = {};
                        	campaign.callGuide.confirmSection.script = "";
                        	campaign.callGuide.questionSection = {};
                        	campaign.callGuide.questionSection.script = "";
                        	campaign.callGuide.consentSection = {};
                        	campaign.callGuide.consentSection.script = "";
                        	campaign.callGuide.noteSection = {};
                        	campaign.callGuide.noteSection.script = "";
                        }
                        if (campaign.callGuide.confirmSection == null) {
                        	campaign.callGuide.confirmSection = {};
                        	campaign.callGuide.confirmSection.script = "";
                        }
                        if (campaign.callGuide.questionSection == null) {
                        	campaign.callGuide.questionSection = {};
                        	campaign.callGuide.questionSection.script = "";
                        }
                        if (campaign.callGuide.consentSection == null) {
                        	campaign.callGuide.consentSection = {};
                        	campaign.callGuide.consentSection.script = "";
                        }
                        if (campaign.callGuide.noteSection == null) {
                        	campaign.callGuide.noteSection = {};
                        	campaign.callGuide.noteSection.script = "";
                        }
                        /* END */

                        campaign.callGuide.confirmSectionFn = _.template(campaign.callGuide.confirmSection.script);
                        campaign.callGuide.questionSectionFn = _.template(campaign.callGuide.questionSection.script);
                        campaign.callGuide.consentSectionFn = _.template(campaign.callGuide.consentSection.script);
                        campaign.callGuide.noteSectionFn = _.template(campaign.callGuide.noteSection.script);

                        $log.debug("Changing the state to campaign state");
                        var currentAsset;

                        /* START
                         * DATE : 18/04/2017
                         * REASON : Added to handle errors if assets are not present for campaign */
                        if (campaign.assets == null) {
                        	campaign.assets = {};
						}
                        /* END */

                        $scope.assetInfo = {};
                        for (var index = 0; index < campaign.assets.length; index++) {
                            if (campaign.assets[index].assetId == campaign.currentAssetId && (campaign.assets[index].isRemoved == null || campaign.assets[index].isRemoved == undefined || !campaign.assets[index].isRemoved)) {
                                currentAsset = campaign.assets[index];
                                $scope.assetName = currentAsset.assetId;        // To bind the active asset name to dropdown.
                                $scope.assetInfo.name = currentAsset.name;     // Set 3 parameters to  display the selected asset info in asset section.
                                $scope.assetInfo.description = currentAsset.description;
                                $scope.assetInfo.url = currentAsset.url;
                                break;
                            }
                        }
                        $scope.variable.campaign = {
                            name: campaign.name,
                            currentAssetName: angular.isDefined(currentAsset) ? currentAsset.name : undefined
                        }
                        $log.debug("Prev state " + $scope.parent.state);
                        console.log($scope.$id);
                        $scope.parent.state = 1;
                        $log.debug("New state " + $scope.parent.state);
                    }
                    $scope.campaign = campaign;

                    $scope.checkDialerMode();

                    if ($scope.campaign != null && $scope.campaign != undefined && $scope.campaign.enableMachineAnsweredDetection) {
                        $scope.loadAutoMachineDetectedValue();
                    }

                    $scope.showCustomQuestionsOnMobileUI = false;
                    if (Object.keys($scope.campaign.questionAttributeMap).length !== 0) {
                        $scope.showCustomQuestionsOnMobileUI = true;
                    }
                    $scope.checkAllLeadDetailsInConfirmTab();
                    if ($scope.campaign != undefined && ($scope.campaign.dialerMode == 'RESEARCH'
                		|| ($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'MANUAL') || $scope.campaign.type == 'Prospecting')) {
                        if ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined) {
                            $scope.showCreateLink = true;    
                        }
                        if ($scope.campaign.hidePhone) {
                            $scope.showPhoneNumber = false;
                        } else {
                            $scope.showPhoneNumber = true;
                        }
                    }
                    $scope.totalQuestion = 0;
                    angular.forEach($scope.campaign.questionAttributeMap, function(v, k) {
                    	$scope.totalQuestion = $scope.totalQuestion + 1;
   	             	});
                    console.log($scope.totalQuestion);

                    $scope.isCurrentQuestion = 1;

                    twilioService.disconnect();
                    $rootScope.$broadcast("whisper-stop");
                    $rootScope.$broadcast("onCampaignChange", campaign);
                }

                $scope.checkDialerMode = function() {
                    if ($scope.campaign != null && $scope.campaign != undefined) {
                        if ($scope.campaign.dialerMode == 'PREVIEW') {
                            $scope.checkIfAgentDialedToProspect = "Yes";
                            $scope.showSimpleDisposition = "";
                        } else {
                            if ($scope.campaign.simpleDisposition) {
                                $scope.showSimpleDisposition = "simpleDisposition";
                            } else {
                                $scope.showSimpleDisposition = "nonSimpleDisposition";   
                            }
                        }
                    }
                }

                $scope.checkAllLeadDetailsInConfirmTab = function() {
                    if ($scope.campaign.type === 'Lead Verification' || $scope.campaign.type === 'LeadVerification') {
                        $scope.checkeditname = "yes";
                        $scope.checkeditemail = "yes";
                        $scope.checkedittitle = "yes";
                        $scope.checkeditdept = "yes";
                        $scope.checkeditphone = "yes";
                        $scope.checkeditcompany = "yes";
                    }
                }

                $scope.checkCampaignCountry = function () {
                    angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionValue, expressionKey) {
                        if(expressionValue.attribute == "VALID_COUNTRY") {
                        	var countryArray = [];
                        	if (expressionValue.value != undefined) {
                        		countryArray = expressionValue.value[0];
							} else {
								countryArray.push('USA');
							}
                        	$scope.campaignCountry = countryArray[0];
                        }
                    });
                    angular.forEach($scope.countryDetails, function(expressionValue, expressionKey) {
                        if ($scope.campaignCountry == expressionValue.countryName) {
                        	$scope.campaignCountryCode = expressionValue.isdCode;
                        }
                    });
                }

                $scope.changeProspectCountry = function () {
                    angular.forEach($scope.countryDetails, function(value, key) {
                        if ($scope.campaignCountryCode == value.isdCode) {
                        	$scope.campaignCountry = value.countryName;
                        }
                    });
                    $scope.prospectCall.prospect.country = $scope.campaignCountry;
                }

                $scope.isAutoDialingEnabled = function () {
                    if ($scope.parent.state < 1) {
                        return false;
                    } else if ($scope.campaign.type == "Prospecting") {
                        return false;
                    }  else if ($scope.campaign.dialerMode == "PREVIEW"
                    	|| $scope.campaign.dialerMode == "PREVIEW_SYSTEM"
                    	|| $scope.campaign.dialerMode == 'RESEARCH') {
                        return false;
                    } else if ($scope.agentType != undefined
                    		&& $scope.agentType != null
                    		&& $scope.agentType == 'MANUAL'
                    		&& $scope.campaign.dialerMode == "HYBRID") {
                    	return false;
                    } else if ($scope.agentType != undefined
                    		&& $scope.agentType != null
                    		&& $scope.agentType == 'AUTO'
                    		&& $scope.campaign.dialerMode == "HYBRID") {
                    	return true;
                    } else {
                        return true;
                    }
                }

                $scope.$on('onChangeStatus', function (event, status) {
                    if ($scope.parent.status != status) {
                    	$scope.parent.status = status;
                        $scope.parent.userId = user.id;
                        if ($sessionStorage.callbackProspect != null && $sessionStorage.callbackProspect != undefined 
                              && $sessionStorage.callbackProspect.prospect != null && $sessionStorage.callbackProspect.prospect != undefined) {
                            $scope.prospect = {};
                            $scope.prospectCall = $sessionStorage.callbackProspect;
                            $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                            $rootScope.originalHiddenPhone = $scope.prospect.originalHiddenPhone;
                            if ($scope.campaign.hidePhone) {
                                $scope.parent.numberToDial = "xxx-xxx-xxxx";
                                $scope.prospectCall.prospect.phone = "xxx-xxx-xxxx";
                            } else {
                                $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                            }
                            $scope.prospectCall.prospect.name = $scope.prospectCall.prospect.firstName + " " + $scope.prospectCall.prospect.lastName;
                            $scope.callHistory = $scope.prospectCall.notes;
                            $scope.prospectNote = [];
                            if ($scope.prospectCall.notes != null) {
                                angular.forEach($scope.prospectCall.notes,function(note,index) {
                                    $scope.prospectNote.push(note);
                                });
                            }
                            $scope.checkIfAgentDialedToProspect = "Yes";
                            $scope.showSimpleDisposition = "";
                            $scope.prospectCall.prospect.prospectType = '';
                            $scope.parent.state = 4;
                            // set prospect received time for agent call monitoring
                            $scope.prospectReceivedTime = new Date();
                            // $scope.callbackEventOccured = false;
                            $scope.createEndPointAndChangeStatusToOnline(status);
                        } else {
                            if ($scope.isAutoDialingEnabled()) {
                                if (status == "ONLINE") {
                                    $rootScope.$broadcast("whisper-start", user.id);
                                    $log.debug("Starting conference for user: " + user.id);
                                    var from = 'client:' + user.id;
                                    var to = user.id;
                                    var isAgent = true;
                                    var agentType = "";
                                    if ($scope.agentType != undefined && $scope.agentType != null) {
                                        agentType = $scope.agentType;
                                    }
                                    if ($scope.getCallControlProvider() == 'Plivo') {
                                        if ($scope.plivoWebSdk == null || !$scope.plivoWebSdk.client.isLoggedIn){
                                            PlivoService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
                                                if (endpoint.PLIVO_ENDPOINT1 && endpoint.PLIVO_ENDPOINT2) {
                                                    $scope.plivoEndpointId = endpoint.PLIVO_ENDPOINT_ID;
                                                    $scope.plivoEndpointUsername = endpoint.PLIVO_ENDPOINT1;
                                                    $scope.plivoInitPhone();
                                                    var toNumber = user.id;
                                                    var extraHeaders = {
                                                        'X-PH-from': 'client:' + user.id,
                                                        'X-PH-agentId' : user.id,
                                                        'X-PH-agentType' : agentType,
                                                        'X-PH-campaignId' : $scope.campaign.id,
                                                        'X-PH-dialerMode': $scope.campaign.dialerMode
                                                    };
                                                    $scope.setPlivoToNumberAndHeaders(toNumber, extraHeaders);
                                                    $scope.plivoLogin(endpoint.PLIVO_ENDPOINT1, endpoint.PLIVO_ENDPOINT2);
                                                }
                                            }, function (error) {
                                                NotificationService.log('error', 'An error occurred while creating plivo endpoint.');
                                            });
                                        }
                                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                                        if ($scope.telnyxClient == null ) {
                                            TelnyxService.createConnection({username: user.id}).$promise.then(function (connection) {
                                                if (connection.TELNYX_CONNECTION_USERNAME && connection.TELNYX_CONNECTION_PASSWORD) {
                                                    $scope.telnyxConnectionId = connection.TELNYX_CONNECTION_ID;
                                                    $scope.telnyxConnectionUsername = connection.TELNYX_CONNECTION_USERNAME;
                                                    $scope.telnyxInit(connection.TELNYX_CONNECTION_USERNAME, connection.TELNYX_CONNECTION_PASSWORD);
                                                }
                                            }, function (error) {
                                                NotificationService.log('error', 'An error occurred while creating Telnyx connection.');
                                            });
                                        }
                                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                                        // if ($scope.plivoWebSdk == null || !$scope.plivoWebSdk.client.isLoggedIn){
                                        if ($scope.signalwireClient == null) {
                                            SignalwireService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
                                                if (endpoint.SIGNALWIRE_ENDPOINT_USERNAME && endpoint.SIGNALWIRE_ENDPOINT_PASSWORD) {
                                                    $scope.signalwireEndpointId = endpoint.SIGNALWIRE_ENDPOINT_ID;
                                                    $scope.signalwireEndpointUsername = endpoint.SIGNALWIRE_ENDPOINT_USERNAME;
                                                    $scope.signalwireDomainApp = endpoint.SIGNALWIRE_DOMAINAPP;
                                                    $scope.signalwireInit($scope.signalwireEndpointUsername, endpoint.SIGNALWIRE_ENDPOINT_PASSWORD);
                                                    // var toNumber = user.id;
                                                    // var extraHeaders = {
                                                    // 	'X-PH-from': 'client:' + user.id,
                                                    // 	'X-PH-agentId' : user.id,
                                                    // 	'X-PH-agentType' : agentType,
                                                    // 	'X-PH-campaignId' : $scope.campaign.id,
                                                    // 	'X-PH-dialerMode': $scope.campaign.dialerMode
                                                    // };
                                                    // $scope.setPlivoToNumberAndHeaders(toNumber, extraHeaders);
                                                    // $scope.plivoLogin(endpoint.SIGNALWIRE_ENDPOINT_USERNAME, endpoint.SIGNALWIRE_ENDPOINT_PASSWORD);
                                                }
                                            }, function (error) {
                                                NotificationService.log('error', 'An error occurred while creating Signalwire endpoint.');
                                            });
                                        }
                                    } else {
                                        twilioTokenService.getToken().then(function (token) {
                                            twilioService.connect(token, {
                                                From: from,
                                                To: to,
                                                pn: user.id,
                                                agent: isAgent,
                                                agentType: agentType,
                                                campaignId: $scope.campaign.id
                                            }).then(function (parameters) {
                                                $log.debug("Conference started successfully with call sid:" + parameters.CallSid);
                                                changeStatus(status, $scope.campaign == null ? undefined : $scope.campaign.id)
                                            }, function (error) {
                                                $log.debug("Error occured while creating conference");
                                                $log.debug(error);
                                            });
                                        });
                                    }
                                    changeStatus(status, $scope.campaign == null ? undefined : $scope.campaign.id)
                                } else {
                                    changeStatus(status, $scope.campaign == null ? undefined : $scope.campaign.id).then(function () {
                                        if ($scope.parent.state < 2) {
                                            $log.debug("Disconnecting active connections because of status chagne");
                                            twilioService.disconnect();
                                            $rootScope.$broadcast("whisper-stop");
                                        }
                                    });
                                }
                            } else {
                                $scope.createEndPointAndChangeStatusToOnline(status);
                            }
                        }
                    }
                });


                $scope.createEndPointAndChangeStatusToOnline = function(status) {
                    if ($scope.getCallControlProvider() == 'Plivo') {
                        if (status == 'ONLINE' && ($scope.plivoWebSdk == null || !$scope.plivoWebSdk.client.isLoggedIn)) {
                            PlivoService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
                                if (endpoint.PLIVO_ENDPOINT1 && endpoint.PLIVO_ENDPOINT2) {
                                    $scope.plivoEndpointId = endpoint.PLIVO_ENDPOINT_ID;
                                    $scope.plivoEndpointUsername = endpoint.PLIVO_ENDPOINT1;
                                    $scope.plivoInitPhone();
                                    $scope.plivoLogin(endpoint.PLIVO_ENDPOINT1, endpoint.PLIVO_ENDPOINT2);
                                }
                            }, function (error) {
                                NotificationService.log('error', 'An error occurred while creating plivo endpoint.');
                            });
                        }
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        // Telnyx code for manual dial
                        if (status == 'ONLINE' && $scope.telnyxClient == null ){
                            TelnyxService.createConnection({username: user.id}).$promise.then(function (connection) {
                                if (connection.TELNYX_CONNECTION_USERNAME && connection.TELNYX_CONNECTION_PASSWORD) {
                                    $scope.telnyxConnectionId = connection.TELNYX_CONNECTION_ID;
                                    $scope.telnyxConnectionUsername = connection.TELNYX_CONNECTION_USERNAME;
                                    $scope.telnyxInit(connection.TELNYX_CONNECTION_USERNAME, connection.TELNYX_CONNECTION_PASSWORD);
                                    }
                                }, function (error) {
                                NotificationService.log('error', 'An error occurred while creating Telnyx connection.');
                                });
                            }
                        } else if ($scope.getCallControlProvider() == 'Signalwire') {
                            if (status == 'ONLINE' && $scope.signalwireClient == null) {
                                SignalwireService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
                                    if (endpoint.SIGNALWIRE_ENDPOINT_USERNAME && endpoint.SIGNALWIRE_ENDPOINT_PASSWORD) {
                                        $scope.signalwireEndpointId = endpoint.SIGNALWIRE_ENDPOINT_ID;
                                        $scope.signalwireEndpointUsername = endpoint.SIGNALWIRE_ENDPOINT_USERNAME;
                                        $scope.signalwireDomainApp = endpoint.SIGNALWIRE_DOMAINAPP;
                                        $scope.signalwireInit($scope.signalwireEndpointUsername, endpoint.SIGNALWIRE_ENDPOINT_PASSWORD);
                                    }
                                }, function (error) {
                                    NotificationService.log('error', 'An error occurred while creating Signalwire endpoint.');
                                });
                                // $scope.customAlert('error', 'PLIVO - You are not Logged in. Please try again.');
                                // return;
                            }
                        }
                        // Hack for callback online in case of AUTO dial.
                        if (($sessionStorage.callbackProspect != null || $sessionStorage.callbackProspect != undefined) && status == "ONLINE") {
                            
                        } else {
                            changeStatus(status, $scope.campaign == null ? undefined : $scope.campaign.id);
                        }
                }

                $scope.createEndPointOnPlivo = function() {
                    PlivoService.createEndpoint({username: user.id}).$promise.then(function (endpoint) {
                        if (endpoint.PLIVO_ENDPOINT1 && endpoint.PLIVO_ENDPOINT2) {
                            $scope.plivoEndpointId = endpoint.PLIVO_ENDPOINT_ID;
                            $scope.plivoEndpointUsername = endpoint.PLIVO_ENDPOINT1;
                            $scope.plivoInitPhone();
                            $scope.plivoLogin(endpoint.PLIVO_ENDPOINT1, endpoint.PLIVO_ENDPOINT2);
                        }
                    }, function (error) {
                        NotificationService.log('error', 'An error occurred while creating plivo endpoint.');
                    });
                }

                $scope.$on('saveOldStatus', function (event, tempStatus) {
                	$scope.oldStstus = tempStatus;
                });

                var setNextCampaign = function (campaign) {
                    if (campaign == null) {
                        changeStatus("IDLE");
                    } else {
                    	// changeStatus("GEARING", campaign.id);
                    	// 02/08/2019 - creating issue. sending 2 calls to the backend for online state
                        // changeStatus("ONLINE", campaign.id);
                    }
                    // $log.debug("Setting the next campaign: ");
                    // $log.debug(campaign);
                    nextCampaign = campaign;
                    setCampaign(nextCampaign);
                    if (campaign == null) {
                    	 $scope.$broadcast("onChangeStatus", "IDLE");
                    } else {
                    	 $scope.$broadcast("onChangeStatus", "ONLINE");
                    }
                }

                
                var today = new Date();
                var nextmonthdate = new Date().setDate(today.getDate() + 30);
                var mindate = $filter('date')(today, 'yyyy-MM-dd');
                var maxdate = $filter('date')(nextmonthdate, 'yyyy-MM-dd');

                $scope.callback = {
                    currentstateName: "",
                    minDate: mindate,
                    maxDate: maxdate,
                    callbackDate: "",
                    hoursList: [],
                    minsList: [],
                    callbackHours: 0,
                    callbackMins: 0,
                    timeZone: "US/Pacific",
                    currentTimeZone: "US/Pacific",
                    currentTime: new Date(),
                    currentEventDate: "",
                    starthours: "",
                    endhours: "",
                    ampm: "",
                    endworkinghours: ""
                };
                $scope.isSelectState = false;
                $scope.isMinDisable = false;
                $scope.callback.minsList = ["00", "15", "30", "45"];

                $scope.open = function ($event) {

                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.opened = true;
                };

                /*$scope.setCallStatus1 = function (dispositionStatus) {
                    console.log($scope.prospectCall.prospect.stateCode)
                    getStateService.query().$promise
                        .then(
                        function (data) {
                            //$scope.stateList = data;
                            $scope.stateList = [];
                            angular
                                .forEach(
                                data,
                                function (v, k) {
                                    var state = v.stateCode.split("-");
                                    $scope.stateList.push({"stateCode": state[1], "stateName": v.stateName});

                                });


                        },
                        function (error) {

                        });

                    getCallbackTimezoneService.get({id: $scope.prospectCall.prospect.stateCode}).$promise
                        .then(
                        function (data) {

                            console.log(data);
                            angular
                                .forEach(
                                $scope.stateList,
                                function (v, k) {

                                    if (v.stateCode == $scope.prospectCall.prospect.stateCode) {
                                        $scope.callback.currentstateName = v.stateName;
                                    }

                                });

                            $scope.callback.currentTimeZone = data.StateTimeZone;
                            $scope.callback.currentTime = data.CurrentTimeInStateTZ;
                            $scope.callback.endworkinghours = data.AgentWorkEndHrInStateTZ;
                            $scope.callback.timeZone = data.StateTimeZone;
                            $scope.callback.hoursList = [];
                            for (i = data.AgentWorkStartHrInStateTZ; i <= data.AgentWorkEndHrInStateTZ; i++) {

                                $scope.callback.hoursList.push($scope.hrsList[i]);

                            }
                            console.log( $scope.callback.hoursList);


                        },
                        function (error) {

                        });


                    $scope.isSelectState = false;
                    var callbackModal = $modal.open({
                        scope: $scope,
                        templateUrl: 'callback-modal.html',
                        controller: 'CallbackController',
                        windowClass: 'setcallback'
                    });

                };*/

                $scope.$watch('callback.currentEventDate', function(newval, oldval){
                 if ($scope.callback.currentEventDate != undefined) {

                 var now = new Date().getTime();
                 var today =  $filter('date')(now, 'yyyy-MM-dd');

                 if ($scope.callback.currentEventDate < today) {
                 $scope.callback.currentEventDate ='';
                 }
                 //$scope.minDate = date[0]+'-'+date[1]+'-'+day;
                 }
                 });

                ////////////////////////end by pt//////////////

                // hotkeys.bindTo($scope)
                //     .add({
                //         combo: "ctrl+alt+r",
                //         description: "Reset the entire state of application",
                //         callback: function () {
                //             $log.debug("Reset activated");
                //             $scope.prospectCall = null;
                //             $scope.campaign = null;
                //             nextCampaign = null;
                //             $scope.variable.campaign = campaignVariable;
                //             $scope.variable.prospect = prospectVariable;
                //             delete $scope.parent.numberToDial;
                //             delete $scope.parent.note;
                //             $rootScope.$broadcast("onCampaignChange", null);
                //             $rootScope.$broadcast("timer-reset");
                //             changeStatus("IDLE");
                //             $log.debug("Prev state " + $scope.parent.state);
                //             console.log($scope.$id);
                //             $scope.parent.state = 0;
                //             $log.debug("New state " + $scope.parent.state);
                //             twilioService.disconnect();
                //         }
                //     })
                //     .add({
                //         combo: "ctrl+alt+t",
                //         description: "Reset the twilio call state",
                //         callback: function () {
                //             console.log("Reset the twilio state");
                //             $scope.prospectCall = null;
                //             delete $scope.parent.numberToDial;
                //             delete $scope.parent.note;
                //             $scope.variable.prospect = prospectVariable;
                //             if (nextCampaign != $scope.campaign) {
                //                 $log.debug("Lazy loading campaign");
                //                 setCampaign(nextCampaign);
                //             } else {
                //                 $log.debug("Changing the state to campaign state")
                //                 $log.debug("Prev state " + $scope.parent.state);
                //                 console.log($scope.$id);
                //                 $scope.parent.state = 1;
                //                 $log.debug("New state " + $scope.parent.state);
                //             }
                //             $log.debug("Disconnecting active connections because of status chagne");
                //             twilioService.disconnect();
                //             if ($scope.campaign == null) {
                //                 changeStatus("IDLE");
                //             } else {
                //                 changeStatus("GEARING", $scope.campaign.id);
                //             }
                //             $log.debug("Broadcasting the prospect change");
                //             $rootScope.$broadcast("timer-reset");
                //         }
                //     });

                // Code related to heartbeat
                var heartBeatCheck = 0;
                var stop = $interval(function () {
                	var tmpstatus = $scope.parent.status;
                	var agentType = "";
                	if ($scope.agentType != undefined && $scope.agentType != null) {
                		agentType = $scope.agentType;
                	}
                    var startTime = (new Date()).getTime(), endTime;
                    agentStatusService.save({}, {
                        agentCommand: "HEARTBEAT",
                        agentStatus: tmpstatus,
                        campaignId: $scope.campaign != null ? $scope.campaign.id : undefined,
                        agentType: agentType
                    }).$promise.then(function (data) {
                        heartBeatCheck = 0;
                        if ($scope.campaign != null && $scope.campaign != undefined && $scope.campaign.enableInternetCheck) {
                                endTime = (new Date()).getTime();
                                var timeDiff = endTime - startTime;
                                if ((timeDiff / 1000) > 1.2) {
                                    if ($scope.isAutoDialingEnabled()) {
                                        NotificationService.log('error', 'Your network is unsuitable to be working with XTaaS, please check network and relogin.');
                                        alert("Your network is unsuitable to be working with XTaaS, please check network and relogin.");
                                        $scope.endCall();
                                        $scope.navigateToLoginScreen();
                                    } else {
                                        NotificationService.log('error', 'Unstable network detected - Please check network.');
                                        alert("Unstable network detected - Please check network.");
                                    }
                                }
                        }
                    }, function (error) {
                        if ($scope.campaign != null && $scope.campaign != undefined && $scope.campaign.enableInternetCheck) {
                            if (error.status == 0) {
                                heartBeatCheck++;
                                if (heartBeatCheck >= 2) {
                                    if ($scope.isAutoDialingEnabled()) {
                                        NotificationService.log('error', 'Your network is unsuitable to be working with XTaaS, please check network and relogin.');
                                        alert("Your network is unsuitable to be working with XTaaS, please check network and relogin.");
                                        $scope.endCall();
                                        $scope.navigateToLoginScreen();
                                    } else {
                                        NotificationService.log('error', 'Unstable network detected - Please check network.');
                                        alert("Unstable network detected - Please check network.");
                                    }
                                }
                            } else {
                                endTime = (new Date()).getTime();
                                var timeDiff = endTime - startTime;
                                if ((timeDiff / 1000) > 1.2) {
                                    if ($scope.isAutoDialingEnabled()) {
                                        NotificationService.log('error', 'Your network is unsuitable to be working with XTaaS, please check network and relogin.');
                                        alert("Your network is unsuitable to be working with XTaaS, please check network and relogin.");
                                        $scope.endCall();
                                        $scope.navigateToLoginScreen();
                                    } else {
                                        NotificationService.log('error', 'Unstable network detected - Please check network.');
                                        alert("Unstable network detected - Please check network.");
                                    }
                                }
                            }
                        }
                    });
                    if($scope.getCallControlProvider() == 'Plivo') {
                    	if ($scope.isAutoDialingEnabled() && ($scope.parent.status == 'ONLINE' || $scope.isStateThreeAbove())) {
                    		// TODO - PLIVO - need to revisit
                    	}
                    } else if($scope.getCallControlProvider() == 'Telnyx') {
                    	if ($scope.isAutoDialingEnabled() && ($scope.parent.status == 'ONLINE' || $scope.isStateThreeAbove())) {
                    		// TODO - Telnyx - need to revisit
                    	}
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {

                    } else {
                    	if (!twilioService.isInProgress() && $scope.isAutoDialingEnabled()
                            && ($scope.parent.status == 'ONLINE' || $scope.isStateThreeAbove())) {
                            $log.debug("Retrying to join conference room");
                            twilioTokenService.getToken().then(function (token) {
                            	var from = 'client:' + user.id;
                            	var to = user.id;
                            	var isAgent = true;
                            	var agentType = "";
                            	if ($scope.agentType != undefined && $scope.agentType != null) {
                            		agentType = $scope.agentType;
                            	}
                                twilioService.connect(token, {
                                	From: from,
                                	To: to,
                                    pn: user.id,
                                    agent: isAgent,
                                    agentType: agentType,
                                    campaignId: $scope.campaign.id
                                })
                            });
                        }
                    }
                    $rootScope.$broadcast('background-tick');
                }, 30000, false);

                var stopHeartBeat = function () {
                    if (angular.isDefined(stop)) {
                        $interval.cancel(stop);
                        stop = undefined;
                    }
                };

                $scope.$on('$destroy', function () {
                    $log.debug("Unsubscribing channel for $scope:" + $scope.$id);
                    Pusher.unsubscribe(user.id);
                    $log.debug("Unsubscribing from twilio for $scope:" + $scope.$id);
                    twilioService.disconnect();
                    stopHeartBeat();
                    $interval.cancel($scope.dialerTimer);
                    $interval.cancel($scope.currentProspectTimer);
                    if (angular.isDefined($scope.forceSubmitInterval)) {
                        $interval.cancel($scope.forceSubmitInterval); // cancel force auto submit interval
                        $scope.forceSubmitInterval = undefined;
                    }
                    $window.localStorage.clear();
                });
                // Code related to heart beat ends here


                var prospectVariable = {
                    prefix: '<%= prospect.prefix %>',
                    firstName: '<%= prospect.firstName %>',
                    lastName: '<%= prospect.lastName %>',
                    suffix: '<%= prospect.suffix %>',
                    fullname: '<%= prospect.fullName %>',
                    fullAddress: '<%= prospect.fullAddress %>',
                    stateCode: '<%= prospect.stateCode %>',
                    country: '<%= prospect.country %>',
                    email: '<%= prospect.email %>',
                    company: '<%= prospect.company %>',
                    industry: '<%= prospect.industry %>'
                }

                var campaignVariable = {
                    name: '<%= campaign.currentAssetName %>',
                    currentAssetName: '<%= campaign.currentAssetName %>'
                }

                $scope.variable = {
                    agent: {
                        firstName: user.firstName,
                        lastName: user.lastName
                    },
                    campaign: campaignVariable,
                    prospect: prospectVariable
                };
                var setProspectVariables = function () {
                    $scope.variable.prospect = {
                        prefix: $scope.prospectCall.prospect.prefix != null ? $scope.prospectCall.prospect.prefix : '<%= prospect.prefix %>',
                        firstName: $scope.prospectCall.prospect.firstName != null ? $scope.prospectCall.prospect.firstName : '<%= prospect.firstName %>',
                        lastName: $scope.prospectCall.prospect.lastName != null ? $scope.prospectCall.prospect.lastName : '<%= prospect.lastName %>',
                        suffix: $scope.prospectCall.prospect.suffix != null ? $scope.prospectCall.prospect.suffix : '<%= prospect.suffix %>',
                        fullname: $scope.getProspectName(),
                        fullAddress: $scope.getProspectAddress(),
                        stateCode: $scope.prospectCall.prospect.stateCode != null ? $scope.prospectCall.prospect.stateCode : '<%= prospect.stateCode %>',
                        country: $scope.prospectCall.prospect.country != null ? $scope.prospectCall.prospect.country : '<%= prospect.country %>',
                        email: $scope.prospectCall.prospect.email != null ? $scope.prospectCall.prospect.email : '<%= prospect.email %>',
                        company: $scope.prospectCall.prospect.company != null ? $scope.prospectCall.prospect.company : '<%= prospect.company %>',
                        industry: $scope.prospectCall.prospect.industry != null ? $scope.prospectCall.prospect.industry : '<%= prospect.industry %>'
                    }
                }

                $scope.prospectCall = {};
                $scope.callHistory = [];
                $scope.campaign = null; ////$rootScope.agent.currentCampaign;
                if ($rootScope.agent != null && $rootScope.agent != undefined) {
                    setNextCampaign($rootScope.agent.currentCampaign);

                    $scope.setConfirmMenuOptions = function () {
                    	if($scope.campaign.questionAttributeMap) {
                    		$scope.noTitleAvailable = false;
                        	$scope.noDepartmentAvailable = false;
                        	angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                        		if(questAttrValue.attributeType == XTAAS_CONSTANTS.attributeType.role) {
            						$scope.noTitleAvailable = true;
            					}
                        		if(questAttrValue.attributeType == XTAAS_CONSTANTS.attributeType.department) {
            						$scope.noDepartmentAvailable = true;
            					}
                        	});
                    	}
                    }

                    $scope.setConfirmMenuOptions();

                } else {
                    $state.go("agentsplash");
                    $state.go("agentsplash");
                }


                $log.debug("Prev state " + $scope.parent.state);
                console.log($scope.$id);
                //$scope.parent.state = 0; // 0 - Idle, 1 - Campaign Allocated, 2 -
                // Propsect Allocated, 3 - Call in progress, 4 -
                // wrap up state
                $log.debug("New state " + $scope.parent.state);

                $scope.$watch(function (scope) {
                        return scope.variable
                    },
                    function () {
                        if ($scope.campaign != null) {
                            $scope.guide.confirmSectionGuide = $scope.campaign.callGuide.confirmSection.script;
                            $scope.guide.questionSectionGuide = $scope.campaign.callGuide.questionSection.script;
                            $scope.guide.consentSectionGuide = $scope.campaign.callGuide.consentSection.script;
                            $scope.guide.noteSectionGuide = $scope.campaign.callGuide.noteSection.script;

                            $scope.rebuttals.confirmSectionRebuttals = $scope.campaign.callGuide.confirmSection.rebuttals;
                            $scope.rebuttals.questionSectionRebuttals = $scope.campaign.callGuide.questionSection.rebuttals;
                            $scope.rebuttals.consentSectionRebuttals = $scope.campaign.callGuide.consentSection.rebuttals;
                            $scope.rebuttals.noteSectionRebuttals = $scope.campaign.callGuide.noteSection.rebuttals;
                        } else {
                            $scope.guide = {};
                        }
                    },
                    true
                );


                $scope.checkQualifiedCriteria = function (selectedQuestionAttribute, selectedOption, questionPickListObj, questionIndex, selectedQuestionsObj) {
                	$scope.disQualifiedFlag = false;
                	selectedQuestionsObj.qualifiedFlag = false;
                	var attributeFound = false;

                	$scope.prospectCall.answers[questionIndex].answer = selectedOption;

                	if($scope.campaign.qualificationCriteria != null && $scope.campaign.qualificationCriteria != undefined) {
		                 angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionsValue, expressionsKey) {
		                		if(expressionsValue.attribute == selectedQuestionAttribute) {
		                			attributeFound = true;
		                			if(expressionsValue.operator == "IN" || expressionsValue.operator == "=") {
                                        var attributeValues = {};
                                        if (!angular.isArray(expressionsValue.value)) {
                                            attributeValues = expressionsValue.value.split(',');
                                        } else {
                                            attributeValues = expressionsValue.value;
                                        }
		                				var qualifiedResult = $filter('filter')(attributeValues, selectedOption)[0];
		                				if(qualifiedResult == undefined) {
		                					$scope.disQualifiedFlag = true;
		                				}
		                			}
		                			if(expressionsValue.operator == "NOT IN" || expressionsValue.operator == "!=") {
		                				var attributeValues = expressionsValue.value.split(',');
		                				var qualifiedResult = $filter('filter')(attributeValues, selectedOption)[0];
		                				if(qualifiedResult != undefined) {
		                					$scope.disQualifiedFlag = true;
		                				}
		                			}

									if(expressionsValue.operator == ">") {
										if(parseInt(selectedOption) <= parseInt(expressionsValue.value) || isNaN(selectedOption) == true) {
											$scope.disQualifiedFlag = true;
										}
									}
									if(expressionsValue.operator == "<") {
										if(parseInt(selectedOption) >= parseInt(expressionsValue.value) || isNaN(selectedOption) == true) {
											$scope.disQualifiedFlag = true;
										}
									}
									if(expressionsValue.operator == ">=") {
										if(parseInt(selectedOption) < parseInt(expressionsValue.value) || isNaN(selectedOption) == true) {
											$scope.disQualifiedFlag = true;
										}
									}
									if(expressionsValue.operator == "<=") {
										if(parseInt(selectedOption) > parseInt(expressionsValue.value) || isNaN(selectedOption) == true) {
											$scope.disQualifiedFlag = true;
										}
									}
									if ($scope.disQualifiedFlag) {
										$scope.disqualifiedProspectModal();
										return false;
									} else {
										$timeout(function() {
											$scope.changedisposition(questionIndex+2);
					                    }, 1000);
										selectedQuestionsObj.qualifiedFlag = true;

									}
		                		} else {
		                			if (!attributeFound) {
		                				attributeFound = false;
		                			}
		                		}
	                	});
                	}
                	if (!attributeFound) {
                		$timeout(function() {
							$scope.changedisposition(questionIndex+2);
	                    }, 1000);
                		selectedQuestionsObj.qualifiedFlag = true;
                	}

                }

                $scope.setDepartmentTitle = function (attribute, selectedOption, questionObj, questionIndex) {
                		if(attribute.attributeType == XTAAS_CONSTANTS.attributeType.department) {
                			if (selectedOption == "" || selectedOption == undefined) {//if selected option is removed then set as original value
                    			$scope.prospectCall.prospect.department = $scope.prospect.originalDepartment;
                    		} else {
	                			angular.forEach(questionObj, function(deptValue, deptKey) {
	                					if(deptValue.value == selectedOption) {
	                						$scope.prospectCall.prospect.department = deptValue.label;
	                					}
	                			});
                    		}
                    	}
                    	if(attribute.attributeType == XTAAS_CONSTANTS.attributeType.role) {
                    		if (selectedOption == "" || selectedOption == undefined) {//if selected option is removed then set as original value
                    			$scope.prospectCall.prospect.title = $scope.prospect.originalTitle;
                    		} else {
                    			angular.forEach(questionObj, function(deptValue, deptKey) {
                					if(deptValue.value == selectedOption) {
                						$scope.prospectCall.prospect.title = deptValue.label;
                					}
                    			});
                    		}
                    	}

                }

                $scope.getQuestionType = function (question) {
                	var crmType;
                	angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                		if(question.crmName == questAttrValue.crmName) {
    						crmType = questAttrValue.crmDataType;
    					}
                	});
                	return crmType;
                	/*if(question.crmDataType == "double") {
                		return "double";
                	} else if ((question.pickList == null || question.pickList.length == 0) && question.crmDataType == "string") {
                        return "text";
                    } else if (question.pickList.length < 5) {
                        return "radio";
                    } else {
                        return "combo";
                    }*/
                }

                $scope.getProspectName = function () {
                    if ($scope.parent.state < 2) {
                        return "--";
                    } else {
                        var name = "";
                        if ($scope.prospectCall.prospect.prefix != null) {
                            name += (' ' + $scope.prospectCall.prospect.prefix);
                        }
                        if ($scope.prospectCall.prospect.firstName != null) {
                            name += (' ' + $scope.prospectCall.prospect.firstName);
                        }
                        if ($scope.prospectCall.prospect.lastName != null) {
                            name += (' ' + $scope.prospectCall.prospect.lastName);
                        }
                        if ($scope.prospectCall.prospect.suffix != null) {
                            name += (' ' + $scope.prospectCall.prospect.suffix);
                        }
                        return name.trim();
                    }
                }

                $scope.getProspectCity = function () {
                    if ($scope.parent.state < 2) {
                        return "--";
                    } else {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.city != null 
                            && $scope.prospectCall.prospect.city != undefined) {
                            return $scope.prospectCall.prospect.city;
                        }
                    }
                }

                $scope.getProspectCompany = function () {
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && $scope.prospectCall != null) {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.company != null 
                            && $scope.prospectCall.prospect.company != undefined) {
                            return $scope.prospectCall.prospect.company;
                        }
                    } else {
                        if ($scope.parent.state < 2) {
                            return "--";
                        } else {
                            if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                                && $scope.prospectCall.prospect != null 
                                && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.company != null 
                                && $scope.prospectCall.prospect.company != undefined) {
                                return $scope.prospectCall.prospect.company;
                            }
                        }
                    }
                }

                $scope.getProspectAddress = function () {
                    if ($scope.parent.state < 2) {
                        return "--";
                    } else {
                        var address = "";
                        if ($scope.prospectCall.prospect.addressLine1 != null) {
                            address += (' ' + $scope.prospectCall.prospect.addressLine1 + ',');
                        }
                        if ($scope.prospectCall.prospect.addressLine2 != null) {
                            address += (' ' + $scope.prospectCall.prospect.addressLine2 + ',');
                        }
                        if ($scope.prospectCall.prospect.city != null) {
                            address += (' ' + $scope.prospectCall.prospect.city + ',');
                        }
                        if ($scope.prospectCall.prospect.stateCode != null) {
                            address += (' ' + $scope.prospectCall.prospect.stateCode);
                        }
                        return address;
                    }
                }

                $scope.getDispositionStatus = function () {
                    if ($scope.parent.state < 2 || $scope.prospectCall.dispositionStatus == null) {
                        return "Disposition Status";
                    } else {
                        return $scope.prospectCall.dispositionStatus;
                    }
                }

                $scope.getProspectPhone = function () {
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && $scope.prospectCall != null) {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.phone != null 
                            && $scope.prospectCall.prospect.phone != undefined) {
                            return $scope.prospectCall.prospect.phone;
                        }
                    } else {
                        if ($scope.parent.state < 2) {
                            return "--";
                        } else {
                            if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                                && $scope.prospectCall.prospect != null 
                                && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.phone != null 
                                && $scope.prospectCall.prospect.phone != undefined) {
                                return $scope.prospectCall.prospect.phone;
                            }
                        }
                    }
                }

                $scope.getCompletePhone = function () {
                    if ($scope.parent.state < 2) {
                        return "--";
                    } else {
                        if ($scope.prospectCall.prospect.extension != null && $scope.prospectCall.prospect.extension != '') {
                            return $scope.prospectCall.prospect.phone;////$scope.prospectCall.prospect.phone + ' x ' + $scope.prospectCall.prospect.extension; + ' x ' + $scope.prospectCall.prospect.extension;
                        } else {
                            return $scope.prospectCall.prospect.phone;
                        }
                    }
                }

                /////////////////////////////////////added by pt////////////////////////////////////
                    $scope.getCompanyOrDomainName = function (itemObject) {
                            if (itemObject != null && itemObject != undefined && itemObject.customAttributes.domain != null && itemObject.customAttributes.domain != undefined) {
                                return itemObject.customAttributes.domain;
                            } else if (itemObject != null && itemObject != undefined && itemObject.company != null && itemObject.company != undefined) {
                                return itemObject.company;
                            }else {
                                return "";
                            }
                        }

                $scope.getProspectOutbondNumer = function () {
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && $scope.prospectCall != null) {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.outboundNumber != null 
                            && $scope.prospectCall.outboundNumber != undefined) {
                            return $scope.prospectCall.outboundNumber;
                        }
                    } else {
                        if ($scope.parent.state < 2) {
                            return "--";
                        } else {
                            if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                                && $scope.prospectCall.outboundNumber != null 
                                && $scope.prospectCall.outboundNumber != undefined) {
                                return $scope.prospectCall.outboundNumber;
                            }
                        }
                    }
                }

                $scope.getProspectCallCreated = function () {
                    if ($scope.parent.state < 2) {
                        return "";
                    } else {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.createdDate != null 
                            && $scope.prospectCall.createdDate != undefined) {
                            return $scope.prospectCall.createdDate;
                        }
                    }
                }

                $scope.getSource = function() {
                    if ($scope.dataProvider != null && $scope.dataProvider != undefined && 
                        $scope.dataProvider.includes($scope.prospectCall.prospect.source) &&
                        $scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.source != null 
                            && $scope.prospectCall.prospect.source != undefined) {
                        $scope.prospectCall.prospect.dataSource = "Xtaas Data Source";
                        return $scope.prospectCall.prospect.dataSource;
                    } else {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.source != null 
                            && $scope.prospectCall.prospect.source != undefined) {
                            return $scope.prospectCall.prospect.source;
                        }
                    }
                }

                $scope.getProspectSource = function () {
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true") {
                        if ($scope.parent.state < 2) {
                            return "";
                        } else {
                            $scope.getSource();
                        }
                    } else {
                        if ($scope.parent.state < 2) {
                            return "";
                        } else {
                            $scope.getSource();
                        }
                    }
                }

                $scope.getProspectBrand = function () {
                    if ($scope.parent.state < 2) {
                        return "";
                    } else {
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined 
                            && $scope.prospectCall.prospect != null 
                            && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.industry != null 
                            && $scope.prospectCall.prospect.industry != undefined) {
                            return $scope.prospectCall.prospect.industry;
                        }
                    }
                }

                $scope.getProspectDirection = function () {
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true") {
                        return 'Outbound';
                    } else {
                        if ($scope.parent.state < 2) {
                            return "";
                        } else {
                            return 'Outbound';
                        }
                    }
                }


                $scope.showFailureCodeModal = function () {
                    var failurecodemodal = $modal.open({
                        scope: $scope,
                        templateUrl: 'finishcodemodal.html',
                        controller: 'FailureCodeController',
                        windowClass: 'finishcodemodal',
                    });
                    failurecodemodal.result.then(function (selectedItem) {
                    }, function () {
                    	//Below condition will execute when modal getting close
                    	if (!$rootScope.failureSubStatusSelected) {//condition true, when failure sub-status not selected
                    										//condtion false, when failure substatus selected
                    		$scope.dialercodestatus = null;
                    		$scope.disposition = "";
                    		//if substatus not checked, uncheck the disposition status Or if substatus checked already for other disposition status also uncheck disposition status
                    		if ($scope.prospectCall.subStatus == undefined || $scope.prospectCall.subStatus == "" ||  $scope.prospectCall.subStatus == "CALLBACK_USER_GENERATED") {
                    			$scope.prospectCall.dispositionStatus = null;
                    			$scope.prospectCall.subStatus = null;
                    		}
                    	}
                    });
                }

                $scope.showNoConsentModal = function () {
                	$scope.showEmailmsg = false;
                	var noconsentmodal = $modal.open({
                        scope: $scope,
                        templateUrl: 'noconsent.html',
                        controller: 'FailureCodeController',
                        windowClass: 'finishcodemodal',
                    });
                }

                $scope.disqualifiedProspectModal = function () {
                	$scope.showEmailmsg = false;
                	var noconsentmodal = $modal.open({
                        scope: $scope,
                        templateUrl: 'disqualifiedprosp.html',
                        controller: 'disqualifiedProspectController',
                        backdrop: 'static'
                    });
                }
                ///////////////////////////////////////end by pt/////////////////////////////////////////////////////
                $scope.addTempNote = function (note) {
                	$scope.isAddNote = true;
                }

                $scope.addNote = function (note) {
                    if ($scope.parent.state >= 2) {
                        if (angular.isUndefined(note)) {
                            note = $scope.parent.note;
                            ///  delete $scope.parent.note;
                        }
                        if ($scope.prospectCall.notes == null) {
                           // $scope.prospectCall.notes = new Array();
                        	$scope.prospectCall.notes = [];
                        }
                        if ($scope.prospectCall.previousNotes == null) {
                            $scope.prospectCall.previousNotes = new Array();
                        }
                        var agentname = user.firstName+" "+user.lastName;
                        var noteObject = {
                            creationDate: new Date(),
                            text: note,
                            agentId: user.id,
                            agentName: agentname,
                            dispositionStatus: $scope.prospectCall.dispositionStatus,
                            subStatus: $scope.prospectCall.subStatus
                        }
                        $scope.prospectCall.notes.splice(0, 0, noteObject);
                        $scope.prospectCall.previousNotes.splice(0, 0, noteObject);
                    }
                }

                $scope.isStateOneAbove = function () {
                    return $scope.parent.state >= 1;
                }

                $scope.isStateTwoAbove = function () {
                    return $scope.parent.state >= 2;
                }

                $scope.isStateThreeAbove = function () {
                    return $scope.parent.state >= 3;
                }

                $scope.isStateThree = function () {
                    return $scope.parent.state == 3;
                }

                $scope.isStateFourAbove = function () {
                    return $scope.parent.state >= 4;
                }

                $scope.isNoteEnabled = function () {
                    return angular.isUndefined($scope.parent.note) || $scope.parent.note.trim() == '';
                }

                $scope.canSubmit = function () {////$scope.parent.state == 4 &&
                    ///$scope.prospectCall.dispositionStatus =  $scope.disposition; /// && $scope.isEditFormEnable == false
                    return $scope.parent.state == 4 &&  $scope.prospectCall != null && $scope.prospectCall.dispositionStatus != null;
                }

                $scope.isDialPad = false; // show dial pad by default
                $scope.toggleDialPadOnClick = function() {
                	$scope.isDialPad = !$scope.isDialPad;
                }

            	$scope.isClickPad = false;
                $scope.clickPad = function (digit) {
                    if (!$scope.isClickPad) {
                        $scope.parent.numberWithExtensionToDial = $scope.parent.numberToDial;
                        $scope.isClickPad = true;
                    }
                    //$scope.parent.numberWithExtensionToDial += digit;
                    /*callDTMFService.save({"callsid":$scope.prospectCall.twilioCallSid}, digit).$promise.then(function(data) {
                        $log.debug("DTMF sent successfully");
                    },function(error) {
                        NotificationService.log('error', 'An error occurred while sending digit request.');
                    });*/
                    if ($scope.getCallControlProvider() == 'Plivo') {
						$scope.plivoSendDtmf(digit);
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        $scope.telnyxSendDtmf(digit);
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                        $scope.signalwireSendDtmf(digit);
                    } else {
						twilioService.sendDigits(digit);
					}
                }

                $scope.playBeepSound = function() {
                    document.getElementById('play').play();
                }

                $scope.muteStatus = false;
                $scope.toggleMute = function () {
                    if ($scope.muteStatus) {
                        var timerStatus = 'mute-timer-stop';
                        var callState = 'UNMUTE';
                    } else {
                        var timerStatus = 'mute-timer-start';
                        var callState = 'MUTE';
                    }
                    $scope.previousCallStateTime = $scope.currentCallStateTime;
                    $scope.currentCallStateTime = new Date().getTime();
                    $log.debug("Changing the state to " + callState + " state");
                    callStatusService.save({}, {
                        callState: callState,
                        callStartTime: $scope.prospectCall.callStartTime,
                        callStateTransitionDuration: Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime) / 1000)
                    }).$promise.then(function (data) {
                        $rootScope.$broadcast(timerStatus);
                        if ($scope.getCallControlProvider() == 'Plivo') {
                        	$scope.plivoCallMuteUnmute();
                        } else if ($scope.getCallControlProvider() == 'Telnyx') {
                            $scope.telnyxCallMuteUnmute();
                        } else if ($scope.getCallControlProvider() == 'Signalwire') {
                            $scope.signalwireCallMuteUnmute();
                        } else {
							twilioService.toggleMute();
						}
                        $scope.muteStatus = !$scope.muteStatus;
                    }, function (error) {
                        // NotificationService.log('error', 'Some error occurred while placing call on Hold. Please try again.');
                    });
                }

                $scope.callOnHoldShow = true;

                $scope.callOnHold = function () {
                    console.log($scope.prospectCall.twilioCallSid);
                    if ($scope.parent.state === 3) {
                        $scope.previousCallStateTime = $scope.currentCallStateTime;
                        $scope.currentCallStateTime = new Date().getTime();
                        $log.debug("Changing the state to hold state");
                        var isConferenceCall = false;
                        if (($scope.agentType == 'AUTO' && $scope.campaign.dialerMode == 'HYBRID')
                        		|| $scope.campaign.dialerMode == 'POWER') {
                        	isConferenceCall = true;
                        }
                        var usePlivo = false;
                        if ($scope.getCallControlProvider() == 'Plivo') {
                            usePlivo = true;
                        }

                        callHoldService.save({
                        	callsid: $scope.prospectCall.twilioCallSid,
                        	usePlivo: usePlivo,
                        	isConferenceCall: isConferenceCall
                        }, {
                            callState: 'HOLD',
                            callStartTime: $scope.prospectCall.callStartTime,
                            callStateTransitionDuration: Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime) / 1000)
                        }).$promise.then(function (data) {
                            $scope.callOnHoldShow = false;
                            $rootScope.$broadcast('hold-timer-start');
                        }, function (error) {
                            NotificationService.log('error', 'Participant not found. Please disconnect the call.');
                        });
                    }
                }

                $scope.callOnUNHOLD = function () {
                    if ($scope.parent.state === 3) {
                        $scope.previousCallStateTime = $scope.currentCallStateTime;
                        $scope.currentCallStateTime = new Date().getTime();
                        $log.debug("Changing the state to unhold state");
                        var isConferenceCall = false;
                        if (($scope.agentType == 'AUTO' && $scope.campaign.dialerMode == 'HYBRID')
                        		|| $scope.campaign.dialerMode == 'POWER') {
                        	isConferenceCall = true;
						}
                        var usePlivo = false;
                        if ($scope.getCallControlProvider() == 'Plivo') {
                            usePlivo = true;
                        }
                        callUnholdService.save({
                        	callsid: $scope.prospectCall.twilioCallSid,
                        	usePlivo: usePlivo,
                        	isConferenceCall: isConferenceCall
                        }, {
                            callState: 'UNHOLD',
                            callStartTime: $scope.prospectCall.callStartTime,
                            callStateTransitionDuration: Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime) / 1000)
                        }).$promise.then(function (data) {
                            $scope.callOnHoldShow = true;
                            $rootScope.$broadcast('hold-timer-stop');
                        }, function (error) {
                            NotificationService.log('error', 'Participant not found. Please disconnect the call.');
                        });
                    }
                }

                $scope.isPhoneNumberConnected = false;

                $scope.connectCall = function () {
                    if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall != '' && $scope.prospectCall.prospect != undefined && $scope.prospectCall.prospect.phone != undefined && $scope.prospectCall.prospect.phone != "") {
                        if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true") {
                            $scope.disconnectButtonMethod = $timeout($scope.disconnectButtonTimeout, $scope.disconnectButtonManualTimer);
                        }
                        $scope.resetNotices();
                        $rootScope.$broadcast('wrapup-timer-stop');
                        if ($scope.campaign.dialerMode == "PREVIEW" || ($sessionStorage.callbackProspect != null || $sessionStorage.callbackProspect != undefined)) {
                            $scope.checkIfAgentDialedToProspect = "No";
                            // $scope.parent.state = 3;
                            if ($scope.campaign.simpleDisposition) {
                                $scope.showSimpleDisposition = "simpleDisposition";
                            } else {
                                $scope.showSimpleDisposition = "nonSimpleDisposition";   
                            }
                        }
                        if ($scope.parent.state >= 2 && !$scope.isAutoDialingEnabled()) {
                            $scope.initiateManualCall();
                        } else {
                            $scope.initiateManualCall();
                        }
                    } else {
                        NotificationService.log('error', 'Please enter prospect details to call.');
                    }
                }

                $scope.callRetryCount = 0;
                $scope.initiateManualCall = function() {
                    if ($scope.agentIdleTimeInterval != null && $scope.agentIdleTimeInterval != undefined) {
                        $interval.cancel($scope.agentIdleTimeInterval);	
                    }
                	$scope.isPhoneNumberConnected = true;
                	$log.debug("Connecting a call to phone number: " + $scope.parent.numberToDial);
                	var agentType = "";
                	var firstName = "";
                	var lastName = "";
                	var domain = "";
                	if ($scope.agentType != undefined && $scope.agentType != null) {
                		agentType = $scope.agentType;
                	}
                	if ($scope.prospectCall.outboundNumber == undefined || $scope.prospectCall.outboundNumber == null) {
                		console.log("***** outbound Number is NULL ***** ");
                		$scope.prospectCall.outboundNumber = '14086426850';
					}
                	if ($scope.prospectCall.prospectCallId == undefined || $scope.prospectCall.prospectCallId == null) {
                		console.log("***** prospectCallId is NULL ***** ");
					}
                	if ($scope.prospectCall.prospect.firstName) {
                		firstName = $scope.prospectCall.prospect.firstName;
					}
                	if ($scope.prospectCall.prospect.lastName) {
                		lastName = $scope.prospectCall.prospect.lastName;
					}
                	if ($scope.prospectCall.prospect.customAttributes) {
                		domain = $scope.prospectCall.prospect.customAttributes.domain;
                    }
                	if ($scope.getCallControlProvider() == 'Plivo') {
                        $scope.fetchOutboundNumberAndDial(firstName, lastName,domain,agentType);
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        $scope.fetchTelnyxOutboundNumberAndDial(firstName, lastName,domain,agentType);
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                        $scope.fetchSignalwireOutboundNumberAndDial(firstName, lastName, domain, agentType);
                    } else {
                    	twilioTokenService.getToken().then(function(token) {
                            twilioService.connect(token, {
//                            	"pn": $scope.parent.numberToDial,
//                            	"pcid": $scope.prospectCall.prospectCallId,
//                            	"callerId": $scope.prospectCall.outboundNumber,
//                            	"dialerMode": $scope.campaign.dialerMode,
//                            	"campaignId": $scope.campaign.id,
//                            	"agentId": user.id,
//                            	"agentType": agentType,
//                            	"To": $scope.parent.numberToDial,
//                            	"fname": firstName,
//                				"lname": lastName,
//                				"domain": domain
                            }).then(function(parameters) {
                                $scope.updateCallStatusManualDial(parameters.CallSid);
                            }, function(error) {
                                if ($scope.loggedInUserOrganization.enableRedial) {
                                    $scope.isPhoneNumberConnected = false;
                                }
                                $log.debug("Error occured while making a call");
                                $log.debug(error);
                            });
                        });
                    }
                }

                $scope.fetchOutboundNumberAndDial = function(firstName, lastName, domain, agentType) {
                    if ($scope.prospect == null || $scope.prospect == undefined || $scope.prospect.originalHiddenPhone == null || $scope.prospect.originalHiddenPhone == undefined) {
                        $scope.prospect.originalHiddenPhone = $rootScope.originalHiddenPhone;
                    }
                    $scope.callRetryCount++;
                    var redialNumberDTO = {
                        phone: $scope.prospect.originalHiddenPhone,
                        callRetryCount: $scope.callRetryCount,
                        country: $scope.prospectCall.prospect.country,
                        organizationId: $scope.campaign.organizationId
                    };
                    researchAnDialService.getOutboundNumberForRedial(redialNumberDTO).$promise.then(function(data) {
                        if (data.redialoutboundnumber == "" || data.redialoutboundnumber == null || data.redialoutboundnumber == undefined) {
                             data.redialoutboundnumber = $scope.prospectCall.outboundNumber;
                        }
                        $scope.prospectCall.outboundNumber = data.redialoutboundnumber;
                        $sessionStorage.plivoOutboundNumber = data.redialoutboundnumber;
                        if ($sessionStorage.callbackProspect != null && $sessionStorage.callbackProspect != undefined &&
                            (($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'AUTO') || $scope.campaign.dialerMode == 'POWER')) {
                                agentType = "MANUAL";
                        }
                        var toNumber = $scope.prospect.originalHiddenPhone;
                        var extraHeaders = {
                             'X-PH-pn' : $scope.prospect.originalHiddenPhone,
                             "X-PH-pcid": $scope.prospectCall.prospectCallId,
                             'X-PH-agentId': user.id,
                             'X-PH-agentType': agentType,
                             'X-PH-campaignId': $scope.campaign.id,
                             'X-PH-dialerMode': $scope.campaign.dialerMode,
                             'X-PH-callerId': data.redialoutboundnumber,
                             'X-PH-fname': firstName,
                             'X-PH-lname': lastName,
                             'X-PH-domain': domain
                         };
                     $scope.setPlivoToNumberAndHeaders(toNumber, extraHeaders);
                     $scope.plivoManualCall(toNumber, extraHeaders);                
                     },function(error) {
                         NotificationService.log('error', 'An error occured while fetching outboundnumber.');          
                     });
                }

                $scope.fetchTelnyxOutboundNumberAndDial = function(firstName, lastName, domain, agentType) {
                    $scope.callRetryCount++;
                    var redialNumberDTO = {
                        phone: $scope.prospect.originalHiddenPhone,
                        callRetryCount: $scope.callRetryCount,
                        country: $scope.prospectCall.prospect.country,
                        organizationId: $scope.campaign.organizationId
                    };
                    TelnyxService.getOutboundNumberForRedial(redialNumberDTO).$promise.then(function(data) {
                        if (data.redialoutboundnumber == "" || data.redialoutboundnumber == null || data.redialoutboundnumber == undefined) {
                             data.redialoutboundnumber = $scope.prospectCall.outboundNumber;
                        }
                        $scope.prospectCall.outboundNumber = data.redialoutboundnumber;
                        $sessionStorage.telnyxOutboundNumber = data.redialoutboundnumber;
                        var toNumber = $scope.prospect.originalHiddenPhone;
                        var agentCallInfo = {
                            from: $scope.telnyxConnectionUsername,
                            to: toNumber,
                            campaignId: $scope.campaign.id,
                            dialerMode: $scope.campaign.dialerMode,
                            agentType: agentType,
                            pcid: $scope.prospectCall.prospectCallId,
                            organizationId : $scope.campaign.organizationId,
                            agentId: $scope.parent.userId,
                            outboundNumber: data.redialoutboundnumber
                          };
                        $scope.telnyxCreateCallForManual(agentCallInfo);       
                     },function(error) {
                         NotificationService.log('error', 'An error occured while fetching outboundnumber.');          
                     });
                }

                $scope.fetchSignalwireOutboundNumberAndDial = function(firstName, lastName, domain, agentType) {
                    $scope.callRetryCount++;
                    var redialNumberDTO = {
                        phone: $scope.prospect.originalHiddenPhone,
                        callRetryCount: $scope.callRetryCount,
                        country: $scope.prospectCall.prospect.country,
                        organizationId: $scope.campaign.organizationId
                    };
                    SignalwireService.getOutboundNumberForRedial(redialNumberDTO).$promise.then(function(data) {
                        if (data.redialoutboundnumber == "" || data.redialoutboundnumber == null || data.redialoutboundnumber == undefined) {
                             data.redialoutboundnumber = $scope.prospectCall.outboundNumber;
                        }
                        $scope.prospectCall.outboundNumber = data.redialoutboundnumber;
                        $sessionStorage.signalwireOutboundNumber = data.redialoutboundnumber;
                        
                        var toNumber = $scope.prospect.originalHiddenPhone;
                        var extraHeaders = {
                            //  'X-PH-pn' : $scope.prospect.originalHiddenPhone,
                             "pcid": $scope.prospectCall.prospectCallId,
                             'agentId': user.id,
                             'agentType': agentType,
                             'campaignId': $scope.campaign.id,
                             'dialerMode': $scope.campaign.dialerMode,
                             'callerId': data.redialoutboundnumber,
                             'fname': firstName,
                             'lname': lastName,
                             'domain': domain
                         };
                     $scope.setSignalwireToNumberAndHeaders(toNumber, extraHeaders);
                     $scope.signalwireManualCall(toNumber, extraHeaders, user.id);                
                     },function(error) {
                         NotificationService.log('error', 'An error occured while fetching Signalwire outboundnumber.');          
                     });
                }

                $scope.updateCallStatusManualDial = function(callSid) {
                	$log.debug("Successfully connected with call sid:" + callSid);
                    $scope.prospectCall.twilioCallSid = callSid;
                    $rootScope.$broadcast('call-timer-start');
                    $scope.prospectCall.callStartTime = new Date();
                    $scope.previousCallStateTime = $scope.currentCallStateTime;
                    $scope.currentCallStateTime = new Date().getTime();
                    $log.debug("Changing the state to call state");
                    var outboundNumber = "";
                    if ($scope.getCallControlProvider() == 'Plivo') {
                        outboundNumber = $sessionStorage.plivoOutboundNumber;
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        outboundNumber = $sessionStorage.telnyxOutboundNumber;
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                        outboundNumber = $sessionStorage.signalwireOutboundNumber;
                    }
                    callStatusService.save({}, {
                        callState: XTAAS_CONSTANTS.callStatus.dialed,
                        callStartTime: $scope.prospectCall.callStartTime,
                        callStateTransitionDuration : Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime)/ 1000),
                        twilioCallSid : $scope.prospectCall.twilioCallSid,
                        outboundNumber : outboundNumber
                    });
                    $log.debug("Prev state " + $scope.parent.state);
                    console.log($scope.$id);
                    // $scope.parent.state = 3;
                    if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined)) {
                            $scope.parent.state = 1;
                    } else {
                            $scope.parent.state = 3;
                    }
                    $log.debug("New state " + $scope.parent.state);
                }

                $scope.invalidCall = function(message,callInfo){
                	$log.debug("Broadcasting the timer to stop");
                    $rootScope.$broadcast('call-timer-stop');
                    $rootScope.$broadcast('mute-timer-stop');
                    $rootScope.$broadcast('hold-timer-stop');
                    $rootScope.$broadcast('wrapup-timer-start');
                    $scope.isPhoneNumberConnected = false;
                    if ($scope.currentProspectType == 'RESEARCH' || $scope.currentProspectType == 'PREVIEW') {
                        $scope.disableDialButtonInPreviewMode = true;
                    }
                    $log.debug("Changing the state to wrap up state");
                    callStatusService.save({}, {
                        callState: XTAAS_CONSTANTS.callStatus.disconnected,
                    });
                    $scope.previousProspectTime = $scope.currentProspectTime;
                    $log.debug("Prev state " + $scope.parent.state);
                    console.log($scope.$id);
                    $scope.parent.state = 4;
                    $log.debug("New state " + $scope.parent.state);
                    // cleanup data and flags
                    if ($scope.isGlobalForceSubmit) {
						$scope.submitProspect();
					}
                    $scope.showCallDisconnectMessage(message);
                	console.info('PLIVO - onCallFailed : ', callInfo);

                }

                // $scope.startTimerAfterCallDisconnect = function() {
                //     $rootScope.$broadcast('call-timer-stop');
                //     $rootScope.$broadcast('mute-timer-stop');
                //     $rootScope.$broadcast('hold-timer-stop');
                //     $rootScope.$broadcast('wrapup-timer-start');
                //     $scope.parent.state = 4;
                // }

                $scope.endCall = function () {
                    removeBlinkerFromDialButton();
                    $sessionStorage.plivoOutboundNumber = "";
                    $sessionStorage.telnyxOutboundNumber = "";
                    $sessionStorage.signalwireOutboundNumber = "";
                	if ($scope.parent.state == 3) {
                		$scope.isClickPad = false;
                        if ($scope.isAutoDialingEnabled() && ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined)) {
                            $log.debug("Calling endcall for autodialed call");
                            // set prospect disconnected time for agent call monitoring
                            $scope.prospectDisconnectedTime = new Date();
                            $scope.callOnHoldShow = true;
                            var isConferenceCall = false;
                            if (($scope.agentType == 'AUTO' && $scope.campaign.dialerMode == 'HYBRID')
                            		|| $scope.campaign.dialerMode == 'POWER') {
                            	isConferenceCall = true;
    						}
                            var callControlProvider = "NoProvider";
                            if ($scope.getCallControlProvider() == 'Plivo') {
                                callControlProvider = 'Plivo';
                                // $scope.plivoHangupCall();
                            } else if ($scope.getCallControlProvider() == 'Telnyx') {
                                $scope.telnyxHangupCall();
                            } else if ($scope.getCallControlProvider() == 'Signalwire') {
                                // $scope.signalwireClient.hangup();
                                callControlProvider = 'Signalwire';
                            }

                            if ($scope.campaign.isNotConference) {
                                isConferenceCall = false;
                            }
                            conferenceService.delete({
                            	callsid: $scope.prospectCall.twilioCallSid,
                            	callControlProvider: callControlProvider,
                            	isConferenceCall: isConferenceCall
                            }).$promise.then(function() {
                                $log.debug("Broadcasting the timer to stop");
                                // if ($scope.getCallControlProvider() == 'Telnyx' && callHangupFrom != null && callHangupFrom != undefined && callHangupFrom == "fromTelnyxServer") {
                                //     $log.debug("Call got hang up from telnyx end.");
                                //     $scope.startTimerAfterCallDisconnect();
                                // }
                                // if ($scope.getCallControlProvider() == 'Plivo')  {
                                //     $scope.startTimerAfterCallDisconnect();
                                // }
                                $rootScope.$broadcast('call-timer-stop');
                                $rootScope.$broadcast('mute-timer-stop');
                                $rootScope.$broadcast('hold-timer-stop');
                                if (!$scope.isAutoAMD) {
                                    $rootScope.$broadcast('wrapup-timer-start');
                                    $scope.timeOutMethod = $timeout($scope.submitProspectTimeout, 15000);
                                }                                
                                $scope.isPhoneNumberConnected = false;
                                if ($scope.currentProspectType == 'RESEARCH' || $scope.currentProspectType == 'PREVIEW') {
                                    $scope.disableDialButtonInPreviewMode = true;
                                }
                                $log.debug("Changing the state to wrap up state");
                                if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall.callStartTime != null && $scope.prospectCall.callStartTime != undefined) {
                                    var startTime = $scope.prospectCall.callStartTime.getTime();
                                    var endTime = (new Date()).getTime();
                                    $scope.previousCallStateTime = $scope.currentCallStateTime;
                                    $scope.currentCallStateTime = new Date().getTime();
                                    $scope.prospectCall.callDuration = Math.round((endTime - startTime) / 1000);
                                    $log.debug("Notifying the backend system about the call status");
                                    callStatusService.save({}, {
                                        callState: XTAAS_CONSTANTS.callStatus.disconnected,
                                        callStateTransitionDuration : Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime)/ 1000)
                                    });
                                    $scope.previousProspectTime = $scope.currentProspectTime;
                                }
                                $log.debug("Prev state " + $scope.parent.state);
                                console.log($scope.$id);
                                $scope.parent.state = 4;
                                $log.debug("New state " + $scope.parent.state);
                                if ($scope.isAutoAMD) {
                                    $scope.submitProspect("AUTO_AMD");
                                } else if ($scope.isGlobalForceSubmit) {
                                    // cleanup data and flags
									$scope.submitProspect();
								}
                                $scope.isAutoAMD = false;
                            }, function (error) {
                                if ($scope.loggedInUserOrganization.enableRedial) {
                                    $scope.isPhoneNumberConnected = false;
                                }
                            });
                        } else {
                        	// set prospect disconnected time for agent call monitoring
                        	$scope.prospectDisconnectedTime = new Date();
                            $log.debug("Calling endcall for manual call");
                            if ($scope.getCallControlProvider() == 'Plivo') {
                            	$scope.plivoHangupCall();
                            } else if ($scope.getCallControlProvider() == 'Telnyx') {
                                $scope.telnyxHangupCall();
                                $scope.telnyxCall = null;
                            } else if ($scope.getCallControlProvider() == 'Signalwire') {
                                $scope.signalwireClient.hangup();
                            } else {
								twilioService.disconnect();
							}
                            $log.debug("Broadcasting the timer to stop");
                            //$scope.$broadcast('timer-stop');
                            $rootScope.$broadcast('call-timer-stop');
                            $rootScope.$broadcast('mute-timer-stop');
                            $rootScope.$broadcast('wrapup-timer-start');
                            if ($scope.campaign.dialerMode == "PREVIEW") {
                                $scope.timeOutMethod = $timeout($scope.submitProspectTimeout, 15000);
                            }    
                            $rootScope.$broadcast('hold-timer-stop');
                            var startTime = $scope.prospectCall.callStartTime.getTime();
                            var endTime = (new Date()).getTime();
                            $scope.previousCallStateTime = $scope.currentCallStateTime;
                            $scope.currentCallStateTime = new Date().getTime();
                            if ($scope.loggedInUserOrganization.enableRedial) {
                                $scope.isPhoneNumberConnected = false;
                            }
                            if ($scope.currentProspectType == 'RESERACH' || $scope.currentProspectType == 'PREVIEW') {
                                $scope.disableDialButtonInPreviewMode = true;
                            }
                            $log.debug("Changing the state to wrap up state");
                            $scope.prospectCall.callDuration = Math.round((endTime - startTime) / 1000);
                            $log.debug("Notifying the backend system about the call status");
                            callStatusService.save({}, {
                                callState: XTAAS_CONSTANTS.callStatus.disconnected,
                                callStateTransitionDuration : Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime)/ 1000)
                            });
                            $log.debug("Prev state " + $scope.parent.state);
                            console.log($scope.$id);
                            $scope.parent.state = 4;
                            $log.debug("New state " + $scope.parent.state);
                        }
                    }
                }

                $scope.submitProspectTimeout = function () {
                        if (($scope.agentType == 'AUTO' && $scope.campaign.dialerMode == 'HYBRID')
                                || $scope.campaign.dialerMode == 'POWER' || $scope.campaign.dialerMode == 'PREVIEW') {
                        if (!$scope.isMobileTablet() && $scope.campaign.simpleDisposition && $scope.parent.state == 4 && !$scope.isAutoDisposeModalOpen) {
                                $scope.showAutoDisposeAlertModal();
                        }
                    }
                }

                $scope.inviteSupervisor = function () {
                    supervisorInvitationService.save().$promise.then(function () {
                        NotificationService.log('success', 'Supervisor invitation send successfully');
                    });
                }

                /*var setCampaign = function(campaign) {
                 if (campaign == null) {
                 $log.debug("Changing the state to idle state");
                 $scope.variable.campaign = campaignVariable;
                 $log.debug("Prev state " + $scope.parent.state);
                 console.log($scope.$id);
                 $scope.parent.state = 0;
                 $log.debug("New state " + $scope.parent.state);
                 } else {
                 $log.debug("Compiling campaign guides");
                 campaign.callGuide. = _.template(campaign.callGuide.confirmSection.script);
                 campaign.callGuide.questionSectionFn = _.template(campaign.callGuide.questionSection.script);
                 campaign.callGuide.consentSectionFn = _.template(campaign.callGuide.consentSection.script);
                 campaign.callGuide.noteSectionFn = _.template(campaign.callGuide.noteSection.script);

                 $log.debug("Changing the state to campaign state");
                 var currentAsset;
                 for (var index = 0; index < campaign.assets.length; index++) {
                 if (campaign.assets[index].assetId == campaign.currentAssetId) {
                 currentAsset =  campaign.assets[index];
                 break;
                 }
                 }
                 $scope.variable.campaign = {
                 name: campaign.name,
                 currentAssetName:  angular.isDefined(currentAsset) ? currentAsset.name : undefined
                 }
                 $log.debug("Prev state " + $scope.parent.state);
                 console.log($scope.$id);
                 $scope.parent.state = 1;
                 $log.debug("New state " + $scope.parent.state);
                 }
                 $scope.campaign = campaign;

                 $scope.isCurrentQuestion = 0;

                 twilioService.disconnect();
                 $rootScope.$broadcast("whisper-stop");
                 $rootScope.$broadcast("onCampaignChange", campaign);
                 }*/

                /*var setNextCampaign = function(campaign) {
                 if (campaign == null) {
                 changeStatus("IDLE");
                 } else {
                 changeStatus("ONLINE", campaign.id);//changeStatus("GEARING", campaign.id);
                 }
                 $log.debug("Setting the next campaign: ");
                 $log.debug(campaign);
                 nextCampaign = campaign;
                 }*/

                $scope.showCampaignChangeModal = function() {
                	$scope.campaignChangeModalInstance = $modal.open({
						scope: $scope,
						templateUrl: 'campaignChangeConfirmModal.html',
						controller: CampaignChangeModalInstanceCtrl,
						backdrop: 'static',
						resolve: {
							campaign: function () {
					          return $scope.agentChangeCampaign;
					        }
						}
					});
                }

                $scope.campaignChange = function() {
                	$scope.showCampaignChangeModal();
       			 	$rootScope.$broadcast("onChangeStatus", 'REQUEST_OFFLINE');
                }
                $scope.isCampaignChange = false;
                Pusher.subscribe(user.id, 'campaign_change', function (campaign) {
                	$log.debug("Received a new campaign");
                	$log.debug(campaign);
                	$scope.agentChangeCampaign = campaign;
                	if ($scope.prospectCall != undefined) {//check if agent handling the prospect while campaign change event occur
                										//if agent handling another prospect then agent should handle prospect first
                										//then showing the popup to switch to new campaign
                		$scope.isCampaignChange = true;//set flag= true if campaign is change for agent
                	} else {
                		if ($scope.campaignChangeModalInstance == undefined) {//check if already modal has open
                			 $scope.campaignChange();
                		}
                	}
                	/*if (campaign == null) {
                		if ($scope.campaign != null) {
                			if ($scope.parent.state <= 1) { // Either idle or campaign allocated. In any other state just set the nextState
                				$log.debug("Changing the state to idle state");
                				setCampaign(campaign);
                			}
                			setNextCampaign(campaign);
                		}
                	} else if ($scope.campaign == null || campaign.id != $scope.campaign.id) {
                		if ($scope.parent.state <= 1) { // Either idle or campaign allocated. In any other state just set the nextState
                			$log.debug("Changing the state to campaign state");
                			setCampaign(campaign);
                		}
                		setNextCampaign(campaign);
                	}*/
                });

                $scope.showStateChangeModal = function() {
                    $scope.stateChangeModalInstance = $modal.open({
                    scope: $scope,
                    templateUrl: 'stateChangeConfirmModal.html',
                    controller: StateChangeModalInstanceCtrl,
                    backdrop: 'static'
                        });
                }

                $scope.isMachineDetectModalOpen = false;
                $scope.showMachineDetectedModal = function() {
                    $scope.amdOptionSelected = false;
                    $scope.machineDetectedModalInstance = $modal.open({
                        scope: $scope,
                        templateUrl: 'machineDetectedModal.html',
                        controller: MachineDetectedModalInstanceCtrl,
                        windowClass: 'autodisposemodal',
                    });

                    $scope.machineDetectedModalInstance.opened.then(function () {
                        $scope.isMachineDetectModalOpen = true;
                    });
                    
                    $scope.machineDetectedModalInstance.result.then(function () {
                        $scope.isMachineDetectModalOpen = false;
                    }, function () {
                        $scope.isMachineDetectModalOpen = false;
                    });
                }

                var MachineDetectedModalInstanceCtrl = function ($scope, $modalInstance) {
                    $scope.totalSec = 15;
                    var countDownTime = function() {
                        $scope.totalSec--;
                        if ($scope.totalSec == 0) {
                            $modalInstance.dismiss('cancel');
                            if (!$scope.amdOptionSelected) {
                                $scope.$parent.isAutoAMD = true;
                                $scope.endCall();
                            }
                        }
                    };

                    $scope.$parent.machineDetectionTimeInterval =   $interval(function() {
                        countDownTime();
                    }, 1000);

                    $scope.selectOption = function (selectedOption) {
                        $scope.amdOptionSelected = true;
                        if (selectedOption == 'DISCONNECT') {
                            $scope.$parent.isAutoAMD = true;
                            $scope.endCall();
                        } else if (selectedOption == 'CONTINUE') {
                            $modalInstance.dismiss('cancel');    
                        }
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.showAutoDisposeAlertModal = function() {
                    $scope.autoDisposeAlertModalInstance = $modal.open({
                        scope: $scope,
                        templateUrl: 'autoDisposeAlertConfirmModal.html',
                        controller: AutoDisposeAlertModalInstanceCtrl,
                        windowClass: 'autodisposemodal',
                        resolve: {
							callLegId: function () {
					          return $scope.telnyxCallLegId;
                            },
                            sessionStorage: function () {
                                return $sessionStorage;
                            }
						}
                    });

                    $scope.autoDisposeAlertModalInstance.opened.then(function () {
                        $scope.isAutoDisposeModalOpen = true;
                    });
                    
                    $scope.autoDisposeAlertModalInstance.result.then(function () {
                        $scope.isAutoDisposeModalOpen = false;
                    }, function () {
                        $scope.isAutoDisposeModalOpen = false;
                    });
                }

                var AutoDisposeAlertModalInstanceCtrl = function ($scope, $modalInstance,callLegId, sessionStorage) {
                    $scope.totalSec = 15;
                    var countDownTime = function() {
                        $scope.totalSec--;
                        if ($scope.totalSec == 0) {
                            $modalInstance.dismiss('cancel');
                            if (sessionStorage.callLegId == callLegId) {
                                $scope.endCall();
                            }
                            $scope.submitProspect("AUTO");
                        }
                    };

                    $scope.$parent.autoDisposeTimeInterval =   $interval(function() {
                        countDownTime();
                    }, 1000);

                    $scope.confirmDisposition = function (status) {
                        if ($scope.autoAlertDisposition == "SUCCESS") {
                            // $scope.setDisposition('SUCCESS');
                            $scope.prospectCall.dispositionStatus = "SUCCESS";
                        } else if ($scope.autoAlertDisposition == "FAILURE") {
                            $scope.setDisposition('FAILURE');
                            $scope.prospectCall.dispositionStatus = "FAILURE";
                        } else if ($scope.autoAlertDisposition == "DNCL") {
                            $scope.setDisposition('DNCL');
                            $scope.prospectCall.dispositionStatus = "DNCL";
                        } else if ($scope.autoAlertDisposition == "CALLBACK") {
                            $scope.setDisposition('CALLBACK');
                        } else if ($scope.autoAlertDisposition == "CALLBACK_USER_GENERATED") {
                            $scope.setCallbackStatus('CALLBACK_USER_GENERATED');
                            $scope.$parent.callbackcode = "CALLBACK_USER_GENERATED";
                        } else if ($scope.autoAlertDisposition == "ANSWERMACHINE") {
                            $scope.setDialerCodeStatus('ANSWERMACHINE');
                            $scope.dialercodestatus = "dialercodestatus";
                        } else if ($scope.autoAlertDisposition == "INVALID_DATA") {
                            $scope.setDisposition('INVALID_DATA');
                            $scope.prospectCall.dispositionStatus = "INVALID_DATA";
                        } else if ($scope.autoAlertDisposition == "CALL_LATER") {
                            $scope.setDialerCodeStatus('ANSWERMACHINE');
                            $scope.dialercodestatus = "dialercodestatus";
                        }
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.showScriptChangeModal = function() {
                    $scope.scriptChangeModalInstance = $modal.open({
                    scope: $scope,
                    templateUrl: 'scriptChangeConfirmModal.html',
                    controller: ScriptChangeModalInstanceCtrl,
                    backdrop: 'static'
                        });
                }

                var StateChangeModalInstanceCtrl = function ($scope, $modalInstance) {
                    $scope.confirmStateChange = function (status) {
                    $modalInstance.dismiss('cancel');
                    }
                }

                var ScriptChangeModalInstanceCtrl = function ($scope, $modalInstance) {
                    $scope.confirmScriptChange = function (status) {
                    $modalInstance.dismiss('cancel');
                    $scope.$parent.loading = true;
                    $state.go('agentsplash');
                    $state.go('agentsplash');
                    }
                }

                $scope.scriptChange = function() {
                    $scope.showScriptChangeModal();
                    $rootScope.$broadcast("onChangeStatus", 'REQUEST_OFFLINE');
                }


                $scope.isScriptChange = false;
                Pusher.subscribe(user.id, 'script_change', function (message) {
                    $log.debug("Received a new script");
                    $log.debug(message);
                    // $scope.agentChangeCampaign = message;
                    if ($scope.prospectCall != undefined) {
                        $scope.isScriptChange = true;
                    } else {
                        if ($scope.scriptChangeModalInstance == undefined) {//check if already modal has open
                            $scope.scriptChange();
                        }
                    }
                });

                $scope.showAgentTypeChangeModal = function() {
                	$scope.agentTypeChangeModalInstance = $modal.open({
						scope: $scope,
						templateUrl: 'agentTypeChangeConfirmModal.html',
						controller: AgentTypeChangeModalInstanceCtrl,
						backdrop: 'static',
						resolve: {
							agentType: function () {
					          return $scope.agentType;
					        }
						}
					});
                }

                var AgentTypeChangeModalInstanceCtrl = function ($scope, $modalInstance, agentType) {
                	$scope.confirmAgentTypeChange = function (status) {
                        $modalInstance.dismiss('cancel');
						$scope.$parent.loading = true;
						$state.go("agentsplash");
						$state.go("agentsplash");
                	}
                }

                $scope.agentTypeChange = function() {
                	$scope.showAgentTypeChangeModal();
       			 	$rootScope.$broadcast("onChangeStatus", 'REQUEST_OFFLINE');
                }

                $scope.isAgentTypeChange = false;
                Pusher.subscribe(user.id, 'agent_type_change', function (agentType) {
                    $log.debug("Received a agent type change");
                    $scope.agentType = agentType;
                	if ($scope.prospectCall != undefined) {
                		$scope.isAgentTypeChange = true;
                	} else {
                		if ($scope.agentTypeChangeModalInstance == undefined) {//check if already modal has open
                			 $scope.agentTypeChange();
                		}
                	}
                });

                var CampaignChangeModalInstanceCtrl = function ($scope, $modalInstance, campaign) {
                	$scope.campaign = campaign;
                	$scope.confirmCampaignChange = function (status) {
                        $modalInstance.dismiss('cancel');
						$scope.$parent.loading = true;
						$state.go("agentsplash");
						$state.go("agentsplash");
                	}
                }

                $scope.resetNotices = function() {
                	 $scope.notices = [];
                }

                Pusher.subscribe(user.id, 'status_approve_notice', function (noticeInfo) {
                	// $log.debug("Received a new status approve notice");
                	// console.log(noticeInfo);
                	$scope.resetNotices();
                    $scope.notices.push(noticeInfo.notice + ".");
                    $timeout(function() {
                    	$scope.resetNotices();
                    }, 6000);
                    $rootScope.$broadcast('onRequestStatusApproved', noticeInfo.new_status);
                    if (noticeInfo.new_status == 'On-Break') {
                    	var status = "ONBREAK";
                    } else if (noticeInfo.new_status == 'In-Meeting') {
                    	var status = "MEETING";
                    } else if (noticeInfo.new_status == 'In-Training') {
                    	var status = "TRAINING";
                    }else{
                    	var status = noticeInfo.new_status;
                    }
                    $rootScope.$broadcast("prospect-timer-reset");
                    $rootScope.$broadcast("prospect-timer-start");
                    ///var user_status = status.replace('-','');
                    changeStatus(status.toUpperCase(),$scope.campaign.id);
                });

                $scope.socialPersonUrl = "";
                $scope.socialCompanyUrl = "";
                $scope.setSocialLinks = function() {
                    // Set person url
                    if ($scope.prospectCall.prospect.zoomPersonUrl != null && $scope.prospectCall.prospect.zoomPersonUrl != undefined && $scope.prospectCall.prospect.zoomPersonUrl != '') {
                        $scope.socialPersonUrl = $scope.prospectCall.prospect.zoomPersonUrl;
                    } else if ($scope.prospectCall.zoomPersonUrl != null && $scope.prospectCall.zoomPersonUrl != undefined && $scope.prospectCall.zoomPersonUrl != '') {
                        $scope.socialPersonUrl = $scope.prospectCall.zoomPersonUrl;
                    } else {
                        $scope.socialPersonUrl = "";
                    }

                    // Set company url
                    if ($scope.prospectCall.prospect.zoomCompanyUrl != null && $scope.prospectCall.prospect.zoomCompanyUrl != undefined && $scope.prospectCall.prospect.zoomCompanyUrl != '') {
                        $scope.socialCompanyUrl = $scope.prospectCall.prospect.zoomCompanyUrl;
                    } else if ($scope.prospectCall.zoomCompanyUrl != null && $scope.prospectCall.zoomCompanyUrl != undefined && $scope.prospectCall.zoomCompanyUrl != '') {
                        $scope.socialCompanyUrl = $scope.prospectCall.zoomCompanyUrl;
                    } else {
                        $scope.socialCompanyUrl = "";
                    }
                }

                $scope.addressLine1 = "";
                $scope.addressLine2 = "";
                $scope.setAddressLine = function() {
                    // set address line 1
                    if ($scope.prospectCall.prospect.addressLine1 != null || $scope.prospectCall.prospect.addressLine1 != undefined || $scope.prospectCall.prospect.addressLine1 != '') {
                        $scope.addressLine1 = $scope.prospectCall.prospect.addressLine1;
                    } else {
                        $scope.addressLine1 = "";
                    }

                    // set address line 2
                    if ($scope.prospectCall.prospect.addressLine2 != null || $scope.prospectCall.prospect.addressLine2 != undefined || $scope.prospectCall.prospect.addressLine2 != '') {
                        $scope.addressLine2 = $scope.prospectCall.prospect.addressLine2;
                    } else {
                        $scope.addressLine2 = "";
                    }
                }

                Pusher.subscribe(user.id, 'SIGNALWIRE_MANUAL_CALLSID', function (callSid) {
                    $scope.signalwireCallSid = callSid;
                    $scope.prospectCall.twilioCallSid = $scope.signalwireCallSid;
                    $scope.updateCallStatusManualDial($scope.signalwireCallSid);
                });

                Pusher.subscribe(user.id, 'prospect_call', function (prospectCall) {
                    $log.debug("Received a new prospect");
                    $scope.opentabs('Questions');
                    var isCampaignModeChanged = false;
                    $scope.muteStatus = false;
                    $scope.telnyxCallLegId = prospectCall.twilioCallSid;
                    $sessionStorage.callLegId = $scope.telnyxCallLegId;
                    $scope.signalwireCallSid = prospectCall.twilioCallSid;

                    if ($scope.parent.state == 1) {
                    	// Prospect is sent only when the state is campaign allocated
                        $scope.prospectCall = prospectCall;
                        if (($scope.prospectCall.prospect.dataSource != null && $scope.prospectCall.prospect.dataSource != undefined && $scope.prospectCall.prospect.dataSource.includes("Xtaas Data Source")) || ($scope.prospectCall.prospect.source != null && $scope.prospectCall.prospect.source != undefined && ($scope.prospectCall.prospect.source.includes("ZOOINFO") || $scope.prospectCall.prospect.source.includes("LeadIQ") || $scope.prospectCall.prospect.source.includes("INSIDEVIEW")))) {
                            $scope.emailRevealed = false;
                            $scope.defEmailUnhide = false;
                        } else {
                            $scope.defEmailUnhide = true;
                        }

                        // set prospect received time for agent call monitoring
                        $scope.prospectReceivedTime = new Date();

                        $scope.setCustomAttributes();
                        $scope.hideSocialLinkAndAddress = false;
                        if ($scope.campaign != null && $scope.campaign != undefined && $scope.campaign.unHideSocialLink) {
                            $scope.setSocialLinks();
                            $scope.setAddressLine();
                            $scope.hideSocialLinkAndAddress = true;
                        }

                        $scope.alternateProspectLimit = 4;
                        $scope.previousProspectType = $scope.currentProspectType;

                        if ($scope.campaign.dialerMode == "PREVIEW") {
                            $scope.prospectCall.prospect.prospectType = '';
                            $scope.parent.state = 4;
                        }

                        if ($scope.prospectCall.prospect.prospectType == 'POWER') {
                        	$scope.isPhoneNumberConnected = true;
                        }

                        if ($scope.campaign.dialerMode == "RESEARCH_AND_DIAL") {
                        	// Below code is written to identify that campaign mode has been changed from one mode to another. and notify agent with changed mode.
                            // Campaign modes are 1) Research 2) Power 3) Preview
                            if ($scope.currentProspectType != undefined && $scope.currentProspectType != null) {
                                if ($scope.currentProspectType != prospectCall.prospect.prospectType) {
                                    // NotificationService.log('success', 'Campaign mode has been changed from ' + $scope.currentProspectType + ' to ' + prospectCall.prospect.prospectType);
                                    $scope.currentProspectType = prospectCall.prospect.prospectType;
                                    isCampaignModeChanged = true;
                                }
                            }  else {
                                $scope.currentProspectType = prospectCall.prospect.prospectType;
                            }
                            if ($scope.currentProspectType == 'RESEARCH' || $scope.currentProspectType == 'PREVIEW') {
                                $scope.disableDialButtonInPreviewMode = false;
                            }
                        }
                    	// add primary prospect and indirect prospects into an array  to show on UI
                    	$scope.indirectProspects = [];
                    	if (prospectCall.indirectProspects != undefined && prospectCall.indirectProspects != null
                    			&& prospectCall.indirectProspects.length > 0) {
                    		$scope.addProspectsInIndirectArray(prospectCall);
                    		$scope.dispositionModel = []; // array for popup disposition binding
                			angular.forEach(prospectCall.indirectProspects, function(value, key) {
                                if ($scope.campaign.enableCustomFields && (value.customFields == null || value.customFields == undefined)) {
                                    if ($scope.campaign.enableCustomFields && $scope.prospectCall.prospect.customFields != null && $scope.prospectCall.prospect.customFields != undefined) {
                                        value.customFields = $scope.prospectCall.prospect.customFields;
                                    } else if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                                        value.customFields = angular.copy($scope.defaultCustomFields);
                                    }
                                }
                    			$scope.indirectProspects.push(value);
                    			$scope.dispositionModel[key] = undefined;
    						});

                    		// $scope.popupSubmitButtons.push(0);
                            $scope.showAlternateProspectModal(); // open alternate prospect popup
                            if (isCampaignModeChanged) {
                                $scope.showCampaignModeChangeModal();
                            }
						} else if ($scope.currentProspectType == 'PREVIEW'
								&& prospectCall.prospect.directPhone == false) {
							$scope.addProspectsInIndirectArray(prospectCall);
							$scope.dispositionModel = []; // array for popup disposition binding
                    		// $scope.popupSubmitButtons.push(0);
                            $scope.showAlternateProspectModal(); // open alternate prospect popup
                            if (isCampaignModeChanged) {
                                $scope.showCampaignModeChangeModal();
                            }
						}

                    	// $scope.multiProspectDetails = $scope.prospectCall.prospect.firstName + "_" + $scope.prospectCall.prospect.lastName;
                    	$scope.multiProspectDetails = $scope.prospectCall.prospectCallId;
                    	$scope.previousAlternateProspects = []; // reset to disable alternate prospect button, if present
                        $scope.prospect.originalDepartment =  $scope.prospectCall.prospect.department;//store original value as temporary required for empty selected option for questions
                        $scope.prospect.originalTitle =  $scope.prospectCall.prospect.title;
                        $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                        /* START PHONE HIDE
                         * DATE : 03/11/2017
                         * REASON : Added to hide phone number on agent screen from third party center */
                        if (($scope.campaign.dialerMode == "POWER" || $scope.campaign.dialerMode == "PREVIEW" || $scope.campaign.dialerMode == "RESEARCH_AND_DIAL" || ($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'AUTO')) && $scope.campaign.hidePhone) {
                        	$scope.prospectCall.prospect.phone = "xxx-xxx-xxxx";
                        }

                        /* END PHONE HIDE */
                        setProspectVariables();
                        $rootScope.$broadcast("prospect-timer-reset");
                        $rootScope.$broadcast("prospect-timer-start");
                        $scope.currentCallStateTime = new Date().getTime();
                        $scope.initialCallStateTime =  $scope.currentCallStateTime;
                        $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                        $scope.callHistory = $scope.prospectCall.notes;
                        $scope.prospectNote = [];
                		angular.forEach($scope.prospectCall.notes,function(note,index) {
                			$scope.prospectNote.push(note);
						});
                		if ($scope.campaign.enableCustomFields) {
                            var data = [];
                            angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                                if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall.answers != null 
                                    && $scope.prospectCall.answers != undefined && $scope.prospectCall.answers.length > 0) {
                                        // angular.forEach($scope.prospectCall.answers, function(value, key) {
                                            var filteredAns = $scope.prospectCall.answers.filter(e => e.question.toLowerCase() == questAttrValue.questionSkin.toLowerCase());
                                            // if (value.question.toLowerCase() == questAttrValue.questionSkin.toLowerCase()) {
                                            if (filteredAns != null && filteredAns != undefined && filteredAns.length > 0) {
                                                var answer = {
                                                    question: questAttrValue.questionSkin,
                                                    sequence: questAttrValue.sequence,
                                                    answer: filteredAns[0].answer
                                                }
                                                data.push(answer);
                                            } else {
                                                var answer = {
                                                    question: questAttrValue.questionSkin,
                                                    sequence: questAttrValue.sequence,
                                                    answer: questAttrValue.value
                                                }
                                                data.push(answer);
                                            }
                                        // })
                                } else {
                                    var answer = {
                                        question: questAttrValue.questionSkin,
                                        sequence: questAttrValue.sequence,
                                        answer: questAttrValue.value
                                    }
                                    data.push(answer);
                                }
                            });
                            $scope.prospectCall.answers = data;
                        } else {
                            $scope.prospectCall.answers = [];
                            angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                                var answer = {
                                        question: questAttrValue.questionSkin,
                                        sequence: questAttrValue.sequence,
                                }
                                $scope.prospectCall.answers.push(answer);
                            });
                        }
                        $scope.totalQuestion = 0;
                        angular.forEach($scope.campaign.questionAttributeMap, function(v, k) {
                            $scope.totalQuestion = $scope.totalQuestion + 1;
                        });
                        $scope.showHidePhoneNumberOnScreen();
                        $scope.showHideEmailOnScreen();
                        if ($scope.isAutoDialingEnabled() && $scope.prospectCall.prospect.prospectType == 'POWER') {
                            $log.debug("Notifying the timer and calling time");
                            //$rootScope.$broadcast("timer-start");
                            $rootScope.$broadcast('call-timer-start');
                            $scope.prospectCall.callStartTime = new Date();
                            $log.debug("Changing the state to call state");

                            $log.debug("Notifying the backend system about the call status");
                            callStatusService.save({}, {
                                callState: XTAAS_CONSTANTS.callStatus.connected,
                                callStartTime: $scope.prospectCall.callStartTime
                            });
                            $log.debug("Prev state " + $scope.parent.state);
                            console.log($scope.$id);
                            if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && $scope.campaign.dialerMode !== "PREVIEW") {
                                $scope.disconnectButtonMethod = $timeout($scope.disconnectButtonTimeout, $scope.disconnectButtonAutoTimer);
                            } else {
                                if ($scope.campaign.dialerMode !== "PREVIEW") {
                                    $scope.parent.state = 3;
                                }
                            }
                            $log.debug("New state " + $scope.parent.state);
                        } else {
                            $log.debug("Changing the state to prospect state");
                            $scope.prospectCall.callStartTime = new Date();
                            callStatusService.save({}, {
                                callState: XTAAS_CONSTANTS.callStatus.preview
                            });
                            $log.debug("Prev state " + $scope.parent.state);
                            console.log($scope.$id);
                            // $scope.parent.state = 2;
                            if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true" && $scope.campaign.dialerMode !== "PREVIEW") {
                                $scope.disconnectButtonMethod = $timeout($scope.disconnectButtonTimeout, $scope.disconnectButtonAutoTimer);
                            } else {
                                if ($scope.campaign.dialerMode !== "PREVIEW") {
                                    $scope.parent.state = 3;
                                }
                            }
                            if ($scope.campaign.dialerMode == "PREVIEW_SYSTEM") {
                                $scope.connectCall();
                            }
                            $log.debug("New state " + $scope.parent.state);
                        }
                        $log.debug("Broadcasting the prospect change");
                        $rootScope.$broadcast("onProspectChange", $scope.getProspectName());
                        if ($scope.currentProspectType == 'RESEARCH') {
                            if ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined) {
                                $scope.showCreateLink = true;    
                            }
                            $scope.showAlternateLink = false;
                            $scope.showResearchAndDialProspectModal($scope.currentProspectType);
                            if (isCampaignModeChanged) {
                                $scope.showCampaignModeChangeModal();
                            }
                        }
                    } else {
                        $log.debug("Error as previous state issue:" + $scope.parent.state);
                        throw "The current state doesn't support prospects";
                    }
                });

                Pusher.subscribe(user.id, 'callback_prospect', function (prospectCall) {
                    $sessionStorage.callbackProspect = prospectCall;
                    $scope.prospect.originalHiddenPhone = prospectCall.prospect.phone;
                    var prospectName = prospectCall.prospect.firstName + " " + prospectCall.prospect.lastName;
                    if ($scope.parent.state == 1) {
                        // $rootScope.$broadcast("onProspectChange", $scope.getProspectName());
                        $rootScope.$emit("onCallBackEventReceived", prospectName);
                        $sessionStorage.showStatusChangeModal = false;
                        $scope.requestOfflineAndNavigateToSplash();
                    } else {
                        $rootScope.$emit("onCallBackEventReceived", prospectName);
                        // $scope.callbackEventOccured = true;
                    }
                })

                Pusher.subscribe(user.id, 'no_callable_logout', function (data) {
                    if ($scope.parent.state == 1) {
                        $scope.refreshPlivo();
                        var msg = "noData";
                        $scope.logOutAgent(msg);
                    } else {
                        $scope.noCallableLogOut = true;
                    }
                })

                $scope.requestOfflineAndNavigateToSplash = function() {
                        $rootScope.$broadcast("onChangeStatus", 'REQUEST_OFFLINE');
                        if ($scope.isAutoDialingEnabled()) {
                            var agentType = "";
                            if ($scope.agentType != undefined && $scope.agentType != null) {
                                agentType = $scope.agentType;
                            }
                            agentStatusService.save({}, {
                                agentCommand: "STATUS_CHANGE",
                                agentStatus: "OFFLINE",
                                campaignId: $scope.campaign.id,
                                agentType: agentType
                            }).$promise.then(function (data) {
                                $scope.deleteEndpointAndNavigate();
                                }, function (error) {
                                $scope.deleteEndpointAndNavigate();   
                            });
                        }  else {
                            $scope.deleteEndpointAndNavigate();
                        }                      
                }

                $scope.deleteEndpointAndNavigate = function() {
                    if ($scope.getCallControlProvider() == 'Plivo') {
                        $scope.refreshPlivo();
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        $scope.telnyxHangupCall();
                        $scope.refreshTelnyx();
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                        $scope.refreshSignalwire();
                    }
                    $state.go('agentsplash');
                    $state.go('agentsplash');  
                }

                $scope.disconnectButtonTimeout = function () {
                    $scope.parent.state = 3;
                }

                $scope.hideEmailAddress = function() {
                    $scope.originalEmail = $scope.prospectCall.prospect.email;
                    if ($scope.prospectCall.prospect.email != null && $scope.prospectCall.prospect.email != undefined) {
                        var emailHide = $scope.prospectCall.prospect.email.replace(/(.{0})(.*)(?=@)/,
                        function(gp1, gp2, gp3) {
                            for(let i = 0; i < gp3.length; i++) {
                            gp2+= "*";
                            } return gp2;
                        });
                        if($scope.campaign.revealEmail) {

                        }else {
                            // $scope.prospectCall.prospect.email = emailHide;
                        }

                    }
                    return $scope.prospectCall.prospect.email;
                };

                $scope.showHideEmailOnScreen = function() {
                    if ($scope.campaign.dialerMode == 'RESEARCH'|| ($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'MANUAL')){
                        return $scope.prospectCall.prospect.email;
                     }

                     if ($scope.campaign.organizationId === $scope.prospectCall.organizationId) {
                        if (($scope.prospectCall.prospect.source !== "INTERNAL" || $scope.prospectCall.prospect.source !== "ZOOINFO") && $scope.campaign.hideEmail) {
                            return $scope.hideEmailAddress();
                        } else {
                            return $scope.emailHide = $scope.prospectCall.prospect.email;
                        }
                   } else {
                       if ($scope.campaign.hideEmail) {
                        return $scope.hideEmailAddress();
                       } else {
                        return $scope.emailHide = $scope.prospectCall.prospect.email;
                       }
                   }
                }

                $scope.showHidePhoneNumberOnScreen = function() {
                    $scope.showPhoneNumber = false;
                    if (($scope.prospectCall.prospect.source == "INTERNAL" || $scope.prospectCall.prospect.source == "ZOOINFO")) {
                        $scope.showPhoneNumber = false;
                    } else {
                        if ($scope.campaign.organizationId === $scope.prospectCall.organizationId) {
                            if (($scope.prospectCall.prospect.source !== "INTERNAL" || $scope.prospectCall.prospect.source !== "ZOOINFO") && $scope.campaign.hidePhone) {
                                $scope.showPhoneNumber = false;
                            } else {
                                $scope.showPhoneNumber = true;
                            }
                       } else {
                           if ($scope.campaign.hidePhone) {
                               $scope.showPhoneNumber = false;
                           } else {
                               $scope.showPhoneNumber = true;
                           }
                       }
                    }
                }

                $scope.addProspectsInIndirectArray = function (prospectCall) {
                	var obj = {};
            		obj.firstName = prospectCall.prospect.firstName;
            		obj.lastName = prospectCall.prospect.lastName;
            		obj.title = prospectCall.prospect.title;
            		obj.department = prospectCall.prospect.department;
                    obj.company = prospectCall.prospect.company;
                    obj.email = prospectCall.prospect.email;
                    obj.extension = prospectCall.prospect.extension;
                    obj.city = prospectCall.prospect.city;
                    obj.zipCode = prospectCall.prospect.zipCode;
                    obj.domain = prospectCall.prospect.customAttributes.domain;
            		obj.campaignId = prospectCall.campaignId;
            		obj.prospectCallId = prospectCall.prospectCallId;
            		obj.qualityBucket = prospectCall.qualityBucket;
            		obj.qualityBucketSort = prospectCall.qualityBucketSort;
            		obj.multiProspect = prospectCall.multiProspect;
                    if ($scope.campaign.enableCustomFields && prospectCall.prospect.customFields != null && prospectCall.prospect.customFields != undefined) {
                        obj.customFields = prospectCall.prospect.customFields;
                    } else if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                        obj.customFields = angular.copy($scope.defaultCustomFields);
                    }
            		$scope.indirectProspects.push(obj);
                }

                Pusher.subscribe(user.id, 'telnyx_conference_creation_issue', function (message) {
                    NotificationService.log('error', 'There is some technical issue. Please try login again.');
                    $scope.notices = [];
                    var message = "There is some technical issue. Please try login again.";
                    $scope.notices.push(message);
                });

                Pusher.subscribe(user.id, 'call_disconnect', function (message) {
                    removeBlinkerFromDialButton();
                    if ($scope.parent.state == 3) {
                        // if ($scope.getCallControlProvider() == "Telnyx") {
                        //     $scope.endCall("fromTelnyxServer");
                        // } else if ($scope.getCallControlProvider() == "Plivo") {
                        //     $scope.endCall();
                        // }
                        $scope.endCall();
                    	$log.debug("Disconnecting the agent call as it has already been disconnected by the prospect");
                    	$scope.showCallDisconnectMessage(message);
//                    	$timeout(function() {
//                    		$scope.resetNotices();
//                    	}, 8000);
                    } else if (message != null && message != undefined && message != '') {
                        $scope.showCallDisconnectMessage(message);
                        // $scope.showCallDisconnectErrorMessage(message);
                    }
                });

                Pusher.subscribe(user.id, 'telnyx_manual_call_answered', function (message) {
                    removeBlinkerFromDialButton();
                });


                Pusher.subscribe(user.id, 'amd_detect', function (message) {
                    if (!$scope.isMachineDetectModalOpen) {
                        $scope.showMachineDetectedModal();
                    }
                });

                $scope.showCallDisconnectErrorMessage = function (message) {
                    $scope.notices = [];
                    $scope.parent.state = 4;
                    $scope.notices.push(message);
                    NotificationService.log('error', message);
                }

                $scope.showCallDisconnectMessage = function (message) {
                	$scope.resetNotices();
                	if (message == 'failed') {
                		$scope.notices.push('Call failed.');
                        NotificationService.log('error', 'Call failed.');
                	} else if (message == 'busy') {
                		$scope.notices.push('Number busy.');
                        NotificationService.log('error', 'Number busy.');
                	} else if (message == 'sip-failure') {
                		$scope.notices.push('Issue with Voice provider, please redial or contact support.');
                        NotificationService.log('error', 'Issue with Voice provider, please redial or contact support.');
                	} else if (message == 'no-answer') {
                		$scope.notices.push('Call not answered.');
                        NotificationService.log('error', 'Call not answered.');
                	} else if (message == 'invalid-phone') {
                		$scope.notices.push('Invalid phone number.');
                        NotificationService.log('error', 'Invalid phone number.');
                	} else if (message == 'dest-unavailable') {
                		$scope.notices.push(' The number you have dialed is not available. Please check the number.');
                        NotificationService.log('error', ' The number you have dialed is not available. Please check the number.');
                	} else if (message == 'CALL_MACHINE_ANSWERED') {
                        $scope.notices.push('Answering Machine – call auto-disposed.');
                		NotificationService.log('error', 'Answering Machine – call auto-disposed.');
                		$scope.submitProspect("AUTO_AMD");
                	} else if (message == 'TELNYX_CALL_MACHINE_ANSWERED') {
                        $scope.refreshData();
                        $scope.notices.push('Answering Machine – call auto-disposed.');
                        NotificationService.log('error', 'Answering Machine – call auto-disposed.');
                    } else if (message == 'server-failure') {
                		$scope.notices.push('Error reaching server.');
                        NotificationService.log('error', 'Error reaching server.');
                	}
                }

                $scope.setDialerCodeStatus = function (subStatus) {
                    $scope.submitLeadFlag = true;
                    // $scope.prospectCall.dispositionStatus = "DIALERCODE";
                    if (subStatus == "CALL_LATER") {
                        $scope.prospectCall.dispositionStatus = "DIALERCODE";
                        $scope.prospectCall.subStatus = "CALL_LATER";
                        $scope.callLaterStatus = "CALL_LATER";
                        $scope.disposition = "CALL_LATER";
                        $scope.dialercodestatus = subStatus;
                        $scope.callbackcode = undefined;
                    } else if (subStatus == "ANSWERMACHINE") {
                        $scope.prospectCall.dispositionStatus = subStatus;
                        $scope.prospectCall.subStatus = subStatus;
                        $scope.disposition = "DIALERCODE";
                        $scope.dialercodestatus = subStatus;
                        $scope.callbackcode = undefined;//reset callback code status if it is set
                    } else {
                        $scope.prospectCall.dispositionStatus = "DIALERCODE";
                        $scope.prospectCall.subStatus = subStatus;
                        $scope.disposition = "DIALERCODE";
                        $scope.dialercodestatus = subStatus;
                        $scope.callbackcode = undefined;//reset callback code status if it is set
                    }
                }

                $scope.setCallbackStatus = function (subStatus) {
                   /* $scope.prospectCall.dispositionStatus = "CALLBACK";
                    $scope.prospectCall.subStatus = subStatus;
                    $scope.disposition = "CALLBACK";
                    $scope.dialercodestatus = subStatus;*/
                	if ($scope.prospectCall.subStatus != subStatus) {
                		$scope.prospectCall.dispositionStatus = undefined;
                        $scope.prospectCall.subStatus = undefined;
                        $scope.disposition = undefined;
                        $scope.dialercodestatus = undefined;
                    }
                    $scope.submitLeadFlag = true;
                    $scope.dialercodestatus = undefined;//reset dialercode status if it is set
                	$scope.setCallStatus("CALLBACK");
                }

                $scope.setDisposition = function (dispositionStatus) {
                    NotificationService.clear();
                	if (dispositionStatus == "FAILURE") {
                        $scope.submitLeadFlag = true;
                    	if ($scope.dialercodestatus != undefined) { //reset dialer code substatus if it is already set
                    		$scope.prospectCall.subStatus = null;
                    		$scope.dialercodestatus = null;
                    	}
                    	if (!$scope.campaign.simpleDisposition) {
                            $scope.prospectCall.subStatus = "OTHER";
                            if (!$scope.isMobileTablet()) {
                                $scope.showFailureCodeModal();
                            }
                    	} else {
                            // $scope.showFailureCodeModal();
                    		$scope.prospectCall.subStatus = "OTHER";
                    	}
                    } else if (dispositionStatus == "SUCCESS") {
                        // Below IF condition is for campaign type id LEAD VERIFICATION.
                        if ($scope.campaign.type === 'Lead Verification' || $scope.campaign.type === 'LeadVerification') {
                            if (!$scope.checkSuccessStatusForLeadVerificationCampaign()) {
                                return;
                            }
                            $scope.dialercodestatus = "SUBMITLEAD";
                        }
                        $scope.extendSimpleDispositionTime = true;
                        if ($scope.isMobileTablet() || window.location.hash == "#!/mobileagent") {
                            $scope.submitLeadFlag = true;
                            $scope.prospectCall.dispositionStatus = "SUCCESS";
                            $scope.prospectCall.subStatus = "SUBMITLEAD";
                            $scope.disposition = "SUCCESS";
                            $scope.dialercodestatus = "SUBMITLEAD";
                        } else if ($scope.checkeditname == "yes" &&  $scope.checkeditcompany == "yes" &&  $scope.checkedittitle == "yes" && $scope.checkeditphone == "yes" && $scope.checkeditemail == "yes" && $scope.checkeditdept == "yes") {
                            // $scope.submitLeadFlag = false;
                            if ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification') {
                                $scope.showSubmitProspectModal($scope.currentProspectType);
                            }
                        } else {
                            $scope.extendSimpleDispositionTime = true;
                            if ($scope.checkeditname == "yes" &&  $scope.checkeditcompany == "yes" &&  $scope.checkedittitle == "yes" && $scope.checkeditphone == "yes" && $scope.checkeditemail == "yes" && $scope.checkeditdept == "yes") {
                                $scope.submitLeadFlag = false;
                                $scope.showSubmitProspectModal($scope.currentProspectType);
                            } else {
                                NotificationService.log('error', 'Please confirm all prospect details first.');
                                $scope.submitLeadFlag = false;
		                        return;
                            }
                            $scope.dialercodestatus = "SUBMITLEAD";
                            if ($scope.prospectCall.dispositionStatus != "SUCCESS") {//do validation when checking the success checkbox
                                $scope.checkSuccessStatus();
                            } else {//no validation when unchecking success checkbox
                                $scope.prospectCall.dispositionStatus = null;
                                $scope.prospectCall.subStatus = null;
                            }
                        }
                        // if (!$scope.isMobileTablet()) {
                        //     $scope.dialercodestatus = "SUBMITLEAD";
                        //     if ($scope.prospectCall.dispositionStatus != "SUCCESS") {//do validation when checking the success checkbox
                        //         $scope.checkSuccessStatus();
                        //     } else {//no validation when unchecking success checkbox
                        //         $scope.prospectCall.dispositionStatus = null;
                        //         $scope.prospectCall.subStatus = null;
                        //     }
                        // }
                    } else if (dispositionStatus == "DNCL") {
                        $scope.submitLeadFlag = true;
                        $scope.prospectCall.dispositionStatus = "DNCL";
                        $scope.prospectCall.subStatus = "DNCL";
                        $scope.disposition = "DNCL";
                        $scope.dialercodestatus = "DNCL";
                    } else if (dispositionStatus == "INVALID_DATA") {
                        $scope.submitLeadFlag = true;
                        $scope.prospectCall.dispositionStatus = "INVALID_DATA";
                        $scope.prospectCall.subStatus = "INVALID_DATA";
                        $scope.disposition = "PROSPECT_UNREACHABLE";
                        $scope.dialercodestatus = "PROSPECT_UNREACHABLE";
                    } else if (dispositionStatus == "CALLBACK") {
                        $scope.submitLeadFlag = true;
                        $scope.prospectCall.dispositionStatus = "CALLBACK";
                        $scope.prospectCall.subStatus = "CALLBACK";
                        $scope.disposition = "CALLBACK";
                        $scope.dialercodestatus = "CALLBACK";
                    } else if (dispositionStatus == "ANSWERMACHINE") {
                        $scope.setDialerCodeStatus(dispositionStatus);
                    }
                }
                $scope.callback.starthours = null;
                $scope.callback.endhours = null;
                $scope.callback.ampm = null;

                $scope.setCallStatus = function (dispositionStatus) {
                    if (dispositionStatus == "CALLBACK") {
                        /*var callbackModal = $modal.open({
                         templateUrl: 'callback-modal.html',
                         controller: 'CallbackController',
                         resolve: {
                         callbackHours: function () {
                         return $scope.prospectCall.callbackHours;
                         }
                         }
                         });
                         callbackModal.result.then(function (callbackHours) {
                         $scope.prospectCall.callbackHours = callbackHours;
                         $scope.prospectCall.dispositionStatus = dispositionStatus;
                         }, function () {
                         $log.debug('Modal dismissed at: ' + new Date());
                         });*/
                    	if ($scope.callback.currentEventDate == null || $scope.callback.currentEventDate == "") {
                    		$scope.callback.currentEventDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                    	}
                        getStateService.query().$promise.then(function (data) {
                        	$scope.stateList = [];
                        	angular.forEach(data,function (v, k) {
                        		var state = v.value.split("-");
                        		$scope.stateList.push({"stateCode": state[1], "stateName": v.label});
                        	});
                        },function (error) {
                        });

                        getCallbackTimezoneService.get({id: $scope.prospectCall.prospect.stateCode}).$promise.then(
                            function (data) {
                                console.log(data);
                                angular
                                    .forEach(
                                    $scope.stateList,
                                    function (v, k) {

                                        if (v.stateCode == $scope.prospectCall.prospect.stateCode) {
                                            $scope.callback.currentstateName = v.stateName;
                                        }

                                    });
                                $scope.callback.currentTimeZone = data.StateTimeZone;
                                $scope.callback.currentTime = data.CurrentTimeInStateTZ;
                                $scope.callback.endworkinghours = data.AgentWorkEndHrInStateTZ;
                                $scope.callback.timeZone = data.StateTimeZone;
                                $scope.callback.hoursList = [];
                                for (i = data.AgentWorkStartHrInStateTZ; i <= data.AgentWorkEndHrInStateTZ; i++) {
                                    $scope.callback.hoursList.push($scope.hrsList[i]);
                                }
                            },
                            function (error) {
                            });

                        // Don't open callback modal
                        if ($scope.campaign.simpleDisposition) {
                        	$scope.prospectCall.dispositionStatus = "CALLBACK";
                        	return;
                        }

                        $scope.isSelectState = false;
                         var callbackModal = $modal.open({
                            scope: $scope,
                            templateUrl: 'callback-modal.html',
                            controller: 'CallbackController',
                            windowClass: 'setcallback',
                        });

                        callbackModal.result.then(function (selectedItem) {
                  		 }, function () {
                  			//Below condition will execute when modal getting close
                  			if (!$rootScope.callbackSubStatusSelected) {//condition true, when callback sub-status not selected
                  												//condtion false, when callback substatus selected
                  				$scope.dialercodestatus = null;
                  				$scope.disposition = "";
                  				//if substatus not checked, uncheck the disposition status Or if substatus checked already for other disposition status also uncheck disposition status
                        		if ($scope.prospectCall.subStatus == undefined || $scope.prospectCall.subStatus == "" || ($scope.prospectCall.subStatus != "CALLBACK_USER_GENERATED" &&
                        				$scope.prospectCall.subStatus != "CALLBACK_GATEKEEPER")) {
                  					$scope.prospectCall.dispositionStatus = null;
                  					$scope.callback.starthours = null;
                  	                $scope.callback.endhours = null;
                  	                $scope.callback.ampm = null;
                  	                $scope.callbackcode = null;
                  	                $scope.callback.currentEventDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                              	}
                  			}
                  		 });
                    } else {
                        var items;
                        switch (dispositionStatus) {
                            case 'SUCCESS':
                                items = successDispositions;
                                break;
                            case 'FAILURE':
                                items = failureDispositions;
                                break;
                            case 'DIALERCODE':
                                items = dialerCodeDispositions;
                                break;
                        }
                        var callbackModal = $modal.open({
                            templateUrl: 'disposition-modal.html',
                            controller: 'DispositionStatusController',
                            resolve: {
                                items: function () {
                                    return items;
                                },
                                item: function () {
                                    return $scope.prospectCall.subStatus;
                                },
                                disposition: function () {
                                    return dispositionStatus;
                                }
                            }
                        });
                        callbackModal.result.then(function (subStatus) {
                            $scope.prospectCall.dispositionStatus = dispositionStatus;
                            $scope.prospectCall.subStatus = subStatus;
                        }, function () {
                            $log.debug('Modal dismissed at: ' + new Date());
                        });
                    }
                    $timeout(function () {
                        angular.element('#disposition-status').trigger('click'); // Hack
                        // as
                        // the
                        // popover
                        // doesn't
                        // support
                        // close
                        // on
                        // window
                        // actions
                    }, 0);

                }

                var validateStatus = function () {
                	$scope.disQualifyProsp = false;
                	angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                    	if(questAttrValue.qualifiedFlag == false && questAttrValue.qualifiedFlag != undefined) {
                    		$scope.disQualifyProsp = true;
                    	}
                    	return;
                    });

                	if($scope.disQualifyProsp == true && !$scope.campaign.simpleDisposition && !($scope.prospectCall.dispositionStatus == "FAILURE" && $scope.prospectCall.subStatus == "FAILED_QUALIFICATION")) {
                		NotificationService.log('error', 'Prospect is not qualified based on the responses. Please dispose call as Failure - Qualification');
                		return false;
                    }

                    if($scope.disQualifyProsp == true && $scope.campaign.simpleDisposition && $scope.prospectCall.dispositionStatus != "FAILURE") {
                		NotificationService.log('error', 'Prospect is not qualified based on the responses. Please dispose call as Failure.');
                		return false;
            		}

                	if ($scope.prospectCall.dispositionStatus == "" || $scope.prospectCall.dispositionStatus == undefined) {
                		NotificationService.log('error', 'Please select Disposition status or Dialer code first.');
                		return false;
                	}
                	if ($scope.prospectCall.dispositionStatus == "SUCCESS") {
                		if (!$scope.checkSuccessStatus()) {//condition true, when success status not valid else submit prospect
                			return false;
                		}
                	}
                	if ($scope.prospectCall.dispositionStatus == "FAILURE") {
                		if ($scope.prospectCall.subStatus == "" || $scope.prospectCall.subStatus == undefined) {
                    		NotificationService.log('error', 'Please select Substatus in Failure disposition.');
                    		return false;
                    	}
                	}

                	if (!$scope.campaign.simpleDisposition) {
                		if ($scope.prospectCall.dispositionStatus == "CALLBACK") {
                    		if($scope.callback.starthours == null || $scope.callback.starthours == undefined || $scope.callback.starthours == '') {
                        		NotificationService.log('error', 'Please set Hour in Callback disposition.');
                        		return false;
                        	}
                    		if($scope.callback.endhours == null || $scope.callback.endhours == undefined || $scope.callback.endhours == '') {
                        		NotificationService.log('error', 'Please set Minute in Callback disposition.');
                        		return false;
                        	}
                    	}
                	}
                	return true;
                }

                $scope.validateCustomQuestionAnswers = function() {
                    var invalidCustomQuestionAnswers = false;
                    if ($scope.campaign.qualificationCriteria.expressions != null && $scope.campaign.qualificationCriteria.expressions != undefined
                        && $scope.prospectCall.answers != null && $scope.prospectCall.answers != undefined) {
                        angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionsValue, expressionsKey) {
                            angular.forEach($scope.prospectCall.answers, function(agentAnswer, agentKey) {
                                    if (expressionsValue.operator != "FREE_TEXT") {
                                        if (expressionsValue.questionSkin == agentAnswer.question) {
                                            var attributeValues = {};
                                            if (!angular.isArray(expressionsValue.value)) {
                                                attributeValues = expressionsValue.value.split(',');
                                            } else {
                                                attributeValues = expressionsValue.value;
                                            }
                                            var qualifiedResult = $filter('filter')(attributeValues, agentAnswer.answer)[0];
                                            if (qualifiedResult == undefined) {
                                                invalidCustomQuestionAnswers = true;
                                            }
                                        }
                                    }
                            })
                    })
                  }
                    return invalidCustomQuestionAnswers;
                }

                $scope.submitProspect = function (submitType) {
                	// if (submitType != "AUTO" && submitType != "AUTO_SUCCESS") {
                    $scope.isCustomFieldCollapsed = true;
                    $scope.isNewExtensionDialed = false;
                     $scope.search = false;
                    $scope.submitType = submitType;
                    if ($scope.prospectCall.dispositionStatus == "ANSWERMACHINE") {
                        $scope.prospectCall.dispositionStatus = "DIALERCODE"; 
                    }
                	if (submitType != undefined && submitType != "AUTO" && submitType != "AUTO_AMD") {
                		if (!validateStatus()) {//validate disposition status and substatus first.
                    		return;
                    	}
                    }

                    if ($scope.prospectCall.dispositionStatus === "SUCCESS" && $scope.validateCustomQuestionAnswers()) {
                        NotificationService.log('error', 'Prospect is not qualified - Please dispose it as FAILURE.');
                        return;
                    }

                    if ($scope.prospectCall.dispositionStatus === "SUCCESS" && ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification')){
                        if(!checkQuestionValidation()){
                            $scope.opentabs("Questions");
                            NotificationService.log('error', 'Please answer all questions first.');
                            return;
                        }
                    }

                    if (!$scope.isMobileTablet() && window.location.hash !== "#!/mobileagent" && $scope.prospectCall.dispositionStatus === "SUCCESS" && ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification')) {
                        if (!$scope.checkSuccessStatus()) {
                        return;
                        }
                    }

                    if (!$scope.isMobileTablet() && $scope.prospectCall.dispositionStatus === "SUCCESS" && ($scope.campaign.type === 'Lead Verification' || $scope.campaign.type === 'LeadVerification')) {
                        if (!$scope.checkSuccessStatusForLeadVerificationCampaign()) {
                            return;
                        }
                    }

                	if (submitType != undefined && (submitType == "AUTO" || submitType == "AUTO_AMD") && $scope.extendSimpleDispositionTime) {
                		// it will not auto submit prospects if agent selects success checkbox within 15 seconds
                		// $scope.autoSuccessInterval = $interval(function(){ $scope.submitProspect("AUTO_SUCCESS"); }, 105000, 1);
                		return;
					}
                	// if (submitType == "AUTO" || submitType == "AUTO_SUCCESS") {
                	if (submitType == "AUTO") {
						$scope.prospectCall.dispositionStatus = "AUTO";
	                    $scope.prospectCall.subStatus = "AUTO";
	                    $scope.disposition = "AUTO";
	                    $scope.dialercodestatus = "AUTO";
					} else if (submitType == "AUTO_AMD") {
						$scope.prospectCall.dispositionStatus = "DIALERCODE";
                        $scope.prospectCall.subStatus = "ANSWERMACHINE";
                        $scope.prospectCall.autoMachineDetected = true;
						$scope.disposition = "DIALERCODE";
						$scope.dialercodestatus = "ANSWERMACHINE";
					}
                	$scope.previousCallStateTime = $scope.currentCallStateTime;
                	$scope.currentCallStateTime = new Date().getTime();

                	var agentname = user.firstName+" "+user.lastName;
                	if ($scope.parent.note != undefined && $scope.parent.note != "") {
                		var noteObject = {
                                creationDate: new Date(),
                                text: $scope.parent.note,
                                agentId: user.id,
                                agentName: agentname,
                                dispositionStatus: $scope.prospectCall.dispositionStatus,
                                subStatus: $scope.prospectCall.subStatus
                		};
                		$scope.prospectNote.push(noteObject);
                	}

                    // when agent recieves prospect; campaign is in power mode and campaign hidePhone field is true then hiding phone from agent. here adding phone number to prospect again if it is modified or not
                    if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall.prospect != null && $scope.prospectCall.prospect != undefined) {
                        $scope.prospectCall.prospect.phone = $scope.prospect.originalHiddenPhone;
                    }

                	// HACK - to make dispositionStatus Failure if it is DNCL & Dialercode if it is Callback
                	if ($scope.campaign.simpleDisposition) {
                		if ($scope.prospectCall.dispositionStatus == "DNCL") {
                    		$scope.prospectCall.dispositionStatus = "FAILURE";
                    	}
                		if ($scope.prospectCall.dispositionStatus == "CALLBACK") {
                    		$scope.prospectCall.dispositionStatus = "CALLBACK";
                    		$scope.prospectCall.subStatus = "CALLBACK";
                    	}
                    }
                    if($scope.assetList.length == 1) {
                        $scope.prospectCall.deliveredAssetId = $scope.assetList[0].assetId;
                    }
                    if ($scope.prospectCall.dispositionStatus == "INVALID_DATA") {
                        $scope.prospectCall.dispositionStatus = "FAILURE";
                        $scope.prospectCall.subStatus = "INVALID_DATA";
                    }
                    if (!$scope.isMobileTablet() && $scope.originalEmail != null && $scope.originalEmail != undefined) {
                        $scope.prospectCall.prospect.email = $scope.originalEmail;
                    }
                    if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall.prospect != null && $scope.prospectCall.prospect != undefined) {
                        $scope.prospectCall.prospect.firstName = $scope.filterName($scope.prospectCall.prospect.firstName);
                        $scope.prospectCall.prospect.lastName = $scope.filterName($scope.prospectCall.prospect.lastName);
                    }
                	var prospectCall = {
                        prospectCallId: $scope.prospectCall.prospectCallId,
                        prospect: $scope.prospectCall.prospect,
                        twilioCallSid: $scope.prospectCall.twilioCallSid,
                        callStartTime: $scope.prospectCall.callStartTime,
                        callDuration: $scope.prospectCall.callDuration,
                        notes: $scope.prospectNote,
                        answers: $scope.prospectCall.answers,
                        deliveredAssetId: $scope.prospectCall.deliveredAssetId,
                        dispositionStatus: $scope.prospectCall.dispositionStatus,
                        subStatus: $scope.prospectCall.subStatus,
                        campaignId: $scope.prospectCall.campaignId,
                        agentId: user.id,
                        callbackTimezone: $scope.callback.timeZone,
                        callbackSelectedDate: $scope.callback.callbackDate,
                        callbackSelectedHours: $scope.callback.callbackHours,
                        callbackSelectedMin: $scope.callback.callbackMins,
                        callbackAmPm : $scope.callback.ampm,
                        outboundNumber: $scope.prospectCall.outboundNumber,
                        wrapupDuration : Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime)/ 1000),
                		prospectHandleDuration : Math.round(($scope.currentCallStateTime - $scope.initialCallStateTime)/ 1000),
                		prospectVersion: $scope.prospectCall.prospectVersion,
                		indirectProspects: $scope.prospectCall.indirectProspects,
                		qualityBucket: $scope.prospectCall.qualityBucket,
                		qualityBucketSort: $scope.prospectCall.qualityBucketSort,
                		multiProspect: $scope.prospectCall.multiProspect,
                		zoomCompanyUrl: $scope.prospectCall.zoomCompanyUrl,
                		zoomPersonUrl: $scope.prospectCall.zoomPersonUrl,
                		zoomValidated: $scope.prospectCall.zoomValidated,
                		leadDelivered: $scope.prospectCall.leadDelivered,
                		dataSlice: $scope.prospectCall.dataSlice,
                		machineDetectedIncrementer: $scope.prospectCall.machineDetectedIncrementer,
                        isFinalSubmit: true,
                        emailRevealed: $scope.emailRevealed
                    };

                    $scope.setAjaxWaiting();
                    prospectCallService.save(prospectCall).$promise.then(function () {
                    	// set parameters for agent wait time monitoring
                    	if ($scope.prospectSubmittedTime == null) {
                    		$scope.prospectSubmittedTime = new Date();
                    	}
                    	$scope.prospectSubmittedTimeOld = $scope.prospectSubmittedTime;
                    	$scope.prospectSubmittedTime = new Date();
                    	$scope.agentCallMonitoring(prospectCall);

                    	$scope.refreshData();
                    }, function () {
                        $scope.resetAjaxWaiting();
                    });
                }

                $scope.refreshData = function() {
                        $scope.socialPersonUrl = "";
                        $scope.socialCompanyUrl = "";
                        $scope.addressLine1 = "";
                        $scope.addressLine2 = "";
                        if ($scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization.showAlternateLink != null 
                            && $scope.loggedInUserOrganization.showAlternateLink != undefined 
                            && !$scope.loggedInUserOrganization.showAlternateLink && 
                            (($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'AUTO') || 
                              $scope.campaign.dialerMode == 'PREVIEW' || $scope.campaign.dialerMode == 'POWER')) {
                            $scope.showAlternateLink = false;
                        }
                        $scope.emailRevealed = false;
                        $scope.defEmailUnhide = false;
                        $scope.callRetryCount = 0;
                        $scope.checkDialerMode();
                        $log.debug("Reset the initialization values");
                        // Reset the intervals and timeouts of auto dispose modal popup in case of simple disposition.
                        if ($scope.prospectCall != null && $scope.prospectCall != undefined) {
                            $timeout.cancel($scope.timeOutMethod);
                            if ($scope.isFifteenSecDisconnectTimer != null && $scope.isFifteenSecDisconnectTimer != undefined && $scope.isFifteenSecDisconnectTimer == "true") {
                                $timeout.cancel($scope.disconnectButtonMethod);
                            }
                            $interval.cancel($scope.autoDisposeTimeInterval);
                            $interval.cancel($scope.machineDetectionTimeInterval);
                        }
                        $scope.agentOnlineCheckInTime = new Date();
                        $scope.prospectCall = {};
                        $scope.resetNotices();
                        $scope.resetAfterProspect();
                        delete $scope.parent.numberToDial;
                        delete $scope.parent.note;
                        $scope.resetStep();
                        $scope.variable.prospect = prospectVariable;
                        $rootScope.$broadcast('wrapup-timer-stop');
                        if (nextCampaign != $scope.campaign) {
                            $log.debug("Lazy loading campaign");
                            setCampaign(nextCampaign);
                        } else {
                            $log.debug("Changing the state to campaign state")
                            $log.debug("Prev state " + $scope.parent.state);
                            console.log($scope.$id);
                            $scope.parent.state = 1;
                            $log.debug("New state " + $scope.parent.state);
                        }
                        if ($scope.parent.status != 'ONLINE') {
                            $log.debug("Disconnecting active connections because of status chagne");
                            $rootScope.$broadcast("whisper-stop");
                            twilioService.disconnect();
                        }
                        $scope.callback.starthours = "hh";
                        $scope.callback.endhours = "00";
                        $scope.callback.ampm = "AM";
                        $scope.callback.currentEventDate = "";
                        $log.debug("Broadcasting the prospect change");
                        $rootScope.$broadcast("timer-reset");
                        $rootScope.$broadcast("wrapup-timer-reset");
                        $rootScope.$broadcast("prospect-timer-start");
                        $scope.resetAjaxWaiting();
                        // check if campaign has been changed for agent if changed then showing popup for agent to switch to new campaign
                        if ($scope.isCampaignChange) {
                        	 $scope.campaignChange();
                        }
                        // check if agent type  has been changed for agent if changed then showing popup for agent to new mode.
                        if ($scope.isAgentTypeChange) {
                            $scope.agentTypeChange();
                        }

                        if ($scope.isScriptChange) {
                            $scope.scriptChange();
                        }
                        // check is campaign settings changed
                        if ($scope.isCampaignSettingsChange) {
                            $scope.campaignSettingsChange();
                        }
                        $scope.extendSimpleDispositionTime = false;
                        if ($scope.campaign.dialerMode == 'RESEARCH'
                        	|| ($scope.campaign.dialerMode == 'HYBRID'
                        		&& $scope.agentType == 'MANUAL') || $scope.campaign.type == 'Prospecting') {
                            if ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined) {
                                $scope.showResearchAndDialProspectModal();
                                $scope.showCreateLink = true;
                                $scope.showAlternateLink = false;
                            }
                        }
                        $scope.phoneNumber = '';
                        $scope.revenue = '';
                        $scope.employeeCount = '';
                        $scope.callInfo = undefined;
                        $scope.checkAllLeadDetailsInConfirmTab();
                        if ($scope.loggedInUserOrganization.enableAgentInactivity != null && $scope.loggedInUserOrganization.enableAgentInactivity != undefined && $scope.loggedInUserOrganization.enableAgentInactivity) {
                            if (!$scope.isAutoDialingEnabled()) {
                                $scope.agentOnlineCheckInTime = new Date();
                                $scope.startAgentActivityCheckTimer();
                            } else {
                                $scope.checkAgentForContinousAutoDispose();
                            }
                        }
                        // if ($sessionStorage.callbackProspect) {
                        //     if (!$scope.callbackEventOccured) {
                        //         $sessionStorage.$reset();
                        //     }
                        //     $scope.requestOfflineAndNavigateToSplash();
                        // }
                        if ($sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined) {
                            $sessionStorage.showStatusChangeModal = true;
                        }
                        if (($sessionStorage.callbackProspect != null || $sessionStorage.callbackProspect != undefined) && $sessionStorage.showStatusChangeModal == false) {
                            $sessionStorage.callbackProspect = null;
                            $scope.requestOfflineAndNavigateToSplash();
                        }
                        if (($sessionStorage.callbackProspect != null || $sessionStorage.callbackProspect != undefined) 
                            && ($sessionStorage.showStatusChangeModal == true || 
                            $sessionStorage.callbackProspect == null || $sessionStorage.callbackProspect == undefined)) {
                            $sessionStorage.showStatusChangeModal = false;
                            $scope.requestOfflineAndNavigateToSplash();
                        }

                        if ($sessionStorage.missedCallback != null && $sessionStorage.missedCallback != undefined && $sessionStorage.missedCallback == true) {
                            $sessionStorage.showStatusChangeModal = false;
                            $sessionStorage.callbackProspect = null;
                            $scope.requestOfflineAndNavigateToSplash();
                        }
                        
                        if ($scope.noCallableLogOut != undefined && $scope.noCallableLogOut != null && $scope.noCallableLogOut == true) {
                            $scope.refreshPlivo();
                            var msg = "noData";
                            $scope.logOutAgent(msg);
                        }
                    }

                $scope.notifyAndLogOutAutoAgent = function () {
                    NotifySupervisorService.query({agentId : $scope.loggedInUser.id, mode: "AUTO" }).$promise.then(function(organization) {
                        $scope.refreshPlivo();
                        $scope.logOutAgent();
                    });
                }

                $scope.continuousAutoDispose = 0;
                $scope.checkAgentForContinousAutoDispose = function() {
                        if ($scope.submitType == "AUTO") {
                            $scope.continuousAutoDispose++;
                        } else {
                            $scope.continuousAutoDispose = 0;
                        }

                        if ($scope.campaign.supervisorAgentStatusApprove) {
                            if ($scope.parent.status != undefined && $scope.parent.status == 'ONLINE' && $scope.campaign != null && $scope.campaign != undefined  && $scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization != undefined 
                                && $scope.loggedInUserOrganization.agentAutoDisposeCount != 0 && $scope.loggedInUserOrganization.agentAutoDisposeCount == $scope.continuousAutoDispose) {
                                    $scope.notifyAndLogOutAutoAgent();
                            }
                        } else {
                            if ($scope.campaign != null && $scope.campaign != undefined  && $scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization != undefined 
                                && $scope.loggedInUserOrganization.agentAutoDisposeCount != 0 && $scope.loggedInUserOrganization.agentAutoDisposeCount == $scope.continuousAutoDispose) {
                                    $scope.notifyAndLogOutAutoAgent();
                            }
                        }
                }

                ////  changeStatus("IDLE");

//////////////////////////////////////////////Functionality for new screen show tabs , slide question etc//////////////////////////////////

                $scope.showNameForm = false;
                $scope.showTitleForm = false;
                $scope.showDeptForm = false;
                $scope.showPhoneForm = false;
                $scope.showRoleForm = false;
                $scope.isstatusdetail = false;
                $scope.showEmailForm = false;
                $scope.showCompanyForm = false;
                $scope.isEmailInfoCorrect = false;
                $scope.prospect = {
                    "titleCopy": null,
                    "deptCopy": null,
                    "nameCopy": null,
                    "roleCopy": null,
                    "phoneCopy": null,
                    "emailCopy": null,
                    "companyCopy": null,
                    "role": null
                }

                $scope.isEditFormEnable = function() {
                	if($scope.showNameForm == true || $scope.showTitleForm == true || $scope.showDeptForm == true || $scope.showPhoneForm == true || $scope.showEmailForm == true || $scope.showCompanyForm == true) {
                		return true;
                	} else {
                		return false;
                	}

                }


                var validateEmail = function(email) {
                    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                    return re.test(email);
                }

                $scope.expandasset = function () {
                    $scope.isAssetExpand = true;
                }
                $scope.collapseasset = function () {
                    $scope.isAssetExpand = false;
                }

                $scope.isExtraRibuttal = false;
                $scope.isCurrentQuestion = 1;
                $scope.isCurrentTab = 'Questions';
                $scope.isAddNote = false;
                $scope.showConfirmCode = false;
                $scope.emailRevealed = false;
                $scope.defEmailUnhide = false;

                $scope.showextrarubuttal = function () {
                    $scope.isExtraRibuttal = true;
                }

                $scope.hideextrarubuttal = function () {
                    $scope.isExtraRibuttal = false;
                }

                $scope.opentabs = function (tab) {
                	$scope.isAddNote = false;
                	filterTextTimeout = $timeout(function() {
                		$scope.isCurrentTab = tab;
                    }, 1000); // delay 250 ms

                    if (tab == 'Questions') {
                        $scope.isCurrentQuestion = 1;
                    }
                    if (tab == 'Confirm') {
                        if($scope.campaign.revealEmail){
                            $scope.emailRevealed = true;
                            $scope.defEmailUnhide = true;
                        }
                    }
                }

                $scope.revealEmail = function() {
                    $scope.emailRevealed = true;
                    $scope.defEmailUnhide = true;
                }

                $scope.slidequestion = function (tab) {

                    $scope.isCurrentQuestion = tab;
                }

                $scope.leftSlide = function () {

                    var i = $scope.isCurrentQuestion;
                    if (i == 1) {
                       // i = 3;
                    } else {
                        i--;

                    }
                    $scope.slidequestion(i);
                }

                $scope.rightSlide = function () {
                    console.log( $scope.campaign.questionAttributeMap.length);
                    var i = $scope.isCurrentQuestion;
                    if (i == $scope.totalQuestion) {
                        $scope.opentabs("Confirm");
                    } else {
                        i++;
                    }
                    $scope.slidequestion(i);
                }

                $scope.editShowNameInfo = function () {
	                    $scope.showNameForm = true;
                        $scope.prospect.nameCopy = $scope.prospectCall.prospect.name;
                        $scope.prospectCall.dispositionStatus = "";
                        $scope.prospectCall.subStatus = "";
                }

                $scope.editHideNameInfo = function () {
                    $scope.showNameForm = false;
                    $scope.prospect.nameCopy = $scope.prospectCall.prospect.name;
                    $scope.checkeditname = "yes";
                }
                $scope.editSaveNameInfo = function () {
                    if ($scope.prospect.nameCopy == "" || $scope.prospect.nameCopy == null || $scope.prospect.nameCopy == undefined) {
                        NotificationService.log('error','Please enter the name.');
                        return;
                    }
                    var name = $scope.prospect.nameCopy;
                    var namear = name.split(" ");
                    var tot = namear.length;
                    var lastname = namear[tot-1];
                    var firstname ="";
                    for(i=0;i<tot-1;i++){
                        if (i == 0) {
                            firstname = namear[i];
                        } else {
                            firstname = firstname+" "+namear[i];
                        }
                    }
                    $scope.showNameForm = false;
                    $scope.prospectCall.prospect.name = $scope.prospect.nameCopy;
                    $scope.prospectCall.prospect.lastName = lastname;
                    $scope.prospectCall.prospect.firstName = firstname;
                    $scope.checkeditname = "yes";
                }

                $scope.editShowPhoneInfo = function () {
	                	$scope.showPhoneForm = true;
                        $scope.prospect.phoneCopy = $scope.getCompletePhone();
                        $scope.prospectCall.dispositionStatus = "";
                        $scope.prospectCall.subStatus = "";
                }

                $scope.editHidePhoneInfo = function () {
                    $scope.showPhoneForm = false;
                    $scope.prospect.phoneCopy = $scope.getCompletePhone();
                    $scope.checkeditphone = "yes";
                }

                $scope.editSavePhoneInfo = function () {
                    // $scope.validatePhone();
                	// Hack - removed validation for now
                	$scope.showPhoneForm = false;
                    $scope.checkeditphone = "yes";
                    if ($scope.prospect.phoneCopy != 'XXX-XXX-XXXX' && $scope.prospect.phoneCopy != 'xxx-xxx-xxxx') {
                        $scope.prospectCall.prospect.phone = $scope.prospect.phoneCopy;
                        $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                        $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                    }
                }

                $scope.validatePhone = function () {
                	var regex1 = new RegExp(/^\d{3}-\d{3}-\d{4}$/);
                    var regex2 = new RegExp(/^\d{10}$/);
                    if (regex1.test($scope.prospect.phoneCopy)) {
                    	$scope.showPhoneForm = false;
                    	$scope.prospectCall.prospect.phone = $scope.prospect.phoneCopy;
                        $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                        // if agent modifies phone then adding it in originalHiddenPhone to store it in prospect.phone
                        $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                        $scope.checkeditphone = "yes";
                    }
                    else if (regex2.test($scope.prospect.phoneCopy)){
                    	var phoneCopy = $scope.prospect.phoneCopy;
                    	$scope.showPhoneForm = false;
                    	$scope.prospect.phoneCopy = phoneCopy.slice(0, 3) + '-' + phoneCopy.slice(3,6) + '-' + phoneCopy.slice(6,10);
                    	$scope.prospectCall.prospect.phone = $scope.prospect.phoneCopy;
                        $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                        // if agent modifies phone then adding it in originalHiddenPhone to store it in prospect.phone
                        $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                        $scope.checkeditphone = "yes";
                    }
                    else {
                    	$scope.showPhoneForm = true;
                		$scope.checkeditphone = "no";
                		NotificationService.log('error','Please enter phone in format xxx-xxx-xxxx OR xxxxxxxxxx');
                    }
                }

                $scope.showEmailmsg = false;
                $scope.showAlternateEmailInfo = function () {
                	$scope.isEmailInfoCorrect = true
                	$scope.checkeditemail = undefined;
                	$scope.showEmailmsg = false;
                	$scope.showEmailForm = false;
                	$scope.resetStatus();
                }

                $scope.editShowEmailInfo = function () {
                        $scope.showEmailForm = true;
                        $scope.prospect.emailCopy = $scope.prospectCall.prospect.email;
                        $scope.prospectCall.dispositionStatus = "";
                        $scope.prospectCall.subStatus = "";
                }

                $scope.editHideEmailInfo = function () {
                    $scope.showEmailForm = false;
                    $scope.prospect.emailCopy = $scope.prospectCall.prospect.email;
                    $scope.checkeditemail = "yes";
                }

                $scope.hideAlternateEmailInfo = function () {
                	$scope.resetStatus();
                	$scope.opentabs('Notes');
                }

                $scope.editSaveEmailInfo = function () {
                	if(validateEmail($scope.prospect.emailCopy)){
	                    $scope.showEmailForm = false;
	                    $scope.prospectCall.prospect.email = $scope.prospect.emailCopy;
	                    $scope.checkeditemail = "yes";
	                }else{
	                	NotificationService.log('error', 'Please enter a valid email address.');
	                }
                }

                $scope.editShowTitleInfo = function () {
                	$scope.showTitleForm = true;
                    $scope.prospect.titleCopy = $scope.prospectCall.prospect.title;
                    $scope.prospectCall.dispositionStatus = "";
                    $scope.prospectCall.subStatus = "";
                }

                $scope.editHideTitleInfo = function () {
                    $scope.showTitleForm = false;
                    $scope.prospect.titleCopy = $scope.prospectCall.prospect.title;
                    $scope.checkedittitle = "yes"
                }

                $scope.editHideDeptInfo = function () {
                    $scope.showDeptForm = false;
                    $scope.prospect.deptCopy = $scope.prospectCall.prospect.department;
                    $scope.checkedittitle = "yes"
                }

                $scope.editSaveTitleInfo = function () {
                    $scope.showTitleForm = false;
                    $scope.prospectCall.prospect.title = $scope.prospect.titleCopy;
                    $scope.checkedittitle = "yes"
                }

                $scope.editSaveDeptInfo = function () {
                    $scope.showDeptForm = false;
                    $scope.prospectCall.prospect.department = $scope.prospect.deptCopy;
                    $scope.checkeditdept = "yes"
                }

                $scope.editShowDeptInfo = function () {
                	$scope.showDeptForm = true;
                    $scope.prospect.deptCopy = $scope.prospectCall.prospect.department;
                    $scope.prospectCall.dispositionStatus = "";
                    $scope.prospectCall.subStatus = "";
                }

                $scope.editShowCompanyInfo = function () {
                	$scope.showCompanyForm = true;
                    $scope.prospect.companyCopy = $scope.prospectCall.prospect.company;
                    $scope.prospectCall.dispositionStatus = "";
                    $scope.prospectCall.subStatus = "";
                }

                $scope.editHideCompanyInfo = function () {
                    $scope.showCompanyForm = false;
                    $scope.prospect.companyCopy = $scope.prospectCall.prospect.company;
                    $scope.checkeditcompany = "yes"
                }

                $scope.editSaveCompanyInfo = function () {
                    $scope.showCompanyForm = false;
                    $scope.prospectCall.prospect.company = $scope.prospect.companyCopy;
                    $scope.checkeditcompany = "yes"
                }

                $scope.editShowRoleInfo = function () {
                	$scope.showRoleForm = true;
                    $scope.prospect.roleCopy = $scope.prospect.role;
                }

                $scope.editHideRoleInfo = function () {
                	$scope.showRoleForm = false;
                    $scope.prospect.roleCopy = $scope.prospect.role;
                    $scope.checkeditrole = "yes";
                }

                $scope.editSaveRoleInfo = function () {
                    $scope.showRoleForm = false;
                    $scope.prospect.role = $scope.prospect.roleCopy;
                    $scope.checkeditrole = "yes";
                }

                $scope.checkConfirmStatus = function(type){
                	if (!$scope.noTitleAvailable) {
                	   if ($scope.checkeditname == "yes" &&  $scope.checkeditcompany == "yes" &&  $scope.checkedittitle == "yes" && $scope.checkeditphone == "yes" && $scope.checkeditemail == "yes" && ($scope.assetName != "" || $scope.assetList.length < 1)){
                            if ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification') {
                                $scope.opentabs("Consent");
                            }
                 	   }
                	} else if (!$scope.noDepartmentAvailable) {
                		if ($scope.checkeditname == "yes" &&  $scope.checkeditcompany == "yes" &&  $scope.checkeditdept == "yes" && $scope.checkeditphone == "yes" && $scope.checkeditemail == "yes" ){
                            if ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification') {
                                $scope.opentabs("Consent");
                            }
                  	   }
                	} else {
                		if ($scope.checkeditname == "yes" &&  $scope.checkeditcompany == "yes" && $scope.checkeditphone == "yes" && $scope.checkeditemail == "yes" ){
                            if ($scope.campaign.type !== 'Lead Verification' && $scope.campaign.type !== 'LeadVerification') {
                                $scope.opentabs("Consent");
                            }
                  	   }
                	}

            	   if(type == 'Name') {
            		   $scope.showNameForm = false;
            	   } else if(type == 'Title') {
            		   $scope.showTitleForm = false;
            	   } else if(type == 'Phone') {
            		   $scope.showPhoneForm = false;
            		   $scope.showRoleForm = false;
            	   } else if(type == 'Email') {
            		   $scope.showEmailForm = false;
            	   } else if(type == 'Company') {
            		   $scope.showCompanyForm = false;
            	   } else if(type == 'Department') {
            		   $scope.showDeptForm = false;
            	   }

               }

                var checkQuestionValidation = function () {
                    var count = 0;
                    if ($scope.prospectCall != undefined && $scope.prospectCall != null && $scope.prospectCall.answers != undefined && $scope.prospectCall.answers != null) {
                        angular.forEach($scope.prospectCall.answers, function (answerValue, answerKey) {
                            if (answerValue.answer != '' && answerValue.answer != null && answerValue.answer != undefined) {
                                count = count + 1;
                            }
                        });
                        if (count == $scope.totalQuestion) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return true;
                }

               $scope.resetStatus = function () {
            	   $scope.prospectCall.dispositionStatus = undefined;
            	   $scope.prospectCall.subStatus = undefined;
            	   $scope.disposition = undefined;
            	   $scope.dialercodestatus = undefined;
               }

               $scope.changedisposition = function(index) {
                if(index > $scope.totalQuestion) {
                    $scope.opentabs("Confirm");
                } else {
                    $scope.isCurrentQuestion = index;
                }
              }

               $scope.successChecked = false;
               $scope.checkSuccessStatus = function(){
            	   // DATE:29/01/2017		REASON:Added email validation on success disposition submit for agent screen.
                //    if (!$scope.isMobileTablet() && $scope.originalEmail != null && $scope.originalEmail != undefined ) {
                //     $scope.prospectCall.prospect.email =
                //    }
                   if ($scope.originalEmail == null && !validateEmail($scope.prospectCall.prospect.email)){
	               		NotificationService.log('error', 'Please enter a valid email address.');
	               		return;
                       }
                    // Asset name is mandatory for success leads.
                    if ($scope.assetList.length > 1 && $scope.assetName == "") {
                        $scope.opentabs("Confirm");
                        NotificationService.log('error', 'Please select the asset for the prospect.');
                        $scope.prospectCall.dispositionStatus = null;
                        $scope.prospectCall.subStatus = null;
                        $scope.prospectCall.dispositionStatus = null;
                        return false;
                    }
            	   if(!$scope.noTitleAvailable && ($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes" || $scope.checkedittitle  != "yes")) {
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else if(!$scope.noDepartmentAvailable  && ($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes" || $scope.checkeditdept  != "yes")) {
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else if(!checkQuestionValidation()){
            		   $scope.opentabs("Questions");
            		   NotificationService.log('error', 'Please answer all questions first.');
            		   return false;
            	   } else if($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes"){
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else if(($scope.checkeditemail == "no" || $scope.checkeditemail == undefined) && ($scope.consentAffermation == "no" || $scope.consentAffermation == undefined)){
            		   $scope.opentabs("Consent");
            		   NotificationService.log('error', 'Please obtain consent first.');
            		   return false;
            	   } else if($scope.checkeditemail == "yes" && ($scope.consentAffermation == "no" || $scope.consentAffermation == undefined)){
             		   $scope.opentabs("Consent");
             		   NotificationService.log('error', 'Please obtain consent first.');
             		   return false;
             	   } else {
            		   $scope.prospectCall.dispositionStatus = "SUCCESS";
                       $scope.prospectCall.subStatus = "SUBMITLEAD";
                       $scope.disposition = "SUCCESS";
                       $scope.dialercodestatus = "SUBMITLEAD";
                       return true;
            	   }
                }

                $scope.successChecked = false;
               $scope.checkSuccessStatusForLeadVerificationCampaign = function(){
            	   // DATE:29/01/2017		REASON:Added email validation on success disposition submit for agent screen.
            	   if (!validateEmail($scope.prospectCall.prospect.email)){
	               		NotificationService.log('error', 'Please enter a valid email address.');
	               		return false;
                       }
                    // Asset name is mandatory for success leads.
                    if ($scope.assetList.length > 1 && $scope.assetName == "") {
                        $scope.opentabs("Confirm");
                        NotificationService.log('error', 'Please select the asset for the prospect.');
                        $scope.prospectCall.dispositionStatus = null;
                        $scope.prospectCall.subStatus = null;
                        $scope.prospectCall.dispositionStatus = null;
                        return false;
                    }
            	   if(!$scope.noTitleAvailable && ($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes" || $scope.checkedittitle  != "yes")) {
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else if(!$scope.noDepartmentAvailable  && ($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes" || $scope.checkeditdept  != "yes")) {
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else if(!checkQuestionValidation()){
                       $scope.opentabs("Questions");
            		   NotificationService.log('error', 'Please answer all questions first.');
            		   return false;
            	   } else if($scope.checkeditname != "yes" || $scope.checkeditphone != "yes" || $scope.checkeditcompany != "yes" || $scope.checkeditemail  != "yes"){
            		   $scope.opentabs("Confirm");
            		   NotificationService.log('error', 'Please confirm the Prospect Details first.');
            		   $scope.prospectCall.dispositionStatus = null;
                       $scope.prospectCall.subStatus = null;
                       $scope.prospectCall.dispositionStatus = null;
                       return false;
            	   } else {
                       $scope.submitLeadFlag = true;
            		   $scope.prospectCall.dispositionStatus = "SUCCESS";
                       $scope.prospectCall.subStatus = "SUBMITLEAD";
                       $scope.disposition = "SUCCESS";
                       $scope.dialercodestatus = "SUBMITLEAD";
                       return true;
            	   }
            	}



               $scope.resetAfterProspect = function() {
            	   $scope.muteStatus = false;
            	   	$scope.prospect.nameCopy = "";
					$scope.disposition = "";
					$scope.parent.note = "";
					$scope.checkeditrole = "";
					$scope.checkeditname = "";
					$scope.checkedittitle = "";
					$scope.checkeditdept = "";
					$scope.checkeditphone = "";
                    $scope.checkeditcompany = "";
                    $scope.assetName = "";
					$scope.tempProspectEmail = undefined;
					$scope.prospect.emailCopy = undefined;
					$scope.prospect.originalDepartment = undefined;
					$scope.prospect.originalTitle = undefined;
					$scope.showEmailmsg = false;
					$scope.checkeditemail = undefined;
					$scope.isEmailInfoCorrect = false;
					$scope.consentAffermation = "";
					$scope.dialercodestatus = "";
                    $scope.callLaterStatus = "";
                    $scope.callbackcode = "";
					$scope.callHistory = [];
					$scope.isAddNote = false;
					$scope.showNameForm = false;
					$scope.showTitleForm = false;
					$scope.showDeptForm = false;
					$scope.showPhoneForm = false;
					$scope.showRoleForm = false;
					$scope.isstatusdetail = false;
					$scope.showEmailForm = false;
					$scope.showCompanyForm = false;
					$scope.callOnHoldShow = true;
					$scope.prospectNote = [];
					$scope.isPhoneNumberConnected = false;
					angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                    	questAttrValue.qualifiedFlag = undefined;
                    });
                    if (angular.isDefined($scope.forceSubmitInterval)) {
                        $interval.cancel($scope.forceSubmitInterval); // cancel force auto submit interval
                        $scope.forceSubmitInterval = undefined;
                    }
                    $scope.indirectProspects = undefined;
                    $scope.previousAlternateProspects = []; // reset to disable alternate prospect button, if present
                    $scope.forceSubmittedProspectsCount = 0;
                    $scope.disableGlogalSubmit = false;
                    $scope.isGlobalForceSubmit = false;
                    $scope.globalDispositionModel = undefined;
                    if ($scope.alternateprospectmodal) {
                    	$scope.alternateprospectmodal.dismiss('cancel');
                    	$scope.alternateprospectmodal = undefined;
					}
				}

               $window.onbeforeunload = function (evt) {
                    $log.debug("Submitting status change to server with status offline");
                    var agentType = "";
                    if ($scope.agentType != undefined && $scope.agentType != null) {
                        agentType = $scope.agentType;
                    }
                    var twilioCallSid = "";
                    if ($scope.prospectCall != undefined && $scope.prospectCall != null && $scope.prospectCall.twilioCallSid != undefined && $scope.prospectCall.twilioCallSid != null) {
                        twilioCallSid = $scope.prospectCall.twilioCallSid;
                    }
                    if ($scope.agentIdleTimeInterval != null && $scope.agentIdleTimeInterval != undefined) {
                        $interval.cancel($scope.agentIdleTimeInterval);
                    }
                    agentOfflineStatusService.save({}, {
                        agentCommand: "BROWSER_CLOSED",
                        agentStatus: "OFFLINE",
                        campaignId: $scope.campaign.id,
                        agentType: agentType,
                        callSid: twilioCallSid,
                        usePlivo: true,
                        isConferenceCall: true
                    });
                    //    synchronousService("/spr/agent/agentcommand", "POST", data);
                    if ($scope.getCallControlProvider() == 'Plivo') {
                        $scope.refreshPlivo();
                    } else if ($scope.getCallControlProvider() == 'Telnyx') {
                        $scope.telnyxHangupCall();
                        $scope.refreshTelnyx();
                    } else if ($scope.getCallControlProvider() == 'Signalwire') {
                        $scope.refreshSignalwire();
                    }
               }

            // ##### ALTERNATE PROSPECTS SELECTION/SUBMIT #####
               $scope.showAlternateProspectModal = function () {
            	   $scope.alternateprospectmodal = $modal.open({
					   scope: $scope,
					   templateUrl: 'alternateprospectmodal.html',
					   controller: 'AlternateProspectController',
					   windowClass: 'alternateprospectmodal',
					   backdrop: 'static'
					});
               }

               $scope.previousAlternateProspects = [];
               $scope.forceSubmittedProspectsCount = 0;
               $scope.disableGlogalSubmit = false;
               $scope.isGlobalForceSubmit = false;
               $scope.globalDispositionModel = undefined;
               // $scope.isLastSubmitButton = false;
               // $scope.popupSubmitButtons = [];

               $scope.forceDispositionSelection = function(disposition) {
            	   if (disposition == 'FAILURE') {
            		   $scope.prospectCall.dispositionStatus = "FAILURE";
                       $scope.prospectCall.subStatus = "OTHER";
                       $scope.disposition = "FAILURE";
                       $scope.globalDispositionModel = "FAILURE";
                       $scope.dialercodestatus = "OTHER";
                       $scope.callbackcode = undefined;//reset callback code status if it is set
            	   } else if (disposition == 'DNCL') {
            		   $scope.prospectCall.dispositionStatus = "FAILURE";
                       $scope.prospectCall.subStatus = "DNCL";
                       $scope.disposition = "DNCL";
                       $scope.globalDispositionModel = "DNCL";
                       $scope.dialercodestatus = "DNCL";
                       $scope.callbackcode = undefined;//reset callback code status if it is set
            	   } else if (disposition == 'DIALERCODE') {
            		   $scope.prospectCall.dispositionStatus = "DIALERCODE";
                       $scope.prospectCall.subStatus = "ANSWERMACHINE";
                       $scope.disposition = "DIALERCODE";
                       $scope.globalDispositionModel = "DIALERCODE";
                       $scope.dialercodestatus = "ANSWERMACHINE";
                       $scope.callbackcode = undefined;//reset callback code status if it is set
            	   } else if (disposition == 'GATEKEEPER') {
            		   $scope.prospectCall.dispositionStatus = "FAILURE";
                       $scope.prospectCall.subStatus = "ANSWERMACHINE";
                       $scope.disposition = "FAILURE";
                       $scope.globalDispositionModel = "GATEKEEPER";
                       $scope.dialercodestatus = "ANSWERMACHINE";
                       $scope.callbackcode = undefined;//reset callback code status if it is set
                   }
               }

               $scope.alternateProspect = function(selectionState, indirectProspect, index) {
            	   if (selectionState == 'new') {
						$scope.showNameForm = true;
		               	$scope.showTitleForm = true;
		               	$scope.showDeptForm = true;
		               	$scope.showEmailForm = true;
		               	if (!$scope.prospectCall.prospect) {
		               		$scope.prospectCall.prospect = {};
		               	}
		           		$scope.prospectCall.prospect.firstName = "";
		               	$scope.prospectCall.prospect.lastName = "";
		               	$scope.prospectCall.prospect.name = "";
		               	$scope.prospectCall.prospect.title = "";
		               	$scope.prospectCall.prospect.department = "";
                        $scope.prospectCall.prospect.email = "";
                        $scope.prospectCall.prospect.linkedInURL = "";
		               	$scope.prospectCall.prospectCallId = null;
		               	$scope.prospectCall.multiProspect = $scope.multiProspectDetails;
					} else if (selectionState == 'old') {
						if (!$scope.prospectCall.prospect) {
		               		$scope.prospectCall.prospect = {};
		               	}
		           		$scope.prospectCall.prospect.firstName = indirectProspect.firstName;
		               	$scope.prospectCall.prospect.lastName = indirectProspect.lastName;
		               	$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.firstName.trim();
		               	if ($scope.prospectCall.prospect.lastName != null) {
		               		$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.name + " " + $scope.prospectCall.prospect.lastName.trim();
                           }
                        $scope.prospectCall.prospect.company = indirectProspect.company;
		               	$scope.prospectCall.prospect.title = indirectProspect.title;
                        $scope.prospectCall.prospect.email = indirectProspect.email;
                        $scope.prospectCall.prospect.department = indirectProspect.department;
                        // following values are not passed in an indirectProspect
//                        $scope.prospectCall.prospect.managementLevel = indirectProspect.managementLevel;
//                        $scope.prospectCall.prospect.industry = indirectProspect.industry;
//                        $scope.prospectCall.prospect.customAttributes.domain = indirectProspect.domain;
//                        if (indirectProspect.employeeCount == '' || indirectProspect.employeeCount == null || indirectProspect.employeeCount == undefined) {
//                            $scope.employeeCount = '';
//                        } else {
//                            $scope.employeeCount = indirectProspect.employeeCount;
//                        }
//                        if (indirectProspect.revenue == '' || indirectProspect.revenue == null || indirectProspect.revenue == undefined) {
//                            $scope.revenue = '';
//                        } else {
//                            $scope.revenue = indirectProspect.revenue;
//                        }
//                        $scope.employeeRangeChange($scope.employeeCount);
//                        $scope.revenueRangeChange($scope.revenue);
		               	$scope.prospectCall.prospectCallId = indirectProspect.prospectCallId;
		               	$scope.prospectCall.qualityBucket = indirectProspect.qualityBucket;
		               	$scope.prospectCall.qualityBucketSort = indirectProspect.qualityBucketSort;
                        $scope.prospectCall.multiProspect = indirectProspect.multiProspect;
                        $scope.isCustomFieldCollapsed = true;
                        $scope.prospectCall.prospect.customFields = indirectProspect.customFields;
                    }
                /* The below 3 lines are commented because agent can select the disposition status and submit the prospect after first time selection of prospect in alternate prospects.*/
            	   	// $scope.previousAlternateProspects.push(index);
            	   	// $scope.previousAlternateProspects.push('disableAll');
            	   	// $scope.disableGlogalSubmit = true;
//	               	$scope.forceSubmitProspect('old', indirectProspect);
//	               	$scope.prospectCall.indirectProspects[index].status = "SELECTED";
               }

               $scope.globalForceSubmitProspect = function() {
                   if ($scope.globalDispositionModel == undefined) {
                        NotificationService.log('error', 'Please select the disposition.');
            		    return;
                   }
                   $scope.disableGlogalSubmit = true;
            	   $scope.isGlobalForceSubmit = true;
                   $scope.previousAlternateProspects.push('disableAll');
            	   $scope.forceSubmitProspect('old', $scope.indirectProspects[0], 0);
                   $scope.isNewExtensionDialed = false;
               }

               $scope.forceSubmitProspect = function(selectionState, indirectProspect, index) {
            	   if (!$scope.disableGlogalSubmit && ($scope.dispositionModel[index] === null || $scope.dispositionModel[index] === undefined)) {
            		   NotificationService.log('error', 'Please select the disposition.');
            		   return;
            	   }
                   $scope.isNewExtensionDialed = false;
                   $scope.previousAlternateProspects.push(index);
            	   if (selectionState == 'old') {
						if (!$scope.prospectCall.prospect) {
		               		$scope.prospectCall.prospect = {};
		               	}
		           		$scope.prospectCall.prospect.firstName = indirectProspect.firstName;
		               	$scope.prospectCall.prospect.lastName = indirectProspect.lastName;
		               	$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.firstName.trim();
		               	if ($scope.prospectCall.prospect.lastName != null) {
		               		$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.name + " " + $scope.prospectCall.prospect.lastName.trim();
		               	}
		               	$scope.prospectCall.prospect.title = indirectProspect.title;
		               	$scope.prospectCall.prospect.department = indirectProspect.department;
		               	$scope.prospectCall.prospect.email = indirectProspect.email;
		               	$scope.prospectCall.prospectCallId = indirectProspect.prospectCallId;
		               	$scope.prospectCall.qualityBucket = indirectProspect.qualityBucket;
		               	$scope.prospectCall.qualityBucketSort = indirectProspect.qualityBucketSort;
                        $scope.prospectCall.multiProspect = indirectProspect.multiProspect;
					} else if (selectionState == 'new') {
						$scope.showNameForm = true;
		               	$scope.showTitleForm = true;
		               	$scope.showDeptForm = true;
		               	$scope.showEmailForm = true;
		               	if (!$scope.prospectCall.prospect) {
		               		$scope.prospectCall.prospect = {};
		               	}
		           		$scope.prospectCall.prospect.firstName = "";
		               	$scope.prospectCall.prospect.lastName = "";
		               	$scope.prospectCall.prospect.name = "";
		               	$scope.prospectCall.prospect.title = "";
		               	$scope.prospectCall.prospect.department = "";
                        $scope.prospectCall.prospect.email = "";
                        $scope.prospectCall.prospect.linkedInURL = "";
		               	$scope.prospectCall.prospectCallId = null;
		               	$scope.prospectCall.multiProspect = $scope.multiProspectDetails;
		               	return;
					}
            	   $scope.disableGlogalSubmit = true;
					$scope.previousCallStateTime = $scope.currentCallStateTime;
					$scope.currentCallStateTime = new Date().getTime();
					var agentname = user.firstName + " " + user.lastName;
                    var prospectNote = [];
                    /* do no add notes when force submit (so commented)*/

					// var noteObject = {
					// 	creationDate : new Date(),
					// 	text : $scope.prospectCall.dispositionStatus,
					// 	agentId : user.id,
					// 	agentName : agentname,
					// 	dispositionStatus : $scope.prospectCall.dispositionStatus,
					// 	subStatus : $scope.prospectCall.subStatus
					// };
                    // prospectNote.push(noteObject);

					// when agent recieves prospect; campaign is in power mode and campaign hidePhone field is true then hiding phone from agent. here adding phone number to prospect again if it is modified or not
					$scope.prospectCall.prospect.phone = $scope.prospect.originalHiddenPhone;
					var startTime = $scope.prospectCall.callStartTime.getTime();
                    var endTime = (new Date()).getTime();
                    var callDuration = Math.round((endTime - startTime) / 1000);
					var prospectCall = {
						prospectCallId : $scope.prospectCall.prospectCallId,
						prospect : $scope.prospectCall.prospect,
						twilioCallSid : $scope.prospectCall.twilioCallSid,
						callStartTime : $scope.prospectCall.callStartTime,
						// callDuration : callDuration,
						notes : prospectNote,
						answers : $scope.prospectCall.answers,
						deliveredAssetId : $scope.prospectCall.deliveredAssetId,
						dispositionStatus : $scope.prospectCall.dispositionStatus,
						subStatus : $scope.prospectCall.subStatus,
						campaignId : $scope.prospectCall.campaignId,
						agentId : user.id,
						callbackTimezone : $scope.callback.timeZone,
						callbackSelectedDate : $scope.callback.callbackDate,
						callbackSelectedHours : $scope.callback.callbackHours,
						callbackSelectedMin : $scope.callback.callbackMins,
						outboundNumber : $scope.prospectCall.outboundNumber,
						wrapupDuration : Math.round(($scope.currentCallStateTime - $scope.previousCallStateTime) / 1000),
						// prospectHandleDuration : callDuration,
						prospectVersion : $scope.prospectCall.prospectVersion,
						indirectProspects: $scope.prospectCall.indirectProspects,
						qualityBucket: $scope.prospectCall.qualityBucket,
						qualityBucketSort: $scope.prospectCall.qualityBucketSort,
						multiProspect: $scope.prospectCall.multiProspect,
						zoomCompanyUrl: $scope.prospectCall.zoomCompanyUrl,
                		zoomPersonUrl: $scope.prospectCall.zoomPersonUrl,
                		zoomValidated: $scope.prospectCall.zoomValidated,
                		leadDelivered: $scope.prospectCall.leadDelivered,
                		dataSlice: $scope.prospectCall.dataSlice,
                		machineDetectedIncrementer: $scope.prospectCall.machineDetectedIncrementer,
						isFinalSubmit: false
                    };
                    $scope.setAjaxWaiting();
					prospectCallService.save(prospectCall).$promise.then(function() {
                        console.log("prospect submitted successfully...");

                        // set parameters for agent wait time monitoring
                        $scope.prospectDisconnectedTime = new Date();
                        $scope.prospectSubmittedTime = new Date();
                        $scope.agentCallMonitoring(prospectCall);

                        $scope.resetAjaxWaiting();

                        // show prospect submitted msg if not global force submit
                        if (!$scope.isGlobalForceSubmit) {
                        	NotificationService.log('success', 'Prospect details submitted successfully.');
                        }

						$scope.forceSubmittedProspectsCount = $scope.forceSubmittedProspectsCount + 1;
						if ($scope.forceSubmittedProspectsCount === $scope.indirectProspects.length) {
							// $scope.showNameForm = true;
			               	// $scope.showTitleForm = true;
			               	// $scope.showDeptForm = true;
			               	// $scope.showEmailForm = true;
			               	if (!$scope.prospectCall.prospect) {
			               		$scope.prospectCall.prospect = {};
			               	}
			           		$scope.prospectCall.prospect.firstName = "";
			               	$scope.prospectCall.prospect.lastName = "";
			               	$scope.prospectCall.prospect.name = "";
			               	$scope.prospectCall.prospect.title = "";
			               	$scope.prospectCall.prospect.department = "";
                            $scope.prospectCall.prospect.email = "";
                            $scope.prospectCall.prospect.linkedInURL = "";
			               	$scope.prospectCall.prospectCallId = null;
			               	$scope.prospectCall.multiProspect = $scope.multiProspectDetails;
			               	$scope.alternateprospectmodal.dismiss('cancel');
			               	$scope.previousAlternateProspects.push('disableAll');
                            NotificationService.log('success', 'Prospect details submitted successfully.'); 
                            $scope.refreshData();  
						} else {
							$scope.prospectCall.prospect.firstName = "";
			               	$scope.prospectCall.prospect.lastName = "";
			               	$scope.prospectCall.prospect.name = "";
			               	$scope.prospectCall.prospect.title = "";
			               	$scope.prospectCall.prospect.department = "";
                            $scope.prospectCall.prospect.email = "";
                            $scope.prospectCall.prospect.linkedInURL = "";
						}
						if ($scope.isGlobalForceSubmit) {
							var newIndex = index + 1;
							if (newIndex < $scope.indirectProspects.length) {
								$scope.forceSubmitProspect('old', $scope.indirectProspects[newIndex], newIndex, true);
							} else {
								// show prospect submitted msg if global force submit
								NotificationService.log('success', 'Prospect details submitted successfully.');
								$scope.endCall();
								$scope.prospectSubmittedTimeOld = $scope.prospectSubmittedTime;
								$scope.prospectSubmittedTime = new Date();
							}
						}
						/*$scope.popupSubmitButtons.push(index + 1);
						if (index < $scope.indirectProspects.length) {
							$scope.replaceProspectDetails(index + 1);
						}
						if ((index + 1) === $scope.indirectProspects.length - 1) {
							$scope.isLastSubmitButton = true;
						}*/
					});
               }

               /*$scope.replaceProspectDetails = function(index) {
        		   var indirectProspect = $scope.indirectProspects[index];
            	   if (!$scope.prospectCall.prospect) {
	               		$scope.prospectCall.prospect = {};
	               	}
	           		$scope.prospectCall.prospect.firstName = indirectProspect.firstName;
	               	$scope.prospectCall.prospect.lastName = indirectProspect.lastName;
	               	$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.firstName.trim();
	               	if ($scope.prospectCall.prospect.lastName != null) {
	               		$scope.prospectCall.prospect.name = $scope.prospectCall.prospect.name + " " + $scope.prospectCall.prospect.lastName.trim();
	               	}
	               	$scope.prospectCall.prospect.title = indirectProspect.title;
	               	$scope.prospectCall.prospect.department = indirectProspect.department;
	               	$scope.prospectCall.prospect.email = indirectProspect.email;
	               	$scope.prospectCall.prospectCallId = indirectProspect.prospectCallId;
	               	$scope.prospectCall.qualityBucket = indirectProspect.qualityBucket;
	               	$scope.prospectCall.qualityBucketSort = indirectProspect.qualityBucketSort;
	               	$scope.prospectCall.multiProspect = indirectProspect.multiProspect;
               }*/

			// ##### ALTERNATE PROSPECTS SELECTION/SUBMIT #####


               // ########## RESEARCH_AND_DIAL ##########

               // Dropdown menu for prospect edit
               $scope.leadDetailsMapping = {};
               if (attributeMappings.INDUSTRY) {
            	   $scope.leadDetailsMapping.industries = [];
            	   angular.forEach(attributeMappings.INDUSTRY.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   $scope.leadDetailsMapping.industries.push(object);
            		   }
            	   });
            	   $scope.leadDetailsMapping.industries.sort(function(obj1, obj2) {
            		   var x = obj1.label.toLowerCase();
            		   var y = obj2.label.toLowerCase();
            		   if (x < y) {return -1;}
            		   if (x > y) {return 1;}
            		   return 0;
            	   });
               }
               if (attributeMappings.DEPARTMENT) {
            	   $scope.leadDetailsMapping.departments = [];
            	   angular.forEach(attributeMappings.DEPARTMENT.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   $scope.leadDetailsMapping.departments.push(object);
            		   }
            	   });
            	   $scope.leadDetailsMapping.departments.sort(function(obj1, obj2) {
            		   var x = obj1.label.toLowerCase();
            		   var y = obj2.label.toLowerCase();
            		   if (x < y) {return -1;}
            		   if (x > y) {return 1;}
            		   return 0;
            	   });
               }
               if (attributeMappings.REVENUE) {
            	   $scope.leadDetailsMapping.revenueRange = [];
            	   angular.forEach(attributeMappings.REVENUE.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   $scope.leadDetailsMapping.revenueRange.push(object);
            		   }
            	   });
               }
               if (attributeMappings.EMPLOYEE) {
            	   $scope.leadDetailsMapping.employeeRange = [];
            	   angular.forEach(attributeMappings.EMPLOYEE.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   $scope.leadDetailsMapping.employeeRange.push(object);
            		   }
            	   });
               }
               if (attributeMappings.MANAGEMENT_LEVEL) {
            	   $scope.leadDetailsMapping.managementLevel = [];
            	   angular.forEach(attributeMappings.MANAGEMENT_LEVEL.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   if(object.label == 'MANAGER') {
            				   newObject = {
            						'label':'Manager',
            						'value':'Manager'
            				   }
							} else if(object.label == 'Non Management') {
								newObject = {
									'label':'Non-Manager',
									'value':'Non-Manager'
								}
							} else if(object.label == 'DIRECTOR') {
								newObject = {
									'label':'Director',
									'value':'Director'
								}
							} else if(object.label == 'VP_EXECUTIVES') {
								newObject = {
									'label':'VP-Level',
									'value':'VP-Level'
								}
							} else if(object.label == 'C_EXECUTIVES') {
								newObject = {
									'label':'C-Level',
									'value':'C-Level'
								}
							} else {
								newObject = {
									'label': object.label,
									'value': object.value
								}
							}
            			   $scope.leadDetailsMapping.managementLevel.push(newObject);
            		   }
            	   });
            	   $scope.leadDetailsMapping.managementLevel.sort(function(obj1, obj2) {
            		   var x = obj1.label.toLowerCase();
            		   var y = obj2.label.toLowerCase();
            		   if (x < y) {return -1;}
            		   if (x > y) {return 1;}
            		   return 0;
            	   });
               }
               if (attributeMappings.VALID_COUNTRY) {
            	   $scope.leadDetailsMapping.countries = [];
            	   angular.forEach(attributeMappings.VALID_COUNTRY.pickList, function(object, index) {
            		   if (object.value.toLowerCase() != 'all') {
            			   if (object.value == 'USA') {
            				   object.label = 'United States';
            				   object.value = 'United States';
                    		   $scope.leadDetailsMapping.countries.push(object);
                    	   } else {
                    		   $scope.leadDetailsMapping.countries.push(object);
                    	   }
                       }
                       $scope.leadDetailsMapping.countries.sort(function(obj1, obj2) {
                            var x = obj1.label.toLowerCase();
                            var y = obj2.label.toLowerCase();
                            if (x < y) {return -1;}
                            if (x > y) {return 1;}
                            return 0;
                       });
                   });
               }

               $scope.showResearchAndDialProspectModal = function (currentProspectType) {
                   // don't open popup modal if call is disconnected.
                   $scope.showLinkedinUrl = true;
                   $scope.search = false;
                       $scope.prospectDTOList = [];
                       $scope.similarProspectList = [];
            	   if ($scope.parent.state == 4) {
            		   NotificationService.log('error', 'Call is disconnected.');
            	   } else {
                       if (currentProspectType == 'RESEARCH') {
                            $scope.prospectCall = {};
                       }
            		   if (!$scope.prospectCall) {
            			   $scope.prospectCall = {};
            		   }
            		   $scope.prospectCall.campaignId = $scope.campaign.id;
            		   $scope.prospectCall.agentId = user.id;
            		   $scope.prospectCall.partnerId = user.organization;
            		   if (!$scope.prospectCall.prospect) {
		               		$scope.prospectCall.prospect = {};
		               }
            		   if (!$scope.prospectCall.prospect.customAttributes) {
            			   $scope.prospectCall.prospect.customAttributes = {};
            		   }
            		//    if (!$scope.prospectCall.prospect.country) {
                	// 	   $scope.prospectCall.prospect.country = 'United States';
                	// 	   $scope.selectedCountry = 'USA';
                    //    }
                        if ($scope.campaign.isLeadIQ) {
                            $scope.openLeadIQSearProspectModal();
                        } else {
                            if ($scope.campaign.type == 'Prospecting') {
                                $scope.showProspectingModal();
                            } else {
                                $scope.openResearchAndDialProspectModal();
                            }
                        }
            	   }
               }

                $scope.openLeadIQSearProspectModal = function () {
                    $scope.searchprospectmodal = $modal.open({
                        scope: $scope,
                        templateUrl: 'researchanddialprospectleeadiqmodal.html',
                        controller: 'ResearchAndDialProspectLeadIQController',
                        windowClass: 'researchanddialprospectleeadiqmodal',
                        backdrop: 'static'
                    });
                }

                $scope.openResearchAndDialProspectModal = function () {
                    $scope.researchanddialprospectmodal = $modal.open({
                        scope: $scope,
                        templateUrl: 'researchanddialprospectmodal.html',
                        controller: 'ResearchAndDialProspectController',
                        windowClass: 'researchanddialprospectmodal',
                        backdrop: 'static'
                    });
                }

                $scope.showSubmitProspectModal = function (currentProspectType) {
                    // don't open popup modal if call is disconnected.
                        if (!$scope.prospectCall) {                                                                                                                                                                                 
                            $scope.prospectCall = {};
                        }
                        $scope.prospectCall.campaignId = $scope.campaign.id;
                        $scope.prospectCall.agentId = user.id;
                        $scope.prospectCall.partnerId = user.organization;
                        if ($scope.prospectCall.prospect == null && $scope.prospectCall.prospect == undefined) {
                                $scope.prospectCall.prospect = {};
                        }
                        if ($scope.prospectCall.prospect.customAttributes == undefined && $scope.prospectCall.prospect.customAttributes == null) {
                            $scope.prospectCall.prospect.customAttributes = {};
                        } else {
                            $scope.setCustomAttributes();
                        }
                        if ($scope.agentType == 'MANUAL') {
                            $scope.prospectCall.prospect.stateCode = "";
                        }
                        if ($scope.campaign.enableStateCodeDropdown) {
                            getStateDetailService.get({countryName:$scope.prospectCall.prospect.country}).$promise.then(function(data) {
                                $scope.stateNames = data;
                                $scope.submitleadprospectmodal = $modal.open({
                                    scope: $scope,
                                    templateUrl: 'submitleadprospectmodal.html',
                                    controller: 'SubmitleadprospectmodalController',
                                    windowClass: 'submitleadprospectmodal',
                                    backdrop: 'static'
                                 });
                            })
                        } else {
                            $scope.submitleadprospectmodal = $modal.open({
                                scope: $scope,
                                templateUrl: 'submitleadprospectmodal.html',
                                controller: 'SubmitleadprospectmodalController',
                                windowClass: 'submitleadprospectmodal',
                                backdrop: 'static'
                             });
                        }
                }

             $scope.showCreateManualProspectModel = function () {
                            $scope.researchanddialprospectmodal = $modal.open({
                                scope: $scope,
                                templateUrl: 'prospectListToSelectmodal.html',
                                controller: 'LeadIQListController',
                                windowClass: 'prospectListToSelectmodal',
                                backdrop: 'static'
                            });
             }

            $scope.showProspectingModal = function () {
                $scope.prospectingModal = $modal.open({
                    scope: $scope,
                    templateUrl: 'campaignTypeProspectingModal.html',
                    controller: 'campaignTypeProspectingModalController',
                    windowClass: 'campaignTypeProspectingModal',
                    backdrop: 'static'
                    });
            }

            $scope.showNewIndirectProspectModal = function () {
                $scope.newindirectprospectmodal = $modal.open({
                    scope: $scope,
                    templateUrl: 'newindirectprospectmodal.html',
                    controller: 'NewIndirectProspectController',
                    windowClass: 'newindirectprospectmodal',
                    backdrop: 'static'
                });
            }

            $scope.showCampaignModeChangeModal = function () {
                $scope.campaignModeChangeModal = $modal.open({
                    scope: $scope,
                    templateUrl: 'campaignModeChangeModal.html',
                    controller: 'CampaignModeChangeController',
                    windowClass: 'newindirectprospectmodal',
                    backdrop: 'static'
                });
            }

            $scope.setCampaignModeChange = function () {
                $scope.campaignModeChangeModal.close();
            }

            $scope.isModalOpened = false;
            $scope.isCampaignSettingsChange = false;
            Pusher.subscribe(user.id, 'campaign_settings_change', function (agentType) {
                $log.debug("Received a campaign_settings_change");
                if ($scope.prospectCall != undefined && Object.keys($scope.prospectCall).length !== 0) {
                    $scope.isCampaignSettingsChange = true;
                } else {
                	//check if already modal has open
                    if ($scope.campaignSettingsChangeModal == undefined) {
                        $scope.campaignSettingsChange();
                    }
                }
            });
            $scope.campaignSettingsChange = function() {
            	$rootScope.$broadcast("onChangeStatus", 'REQUEST_OFFLINE');
                $scope.showCampaignSettingsChangeModal();
            }
            $scope.showCampaignSettingsChangeModal = function () {
            	$scope.isModalOpened = true;
                $scope.campaignSettingsChangeModal = $modal.open({
                    scope: $scope,
                    templateUrl: 'campaignSettingsChangeConfirmModal.html',
                    controller: CampaignSettingsChangeModalInstanceCtrl,
                    backdrop: 'static'
                });
            }
            var CampaignSettingsChangeModalInstanceCtrl = function ($scope, $modalInstance) {
                $scope.setCampaignSettingsChange = function () {
                    $modalInstance.dismiss('cancel');
	                $scope.$parent.loading = true;
	                $scope.isModalOpened = false;
	                $state.go('agentsplash');
	                $state.go('agentsplash');
                }
            }

            $scope.addNewIndirectProspect = function () {
                $scope.setAjaxWaiting();
                var  prospectCall = {};
                $scope.setAjaxWaiting();
                prospectCall.prospect = {};
                prospectCall.prospect.customAttributes = {};
                prospectCall.agentId = user.id;
                prospectCall.campaignId = $scope.prospectCall.campaignId;
                prospectCall.partnerId = $scope.prospectCall.partnerId;
                // seperate FirstName and LastName
                prospectCall.prospect.name = $scope.newIndirectProspect.name;
                seperateFirstNameLastName($scope.newIndirectProspect.name, prospectCall.prospect);
                prospectCall.prospect.title = $scope.newIndirectProspect.title;
                prospectCall.prospect.email = $scope.newIndirectProspect.email;
                prospectCall.prospect.department = $scope.newIndirectProspect.department;
                prospectCall.prospect.managementLevel = $scope.newIndirectProspect.managementLevel;
                prospectCall.prospect.phone = $scope.prospect.originalHiddenPhone;
                prospectCall.prospect.company = $scope.prospectCall.prospect.company;
                prospectCall.prospect.country = $scope.prospectCall.prospect.country;
                prospectCall.prospect.stateCode = $scope.prospectCall.prospect.stateCode;
                prospectCall.prospect.timeZone = $scope.prospectCall.prospect.timeZone;
                prospectCall.prospect.companyPhone = $scope.prospectCall.prospect.companyPhone;
                prospectCall.prospect.directPhone = $scope.prospectCall.prospect.directPhone;
                prospectCall.prospect.customAttributes.domain = $scope.prospectCall.prospect.customAttributes.domain;
                prospectCall.twilioCallSid = $scope.prospectCall.twilioCallSid;
                if ($scope.campaign.enableCustomFields && $scope.prospectCall.prospect.customFields != null && $scope.prospectCall.prospect.customFields != undefined) {
                    prospectCall.prospect.customFields = $scope.prospectCall.prospect.customFields;
                } else if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                    prospectCall.prospect.customFields = angular.copy($scope.defaultCustomFields);
                }

                if (!validateIndirectProspectFields(prospectCall.prospect)) {
                    $scope.resetAjaxWaiting();
                    return;
                }
                researchAnDialService.saveProspectDetails(prospectCall).$promise.then(function(prospectCallDTO) {
                    NotificationService.log('success', 'Prospect details saved successfully.');
                    var indirectProspect = {};
                    $scope.resetAjaxWaiting();
                    indirectProspect.firstName = prospectCallDTO.prospect.firstName;
                    indirectProspect.lastName = prospectCallDTO.prospect.lastName;
                    indirectProspect.title = prospectCallDTO.prospect.title;
                    indirectProspect.department = prospectCallDTO.prospect.department;
                    indirectProspect.managementLevel = prospectCallDTO.prospect.managementLevel;
                    indirectProspect.company = prospectCallDTO.prospect.company;
                    indirectProspect.email = prospectCallDTO.prospect.email;
                    indirectProspect.campaignId = prospectCallDTO.campaignId;
                    indirectProspect.prospectCallId = prospectCallDTO.prospectCallId;
                    indirectProspect.qualityBucket = prospectCallDTO.qualityBucket;
                    indirectProspect.qualityBucketSort = prospectCallDTO.qualityBucketSort;
                    indirectProspect.multiProspect = prospectCallDTO.multiProspect;
                    if ($scope.campaign.enableCustomFields && prospectCallDTO.prospect.customFields != null && prospectCallDTO.prospect.customFields != undefined) {
                        indirectProspect.customFields = prospectCallDTO.prospect.customFields;
                    } else if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                        indirectProspect.customFields = angular.copy($scope.defaultCustomFields);
                    }
                    $scope.indirectProspects.push(indirectProspect);
                    $scope.newindirectprospectmodal.dismiss('cancel');

                    /* START :- Below lines are added because revenue and employee count does not bind previous value in submit lead modal. */
                    $scope.revenue = '';
                    $scope.employeeCount = '';
                    $scope.prospectCall.prospect.customAttributes = {};
                    /* END */
                    $scope.newIndirectProspect = {};
                    $scope.dispositionModel = []; // array for popup disposition binding
                    angular.forEach(prospectCall.indirectProspects, function(value, key) {
                        $scope.indirectProspects.push(value);
                        $scope.dispositionModel[key] = undefined;
                    });
                }, function (error) {
                    $scope.resetAjaxWaiting();
               })
            }

            var seperateFirstNameLastName = function(name, prospect) {
                if (name == undefined) {
					return;
                }
                var firstname = "";
        		var lastname = "";
        		name = name.replace('\t', " ");
        		var tempName = name.split(" ");
                var tot = tempName.length;
                if (tot > 0) {
                	for (i = 0; i < tot; i++) {
						if (i == 0) {
							firstname = tempName[0];
						} else {
							if (lastname == '') {
								lastname = tempName[i];
							} else {
								lastname = lastname + " " + tempName[i];
							}
						}
            		}
				}
                firstname = firstname.replace(/[-&[\]/\\`!|@^_=;#,+()$~%.'":*?<>{}\r\n\t]/g,'');
                lastname = lastname.replace(/[-&[\]/\\`!|@^_=;#,+()$~%.'":*?<>{}\r\n\t]/g,'');
                prospect.firstName = firstname.trim();
                prospect.lastName = lastname.trim();
            }

            var validateIndirectProspectFields = function(prospect) {
                if (prospect.name == null || prospect.name == undefined || prospect.name == '') {
                    NotificationService.log('error', 'Please enter name.');
                    return false;
                }
                if (prospect.title == null || prospect.title == undefined || prospect.title == '') {
                	NotificationService.log('error', 'Please enter title.');
                	return false;
                }
                return true;
            }

               $scope.employeeRangeChange = function (employeeCount) {
            	   if (employeeCount != null && employeeCount != undefined && employeeCount != '') {
						if (employeeCount == '1-5') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 1;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 5;
						} else if (employeeCount == '5-10') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 5;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 10;
						} else if (employeeCount == '10-20') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 10;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 20;
						} else if (employeeCount == '20-50') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 20;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 50;
						} else if (employeeCount == '50-100') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 50;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 100;
						} else if (employeeCount == '100-250') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 100;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 250;
						} else if (employeeCount == '250-500') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 250;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 500;
						} else if (employeeCount == '500-1000') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 500;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 1000;
						} else if (employeeCount == '1000-5000') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 1000;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 5000;
						} else if (employeeCount == '5000-10000') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 5000;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 10000;
						} else if (employeeCount == 'Over 10000') {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 10000;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 15000;
						} else {
							$scope.prospectCall.prospect.customAttributes.minEmployeeCount = 0;
		            		$scope.prospectCall.prospect.customAttributes.maxEmployeeCount = 0;
						}
            	   } else {
                            $scope.prospectCall.prospect.customAttributes.minEmployeeCount = null;
                            $scope.prospectCall.prospect.customAttributes.maxEmployeeCount = null;
                   }
            	}

            	$scope.revenueRangeChange = function (revenue) {
            		if (revenue != null && revenue != undefined && revenue != '') {
						if (revenue == '0-$10M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 0;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 10000000;
						} else if (revenue == '$5M-$10M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 5000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 10000000;
						} else if (revenue == '$10M-$25M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 10000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 25000000;
						} else if (revenue == '$25M-$50M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 25000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 50000000;
						} else if (revenue == '$50M-$100M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 50000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 100000000;
						} else if (revenue == '$100M-$250M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 100000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 250000000;
						} else if (revenue == '$250M-$500M') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 250000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 500000000;
						} else if (revenue == '$500M-$1B') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 500000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 1000000000;
						} else if (revenue == '$1B-$5B') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 1000000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 5000000000;
						} else if (revenue == 'More than $5Billion') {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 5000000000;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 10000000000;
						} else {
							$scope.prospectCall.prospect.customAttributes.minRevenue = 0;
		            		$scope.prospectCall.prospect.customAttributes.maxRevenue = 0;
						}
					}
                }

               $scope.saveSelectedProspect = function (selectedProspect) {
                           $scope.selectedFromTableLeadIQ = true;
                           researchAnDialService.leadIQFetchProspect({sourceId: selectedProspect.sourceId}).$promise.then(function (data) {
                               $scope.prospectCall.prospect = data;
                               $scope.prospect.nameCopy = selectedProspect.name;
                               $scope.isLeadIQProspect = true;
                               // set prospect received time for agent call monitoring
                               $scope.prospectReceivedTime = new Date();
                               if ($scope.campaign.type == 'Prospecting') {
                                   $scope.leadIqOriginalEmail = "********@******";
                                   // document.getElementById('params_contact_phone_number_2').value='XXX-XXX-XXXX';
                                   // $scope.leadIqOriginalPhone = "";
                                   $scope.researchanddialprospectmodal.close();
                                   $scope.showProspectingModal();
                               } else {
                                   $scope.saveAndDialProspect();
                               }
                           }, function (error) {
                               NotificationService.log('error', 'An error occurred while fetching prospect.');
                           })
                };

                $scope.saveAndDialProspect = function () {
                    if ($scope.campaign.type == 'Prospecting') {
                        $scope.researchanddialprospectmodal.close();
                        $scope.showProspectingModal();
                    } else {
                        $scope.hidePhoneLeadIQ = false;
                        if ($scope.parent.status == 'ONLINE' && ($scope.parent.state == 1 || $scope.parent.state == 2)) {
                            let prospectPhone = "";
                            if ($scope.selectedFromTableLeadIQ) {
                                $scope.selectedFromTableLeadIQ = false;
                                if(document.getElementById('params_contact_phone_number_1') != null && document.getElementById('params_contact_phone_number_1').value != undefined && document.getElementById('params_contact_phone_number_1').value != '' && document.getElementById('params_contact_phone_number_1').value != null){
                                    prospectPhone = document.getElementById('params_contact_phone_number_1').value;
                                }else{
                                      prospectPhone = $scope.prospectCall.prospect.phone;
                                }
                                if(document.getElementById('rnd-title') != null && document.getElementById('rnd-title').value != undefined && document.getElementById('rnd-title').value != '' && document.getElementById('rnd-title').value != null){
                                   $scope.prospectCall.prospect.title = document.getElementById('rnd-title').value;
                                }
                                if(document.getElementById('params_email') != null && document.getElementById('params_email').value != undefined && document.getElementById('params_email').value != ''){
                                    $scope.prospectCall.prospect.email = document.getElementById('params_email').value;
                                }
                                //prospectPhone = document.getElementById('params_contact_phone_number_1').value;
                            } else {
                                prospectPhone = document.getElementById('params_contact_phone_number_1').value;
                                if (document.getElementById('params_email')) {
                                	$scope.prospectCall.prospect.email = document.getElementById('params_email').value;
								}
                            }

                            if (!prospectPhone.startsWith("+")) {
                                NotificationService.log('error', 'Please enter telephone number along with the ISD code. Example: +15036483216');
                                return;
                            } else {
                                $scope.prospectCall.prospect.phone = "+" + prospectPhone.replace(/\D/g,'');
                                //$scope.prospectCall.prospect.phone = prospectPhone.replace(/[-&[\]/\s\`!|@^_=;#,()$~%.'":*?<>{}]/g,'');
                                $scope.phoneNumber = $scope.prospectCall.prospect.phone;

                               //  $scope.prospectCall.prospect.phone = $scope.phoneNumber;
                            }
                            // $scope.prospectCall.prospect.phone = $scope.campaignCountryCode + $scope.phoneNumber;
                            // if($scope.campaignCountry == undefined) {
                            // 	$scope.changeProspectCountry();
                            // } else if ($scope.campaignCountry == "USA") {
                            //     $scope.prospectCall.prospect.country = "United States";
                            // }
                            let campCountry = localStorage.getItem("country");
                            if ($scope.prospectCall.prospect.country == null || $scope.prospectCall.prospect.country == "") {
                                if (campCountry != null) {
                                    $scope.prospectCall.prospect.country = campCountry;
                                } else {
                                    $scope.prospectCall.prospect.country = "United States";
                                }
                            }
                            // $scope.prospect.nameCopy = $scope.prospect
                            if (!validateProspectFields()) {
                                return;
                            }
                            // if ($scope.prospectCall.prospect.stateCode == null || $scope.prospectCall.prospect.stateCode == "") {
                            $scope.prospectCall.prospect.stateCode = "";
                            // }
                            if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                                $scope.prospectCall.prospect.customFields = angular.copy($scope.defaultCustomFields);
                            }
                            $scope.setAjaxWaiting();
                            researchAnDialService.saveProspectDetails($scope.prospectCall).$promise.then(function(prospectCallDTO) {
                                $scope.resetAjaxWaiting();
                                $scope.isPhoneNumberConnected = false;
                                NotificationService.log('success', 'Prospect details saved successfully.');
                                // set prospect received time for agent call monitoring
                                $scope.prospectReceivedTime = new Date();
                                $scope.showCreateLink = false;
                                $scope.showAlternateLink = true;
                                $scope.prospectCall = prospectCallDTO;
                                if (($scope.prospectCall.prospect.dataSource != null && $scope.prospectCall.prospect.dataSource != undefined && $scope.prospectCall.prospect.dataSource.includes("Xtaas Data Source")) || ($scope.prospectCall.prospect.source != null && $scope.prospectCall.prospect.source != undefined && ($scope.prospectCall.prospect.source.includes("ZOOINFO") || $scope.prospectCall.prospect.source.includes("LeadIQ") || $scope.prospectCall.prospect.source.includes("INSIDEVIEW")))) {
                                    $scope.emailRevealed = false;
                                    $scope.defEmailUnhide = false;
                                } else {
                                    $scope.defEmailUnhide = true;
                                }
                                if ($scope.searchprospectmodal != null && $scope.searchprospectmodal != undefined) {
                                    $scope.searchprospectmodal.dismiss('cancel');
                                }
                            if (!$scope.leadIQEmail && $scope.isLeadIQProspect && $scope.campaign.hideEmail && !$scope.campaign.revealEmail) {
                                // $scope.hideEmailAddress();
                                $scope.originalEmail = $scope.prospectCall.prospect.email;
                                $scope.prospectCall.prospect.email = "********@******";
                            } else {
                                $scope.emailHide = $scope.prospectCall.prospect.email;
                            }
                            if ($scope.campaign.enableCustomFields && ($scope.prospectCall.prospect.customFields == null || $scope.prospectCall.prospect.customFields == undefined)) {
                                if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                                    $scope.prospectCall.prospect.customFields = angular.copy($scope.defaultCustomFields);
                                }
                            }
                            $scope.prospectCall.prospect.prospectType = 'MANUAL';
                            $scope.setCustomAttributes($scope.prospectCall);
                            $scope.currentCallStateTime = new Date().getTime();
                            $scope.initialCallStateTime =  $scope.currentCallStateTime;
                            $scope.parent.numberToDial = $scope.prospectCall.prospect.phone;
                            $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                            $scope.callHistory = $scope.prospectCall.notes;
                            $scope.prospectNote = [];
                            if ($scope.campaign.isLeadIQ && $scope.isLeadIQProspect && $scope.campaign.hidePhone) {
                                $scope.parent.numberToDial = "xxx-xxx-xxxx";
                                $scope.prospect.originalHiddenPhone = $scope.prospectCall.prospect.phone;
                                $scope.prospectCall.prospect.phone = "xxx-xxx-xxxx";
                                $scope.hidePhoneLeadIQ = true;
                            }
                            $scope.isLeadIQProspect = false;
                            angular.forEach($scope.prospectCall.notes,function(note,index) {
                                $scope.prospectNote.push(note);
                            });
                            $scope.prospectCall.answers = [];
                            angular.forEach($scope.campaign.questionAttributeMap, function(questAttrValue, questAttrKey) {
                                var answer = {
                                        question: questAttrValue.questionSkin,
                                        sequence: questAttrValue.sequence
                                }
                                $scope.prospectCall.answers.push(answer);
                            });
                            // change state to 2 - Propsect Allocated
                            $scope.parent.state = 2;
                            if ($scope.currentProspectType != undefined && $scope.currentProspectType != 'RESEARCH') {
                                researchAnDialService.registerCall({callsid: $scope.prospectCall.twilioCallSid}, $scope.prospectCall).$promise.then(function(registerCallResponse) {
                                    researchAnDialService.allocateAgent({
                                    	callsid: $scope.prospectCall.twilioCallSid,
                                    	dialerMode: $scope.campaign.dialerMode,
                                    	agentType: $scope.agentType
                                    }).$promise.then(function(allocateAgentResponse) {
                                        $scope.researchanddialprospectmodal.close();
                                        $scope.connectCall();
                                    });
                                });
                            } else {
                            	if ($scope.campaign.dialerMode == 'RESEARCH' || ($scope.campaign.dialerMode == 'HYBRID' && $scope.agentType == 'MANUAL')) {
	                        		researchAnDialService.registerCall({callsid: $scope.prospectCall.twilioCallSid}, $scope.prospectCall).$promise.then(function(registerCallResponse) {
	                                    researchAnDialService.allocateAgent({
	                                    	callsid: $scope.prospectCall.twilioCallSid,
	                                    	dialerMode: $scope.campaign.dialerMode,
	                                    	agentType: $scope.agentType
	                                    }).$promise.then(function(allocateAgentResponse) {
                                            if ($scope.researchanddialprospectmodal != undefined && $scope.researchanddialprospectmodal != null) {
                                                $scope.researchanddialprospectmodal.close();
                                            }
	                                    });
	                                });
								}
                            	if ($scope.indirectProspects == undefined) {
                            		$scope.indirectProspects = [];
								}
                                var obj = {};
                                obj.firstName = $scope.prospectCall.prospect.firstName;
                                obj.lastName = $scope.prospectCall.prospect.lastName;
                                obj.managementLevel = $scope.prospectCall.prospect.managementLevel;
                                obj.department = $scope.prospectCall.prospect.department;
                                obj.employeeCount = $scope.employeeCount;
                                obj.revenue = $scope.revenue;
                                obj.title = $scope.prospectCall.prospect.title;
                                obj.department = $scope.prospectCall.prospect.department;
                                obj.company = $scope.prospectCall.prospect.company;
                                obj.industry = $scope.prospectCall.prospect.industry;
                                obj.email = $scope.prospectCall.prospect.email;
                                obj.campaignId = $scope.prospectCall.campaignId;
                                obj.prospectCallId = $scope.prospectCall.prospectCallId;
                                obj.qualityBucket = $scope.prospectCall.qualityBucket;
                                obj.qualityBucketSort = $scope.prospectCall.qualityBucketSort;
                                obj.multiProspect = $scope.prospectCall.multiProspect;
                                obj.domain = $scope.prospectCall.prospect.customAttributes.domain;
                                if ($scope.campaign.enableCustomFields && $scope.defaultCustomFields != null && $scope.defaultCustomFields != undefined) {
                                    obj.customFields = angular.copy($scope.defaultCustomFields);
                                }
                                $scope.indirectProspects.push(obj);
                                $scope.dispositionModel = []; // array for popup disposition binding
                                if ($scope.researchanddialprospectmodal != undefined && $scope.researchanddialprospectmodal != null) {
                                    $scope.researchanddialprospectmodal.close();
                                }
                                $scope.showAlternateProspectModal(); // open alternate prospect popup
                            }
            		   }, function (error) {
                           // $scope.prospectDTOList = [];
                            /*$scope.prospectCall.prospect.name = "";
                            $scope.prospectCall.prospect.firstName = "";
                            $scope.prospectCall.prospect.lastName = "";
                            $scope.prospectCall.prospect.title = "";
                            $scope.prospectCall.prospect.customAttributes.domain = "";
                            $scope.prospect.nameCopy = "";
                            */
                              $scope.isProspectInDNC = true;
                            $scope.prospectCall.prospect.city = "";
                            $scope.prospectCall.prospect.phone = "";
                            $scope.prospectCall.prospect.company = "";
                            $scope.prospectCall.prospect.department = ""

                            $scope.prospectCall.prospect.email = "";
                            $scope.prospectCall.prospect.linkedInURL = "";
                            $scope.search = false;
                            $scope.isPhoneNumberConnected = true;
                            $scope.resetAjaxWaiting();
            		   });
                    } else {
                        $scope.resetAjaxWaiting();
                        NotificationService.log('error', 'Invalid state. Can not save prospect details.');
                    }
                  }
               }

            //    $scope.leadIqOriginalPhone = "";
               $scope.leadIqOriginalEmail = "";
               $scope.createProspect = function () {
                    let prospectPhone = "";
                    $scope.seperateName();
                    prospectPhone = document.getElementById('params_contact_phone_number_2').value;
                    if (!prospectPhone.includes("XXX")) {
                        if (!prospectPhone.startsWith("+")) {
                            NotificationService.log('error', 'Please enter telephone number along with the ISD code. Example: +15036483216');
                            return;
                        } else {
                            $scope.prospectCall.prospect.phone = "+" + prospectPhone.replace(/\D/g,'');
                            $scope.phoneNumber = $scope.prospectCall.prospect.phone;
                        }
                    }

                    if (!validateCreateLeadProspectFields()) {
                        return;
                    }
                    var agentname = user.firstName +" "+ user.lastName;
                	if ($scope.parent.note != undefined && $scope.parent.note != "") {
                		var noteObject = {
                                creationDate: new Date(),
                                text: $scope.parent.note,
                                agentId: user.id,
                                agentName: agentname,
                                dispositionStatus: $scope.prospectCall.dispositionStatus,
                                subStatus: $scope.prospectCall.subStatus
                		};
                        $scope.prospectCall.notes = [];
                		$scope.prospectCall.notes.push(noteObject);
                	}
                    if (!$scope.leadIqOriginalEmail.includes("********@")) {
                        $scope.prospectCall.prospect.email = $scope.leadIqOriginalEmail;
                    }
                    if ($scope.prospectCall.prospect.stateCode == null || $scope.prospectCall.prospect.stateCode == undefined || $scope.prospectCall.prospect.stateCode == '') {
                        $scope.prospectCall.prospect.stateCode = '';
                    }

                    $scope.setAjaxWaiting();
                    if ($scope.prospectCall.campaignId == null || $scope.prospectCall.campaignId == '') {
                        $scope.prospectCall.campaignId = $scope.campaign.id;
                        $scope.prospectCall.agentId = user.id;
                        $scope.prospectCall.partnerId = user.organization;
                    }
                    researchAnDialService.createProspectingDetails($scope.prospectCall).$promise.then(function(prospectCallDTO) {
                        $scope.resetAjaxWaiting();
                        NotificationService.log('success', 'Prospect details created successfully.');
                        // set prospect received time for agent call monitoring
                        $scope.prospectReceivedTime = new Date();

                        $scope.prospectingModal.dismiss('cancel');
                        $scope.prospectCall = null;
                        // $scope.prospectCall.prospect = {};
                        // $scope.prospectCall.prospect.customAttributes = {};
                        $scope.prospect.nameCopy = "";
                        $scope.parent.note = "";
                        // document.getElementById('params_contact_phone_number_2').value='';
                        if ($scope.campaign.isLeadIQ) {
                            $scope.prospectingModal.dismiss('cancel');
                            $scope.openLeadIQSearProspectModal();
                        } else {
                            $scope.showProspectingModal();
                        }
                   }, function (error) {
                        $scope.resetAjaxWaiting();
                   });
           }
                    $scope.clearLeadIQDataProspectCall = function () {
                        $scope.prospect.nameCopy = null;
                        $scope.prospectCall.prospect.firstName = null;
                        $scope.prospectCall.prospect.email = null;
                        $scope.prospectCall.prospect.linkedInURL = null;
                        $scope.prospectCall.prospect.company = null;
                        $scope.prospectCall.prospect.companyId = null;
                        $scope.prospectCall.prospect.customAttributes.domain = null;
                        $("#tags").children('option').first().prop('selected', true)
                        $("#tags").trigger("chosen:updated");
                        $scope.search = false;
                        $scope.prospectDTOList = [];
                //$scope.prospectCall.prospect.name=null;
                    }
                      $scope.clearAllOldList = function () {
                         if($scope.isProspectInDNC){
                             $scope.similarProspectList = [];
                             $scope.prospectDTOList = [];
                             $scope.isProspectInDNC = false;
                         }

                      }



                    $scope.prospectFullName = "";
                    $scope.prospectDomain = "";
                    $scope.searchProspectDetailsFromLeadIQMultipleProspect = function (showName, showEmailId, showLinkedinUrl) {
                         $scope.prospectFullName = "";
                         $scope.prospectDomain = "";
                        if(showName){
                            if ($scope.prospect.nameCopy == "" || $scope.prospect.nameCopy == null && $scope.prospect.nameCopy == undefined || $scope.prospectCall.prospect.customAttributes.domain == "" || $scope.prospectCall.prospect.customAttributes.domain == null || $scope.prospectCall.prospect.customAttributes.domain == undefined){
                                NotificationService.log('error', 'Please enter Prospect and Company/Domain');
                            }
                        }
                        if(showEmailId){
                            if($scope.prospectCall.prospect.email == "" || $scope.prospectCall.prospect.email == null || $scope.prospectCall.prospect.email == undefined){
                                NotificationService.log('error', 'Please enter Email ID');
                            }
                        }
                        if(showLinkedinUrl){
                            if($scope.prospectCall.prospect.linkedInURL == "" || $scope.prospectCall.prospect.linkedInURL == null || $scope.prospectCall.prospect.linkedInURL == undefined){
                                NotificationService.log('error', 'Please enter LinkedIn URL');
                            }
                             if($scope.prospectCall.prospect.linkedInURL != "" && $scope.prospectCall.prospect.linkedInURL != null
                                        && $scope.prospectCall.prospect.linkedInURL != undefined
                                        && !$scope.prospectCall.prospect.linkedInURL.includes("linkedin")){
                                                NotificationService.log('error', 'Please enter valid LinkedIn URL');
                                                return;
                              }
                        }
                    if (($scope.prospect.nameCopy != "" && $scope.prospect.nameCopy != null && $scope.prospect.nameCopy != undefined && $scope.prospectCall.prospect.customAttributes.domain != "" && $scope.prospectCall.prospect.customAttributes.domain != null && $scope.prospectCall.prospect.customAttributes.domain != undefined) || ($scope.prospectCall.prospect.email != "" && $scope.prospectCall.prospect.email != null && $scope.prospectCall.prospect.email != undefined) || ($scope.prospectCall.prospect.linkedInURL != "" && $scope.prospectCall.prospect.linkedInURL != null && $scope.prospectCall.prospect.linkedInURL != undefined)) {
                        if ($scope.prospectCall.prospect.email != "" && $scope.prospectCall.prospect.email != null && $scope.prospectCall.prospect.email != undefined) {
                            if (!validateEmail($scope.prospectCall.prospect.email)) {
                                NotificationService.log('error', 'Please enter a valid email address.');
                                return;
                            } else {
                                $scope.leadIqOriginalEmail = $scope.prospectCall.prospect.email;
                            }
                        }
                        $scope.setAjaxWaiting();
                        // LeadIQ search first by Name and Company name, if no result search by Name and Domain (UI has same input for Campany name and domain so below value assigned)
                        if ($scope.prospectCall.prospect.customAttributes.domain != "" && $scope.prospectCall.prospect.customAttributes.domain != null && $scope.prospectCall.prospect.customAttributes.domain != undefined) {
                            $scope.prospectCall.prospect.company = $scope.prospectCall.prospect.customAttributes.domain;
                        }
                        $scope.prospectCall.prospect.campaignContactId = $scope.campaign.id;
                        //TODO removing title from search if we intoduce on the ui, this should be handled.
                        $scope.prospectCall.prospect.title = "";

                        researchAnDialService.leadIQSearchInfoMultiple({campaignId: $scope.campaign.id,agentId: user.id},$scope.prospectCall.prospect).$promise.then(function(prospectDTOs){
                            //$scope.searchprospectmodal.dismiss('cancel');
                            $scope.similarProspectList = [];
                            if (prospectDTOs == null || prospectDTOs == undefined) {
                                NotificationService.log('error', 'Prospect details not found');
                            }
                            if ($scope.campaign.type == 'Prospecting') {
                                if (prospectDTOs.length > 5) {
                                    $scope.prospectDTOList = [];
                                    var i;
                                    for (i = 0; i < 5; i++) {
                                        $scope.prospectDTOList.push(prospectDTOs[i]);
                                    }
                                } else {
                                    $scope.prospectDTOList = prospectDTOs;
                                }
                                $scope.resetAjaxWaiting();
                                if ($scope.prospectDTOList.length == 0) {
                                    $scope.showProspectingModal();
                                } else {
                                    $scope.researchanddialprospectmodal = $modal.open({
                                        scope: $scope,
                                        templateUrl: 'prospectListToSelectmodal.html',
                                        controller: 'LeadIQListController',
                                        windowClass: 'prospectListToSelectmodal',
                                        backdrop: 'static'
                                    });
                                }
                            } else {
                                // if (prospectDTOs.manualSearchProspect.length > 5) {
                                //     $scope.prospectDTOList = [];
                                //     var i;
                                //     for (i = 0; i < 5; i++) {
                                //         $scope.prospectDTOList.push(prospectDTOs.manualSearchProspect[i]);
                                //     }
                                // } else {
                                //     $scope.prospectDTOList = prospectDTOs.manualSearchProspect;
                                // }
                                 $scope.search = true;
                                $scope.prospectDTOList = prospectDTOs.manualSearchProspect;
                                $scope.similarProspectList = prospectDTOs.similarSearchProspect;
                                 $scope.similarProspect = prospectDTOs.similarProspect;
                                if ($scope.similarProspectList == null){
                                    $scope.similarProspectList = [];
                                }
                                 if ($scope.prospectDTOList == null){
                                      $scope.prospectDTOList = [];
                                 }
                                //if ($scope.prospect.nameCopy == '' || $scope.prospect.nameCopy == null || $scope.prospect.nameCopy == undefined || $scope.prospectCall.prospect.customAttributes.domain == '' || $scope.prospectCall.prospect.customAttributes.domain == null || $scope.prospectCall.prospect.customAttributes.domain == undefined || $scope.prospectDTOList.length == 0) {
                                if ($scope.prospectDTOList.length == 0) {
                                     $scope.showNameDomainLeadIQ = true;
                                     $scope.prospectFullName = $scope.prospect.nameCopy;
                                     $scope.prospectDomain = $scope.prospectCall.prospect.customAttributes.domain
                                } else {
                                    $scope.showNameDomainLeadIQ = false;
                                }
                                if ($scope.prospectCall.prospect.email != "" && $scope.prospectCall.prospect.email != null && $scope.prospectCall.prospect.email != undefined) {
                                    $scope.leadIQEmail = true;
                                } else {
                                    $scope.leadIQEmail = false;
                                }
                                if (!$scope.campaignDetails.isEmailSuppressed) {
                                    $scope.prospectCall.prospect.email = "";
                                }
                                $scope.resetAjaxWaiting();
                                /*$scope.researchanddialprospectmodal = $modal.open({
                                    scope: $scope,
                                    templateUrl: 'prospectListToSelectmodal.html',
                                    controller: 'LeadIQListController',
                                    windowClass: 'prospectListToSelectmodal',
                                    backdrop: 'static'
                                });*/
                            }
                            // } else {
                            //     NotificationService.log('error', 'Prospect info is not available in the database. Please enter the details manually');
                            // }
                            // if (prospectDTO.phone != null && prospectDTO.phone != "") {
                            //     var phoneInput = document.getElementById('params_contact_phone_number_1_LeadIQ');
                            //     phoneInput.value = prospectDTO.phone;
                            //     $scope.prospectCall.prospect.title = prospectDTO.title;
                            // } else {
                            //     $scope.prospectCall.prospect.phone = "";
                            //     NotificationService.log('error', 'Prospect details not found');
                            // }
                        }, function (error) {
                            $scope.prospectCall.prospect.phone = "";
                            $scope.resetAjaxWaiting();
                            // NotificationService.log('error', 'An error occurred while fetching prospect details');
                        });
                    // } else {
                    //     NotificationService.log('error', 'Please enter Name and Company name/Domain or Email');
                    }
                }

               $scope.setCustomAttributes = function() {
                    $scope.prospectCall.prospect.domain = $scope.prospectCall.prospect.customAttributes.domain;
                    $scope.employeeCount = $scope.prospectCall.prospect.customAttributes.minEmployeeCount + '-' + $scope.prospectCall.prospect.customAttributes.maxEmployeeCount;
                    if ($scope.prospectCall.prospect.customAttributes.minRevenue == 0 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 10000000) {
                        $scope.revenue = '0-$10M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 5000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 10000000) {
                            $scope.revenue = '$5M-$10M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 10000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 25000000) {
                            $scope.revenue = '$10M-$25M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 25000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 50000000) {
                            $scope.revenue = '$25M-$50M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 50000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 100000000) {
                            $scope.revenue = '$50M-$100M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 100000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 250000000) {
                            $scope.revenue = '$100M-$250M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 250000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 500000000) {
                            $scope.revenue = '$250M-$500M';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 500000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 1000000000) {
                            $scope.revenue = '$500M-$1B';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue == 1000000000 && $scope.prospectCall.prospect.customAttributes.maxRevenue == 5000000000) {
                            $scope.revenue = '$1B-$5B';
                        } else if ($scope.prospectCall.prospect.customAttributes.minRevenue >= 5000000000) {
                            $scope.revenue = 'More than $5Billion';
                        }
               }

               $scope.loadStates = function() {
                   getStateDetailService.get({countryName:$scope.prospectCall.prospect.country}).$promise.then(function(data) {
                           $scope.stateNames = data;
                       })
               }

               $scope.loadCountries = function() {
                   getCountryDetailService.get().$promise.then(function(data) {
                        $scope.countryNames = data;
                    })
               }
               $scope.loadCountries();

               $scope.submitLeadProspect = function() {
                    if (!validateSubmitProspectFields()) {
                        return;
                    } else {
                        $scope.submitLeadFlag = true;
                        $scope.submitleadprospectmodal.close();
                        if ($scope.parent.state == 3) {
                            NotificationService.log('error', 'Prospect details updated successfully. Please disconnect the call before submitting the prospect.');
                            return;
                        } else {
//                            NotificationService.log('success', 'Prospect details updated successfully.');
                            $scope.submitProspect();
                        }
                    }
               }

               var validateProspectFields = function() {
                if ($scope.prospect.nameCopy == null
                        || $scope.prospect.nameCopy == undefined
                        || $scope.prospect.nameCopy == '') {
                            if($scope.prospectDTOList.length == 0){
                                NotificationService.log('error', 'Please enter name.');
                            }else{
                                 NotificationService.log('error', 'Please select Prospect.');
                            }
                        return false;
                }
               if ($scope.phoneNumber == null
                       || $scope.phoneNumber == undefined
                       || $scope.phoneNumber == '' || $scope.phoneNumber.length < 6) {
                   NotificationService.log('error', 'Please enter phone number.');
                   return false;
               }
               if ($scope.prospectCall.prospect.title == null
                        || $scope.prospectCall.prospect.title == undefined
                        || $scope.prospectCall.prospect.title == '') {
                    NotificationService.log('error', 'Please enter title.');
                    return false;
                }
               /*if ($scope.prospectCall.prospect.customAttributes.domain == null
                       || $scope.prospectCall.prospect.customAttributes.domain == undefined
                       || $scope.prospectCall.prospect.customAttributes.domain == '') {
                        if($scope.campaign.abm){
                            NotificationService.log('error', 'Please select domain from ABM list.');
                        }else{
                            NotificationService.log('error', 'Please enter domain name.');
                        }
                   return false;
               }*/
               if ($scope.campaign.isEmailSuppressed && ($scope.prospectCall.prospect.email == null
                        || $scope.prospectCall.prospect.email == undefined
                        || $scope.prospectCall.prospect.email == '')) {
                    NotificationService.log('error', 'Please enter email.');
                    return false;
                }
               if ($scope.campaign.isEmailSuppressed && !validateEmail($scope.prospectCall.prospect.email)) {
                    NotificationService.log('error', 'Please enter a valid email address.');
                    return false;
                }
               return true;
           }

               var validateCreateLeadProspectFields = function() {
                if ($scope.prospectCall.prospect.firstName == null
                        || $scope.prospectCall.prospect.firstName == undefined
                        || $scope.prospectCall.prospect.firstName == '') {
                    NotificationService.log('error', 'Please enter first name.');
                    return false;
                }
                if ($scope.prospectCall.prospect.lastName == null
                        || $scope.prospectCall.prospect.lastName == undefined
                        || $scope.prospectCall.prospect.lastName == '') {
                    NotificationService.log('error', 'Please enter last name.');
                    return false;
                }
                if ($scope.prospectCall.prospect.phone == null
                    || $scope.prospectCall.prospect.phone == undefined
                    || $scope.prospectCall.prospect.phone == '') {
                    NotificationService.log('error', 'Please enter phone number.');
                    return false;
                }
                // if ($scope.leadIqOriginalPhone == null
                //         || $scope.leadIqOriginalPhone == undefined
                //         || $scope.leadIqOriginalPhone == '') {
                //     NotificationService.log('error', 'Please enter phone number.');
                //     return false;
                // }
                if ($scope.prospectCall.prospect.company == null
                        || $scope.prospectCall.prospect.company == undefined
                        || $scope.prospectCall.prospect.company == '') {
                    NotificationService.log('error', 'Please enter company name.');
                    return false;
                }
                if ($scope.prospectCall.prospect.title == null
                        || $scope.prospectCall.prospect.title == undefined
                        || $scope.prospectCall.prospect.title == '') {
                    NotificationService.log('error', 'Please enter title.');
                    return false;
                }

                 if ($scope.prospectCall.prospect.managementLevel == null
                        || $scope.prospectCall.prospect.managementLevel == undefined
                        || $scope.prospectCall.prospect.managementLevel == '') {
                    NotificationService.log('error', 'Please enter managementLevel.');
                    return false;
                }
                if ($scope.prospectCall.prospect.customAttributes.minEmployeeCount == null
                    || $scope.prospectCall.prospect.customAttributes.minEmployeeCount == undefined
                    || $scope.prospectCall.prospect.customAttributes.minEmployeeCount == ''
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == null
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == undefined
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == '') {
                NotificationService.log('error', 'Please select employee range.');
                return false;
            }
                if ($scope.prospectCall.prospect.industry == null
                        || $scope.prospectCall.prospect.industry == undefined
                        || $scope.prospectCall.prospect.industry == '') {
                    NotificationService.log('error', 'Please enter industry.');
                    return false;
                }
                if ($scope.prospectCall.prospect.department == null
                    || $scope.prospectCall.prospect.department == undefined
                    || $scope.prospectCall.prospect.department == '') {
                NotificationService.log('error', 'Please enter department.');
                return false;
                }
                if ($scope.prospectCall.prospect.country == null
                        || $scope.prospectCall.prospect.country == undefined
                        || $scope.prospectCall.prospect.country == '') {
                    NotificationService.log('error', 'Please enter country.');
                    return false;
                }
                if ($scope.prospectCall.prospect.stateCode == null
                    || $scope.prospectCall.prospect.stateCode == undefined
                    || $scope.prospectCall.prospect.stateCode == '') {
                NotificationService.log('error', 'Please enter state code.');
                return false;
            }
                return true;
            }

            var validateSubmitProspectFields = function() {
                if ($scope.prospectCall.prospect.firstName == null
                        || $scope.prospectCall.prospect.firstName == undefined
                        || $scope.prospectCall.prospect.firstName == '') {
                    NotificationService.log('error', 'Please enter first name.');
                    return false;
                }
                if ($scope.prospectCall.prospect.lastName == null
                        || $scope.prospectCall.prospect.lastName == undefined
                        || $scope.prospectCall.prospect.lastName == '') {
                    NotificationService.log('error', 'Please enter last name.');
                    return false;
                }
                // if ($scope.prospectCall.prospect.phone == null
                //         || $scope.prospectCall.prospect.phone == undefined
                //         || $scope.prospectCall.prospect.phone == '') {
                //     NotificationService.log('error', 'Please enter phone number.');
                //     return false;
                // }
                if ($scope.prospectCall.prospect.company == null
                        || $scope.prospectCall.prospect.company == undefined
                        || $scope.prospectCall.prospect.company == '') {
                    NotificationService.log('error', 'Please enter company name.');
                    return false;
                }
                if ($scope.prospectCall.prospect.title == null
                        || $scope.prospectCall.prospect.title == undefined
                        || $scope.prospectCall.prospect.title == '') {
                    NotificationService.log('error', 'Please enter title.');
                    return false;
                }
                if ($scope.prospectCall.prospect.email == null
                        || $scope.prospectCall.prospect.email == undefined
                        || $scope.prospectCall.prospect.email == '') {
                    NotificationService.log('error', 'Please enter email.');
                    return false;
                 } else if($scope.originalEmail == null && !validateEmail($scope.prospectCall.prospect.email)) {
                    NotificationService.log('error', 'Please enter a valid email address.');
                    return;
                 }
                 if ($scope.prospectCall.prospect.managementLevel == null
                        || $scope.prospectCall.prospect.managementLevel == undefined
                        || $scope.prospectCall.prospect.managementLevel == '') {
                    NotificationService.log('error', 'Please enter managementLevel.');
                    return false;
                }
                if ($scope.prospectCall.prospect.customAttributes.minEmployeeCount == null
                    || $scope.prospectCall.prospect.customAttributes.minEmployeeCount == undefined
                    || $scope.prospectCall.prospect.customAttributes.minEmployeeCount == ''
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == null
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == undefined
                    || $scope.prospectCall.prospect.customAttributes.maxEmployeeCount == '') {
                NotificationService.log('error', 'Please select employee range.');
                return false;
            }
                if ($scope.prospectCall.prospect.industry == null
                        || $scope.prospectCall.prospect.industry == undefined
                        || $scope.prospectCall.prospect.industry == '') {
                    NotificationService.log('error', 'Please enter industry.');
                    return false;
                }
                if ($scope.prospectCall.prospect.country == null
                        || $scope.prospectCall.prospect.country == undefined
                        || $scope.prospectCall.prospect.country == '') {
                    NotificationService.log('error', 'Please enter country.');
                    return false;
                }
                if ($scope.prospectCall.prospect.addressLine1 == null
                        || $scope.prospectCall.prospect.addressLine1 == undefined
                        || $scope.prospectCall.prospect.addressLine1 == '') {
                    NotificationService.log('error', 'Please enter address.');
                    return false;
                }
                if ($scope.prospectCall.prospect.city == null
                        || $scope.prospectCall.prospect.city == undefined
                        || $scope.prospectCall.prospect.city == '') {
                    NotificationService.log('error', 'Please enter city.');
                   return false;
                }
                if ($scope.prospectCall.prospect.stateCode == null
                        || $scope.prospectCall.prospect.stateCode == undefined
                        || $scope.prospectCall.prospect.stateCode == '') {
                    NotificationService.log('error', 'Please enter stateCode.');
                    return false;
                }
                if ($scope.prospectCall.prospect.zipCode == null
                        || $scope.prospectCall.prospect.zipCode == undefined
                        || $scope.prospectCall.prospect.zipCode == '') {
                    NotificationService.log('error', 'Please enter zipCode.');
                    return false;
                }
                if ($scope.prospectCall.prospect.customAttributes.domain == null
                        || $scope.prospectCall.prospect.customAttributes.domain == undefined
                        || $scope.prospectCall.prospect.customAttributes.domain == '') {
                    NotificationService.log('error', 'Please enter domain.');
                    return false;
                }

                return true;
            }

               function checkPhoneNumberPattern() {
                    var phonePattern = /^\+?\d{0,4}[-]\d{0,12}$/;
                    return  phonePattern.test($scope.prospectCall.prospect.phone);
               }

               $scope.seperateName = function() {
                    if ($scope.prospect.nameCopy == undefined) {
						return;
                    }
                    var name = $scope.prospect.nameCopy;
                    var firstname = "";
            		var lastname = "";
            		name = name.replace('\t', " ");

            		var tempName = name.split(" ");
                    var tot = tempName.length;
                    if (tot > 0) {
                    	for (i = 0; i < tot; i++) {
							if (i == 0) {
								firstname = tempName[0];
							} else {
								if (lastname == '') {
									lastname = tempName[i];
								} else {
									lastname = lastname + " " + tempName[i];
								}
							}
                		}
					}
                    if ($scope.prospectCall == null) {
                        $scope.prospectCall = {};
                        $scope.prospectCall.prospect = {};
                        $scope.prospectCall.prospect.customAttributes = {};
                    }
                    $scope.prospectCall.prospect.name = $scope.prospect.nameCopy;
                    firstname = firstname.replace(/[-&[\]/\\`!|@^_=;#,+()$~%.'":*?<>{}\r\n\t]/g,'');
                    lastname = lastname.replace(/[-&[\]/\\`!|@^_=;#,+()$~%.'":*?<>{}\r\n\t]/g,'');
                    $scope.prospectCall.prospect.firstName = firstname.trim();
                    $scope.prospectCall.prospect.lastName = lastname.trim();
               }

               $scope.filterName = function(name) {
                   if (name != null && name != undefined) {
                    name = name.replace(/[-&[\]/\\`!|@^_=;#,+()$~%.'":*?<>{}\r\n\t]/g,'');
                   }
                   if (name.includes('/') || name.includes('\\')) {
                       name.replace('/','');
                       name.replace('\\','');
                   }
                   return name;
               }

               $scope.checkDNC = function () {
            	   if (!validateDNCFields()) {
                       return;
                   }
            	   var dncProspect = {};
            	   if($scope.prospectCall.prospect.customAttributes != undefined
            			   && $scope.prospectCall.prospect.customAttributes != null){
            		   dncProspect.companyDomain = $scope.prospectCall.prospect.customAttributes.domain;
            	   } else{
            		   dncProspect.companyDomain = null;
            	   }
            	//    $scope.changeProspectCountry();
            	   var country = $scope.prospectCall.prospect.country;
            	   var isUSorCanada = false;
            	   if (country != undefined && (country == 'US' || country == 'USA'
            		   || country == 'United States' || country == 'CA' || country == 'Canada')) {
            		   isUSorCanada = true;
           		   }
            	   var domain = $scope.prospectCall.prospect.customAttributes.domain;
            	   var phoneNumber = $scope.campaignCountryCode + $scope.phoneNumber;
            	   dncProspect.firstName = $scope.prospectCall.prospect.firstName;
            	   dncProspect.lastName = $scope.prospectCall.prospect.lastName;
            	   dncProspect.phone = phoneNumber;
            	   dncProspect.companyDomain = domain;
            	   dncProspect.isUSorCanada = isUSorCanada;

            	   researchAnDialService.checkDNC(dncProspect).$promise.then(function(dnc) {
            		   if (dnc != null && dnc != undefined && dnc[0] == 'f') {
            			   NotificationService.log('error', 'Can not call this prospect. Prospect is in DNC list.');
            		   }
            	   },function(error) {

            	   });
               }

               var validateDNCFields = function() {
                   if ($scope.prospectCall.prospect.firstName == null
                           || $scope.prospectCall.prospect.firstName == undefined
                           || $scope.prospectCall.prospect.firstName == '') {
                       return false;
                   }
                   if ($scope.prospectCall.prospect.lastName == null
                           || $scope.prospectCall.prospect.lastName == undefined
                           || $scope.prospectCall.prospect.lastName == '') {
                       return false;
                   }
                   if ($scope.phoneNumber == null || $scope.phoneNumber == undefined || $scope.phoneNumber == '') {
                       return false;
                   }
                   if ($scope.prospectCall.prospect.customAttributes.domain == null
                           || $scope.prospectCall.prospect.customAttributes.domain == undefined
                           || $scope.prospectCall.prospect.customAttributes.domain == '') {
                       return false;
                   }
                   return true;
               }
            // ########## RESEARCH_AND_DIAL ##########


			///////////////////////////////////////////////end test.js////////////////////////
			}
        ]).controller('ResearchAndDialProspectController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $scope.phoneNumber = "";
                $modalInstance.dismiss('cancel');
            };
            $scope.setPhoneNumber = function () {
                $scope.$parent.phoneNumber = $scope.phoneNumber;
            }
            $scope.setCountryCode = function () {
                $scope.$parent.campaignCountryCode = $scope.campaignCountryCode;
            }
        }).controller('ResearchAndDialProspectLeadIQController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $scope.prospectCall.prospect.name = "";
               $scope.prospectCall.prospect.firstName = "";
               $scope.prospectCall.prospect.lastName = "";
               $scope.prospectCall.prospect.city = "";
               $scope.prospectCall.prospect.phone = "";
               $scope.prospectCall.prospect.title = "";
               $scope.prospectCall.prospect.company = "";
               $scope.prospectCall.prospect.department = ""
               $scope.prospectCall.prospect.customAttributes.domain = "";
               $scope.prospect.nameCopy = "";
               $scope.prospectCall.prospect.email = "";
               $scope.prospectCall.prospect.linkedInURL = "";
                $modalInstance.dismiss('cancel');
            };
            if ($scope.prospectCall != null && $scope.prospectCall != undefined && $scope.prospectCall.prospect != null && $scope.prospectCall.prospect != undefined) {
                $scope.prospectCall.prospect.name = "";
                $scope.prospectCall.prospect.firstName = "";
                $scope.prospectCall.prospect.lastName = "";
                $scope.prospectCall.prospect.city = "";
                $scope.prospectCall.prospect.phone = "";
                $scope.prospectCall.prospect.title = "";
                $scope.prospectCall.prospect.company = "";
                $scope.prospectCall.prospect.department = ""
                $scope.prospectCall.prospect.customAttributes.domain = "";
                $scope.prospect.nameCopy = "";
                $scope.prospectCall.prospect.email = "";
                $scope.prospectCall.prospect.linkedInURL = "";
            }
            $scope.searchProspectDetailsFromLeadIQMultiple= function () {
                $scope.searchProspectDetailsFromLeadIQMultipleProspect($scope.showName, $scope.showEmailId, $scope.showLinkedinUrl)
              };

             $scope.clearLeadIQData = function () {
                $scope.clearLeadIQDataProspectCall()
              };

                 $scope.clearOldList = function(){
                      $scope.clearAllOldList()
                };


        }).controller('LeadIQListController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $scope.prospectDTOList = [];
                 $scope.prospectCall.prospect.name = "";
                $scope.prospectCall.prospect.firstName = "";
                $scope.prospectCall.prospect.lastName = "";
                $scope.prospectCall.prospect.city = "";
                $scope.prospectCall.prospect.title = "";
                $scope.prospectCall.prospect.phone = "";
                $scope.prospectCall.prospect.company = "";
                $scope.prospectCall.prospect.department = ""
                $scope.prospectCall.prospect.customAttributes.domain = "";
                $scope.prospect.nameCopy = "";
                $scope.prospectCall.prospect.email = "";
                $scope.prospectCall.prospect.linkedInURL = "";
                $modalInstance.dismiss('cancel');
            };
            $scope.resetLeadIQ = function () {
                $scope.prospectDTOList = [];
                $modalInstance.dismiss('cancel');
                $scope.prospectCall = {};
                $scope.prospect.nameCopy = "";
                $scope.openLeadIQSearProspectModal();
            };
            if ($scope.prospectFullName != '' && $scope.prospectFullName != null && $scope.prospectFullName != undefined &&
                $scope.prospectDomain != '' && $scope.prospectDomain != null && $scope.prospectDomain != undefined) {
                        $scope.prospect.nameCopy = $scope.prospectFullName;
                        $scope.prospectCall.prospect.name = $scope.prospectFullName;
                        $scope.prospectCall.prospect.customAttributes.domain =  $scope.prospectDomain;
                        $scope.seperateName();
            }


            // prospectDTOList = $scope.prospectDTOList;
            // $scope.setPhoneNumber = function () {
            //     $scope.$parent.phoneNumber = $scope.phoneNumber;
            // }
            // $scope.setCountryCode = function () {
            //     $scope.$parent.campaignCountryCode = $scope.campaignCountryCode;
            // }
        }).controller('SubmitleadprospectmodalController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.setPhoneNumber = function () {
                $scope.$parent.phoneNumber = $scope.phoneNumber;
            }
            $scope.setCountryCode = function () {
                $scope.$parent.campaignCountryCode = $scope.campaignCountryCode;
            }
        }).controller('campaignTypeProspectingModalController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $scope.$parent.prospectCall = {};
                $scope.revenue = '';
                $scope.employeeCount = '';
                $scope.leadIqOriginalEmail = '';
                $modalInstance.dismiss('cancel');
            };
           
            $scope.setProspectEmail = function () {
                $scope.$parent.leadIqOriginalEmail = $scope.leadIqOriginalEmail;
            }

            $scope.clearEmployeeAndRevenue = function() {                  
                $scope.revenue = '';
                $scope.employeeCount = '';
                $scope.leadIqOriginalEmail = '';
            }
        }).controller('NewIndirectProspectController', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }).controller('CampaignModeChangeController', function ($scope, $modalInstance) {
        	$scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }).controller('AlternateProspectController', function ($scope, $rootScope, $timeout, $modalInstance, twilioService) {
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.clickPad = function (digit) {
            	if ($scope.getCallControlProvider() == 'Plivo') {
					$scope.plivoSendDtmf(digit);
				} else if ($scope.getCallControlProvider() == 'Telnyx') {
                    $scope.telnyxSendDtmf(digit);
                } else if ($scope.getCallControlProvider() == 'Signalwire') {
                    $scope.signalwireSendDtmf(digit);
                } else {
					twilioService.sendDigits(digit);
				}
            }
        }).controller('CallbackController', function ($scope, $rootScope, $timeout, $modalInstance, getCallbackTimezoneService) {
            $scope.checkTimeZone = function () {
                $scope.prospectCall.prospect.stateCode = $scope.getState.selectedState.stateCode;
                getCallbackTimezoneService.get({id: $scope.prospectCall.prospect.stateCode}).$promise
                    .then(
                    function (data) {
                        $scope.callback.currentTimeZone = data.StateTimeZone;
                        $scope.callback.currentTime = data.CurrentTimeInStateTZ;
                        $scope.callback.endworkinghours = data.AgentWorkEndHrInStateTZ;
                        $scope.callback.timeZone = data.StateTimeZone;
                        $scope.callback.hoursList = [];
                        for (i = data.AgentWorkStartHrInStateTZ; i <= data.AgentWorkEndHrInStateTZ; i++) {
                            $scope.callback.hoursList.push($scope.hrsList[i]);
                        }
                    },
                    function (error) {

                    });
            };
            $scope.validateTime = function () {
                if ($scope.callback.starthours == "08" || $scope.callback.starthours == "09" || $scope.callback.starthours == "10" || $scope.callback.starthours == "11") {
                    $scope.callback.ampm = "AM";
                } else {
                    $scope.callback.ampm = "PM";
                }
            };
            if ($scope.prospectCall.subStatus != undefined && $scope.prospectCall.subStatus != "") {
            	 $scope.callbackcode = $scope.prospectCall.subStatus;
            }
            $rootScope.callbackSubStatusSelected = false;
            $scope.showCallbackAlert = false;
            $scope.$watch('callback.starthours', function(newval, oldval){
            	$scope.showCallbackAlert = false;
      	  	});
            $scope.$watch('callback.endhours', function(newval, oldval){
            	$scope.showCallbackAlert = false;
      	  	});
            $scope.ok = function () {

            	if(($scope.callback.starthours == null || $scope.callback.starthours == undefined || $scope.callback.starthours == '') || ($scope.callback.endhours == null || $scope.callback.endhours == undefined || $scope.callback.endhours == '')) {
            		$scope.showCallbackAlert = true;
            		$timeout(function(){
            			$scope.showCallbackAlert = false;
            		}, 2500);
            	} else {
            		$rootScope.callbackSubStatusSelected = true;
            		$scope.disposition = "CALLBACK";
               	 	$scope.prospectCall.dispositionStatus = "CALLBACK";
               	 	$scope.prospectCall.subStatus = $scope.$parent.callbackcode;
                    $scope.callback.callbackHours = $scope.callback.starthours;
               	 	var cdate = $scope.callback.currentEventDate.split("-");
               	 	$scope.callback.callbackDate = cdate[2] + "-" + cdate[1] + "-" + cdate[0];
               	 	$scope.callback.callbackMins = parseInt($scope.callback.endhours);
               	 	console.log($scope.disposition);
               	 	$modalInstance.dismiss('close');
            	}
            };
            $scope.openSelectState = function ($event) {
                $scope.isSelectState = true;
                ;
            };
            $scope.closeSelectState = function ($event) {
                $scope.isSelectState = false;
                ;
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }).controller('CallbackControllerOld', function ($scope, $modalInstance, callbackHours) {
            $scope.output = {
                callbackHours: callbackHours
            };

            $scope.ok = function () {
                $modalInstance.close($scope.output.callbackHours);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }).controller('DispositionStatusController', function ($scope, $modalInstance, disposition, items, item) {
            $scope.disposition = disposition;
            $scope.items = items;
            $scope.selected = {
                item: angular.isDefined(item) ? item : $scope.items[0]
            };

            $scope.ok = function () {
                $modalInstance.close($scope.selected.item);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }).controller('callTimerController', function ($scope, $rootScope) {
        	$scope.showCallTimer = false;
            $scope.$on("call-timer-start", function (event) {
                $scope.$broadcast("timer-start");
                $scope.timerRunning = true;
                $scope.showCallTimer = true;
            });

            $scope.$on("call-timer-reset", function (event) {
                $scope.$broadcast("timer-reset");
            });

            $scope.$on("call-timer-stop", function (event) {
                $scope.$broadcast("timer-stop");
                $scope.timerRunning = false;
                $scope.showCallTimer = false;
            });
        }).controller('muteCallTimerController', function ($scope, $rootScope) {
            $scope.$on("mute-timer-start", function (event) {
                $scope.$broadcast("timer-start");
                $scope.timerRunning = true;
                $scope.showMuteTimer = true;
             });

            $scope.$on("mute-timer-reset", function (event) {
                $scope.$broadcast("timer-reset");
            });

            $scope.$on("mute-timer-stop", function (event) {
                $scope.$broadcast("timer-stop");
                $scope.timerRunning = false;
                $scope.showMuteTimer = false;
                $scope.showHoldTimer = false;
            });
        }).controller('holdCallTimerController', function ($scope, $rootScope) {
            $scope.$on("hold-timer-start", function (event) {
                $scope.$broadcast("timer-start");
                $scope.timerRunning = true;
                $scope.showHoldTimer = true;
             });

            $scope.$on("hold-timer-reset", function (event) {
                $scope.$broadcast("timer-reset");
            });

            $scope.$on("hold-timer-stop", function (event) {
                $scope.$broadcast("timer-stop");
                $scope.timerRunning = false;
                $scope.showHoldTimer = false;
            });
        }).controller('wrapupTimerController', function ($scope, $rootScope, $modal, $timeout, $interval) {
        	$scope.showWrapUpTimer = false;
            $scope.$on("wrapup-timer-start", function (event) {
                $scope.$broadcast("timer-start");
                $scope.timerRunning = true;
                $scope.showWrapUpTimer = true;
                $scope.hideCallStatusTimer = true;
                // $scope.$parent.forceSubmitInterval = $interval($scope.submitProspectTimeout, 15000, 1);
            });

            $scope.$on("wrapup-timer-reset", function (event) {
                $scope.$broadcast("timer-reset");
                $scope.hideCallStatusTimer = false;
            });

            $scope.$on("wrapup-timer-stop", function (event) {
                $scope.$broadcast("timer-stop");
                $scope.timerRunning = false;
                $scope.showWrapUpTimer = false;
                $scope.hideCallStatusTimer = false;
            });

            /* START
             * DATE : 22/05/2017
             * REASON : Used to show the pop up when wrap-up timer goes beyond 30 seconds. */
            /*$scope.isWrapupModelOpen = false;
            $scope.$on('timer-tick', function (event, data) {
            	if ($scope.showWrapUpTimer && $scope.timerRunning === true) {
            		if (!$scope.isWrapupModelOpen) {
            			$timeout($scope.wrapupProspectSubmitModal, 3600000);
                    	$scope.isWrapupModelOpen = true;
					}
				}
            });

            $scope.wrapupProspectSubmitModal = function () {
            	var noconsentmodal = $modal.open({
                    scope: $scope,
                    templateUrl: 'wrapupprospectsubmit.html',
                    controller: 'wrapupProspectSubmitController',
                    windowClass: 'finishcodemodal',
                    backdrop: 'static'
                });
            	 $model returns promise(opened, close, dismiss, result)
            	 * In below case we are setting isWrapupModelOpen to false in case of pop-up close or dismiss
            	noconsentmodal.result.finally(function () {
            		$scope.isWrapupModelOpen = false;
                });
            }*/
            /* END */


        })

        /* START
         * DATE : 29/03/2017
         * REASON : Below controller is used to handle pop up when wrap-up timer goes beyond 30 seconds. */
		/*.controller('wrapupProspectSubmitController', function ($scope, $rootScope, $modalInstance) {
			$scope.cancelWrapupModel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.okWrapupModel = function() {
                $modalInstance.dismiss('cancel');
            }
          })*/
          /* END */

          .controller('prospectTimerController', function ($scope, $rootScope) {
        	$scope.$on("prospect-timer-reset", function (event) {
                $scope.$broadcast("timer-reset");
                $scope.prospectName = "No prospect";
                $scope.contactTime = "NA";
            });

            $scope.$on("prospect-timer-start", function (event) {
                $scope.$broadcast("timer-start");
                $scope.contactTime = (new Date()).toLocaleTimeString()
                $scope.timerRunning = true;
            });

            $scope.$on("prospect-timer-stop", function (event) {
                $scope.$broadcast("timer-stop");
                $scope.timerRunning = false;
            });
        }).controller('disqualifiedProspectController', function ($scope, $rootScope, $modalInstance, twilioService) {
        	$scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.setNoQualify = function() {
            	$scope.$parent.prospectCall.dispositionStatus = "FAILURE";
                $scope.$parent.prospectCall.subStatus = "FAILED_QUALIFICATION";
                $scope.$parent.dialercodestatus = "FAILED_QUALIFICATION";
                $scope.$parent.disposition = "FAILURE";
                $scope.hidesu = false;
                $modalInstance.dismiss('cancel');
                //$scope.opentabs("Questions");
            }

            $scope.clickPad = function (digit) {
            	if ($scope.getCallControlProvider() == 'Plivo') {
					$scope.plivoSendDtmf(digit);
				} else if ($scope.getCallControlProvider() == 'Telnyx') {
                    $scope.telnyxSendDtmf(digit);
                } else if ($scope.getCallControlProvider() == 'Signalwire') {
                    $scope.signalwireSendDtmf(digit);
                } else {
					twilioService.sendDigits(digit);
				}
            }

        }).controller('FailureCodeController', function ($scope, $rootScope, $modalInstance, NotificationService, $timeout) {
        	$scope.failurecode = $scope.prospectCall.subStatus;
        	$scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.showFinishCodeAlert = false;
            $scope.$watch('failurecode', function(newval, oldval){
            	$scope.showFinishCodeAlert = false;
      	  	});
            $rootScope.failureSubStatusSelected = false;
            $scope.setFailureCodeStatus = function () {
            	$scope.prospectCall.dispositionStatus = "FAILURE";
            	if ($scope.failurecode == undefined || $scope.failurecode == "") {
            		$scope.prospectCall.dispositionStatus = null;
            		$scope.showFinishCodeAlert = true;
            		$timeout(function(){
            			$scope.showFinishCodeAlert = false;
               		 }, 2500);
            	} else {
            		$rootScope.failureSubStatusSelected = true;
            		$scope.prospectCall.subStatus = $scope.failurecode;
                    $scope.dialercodestatus = $scope.failurecode;

                    //set other disposition substatus value null
                    $scope.callback.starthours = null;
                    $scope.callback.endhours = null;
                    $scope.callback.ampm = null;

                    $modalInstance.dismiss('close');
                    $scope.disposition = "FAILURE";
                    console.log( $scope.disposition);
                    console.log( $scope.campaign);
            	}
            }
            $scope.setNoConsent = function() {
            	$scope.prospectCall.dispositionStatus = "FAILURE";
                $scope.prospectCall.subStatus = "NO_CONSENT";
                $scope.dialercodestatus = "NO_CONSENT";
                $scope.disposition = "FAILURE";
                $modalInstance.dismiss('cancel');
                $scope.opentabs("Notes");
            }
        }).filter("sanitize", ['$sce', function($sce) {
        	return function(htmlCode){
        		return $sce.trustAsHtml(htmlCode);
        	}
        }]).filter('orderObjectBy', function(){
        	return function(input, attribute) {
        		if (!angular.isObject(input)) return input;
        			var array = [];
        			for(var objectKey in input) {
        				array.push(input[objectKey]);
        			}
        			array.sort(function(a, b){
        				a = parseInt(a[attribute]);
        				b = parseInt(b[attribute]);
        				return a - b;
        			});
        			return array;
        	}
        }).directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
            return {
                //scope: true,   // optionally create a child scope
                link: function (scope, element, attrs) {
                    var model = $parse(attrs.focusMe);
                    scope.$watch(model, function (value) {
                        console.log('value=', value);
                        if (value === true) {
                            $timeout(function () {
                                element[0].focus();
                            });
                        }
                    });
                    // to address @blesh's comment, set attribute value to 'false'
                    // on blur event:
                    element.bind('blur', function () {
                        console.log('blur');
                        scope.$apply(model.assign(scope, false));
                    });
                }
            };
        }]);
});
