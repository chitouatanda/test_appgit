define(['angular', 'agent/twilio-service'], function(angular) {
    angular.module('dialer-helper-service', ['twilio-service'])
    .factory('dialerHelperService', function($rootScope, twilioService) {
    	
    	
    	return {
    		numberToDial: "Raghava",
    		setNumberToDial: setNumberToDial,
    		clickPad: clickPad,
    		toggleMute: toggleMute,
    		isAutoDialingEnabled: isAutoDialingEnabled,
    		manualCall: manualCall,
    		manualEndCall: manualEndCall,
    		conference: conference,
    		endConference: endConference
    	}
    });
});