define(
		[ 'angular', 'user-service', 'login-service', 'notification-service', 'teamnotice-service',
				'angular-ui-bootstrap', 'angular-timer', 'common/xtaas-filters' ],
		function(angular) {
			angular
					.module(
							'agent-header-controller',
							[ 'user-service', 'login-service',
									'notification-service', 'teamnotice-service','ui.bootstrap',
									'timer', 'xtaas-filters' ])
					.controller(
							'AgentHeaderController',
							[
									'$scope',
									'$rootScope',
									'$location',
									'$modal',
									'$timeout',
									'$state',
									'$window',
									'UserPasswordService',
									'LoginService',
									'user',
									'NotificationService',
									'NoticeService',
									'Pusher',
									function($scope, $rootScope, $location,
											$modal, $timeout, $state, $window,
											UserPasswordService, LoginService,
											user, NotificationService, NoticeService, Pusher) {
										
										$window.userGuidingUserId = user.id;
										$scope.notice = NoticeService.get({userId: user.id});
		
										NoticeService.getUnReadCount({userId: user.id}).$promise.then(function (data) {
											$scope.newNoticesCount = data[0];
										});
										
										Pusher.subscribe(user.id, 'team_notification', function (message) {
											var count = parseInt($scope.newNoticesCount) + 1;
											$scope.newNoticesCount = count.toString();
											$scope.notice = NoticeService.get({userId: user.id});
										});
												
										$scope.markAsRead = function () {
											$scope.notificationShowFlag = !$scope.notificationShowFlag;
											$scope.newNoticesCount = "0";
											if ($scope.notificationShowFlag) {
												NoticeService.markAsRead(user.id);
											}
										}
										$scope.statuses = [ "ONLINE",
												"REQUEST_BREAK",
												"REQUEST_MEETING",
												"REQUEST_TRAINING",
												"REQUEST_OFFLINE",
												"REQUEST_OTHER", "ONBREAK",
												"MEETING", "TRAINING",
												"OFFLINE", "OTHER" ];
										$scope.status = "ONLINE";
										$scope.statusmsg = "ONLINE";
										$scope.prospectName = "No prospect";
										$scope.contactTime = "NA";
										$scope.dialerMode = "NA";
										
										$scope.getStatusClass = function(status) {
											if (status == "REQUEST_BREAK"
													|| status == "ONBREAK") {
												return "onbreak";
											} else if (status == "REQUEST_MEETING"
													|| status == "MEETING") {
												return "meeting";
											} else if (status == "REQUEST_TRAINING"
													|| status == "TRAINING") {
												return "training";
											} else if (status == "REQUEST_OFFLINE"
													|| status == "OFFLINE") {
												return "offline";
											} else if (status == "REQUEST_OTHER"
													|| status == "OTHER") {
												return "other";
											} else if (status == "ONLINE") {
												return "online";
											}
										}

										$scope.removeUnderScore = function(text) {
											return text.replace('_', ' ');
										}
										
										$scope.getStatusMessages = function(text) {
											if (text == "REQUEST_BREAK") {
												text = "BREAK REQUESTED";
											} else if (text == "REQUEST_MEETING") {
												text = "MEETING REQUESTED";
											} else if (text == "REQUEST_TRAINING") {
												text = "TRAINING REQUESTED";
											} else if (text == "REQUEST_OFFLINE") {
												text = "OFFLINE REQUESTED";
											} else if (text == "REQUEST_OTHER") {
												text = "OTHER REQUESTED";
											}
											return text.replace('_', ' ');
										}

										$scope.statusClose = function() {
											console.log('click to close');
											angular
													.element(
															'#available-status')
													.trigger('click');
										}

										$scope.changeStatus = function(status) {
											/// console.log($scope.getStatusValue(status));
											$timeout(function() {
												angular.element('#available-status').trigger('click'); // Hack as the popover doesn't support close on window actions
											}, 0);
											if(status == 'ONLINE' && ($scope.status == 'ONBREAK' || $scope.status == 'MEETING' || $scope.status == 'TRAINING' || $scope.status == 'OFFLINE')){
						                        $rootScope.$broadcast("prospect-timer-reset");
						                        $rootScope.$broadcast("prospect-timer-start");
						                    }
											$scope.oldStatus = $scope.status;
											$scope.status = status;
											if (status != "ONLINE") {
												$scope.statusmsg = status;
											} else {
												$scope.statusmsg = status;
											}
											$rootScope.$broadcast(
													"onChangeStatus", status);
											$rootScope.$broadcast(
													"saveOldStatus", $scope.oldStatus);
										}

										 $scope.$on('checkSt', function (event, oldSt,newSt) {
											 if(oldSt == '' && newSt == 'ONLINE'){
												 $rootScope.$broadcast("prospect-timer-reset");
							                     $rootScope.$broadcast("prospect-timer-start");
											 }
										 })
										
										$scope
												.$on(
														"onCampaignChange",
														function(event,
																campaign) {
															if (campaign == null) {
																$scope.dialerMode = "NA";
																$$scope.statuses = [
																		"ONLINE",
																		"REQUEST_BREAK",
																		"REQUEST_MEETING",
																		"REQUEST_TRAINING",
																		"REQUEST_OFFLINE",
																		"REQUEST_OTHER",
																		"ONBREAK",
																		"MEETING",
																		"TRAINING",
																		"OFFLINE",
																		"OTHER" ];
															} else {
																$scope.dialerMode = campaign.dialerMode;
																$scope.statuses = [
																		"ONLINE",
																		"REQUEST_BREAK",
																		"REQUEST_MEETING",
																		"REQUEST_TRAINING",
																		"REQUEST_OFFLINE",
																		"REQUEST_OTHER",
																		"ONBREAK",
																		"MEETING",
																		"TRAINING",
																		"OFFLINE",
																		"OTHER" ];
															}
														});

										$scope.$on("onStartCall", function(
												event, time) {
											$scope.contactTime = time;
										});

										$scope.$on("onProspectChange",
												function(event, name) {
													$scope.prospectName = name;
												});

										$scope
												.$on(
														"prospect-timer-reset",
														function(event) {
															$scope
																	.$broadcast("timer-reset");
															$scope.prospectName = "No prospect";
															$scope.contactTime = "NA";
														});

										$scope
												.$on(
														"prospect-timer-start",
														function(event) {
															$scope
																	.$broadcast("timer-start");
															$scope.contactTime = (new Date())
																	.toLocaleTimeString()
															$scope.timerRunning = true;
														});

										$scope
												.$on(
														"prospect-timer-stop",
														function(event) {
															$scope
																	.$broadcast("timer-stop");
															$scope.timerRunning = false;
														});

										$scope
												.$on(
														"onStatusApproved",
														function(event,
																statusValue) {
															if (statusValue != $scope.status) { // This is when 
																if ($scope.status != 'OFFLINE'
																		&& $scope.status != 'ONBREAK'
																		&& $scope.status != 'OTHER') {
																	if (statusValue == "OFFLINE") {
																		$rootScope.$broadcast("whisper-stop");
																	}
																	$scope.status = statusValue;
																}
															} else {
																$scope.oldStatus = $scope.status;
															}
														})

										$scope
												.$on(
														"onRequestStatusApproved",
														function(event,
																statusValue) {
															$scope.statusmsg = statusValue;
														})

										$scope
												.$on(
														"onStatusRejected",
														function(event,
																statusValue) {
															if (statusValue == $scope.status) {
																$scope.status = $scope.oldStatus;
															}
														})

										$scope.user = user;

										$scope.logout = function() {

											if ($scope.status != "OFFLINE") {
												NotificationService
														.log('error',
																'You have to be in OFFLINE status before logging out.');
											} else {
												var temp = LoginService
														.logout();
												temp.then(function() {
													$location.path('/login');

												});
											}
										};

										$scope.passwordValid = false;
										$scope.changePassword = function() {
											var modalInstance = $modal
													.open({
														templateUrl : 'changePasswordModal.html',
														controller : ModalInstanceCtrl,
														resolve : {
															userId : function() {
																return $scope.user.id;
															}
														}
													});

										};

										var ModalInstanceCtrl = function(
												$scope, $modalInstance, userId) {
											$scope.ok = function(
													currentPassword,
													confirmPassword) {
												$scope.password = {
													newPassword : confirmPassword,
													currentPassword : currentPassword
												};
												if ($scope.myForm.$valid) {
													UserPasswordService.update(
															{
																id : userId
															}, $scope.password).$promise
															.then(function(data) {
																NotificationService
																		.log(
																				'success',
																				'Password changed successfully.');
																$modalInstance
																		.dismiss('cancel');
															})
												} else {
													$scope.passwordValid = true;
												}
											};

											$scope.cancel = function() {
												$modalInstance
														.dismiss('cancel');
											};
										};
									} ])
					.directive(
							'match',
							function() {
								return {
									require : 'ngModel',
									restrict : 'A',
									scope : {
										match : '='
									},
									link : function(scope, elem, attrs, ngModel) {
										scope
												.$watch(
														function() {
															return (ngModel.$pristine && angular
																	.isUndefined(ngModel.$modelValue))
																	|| scope.match === ngModel.$viewValue;
														},
														function(currentValue,
																previousValue) {
															ngModel
																	.$setValidity(
																			'match',
																			currentValue);
														});
									}
								};
							});
		});
