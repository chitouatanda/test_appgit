define(['angular', 'twilio'], function(angular, twilio) {
    angular.module('twilio-service', [])
    .factory("twilioService", function($q, $timeout) {
    	var connection = null;
    	var connectionDeferred = null;
    	var inProgress = false;
    	
    	function disconnect() {
			if (connection != null) {
				return connection.disconnect();
			}
		}
    	
    	twilio.error(function (error) {
    		console.log("Error occured:");
    		console.log(error);
    		if (connectionDeferred != null) { // As twilio error cannot be corelated, any error condition when the connection is made is correlated to connection itself
				twilio.disconnectAll();
    			connectionDeferred.reject(error);
				connectionDeferred = null;
				inProgress = false;
				console.log("Error occurred. Disconnected from Twilio");
			}
	    });
    	
    	twilio.ready(function() {
            console.log("Twilio Device is ready");
        });
     
        twilio.offline(function() {
        	disconnect();
        });
	 
	    twilio.connect(function (conn) {
	    	if (inProgress) {
	    		console.log("Successfully established Client call.");
	    		connection = conn;
	    		connectionDeferred.resolve(conn.parameters);
	    		connectionDeferred = null;
	    	} else {
	    		// This might occur if there is an error unrelated to connection so that the current inprogress call is terminated.
	    		console.log("Connection successfully established but there was an error prior to this, so terminating the call");
	    		conn.disconnect();
	    	}
	    });
	
	    twilio.disconnect(function (conn) {
	    	inProgress = false;
	    	connection = null;
	    	console.log("Call ended");
	    });
	    
    	return {
    		connect: function(token, params) {
    			if (!inProgress) {
	    			connectionDeferred = $q.defer();
	    			// In synchronized errors connectionDeferred is getting null immediately
    				var connectionPromise = connectionDeferred.promise;
    				console.log("Setting up Twilio Device.");
    				twilio.setup(token.data, {region: 'sg1', debug: false});
    				$timeout(function() {
    					// The device is now ready
    					console.log("Initiating Twilio Client Call.");
        				twilio.connect(params);
        				inProgress = true;
    				}, 3000); //a hack (recommended by Twilio support) to try connecting after 3 secs of setup as Twilio does not notify when setup is complete. Twilio just makes ready callback only during first setup.
	    			return connectionPromise;
    			} else {
    				throw "A call already in progress";
    			}
    		},
    		
    		isInProgress: function() {
    			return inProgress;
    		},
    		
    		toggleMute: function() {
    			if (connection != null) {
    				connection.mute(!connection.isMuted());
    			}
    		},
    		
    		getMuteStatus: function() {
    			if (connection != null) {
    				return connection.isMuted();
    			}
    			return false;
    		},
    		
    		disconnect: disconnect,
    		
    		sendDigits: function(digit) {
    			if (connection != null) {
    	    		connection.sendDigits(digit);
    			} else {
    				throw "Not a valid connection. Cannot send DTMF: "+ digit;
    			}
    		}
    	}
    })
});
