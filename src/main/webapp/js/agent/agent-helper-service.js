define(['angular', 'agent/agent-service'], function(angular) {
    angular.module('agent-helper-service', ['agent-service'])
    .factory('agentHelperService', function(assetSenderService) {
    	    	
    	return {
    		parent: {},
    		getQuestionType: getQuestionType,
    		sendAsset: sendAsset,
    		addNote: addNote,
    		isNoteEnabled: isNoteEnabled,
    		getProspectName: getProspectName,
    		getProspectCity: getProspectCity,
    		getProspectCompany: getProspectCompany,
    		getProspectAddress: getProspectAddress,
    		getProspectPhone: getProspectPhone
    	};
    	
    })
});

