define(['angular'], function(angular) {
    angular.module('ui-helper-service', [])
    .factory('uiHelperService', function() {
    	var changeStep = function(step) {
    		this.parent.currentStep = step;
    	};
    	
    	var toggleDialPad = function() {
    		this.parent.showDialPad = !this.showDialPad;
    	}
    	
    	var resetStep = function() {
    		this.parent.currentStep = "lead Details";
    	}
    	
    	var setAjaxWaiting = function() {
    		this.parent.ajaxWaiting = true;
    	}
    	
    	var resetAjaxWaiting = function() {
    		this.parent.ajaxWaiting = false;
    	}
    	
    	return {
    		apply: function($scope) {
    			$scope.parent.currentStep = "lead Details";
    			$scope.changeStep = changeStep;
    			$scope.resetStep = resetStep;
    			$scope.parent.showDialpad = true;
    			$scope.toggleDialPad = toggleDialPad;
    			$scope.parent.ajaxWaiting = false;
    			$scope.setAjaxWaiting = setAjaxWaiting;
    			$scope.resetAjaxWaiting = resetAjaxWaiting;
    			
    	    }
    	}
    });
});