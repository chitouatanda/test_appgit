define(['angular', 'angular-resource'], function(angular) {
    angular.module('agent-service', ['ngResource'])
    .factory("currentCampaignService", function ($resource) {
        return $resource("/spr/agent/currentcampaign");
    })
    .factory("currentProspectService", function ($resource) {
        return $resource("/spr/agent/currentprospect");
    })
    .factory("agentStatusService", function ($resource) {
        return $resource("/spr/agent/agentcommand");
    })
    .factory("agentOfflineStatusService", function ($resource) {
        return $resource("/spr/agent/offline");
    })
    .factory("prospectCallService",function($resource) {
        return $resource("/spr/agent/prospectcall");
    })
    .factory("assetSenderService",function($resource) {
        return $resource("/spr/campaign/:campaignId/activeasset/deliver");
    })
    .factory("conferenceService", function($resource) {
        return $resource("/spr/call/:callsid/:callControlProvider/:isConferenceCall");
    })
    .factory("callHoldService", function($resource) {
        return $resource("/spr/call/hold/:callsid/:usePlivo/:isConferenceCall");
    })
    .factory("callUnholdService", function($resource) {
        return $resource("/spr/call/unhold/:callsid/:usePlivo/:isConferenceCall");
    })
    .factory("callDTMFService", function($resource) {
        return $resource("/spr/call/dtmf/:callsid");
    })
    .factory("callStatusService", function($resource) {
        return $resource("/spr/call/status");
    })
    .factory("supervisorInvitationService", function($resource) {
        return $resource("/spr/agent/supervisor/invite/call");
    })
    .factory("getDisplayLabelService", function($resource) {
        return $resource("/spr/label/:labelentity/:key", null, {
            get: {
                isArray: false
            }
        });
    })
    .factory("dispositionStatusService", function($resource) {
        return $resource("/spr/masterdata/dispositionstatus/:dispositiontype", {}, {
            get: {
                isArray: true
            }
        });
    }).factory("getStateService", function($resource) {
        return $resource("/spr/geography/states", {}, {
            get: {
                isArray: true
            }
        });
    }).factory("getCallbackTimezoneService", function($resource) {
        return $resource("spr/agent/callbackconfig/:id", null, {
            get: {
                isArray: false
            }
        });
    }).factory("twilioTokenService", function($http) {
        return {
            getToken: function() {
                return $http.get("/spr/agent/twiliotoken");
            },
            getWhisperToken: function() {
                return $http.get("/spr/agentwhisper/twiliotoken");
            }
        }
    }).factory("getTrainingDocs", function($resource) {
        return $resource("spr/agent/training/docs", {}, {
            get: {
                isArray: true
            }
        });
   }).factory("getDialerStatus", function($resource) {
	   	 return $resource("spr/dialer/campaignrunstatus/:campaignid", null, {
	         get: {
	             isArray: false
	         }
	     });
    }).factory("researchAnDialService", function($resource) {
	   	 return $resource("", {}, {
	   		saveProspectDetails: {
	        	 method: 'POST',
	        	 url: "/spr/prospectcalllog/create",
	        	 headers : {
	        		 'Content-Type': 'application/json'
	        	 }
	         },
            createProspectingDetails: {
                method: 'POST',
                url: "/spr/prospectcalllog/prospecting/create",
                headers : {
                    'Content-Type': 'application/json'
                }
            },
	         registerCall: {
	        	 method: 'POST',
	        	 url: "/spr/call/:callsid",
	        	 headers : {
	        		 'Content-Type': 'application/json'
	        	 }
	         },
	         allocateAgent: {
	        	 method: 'GET',
	        	 url: "/spr/agent/allocate/:callsid/:dialerMode/:agentType"
	         },
	         checkDNC: {
	             method: 'POST',
	             url: "/spr/prospectcalllog/dnc",
	             headers : {
	                  'Content-Type': 'application/json'
	             }
             },
             leadIQSearchInfo: {
                method: 'POST',
                url: "/spr/leadiq/search",
                headers : {
                     'Content-Type': 'application/json'
                }
            },
            leadIQSearchInfoMultiple: {
               method: 'POST',
               url: "/spr/leadiq/searchmultiple/:campaignId/:agentId",
               isArray: false,
               headers : {
                    'Content-Type': 'application/json'
               }
           },
           leadIQFetchProspect: {
                   method: 'GET',
                   url: "/spr/leadiq/fetchprospect/:sourceId",
                   isArray: false,
                   headers : {
                       'Content-Type': 'application/json'
                   }
           },
            getOutboundNumberForRedial: {
                method: 'POST',
                url: "/spr/plivo/redial/outboundnumber",
                isArray: false,
                headers : {
                    'Content-Type': 'application/json'
                }
            }
	     });
    }).factory("manualDialingService", function($resource) {
	   	 return $resource("/spr/twilio/dial/:prospectCallId", null, {
	         get: {
	        	 method: 'GET',
	        	 isArray: false
	         }
	     });
    }).factory("getCampaignDetailsForAgentService", function($resource) {
        return $resource("/spr/rest/campaigndetails/agent/:id", null, {
         get: {
             method: 'GET',
             isArray: false
         }
     });
    }).factory("PropertyService", function($resource) {
        return $resource("/spr/applicationproperties/:name", null, {
         get: {
             method: 'GET',
             isArray: false
         }
        });
    }).factory("getCountryDetailsService", function($resource) {
        return $resource("/spr/outboundnumbers/countrydetails", {}, {
            get: {
                isArray: true
            }
        });
    }).factory("AgentDetailService",function($resource) {
		return $resource("/spr/agent/details",{},{
			query: {
	            method: 'GET',
	            params: {}
	        }
		});
	}).factory("PlivoService",function($resource) {
		return $resource("",{},{
			createEndpoint: {
	            method: 'GET',
	            isArray: false,
	            url: "/spr/plivo/endpoint/:username"
	        },
	        deleteEndpoint: {
	            method: 'DELETE',
	            isArray: false,
	            url: "/spr/plivo/endpoint/:endpointId"
	        }
		});
	}).factory("TelnyxService",function($resource) {
		return $resource("",{},{
			createConnection: {
	            method: 'GET',
	            isArray: false,
	            url: "/spr/telnyx/connection/:username"
	        },
	        deleteConnection: {
	            method: 'DELETE',
	            isArray: false,
	            url: "/spr/telnyx/connection/:connectionId"
            },
            createIncomingCallForAgent: {
                method: 'POST',
                url: "/spr/telnyx/callagent/auto",
                headers : {
                     'Content-Type': 'application/json'
                }
            },
            startRecording: {
                method: 'POST',
                isArray: false,
	            url: "/spr/telnyx/record/start"
            },
            createCallForManualDial: {
                method: 'POST',
                isArray: false,
	            url: "/spr/telnyx/callagent/manual"
            },
            hangupCall: {
                method: 'POST',
                isArray: false,
	            url: "/spr/telnyx/call/hangup"
            },
            sendDtmf: {
                method: 'POST',
                isArray: false,
	            url: "/spr/telnyx/call/senddtmf"
            },
            getOutboundNumberForRedial: {
                method: 'POST',
                url: "/spr/telnyx/redial/outboundnumber",
                isArray: false,
                headers : {
                    'Content-Type': 'application/json'
                }
            }
		});
	}).factory("SignalwireService",function($resource) {
		return $resource("",{},{
			createEndpoint: {
	            method: 'GET',
	            isArray: false,
	            url: "/spr/signalwire/endpoint/:username"
	        },
	        deleteEndpoint: {
	            method: 'DELETE',
	            isArray: false,
	            url: "/spr/signalwire/endpoint/:endpointId"
	        },
            getOutboundNumberForRedial: {
                method: 'POST',
                url: "/spr/signalwire/redial/outboundnumber",
                isArray: false,
                headers : {
                    'Content-Type': 'application/json'
                }
            }
		});
	}).factory("GetOrganizationService", function ($resource) {
        return $resource("/spr/organization/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        })
    }).factory("ApplicationPropertyService", function ($resource) {
        return $resource("spr/applicationproperties", null, {
        	query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("getStateDetailService", function($resource) {
        return $resource("spr/countryState/:countryName", null, {
            get: {
                isArray: true
            }
        });
    }).factory("getCountryDetailService", function($resource) {
        return $resource("spr/countryState", null, {
            get: {
                isArray: true
            }
        });
    }).factory("NotifySupervisorService", function ($resource) {
        return $resource("/spr/teamnotice/:agentId/supervisor/:mode", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        })
    }).factory("GetMissedCallbackService", function ($resource) {
        return $resource("/spr/callback/:agentId", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        })
    }).factory("AgentNetworkStatusUpdateService", function ($resource) {
        return $resource("", {}, {
            getAgent: {
                method : 'GET',
                isArray : false,
                url: "/spr/rest/agent/getnetworkstatus/:username"
            },
            postAgent: {
                method : 'GET',
                isArray : false,
                url: "/spr/rest/agent/postnetworkstatus/:username/:networkstatus"
            }
        })
    }).factory("ProviderDetailService",function($resource) {
		return $resource("/spr/campaign/xtaas/data/provider",{},{
			query: {
	            method: 'GET',
                isArray: true
	        }
		});
	})
	.factory("agentMonitoringService",function($resource) {
		return $resource("", {}, {
	   		printLog: {
	        	 method: 'POST',
	        	 url: "/spr/agent/callmonitoring",
	        	 headers : {
	        		 'Content-Type': 'application/json'
	        	 }
	         },
		});
    })
});
