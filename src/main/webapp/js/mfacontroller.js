define(['angular','user-service', 'login-service', 'notification-service','mfa-service','angular-localStorage'], function(angular) {
	angular.module('MfaController', ['user-service', 'login-service', 'notification-service','mfa-service','ngStorage'])
		.controller('MfaController', [
            '$scope', 
            'ChangeForgotPasswordService',
            'NotificationService',
			'$location',
			'user',
			'MfaService',
			'$modal',
			'$timeout',
			'LoginService',
			'$rootScope',
			'$localStorage',
             function($scope, ChangeForgotPasswordService, NotificationService, $location,user,MfaService,$modal,$timeout,LoginService,$rootScope,$localStorage) {
            	$scope.invalid = false;
				$scope.loading = false;
				$scope.loggedInUser = user;
				$scope.skipMFABtn = false;
				MfaService.getUser({username:btoa($scope.loggedInUser.id)}).$promise.then(function(data){
					$scope.skipMFABtn = data.enforeMFA;
				},function(error){
					console.log(error);
				});
				$scope.btnMFA = function() {
					$scope.fetchQRCode($scope.loggedInUser.id);	
				};

				$scope.skipMFA = function () {
					$scope.loading = true;
					$location.path("/launch");
					$rootScope.isLaunch = true;
				};

				$scope.fetchQRCode = function(userName) {
					$scope.qrCode = '';
					MfaService.getQRCode({username:userName}).$promise.then(function(data){
						if(data != undefined && data != null) {
							for(i = 0; i <= 1500; i++) {
								if (data[i] != undefined) {
									$scope.qrCode = $scope.qrCode + data[i];
								}
							}
						}
						if($scope.qrCode != null && $scope.qrCode != undefined && $scope.qrCode != '') {
							$scope.showBarcodeAndTotpDialog($scope,userName);
						}else {
							$scope.callNotificationService("Failed to load QR code");
						}
					},function(error){
						console.log(error);
						$scope.callNotificationService("Failed to load QR code");
					});
				}

				$scope.showBarcodeAndTotpDialog = function($scope,userName) {		                                	
					$scope.showBarcodeAndTotpmodalInstance = $modal.open({
						scope : $scope,
						templateUrl: 'totp.html',
						controller: BarcodeAndTotpModalInstanceCtrl,
						windowClass : 'forgotpassword-modal',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							userName: function(){
								return userName;
							}
						}
					});
				};
				var BarcodeAndTotpModalInstanceCtrl = function ($scope,userName) {
					$scope.completeMFA = function() {
						$scope.fetchTOTPCode($scope,userName);
					};
				};

				$scope.fetchTOTPCode = function($scope,userName) {
					if($scope.userotp != null && $scope.userotp != undefined && $scope.userotp != ''){
						var s = new String($scope.userotp);
						var lengthofString = s.length;
						if(lengthofString > 6 || lengthofString < 6){
							$scope.callNotificationService("The 2FA code should be 6 digits only.");
							return;
						} else if(lengthofString == 6) {
							$scope.totpCode = '';
							MfaService.getTOTPCode({username:userName,totp:$scope.userotp}).$promise.then(function(data){
								if(data != undefined && data != null) {
									for(i = 0; i <= 10; i++) {
										if (data[i] != undefined) {
											$scope.totpCode = $scope.totpCode + data[i];
										}
									}
								}
								if ($scope.totpCode == 'true') {
									$scope.showBarcodeAndTotpmodalInstance.close();
									$location.path("/launch");
									$rootScope.isLaunch = true;
								} else {
									$scope.callNotificationService("The 2FA code did not match your account.");
								}
							},function(error){
								console.log(error);
								if (error.message == "method is not defined") {
									$timeout(function() {
										var temp = LoginService.logout();
										temp.then(function() {
											$scope.showBarcodeAndTotpmodalInstance.close();
											$rootScope.isUserLoggedIn = false;				
											$localStorage.$reset();	
											$location.path('/login');
										});
									},6000);
								}
							});
						}
					}else {
						$scope.callNotificationService("Please enter TOTP code.");
					}
					
				}

				$scope.callNotificationService = function(msg) {
					$scope.loading = false;
					NotificationService.log('error',msg);
					$timeout(function() {
						NotificationService.clear();
					}, 6000);
				}
            }]
		);
});