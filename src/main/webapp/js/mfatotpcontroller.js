define(['angular','user-service', 'login-service', 'notification-service','mfatotp-service','angular-localStorage'], function(angular) {
	angular.module('MfaTotpController', ['user-service', 'login-service', 'notification-service','mfatotp-service','ngStorage'])
		.controller('MfaTotpController', [
            '$scope', 
            'ChangeForgotPasswordService',
            'NotificationService',
			'$location',
			'user',
			'MfaTotpService',
			'$modal',
			'$timeout',
			'LoginService',
			'$rootScope',
			'$localStorage',
             function($scope, ChangeForgotPasswordService, NotificationService, $location,user,MfaTotpService,$modal,$timeout,LoginService,$rootScope,$localStorage) {
            	$scope.invalid = false;
				$scope.loading = false;
				$scope.loggedInUser = user;
				
				$scope.continue = function() {
					$scope.fetchTOTPCode($scope.loggedInUser.id);
				};

				$scope.fetchTOTPCode = function(userName) {
					if($scope.userotp != null && $scope.userotp != undefined && $scope.userotp != ''){
						var s = new String($scope.userotp);
						var lengthofString = s.length;
						if(lengthofString > 6 || lengthofString < 6){
							$scope.callNotificationService("The 2FA code should be 6 digits only.");
							return;
						} else if(lengthofString == 6){
							$scope.totpCode = '';
							MfaTotpService.getTOTPCode({username:userName,totp:$scope.userotp}).$promise.then(function(data){
								if(data != undefined && data != null) {
									for(i = 0; i <= 10; i++) {
										if (data[i] != undefined) {
											$scope.totpCode = $scope.totpCode + data[i];
										}
									}
								}
								if ($scope.totpCode == 'true') {
									$location.path("/launch");
									$rootScope.isLaunch = true;
								} else {
									$scope.callNotificationService("The 2FA code did not match your account.");
								}
							},function(error){
								console.log(error);
								if (error.message == "method is not defined") {
									$timeout(function() {
										var temp = LoginService.logout();
										temp.then(function() {
											$rootScope.isUserLoggedIn = false;				
											$localStorage.$reset();	
											$location.path('/login');
										});
									},6000);
								}
							});
						}
					}else {
						$scope.callNotificationService("Please enter TOTP code.");
					}
					
				}

				$scope.callNotificationService = function(msg) {
					$scope.loading = false;
					NotificationService.log('error',msg);
					$timeout(function() {
						NotificationService.clear();
					}, 6000);
				}
            }]
		);
});