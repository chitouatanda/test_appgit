define(['angular', 'angular-ui-router', 'notification-service'], function(angular) {
	angular.module('xtaas.interceptors', ['ui.router', 'notification-service'])
		.config(['$httpProvider', function($httpProvider) {
			  $httpProvider.interceptors.push(function($q, $rootScope, $injector, $interval) {
				 return {
		     			 request: function (config) {
		     				 
		     				 var url = config.url;
		     				 
		     				 if (url.indexOf(".html") == -1 && url.indexOf("j_spring_security_logout") == -1) {
		     					// console.log("Request sent to url " + url);
		     					$rootScope.timerStart = new Date().getTime();
		     				 }
		     				 
		     				 if (url == "/j_spring_security_logout") {
		     					$interval.cancel($rootScope.sessionTimeOutPromise);
		     				 }
		     				return  config;
		     			}
		     		};
		     	 });
		}])
});