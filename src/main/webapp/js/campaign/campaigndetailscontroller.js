define(['angular', 'campaign/campaign-service', 'notification-service'], function(angular) {
	angular.module('CampaignDetailsController', ['campaign-service', 'notification-service'])
		.controller('CampaignDetailsController', [
		                                      '$scope', 
		                                      '$modal',
		                                      '$stateParams',
		                                      'CampaignService',
		                                      'TeamSearchService',
		                                      'CampaignCommandService',
		                                      'GlobalCampaignTypes',
		                                      '$state',
		                                      '$location',
		                                      'NotificationService',
		                                      '$localStorage',
		                                      'attributeMappings',
											  '$filter',
											  'user',
		                                      function ($scope, $modal, $stateParams, CampaignService, TeamSearchService,CampaignCommandService, GlobalCampaignTypes, $state, $location,  NotificationService, $localStorage, attributeMappings, $filter, user) {
												

												/** DATE :- 11/01/2021
												 *  If Logged in user role is "CAMPAIGN_MANAGER" then only user will be able to access the page
												 *  otherwise redirected to login page. 
												 */
												$scope.isCampaignManagerLoggedIn = function() {
													var role = $filter('filter')(user.roles, "CAMPAIGN_MANAGER")[0];
													if (role != 'CAMPAIGN_MANAGER') {
														$state.go('login');
														return;
													}
												}
												$scope.isCampaignManagerLoggedIn();
												// END
				
		                                       $scope.attributeMappings = attributeMappings;
		                                       $scope.uiRouterState = $state;
		                                       $scope.campaignAction = "";                                    		  
		                                       CampaignService.get({
		                                    			  id: $stateParams.campaignId
		                                       }).$promise.then(function(getData){
		                                    			  $scope.loading = false;
		                                    			  $scope.campaign	= getData;
		                                    			  $scope.campaignAction  = $scope.campaign.status;    
		                                    			  $scope.supervisorData =  $scope.campaign.supervisorId;     
		                                    			  $scope.generalInformation = [
		                                             	                             	{label:"Campaign Name" , value :$scope.campaign.name},
		                                             	                             	{label:"Brand Name" , value :$scope.campaign.brand},
		                                             	                             	{label:"Campaign Type" , value :$scope.campaign.type},
		                                             	                             	{label:"Campaign Runs" , value :"From: "+$filter('date')($scope.campaign.startDate, 'MM/dd/yyyy')+" to "+$filter('date')($scope.campaign.endDate, 'MM/dd/yyyy')},
		                                             	                             	{label:"Target" , value :$scope.campaign.deliveryTarget},
		                                             	                             	{label:"Daily Cap" , value :$scope.campaign.dailyCap}
		                                             	                             	
		                                             	                             ]
		                                             	                            		 
                 	                            		$scope.targetSegment = [
	                                          	                             	{label:"Company Size" , value :""},
	                                          	                             	{label:"Role" , value :""},
	                                          	                             	{label:"Geo" , value :""},
	                                          	                             	{label:"Budget" , value :""}
	                                          	                             ]
		                        	                                          		
    	                                          		$scope.getSymbolLabel = function(symbol) {
    	                                          			switch (symbol) {
                                              		            case "=":
                                              		                return "is";
                                              		            case "!=":
                                            		                return "is not Equal to";
                                              		            case ">":
                                              		            	return "greater Than";
                                            		            case "<":
                                            		            	return "less Than";
                                            		            case ">=":
                                              		                return "greater than or equals";
                                              		            case "<=":
                                            		                return "less than or equals";
                                              		            default:
                                              		            	return symbol;
    	                                          			}
    	                                          		}
		                                    			  
	                                    			  /* Select Team*/
		                                    		  $scope.selectedCampaignTeams = [];	  
	                                    			  $scope.pramas = { 
                              									"clause": 'ByCurrentSelection',
                              									"campaignId": $stateParams.campaignId,
                              									"page": 0,
	                              								"size": 10,
	                              								"direction": "ASC"
                              							};
	                                    			  TeamSearchService.query({searchstring: JSON.stringify($scope.pramas)}).$promise.then(function(data) {
                            								angular.forEach(data, function(v, k) {                            									
                            									$scope.selectedCampaignTeams.push(v);  
                            								}, '');
                            								
                            							},function(error) {
                            								
                            							});
		                                       });
		                                       
		                                       
		                                       $scope.getLabelAttributeSpecificValue = function(attributeName,value) {
     	                                      		if ($scope.isFromGeography(attributeName)) {
     	                                      			return value;
     	                                      		}
     	                                      		var pickAttributeData = $scope.getAttributePickList(attributeName);
     	                                      		var specificPicklist = $filter('filter')(pickAttributeData, value)[0];
     	                                      		if (!specificPicklist) {
    													return value;
    											  	}
     	                                      		return specificPicklist.label;
     	                                      	};  
		                                    	
     	                                      	$scope.isFromGeography = function (attribute) {
		                                        	  if (attribute == "Area" || attribute == "Country" || attribute == "State" || attribute == "City" ) {
		                                        		  return true;
		                                        	  }
		                                        }
     	                                      	
     	                                      	$scope.openConfirmCampaignControlModal = function(campaignActionAttribute) {
    												$scope.confirmCampaignActionModalInstance = $modal.open({
    													scope : $scope,
    													templateUrl : 'confirmCampaignActionModal.html',
    													controller : ConfirmCampaignActionModalInstanceCtrl,
    													windowClass : 'confirm-Action-modal',
    													resolve: {
    														actionAttribute: function(){
    															return campaignActionAttribute;
    														}
    													}
    												});
    												if(campaignActionAttribute == 'Complete'){
    													 $scope.classes =  "Are you sure you want to mark the campaign as Completed";
    													 $scope.detailMsg = "This disables the campaign and so restarting this will require a new campaign setup.";
    												}
    												if(campaignActionAttribute == 'Pause'){
    													$scope.classes =  "Are you sure you want to mark the campaign as Paused";
    													$scope.detailMsg = "This disables calling on the campaign. You can enable calling on the campaign anytime by moving the campaign from Paused to Running state.";
    												}
    												if(campaignActionAttribute == 'Run'){
    													$scope.classes =  "Are you sure you want to mark the campaign as Running";
    													$scope.detailMsg = "Once the campaign is marked as Running, the dialer will start making calls as agents assigned on the campaign log in."; 
    												}
    												if(campaignActionAttribute == 'Delist'){
    													$scope.classes =  "Are you sure you want to mark the campaign as Delisted";
    													$scope.detailMsg = "This will remove the campaign from the listing and will mark it for subsequent Archival.";
    												}
    												if(campaignActionAttribute == 'Plan'){
    													$scope.classes =  "Are you sure you want to mark the campaign Planning";
    													$scope.detailMsg = "";
    												}
    												if(campaignActionAttribute == 'Stop'){
    													$scope.classes =  "Are you sure you want to mark the campaign to Stop";
    													$scope.detailMsg = "";
    												}
    												if(campaignActionAttribute == 'Resume'){
    													$scope.classes =  "Are you sure you want to mark the campaign to Resume";
    													$scope.detailMsg = "";
    												}
    											};
    											
    											var ConfirmCampaignActionModalInstanceCtrl = function ($scope, actionAttribute) {
    												$scope.confirmAction = function() {													
    													$scope.campaignControl(actionAttribute);
    													$scope.confirmCampaignActionModalInstance.close();
    												};
    												
    												$scope.cancel = function() {
    													$scope.confirmCampaignActionModalInstance.close();
    												}
    											}
    												
     	                                      	
     	                                      	// this method is used when user click on campaign action button (like STOP,COMPLETE, RUN  etc)
     	                                      	$scope.campaignControl = function(actionAttribute){
     	                                      		$scope.loading = true;
     	                                      		$scope.commanddata	={
     	                          	                     "campaignCommand":actionAttribute
     	                          	                 };
     	                            				CampaignCommandService.query({
     	                            	                 id: $stateParams.campaignId
     	                            	             }, $scope.commanddata).$promise.then(function(data) {
     	                            	            	$scope.loading = false;
     	                            	            	 switch (actionAttribute) {
     	                            	            	 case 'Plan': 
     	                            	            		 $scope.campaign.status = "PLANNING";
     	                            						 NotificationService.log('success','Campaign moved to Planning state');
     	                            	         			 break;
     	                            	            	 case 'Run': 
     	                            	            		 $scope.pausevalue=1;
     	                            	            		 $scope.runvalue=0;
     	                            	            		 $scope.campaign.status = "RUNNING";
     	                            						NotificationService.log('success','Campaign moved to Running state');
     	                            	         			break;
     	                            	         		case 'Pause': 
     	                            	         			$scope.resumevalue=1;
     	                            	         			$scope.pausevalue=0;
     	                            	         			$scope.campaign.status = "PAUSED";
     	                            	         			NotificationService.log('success','Campaign Paused successfully');
     	                            	         			break;
     	                            	         		case 'Stop':
     	                            	         			$scope.stopvalue=1;
     	                            	         			$scope.campaign.status = "STOPPED";
     	                            						NotificationService.log('success','Campaign Stopped successfully');
     	                            	         			break;
     	                            	         		case 'Resume':
     	                            	         			$scope.resumevalue=0;
     	                            	         			$scope.pausevalue=1;
     	                            	         			$scope.campaign.status = "RESUME";
     	                            						NotificationService.log('success','Campaign Resumed successfully');
     	                            	         			break;
     	                            	         		case 'Complete':
     	                            	         			$scope.campaign.status = "COMPLETED";
     	                            						NotificationService.log('success','Campaign set as Completed');
     	                            	         			break;
     	                            	         		case 'Delist':
     	                            	         			$scope.campaign.status = "DELISTED";
     	                            						NotificationService.log('success','Campaign is Delisted');
     	                            	         			break;
     	                            	         		default:
     	                            						NotificationService.log('success','Campaign is in Unknown state');
     	                            	            	}
     	                            	            	$scope.loading = true;
     	                            	            	$state.go('listcampaigns');
     	                            	             });
     	                            				$scope.loading = false;
     	                                      	}
     	                                      	
 		                                       $scope.getAttributePickList = function(attributeName) {
 		                                      		  $scope.pickData = [];
 		                                      		  if ($scope.attributeMappings[attributeName] != undefined) {
 		                                      			  angular.forEach($scope.attributeMappings[attributeName].pickList, function(v, k) {
 		                                      				  	$scope.pickData[k]=v;		
 		                                      			  });
 		                                      			}
 		                                      		  return $scope.pickData;
 		                                      	  };
		                                       
 		                                      	$scope.backToList = function() { 
													  $scope.loading = true;		                                            	  
 		                                           	  $state.go('listcampaigns');       		  
 		                                    	}
		                                      
    	  
										}]).filter('capitalize', function() {
										    return function(input, all) {
										        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
										      }
										    });
									});
