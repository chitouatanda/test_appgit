define(['angular', 'angular-resource'], function(angular) {
    angular.module('campaign-service', ['ngResource'])
    .factory("CampaignService", function ($resource) {
        return $resource("/spr/rest/campaign/:id", null, {
            update: {
                method: 'PUT',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
            save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
            query: {
                method : 'GET',
                isArray:false
            },
            remove: {
                method: 'DELETE',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("CampaignCloneService", function ($resource) {
    	return $resource("/spr/rest/campaign/clone/:id", null, {
    		 query: {
                 method : 'GET',
                 isArray: false
             }
    	 });
    }).factory("SupervisorService", function ($resource) {
    	return $resource("/spr/rest/:id/supervisor", null, {
    		 query: {
                 method : 'GET',
                 isArray: true
             }
    	 });
    }).service('UploadURL', function() {
    
        var uploadURL="/spr/file/upload";
    
        return {
            getuploadURL: function() {
                return uploadURL;
            },
            setuploadURL: function(value) {
                uploadURL = value;
            }
		
        }
    }).service('GlobalCampaign', function() {
    
        var globalcampaign = null;
        
        return {
            getGlobalCampaign: function() {
                return globalcampaign;
            },
            setGlobalCampaign: function(value) {
                globalcampaign = value;
            }
           		
        }
    }).service('GlobalBrandName', function() {
    
        var globalBrandName  = null;
        
        return {
        	getGlobalBrandName: function() {
                return globalBrandName;
            },
            setGlobalBrandName: function(value) {
            	globalBrandName = value;
            }
           		
        }
    }).service('GlobalEditCampaign', function() {
    
        var globalEditCampaign = false;
        
        return {
            getGlobalEditCampaign: function() {
                return globalEditCampaign;
            },
            setGlobalEditCampaign: function(value) {
            	globalEditCampaign = value;
            }
           		
        }
    }).service('GlobalCampaignTypes', function() {
    
        var globalcampaigntypes = null;
        return {
           getGlobalCampaignTypes: function() {
                return globalcampaigntypes;
            },
            setGlobalCampaignTypes: function(value) {
            	globalcampaigntypes = value;
            }
		
        }
    }).service('fileUpload', ['$http', function ($http) {
          
         
        this.uploadFileToUrl = function(file, uploadUrl, folder){
        	
            var fd = new FormData();
            fd.append('file', file);
            if(folder == "" || folder == undefined)
            {
            	
            }else{
            	fd.append('folderName', folder);
            	
            }
            
           return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
          /*  .success(function(data, status, headers, config) {
               
              return data;
              
                })*/
            .error(function(data, status, headers, config){
                alert(data.msg);
                	console.log(data);	
                });
        }
    }]).factory("FileUploadTrackService", function ($resource) {
        return $resource("/spr/campaigncontact/:id/upload/status", {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }).factory("GetCampaignService", function ($resource) {
        return $resource("/spr/rest/campaign/:id", {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }).factory("TestLeadSelection", function ($resource) {
        return $resource("/spr/rest/lead/counter", {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }).factory("CRMSelectionService", function ($resource) {
    	return $resource("/spr/rest/crmmapping/crmattribute", {}, {
    		save: {
                url: "/spr/rest/crmmapping/crmattribute/:campaignId",
	            method: 'POST',
	            isArray: false
            },
            getByCampaignId: {
	        	method: 'GET',
                url: "/spr/rest/crmmapping/crmattribute/:campaignId",
                isArray: false
	        },
	        createQuestion: {
                method: 'POST',
                url: "/spr/rest/crmmapping/crmattribute/questions",
                headers : {
                    'Content-Type': 'application/json'
                }
            }
    	});
    }).factory("MasterDataService", function ($resource) {
        return $resource("/spr/masterdata/common/:id");
    }).factory("CRMMappingDataService", function ($resource) {
        return $resource("/spr/rest/crmmapping", {}, {
            query: {
                method: 'GET',
                isArray: false
            },
            getSystemMapping: {
	        	method: 'GET',
                url: "/spr/rest/crmmapping/crmattribute/system",
                isArray: false
	        },
        });
    }).factory("DomainService", function ($resource) {
        return $resource("/spr/masterdata/domain");
    }).factory("AreaService", function ($resource) {
        return $resource('/spr/geography/areas');
    }).factory("CountryService", function ($resource) {
        return $resource('/spr/geography/countries');
    }).factory("StateService", function ($resource) {
        return $resource('/spr/geography/states');
    }).factory("CityService", function ($resource) {
        return $resource('/spr/masterdata/city');
    }).factory("MasterDataService", function ($resource) {
        return $resource("/spr/masterdata/common/:id");
    }).factory("GetCampaignService", function ($resource) {
        return $resource("/spr/rest/campaign/:id", {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }).factory("TeamSearchService", function ($resource) {
        return $resource("/spr/team?searchstring=:searchstring", {}, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("AgentSearchService", function ($resource) {
        return $resource("/spr/agent?searchstring=:searchstring", {}, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("TeamCounterService", function ($resource) {
        return $resource("/spr/team/counter?searchstring=:searchstring", {}, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("AgentCounterService", function ($resource) {
        return $resource("/spr/agent/counter?searchstring=:searchstring", {}, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("InvitedTeamService", function ($resource) { 
        return $resource("/spr/campaign/:id/teaminvitation", {}, {
        	 query: {
                 method: 'GET',
                 isArray: true
             }
        });
    }).factory("ShortlistedTeamService", function ($resource) { 
        return $resource("/spr/campaign/:id/teamshortlisted", {}, {
       	 query: {
                method: 'GET',
                isArray: true
            }
       });
   }).factory("InvitedAgentService", function ($resource) { 
        return $resource("/spr/campaign/:id/agentinvitation", {}, {
        	 query: {
                 method: 'GET',
                 isArray: true
             }
        });
    }).factory("TeamInvitationService", function ($resource) { 
        return $resource("/spr/campaign/:id/teaminvitation", null, {
            query: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("TeamShortlistedService", function ($resource) { 
        return $resource("/spr/campaign/:id/teamshortlisted", null, {
            query: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("CancelTeamShortlistedService", function ($resource) { 
        return $resource("/spr/campaign/:id/teamshortlisted/:teamId", null, {
            remove: {
                method: 'DELETE',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("CancelTeamInvitationService", function ($resource) { 
        return $resource("/spr/campaign/:id/teaminvitation/:teamId", null, {
            remove: {
                method: 'DELETE',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("TeamSelectionService", function ($resource) { 
        return $resource("/spr/campaign/:campaignId/campaignteam", null, {
            save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("AgentSelectionService", function ($resource) { 
        return $resource("/spr/campaign/:campaignId/campaignagent/:agentId", null, {
            save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("AgentInvitationService", function ($resource) { 
        return $resource("/spr/campaign/:id/agentinvitation", null, {
            query: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    }).factory("CampaignStatusService", function ($resource) { 
        return $resource("/spr/rest/campaignstatus", {}, {
        	 query: {
                 method: 'GET',
                 isArray: true
             }
        });
    }).factory("CampaignTypeService", function ($resource) { 
        return $resource("/spr/rest/campaigntypes", {}, {
            query: {
            	method: 'GET',
                isArray: true
            }
        });
    }).factory("CampaignCommandService", function ($resource) { 
        return $resource("/spr/campaign/:id/campaigncommand", null, {
            query: {
            	 method: 'POST',
                 headers : {
                     'Content-Type': 'application/json'
                 }
            }
        });
    }).factory("OrganizationService", function ($resource) { 
        return $resource("/spr/organization/:id", null, {
            query: {
            	 method: 'GET',
                 headers : {
                     'Content-Type': 'application/json'
                 }
            }
        });
    }).factory("UserOrganizationService", function ($resource) { 
        return $resource("/spr/rest/user/orgexperience", null, {
            query: {
            	 method: 'GET',
                 headers : {
                     'Content-Type': 'application/json'
                 }
            }
        });
    }).factory("PreviousWorkedCampaignsService", function ($resource) { 
        return $resource("/spr/team/previousworkedcampaigns", null, {
            query: {
            	 method: 'GET',
                 headers : {
                     'Content-Type': 'application/json'
                 }
            }
        });
    }).factory("GetOrganizationService", function ($resource) {
        return $resource("/spr/organization/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        })
    }).factory("SendEmailService", function ($resource) {
        return $resource("/spr/rest/plancampaign/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("campaignSearchService", function ($resource) {
        return $resource("spr/rest/searchcampaign?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    }).factory("getSuppressionListService", function ($resource) {
        return $resource("/spr/campaignsuppression/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            }, 
            suppression: {
	        	method: 'GET',
                url: "/spr/campaignsuppression/:campaignId/:suppressionType",
                isArray: false
	        }
        });
    }).factory("abmListCountService", function ($resource) {
        return $resource("/spr/abmlist/count/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            },
            remove: {
                url: "/spr/abmlist/remove/:campaignId",
                method: 'DELETE',
                isArray : false
            },
            multidownload: {
	        	method: 'GET',
                url: "/spr/abmlist/:campaignId",
                isArray: false
	        }
        });
    }).factory("clientMappingService",function ($resource) {
        return $resource("/spr/clientMapping/download/new/:campaignId", null, {
            download: {
	        	method: 'GET',
                isArray: false
	        }
        });
    })
});
