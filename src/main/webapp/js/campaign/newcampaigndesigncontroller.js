define(['angular', 'angular-ui-select', 'campaign/campaign-service', 'notification-service', 'xtaas-constants', 'angular-x-editable', 'ngFileUpload'], function(angular) {
	angular.module('NewCampaignDesignController', ['campaign-service', 'ui.select', 'ngCookies', 'xtaas.constants', 'xeditable', 'ngFileUpload'])
	.controller('NewCampaignDesignController', [
	                                      '$scope',
	                                      '$cookieStore',
	                                      '$stateParams',
	                                      '$modal',
	                                      '$state',
	                                      '$window',
	                                      '$location',
	                                      'GlobalCampaign',
	                                      'GlobalCampaignTypes',
	                                      'CRMSelectionService',
	                                      'CRMMappingDataService',
	                                      'GlobalBrandName',
	                                      'GlobalEditCampaign',
	                                      'GetCampaignService',
	                                      'CampaignService', 
	                                      'SupervisorService',
	                                      'CampaignTypeService',
	                                      'MasterDataService', 
	                                      'restrictions',
	                                      'attributeMappings',
	                                      '$filter',
	                                      'domains',
	                                      'TeamSelectionService', 
	                                      'AgentSelectionService',
	                                      'CountryService',
	                                      'arealist',
	                                      'countrylist',
	                                      'statelist',
	                                      'StateService',
	                                      'CityService', 
	                                      'ratingilist',
	                                      'timezonelist',
	                                      'langitemlist',
	                                      'TeamInvitationService',
	                                      'AgentInvitationService',
	                                      'TeamSearchService',
	                                      'AgentSearchService',
	                                      'InvitedTeamService',
	                                      'InvitedAgentService',
	                                      'TeamCounterService',
	                                      'AgentCounterService',
	                                      'TeamShortlistedService',
	                                      'ShortlistedTeamService',
	                                      'NotificationService',
	                                      '$interval',
	                                      '$rootScope',
	                                      '$localStorage',
	                                      'CancelTeamShortlistedService',
	                                      'CancelTeamInvitationService',
	                                      'fileUpload',
	                                      'UploadURL',
	                                      'OrganizationService',
	                                      'PreviousWorkedCampaignsService',
	                                      'UserOrganizationService',
	                                      'XTAAS_CONSTANTS',
										  'Upload',
										  'GetOrganizationService',
										  'user',
										  'SendEmailService',
										  'CampaignCloneService',
										  'getSuppressionListService',
										  'abmListCountService',
										  'clientMappingService',
										  'Pusher',
	                                      function ($scope,$cookieStore,$stateParams, $modal,$state,$window ,$location, GlobalCampaign,GlobalCampaignTypes, CRMSelectionService , CRMMappingDataService ,GlobalBrandName,GlobalEditCampaign, GetCampaignService, CampaignService,SupervisorService, CampaignTypeService, MasterDataService, restrictions, attributeMappings, $filter, domains, 
	                                    		   TeamSelectionService, AgentSelectionService, CountryService, arealist, countrylist,statelist, StateService, CityService, ratingilist,timezonelist,langitemlist, TeamInvitationService,AgentInvitationService,TeamSearchService,
	                                    		   AgentSearchService,InvitedTeamService,InvitedAgentService, TeamCounterService,AgentCounterService,TeamShortlistedService, ShortlistedTeamService, NotificationService, $interval, $rootScope, $localStorage, CancelTeamShortlistedService, CancelTeamInvitationService, 
	                                    		   fileUpload, UploadURL, OrganizationService, PreviousWorkedCampaignsService, UserOrganizationService, XTAAS_CONSTANTS, Upload, GetOrganizationService, user, SendEmailService,CampaignCloneService,getSuppressionListService,abmListCountService,clientMappingService,Pusher) {
	                                    	
	                                    	  $scope.params = $location.search()['byId'];//for clone functionality
											  $scope.alerts = [];

											  if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("CAMPAIGN_MANAGER")) {
													$state.go('login');
													return;
											   }

	                                    	  $scope.campaign = {};
											  $scope.freeText = "Agent can enter free text";
	                                    	  $scope.tempInformationCriteria = [];	                                    	  
											  $scope.attributeMappings = angular.copy(attributeMappings);//avoiding references
											  
											  /*  ############### Date :- 12/12/18 Get the organization of loggedInUser  Start ############### */
											  $scope.loggedInUserOrganization = {};
											  GetOrganizationService.query({id : user.organization}).$promise.then(function(organization) {
												  $scope.loggedInUserOrganization = organization;
											  });
											  /*  ############### Date :- 12/12/18 Get the organization of loggedInUser  End ############### */
											 
											  /**
											   * DATE :- 21/04/2020
											   *  Load suppression list of campaign.
											   */
											  $scope.getSuppressionList = function (campaignId) {
													getSuppressionListService.query({campaignId : campaignId}).$promise.then(function (data) {
														$scope.suppressionList = data;
														if ($scope.suppressionList != null && $scope.suppressionList != undefined && $scope.suppressionList.domainCount != undefined) {
															$scope.companySuppressionCount = $scope.suppressionList.domainCount;
														}
														if ($scope.suppressionList != null && $scope.suppressionList != undefined && $scope.suppressionList.emailCount != undefined) {
															$scope.contactSuppressionCount = $scope.suppressionList.emailCount;
														}
													}, function (error) {
														console.log("Error occured while fetching suppression list.");
													});
											}

											Pusher.subscribe(user.id, 'abm_count_refresh', function (message) {
												$scope.getABMListCount();
											});

											$scope.getABMListCount = function () {
												$scope.abmListCount = '';
												abmListCountService.query({campaignId : $stateParams.campaignId}).$promise.then(function (data) {
													if(data != undefined && data != null) {
														for(i = 0; i <= 15; i++) {
															if (data[i] != undefined) {
																$scope.abmListCount = $scope.abmListCount + data[i];
															}
														}
													}
													$scope.abmCount = $scope.abmListCount;
												}, function (error) {
													console.log("Error occured while fetching abmlist count.");
												});
											}
											$scope.openConfirmRemoveABMListModal = function() {
												$scope.confirmRemoveABMListModalInstance = $modal.open({
													scope : $scope,
													templateUrl : 'confirmCampaignABMModal.html',
													controller : ConfirmRemoveABMListModalInstanceCtrl,
													windowClass : 'confirm-remove-abm-modal',
												});
											}
											var ConfirmRemoveABMListModalInstanceCtrl = function ($scope) {
												$scope.confirmRemove = function() {													
													$scope.removeABMList();
													$scope.confirmRemoveABMListModalInstance.close();
												};

												$scope.cancel = function() {
													$scope.confirmRemoveABMListModalInstance.close();
												}
											}
											$scope.removeABMList = function () {
												if($scope.campaign.name != undefined) {
													$scope.loading = true;
													abmListCountService.remove({campaignId : $stateParams.campaignId}).$promise.then(function(data) {
														$scope.loading = false;
														NotificationService.log('success','ABM Remove Request Received. Please Check Your Email For Status..');
														console.log("ABM List Remove Successfully.");
														$scope.abmCount = '0';
													}, function(error) {
														$scope.loading = false;
														NotificationService.log('error','Error occured while remove abmlist.');
														console.log("Error occured while remove abmlist.");
													});
												}
											}

											if ($stateParams.campaignId !== null && $stateParams.campaignId !== "" && $stateParams.campaignId !== undefined) {
												$scope.getSuppressionList($stateParams.campaignId);
												$scope.getABMListCount();
											}

	                                    	  $scope.largestSequenceNumber = 0;
	                                    	  /*START	DATE:06/12/2017		REASON:Hack for crmmapping ("Define Further" in Audience selection)*/
	                                    	  $scope.attributeMappingsTemp = {};
	                                    	  angular.forEach($scope.attributeMappings, function(value, key) {
												if(!value.fromOrganization) {
	                                    			  $scope.attributeMappingsTemp[key] = value;
	                                    		  }
	                                    	  });                                  	  
	                                    	  $scope.supervisorIds = [];
	                                    	  $scope.geoAttrMapping = {};
	                                    	  $scope.supervisorList = {};
	                                    	  $scope.supervisorDetails = {};
	                                    	  $scope.selectedRadioBoxCriteria =[];
	                                    	  $scope.supervisorId  = ""; 
	                                    	  $scope.organizationTypes  = ""; 
	                                    	  $scope.enableAssetUpload = false;		// DATE : 23/05/2017	REASON : used to enable/disable asset modification (enabled if campaign status is created/planning/runready/running/paused)
	                                    	  //$scope.getOrganizationTypeDetails();
											  angular.forEach(attributeMappings, function(value, key) {
	                                    		  if (value.attributeType == XTAAS_CONSTANTS.attributeType.area || value.attributeType == XTAAS_CONSTANTS.attributeType.country  || 
	                                    				  value.attributeType == XTAAS_CONSTANTS.attributeType.state || value.attributeType == XTAAS_CONSTANTS.attributeType.city) {
	                                    			  $scope.geoAttrMapping[key] = value;
	                                    		  }
	                                    	  });
	                                    	  
	                                    	  UserOrganizationService.query().$promise.then(function(data){
                                    	    	 console.log(data);  
                                    	    	 $scope.organizationTypes = data.experienceTypes;
                                    	    },
		                                         function(error) {                                    					  
		                        					  console.log(JSON.stringify(error));                                    					  
		                        					  $scope.loading = false;
		                        				  }
                                    	    );	
	                                    	  
	                                    	  
	                                    	  $scope.campaign.restrictions = [];
	                                    	  $scope.asset = {};
	                                    	  $scope.enableNavBar = false;
											  $scope.isSupervisorList = true;
	                                    	  /* Campaign Details Tab */
	                                    	  	                                    	  	                                    	  
	                                    	  $scope.uiRouterState = $state;
	                                    	  
	                                    	  $scope.fExpr = [];
	                                    	  
	                                    	  /* Toggle swich */
	                                    	  $scope.initialSwitch = function(){
	                                  		    $scope.switchStatus = true;
	                                  		  }
	                                  		  
	                                  		  $scope.changeStatus = function(){
	                                  		    $scope.switchStatus = !$scope.switchStatus;
	                                  		  }
	                                  		  
	                                  		/* End of Toggle swich */
	                                    	  
	                                  		/* Upload Asset document */
	                                  		  $scope.uploadFile = function(myFile){
	                                  			  var file = myFile;	                                           
	                                  			  var uploadUrl = UploadURL.getuploadURL();
	                                  			  if(file == "" || file == null || file == undefined) {
	                                  				  NotificationService.log('error','Please select file to upload.');
		                                            	//alert('Please selet file to upload.');
	                                  			  } else {
	                                  				  $scope.loading = true;
		                                            	//$scope.$parent.loading = true;
	                                  				  fileUpload.uploadFileToUrl(file, uploadUrl,$stateParams.campaignId).success(function(data, status, headers, config) {
	                                  					  if(data.code=="SUCCESS"){
																//console.log(data.url);
															  NotificationService.log('success', 'File uploaded successfully.');	
	                                  						  $scope.assetUrl=data.url;	
	                                  					  } else {
	                                  						  angular.forEach(angular.element("input[type='file']"),function(inputElem) {
	                                  							  angular.element(inputElem).val(null);
	                                  						  });
	                                  						  if(data.code == "ERROR") {
	                                  							  NotificationService.log('error', data.msg);   
	                                  						  }		                                                        
	                                  					  }
		                                                    //$scope.$parent.loading = false;
	                                  					  $scope.loading = false;
	                                  				  });
	                                  			  }											
	                                  		  }
		                                        /* End of Upload Asset document */  
	                                  		  
		                                  	//For Multiple asset selection
		                                  		
		                                  		 /* Add New Asset */		                                  			                                  		 
	                                  		  $scope.assetUrl = "";
	                                  		  $scope.assetName = "";
	                                  		  $scope.assetDescription = "";
		                                        
	                                  		  $scope.addAssets = function() {
	                                  			  if(!$scope.campaign.assets || $scope.campaign.assets.length < 1) {
	                                  				  $scope.assets = [];	
	                                  			  } else {
	                                  				  $scope.assets = $scope.campaign.assets;
	                                  			  }  
			                                  		  
	                                  			  var assetURL = $scope.assetUrl;
	                                  			  var assetTitle = $scope.assetName;
	                                  			  var assetDescription = $scope.assetDescription;
			                                  		
	                                  			  if(assetURL != "" && assetTitle != "" && assetDescription != "") {			                                  		
	                                  				  $scope.loading = true;
	                                  				  $scope.assets.push({
															name:assetTitle, 
															url:assetURL, 
															description:assetDescription,
															isRemoved: false
	                                  				  });		
	                                  				  $scope.campaign.assets = $scope.assets;
	                                  				  $scope.loading = false;
				                                        
	                                  				  $scope.assetUrl = "";
	                                  				  $scope.assetName = "";
	                                  				  $scope.assetDescription = "";
	                                  			  } else {
	                                  				  NotificationService.log('error','Please enter asset details');
	                                  			  }
	                                  		  }
			                                  	  
	                                  		  $scope.removeAssetExpression = function(index) {
	                                  			  if ($scope.enableAssetUpload) {
	                                  				//   $scope.campaign.assets.splice(index, 1);
													  $scope.campaign.assets[index].isRemoved = true;
	                                  			  }
	                                  		  }; 
			                                  	  
			                                  	  
		                                        /* End of Add New Asset */
	                                  		
	                                  		  
	                                  		/*var timerstartSaveAssetDetails = $interval(function() {
	                                  			if($scope.asset.url != undefined && $scope.asset.name != undefined && $scope.asset.description != undefined) {
	                                  				$scope.addAssets();	
	                                  			}	      
	                                  			//console.log('Call Asset Save method..');
	                                  		}, 1000);*/
	                                  		
	                                    	  var timerstartSaveToLocalStorage = $interval(function() {
	                                    		  //console.log($scope.campaign);
		                                    	  if ($scope.campaign != null && $scope.campaign != '') {
		                                    		  $scope.updatedSearchCriteria ();
		                                    		  $localStorage.campaign = $scope.campaign;
		                                    		 // console.log('Saved in Localstorage..');
		                                    	  }
	                                    	  }, 500);
	                                    	  
	                                    	  
	                                    	  
											  $scope.updatedSearchCriteria = function () {	                                    		  
												angular.forEach($scope.selectedCriteria, function(item, key) {                                                      
													$scope.valueArr = [];
													var csvText = [];
													var expr = {};			                                    	  
													var flag = false;
																						if(item.operator != undefined && item.value != null &&  item.value != undefined) {
														angular.forEach($scope.fExpr, function(item2, key2) {
															if ($scope.geoAttrMapping != undefined) {
																$scope.geoAttrMapPresent = $scope.geoAttrMapping[item2.attribute];
															}
															if((item2.attribute == item.systemName && item.systemName) || (item.systemName == "Geography" && $scope.geoAttrMapPresent != undefined)) {
																flag = true;
																$scope.fExpr[key2].attribute = item.systemName == "Geography" ? item.target : item.systemName;
																
																/* DATE : 10/05/2017	REASON : Below property is added for handling CheckBox value on "edit campaign" page. Which will decide whether question will be shown on agent screen or not */
																$scope.fExpr[key2].agentValidationRequired = item.agentValidationRequired;
																
																$scope.fExpr[key2].questionSkin = item.questionSkin;
																$scope.fExpr[key2].pickList = item.pickList
																
																if(item.operator != undefined && item != undefined) {
																	$scope.fExpr[key2].operator = item.operator;
																}
																if (item.value != undefined && item != undefined) {
																	  if (item.systemName == 'ZIP_CODE' || item.systemName == 'REMOVE_INDUSTRIES' || item.systemName == 'VALID_STATE' 
																		   || item.systemName == 'SEARCH_TITLE' || item.systemName == 'REMOVE_DEPT' || item.systemName == 'NEGATIVE_TITLE' || (item.systemName == 'CUSTOMFIELD_01' || item.systemName == 'CUSTOMFIELD_02' 
																		   || item.systemName == 'CUSTOMFIELD_03' || item.systemName == 'CUSTOMFIELD_04' || item.systemName == 'CUSTOMFIELD_05' 
																		   || item.systemName == 'CUSTOMFIELD_06' || item.systemName == 'CUSTOMFIELD_07' || item.systemName == 'CUSTOMFIELD_08' 
																		   || item.systemName == 'CUSTOMFIELD_09' || item.systemName == 'CUSTOMFIELD_10')) {		                                    				  
																		  if (Array.isArray(item.value)) {
																			  $scope.fExpr[key2].value = item.value;
																		  }
																		  else {
																			  csvText.push(item.value);
																			  $scope.fExpr[key2].value = csvText;
																		  }
																	  } else {
																		  angular.forEach(item.value, function(valueArrItem, valueArrKey) {
																			  if (valueArrItem.value != undefined) {
																					  $scope.valueArr.push(valueArrItem.value);  
																			  } else {
																					  $scope.valueArr.push(valueArrItem);
																			  }
																				  });
																				  $scope.fExpr[key2].value = $scope.valueArr;	
																	  }
																}			                                    				                                     					   
															 }
														});
														if (!flag) {
														   /* angular.forEach($scope.fExpr, function(item2, key2) {
																if (item2.attribute == "Area" || item2.attribute == "Country" || item2.attribute == "State"  || item2.attribute == "City") {
																	$scope.fExpr.splice(key2, 1);
																}
															});*/
															expr = {
																	attribute:item.systemName == "Geography" ? item.target : item.systemName,
																	operator:item.operator,
																	value:item.value,
																	/* DATE : 10/05/2017	REASON : Below property is added for handling CheckBox value on "edit campaign" page. Which will decide whether question will be shown on agent screen or not */
																	agentValidationRequired: item.agentValidationRequired,
																	questionSkin: item.questionSkin,
																	pickList: item.pickList,
																	sequenceNumber: item.sequenceNumber
															};
															$scope.fExpr.push(expr);
														}
													} else {
														if (item.operator == undefined || item.operator == null) {
															$scope.fExpr[key].operator = null;
														} else {
															$scope.fExpr[key].operator = item.operator;
														} 
														if(item.value == undefined || item.value == null || item.value.length < 1) {
															$scope.fExpr[key].value = item.value;
														} else {
															$scope.fExpr[key].value = item.value;
														}
													}
											   });

											  angular.forEach($scope.fExpr, function(resValue, resKey) {
												  if (resValue.attributeType == "CUSTOM" && resValue.operator == "FREE_TEXT") {
													  resValue.value = ["FREE_TEXT"];
												  }
											  });
											   
											   $scope.campaign.qualificationCriteria = {
														   groupClause: "And",
															expressions: $scope.fExpr,
															expressionGroups: []
												}; 
											}
	                                    	  
		                                       var timerstartUpdate = $interval(function() {  	
		                                    	     $scope.updatedSearchCriteria(); 
				                                     //console.log('Set Qualification Criteria....');
				                                      
				                               }, 60000);
	                                    	  
		                                       $scope.popopen = function() {
		                                    	   $scope.openpopup = true;
		                                       }
		                                     
		                                       //$scope.geoAttrMapping = {};
		                                       
		                                     
	                                    	  $scope.getService = function() {	                                    		  
	                                    		  CampaignService.get({
	                                    			  id: $stateParams.campaignId
	                                    		  }).$promise.then(function(getData){
	                                    			  $scope.loading = false;
	                                    			  $scope.campaign = getData;
	                                    			  if ($scope.campaign.status !== null && $scope.campaign.status !== undefined) {
	                                    				  $scope.isCampaignCreatd = true;
	                                    			  }
	                                    			  $scope.supervisorDetails = $scope.campaign.supervisorId;
	                                    			  /* START CAMPAIGN REQUIREMENTS
	                                    			   * DATE : 03/05/2017
	                                    			   * REASON : Added to fetch campaign requirements from DB */
	                                    			  if($scope.campaign.campaignRequirements == undefined) {
	                                    				  $scope.campaign.campaignRequirements = "";
	                                            	  }
	                                    			  /* END CAMPAIGN REQUIREMENTS */
	                                    			  $scope.campaign.startDate= $filter('date')($scope.campaign.startDate, 'yyyy-MM-dd');
    	                                              $scope.campaign.endDate= $filter('date')($scope.campaign.endDate, 'yyyy-MM-dd');
    	                                              $scope.minDate = new Date().toISOString().substring(0, 10);//scope for not allow to user to select past dates
    	                                              $scope.noOfDays($scope.campaign.startDate, $scope.campaign.endDate);
    	                                              $scope.lastUpdatedTime = $filter('date')(getData.updatedDate, 'hh:mm a');
    	                                              $scope.getAudienceSelectionValues(getData);
    	                                              $scope.getAudienceSelectionValuesEdit(getData);
    	                                              
    	                                              /* DATE:11/01/2017 	REASON:Added to get largest CustomQuestion Sequence Number */
    	                                              $scope.getLargestCustomQuestionSequenceNumber();
    	                                              
    	                                              angular.forEach(getData.restrictions, function(value, key) {
    	                                            	  angular.forEach($scope.restrictionsArr, function(resValue, resKey) {
    	                                            		  if(resValue.name == value) {
    	                                            			  $scope.restrictionsArr[resKey].checked = true;
    	    	                                                  $scope.checkedopt(value);
    	                                            		  }
    	                                            	  });
    	                                              });
    	                                              
    	                                              OrganizationService.query({id: $scope.campaign.organizationId}).$promise.then(function(organizationData){
    	  	                                  			$scope.organization = organizationData;
    	                                              });
    	                                              
    	                                              if($scope.campaign.assets != undefined) {
    	                                            	  angular.forEach($scope.campaign.assets, function(assetValue, assetKey) {    	                                            		 
    	                                            		  	 $scope.asset.url = assetValue.url;	 
    	                                            			 $scope.asset.name = assetValue.name;	 
    	                                            			 $scope.asset.description = assetValue.description;
    	                                            	  });
    			                                  			
    			                                  		}
    	                                              
    	                                              $scope.tempInformationCriteria = [];
   	                                    			  angular.forEach($scope.attributeMappings, function(value, key) {
																									 if ($scope.campaign.informationCriteria != null && $scope.campaign.informationCriteria != undefined) {
																										for(var i=0; i<$scope.campaign.informationCriteria.length; i++) {
																											if(value.systemName == $scope.campaign.informationCriteria[i]) {
																													$scope.tempInformationCriteria.push(value);
																											}
																									  }
																									 }
   	                                    			  });

														if(getData.qualificationCriteria != undefined && getData.qualificationCriteria.expressions.length > 0) {
															$scope.setDefaultSelectedCriteria(getData.qualificationCriteria.expressions);	
														} else if (getData.status == null || getData.status == undefined || getData.status == 'CREATED') {
															$scope.setDefaultSelectedCriteria('');
														}

	   	                                    			if($scope.campaign.status ==='CREATED') {//editable all field
	  	                                            	  $scope.isCampaingTypeDisable = false;
	  	                                				  $scope.isSelfManageTeamDisable = false;
	  	                                				  $scope.isNotChangeable = false;
	  	                                				  $scope.isIOSignedDisable=false;
	  	                                				  $scope.isStartDateDisable=false;
	  	                                				  $scope.enableAssetUpload = true;		// DATE : 23/05/2017	REASON : used to enable asset modification
	   	                                    			} else if($scope.campaign.status ==='BIDDING') {//non editable campaign type and self manage team
	                                    				  $scope.isCampaingTypeDisable = true;
	                                    				  $scope.isSelfManageTeamDisable = true;
	   	                                    			} else if($scope.campaign.status ==='RUNNING' || $scope.campaign.status ==='PAUSED' || $scope.campaign.status ==='PLANNING' || $scope.campaign.status ==='RUNREADY') {
	                                    				  /* START
	                                    				   * DATE : 09/10/2017 
	                                    				   * REASON : allowing CM to modify all setting (except start Date) in all statuses (CREATED, PLANNING, RUNREADY, RUNNING, PAUSED) */
	   	                                    			  $scope.isIOSignedDisable=false;
	                                    				  $scope.isCampaingTypeDisable = true;
	                                    				  /* END */
	                                    				  $scope.isSelfManageTeamDisable = true;
	                                    				  $scope.isStartDateDisable=true;
	                                    				  $scope.enableAssetUpload = true;		// DATE : 23/05/2017	REASON : used to enable asset modification
	                                    			  	} else {
	                                    				  $scope.isCampaingTypeDisable = true;
	                                    				  $scope.isSelfManageTeamDisable = true;//non editable all field
	                                    				  $scope.isNotChangeable = true;
	                                    				  $scope.isIOSignedDisable=true;
	                                    				  $scope.isStartDateDisable=true;
	                                    				  $scope.enableAssetUpload = false;		// DATE : 23/05/2017	REASON : used to disable asset modification
	                                    			  	}
	   	                                    			if($scope.campaign.status ==='PLANNING' || $scope.campaign.status ==='RUNREADY') {
	   	                                    				$scope.isStartDateDisable=false;
	   	                                    			}
	                                    		  });
	                                    	  }
	                                    	  
	                                    	  $scope.getAudienceSelectionValues = function (setData){
	                                    		  angular.forEach(setData.qualificationCriteria, function(qualificationValues, qualificationKey) {	                                    			  
 	                                    				angular.forEach($scope.selectedCriteria, function(selValue, selKey) {	
 	                                    					  if (qualificationKey != undefined && qualificationKey == 'groupClause') {
 	                                    						selValue.groupClause = 	qualificationValues.groupClause;
 	                                    					  }
 	                                    					});
 	                                    				if (qualificationKey != undefined && qualificationKey == 'expressions') {
 	                                    					angular.forEach(qualificationValues, function(expressionValue, expressionKey) { 	                                    						
	   	                                    					angular.forEach($scope.selectedCriteria, function(selValue, selKey) {
	   	                                    						
	   	                                    						if(selValue.crmName == qualificationValues[expressionKey].attribute) {
		   	                                    						selValue.value = [];
		   	                                    						
			   	                                    					if(qualificationValues[expressionKey].operator != undefined) {
			   	                                    						selValue.operator = qualificationValues[expressionKey].operator;	
			   	                                    					}
			   	                                    					if(qualificationValues[expressionKey].attribute != undefined) {
			   	                                    						selValue.attribute = qualificationValues[expressionKey].attribute;	
			   	                                    					}
			   	                                    					if(qualificationValues[expressionKey].value != undefined) {
			   	                                    						selValue.value = qualificationValues[expressionKey].value;	
			   	                                    					}
	   	                                    						}
	   	                                    					});
	   	                                    				});
 	                                    				}
 	                                    			});	 
	                                    	  }
	                                    	  
	                                    	  
	                                    	  // To get data from DB and assign as selected criteria on EDIT
	                                    	  $scope.getAudienceSelectionValuesEdit = function (setData){
	                                    		  angular.forEach(setData.qualificationCriteria, function(qualificationValues, qualificationKey) {
 	                                    				if (qualificationKey != undefined && qualificationKey == 'expressions') {
 	                                    					angular.forEach(qualificationValues, function(expressionValue1, expressionKey1) {	
 	                                    						angular.forEach($scope.attributeMappings, function(attributeValue, attributeKey) {	
 	                                    						if (expressionValue1.attribute == attributeValue.systemName) {
 	                                    							attributeValue.operator = expressionValue1.operator;
 	                                    							attributeValue.value = [];
 	                                    							attributeValue.value = expressionValue1.value;
 	                                    							
 	                                    							/* DATE : 10/05/2017	REASON : Retrieving agentValidationRequired Flag from DB and assign as selected criteria on edit */
 	                                    							attributeValue.agentValidationRequired = expressionValue1.agentValidationRequired;
 	                                    							attributeValue.sequenceNumber = expressionValue1.sequenceNumber;
 	                                    							attributeValue.questionSkin = expressionValue1.questionSkin;
 	                                    							attributeValue.pickList = expressionValue1.pickList;
 	                                    							
 	                                    							$scope.selectedCriteria.push(attributeValue); 	                                    							
 	                                    						}
 	                                    					});
 	                                    				});	 
 	                                    			}
	                                    		  });
	                                    	  }
	                                    	  
	                                    	  
	                                    	  if ($stateParams.campaignId != undefined && $stateParams.campaignId != null) {
	                                    		  $scope.getService();
	                                    		  $scope.enableNavBar = true;
	                                    	  }
	                                    	  
	                                    	  $scope.toggleTabs = function(selectedOpt) {
	                                    		  if ($scope.campaign.name == undefined) {
	                                            	  NotificationService.log('error','Please enter Campaign name.');
	                                            	  $scope.campaignSelected = true;
	                                            	  return false;
												  } 
												  else if ($scope.campaign.type == undefined) {
													NotificationService.log('error','Please enter campaign type.');
													$scope.campaignSelected = true;
													return false;
												 } 
												  else if ($scope.campaign.startDate == undefined) {
													NotificationService.log('error','Please enter start date.');
													$scope.campaignSelected = true;
													return false;
												 }
												 else if ($scope.campaign.endDate == undefined) {
													NotificationService.log('error','Please enter end date.');
													$scope.campaignSelected = true;
													return false;
												 } 
												 else if ($scope.campaign.deliveryTarget == undefined || $scope.campaign.deliveryTarget == 0) {
													NotificationService.log('error','Please enter delivery target.');
													$scope.campaignSelected = true;
													return false;
												 } 
												 else if ($scope.checkDuplicateQuestion()) {
                                                    $scope.audienceSelected = true;
                                                    return false;
                                                 }
												//  else if ($scope.campaign.clientName == null || $scope.campaign.clientName == undefined || $scope.campaign.clientName == '') {
												// 	if ($scope.validateLoggedInUserOrganization()) {
												// 		NotificationService.log('error','Please enter client name.');
												// 		$scope.campaignSelected = true;
												// 		return false;
												// 	}
												// 	 else {
												// 		$scope.campaignSelected = false;
												// 		$scope.setCampaignFlags(selectedOpt);
												// 	 }
											  	//  }	 
	                                    		  else {
	                                            	   $scope.setCampaignFlags(selectedOpt);
	                                              }	  	                                    		  
  	  
	                                    		 /*if($scope.campaign.status ==='BIDDING' || $scope.campaign.status ==='PLANNING' || $scope.campaign.status ==='CREATED') {
	                                    			 $scope.UpdateDataAPI(); 
	                                    		 }*/
	                                    		  
	                                    		  // DATE:09/10/2017	REASON:allowing CM to modify all setting (except start Date) in all statuses (CREATED, PLANNING, RUNREADY, RUNNING, PAUSED)
	                                    		  $scope.UpdateDataAPI();
	                                    	  }
											  
											  $scope.setCampaignFlags = function(selectedOpt) {
												if(selectedOpt == 'newcampaignform'){
													$scope.campaignSelected = true;
													$scope.audienceSelected = false;
													$scope.teamSelected = false;
												}
												if(selectedOpt == 'audienceform'){
													if (!$scope.enableNavBar) {
														return;
													}
													//$scope.loading = true;
													//$scope.getService();	                                    			  
													$scope.campaignSelected = false;
													$scope.audienceSelected = true;
													$scope.teamSelected = false;
												}
												if(selectedOpt == 'teamselectionform'){
													if (!$scope.enableNavBar) {
														return;
													}
													$scope.campaignSelected = false;
													$scope.audienceSelected = false;
													$scope.teamSelected = true;
													if ($scope.checkQuestionOptionsSelected()) {
														$scope.getDifferentTypesOfTeams();
														$scope.getShortlistedTeams();
														$scope.getInvitedTeams();
														$scope.supervisorLists($scope.campaign.organizationId);
													}
												} 
											  }
	                                    	  $scope.toggleTabs('newcampaignform');
	                                    	  
	                                    	  $scope.diff = 0;
	                                    	  
	                                    	  /* GET API */
	                                    	  
	                                    	  //var timerstartGet = $interval(function() {  
	                                    	  if($scope.params !== undefined) {
	                                    		//   CampaignService.get({
	                                    		// 	  id: $scope.params
	                                    		//   }).$promise.then(function(cloneData) {
	                                    		// 	  $scope.loading = true;
	                                    		// 	  $scope.campaign=cloneData;
	                                    		// 	  $scope.campaign.name = "Clone of "+cloneData.name;
	                                    		// 	  $scope.isCampaingTypeDisable = false;
	                                    		// 	  $scope.isSelfManageTeamDisable = false;
	                                    		// 	  cloneData.startDate = null;
	                                    		// 	  cloneData.endDate = null;
		                                        //     //   if($scope.params !== undefined) {
	                                    		// 	// 	  delete $scope.campaign["id"];
	                                    		// 	//   }
		                                        //       $scope.campaign['status']="CREATED";
                                    				  CampaignCloneService.query({id:$scope.params}).$promise.then(function(cloneSavedData) {
                                    					  $scope.lcampaign=cloneSavedData;	
                                    					  $cookieStore.put('camp_id',cloneSavedData['id']);
                                    					  GlobalCampaign.setGlobalCampaign(cloneSavedData);
                                    					  $scope.loading = false;
                                    					  $scope.campaign.startDate= $filter('date')(cloneSavedData.startDate, 'yyyy-MM-dd');
    		                                              $scope.campaign.endDate= $filter('date')(cloneSavedData.endDate, 'yyyy-MM-dd');
    		                                              $scope.minDate = new Date().toISOString().substring(0, 10);
                                    					  $state.go("editcampaign", {
                                    						  'campaignId' : cloneSavedData['id']
                                    					  });                                    					  
                                    				  },
                                    				  function(error) {
                                    					  console.log("rejected " + JSON.stringify(error));                                    					  
                                    					  $scope.loading = false;
                                    				  });	                                    			
	                                    		//   });
	                                    	  } else if($stateParams.campaignId == null || $stateParams.campaignId == "" || $stateParams.campaignId == undefined) {
	                                    		  $scope.isCampaignCreatd = false;
	                                    		  $scope.campaign = {
                                    				  restrictions: [],
                                    				  informationCriteria: [],
                                    				  qualificationCriteria: {
		                                    			  groupClause: "",
		                                    			  expressions: [],
		                                    			  expressionGroups: []
                                    		  		  }
	                                    		  };
	                                    		  $scope.minDate = new Date().toISOString().substring(0, 10);	                                    		 
	                                    		} else {
	                                    			 //$scope.getService();	                                    		 
	                                    		}
	                                    	 // }, 60000);
	                                    	  
	                                    	  
	                                    	  
	                                    	  /* END OF GET API */
	                                    	  
	                                    	  $scope.noOfDays = function(sDate, eDate) {	                                    		 
	                                    		  if(sDate != '' && sDate != undefined) {
	                                    			  var firstDate = new Date(sDate);
		                                    		  firstDate.setDate(firstDate.getDate() + 1);
		                                    		  $scope.minEndDate = $filter('date')(firstDate, 'yyyy-MM-dd');  
	                                    		  }	                                    		  	                                    		  
	                                    		  
	                                    		  if(sDate != '' && sDate != undefined && eDate != '' && eDate != undefined) {
	                                    			  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	                                    			  var firstDate = new Date(sDate);	                                    			  
	                                    			  var secondDate = new Date(eDate);
	                                    			  if(firstDate >= secondDate) {
	                                    				  NotificationService.log('error','End date should be greater than start date.');
	                                    			  } else {
	                                    				  $scope.diff = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));  
	                                    			  }	                                    			  	                                    			 
	                                    		  }
	                                    	  }
	                                    	  
	                                    	  $scope.addRestriction = function(value, flag, index) {
	                                    		  if($scope.isIOSignedDisable != true) {
	                                    			  $scope.frmAttributeChanged = true;
		                                    		  if($scope.restrictionsArr[index].checked == true) {
		                                    			  $scope.campaign.restrictions.splice(index, 1);	                                    			  
		                                    			  $scope.restrictionsArr[index].checked = false;	                                    			  
		                                    		  } else {
		                                    			  $scope.restrictionsArr[index].checked = true;
		                                    			  if ($scope.campaign.restrictions.indexOf(value) == -1) {
		                                    				  $scope.campaign.restrictions.push(value);	                                    				  
		                                    			  }
		                                    		  }  
	                                    		  }
	                                    	  };
	                                    	  
	                                    	  $scope.restrictionsArr = [
	                	                             {name:'Corporate emails only',checked:"false"},
	                	                             {name:'De-duplicate periodically',checked:"false"},
	                	                             {name:'No competitor emails',checked:"false"},
	                	                             {name:'No mobile phones',checked:"false"},
	                	                             {name:'Specific delivery dates and times',checked:"false"}
                	                           ];

	                                    	  $scope.backToList = function() {
												if ($scope.duplicateQuestion != null && $scope.duplicateQuestion != undefined && $scope.duplicateQuestion) {
                                                    NotificationService.log('error','Duplicate questions are not allowed.');
                                                    return false;
                                                } else {
													if($scope.campaign.name == undefined) {
														$state.go('listcampaigns');
														$scope.loading = true;
													} else {
														$scope.UpdateDataAPI();
														$state.go('listcampaigns');  
														$scope.loading = true;
													}
												}
	                                    	  }

	                                    	  $scope.campaignTypes = GlobalCampaignTypes.getGlobalCampaignTypes();

	                                    	  //Save API Service call after Campaign type entered
	                                    	  $scope.saveAPICall = function() {
	                                    		  if($scope.campaign.type != undefined) {
													$scope.campaign.startDate= $filter('date')($scope.campaign.startDate, 'MM/dd/yyyy');
		                                              $scope.campaign.endDate= $filter('date')($scope.campaign.endDate, 'MM/dd/yyyy');
		                                              if($stateParams.campaignId == null || $stateParams.campaignId == "" || $stateParams.campaignId == undefined) {
		                                            	  $scope.campaign['status']="CREATED";
		                                            	  CampaignService.save($scope.campaign).$promise.then(function(saveData) {
		                                            		   $scope.isCampaignCreatd = true;
	                                    					   $scope.lcampaign=saveData;	
	                                    					   $cookieStore.put('camp_id',saveData['id']);
	                                    					   GlobalCampaign.setGlobalCampaign(saveData);
	                                    					   $scope.lastUpdatedTime = $filter('date')(saveData.updatedDate, 'hh:mm a');
	                                    					   $scope.loading = false;
	                                    					   //console.log("Campaign Details Saved.....");	                                    					   
															   $scope.enableNavBar = true;	 
	                                    					   $state.go('editcampaign', {
	                                    						  'campaignId' : saveData['id']
	                                    					   });	                                    					  
	                                    				  },
	                                    				  function(error) {
	                                    					  console.log("rejected " + JSON.stringify(error));
	                                    					  $scope.loading = false;
	                                    				  });
													  }
	                                              }	          
	                                    	  }
	                                    	  
	                                    	  
	                                    	  //Update service called after every 1 min
	                                    	  /*var timerstartUpd = $interval(function() { 
	                                    		  
	                                    		  if($scope.campaign.name != undefined && $scope.isIOSignedDisable == false) {
	                                    		  
		                                    		  $scope.campaign.startDate= $filter('date')($scope.campaign.startDate, 'MM/dd/yyyy');
		                                              $scope.campaign.endDate= $filter('date')($scope.campaign.endDate, 'MM/dd/yyyy');
		                                              
		                                              if($scope.campaign.startDate != undefined && $scope.campaign.endDate == undefined) {
		                                            	  NotificationService.log('error','Please enter End date.');		                                            	  
		                                              } else if($scope.campaign.endDate != undefined && $scope.campaign.startDate == undefined) {
		                                            	  NotificationService.log('error','Please enter Start date.');		                                            	  
		                                              } else {
		                                            	  if($stateParams.campaignId != null && $stateParams.campaignId != "" && $stateParams.campaignId != undefined) {
			                                            	  CampaignService.update({
			                                   					  id: $stateParams.campaignId
			                        				  		  }, $scope.campaign).$promise.then(function(updateData) {
			   	                                    			  
			                        				  			   //console.log($scope.campaign);
			                        				  			   $scope.campaign = updateData;
			                        				  			   GlobalCampaign.setGlobalCampaign(updateData);
			                        				  			   $scope.lastUpdatedTime = $filter('date')(updateData.updatedDate, 'hh:mm a');
			                        				  			   $scope.campaign.startDate= $filter('date')(updateData.startDate, 'yyyy-MM-dd');
					                                               $scope.campaign.endDate= $filter('date')(updateData.endDate, 'yyyy-MM-dd');
			   	                                    			   $scope.loading = false;
			   	                                    			   console.log("Audience Selection Details Updated.....");
			   	                                    			   console.log(updateData);
			   	                                    			   $state.go('editcampaign' , {
			   	                                    				  'campaignId' : updateData['id']
			   	                                    			   });
			   	                                    		  },
			                        				  			
				                                    		   function(error) {
				                                    				    console.log("rejected " + JSON.stringify(error));
				                                    					$scope.loading = false;
				                                    		   });
			                                              }   		                                            	  
		                                              }                                		                                             
	                                              }	          
	                                    	  }, 60000);*/
	                                    	  
	                                    	    //Update Service call when user click on other links
												$scope.UpdateDataAPI = function () {
													if($scope.frmAttributeChanged == true) {
														if ($scope.checkQuestionOptionsSelected()) {
																$scope.updatedSearchCriteria ();
																$scope.generateCustomQuestionSequenceNumber();
																$scope.supervisorLists($scope.campaign.organizationId);
															  $scope.updateCampaign();
														}
													}		                                    	  
												}

												$scope.checkQuestionOptionsSelected = function() {
													var validQuestion = true;
													angular.forEach($scope.fExpr, function(item,value) {
														if (item.operator != null && item.operator != undefined && item.operator == "FREE_TEXT") {
															return validQuestion;
														}else {
															if (item.operator == "" || item.operator == null || item.operator == undefined) {
																NotificationService.log('error','Please select operator.');
																$scope.campaignSelected = false;
																$scope.audienceSelected = true;
																$scope.teamSelected = false;
																validQuestion = false;
															} else if (item.value == "" || item.value == null || item.value == undefined || item.value.length == 0) {
																		var errorMsg = 'Please select '+item.attribute;
																		NotificationService.log('error',errorMsg);
																		$scope.campaignSelected = false;
																		$scope.audienceSelected = true;
																		$scope.teamSelected = false;
																		validQuestion = false;
															}
														}
													})
													return validQuestion;
												}

												$scope.checkDuplicateQuestion = function() {
                                                    if ($scope.campaign != null && $scope.campaign != undefined && $scope.campaign.qualificationCriteria != null 
                                                        && $scope.campaign.qualificationCriteria != undefined 
                                                        && $scope.campaign.qualificationCriteria.expressions != null && $scope.campaign.qualificationCriteria.expressions != undefined) {
																var questionSkinsList = [];
																angular.forEach($scope.campaign.qualificationCriteria.expressions, function(value, key) {
																	questionSkinsList.push(value.questionSkin);
																});

																let duplicatesArray = arr => questionSkinsList.filter((item, index) => questionSkinsList.indexOf(item) != index);
																var result = duplicatesArray(questionSkinsList); 

																if (result.length > 0) {
																	NotificationService.log('error','Duplicate questions are not allowed.');
																	$scope.duplicateQuestion = true;
																	return true;
																} else {
																	$scope.duplicateQuestion = false;
																	return false;
																}
                                                    }   
                                              }

											  $scope.updateCampaign = function() {
												if ($stateParams.campaignId != null && $stateParams.campaignId != "" && $stateParams.campaignId != undefined) {
													$scope.campaign.startDate= $filter('date')($scope.campaign.startDate, 'MM/dd/yyyy');
													$scope.campaign.endDate= $filter('date')($scope.campaign.endDate, 'MM/dd/yyyy');
													angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionValue, expressionKey) {
														angular.forEach($scope.attributeMappings, function(value, key) {
															if (expressionValue.attribute == value.systemName) {
																if (value.attributeType == "CUSTOM" && expressionValue.value.length == 0) {
																	expressionValue.value = ["FREE_TEXT"];
																}	
														}
														});
													});
													CampaignService.update({
													id: $stateParams.campaignId
													}, $scope.campaign).$promise.then(function(updateDataAPI) {	
														//$scope.selectedRadioBoxCriteria = [];
														$scope.campaign = updateDataAPI;
														angular.forEach($scope.supervisorList, function(element){
															if ($scope.campaign.adminSupervisorIds != null && $scope.campaign.adminSupervisorIds != undefined) {
															angular.forEach($scope.campaign.adminSupervisorIds, function(element1){
																if (element1 == element.id) {
																	element.selected = true;
																	element.isActive = true;
																	if ($scope.supervisorIds.indexOf(element.id) == -1) {
																		$scope.supervisorIds.push(element.id);
																		if($scope.supervisorIds.length == $scope.supervisorList.length){
																			$scope.isAllSelected = true;
																		}
																	}
																}
															});
														 }
														});
														$scope.campaign.startDate= $filter('date')(updateDataAPI.startDate, 'yyyy-MM-dd');
														$scope.campaign.endDate= $filter('date')(updateDataAPI.endDate, 'yyyy-MM-dd');
														$scope.getDifferentTypesOfTeams();
														if(updateDataAPI.supervisorId != undefined ||  updateDataAPI.supervisorId != null){
															$scope.supervisorDetails = {};
															$scope.supervisorId = updateDataAPI.supervisorId;
															$scope.supervisorDetails = updateDataAPI.supervisorId;
														}
													},

													function(error) {
														console.log("rejected " + JSON.stringify(error));
														$scope.loading = false;
													});
												} 
										  }
											  
											  $scope.validateLoggedInUserOrganization = function() {
												if ($scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization != undefined) {
													if ($scope.loggedInUserOrganization.organizationLevel == 'INTERNAL') {
														return true;
													}
													else {
														return false;
													}                   	  
											   	} 
											  }
	                                    	  
	                                    	  $scope.getLargestCustomQuestionSequenceNumber = function () {
	                                    		  var sequenceNo = [];
	                                    		  angular.forEach($scope.selectedCriteria, function(item, key) {
			                                    	  if (item.fromOrganization && (item.sequenceNumber !== null && item.sequenceNumber !== undefined)) {
			                                    		  if (item.sequenceNumber) {
			                                    			  var seqNo = item.sequenceNumber.slice(2);
				                                    		  sequenceNo.push(seqNo);
			                                    		  }
			                                    	  }
	                                    		  });
	                                    		  if(sequenceNo.length > 0) {
	                                    			  $scope.largestSequenceNumber = Math.max.apply(Math, sequenceNo);
	                                    		  } else {
	                                    			  $scope.largestSequenceNumber = 0;
	                                    		  }
	                                    	  }
	                                    	  
	                                    	  $scope.generateCustomQuestionSequenceNumber = function () {
		                        					angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionValue, expressionKey) { 	                                    						
		                            					angular.forEach($scope.selectedCriteria, function(item, key) {
		                            						if(item.crmName == expressionValue.attribute) {
		                            							if (item.fromOrganization && (expressionValue.sequenceNumber == null || expressionValue.sequenceNumber == undefined)) {
		                            								$scope.largestSequenceNumber = $scope.largestSequenceNumber + 1;
		                            								expressionValue.sequenceNumber = "CQ" + $scope.largestSequenceNumber;
																}
		                            						}
		                            					});
		                            				});
	                                    	  }
	                                    	  
	                                    	  $scope.supervisorLists = function(organizationId){
	                                    		  SupervisorService.query({
                                   					  id: organizationId
                                            	  	}).$promise.then(function(data) {	
                                            	  		$scope.selectedRadioBoxCriteria = [];
			                                            $scope.supervisorList = data;
			                                            angular.forEach($scope.supervisorList, function(value, key) {
			                                            	if (value.id == $scope.supervisorDetails) {
			                                            		$scope.selectedRadioBoxCriteria.push(value);
															}
														});
                                            	  	    //$scope.selectedRadioBoxCriteria.push(updateDataAPI.login);
			                                            //$scope.supervisorDetails = updateDataAPI.login;
                                            	  		/*if( updateDataAPI.login != undefined ||  updateDataAPI.login != null){
                                            	  			$scope.supervisorId = updateDataAPI.login.id;
                                            	  		}*/
                                            	  	},
                        				  			
                                            	  	function(error) {
	                                    				    console.log("rejected " + JSON.stringify(error));
	                                    					$scope.loading = false;
                                            	  	});
	                                    	  }
	                                    	  
	                                    	  /* EOC Campaign Details Tab */
	                                    	  
	                                    	  /* Audience Selection Tab */
											  $scope.selectedCriteria = [];	                                    	  
	                                    	  $scope.addCriteria = function(flag, obj, index, sysName) {
	                                    		  if($scope.isIOSignedDisable != true) {
		                                    		  if(flag == true){
		                                    			  return;
		                                    		  }
		                                    		  if (flag == undefined) {
															if ($scope.campaign.enableCustomFields && (sysName == 'CUSTOMFIELD_01' || sysName == 'CUSTOMFIELD_02' 
																|| sysName == 'CUSTOMFIELD_03' || sysName == 'CUSTOMFIELD_04' || sysName == 'CUSTOMFIELD_05' 
																|| sysName == 'CUSTOMFIELD_06' || sysName == 'CUSTOMFIELD_07' || sysName == 'CUSTOMFIELD_08' 
																|| sysName == 'CUSTOMFIELD_09' || sysName == 'CUSTOMFIELD_10')) {
																	obj.operator = "FREE_TEXT";
																	obj.agentValidationRequired = true;
																	// obj.attributeType = 'CUSTOM';
															}
															$scope.selectedCriteria.push(obj);
															$scope.exression = {
																	attribute: sysName
															}
															$scope.fExpr.push($scope.exression); 
		                                    		  }
		                                    		  if(flag == false){                        			  
		                                    			  obj.operator = null;	                                    			 
		                                    			  angular.forEach($scope.selectedCriteria, function(item, key) {
			                                    			  if(sysName == item.systemName) {		                                    				  
			                                    				  $scope['checkedRow' + sysName] = false;
			                                    				  item.checked = undefined;
			                                    				  $scope.selectedCriteria.splice(key, 1);		                                    				  
			                                    				  $scope.fExpr.splice(key, 1);
			                                    				  $scope.updatedSearchCriteria(); 
			                                    			  }
		                                    			  });
		                                    			  angular.forEach($scope.attributeMappings, function(attrItem, attrKey) {
		                                    				  if(sysName == attrKey) {	
		                                    					  attrItem.checked = undefined;
		                                    				  }
		                                    			  });
		                                    		  }
		                                    		  obj.value = [];
		                                    		  $scope.valArr = [];
		                                    		  $scope.valArr.push(obj.pickList);
		                                    		  $scope.exression = {
		                                    				  attribute: sysName
		                                    		  }
	                                    		  }
	                                    		  
	                                    	  }
	                                    	  
	                                    	 // this method is used when user click on supervisor radio button (old method)
	                                    	  $scope.addSupervisorCriteriaold = function(flag, obj, index, supId) {
																							$scope.selectedRadioBoxCriteria.splice(0, 2);	
																							$scope.supervisorId  = supId; 
																							$scope.selectedRadioBoxCriteria.push(obj);
																							$scope.supervisorDetails = supId;
											  									}

											$scope.toggleAll = function(event) {
												var toggleStatus;
												if (event.target.checked) {
													toggleStatus = true;
												} else {
													toggleStatus = false;
												}
												$scope.supervisorIds = [];
												
												if (toggleStatus) {
													angular.forEach($scope.supervisorList, function(itm) {
														// if(itm.adminRole == null) {
														// 	itm.selected = toggleStatus;
														// 	$scope.supervisorIds.push(itm.id);
														// 	if($scope.campaign.partnerSupervisors.indexOf(itm.id) == -1){
														// 		$scope.campaign.partnerSupervisors.push(itm.id);
														// 	}
														// }  else 
														if (itm.adminRole == "ADMIN_SUPERVISOR") {
															$scope.supervisorId = itm.id;
															itm.selected = toggleStatus;
															$scope.supervisorIds.push(itm.id);
															// if($scope.campaign.partnerSupervisors.indexOf(itm.id) == -1){
															// 	$scope.campaign.partnerSupervisors.push(itm.id);
															// }
														}
													});
												} else {
													angular.forEach($scope.supervisorList, function(itm) {
														if (itm.adminRole == "ADMIN_SUPERVISOR") {
															// if($scope.campaign.partnerSupervisors.indexOf(itm.id) == -1){
															// 	$scope.campaign.partnerSupervisors.push(itm.id);
															// }
															$scope.supervisorIds.push(itm.id);
														} 
														// else if(itm.adminRole == null) {
														// 	let index = $scope.campaign.partnerSupervisors.findIndex(data => data == itm.id);
														// 	$scope.campaign.partnerSupervisors.splice(index,1);
														// 	let indx = $scope.campaign.partnerSupervisors.findIndex(data => data == itm.id);
														// 	$scope.campaign.partnerSupervisors.splice(indx,1);
														// 	itm.selected = toggleStatus;
														// }
													});
												}
											 }

											 
											  $scope.addSupervisorCriteria = function(flag, obj, index, supId, event) {
												if (event.target.checked) {
												 $scope.selectedRadioBoxCriteria.splice(0, 2);	
												 $scope.supervisorId  = supId; 
												 $scope.supervisorIds.push(supId); 
												 $scope.selectedRadioBoxCriteria.push(obj);
												 if ($scope.campaign.adminSupervisorIds != null && $scope.campaign.adminSupervisorIds != undefined && $scope.campaign.adminSupervisorIds.indexOf(supId) == -1) {
													$scope.campaign.adminSupervisorIds.push(supId);
												 }
												 // $scope.supervisorDetails = supId;
												 angular.forEach($scope.supervisorList, function(itm) { 
													if(itm.id == supId) {
														itm.selected = true;
													} 
													// else if(itm.adminRole == "ADMIN_SUPERVISOR") {
													// 	let idx = $scope.supervisorIds.indexOf(itm.id); 
													// 	if(idx == -1){
													// 		$scope.supervisorIds.push(itm.id);
													// 	}
													// }
												});
												} else {
													if ($scope.campaign.adminSupervisorIds != null && $scope.campaign.adminSupervisorIds != undefined) {
													 	let indx = $scope.campaign.adminSupervisorIds.findIndex(data => data == supId);
													 	$scope.campaign.adminSupervisorIds.splice(indx,1);
													}
													 angular.forEach($scope.supervisorList, function(itm) { 
														if(itm.id == supId) {
															itm.selected = false;
															let index = $scope.supervisorIds.findIndex(data => data == supId);
															$scope.supervisorIds.splice(index,1);
															let indx = $scope.selectedRadioBoxCriteria.findIndex(item => item.id == obj.id);
															$scope.selectedRadioBoxCriteria.splice(indx,1);
														}  
														// else if(itm.adminRole == "ADMIN_SUPERVISOR"){
														// 	let idx = $scope.supervisorIds.indexOf(itm.id); 
														// 	if(idx == -1){
														// 		$scope.supervisorIds.push(itm.id);
														// 	}
														// }
													});
												}
												if($scope.supervisorList.length == $scope.supervisorIds.length){
													$scope.isAllSelected = true;
												} else {
													$scope.isAllSelected = false;
												}
											}
	                                    	  
	                                    	  // This method is used for saving supervisor and team in campaign
	                                    	$scope.saveSupervisor = function() { 	
												if ($scope.supervisorId == "") {
													if ($scope.loggedInUserOrganization.id != "XTAAS CALL CENTER") {
															angular.forEach($scope.supervisorList, function(element) {
																if (element.adminRole == "ADMIN_SUPERVISOR") {
																	$scope.supervisorId = element.id;
																	$scope.supervisorIds.push(element.id);
																	return true;
																}
															});
													} else {
														NotificationService.log('error','Please select Supervisor.');
																return false;
													}
	                                    	    }
												$scope.selectedTeamData = {
														"bid" : 0.0,
														"leadSortOrder" : "QUALITY_LIFO",
														"supervisorId" : $scope.supervisorId,
														"callSpeedPerMinPerAgent" : 0,
														"supervisorIds" : $scope.supervisorIds
												};
	                                    		  TeamSelectionService.save({campaignId: $stateParams.campaignId}, $scope.selectedTeamData).$promise.then(function(data) {
			                                            $scope.$parent.loading = false;
			                                            $scope.isSupervisorList = false;
														$scope.$parent.isCampaignTeamInvited = true;
														if($scope.campaign.status == "CREATED"){
															$scope.showLaunch = true;
														} else {
															$scope.showLaunch = false;
															NotificationService.log('success','Campaign updated successfully.');
															$state.go('listcampaigns');
														}
		                                    			
		                                    			$scope.detailOfCampaign = true;
		                                    			$scope.$parent.showTeamSearch = false;
		                                    			$scope.$parent.showinvited = false;
		                                    			$scope.showTeamSearch = true;            			
	                                    				$scope.isSelfManageTeamDisable = true;//non editable all field
	                                    				/* START
	                                    				   * DATE : 09/10/2017 
	                                    				   * REASON : allowing CM to modify all setting planning mode*/
	                                    				$scope.isNotChangeable = false;
	                                    				$scope.isIOSignedDisable=false;
	                                    				$scope.isStartDateDisable=false;
	                                    				$scope.isCampaingTypeDisable = false;
		                                    			//$scope.$parent.getDifferentTeams('selectedCampaignTeams','ByCurrentSelection');
		                                    			 /* Select Team*/
			                                    		  $scope.selectedCampaignTeams = [];	  
		                                    			  $scope.pramas = { 
	                              									"clause": 'ByCurrentSelection',
	                              									"campaignId": $stateParams.campaignId,
	                              									"page": 0,
		                              								"size": 10,
		                              								"direction": "ASC"
	                              							};
		                                    			TeamSearchService.query({searchstring: JSON.stringify($scope.pramas)}).$promise.then(function(data) {
                            								angular.forEach(data, function(v, k) {                            									
                            									$scope.selectedCampaignTeams.push(v);  
                            								}, '');
                            								
                            							},function(error) {
                            								
                            							});
		                                    		  },
	                                    		   function(error) {
	                                    				    console.log("rejected " + JSON.stringify(error));
	                                    					$scope.loading = false;
	                                    		   });
		                                    	  }
		                                    	  
	                                    	  // When user selects checkbox. e.g. Annual Revenue
	                                    	  $scope.checkedopt = function(option, index) {
	                                    		  if($scope.isIOSignedDisable != true) {
	                                    			  $scope.frmAttributeChanged = true;
		                                    		  $scope['checkedRow' + index] = option;  
	                                    		  }	                                    		  	                                    		  
	                                    	  }	                                    	                             
	                                    	  
	                                    	  //Show delete button on hovering
	                                    	  $scope.showDel = function(index) {
	                                    		  if($scope.isIOSignedDisable != true) {
	                                    			  $scope['showDelButton' + index] = true;  
	                                    		  }	                                    		  	                                    		  
	                                    	  }
	                                    	  
	                                    	  //Hide delete button on hovering
	                                    	  $scope.hideDel = function(index) {
	                                    		  if($scope.isIOSignedDisable != true) {
	                                    			  $scope['showDelButton' + index] = false;  
	                                    		  }	                                    		  	                                    		  
	                                    	  }
	                                    	  
	                                    	  $scope.getAttributeType = function(attributeName) {
	                                              if ($scope.attributeMappings[attributeName] != undefined) {
	                                                  return $scope.attributeMappings[attributeName].systemDataType;
	                                              }
	                                          };
	                                          
	                                          $scope.getAttributeLabel = function(attributeName) {
	                                              if ($scope.attributeMappings[attributeName] != undefined) {
	                                                  return $scope.attributeMappings[attributeName].label;
	                                              }
	                                          };
	                                          
	                                          $scope.getQuestionSkin = function(attributeName) {
	                                              if ($scope.attributeMappings[attributeName] != undefined) {
	                                                  return $scope.attributeMappings[attributeName].questionSkin;
	                                              }
											  };
											  
											  $scope.sortPickList = function() {
												$scope.pickData.sort(function(a, b) {
													if (a.label.toLowerCase() < b.label.toLowerCase()) return -1;
													if (a.label.toLowerCase() > b.label.toLowerCase()) return 1;
													return 0;
												});
											}
	                                          
	                                          $scope.getAttributePickList = function(attributeName) {
	                                      		  $scope.pickData = [];
	                                      		  if ($scope.attributeMappings[attributeName] != undefined) {
	                                      			  angular.forEach($scope.attributeMappings[attributeName].pickList, function(v, k) {
	                                      				  	$scope.pickData[k]=v;		
	                                      			  });
													  }
												  if (!(attributeName == 'EMPLOYEE' || attributeName == 'REVENUE')) {
													$scope.sortPickList();
												  }	  
	                                      		  return $scope.pickData;
	                                      	  };
	                                      	  
	                                      	  $scope.isGeoAttrMap = function(attribute) {
	                                      		  if ($scope.geoAttrMapping[attribute] == undefined){
	                                      			  return false;
	                                      		  }
	                                      		  return true;
	                                      	  }
	                                      	  
	                                      	$scope.getLabelAttributeSpecificValue = function(attributeName,value) {
	                                      	  	if ($scope.isGeoAttrMap(attributeName)) {
	                                      			return value;
	                                      	  	}
	                                      	  	var pickAttributeData = $scope.getAttributePickList(attributeName);
											  	var specificPicklist = $filter('filter')(pickAttributeData, value)[0];
											  	if (!specificPicklist) {
													return value;
											  	}
	                                      		return specificPicklist.label;
	                                      	};
	                                      	
	                                      	
	                                      	  
	                                      	
	                                      	  /* Set default criteria for Audience selection tab */	                                      	  
	                                      	  $scope.setDefaultSelectedCriteria = function(criteriaData) {
	                                      		  $scope.selectedCriteria = [];	                                      		
	                                      		  if(!criteriaData) {	                                      			 
	                                      			if($scope.selectedCriteria.length == 0) {
//	                                      			    DATE:14/12/2017	Do not show default selected Question ex.Department,Title
														if ($scope.campaign.type == "LeadGeneration") {
															angular.forEach($scope.attributeMappings, function(item, key) {
																if(item.defaultQuestion || item.systemName == 'EMPLOYEE' || item.systemName == 'VALID_COUNTRY' || item.systemName == 'DEPARTMENT' || item.systemName == 'MANAGEMENT_LEVEL' || item.systemName == 'INDUSTRY' || item.systemName == 'LAST_UPDATED_DATE') {		                                    				  
																	$scope['checkedRow' + item.systemName] = true;
																	item.checked = true;
																	if (item.defaultValues && item.defaultValues.length > 0) {
																		item.value = [];
																		angular.forEach(item.defaultValues, function(defaultValue, defaultValueKey) {
																			angular.forEach(item.pickList, function(pickListValue, pickListKey) {
																				if (defaultValue.label == pickListValue.label) {
																					item.value.push(pickListValue);
																				}
																			})
																		})
																	}
																	if (item.value == null || item.value == undefined) {
																		item.value = [];
																		defaultFirstSelected = false;
																		angular.forEach(item.pickList, function(pickListItem, pickListKey) {
																			if (item.systemName == 'VALID_COUNTRY') {
																				if (pickListItem.value == "USA") {
																					item.value.push(pickListItem);
																				}	
																			}else if (item.systemName == 'LAST_UPDATED_DATE') {
																				if (pickListItem.value == "60") {
																					item.value.push(pickListItem);
																				}	
																			} else if (item.systemName == 'INDUSTRY') {
																				if (pickListItem.value == "All") {
																					item.value.push(pickListItem);
																				}
																			} /* else {
																				if (!defaultFirstSelected) {
																					item.value.push(pickListItem);
																					defaultFirstSelected = true;
																				}
																			} */
																		});
																	}
																	if (item.attributeType == 'CUSTOM' && item.pickList.length == 0) {
																		item.operator = "FREE_TEXT";
																	} else {
																		item.operator = "IN";
																	}
																	$scope.selectedCriteria.push(item);	
																	$scope.exression = {
																		attribute: item.systemName
																	}
																		$scope.fExpr.push($scope.exression); 		                                    				  
																}
															});
														}else {
															angular.forEach($scope.attributeMappings, function(item, key) {
																if(item.defaultQuestion || item.systemName == 'EMPLOYEE' || item.systemName == 'VALID_COUNTRY' || item.systemName == 'DEPARTMENT' || item.systemName == 'MANAGEMENT_LEVEL' || item.systemName == 'INDUSTRY' || item.systemName == 'CDQAVALIDATIONS') {		                                    				  
																	$scope['checkedRow' + item.systemName] = true;
																	item.checked = true;
																	if (item.defaultValues && item.defaultValues.length > 0) {
																		item.value = [];
																		angular.forEach(item.defaultValues, function(defaultValue, defaultValueKey) {
																			angular.forEach(item.pickList, function(pickListValue, pickListKey) {
																				if (defaultValueKey == pickListKey) {
																					item.value.push(pickListValue);
																				}
																			})
																		})
																	}
																	if (item.value == null || item.value == undefined) {
																		item.value = [];
																		angular.forEach(item.pickList, function(pickListItem, pickListKey) {
																			if (item.systemName == 'VALID_COUNTRY') {
																				if (pickListItem.value == "USA") {
																					item.value.push(pickListItem);
																				}	
																			}else if (item.systemName == 'CDQAVALIDATIONS') {
																				//if (pickListItem.value == "Dead Contact" || pickListItem.value == "Sale Made" || pickListItem.value == "No Disposition" || pickListItem.value == "Wrong Number") {
																					item.value.push(pickListItem);
																				//}
																			} /* else {
																				if (pickListItem.value == "All") {
																					item.value.push(pickListItem);
																				}
																			} */
																		});
																	}
																	if (item.attributeType == 'CUSTOM' && item.pickList.length == 0) {
																		item.operator = "FREE_TEXT";
																	} else {
																		item.operator = "IN";
																	}
																	if (item.systemName == 'CDQAVALIDATIONS') {
																		item.agentValidationRequired = true;
																		$scope.exression = {
																			attribute: item.systemName,
																			agentValidationRequired: true
																		}
																	}else {
																		$scope.exression = {
																			attribute: item.systemName
																		}
																	}
																	
																	$scope.selectedCriteria.push(item);
																	$scope.fExpr.push($scope.exression); 		                                    				  
																}
															});
														}  
	                                      		    }   
	                                      		  } else {	                                      			
	                                      			angular.forEach(criteriaData, function(item, key) {	                                      				
	                                      				$scope['checkedRow' + item.attribute] = true;
	                                      				
	                                      				angular.forEach($scope.attributeMappings, function(mapItem, mapKey) {
	                                      					
	                                      					if(mapItem.systemName == item.attribute) {
	                                      						
	                                      						/*if (item.attribute == "Area" || item.attribute == "Country" || item.attribute == "State" || item.attribute == "City") {
                                      								$scope.geographyPresent = true;
                                      							}*/
	                                      						if ($scope.isGeoAttrMap(item.attribute)) {
	                                      							$scope.geographyPresent = true;
	                                      						}
	                                      						
	                                      						mapItem.checked = true;	
	                                      						mapItem.value = [];
	                                      						angular.forEach(item.value, function(dbexprValue, dbexprKey) {
	                                      							if(mapItem.pickList != undefined && mapItem.pickList.length > 0) {
	                                      								if (mapItem.attributeType != XTAAS_CONSTANTS.attributeType.area && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.country && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.state && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.city ) {
	                                      									$scope.pickListArr = [];
			                                      							angular.forEach(mapItem.pickList, function(pickListItem, pickListKey) {
			                                      								if (pickListItem.value == dbexprValue) {
			                                      									mapItem.value.push(pickListItem);
			                                      								}
			                                      							});
	                                      								}
		                                      						}
	                                      							
	                                      							if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area) {
	                                      								angular.forEach(arealist, function(area, index) {
	                                      									if (area.value == dbexprValue) {
	                                      										mapItem.value.push(area);
	                                      									}
	                                      								});
	                                      							}
	                                      							if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country) {
	                                      								angular.forEach(countrylist, function(country, index) {
		                                      								if (country.value == dbexprValue) {
		                                      									mapItem.value.push(country);
		                                      								}
		                                      							});
	                                      							}
	                                      							if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state) {
	                                      								angular.forEach(statelist, function(state, index) {
		                                      								if (state.value == dbexprValue) {
		                                      									mapItem.value.push(state);
		                                      								}
		                                      							});
	                                      							}
	                                      							
	                                      							
	                                      						});
	                                      						
	                                      						if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.city ) {
                                      								mapItem.target = mapItem.systemName;
                                      								mapItem.systemName = "Geography";
                                      								mapItem.label = "Geography";
                                      								mapItem.systemDataType = "popup";
                                          							mapItem.crmDataType = "popup";
                                          							mapItem.checked = true;
                                      							}
	                                      						
	                                      						if(mapItem.pickList != undefined && mapItem.pickList.length <= 0 && mapItem.systemName != "Geography") {
	                                      							mapItem.value = item.value;
	                                      						}	
	                                      						
	                                      						if(item != undefined) {
	                                      							mapItem.operator = item.operator;	
	                                      						}	  
	                                      						$scope.selectedCriteria.push(mapItem);
	                                      					}  
	                                      				});	
	                                      			});
	                                      		  }
	                                      		//Replace area, country, state, city with Geography 
	                                      		angular.forEach($scope.attributeMappings, function(mapItem, mapKey) {
	                                      			if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.city ) {
                              							if (!$scope.changedToGeography) {
                              								if ($scope.geographyPresent) {
                              									mapItem.checked = true;
                              								}
                              								$scope.changedToGeography = true;
                                  							mapItem.systemName = "Geography";
                                  							//mapItem.crmName = "Geography";
                                  							mapItem.label = "Geography";
                                  							mapItem.systemDataType = "popup";
                                  							mapItem.crmDataType = "popup";
                                  							mapItem.fromOrganization = false;
                                  							$scope.attributeMappings["Geography"] = $scope.attributeMappings[mapKey];
                                  							delete $scope.attributeMappings[mapKey];
                                  							//$scope['checkedRow' + mapItem.systemName] = true;
                                  							
                              							} else {
                              								delete $scope.attributeMappings[mapKey];
                              							}
                              							
                          							}
                              					});
	                                      		
	                                      		/*START	DATE:06/12/2017		REASON:Hack for crmmapping ("Define Further" in Audience selection)*/
												$scope.attributeMappingsTemp = {};
	                                      		angular.forEach($scope.attributeMappings, function(value, key) {
	                                      			if (value.systemName == 'ZIP_CODE' || value.systemName == 'REMOVE_INDUSTRIES' || value.systemName == 'VALID_STATE' || value.systemName == 'SEARCH_TITLE' || value.systemName == 'REMOVE_DEPT' || value.systemName == 'NEGATIVE_TITLE' ) {
	                                      				value.fromOrganization = false;
													}
													if (!$scope.campaign.enableCustomFields && (value.systemName == 'CUSTOMFIELD_01' || value.systemName == 'CUSTOMFIELD_02' 
																|| value.systemName == 'CUSTOMFIELD_03' || value.systemName == 'CUSTOMFIELD_04' || value.systemName == 'CUSTOMFIELD_05' 
																|| value.systemName == 'CUSTOMFIELD_06' || value.systemName == 'CUSTOMFIELD_07' || value.systemName == 'CUSTOMFIELD_08' 
																|| value.systemName == 'CUSTOMFIELD_09' || value.systemName == 'CUSTOMFIELD_10')) {
														value.fromOrganization = true;	
													}	
													if(!value.fromOrganization) {
														$scope.attributeMappingsTemp[key] = value;
													}
													// if(!value.fromOrganization) {
													// 	$scope.attributeMappingsTemp[key] = value;
		                                    		// }
		                                    	});
												  
												// angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionValue, expressionKey) { 	                                    						
												// 	if (expressionValue.attribute == 'SEARCH_TITLE' || expressionValue.attribute == 'SIC_CODE' || expressionValue.attribute == 'VALID_STATE' || expressionValue.attribute == 'REMOVE_DEPT' || expressionValue.attribute == 'REMOVE_INDUSTRIES') {
												// 		expressionValue.value = expressionValue.value.toString();
												// 	}
												// });
	                                      	  }  		                                       
	                                      	  
	                                      	  
	                                      	  
	                                      	  $scope.selectFromPopup = function(attribute) {
	                                      		  if ($scope.isIOSignedDisable) return;
	                                      		 	
	                                      		  $scope.popupValuesModalInstance = $modal.open({
	                                      			  scope : $scope,
	                                      			  templateUrl: 'popupValuesModal.html',
	                                      			  controller: PopValuesModalInstanceCtrl,
	                                      			  windowClass : 'popupValuesClass',
	                                      			  resolve: {
	                                      				  attribute: function(){
	                                      					  return attribute;
	                                      				  },
	                                      				  atrMappingforGeoOptions: function(){
	                                      					  return $scope.geoAttrMapping;
	                                      				  }
	                                      			  }
	                                      		  }); 
	                                      	  }
	                                      	  
	                                      	  var PopValuesModalInstanceCtrl = function ($scope, attribute, atrMappingforGeoOptions, XTAAS_CONSTANTS) {
	                                        	  //$scope.attribute = attribute;
	                                      		  $scope.targetBy = {};
	                                      		  $scope.cancel = function () {
	                                        		  $scope.popupValuesModalInstance.close();
	                                        	  };
	                                        	  
	                                        	  //$scope.atrMappingforGeoOptions = atrMappingforGeoOptions;
	                                        	 
	                                        	  $scope.targets = [];
	                                        	  $scope.selectedTarget = [];
	                                        	  if (attribute.value.length <= 0) {
	                                        		  $scope.targetBy = atrMappingforGeoOptions["Area"];
	                                        	  } else {
	                                        		  $scope.targetBy = atrMappingforGeoOptions[attribute.target];
	                                        		  $scope.targetBy.label = attribute.target;
	                                        		  angular.forEach(attribute.value, function(value, index) {
	                                        			  $scope.targets.push(value);
	                                        		  });
	                                        	  }
	                                        	  
	                                        	  
	                                        	  /*$scope.differentTargets = [
	                                        	                             {key:"Area",value:"Area"},
	                                        	                             {key:"Country", value:"Country"},
	                                        	                             {key:"State", value:"State"}
	                                        	                             ]*/
	                                        	  
	                                        	  $scope.addTarget = function() {
	                                      			  if ($scope.selectedTarget.item != undefined) {
	                                      				  $scope.targetAvailable = $filter('filter')($scope.targets, $scope.selectedTarget.item.value)[0];
	                                      				  if ($scope.targetAvailable == undefined) {
	                                      					 $scope.targets.push($scope.selectedTarget.item);
	                                      					 $scope.selectedTarget.item = undefined;
	                                      				  }
	                                      			  }
	                                      		  }
	                                      		  
	                                      		  $scope.getValuesByTarget = function() {
	                                    			  $scope.targets = [];
	                                    			  $scope.selectedTarget = [];
	                                    		  }
	                                      		  
	                                      		  $scope.removeTarget = function(item) {
	                                      			angular.forEach($scope.targets, function(value, index) {
	                                  					if(value.value == item.value) {
	                                  						$scope.targets.splice(index, 1 );
	                                  					}
	                                      			});
	                                      		  }
		                                          $scope.finalGeography = function() {
		                                        	  if ($scope.isIOSignedDisable) return; 
		                                        	  angular.forEach($scope.selectedCriteria, function(mainAttribute, index) {
		                                  					if(mainAttribute.systemName == attribute.systemName) {
		                                  						mainAttribute.value = [];
		                                  						mainAttribute.target = $scope.targetBy.systemName;
		                                  						angular.forEach($scope.targets, function(targetValue, targetIndex) {
		                                  							mainAttribute.value.push(targetValue);
		    	                                      			});
		                                  					}
		                                      			});
		                                        	  $scope.cancel();
		                                          }
	                                      	  }
	                                        	  
	                                      	  $scope.getTargetSegment = function(tab) {
	                                      		  $scope.activeTab = tab;
	                                      	  }
                                      		  
                                      		  $scope.getMandates = function(tab) {
                                      			  $scope.activeTab = tab;
                                      			
                                      		  }
                                      		  
                                      		  $scope.showMoretargets = function() {
                                      			if ($scope.isMoreTargetOpen) {
                                      				$scope.isMoreTargetOpen = false;
                                      			} else {
                                      				$scope.isMoreTargetOpen = true;
                                      			}
                                      		  }

                                      		  $scope.downloadSuppressionTemplate = function(suppressionType) {
													$scope.getSuppressionList($scope.campaign.id);
                                      			  if ($scope.campaign.status === null || $scope.campaign.status === undefined) {
                                      				  return;
                                      			  }
													if (suppressionType === "companySuppression") {
														getSuppressionListService.suppression({campaignId : $scope.campaign.id , suppressionType : suppressionType}).$promise.then(function (data) {
															console.log("Download Success");
															NotificationService.log('success','Suppression Download Request Accepted. Please Check your Email or Notification After Sometime.');
														}, function (error) {
															console.log("Error occured while downloading suppression list.");
														});
													} else if(suppressionType === "contactSuppression"){
														getSuppressionListService.suppression({campaignId : $scope.campaign.id , suppressionType : suppressionType}).$promise.then(function (data) {
															console.log("Download Success");
															NotificationService.log('success','Suppression Download Request Accepted. Please Check your Email or Notification After Sometime.');
														}, function (error) {
															console.log("Error occured while downloading suppression list.");
														});
													}
                                      		  }

											/* #################### Upload suppression file START #################### */	
                                      		  $scope.uploadSuppressionFile = function(file, errFiles, suppressionType) {
													$scope.errFile = errFiles && errFiles[0];
													var campaignSuppressionType;
													if (suppressionType == 'contactSuppression') {
														campaignSuppressionType = "contactSuppression";
													}
													else {
														campaignSuppressionType = "companySuppression";
													}
													if (file) {
														$scope.loading = true;
														file.upload = Upload.upload({
															url: `/spr/campaignsuppression/upload/${$scope.campaign.id}/${campaignSuppressionType}`,
															data: { file: file }
														});
														file.upload.then(function (response) {
															$scope.loading = false;
															file.result = response.data;
															NotificationService.log('success', 'File Upload Request Received. Please Check Your Email For Status.');
														}, function (response) {
															$scope.loading = false;
														});
													}    
												}
												/* #################### Upload suppression file END #################### */	


												/* #################### Download client mapping file START #################### */
												$scope.downloadClientMappingTemplate = function() {
													if ($scope.campaign.status === null || $scope.campaign.status === undefined) {
														return;
													}
													window.location.href = "/spr/clientMapping/downloadTemplate/" + $scope.campaign.id;
												}
												/* #################### Download client mapping file END #################### */


												/* #################### Download new client mapping file START #################### */
												$scope.downloadNewClientMappingTemplate = function() {
													if ($scope.campaign.status === null || $scope.campaign.status === undefined) {
														return;
													}
													clientMappingService.download({campaignId : $scope.campaign.id}).$promise.then(function (data) {
														console.log("Download Success");
														console.log('success','Client Mapping Success');
													}, function (error) {
														console.log("Error occured while downloading client mapping list.");
													});
													NotificationService.log('success','Client Mapping Download Request Accepted Please Check Your Email or Notification After Sometime');
													//window.location.href = "/spr/clientMapping/download/new/" + $scope.campaign.id;
												}
												/* #################### Download new client mapping file END #################### */

										

												/* #################### Upload client mapping file START #################### */
												$scope.uploadClientMappingFile = function(file, errFiles) {
													$scope.errFile = errFiles && errFiles[0];
														if (file) {
																			$scope.loading = true;
																	file.upload = Upload.upload({
																				url: `/spr/clientMapping/upload/${$scope.campaign.id}`,
																				data: { file: file }
																	});
																	file.upload.then(function (response) {
																				$scope.loading = false;
																				file.result = response.data;
																				NotificationService.log('success', 'File uploaded successfully.');
																	}, function (response) {
																				$scope.loading = false;
																	});
														}
												}
												 /* #################### Upload client mapping file END #################### */
												 

													/* #################### Upload new client mapping file START #################### */
													$scope.uploadNewClientMappingFile = function(file, errFiles) {
														$scope.errFile = errFiles && errFiles[0];
															if (file) {
																				$scope.loading = true;
																		file.upload = Upload.upload({
																					url: `/spr/clientMapping/upload/new/${$scope.campaign.id}`,
																					data: { file: file }
																		});
																		file.upload.then(function (response) {
																					$scope.loading = false;
																					file.result = response.data;
																					NotificationService.log('success', 'File uploaded successfully.');
																		}, function (response) {
																					$scope.loading = false;
																		});
															}
													}
													 /* #################### Upload new client mapping file END #################### */

											   	/* #################### Download ABM List file START #################### */
												$scope.downloadABMListTemplate = function() {
													//$scope.getABMListCount();
													if ($scope.campaign.status === null || $scope.campaign.status === undefined) {
														return;
													}
													//$scope.noOfABM = Number.parseInt($scope.abmCount);
													//if($scope.noOfABM > 20000){
														abmListCountService.multidownload({campaignId : $scope.campaign.id}).$promise.then(function (data) {
															console.log("Download Success");
															NotificationService.log('success','ABM Download Request Accepted Please Check Your Email or Notification After Sometime');
														}, function (error) {
															console.log("Error occured while downloading ABM list.");
														});
													//}else {
													//	window.location.href = "/spr/abmlist/downloadTemplate?campaignId=" + $scope.campaign.id;
													//}
												}
												/* #################### Download ABM List file END #################### */
												
											   /* #################### Upload client mapping file START #################### */
												$scope.uploadABMListFile = function(file, errFiles) {
                                      			  $scope.errFile = errFiles && errFiles[0];
                                      			  if (file) {
                                      				  $scope.loading = true;
                                      				  file.upload = Upload.upload({
                                      					  url: `/spr/abmlist/upload/${$scope.campaign.id}`,
                                      					  data: { file: file }
                                      				  });
                                      				  file.upload.then(function (response) {
                                      					  $scope.loading = false;
                                      					  file.result = response.data;
															NotificationService.log('success', 'File Upload Request Received. Please Check Your Email For Status.');
                                      				  }, function (response) {
                                      					  $scope.loading = false;
														});
													}
													   
												 
                                      		  }
											   /* #################### Upload client mapping file END #################### */

		                                       $scope.$on('$destroy', function ()  {
		  											$interval.cancel(timerstartUpdate);
		  									   });
		                                       $scope.$on('$destroy', function ()  {
		  											$interval.cancel(timerstartSaveToLocalStorage);
		  									   });
//		                                       DATE:14/12/2017	On destroy stopped calling for timerstartUpd() method
		                                       /*$scope.$on('$destroy', function ()  {
		  											$interval.cancel(timerstartUpd);
		  									   });*/
		                                       /*$scope.$on('$destroy', function ()  {
		  											$interval.cancel(timerstartSaveAssetDetails);
		  									   });*/		                                       
	                                    	  
		                                       $rootScope.frmAttributeChanged = false;
		                                       
		                                       $scope.formChanged = function () {
												   $rootScope.frmAttributeChanged = true;
		                                       }
		                                       
	                                    	  /***********************team selection start here********************************************/
	                                    	  
	                                    	  $scope.perLeadPriceSamples = [
	                                    	                                {name : "Under $10",count:20},
	                                    	                                {name : "$10 to $20",count:40},
	                                    	                                {name : "$20 to $30",count:30},
	                                    	                                {name : "$30 to $40",count:10}
	                                    	                                
	                                    	                                ]
	                                    	  $scope.completedCampaignSamples = [
	                                    	                                {name : "Under 15",count:20},
	                                    	                                {name : "Upto 30",count:40},
	                                    	                                {name : "Upto 40",count:30},
	                                    	                                {name : "Upto 50",count:10},
	                                    	                                {name : "50 and Above",count:20}
	                                    	                                
	                                    	                                ]
	                                    	  $scope.teamSizeSamples = [
	                                    	                                {name : "1 to 5",count:20},
	                                    	                                {name : "5 to 15",count:40},
	                                    	                                {name : "15 to 30",count:30},
	                                    	                                {name : "Above 30",count:10}
	                                    	                                
	                                    	                                ]
	                                    	  $scope.averageAvailibitySamples = [
	                                    	                                {name : "8 to 12 hours",count:20},
	                                    	                                {name : "5 to 8 hours",count:40},
	                                    	                                {name : "0 to 5 Hours",count:30}
	                                    	                                
	                                    	                                ]
	                                    	  
	                                    	  $scope.searchTeams = [
	                                    	                                {name : "Patorg Technolgy Team1",averageRating:3,selected :"false"},
	                                    	                                {name : "Patorg Technolgy Team2",averageRating:4,selected:"true"}
	                                    	                              
	                                    	                                
	                                    	                                ]
	                                    	  
	                                    	  $scope.isMoreFilterOpen = false;
	                                    	  $scope.showMoreFilters  = function() {
	                                    		  if ($scope.isMoreFilterOpen) {
	                                    			  $scope.isMoreFilterOpen = false;
	                                    		  }else{
	                                    			  $scope.isMoreFilterOpen = true;
	                                    		  }
	                                    	  }
	                                    	  $scope.showSearchResult=false;
	                                    	  $scope.SerachTab = false;
	                                    	  $scope.alerts = [];
	                                    	  $scope.idTimezone = [];
	                                    	  $scope.idRatings = [];
	                                    	  $scope.idLanguage = [];
	                                    	  $scope.idValid = false;
	                                    	  $scope.campaignSelectedTeams = [];
	                                    	  $scope.campaignSelectedAgents = [];
	                                    	  $scope.campaignInvitedAgents = [];
	                                    	  $scope.sortOptions = [{ 'key':'Per Lead Price', 'value': 'PerLeadPrice' },
	                                    	                        { 'key':'Total Completed Campaigns', 'value': 'TotalCompletedCampaigns' },
	                                    	                        { 'key':'Average Available Hours', 'value': 'AverageAvailableHours' },
	                                    	                        { 'key':'TeamSize', 'value': 'TeamSize' },
	                                    	                        { 'key':'Average Rating', 'value': 'AverageRating' }
	                                    	                        ];
	                                    	  $scope.sortBy = {};
	                                    	  $scope.Metrics = {};
	                                    	  $scope.geooption= 'Location';
	                                    	 // $scope.tabSwitchTeam('ByPreviousCampaign');
	                                    	  //$scope.tabSwitchTeam('ByCurrentInvitation');
	                                    	 
	                                    	  
	                                    	  /*CampaignService.get({id: $stateParams.campaignId}).$promise.then(function(data){
	                                    		  $scope.postdata=data;
	                                    		  //$scope.tabSwitchTeam('ByAutoSuggest');
	                              				
	                                    		  $scope.searchforSelectedTeam = {
	                                    				  "clause": "ByCurrentSelection",
	                                    				  "campaignId": $stateParams.campaignId,
	                                    		  };
	                                    		  
	                                    		  TeamSearchService.query({searchstring: JSON.stringify($scope.searchforSelectedTeam)}).$promise.then(function(teamdata) {
	                                    			  angular.forEach(teamdata, function(v, k) {
	                                    				  $scope.campaignSelectedTeams.push(v);         
	                                    			  }, '');
	                                    			  $scope.loading = false;
	                                    		  },function(error) {
	                                    			  $scope.loading = false;
	                                    			  $scope.loadingSearch = false;
	                                    		  });
	                                    	  });*/
	                                    	  
	                                    	  $scope.showinvited = false;
	                                    	  $scope.getInvitedTeams = function () {
	                                    		  InvitedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(data) {
	                                        		  $scope.invitedTeams = data;
	                                        		  if ($scope.invitedTeams.length != 0) {
	                                        			  $scope.showinvited = true;
	                                        			  $scope.showShortlisted = false;
	                                        		  }
	                                    		  },function(error) {
	                                        	  });
	                                    	  }
	                                    	  
	                                    	  $scope.showShortlisted = false;
	                                    	  $scope.getShortlistedTeams = function () {
	                                    		  ShortlistedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(data) {
	                                        		  $scope.shortlistedTeams = data;
	                                        		  if ($scope.shortlistedTeams.length != 0) {
	                                        			  $scope.showShortlisted = true;
	                                        			  $scope.showinvited = false;
	                                        		  }
	                                    		  },function(error) {
	                                        	  });
	                                    	  }
	                                    	  $scope.showTeamSearch = true;
	                                    	 
                                				///$scope.showTeamSearch = false;
                                				///$scope.showinvited = false;
                                				
                                    		  
	                                    	  $scope.showShortlistedTeams = function() {
	                                    		 $scope.showTeamSearch = false;
	                                    		 $scope.getShortlistedTeams();
	                                    	  }
	                                    	  
	                                    	  $scope.showInvitedTeams = function() {
	                                    		  $scope.showTeamSearch = false;
	                                    		  $scope.getInvitedTeams();
	                                    	  }
	                                    		 
	                                    	  $scope.langitems=langitemlist;		
	                                          $scope.ratingitems=ratingilist;
	                                          
	                                         /* angular.forEach($scope.ratingitems, function(ratingItems) {
                                          		if (v.teamId == todo.teamId) {
                                          			v.invited = true;
                                          			v.invitationDate = todo.invitationDate; 
                                          		} else {
                                          			v.invited=false;
                  								}
                                          	});*/
	                                          $scope.areaitems=arealist;
	                                          $scope.countryitems=countrylist;
	                                          $scope.timezoneitems=timezonelist;
	                                          $scope.stateitems = statelist;
	                                          
	                                          
	                                          $scope.idCountry = []; 
	                                          $scope.idState =[];
	                                          $scope.idCity = [];
	                                          
	                                          $scope.getTeam=[];
	                                          $scope.getCountry=[];
	                                          $scope.getState=[];
	                                          $scope.getCity=[];
	                                          $scope.getTimeZone=[];
	                                          $scope.getLanguage= [];///$scope.idLanguage;  ////
	                                          $scope.getFeedback=[];
	                                          $scope.campaign.brand  = GlobalBrandName.getGlobalBrandName();
	                                          $scope.updateCountry = function() {
	                                        	  var pexcludecountries=$scope.excludecountries;
	                                              
	                                        	 /* $scope.stateitems = StateService.query({
	                                                  countrycodes: $scope.idCountry.selectedCountries+',',
	                                                  excludecountries: false
	                                              });*/
	                                        	  $scope.getCountry=[];
	                                        	  $scope.getCountryName=[];
	                                        	  if ($scope.idCountry.selectedCountries != "" && $scope.idCountry.selectedCountries != null) {
	                                        		  $scope.getCountry.push($scope.idCountry.selectedCountries);
	                                        	  }
	                                          }  
	                                          
	                                          $scope.updateStates = function() {
	                                            $scope.getState=[];
	                            				$scope.getStateName=[];
	                            				if ($scope.idState.selectedStates != "" && $scope.idState.selectedStates != null) {
	                            					$scope.getState.push($scope.idState.selectedStates);
	                            				}
	                                          };
	                                          
	                                          $scope.updateFeedback = function(){
	                                        	  $scope.getFeedback = [];
	                                        	  if ($scope.idRatings.selectedRatings != "" && $scope.idRatings.selectedRatings != null) {
	                                        		  angular.forEach($scope.idRatings.selectedRatings, function(v, k) {
		                                        		  if( $scope.getFeedback.indexOf(v) == -1){
		                                        			  $scope.getFeedback.push(v);
		                                        		  }
		                                        	  }, '');
	                                        	  }
	                                          };
	                                          
	                                          $scope.updateLanguage = function(){
	                                        	  $scope.getLanguage = [];
	                                        	  if ($scope.idLanguage.selectedLanguages != "" && $scope.idLanguage.selectedLanguages != null) {
	                                        		  $scope.getLanguage.push($scope.idLanguage.selectedLanguages);
	                                        	  }
	                                          };
	                                        
	                                          $scope.sortBy = {};
	                                          $scope.Metrics = {};
	                                          $scope.geooption= 'Location';
	                                          $scope.direction = "ASC";
	                              			
	                                          $scope.resetSearchFields = function() {
	                                        	  $scope.idCountry.selectedCountries = null;
	                                        	  $scope.idState.selectedStates = null;
	                                        	  $scope.idRatings.selectedRatings = null;
	                                        	  $scope.idLanguage.selectedLanguages = null;
	                                        	  $scope.getCountry=[];
	                                        	  $scope.getState=[];
	                                        	  $scope.getFeedback = [];
	                                        	  $scope.getLanguage = [];
	                                        	  $scope.isTeamSearch = false;
	                                        	  $scope.showSortlisted = false;
	                                    		  $scope.showInvited = false;
	                                          }
	                                          
	                                          $scope.expandDesciption = function (index) {
	                                        	  $scope.isDesciptionExpand = {};
	                                              $scope.isDesciptionExpand[index] = true;
	                                          }
	                                          $scope.collapseDescription = function (index) {
	                                        	 
	                                              $scope.isDesciptionExpand[index] = false;
	                                          }
	                                          
	                                          $scope.getTeamSearchResult = function () {
	                                        	  
	                                          }
	                                          
	                                          $scope.getSearchTeams = function(){
	                                        	  
	                                        	  $scope.showTeamSearch = true;
	                                        	  
	                                        	  $scope.isTeamSearch = true;
	                                        	  $scope.showSortlisted = false;
	                                    		  $scope.showInvited = false;
	                                        	  
	                                        	  $scope.pageNumber = 0;
	                                        	 /* TotalVolume =  $scope.Metrics.minimumTotalVolume.split(';');
	                                        	  AverageQuality = $scope.Metrics.minimumAverageQuality.split(';');
	                                        	  AverageConversion = $scope.Metrics.minimumAverageConversion.split(';');
	                                        	  minimumTotalVolume = TotalVolume[0];
	                                        	  minimumAverageQuality = AverageQuality[0];
	                                        	  minimumAverageConversion = AverageConversion[0];*/
	                                        	  
	                                        	  counter = 0;
	                                        	  $scope.loading = true;
	                                        	  $scope.showSearchResult=true;
	                                        	  $scope.counter = 0;
	                                        	  $scope.campaignTeams=[];
	                                        	  $scope.searchTearm=[
	                                        	                      {
	                                        	                    	  name:'Team', 
	                                        	                    	  value:$scope.getTeam
	                                        	                      },
	                                        	                      {
	                                        	                    	  name:'Country', 
	                                        	                    	  value:$scope.getCountry
	                                        	                      },
	                                        	                      {
							                                                name:'State', 
							                                                value :$scope.getState
	                                        	                      },
	                                        	                      {
	                                        	                    	  name:'City', 
	                                        	                    	  value :$scope.getCity
	                                        	                      },
	                                        	                      {
	                                        	                    	  name:'TimeZone', 
	                                        	                    	  value :$scope.getTimeZone
	                                        	                      },
	                                        	                      {
	                                        	                    	  name:'Language', 
	                                        	                    	  value :$scope.getLanguage
	                                        	                      },
	                                        	                      {
	                                        	                    	  name:'Feedback', 
	                                        	                    	  value :$scope.getFeedback
	                                        	                      },
	                                        	                      ];
	                                        	  console.log($scope.geooption);
	                            		
	                                        	 // $scope.setGeographyOption($scope.geooption);
	                                        	  if ($scope.geooption == 'Time Zone') {
	                                        		  var var_timezone = $scope.getTimeZone;
	                                        		  var var_countries = [];
	                                        		  var var_states = [];
	                                        		  var var_cities = [];
	                                        	  } else {
	                                        		  var var_timezone = [];
	                                        		  var var_countries = $scope.getCountry;
	                                        		  var var_states = $scope.getState;
	                                        		  var var_cities = $scope.getCity;
	                                        	  }
	                                        	  
	                                        	 /* InvitedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(data) {
	                                        		  $scope.invitedTeam = data;
	                                        	  },function(error) {
	                                        	  });*/
	                                        	  $scope.teamCountLoaded = false;
	                                        	  $scope.searchObject = {
	                                        			  "clause": "BySearch",
	                                        			  "countries": var_countries,
	                                        			  "excludeCountries": false,
	                                        			  "states": var_states,
	                                        			  "excludeStates": $scope.excludecountries,
	                                        			  "cities": var_cities,
	                                        			  "excludeCities": $scope.excludestates,
	                                        			  "timeZones": var_timezone,
	                                        			  "languages": $scope.getLanguage,
	                                        			  "ratings": $scope.getFeedback,
	                                        			  /*"includeTeamsWithBandwidth": $scope.includeTeamsWithBandwidth,
	                                        			  "minimumAverageQuality": minimumAverageQuality,
	                                        			  "minimumAverageConversion": minimumAverageConversion,
	                                        			  "maximumPerLeadPrice": $scope.Metrics.maximumPerLeadPrice,
	                                        			  "minimumTotalVolume": minimumTotalVolume,*/
	                                        			  "includeDomain": false,
	                                        			  "campaignId": $stateParams.campaignId,
	                                        			  "page": 0,
	                                        			  "size": 10,
	                                        			  "sort": $scope.sortBy.sortByOption,
	                                        			  "direction": $scope.direction
	                                        	  };
	                                        	  
	                                        	  TeamSearchService.query({searchstring: JSON.stringify($scope.searchObject)}).$promise.then(function(data) {
	                                                    angular.forEach(data, function(v, k) {
	                                                    	angular.forEach($scope.invitedTeams, function(todo) {
	                                                    		if (v.teamId == todo.teamId) {
	                                                    			v.invited = true;
	                                                    			v.invitationDate = todo.invitationDate; 
	                                                    		} else {
	                                                    			v.invited=false;
	                            								}
	                                                    	});
	                                                    	angular.forEach($scope.shortlistedTeams, function(todo) {
	                                                    		if (v.teamId == todo.teamId) {
	                                                    			v.shortlisted = true;
	                                                    			v.shortlistedDate = todo.shortlistedDate; 
	                                                    		} else {
	                                                    			v.shortlisted=false;
	                            								}
	                                                    	});
	                                                    	$scope.campaignTeams.push(v);  
	                                                    }, '');
	                                                    console.log($scope.getTimeZone);
	                            						console.log($scope.getTimeZoneName);
	                            						$scope.getSearchBarCountryName  = $scope.getCountryName;
	                            						$scope.getSearchBarStateName  = $scope.getStateName;
	                            						$scope.getSearchBarTimeZoneName = $scope.getTimeZoneName;
	                            						$scope.getSearchBarLanguage = $scope.getLanguage;
	                            						
	                            						TeamCounterService.query({searchstring: JSON.stringify($scope.searchObject)}).$promise.then(function(data) {
	                            							$scope.teamCounter=[];
	                            							$scope.excludeStates =[];
	                            							$scope.excludeCities =[];
	                            							$scope.teamCounter = 0;
	                            							angular.forEach(data, function(val) {
	                            								var isCountFromMain = false;
                            									angular.forEach(val, function(value, key) {
                            										if (key == "_id" && value != null) {
                            											$scope.teamCounter = value;
                            											return;
                            										}
	                            									key=key.split("_");	
	                            									var kk=key[0];
	                            									var kv=key[1];
	                            									if (kk == "countries" && !isCountFromMain) {
	                            										$scope.teamCounter = value;
	                            										isCountFromMain = true;
	                            									}
	                            									if (kk == "states" && !isCountFromMain) {
	                            										$scope.teamCounter = $scope.teamCounter+value;
	                            									}
	                            									if (kk == "cities") {
	                            										$scope.teamCounter = $scope.teamCounter+value;
	                            									}
	                            									//$scope.teamCounter.push({"type":kk,"name":kv,"value":value});
	                            								});
	                            							});
	                            							$scope.teamCountLoaded = true;
	                            							if ($scope.excludeStates.length > 0) {
	                            								$scope.getState =	$scope.excludeStates;
	                            							}
	                            							if($scope.excludeCities.length > 0) {
	                            								$scope.getCity =	$scope.excludeCities;
	                            							}
	                            						},function(error) {
	                            						});
	                            						$scope.alerts = [];
	                            						$scope.alerts.push({type: 'success',msg: 'Following Teams Found in this search criteria...'});
	                            						$scope.SerachTab = true;
	                            						$scope.loading = false;
	                                        	  },function(error) {
	                                        		  $scope.loading = false;
	                                        		  $scope.loadingSearch = false;
	                                        	  });
	                                          };
	                                          
	                                          
	                                          $scope.tabSwitchTeam = function (tab){
	                              				counter = 0;
	                              				if (tab == "Search"){
	                              					$scope.alerts = [];
	                              					$scope.SerachTab = false;
	                              				} else {
	                              					$scope.showSearchResult = false;
	                              					$scope.loading = true;
	                              					$scope.campaignTeams = [];
	                              					$scope.pramas = { 
	                              							"clause": tab,
	                              							"campaignId": $stateParams.campaignId,
	                              							"page": 0,
	                              							"size": 10,
	                              							"direction": "ASC"
	                              					};
	                              					
	                              					InvitedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(data) {
	                              						$scope.invitedTeam = data;
	                              					},function(error) {
	                              						$scope.invitedTeam = [];
	                              					});
	                              					
	                              					TeamSearchService.query({searchstring: JSON.stringify($scope.pramas)}).$promise.then(function(data) {
	                              						angular.forEach(data, function(v, k) {
	                              							angular.forEach($scope.invitedTeam, function(todo) {
	                              								if (v.teamId == todo.teamId){
	                              									v.invited=true;
	                              									v.invitationDate=todo.invitationDate; 
	                              								} else {
	                              									v.invited=false;
	                              								}
	                              							});
	                              							$scope.campaignTeams.push(v);  
	                              						}, '');
	                              						busy=0;
	                              						$scope.loading = false;
	                              					},function(error) {
	                              						$scope.loading = false;
	                              						busy=0;
	                              					});
	                              				}
	                                          };
	                                          
	                                          $scope.isTeamSearch = false;
	                                          
	                                          $scope.setTeamShortlisted = function (tid,type) {
	                                        	  if($scope.organizationTypes == "NETWORK"){
	                                        		  if ($scope.shortlistedTeams.length >= 3) {
		                                        		  NotificationService.log('error','At max only three teams can be shortlisted.');
		                                        		  return;
		                                        	  }
	                                        	  }
	                                        	  $scope.loading = true;
	                                        	  $scope.shortlisteddata = {
	                                                      "teamId":tid
	                                        	  };
	                              													
	                                        	  TeamShortlistedService.query({id: $stateParams.campaignId}, $scope.shortlisteddata).$promise.then(function(data) {
	                                        		  angular.forEach($scope[type], function(v, k) {
	                                        			  if(v.id == tid){
	                                        				  v.teamIdshortlisted=true;
	                                        				  v.shortlisted = true;
	                                        				  v.shortlistedDate=new Date();
	                                        				 // $scope.campaignTeams[k] = v;
	                                        			  }
	                                        		  }, '');
	                                        		  $scope.loading = false;
	                                        		  $scope.getShortlistedTeams();
	                                        		  if($scope.organizationTypes == "NETWORK"){
	                                        			  $scope.getDifferentTeams('shortlistCampaignTeams','ByShortlist',3,"byshortlistLoaderFlag");
	                                        		  }
	                                        	  },function(error) {
	                                        		  $scope.loading = false;							
	                                        	  });
	                                          };
	                                          
	                                          $scope.cancelTeamSelection = function (teamId) {
	                                        	  $scope.loading = true;
	                                          		
	                                        	  CancelTeamShortlistedService.remove({id: $stateParams.campaignId,teamId:teamId}).$promise.then(function(data) {
	                                        		  $scope.loading = false;
	                                        		  $scope.getShortlistedTeams();
	                                        		  
	                                        		  angular.forEach($scope.campaignTeams, function(todo) {
	                                        			  if (teamId == todo.teamId) {
	                                        				  todo.shortlisted = false;
	                                        				  todo.shortlistedDate = null; 
	                                        			  }
	                                        		  });
	                                        		  if($scope.organizationTypes == "NETWORK"){
	                                        			  $scope.getDifferentTypesOfTeams();
	                                        		  }
	                                        	  },function(error) {
	                                        		  $scope.loading = false;							
	                                        	  });
	                                          }
	                                          
	                                          $scope.cancelTeamInvitation = function (teamId) {
	                                        	  $scope.loading = true;
	                                          		
	                                        	  CancelTeamInvitationService.remove({id: $stateParams.campaignId,teamId:teamId}).$promise.then(function(data) {
	                                        		  $scope.loading = false;
	                                        		  $scope.getInvitedTeams();
	                                        		  
	                                        		  angular.forEach($scope.campaignTeams, function(todo) {
	                                        			  if (teamId == todo.teamId) {
	                                        				  todo.invited = false;
	                                        				  todo.invitedDate = null; 
	                                        			  }
	                                        		  });
	                                        		  $scope.getDifferentTeams('invitedCampaignTeams','ByCurrentInvitation',10);
	                                        	  },function(error) {
	                                        		  $scope.loading = false;							
	                                        	  });
	                                          }
	                                    	  
	                                          $scope.goToTeamSearch = function() {
	                                        	 $scope.getDifferentTypesOfTeams();
	                                        	  $scope.showTeamSearch = true;
	                                          }
	                                    	  
	                                          $scope.selectTeamForBid = function() {
	                                        	  if ($scope.invitedTeams.length >= 3) {
	                                        		  NotificationService.log('error','At max only three teams can be invited for bidding.');
	                                        		  return;
	                                        	  }
	                                        	  var lastTeamFlag = false;
	                                        	  $scope.loading = true;
	                                        	  $scope.shortlistedTeamIds = [];
	                                        	  angular.forEach($scope.shortlistedTeams, function(value, key) {
	                                        		  $scope.shortlistedTeamIds.push(value.teamId);
	                                        	  }, '');
	                                        	  
	                                        	  $scope.sendinvite($scope.shortlistedTeamIds); 
	                                        	  $scope.getDifferentTypesOfTeams();
	                                          }
	                                          
	                                          $scope.isFromGeography = function (attribute) {
	                                        	  if (attribute == "Area" || attribute == "Country" || attribute == "State" || attribute == "City" ) {
	                                        		  return true;
	                                        	  }
	                                          }
	                                          
	                                          $scope.validateCampaign = function () {
	                                        	  var absentCampaignFields = [];
	                                        	  if ($scope.campaign.type == null || $scope.campaign.type == undefined) {
	                                        		  absentCampaignFields.push("Campaign Type");
	                                        	  }
	                                        	  if ($scope.campaign.startDate == null || $scope.campaign.startDate == undefined) {
	                                        		  absentCampaignFields.push("Start Date");
	                                        	  }
	                                        	  if ($scope.campaign.endDate == null || $scope.campaign.endDate == undefined) {
	                                        		  absentCampaignFields.push("End Date");
	                                        	  }
	                                        	  if ($scope.campaign.deliveryTarget <= 0) {
	                                        		  absentCampaignFields.push("Delivery Target");
	                                        	  }
	                                        	 
	                                        	  return absentCampaignFields;
	                                          }
	                                        
	                                          // This method is used for to define CRM attribute
	                                          $scope.defineInformationParameter = function() {
	                                              $scope.defineInformationParameterModalInstance = $modal.open({
                                                      scope : $scope,
                                                      templateUrl: 'defineInformationCriteria.html',
                                                      controller: DefineInformationParamterCtrl
                                                  }); 

												  $scope.defineInformationParameterModalInstance.result.then(function () {
													if ($scope.checkDuplicateQuestion()){
														return;
													}
												}, function () {
													if ($scope.checkDuplicateQuestion()){
														return;
													}
												});
                                              }
	                                          $scope.calllback = false;
	                                          var DefineInformationParamterCtrl = function ($scope) {
	                                              $scope.model = {
	                                            		  options: []
	                                              };
	                                              
	                                              $scope.cancel = function () {
                                                      $scope.defineInformationParameterModalInstance.close();
                                                  };
                                                  
                                                  $scope.addOption = function () {
                                                	  if($scope.model.optionTextKey == undefined || $scope.model.optionTextKey == ""){
                                                    	  NotificationService.log('error','Please enter Key');
    	                                        		  return;
                                                      } else {
                                                    	  var regex = new RegExp(/^[a-zA-Z0-9()? -]*$/);
                                                    	  if(!regex.test($scope.model.optionTextKey)) {
                                                    		   NotificationService.log('error','Answer option can contain only alphanumeric, hyphen, question mark and brackets().');
                                                    		   return;
                                                    	  }
                                                    	}
                                                      /*if($scope.model.optionTextValue == undefined || $scope.model.optionTextValue == ""){
                                                    	  NotificationService.log('error','Please enter Value');
    	                                        		  return;
                                                      }*/
                                                      if (angular.isDefined($scope.model.optionTextKey) /*&& angular.isDefined($scope.model.optionTextValue)*/) {
														if ($scope.model.options == undefined || $scope.model.options == null) {
															$scope.model.options = [];
														}
														  var indexOf = $scope.model.options.findIndex(item => $scope.model.optionTextKey.toLowerCase() === item.toLowerCase());
                                                    	  if (indexOf >= 0) {
                                                    		  NotificationService.clear();
                                                    		  var errmsg = "'" + $scope.model.optionTextKey + "' already exists";
                                                    		  NotificationService.log('error', errmsg);
                                                    		  return;
                                                    	  }
                                                          $scope.model.options.push($scope.model.optionTextKey);
//                                                          $scope.model.options.push($scope.model.optionTextKey);
                                                          delete $scope.model.optionTextKey;
                                                          //delete $scope.model.optionTextkey;
                                                      }
                                                  };
                                                  $scope.deleteKeyValue = function(deleteIndex){
                                                	  var fIndex = deleteIndex-1;
                                                	  $scope.model.options.splice(fIndex, 1);	
                                                  }
                                                  $scope.model.label = "";
                                                  $scope.model.pickList = "";
                                                  $scope.model.questionSkin = "";
                                                  $scope.bList = [];
                                                  $scope.save = function () {
                                                	  NotificationService.clear();
    	                                        	  if($scope.model.label == null || $scope.model.label == undefined || $scope.model.label == "") {
    	                                        		  NotificationService.log('error','Please enter Parameter Name');
    	                                        		  return;
    	                                        	  } else {
    	                                                  var regex = new RegExp(/^[a-zA-Z0-9_]*$/);
    	                                                  if(!regex.test($scope.model.label)) {
	    	                                                   NotificationService.log('error','Question name can contain only alphanumeric and underscores.');
	    	                                                   return;
    	                                                  }                                   
    	                                        	  }
    	                                        	  if($scope.model.questionSkin == null || $scope.model.questionSkin == undefined || $scope.model.questionSkin =="") {
    	                                        		  NotificationService.log('error','Please enter Question');
    	                                        		  return;
    	                                        	  } else {
    	                                                  var regex = new RegExp(/^[a-zA-Z0-9()? -]*$/);
    	                                                  if(!regex.test($scope.model.questionSkin)) {
	    	                                                   NotificationService.log('error','Question skin can contain only alphanumeric, hyphen, question mark and brackets().');
	    	                                                   return;
    	                                                  }
    	                                        	  }
                                                      $scope.model.label =  $scope.model.label;
                                                      $scope.model.questionSkin =  $scope.model.questionSkin;
													  $scope.model.fromCQ = true;
													  if ($scope.checkDuplicateQuestion()){
														  return;
													  }
                                                      var arraysDetail = $scope.model.options;
                                                    //   if (arraysDetail.length < 1) {
    	                                        	// 	  NotificationService.log('error','Please enter Answer Options');
    	                                        	// 	  return;
    	                                        	//   }
                                                      var count = 1;
                                                      for(var i=0; i<arraysDetail.length;i++){
                                                    	  if(arraysDetail[i] != ""){
                                                    			  var pList = {
                                                    	                'label': arraysDetail[i],
                                                    	                'value': arraysDetail[i]
                                                    	            };
                                                    			 $scope.bList.push(pList);
                                                    	  }
                                                      }
                                                      $scope.$parent.loading = true;
                                                      $scope.model.pickList =$scope.bList;
                                                      delete $scope.model.options;
                                                      /*START	DATE:06/12/2017		REASON:Hack for crmmapping ("Define Further" in Audience selection)*/
                                                      var selectAttr = {};
                                                      selectAttr.attribute = $scope.model.label;
                                                      selectAttr.pickList = $scope.model.pickList;
                                                      selectAttr.questionSkin = $scope.model.questionSkin;
													  if (selectAttr.pickList.length == 0) {
														selectAttr.agentValidationRequired = true;
													  } else {
														selectAttr.agentValidationRequired = false;
													  }
                                                      selectAttr.operator = "=";
                                                      selectAttr.value = [];
                                                      
                                                      	CRMSelectionService.save({
															campaignId: $stateParams.campaignId
													  	}, $scope.model).$promise.then(function(data) {
															NotificationService.log('success','Custom question created successfully.');
                                                      		$scope.retriveListDetail(selectAttr);
	                                        		  		$scope.$parent.loading = false;
	                                        		  		$scope.defineInformationParameterModalInstance.close();
	                                        		  	},function(error) {
	                                        			 	$scope.$parent.loading = false;
	                                        			  	console.log(JSON.stringify(error));
	                                        		  	});
                                                  };
                                                  
	                                          }  
                                                  
	                                         $scope.retriveListDetail = function(selectAttr) { 
	                                        	 CRMSelectionService.getByCampaignId({
	                                        			campaignId: $stateParams.campaignId
	                                        		}).$promise.then(function(response) {
	                                        			$scope.attributeMappings = angular.copy(response);
	                                                    /*START	DATE:06/12/2017		REASON:Hack for crmmapping ("Define Further" in Audience selection)*/
	    	                                    	  var geographyAttr = $scope.attributeMappingsTemp["Geography"];
	    	                                    	  $scope.attributeMappingsTemp = {};
	                                                  angular.forEach($scope.attributeMappings, function(value, key) {
														if (!$scope.campaign.enableCustomFields && (value.systemName == 'CUSTOMFIELD_01' || value.systemName == 'CUSTOMFIELD_02' 
															|| value.systemName == 'CUSTOMFIELD_03' || value.systemName == 'CUSTOMFIELD_04' || value.systemName == 'CUSTOMFIELD_05' 
															|| value.systemName == 'CUSTOMFIELD_06' || value.systemName == 'CUSTOMFIELD_07' || value.systemName == 'CUSTOMFIELD_08' 
															|| value.systemName == 'CUSTOMFIELD_09' || value.systemName == 'CUSTOMFIELD_10')) {
															value.fromOrganization = true;	
														}
	    	                                    		  if(!value.fromOrganization) {
	    	                                    			  if ((value.attributeType != XTAAS_CONSTANTS.attributeType.area) && (value.attributeType != XTAAS_CONSTANTS.attributeType.country) && (value.attributeType != XTAAS_CONSTANTS.attributeType.state) && (value.attributeType != XTAAS_CONSTANTS.attributeType.city) ) {
	    	                                    				  $scope.attributeMappingsTemp[key] = value;
	    	                                    			  }
	    	                                    		  }
														  var agentValidation = false;
														  if (value.attributeType == "CUSTOM" && value.pickList.length == 0) {
															   value.agentValidationRequired = true;
															   value.operator = "FREE_TEXT";
															   agentValidation = true;
														  }
	    	                                    		  if(selectAttr.attribute == value.label) {
	    	                                    			  $scope.selectedCriteria.push(value);
	    	                                    			  var expr = {
					                                    			  attribute : value.systemName,
			                                    		              operator: null,
			                                    		              value: [],
			                                    		              agentValidationRequired: agentValidation,
			                                    		              questionSkin: value.questionSkin,
			                                    		              pickList: value.pickList,
			                                    		              sequenceNumber: value.sequenceNumber,
			                                    		              attributeType: value.attributeType
					                                    	      };
					                                    	  $scope.fExpr.push(expr);
														   }
															$scope.checkQuestionInFexprArray(value);
	    	                                    	  });
	                                                  if (geographyAttr != null && geographyAttr != undefined) {
	                                                	  $scope.attributeMappingsTemp["Geography"] = geographyAttr;
	                                                  }
	                                                  $state.go("editcampaign");
	                                               	  $scope.calllback = true;
	                                               	  $scope.campaignSelected = false;
		                                    		  $scope.audienceSelected = true;
		                                    	      $scope.teamSelected = false;
		                                    		  $scope.defineInformationParameterModalInstance.close();	
	                                                 },function(error) {
	                                       			  $scope.defineInformationParameterModalInstance.close();
	                                       			  console.log(JSON.stringify(error));
	                                       		  });
	                                         }
																					 
																					 $scope.checkQuestionInFexprArray = function(item) {
																								angular.forEach($scope.fExpr, function(value, key) {
																										if (value.attribute == item.systemName) {
																											$scope['checkedRow' + item.systemName] = true;
																											item.checked = true;
																										}
																								})
																					 }

	                                         // This method is used for direct saving shortlisted team in case of orgType is 'NETWORK'
	                                         $scope.saveShortlistTeam = function(finalTeam){
	                                        	 $scope.team = finalTeam;
	                                        	 var absentCampaignFields = $scope.validateCampaign();
	                                        	  if (absentCampaignFields.length > 0) {
	                                        		  NotificationService.log('error','Team selection is not allow as some of the required information is missing - '+absentCampaignFields.join());
	                                        		  return;
	                                        	  }
	                                        	  $scope.acceptBidTeam = finalTeam;
	                                        	  $scope.selectedCampaignTeams = [];
	                                        	  $scope.selectedCampaignTeams.push(finalTeam);
	                                        	  $scope.loading = true;
                                        		  $scope.selectedTeamData = {
                                        				  "teamId" : finalTeam.id,
                                        				  "bid" : 0.0,
                                        				  "leadSortOrder" : "FIFO",
                                        				  "callSpeedPerMinPerAgent" : 1
                                        		  };
                                        		  TeamSelectionService.save({campaignId: $stateParams.campaignId}, $scope.selectedTeamData).$promise.then(function(data) {
                                        			  $scope.isCampaignTeamInvited = true;
                                        			  NotificationService.log('success','Team is selected successfully.');
                                        			  $scope.showTeamSearch = false;
                                        			  $scope.showinvited = false;
                                        			  $scope.showLaunch = true;
                                        			  $scope.showShortlisted = false;
                                        			  $scope.getDifferentTeams('selectedCampaignTeams','ByCurrentSelection');
                                        			  $scope.loading = false;
                                        		  },function(error) {
                                        			  $scope.loading = false;	
                                        		  });
	                                         }
	                                         
	                                          $scope.showAcceptBidModal = function(finalTeam) {
	                                        	  $scope.team = finalTeam;
	                                        	  var absentCampaignFields = $scope.validateCampaign();
	                                        	  if (absentCampaignFields.length > 0) {
	                                        		  NotificationService.log('error','Team selection is not allow as some of the required information is missing - '+absentCampaignFields.join());
	                                        		  return;
	                                        	  }
	                                        	  $scope.acceptBidmodalInstance = $modal.open({
	                                        		  scope : $scope,
	                                        		  templateUrl: 'acceptBidModal.html',
	                                        		  controller: AcceptBidModalInstanceCtrl,
	                                        		  windowClass : 'acceptBidClass',
	                                        		  resolve: {
	                                        			  team: function(){
	                                        				  return finalTeam;
	                                        			  }
	                                        		  }
	                                        	  }); 
	                                          }
	                                          
	                                          $scope.showLaunch = false;
	                                          $scope.detailOfCampaign = false;
	                                          	///$scope.showLaunch = true;
	                                          $scope.isCampaignTeamInvited = false;
	                                          
	                                          var AcceptBidModalInstanceCtrl = function ($scope, team) {
	                                        	  $scope.acceptBidTeam = team;
	                                        	  $scope.selectedCampaignTeams = [];
	                                        	  $scope.selectedCampaignTeams.push(team);
	                                        	  $scope.acceptBid = function (id) {
	                                        		  $scope.$parent.loading = true;
	                                        		  $scope.selectedTeamData = {
	                                        				  "teamId" : id,
	                                        				  "bid" : 0.0,
	                                        				  "leadSortOrder" : "FIFO",
	                                        				  "callSpeedPerMinPerAgent" : 1
	                                        		  };
	                                        		  TeamSelectionService.save({campaignId: $stateParams.campaignId}, $scope.selectedTeamData).$promise.then(function(data) {
	                                        			  $scope.$parent.loading = false;
	                                        			  $scope.$parent.isCampaignTeamInvited = true;
	                                        			  $scope.acceptBidmodalInstance.close();
	                                        			  NotificationService.log('success','Team is selected successfully.');
	                                        			  $scope.$parent.showLaunch = true;
	                                        			  $scope.$parent.showTeamSearch = false;
	                                        			  $scope.$parent.showinvited = false;
	                                        			  $scope.showTeamSearch = true;
	                                        			  
	                                        			  $scope.$parent.getDifferentTeams('selectedCampaignTeams','ByCurrentSelection');
	                                          				
	                                        		  },function(error) {
	                                        			  $scope.$parent.loading = false;	
	                                        			  $scope.acceptBidmodalInstance.close();
	                                        		  });
	                                        		  
	                                        		 
	                                          			
	                                        	  };

	                                        	  $scope.cancel = function () {
	                                        		  $scope.acceptBidmodalInstance.close();
	                                        	  };
	                                        	  $scope.activeTab = "General";
	                                        	  $scope.getGeneralInformation = function(tab) {
                                        			  $scope.activeTab = tab;
                                        			  
                                        		  }
	                                        	  
	                                        	  $scope.getTargetSegment = function(tab) {
	                                        		  $scope.activeTab = tab;
	                                        		 
                                        		  }
                                        		  
                                        		  $scope.getMandates = function(tab) {
                                        			  $scope.activeTab = tab;
                                        			
                                        		  }
                                        		  
                                        		  /*$scope.getLabelAttributeSpecificValue = function(attributeName,value) {
      	                                      		var pickAttributeData = $scope.getAttributePickList(attributeName);
      	                                      		var specificPicklist = $filter('filter')(pickAttributeData, value)[0];
      	                                      		return specificPicklist.label;
                                        		  };*/
                                        		  
                                        		  $scope.constructAddress = function(address) {
                                        			  var addressFieldArray = [];
                                        			  var addressString;
                                        			  if (address.line1) addressString = address.line1 ;
                                        			  if (address.line2) addressString = addressString + ', '+ address.line2;
                                        			  if (address.city) addressString = addressString + ', '+ address.city;
                                        			  if (address.state) addressString = addressString + ', '+ address.state;
                                        			  if (address.country) addressString = addressString + ', '+ address.country;
                                        			  if (address.postalCode) addressString = addressString + ', '+ postalCode;
                                        			  
                                        			  return addressString;
                                        		  }
	                                          };
	                                          
	                                          	$scope.sendinvite= function ( teamIds, lastTeamFlag ) {
	                                          		
	                                          		$scope.loading = true;
	                                          		$scope.invitedata = {
	                                          				"teamIds":teamIds
	                                          		};
	                              													
	                                          		TeamInvitationService.query({id: $stateParams.campaignId}, teamIds).$promise.then(function(data) {
	                                          			angular.forEach($scope.campaignTeams, function(campaignTeam, campaignTeamIndex) {
	                                          				angular.forEach(teamIds, function(invitedTeamId, invitedTeamIdIndex) {
	                                          					if (campaignTeam.teamId == invitedTeamId) {
	                                          						campaignTeam.teamIdinvited=true;
	                                          						campaignTeam.invited = true;
	                                          						campaignTeam.invitationDate=new Date();
	                                          						$scope.campaignTeams[campaignTeamIndex] = campaignTeam;
		                              							}
	                                          				});
	                                          			}, '');
	                                          			
	                                          			$scope.loading = false;
	                                          			//$scope.showTeamSearch = true;
	                                          			NotificationService.log('success','All teams are invited successfully.');
	                                          			$scope.showTeamSearch = true;
	                                          			$scope.getInvitedTeams();
	                                          		},function(error) {
	                                          			$scope.loading = false;							
	                                          		});
	                                          	};
	                                          	
	                                          	$scope.goToLaunch = function() {
													$scope.loading = true;
													SendEmailService.query({id : $stateParams.campaignId}).$promise.then(function(response) {
														NotificationService.log('success','Email sent successfully.');
														$localStorage.selectedStatus = 'PLANNING';
	                                          			$state.go('listcampaigns');
													}, function(error) {
														$localStorage.selectedStatus = 'PLANNING';
														$state.go('listcampaigns');						
													});			
	                                          	}	                                          
	                                          
	                                          	$scope.autoSuggestCampaignTeams = [];
	                                          	$scope.previousCampaignTeams = [];
	                                          	$scope.invitedCampaignTeams = [];
	                                          	$scope.selectedCampaignTeams = [];
	                                          	$scope.bestRatedCampaignTeams = [];
	                                          	$scope.shortlistCampaignTeams = [];
	                                          	
	                                          	$scope.getDifferentTeams = function(type, clause,size, loaderFlag) {//different teams-invited, suggested,  previous teams
	                                          		$scope[type] = [];
	                                          		$scope[loaderFlag] = true;
	                                          		$scope.showSearchResult=true;
	                                          		$scope.counter = 0;
	                                          		ShortlistedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(shortlistedData) {
	                                          			$scope.shortlistedTeams = shortlistedData;
	                                          			InvitedTeamService.query({id: $stateParams.campaignId}).$promise.then(function(data) {
	                                          				$scope.invitedTeams = data;
	                              							$scope.pramas = { 
	                              									"clause": clause,
	                              									"campaignId": $stateParams.campaignId,
	                              									"page": 0,
		                              								"size": size,
		                              								"direction": "ASC"
	                              							};
	                              							TeamSearchService.query({searchstring: JSON.stringify($scope.pramas)}).$promise.then(function(data) {
	                              								
	                              								angular.forEach(data, function(v, k) {
	                              									angular.forEach($scope.invitedTeams, function(invitedTeam) {
	                              										if (v.teamId == invitedTeam.teamId){
	                              											v.invited=true;
	                              											v.invitationDate=invitedTeam.invitationDate; 
	                              											v.shortlisted=false;
	                              											v.shortlisted=undefined;
	                              										}
	                              									});
	                              									angular.forEach($scope.shortlistedTeams, function(shortlistedTeam) {
	                              										if (v.teamId == shortlistedTeam.teamId){
	                              											v.shortlisted=true;
	                              											v.shortlistedDate=shortlistedTeam.shortlistedDate; 
	                              										}
	                              									});
	                              									
	                              									$scope.teamAvailable = $filter('filter')($scope[type], v.teamId)[0];
	                              									if ($scope.teamAvailable == undefined)  {
	                              										$scope[type].push(v); 
	                              									}
	                              								}, '');
	                              								busy=0;
	                              								$scope[loaderFlag] = false;
	                              								
	                              							},function(error) {
	                              								$scope[loaderFlag] = false;
	                              								busy=0;
	                              							});
	                              							
		                              						
	                              						},function(error) {
	                              							$scope.invitedTeam = [];
	                              						});
	                                          		});
	                                          	}
	                                          	$scope.flag = {};
	                                          	$scope.showteamDetail = function(teamId,type) {
	                                          		if (!($scope.flag.type == type && $scope.flag.teamId == teamId)) {
	                                          			$scope.flag = {};
	                                          		}
	                                          		$scope.flag.type = type;
	                                          		$scope.flag.teamId = teamId;
	                                          		if ($scope.flag[type+teamId]) {
	                                          			$scope.flag[type+teamId] = false;
	                                          		} else {
	                                          			$scope.flag[type+teamId] = true;
	                                          		}
	                                          	}
	                                          	
	                                          	$scope.getDifferentTypesOfTeams = function() {
	                                          		
	                                          		$scope.getDifferentTeams('autoSuggestCampaignTeams','ByAutoSuggest',3,"byAutoSuggestLoaderFlag");
	                                          		$scope.getDifferentTeams('previousCampaignTeams','ByPreviousCampaign',3, "byPreviousCampaignLoaderFlag");
	                                          		$scope.getDifferentTeams('invitedCampaignTeams','ByCurrentInvitation',10,"byCurrentInvitationLoaderFlag");
	                                          		$scope.getDifferentTeams('selectedCampaignTeams','ByCurrentSelection',10, "byCurrentSelectionLoaderFlag");
	                                          		$scope.getDifferentTeams('bestRatedCampaignTeams','ByRating',3, "byRatingLoaderFlag");
	                                          		$scope.getDifferentTeams('shortlistCampaignTeams','ByShortlist',3,"byshortlistLoaderFlag");
				                                          		
	                                          		$scope.generalInformation = [
		                                          	                             	{label:"Campaign Name" , value :$scope.campaign.name},
		                                          	                             	{label:"Brand Name" , value :$scope.campaign.brand},
		                                          	                             	{label:"Campaign Type" , value :$scope.campaign.type},
		                                          	                             	{label:"Campaign Runs" , value :"From: "+$filter('date')($scope.campaign.startDate, 'MM/dd/yyyy')+" to "+$filter('date')($scope.campaign.endDate, 'MM/dd/yyyy')},
		                                          	                             	{label:"Target" , value :$scope.campaign.deliveryTarget},
		                                          	                             	{label:"Daily Cap" , value :$scope.campaign.dailyCap}
		                                          	                             	
		                                          	                             ]
		                                          	
	                                          		
	                                          		$scope.getSymbolLabel = function(symbol) {
	                                          			switch (symbol) {
                                          		            case "=":
                                          		                return "is";
                                          		            case "!=":
                                        		                return "is not Equal to";
                                          		            case ">":
                                          		            	return "greater Than";
                                        		            case "<":
                                        		            	return "less Than";
                                        		            case ">=":
                                          		                return "greater than or equals";
                                          		            case "<=":
                                        		                return "less than or equals";
                                          		            default:
                                          		            	return symbol;
	                                          			}
	                                          		}
	                                          		
	                                          		
	                                          	}
	                                          	$scope.loadMore = function() {
	                                               if ( busy==0 && ($scope.campaignTeams.length%10 == 0 && $scope.campaignTeams.length > 0)){
	                                                	/*TotalVolume =  $scope.Metrics.minimumTotalVolume.split(';');
	                                                	AverageQuality = $scope.Metrics.minimumAverageQuality.split(';');
	                                    			    AverageConversion = $scope.Metrics.minimumAverageConversion.split(';');
	                                    			    minimumTotalVolume = TotalVolume[0];
	                                    			    minimumAverageQuality = AverageQuality[0];
	                                    			    minimumAverageConversion = AverageConversion[0];*/
	                                            	   
		                                            	
	                                                    $scope.loadingSearch = true;
	                                                    busy = 1;
	                                                    counter = counter+1;
	                                                    $scope.searchTeam = [
					                                                    {
					                                                        name:'Team', 
					                                                        value:$scope.ptteam
					                                                    },
				
					                                                    {
					                                                        name:'Country', 
					                                                        value:$scope.getCountry
					                                                    },
				
					                                                    {
					                                                        name:'State', 
					                                                        value :$scope.getState
					                                                    },
				
					                                                    {
					                                                        name:'City', 
					                                                        value :$scope.getCity
					                                                    },
				
					                                                    {
					                                                        name:'TimeZone', 
					                                                        value :$scope.getTimeZone
					                                                    },
				
					                                                    {
					                                                        name:'Language', 
					                                                        value :$scope.getLanguage
					                                                    },
				
					                                                    {
					                                                        name:'Feedback', 
					                                                        value :$scope.getFeedback
					                                                    },
	                                                    ];
	                                		
	                                                   // $scope.setGeographyOption($scope.geooption);
	                                                    
	                                                    	$scope.pageNumber = $scope.pageNumber+1;
	                                                    	$scope.searchObject={
	                                                        		 "clause": "BySearch",
	                                                                 "countries": $scope.getCountry,
	                                                                 "excludeCountries": false,
	                                                                 "states": $scope.getState,
	                                                                 "excludeStates": $scope.excludecountries,
	                                                                 "cities": $scope.getCity,
	                                                                 "excludeCities": $scope.excludestates,
	                                                                 "timeZones": $scope.getTimeZone,
	                                                                 "languages": $scope.getLanguage,
	                                                                 "ratings": $scope.getFeedback,
	                                                                /* "includeTeamsWithBandwidth": $scope.includeTeamsWithBandwidth,
	                                                                 "minimumAverageQuality": minimumAverageQuality,
	                                                                 "minimumAverageConversion": minimumAverageConversion,
	                                         						 "maximumPerLeadPrice": $scope.Metrics.maximumPerLeadPrice,
	                                         						 "minimumTotalVolume": minimumTotalVolume,*/
	                                                                 "includeDomain": true,
	                                                                 "campaignId": $stateParams.campaignId,
	                                                                 "page": $scope.pageNumber,
	                                                                 "size": 10,
	                                                                 "sort": $scope.sortBy.sortByOption,
	                                                                 "direction": $scope.direction
	                                                        };


	                                                        TeamSearchService.query({
	                                                            searchstring: JSON.stringify($scope.searchObject)
	                                                        }).$promise.then(function(data) {
	                                                            ///	alret('i m here');										console.log(data);
	                                                            angular.forEach(data, function(v, k) {
	                                				
	                                                               
	                                							   angular.forEach($scope.invitedTeam, function(todo) {
	                                      								
	                                									if(v.teamId==todo.teamId){
		                                									v.invited=true;
		                                									v.invitationDate=todo.invitationDate; 
	                                									}else{
	                                										v.invited=false;
	                                									}
	                                								});
	                                							
	                                							 $scope.campaignTeams.push(v);  
	                                                    
	                                                    
	                                				
	                                                            }, '');
	                                                            busy=0;
	                                                            $scope.loadingSearch = false;
	                                                        ///
	                                                        },
	                                                        function(error) {
	                                                           

	                                                            $scope.loading = false;
	                                                            $scope.loadingSearch = false;
	                                                            busy=0;
	                                                        });
	                                               }
	                                            };
	                                        
	                                          	
	                                          
	                                          
	                                          /*********************team selection end here**************************************************/
	                                           
	                                      }]).directive('focusMe', function ($timeout, $parse) {
	                                    	  return {
	                                    		  link: function (scope, element, attrs) {
	                                    		  var model = $parse(attrs.focusMe);
	                                    		  scope.$watch(model, function (value) { 
	                                    			  if (value === true) {
	                                    				  $timeout(function () {
	                                    					  scope.$apply(model.assign(scope, false));
	                                    					  element[0].focus();
	                                    				  }, 30);
	                                    			  }
	                                    		  });
	                                    	  }
	                                    	  };
	                                      }).directive('numberConverter', function() {
	                                    	  return {
	                                    		    priority: 1,
	                                    		    restrict: 'A',
	                                    		    require: 'ngModel',
	                                    		    link: function(scope, element, attr, ngModel) {
	                                    		      function toModel(value) {
	                                    		        return "" + value; // convert to string
	                                    		      }

	                                    		      function toView(value) {
	                                    		        return parseInt(value); // convert to number
	                                    		      }

	                                    		      ngModel.$formatters.push(toView);
	                                    		      ngModel.$parsers.push(toModel);
	                                    		    }
	                                    		  };
	                                    		}).directive('numbersOnly', function () {
	                                    		    return {
	                                    		        require: 'ngModel',
	                                    		        link: function (scope, element, attr, ngModelCtrl) {
	                                    		            function fromUser(text) {
	                                    		                if (text) {
	                                    		                    var transformedInput = text.replace(/[^0-9]/g, '');

	                                    		                    if (transformedInput !== text) {
	                                    		                        ngModelCtrl.$setViewValue(transformedInput);
	                                    		                        ngModelCtrl.$render();
	                                    		                    }
	                                    		                    return transformedInput;
	                                    		                }
	                                    		                return undefined;
	                                    		            }            
	                                    		            ngModelCtrl.$parsers.push(fromUser);
	                                    		        }
	                                    		    };
	                                    		}).directive('showonhoverparent',
	                                    				   function() {
	                                    		      return {
	                                    		         link : function(scope, element, attrs) {
	                                    		            element.parent().bind('mouseenter', function() {
	                                    		                element.show();
	                                    		            });
	                                    		            element.parent().bind('mouseleave', function() {
	                                    		                 element.hide();
	                                    		            });
	                                    		       }
	                                    		   };
	                                    		}).directive('readsOnly', function () {
	                                    		    return  {
	                                    		        restrict: 'A',
	                                    		        link: function (scope, elm, attrs, ctrl) {
	                                    		            elm.on('keydown', function (event) {                
	                                    		                    event.preventDefault();
	                                    		                    return false;                
	                                    		            });
	                                    		        }
	                                    		    }
	                                    		}).filter('geographyOptions', function(XTAAS_CONSTANTS) {
	                                    			   return function(collection, keyname) {
	                                    				      var output = []; 
	                                    				      
	                                    				      angular.forEach(collection, function(value, key) {
	                                    				    	  if (value.attributeType == XTAAS_CONSTANTS.attributeType.area || value.attributeType == XTAAS_CONSTANTS.attributeType.country  || value.attributeType == XTAAS_CONSTANTS.attributeType.state || value.attributeType == XTAAS_CONSTANTS.attributeType.city) {
	                                    				        	 output.push(value);
	                                    				          }
	                                    				      });

	                                    				      return output;
	                                    				   };
	                                    				});

	
	


	/*var ModalInstanceCtrl = function ($scope, $modalInstance) {
		$scope.result = {
		};

		$scope.ok = function () {
			$modalInstance.close($scope.result.data);
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};*/
});/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


