define(['angular', 'campaign/campaign-service', 'notification-service'], function(angular) {
	angular.module('DashboardController', ['campaign-service', 'notification-service'])
		.controller('DashboardController', [
		                                      '$scope', 
		                                      '$modal',
		                                      '$state',
		                                      '$location',
		                                      'NotificationService',
		                                      '$localStorage',
		                                      '$stateParams',
		                                      function ($scope, $modal, $state, $location, NotificationService, $localStorage, $stateParams) {
		                                    	  
		                                    	  // fetching current state i.e. url path like http://localhost:8080/#!/dashboard
		                                    	  $scope.uiRouterState = $state;
		                                    	  
		                                    	  
	}])
});
