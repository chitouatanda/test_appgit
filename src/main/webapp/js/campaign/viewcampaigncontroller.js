define(['angular', 'campaign/campaign-service', 'notification-service'], function(angular) {
	angular.module('ViewCampaignController', ['campaign-service', 'notification-service'])
		.controller('ViewCampaignController', [
		                                      '$scope',
		                                      '$modal',
		                                      '$stateParams',
		                                      'GlobalCampaignTypes',
		                                      'CampaignService',
		                                      'TeamSearchService',
		                                      'AgentSearchService',
		                                      'CampaignCommandService',
		                                      'TeamSelectionService',
		                                      'AgentSelectionService',
		                                      'NotificationService',
		                                       function ($scope, $modal, $stateParams, GlobalCampaignTypes, CampaignService, TeamSearchService, AgentSearchService, CampaignCommandService, TeamSelectionService, AgentSelectionService, NotificationService) {
		  $scope.campaignTypes = GlobalCampaignTypes.getGlobalCampaignTypes();
    	  $scope.getType = function(type){
    		  for(i=0; i<$scope.campaignTypes.length;i++){
    			  if($scope.campaignTypes[i].value == type)
    			  return $scope.campaignTypes[i].key;
    		  }
    	  }
         $scope.isCampaignCollapsed = false;
         $scope.isLeadCollapsed = false;
         $scope.isSelectedTeamCollapsed = false;
         $scope.isInvitedTeamCollapsed = false;
         $scope.isAgentSelectedCollapsed = false;
         $scope.isAgentInvitedCollapsed = false;
         $scope.runvalue = 0;
         $scope.pausevalue = 0;
         $scope.resumevalue = 0;
         $scope.stopvalue = 0;
         $scope.campaignSelectedTeams = [];
         $scope.campaignInvitedTeams = [];
        
         $scope.campaignInvitedAgents = [];
         $scope.campaignSelectedAgents = [];
        
         //show the initial list of agents or team
         CampaignService.get({id: $stateParams.campaignId}).$promise.then(function(data){
        	 $scope.campaign=data;
          
        	 if ($scope.campaign.status !== "CREATED" || $scope.campaign.status !== "BIDDING") {
    	     	 $scope.search={
    	                  "clause": "ByCurrentSelection",
    	                  "campaignId": $stateParams.campaignId,
    	          };
    	      }
    	      if ($scope.campaign.status === "BIDDING") {
    	     	 $scope.search={
    	                  "clause": "ByCurrentInvitation",
    	                  "campaignId": $stateParams.campaignId,
    	          };
    	      }
    	      
    	     
    	      //condition for agents
    	      if ($scope.campaign.selfManageTeam == true) {
    	    	  $scope.searchforselectedAgents={
    	                  "clause": "ByCurrentSelection",
    	                  "campaignId": $stateParams.campaignId,
    	          };
    	    	  $scope.searchforInvitedAgents={
    	                  "clause": "ByCurrentInvitation",
    	                  "campaignId": $stateParams.campaignId,
    	          };
    	    	  
    	    	  //search for initial list of selected agents
    	    	  AgentSearchService.query({
     	             searchstring: JSON.stringify($scope.searchforselectedAgents)
     	         }).$promise.then(function(agentdata) {
     	             ///	alret('i m here');										console.log(data);
     	             angular.forEach(agentdata, function(v, k) {
     			
     	                 $scope.campaignSelectedAgents.push(v);         
     	         
     	             }, '');
     	            
     	             $scope.loading = false;
     	        },
     	         function(error) {
     	                
     	             $scope.loading = false;
     	             $scope.loadingSearch = false;
     	    
     	         });
    	    	  //end of initial list of selected agents
    	    	 
    	    	  //start of initial list of invited agents
    	    	  AgentSearchService.query({
     	             searchstring: JSON.stringify($scope.searchforInvitedAgents)
     	         }).$promise.then(function(agentdata) {
     	            
     	        	 
     	        	if($scope.campaignSelectedAgents.length != 0) {
     	        		angular.forEach(agentdata, function(v, k) {
     	        			$scope.valid = 0;
     	            		angular.forEach( $scope.campaignSelectedAgents, function(todo) {
     	            			if(v.id == todo.id) { //condition to avoid selected agents
	    							$scope.valid = 1;
	    						}
     	            		});
     	            		if ($scope.valid != 1) {
     	            			$scope.campaignInvitedAgents.push(v); 
     	            		}
     	            	}, '');
     	            } else {
     	            		angular.forEach(agentdata, function(v, k) {
     	            			 $scope.campaignInvitedAgents.push(v);   
     	            		  }, '');
     	            	}
     	            $scope.loading = false;
     	         },
     	         function(error) {
     	             $scope.loading = false;
     	             $scope.loadingSearch = false;
     	         });//end of initial list of invited agents
    	      } else {//end of agent condition
    	    	  //initial list of invited team or selected team
        	      TeamSearchService.query({
        	             searchstring: JSON.stringify($scope.search)
        	         }).$promise.then(function(teamdata) {
        	             ///	alret('i m here');										console.log(data);
        	             angular.forEach(teamdata, function(v, k) {
        	            	 if ($scope.search.clause === "ByCurrentSelection") {
        	            		 $scope.campaignSelectedTeams.push(v);
        	            	 }
        	            	 else {
        	            		 $scope.campaignInvitedTeams.push(v);   
        	            	 }
        	                       
        	         
        	             }, '');
        	            
        	             $scope.loading = false;
        	        },
        	         function(error) {
        	                
        	             $scope.loading = false;
        	             $scope.loadingSearch = false;
        	    
        	         });//end of initial list of invited team or selected team
        	      
    	      }
          
    	      
         }); //end of the initial list of agents or team
         
        
         
         $scope.isExist = function(restriction) {
        	 if(restriction == null || restriction == undefined || restriction == '')
        		 return false;
        	 else
        		 return true;
 		
 		};
 		
 		//function to get current time
 		$scope.getDatetime = function() {
 			  return new Date();
 		};
 		
 		//funtion for select agent or team
 		$scope.selectTeamOrAgent = function(id, index) {
 			
 			eval("$scope.hideValue_"+index+" = 0;");
 			$scope.selectDisable = 1; 
 			
 			$scope.searchforselected={
	                  "clause": "ByCurrentSelection",
	                  "campaignId": $stateParams.campaignId,
	             };
 			
 			//condition for select team
 			if($scope.campaign.selfManageTeam == false) {
 				 $scope.campaignTeam = {
 	  					"teamId" : id,
 	  					"bid" : 0.0,
 	  					"leadSortOrder" : "FIFO",
 	  					"callSpeedPerMinPerAgent" : 1
 	  			};
 				TeamSelectionService.save({
 	 				campaignId: $stateParams.campaignId 
 	             }, $scope.campaignTeam).$promise.then(function(data) {
 	            	 $scope.newcampaign=data;	
 	               });
 			}//end of select team 
 			else { //condition for select agent
 				AgentSelectionService.save({campaignId: $stateParams.campaignId,agentId:id}, {}).$promise.then(function(data) {
 	 				AgentSearchService.query({
		             searchstring: JSON.stringify($scope.searchforselected)
	 	             }).$promise.then(function(agentdata) { //start of push selected agent
	 	            	 
		 	             $scope.campaignSelectedAgents.length = 0;//clear selected agents
			        	 
			             angular.forEach(agentdata, function(v, k) {
					
			                 $scope.campaignSelectedAgents.push(v);         
			         
			             }, '');
			            
			             $scope.loading = false;
			        
		 	             },
		 	             function(error) {
				             $scope.loading = false;
				             $scope.loadingSearch = false;
			    
		 	             });//end of pushing the selected agent
	 	            });
 			}//end of condition for select agent
 		};//end of function to select agent or team
		
		$scope.UnselectTeamOrAgent = function(index) {
	       	eval("$scope.hideValue_"+index+" = 1;");
	   };
 		
 		 $scope.save = function(command) {
 			$scope.commanddata	={
                     "campaignCommand":command
                 };
 			if(command =='Run' && $scope.campaign.endDate < new Date()) {
	       		 var modalInstance = $modal.open({
	  			      templateUrl: 'runModal.html',
	  			    controller: ModalInstanceCtrl  
	       		 }); 
 			} else {
 				$scope.loading = true;
 				CampaignCommandService.query({
 	                 id: $stateParams.campaignId
 	             }, $scope.commanddata).$promise.then(function(data) {
 	            	$scope.loading = false;
 	            	 switch (command) {
 	            	 case 'Plan': 
 	            		 $scope.campaign.status = "PLANNING";
 						 NotificationService.log('success','Campaign moved to Planning state');
 	         			 break;
 	            	 case 'Run': 
 	            		 $scope.pausevalue=1;
 	            		 $scope.runvalue=0;
 	            		 $scope.campaign.status = "RUNNING";
 						NotificationService.log('success','Campaign moved to Running state');
 	         			break;
 	         		case 'Pause': 
 	         			$scope.resumevalue=1;
 	         			$scope.pausevalue=0;
 	         			$scope.campaign.status = "PAUSED";
 	         			NotificationService.log('success','Campaign Paused successfully');
 	         			break;
 	         		case 'Stop':
 	         			$scope.stopvalue=1;
 	         			$scope.campaign.status = "STOPPED";
 						NotificationService.log('success','Campaign Stopped successfully');
 	         			break;
 	         		case 'Resume':
 	         			$scope.resumevalue=0;
 	         			$scope.pausevalue=1;
 	         			$scope.campaign.status = "RESUME";
 						NotificationService.log('success','Campaign Resumed successfully');
 	         			break;
 	         		default:
 						NotificationService.log('success','Campaign is in Unknown state');
 	            	}
 	            	
 	             });
 				$scope.loading = false;
 			}
 			
     	};
                                    	  
	}]);
	
	var ModalInstanceCtrl = function ($scope, $state, $modalInstance) {
		$scope.ok = function (id) {
			$modalInstance.close($state.go('editcampaign', { 'campaignId' : id }));
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};
	
});
