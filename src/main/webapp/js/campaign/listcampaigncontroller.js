define(['angular', 'campaign/campaign-service', 'notification-service', 'login-service'], function(angular) {
	angular.module('ListCampaignController', ['campaign-service', 'notification-service', 'login-service'])
		.controller('ListCampaignController', [
		                                      '$scope', 
		                                      '$modal',
		                                      'CampaignService',
		                                      'GlobalCampaignTypes',
		                                      '$state',
		                                      '$location',
		                                      'campaignlist',
		                                      'campaignstatus',
		                                      'NotificationService',
		                                      '$localStorage',
		                                      'CampaignCommandService',
		                                      '$stateParams',
																					'$timeout',
																					'user',
																					'campaignSearchService',
																					'$rootScope',
																					'LoginService',
		                                      function ($scope, $modal, CampaignService, GlobalCampaignTypes, $state, $location, campaignlist, campaignstatus, NotificationService, $localStorage, CampaignCommandService, $stateParams, $timeout, user, campaignSearchService,$rootScope,LoginService) {
		  //Get CampaignTypes from global service 
          $scope.campaignTypes = GlobalCampaignTypes.getGlobalCampaignTypes();
    	  $scope.getType = function(type) {
    		  for(i=0; i<$scope.campaignTypes.length;i++){
    			  if($scope.campaignTypes[i].value == type)
    			  return $scope.campaignTypes[i].key;
    		  }
		  }
		  
		  if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("CAMPAIGN_MANAGER")) {
			$state.go('login');
			return;
	  	   }
			if($localStorage.validUser == null || $localStorage.validUser == undefined || $localStorage.validUser == '') {
				var temp = LoginService.logout();
				temp.then(function() {
					$rootScope.isUserLoggedIn = false;				
					$localStorage.$reset();	
					$location.path('/login');
				});
			}	  
    	  $scope.uiRouterState = $state;
    	  
	      //get all campaigns list
    	  $scope.allcampaigns = campaignlist;
    	  $scope.campaignCount = $scope.allcampaigns.campaignCount;
	      $scope.campaignLength =  $scope.allcampaigns.campaignsList.length;
				$scope.columnSort = "name";
				$scope.columnASC == true;
	      $scope.noSize = false;
    	  $scope.allCampaignStatus = campaignstatus;
				$scope.noStatus = false;
				$scope.campaignName = '';
	      //If no campaign present
	      $scope.noCampaign = false; 
	          if($scope.campaignLength == 0) {
	        	  $scope.noCampaign = true;
	        	  $scope.noStatus = false;
	          }
        $scope.isCampaignSearched = false;
              
          //Show Rows drop down
          $scope.totalPageSize = [
	    	                  { id: 10, name: '10 rows' },
	    	                  { id: 20, name: '20 rows' },
	    	                  { id: 30, name: '30 rows' },
	    	                  { id: 40, name: '40 rows' },
	    	                  { id: 60, name: '60 rows' }
	    	                ];
		  $scope.pageSize = $scope.totalPageSize[0].id;
		  //Table Header 
		  // $scope.campaignHead = {
			// 	    name: "Name",
			// 			type: "Type",
			// 			startDate: "Start Date",
			// 			endDate: "End Date",
			// 			deliveryTarget: "Achieved Vs Target",
			// 			status: "status"
		  // };
		 
		  $scope.notSorted = function(obj) {
		        if (!obj) {
		            return [];
		        }
		        return Object.keys(obj);
		    }
		  
		  //Add new Campaign
		  $scope.addCampaign = function () {
			  $location.path('newcampaign');
		  }
//		  DATE:14/12/2017	Added method to show loader on NEW campaign
		  $scope.newCampaignCreation = function() {
			  $scope.loading = true;
			  $state.go('newcampaign');
		  }
		  
		  //Pagination
			// $scope.currentPage = 1;
			$scope.main = {
					page: 0,
					take: 10
			};

			$scope.searchCampaignsByName = function(campaignName) {
							$scope.campaignName = campaignName;
							$scope.loading = true;
					if (campaignName == '' || campaignName == null || campaignName == undefined) {
								$scope.isCampaignSearched = false;
								$scope.pageChanged($scope.selectedStatus, undefined, $scope.currentPage, $scope.pageSize);
					} else {
								$scope.isCampaignSearched = true;
								campaignSearchService.query({searchstring: angular.toJson({"campaignName" : campaignName != undefined ? campaignName : "","campaignManagerId":user.id, "page" : $scope.currentPage - 1, "size" : $scope.pageSize})}, null).$promise.then(function(data) { 
										$scope.allcampaigns.campaignsList = data.campaigns;
										$scope.campaignCount =  data.campaignCount;
										$scope.noSize = false;
										$scope.noStatus = false;
										if ($scope.campaignCount == 0 ) {
													$scope.noStatus = true;
													$scope.noCampaign = false;
										}
										if ($scope.campaignCount < 10 ) {
													$scope.noSize = true;
										}
										$scope.loading = false;
								}, function(error) {
										$scope.loading = false;
						});
			  }
		}

		$scope.openConfirmationModal = function(campaignAction, campaignId) {
						$scope.confirmCampaignActionModalInstance = $modal.open({
						scope : $scope,
						templateUrl : 'confirmCampaignActionModal.html',
						controller : ConfirmCampaignActionModalInstanceCtrl,
						windowClass : 'confirm-Action-modal',
						resolve: {
								actionAttribute: function() {
									return campaignAction;
								},
								campaignId: function() {
									return campaignId;
								}
						}
			});
				if (campaignAction == 'Pause') {
					$scope.classes =  "Are you sure you want to mark the campaign as Paused";
					$scope.detailMsg = "This disables calling on the campaign. You can enable calling on the campaign anytime by moving the campaign from Paused to Running state.";
				}
				if (campaignAction == 'Run') {
					$scope.classes =  "Are you sure you want to mark the campaign as Running";
					$scope.detailMsg = "Once the campaign is marked as Running, the dialer will start making calls as agents assigned on the campaign log in."; 
				}
		};
		
		var ConfirmCampaignActionModalInstanceCtrl = function ($scope, actionAttribute, campaignId) {
			$scope.confirmAction = function() {													
					$scope.changeCampaignStatus(actionAttribute, campaignId);
					$scope.confirmCampaignActionModalInstance.close();
			};
			
			$scope.cancel = function() {
					$scope.confirmCampaignActionModalInstance.close();
			}
		}

		$scope.changeCampaignStatus = function(actionAttribute, campaignId) {
			$scope.loading = true;
			$scope.commanddata	= {
						 "campaignCommand":actionAttribute
			};
			CampaignCommandService.query({
					 id: campaignId
			 }, $scope.commanddata).$promise.then(function(data) {
			
				 switch (actionAttribute) {
							case 'Run': 
										$scope.pausevalue=1;
										$scope.runvalue=0;
										NotificationService.log('success','Campaign moved to Running state');
										if ($scope.campaignName == '' || $scope.campaignName == undefined || $scope.campaignName == null) {
												$scope.pageChanged($scope.selectedStatus, undefined, $scope.currentPage, $scope.pageSize);
										} else {
												$scope.searchCampaignsByName($scope.campaignName);
										}
										break;
							case 'Pause': 
										$scope.resumevalue=1;
										$scope.pausevalue=0;
										// $scope.campaign.status = "PAUSED";
										NotificationService.log('success','Campaign Paused successfully');
										if ($scope.campaignName == '' || $scope.campaignName == undefined || $scope.campaignName == null) {
												$scope.pageChanged($scope.selectedStatus, undefined, $scope.currentPage, $scope.pageSize);
									  } else {
												$scope.searchCampaignsByName($scope.campaignName);
									  }
										break;
						default:
										NotificationService.log('success','Campaign is in Unknown state');
				}
			 });
				$scope.loading = false;
		}
		  
		  $scope.pageChanged = function(selectedStatus, selectedName, currentPage, pageSize) {
						if ($scope.campaignName != '' && $scope.campaignName != null && $scope.campaignName != undefined) {
									$scope.searchCampaignsByName($scope.campaignName);
						} else {
									$scope.loading = true;
									$scope.isCampaignSearched = false;
									$scope.sortColumn = $scope.columnSort;
									$localStorage.selectedStatus =  selectedStatus;
									$localStorage.currentPage =  currentPage;
									$localStorage.pageSize =  pageSize;
									if ( $scope.columnASC == true) {
											$scope.direction = "ASC";
									}
									if ($scope.columnDESC == true) {
											$scope.direction = "DESC";
									}
									CampaignService.query({searchstring :angular.toJson({"clause": "ByCampaignManager", "direction": $scope.direction, "sort" : $scope.sortColumn, "status" : selectedStatus, "name" : selectedName, "page" : currentPage-1, "size" : pageSize})}).$promise.then(function(data) {
											$scope.allcampaigns = data;
											$scope.campaignCount =  $scope.allcampaigns.campaignCount;
											$scope.campaignLength =  $scope.allcampaigns.campaignsList.length;
											$scope.noSize = false;
											$scope.noStatus = false;
											if ($scope.campaignCount == 0 ) {
														$scope.noStatus = true;
														$scope.noCampaign = false;
											}
											if ($scope.campaignLength < 10 ) {
												$scope.noSize = true;
											}
											$scope.loading = false;
									});
								}
				}//End of pageChanged function

		
		// sorting                              
		$scope.sort = {
			column: 'name',
			direction: false
		};				

				$scope.changeSorting = function(column, selectedStatus, selectedName, pageSize, currentPage) {
					$scope.changeBgColor = {};
					$scope.loading = true;
					$scope.isCampaignSearched = false;
					$scope.columnSort = column;			
					$scope.changeBgColor[column] = true;
					$localStorage.column =  column;
					
								var sort = $scope.sort;
								if (sort.column == column) {
									sort.direction = !sort.direction;
									 if(sort.direction == true){
										$localStorage.direction = sort.direction;
												CampaignService.query({searchstring :angular.toJson({"clause": "ByCampaignManager", "direction": "ASC", "sort" : column, "status" : selectedStatus, "name" : selectedName, "size" : pageSize, "page" : currentPage-1})}).$promise.then(function(data) {
											$scope.allcampaigns = data;
											$scope.columnASC = true;
											$scope.columnDESC = false;
											$scope.loading = false;
												});
									 } else if(sort.direction == false) {
										$localStorage.direction = sort.direction;
											 CampaignService.query({searchstring :angular.toJson({"clause": "ByCampaignManager", "direction": "DESC", "sort" : column, "status" : selectedStatus, "name" : selectedName, "size" : pageSize, "page" : currentPage-1})}).$promise.then(function(data) {
											$scope.allcampaigns = data;
											$scope.columnDESC = true;
											$scope.columnASC = false;
											$scope.loading = false;
												});
									 }
								} else {
										sort.column = column;
										sort.direction = true;
										$localStorage.direction = sort.direction;
										if(sort.direction == false) {
												CampaignService.query({searchstring :angular.toJson({"clause": "ByCampaignManager", "direction":"DESC" , "sort" : column, "status" : selectedStatus, "name" : selectedName, "size" : pageSize, "page" : currentPage-1})}).$promise.then(function(data) {
											$scope.allcampaigns = data;
											$scope.columnDESC = true;
											$scope.columnASC = false;
											$scope.loading = false;  
												});
										}
										else if(sort.direction == true) {
										 CampaignService.query({searchstring :angular.toJson({"clause": "ByCampaignManager", "direction": "ASC" , "sort" : column, "status" : selectedStatus, "name" : selectedName, "size" : pageSize, "page" : currentPage-1})}).$promise.then(function(data) {
										$scope.allcampaigns = data;
										$scope.columnASC = true;
										$scope.columnDESC = false;
										$scope.loading = false;
											});
										}
								}
					};
		 
		  if (($localStorage.selectedStatus != undefined) || ($localStorage.currentPage != undefined) || ($localStorage.pageSize != undefined) ) {
				$scope.selectedStatus = $localStorage.selectedStatus;
				$scope.currentPage = $localStorage.currentPage;
				$scope.pageSize = $localStorage.pageSize;
			  $scope.pageChanged($localStorage.selectedStatus, undefined, $localStorage.currentPage, $localStorage.pageSize);
				$scope.changeSorting($localStorage.column, $localStorage.selectedStatus, undefined, $localStorage.pageSize, $localStorage.currentPage);
			} else {
			  $scope.selectedStatus = 'RUNNING';
			  $scope.pageChanged('RUNNING', undefined, 1, 10);  
		  }

		$scope.selectedColumn = function(column) {
				return column == $scope.sort.column && 'sort-' + $scope.sort.direction;
		};
	    
	//End of Sort function
		  $scope.retriveCampaign = function(id) {
			  $state.go('campaigndetails', { 'campaignId' : id });
		  };
          $scope.editCampaign = function(id) {
        	  $state.go('editcampaign', { 'campaignId' : id });
		  };
		  
		  $scope.cloneCampaign = function(id) {
			$location.path('newcampaign').search({byId: id});
		 }
		  
		  
		 $scope.deleteCampaign = function(id) {
				var modalInstance = $modal.open({
				      templateUrl: 'deleteCampaignModal.html',
				      controller: ModalInstanceCtrl,
				      resolve: {
				            campaignId: function(){
				                return id;
				            }
				           
				        }
				});
			
			};
		var ModalInstanceCtrl = function ($scope, $state, $modalInstance, campaignId) {
				   
			    $scope.ok = function () {
			    	 CampaignService.remove({
						  id: campaignId
		  			 }).$promise.then(function(data) {
		  				 $modalInstance.dismiss('cancel');
		  				 $state.go('listcampaigns', {}, { reload: true });
		  			 });
				};

				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
		};
		
		
		 $scope.save = function(command, campObj) {
	 			$scope.commanddata	={
	                     "campaignCommand":command
	                 };
	 			if(command =='Run'  && campObj.endDate < new Date()) {
		       		 var modalInstance = $modal.open({
		  			      templateUrl: 'runModal.html',
		  			      controller: ModalInstanceListCtrl, 
		  			      resolve: {
		  			        campid: function () {
		  			          return campObj.id;
		  			        }
		  			      }
		       		 }); 
	 			} else {
	 				$scope.loading = true;
	 				CampaignCommandService.query({
	 	                 id: campObj.id
	 	             }, $scope.commanddata).$promise.then(function(data) {
	 	            	$scope.loading = false;
	 	            	 switch (command) {
	 	            	 case 'Plan': 
	 	            		 campObj.status = "PLANNING";
	 						 NotificationService.log('success','Campaign moved to Planning state');
	 	         			 break;
	 	            	 case 'Run': 
	 	            		 $scope.pausevalue=1;
	 	            		 $scope.runvalue=0;
	 	            		campObj.status = "RUNNING";
	 						NotificationService.log('success','Campaign moved to Running state');
	 	         			break;
	 	         		case 'Pause': 
	 	         			$scope.resumevalue=1;
	 	         			$scope.pausevalue=0;
	 	         			campObj.status = "PAUSED";
	 	         			NotificationService.log('success','Campaign Paused successfully');
	 	         			break;
	 	         		case 'Stop':
	 	         			$scope.stopvalue=1;
	 	         			campObj.status = "STOPPED";
	 						NotificationService.log('success','Campaign Stopped successfully');
	 	         			break;
	 	         		case 'Resume':
	 	         			$scope.resumevalue=0;
	 	         			$scope.pausevalue=1;
	 	         			campObj.status = "RESUME";
	 						NotificationService.log('success','Campaign Resumed successfully');
	 	         			break;
	 	         		default:
	 						NotificationService.log('success','Campaign is in Unknown state');
	 	            	}	 	            	
	 	             });
	 				$scope.loading = false;
	 				$timeout(function () { 
	 				   $state.go('listcampaigns', {}, { reload: true });
	 				}, 3000);
	 			}
	 			
	     	};
	    
		
		var ModalInstanceListCtrl = function ($scope, $state, $modalInstance, campid) {
			$scope.ok = function (id) {
				$modalInstance.close($state.go('editcampaign', { 'campaignId' : campid }));
			};

			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		};
		
		  
	}]).filter('capitalize', function() {
	    return function(input, all) {
	        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
	      }
	    });
});
