define(['angular', 'campaign/campaign-service'], function(angular) {
    angular.module('CampaignSummaryController', ['campaign-service','ngCookies'])
    .controller('CampaignSummaryController', [
        '$scope', 
        '$cookieStore',
        '$stateParams',
        'CampaignService',
		'GlobalEditCampaign',
        '$modal',
        '$state',
        'GlobalCampaign',
        'GetCampaignService',
		 'GlobalCampaignTypes',

        function ($scope,$cookieStore,$stateParams,CampaignService,GlobalEditCampaign, $modal, $state, GlobalCampaign,GetCampaignService, GlobalCampaignTypes) {



          $scope.campaignTypes = GlobalCampaignTypes.getGlobalCampaignTypes();
		  $scope.globalEditCampaign = GlobalEditCampaign.getGlobalEditCampaign();;
    	  $scope.getType = function(type){
    		  for(i=0; i<$scope.campaignTypes.length;i++){
    			  if($scope.campaignTypes[i].value == type)
    			  return $scope.campaignTypes[i].key;
    		  }
    	  }
          
          
            CampaignService.get({
                id: $stateParams.campaignId
            }).$promise.then(function(data){
                $scope.campaign=data;
            });
					
            $scope.isExist = function(restriction) {
           	 if(restriction == null || restriction == undefined || restriction == '')
           		 return false;
           	 else
           		 return true;
    		
    		};		

            $scope.save = function(path) {
				
				if(path == "" || path == undefined || path == null)
            	{
            		path = 'listcampaigns';
            	}
                $state.go(path, {
                    'campaignId' : $stateParams.campaignId
                });

            };
            
            $scope.cancelCampaign=function(){
                $state.go('listcampaigns');
            }
		

        }]);
});
