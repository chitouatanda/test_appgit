define(['angular', 'angular-resource'], function(angular) {
	angular.module('user-service', ['ngResource'])
		.factory("UserService",function($resource) {
			return $resource("/spr/rest/user",{},{
				query: {
		            method: 'GET',
		            params: {}
		                }
			});
		}).factory("UserServiceById",function($resource) {
			return $resource("/spr/user/:username",{},{
				query: {
		            method: 'GET',
		            params: {}
		                }
			});
		}).factory("UserPasswordService",function($resource) {
			return $resource("/spr/rest/user/:id/attribute/password",null,{
				update: {
					method : 'POST',
					headers : {
	                    'Content-Type': 'application/json'
	                }
					
				}
			});
		}).factory("ForgotPasswordLinkService",function($resource) {
			return $resource("/spr/rest/user/resetpassword?username=:username",{},{
				query: {
					method : 'GET',
					headers : {
	                    'Content-Type': 'application/json'
	                }
					
				}
			});
		}).factory("ChangeForgotPasswordService",function($resource) {
			return $resource("/spr/rest/user/changeforgotpassword",{},{
				update: {
					method : 'PUT',
					headers : {
	                    'Content-Type': 'application/json'
	                }
					
				}
			});
		}).factory("AgentNetworkStatusUpdateService", function ($resource) {
			return $resource("", {}, {
				getAgent: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/agent/getnetworkstatus/:username"
				},
				postAgent: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/agent/postnetworkstatus/:username/:networkstatus"
				}
			})
		}).factory("PasswordService", function ($resource) {
			return $resource("", {}, {
				getUser: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/getuserfromdb/:username"
				}
			});
		});
});