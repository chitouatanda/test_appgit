define(['angular', 'angular-ui-router', 'notification-service'], function(angular) {
	angular.module('xtaas.notification', ['ui.router', 'notification-service'])
		.config(['$httpProvider', function($httpProvider) {
			  $httpProvider.interceptors.push(function($q, $rootScope, $injector) {
		     		return {
		     			'responseError': function(rejection)	 {
		     				var status = rejection.status;
		     				if (status == 400) {
		     					$injector.invoke(function(NotificationService) {
		     						NotificationService.log('error',rejection.data.fieldErrors[0].message);
		     					});
		     				} 
		     				if (status == 409) {
		     					$injector.invoke(function(NotificationService) {
		     						NotificationService.log('error',rejection.data.Error);
		     					});
		     				} 
		     				return $q.reject(rejection);
		     			}
		     		};
		     	 });
		}])
});