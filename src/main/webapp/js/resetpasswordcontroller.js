define(['angular','user-service', 'login-service', 'notification-service'], function(angular) {
	angular.module('ResetPasswordController', ['user-service', 'login-service', 'notification-service'])
		.controller('ResetPasswordController', [
            '$scope', 
            'ChangeForgotPasswordService',
            'NotificationService',
			'$location',
			'user',
			'PasswordService',
             function($scope, ChangeForgotPasswordService, NotificationService, $location,user,PasswordService) {
            	$scope.invalid = false;
				$scope.loading = false;
				$scope.loggedInUser = user;
				//$scope.user = user;
				PasswordService.getUser({username:$scope.loggedInUser.id}).$promise.then(function(data){
					if(data != null && data != undefined){
						if (data.passwordPolicy) {
							if(!data.activateUser){
								NotificationService.log('success', "You must change your password before logging on the first time. Please update your password below.");
							}
						}
					}
				});
            	$scope.resetPassword = function(newPassword) {
            		if($scope.confirmPasswordCheck) {//check confirm password match or not
            			$scope.confirmPasswordValid = true;
            		}
            		if ($scope.forgotpasswordform.$valid) {//check form is valid or not
            			$scope.loading = true;
    					ChangeForgotPasswordService.update(newPassword).$promise.then(function(data){ //call change password service
    						$scope.loading = false;
    						NotificationService.log('success', "Password changed successfully");
    						console.log("Password changed successfully");
    						$location.path("/spr/landingpage");
						}, function(error){
							$scope.loading = false;
						});
    				}else {
    					$scope.loading = false;
    					if (!$scope.confirmPasswordValid) {
    						NotificationService.log('error','Please enter new Password & conform password');
    					}
    				}
    			};
            }]
		).directive('pwCheck', [function () {//directive for check two password match or not
		    return {
		        require: 'ngModel',
		        link: function ($scope, elem, attrs, ctrl) {
		          var firstPassword = '#' + attrs.pwCheck;
		          elem.add(firstPassword).on('keyup', function () {
		        	  $scope.$apply(function () {
		              var v = elem.val()===$(firstPassword).val();
		              ctrl.$setValidity('pwmatch', v);
		              $scope.confirmPasswordCheck = true;
		            });
		          });
		        }
		      }
		    }]).directive('focusMe', function ($timeout, $parse) {//directive for autofocus an input field
	      	  return {
	    		  link: function (scope, element, attrs) {
	    		  var model = $parse(attrs.focusMe);
	    		  scope.$watch(model, function (value) { 
	    			  if (value === true) {
	    				  $timeout(function () {
	    					  scope.$apply(model.assign(scope, false));
	    					  element[0].focus();
	    				  }, 30);
	    			  }
	    		  });
	    		  }
	      	  };
		    });
});