define(['angular', 'angular-ui-router'], function(angular) {
	angular.module('xtaas.security', ['ui.router'])
		.config(['$httpProvider', function($httpProvider) {
		    $httpProvider.defaults.withCredentials = true;
		     $httpProvider.interceptors.push(function($q, $rootScope, $injector, loginSuccessUrl) {
	     		return {
	     			'response': function(response)	 {
	     				var redirectLocation = response.headers('Location');
	     				if (angular.isDefined(redirectLocation) && redirectLocation != null) {
	     					$injector.invoke(function($window) {
	     						$window.location.href = redirectLocation;
	     					});
	     				}
 	     				return response;
	     			},
	     			'responseError': function(rejection)	 {
	     				var status = rejection.status;
	     				if (status == 401 ) {
	     					$rootScope.isUserLoggedIn = false;
	     					$injector.invoke(function($location, NotificationService, $window) {
	     						if ($location.path() != "/login") {
	     							loginSuccessUrl.url = $location.path();
	     							NotificationService.log('error','You are unauthorised to access '+loginSuccessUrl.url+'.');
	     						}
	     						if(rejection.data == "CONCURRENT_SESSION"){
	     							$location.path('/login').search({"err": "CONCURRENT_SESSION"});
	     						} else {
	     							$location.path("/login")	
	     						}	     						
	     					});
	     				} else if (status == 0) {
							$rootScope.error = {};
							$rootScope.error.status = status;
						} else {
	     					$rootScope.error = method + " on " + url + " failed with status " + status;
	     	        	}
	     				return $q.reject(rejection);
	     			}
	     		};
	     	 });
		}])
		.value('loginSuccessUrl', {});

});
