define(['angular', 'angular-ui-router', 'oc-lazyload', 'angular-ui-bootstrap', 'angular-cookie', 'xtaas-config', 'xtaas-security', 'xtaas-notification', 'xtaas-interceptors', 'angular-pusher'], function(angular) {
	var xtaas = angular.module('xtaas', ['ui.router', 'oc.lazyLoad', 'ui.bootstrap', 'ngCookies', 'xtaas.config', 'xtaas.security', 'xtaas.notification', 'xtaas.interceptors', 'doowb.angular-pusher']);
	
	xtaas.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
   	 };
	}]);

	xtaas.directive('noSpacialCharAllowed', function() {
		function link(scope, elem, attrs, ngModel) {
					ngModel.$parsers.push(function(inputValue) {
						var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
						if (inputValue == null || inputValue == undefined) {
								return;
						}  
						if (inputValue.match(reg)) {
								return inputValue;
						}
						var transformedValue = ngModel.$modelValue;
						ngModel.$setViewValue(transformedValue);
						ngModel.$render();
						return transformedValue;
					});
		 }
			return {
				restrict: 'A',
				require: 'ngModel',
				link: link
			};      
	 });
	/*	for character and space */
	 xtaas.directive('onlyLettersInput', onlyLettersInput);
  
	 function onlyLettersInput() {
		 return {
		   require: 'ngModel',
		   link: function(scope, element, attr, ngModelCtrl) {
			 function fromUser(text) {
			   var transformedInput = text.replace(/[^a-zA-Z\s]/g, '');
			   //console.log(transformedInput);
			   if (transformedInput !== text) {
				 ngModelCtrl.$setViewValue(transformedInput);
				 ngModelCtrl.$render();
			   }
			   return transformedInput;
			 }
			 ngModelCtrl.$parsers.push(fromUser);
		   }
		 };
	   };



	 //a directive to 'enter key press' in elements with the "ng-enter" attribute
    xtaas.directive('ngEnter', function () { 

        return function (scope, element, attrs) {

            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
	
	
	xtaas.filter('tel', function () {
	    return function (tel) {
	        if (!tel) { return ''; }

	        var value = tel.toString().trim().replace(/^\+/, '');
	        value = value.replace("-", '');

	        if (value.match(/[^0-9]/)) {
	            return tel;
	        }

	        var country, city, number;
	        var len = value.length
	        if(len > 10){
	        	var pos = len-10;
	        	var extra_char = "+" + value.slice(0,pos)+"-";
	        }else{
	        	var extra_char = "";
	        }
	        var phone = extra_char+value.slice(-10, -7)+"-"+value.slice(-7, -4)+"-"+value.slice(-4);
	        return phone.trim();
	    };
	});
	
	xtaas.directive('whenScrolled', function() {
       return function(scope, elm, attr) {
        var raw = elm[0];
        
        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});

 /*xtaas.directive('numberMask', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).numeric();
        }
    }
});*/
 xtaas.directive("dynamicPopover", function() {
	return {
		restrict: 'A',
		template: '<span>This is a link for popover</span>',
		link: function(scope, el, attrs) {
			scope.label = attrs.popoverLabel;
			angular.element(el).popover({
                trigger: 'click',
                html: true,
                content: attrs.popoverHtml,
                placement: attrs.popoverPlacement
            });
		}
	}
});
 
 /*Directive for copy to clipboard added on 5th sept 15*/
 xtaas.directive('clipboard', ['$document', function ($document) {
     return {
         restrict: 'A',
         scope: {
             onCopied: '&',
             onError: '&',
             text: '='
         },
         link: function (scope, element) {
             function createNode(text) {
                 var node = $document[0].createElement('span');
                 node.style.position = 'absolute';
                 node.style.left = '-10000px';
                 node.textContent = text;
                 return node;
             }

             function copyNode(node) {
                 // Set inline style to override css styles
                 $document[0].body.style.webkitUserSelect = 'initial';

                 var selection = $document[0].getSelection();
                 selection.removeAllRanges();

                 var range = $document[0].createRange();
                 range.selectNodeContents(node);
                 selection.addRange(range);

                 $document[0].execCommand('copy');
                 selection.removeAllRanges();

                 // Reset inline style
                 $document[0].body.style.webkitUserSelect = '';
             }

             function copyText(text) {
                 var node = createNode(text);
                 $document[0].body.appendChild(node);
                 copyNode(node);
                 $document[0].body.removeChild(node);
             }

             element.on('click', function (event) {
                 try {
                     copyText(scope.text);
                     if (scope.onCopied) {
                         scope.onCopied();
                     }
                 } catch (err) {
                     if (scope.onError) {
                         scope.onError({err: err});
                     }
                 }
             });
         }
     };
 }]);
 
 /*xtaas.directive("slider", function() {
    return {
        restrict: 'A',
        scope: {
            config: "=config",
            minimumTotalVolume: "=model"
        },
        link: function(scope, elem, attrs) {
            var setModel = function(value) {
                scope.model = value;   
            }
            
            $(elem).slider({
                range: false,
	            min: scope.config.min,
	            max: scope.config.max,
                step: scope.config.step,
                slide: function(event, ui) { 
                    scope.$apply(function() {
                        scope.minimumTotalVolume = ui.value;
                    });
	            }
	        });
    	}
    }
});*/
/* xtaas.directive('mypopover', function ($compile,$templateCache) {

var getTemplate = function (contentType) {
    var template = '';
    switch (contentType) {
        case 'user':
            template = $templateCache.get("templateId.html");
            break;
    }
    return template;
}
return {
    restrict: "A",
	scope: {
        hide: '&hide' // did not understand what is this
    },
    link: function (scope, element, attrs) {
        var popOverContent;
      
        popOverContent = getTemplate("user");     
        
        popOverContent = $compile("<div>" + popOverContent+"</div>")(scope);
        
        var options = {
            content: popOverContent,
            placement: "right",
            html: true,
            date: scope.date
        };
        $(element).popover(options);
    }
};
});*/

 xtaas.run(['$rootScope','$interval','$modal','$location', function($rootScope, $interval, $modal, $location) {
		
	 //watching the timerstart value to be change.
	 $rootScope.$watch('timerStart', function(newTime, oldTime) {
		if (newTime != undefined) {
			$interval.cancel($rootScope.sessionTimeOutPromise);
			var count = 0;
			$rootScope.sessionTimeOutPromise = $interval(function(){//starting the timer by by 1 min interval
				var currentTime = new Date().getTime();
				$rootScope.counter = $rootScope.counter -20;
				//checking 25 min has passed or not
				if (currentTime - newTime >= 1500000 && currentTime - newTime < 1800000) {
					count++;
					if ($rootScope.isUserLoggedIn) {
						if (count == 1) {//condition for showing the pop-up first time with full width progress bar.
							console.log("session is about to timeout");
							$rootScope.counter = 100;//for displaying the progress bar in view
							$rootScope.$broadcast("SessionToBeTimeOut");//broadcast the request to a popup to display warning message to display session will be end in 5 min.
						}
						if ($rootScope.modalDismissed) {//condition for showing the pop-up when user manually dismissed the pop-up.
							console.log("session is about to timeout");
							$rootScope.$broadcast("SessionToBeTimeOut");//broadcast the request to a popup to display warning message to display session will be end in 5 min.
							$rootScope.modalDismissed = false;
						}
					} else {
						$interval.cancel($rootScope.sessionTimeOutPromise);
					}
				}
				//checking 30 min has passed or not
				if (currentTime - newTime >= 1800000) {
					if ($rootScope.isUserLoggedIn) {
						console.log("session timeout");
						$interval.cancel($rootScope.sessionTimeOutPromise);
						$rootScope.$broadcast("SessionTimeOut");//broadcast the request to a popup to display session timeout message to display session is end.
					}
				}
			},60000);
		}
	});

	$rootScope.$on("SessionToBeTimeOut",function () {
		$rootScope.closeModals();
		$rootScope.warning = $modal.open({
			templateUrl: "warning-dialog.html",
			controller: SessionToBeTimeOutModalInstanceCtrl
	        });
		 $rootScope.warning.result.then(function (selectedItem) {
			    
		 }, function () {
		    	$rootScope.modalDismissed = true;//True when user dismissed the pop-up manually
		    
		});
	});

	$rootScope.$on("SessionTimeOut",function () {
		$rootScope.closeModals();
		$rootScope.timedout = $modal.open({
			templateUrl: "timedout-dialog.html",
			controller: TimedoutModalInstanceCtrl
	    });
		$interval.cancel($rootScope.sessionTimeOutPromise);
	});
	$rootScope.closeModals = function () {
        if ($rootScope.warning) {
        	$rootScope.warning.close();
        	$rootScope.warning = null;
        }

        if ($rootScope.timedout) {
        	$rootScope.timedout.close();
        	$rootScope.timedout = null;
        }
      }
	
	var SessionToBeTimeOutModalInstanceCtrl = function ($scope, $modalInstance, $resource, $http) {
		 $scope.ok = function () {
	    	$http.get("/spr/landingpage");
	    	$modalInstance.dismiss('cancel');
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};
	var TimedoutModalInstanceCtrl = function ($scope, $modalInstance, $http) {
		 $http.get("/j_spring_security_logout");
		 $scope.ok = function () {
	    	$location.path('/login');
	    	$modalInstance.dismiss('cancel');
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};
	
	
	}])
 
xtaas.directive('infiniteScroll', [
  '$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
    return {
      link: function(scope, elem, attrs) {
        var checkWhenEnabled, handler, scrollDistance, scrollEnabled;
        $window = angular.element($window);
        scrollDistance = 0;
        if (attrs.infiniteScrollDistance != null) {
          scope.$watch(attrs.infiniteScrollDistance, function(value) {
            return scrollDistance = parseInt(value, 10);
          });
        }
        scrollEnabled = true;
        checkWhenEnabled = false;
        if (attrs.infiniteScrollDisabled != null) {
          scope.$watch(attrs.infiniteScrollDisabled, function(value) {
            scrollEnabled = !value;
            if (scrollEnabled && checkWhenEnabled) {
              checkWhenEnabled = false;
              return handler();
            }
          });
        }
        handler = function() {
          var elementBottom, remaining, shouldScroll, windowBottom;
          windowBottom = $window.height() + $window.scrollTop();
          elementBottom = elem.offset().top + elem.height();
          remaining = elementBottom - windowBottom;
          shouldScroll = remaining <= $window.height() * scrollDistance;
          if (shouldScroll && scrollEnabled) {
            if ($rootScope.$$phase) {
              return scope.$eval(attrs.infiniteScroll);
            } else {
              return scope.$apply(attrs.infiniteScroll);
            }
          } else if (shouldScroll) {
            return checkWhenEnabled = true;
          }
        };
        $window.on('scroll', handler);
        scope.$on('$destroy', function() {
          return $window.off('scroll', handler);
        });
        return $timeout((function() {
          if (attrs.infiniteScrollImmediateCheck) {
            if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
              return handler();
            }
          } else {
            return handler();
          }
        }), 0);
      }
    };
  }
]);
	
	xtaas.config([
	              '$stateProvider',
	              '$locationProvider',
	              '$urlRouterProvider',
	              '$ocLazyLoadProvider', 
	              '$httpProvider',
	              'PusherServiceProvider',
	              'XTAAS_CONFIG',
	              function($stateProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider, PusherServiceProvider, XTAAS_CONFIG) {
	            	  PusherServiceProvider
	            	  	.setToken(XTAAS_CONFIG.pusherToken)
						.setOptions({cluster : XTAAS_CONFIG.pusherCluster});
	            	  $locationProvider.hashPrefix('!');
	            	  $urlRouterProvider.otherwise("/launch");
	            	  // Initially calls landing page API from launch controller to check user is remembered to server, otherwise Go to login
	            	  $ocLazyLoadProvider.config({
	            		  loadedModules: ['xtaas'],
	            		  asyncLoader: require
	            	  });
	            	  $stateProvider.state('launch', {
	            		  url:'/launch',
	                      views: {
							"main": {
	            				  controller: 'LaunchController',
	            				  resolve: {
	            					  module: function($ocLazyLoad, $q) {
		    	                      		return $ocLazyLoad.load({
		    	                  				name: 'LaunchController',
		    	                  				files: ['/js/launchcontroller.js']
		    	                      		});
		    	                      	}
	            				  }
	            			  }
	          			}
					  })
					//   .state('oldnewcampaign', {
	                //       url:'/oldnewcampaign',
	                //       views: {	
	          		// 		"header": {
	          		// 			templateUrl: '/partials/header.html',
	          		// 			controller:"HeaderController",
	          		// 			resolve:{
	          		// 				module:	function($ocLazyLoad,$q){
	          		// 					return $ocLazyLoad.load({
	          		// 						name: 'HeaderController',
	          		// 						files: ['/js/headercontroller.js']
	          		// 					});
	          		// 				},
	          		// 				user: function(module, UserService){
	          		// 					return UserService.query().$promise;
	          		// 				},
	          		// 				campaignTypes: function(module, CampaignTypeService) {
	    	        //               		return CampaignTypeService.query().$promise;
	          		// 				},
	           		// 			}	
	          		// 		},
	          		// 		"main": {
	          		// 			templateUrl: '/partials/campaign/newcampaign.html',
	          		// 			controller: "NewCampaignController",
	          		// 			resolve: {
	    	        //               	module: function($ocLazyLoad, $q) {
	    	        //               		return $ocLazyLoad.load({
	    	        //           				name: 'NewCampaignController',
	    	        //           				files: ['/js/campaign/newcampaigncontroller.js']
	    	        //               		});
	    	        //               	},
	    	        //               	restrictions: function(module, MasterDataService) {
	    	        //               		return MasterDataService.query({id: "Restrictions"}).$promise;
	    	        //               	},
	    	        //                	attributeMappings: function(module, CRMMappingDataService) {
	    	        //               		return CRMMappingDataService.query().$promise;
	    	        //               	},
	    	        //               	domains: function(module, DomainService) {
	    	        //               		return DomainService.query().$promise;
	    	        //               	}
	          		// 			}//end of resolve
	          		// 		}//end of main
	          		// 	}//end of views
					//   })
					  .state('newcampaign', {
	                      url:'/newcampaign',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/newcampaigndesign.html',
	          					controller: "NewCampaignDesignController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'NewCampaignDesignController',
	    	                  				files: ['/js/campaign/newcampaigndesigncontroller.js']
	    	                      		});
	    	                      	},
	    	                      	restrictions: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Restrictions"}).$promise;
	    	                      	},
	    	                       	attributeMappings: function(module, CRMMappingDataService) {
	    	                      		return CRMMappingDataService.query().$promise;
	    	                      	},
	    	                      	domains: function(module, DomainService) {
	    	                      		return DomainService.query().$promise;
	    	                      	},
	    	                      	arealist: function(module,AreaService) {
	    	                      		return AreaService.query().$promise;
	    	                      	},
									countrylist: function(module,CountryService) {
	    	                      		return CountryService.query().$promise;
	    	                      	},
	    	                      	statelist: function(module,StateService) {
	    	                      		return StateService.query().$promise;
	    	                      	},
									ratingilist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Rating"}).$promise;
	    	                      	},
									timezonelist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Timezone"}).$promise;
	    	                      	},
									langitemlist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Language"}).$promise;
									},
									user: function(module, UserService) {
										return UserService.query().$promise;
									},
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('agentsplash', {
	                      url:'/agentsplash',
	                      views: {	
	                    	  "header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/agent/splash.html',
	          					controller: "AgentSplashController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'AgentSplashController',
	    	                  				files: ['/js/agent/agent-splash-controller.js']
	    	                      		});
	    	                      	},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						agentDetails: function(module, AgentDetailService){
	          							return AgentDetailService.query().$promise;
	          						},
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('mobileagentsplash', {
						url:'/mobileagentsplash',
						views: {	
							"header": {
								templateUrl: '/partials/header.html',
								controller:"HeaderController",
								resolve:{
									module:	function($ocLazyLoad,$q){
										return $ocLazyLoad.load({
											name: 'HeaderController',
											files: ['/js/headercontroller.js']
										});
									},
									user: function(module, UserService){
										return UserService.query().$promise;
									},
									campaignTypes: function(module, CampaignTypeService) {
										return CampaignTypeService.query().$promise;
									},
								 }	
							},
							"main": {
								templateUrl: '/partials/agent/mobile-agent-splash.html',
								controller: "AgentSplashController",
								resolve: {
									module: function($ocLazyLoad, $q) {
										return $ocLazyLoad.load({
											name: 'AgentSplashController',
											files: ['/js/agent/agent-splash-controller.js']
										});
									},
									user: function(module, UserService){
										return UserService.query().$promise;
									},
									agentDetails: function(module, AgentDetailService){
										return AgentDetailService.query().$promise;
									},
								}//end of resolve
							}//end of main
						}//end of views
					}).state('agent', {
	                      url:'/agent',
	                      views: {	
//	                    	  "header": {
//		          					templateUrl: '/partials/agent/custom-popover-test-header.html',
//		          					controller:"CustomPopoverTestController",
//		          					resolve:{
//		          						module:	function($ocLazyLoad,$q){
//		          							return $ocLazyLoad.load({
//		          								name: 'custom-popover-test-controller',
//		          								files: ['/js/agent/custom-popover-test-controller.js']
//		          							});
//		          						}
//		           					}	
//		          				}
	          				"header": {
	          					templateUrl: '/partials/agent/agent-header.html',
	          					controller:"AgentHeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'agent-header-controller',
	          								files: ['/js/agent/agent-header-controller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						}
	           					}	
	          				}
	            	  		,
	          				"main": {
	          					templateUrl: '/partials/agent/index.html',
	          					controller: "AgentController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'AgentController',
	    	                  				files: ['/js/agent/agentcontroller.js', '/js/logincontroller.js']
	    	                      		});
	    	                      	},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						agentDetails: function(module, AgentDetailService){
	          							return AgentDetailService.query().$promise;
	          						},
									attributeMappings: function(module, CRMMappingDataService) {
										return CRMMappingDataService.getSystemMapping().$promise;
									},
									applicationProperties: function(module, ApplicationPropertyService) {
										return ApplicationPropertyService.query().$promise;
									}
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('mobileagent', {
						url:'/mobileagent',
						views: {
							"header": {
								templateUrl: '/partials/agent/agent-header.html',
								controller:"AgentHeaderController",
								resolve:{
									module:	function($ocLazyLoad,$q){
										return $ocLazyLoad.load({
											name: 'agent-header-controller',
											files: ['/js/agent/agent-header-controller.js']
										});
									},
									user: function(module, UserService){
										return UserService.query().$promise;
									}
								 }	
							}
							,
							"main": {
								templateUrl: '/partials/agent/mobile-agent.html',
								controller: "AgentController",
								resolve: {
									module: function($ocLazyLoad, $q) {
										return $ocLazyLoad.load({
											name: 'AgentController',
											files: ['/js/agent/agentcontroller.js']
										});
									},
									user: function(module, UserService){
										return UserService.query().$promise;
									},
									agentDetails: function(module, AgentDetailService){
										return AgentDetailService.query().$promise;
									},
									attributeMappings: function(module, CRMMappingDataService) {
										return CRMMappingDataService.getSystemMapping().$promise;
									},
									applicationProperties: function(module, ApplicationPropertyService) {
										return ApplicationPropertyService.query().$promise;
								    }
								}
							}
						}
					})
					// .state('oldeditcampaign', {
	                //       url:'/oldeditcampaign/{campaignId}',
	                //       views: {	
	          		// 		"header": {
	          		// 			templateUrl: '/partials/header.html',
	          		// 			controller:"HeaderController",
	          		// 			resolve:{
	          		// 				module:	function($ocLazyLoad,$q){
	          		// 					return $ocLazyLoad.load({
	          		// 						name: 'HeaderController',
	          		// 						files: ['/js/headercontroller.js']
	          		// 					});
	          		// 				},
	          		// 				user: function(module, UserService){
	          		// 					return UserService.query().$promise;
	          		// 				},
	          		// 				campaignTypes: function(module, CampaignTypeService) {
	    	        //               		return CampaignTypeService.query().$promise;
	          		// 				},
	           		// 			}	
	          		// 		},
	          		// 		"main": {
	          		// 			templateUrl: '/partials/campaign/newcampaign.html',
	          		// 			controller: "NewCampaignController",
	          		// 			resolve: {
	    	        //               	module: function($ocLazyLoad, $q) {
	    	        //               		return $ocLazyLoad.load({
	    	        //           				name: 'NewCampaignController',
	    	        //           				files: ['/js/campaign/newcampaigncontroller.js']
	    	        //               		});
	    	        //               	},
	    	        //               	campaignTypes: function(module, CampaignTypeService) {
		    	    //                   		return CampaignTypeService.query().$promise;
		    	    //                 },
	    	        //               	restrictions: function(module, MasterDataService) {
	    	        //               		return MasterDataService.query({id: "Restrictions"}).$promise;
	    	        //               	},
	    	        //                 attributeMappings: function(module, CRMMappingDataService) {
	    	        //               		return CRMMappingDataService.query().$promise;
	    	        //               	},
	    	        //               	domains: function(module, DomainService) {
	    	        //               		return DomainService.query().$promise;
	    	        //               	}
	          		// 			}//end of resolve
	          		// 		}//end of main
	          		// 	}//end of views
					//   })
					  .state('editcampaign', {
						  url:'/editcampaign/{campaignId}',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/newcampaigndesign.html',
	          					controller: "NewCampaignDesignController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'NewCampaignDesignController',
	    	                  				files: ['/js/campaign/newcampaigndesigncontroller.js']
	    	                      		});
	    	                      	},
	    	                      	campaignTypes: function(module, CampaignTypeService) {
		    	                      		return CampaignTypeService.query().$promise;
		    	                    },
	    	                      	restrictions: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Restrictions"}).$promise;
	    	                      	},
	    	                        attributeMappings: function(module, CRMMappingDataService, CRMSelectionService, $stateParams) {
										  // return CRMMappingDataService.query().$promise;
										  // 07/09/2018 added to fetch question by campaignId from crmmapping collection
	    	                      		  return CRMSelectionService.getByCampaignId({ campaignId: $stateParams.campaignId }).$promise;
	    	                      	},
	    	                      	domains: function(module, DomainService) {
	    	                      		return DomainService.query().$promise;
	    	                      	},
	    	                      	arealist: function(module,AreaService) {
	    	                      		return AreaService.query().$promise;
	    	                      	},
									countrylist: function(module,CountryService) {
	    	                      		return CountryService.query().$promise;
	    	                      	},
	    	                      	statelist: function(module,StateService) {
	    	                      		return StateService.query().$promise;
	    	                      	},
	    	                      	ratingilist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Rating"}).$promise;
	    	                      	},
									timezonelist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Timezone"}).$promise;
	    	                      	},
									langitemlist: function(module, MasterDataService) {
	    	                      		return MasterDataService.query({id: "Language"}).$promise;
									},
									user: function(module, UserService) {
										return UserService.query().$promise;
									},
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('listcampaigns', {
	                      url:'/listcampaigns',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/listcampaigns.html',
	          					controller: "ListCampaignController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'ListCampaignController',
	    	                  				files: ['/js/campaign/listcampaigncontroller.js']
	    	                      		});
																},
																user: function(module, UserService){
																	return UserService.query().$promise;
																},
	    	                      	campaignlist: function(module,CampaignService){
	    	                      		return CampaignService.query({searchstring: angular.toJson({"clause": "ByCampaignManager", "direction" : "ASC", "size": "10", "sort" : "name", "status": "RUNNING"})}).$promise;
	    	                      	},
	    	                      	campaignstatus: function(module,CampaignStatusService){
	    	                      		return CampaignStatusService.query().$promise;
	    	                      	}
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  })
	            	  
	            	  /* START DASHBOARD STATE
	            	   * DATE : 30/05/2017
	            	   * REASON : new tab to show TABLEAU reports*/
	            	  .state('dashboard', {
	                      url:'/dashboard',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/dashboard.html',
	          					controller: "DashboardController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'DashboardController',
	    	                  				files: ['/js/campaign/dashboardcontroller.js']
	    	                      		});
	    	                      	}	                      	
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  })
	            	  /* END DASHBOARD STATE */
	            	  
	            	  .state('viewcampaign', {
	                      url:'/viewcampaign/{campaignId}',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/viewcampaign.html',
	          					controller: "ViewCampaignController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'ViewCampaignController',
	    	                  				files: ['/js/campaign/viewcampaigncontroller.js']
	    	                      		});
	    	                      	}
	    	                      	    	                      	
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('campaigndetails', {
	                      url:'/campaigndetails/{campaignId}',
	                      views: {	
	          				"header": {
	          					templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	           					}	
	          				},
	          				"main": {
	          					templateUrl: '/partials/campaign/campaigndetails.html',
	          					controller: "CampaignDetailsController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'CampaignDetailsController',
	    	                  				files: ['/js/campaign/campaigndetailscontroller.js']
	    	                      		});
	    	                      	},	          				
	          						attributeMappings: function(module, CRMMappingDataService) {
	    	                      		return CRMMappingDataService.query().$promise;
									  },
									user: function(module, UserService){
										return UserService.query().$promise;
									}  	                      	
	          					}//end of resolve
	          				}//end of main
	          			}//end of views
	            	  }).state('campaignsummary', {
	                      url:'/campaignsummary/{campaignId}',
	                      views: {	
	          				"header": {templateUrl: 'partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/campaign/campaignsummary.html',
	          					controller: "CampaignSummaryController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'CampaignSummaryController',
	    	                  				files: ['/js/campaign/campaignsummarycontroller.js']
	    	                      		});
	    	                      	},

	          					}
	          				}
	          			}
	            	  }).state('supervisor', {
	                      url:'/supervisor',
	                      views: {	
	          				"header": {templateUrl: 'partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						},
	          						
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/supervisor/supervisor.html',
	          					controller: "NewSupervisorController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'NewSupervisorController',
	    	                  				files: ['/js/supervisor/newsupervisorcontroller.js']
	    	                      		});
	    	                      	},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						applicationProperties: function(module, ApplicationPropertyService) {
	          							return ApplicationPropertyService.query().$promise;
									},
									attributeMappings: function(module, CRMMappingDataService) {
										return CRMMappingDataService.getSystemMapping().$promise;
									}  
	          					}
	          				} 
	          			}
	            	  }).state('healthchecks', {
										url:'/healthchecks/{campaignId}',
										views: {	
										"header": {
														templateUrl: 'partials/header.html',
														controller:"HeaderController",
														resolve:{
															module:	function($ocLazyLoad,$q){
																return $ocLazyLoad.load({
																	name: 'HeaderController',
																	files: ['/js/headercontroller.js']
																});
															},
															user: function(module, UserService) {
																return UserService.query().$promise;
															},
															campaignTypes: function(module, CampaignTypeService) {
																				return CampaignTypeService.query().$promise;
															},
												}	
					          },
								    "main": {
													templateUrl: '/partials/supervisor/healthchecks.html',
													controller: "HealthChecksController",
													resolve: {
														module: function($ocLazyLoad, $q) {
																return $ocLazyLoad.load({
																name: 'HealthChecksController',
																files: ['/js/supervisor/healthcheckscontroller.js']
																});
														},
														user: function(module, UserService) {
																return UserService.query().$promise;
														}
												}
									  } 
									}
							}).state('qa', {
	                      url:'/qa',
	                      views: {	
	          				"header": {templateUrl: '/partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/qa/index.html',
	          					controller: "QaController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'QaController',
	    	                  				files: ['/js/qa/qacontroller.js']
	    	                      		});
	    	                      	},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						}
	    	                      
	    	                      
	          					}
	          				} 
	          			}
	            	  })
	            	  .state('secondaryqa', {
	                      url:'/secondaryqa',
	                      views: {	
	          				"header": {templateUrl: '/partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/secondaryqa/secondaryqa.html',
	          					controller: "SecondaryQaController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'SecondaryQaController',
	    	                  				files: ['/js/secondaryqa/secondaryqacontroller.js']
	    	                      		});
	    	                      	},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						}
	    	                      
	    	                      
	          					}
	          				} 
	          			}
	            	  })
	            	  .state('qadetail', {
	                      url:'/qadetail/{prospectCallId}',
	                      views: {	
	          				"header": {templateUrl: '/partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	    
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/qa/qadetail.html',
	          					controller: "QaDetailController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'QaDetailController',
	    	                  				files: ['/js/qa/qadetailcontroller.js']
	    	                      		});
	    	                      	},
	    	                    	qadetail: function(module, qADetailService,$stateParams) {
	    	                      		return qADetailService.query({"prospectCallId": $stateParams.prospectCallId}).$promise;
	          						},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						qaRejectReasons: function(module, qaRejectReasonService) {
	    	                      		return qaRejectReasonService.query().$promise;
									},
									attributeMappings: function(module, CRMMappingDataService) {
										return CRMMappingDataService.getSystemMapping().$promise;
									}
	          					}
	          				} 
	          			}
	            	  })
	            	  .state('secondaryqadetail', {
	                      url:'/secondaryqadetail/{prospectCallId}',
	                      views: {	
	          				"header": {templateUrl: '/partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	    
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/secondaryqa/secondaryqadetail.html',
	          					controller: "SecondaryQaDetailController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'SecondaryQaDetailController',
	    	                  				files: ['/js/secondaryqa/secondaryqadetailcontroller.js']
	    	                      		});
	    	                      	},
	    	                    	qadetail: function(module, qADetailService,$stateParams) {
	    	                      		return qADetailService.query({"prospectCallId": $stateParams.prospectCallId}).$promise;
	          						},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						qaRejectReasons: function(module, qaRejectReasonService) {
	    	                      		return qaRejectReasonService.query().$promise;
	          						},
									  attributeMappings: function(module, CRMMappingDataService) {
										return CRMMappingDataService.getSystemMapping().$promise;
									}
	          					}
	          				} 
	          			}
	            	  })
	            	  .state('qascore', {
	                      url:'/qascore/{prospectCallId}',
	                      views: {	
	          				"header": {templateUrl: '/partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	    
							},
	          				"main": {
	          					templateUrl: '/js/qascore/qascore.html',
	          					controller: "QaScoreController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'qaScore',
	    	                  				files: 
	    	                  					[
	    	                  					 '/js/qascore/qascore.module.js',
	    	                  					 '/js/qascore/qascore.service.js',
	    	                  				     '/js/qascore/qascore.controller.js',
	    	                  				     '/js/qascore/components/navigation/qascore-navigation.directive.js',
	    	                  				     ]
	    	                      		});
	    	                      	},
	    	                    	qadetail: function(module, qADetailService,$stateParams) {
	    	                      		return qADetailService.query({"prospectCallId": $stateParams.prospectCallId}).$promise;
	          						},
	    	                      	user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						qaRejectReasons: function(module, qaRejectReasonService) {
	    	                      		return qaRejectReasonService.query().$promise;
	          						}
	          					}
	          				} 
	          			}
	            	  }).state('adminhome', {
	                      url:'/adminhome',
	                      views: {
							"header": {templateUrl: 'partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/adminhome.html'
	          				} 
	          			
	          			}
	            	  }).state('idnc', {
	                      url:'/idnc',
	                      views: {
							"header": {templateUrl: 'partials/header.html',

	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}	
	          				
							},
	          				"main": {
	          					templateUrl: '/partials/idnc.html',
	          					controller: "IdncController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'IdncController',
	    	                  				files: ['/js/idnccontroller.js']
	    	                      		});
	    	                      	},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						}
	    	                      	

	          					}
	          				} 
	          			}
	            	  }).state('login', {
	            		  url: '/login',
	            		  views: {
	            			  "header": {templateUrl: '/partials/login-header.html'},
	            			  "main": {
	            				  templateUrl: '/partials/login-main.html',
	            				  controller: 'LoginController',
	            				  resolve: {
	            					  module: function($ocLazyLoad, $q) {
		    	                      		return $ocLazyLoad.load({
		    	                  				name: 'LoginController',
		    	                  				files: ['/js/logincontroller.js']
		    	                      		});
		    	                      	}
	            				  }
	            			  }
	            			  
	            		  }
	            	  }).state('mfa', {
						url: '/mfa',
						views: {
							"header": {templateUrl: '/partials/login-header.html'},
							"main": {
								templateUrl: '/partials/mfa.html',
								controller: 'MfaController',
								resolve: {
									module: function($ocLazyLoad, $q) {
											return $ocLazyLoad.load({
												name: 'MfaController',
												files: ['/js/mfacontroller.js']
											});
										},user: function(module, UserService){
											return UserService.query().$promise;
										},
								}
							}
							
						}
					}).state('mfatotp', {
						url: '/mfatotp',
						views: {
							"header": {templateUrl: '/partials/login-header.html'},
							"main": {
								templateUrl: '/partials/mfatotp.html',
								controller: 'MfaTotpController',
								resolve: {
									module: function($ocLazyLoad, $q) {
											return $ocLazyLoad.load({
												name: 'MfaTotpController',
												files: ['/js/mfatotpcontroller.js']
											});
										},user: function(module, UserService){
											return UserService.query().$promise;
										},
								}
							}
							
						}
					}).state('resetpassword', {
	            		  url: '/resetpassword',
	            		  views: {
	            			  "main": {
	            				  templateUrl: '/partials/reset-password.html',
	            				  controller: 'ResetPasswordController',
	            				  resolve: {
	            					  module: function($ocLazyLoad, $q) {
		    	                      		return $ocLazyLoad.load({
		    	                  				name: 'ResetPasswordController',
		    	                  				files: ['/js/resetpasswordcontroller.js']
		    	                      		});
										  },
										user: function(module, UserService){
											return UserService.query().$promise;
										},
	            				  }
	            			  }
	            		}
					 }).state('supportadmin', {
	                      url:'/supportadmin',
	                      views: {	
	          				"header": {
	          					templateUrl: 'partials/header.html',
	          					controller:"HeaderController",
	          					resolve:{
	          						module:	function($ocLazyLoad,$q){
	          							return $ocLazyLoad.load({
	          								name: 'HeaderController',
	          								files: ['/js/headercontroller.js']
	          							});
	          						},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          						campaignTypes: function(module, CampaignTypeService) {
	    	                      		return CampaignTypeService.query().$promise;
	          						}
	           					}
							},
	          				"main": {
	          					templateUrl: '/partials/support-admin/supportadmin.html',
	          					controller: "SupportAdminController",
	          					resolve: {
	    	                      	module: function($ocLazyLoad, $q) {
	    	                      		return $ocLazyLoad.load({
	    	                  				name: 'SupportAdminController',
	    	                  				files: ['/js/support-admin/supportadmincontroller.js']
	    	                      		});
	    	                      	},
	          						user: function(module, UserService){
	          							return UserService.query().$promise;
	          						},
	          					}
	          				} 
	          			}
	            	  })
	            	 
	              }]
	);
	
	
});
