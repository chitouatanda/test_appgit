define(['angular'], function(angular) {
	angular.module('xtaas.constants', [])
		.constant("XTAAS_CONSTANTS", {
			callStatus: {
				connected: 'CONNECTED',
				disconnected: 'DISCONNECTED',
				preview: 'PREVIEW',
				dialed: "DIALED"
			},
			dispositionStatus: [{
				key: 'Success',
				value: 'SUCCESS',
			}, {
				key: 'Failure',
				value: 'FAILURE',
			}, {
				key: 'Dialer Code',
				value: 'DIALERCODE',
			}, {
				key: 'Callback',
				value: 'CALLBACK'
			}],
			attributeType: {
				area: 'AREA',
				country: 'COUNTRY',
				state: 'STATE',
				city: 'CITY',
				role: 'ROLE',
				department: 'DEPARTMENT'
			}
		});
});