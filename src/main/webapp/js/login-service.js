define(['angular', 'angular-resource'], function(angular) {
	angular.module('login-service', ['ngResource'])
	.factory("LoginService", function ($http, $resource) {
			var getLandingUrl = function() {
				return $resource("/spr/landingpage");
			};
			return {
				authenticate: function(username, password, rememberMe) {
					return $http.post (
						"/j_spring_security_check", 
						"j_username=" + username + "&j_password=" + password + "&_spring_security_remember_me=" + rememberMe, 
						{
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});
				},
				logout: function() {
					return $http.get("/j_spring_security_logout");
				},
				getLandingUrl: getLandingUrl
			};
		}).factory("PasswordService", function ($resource) {
			return $resource("", {}, {
				lockUserAfterFiveAttempts: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/lockuser/:username"
				},
				resetUserAfterFiveAttempts: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/resetuser/:username"
				},
				getUser: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/getuserfromdb/:username"
				},
				getQRCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/createqrcode/:username"
				},
				getTOTPCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/totp/:username/:totp"
				}
			});
		});
});




