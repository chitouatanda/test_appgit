define(['angular'], function(angular) {
	angular.module('xtaasaudioplayer',[])
	.directive('audioplayer',function($interval) {
	    return {
	        restrict:'A',
	        templateUrl: '/partials/audio-plugin.html',
	        link: function($scope, element, attrs){
	        	$scope.audio.src = attrs.audioplayer;
	        },
	        controller: function($scope){
	        	$scope.audio = new Audio();
	            $scope.vol = 0.6;
	            $scope.audio.volume = $scope.vol;
	            
	            $scope.play = function(){
	                $scope.audio.play();
	            };
	            $scope.pause = function(){
	                $scope.audio.pause();
	            };
	            $scope.restart = function(){
	            	$scope.audio.currentTime = 0.0;
	            	$scope.play = false;
            	  	$scope.pause = true;
	            };
	            $scope.play = true;
        	  	$scope.pause = false;
	            $scope.playOrPause = function (){
        	  		if($scope.audio.paused){
        	  			 $scope.audio.play();
        	  			 $scope.play = false;
        	  			 $scope.pause = true;
        	  		} else {
        	  	    	$scope.audio.pause();
        	  	    	$scope.play = true;
                	  	$scope.pause = false;
        	  		}
        	  	}
	            var timerstart = $interval(function(){
	                $scope.ctime = $scope.audio.currentTime.toFixed(1);
	                if ($scope.ctime == $scope.duration) {
	                	if ($scope.audio.loop) {
	                		$scope.play = false;
                    	  	$scope.pause = true;
	                	} else {
	                		$scope.play = true;
                    	  	$scope.pause = false;
	                	}
	                	$scope.ctime = 0.0;
        	        }
	            },100);
	            $scope.$watch('audio.duration', function(newval){//watch for getting new duration value when audio object is loaded
	            	if (isNaN(newval)) {
	            		$scope.duration = 0.0;
	            	} else {
	            		$scope.duration = $scope.audio.duration.toFixed(1);
	            	}
	            });
	            $scope.changetime = function(){
	                $scope.audio.currentTime = $scope.ctime;
	            };
	            $scope.fastForward = function(){
	            	$scope.audio.currentTime += 15.0;
	            };
	            $scope.fastBackward = function(){
	            	$scope.audio.currentTime -= 15.0;
	            };
	            $scope.loop = function(){
	            	if ($scope.audio.loop) {
	            		$scope.audio.loop = false;
	            	} else {
	            		$scope.audio.loop = true;	
	            	}
	            	$scope.play = false;
            	  	$scope.pause = true;
	            };
	            $scope.changevol = function(t){
	                $scope.audio.volume = $scope.vol;
	            };
	            $scope.isMute = false;
	            $scope.muteOrUnmuteVolume = function() {
	            	if ($scope.audio.muted) {
	            		$scope.audio.volume = $scope.vol;
	            		$scope.isMute = false;
	            		$scope.audio.muted = false;
	            	} else {
	            		$scope.audio.muted = true;
	            		$scope.isMute = true;
	            	}
	            }
	            $scope.$on('$destroy', function ()  {
	            	$scope.audio.pause();
	            	$scope.audio = undefined;
	            	$interval.cancel(timerstart);
				});
	            $scope.changeSecondFormat = function(secs) {
	                  var hr  = Math.floor(secs / 3600);
	                  var min = Math.floor((secs - (hr * 3600))/60);
	                  var sec = Math.floor(secs - (hr * 3600) -  (min * 60));
	                  if (min < 10){ 
	                    min = "0" + min; 
	                  }
	                  if (sec < 10){ 
	                    sec  = "0" + sec;
	                  }
	                  return min + ':' + sec;
	            }
	        }
	    };
	});

});
