define(['angular', 'login-service',  'user-service', 'xtaas-security', 'notification-service', 'angular-localStorage','password-service'], function(angular) {
	angular.module('LoginController', ['login-service', 'user-service', 'xtaas.security', 'notification-service', 'ngStorage','password-service'])
		.controller('LoginController', [
		                                '$scope', 
		                                '$location',
		                                '$window',
		                                '$modal',
		                                '$rootScope',
		                                'LoginService',
		                                'loginSuccessUrl',
		                                'NotificationService',
		                                'ForgotPasswordLinkService',
		                                '$localStorage',
		                                '$timeout',
										'PasswordService',
										'$filter',
										function($scope, $location, $window, $modal, $rootScope, LoginService, loginSuccessUrl, NotificationService, ForgotPasswordLinkService, $localStorage, $timeout,PasswordService, $filter) {
		                                	$scope.myInputFocus = true;
		                                	$rootScope.isUserLoggedIn = false;
		                                	$scope.loading = false;
		                                	$scope.showCallbackAlert = false;
											$scope.showExpiredAlert = false;	
											$scope.userData = {};                               	
		                                	//Show alert message when get url string
		                                	if ($location.search()['err'] == "CONCURRENT_SESSION") {		                                		 
		                                		$scope.showExpiredAlert = true;
		                                		$window.location.href = '/#!/login';
                                			} 

											// $rootScope.agentInactivityMessage = "You were logged out of the system automatically at " + $filter('date')(new Date(), 'HH:mm') + " on " + $filter('date')(new Date(), 'dd MMM yyyy') + " because the system detected you are idle since last 20 minutes.";
											$rootScope.$on('onAgentLoggedOut', function (event, status) {
												$rootScope.agentInactivityMessage = status;
											})
		                                	
		                                	//Hide alert message after few seconds
		                                	$timeout(function () { $scope.showCallbackAlert = false; $scope.showExpiredAlert = false; }, 15000);
		                                	
		                                	//Hide alert message on click of close button
		                                	$scope.hideError = function() {		                                		
		                                		$scope.showCallbackAlert = false;
		                                		$scope.showExpiredAlert = false;
		                                		$window.location.href = '/#!/login';
		                                	}
		                                	
		                                	$scope.login = function() {
												$rootScope.agentInactivityMessage = "";
		                                		NotificationService.clear();
		                                		$scope.loading = true;	
		                                		$scope.showCallbackAlert = false;
		                                		$scope.showExpiredAlert = false;
		                                		if ($scope.loginForm.$valid) {
													PasswordService.getUser({username:btoa($scope.username)}).$promise.then(function(data){
														if(data != null && data != undefined) {
															if (data.passwordPolicy) {
																if(!data.userLock) {
																	if($scope.check90Days(data.lastPasswordUpdated) < 90) {
																		var page = LoginService.authenticate($scope.username, $scope.password,$scope.rememberMe).then(function() {
																			$rootScope.isUserLoggedIn = true;
																			delete $localStorage.prospectCallList;
																			delete $localStorage.filterCriteriaObj;
																			delete $localStorage.selectedStatus;
																			loginSuccessUrl.url = undefined;
																			//$localStorage.$reset();
																			//PasswordService.getUser({username:$scope.username}).$promise.then(function(data){
																				//if(data != null && data != undefined) {
																					if ($location.search()['redir']) {
																						$window.location.href = $location.search()['redir'];
																					}/* else if ($location.search()['err'] == "CONFLICT") {
																						UserService.query({}).$promise.then(function(userData) {
																							$scope.user = userData;		                                		
																						});
																						
																						$scope.showCallbackAlert = true;
																						$location.search()['err'] = "";
																						$window.location.href = '/#!/login';
																					}*/ else if (loginSuccessUrl.url != undefined) {		                                					
																						$location.path(loginSuccessUrl.url);
																					} else {										
																						/*var landingPage = LoginService.getLandingUrl().get();
																						landingPage.$promise.then(function(data) {
																							$rootScope.isUserLoggedIn = true;
																							if (data.page.indexOf("http") === 0) {
																								$window.location.href = data.page;
																							} else {*/
																								//$location.path("/launch");										
																							/*}
																						});	*/ 
																						//if (data.passwordPolicy) {
																							//if($scope.check90Days(data.lastPasswordUpdated) < 90) {
																								//if (data.activateUser) {
																									if(data.noOfAttempt > 0) {
																										PasswordService.resetUserAfterFiveAttempts({username:$scope.username}).$promise.then(function(){
																											$scope.setLoadingFalse();
																										}, function(error) {	
																											 console.log(error);	
																											 $scope.setLoadingFalse();	                                			 
																										});
																									}
																									
																									$location.path("/launch");
																									$rootScope.isLaunch = true;
																								//}else {
																									//$location.path("/resetpassword");
																								//} 
																							//}else {
																								//NotificationService.log('error',' Your password is expired. Please reset your password using Forget Password link.');
																								//var temp = LoginService.logout();
																								// temp.then(function() {	
																								// 	$location.path('/login');
																								// });
																								//$scope.setLoadingFalse();
																							//}
																						//}else {
																							//$location.path("/launch");
																						//}                             			
																					}
																				//}
																			// }, function(error) {	
																			// 	console.log(error);	
																			// 	$scope.setLoadingFalse();	                                			 
																			// }); 
																			$scope.loading = false;
																		}, function(error) {	
																			console.log(error);		                                			
																			if (error.message == "method is not defined") {
																				//When status 409 error occurs, below else block also executed so used this condition
																				$scope.setLoadingFalse();
																			} else {
																				PasswordService.lockUserAfterFiveAttempts({username:btoa($scope.username)}).$promise.then(function(data){
																					$scope.setLoadingFalse();	
																				}, function(error) {	
																				 	console.log(error);	
																				 	$scope.setLoadingFalse();	                                			 
																				});
																				NotificationService.log('error','Login failed! Invalid username/password. Please try again.');
																				$scope.setLoadingFalse();
																			}
																		});
																 	}else {
																	 	NotificationService.log('error',' Your password is expired. Please reset your password using Forget Password link.');
																		$scope.setLoadingFalse();
																 	}
															 	}else {
																	NotificationService.log('error',' Your account is Locked. Please contact support team..');
																	$scope.setLoadingFalse();
																}
															} else if (data.multiFactorRequired) {
																if (!data.userLock) {
																	if (data.allowMFAUser) {
																			if (!data.activateMFAUser) {
																				$scope.userLoginAPI($scope.username,$scope.password,$scope.rememberMe,"FirstTime");
																			} else {
																				$scope.userLoginAPI($scope.username,$scope.password,$scope.rememberMe,"SecondTime");
																			}
																		}  else {
																		     $scope.userLoginAPI($scope.username, $scope.password,$scope.rememberMe,"Login");
																		}
																}else {
																	NotificationService.log('error',' Your account is Locked. Please contact support team..');
																	$scope.setLoadingFalse();
																}	
															} else {
																$scope.userLoginAPI($scope.username, $scope.password,$scope.rememberMe,"Login");
															}	
														}
													 }, function(error) {	
													 	console.log(error);		                                			
													 	if (error.message == "method is not defined") {
													 		//When status 409 error occurs, below else block also executed so used this condition
													 		$scope.setLoadingFalse();
													 	} else {
													 		NotificationService.log('error','Login failed! Invalid username/password. Please try again.');
													 		$scope.setLoadingFalse();
													 	}
													});
	                                			
		                                	} else {
		                                		NotificationService.log('error','Please enter username & password');
                                				$scope.setLoadingFalse();
                                			}
		                                };
		                                $scope.forgotPassword = function() {		                                	
		                                	var modalInstance = $modal.open({
		                                		templateUrl: 'forgot-password.html',
		                                		controller: ForgotPasswordModalInstanceCtrl,
		                                		windowClass : 'forgotpassword-modal'
		                                	});
										};
										$scope.setLoadingFalse = function() {
											$scope.loading = false;
                                			$scope.showCallbackAlert = false;
                                			$scope.showExpiredAlert = false;
										}

										$scope.userLoginAPI = function(userName,password,rememberMe,loginType) {
											var page = LoginService.authenticate(userName, password,rememberMe).then(function() {
												$rootScope.isUserLoggedIn = true;
												delete $localStorage.prospectCallList;
												delete $localStorage.filterCriteriaObj;
												delete $localStorage.selectedStatus;
												loginSuccessUrl.url = undefined; 
												if ($location.search()['redir']) {
													$window.location.href = $location.search()['redir'];
												} else if (loginSuccessUrl.url != undefined) {		                                					
													$location.path(loginSuccessUrl.url);
												} else {
													if(loginType == "Login"){
														$location.path("/launch");
														$rootScope.isLaunch = true;
													} else if(loginType == "FirstTime"){
														$location.path("/mfa");
													}else if(loginType == "SecondTime"){
														$location.path("/mfatotp");
													}								                               			
												}
												$scope.loading = false;
											}, function(error) {	
												console.log(error);		                                			
												if (error.message == "method is not defined") {
													$scope.setLoadingFalse();
												} else {
													NotificationService.log('error','Login failed! Invalid username/password. Please try again.');
													$scope.setLoadingFalse();
												}
											});
										}
										
										$scope.check90Days = function(lastPasswordUpdated) {
											var firstDate = null;
											if(lastPasswordUpdated == null || lastPasswordUpdated == undefined){
												firstDate = new Date();
											}else {
												firstDate = new Date(lastPasswordUpdated);
											}
											var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
											//var firstDate = new Date(lastPasswordUpdated);//last Password Updated Date
											var secondDate = new Date();//Today Date
											var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
											return diffDays;
										}
		                                $scope.forgotPasswordToaster = false;
		                                var ForgotPasswordModalInstanceCtrl = function ($scope, $state, $modalInstance) {
		                                	$scope.forgotPassword = function(userId) {
		                                		if ($scope.forgotpasswordform.$valid) {
		                                			$scope.loading = true;
			                                		ForgotPasswordLinkService.query({username: userId}).$promise.then(function(userData) {
		                                				$scope.loading = false;
		                                				//$scope.forgotPasswordToaster = true;
		                                				NotificationService.log('success','Password reset email has been sent successfully. Please follow the instructions in the email.');
		                                				$modalInstance.dismiss('cancel');
						       		    	        },function() {
						       		    	        	$scope.loading = false;
						       		    	        });
		                                		}else {
						            				$scope.invalid = true;
						            				$scope.loading = false;
						            			}
		                                	};

		                      				$scope.cancel = function () {
		                      					$modalInstance.dismiss('cancel');
		                      				};
		                                };
		                           }]).directive('focusMe', function ($timeout, $parse) {
									      	  return {
									    		  link: function (scope, element, attrs) {
									    		  var model = $parse(attrs.focusMe);
									    		  scope.$watch(model, function (value) { 
									    			  if (value === true) {
									    				  $timeout(function () {
									    					  scope.$apply(model.assign(scope, false));
									    					  element[0].focus();
									    				  }, 30);
									    			  }
									    		  });
									    	  }
									    	  };
									      });
});
