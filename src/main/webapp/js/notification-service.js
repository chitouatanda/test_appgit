define(['angular', 'angular-toaster'], function(angular) {
	angular.module('notification-service', ['toaster'])
		.service("NotificationService", function (toaster) {
			return {
				log: function(type, message) {
					console.log(message);
					toaster.pop(type, "", message, null, 'trustedHtml');
				 },
			 	clear : function() {
			 		toaster.clear();
			    }
			};
		});

});
