define(['angular', 'angular-resource'], function(angular) {
	angular.module('agent-service', ['ngResource'])
		.factory("AgentService",function($resource) {
			return $resource("/spr/rest/agent/{agentId}/schedule?searchstring=:searchstring",{},{
				query: {
		            method: 'GET',
		            isArray:true
		                }
			});
		}).factory("UserPasswordService",function($resource) {
			return $resource("/spr/rest/user/:id/attribute/password",null,{
				update: {
					method : 'PUT',
					headers : {
	                    'Content-Type': 'application/json'
	                }
					
				}
			});
		});
});