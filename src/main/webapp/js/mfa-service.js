define(['angular', 'angular-resource'], function(angular) {
	angular.module('mfa-service', ['ngResource'])
	.factory("MfaService", function ($resource) {
			return $resource("", {}, {
				getUser: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/getuserfromdb/:username"
				},
				getQRCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/createqrcode/:username"
				},
				getTOTPCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/totp/:username/:totp"
				}
			});
		});
});




