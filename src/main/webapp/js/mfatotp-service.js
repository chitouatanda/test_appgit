define(['angular', 'angular-resource'], function(angular) {
	angular.module('mfatotp-service', ['ngResource'])
	.factory("MfaTotpService", function ($resource) {
			return $resource("", {}, {
				getQRCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/createqrcode/:username"
				},
				getTOTPCode: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/totp/:username/:totp"
				}
			});
		});
});




