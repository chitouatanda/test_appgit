var app = angular.module('AddNewIdncModalInstanceCtrl', ['angular', 'angular-ui-select', 'user-service','idnc-service','partner-service','notification-service']);

app.controller('AddNewIdncModalInstanceCtrl', ['$scope', '$modalInstance', '$rootScope','PartnerService', 'idncSaveService', 'user', 'campaignId', '$filter', 'NotificationService']);

	var AddNewIdncModalInstanceCtrl = function($scope, $modalInstance, $rootScope,PartnerService, idncSaveService, user, campaignId, $filter, NotificationService) {
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
		$scope.dncType = 'PROSPECT';
		$scope.idncTriggers = [];
		$scope.partnerIds = [];
		$scope.modal = {};
		PartnerService.query({}).$promise.then(function(partners){
			angular.forEach(partners, function(v, k) {
				 $scope.partnerIds.push(v);		
				});
		});
		$scope.userRoleSupervisor = $filter('filter')(user.roles, "SUPERVISOR")[0];
		$rootScope.showIDNCToastMsg = false;
		
		$scope.save = function() {
			// if ($scope.addnewidncForm.$valid) {
				if (validateForm($scope.dncType)) {
				setFirstNameAndLastName();
				$scope.loading = true;
				if ($scope.userRoleSupervisor == "SUPERVISOR") {
					$scope.modal.partnerId = user.organization;
				}
				var obj= {"firstName": $scope.modal.firstName, "lastName": $scope.modal.lastName, "phoneNumber" : $scope.modal.pnum,"note":$scope.modal.note,"partnerId":$scope.modal.partnerId, "domain": $scope.modal.domain, "company":$scope.modal.company, "dncLevel":$scope.dncType };
				idncSaveService.save({campaignId: campaignId}, obj).$promise.then(function(data) { 
					$scope.loading = false;
					$rootScope.showIDNCToastMsg = true;
					NotificationService.log('success','Successfully added to internal do not call list.');
					$scope.filterIdncList();
					$modalInstance.dismiss('cancel');
				},function(error) {
					$scope.loading = false;
				});
			}
		// } else {
		// 	$scope.invalid = true;
		// }
	};

	setFirstNameAndLastName = function() {
		if ($scope.modal.firstName == undefined || $scope.modal.firstName == null) {
			$scope.modal.firstName = "";
		} 
		if ($scope.modal.lastName == undefined || $scope.modal.lastName == null) {
			$scope.modal.lastName = "";
		}
	}

	validateForm = function(type) {
		if(type == 'PROSPECT') {
			if (!$scope.modal.firstName ||  !$scope.modal.lastName){
				NotificationService.log('error', 'First name, Last name, Phone/Domain is mandatory');
				return false;
			}
			if ($scope.modal.pnum == null && $scope.modal.domain == null) {
				NotificationService.log('error', 'First name, Last name, Phone/Domain is mandatory');
				return false;
			}
		} else if(type == 'COMPANY') {
			if (!$scope.modal.company &&  !$scope.modal.domain && !$scope.modal.pnum){
				NotificationService.log('error', 'Either Phone, Domain or Company name is mandatory');
				return false;
			}
		} else {
			NotificationService.log('error', 'Please select DNC level');
			return false;
		}
		 return true;
	};

	$scope.radioBoxSelected = function(type) {
		$scope.dncType = type;
	}
}

