define(['angular', 'angular-resource'], function(angular) {
	angular.module('teamnotice-service', ['ngResource'])
		.factory("NoticeService", function($resource) {
        return $resource("", {}, {
            get: {
                method: 'GET',
                isArray: true,
                url: "/spr/teamnotice/:userId"
            },
            save: {
                method: 'POST',
                url: "/spr/teamnotice",
                headers : {
                    'Content-Type': 'application/json'
                }
            },
            markAsRead: {
                method: 'POST',
                url: "/spr/teamnotice/markAsRead",
                headers : {
                    'Content-Type': 'application/json'
                }
            },
            getUnReadCount: {
                method: 'GET',
                url: "/spr/teamnotice/:userId/unreadcount"
            }
        });
    });
});
