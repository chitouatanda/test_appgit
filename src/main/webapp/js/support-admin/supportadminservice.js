define(['angular', 'angular-resource'], function (angular) {
	angular.module('supportadminservice', ['ngResource'])
		.factory("impersonateService", function ($resource) {
			return $resource("", null, {
				getUsers: {
					method: 'GET',
					isArray: false,
					url: "spr/rest/users?searchstring=:searchstring",
				},
				impersonate: {
					method: 'GET',
					isArray: false,
					url: "spr/impersonate?username=:username"
				}
			});
		})
});
