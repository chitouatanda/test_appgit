define(['angular', 'support-admin/supportadminservice', 'notification-service'], function (angular) {
	angular.module('SupportAdminController', ['supportadminservice', 'notification-service'])
		.controller('SupportAdminController', ['$scope', '$state', 'impersonateService', 'NotificationService',
			function ($scope, $state, impersonateService, NotificationService) {
				$scope.loading = true;
				$scope.searchText = '';
				$scope.searchType = '';
				$scope.userList = [];
				$scope.userCount = 0;
				$scope.userListLength = 0;
				$scope.noSize = false;
				$scope.currentPage = 1;
				$scope.totalPageSize = [{
					id: 20, name: '20 rows'
				}, {
					id: 50, name: '50 rows'
				}, {
					id: 100, name: '100 rows'
				}];
				$scope.pageSize = $scope.totalPageSize[2].id;

				$scope.getUserList = function () {
					$scope.loading = true;
					impersonateService.getUsers({
						searchstring: angular.toJson({
							"searchType": $scope.searchType,
							"searchText": $scope.searchText,
							"sort": "id",
							"direction": "ASC",
							"page": $scope.currentPage - 1,
							"size": $scope.pageSize
						})
					}).$promise.then(function (data) {
						$scope.loading = false;
						$scope.userList = data.userList;
						$scope.userCount = data.userCount;
						$scope.noSize = false;
						if ($scope.userList.length < 20) {
							$scope.noSize = true;
						}
					}, function (error) {
						$scope.loading = false;
						$scope.searchByField = {};
						console.error('Failed to fetch user list. Error: ' + error);
						NotificationService.log('error', 'Failed to fetch user list');
					});
				}

				// on page load fetch user list
				$scope.getUserList();

				$scope.impersonate = function (username) {
					$scope.loading = true;
					impersonateService.impersonate({
						username: username
					}).$promise.then(function (data) {
						$state.go(data.page.substring(1, data.page.length));
					}, function (error) {
						$scope.loading = false;
						console.error('Failed to perform user impersonate. Error: ' + error);
						NotificationService.log('error', 'Failed to perform user impersonate');
					});
				}

				$scope.filterUserList = function () {
					if ($scope.searchText == '') {
						NotificationService.log('error', 'Please enter search text');
						return;
					}
					if ($scope.searchType == '') {
						NotificationService.log('error', 'Please select search type');
						return;
					}
					$scope.getUserList();
				}

				$scope.reset = function () {
					$scope.searchText = '';
					$scope.searchType = '';
					$scope.getUserList();
				}

			}
		]);
});
