define([ 'angular', 'angular-ui-select', 'moment', 'angular-ui-bootstrap','angular-slider', 'ang-drag-drop', 'angular-ui-tinymce', 'campaign/campaign-service',
         'supervisor/supervisor-service', 'notification-service','agent/agent-service', 'agent/twilio-service', 'angular-timer', 'teamnotice-service', 'angular-timeZone',
         'lodash', 'xtaas-config', 'angular-localStorage','idnc-service','idnccontroller', 'angular-loadjs', 'pagination', 'ngFileUpload', 'login-service'],
function(angular) {
angular.module('NewSupervisorController',[ 'campaign-service', 'supervisor-service','ui.select', 'ui.bootstrap', 'ngSlider','ang-drag-drop', 'notification-service', 'ui.tinymce', 
                                           'agent-service', 'twilio-service','timer', 'teamnotice-service', 'xtaas.config', 'ngStorage','idnc-service','IdncController','angularLoad', 'angularUtils.directives.dirPagination', 'ngFileUpload', 'login-service'])
.controller('NewSupervisorController',[
									'$scope',
									'$rootScope',
									'$stateParams',
									'$window',
									'$modal',
									'$state',
									'$timeout',
									'$interval',
									'$filter',
									'$http',
									'MasterDataService',
									'CampaignService',
									'campaignCallableRecordService',
									'CampaignCommandService',
									'CampaignTeamService',
									'CallGuideService',
									'ActivateAssetService',
									'MetricsService',
									'ProspectAttributeService',
									'contactSearchService',
									'recycleStatusService',
									'purchaseService',	
									'purchaseHistoryService',
									'fileUpload',
									'preferenceMetricsService',
									'preferenceMetricsSaveService',
									'agentAllocationService',
									'agentAllocationOverrideService',
									'AgentCalendarService',
									'AssetEmailService',
									'NotificationService',
									'agentMtricsService',
									'$log',
									'Pusher',
									'user',
									'applicationProperties',
									'twilioService',
									'twilioTokenService',
									'FileUploadTrackService',
									'campaignListService',
									'agentListService',
									'campaignListMetricsService',
									'emailBouncesService',
									'SupervisorNoticeService',
									'XTAAS_CONFIG',
									'NoticeService',
									'$localStorage',
									'idncService',
									'idncSaveService',
									'angularLoad',
									'Upload',
									'PivotService',
									'GetOrganizationService',
									'ZoomBuyService',
									'CampaignSettingService',
									'agentModeChangeService',
									'CampaignReportService',
									'AgentReportService',
									'$q',
									'sendPivotEmailService',
									'agentUnAllocationOverrideService',
									'healthCheckService',
									'SupervisorCampaignService',
									'MoveDataToCallableService',
									'suppressCallableService',
									'EverstringService',
									'abmListCountService',
									'attributeMappings',
									'getSuppressionListService',
									'supervisorAgentStatusService',
									'supervisorApproveAllAgentStatusService',
									'searchCampaignService',
									'LoginService',
									'$location',
									function($scope, $rootScope, $stateParams,$window, $modal, $state, $timeout, $interval, $filter, $http,MasterDataService, CampaignService, campaignCallableRecordService, CampaignCommandService,CampaignTeamService,
											CallGuideService,ActivateAssetService,MetricsService,ProspectAttributeService,contactSearchService,recycleStatusService,purchaseService,purchaseHistoryService,fileUpload,preferenceMetricsService,
											preferenceMetricsSaveService,agentAllocationService,agentAllocationOverrideService,AgentCalendarService,AssetEmailService,NotificationService,
											agentMtricsService, $log, Pusher, user, applicationProperties, twilioService,twilioTokenService,FileUploadTrackService,campaignListService,agentListService,campaignListMetricsService, emailBouncesService,
											SupervisorNoticeService, XTAAS_CONFIG, NoticeService, $localStorage, idncService, idncSaveService, angularLoad, Upload, PivotService, GetOrganizationService, ZoomBuyService, 
											CampaignSettingService, agentModeChangeService,CampaignReportService,AgentReportService, $q, sendPivotEmailService, agentUnAllocationOverrideService, healthCheckService, SupervisorCampaignService, MoveDataToCallableService, suppressCallableService, EverstringService, abmListCountService, attributeMappings, getSuppressionListService, supervisorAgentStatusService, supervisorApproveAllAgentStatusService, searchCampaignService,LoginService, $location) {
										$scope.loading = true;
										$scope.disableActivateDataButton = false;
										$scope.isResponseReceived = false;
										$scope.attributeMappings = attributeMappings;
										$scope.selectedCampaignCriteria = [];
										$scope.listOfCriteria = [];
										$scope.isShowOptions = false;
										$scope.isPicklistvalues = false;
										$scope.valueList = '';
										$scope.listOfCriteria1 = [];
										$scope.campaignName1 = "";
										$scope.pivotReview = false;
										$scope.showDataUpload = false;
										if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("SUPERVISOR")) {
											$state.go('login');
											return;
										}
										if($localStorage.validUser == null || $localStorage.validUser == undefined || $localStorage.validUser == '') {
											var temp = LoginService.logout();
											temp.then(function() {
												$rootScope.isUserLoggedIn = false;				
												$localStorage.$reset();	
												$location.path('/login');
											});
										}

										
										$scope.approveAllAgentRequest = function() {
											$scope.loading = true;
											var agentCommandDTOs = [];
											if ($scope.agentList != null && $scope.agentList != undefined) {
												angular.forEach($scope.agentList, function(agent, key) {
													if (agent.agentStatusChangeApprove && (agent.agentStatus == 'REQUEST_OTHER' || agent.agentStatus ==  'REQUEST_MEETING'  || agent.agentStatus == 'REQUEST_BREAK'  || agent.agentStatus == 'REQUEST_OFFLINE' || agent.agentStatus == 'REQUEST_TRAINING')) {
														var agentDTO = {};
														if (agent.agentStatus == 'REQUEST_BREAK') {
															agentDTO.agentStatus = 'ONBREAK';
														} else if (agent.agentStatus == 'REQUEST_MEETING') {
															agentDTO.agentStatus = 'MEETING';
														} else if (agent.agentStatus == 'REQUEST_TRAINING') {
															agentDTO.agentStatus = 'TRAINING';
														} else if (agent.agentStatus == 'REQUEST_OFFLINE') {
															agentDTO.agentStatus = 'OFFLINE';
														} else if (agent.agentStatus == 'REQUEST_OTHER') {
															agentDTO.agentStatus = 'OTHER';
														}
														agentDTO.agentCommand = "STATUS_CHANGE";
														agentDTO.campaignId = agent.campaignId,
														agentDTO.agentType = agent.agentType;
														agentDTO.agentId = agent.agentId;
														agentCommandDTOs.push(agentDTO);
													}
												});
											}
											if (agentCommandDTOs.length > 0) {
												supervisorApproveAllAgentStatusService.query({}, agentCommandDTOs).$promise.then(function () {
													$scope.getAgentList();
													$scope.loading = false;
													NotificationService.log('success', "All Agents request approved successfully.");
												},function(error) {
													$scope.loading = false;
												})
											} else {
												NotificationService.log('error', "There are no agent requests to approve the break.");
											}
										}

										
										$scope.approveAgentRequest = function(agentDTO) {
											$scope.loading = true;
											if (agentDTO.agentStatus == 'REQUEST_BREAK') {
												agentDTO.agentStatus = 'ONBREAK';
											} else if (agentDTO.agentStatus == 'REQUEST_MEETING') {
												agentDTO.agentStatus = 'MEETING';
											} else if (agentDTO.agentStatus == 'REQUEST_TRAINING') {
												agentDTO.agentStatus = 'TRAINING';
											} else if (agentDTO.agentStatus == 'REQUEST_OFFLINE') {
												agentDTO.agentStatus = 'OFFLINE';
											} else if (agentDTO.agentStatus == 'REQUEST_OTHER') {
												agentDTO.agentStatus = 'OTHER';
											}
											var agent = agentDTO;
											supervisorAgentStatusService.query({}, {
												agentCommand: "STATUS_CHANGE",
												agentStatus: agentDTO.agentStatus,
												campaignId: agentDTO.campaignId,
												agentType: agentDTO.agentType,
												agentId: agentDTO.agentId
											}).$promise.then(function () {
												$scope.getAgentList();
												$scope.loading = false;
												NotificationService.log('success', "Agent status approved successfully.");
											},function(error) {
												$scope.loading = false;
											})
										}
										Pusher.subscribe(user.id, 'reload_agent_info', function (agentId) {
											//$scope.loading = true;
											agentListService.query({
												searchstring : angular.toJson({
													"clause" : "BySupervisor",
													"timeZone" : timeZone
												})
											}, null).$promise.then(function(data) {
												$scope.agentList = data;
												$scope.generateAgentListForComponent();
												$scope.moveApprovalAgentOnTopInRow(agentId);
												$scope.$broadcast('timer-start');
												angular.forEach($scope.campaignsList,function(item,key) {
													$scope.$broadcast('timer-add-cd-seconds', 5);
												});
												$scope.timerRunning = true;
												$scope.loading = false;
											}, function(error) {
												$scope.loading = false;
											});
										});

										$scope.moveApprovalAgentOnTopInRow = function(agentId) {
											if ($scope.agentList != null && $scope.agentList != undefined && $scope.agentList.length > 0) {
												var agentIndex = $scope.agentList.findIndex(x => x.agentId == agentId);
												if (agentIndex != -1) {
													var filteredAgent = $scope.agentList[agentIndex];
													var tempAgentList = [];
													tempAgentList.push(filteredAgent);
													angular.forEach($scope.agentList, function(agent, key) {
														if (agent.agentStatusChangeApprove && (agent.agentStatus == 'REQUEST_OTHER' || agent.agentStatus ==  'REQUEST_MEETING'  || agent.agentStatus == 'REQUEST_BREAK'  || agent.agentStatus == 'REQUEST_OFFLINE' || agent.agentStatus == 'REQUEST_TRAINING')) {
															let index = tempAgentList.findIndex(agentInArray => agentInArray.agentId === agent.agentId);
															if (index == -1) {
																tempAgentList.push(agent);
															}
														}
													});
													var startSubList = $scope.agentList.slice(0, agentIndex);
													var endSubList = $scope.agentList.slice(agentIndex + 1, $scope.agentList.length);
													angular.forEach(startSubList, function(agent, key) {
														let index = tempAgentList.findIndex(agentInArray => agentInArray.agentId === agent.agentId);
														if (index == -1) {
															tempAgentList.push(agent);
														}
													});
													angular.forEach(endSubList, function(agent, key) {
														let index = tempAgentList.findIndex(agentInArray => agentInArray.agentId === agent.agentId);
														if (index == -1) {
															tempAgentList.push(agent);
														}
													});											
													$scope.agentList = tempAgentList;
												}
											}
										}

										// <!-- HEALTH CHECK SCREEN ON DATA LIST POPUP START -->
											$scope.getABMListCount = function (campaignId) {
												$scope.abmListCount = '';
												abmListCountService.query({campaignId : campaignId}).$promise.then(function (data) {
													if(data != undefined && data != null) {
														for(i = 0; i <= 15; i++) {
															if (data[i] != undefined) {
																$scope.abmListCount = $scope.abmListCount + data[i];
															}
														}
													}
												}, function (error) {
													console.log("Error occured while fetching abmlist count.");
												});
											}

											$scope.getSuppressionList = function (campaignId) {
												getSuppressionListService.query({campaignId : campaignId}).$promise.then(function (data) {
													$scope.suppressionList = data;
													if ($scope.suppressionList != null && $scope.suppressionList != undefined && $scope.suppressionList.domainCount != undefined) {
														$scope.companySuppressionCount = $scope.suppressionList.domainCount;
													} else {
														$scope.companySuppressionCount = 0;
													}
													if ($scope.suppressionList != null && $scope.suppressionList != undefined && $scope.suppressionList.emailCount != undefined) {
														$scope.contactSuppressionCount = $scope.suppressionList.emailCount;
													} else {
														$scope.contactSuppressionCount = 0;
													}
												}, function (error) {
													console.log("Error occured while fetching suppression list.");
												});
											}
									//  <!-- HEALTH CHECK SCREEN ON DATA LIST POPUP START -->
										

											
											/* * 
										 * DATE :- 31/03/2020
										 * ADDED CAMPAIGN SEARCH FUNCTIONALITY ON SUPERVISOR SCREEN. 
										 * 
										 */
										$scope.campaignName = "";
										$scope.searchCampaigns = function () {
											$scope.loading = true;
											if ($scope.campaignName == "" || $scope.campaignName == null || $scope.campaignName == undefined) {
												$scope.getCampaignList();
											} else {
												searchCampaignService.query({name : $scope.campaignName}).$promise.then(function (data) {
													$scope.campaignsList = data.campaignsList;
													$scope.showDataUpload = data.prospectUploadEnabled;
													$scope.loading = false;
												}, function (error) {
													$scope.loading = false;
													console.log("Error occured while searching the campaigns.");
												});
											}
										}
										// END

										/*Set default pagination values. Show drop down rows in campaigns tab*/
								        $scope.totalPageSize = [
								        	{ id: 10, name: '10 rows' },
								        	{ id: 20, name: '20 rows' },
								        	{ id: 30, name: '30 rows' },
					    	                { id: 40, name: '40 rows' },
											{ id: 60, name: '60 rows' },
											{ id: 80, name: '80 rows' }
										];
										$scope.pageSize = $scope.totalPageSize[1].id;
										$scope.disableSaveAssetBtn = true;
										/* START
										 * DATE : 16/05/2019
										 * REASON : agent allocation code starts */
										$scope.selectedAgents = [];
										$scope.agentMultiSelectSettings = {
											scrollable: true,
											enableSearch: true
										};

										$scope.generateAgentListForComponent = function() {
											$scope.showApproveAll = false;
											$scope.agentsForComponent = [];
											angular.forEach($scope.agentList, function(agent, key) {
												var obj = {};
												obj.label = agent.agentName;
												obj.id = agent.agentId;
												obj.agentStatus = agent.agentStatus;
												if (agent.agentStatusChangeApprove && (agent.agentStatus == 'REQUEST_OTHER' || agent.agentStatus ==  'REQUEST_MEETING'  || agent.agentStatus == 'REQUEST_BREAK'  || agent.agentStatus == 'REQUEST_OFFLINE' || agent.agentStatus == 'REQUEST_TRAINING')) {
													$scope.showApproveAll = true;
												}
												$scope.agentsForComponent.push(obj);
											});
										}

										$scope.assignCampaign = function(campaign, agentId) {
											$scope.loading = true;
											if (campaign.assignedAgents == undefined) {
												campaign.assignedAgents = [];
											}
											if (campaign.CampaignId != undefined && agentId != undefined) {
												agentAllocationOverrideService.override({
													agentId : agentId,
													campaignId : campaign.CampaignId
												}, null).$promise.then(function(data) {
													$scope.removeAgentFromAssignedCampaign(campaign, agentId);
													if (agentId == "assignAllAgents") {
														$scope.unSelectAllAgentsForOtherCampaign(campaign);
														$scope.addAllAgentsInCampaign(campaign);
														$scope.removeAllAgentsFromAvailablePool(campaign);
													} else {	
														var agentWithStatus  = $scope.agentList.filter(ag => ag.agentId == agentId);
														if (agentWithStatus[0] != null && agentWithStatus[0] != undefined) {
															let agent = {};
															agent.currentStatus = agentWithStatus[0].agentStatus;
															agent.name = agentWithStatus[0].agentName;
															agent.id = agentWithStatus[0].agentId;
															let index = campaign.assignedAgents.findIndex(agentInArray => agentInArray.id === agentWithStatus[0].agentId);
															if (index == -1) {
																campaign.assignedAgents.push(agent);
															}
															$scope.removeAgentFromAvailablePool(agentId);
														}
													}
													$scope.loading = false;
													NotificationService.log('success','Campaign assigned successfully.');
												},function(error) {
													$scope.loading = false;
												});
											}
										}

										$scope.removeAgentFromAssignedCampaign = function(campaign, agentId) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (campaign.CampaignId !== item.CampaignId) {
													if (item.assignedAgents != undefined && item.selectedAgents != undefined ) {
														let index = item.assignedAgents.findIndex(agentInArray => agentInArray.id === agentId);
														if (index != -1) {
															item.assignedAgents.splice(index,1);
														}
														let indexInModel = item.selectedAgents.findIndex(agentInArray => agentInArray.id === agentId);
														if (indexInModel != -1) {
															item.selectedAgents.splice(indexInModel,1);
														}
													}
												} 
											})
										}

										$scope.unSelectAllAgentsForOtherCampaign = function(campaign) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (campaign.CampaignId !== item.CampaignId) {
													if (item.assignedAgents != undefined && item.selectedAgents != undefined) {
														item.assignedAgents = [];
														item.selectedAgents= [];
													}
												}
												if (item.CampaignId == "UNALLOCATED") {
													item.Agents = [];
												} 
											})
										}

										$scope.addAllAgentsInCampaign = function(campaign) {
											angular.forEach($scope.agentsForComponent, function(item, key) {
													var agentWithStatus  = $scope.agentList.filter(ag => ag.agentId == item.id);
													if (agentWithStatus[0] != null && agentWithStatus[0] != undefined) {
														let agent = {};
														agent.currentStatus = agentWithStatus[0].agentStatus;
														agent.name = agentWithStatus[0].agentName;
														agent.id = agentWithStatus[0].agentId;
														let index = campaign.assignedAgents.findIndex(agentInArray => agentInArray.id === agentWithStatus[0].agentId);
														if (index == -1) {
															campaign.assignedAgents.push(agent);
														}
													}
											});
										}

										$scope.unAssignCampaign = function(campaign, assignedAgent) {
											if (assignedAgent == "unAssignAllAgents") { 
												if (campaign.selectedAgents != undefined && campaign.selectedAgents.length == 0) {
													return;
												}
											}
											$scope.loading = true;
											if (assignedAgent != undefined) {
													agentAllocationOverrideService.delete({agentId : assignedAgent, campaignId : campaign.CampaignId}, null).$promise.then(function(data) {
													if (assignedAgent == "unAssignAllAgents") {
														$scope.addAllAgentsInAvailablePool(campaign);
														campaign.assignedAgents = [];
													} else {	
														if (campaign.assignedAgents != null && campaign.assignedAgents != undefined) {
															let index = campaign.assignedAgents.findIndex(agentInArray => agentInArray.id === assignedAgent);
															campaign.assignedAgents.splice(index,1);
														}
														$scope.addAgentInAvailablePool(assignedAgent);
													}
													$scope.loading = false;
													NotificationService.log('success','Campaign unassigned successfully.');
												},function(error) {
													$scope.$parent.loading = false;
												});
											}
										}

										$scope.addAllAgentsInAvailablePool = function(campaign) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (item.CampaignId == "UNALLOCATED") {
													angular.forEach(campaign.assignedAgents, function(campaignItem, key) {
														var agentWithStatus  = $scope.agentList.filter(ag => ag.agentId == campaignItem.id);
														if (agentWithStatus.length > 0) {
															let agent = {};
															agent.currentStatus = agentWithStatus[0].agentStatus;
															agent.name = agentWithStatus[0].agentName;
															agent.id = agentWithStatus[0].agentId;
															let index = item.Agents.findIndex(agentInArray => agentInArray.id === agentWithStatus[0].agentId);
															if (index == -1) {
																item.Agents.push(agent);
															}
														}
													});
												} 
											})
										}

										$scope.removeAllAgentsFromAvailablePool = function(campaign) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (item.CampaignId == "UNALLOCATED") {
													item.Agents = [];
												} 
											})
										}

										$scope.addAgentInAvailablePool = function(availableAgent) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (item.CampaignId == "UNALLOCATED") {
													var agentWithStatus  = $scope.agentList.filter(ag => ag.agentId == availableAgent);
													if (agentWithStatus[0] != null && agentWithStatus[0] != undefined) {
														let agent = {};
														agent.currentStatus = agentWithStatus[0].agentStatus;
														agent.name = agentWithStatus[0].agentName;
														agent.id = agentWithStatus[0].agentId;
														let index = item.Agents.findIndex(agentInArray => agentInArray.id === agentWithStatus[0].agentId);
														if (index == -1) {
															item.Agents.push(agent);
														}	
													}
												} 
											})
										}

										$scope.removeAgentFromAvailablePool = function(assignedAgent) {
											angular.forEach($scope.modal.campaignAgent, function(item, key) {
												if (item.CampaignId == "UNALLOCATED") {
													let index = item.Agents.findIndex(agentInArray => agentInArray.id === assignedAgent);
													if (index != -1) {
														item.Agents.splice(index, 1);	
													}
												} 
											})
										}

										$scope.getAgentStatusClass = function(agent) {
											if (agent.currentStatus != undefined) {
												if (agent.currentStatus == "ONLINE") {
													return 'green_bullet';
												} else if (agent.currentStatus == "OFFLINE") { 
													return 'gray_bullet';
												} else {
													return 'red_bullet';
												}
											}
										}
										/* ####### agent allocation code ends */

										$scope.pageSize = $scope.totalPageSize[1].id;
										$scope.currentPage = 1;
										//Show drop down rows in configurations tab
								        $scope.totalPageSizeConfigTab = [
								        	{ id: 10, name: '10 rows' },
								        	{ id: 20, name: '20 rows' },
								        	{ id: 30, name: '30 rows' },
								        	{ id: 40, name: '40 rows' },
								        	{ id: 60, name: '60 rows' },
								        	{ id: 80, name: '80 rows' }
				    	                ];
								        $scope.pageSizeConfigTab = $scope.totalPageSizeConfigTab[1].id;
									  	$scope.currentPageConfigTab = 1;

										$scope.drop = {
												campaignId : "",
												agentId : "",
												agentData : ""
										};
										$scope.popover = {
												currentMonthYear : "",
												currentWeek : [],
										};
										
										$scope.tinymceOptions = {
										        plugins: 'link image code',
										        menubar: false,
										        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | fontselect fontsizeselect | code'
										};

										$scope.campaignSettingDTO = {};
										
										/* START
										 * DATE : 28/06/2017
										 * REASON : added to open textarea in read only mode (non editable) for partner supervisor */
										$scope.tinymceOptionsReadOnly = {
										        plugins: 'link image code',
										        menubar: false,
										        readonly: true,
										        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | fontselect fontsizeselect | code'
										};

										/*  ############### Date :- 30/10/18 Get the organization of loggedInUser  ############### */
										$scope.loggedInUser = user;
										$scope.loggedInUserOrganization = {};
										GetOrganizationService.query({id : $scope.loggedInUser.organization}).$promise.then(function(organization) {
											$scope.loggedInUserOrganization = organization;
										});

										$scope.liveQueueFetchTimer = 4000;
										$scope.campaignFetchTimer = 180000;
										angular.forEach(applicationProperties, function(item, key) {
											if (item.name === 'SUPERVISOR_LIVE_QUEUE_FETCH_TIMER') {
												$scope.liveQueueFetchTimer = item.value;
												console.log('SUPERVISOR_LIVE_QUEUE_FETCH_TIMER : ', $scope.liveQueueFetchTimer);
											} else if (item.name === 'SUPERVISOR_CAMPAIGN_FETCH_TIMER') {
												$scope.campaignFetchTimer = item.value;
												console.log('SUPERVISOR_CAMPAIGN_FETCH_TIMER : ', $scope.campaignFetchTimer);
											}
										});

										NoticeService.get({userId: user.id}).$promise.then(function(data) {
											$scope.notices = data;
											$scope.loading = false;
										});
										
										$scope.agentSearchOption = {selectedAgent:[]};
										$scope.joinCallStatus = false;
										$scope.supportRole = false;
										
										var secondstotime = function (seconds) {
											var sec_num = parseInt(seconds, 10); // don't forget the second param
										    var hours   = Math.floor(sec_num / 3600);
										    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
										    var seconds = sec_num - (hours * 3600) - (minutes * 60);

										    if (hours   < 10) {hours   = "0"+hours;}
										    if (minutes < 10) {minutes = "0"+minutes;}
										    if (seconds < 10) {seconds = "0"+seconds;}
										    var time    = hours+':'+minutes+':'+seconds;
										    return time;
										}
										
										//Get client time-zone
										var tz = jstz.determine();
										var timeZone = tz.name();// returns "America/Los Angeles"
										
										$scope.getTodayDates = function() {
											var today = Date.now();
											var lastday = today - 86400000;												
											$scope.startDate = $filter('date')(today, 'yyyy-MM-dd');
											$scope.endDate = $filter('date')(today, 'yyyy-MM-dd');
											$scope.totalDays= 0;
										}
										
										$scope.getTodayDates();
										$scope.filterBy = "Today";
										$scope.getPerformance = function(time) {
											for(var i = 0 ; i <  $scope.campaignsList.length ; i++){
												// Date:10/10/2017	Reason:Added to handle console error when user clicks on Today/Weekly/Monthly before loading records
												if ($scope.campaignsList[i].metrics == undefined || $scope.campaignsList[i].metrics == null) {
													return;
												}
												$scope.campaignsList[i].emailBounceCount = null; 
												$scope.campaignsList[i].metrics.AVG_QA_SCORE = null; 
												$scope.campaignsList[i].metrics.PERCENT_PROSPECTS_DELIVERED = null; 
												$scope.campaignsList[i].metrics.COUNT_PROSPECTS_DELIVERED = null; 
											}
											var curr = new Date; // get current date
											if (time == "Today") {
												$scope.getTodayDates();
												$scope.filterBy = "Today";
												$scope.totalDays= 0;
											}
											if (time == "Week") {
												$scope.filterBy = "Week";
												var currDate = curr.getDate();
												var first = currDate - 7 // First day is the day of the month - the day of the week
												var firstday = new Date(curr.setDate(first));
												var lastday = Date.now();
																								
												$scope.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
												$scope.endDate = $filter('date')(lastday, 'yyyy-MM-dd');
												$scope.totalDays= 7;
											}
											if (time == "Month") {
												$scope.filterBy = "Month";
												var currDate = curr.getDate();
												var first = currDate - 30 // First day is the day of the month - the day of the week
												var firstday = new Date(curr.setDate(first));
												var lastday = Date.now();
																								
												$scope.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
												$scope.endDate = $filter('date')(lastday, 'yyyy-MM-dd');
												$scope.totalDays= 30;
											}
											if (time == "All") {
												$scope.filterBy = "All";
												$scope.startDate = null;
												$scope.endDate = null;
												$scope.totalDays= 365;
											}
											$scope.getCampaignMetrics();
//											$scope.getEmailBounce();
										}

										$scope.downloadCampaignReport = function() {
											// window.location.href = "/spr/supervisor/campaignReport";
											CampaignReportService.query().$promise.then(function(data) {
												var message = '';
												if(data != undefined && data != null) {
													for(i = 0; i < 7; i++) {
														message = message + data[i];
													}
												}
												if(message == 'Success') {
													NotificationService.log('success','Campaign Report Download Request Accepted. Please Check Your Email or Notification After Sometime.');
												} else {
													NotificationService.log('error','Already a process is running to download stats, Try after sometime.');
												}
											});
										}

										$scope.downloadAgentReport = function() {
											// window.location.href = "/spr/supervisor/agentReport";
											AgentReportService.query().$promise.then(function(data) {
												var message = '';
												if(data != undefined && data != null) {
													for(i = 0; i < 7; i++) {
														message = message + data[i];
													}
												}
												if(message == 'Success') {
													NotificationService.log('success','Agent Report Download Request Accepted. Please Check Your Email or Notification After Sometime.');
												} else {
													NotificationService.log('error','Already a process is running to download stats, Try after sometime.');
												}
											});
										}

										$scope.refreshStats = function(tab) {
											switch (tab) {
												case 'CAMPAIGN':
													$scope.getCampaignMetrics();
													if ($scope.loggedInUserOrganization.organizationLevel != undefined && $scope.loggedInUserOrganization.organizationLevel != "PARTNER") {
														$scope.getCampaignCallableRecord();
													}
													break;
												case 'AGENT':
													$scope.getAgentList();
													break;
											}
										}

										$scope.openConfirmActionCampaignModal = function(actionVal, campaignId) {
											$scope.confirmCampActModalInstance = $modal.open({
												scope : $scope,
												templateUrl : 'confirmActionCampaignModal.html',
												controller : ConfirmActionCampaignModalInstanceCtrl,
												windowClass : 'confirm-Purchase-modal',
												resolve: {
													actionAttr: function(){
														return actionVal;
													},
													actionCampaignId: function(){
														return campaignId;
													}
												}
											});
											if(actionVal == 'Complete') {
												 $scope.classes =  "Are you sure you want to mark the campaign as Completed";
												 $scope.detailMsg = "This disables the campaign and so restarting this will require a new campaign setup.";
											}
											if(actionVal == 'RunReady') {
												$scope.classes =  "Are you sure you want to mark the campaign as Run Ready";
												$scope.detailMsg = "Once the campaign is marked Running it will be assigned to the Campaign Manager to initiate the campaign. All essential campaign configurations must be in place before you do this.";
											}
											if(actionVal == 'Running') {
												$scope.classes =  "Are you sure you want to mark the campaign as Running";
												$scope.detailMsg = "All essential campaign configurations must be in place before you do this.";
											}
											if (actionVal == 'Pause') {
												$scope.classes =  "Are you sure you want to mark the campaign as Paused";
												$scope.detailMsg = "All essential campaign configurations must be in place before you do this.";
											}
										};
										
										var ConfirmActionCampaignModalInstanceCtrl = function ($scope, actionAttr, actionCampaignId) {
											$scope.confirmActionForCampaign = function() {	
												$scope.campaignActionControl(actionAttr,actionCampaignId);
												$scope.confirmCampActModalInstance.close();
											};
											
											$scope.cancel = function() {
												$scope.confirmCampActModalInstance.close();
											}
										}
													
										
										// this method is used when user click on RunReady and Complete button
	                                      	$scope.campaignActionControl = function(actionAttribute,campaignId){
	                                      		$scope.loading = true;
	                                      		$scope.commanddata	={
	                          	                     "campaignCommand":actionAttribute
	                          	                 };
	                            				CampaignCommandService.query({
	                            	                 id: campaignId
	                            	             }, $scope.commanddata).$promise.then(function(data) {
	                            	            	 $scope.loading = false;
	                            	            	if (actionAttribute ==  'Complete') {
	                            	            		NotificationService.log('success','Campaign Completed successfully.');
	                            	            	} if(actionAttribute ==  'RunReady') {
	                            	            		NotificationService.log('success','Campaign status successfully changed to RunReady.');
	                            	            	} if(actionAttribute ==  'Running') {
	                            	            		NotificationService.log('success','Campaign status successfully changed to Running.');
	                            	            	} if(actionAttribute ==  'Pause') {
	                            	            		NotificationService.log('success','Campaign status successfully changed to Paused.');
	                            	            	}
	                            	            	/* $scope.getCampaignListConfigTab(); */	// used to get new list of campaigns from DB
													//$scope.removePausedCampaignFromList(campaignId);
													$scope.getCampaignList();
	                            	             },
	                            	             function(error) {
                                 				    console.log(JSON.stringify(error));
                                 					$scope.loading = false;
	                            	             });
											  }
											  
										$scope.removePausedCampaignFromList = function(campaignId) {
											angular.forEach($scope.campaignsList, function(value, index) {
												if (campaignId != undefined && campaignId != null) {
													if (value.campaignId == campaignId) {
														$scope.campaignsList.splice(index, 1);
													}
												}
											 });
										}	  
	                                      	
										$scope.isNumber = angular.isNumber;
										$scope.agentWithSupervisorCalls = [];
										
										$scope.getCampaignList = function() {
											$scope.loading = true;
											SupervisorCampaignService.get({
												searchstring : angular.toJson({
													"clause" : "BySupervisor",
													"sort" : "name",
													"direction" : "ASC",
													"page" : $scope.currentPage-1, 
													"size" : $scope.pageSize 
													// "flag" : "true"			// added to get campaigns with statuses as RUNNING & PAUSED 
												})
											}).$promise.then(function(data) {
												$scope.campaignsList = data.campaignsList;
												$scope.campaignCount = data.campaignCount;
												$scope.campaignCountConfigTab = data.campaignCount;
												$scope.campaignLength =  $scope.campaignsList.length;
												$scope.showDataUpload = data.prospectUploadEnabled;
												/* $scope.campaignCount =  data.campaignCount;
												$scope.campaignLength =  $scope.campaignsList.length;
												console.log(data);
												console.log($scope.campaignsList.length);
												$scope.noSize = false;
												if ($scope.campaignLength < 10 ) {
													  $scope.noSize = true;
												  } */
												$scope.loading = false;
												$scope.getCampaignMetrics();
												if ($scope.loggedInUserOrganization.organizationLevel != undefined && $scope.loggedInUserOrganization.organizationLevel != "PARTNER") {
													$scope.getCampaignCallableRecord();
												}
//												$scope.getEmailBounce();
											}, function(error) {
												$scope.loading = false;
											});
										}
										
										/* START
										 * DATE : 19/04/2017
										 * REASON : Added to get campaigns list for "Configuration" Tab.
										 * 			This method is also used when user clicks on "RunReady" and "Complete" Button*/
										/*$scope.getCampaignListConfigTab = function() {
											$scope.currentPageConfigTab = 1;
											$scope.loading = true;
											campaignListService.get({
												searchstring : angular.toJson({
													"clause" : "BySupervisor",
													"sort" : "name",
													"direction" : "ASC",
													"page" : $scope.currentPageConfigTab-1, 
													"size" : $scope.pageSizeConfigTab, 
												})
											}).$promise.then(function(data) {
												$scope.campaignsList = data.campaignsList;
												$scope.campaignCountConfigTab =  data.campaignCount;
												$scope.campaignLength =  $scope.campaignsList.length;
												console.log(data);
												console.log($scope.campaignsList.length);
												$scope.noSizeConfigTab = false;
												if ($scope.campaignLength < 10 ) {
													  $scope.noSizeConfigTab = true;
												  }
												$scope.loading = false;
											}, function(error) {
												$scope.loading = false;
											});
										}*/
										/* END */
										
										$scope.getCampaignMetrics = function () {
											$scope.disableLoadStatsButton = true;
											if ($scope.campaignsList != undefined && $scope.campaignsList != null && $scope.campaignsList.length > 0) {
												var campaignCounter =  $scope.campaignsList.length;
												angular.forEach($scope.campaignsList,function(item,key) {
													$scope.campaignTimezone = item.timeZone;
													if(item.timeZone == undefined || item.timeZone == "") {
														$scope.campaignTimezone = timeZone;
													}
													campaignListMetricsService.query({
																searchstring : angular.toJson({
																	"clause" : "ByCampaign",
																	"campaignId" : item.campaignId,
																	"timeZone" : $scope.campaignTimezone,
																	"noOfDays" :$scope.totalDays,
																	"fromDate" : $scope.startDate,
																	"toDate" : $scope.endDate
																})
													},null).$promise.then(function(data1) {
														// console.log(data1);
														$scope.campaignsList[key].metrics = data1.metrics;
														if (data1.metrics.COUNT_PROSPECTS_DIALED != "0" && data1.metrics.COUNT_PROSPECTS_DIALED != undefined) {
															$scope.campaignsList[key].metrics.PERCENT_PROSPECTS_CONTACTED = parseFloat(data1.metrics.COUNT_PROSPECTS_CONTACTED/data1.metrics.COUNT_PROSPECTS_DIALED)*100;
														} else {
															$scope.campaignsList[key].metrics.PERCENT_PROSPECTS_CONTACTED = 0;
														}
														if (data1.metrics.COUNT_PROSPECTS_CONTACTED != "0" && data1.metrics.COUNT_PROSPECTS_CONTACTED != undefined) {
															$scope.campaignsList[key].metrics.PERCENT_PROSPECTS_DELIVERED = parseFloat(data1.metrics.COUNT_PROSPECTS_DELIVERED/data1.metrics.COUNT_PROSPECTS_CONTACTED)*100;
														} else {
															$scope.campaignsList[key].metrics.PERCENT_PROSPECTS_DELIVERED = 0;
														}
														campaignCounter-- ;
														if (campaignCounter == 0) {
															$scope.disableLoadStatsButton = false;
														}
													},function(error) {
														
													});
												});
											}
										} 
										
										$scope.getEmailBounce = function () {
											angular.forEach($scope.campaignsList,function(item,key) {
												$scope.campaignTimezone = item.timeZone;
												if(item.timeZone == undefined){
													$scope.campaignTimezone = timeZone
												}
												emailBouncesService.query({
															searchstring : angular.toJson({
																"clause" : "ByCampaign",
																"campaignId" : item.id,
																"timeZone" : $scope.campaignTimezone,
																"noOfDays" :$scope.totalDays,
																"fromDate" : $scope.startDate,
																"toDate" : $scope.endDate
															})
												},null).$promise.then(function(data) {
													// console.log(data);
													if (data.EMAIL_BOUNCE_COUNT != 0 && data.EMAIL_BOUNCE_COUNT != undefined) {
														 $scope.campaignsList[key].emailBounceCount = data.EMAIL_BOUNCE_COUNT;
													} else {
														$scope.campaignsList[key].emailBounceCount = 0;
													}
													$scope.loading = false;
												},function(error) {
													 console.log(JSON.stringify(error));   
												});
											});
										} 
										
										$scope.getCampaignCallableRecord = function () {
											angular.forEach($scope.campaignsList,function(item,key) {
												campaignCallableRecordService.query({
													campaignid: item.campaignId
												}).$promise.then(function(data) {
													// console.log(data);
													if (data.CALLABLE_COUNT != undefined) {
													    $scope.campaignsList[key].callableCount = data.CALLABLE_COUNT;
													} else {
														$scope.campaignsList[key].callableCount = 0;
													}
												},function(error) {
													 console.log(JSON.stringify(error));   
												});
											});
										} 
										
										//Popups after session expired
										$rootScope.$on("SessionToBeTimeOut",function () {
											$scope.configurationModalInstance.close();
											$interval.cancel(timerstart);
										});
										$rootScope.$on("SessionTimeOut",function () {
											$scope.configurationModalInstance.close();
											$interval.cancel(timerstart);
										});
										
										
										$scope.getAgentList = function() {
											$scope.loading = true;
											agentListService.query({
												searchstring : angular.toJson({
													"clause" : "BySupervisor",
													"timeZone" : timeZone
												})
											}, null).$promise.then(function(data) {
												$scope.agentList = data;
												$scope.generateAgentListForComponent();
												$scope.checkIsAnyAgentNeedsApproval();
												$scope.$broadcast('timer-start');
												angular.forEach($scope.campaignsList,function(item,key) {
													$scope.$broadcast('timer-add-cd-seconds', 5);
												});
												$scope.timerRunning = true;
												$scope.loading = false;
											}, function(error) {
												$scope.loading = false;
											});
										}

										$scope.checkIsAnyAgentNeedsApproval = function () {
											if ($scope.agentList != null && $scope.agentList != undefined && $scope.agentList.length > 0) {
												var index = $scope.agentList.findIndex(agent => agent.agentStatusChangeApprove == true && (agent.agentStatus == 'REQUEST_OTHER' || agent.agentStatus ==  'REQUEST_MEETING'  || agent.agentStatus == 'REQUEST_BREAK'  || agent.agentStatus == 'REQUEST_OFFLINE' || agent.agentStatus == 'REQUEST_TRAINING'));
												if (index != -1) {
													var tempAgentList = [];
													angular.forEach($scope.agentList, function(agent, key) {
														if (agent.agentStatusChangeApprove && (agent.agentStatus == 'REQUEST_OTHER' || agent.agentStatus ==  'REQUEST_MEETING'  || agent.agentStatus == 'REQUEST_BREAK'  || agent.agentStatus == 'REQUEST_OFFLINE' || agent.agentStatus == 'REQUEST_TRAINING')) {
															tempAgentList.push(agent);
														}
													});
													angular.forEach($scope.agentList, function(agent, key) {
														var index = tempAgentList.findIndex(y => y.agentId == agent.agentId);
														if (index == -1) {
															tempAgentList.push(agent);
														}
													});											
													$scope.agentList = tempAgentList;
												}
											}
										}
										$scope.joinCallStatus = true;
										
										$scope.getLiveQueueList = function() {
											agentMtricsService.query({
												searchstring : angular.toJson({
													"clause" : "BySupervisorLiveQueue"
												})
											}, null).$promise.then(function(data) {
												$scope.allLiveQueueList = data;
												$log.debug("Received a join_call_invitation");
								          		//$log.debug(agentList);
								          		for (var i = 0; i < $scope.allLiveQueueList.length; i++) {
								          			if ($scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_CONNECTED" || 
								          					$scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_ONMUTE" || $scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_UNMUTE"||
								          					$scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_ONHOLD" || $scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_UNHOLD") {
								          				$scope.allLiveQueueList[i].inviteStatus = true;
									          	    	$scope.allLiveQueueList[i].acceptCallInMute = true;
									          	        //$scope.joinCallStatus = true;
									          	        
									          	        angular.forEach($scope.agentWithSupervisorCalls, function(value, index) {
															 if(value.agentId == $scope.allLiveQueueList[i].agentId) {
																 $scope.allLiveQueueList[i].incall = true;
															 } else {
																 $scope.allLiveQueueList[i].incall = false;
															 }
									          	        });
									          	    } else if ($scope.allLiveQueueList[i].metrics.AGENT_CALL_STATUS == "CALL_DISCONNECTED") {
									          	    	angular.forEach($scope.agentWithSupervisorCalls, function(value, index) {
															 if(value.agentId == $scope.allLiveQueueList[i].agentId) {
																 $scope.allLiveQueueList[i].incall = false;
																 $scope.endCall($scope.allLiveQueueList[i].agentId);
															 }
									          	        });
									          	    }
												  }
											}, function(error) {
												
											});
										}
										
										$scope.connectCall = function(agentId,prospectCallId, prospectInteractionSessionId) {
											if($scope.joinCallStatus){
						          				twilioTokenService.getToken().then(function(token) {
							          				twilioService.connect(token, {pn: agentId, supervisorjoin:true, pcid : prospectCallId, pisid : prospectInteractionSessionId}).then(function(parameters) {
							              				$log.debug("Successfully connected with call sid:" + parameters.CallSid);
							              				$scope.joinCallStatus = false;
							              				//$log.debug("Notifying the timer and calling time");
							              				for (var i = 0; i < $scope.allLiveQueueList.length; i++) {
							              					if ($scope.allLiveQueueList[i].agentId === agentId) {
							              						$scope.allLiveQueueList[i].incall = true;
							              						
							              						$scope.conferenceInfo = {
							              								agentId:$scope.allLiveQueueList[i].agentId,
							              								supervisorInCall: true
							              						}
							              						
							              						$scope.agentAvailable = $filter('filter')($scope.agentWithSupervisorCalls, $scope.allLiveQueueList[i].agentId)[0];
							              						if ($scope.agentAvailable == undefined) {
							              							$scope.agentWithSupervisorCalls.push($scope.conferenceInfo);
							              						}
							              						if ($scope.allLiveQueueList[i].acceptCallInMute == true) {
							              							twilioService.toggleMute();
							              						}
							              						break;
							              					}
							              				}
							          				}, function(error) {
							              				$log.debug("Error occured while making a call");
							              				$log.debug(error);
							              			});
							          			});
						          			} else {
						          				NotificationService.log('error','Call already in progress.');
						          			}
										}

										$scope.endCall = function(agentId) {
											twilioService.disconnect();
											$scope.joinCallStatus = true;
											for (var i = 0; i < $scope.allLiveQueueList.length; i++) {
												if ( $scope.allLiveQueueList[i].agentId === agentId) {
													 $scope.allLiveQueueList[i].incall = false;
													 $scope.allLiveQueueList[i].inviteStatus = false;
													 angular.forEach($scope.agentWithSupervisorCalls, function(value, index) {
														 if(value.agentId == $scope.allLiveQueueList[i].agentId) {
															 $scope.agentWithSupervisorCalls.splice(index, 1 );
														 }
													 });
													 break;
												}
											}
										}
										
										$scope.onTimeout = function(){											
											angular.forEach($scope.agentList, function (item,key) {
											    if (item.metrics != null && item.metrics.LAST_STATUS_DURATION != null) {
											    	item.metrics.LAST_STATUS_DURATION = parseInt(parseInt(item.metrics.LAST_STATUS_DURATION) + 1);
											    	item.metrics.TIMER = secondstotime(item.metrics.LAST_STATUS_DURATION);
											    }
											});
											angular.forEach($scope.allLiveQueueList, function (item,key) {
											    if (item.metrics != null) {
											    	if (item.metrics.AGENT_CALL_STATUS_DURATION != null) {
											    		item.metrics.AGENT_CALL_STATUS_DURATION = parseInt(parseInt(item.metrics.AGENT_CALL_STATUS_DURATION) + 1);
											    		item.metrics.LAST_CALL_STATUS_TIMER = secondstotime(item.metrics.AGENT_CALL_STATUS_DURATION);
											    	}
											    	if (item.metrics.AGENT_LEAD_DURATION != null) {
											    		item.metrics.AGENT_LEAD_DURATION = parseInt(parseInt(item.metrics.AGENT_LEAD_DURATION) + 1);
											    		item.metrics.LEAD_TIMER = secondstotime(item.metrics.AGENT_LEAD_DURATION);
											    	}
											    }
											});
									        mytimeout = $timeout($scope.onTimeout,1000);
									    }
									    var mytimeout = $timeout($scope.onTimeout,1000);
									    
									    $scope.stop = function(){
									        $timeout.cancel(mytimeout);
									    }
									    
									    $scope.timerRunning = false;
										$scope.campaignsList = [];
										$scope.agentList = [];
										$scope.allCampaignList = [];
										$scope.allLiveQueueList = [];
										$scope.getCampaignList();
										/* START
										 * DATE : 24/04/2017
										 * REASON : Added to handle cache issue & get campaigns list */
										/* if ($localStorage.selectedTab == undefined || $localStorage.selectedTab == "") {
											$scope.getCampaignList();
										} else if ($localStorage.selectedTab == '"configurations"') {
											$scope.getCampaignListConfigTab();
										} */
										/* END */
										$scope.getCampaignMetrics();
										if ($scope.loggedInUserOrganization.organizationLevel != undefined && $scope.loggedInUserOrganization.organizationLevel != "PARTNER") {
											$scope.getCampaignCallableRecord();
										}
										// $scope.getEmailBounce();
										$scope.getAgentList();
										$scope.getLiveQueueList();
										$scope.selectedtabValue  ="";
										
										$scope.getSelectedtabVal = function(val){
											$scope.selectedtabValue = val;
											/* if (val == 'configurations') {
												$scope.getCampaignListConfigTab();
											} else if (val == 'campaigns') {
												$scope.getCampaignList();
											} */
										}
										
// 										var timerstart =   $interval(function() {
// 											if($scope.selectedtabValue == 'campaigns'){
// 												$scope.getCampaignMetrics();
// 											    $scope.getCampaignCallableRecord();
// //											    $scope.getEmailBounce();
// 											}
// 											if($scope.selectedtabValue == 'agents'){
// 												 $scope.getAgentList();
// 											}
// 											//$scope.getLiveQueueList();
// 										}, $scope.campaignFetchTimer);
										
										var liveQueueTimerStart =   $interval(function() {
											if($scope.selectedtabValue == 'livequeue'){
												$scope.getLiveQueueList();
											}
										}, $scope.liveQueueFetchTimer);
										
										$scope.$on('$destroy', function ()  {
											// $interval.cancel(timerstart);
											$interval.cancel(liveQueueTimerStart);
										});
										
										$scope.waitMsg = false;
										$scope.campaignContact = {
												progress:undefined
										}
										
										$scope.waitPurchaseMsg = false;
										$scope.purchaseContact = {
												progress:undefined
										}
										
										$scope.sections = [];
										
										var pushRebuttals = function (key, value) {
											angular.forEach(value,function(rebuttal,index) {
												rebuttal.section = key
												$scope.rebuttals.push (rebuttal)
											});
										}
										
										$scope.DefaultsetTabClass = "active";
										$scope.setTabClass = "";
										$scope.selectedTab = "campaigns";
										$scope.selectedtabValue = "campaigns";
										
										/*if($localStorage.setTabClass){											
											$scope.setTabClass = $localStorage.setTabClass;
											$scope.DefaultsetTabClass = ""; 
										}*/
										if ($localStorage.selectedTab != undefined && $localStorage.selectedTab != "") {
								    		$scope.selectedTab = JSON.parse($localStorage.selectedTab);
								    	}
										
										
										var constructSectionRebuttalArray = function() {
											$scope.sections = [];
											if ($scope.callGuideData.questionSection != null && $scope.callGuideData.questionSection.rebuttals != null && 
													$scope.callGuideData.questionSection.rebuttals.length != 0) {
												$scope.section = {};
												$scope.section.name = "Question";
												$scope.section.rebuttals = $scope.callGuideData.questionSection.rebuttals;
												$scope.sections.push($scope.section);
											}
											if ($scope.callGuideData.confirmSection != null && $scope.callGuideData.confirmSection.rebuttals != null && 
													$scope.callGuideData.confirmSection.rebuttals.length != 0) {
												$scope.section = {};
												$scope.section.name = "Confirm";
												$scope.section.rebuttals = $scope.callGuideData.confirmSection.rebuttals;
												$scope.sections.push($scope.section);
											}
											if ($scope.callGuideData.consentSection != null && $scope.callGuideData.consentSection.rebuttals != null && 
													$scope.callGuideData.consentSection.rebuttals.length != 0) {
												$scope.section = {};
												$scope.section.name = "Consent";
												$scope.section.rebuttals = $scope.callGuideData.consentSection.rebuttals;
												$scope.sections.push($scope.section);
											}
											if ($scope.callGuideData.noteSection != null && $scope.callGuideData.noteSection.rebuttals != null && 
													$scope.callGuideData.noteSection.rebuttals.length != 0) {
												$scope.section = {};
												$scope.section.name = "Note";
												$scope.section.rebuttals = $scope.callGuideData.noteSection.rebuttals;
												$scope.sections.push($scope.section);
											}
										} 
										
										var getCallGuide = function(campaignId) {
											CallGuideService.query({id : campaignId}, null).$promise.then(function(callGuideData) {
												$scope.callGuideData = callGuideData;
												$scope.confirmSectionScript = callGuideData.confirmSection != null ? callGuideData.confirmSection.script : null;
												$scope.questionSectionScript = callGuideData.questionSection != null ? callGuideData.questionSection.script : null;
												$scope.consentSectionScript = callGuideData.consentSection != null ? callGuideData.consentSection.script : null;
												$scope.noteSectionScript = callGuideData.noteSection != null ? callGuideData.noteSection.script : null;
												constructSectionRebuttalArray();
												
												
											},function () {
												$scope.loading = false;
											});
										}
										
										var loadScript = function (campaignId, tab, value) {
											/*getCallGuide(campaignId);
											if (tab == "Script" || tab == "Rebuttals") {
												$scope.loading = false;
												$scope.showconfigModal(campaignId, tab);
											}*/
											
											/* START
											 * DATE : 19/04/2017
											 * REASON : Added to resolve script override issue on supervisor screen when supervisor switches among different campaign scripts */
											CallGuideService.query({id : campaignId}, null).$promise.then(function(callGuideData) {
												$scope.callGuideData = callGuideData;
												$scope.confirmSectionScript = callGuideData.confirmSection != null ? callGuideData.confirmSection.script : null;
												$scope.questionSectionScript = callGuideData.questionSection != null ? callGuideData.questionSection.script : null;
												$scope.consentSectionScript = callGuideData.consentSection != null ? callGuideData.consentSection.script : null;
												$scope.noteSectionScript = callGuideData.noteSection != null ? callGuideData.noteSection.script : null;
												constructSectionRebuttalArray();
												if ((tab == "Script" || tab == "Rebuttals") && (value == undefined && value != "ClickedFromPopUp")) {
													$scope.loading = false;
													$scope.showconfigModal(campaignId, tab);
												}
											},function () {
												$scope.loading = false;
											});
										}
										var loadDataSource = function (campaignId, tab, value) {
											$scope.pivotReview = false;
											$scope.searchstring = {};
											$scope.modal.expressionAttributes = [];
											$scope.showFileUpload = 1;
											$scope.modal.prospectCriteria = {
												groupClause : "AND",
												expressions : []
											};
											if(user.roles[1] == 'SUPPORT'){
												$scope.showFileUpload = 1;
												$scope.supportRole = true;
											}
											$scope.searchstring.prospectattribute = {
													"dataProvider" : "NetProspexDataProvider",
													"expressionGroup" : {
														"groupClause" : "AND",
														"expressions" : [ {
															"attribute" : "mgmtLevel",
															"operator" : "IN",
															"value" : "C, VP"
														} ]
													},
													"size" : 1
											};
											ProspectAttributeService.query({
															searchstring : JSON.stringify($scope.searchstring.prospectattribute)
											}).$promise.then(function(ProspectData) {
												$scope.modal.prospectCriteria = {
														groupClause : "AND",
														expressions : []
												};
												$scope.databuycampaign = {};
												$scope.modal.prospectAttribute = ProspectData;
												$scope.campaign  = $scope.campaignsList.filter(campaign => campaign.campaignId == campaignId);
												$scope.campaignName1 = $scope.campaign[0].name;
												$scope.getABMListCount(campaignId);
												$scope.getSuppressionList(campaignId);
												$scope.loadDbq(campaignId);
												$scope.checkDatabuyProcess(campaignId);
												$scope.bindCampaignCriteriaInEdit($scope.campaign[0]);
												$scope.checkDatabuyStatus(campaignId);

												$scope.modal.prospectSearchResult = [];
												$scope.modal.prospectSearchResultView = false;
												if (tab == 'Data List' && (value == undefined && value != "ClickedFromPopUp"))  {
													$scope.showconfigModal(campaignId, tab);
												}
											},function () {
												$scope.loading = false;
											});
												
											$scope.addExpression = function() {
												$scope.listOfCriteria.push({});
											};
										}
										if(user.roles[1] == 'SUPPORT'){
											$scope.showFileUpload = 1;
											$scope.supportRole = true;
										}
										$scope.isAgentLoaded = false;
										$scope.isAgentTabClicked = false;

										$scope.getAttributePickList = function(attributeName) {
											$scope.pickData = [];
											if (attributeName == "Title" || attributeName == "title" || attributeName == "MANAGEMENT_LEVEL") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['MANAGEMENT_LEVEL'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "INDUSTRY" || attributeName == "industry") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['INDUSTRY'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "DEPARTMENT" || attributeName == "department") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['DEPARTMENT'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "VALID_COUNTRY" || attributeName == "VALID_COUNTRY") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['VALID_COUNTRY'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "EMPLOYEE" || attributeName == "EMPLOYEE") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['EMPLOYEE'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "REVENUE" || attributeName == "REVENUE") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['REVENUE'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "SIC_CODE" || attributeName == "SIC_CODE") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['SIC_CODE'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "NAICS_CODE" || attributeName == "NAICS_CODE") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['NAICS_CODE'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "STATE (US ONLY)" || attributeName == "STATE (US ONLY)") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['STATE (US ONLY)'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											  if (attributeName == "LAST_UPDATED_DATE" || attributeName == "LAST_UPDATED_DATE") {
												$scope.isShowOptions = true;
												angular.forEach($scope.attributeMappings['LAST_UPDATED_DATE'].pickList, function(v, k) {
														$scope.pickData[k]=v;		
												});
											  }
											return $scope.pickData;
										};

										$scope.sortPickList = function() {
											if ($scope.listOfCriteria != null && $scope.listOfCriteria != undefined) {
												angular.forEach($scope.listOfCriteria, function(value, key) {
													if (value.pickList != null && value.pickList != undefined) {
														value.pickList.sort(function(a, b) {
															if (a.label.toLowerCase() < b.label.toLowerCase()) return -1;
															if (a.label.toLowerCase() > b.label.toLowerCase()) return 1;
															return 0;
														});
													}
												});
										    }
										}

										$scope.sortGivenPickList = function(unSortedList) {
											if (unSortedList != null && unSortedList != undefined) {
												unSortedList.sort(function(a, b) {
													if (a.label.toLowerCase() < b.label.toLowerCase()) return -1;
													if (a.label.toLowerCase() > b.label.toLowerCase()) return 1;
													return 0;
												});
												return unSortedList;
										    }
										}

										$scope.getPickList = function(attributeName) {
											
												$scope.listOfPick =  [];
												
												if (attributeName == "INDUSTRY" || attributeName == "industry") {
													$scope.listOfPick.push($scope.attributeMappings['INDUSTRY'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "INDUSTRY" || listElement.attribute == "industry") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "DEPARTMENT" || attributeName == "department") {
													$scope.listOfPick.push($scope.attributeMappings['DEPARTMENT'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "DEPARTMENT" || listElement.attribute == "department") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "Title" || attributeName == "MANAGEMENT_LEVEL" || attributeName == "title") {
													$scope.listOfPick.push($scope.attributeMappings['MANAGEMENT_LEVEL'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "Title" || listElement.attribute == "MANAGEMENT_LEVEL" || attributeName == "title") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "VALID_COUNTRY") {
													$scope.listOfPick.push($scope.attributeMappings['VALID_COUNTRY'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "VALID_COUNTRY") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "EMPLOYEE") {
													$scope.listOfPick.push($scope.attributeMappings['EMPLOYEE'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "EMPLOYEE") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													//$scope.sortPickList();
												}
												if (attributeName == "REVENUE") {
													$scope.listOfPick.push($scope.attributeMappings['REVENUE'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "REVENUE") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													//$scope.sortPickList();
												}
												if (attributeName == "SIC_CODE") {
													$scope.listOfPick.push($scope.attributeMappings['SIC_CODE'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "SIC_CODE") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "NAICS_CODE") {
													$scope.listOfPick.push($scope.attributeMappings['NAICS_CODE'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "NAICS_CODE") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "STATE (US ONLY)") {
													$scope.listOfPick.push($scope.attributeMappings['STATE (US ONLY)'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "STATE (US ONLY)") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
												if (attributeName == "LAST_UPDATED_DATE") {
													$scope.listOfPick.push($scope.attributeMappings['LAST_UPDATED_DATE'].pickList);
													$scope.listOfCriteria.forEach(listElement => {
														if(listElement.attribute == "LAST_UPDATED_DATE") {
															listElement.pickList = $scope.listOfPick[0];
														}
													});
													$scope.sortPickList();
												}
										};



										//  <!-- HEALTH CHECK SCREEN ON DATA LIST POPUP START -->
										$scope.bindCampaignCriteriaInEdit = function(campaign) {
											if ($scope.listOfCriteria.length > 0) {
												$scope.listOfCriteria = [];
											}
											
											angular.forEach(campaign.qualificationCriteria.expressions, function(criteriaValue, key) {
												if (criteriaValue.attribute == "INDUSTRY") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "DEPARTMENT") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "VALID_COUNTRY") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "SIC_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "NAICS_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "STATE (US ONLY)") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "LAST_UPDATED_DATE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "ZIP_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "VALID_STATE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "SEARCH_TITLE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "NEGATIVE_TITLE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "MANAGEMENT_LEVEL") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "EMPLOYEE") {
														$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : criteriaValue.pickList
													});
												} else if (criteriaValue.attribute == "REVENUE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : criteriaValue.pickList
													});
												} 
											});
										}



										$scope.bindDbqCampaignCriteriaInEdit = function(databuycampaign) {
                                                if ($scope.listOfCriteria.length > 0) {
                                                    $scope.listOfCriteria = [];
                                                }

                                                angular.forEach(databuycampaign.expressions, function(criteriaValue, key) {
                                                    if (criteriaValue.attribute == "INDUSTRY") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    } else if (criteriaValue.attribute == "DEPARTMENT") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    } else if (criteriaValue.attribute == "VALID_COUNTRY") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    } else if (criteriaValue.attribute == "SIC_CODE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    }else if (criteriaValue.attribute == "NAICS_CODE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    }else if (criteriaValue.attribute == "STATE (US ONLY)") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    }else if (criteriaValue.attribute == "LAST_UPDATED_DATE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    }else if (criteriaValue.attribute == "ZIP_CODE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : criteriaValue.value
                                                        });
                                                    } else if (criteriaValue.attribute == "VALID_STATE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : criteriaValue.value
                                                        });
                                                    } else if (criteriaValue.attribute == "SEARCH_TITLE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : criteriaValue.value
                                                        });
                                                    } else if (criteriaValue.attribute == "NEGATIVE_TITLE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : criteriaValue.value
                                                        });
                                                    } else if (criteriaValue.attribute == "MANAGEMENT_LEVEL") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
                                                        });
                                                    } else if (criteriaValue.attribute == "EMPLOYEE") {
                                                            $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : criteriaValue.pickList
                                                        });
                                                    } else if (criteriaValue.attribute == "REVENUE") {
                                                        $scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
                                                            "operator" : criteriaValue.operator,
                                                            "value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
                                                            "pickList" : criteriaValue.pickList
                                                        });
                                                    }
                                                });
                                        }

										$scope.getPickListVal = function(pickList, selectedValues) {
											var pickListObjects = [];
											angular.forEach(pickList, function(pickListItem, pickListKey) {
													angular.forEach(selectedValues, function(value, key) {
														if (value == pickListItem.value) {
															pickListObjects.push(pickListItem);
														}
													});
											})
											
											return pickListObjects;
										}
										//  <!-- HEALTH CHECK SCREEN ON DATA LIST POPUP END -->
										/* Set default criteria for Audience selection tab */	                                      	  
// 										$scope.setDefaultSelectedCriteria = function(criteriaData) {
// 											$scope.selectedCriteria = [];	                                      		
// 											if(!criteriaData) {	                                      			 
// 											  if($scope.selectedCriteria.length == 0) {
// //	                                      			    DATE:14/12/2017	Do not show default selected Question ex.Department,Title
// 												angular.forEach($scope.attributeMappings, function(item, key) {
// 													if(item.systemName == 'EMPLOYEE' || item.systemName == 'VALID_COUNTRY' || item.systemName == 'DEPARTMENT' || item.systemName == 'MANAGEMENT_LEVEL' || item.systemName == 'INDUSTRY') {		                                    				  
// 															$scope['checkedRow' + item.systemName] = true;
// 															item.checked = true;	
// 														if (item.value == undefined) {
// 															item.value = [];
// 															angular.forEach(item.pickList, function(pickListItem, pickListKey) {
// 																if (item.systemName == 'VALID_COUNTRY') {
// 																	if (pickListItem.value == "USA") {
// 																		item.value.push(pickListItem);
// 																	}	
// 																} else {
// 																	if (pickListItem.value == "All") {
// 																		item.value.push(pickListItem);
// 																	}
// 																}
// 															});
// 														}
// 														item.operator = "IN";
// 														$scope.selectedCriteria.push(item);	
// 														$scope.exression = {
// 															attribute: item.systemName
// 														}
// 															$scope.fExpr.push($scope.exression); 		                                    				  
// 													}
// 												});  
// 											  }   
// 											} else {	                                      			
// 											  angular.forEach(criteriaData, function(item, key) {	                                      				
// 												  $scope['checkedRow' + item.attribute] = true;
												  
// 												  angular.forEach($scope.attributeMappings, function(mapItem, mapKey) {
													  
// 													  if(mapItem.systemName == item.attribute) {
														  
// 														  /*if (item.attribute == "Area" || item.attribute == "Country" || item.attribute == "State" || item.attribute == "City") {
// 															  $scope.geographyPresent = true;
// 														  }*/
// 														  if ($scope.isGeoAttrMap(item)) {
// 															  $scope.geographyPresent = true;
// 														  }
														  
// 														  mapItem.checked = true;	
// 														  mapItem.value = [];
// 														  angular.forEach(item.value, function(dbexprValue, dbexprKey) {
// 															  if(mapItem.pickList != undefined && mapItem.pickList.length > 0) {
// 																  if (mapItem.attributeType != XTAAS_CONSTANTS.attributeType.area && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.country && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.state && mapItem.attributeType != XTAAS_CONSTANTS.attributeType.city ) {
// 																	  $scope.pickListArr = [];
// 																	  angular.forEach(mapItem.pickList, function(pickListItem, pickListKey) {
// 																		  if (pickListItem.value == dbexprValue) {
// 																			  mapItem.value.push(pickListItem);
// 																		  }
// 																	  });
// 																  }
// 															  }
															  
// 															  if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area) {
// 																  angular.forEach(arealist, function(area, index) {
// 																	  if (area.value == dbexprValue) {
// 																		  mapItem.value.push(area);
// 																	  }
// 																  });
// 															  }
// 															  if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country) {
// 																  angular.forEach(countrylist, function(country, index) {
// 																	  if (country.value == dbexprValue) {
// 																		  mapItem.value.push(country);
// 																	  }
// 																  });
// 															  }
// 															  if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state) {
// 																  angular.forEach(statelist, function(state, index) {
// 																	  if (state.value == dbexprValue) {
// 																		  mapItem.value.push(state);
// 																	  }
// 																  });
// 															  }
															  
															  
// 														  });
														  
// 														  if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.city ) {
// 															  mapItem.target = mapItem.systemName;
// 															  mapItem.systemName = "Geography";
// 															  mapItem.label = "Geography";
// 															  mapItem.systemDataType = "popup";
// 															  mapItem.crmDataType = "popup";
// 															  mapItem.checked = true;
// 														  }
														  
// 														  if(mapItem.pickList != undefined && mapItem.pickList.length <= 0 && mapItem.systemName != "Geography") {
// 															  mapItem.value = item.value;
// 														  }	
														  
// 														  if(item != undefined) {
// 															  mapItem.operator = item.operator;	
// 														  }	  
// 														  $scope.selectedCriteria.push(mapItem);
// 													  }  
// 												  });	
// 											  });
// 											}
// 										  //Replace area, country, state, city with Geography 
// 										  angular.forEach($scope.attributeMappings, function(mapItem, mapKey) {
// 											  if (mapItem.attributeType == XTAAS_CONSTANTS.attributeType.area || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.country || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.state || mapItem.attributeType == XTAAS_CONSTANTS.attributeType.city ) {
// 												  if (!$scope.changedToGeography) {
// 													  if ($scope.geographyPresent) {
// 														  mapItem.checked = true;
// 													  }
// 													  $scope.changedToGeography = true;
// 													  mapItem.systemName = "Geography";
// 													  //mapItem.crmName = "Geography";
// 													  mapItem.label = "Geography";
// 													  mapItem.systemDataType = "popup";
// 													  mapItem.crmDataType = "popup";
// 													  mapItem.fromOrganization = false;
// 													  $scope.attributeMappings["Geography"] = $scope.attributeMappings[mapKey];
// 													  delete $scope.attributeMappings[mapKey];
// 													  //$scope['checkedRow' + mapItem.systemName] = true;
													  
// 												  } else {
// 													  delete $scope.attributeMappings[mapKey];
// 												  }
												  
// 											  }
// 										  });
										  
// 										  /*START	DATE:06/12/2017		REASON:Hack for crmmapping ("Define Further" in Audience selection)*/
// 										  $scope.attributeMappingsTemp = {};
// 										  angular.forEach($scope.attributeMappings, function(value, key) {
// 											  if (value.systemName == 'SIC_CODE' || value.systemName == 'REMOVE_INDUSTRIES' || value.systemName == 'VALID_STATE' || value.systemName == 'SEARCH_TITLE' || value.systemName == 'REMOVE_DEPT' || value.systemName == 'NEGATIVE_TITLE' ) {
// 												  value.fromOrganization = false;
// 											}	
// 											if(!value.fromOrganization) {
// 												$scope.attributeMappingsTemp[key] = value;
// 											}
// 											if(!value.fromOrganization) {
// 												$scope.attributeMappingsTemp[key] = value;
// 											}
// 										});
										  
// 										// angular.forEach($scope.campaign.qualificationCriteria.expressions, function(expressionValue, expressionKey) { 	                                    						
// 										// 	if (expressionValue.attribute == 'SEARCH_TITLE' || expressionValue.attribute == 'SIC_CODE' || expressionValue.attribute == 'VALID_STATE' || expressionValue.attribute == 'REMOVE_DEPT' || expressionValue.attribute == 'REMOVE_INDUSTRIES') {
// 										// 		expressionValue.value = expressionValue.value.toString();
// 										// 	}
// 										// });
// 										}  		                       

										// $scope.isGeoAttrMap = function(attribute) {
										// 	if ($scope.geoAttrMapping[attribute] == undefined){
										// 		return false;
										// 	}
										// 	return true;
										// }

										var loadAgentAllocation = function (campaignId, tab, value) {
											$scope.loading = true;
											$scope.showAgentAllocation = true;
											$scope.modal.currentCampaignId = campaignId;
											agentAllocationService.query().$promise.then(function(data) {
												$scope.loading = false;
												var localdata = data.agentAllocationMap;
												// Below code is for agents assigned to campaign should be shown in edit mode.
												var allocatedAgentsList = [];
												var flagUnallocated;
												$scope.modal.campaignAgent = [];
												angular.forEach($scope.campaignsList,function(v,k) {
													var flagCampign = false;
													angular.forEach(localdata,function(Value,Key) {
														if(Key == 'UNALLOCATED') {
															flagUnallocated = true;
														}
														if (v.campaignId === Key) {
															flagCampign = true;
															if (v.status == 'RUNNING') {
																$scope.modal.campaignAgent.push({
																	'CampaignId' : Key,
																	'CampaignName' : v.name,
																	'Agents' : Value,
																	'selectedAgents' : []
																});
															}
														}
													});
													if(flagCampign === false) {
														if (v.status == 'RUNNING') {
															$scope.modal.campaignAgent.push({
																'CampaignId' : v.campaignId,
																'CampaignName' : v.name,
																'Agents' : [],
																'selectedAgents' : []
															});
													 }
													}
												});
												if(flagUnallocated  == true) {
													$scope.modal.campaignAgent.push({
														'CampaignId' : "UNALLOCATED",
														'CampaignName' : "UNALLOCATED",
														'Agents' : localdata.UNALLOCATED,
														'selectedAgents' : []
													});
												} else {
													$scope.modal.campaignAgent.push({
														'CampaignId' : "UNALLOCATED",
														'CampaignName' : "UNALLOCATED",
														'Agents' : [],
														'selectedAgents' : []
													});
												}
												if (tab == "Agent" && (value == undefined && value != "ClickedFromPopUp")) {
													$scope.loading = false
													$scope.showconfigModal(campaignId, tab);
												}
												$scope.isAgentLoaded = true;
												if ($scope.isAgentTabClicked) {//if agent is loaded then hide the loader
													$scope.loading = false;
												}
												if (localdata != null && localdata != undefined) {
													angular.forEach($scope.modal.campaignAgent,function(v,k) {
														angular.forEach(localdata,function(value,key) {
															if (v.CampaignId === key) {
																angular.forEach(value, function(agentValue, agentKey) {
																	if (v.selectedAgents == undefined) {
																		v.selectedAgents = [];
																	}
																	let obj = {};
																	obj.label = agentValue.id;
																	obj.id = agentValue.id;
																	v.selectedAgents.push(obj);
																	if (v.assignedAgents == undefined) {
																		v.assignedAgents = [];
																	}
																	var agentWithStatus  = $scope.agentList.filter(ag => ag.agentId == agentValue.id);
																	if (agentWithStatus[0] != null && agentWithStatus[0] != undefined) {
																		let agent = {};
																		agent.currentStatus = agentWithStatus[0].agentStatus;
																		agent.name = agentWithStatus[0].agentName;
																		agent.id = agentWithStatus[0].agentId;
																		let index = v.assignedAgents.findIndex(agentInArray => agentInArray.name === agentWithStatus[0].agentId);
																		if (index == -1) {
																			v.assignedAgents.push(agent);
																		}
																	}
																})
															}
														});
													 });
												}
											},function () {
												$scope.loading = false;
											});
										}

										$scope.selectFromPopup = function(attribute) {
											if ($scope.isIOSignedDisable) return;
											   
											$scope.popupValuesModalInstance = $modal.open({
												scope : $scope,
												templateUrl: 'popupValuesModal.html',
												controller: PopValuesModalInstanceCtrl,
												windowClass : 'popupValuesClass',
												resolve: {
													attribute: function(){
														return attribute;
													},
													atrMappingforGeoOptions: function(){
														return $scope.geoAttrMapping;
													}
												}
											}); 
										}

										var loadOldAgentAllocation = function (campaignId, tab, value) {
											$scope.showAgentAllocation = true;
											$scope.modal.currentCampaignId = campaignId;
											var flagUnallocated = false;
											agentAllocationService.query().$promise.then(function(data) {
												var localdata = data.agentAllocationMap;
												$scope.modal.campaignAgent = [];
												angular.forEach($scope.campaignsList,function(v,k) {
													var flagCampign = false;
													angular.forEach(localdata,function(Value,Key) {
														if(Key == 'UNALLOCATED') {
															flagUnallocated = true;
														}
														if (v.campaignId === Key) {
															flagCampign = true;
															$scope.modal.campaignAgent.push({
																'CampaignId' : Key,
																'CampaignName' : v.name,
																'Agents' : Value
															});
														}
													});
													if(flagCampign === false) {
														$scope.modal.campaignAgent.push({
															'CampaignId' : v.campaignId,
															'CampaignName' : v.name,
															'Agents' : []
														});
													}
												});
												if(flagUnallocated  == true) {
													$scope.modal.campaignAgent.push({
														'CampaignId' : "UNALLOCATED",
														'CampaignName' : "UNALLOCATED",
														'Agents' : localdata.UNALLOCATED
													});
												} else {
													$scope.modal.campaignAgent.push({
														'CampaignId' : "UNALLOCATED",
														'CampaignName' : "UNALLOCATED",
														'Agents' : []
													});
												}
												if (tab == "Agent" && (value == undefined && value != "ClickedFromPopUp")) {
													$scope.loading = false
													$scope.showconfigModal(campaignId, tab);
												}
												$scope.isAgentLoaded = true;
												if ($scope.isAgentTabClicked) {//if agent is loaded then hide the loader
													$scope.loading = false;
												}
											},function () {
												$scope.loading = false;
											});
											
										}


										var loadNotification = function (campaignId, tab, value) {
											SupervisorNoticeService.get().$promise.then(function(data) {
												 $scope.supervisorNotices = data;
												 if (tab == "Notifications" && (value == undefined && value != "ClickedFromPopUp")) {
														$scope.loading = false
														$scope.showconfigModal(campaignId, tab);
													}
											});
										}
										var loadAsset = function (campaignId, tab, value) {
											$scope.campaign = $scope.campaignsList.filter(campaign => campaign.campaignId == campaignId);
											$scope.campaignType = $scope.campaign[0].type;
											$scope.modal.assetEmailFromEmail = '';
											$scope.modal.assetEmailFromName = '';
											$scope.modal.assetEmailReplyTo = '';
											$scope.modal.assetEmailSubject = '';
											$scope.modal.assetEmailMessage = '';
											$scope.modal.assetEmailUnSubscribeUrl = '';
											$scope.modal.assetEmailClickUrl = '';
											var tempAsset = [];
											$q.all([$http.get(`/spr/rest/campaign/${campaignId}`, null), $http.get(`/spr/campaign/${campaignId}/activeasset`, null)]).then(function(values) {
												if (values[0].data != undefined && values[0].data != null) {
													$scope.modal.currentAssetId = values[0].data.currentAssetId;
													if (values[0].data.multiCurrentAssetId != undefined && values[0].data.multiCurrentAssetId.length >0) {
														tempAsset = values[0].data.multiCurrentAssetId;
													} else if ($scope.modal.currentAssetId != undefined && $scope.modal.currentAssetId != null) {
														tempAsset.push($scope.modal.currentAssetId);
													}
													$scope.modal.acctiveAsset = tempAsset;
													$scope.createCampaignSettingsDTO(values[0].data);
													// $scope.modal.assetList = values[0].data.assets;
													$scope.modal.assetList = [];
													angular.forEach(values[0].data.assets, function(asset) {
														if (asset.isRemoved == null || asset.isRemoved == undefined || !asset.isRemoved) {
															$scope.modal.assetList.push(asset);
														}
													});
													$scope.oldAssetCount = 0;
													if($scope.modal.acctiveAsset && $scope.modal.acctiveAsset.length > 0 && $scope.modal.assetList != undefined && $scope.modal.assetList != null && $scope.modal.assetList.length > 0) {													
														angular.forEach($scope.modal.acctiveAsset,  function(element){
															var keepGoing = true;
															angular.forEach($scope.modal.assetList, function(item){
																if ( keepGoing && item.assetId == element) {
																	item.checked = true;
																	$scope.oldAssetCount++;
																	keepGoing = false;
																}
															});
														});
													}
												}

												if (values[1].data != undefined && values[1].data != null) {
													$scope.modal.assetEmailFromEmail = values[1].data.fromEmail;
													$scope.modal.assetEmailFromName = values[1].data.fromName;
													$scope.modal.assetEmailReplyTo = values[1].data.replyTo;
													$scope.modal.assetEmailSubject = values[1].data.subject;
													$scope.modal.assetEmailMessage = values[1].data.message;
													$scope.modal.assetEmailUnSubscribeUrl = values[1].data.unSubscribeUrl;
													$scope.modal.assetEmailClickUrl = values[1].data.clickUrl;
													if (tab == "Asset" && (value == undefined && value != "ClickedFromPopUp"))  {
														$scope.loading = false;
														$scope.showconfigModal(campaignId, tab);
													}
												}
											});
										}

										 /* ############### Date :- 27/10/18 Update Campaign Settings Start ###############*/
										 $scope.createCampaignSettingsDTO = function(campaign) {
											$scope.campaignSettingDTO.hidePhone = campaign.hidePhone.toString();
											$scope.campaignSettingDTO.qaRequired = campaign.qaRequired.toString();
											$scope.campaignSettingDTO.emailSuppressed = campaign.isEmailSuppressed.toString();
											$scope.campaignSettingDTO.localOutboundCalling = campaign.localOutboundCalling.toString();
											$scope.campaignSettingDTO.sipProvider = campaign.sipProvider.toString();
											$scope.campaignSettingDTO.retrySpeedEnabled = campaign.retrySpeedEnabled.toString();
											$scope.campaignSettingDTO.simpleDisposition = campaign.simpleDisposition.toString();
											$scope.campaignSettingDTO.enableMachineAnsweredDetection = campaign.enableMachineAnsweredDetection.toString();
											$scope.campaignSettingDTO.callMaxRetries = campaign.callMaxRetries.toString();
											$scope.campaignSettingDTO.dailyCallMaxRetries = campaign.dailyCallMaxRetries.toString();
											$scope.campaignSettingDTO.enableMachineAnsweredDetection = campaign.enableMachineAnsweredDetection.toString();
											$scope.campaignSettingDTO.retrySpeedFactor = campaign.retrySpeedFactor;
											$scope.campaignSettingDTO.campaignType = campaign.type;
											if (campaign.dialerMode != null && campaign.dialerMode != undefined) {
												$scope.campaignSettingDTO.dialerMode = campaign.dialerMode.toString();
											}
											// if (campaign.usePlivo) {
											// 	$scope.campaignSettingDTO.voicePlatform = "PLIVO";
											// } else {
											// 	$scope.campaignSettingDTO.voicePlatform = "TWILIO";
											// }
										}
										/* ############### Date :- 27/10/18 Update Campaign Settings End ###############*/
										
										// var loadCallSpeed = function(campaignId, tab, value) {
										// 	$scope.$parent.loading = true;
										// 	CampaignTeamService.query({id : campaignId}, null).$promise.then(function(data) {
										// 		$scope.modal.callSpeed = data.callSpeedPerMinPerAgent;
										// 		$scope.modal.dialerMode = data.dialerMode;
										// 		console.log(data);
										// 		// $scope.$parent.loading = false;
										// 		$scope.loading = false;
										// 		if (tab == "Dialer" && (value == undefined && value != "ClickedFromPopUp"))  {
										// 			$scope.loading = false;
										// 			$scope.showconfigModal(campaignId, tab);
										// 		}
										// 	},function(error) {
										// 		$scope.$parent.loading = false;
										// 	});
										// };
										var loadLeadSortingOrder = function(campaignId, tab) {
											$scope.$parent.loading = true;
											CampaignTeamService.query({id : campaignId}, null).$promise.then(function(data) {
												$scope.modal.leadSortOrder = data.leadSortOrder;
												if (data.leadSortOrder != undefined || data.leadSortOrder != null) {
													$scope.campaignSettingDTO.leadSortOrder = data.leadSortOrder;	
												}
												if (data.callSpeedPerMinPerAgent != undefined || data.callSpeedPerMinPerAgent != null) {
													$scope.campaignSettingDTO.callSpeedPerMinPerAgent = data.callSpeedPerMinPerAgent;
												}
												// console.log(data);
												$scope.$parent.loading = false;
											},function(error) {
												$scope.$parent.loading = false;
											});
										};
										
										$scope.main = {
								                page: 0,
								                take: 10,
								                direction: "ASC",
								                sort:"",
								                start: 1,
								                end: 10,
                                	  };
										$scope.loadAgentAllocationFortab = function() {
											$scope.getAgentList();
											loadAgentAllocation($scope.modal.currentCampaignId,'');
										}

										var loadIdnc = function(campaignId, tab, value) {
											idncService.get({searchstring: angular.toJson({"phoneNumber" : "", "partnerId":user.organization, "page" : 0, "size" : 10})}).$promise.then(function(data){
	                                        	  $scope.idnclist = data.dncNumbers;
												  $scope.idncTotalRecord = data.dncNumberCount;
	                                        	  // console.log(data);
	                                        	  $scope.main.pages = parseInt(data.dncNumberCount/$scope.main.take);
	                                        	  if($scope.idncTotalRecord < $scope.main.end) {
	                                        		  $scope.main.end = $scope.idncTotalRecord;
	                                        	  }
	                                        	  if (tab == "Idnc" && (value == undefined && value != "ClickedFromPopUp"))  {
	                                        		  $scope.showconfigModal(campaignId, tab);
												  }
												  $scope.loading = false;
	                                    	},function(error) {
												
											});
										};

											// <!-- ###############  Admin Campaign Setting Tab Start ###############-->
											var loadAdminSetting = function(campaignId, tab, value) {
													$scope.isCampaignSettingIconClicked = true;
													CampaignService.get({id : campaignId}).$promise.then(function(data) {
														$scope.createCampaignSettingsDTO(data);
														if (tab == "AdminSetting" && (value == undefined && value != "ClickedFromPopUp"))  {
															$scope.loading = false;
															$scope.showconfigModal(campaignId, tab);
														}
													},function () {
														
													});
											};
											// <!-- ###############  Admin Campaign Setting Tab End ###############-->

										/* START
										   DATE : 26/10/18
										   REASON : Added supervisor pivot tabs in DataList Tab */
											$scope.searchByManagementLevel = {};
											$scope.searchByDepartment = {};
											$scope.searchByEmployeeCount = {};
											$scope.searchByRevenueRange = {};
											$scope.searchByIndustry = {};
											$scope.searchByTitle = {};
											$scope.searchByCountry = {};
											$scope.searchByCompany = {};
											$scope.searchByStateCode = {};
											$scope.searchBySource = {};
											$scope.searchByIndustryList = {};
											$scope.searchByIsEuList = {};
											$scope.searchBySicList = {};
											$scope.searchByNaicsList = {};
											$scope.queryBy = '$'
											$scope.pivots = {};
											$scope.pivotSelectedTab = "managementLevel";

											$scope.loadPivots = function (campaignId, pivotSelectedTab) {
												$scope.loading = true;
												if (pivotSelectedTab == undefined || pivotSelectedTab == 'ClickedFromPopUp') {
													var pivotSelectedTab = $scope.pivotSelectedTab;
												} else {
													pivotSelectedTab == 'revenue' ? $scope.pivotSelectedTab = 'revenueRange' : $scope.pivotSelectedTab = pivotSelectedTab;
												}
												$scope.pivots = {};
												PivotService.query({
														campaignId: campaignId, pivotSelectedTab: pivotSelectedTab
													}).$promise.then(function(data) {
														$scope.loading = false;
														$scope.pivots = data;
													},function(error) {
														$scope.loading = false;
												});
										   }
											
											// download pivots into excel file
											$scope.downloadPivots = function (campaignId) {
												$scope.pivotReview = true;
												sendPivotEmailService.downloadpivot({
													campaignId: campaignId
												}).$promise.then(function(data) {
													NotificationService.log('success', 'Pivot Download Request Accepted. Please Check Notification After Sometime.');
												},function(error) {
													$scope.loading = false;
												});
												//$window.open("/spr/supervisorpivot/"+$scope.campaign[0].campaignId+"/download");
											}
										//    <!-- ############### End ############### -->

										$scope.getHealthCheck = function () {
											$scope.loading = true;
											if (!$scope.validateHealthCheckCriteria($scope.listOfCriteria)) {
												$scope.loading = false;
												return;
											 }
											angular.forEach($scope.listOfCriteria, function(v, k) {
												if(v.attribute == "DEPARTMENT" || v.attribute == "department" || v.attribute == "title" || v.attribute == "Title" || v.attribute == "MANAGEMENT_LEVEL" || v.attribute == "INDUSTRY" || v.attribute == "industry" || v.attribute == "VALID_COUNTRY" || v.attribute == "EMPLOYEE" || v.attribute == "REVENUE" || v.attribute == "SIC_CODE" || v.attribute == "NAICS_CODE" || v.attribute == "STATE (US ONLY)" || v.attribute == "LAST_UPDATED_DATE") {
													angular.forEach(v.value, function(x,y) {
														if($scope.valueList != '') {
														$scope.valueList = $scope.valueList + ',' + x.value;
														} else {
															$scope.valueList = x.value;
														}
													});
													v.value = $scope.valueList;
													$scope.valueList = "";
												}
											});
												
											var healthChecksDTO = {};
										    healthChecksDTO.campaignId = $scope.campaign[0].campaignId;
										    healthChecksDTO.criteria = $scope.listOfCriteria;
											$scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
											
											// $scope.campaign[0].qualificationCriteria.expressions = [];
											// 	angular.forEach($scopelistOfCriteria, function(listCriteriaValue, key) {
											// 		$scope.campaign[0].qualificationCriteria.expressions.push({"attribute" : criteriaValue.attribute,
											// 		"operator" : criteriaValue.operator,
											// 		"value" : criteriaValue.value,
											// 		"pickList" : criteriaValue.pickList
											// 		});
											// 	});
											//$scope.bindCampaignCriteriaInEdit($scope.campaign[0]);
											$scope.convertDecimalValueToInt($scope.listOfCriteria);
											//TODO Need to comment after testing health check
											if ($scope.validateHealthCheckCriteria($scope.listOfCriteria)) {
												healthCheckService.query(healthChecksDTO).$promise.then(function (data) {
													$scope.healthCheckDto = data;
													$scope.isResponseReceived = true;
													if ($scope.healthCheckDto.totalRecords > 0) {
														$scope.loading = false;
														NotificationService.log('success', $scope.healthCheckDto.totalRecords + ' Records removed successfully.');
													} else {
														$scope.loading = false;
														NotificationService.log('success', ' No records found for health checks.');
													}
												}, function (error) {
													NotificationService.log('error',"Error occured while fetching health checks.");
													$scope.loading = false;
												});
											}
											//$scope.bindCampaignCriteriaInEdit($scope.campaign[0]);
										}

										
										$scope.bindCampaignCriteriaInEditForHealthCheck = function(listOfHealthCheck) {
											if ($scope.listOfCriteria.length > 0) {
												$scope.listOfCriteria = [];
											}
											//$scope.listOfCriteria = [];
											
											angular.forEach(listOfHealthCheck, function(criteriaValue, key) {
												if (criteriaValue.attribute == "INDUSTRY" || criteriaValue.attribute == "industry") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListValForHealthCheck(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "DEPARTMENT" || criteriaValue.attribute == "department") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListValForHealthCheck(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "VALID_COUNTRY") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "SIC_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "NAICS_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "STATE (US ONLY)") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}else if (criteriaValue.attribute == "ZIP_CODE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "VALID_STATE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "SEARCH_TITLE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "NEGATIVE_TITLE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : criteriaValue.value
													});
												} else if (criteriaValue.attribute == "MANAGEMENT_LEVEL" || criteriaValue.attribute == "Title") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												} else if (criteriaValue.attribute == "EMPLOYEE") {
														$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : criteriaValue.pickList
													});
												} else if (criteriaValue.attribute == "REVENUE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListVal(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : criteriaValue.pickList
													});
												} else if (criteriaValue.attribute == "LAST_UPDATED_DATE" || criteriaValue.attribute == "LAST_UPDATED_DATE") {
													$scope.listOfCriteria.push({"attribute" : criteriaValue.attribute,
														"operator" : criteriaValue.operator,
														"value" : $scope.getPickListValForHealthCheck(criteriaValue.pickList, criteriaValue.value.split(",")),
														"pickList" : $scope.sortGivenPickList(criteriaValue.pickList)
													});
												}
											});
										}
										
										$scope.getPickListValForHealthCheck = function(pickList, selectedValues) {
											var pickListObjects = [];
											angular.forEach(pickList, function(pickListItem, pickListKey) {
													angular.forEach(selectedValues, function(value, key) {
														if (value == pickListItem.value) {
															pickListObjects.push(pickListItem);
														}
													});
											})
											
											return pickListObjects;
										}

										$scope.validateHealthCheckCriteria = function(healthCheckCriteria) {
											var isMandatoryFieldsEntered = true;
											angular.forEach(healthCheckCriteria, function(criteria, key) {
												if (criteria.attribute == null || criteria.attribute == undefined || criteria.attribute == '') {
													NotificationService.log('error', 'Please select criteria.');
													isMandatoryFieldsEntered = false;
													return false;
												}
												if (criteria.operator == null || criteria.operator == undefined || criteria.operator == '') {
													NotificationService.log('error', 'Please select operator.');
													isMandatoryFieldsEntered = false;
													return false;
												}
												if (criteria.value.length == 0 || criteria.value == undefined || criteria.value == '') {
													NotificationService.log('error', 'Please enter value.');
													isMandatoryFieldsEntered = false;
													return false;
												}
												if (isMandatoryFieldsEntered && criteria.value != undefined && criteria.value != '' && criteria.value.length > 0 && (criteria.attribute == 'VALID_COUNTRY' || criteria.attribute == 'DEPARTMENT' || criteria.attribute == 'department' || criteria.attribute == 'MANAGEMENT_LEVEL')) {
													for(var i = 0; i < criteria.value.length; i++) {
														if (criteria.value[i].value == 'All') {
															NotificationService.log('error', '"All" is not a valid attibute for '+ criteria.attribute + '. Please select '+ criteria.attribute + ' from the list.');
															isMandatoryFieldsEntered = false;
															return false;
														}
													}
												}
											});
											if (isMandatoryFieldsEntered) {
												return true;
											}
										}

										
										$scope.dataAttributeCheck = function(healthCheckCriteria) {
											var mandatoryAttributeCheck = [];
											angular.forEach(healthCheckCriteria, function(criteria, key) {
												mandatoryAttributeCheck.push(criteria.attribute);
											});
											var isFirst = true;
											var errorMessage = "";

											if (mandatoryAttributeCheck.length < 1) {
												NotificationService.log('error', 'VALID_COUNTRY, MANAGEMENT_LEVEL, DEPARTMENT, EMPLOYEE is Mandatory');
												return false;
											}

											if (mandatoryAttributeCheck.length > 0 && !mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL')) {
												if (isFirst) {
													isFirst = false;
												} else {
													errorMessage += ", ";
												}
												errorMessage += "MANAGEMENT_LEVEL";
											}

											if (mandatoryAttributeCheck.length > 0 && !mandatoryAttributeCheck.includes('VALID_COUNTRY')) {
												if (isFirst) {
													isFirst = false;
												} else {
													errorMessage += ", ";
												}
												errorMessage += "VALID_COUNTRY";
											}

											if (mandatoryAttributeCheck.length > 0 && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department'))) {
												if (isFirst) {
													isFirst = false;
												} else {
													errorMessage += ", ";
												}
												errorMessage += "DEPARTMENT";
											}

											/*if (mandatoryAttributeCheck.length > 0 && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
												if (isFirst) {
													isFirst = false;
												} else {
													errorMessage += ", ";
												}
												errorMessage += "EMPLOYEE";
											}*/

											if(!$scope.campaign[0].abm) {
                                                if (mandatoryAttributeCheck.length > 0 && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
                                                    if (isFirst) {
                                                        isFirst = false;
                                                    } else {
                                                        errorMessage += ", ";
                                                    }
                                                    errorMessage += "EMPLOYEE";
                                                }
                                            }

											errorMessage += " is Mandatory";

											if (errorMessage == " is Mandatory") {
												return true;
											} else {
												NotificationService.log('error', errorMessage);
												return false;
											}

										// 	if (mandatoryAttributeCheck.length > 0 && (!mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') 
										// 	&& !mandatoryAttributeCheck.includes('VALID_COUNTRY') && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && !mandatoryAttributeCheck.includes('EMPLOYEE'))) {
										// 		NotificationService.log('error', 'VALID_COUNTRY, MANAGEMENT_LEVEL, DEPARTMENT, EMPLOYEE is Mandatory');
										// 		return false;
										// 	}else if (mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') && 
										// 		!mandatoryAttributeCheck.includes('VALID_COUNTRY') && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
										// 			NotificationService.log('error', 'VALID_COUNTRY, DEPARTMENT, EMPLOYEE is Mandatory');
										// 		return false;
										// 	} else if (mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') && 
										// 		!mandatoryAttributeCheck.includes('VALID_COUNTRY') && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && mandatoryAttributeCheck.includes('EMPLOYEE')) {
										// 			NotificationService.log('error', 'MANAGEMENT_LEVEL, VALID_COUNTRY, DEPARTMENT is Mandatory');
										// 		return false;
										// 	} else if (!mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') && 
										// 		mandatoryAttributeCheck.includes('VALID_COUNTRY') && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
										// 			NotificationService.log('error', 'MANAGEMENT_LEVEL, DEPARTMENT, EMPLOYEE is Mandatory');
										// 		return false;
										// 	}else if (!mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') && 
										// 		!mandatoryAttributeCheck.includes('VALID_COUNTRY') && !(mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
										// 			NotificationService.log('error', 'VALID_COUNTRY, MANAGEMENT_LEVEL, DEPARTMENT, EMPLOYEE is Mandatory');
										// 		return false;
										// 	}else if (!mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL') && 
										// 	!mandatoryAttributeCheck.includes('VALID_COUNTRY') && (mandatoryAttributeCheck.includes('DEPARTMENT') || mandatoryAttributeCheck.includes('department')) && !mandatoryAttributeCheck.includes('EMPLOYEE')) {
										// 		NotificationService.log('error', 'VALID_COUNTRY, MANAGEMENT_LEVEL, EMPLOYEE is Mandatory');
										// 	return false;
										// }else if (!mandatoryAttributeCheck.includes('MANAGEMENT_LEVEL')) {
										// 		NotificationService.log('error', 'MANAGEMENT_LEVEL is Mandatory');
										// 		return false;
										// 	}else if (!mandatoryAttributeCheck.includes('VALID_COUNTRY')) {
										// 		NotificationService.log('error', 'VALID_COUNTRY is Mandatory');
										// 		return false;
										// 	}else if (!mandatoryAttributeCheck.includes('DEPARTMENT') && !mandatoryAttributeCheck.includes('department')) {
										// 		NotificationService.log('error', 'DEPARTMENT is Mandatory');
										// 		return false;
										// 	}else {
										// 		return true;
										// 	}
									   	}

										$scope.convertDecimalValueToInt = function(healthCheckCriteria) {
										angular.forEach(healthCheckCriteria, function(criteria, key) {
											if (criteria.value != undefined && criteria.value != '') {
												if (!isNaN(criteria.value)) {
													criteria.value = parseInt(criteria.value);
												}
											} 
										});
									}

									$scope.removeExpression = function (parent, index) {
										$scope.listOfCriteria.splice(index,1);
										// $scope['size' + index] = 0;
									};
										$scope.sendPivots = function(campaignId) {
											sendPivotEmailService.query({
												campaignId: campaignId
											}).$promise.then(function(data) {
												NotificationService.log('success', 'Pivots will be sent on email.');
											},function(error) {
												$scope.loading = false;
										});
											// /supervisorpivot/mail/{campaignId}
										}

									   $scope.buyData = function(campaignId,databuystatus) {
										   $scope.loading = true;
										   if (!$scope.validateHealthCheckCriteria($scope.listOfCriteria)) {
											   $scope.loading = false;
											   return;
											}
											if ($scope.isSearchCompany != null && $scope.isSearchCompany != undefined && $scope.isBuyProspect != null 
												&& $scope.isBuyProspect != undefined && $scope.isSearchCompany && !$scope.isBuyProspect) {
													$scope.loading = false;
													NotificationService.log('error', "Company details fetching in progress. Please initiate buy once Company details are fetched.");
											 		return;
										     }
										   angular.forEach($scope.listOfCriteria, function(v, k) {
												if(v.attribute == "DEPARTMENT" || v.attribute == "department" || v.attribute == "title" || v.attribute == "Title" || v.attribute == "MANAGEMENT_LEVEL" || v.attribute == "INDUSTRY" || v.attribute == "industry" || v.attribute == "VALID_COUNTRY" || v.attribute == "EMPLOYEE" || v.attribute == "REVENUE" || v.attribute == "SIC_CODE" || v.attribute == "NAICS_CODE" || v.attribute == "STATE (US ONLY)" || v.attribute == "LAST_UPDATED_DATE") {
													angular.forEach(v.value, function(x,y) {
														if($scope.valueList != '') {
														$scope.valueList = $scope.valueList + ',' + x.value;
														} else {
															$scope.valueList = x.value;
														}
													});
													v.value = $scope.valueList;
													$scope.valueList = "";
												}
											});
											if (databuystatus == "Cancel Buy") {
												$scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
												$scope.databuyStop(campaignId);
												$scope.loading = false;
												return;
											}
										   if (!$scope.modal.totalBuyCount || $scope.modal.totalBuyCount < 1) {
												$scope.loading = false;
												NotificationService.log('error', "Please enter valid number.");
												$scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
												return;
										   }
										   if(!$scope.dataAttributeCheck($scope.listOfCriteria)) {
												$scope.loading = false;
												$scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
												return;
											}
											$scope.isMaxEmployeeAllow = false;
											angular.forEach($scope.listOfCriteria, function(criteria, key) {
                                                if (criteria.attribute == "EMPLOYEE") {
                                                    var empValue = criteria.value.split(',');
                                                    if(empValue != undefined && empValue != null && empValue.length == 1 && empValue.includes('Over 10000')){
                                                       $scope.isMaxEmployeeAllow = true;
                                                    }
                                                }
                                            });


                                             if($scope.isMaxEmployeeAllow && !$scope.campaign[0].abm && !$scope.campaign[0].maxEmployeeAllow) {
                                                     $scope.loading = false;
                                                     NotificationService.log('error', "No records found for Employee size over 10000");
                                                     $scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
                                                     return;
                                              }


										   var zoomBuySearchDTO = {};
										   zoomBuySearchDTO.campaignId = $scope.campaign[0].campaignId;
										   zoomBuySearchDTO.totalCount = $scope.modal.totalBuyCount;
										   zoomBuySearchDTO.organizationId = $scope.loggedInUser.organization;
										   zoomBuySearchDTO.criteria = $scope.listOfCriteria;
										   $scope.bindCampaignCriteriaInEditForHealthCheck($scope.listOfCriteria);
										   ZoomBuyService.buy(zoomBuySearchDTO).$promise.then(function(data) {
											   	NotificationService.log('success', "Data buy in progress, please check this screen in sometime.");
												$scope.loading = false;
												$scope.checkDatabuyStatus($scope.campaign[0].campaignId);
												$scope.checkDatabuyProcess($scope.campaign[0].campaignId);
											},function(responseError) {
												$scope.loading = false;
												$scope.isBuyProspect = undefined;
											    $scope.isSearchCompany = undefined;
												NotificationService.log('error', "Data Buy Already in Process Please Check After Sometime.");
											});
									   }

									   $scope.checkDatabuyProcess = function(campaignId) {
											$scope.dataBuyTrackerPromise = $interval(function() {
												if ($scope.databuystatus != null && $scope.databuystatus != undefined && $scope.databuystatus.dataBuyQueueId != 'NA') {
													ZoomBuyService.databuyprocess({
														campaignId: campaignId,
														id: $scope.databuystatus.dataBuyQueueId,
													}).$promise.then(function (data) {
														$scope.dataBuyTracker = data;
														if ($scope.dataBuyTracker.percentage > 0 ) {
															$scope.showDatabuyProgressBar = true;
														}
														if($scope.dataBuyTracker.dataBuyStatus == 'COMPLETED' || $scope.dataBuyTracker.dataBuyStatus == 'READY_FOR_REVIEW') {
															$scope.showDatabuyProgressBar = false;
															$scope.checkDatabuyStatus(campaignId);
															$scope.isBuyProspect = undefined;
											                $scope.isSearchCompany = undefined;
															$interval.cancel($scope.dataBuyTrackerPromise);
														}
														//console.log(data);
													}, function (error) {
														console.log(error);
														$interval.cancel($scope.dataBuyTrackerPromise);
													});
											    	}
											},5000);
										}

									   $scope.checkDatabuyStatus = function(campaignId) {
										   ZoomBuyService.databuystatus({
											   campaignId: campaignId,
										   }).$promise.then(function (data) {
											$scope.databuystatus = data;
											$scope.isBuyProspect = data.isBuyProspect;
											$scope.isSearchCompany = data.isSearchCompany;
											//console.log($scope.databuystatus);
										   }, function (error) {
											   $scope.databuystatus.status = 'Data Buy';
											   console.log(error);
										   });
									   }

									   $scope.loadDbq = function(campaignId) {
									     	$scope.loading = true;
                                               ZoomBuyService.databuycampaign({
                                                   campaignId: campaignId,
                                               }).$promise.then(function (data) {
                                               		$scope.loading = false;
                                               		$scope.databuycampaign = data;
                                               		if ($scope.databuycampaign != null && $scope.databuycampaign.expressions != null) {
                                               			$scope.bindDbqCampaignCriteriaInEdit($scope.databuycampaign);
                                               		} else {
                                               			$scope.bindCampaignCriteriaInEdit($scope.campaign[0]);
                                               		}
                                               }, function (error) {
                                            	   $scope.loading = false;
                                            	   NotificationService.log('error', "Failed to fetch campaign data buy criteria");
                                            	   $scope.databuycampaign.expressions = null;
                                                   $scope.bindCampaignCriteriaInEdit($scope.campaign[0]);
                                                   console.log(error);
                                               });
                                       	}

										$scope.databuyStop = function (campaignId) {
											$interval.cancel($scope.dataBuyTrackerPromise);
											ZoomBuyService.databuystop({
												campaignId: campaignId,
											}).$promise.then(function (data) {
												$scope.databuystatus = data;
												$scope.showDatabuyProgressBar = false;
												//console.log($scope.databuystatus);
											}, function (error) {
												$scope.databuystatus.status = 'Data Buy';
												console.log(error);
											});
										}
										
										$scope.pivotReviewStatus = function (campaignId) {
											PivotService.pivotstatus({
												campaignId: campaignId
											}).$promise.then(function(data) {
												$scope.pivotStatus = data;
											},function(error) {
												console.log(error);
											});
										}
										
										$scope.pivotReviewStatus = function (campaignId) {
											PivotService.pivotstatus({
												campaignId: campaignId
											}).$promise.then(function(data) {
												$scope.pivotStatus = data;
											},function(error) {
												console.log(error);
											});
										}

									   $scope.companyDiscover = function(campaignId) {
										   	$scope.loading = true;
											EverstringService.discover({
												campaignId: campaignId,
											}).$promise.then(function(data) {
												$scope.loading = false;
												NotificationService.log('success', "You Have Successfully Purchase Companies.");
											},function(error) {
												$scope.loading = false;
												NotificationService.log('error', "Failed to purchase companies.");
											});
										}

										$scope.openConfigurationModal = function(campaignId, campaign, tab) {
										    $interval.cancel($scope.dataBuyTrackerPromise);
											$scope.disableCloseButton = true;
											$scope.databuystatus = {};
											$scope.dataBuyTracker = {};
											$scope.showDatabuyProgressBar = false;
											$scope.disableActivateDataButton = false;
											$scope.modal = {};
											$scope.loading = true;
											$scope.isCampaignSettingIconClicked = false;
											$scope.pivotSelectedTab = "managementLevel";
											switch (tab) {
												case "Script" :
													loadScript(campaignId, tab);
													break;
												case 'Rebuttals':
													loadScript(campaignId, tab);
													break;
												case 'Data List':
													$scope.pivotReviewStatus(campaignId);
													loadDataSource(campaignId, tab);
													$scope.loadPivots(campaignId);
													break;
												case 'Agent':
													loadAgentAllocation(campaignId, tab);
													break;	
												case 'Notifications':
													loadNotification(campaignId, tab);
													break;		
												case 'Asset':
													loadAsset(campaignId, tab);
													break;	
												case 'Idnc':
													loadIdnc(campaignId, tab);
													break;		
												case 'AdminSetting':
													// loadCallSpeed(campaignId, tab);
													loadLeadSortingOrder(campaignId, tab);
													loadAdminSetting(campaignId, tab);	
													break;		
											}
											$scope.campaign = campaign;
											$localStorage.selectedCampaign = JSON.stringify(campaign);
											$localStorage.selectedTab = JSON.stringify("configurations");
										}
										
										$scope.showconfigModal = function (campaignId, tab) {
											$rootScope.configurationModalInstance = $modal.open({
												scope : $scope,
												templateUrl : 'ConfigurationModal.html',
												controller  :  ConfigurationCtrl,
												backdrop: 'static',
												windowClass : 'configuration-modal-modal',
  		                                		resolve: {
  		                                			campaignId: function(){
	  		                    		                return campaignId;
	  		                    		            },
	  		                    		            tab : function () {
	  		                    		            	return tab;
	  		                    		            },
	  		                    		            user : function () {
	  		                    		            	return user;
	  		                    		            }
  		                                		}
											});	
										}

										/***
										 *  DATE :- 18/10/19 Opens retry setting modal pop up.
										 *  Added to set retry speed from admin setting of supervisor screen.
										 *  START
										 */
										$scope.showRetrySpeedSettingModal = function () {
											$scope.retrySpeedSettingmodal = $modal.open({
												scope: $scope,
												templateUrl: 'retrySpeedSettingmodal.html',
												controller: 'RetrySpeedSettingController',
												windowClass: 'retrySpeedModal',
												backdrop: 'static',
												resolve: {
													CampaignSettingService: function(){
														return CampaignSettingService;
													},
													NotificationService: function(){
														return NotificationService;
													}
												}
											 });
										}
										// END
										
										var ConfigurationCtrl = function($scope, $modalInstance, $rootScope, $interval, campaignId, tab, user) {
											$scope.isCurrentTab = tab;
											
											$scope.userRoleSupervisor = $filter('filter')(user.roles, "SUPERVISOR")[0];
											
											$scope.opentabs = function(tab) {
												if (tab == "Agent" && !$scope.$parent.isAgentLoaded) {//show loader if agent is not loaded yet
													$scope.$parent.isAgentTabClicked = true;
													$scope.$parent.loading = true;
												}
												$scope.isCampaignSettingIconClicked = false;
												switch (tab) {
													case "Script" :
														loadScript(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;
													case 'Rebuttals':
														loadScript(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;
													case 'Data List':
														$scope.pivotReviewStatus(campaignId);
														loadDataSource(campaignId, tab, "ClickedFromPopUp");
														$scope.loadPivots(campaignId, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;
													case 'Agent':
														loadAgentAllocation(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;	
													case 'Notifications':
														loadNotification(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;		
													case 'Asset':
														loadAsset(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;	
													case 'Idnc':
														loadIdnc(campaignId, tab, "ClickedFromPopUp");
														$scope.isCurrentTab = tab;
														break;		
													case 'AdminSetting':
														// loadCallSpeed(campaignId, tab, "ClickedFromPopUp");
														loadLeadSortingOrder(campaignId, tab, "ClickedFromPopUp");
														loadAdminSetting(campaignId, tab, "ClickedFromPopUp");	
														$scope.isCurrentTab = tab;
														$scope.isCampaignSettingIconClicked = true;
														break;		
												}
											}
											
											$scope.$on('$destroy', function ()  {
												$scope.$parent.isAgentLoaded = false;
											});
											
											$scope.script = "Questions";
											$scope.showActivateAsset = true;
											$scope.showCurrentRebuttal = true;
											$scope.showCurrentNotification = true;
											$scope.showCallSpeed = true;
											$scope.showLeadSortingOrder = true;
											$scope.showidnc = true;
											$scope.showAdminSetting = true;
											
											$scope.cancel = function() {
												$modalInstance.dismiss('cancel');
												$scope.listOfCriteria = [];
											}
											
											$scope.submitScript = function() {
												$scope.invalidScript = false;
												$scope.$parent.loading = true;
												NotificationService.clear();
												var callGuideObject = {
														confirmSection : {script : $scope.confirmSectionScript},
														questionSection : {script : $scope.questionSectionScript},
														consentSection : {script : $scope.consentSectionScript},
														noteSection : {script : $scope.noteSectionScript}
														};
												CallGuideService.update({id : campaignId},callGuideObject).$promise.then(function(data) {
													NotificationService.log('success', "Script added successfully.");
													$scope.$parent.loading = false;
												},
												function() {
													$scope.$parent.loading = false;
												});
											}
											
											
											/*start of rebuttal code*/
											$scope.addNewRebuttal = function() {
												$scope.scripts = [
												                  {section : "question",isSelected : false},
												                  {section : "confirm",isSelected : false},
												                  {section : "consent",isSelected : false},
												                  {section : "note",isSelected : false}
												                  ];
												$scope.showCurrentRebuttal = false
											}
											
											$scope.$watch( "scripts" , function(newValue,oldValue){
												var trues = $filter("filter")( newValue , {isSelected:true} );
												$scope.isAnyScriptSelected = trues ? trues.length : undefined;
											}, true );
											 
											var resetRebuttalValues = function() {
												$scope.rebuttalTitle = undefined;
												$scope.rebuttalHelpText = undefined;
												angular.forEach($scope.scripts,  function(script, index){
													script.isSelected = false;
												});
											} 
											
											$scope.submitNewRebuttal = function() {
												$scope.invalidRebuttal = false;
												if ($scope.newRebuttal.$valid) {
													$scope.$parent.loading = true;
													NotificationService.clear();
													if (!$scope.isAnyScriptSelected) {
														NotificationService.log('error', "Please select atleast one section.");
														return;
													}
													
													var newRebuttal = {
															scenario : $scope.rebuttalTitle.replace(/ /g,"_"),
															label : $scope.rebuttalTitle,
															solution : $scope.rebuttalHelpText
													}
													
													angular.forEach($scope.scripts,  function(script, index){
														if (script.isSelected) {
															$scope.callSection = $scope.callGuideData[script.section+'Section'];
															if ($scope.callGuideData[script.section+'Section'] == null) {
																$scope.callGuideData[script.section+'Section'] = {};
															}
															if ($scope.callGuideData[script.section+'Section'].rebuttals == null) {
																$scope.callGuideData[script.section+'Section'].rebuttals = [];
															}
															$scope.callGuideData[script.section+'Section'].rebuttals.push(newRebuttal);
														}
													});
													CallGuideService.update({id : campaignId},$scope.callGuideData).$promise.then(function(callGuideData) {
														NotificationService.log('success', "Rebuttal added successfully.");
														// console.log(callGuideData);
														$scope.callGuideData = callGuideData;
														constructSectionRebuttalArray();
														resetRebuttalValues();
														$scope.$parent.loading = false;
													},
													function() {
														NotificationService.log('error', "Something is wrong.");
														$scope.$parent.loading = false;
													});
												} else {
													$scope.invalidRebuttal = true;
												}
												
											}
											
											// This method is used for to delete the Rebulttals
											$scope.deleteRebulttals = function(indexNo, labelData,solutionData,tempSectionName){
												$scope.$parent.loading = true;
												if(tempSectionName == "Question"){
													var sectionName = "question";
													if ($scope.callGuideData[sectionName+'Section'] == null) {
														$scope.callGuideData[sectionName+'Section'] = {};
													}
													if ($scope.callGuideData[sectionName+'Section'].rebuttals == null) {
														$scope.callGuideData[sectionName+'Section'].rebuttals = [];
													}
													$scope.callGuideData[sectionName+'Section'].rebuttals.splice(indexNo,1);
												}
												if(tempSectionName == "Confirm"){
													var sectionName = "confirm";
													if ($scope.callGuideData[sectionName+'Section'] == null) {
														$scope.callGuideData[sectionName+'Section'] = {};
													}
													if ($scope.callGuideData[sectionName+'Section'].rebuttals == null) {
														$scope.callGuideData[sectionName+'Section'].rebuttals = [];
													}
													$scope.callGuideData[sectionName+'Section'].rebuttals.splice(indexNo,1);
												}
												if(tempSectionName == "Consent"){
													var sectionName = "consent";
													if ($scope.callGuideData[sectionName+'Section'] == null) {
														$scope.callGuideData[sectionName+'Section'] = {};
													}
													if ($scope.callGuideData[sectionName+'Section'].rebuttals == null) {
														$scope.callGuideData[sectionName+'Section'].rebuttals = [];
													}
													$scope.callGuideData[sectionName+'Section'].rebuttals.splice(indexNo,1);
												}
												if(tempSectionName == "Note"){
													var sectionName = "note";
													if ($scope.callGuideData[sectionName+'Section'] == null) {
														$scope.callGuideData[sectionName+'Section'] = {};
													}
													if ($scope.callGuideData[sectionName+'Section'].rebuttals == null) {
														$scope.callGuideData[sectionName+'Section'].rebuttals = [];
													}
													$scope.callGuideData[sectionName+'Section'].rebuttals.splice(indexNo,1);
												}
												CallGuideService.update({id : campaignId},$scope.callGuideData).$promise.then(function(callGuideData) {
													NotificationService.log('success', "Rebuttal deleted successfully.");
													// console.log(callGuideData);
													$scope.callGuideData = callGuideData;
													getCallGuide(campaignId);
													resetRebuttalValues();
													$scope.$parent.loading = false;
												},
												function() {
													NotificationService.log('error', "Something is wrong.");
													$scope.$parent.loading = false;
												});
											}
											
											
											$scope.getCurrentRebuttal = function() {
												$scope.loading = true;
												$scope.showCurrentRebuttal = true;
												getCallGuide(campaignId);
											}
											
											/*end of upload code */
											
											/*start of notification*/
											var resetNotificationValues = function() {
												$scope.noticeMsg = undefined;
												$scope.noticePriority = undefined;
												$scope.noticeStartDate = undefined;
												$scope.noticeEndDate = undefined;
											}
											
											$scope.addNewNotification = function() {
												$scope.invalid = false;
												resetNotificationValues();
												$scope.noticePriorities = ["High","Medium","Low"];
												$scope.showCurrentNotification = false;
												$scope.minDate = new Date().toISOString().substring(0, 10);
											}
											
											$scope.getCurrentNotifications = function() {
												$scope.$parent.loading = true;
												$scope.showCurrentNotification = true;
												SupervisorNoticeService.get().$promise.then(function(data) {
													$scope.supervisorNotices = data;
													$scope.$parent.loading = false;
												});
											}
											$scope.invalidNotification = false;
											$scope.submitNewNotification = function() {
												NotificationService.clear();
												if ($scope.newNotification.$valid) {
													$scope.notice = {
															notice : $scope.noticeMsg,
															priority: $scope.noticePriority ? $scope.noticePriority.toUpperCase() : null,
															type : "ANNOUNCEMENTS",//temporary hard-coded
															startDate : $scope.noticeStartDate,
															endDate : $scope.noticeEndDate,
															timeZone : timeZone
													}
													$scope.$parent.loading = true;
													SupervisorNoticeService.save($scope.notice).$promise.then(function(data){
														$scope.$parent.loading = false;
														resetNotificationValues();
														NotificationService.log('success', "Notice added successfully.");
													},
													function() {
														NotificationService.log('error', "Something is wrong.");
														$scope.$parent.loading = false;
													});
												} else {
													$scope.invalidNotification = true;
												}
												
											}
												
											/*end of notification*/
											
											/*start file upload code*/
											
											$scope.resetReadOnlyComponent = function () {
												$scope.uploadBtnDisabled = false;
												$scope.errorLinkEnabled = false;
												$scope.FileUploadSuccess = false;
												$scope.FileRecordFail = false;
												$scope.FileUploadFail = false;
												$scope.showProgressBar = false;
											};

											/* DATA NUKE - START */
											$scope.uploadDataNukeFile = function (file, errFiles) {
											    $scope.errFile = errFiles && errFiles[0];
											    if (file) {
											        $scope.$parent.loading = true;
											        file.upload = Upload.upload({
											            url: 'spr/supervisorpivot/' + $scope.campaign[0].campaignId + '/upload',
											            data: { file: file }
											        });
											        file.upload.then(function (response) {
											            $scope.$parent.loading = false;
											            file.result = response.data;
											            NotificationService.log('success', 'File uploaded successfully.');
														$scope.loadPivots(campaignId);
														$scope.pivotReviewStatus(campaignId);
											        }, function (response) {
											            $scope.$parent.loading = false;
											            NotificationService.log('error', 'Invalid file format. Only .xls, .xlsx accepted.');
											        });
											    }
											}

											$scope.openConfirmDataNukeModal = function (file, errFiles) {
												if (file) {
													$scope.confirmDataNukeModalInstance = $modal.open({
												        scope: $scope,
												        templateUrl: 'confirmDataNukeModal.html',
												        controller: ConfirmDataNukeModalInstanceCtrl,
												        windowClass: 'confirm-data-nuke-modal',
												        resolve: {
												            file: function () {
												                return file;
												            },
												            errFiles: function () {
																return errFiles;
															}
												        }
												    });
												}
											}

											var ConfirmDataNukeModalInstanceCtrl = function ($scope, file, errFiles) {
											    $scope.confirmDataNuke = function (campaignId) {
											        $scope.uploadDataNukeFile(file, errFiles, campaignId);
											        $scope.confirmDataNukeModalInstance.close();
											    };

											    $scope.cancel = function () {
											        $scope.confirmDataNukeModalInstance.close();
											        file = null;
											        errFiles = null;
											    }
											}
											/* DATA NUKE - START */

											$scope.uploadContacts = function(myFile) {
												NotificationService.clear();
												$rootScope.campaignContactFileUploadStart = true;
												var file = myFile;
												var uploadUrl = 'spr/campaigncontact/'+ campaignId+ '/upload';
												if (file == "" || file == null || file == undefined) {
													NotificationService.log('error', "Please select file to upload.");
												} else {
													$scope.waitMsg = true;
													fileUpload.uploadFileToUrl(file,uploadUrl)
													.success(function(data,status,headers,config) {
														if(data.code=='ERROR') {
															NotificationService.log('error',data.msg);
														} else {
															$scope.fileSuccess = true;
														}
														$scope.$parent.loading = false;
														
													});
													$scope.uploadBtnDisabled = true;
													NotificationService.log('success', "File Upload Request Received. Please Check Your Email For Status.");
													$scope.getFileUploadTracker();
													
												}
											}
											
											$scope.getFileUploadTracker = function () {
												var fileUploaderTrackerPromise = $interval(function(){//starting the timer by by 1 min interval
													FileUploadTrackService.query({id: campaignId}).$promise.then(function(data){
														  $scope.fileUploadStatus=data;
														
												
																
		                                    			  $scope.processedRecordCount = $scope.fileUploadStatus.successRecordCount + $scope.fileUploadStatus.failRecordsCount;
		                                    			  $scope.campaignContact.progress =  parseInt(( $scope.processedRecordCount / $scope.fileUploadStatus.totalRecordsCount) * 100);
		                                    			  if (!isNaN($scope.campaignContact.progress)) {
		                                    				  $scope.waitMsg = false;
			                                    			  $scope.showProgressBar = true;
		                                    			  }
		                                    			  if ($scope.fileUploadStatus.invalidFileError != undefined && $scope.fileUploadStatus.invalidFileError != "" && $scope.fileUploadStatus.invalidFileError != null) {
		                                    				  $scope.waitMsg = false;
		                                    				  $scope.showProgressBar = true;
		                                    				  $rootScope.campaignContactFileUploadStart = false;
		                                    				  $scope.campaignContact.progress = 100;
		                                    				  var progressBarFilePromise = $timeout(function(){//starting the timer by by 1 min interval
		                                    					  $scope.showProgressBar = false;
		                                    					  $scope.FileUploadFail = true;
	                                    						  $scope.errorLinkEnabled = true;
	                                    						  $timeout.cancel(progressBarFilePromise);
																  $interval.cancel(fileUploaderTrackerPromise);
																  $scope.suppressCallable(campaignId)
	                                    						},100);
		                                    				 
		                                    			  } else {
		                                    				  if ($scope.fileUploadStatus.totalRecordsCount != 0 && $scope.fileUploadStatus.totalRecordsCount == $scope.processedRecordCount) {
		                                    					  $rootScope.campaignContactFileUploadStart = false;
		                                    					  $interval.cancel(fileUploaderTrackerPromise);
		                                    					  $scope.campaignContact.progress = 100;
		                                    					  var progressBarRecordPromise = $timeout(function(){//starting the timer by by 1 min interval
		                                    						  $scope.showProgressBar = false;
		                                    						  $scope.FileUploadSuccess = true;
																	  $timeout.cancel(progressBarRecordPromise);
																	  $scope.suppressCallable(campaignId);
		                                    						  if ($scope.fileUploadStatus.failRecordsCount != 0) {
			                                    						  $scope.errorLinkEnabled = true;
			                                    						  $scope.FileRecordFail = true;
			                                    					}
		                                    					},100);
		                                    					
		                                    				  }
		                                    			  }
													});
												},3000);
											}
												
											if ($rootScope.campaignContactFileUploadStart) {
												$scope.uploadBtnDisabled = true;
												$scope.waitMsg = true;
												$scope.getFileUploadTracker();
											}

											$scope.suppressCallable = function (campaignId) {
												suppressCallableService.query({campaignId : campaignId}).$promise.then(function (data) {
												}, function (error) {
													console.log("Error occured while initiating suppress callable thread.");
												});
											}
											
											$scope.getUploadErrors = function () {
												 $window.open("/spr/campaigncontact/"+campaignId+"/upload/errors");
											};
											/*end of file upload*/
											
											/*start of data source*/
											$scope.refreshContactList = false;	/* setting this flag 'false' so that contacts sample list should not refresh when user clicks on delete button */
											// $scope.removeExpression = function(parent, index) {
												// if (!$scope.isIOSignedDisable) {
												// 	parent.splice(index, 1);
												// 	$scope['size'+index] = 0;
												// }
												
												// /* START 
												//  * DATE : 28/04/2017
												//  * REASON : added to refresh contact list when user click on delete button if list is already showing*/
												// if($scope.refreshContactList) {
												// 	$scope.getContacts();
												// }
												// /* END */
											// };
											$scope.size0 = 0;

											$scope.getAttributeType = function(attributeName) {
												if(attributeName == "INDUSTRY" || attributeName == "industry" || attributeName == "DEPARTMENT" || attributeName == "department" || attributeName == "Title" || attributeName == "MANAGEMENT_LEVEL") {
													$scope.isPicklistvalues = true;
												}
												if ($scope.attributeMappings[attributeName] != undefined) {
													return $scope.attributeMappings[attributeName].systemDataType;
												}
											};
											$scope.resetFlags = function() {
												$scope.PurchaseOutstanding = false;
		                   						$scope.purchaseSuccess = false;
		                   						$scope.purchaseFail = false;
		                   						$scope.waitPurchaseMsg = false;
											}
											
											$scope.getContacts = function() {
												NotificationService.clear();
												$scope.resetFlags();												
												
												$scope.refreshContactList = true; /* setting flag 'true' to refresh contacts sample list when user clicks on delete button */
												
												//Hide toaster message as it is coming multiple times
												/*if($window.document.getElementsByClassName("toast-error").length > 0 || $window.document.getElementsByClassName("toast-success").length > 0) {
													$window.document.getElementsByClassName("#toast-container > div").setAttribute('display', 'none');
												}*/
												
												$scope.$parent.loading = true;
												var expressionGroup = {};
												
												for (var index=0; index< $scope.modal.prospectCriteria.expressions.length; index++) {
													var value = $scope.modal.prospectCriteria.expressions[index].bindValue ? $scope.modal.prospectCriteria.expressions[index].bindValue : undefined;
													if (value != undefined) {
														var newValue = [];
														for (var valIndex=0; valIndex<value.length; valIndex++) {
															newValue.push(value[valIndex].value);
														}
														if (newValue.length > 0){
														 $scope.modal.prospectCriteria.expressions[index].value = newValue;
														}
													}
												}
												
												$scope.searchstring.prospectattribute = $scope.modal.prospectCriteria;

												var jsonString = JSON.stringify($scope.searchstring.prospectattribute,function(key,val) {
													if (key == '$$hashKey') {
														return undefined;
													}else{
														return val;
													}
													return val;
												});

												contactSearchService.query({
													searchstring : encodeURIComponent(jsonString)
												}).$promise.then(function(data) {
													$scope.modal.prospectSearchResult = data;
													$scope.modal.prospectSearchResultView = true;
													NotificationService.log('success',$scope.modal.prospectSearchResult.count + " contact's match with your criteria");
													var progressBarFilePromise = $timeout(function(){//starting the timer by by 1 min interval
														$scope.$parent.loading = false;
                              						},100);
													
												},function(error) {
													$scope.$parent.loading = false;
												});
											};
											
											
											/* START RECYCLE MODAL
											 * DATE : 26/04/2017
											 * REASON : added to open new modal when user clicks on recycle button in data list tab */
											$scope.openRecycleStatusModal = function(campaignId, recycleStatus, jsonString) {
												$scope.recycleStatusModalInstance = $modal.open({
													scope : $scope,
													templateUrl : 'recycleStatusModal.html',
													controller : RecycleStatusModalInstanceCtrl,
													windowClass : 'recycle-status-modal',
													//backdrop : 'static',
													resolve: {
														campaignId: function(){
															return campaignId;
														},
														recycleStatus: function() {
															return recycleStatus;
														},
														jsonString: function() {
															return jsonString;
														}
													}
												});
											};
											
											var RecycleStatusModalInstanceCtrl = function ($scope, $modalInstance, campaignId, recycleStatus, jsonString) {
												$scope.recycleStatus = recycleStatus;	
												$scope.closeRecycleStatusModal = function() {
													$scope.recycleStatusModalInstance.close();
												}
												
												/* START SEND RECYCLED LIST
												 * DATE : 26/04/2017
												 * REASON : Added to send selected recycled status list within & across campaigns to back-end */
												$scope.sendRecycleStatus = function() {
													NotificationService.clear();
													$scope.$parent.$parent.recycleLoading = true;
													recycleStatusService.save({
														id : campaignId,
														searchstring : encodeURIComponent(jsonString)
													},
														$scope.recycleStatus
													).$promise.then(function(data) {
														NotificationService.clear();
														$scope.recycleStatusModalInstance.close();
														$scope.$parent.$parent.recycleLoading = false;
														NotificationService.log('success', "Records recycled successfully.");
													},function(error) {
														$scope.recycleStatusModalInstance.close();
														$scope.$parent.$parent.recycleLoading = false;
													});
												};
												/* END SEND RECYCLED LIST */
											};
											/* END RECYCLE MODAL */
											
											
											/* START GET RECYCLED LIST
											 * DATE : 26/04/2017
											 * REASON : Added to get recycled list within & across campaigns */
											$scope.getRecycledList = function() {
												$scope.$parent.recycleLoading = true;
												var expressionGroup = {};
												
												for (var index=0; index< $scope.modal.prospectCriteria.expressions.length; index++) {
													var value = $scope.modal.prospectCriteria.expressions[index].bindValue ? $scope.modal.prospectCriteria.expressions[index].bindValue : undefined;
													if (value != undefined) {
														var newValue = [];
														for (var valIndex=0; valIndex<value.length; valIndex++) {
															newValue.push(value[valIndex].value);
														}
														if (newValue.length > 0){
														 $scope.modal.prospectCriteria.expressions[index].value = newValue;
														}
													}
												}
												
												$scope.searchstring.prospectattribute = $scope.modal.prospectCriteria;
												// create json string to send to back-end 
												var jsonString = JSON.stringify($scope.searchstring.prospectattribute,function(key,val) {
													if (key == '$$hashKey') {
														return undefined;
													}else{
														return val;
													}
													return val;
												});

												recycleStatusService.query({
													id : campaignId,
													searchstring : encodeURIComponent(jsonString)
												}).$promise.then(function(data) {
													$scope.$parent.recycleLoading = false;
													
													// hiding RecycleStatusModal if all acrossCampaignCount & withinCampaignCount are zero
													$scope.recycleStatusCountFlag = true;
													$scope.hideRecycleStatusModalFlag = true;
													angular.forEach(data,function(value, key) {
														if ($scope.recycleStatusCountFlag) {
															if (value.withinCampaignCount == 0 && value.acrossCampaignCount == 0) {
																$scope.hideRecycleStatusModalFlag = true;
															} else {
																$scope.recycleStatusCountFlag = false;
																$scope.hideRecycleStatusModalFlag = false;
															}
														}
													});
													if ($scope.hideRecycleStatusModalFlag) {
														NotificationService.log('error', 'There are no records to recycle');
													} else {
														$scope.openRecycleStatusModal(campaignId, data, jsonString);
													}
													
												},function(error) {
													$scope.$parent.recycleLoading = false;
													//$scope.$parent.loading = false;
												});
											};
											/* END GET RECYCLED LIST */

											
											//Show history of purchase
											
											$scope.showHistoryDetails = function() {
												var expressionGroup = {};
												$scope.$parent.$parent.loading = true;
												$rootScope.configurationModalInstance.close();
												
												$scope.searchstring.prospectHistory = {													
													"campaignId" : campaignId,
													"contactCommand" : "Purchase",
													"expressionGroup" : $scope.modal.prospectCriteria
												};

												var jsonString = JSON.stringify($scope.searchstring.prospectHistory,function(key,val) {
													if (key == '$$hashKey') {
														return undefined;
													}
													return val;
												});
												
												$scope.searchstring.purchasehistory = {															
														"campaignId" : campaignId
												};
												
												$state.go("recordspurchasehistory", {
								                    'campaignId' : campaignId
								                });	

											};
											
											$scope.confirmPivotReviewModal = function(campaignId) {
												$scope.pivotReviewStatus(campaignId);
												$scope.confirmPivotReviewModalInstance = $modal.open({
													scope : $scope,
													templateUrl : 'confirmPivotReviewModal.html',
													controller : ConfirmPivotReviewModalInstanceCtrl,
													windowClass : 'confirm-Purchase-modal',
													resolve: {
														campaignId: function(){
															return campaignId;
														}
													}
												});
											};

											var ConfirmPivotReviewModalInstanceCtrl = function ($scope, campaignId) {
												$scope.confirmPivotReview = function() {													
													$scope.moveBuyDataToCallables();
													$scope.confirmPivotReviewModalInstance.close();
												};
												$scope.cancel = function() {
													$scope.confirmPivotReviewModalInstance.close();
												}
											}

											$scope.openConfirmCampaignPurchaseModal = function(dsFlag, fileToUpload) {//dsFlag tru for Data source and false for file upload
												$scope.resetFlags();
												$scope.modal.purchaseCount = '';
												$scope.confirmPurchaseModalInstance = $modal.open({
													scope : $scope,
													templateUrl : 'confirmUploadContactsModal.html',
													controller : ConfirmCampaignPurchaseModalInstanceCtrl,
													windowClass : 'confirm-Purchase-modal',
													resolve: {
														dataSourceFlag: function(){
															return dsFlag;
														},
														file: function(){
															return fileToUpload;
														}
													}
												});
											};
											
											var ConfirmCampaignPurchaseModalInstanceCtrl = function ($scope, dataSourceFlag, file) {
												$scope.confirmPurchase = function() {													
													if (dataSourceFlag) {
														$scope.$parent.openPurchaseModal();
													} else {
														$scope.$parent.uploadContacts(file);
													}
													$scope.confirmPurchaseModalInstance.close();
												};
												
												$scope.cancel = function() {
													$scope.confirmPurchaseModalInstance.close();
													
												}
											}
																						
											$scope.openPurchaseModal = function() {
												$scope.resetFlags();
												$scope.modal.purchaseCount = '';
												$scope.purchaseModalInstance = $modal.open({
													scope : $scope,
													templateUrl : 'purchaseModal.html',
													controller : PurchaseModalInstanceCtrl,
													windowClass : 'Purchase-modal',
													resolve: {
														campaignId: function(){
															return campaignId;
														}
													}
												});
											};
											
											
											
											var PurchaseModalInstanceCtrl = function ($scope, campaignId) {
												$scope.purchase = function() {													
													$scope.validateErr = false;
													if($scope.modal.purchaseCount){
														$scope.validateErr = false;
														var expressionGroup = {};
														$scope.$parent.$parent.loading = true;
														
														$scope.searchstring.prospectattribute = {
															"count" : $scope.modal.purchaseCount,
															"campaignId" : campaignId,
															"contactCommand" : "Purchase",
															"expressionGroup" : $scope.modal.prospectCriteria
														};
	
														var jsonString = JSON.stringify($scope.searchstring.prospectattribute,function(key,val) {
															if (key == '$$hashKey') {
																return undefined;
															}
															return val;
														});
	
														purchaseService.purchase(jsonString).$promise.then(function(data) {
															$scope.listPurchaseTransactionId = data.listPurchaseTransactionId;
															NotificationService.log('success','Your purchase request has been submitted successfully. Track your purchase status in Purchase History');
															$scope.purchaseModalInstance.close();
															$scope.$parent.$parent.loading = false;
															
															
															//API for progressbar
															$scope.searchstring.prospectprogress = {
																	"count" : $scope.modal.purchaseCount,
																	"campaignId" : campaignId,
																	"contactCommand" : "PurchaseStatus",
																	"listPurchaseTransactionId" : $scope.listPurchaseTransactionId
																};
	
																var jsonString = JSON.stringify($scope.searchstring.prospectprogress,function(key,val) {
																	if (key == '$$hashKey') {
																		return undefined;
																	}
																	return val;
																});
																 $scope.waitPurchaseMsg = true;
																 $scope.getprogressBarTracker(jsonString);
																
															
														},function(error) {
															$scope.$parent.$parent.loading = false;
														});	
													  } else {
															$scope.validateErr = true;	
													  }
													};
												
												$scope.cancel = function() {
													$scope.purchaseModalInstance.close();
												}
											}
											
											$scope.convert_Number_to_Words = function(num) {
												
												function convert_billions(num){
												    if (num>=1000000000){
												        return convert_billions(Math.floor(num/1000000000))+" billion";
												    }
												    else {
												        return convert_millions(num);
												    }
												}
												
												function convert_millions(num){
												    if (num>=1000000){
												        return convert_millions(Math.floor(num/1000000))+" million";
												    }
												    else {
												        return num;
												    }
												}
												if (num==0) return "0";
												else return convert_billions(num);
											}

											/*end of data source*/
											$scope.PurchaseOutstanding = false;
											$scope.purchaseSuccess = false;
											$scope.purchaseFail = false;
											$scope.waitPurchaseMsg = false;
											/* Progress Bar */
											$scope.getprogressBarTracker = function (jsonString) {
												$scope.waitPurchaseMsg = true;
												var progressBarTrackerPromise = $interval(function(){//starting the timer by by 1 min interval
													purchaseService.purchase(jsonString).$promise.then(function(data) {
		                                    			  $scope.progressBarStatus = data;
		                                    			  $scope.purchaseOutstandingCount = parseInt(data.purchaseOutstandingCount);
														  $scope.purchasedStatusCount = parseInt(data.purchasedStatusCount);
														  $scope.transactionStatus = data.transactionStatus;
														  $scope.error = data.error;		                                    			
		                                    			  $scope.purchaseContact.progress =  parseInt(( $scope.purchasedStatusCount / $scope.modal.purchaseCount ) * 100);
		                                    			  if (!isNaN($scope.purchaseContact.progress) && $scope.purchaseContact.progress > 0) {
		                                    				  $scope.waitPurchaseMsg = false;
			                                    			  $scope.showCountProgressBar = true;
		                                    			  }
		                                    			  if ($scope.transactionStatus == "SUCCESS") {
		                                    				  $scope.waitPurchaseMsg = false;
		                                    				  $scope.showCountProgressBar = false;
		                                    				  $rootScope.purchaseContactFileUploadStart = false;//c
		                                    				  $scope.purchaseContact.progress = 100;
		                                    				  $interval.cancel(progressBarTrackerPromise);
		                                    				  $scope.PurchaseOutstanding = true;
                                    						  $scope.purchaseSuccess = true;		                                    				 
		                                    			  }
		                                    			  if ($scope.transactionStatus == "FAILURE" ) {
		                                    				  $scope.waitPurchaseMsg = false;
		                                    				  $scope.showCountProgressBar = false;
		                                    				  $rootScope.purchaseContactFileUploadStart = false;//c
		                                    				  $scope.purchaseContact.progress = 100;
		                                    				  $scope.purchaseFail = true;
	                                    					  $scope.PurchaseOutstanding = true;
	                                    					  $interval.cancel(progressBarTrackerPromise);		                                    				 
		                                    			  }
													});
												},2000);
											}
											
											/*start of activate asset*/
											$scope.activateAsset = function() {
												$scope.assetMap = [];
												$scope.createAssetMap();
												NotificationService.clear();
												$scope.$parent.loading = true;
												var activeAssets = {
													campaignId: campaignId,
													assetTargetMap: $scope.assetMap
												}
												ActivateAssetService.activate({
													campaignId: campaignId},activeAssets
												).$promise.then(function(data) {
													$scope.disableSaveAssetBtn = true;
													if ($scope.assetMap.length > $scope.oldAssetCount) {
														$scope.oldAssetCount = $scope.assetMap.length; 
														NotificationService.log('success','Asset activated Successfully.');
													} else if($scope.assetMap.length < $scope.oldAssetCount){
														$scope.oldAssetCount = $scope.assetMap.length; 
														NotificationService.log('success','Asset deactivated.');
													} else if($scope.assetMap.length == $scope.oldAssetCount){
														$scope.oldAssetCount = $scope.assetMap.length; 
														NotificationService.log('success','Asset updated Successfully.');
													}
													//  else {
													// 	NotificationService.log('success','Asset activated Successfully.');
													// }
													$scope.modal.currentCampaignId = "";
													$scope.modal.currentAssetId = "";
													$scope.$parent.loading = false;
												},function(error) {
													$scope.$parent.loading = false;
												});
											};
											
											$scope.createAssetMap = function() {
												angular.forEach($scope.modal.assetList, function(value, key) {
													if(value.checked) {
													var obj = {};
														obj[value.assetId] = value.assetTarget;
														$scope.assetMap.push(obj);
													}
												  });
											};

											$scope.activateAssetId = function(assetId) {
												$scope.modal.currentAssetId = assetId;
											};

											$scope.toggleCheck = function(event ,assetIndex) {
												$scope.disableSaveAssetBtn = false;
												if (event.target.checked) {
													$scope.modal.assetList[assetIndex].checked = true;
												} else {
													$scope.modal.assetList[assetIndex].checked = false;
												}
											}

											$scope.assetTargetChanged =  function (assetIndex) {
												if ($scope.modal.assetList[assetIndex].checked) {
													$scope.disableSaveAssetBtn = false;	
												}
											}

											$scope.invalidEmailTemplate = false;
											$scope.sendAssetEmail = function() {
												if ($scope.emailTemplateForm.$valid) {
													NotificationService.clear();
													var reg = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
													
													if(!reg.test($scope.modal.assetEmailFromEmail))
													{
														NotificationService.log('error','Please provide a valid Email Address');
														$scope.invalidEmailTemplate = true;
														$scope.emailTemplateForm.to.$error.required = true;
														return false;
													}
													
													$scope.$parent.loading = true;
													if ($scope.campaign.assetEmailTemplate != null && $scope.campaign.assetEmailTemplate != undefined && $scope.campaign.assetEmailTemplate.fromEmail == null && $scope.campaign.assetEmailTemplate.fromEmail == undefined) {
														$scope.campaign.assetEmailTemplate.fromEmail = "support+asset@xtaascorp.com";
													}else if($scope.campaign.assetEmailTemplate == null && $scope.campaign.assetEmailTemplate == undefined){
														var emailObj = {
															fromName: "",
															replyTo: "",
															subject: "",
															message: "",
															unSubscribeUrl: "",
															clickUrl: "",
															fromEmail: "support+asset@xtaascorp.com"
														  };
														$scope.campaign.assetEmailTemplate = emailObj;
													}
													$sendAssetObject = { 
															fromName : $scope.modal.assetEmailFromName,
															//replyTo : $scope.modal.assetEmailReplyTo,
															subject : $scope.modal.assetEmailSubject,
															message : $scope.modal.assetEmailMessage,
															fromEmail : $scope.modal.assetEmailFromEmail,
															unSubscribeUrl : $scope.modal.assetEmailUnSubscribeUrl,
															clickUrl : $scope.modal.assetEmailClickUrl
													};
													AssetEmailService.send({
																campaignId : campaignId,
																string : 'emailtemplate'
													},$sendAssetObject).$promise.then(function(data) {
																$scope.$parent.loading = false;
																NotificationService.log('success','Record updated successfully');
													},function(error) {
														$scope.$parent.loading = false;
													});
												} else {
													$scope.invalidEmailTemplate = true;
												}
												
											};
											
											/*end of activate asset*/
											
											/* START 
											 * REASON : Added to test email template*/
											$scope.invalidEmailTemplate = false;
											$scope.testEmailTemplate = function() {
												if ($scope.emailTemplateForm.$valid) {
													NotificationService.clear();
													var reg = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
													
													if(!reg.test($scope.modal.assetEmailFromEmail))
													{
														NotificationService.log('error','Please provide a valid Email Address');
														$scope.invalidEmailTemplate = true;
														$scope.emailTemplateForm.to.$error.required = true;
														return false;
													}
													
													$scope.$parent.loading = true;
													$sendAssetObject = { 
															fromEmail : $scope.modal.assetEmailFromEmail,
															fromName : $scope.modal.assetEmailFromName,
															replyTo : $scope.modal.assetEmailFromEmail,
															subject : $scope.modal.assetEmailSubject,
															message : $scope.modal.assetEmailMessage,
													};
													AssetEmailService.test({
														campaignId : campaignId,
														string : 'testemailtemplate'
													},$sendAssetObject).$promise.then(function(data) {
																$scope.$parent.loading = false;
																NotificationService.log('success','Email sent');
													},function(error) {
														$scope.$parent.loading = false;
													});													
												} else {
													$scope.invalidEmailTemplate = true;
												}												
											};
											/* END */

											/** =========== Download Template Start ===========**/
											$scope.downloadTemplate = function() {
												window.location.href = "/spr/agent/manage/downloadTemplate";
										    }
											/** =========== Download Template End ===========**/


											
											/** =========== Download Contact Template Start ===========**/
											$scope.downloadContactTemplate = function() {
												// window.location.href = "/spr/contact/downloadTemplate/:$scope.campaign.campaignId";
												window.location.href = "/spr/campaigncontact/" + $scope.campaign[0].campaignId + "/downloadTemplate";
											}
											/** =========== Download Contact Template End ===========**/

											/** =========== Upload Agent and QA creation start ===========**/
										    $scope.uploadAgentCreationFile = function(file, errFiles) {
												$scope.errFile = errFiles && errFiles[0];
										        if (file) {
													$scope.$parent.loading = true;
										            file.upload = Upload.upload({
										                url: 'spr/agent/manage/upload',
										                data: {file: file}
										            });
										            file.upload.then(function (response) {
										            	$scope.$parent.loading = false;
										                file.result = response.data;
														//NotificationService.log('success', 'File uploaded successfully.');
														NotificationService.log('success', 'File Upload Request Received. Please Check Your Email For Status.');
										            }, function (response) {
										            	$scope.$parent.loading = false;
										            });
										        }   
										    }
											/** =========== Upload Agent and QA creation end ===========**/
											$scope.undoAllocation = function(agentId) {
												var adminRole = $scope.loggedInUser.adminRole;
												$scope.$parent.loading = true;
												agentUnAllocationOverrideService.delete({agentId : agentId}, null).$promise.then(function(data) {
													var flagUnallocated = false;
													agentAllocationService.query().$promise.then(function(data) {
														var localdata = data.agentAllocationMap;
														$scope.modal.campaignAgent = [];
														angular.forEach($scope.campaignsList,function(v,k) {
															var flagCampign = false;
															angular.forEach(localdata,function(Value,Key) {
																if(Key == 'UNALLOCATED') {
																	flagUnallocated = true;
																}
																if (v.id === Key) {
																	flagCampign = true;
																	$scope.modal.campaignAgent.push({
																		'CampaignId' : Key,
																		'CampaignName' : v.name,
																		'Agents' : Value
																	});
																}
															});
																													
															if(flagCampign === false) {
																$scope.modal.campaignAgent.push({
																	'CampaignId' : v.id,
																	'CampaignName' : v.name,
																	'Agents' : []
																});
															}
														});
																							
														if(flagUnallocated  == true) {
															$scope.modal.campaignAgent.push({
																'CampaignId' : "UNALLOCATED",
																'CampaignName' : "UNALLOCATED",
																'Agents' : localdata.UNALLOCATED
															});
														} else {
															$scope.modal.campaignAgent.push({
																'CampaignId' : "UNALLOCATED",
																'CampaignName' : "UNALLOCATED",
																'Agents' : []
															});
														}
														$scope.$parent.loading = false;
													});
													
												},function(error) {
													$scope.$parent.loading = false;
												});
											}								              
											  
											$scope.dropSuccessHandler = function($event,index,array,CampaignId){
								            	
												$scope.$parent.loading = true;
								            	  if($scope.drop.campaignId === 'UNALLOCATED'){
								            		  var flagUnallocated = false;
								            		  agentAllocationService.query().$promise.then(function(data) {
								            			  var localdata = data.agentAllocationMap;
								            			  $scope.modal.campaignAgent = [];
								            			  angular.forEach($scope.campaignsList,function(v,k) {
								            				  var flagCampign = false;
								            				  angular.forEach(localdata,function(Value,Key) {
								            					  if(Key == 'UNALLOCATED') {
								            						  flagUnallocated = true;
								            					  }
								            					  if (v.id === Key) {
								            						  flagCampign = true;
								            						  $scope.modal.campaignAgent.push({
								            							  'CampaignId' : Key,
								            							  'CampaignName' : v.name,
								            							  'Agents' : Value
								            						  });
								            					  }
															  });
															  
								            				  if(flagCampign === false) {
								            					  $scope.modal.campaignAgent.push({
								            						  'CampaignId' : v.id,
								            						  'CampaignName' : v.name,
								            						  'Agents' : []
								            					  });
								            				  }
								            			  });
														
								            			  if(flagUnallocated  == true) {
								            				  $scope.modal.campaignAgent.push({
								            					  'CampaignId' : "UNALLOCATED",
								            					  'CampaignName' : "UNALLOCATED",
								            					  'Agents' : localdata.UNALLOCATED
								            				  });
								            			  } else {
								            				  $scope.modal.campaignAgent.push({
								            					  'CampaignId' : "UNALLOCATED",
								            					  'CampaignName' : "UNALLOCATED",
								            					  'Agents' : []
								            				  });
								            			  }
								            			  $scope.$parent.loading = false;
								            		  });
								            	  } else {
								            		  agentAllocationOverrideService.override({
								            			  agentId : $scope.drop.agentId,
									            		  campaignId : $scope.drop.campaignId
								            		  }, null).$promise.then(function(data) {
																			
								            			  // console.log(data);
								            			  $scope.$parent.loading = false;
								            		  },function(error) {
								            			  $scope.$parent.loading = false;
								            		  });
								            	  }
								            	  array.splice(index,1);
								              };
								              
								              $scope.onDrop = function($event,$data,array,campaignId){
								            	  $data.campaignTemporaryAllocated = true;
								            	  // console.log($data);
								            	  
								            	  $scope.drop.campaignId = campaignId;
								            	  $scope.drop.agentId = $data.id;
								            	  array.push($data);
								              };
								              
								              $scope.opencalendar = function(agentId){
								            	  $modalInstance.dismiss('cancel');
								            	  $state.go('calendar', {
								            		  'agentId' : agentId
								            	  });
								              }
								              
								              var count = 0;
								              $scope.showCalendarPopover =function(agentId){
								            	  if (count == 0) {
								            		  count = 1;
								            		  curMon = new Date().getMonth();
								            		  console.log(curMon);
								            		  if (agentId=='' || agentId==undefined || agentId==null) {
								            			  agentId =	$scope.currrentAgentId;
								            		  } else {
								            			  $scope.currrentAgentId = agentId;
								            		  }
								            		  setCurrentDate(moment());
								            		  $scope.popover.currentMonth = $filter('date')(new Date(currentDate), 'MMMM yyyy');
								            		  $scope.setagentCalendar();
								            	  }
								              }
								              
								              $scope.setagentCalendar = function(){
								            	  $scope.eventMonthYear = $filter('date')(new Date(currentDate), 'yyyy-MM');
								            	  $scope.newStartDate = Date.parse($scope.eventMonthYear+'-'+$scope.currentWeekStart());
								            	  $scope.newEndDate = Date.parse($scope.eventMonthYear+'-'+$scope.currentWeekEnd());
													
								            	  AgentCalendarService.query({
								            		  agentId: $scope.currrentAgentId,
								            		  searchstring: angular.toJson({"startDate":$scope.newStartDate, "endDate":$scope.newEndDate})
								            	  }).$promise.then(function(data) {
								            		  $scope.calData = {Mon:[],Tue:[], Wed:[], Thu:[], Fri:[] };
								            		  angular.forEach(data, function(v, k) {
								            			  var startDateNew = k.split("-");
								            			  var  eventstartDate = startDateNew[1]+'/'+startDateNew[2]+'/'+startDateNew[0];  
								            			  angular.forEach(v, function(x, z) {
								            				  v[z].startHour = x.startHour+':00:00'; 
								            				  v[z].endHour = x.endHour+':00:00';
								            				  v[z].from = new Date( eventstartDate + ' ,' +v[z].startHour).getTime();
								            				  v[z].startHour = $filter('date')( v[z].from, 'ha');
								            				  v[z].to = new Date( eventstartDate + ' ,' +v[z].endHour).getTime();
								            				  v[z].endHour = $filter('date')( v[z].to, 'ha');
								            			  });
								            			  $scope.calData[$filter('date')(k, 'EEE')] = v;
								            			  if ($filter('date')(k, 'EEE')=='Mon' || $filter('date')(k, 'EEE') !="Tue" || $filter('date')(k, 'EEE') !="Wed" || 
								            					  $filter('date')(k, 'EEE') !="Thu" || $filter('date')(k, 'EEE') !="Fri") {
								            				  if($filter('date')(k, 'EEE')=='Mon') {
								            					  $scope.calData.Mon = v;
								            				  } else if ($filter('date')(k, 'EEE')=='Tue') {
								            					  $scope.calData.Tue = v;
								            				  } else if ($filter('date')(k, 'EEE')=='Wed') {
								            					  $scope.calData.Wed = v;
								            				  } else if ($filter('date')(k, 'EEE')=='Thu') {
								            					  $scope.calData.Thu = v;
								            				  } else if ($filter('date')(k, 'EEE')=='Fri') {
								            					  $scope.calData.Fri = v;
								            				  }
								            			  }
											    			
								            		  });
								            		  console.log(JSON.stringify($scope.calData));
								            		  count = 0;
								            		  $scope.loading = false;
								            	  },function(error) {
								            		  count = 0;
								            		  $scope.loading = false;
								            	  });
								              }  
								              

								           // ////////////////////get next month///////////////////
								              $scope.nextMonth = function(){
								            	  console.log(curMon);
								            	  if (curMon == 12) {
								            		  curMon = parseInt(1);
								            	  } else {
								            		  curMon = parseInt(curMon) + parseInt(1);
								            	  }
								            	  $scope.popover.currentMonth = moment(currentDate).month(curMon).format("MMMM YYYY");
								            	  console.log($scope.popover.currentMonth);
								            	  console.log(curMon);
								            	  var nextmon = "";
								            	  var getDateVar = false;
								            	  
								            	  while (getDateVar == false){
								            		  setCurrentDate(currentDate.add(7,'days'));
								            		  nextmon = $filter('date')(new Date(currentDate), 'MMMM yyyy');
								            		  if((nextmon == $scope.popover.currentMonth )) {
								            			  getDateVar = true;
								            		  }
								            	  }
								            	  if (getDateVar == true) {
								            		  $scope.setagentCalendar();
								            	  }
								              }
												
								              $scope.prevMonth = function(){
								            	  if (curMon == 1) {
								            		  curMon = parseInt(11);
								            	  } else {
								            		  curMon = curMon - 2;
								            	  }
												
								            	  $scope.popover.currentMonth = moment(currentDate).month(curMon).format("MMMM YYYY");
												
								            	  var pretmon = "";
								            	  var i =1;
								            	  var getDateVar = false;
								            	  while (getDateVar == false) {
								            		  setCurrentDate(currentDate.subtract(7,'days'));
								            		  pretmon = $filter('date')(new Date(currentDate), 'MMMM yyyy');
													 
								            		  if((pretmon == $scope.popover.currentMonth ) || i >5) {
								            			  getDateVar = true;
								            			  console.log("Current : "+pretmon);
								            		  }
								            		  i++;
								            	  } 
								            	  if(getDateVar == true) {
								            		  $scope.nextMonth();
								            	  }
								              }
								              
								              var currentDate,
								              curMon = curMon = $filter('date')(new Date(), 'M'),
								              weekStart,
								              weekEnd,
								              shortWeekFormat = 'DD',
								              MonthFormat = 'MMMM YYYY';

								              function setCurrentDate(aMoment){
								            	  currentDate = aMoment,
								            	  weekStart = currentDate.clone().startOf('week'),
								            	  weekEnd = currentDate.clone().endOf('week')
								              }

								              setCurrentDate(moment());
											  
								              $scope.currentWeek = function(){ 
								            	  return currentDate.format(shortWeekFormat); 
								              };
								              
								              $scope.currentWeekStart = function(){ 
								            	  var wsf = parseInt(weekStart.format(shortWeekFormat)); 
								            	 /* if (wsf == 31) {
								            		  return 0;
								            	  } else {*/
								            		  return parseInt(weekStart.format(shortWeekFormat)); 
								            	 // }
								              };
								              
								              $scope.currentWeekEnd = function(){ return weekEnd.format(shortWeekFormat); };
								              $scope.currentMonth = function(){ return moment('MMMM YYYY'); };

								              $scope.nextWeek = function(){
								            	  setCurrentDate(currentDate.add(7,'days'));
								            	  $scope.popover.currentMonth = $filter('date')(new Date(currentDate), 'MMMM yyyy');
								            	  $scope.setagentCalendar();
								              };
								              
								              $scope.prevWeek = function(){
								            	  setCurrentDate(currentDate.subtract(7,'days'));
								            	  $scope.popover.currentMonth = $filter('date')(new Date(currentDate), 'MMMM yyyy');
								            	  $scope.setagentCalendar();
								              };

								              $scope.week = function(item) {
								            	  var eventTime = moment(item.jsdatetime);
								            	  return (eventTime >= weekStart && eventTime <= weekEnd);
								              };
											
											/*end of agent allocation code*/
								              
								            /*start of call speed*/
								              $scope.invalidCallSpeed = false;
								              $scope.setCallSpeed = function() {
								            	  if ($scope.callSpeedForm.$valid) {
								            		  NotificationService.clear();
								            		  $scope.$parent.loading = true;
									            	  var callSpeedObject = {
									            			  callSpeedPerMinPerAgent : $scope.modal.callSpeed
															////dialerMode : $scope.modal.dialerMode
									            	  };
									            	  CampaignTeamService.update({id : campaignId},callSpeedObject).$promise.then(function(data) {
															//$modalInstance.dismiss('cancel');
									            		  $scope.modal.currentCampaignId = "";
									            		  // console.log(data);
									            		  NotificationService.log('success','Call speed set Successfully');
									            		  $scope.$parent.loading = false;
									            		  //set call speed to campaign list object, for rendering in UI
									            			angular.forEach($scope.$parent.campaignsList,  function(campaign, index){
																if (campaign.id == campaignId) {
																	campaign.callSpeedPerMinPerAgent = $scope.modal.callSpeed;
																}
									            			});
																
									            	  },function(error) {
									            		  $scope.$parent.loading = false;
									            	  });
								            	  } else {
								            		  $scope.invalidCallSpeed = true;
								            	  }
								            	  
								              };
											/*===========end setCallSpeed =============*/
											
											/*===========Update campaign settings start Date:- 5-11-18 =============*/
								              $scope.updateCampaignSettings = function() {
												$scope.campaignSettingDTO.retrySpeed = $scope.campaign.retrySpeed; 
								            	  NotificationService.clear();
								            	  $scope.$parent.loading = true;
								            	  CampaignSettingService.update({campaignId : campaignId}, $scope.campaignSettingDTO).$promise.then(function(data) {
								            		  $scope.$parent.loading = false;
								            		  NotificationService.log('success', "Campaign settings updated successfully.");
								            	  }, function(error) {
								            		  $scope.$parent.loading = false;
								            		  NotificationService.log('error', "Error occurred while updating campaign settings.");
								            	  });
											  }
											 /*===========Update campaign settings end Date:- 5-11-18 =============*/

											 /*===========Update campaign settings start Date:- 5-11-18 =============*/
											 $scope.moveBuyDataToCallables = function() {
												NotificationService.clear();
												$scope.$parent.loading = true;
													MoveDataToCallableService.update({campaignId : campaignId}, {}).$promise.then(function(data) {
														var prospectCount = "";
														for (let index = 0; index < 9; index++) {
															const character = data[index];
															if (character != undefined) {
																prospectCount += character; 	
															}
														}
														$scope.$parent.loading = false;
														$scope.disableActivateDataButton = true;
														NotificationService.log('success', prospectCount + " prospects moved to callables successfully.");
														$scope.pivotReviewStatus(campaignId);
													}, function(error) {
														$scope.$parent.loading = false;
														NotificationService.log('error', "Error occurred while moving data to callables.");
													});
												
											}
										   /*===========Update campaign settings end Date:- 5-11-18 =============*/
								              
								            /* ===========start set Lead Sorting Order =============*/
								              $scope.invalidLeadSortingOrder = false;
								              $scope.setLeadSortingOrder = function() {
								            	  if ($scope.leadSortingOrderForm.$valid)  {
								            		  NotificationService.clear();
								            		  $scope.$parent.loading = true;
									            	  var leadSortingOrderObject = {
															leadSortOrder : $scope.modal.leadSortOrder
									            	  };
									            	  CampaignTeamService.update({id : campaignId},leadSortingOrderObject).$promise.then(function(data) {
									            		  $scope.modal.currentCampaignId = "";
									            		  //$modalInstance.dismiss('cancel');
									            		  // console.log(data);
									            		  NotificationService.log('success','Lead sorting order set Successfully');
									            		  $scope.$parent.loading = false;
									            		  //set lead sort order to campaign list object, for rendering in UI
									            		  angular.forEach($scope.$parent.campaignsList,  function(campaign, index){
																if (campaign.id == campaignId) {
																	campaign.leadSortOrder = $scope.modal.leadSortOrder;
																}
									            			});
									            	  },function(error) {
									            		  $scope.$parent.loading = false;
									            	  });
								            	  } else {
								            		  $scope.invalidLeadSortingOrder = true;
								            	  }
								            	  
								              };
							              /* ===========end set Lead Sorting Order =============*/
								          /* ===========start idnc =============*/
								              
								              $scope.nextPage = function() {
	  								            	console.log($scope.main.pages);
	  								                if ($scope.main.page < $scope.main.pages) {
	  								                    $scope.main.page++;
	  								                    $scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
	  								                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
	  								                    if($scope.idncTotalRecord < $scope.main.end) {
	  								                    	$scope.main.end = $scope.idncTotalRecord;
	  								                    }
	  								                    $scope.filterIdncList();
	  								                }
								              };
	  								            
	  								            $scope.previousPage = function() {
	  								                if ($scope.main.page > 0) {
	  								                    $scope.main.page--;
	  								                    $scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
	  								                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
	  								                    if($scope.idncTotalRecord < $scope.main.end) 
	  								                    {
	  								                    	$scope.main.end = $scope.idncTotalRecord;
	  								                    }
	  								                    $scope.filterIdncList();
	  								                }
	  								            };
	  	                                  	  
	  	                                  	  $scope.showrows = function(){
	  	                                  		$scope.main.page = 0;
	  	                                  		$scope.main.pages = parseInt($scope.idncTotalRecord/$scope.main.take);
	  	                                  		$scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
	  						                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
	  						                    if($scope.idncTotalRecord < $scope.main.end) {
	  						                    	$scope.main.end = $scope.idncTotalRecord;
	  						                    }
	  	                                  		$scope.filterIdncList();
	  	                                  	  }
								              
								              $scope.search = function() {
								            	  $scope.filterIdncList();
								              }
								              
								              $scope.filterIdncList = function(phoneNumber){
								            	  $scope.$parent.loading = true;
								            	  idncService.query({searchstring: angular.toJson({"phoneNumber" : phoneNumber != undefined ? phoneNumber : "","partnerId":user.organization, "page" : $scope.main.page, "size" : $scope.main.take})}, null).$promise.then(function(data) { 
								            		  $scope.idnclist = data.dncNumbers;
								            		  $scope.idncTotalRecord = data.dncNumberCount;
								            		  $scope.$parent.loading = false;
								            	  },function(error) {
								            		  $scope.$parent.loading = false;
								            	  });
								              }
								              
								              $scope.addNewIdnc  =  function(){
													$scope.modal.firstName = '';  
													$scope.modal.lastName = '';
													$scope.modal.pnum = '';
			  	                                	$scope.modal.note = '';
			  	                                	var modalInstance = $modal.open({
														scope : $scope,
														templateUrl : '/partials/addnewidnc.html',
														controller : AddNewIdncModalInstanceCtrl,
														windowClass : 'addnewidnc',
														resolve: {
															campaignId: function(){
																return campaignId;
															},
															user : function () {
																return user;
															}
														}
													});
											  };
											  
											  $scope.uploadBulkDNCList = function (campaignId, file, errFiles) {
												$scope.errFile = errFiles && errFiles[0];
												if (file) {
														$scope.$parent.loading = true;
														file.upload = Upload.upload({
																url: 'spr/idnc/bulkupload/' + campaignId,
																data: { file: file }
														});
														file.upload.then(function (response) {
																$scope.$parent.loading = false;
																file.result = response.data;
																NotificationService.log('success', 'File Upload Request Received. Please Check Your Email For Status.');
														}, function (response) {
																$scope.$parent.loading = false;
														});
												}
										     }
								              
								          /* ===========end idnc =============*/ 
											
											$scope.cancel = function() {
												$scope.configurationModalInstance.close();
												$interval.cancel($scope.dataBuyTrackerPromise);
												$scope.listOfCriteria = [];
											}
										}
										
										$scope.getReport = function (campaignId) {
											// $scope.showReportLoading = true;
											// angularLoad.loadScript('/js/vendor/visualize.js').then(function() {
											//     angularLoad.loadScript('/js/report-controller.js').then(function() {
											// 	    $scope.reportModalInstance = $modal.open({
											// 			scope : $scope,
											// 			templateUrl : 'reportModal.html',
											// 			controller  :  ReportModalInstanceCtrl,
											// 			backdrop: 'static',
											// 			windowClass : 'report-modal-modal',
											// 			show: false,
		  		                            //     		resolve: {
		  		                            //     			campaignId: function(){
			  		                    	// 	                return campaignId;
			  		                    	// 	            }
		  		                            //     		}
											// 		});
											// 	}).catch(function() {
											// 		NotificationService.log('error','There was some error loading the script');
											// 		$scope.showReportLoading = false;
											// 	});
											// }).catch(function() {
											// 	NotificationService.log('error','There was some error loading the script');
											// 	$scope.showReportLoading = false;
											// });
										}

										/* START
										 * DATE : 19/04/2017
										 * REASON : Added Pagination functionality for "Campaigns" Tab & "Configurations" Tab */
										
										/* START PAGINATION CODE FOR CAMPAIGNS TAB  */
											  // below method will be called when user changes drop down rows or when changes page
											  $scope.pageChanged = function(currentPage, pageSize) {
												  $scope.loading = true;
												  SupervisorCampaignService.get({
													  searchstring :angular.toJson({
														  "clause": "BySupervisor", 
														  "sort" : "name", 
														  "direction" : "ASC", 
														  "page" : currentPage-1, 
														  "size" : pageSize, 
														  "flag" : "true"
													  })
												  }).$promise.then(function(data) {
														  $scope.campaignsList = data.campaignsList;
														  $scope.campaignCount =  data.campaignCount;
														  $scope.campaignLength =  $scope.campaignsList.length;
														  $scope.showDataUpload = data.prospectUploadEnabled;
														  $scope.noSize = false;
														  if ($scope.campaignLength < 10 ) {
															  $scope.noSize = true;
														  }
														  $scope.loading = false;
														  $scope.getCampaignMetrics();
														  if ($scope.loggedInUserOrganization.organizationLevel != undefined && $scope.loggedInUserOrganization.organizationLevel != "PARTNER") {
															$scope.getCampaignCallableRecord();
														  }
													},function(error) {
														$scope.loading = false;   
													});
											  } 
										/* END PAGINATION CODE FOR CAMPAIGN TAB */

										/* START PAGINATION CODE FOR CONFIGURATIONS TAB  */
											// below method will be called when user changes drop down rows or when changes page
											  $scope.pageChangedConfigTab = function(currentPageConfigTab, pageSizeConfigTab) {
												  $scope.currentPage = currentPageConfigTab;
												  $scope.pageSize = pageSizeConfigTab;
												  $scope.loading = true;
												  SupervisorCampaignService.get({
													  searchstring :angular.toJson({
														  "clause": "BySupervisor", 
														  "sort" : "name", 
														  "direction" : "ASC", 
														  "page" : currentPageConfigTab-1, 
														  "size" : pageSizeConfigTab
													  })
												  }).$promise.then(function(data) {
														  $scope.campaignsList = data.campaignsList;
														  $scope.campaignCountConfigTab =  data.campaignCount;
														  $scope.campaignLength =  $scope.campaignsList.length;
														  $scope.showDataUpload = data.prospectUploadEnabled;
														  $scope.noSizeConfigTab = false;
														  if ($scope.campaignLength < 10 ) {
															  $scope.noSizeConfigTab = true;
														  }
														  $scope.loading = false;
														  $scope.getCampaignMetrics();
														  if ($scope.loggedInUserOrganization.organizationLevel != undefined && $scope.loggedInUserOrganization.organizationLevel != "PARTNER") {
															$scope.getCampaignCallableRecord();
														  }
													},function(error) {
														$scope.loading = false;   
													});
											  }
										/* END PAGINATION CODE FOR CONFIGURATION TAB  */
										
										/* END */
										
										/* START
										 * DATE : 25/05/2017
										 * REASON : Below function is used to clear multi-select search box (it's a hack. it is not required if we remove "close-on-select" from "ui-select" tag) */
										$scope.clearMultiselectBox = function ($select) {
									        $select.search = '';	// clear search text
									    }
										/* END */

										$scope.setAgentMode = function(agent) {
											if (agent != null && agent != undefined && agent != '') {
												$scope.loading = true;
												agentModeChangeService.get({
													agentId: agent.agentId, agentMode: agent.agentType
												}).$promise.then(function(data) {
													$scope.loading = false;
													NotificationService.log('success','Agent mode changed successfully.');
												}, function(error) {
													$scope.loading = false;
													NotificationService.log('error', 'Error occurred while changing agent mode.');
												});
											}
										}
											  
		} ]).filter('toPercent', function() {
		    return function (value) {
				return parseFloat(value).toFixed(2);
		    }
		}).directive('myEnter', function () {
		    return function (scope, element, attrs) {
		        element.bind("keydown keypress", function (event) {
		            if(event.which === 13) {
		                scope.$apply(function (){
		                    scope.$eval(attrs.myEnter);
		                });

		                event.preventDefault();
		            }
		        });
		    };
		}).controller('RetrySpeedSettingController', function ($scope, $modalInstance, CampaignSettingService, NotificationService) {
			
			// Below line binds the retryspeed setting values from campaign to modal.
			$scope.retrySpeed = $scope.$parent.campaign.retrySpeed;
			// Sets the retryspeed.
			$scope.setRetrySpeed = function() {
				$scope.$parent.campaignSettingDTO.retrySpeed = $scope.retrySpeed;
				$scope.$parent.campaign.retrySpeed = $scope.retrySpeed;
				$scope.updateCampaignSettings();
			}

			$scope.updateCampaignSettings = function() {
				$scope.$parent.loading = true;
				CampaignSettingService.update({campaignId : $scope.$parent.campaign.campaignId}, $scope.$parent.campaignSettingDTO).$promise.then(function(data) {
					$scope.$parent.loading = false;
					NotificationService.log('success', "Retry settings updated successfully.");
					$modalInstance.dismiss('cancel');
				}, function(error) {
					$scope.$parent.loading = false;
					NotificationService.log('error', "Error occurred while updating retry settings.");
					$modalInstance.dismiss('cancel');
				});
			}
			$scope.cancel = function () {
				$scope.retrySpeed = {};
                $modalInstance.dismiss('cancel');
			};
        }).directive('numbersOnly', function () {
			return {
				require: 'ngModel',
				link: function (scope, element, attr, ngModelCtrl) {
					function fromUser(text) {
						if (text) {
							var transformedInput = text.replace(/[^0-9]/g, '');
		
							if (transformedInput !== text) {
								ngModelCtrl.$setViewValue(transformedInput);
								ngModelCtrl.$render();
							}
							return transformedInput;
						}
						return undefined;
					}            
					ngModelCtrl.$parsers.push(fromUser);
				}
			};
		}).directive('ngDropdownMultiselect', ['$filter', '$document', '$compile', '$parse',

		function ($filter, $document, $compile, $parse) {
		
			return {
				restrict: 'AE',
				scope: {
					selectedModel: '=',
					campaign: '=',
					options: '=',
					extraSettings: '=',
					events: '=',
					searchFilter: '=?',
					translationTexts: '=',
					groupBy: '@'
				},
				template: function (element, attrs) {
					var checkboxes = attrs.checkboxes ? true : false;
					var groups = attrs.groupBy ? true : false;
		
					var template = '<div class="multiselect-parent btn-group dropdown-multiselect">';
					template += '<button type="button" class="dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">{{getButtonText()}}&nbsp;<span class="caret"></span></button>';
					template += '<ul class="customheight dropdown-menu dropdown-menu-form" ng-style="{display: open ? \'block\' : \'none\', height : settings.scrollable ? settings.scrollableHeight : \'auto\' }">';
					template += '<li ng-hide="!settings.showCheckAll || settings.selectionLimit > 0"><a data-ng-click="selectAll()"><span class="glyphicon glyphicon-ok"></span>  {{texts.checkAll}}</a>';
					template += '<li ng-show="settings.showUncheckAll"><a data-ng-click="deselectAll();"><span class="glyphicon glyphicon-remove"></span>   {{texts.uncheckAll}}</a></li>';
					template += '<li ng-hide="(!settings.showCheckAll || settings.selectionLimit > 0) && !settings.showUncheckAll" class="divider"></li>';
					template += '<li ng-show="settings.enableSearch"><div class="dropdown-header"><input type="text" class="form-control" style="width: 100%;" ng-model="searchFilter" placeholder="{{texts.searchPlaceholder}}" /></li>';
					template += '<li ng-show="settings.enableSearch" class="divider"></li>';
		
					if (groups) {
						template += '<li ng-repeat-start="option in orderedItems | filter: searchFilter" ng-show="getPropertyForObject(option, settings.groupBy) !== getPropertyForObject(orderedItems[$index - 1], settings.groupBy)" role="presentation" class="dropdown-header">{{ getGroupTitle(getPropertyForObject(option, settings.groupBy)) }}</li>';
						template += '<li ng-repeat-end role="presentation">';
					} else {
						template += '<li role="presentation" ng-repeat="option in options | filter: searchFilter">';
					}
		
					template += '<a role="menuitem" tabindex="-1">';
		
					if (checkboxes) {
						template += '<div class="checkbox"><label><input class="checkboxInput" type="checkbox" ng-click="checkboxClick($event, getPropertyForObject(option,settings.idProp));" ng-checked="isChecked(getPropertyForObject(option,settings.idProp))" /> {{getPropertyForObject(option, settings.displayProp)}}</label></div></a>';
					} else {
						template += '<span data-ng-class="{\'glyphicon glyphicon-ok\': isChecked(getPropertyForObject(option,settings.idProp))}"></span> {{getPropertyForObject(option, settings.displayProp)}}</a>';
					}
		
					template += '</li>';
		
					template += '<li class="divider" ng-show="settings.selectionLimit > 1"></li>';
					template += '<li role="presentation" ng-show="settings.selectionLimit > 1"><a role="menuitem">{{selectedModel.length}} {{texts.selectionOf}} {{settings.selectionLimit}} {{texts.selectionCount}}</a></li>';
		
					template += '</ul>';
					template += '</div>';
		
					element.html(template);
				},
				link: function ($scope, $element, $attrs) {
					var $dropdownTrigger = $element.children()[0];
		
					$scope.toggleDropdown = function () {
						$scope.open = !$scope.open;
					};
		
					$scope.checkboxClick = function ($event, id) {
						if (event.target.checked) {
							$scope.$parent.assignCampaign($scope.campaign, id);
						} else {
							$scope.$parent.unAssignCampaign($scope.campaign, id);
						}
						$scope.setSelectedItem(id);
						$event.stopImmediatePropagation();
					};
		
					$scope.externalEvents = {
						onItemSelect: angular.noop,
						onItemDeselect: angular.noop,
						onSelectAll: angular.noop,
						onDeselectAll: angular.noop,
						onInitDone: angular.noop,
						onMaxSelectionReached: angular.noop
					};
		
					$scope.settings = {
						dynamicTitle: true,
						scrollable: false,
						scrollableHeight: '300px',
						closeOnBlur: true,
						displayProp: 'label',
						idProp: 'id',
						externalIdProp: 'id',
						enableSearch: false,
						selectionLimit: 0,
						showCheckAll: true,
						showUncheckAll: true,
						closeOnSelect: false,
						buttonClasses: 'btn btn-default',
						closeOnDeselect: false,
						groupBy: $attrs.groupBy || undefined,
						groupByTextProvider: null,
						smartButtonMaxItems: 0,
						smartButtonTextConverter: angular.noop
					};
		
					$scope.texts = {
						checkAll: 'Select All',
						uncheckAll: 'Unselect All',
						selectionCount: 'Agent Selected',
						selectionOf: '/',
						searchPlaceholder: 'Search...',
						buttonDefaultText: 'Select',
						dynamicButtonTextSuffix: 'Agent Selected'
					};
		
					$scope.searchFilter = $scope.searchFilter || '';
		
					if (angular.isDefined($scope.settings.groupBy)) {
						$scope.$watch('options', function (newValue) {
							if (angular.isDefined(newValue)) {
								$scope.orderedItems = $filter('orderBy')(newValue, $scope.settings.groupBy);
							}
						});
					}
		
					angular.extend($scope.settings, $scope.extraSettings || []);
					angular.extend($scope.externalEvents, $scope.events || []);
					angular.extend($scope.texts, $scope.translationTexts);
		
					$scope.singleSelection = $scope.settings.selectionLimit === 1;
		
					function getFindObj(id) {
						var findObj = {};
						if ($scope.settings.externalIdProp === '') {
							findObj[$scope.settings.idProp] = id;
						} else {
							findObj[$scope.settings.externalIdProp] = id;
						}
						return findObj;
					}
		
					function clearObject(object) {
						for (var prop in object) {
							delete object[prop];
						}
					}
		
					if ($scope.singleSelection) {
						if (angular.isArray($scope.selectedModel) && $scope.selectedModel.length === 0) {
							clearObject($scope.selectedModel);
						}
					}
		
					if ($scope.settings.closeOnBlur) {
						$document.on('click', function (e) {
							var target = e.target.parentElement;
							var parentFound = false;
							while (angular.isDefined(target) && target !== null && !parentFound) {
								if (_.contains(target.className.split(' '), 'multiselect-parent') && !parentFound) {
									if (target === $dropdownTrigger) {
										parentFound = true;
									}
								}
								target = target.parentElement;
							}
							if (!parentFound) {
								$scope.$apply(function () {
									$scope.open = false;
								});
							}
						});
					}
		
					$scope.getGroupTitle = function (groupValue) {
						if ($scope.settings.groupByTextProvider !== null) {
							return $scope.settings.groupByTextProvider(groupValue);
						}
						return groupValue;
					};
		
					$scope.getButtonText = function () {
						if ($scope.settings.dynamicTitle && ($scope.selectedModel != undefined)) {
							if ($scope.settings.smartButtonMaxItems > 0) {
								var itemsText = [];
		
								angular.forEach($scope.options, function (optionItem) {
									if ($scope.isChecked($scope.getPropertyForObject(optionItem, $scope.settings.idProp))) {
										var displayText = $scope.getPropertyForObject(optionItem, $scope.settings.displayProp);
										var converterResponse = $scope.settings.smartButtonTextConverter(displayText, optionItem);
		
										itemsText.push(converterResponse ? converterResponse : displayText);
									}
								});
		
								if ($scope.selectedModel.length > $scope.settings.smartButtonMaxItems) {
									itemsText = itemsText.slice(0, $scope.settings.smartButtonMaxItems);
									itemsText.push('...');
								}
		
								return itemsText.join(', ');
							} else {
								var totalSelected;
								if ($scope.singleSelection) {
									totalSelected = ($scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp])) ? 1 : 0;
								} else {
									totalSelected = angular.isDefined($scope.selectedModel) ? $scope.selectedModel.length : 0;
								}
								if (totalSelected === 0) {
									return $scope.texts.buttonDefaultText;
								} else {
									if (totalSelected == undefined) {
										totalSelected = '';
									}
									return totalSelected + ' ' + $scope.texts.dynamicButtonTextSuffix;
								}
							}
						} 
						else {
							return $scope.texts.buttonDefaultText;
						}
					};
		
					$scope.getPropertyForObject = function (object, property) {
						if (angular.isDefined(object) && object.hasOwnProperty(property)) {
							return object[property];
						}
						return '';
					};
		
					$scope.selectAll = function () {
						if (confirm(" Are you sure to move all agents to " + $scope.campaign.CampaignName + " campaign?")) {
							$scope.deselectAll(false, "NOT-CALL-DESELECTALL-METHOD");
							$scope.externalEvents.onSelectAll();
							$scope.$parent.assignCampaign($scope.campaign, "assignAllAgents");
							angular.forEach($scope.options, function (value) {
								$scope.setSelectedItem(value[$scope.settings.idProp], true);
							});
						}
					};
		
					$scope.deselectAll = function (sendEvent, value) {
						if (value != "NOT-CALL-DESELECTALL-METHOD") {
							if (confirm(" Are you sure to move assigned agents to available pool?")) {
								sendEvent = sendEvent || true;
								if (value != "NOT-CALL-DESELECTALL-METHOD") {
									$scope.$parent.unAssignCampaign($scope.campaign, "unAssignAllAgents");
								}
								if (sendEvent) {
									$scope.externalEvents.onDeselectAll();
								}
								if ($scope.singleSelection) {
									clearObject($scope.selectedModel);
								} else {
									$scope.selectedModel.splice(0, $scope.selectedModel.length);
								}
						    }
						}
					};
		
					$scope.setSelectedItem = function (id, dontRemove) {
						var findObj = getFindObj(id);
						var finalObj = null;
						if ($scope.settings.externalIdProp === '') {
							finalObj = _.find($scope.options, findObj);
						} else {
							finalObj = findObj;
						}
						if ($scope.singleSelection) {
							clearObject($scope.selectedModel);
							angular.extend($scope.selectedModel, finalObj);
							$scope.externalEvents.onItemSelect(finalObj);
							if ($scope.settings.closeOnSelect) $scope.open = false;
							return;
						}
						dontRemove = dontRemove || false;
						var exists = _.findIndex($scope.selectedModel, findObj) !== -1;
						if (!dontRemove && exists) {
							$scope.selectedModel.splice(_.findIndex($scope.selectedModel, findObj), 1);
							$scope.externalEvents.onItemDeselect(findObj);
						} else if (!exists && ($scope.settings.selectionLimit === 0 || $scope.selectedModel.length < $scope.settings.selectionLimit)) {
							$scope.selectedModel.push(finalObj);
							$scope.externalEvents.onItemSelect(finalObj);
						}
						if ($scope.settings.closeOnSelect) $scope.open = false;
					};
		
					$scope.isChecked = function (id) {
						if ($scope.singleSelection) {
							return $scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp]) && $scope.selectedModel[$scope.settings.idProp] === getFindObj(id)[$scope.settings.idProp];
						}
						return _.findIndex($scope.selectedModel, getFindObj(id)) !== -1;
					};
					$scope.externalEvents.onInitDone();
					
				}
			};
		}]);
	});
