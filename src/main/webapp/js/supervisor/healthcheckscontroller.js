//define(['angular', 'notification-service', 'supervisor/supervisor-service'],
	function (angular) {
		angular.module('HealthChecksController', ['notification-service', 'supervisor-service'])
			.controller('HealthChecksController', [
				'$scope',
				'$rootScope',
				'$modal',
				'$state',
				'$stateParams',
				'ProspectAttributeService',
				'healthCheckService',
				'abmListCountService',
				'NotificationService',
				function ($scope, $rootScope, $modal, $state, $stateParams, ProspectAttributeService, healthCheckService, abmListCountService, NotificationService) {

					$scope.modal = {};
					$scope.isResponseReceived = false;
					// $scope.allCampaignList = [];
					// $scope.allCampaignList = allcampaignlist;
					// $scope.campaignsList = allcampaignlist.campaignsList;
					$scope.loadData = function () {
						$scope.searchstring = {};
						$scope.modal.expressionAttributes = [];
						$scope.searchstring.prospectattribute = {
							"dataProvider": "NetProspexDataProvider",
							"expressionGroup": {
								"groupClause": "AND",
								"expressions": [{
									"attribute": "mgmtLevel",
									"operator": "IN",
									"value": "C, VP"
								}]
							},
							"size": 1
						};
						ProspectAttributeService.query({
							searchstring: JSON.stringify($scope.searchstring.prospectattribute)
						}).$promise.then(function (ProspectData) {
							$scope.modal.prospectCriteria = {
								groupClause: "AND",
								expressions: []
							};
							$scope.modal.prospectAttribute = ProspectData;
							$scope.addExpression($scope.modal.prospectCriteria.expressions);
						}, function () {
							$scope.loading = false;
						});
					}
					$scope.loadData();

					$scope.getABMListCount = function () {
						$scope.abmListCount = '';
						abmListCountService.query({campaignId : $stateParams.campaignId}).$promise.then(function (data) {
							if(data != undefined && data != null) {
								for(i = 0; i <= 15; i++) {
									if (data[i] != undefined) {
										$scope.abmListCount = $scope.abmListCount + data[i];
									}
								}
							}
						}, function (error) {
							console.log("Error occured while fetching abmlist count.");
						});
					}
					$scope.getABMListCount();

					$scope.addExpression = function (parent) {
						parent.push({});
					};

					$scope.navigateToSupervisorScreen = function() {
						$state.go('supervisor');
					}

					$scope.getHealthCheck = function () {
						var healthCheckDTO = {
							campaignId: $stateParams.campaignId,
							criteria: $scope.modal.prospectCriteria.expressions
						}
						$scope.convertDecimalValueToInt($scope.modal.prospectCriteria.expressions);
						if ($scope.validateHealthCheckCriteria($scope.modal.prospectCriteria.expressions)) {
							healthCheckService.query(healthCheckDTO).$promise.then(function (data) {
								$scope.healthCheckDto = data;
								$scope.isResponseReceived = true;
								if ($scope.healthCheckDto.totalRecords > 0) {
									NotificationService.log('success', $scope.healthCheckDto.totalRecords + ' Records removed successfully.');
								} else {
									NotificationService.log('success', ' No records found for health checks.');
								}
							}, function (error) {
								console.log("Error occured while fetching health checks.");
							});
						}
					}

					$scope.validateHealthCheckCriteria = function(healthCheckCriteria) {
						var isMandatoryFieldsEntered = true;
						angular.forEach(healthCheckCriteria, function(criteria, key) {
							if (criteria.attribute == null || criteria.attribute == undefined || criteria.attribute == '') {
								NotificationService.log('error', 'Please select criteria.');
								isMandatoryFieldsEntered = false;
								return false;
							}
							if (criteria.operator == null || criteria.operator == undefined || criteria.operator == '') {
								NotificationService.log('error', 'Please select operator.');
								isMandatoryFieldsEntered = false;
								return false;
							}
							if (criteria.value.length == 0 || criteria.value == undefined || criteria.value == '') {
								NotificationService.log('error', 'Please enter value.');
								isMandatoryFieldsEntered = false;
								return false;
							} 
						});
						if (isMandatoryFieldsEntered) {
							return true;
						}
					}

					$scope.convertDecimalValueToInt = function(healthCheckCriteria) {
						angular.forEach(healthCheckCriteria, function(criteria, key) {
							if (criteria.value != undefined && criteria.value != '') {
								if (!isNaN(criteria.value)) {
									criteria.value = parseInt(criteria.value);
								}
							} 
						});
					}

					$scope.removeExpression = function (parent, index) {
						parent.splice(index, 1);
						$scope['size' + index] = 0;
					};
				}])
	});