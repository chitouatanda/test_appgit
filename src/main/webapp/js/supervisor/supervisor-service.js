define(['angular', 'angular-resource'], function(angular) {
    angular.module('supervisor-service', ['ngResource'])
    .factory("CallGuideService", function ($resource) { 
        return $resource("/spr/campaign/:id/callguide", null, {
          update: {
             method: 'PUT',
             headers : {
                 'Content-Type': 'application/json'
             }
          },
          save: {
              method: 'POST',
              headers : {
                  'Content-Type': 'application/json'
              }
          },
          query: {
              method : 'GET',
              isArray:false
          }
        });
    }).factory("SupervisorNoticeService", function ($resource) { 
        return $resource("/spr/teamnotice", {}, {
            save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
            get: {
                method : 'GET',
                isArray:true
            }
          });
      }).factory("CampaignTeamService", function ($resource) { 
        return $resource("/spr/campaign/:id/campaignteam", null, {
          update: {
             method: 'PUT',
             headers : {
                 'Content-Type': 'application/json'
             }
          },
          save: {
              method: 'POST',
              headers : {
                  'Content-Type': 'application/json'
              }
          },
          query: {
              method : 'GET',
              isArray:false
          }
        });
    }).factory("ProspectAttributeService", function ($resource) {
        return $resource("spr/masterdata/prospectattribute?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:true
            }
        });
    }).factory("contactSearchService", function ($resource) {
        return $resource("spr/contact?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    })
    /* STARTS
     * DATE : 26/04/2017
     * REASON : Added to get list of recycle status */
    .factory("recycleStatusService", function ($resource) {
        return $resource("spr/recycled", null, {
            query: {
                method : 'GET',
                isArray:true,
                url: "spr/recycled?id=:id&searchstring=:searchstring"
            },
            save: {
                method : 'POST',
                url: "spr/recycled?id=:id&searchstring=:searchstring",
                headers : {
                    'Content-Type': 'application/json'
                }
            }
        });
    })
    /* END */
    .factory("purchaseService", function ($resource) {
        return $resource("/spr/contact/contactcommand", null, {
            purchase: {
          method: 'POST',
             headers : {
                 'Content-Type': 'application/json'
             },
            }
        });
    }).factory("purchaseHistoryService", function ($resource) {
        return $resource("spr/campaigncontact/:campaignId/listpurchasehistory", null, {
            query: {
                method : 'GET',
                isArray:true
            }
        });
    }).factory("purchaseContactsService", function ($resource) {
        return $resource("spr/campaigncontact/:campaignId/listpurchasehistory/:purchasedTransactionId", null, {
            query: {
                method : 'GET',
                isArray:true
            }
        });
    }).factory("campaignCallableRecordService", function ($resource) {
        return $resource("spr/prospect/:campaignid/callable/count", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    }).factory("MetricsService", function ($resource) {
        return $resource("spr/campaign/metrics?campaignids=:campaignids&metrics=:metrics", null, {
            query: {
                method : 'GET',
                isArray:true
            }
        });
    }).factory("preferenceMetricsService", function ($resource) {
        return $resource("spr/preference/CampaignMetrics", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    }).factory("preferenceMetricsSaveService", function ($resource) {
        return $resource("spr/preference", null, {
        	save: {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json'
            }
	        }
        });
    }).factory("agentAllocationService", function ($resource) {
        return $resource("spr/supervisor/agentallocation", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    }).factory("agentMtricsService", function ($resource) {
        return $resource("spr/agent/metrics?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:true
            }
        });
    })
    /*===========================new services for new supervisor screen====================*/
    .factory("campaignListService", function ($resource) {
        return $resource("spr/rest/campaign?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    })
    .factory("SupervisorCampaignService", function ($resource) {
        return $resource("spr/rest/campaign/supervisor?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:false
            }
        });
    })
    .factory("campaignListMetricsService", function ($resource) {
        return $resource("spr/campaign/metrics?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray:false
                
            }
        });
    })
    .factory("emailBouncesService", function ($resource) {
    	return $resource("spr/campaign/metrics/emailbounces/count?searchstring=:searchstring", null, {
    		query: {
    			method : 'GET',
    			isArray:false
    			
    		}
    	});
    })
    .factory("agentListService", function ($resource) {
        return $resource("spr/agent/metrics?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray: true
            }
        });
    })
    /*===========================end by pt====================*/
    .factory("agentAllocationOverrideService", function ($resource) {
        return $resource("spr/team/agent/:agentId/override/:campaignId", null, {
        	 override: {
                 method: 'POST',
                 headers : {
                     'Content-Type': 'application/json'
                 }
              },
              delete: {
                  method: 'DELETE',
                  headers : {
                      'Content-Type': 'application/json'
                  }
               },
        });
    }).factory("agentUnAllocationOverrideService", function ($resource) {
        return $resource("spr/team/agent/:agentId/override/:campaignId", null, {
              delete: {
                  method: 'DELETE',
                  headers : {
                      'Content-Type': 'application/json'
                  }
               },
        });
    }).factory("ActivateAssetService", function ($resource) { 
        return $resource("spr/campaign/:campaignId/activeasset/assetId", null, {
         activate: {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json'
            }
         },
         save: {
             method: 'DELETE',
             headers : {
                 'Content-Type': 'application/json'
             }
         },
         query: {
             method : 'GET',
             isArray:false
         }
        });
    }).factory("AssetEmailService", function ($resource) { 
        return $resource("spr/campaign/:campaignId/activeasset/:string", null, {
            send: {
               method: 'POST',
               headers : {
                   'Content-Type': 'application/json'
               }
            },
            query: {
                method: 'GET',
                isArray: false
            },
            /* START TEST EMAIL TEMPLATE
        	 * REASON : Added to test email template on supervisor screen */
            test: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            }
            /* END TEST EMAIL TEMPLATE */
           });
       }).factory("AgentCalendarService", function ($resource) {
        return $resource("spr/agent/:agentId/schedule?searchstring=:searchstring", {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }).factory("AgentCalendarPopupService", function ($resource) {
        return $resource("spr/campaign/:campaignId/campaignagent/:agentId/schedule/:scheduleId", null, {
        create: {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json'
            }
         },
         update: {
            method: 'PUT',
            headers : {
                'Content-Type': 'application/json'
            }
         },
         query: {
                method: 'GET',
                isArray: false
            },
         delete: {
                method: 'DELETE',
                isArray: false
            }
        });
    }).factory("ApplicationPropertyService", function ($resource) {
        return $resource("spr/applicationproperties", null, {
        	query: {
                method: 'GET',
                isArray: true
            }
        });
    }).factory("PivotService", function ($resource) {
        return $resource("spr/supervisorpivot/:campaignId/:pivotSelectedTab", null, {
            query: {
                method : 'GET',
                isArray : false
            },
            pivotstatus: {
                method : 'GET',
                url: "spr/supervisorpivot/status/:campaignId",
                isArray : false
            }
        });
    }).factory("sendPivotEmailService", function ($resource) {
        return $resource("spr/supervisorpivot/mail/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            },
            downloadpivot: {
                method : 'GET',
                url: "spr/supervisorpivot/:campaignId/download",
                isArray : false
            }
        });
    }).factory("healthCheckService", function ($resource) {
        return $resource("spr/supervisor/healthcheck", null, {
            query: {
                method : 'POST',
                isArray : false
            }
        });
    }).factory("GetOrganizationService", function ($resource) {
        return $resource("/spr/organization/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("ZoomBuyService", function ($resource) {
        return $resource("/spr/zoombuy", null, {
            buy: {
                method : 'POST',
                isArray : false
            },
            databuystatus: {
                method : 'GET',
                url: "spr/campaign/databuy/status/:campaignId",
                isArray : false
            },
            databuystop: {
                method : 'GET',
                url: "spr/campaign/databuy/stop/:campaignId",
                isArray : false
            },
            databuyprocess: {
                method : 'GET',
                url: "spr/campaign/databuy/process/:campaignId/:id",
                isArray : false
            },
            databuycampaign: {
                method : 'GET',
                url: "spr/campaign/databuy/dbq/:campaignId",
                isArray : false
            }
        });
    }).factory("EverstringService", function ($resource) {
        return $resource("/spr/everstring/company/discover/:campaignId", null, {
            discover: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("CampaignSettingService", function ($resource) { 
        return $resource("/spr/admin/campaignsettings/:campaignId", {}, {
            update: {
                method: 'PUT',
                headers : {
                    'Content-Type': 'application/json'
                }
             }
          });
    }).factory("MoveDataToCallableService", function ($resource) { 
        return $resource("/spr/supervisor/data/callables/:campaignId", {}, {
            update: {
                method: 'PUT',
                headers : {
                    'Content-Type': 'application/json'
                }
             }
          });
    }).factory("agentModeChangeService", function($resource) {
        return $resource("/spr/agentMode/:agentId/:agentMode", null, {
         get: {
             method: 'GET',
             isArray: false
         }
     });
   }).factory("CampaignReportService", function ($resource) {
    return $resource("/spr/supervisor/campaignReport", null,{
        query: {
            method : 'GET',
            isArray : false
        }
      });
    }).factory("AgentReportService", function ($resource) {
        return $resource("/spr/supervisor/agentReport", null, {
            query: {
                method : 'GET',
                isArray : false
            }
          });
    }).factory("abmListCountService", function ($resource) {
        return $resource("/spr/abmlist/count/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("suppressCallableService", function ($resource) {
        return $resource("/spr/suppress/callable/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("getSuppressionListService", function ($resource) {
        return $resource("/spr/campaignsuppression/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("searchCampaignService", function ($resource) {
        return $resource("/spr/rest/supervisor/searchCampaigns/:name", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("supervisorAgentStatusService", function ($resource) {
        return $resource("/spr/agent/agentcommand/approve", null, {
            query: {
                method: 'POST',
                headers : {
                'Content-Type': 'application/json'
            },
            }
        });
    }).factory("supervisorApproveAllAgentStatusService", function ($resource) {
        return $resource("/spr/agent/agentcommand/approveAll", null, {
            query: {
                method: 'POST',
                headers : {
                'Content-Type': 'application/json'
            },
            }
        });
    })
});
