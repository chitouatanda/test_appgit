define(['angular', 'user-service','campaign/campaign-service','login-service', 'notification-service', 'teamnotice-service', 'angular-localStorage'], function(angular) {
	angular.module('HeaderController', ['user-service','campaign-service','login-service', 'notification-service', 'teamnotice-service', 'ngStorage'])
		.controller('HeaderController', [
		                                      '$scope', 
		                                      '$location',
		                                      '$modal',
		                                      '$state',
		                                      '$rootScope',
		                                      'UserPasswordService',
		                                      'LoginService',
		                                      'user',
		                                      'GlobalCampaignTypes',
		                                      'GlobalBrandName',
		                                      'campaignTypes',
		                                      'NotificationService',
		                                      '$localStorage',
		                                      'NoticeService',
		                                      '$window',
		                                      'CampaignService',
		                                      '$stateParams',
											  '$filter',
											  'Pusher',
											  'AgentNetworkStatusUpdateService',
		                                       function ($scope, $location, $modal, $state, $rootScope, UserPasswordService, LoginService, user, GlobalCampaignTypes, GlobalBrandName ,campaignTypes, NotificationService, $localStorage, NoticeService, $window, CampaignService, $stateParams, $filter, Pusher,AgentNetworkStatusUpdateService) {
		//Set CampaignTypes from global service 
		                                    	  
		GlobalCampaignTypes.setGlobalCampaignTypes(campaignTypes);
		GlobalBrandName.setGlobalBrandName(user.organization);
		$scope.uiRouterState = $state;
		console.log($scope.uiRouterState.current.name);
		$scope.user = user;
		$scope.role = user.roles[0];
		$scope.notificationShowFlag = false;
		$window.userGuidingUserId = $scope.user.id;
		$scope.logout = function() {
			if($stateParams.campaignId != null && $stateParams.campaignId != "" && $stateParams.campaignId != undefined) {
				$scope.UpdateCampaignData();
			}
			var temp = LoginService.logout();
			temp.then(function() {
				$scope.updateAgentNetworkStatus($scope.user.id, false);
				$rootScope.isUserLoggedIn = false;				
				$localStorage.$reset();	
				$location.path('/login');
				$window.location.reload();
			});
		};

		$scope.updateAgentNetworkStatus = function(userId, networkstatus) {
			AgentNetworkStatusUpdateService.postAgent({username:userId,networkstatus:networkstatus}).$promise.then(function(data){
				console.log('Post-Agent Status : ' + data.networkstatus);
			});
		}

		$scope.passwordValid = false;
		$scope.changePassword = function() {
			if($stateParams.campaignId != null && $stateParams.campaignId != "" && $stateParams.campaignId != undefined) {
				$scope.UpdateCampaignData();
			}
			var modalInstance = $modal.open({
			      templateUrl: 'changePasswordModal.html',
			      controller: ModalInstanceCtrl,
			      resolve: {
			            userId: function(){
			                return $scope.user.id;
			            }
			           
			        }
			});
		
		};
		$scope.notice = NoticeService.get({userId: user.id});
		
		NoticeService.getUnReadCount({userId: user.id}).$promise.then(function (data) {
			$scope.newNoticesCount = data[0];
		});
		
		Pusher.subscribe(user.id, 'team_notification', function (message) {
			var count = parseInt($scope.newNoticesCount) + 1;
			$scope.newNoticesCount = count.toString();
			$scope.notice = NoticeService.get({userId: user.id});
		});
				
		$scope.markAsRead = function () {
			$scope.notificationShowFlag = !$scope.notificationShowFlag;
			$scope.newNoticesCount = "0";
			if ($scope.notificationShowFlag) {
				NoticeService.markAsRead(user.id);
			}
		}
		
		var ModalInstanceCtrl = function ($scope, $modalInstance, userId) {
			   
			    $scope.ok = function (currentPassword, confirmPassword) {
				     if($scope.confirmPasswordCheck) {
				    		$scope.confirmPasswordValid = true;
				     }
			       	 $scope.password = {newPassword : confirmPassword, currentPassword : currentPassword};
			    	 if($scope.myForm.$valid) {
					   	UserPasswordService.update({id : userId}, $scope.password).$promise.then(function(data){ 
					   		NotificationService.log('success','Password changed successfully.');
					   		$modalInstance.dismiss('cancel');
						})
					 } else {
						$scope.passwordValid = true;
					 }
				};

			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		};
		
		$scope.campaignList = function() { 
			$state.go('listcampaigns', {}, { reload: true });
		};
		
		
		$scope.UpdateCampaignData = function () {
			if($stateParams.campaignId != null && $stateParams.campaignId != "" && $stateParams.campaignId != undefined) {
				$scope.campaign = $localStorage.campaign;
				if ($scope.campaign != null && $scope.campaign != undefined) {
									$scope.campaign.startDate= $filter('date')(new Date($scope.campaign.startDate), 'MM/dd/yyyy');
									$scope.campaign.endDate= $filter('date')(new Date($scope.campaign.endDate), 'MM/dd/yyyy');
						CampaignService.update({
							id: $stateParams.campaignId
					 }, $scope.campaign).$promise.then(function(updateDataHeader) {   
									$localStorage.campaign = updateDataHeader;
									$scope.campaign = updateDataHeader;
									$scope.campaign.startDate= $filter('date')(updateDataHeader.startDate, 'yyyy-MM-dd');
								  $scope.campaign.endDate= $filter('date')(updateDataHeader.endDate, 'yyyy-MM-dd');
							}, function(error) {
									 console.log("rejected " + JSON.stringify(error));
								   $scope.loading = false;
						 });
					}  	 
      }      
	}
		
		
	}]).directive('pwCheck', [function () {
	    return {
	        require: 'ngModel',
	        link: function ($scope, elem, attrs, ctrl) {
	          var firstPassword = '#' + attrs.pwCheck;
	          elem.add(firstPassword).on('keyup', function () {
	        	  $scope.$apply(function () {
	              var v = elem.val()===$(firstPassword).val();
	              ctrl.$setValidity('pwmatch', v);
	              $scope.confirmPasswordCheck = true;
	            });
	          });
	        }
	      }
	    }]);
});
