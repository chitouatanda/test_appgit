require.config({
	waitSeconds: 0,
    baseUrl: '/js',
    paths: {
    	'jquery': 'jquery-1.11.0.min',
		'jqueryUI': ['vendor/jquery-ui'],
		'angular': ['vendor/angular'],
		'angular-resource': ['vendor/angular-resource'],
		'angular-cookie': ['vendor/angular-cookies'],
		'angular-ui-router': ['vendor/angular-ui-router'],
		'angular-ui-bootstrap': ['vendor/ui-bootstrap-tpls'],
		'angular-ui-select': ['vendor/select'],
		'custom-popover': 'vendor/custom-popover',
		'angular-slider': 'ng-slider.min',
		'ng-slider': 'vendor/ng-slider.min',
		'oc-lazyload': 'vendor/ocLazyLoad',
		'ang-drag-drop': 'vendor/draganddrop',
		'angular-animate': ['vendor/angular-animate'],
		'angular-toaster': 'toaster',
		'moment':'moment.min',
		'pusher': ['//js.pusher.com/2.2/pusher', 'vendor/pusher'],
		'angular-pusher': 'vendor/angular-pusher.min',
		'lodash': ['vendor/lodash'],
		'angular-wizard': 'vendor/angular-wizard',
//		'twilio': ['https://static.twilio.com/libs/twiliojs/1.2/twilio.min'],
//		'twilio': ['https://media.twiliocdn.com/sdk/js/client/releases/1.4.27/twilio.min'],
		'twilio': ['vendor/twilio.min'],
		'telnyx': ['vendor/telnyx'],
		'signalwire': ['vendor/sip-0.20.0.min'],
		'angular-timer': ['vendor/timer'],
		'timepicker':'angular.timepicker',
		'angular-strap': 'vendor/angular-strap',
		'angular-strap-tpl': 'vendor/angular-strap.tpl',
		'angular-sanitize': 'vendor/angular-sanitize',
    	// 'angular-hotkeys': '//cdnjs.cloudflare.com/ajax/libs/angular-hotkeys/1.4.5/hotkeys.min',
    	'visualize': 'vendor/visualize',
    	'angular-upload': 'vendor/ng-file-upload.min',
    	'perfect-scrollbar-mousewheel-min':'vendor/perfect-scrollbar-mousewheel-min',
    	'angular-perfect-scrollbar':'vendor/angular-perfect-scrollbar',
    	'angular-filesaver':'//rawgithub.com/eligrey/FileSaver.js/master/FileSaver',
    	'angucomplete-alt':'vendor/angucomplete-alt',
    	'pagination':'vendor/dirPagination',
    	'angular-localStorage':'vendor/ngStorage.min',
    	'angular-timeZone' : 'vendor/jstz.min',
    	'd3graph' : 'vendor/d3graph/angularjs-nvd3-directives',
    	'd3':'vendor/d3graph/d3',
    	'nv-d3':'vendor/d3graph/nv.d3',
    	'underscore':'vendor/underscore-min',
    	'addnewidnc':'addnewIdnc-controller',
    	'angular-loadjs':'vendor/angular-load.min',
    	'tinymce': 'vendor/tinymce.min',
    	'angular-ui-tinymce': 'vendor/angular-ui-tinymce.min',
    	'angular-x-editable': 'vendor/xeditable.min',
		'ngFileUpload': 'vendor/ng-file-upload.min',
		'phoneNumberUtils': "vendor/utils",
		'phoneController' : "vendor/intlTelInput",
		"intlTelMin": "vendor/intlTelInput.min",
		"chosen-jquery": "vendor/chosen-jquery"
 //    	'jquery': 'jquery-1.11.0.min',
//		'jqueryUI': ['vendor/jquery-ui'],
//		'angular': ['vendor/angular'],
//		'angular-resource': ['vendor/angular-resource'],
//		'angular-cookie': ['vendor/angular-cookies'],
//		'angular-ui-router': ['vendor/angular-ui-router'],
//		'angular-ui-bootstrap': ['vendor/ui-bootstrap-tpls'],
//		'angular-ui-select': ['vendor/select'],
//		'custom-popover': 'vendor/custom-popover',
//		'angular-slider': 'ng-slider.min',
//		'ng-slider': 'vendor/ng-slider.min',
//		'oc-lazyload': 'vendor/ocLazyLoad',
//		'ang-drag-drop': 'vendor/draganddrop',
//		'angular-animate': ['vendor/angular-animate'],
//		'angular-toaster':'toaster',
//		'fullcalendar':'fullcalendar',
//		'moment':'moment.min',
//		'calendar':'calendar',
//		'pusher': ['vendor/pusher'],
//		'angular-pusher': 'vendor/angular-pusher.min',
//		'lodash': ['vendor/lodash'],
//		'angular-wizard': 'vendor/angular-wizard',
//		'twilio': 'vendor/twilio',
//		'angular-timer': ['vendor/timer'],
//		'timepicker':'angular.timepicker',
//		'angular-strap': 'vendor/angular-strap',
//		'angular-strap-tpl': 'vendor/angular-strap.tpl',
//		'angular-sanitize': 'vendor/angular-sanitize',
//		'angular-hotkeys': 'vendor/angular-hotkeys'
    },
	shim: {
		'angular': {
			deps: ['jquery','jqueryUI','perfect-scrollbar-mousewheel-min'],
			exports: 'angular'
		},
		'angular-resource': {
			deps: ['angular']
		},
		'angular-cookie': {
			deps: ['angular']
		},
		'angular-ui-router': {
			deps: ['angular']
		},
		'custom-popover': {
			deps: ['angular']
		},
		'angular-ui-select': {
			deps: ['angular']
		},
		'angular-ui-bootstrap': {
			deps: ['angular']
		},
		'ng-slider': {
			deps: ['angular']
		},
		'ang-drag-drop': {
			deps: ['angular']
		},
		'oc-lazyload': {
			deps: ['angular']
		},
		'angular-animate': {
			deps: ['angular']
		},
		'angular-toaster': {
			deps: ['angular-animate']
		},
		'angular-pusher': {
			deps: ['pusher',
			       'angular']
		},
		'angular-wizard': {
			deps: ['angular', 'lodash']
		},
		'twilio': {
			exports: 'Twilio.Device'
		},
		'angular-timer': {
			deps: ['angular']
		},
		'angular-strap-tpl': {
			deps: ['angular-sanitize',
			       'angular-strap',
			       'angular']
		},
		'angular-strap': {
			deps: ['angular']
		},
		'angular-sanitize': {
			deps: ['angular']
		},
		'visualize': {
			exports: 'visualize'
		},
		'angular-upload': {
			deps: ['angular']
		},
		'angular-perfect-scrollbar': {
			deps: ['angular']
		},
		'angular-filesaver' : {
			deps: ['angular']
		},
		'pagination' : {
			deps: ['angular']
		},
		'angular-localStorage' : {
			deps: ['angular']
		},
		'angular-timeZone' : {
			deps: ['angular']
		},
		'd3graph' : {
			deps: ['angular']
		},
		'd3' : {
			deps: ['angular']
		},
		'nv-d3' : {
			deps: ['angular']
		},
		'underscore' : {
			deps: ['angular']
		},
		'angular-loadjs' : {
			deps: ['angular']
		},
		'angular-ui-tinymce': {
            deps: ['angular',
                   'tinymce']
        },
        'angular-x-editable': {
        	deps: ['angular']
		},
		'phoneNumberUtils' : {
			deps : ['angular']
		},
		'phoneController' : {
			deps : ['angular']
		},
		'intlTelMin' : {
			deps : ['angular']
		}
	},
	// urlArgs: "version=" + (new Date()).getTime()
});
require
(
    [
     	'angular',
        'xtaas',
        'angular-toaster'
    ],
    function(angular)
    {
        angular.bootstrap(document, ['xtaas']);
    }
);
