angular.module('qaScore')
	.controller('QaScoreController', [
	    '$localStorage',
		'$sce',
		'$scope', 
		'qadetail',
		'qaRejectReasons',
		'user',
		'getDisplayLabelService',
		'qaProspectListService',
		'qADetailFeedbackService',
		'qADetailSaveFeedbackService',
		'qADetailSubmitFeedbackService',									
		'UserServiceById',
		
	    function(
	    		$localStorage,
	    		$sce,
	    		$scope,
	    	    qadetail,
	    		qaRejectReasons,
	    		user,
	    		getDisplayLabelService,
	    		qaProspectListService,
	    		qADetailFeedbackService,
	    		qADetailSaveFeedbackService,
	    		qADetailSubmitFeedbackService,									
	    		UserServiceById) {
	    	function init() {
		    	$scope.qadetail = qadetail;
		    	$scope.qaRejectReasons = qaRejectReasons;
		    	$scope.user = user;
		    	
		    	if($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus) {
					getDisplayLabelService.query({labelentity: $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus + 'DISPOSITIONSTATUS',key:$scope.qaDetail.prospectCallLog.prospectCall.subStatus}).$promise.then( function(data) {
						angular.forEach(data,  function(value, key){
							if(key == $scope.qaDetail.prospectCallLog.prospectCall.subStatus) {
								$scope.subStatus = value;	
							}												
						});												 	
					});	
				}
		    	if ($scope.qadetail.callLogMap[0] != null && $scope.qadetail.callLogMap[0] != undefined ) {
					$scope.recordingURL = $sce.trustAsResourceUrl($scope.qadetail.prospectCallLog.prospectCall.recordingUrl);
				}
				
	    	}
	    	
	    	init();
	    }]
	);
