angular.module('QaScore')
		.controller('QaScoreCampaignInfoController', ['$scope', function($scope) {
		}])
		.directive('qaScoreCampaignInfo', function() {
			return {
				restrict: 'E',
				templateUrl: '/js/qascore/components/campaigninfo/qascore-campaigninfo.html',
				controller: 'QaScoreCampaignInfoController',
				scope: {
										
				}
			}
		});