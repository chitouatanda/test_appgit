angular.module('QaScore')
.controller('QaScoreQualificationsController', ['$scope', function($scope) {
}])
.directive('qaScoreQualifications', function() {
	return {
		restrict: 'E',
		templateUrl: '/js/qascore/components/qualifications/qascore-qualifications.html',
		controller: 'QaScoreQualificationsController',
		scope: {

		}
	}
});