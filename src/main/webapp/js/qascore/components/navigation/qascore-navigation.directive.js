angular.module('qaScore')
		.controller('QaScoreNavigationController', ['$scope', function($scope) {
		}])
		.directive('qaScoreNavigation', function() {
			return {
				restrict: 'E',
				templateUrl: '/js/qascore/components/navigation/qascore-navigation.html',
				controller: 'QaScoreNavigationController',
				scope: {
					campaignName: '@',
					prospectCall: '=',
					viewList: '&',
					hasPrevious: '=',
					viewPrevious: '&',
					hasNext: '=',
					viewNext: '&'					
				}
			}
		});