angular.module('qaScore')
.controller('QaScoreQuestionnaireController', ['$scope', function($scope) {
}])
.directive('qaScoreQuestionnaire', function() {
	return {
		restrict: 'E',
		templateUrl: '/js/qascore/components/questionnaire/qascore-questionnaire.html',
		controller: 'QaScoreQuestionnaireController',
		scope: {

		}
	}
});