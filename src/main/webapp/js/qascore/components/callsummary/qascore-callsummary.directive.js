angular.module('QaScore')
.controller('QaScoreCallSummaryController', ['$scope', function($scope) {
}])
.directive('qaScoreCallSummary', function() {
	return {
		restrict: 'E',
		templateUrl: '/js/qascore/components/callsummary/qascore-callsummary.html',
		controller: 'QaScoreCallSummaryController',
		scope: {

		}
	}
});