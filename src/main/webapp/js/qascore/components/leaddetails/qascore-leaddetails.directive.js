angular.module('QaScore')
.controller('QaScoreLeadDetailsController', ['$scope', function($scope) {
}])
.directive('qaScoreLeadDetails', function() {
	return {
		restrict: 'E',
		templateUrl: '/js/qascore/components/leaddetails/qascore-leaddetails.html',
		controller: 'QaScoreLeadDetailsController',
		scope: {

		}
	}
});