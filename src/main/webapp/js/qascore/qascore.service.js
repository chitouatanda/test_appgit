define(['angular', 'angular-resource'], function(angular) {
	angular.module('qaScore', ['ngResource'])
	.factory("qaScoreService", function ($resource) {
		return $resource("spr/qa/:prospectCallId", null, {
			query: {
				method : 'GET',
				isArray: false
			}
		});
	})
	.factory("qaRejectReasonService", function ($resource) {
		return $resource("spr/rest/qarejectreason", null, {
			query: {
				method : 'GET',
				isArray: true
			}
		});
	})
	.factory("getDisplayLabelService", function($resource) {
		return $resource("/spr/label/:labelentity/:key",null, {
			query: {
				method : 'GET',
				isArray: false
			}
		});
	})
	.factory("qaProspectListService", function ($resource) {
		return $resource("spr/qa/prospectcall/search?searchstring=:searchstring", null, {
			query: {
				method : 'GET',
				isArray: false
			}
		});
	})
	.factory("qADetailFeedbackService", function ($resource) {
		return $resource("spr/qa/:prospectCallId/feedback", null, {
			save: {
				method: 'POST',
				headers : {
					'Content-Type': 'application/json'
				}
			},
		});
	})
	.factory("qADetailSaveFeedbackService", function ($resource) {
		return $resource("spr/qa/:prospectCallId/savefeedback", null, {
			save: {
				method: 'POST',
				headers : {
					'Content-Type': 'application/json'
				}
			},
		});
	})
	.factory("qADetailSubmitFeedbackService", function ($resource) {
		return $resource("spr/qa/:prospectCallId/submitfeedback", null, {
			save: {
				method: 'POST',
				headers : {
					'Content-Type': 'application/json'
				}
			},
		});
	})

});