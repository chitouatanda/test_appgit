define(['angular', 'angular-resource'], function(angular) {
	angular.module('idnc-service', ['ngResource'])
		.factory("idncService", function ($resource) {
	        return $resource("spr/idnc?searchstring=:searchstring", null, {
	            query: {
	                method : 'GET',
	                isArray:false
	            }
	        });
	    }).factory("idncSaveService", function ($resource) {
	        return $resource("spr/idnc/:campaignId", null, {
	        	save: {
	                method: 'POST',
	                headers : {
	                    'Content-Type': 'application/json'
	                }
	            },
	        });
	    });
});