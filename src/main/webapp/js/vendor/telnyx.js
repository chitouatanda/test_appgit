(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.TelnyxWebRTC = {}));
}(this, function (exports) { 'use strict';

  /*! *****************************************************************************
  Copyright (c) Microsoft Corporation. All rights reserved.
  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
  this file except in compliance with the License. You may obtain a copy of the
  License at http://www.apache.org/licenses/LICENSE-2.0

  THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
  WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
  MERCHANTABLITY OR NON-INFRINGEMENT.

  See the Apache Version 2.0 License for specific language governing permissions
  and limitations under the License.
  ***************************************************************************** */
  /* global Reflect, Promise */

  var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf ||
          ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
          function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
      return extendStatics(d, b);
  };

  function __extends(d, b) {
      extendStatics(d, b);
      function __() { this.constructor = d; }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }

  var __assign = function() {
      __assign = Object.assign || function __assign(t) {
          for (var s, i = 1, n = arguments.length; i < n; i++) {
              s = arguments[i];
              for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
          return t;
      };
      return __assign.apply(this, arguments);
  };

  function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
          function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
          function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
          function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
          step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
  }

  function __generator(thisArg, body) {
      var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
      return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
      function verb(n) { return function (v) { return step([n, v]); }; }
      function step(op) {
          if (f) throw new TypeError("Generator is already executing.");
          while (_) try {
              if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
              if (y = 0, t) op = [op[0] & 2, t.value];
              switch (op[0]) {
                  case 0: case 1: t = op; break;
                  case 4: _.label++; return { value: op[1], done: false };
                  case 5: _.label++; y = op[1]; op = [0]; continue;
                  case 7: op = _.ops.pop(); _.trys.pop(); continue;
                  default:
                      if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                      if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                      if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                      if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                      if (t[2]) _.ops.pop();
                      _.trys.pop(); continue;
              }
              op = body.call(thisArg, _);
          } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
          if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
      }
  }

  function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
      for (var r = Array(s), k = 0, i = 0; i < il; i++)
          for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
              r[k] = a[j];
      return r;
  }

  var getDeviceString = function (input) {
      if (typeof input === 'boolean') {
          return input ? 'any' : 'none';
      }
      else if (typeof input === 'string') {
          return input;
      }
      return 'none';
  };
  var checkAllowedModules = function (module) {
      if (module === "verto" || module === "telnyx_rtc") {
          return true;
      }
      return false;
  };

  function createCommonjsModule(fn, module) {
    return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var rngBrowser = createCommonjsModule(function (module) {
  // Unique ID creation requires a high quality random # generator.  In the
  // browser this is a little complicated due to unknown quality of Math.random()
  // and inconsistent support for the `crypto` API.  We do the best we can via
  // feature-detection

  // getRandomValues needs to be invoked in a context where "this" is a Crypto
  // implementation. Also, find the complete implementation of crypto on IE11.
  var getRandomValues = (typeof(crypto) != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto)) ||
                        (typeof(msCrypto) != 'undefined' && typeof window.msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto));

  if (getRandomValues) {
    // WHATWG crypto RNG - http://wiki.whatwg.org/wiki/Crypto
    var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

    module.exports = function whatwgRNG() {
      getRandomValues(rnds8);
      return rnds8;
    };
  } else {
    // Math.random()-based (RNG)
    //
    // If all else fails, use Math.random().  It's fast, but is of unspecified
    // quality.
    var rnds = new Array(16);

    module.exports = function mathRNG() {
      for (var i = 0, r; i < 16; i++) {
        if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
        rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
      }

      return rnds;
    };
  }
  });

  /**
   * Convert array of 16 byte values to UUID string format of the form:
   * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
   */
  var byteToHex = [];
  for (var i = 0; i < 256; ++i) {
    byteToHex[i] = (i + 0x100).toString(16).substr(1);
  }

  function bytesToUuid(buf, offset) {
    var i = offset || 0;
    var bth = byteToHex;
    // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
    return ([bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]]]).join('');
  }

  var bytesToUuid_1 = bytesToUuid;

  // **`v1()` - Generate time-based UUID**
  //
  // Inspired by https://github.com/LiosK/UUID.js
  // and http://docs.python.org/library/uuid.html

  var _nodeId;
  var _clockseq;

  // Previous uuid creation time
  var _lastMSecs = 0;
  var _lastNSecs = 0;

  // See https://github.com/broofa/node-uuid for API details
  function v1(options, buf, offset) {
    var i = buf && offset || 0;
    var b = buf || [];

    options = options || {};
    var node = options.node || _nodeId;
    var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

    // node and clockseq need to be initialized to random values if they're not
    // specified.  We do this lazily to minimize issues related to insufficient
    // system entropy.  See #189
    if (node == null || clockseq == null) {
      var seedBytes = rngBrowser();
      if (node == null) {
        // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
        node = _nodeId = [
          seedBytes[0] | 0x01,
          seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]
        ];
      }
      if (clockseq == null) {
        // Per 4.2.2, randomize (14 bit) clockseq
        clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
      }
    }

    // UUID timestamps are 100 nano-second units since the Gregorian epoch,
    // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
    // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
    // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
    var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

    // Per 4.2.1.2, use count of uuid's generated during the current clock
    // cycle to simulate higher resolution clock
    var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

    // Time since last uuid creation (in msecs)
    var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

    // Per 4.2.1.2, Bump clockseq on clock regression
    if (dt < 0 && options.clockseq === undefined) {
      clockseq = clockseq + 1 & 0x3fff;
    }

    // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
    // time interval
    if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
      nsecs = 0;
    }

    // Per 4.2.1.2 Throw error if too many uuids are requested
    if (nsecs >= 10000) {
      throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
    }

    _lastMSecs = msecs;
    _lastNSecs = nsecs;
    _clockseq = clockseq;

    // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
    msecs += 12219292800000;

    // `time_low`
    var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
    b[i++] = tl >>> 24 & 0xff;
    b[i++] = tl >>> 16 & 0xff;
    b[i++] = tl >>> 8 & 0xff;
    b[i++] = tl & 0xff;

    // `time_mid`
    var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
    b[i++] = tmh >>> 8 & 0xff;
    b[i++] = tmh & 0xff;

    // `time_high_and_version`
    b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
    b[i++] = tmh >>> 16 & 0xff;

    // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
    b[i++] = clockseq >>> 8 | 0x80;

    // `clock_seq_low`
    b[i++] = clockseq & 0xff;

    // `node`
    for (var n = 0; n < 6; ++n) {
      b[i + n] = node[n];
    }

    return buf ? buf : bytesToUuid_1(b);
  }

  var v1_1 = v1;

  /*
   * Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   * Copyright (C) 2005-2017, Anthony Minessale II <anthm@freeswitch.org>
   *
   * Version: MPL 1.1
   *
   * The contents of this file are subject to the Mozilla Public License Version
   * 1.1 (the "License"); you may not use this file except in compliance with
   * the License. You may obtain a copy of the License at
   * http://www.mozilla.org/MPL/
   *
   * Software distributed under the License is distributed on an "AS IS" basis,
   * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
   * for the specific language governing rights and limitations under the
   * License.
   *
   * The Original Code is Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   *
   * The Initial Developer of the Original Code is
   * Anthony Minessale II <anthm@freeswitch.org>
   * Portions created by the Initial Developer are Copyright (C)
   * the Initial Developer. All Rights Reserved.
   *
   * Contributor(s):
   *
   * Seven Du <dujinfang@x-y-t.cn>
   * Xueyun Jiang <jiangxueyun@x-y-t.cn>
   *
   * verto-livearray.js - Verto LiveArray
   *
   */
  /**
   * @hidden
   */
  var VertoHashArray = /** @class */ (function () {
      function VertoHashArray() {
          this.hash = {};
          this.array = [];
      }
      VertoHashArray.prototype.reorder = function (a, arg) {
          this.array = a;
          var h = this.hash;
          this.hash = {};
          var len = this.array.length;
          for (var i = 0; i < len; i++) {
              var key = this.array[i];
              if (h[key]) {
                  this.hash[key] = h[key];
                  delete h[key];
              }
          }
          h = undefined;
      };
      VertoHashArray.prototype.clear = function () {
          this.hash = undefined;
          this.array = undefined;
          this.hash = {};
          this.array = [];
      };
      VertoHashArray.prototype.add = function (name, val, insertAt, arg) {
          var redraw = false;
          if (!this.hash[name]) {
              if (insertAt === undefined ||
                  insertAt < 0 ||
                  insertAt >= this.array.length) {
                  this.array.push(name);
              }
              else {
                  var x = 0;
                  var n = [];
                  var len = this.array.length;
                  for (var i = 0; i < len; i++) {
                      if (x++ == insertAt) {
                          n.push(name);
                      }
                      n.push(this.array[i]);
                  }
                  this.array = undefined;
                  this.array = n;
                  n = undefined;
                  redraw = true;
              }
          }
          this.hash[name] = val;
          return redraw;
      };
      VertoHashArray.prototype.del = function (name, arg1, arg2) {
          var r = false;
          if (this.hash[name]) {
              this.array = this.array.filter(function (x) { return x !== name; });
              delete this.hash[name];
              r = true;
          }
          else {
              console.error("can't del nonexistant key " + name);
          }
          return r;
      };
      VertoHashArray.prototype.get = function (name) {
          return this.hash[name];
      };
      VertoHashArray.prototype.order = function () {
          return this.array;
      };
      VertoHashArray.prototype.indexOf = function (name) {
          return this.array.indexOf(name);
      };
      VertoHashArray.prototype.arrayLen = function () {
          return this.array.length;
      };
      VertoHashArray.prototype.asArray = function () {
          var r = [];
          var len = this.array.length;
          for (var i = 0; i < len; i++) {
              var key = this.array[i];
              r.push(this.hash[key]);
          }
          return r;
      };
      VertoHashArray.prototype.each = function (cb) {
          var len = this.array.length;
          for (var i = 0; i < len; i++) {
              cb(this.array[i], this.hash[this.array[i]]);
          }
      };
      VertoHashArray.prototype.dump = function (html) {
          var str = '';
          // vha.each((name, val) => {
          //   str +=
          //     'name: ' +
          //     name +
          //     'val: ' +
          //     JSON.stringify(val) +
          //     (html ? '<br>' : '\n');
          // });
          return str;
      };
      return VertoHashArray;
  }());
  /**
   * @hidden
   */
  var VertoLiveArray = /** @class */ (function (_super) {
      __extends(VertoLiveArray, _super);
      function VertoLiveArray(verto, context, name, config) {
          var _this = _super.call(this) || this;
          _this.verto = verto;
          _this.lastSerno = 0;
          _this.binding = null;
          _this.user_obj = config.userObj;
          _this.config = config;
          _this.local = false;
          // Save the hashArray add, del, reorder, clear methods so we can make our own.
          _this._add = _super.prototype.add;
          _this._del = _super.prototype.del;
          _this._reorder = _super.prototype.reorder;
          _this._clear = _super.prototype.clear;
          _this.context = context;
          _this.name = name;
          _this.errs = 0;
          _this.onChange = config.onChange;
          console.log('VertoLiveArray init....');
          if (context) {
              _this.binding = verto.subscribe(context, {
                  handler: _this.eventHandler.bind(_this),
                  userData: verto,
                  subParams: config.subParams,
              });
          }
          _this.bootstrap(config);
          return _this;
      }
      VertoLiveArray.prototype.broadcast = function (channel, obj) {
          console.log('broadcast', obj);
          this.verto.broadcast(channel, obj);
      };
      VertoLiveArray.prototype.clear = function () {
          this._clear();
          this.lastSerno = 0;
          if (this.onChange) {
              this.onChange(this, {
                  action: 'clear',
              });
          }
      };
      VertoLiveArray.prototype.checkSerno = function (serno) {
          if (serno < 0) {
              return true;
          }
          if (this.lastSerno > 0 && serno != this.lastSerno + 1) {
              if (this.onErr) {
                  this.onErr(this, {
                      lastSerno: this.lastSerno,
                      serno: serno,
                  });
              }
              this.errs++;
              console.debug(this.errs);
              if (this.errs < 3) {
                  this.bootstrap(this.user_obj);
              }
              return false;
          }
          else {
              this.lastSerno = serno;
              return true;
          }
      };
      VertoLiveArray.prototype.reorder = function (serno, a) {
          if (this.checkSerno(serno)) {
              this._reorder(a);
              if (this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'reorder',
                  });
              }
          }
      };
      VertoLiveArray.prototype.init = function (serno, val, key, index) {
          if (key === null || key === undefined) {
              key = serno;
          }
          if (this.checkSerno(serno)) {
              if (this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'init',
                      index: index,
                      key: key,
                      data: val,
                  });
              }
          }
      };
      VertoLiveArray.prototype.bootObj = function (serno, val) {
          if (this.checkSerno(serno)) {
              this.clear();
              for (var i in val) {
                  this._add(val[i][0], val[i][1]);
              }
              if (this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'bootObj',
                      data: val,
                      redraw: true,
                  });
              }
          }
      };
      // 	// @param serno  This is the serial number for this particular request.
      // 	// @param key    If looking at it as a hash table, this represents the key in the hashArray object where you want to store the val object.
      // 	// @param index  If looking at it as an array, this represents the position in the array where you want to store the val object.
      // 	// @param val    La is the object you want to store at the key or index location in the hash table / array.
      VertoLiveArray.prototype.add = function (serno, val, key, index) {
          if (key === null || key === undefined) {
              key = serno;
          }
          if (this.checkSerno(serno)) {
              var redraw = this._add(key, val, index);
              if (this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'add',
                      index: index,
                      key: key,
                      data: val,
                      redraw: redraw,
                  });
              }
          }
      };
      VertoLiveArray.prototype.modify = function (serno, val, key, index) {
          if (key === null || key === undefined) {
              key = serno;
          }
          if (this.checkSerno(serno)) {
              this._add(key, val, index);
              if (this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'modify',
                      key: key,
                      data: val,
                      index: index,
                  });
              }
          }
      };
      VertoLiveArray.prototype.del = function (serno, key, index) {
          if (key === null || key === undefined) {
              key = serno;
          }
          if (this.checkSerno(serno)) {
              if (index === null || index < 0 || index === undefined) {
                  index = this.indexOf(key);
              }
              var ok = this._del(key);
              if (ok && this.onChange) {
                  this.onChange(this, {
                      serno: serno,
                      action: 'del',
                      key: key,
                      index: index,
                  });
              }
          }
      };
      VertoLiveArray.prototype.eventHandler = function (v, e, la) {
          var packet = e.data;
          console.debug('READ:', packet);
          if (packet.name != la.name) {
              return;
          }
          switch (packet.action) {
              case 'init':
                  this.init(packet.wireSerno, packet.data, packet.hashKey, packet.arrIndex);
                  break;
              case 'bootObj':
                  this.bootObj(packet.wireSerno, packet.data);
                  break;
              case 'add':
                  this.add(packet.wireSerno, packet.data, packet.hashKey, packet.arrIndex);
                  break;
              case 'modify':
                  if (!(packet.arrIndex || packet.hashKey)) {
                      console.error('Invalid Packet', packet);
                  }
                  else {
                      this.modify(packet.wireSerno, packet.data, packet.hashKey, packet.arrIndex);
                  }
                  break;
              case 'del':
                  if (!(packet.arrIndex || packet.hashKey)) {
                      console.error('Invalid Packet', packet);
                  }
                  else {
                      this.del(packet.wireSerno, packet.hashKey, packet.arrIndex);
                  }
                  break;
              case 'clear':
                  this.clear();
                  break;
              case 'reorder':
                  this.reorder(packet.wireSerno, packet.order);
                  break;
              default:
                  if (this.checkSerno(packet.wireSerno)) {
                      if (this.onChange) {
                          this.onChange(this, {
                              serno: packet.wireSerno,
                              action: packet.action,
                              data: packet.data,
                          });
                      }
                  }
                  break;
          }
      };
      VertoLiveArray.prototype.destroy = function () {
          this._clear();
          this.verto.unsubscribe(this.binding);
      };
      VertoLiveArray.prototype.sendCommand = function (cmd, obj) {
          this.broadcast(this.context, {
              liveArray: {
                  command: cmd,
                  context: this.context,
                  name: this.name,
                  obj: obj,
              },
          });
      };
      VertoLiveArray.prototype.bootstrap = function (obj) {
          this.sendCommand('bootstrap', obj);
          //this.heartbeat();
      };
      VertoLiveArray.prototype.changepage = function (obj) {
          this.clear();
          this.broadcast(this.context, {
              liveArray: {
                  command: 'changepage',
                  context: this.context,
                  name: this.name,
                  obj: obj,
              },
          });
      };
      VertoLiveArray.prototype.heartbeat = function (obj) {
          var _this = this;
          var callback = function () {
              _this.heartbeat.call(_this, obj);
          };
          this.broadcast(this.context, {
              liveArray: {
                  command: 'heartbeat',
                  context: this.context,
                  name: this.name,
                  obj: obj,
              },
          });
          this.hb_pid = setTimeout(callback, 30000);
      };
      return VertoLiveArray;
  }(VertoHashArray));

  /*
   * Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   * Copyright (C) 2005-2017, Anthony Minessale II <anthm@freeswitch.org>
   *
   * Version: MPL 1.1
   *
   * The contents of this file are subject to the Mozilla Public License Version
   * 1.1 (the "License"); you may not use this file except in compliance with
   * the License. You may obtain a copy of the License at
   * http://www.mozilla.org/MPL/
   *
   * Software distributed under the License is distributed on an "AS IS" basis,
   * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
   * for the specific language governing rights and limitations under the
   * License.
   *
   * The Original Code is Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   *
   * The Initial Developer of the Original Code is
   * Anthony Minessale II <anthm@freeswitch.org>
   * Portions created by the Initial Developer are Copyright (C)
   * the Initial Developer. All Rights Reserved.
   *
   * Contributor(s):
   *
   * Seven Du <dujinfang@x-y-t.cn>
   * Xueyun Jiang <jiangxueyun@x-y-t.cn>
   *
   * ConfMan.js - Verto Conference Manager
   *
   */
  var CONFMAN_SERNO = 0;
  /**
   * @hidden
   */
  var VertoConfMan = /** @class */ (function () {
      function VertoConfMan(verto, params) {
          var _this = this;
          this.params = Object.assign({
              dialog: null,
              hasVid: false,
              laData: null,
              onBroadcast: null,
              onLaChange: null,
              onLaRow: null,
          }, params);
          this.verto = verto;
          this.serno = CONFMAN_SERNO++;
          this.canvasCount = this.params.laData.canvasCount;
          verto.subscribe(this.params.laData.modChannel, {
              handler: function (v, e) {
                  if (_this.params.onBroadcast) {
                      _this.params.onBroadcast(verto, _this, e.data);
                  }
              },
          });
          verto.subscribe(this.params.laData.infoChannel, {
              handler: function (v, e) {
                  if (typeof _this.params.infoCallback === 'function') {
                      _this.params.infoCallback(v, e);
                  }
              },
          });
          verto.subscribe(this.params.laData.chatChannel, {
              handler: function (v, e) {
                  if (typeof _this.params.chatCallback === 'function') {
                      _this.params.chatCallback(v, e);
                  }
              },
          });
          if (this.params.laData.role === 'moderator') {
              verto.subscribe(this.params.laData.modChannel, {
                  handler: function (v, e) {
                      console.error('MODDATA:', e.data);
                      if (_this.params.onBroadcast) {
                          _this.params.onBroadcast(verto, _this, e.data);
                      }
                      if (e.data['conf-command'] === 'list-videoLayouts') {
                          for (var j = 0; j < _this.canvasCount; j++) {
                              if (e.data.responseData) ;
                          }
                      }
                  },
              });
              if (this.params.hasVid) {
                  this.modCommand('list-videoLayouts', null, null);
              }
          }
          // if (this.params.laData.role === "moderator")
      }
      VertoConfMan.prototype.modCommand = function (cmd, id, value) {
          this.verto.call(this.verto.module + ".broadcast", {
              eventChannel: this.params.laData.modChannel,
              data: {
                  application: 'conf-control',
                  command: cmd,
                  id: id,
                  value: value,
              },
          });
      };
      VertoConfMan.prototype.sendChat = function (message, type) {
          this.verto.call(this.verto.module + ".broadcast", {
              eventChannel: this.params.laData.chatChannel,
              data: {
                  action: 'send',
                  message: message,
                  type: type,
              },
          });
      };
      VertoConfMan.prototype.destroy = function () {
          this.destroyed = true;
          if (this.params.laData.chatChannel) {
              this.verto.unsubscribe(this.params.laData.chatChannel);
          }
          if (this.params.laData.infoChannel) {
              this.verto.unsubscribe(this.params.laData.infoChannel);
          }
          if (this.params.laData.modChannel) {
              this.verto.unsubscribe(this.params.laData.modChannel);
          }
      };
      VertoConfMan.prototype.listVideoLayouts = function () {
          this.modCommand('list-videoLayouts', null, null);
      };
      VertoConfMan.prototype.play = function (file) {
          this.modCommand('play', null, file);
      };
      VertoConfMan.prototype.stop = function () {
          this.modCommand('stop', null, 'all');
      };
      VertoConfMan.prototype.deaf = function (memberID) {
          this.modCommand('deaf', parseInt(memberID));
      };
      VertoConfMan.prototype.undeaf = function (memberID) {
          this.modCommand('undeaf', parseInt(memberID));
      };
      VertoConfMan.prototype.record = function (file) {
          this.modCommand('recording', null, ['start', file]);
      };
      VertoConfMan.prototype.stopRecord = function () {
          this.modCommand('recording', null, ['stop', 'all']);
      };
      VertoConfMan.prototype.snapshot = function (file) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          this.modCommand('vid-write-png', null, file);
      };
      VertoConfMan.prototype.setVideoLayout = function (layout, canvasID) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          if (canvasID) {
              this.modCommand('vid-layout', null, [layout, canvasID]);
          }
          else {
              this.modCommand('vid-layout', null, layout);
          }
      };
      VertoConfMan.prototype.kick = function (memberID) {
          this.modCommand('kick', parseInt(memberID));
      };
      VertoConfMan.prototype.muteMic = function (memberID) {
          this.modCommand('tmute', parseInt(memberID));
      };
      VertoConfMan.prototype.muteVideo = function (memberID) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          this.modCommand('tvmute', parseInt(memberID));
      };
      VertoConfMan.prototype.presenter = function (memberID) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          this.modCommand('vid-res-id', parseInt(memberID), 'presenter');
      };
      VertoConfMan.prototype.videoFloor = function (memberID) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          this.modCommand('vid-floor', parseInt(memberID), 'force');
      };
      VertoConfMan.prototype.banner = function (memberID, text) {
          if (!this.params.hasVid) {
              throw 'Conference has no video';
          }
          this.modCommand('vid-banner', parseInt(memberID), escape(text));
      };
      VertoConfMan.prototype.volumeDown = function (memberID) {
          this.modCommand('volume_out', parseInt(memberID), 'down');
      };
      VertoConfMan.prototype.volumeUp = function (memberID) {
          this.modCommand('volume_out', parseInt(memberID), 'up');
      };
      VertoConfMan.prototype.gainDown = function (memberID) {
          this.modCommand('volume_in', parseInt(memberID), 'down');
      };
      VertoConfMan.prototype.gainUp = function (memberID) {
          this.modCommand('volume_in', parseInt(memberID), 'up');
      };
      VertoConfMan.prototype.transfer = function (memberID, exten) {
          this.modCommand('transfer', parseInt(memberID), exten);
      };
      return VertoConfMan;
  }());

  /*
   * Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   * Copyright (C) 2005-2017, Anthony Minessale II <anthm@freeswitch.org>
   *
   * Version: MPL 1.1
   *
   * The contents of this file are subject to the Mozilla Public License Version
   * 1.1 (the "License"); you may not use this file except in compliance with
   * the License. You may obtain a copy of the License at
   * http://www.mozilla.org/MPL/
   *
   * Software distributed under the License is distributed on an "AS IS" basis,
   * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
   * for the specific language governing rights and limitations under the
   * License.
   *
   * The Original Code is Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   *
   * The Initial Developer of the Original Code is
   * Anthony Minessale II <anthm@freeswitch.org>
   * Portions created by the Initial Developer are Copyright (C)
   * the Initial Developer. All Rights Reserved.
   *
   * Contributor(s):
   *
   * Seven Du <dujinfang@x-y-t.cn>
   * Xueyun Jiang <jiangxueyun@x-y-t.cn>
   *
   * RTC.js - Verto RTC glue code
   *
   */
  // Find the line in sdpLines[startLine...endLine - 1] that starts with |prefix|
  // and, if specified, contains |substr| (case-insensitive search).
  function findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
      var realEndLine = endLine != -1 ? endLine : sdpLines.length;
      for (var i = startLine; i < realEndLine; ++i) {
          if (sdpLines[i].indexOf(prefix) === 0) {
              if (!substr ||
                  sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                  return i;
              }
          }
      }
      return null;
  }
  // Find the line in sdpLines that starts with |prefix|, and, if specified,
  // contains |substr| (case-insensitive search).
  function findLine(sdpLines, prefix, substr) {
      return findLineInRange(sdpLines, 0, -1, prefix, substr);
  }
  // Gets the codec payload type from an a=rtpmap:X line.
  function getCodecPayloadType(sdpLine) {
      var pattern = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+');
      var result = sdpLine.match(pattern);
      return result && result.length == 2 ? result[1] : null;
  }
  function onStreamError(self, e) {
      console.error('There has been a problem retrieving the streams - did you allow access? Check Device Resolution', e);
      doCallback(self, 'onError', e);
  }
  function onStreamSuccess(self, stream) {
      // console.log('Stream Success');
      doCallback(self, 'onStream', stream);
  }
  function onICE(self, candidate) {
      self.mediaData.candidate = candidate;
      self.mediaData.candidateList.push(self.mediaData.candidate);
      doCallback(self, 'onICE');
  }
  function doCallback(self, func, arg) {
      if (func in self.options.callbacks) {
          self.options.callbacks[func](self, arg);
      }
  }
  function onICEComplete(self, candidate) {
      // console.log('ICE Complete');
      doCallback(self, 'onICEComplete');
  }
  function onChannelError(self, e) {
      console.error('Channel Error', e);
      doCallback(self, 'onError', e);
  }
  function onICESDP(self, sdp) {
      self.mediaData.SDP = self.stereoHack(sdp.sdp);
      // console.log('ICE SDP');
      doCallback(self, 'onICESDP');
  }
  function FSRTCattachMediaStream(element, stream) {
      if (element &&
          element.id &&
          typeof window.attachMediaStream == 'function') {
          window.attachMediaStream(element, stream);
      }
      else {
          if (typeof element.srcObject !== 'undefined') {
              element.srcObject = stream;
          }
          else if (typeof element.src !== 'undefined') {
              element.src = URL.createObjectURL(stream);
          }
          else {
              console.error('Error attaching stream to element.');
          }
      }
  }
  function onRemoteStream(self, stream) {
      if (self.options.useVideo) {
          self.options.useVideo.style.display = 'block';
      }
      var element = self.options.useAudio;
      // console.log('REMOTE STREAM', stream, element);
      FSRTCattachMediaStream(element, stream);
      self.options.useAudio.play();
      self.remoteStream = stream;
  }
  function onOfferSDP(self, sdp) {
      self.mediaData.SDP = self.stereoHack(sdp.sdp);
      // console.log('Offer SDP');
      doCallback(self, 'onOfferSDP');
  }
  function FSRTCPeerConnection(options) {
      var gathering = false;
      var done = false;
      var config = {};
      var default_ice = {
          urls: ['stun:stun.l.google.com:19302'],
      };
      if (options.iceServers) {
          if (typeof options.iceServers === 'boolean') {
              config.iceServers = [default_ice];
          }
          else {
              config.iceServers = options.iceServers;
          }
      }
      var peer = new RTCPeerConnection(config);
      openOffererChannel();
      function ice_handler() {
          done = true;
          gathering = null;
          if (options.onICEComplete) {
              options.onICEComplete();
          }
          if (options.type == 'offer') {
              options.onICESDP(peer.localDescription);
          }
          else {
              if ( options.onICESDP) {
                  options.onICESDP(peer.localDescription);
              }
          }
      }
      peer.onicecandidate = function (event) {
          if (done) {
              return;
          }
          if (!gathering) {
              gathering = setTimeout(ice_handler, 1000);
          }
          if (event) {
              if (event.candidate) {
                  options.onICE(event.candidate);
              }
          }
          else {
              done = true;
              if (gathering) {
                  clearTimeout(gathering);
                  gathering = null;
              }
              ice_handler();
          }
      };
      // attachStream = MediaStream;
      if (options.attachStream && options.attachStream.getTracks().length) {
          for (var _i = 0, _a = options.attachStream.getTracks(); _i < _a.length; _i++) {
              var track = _a[_i];
              peer.addTrack(track);
          }
      }
      peer.ontrack = function (event) {
          var remoteMediaStream = event.streams[0];
          // onRemoteStreamEnded(MediaStream)
          remoteMediaStream.oninactive = function () {
              if (options.onRemoteStreamEnded)
                  options.onRemoteStreamEnded(remoteMediaStream);
          };
          // onRemoteStream(MediaStream)
          if (options.onRemoteStream)
              options.onRemoteStream(remoteMediaStream);
          //console.debug('on:add:stream', remoteMediaStream);
      };
      //const constraints = options.constraints || {
      //offerToReceiveAudio: true,
      //offerToReceiveVideo: true
      //};
      // onOfferSDP(RTCSessionDescription)
      function createOffer() {
          if (!options.onOfferSDP)
              return;
          peer.createOffer().then(function (offer) {
              return peer.setLocalDescription(offer);
          }).catch(function (reason) {
              console.error("error createOffer", reason);
          });
      }
      // onAnswerSDP(RTCSessionDescription)
      function createAnswer() {
          return __awaiter(this, void 0, void 0, function () {
              return __generator(this, function (_a) {
                  switch (_a.label) {
                      case 0:
                          if (options.type != 'answer')
                              return [2 /*return*/];
                          return [4 /*yield*/, peer.setRemoteDescription(options.offerSDP).catch(onSdpError)];
                      case 1:
                          _a.sent();
                          peer.createAnswer()
                              .then(function (answer) {
                              if (options.onAnswerSDP) {
                                  options.onAnswerSDP(answer);
                              }
                              return peer.setLocalDescription(answer);
                          })
                              .catch(onSdpError);
                          return [2 /*return*/];
                  }
              });
          });
      }
      if (options.onChannelMessage || !options.onChannelMessage) {
          createOffer();
          createAnswer();
      }
      // DataChannel management
      var channel;
      function openOffererChannel() {
          if (!options.onChannelMessage)
              return;
          _openOffererChannel();
          return;
      }
      function _openOffererChannel() {
          channel = peer.createDataChannel(options.channel || 'RTCDataChannel');
          setChannelEvents();
      }
      function setChannelEvents() {
          channel.onmessage = function (event) {
              if (options.onChannelMessage)
                  options.onChannelMessage(event);
          };
          channel.onopen = function () {
              if (options.onChannelOpened)
                  options.onChannelOpened(channel);
          };
          channel.onclose = function (event) {
              if (options.onChannelClosed)
                  options.onChannelClosed(event);
              console.warn('WebRTC DataChannel closed', event);
          };
          channel.onerror = function (event) {
              if (options.onChannelError)
                  options.onChannelError(event);
              console.error('WebRTC DataChannel error', event);
          };
      }
      function onSdpSuccess() { }
      function onSdpError(e) {
          if (options.onChannelError) {
              options.onChannelError(e);
          }
          console.error('sdp error:', e);
      }
      return {
          addAnswerSDP: function (sdp, cbSuccess, cbError) {
              peer.setRemoteDescription(new window.RTCSessionDescription(sdp))
                  .then(cbSuccess ? cbSuccess : onSdpSuccess)
                  .catch(cbError ? cbError : onSdpError);
          },
          addICE: function (candidate) {
              peer.addIceCandidate(new window.RTCIceCandidate({
                  sdpMLineIndex: candidate.sdpMLineIndex,
                  candidate: candidate.candidate,
              }));
          },
          peer: peer,
          channel: channel,
          sendData: function (message) {
              if (channel) {
                  channel.send(message);
              }
          },
          stop: function () {
              peer.close();
              if (options.attachStream) {
                  if (typeof options.attachStream.stop == 'function') {
                      options.attachStream.stop();
                  }
              }
          },
      };
  }
  // getUserMedia
  function getUserMedia(options) {
      return navigator.mediaDevices
          .getUserMedia(options.constraints)
          .then(function (stream) {
          if (options.localVideo) {
              options.localVideo['src'] = URL.createObjectURL(stream);
              options.localVideo.style.display = 'block';
          }
          if (options.onsuccess) {
              options.onsuccess(stream);
          }
      })
          .catch(options.onerror || console.error);
  }
  var resList = [
      [160, 120],
      [320, 180],
      [320, 240],
      [640, 360],
      [640, 480],
      [1280, 720],
      [1920, 1080],
  ];
  var resI = 0;
  var ttl = 0;
  /**
   * @hidden
   */
  var VertoRTC = /** @class */ (function () {
      function VertoRTC(options) {
          this.validRes = [];
          this.options = Object.assign({
              useVideo: null,
              useStereo: false,
              userData: null,
              localVideo: null,
              screenShare: false,
              useCamera: 'any',
              iceServers: false,
              videoParams: {},
              audioParams: {},
              callbacks: {
                  onICEComplete: function () { },
                  onICE: function () { },
                  onOfferSDP: function () { },
              },
          }, options);
          this.audioEnabled = true;
          this.videoEnabled = true;
          this.mediaData = {
              SDP: null,
              profile: {},
              candidateList: [],
          };
          this.constraints = {
              offerToReceiveAudio: this.options.useSpeak === 'none' ? false : true,
              offerToReceiveVideo: this.options.useVideo ? true : false,
          };
          if (this.options.useVideo) {
              this.options.useVideo.style.display = 'none';
          }
      }
      VertoRTC.prototype.useVideo = function (obj, local) {
          if (obj) {
              this.options.useVideo = obj;
              this.options.localVideo = local;
              this.constraints.offerToReceiveVideo = true;
          }
          else {
              this.options.useVideo = null;
              this.options.localVideo = null;
              this.constraints.offerToReceiveVideo = false;
          }
          if (this.options.useVideo) {
              this.options.useVideo.style.display = 'none';
          }
      };
      VertoRTC.prototype.useStereo = function (on) {
          this.options.useStereo = on;
      };
      // Sets Opus in stereo if stereo is enabled, by adding the stereo=1 fmtp param.
      VertoRTC.prototype.stereoHack = function (sdp) {
          if (!this.options.useStereo) {
              return sdp;
          }
          var sdpLines = sdp.split('\r\n');
          // Find opus payload.
          var opusIndex = findLine(sdpLines, 'a=rtpmap', 'opus/48000');
          var opusPayload;
          if (!opusIndex) {
              return sdp;
          }
          else {
              opusPayload = getCodecPayloadType(sdpLines[opusIndex]);
          }
          // Find the payload in fmtp line.
          var fmtpLineIndex = findLine(sdpLines, 'a=fmtp:' + opusPayload.toString());
          if (fmtpLineIndex === null) {
              // create an fmtp line
              sdpLines[opusIndex] =
                  sdpLines[opusIndex] +
                      '\r\na=fmtp:' +
                      opusPayload.toString() +
                      ' stereo=1; sprop-stereo=1';
          }
          else {
              // Append stereo=1 to fmtp line.
              sdpLines[fmtpLineIndex] = sdpLines[fmtpLineIndex].concat('; stereo=1; sprop-stereo=1');
          }
          sdp = sdpLines.join('\r\n');
          return sdp;
      };
      VertoRTC.prototype.answer = function (sdp, onSuccess, onError) {
          this.peer.addAnswerSDP({
              type: 'answer',
              sdp: sdp,
          }, onSuccess, onError);
      };
      VertoRTC.prototype.stopPeer = function () {
          if (this.peer) {
              // console.log('stopping peer');
              this.peer.stop();
          }
      };
      VertoRTC.prototype.stop = function () {
          if (this.options.useVideo) {
              this.options.useVideo.style.display = 'none';
              this.options.useVideo['src'] = '';
          }
          if (this.localStream) {
              if (typeof this.localStream.stop == 'function') {
                  this.localStream.stop();
              }
              else {
                  if (this.localStream.active) {
                      var tracks = this.localStream.getTracks();
                      // console.log(tracks);
                      tracks.forEach(function (track, index) {
                          // console.log('stopping track', track);
                          track.stop();
                      });
                  }
              }
              this.localStream = null;
          }
          if (this.options.localVideo) {
              this.options.localVideo.style.display = 'none';
              this.options.localVideo['src'] = '';
          }
          if (this.options.localVideoStream) {
              if (typeof this.options.localVideoStream.stop == 'function') {
                  this.options.localVideoStream.stop();
              }
              else {
                  if (this.options.localVideoStream.active) {
                      var tracks = this.options.localVideoStream.getTracks();
                      // console.log(tracks);
                      tracks.forEach(function (track, index) {
                          // console.log(track);
                          track.stop();
                      });
                  }
              }
          }
          if (this.peer) {
              // console.log('stopping peer');
              this.peer.stop();
          }
      };
      VertoRTC.prototype.getMute = function () {
          return this.audioEnabled;
      };
      VertoRTC.prototype.setMute = function (what) {
          var audioTracks = this.localStream.getAudioTracks();
          for (var i = 0, len = audioTracks.length; i < len; i++) {
              switch (what) {
                  case 'on':
                      audioTracks[i].enabled = true;
                      break;
                  case 'off':
                      audioTracks[i].enabled = false;
                      break;
                  case 'toggle':
                      audioTracks[i].enabled = !audioTracks[i].enabled;
                  default:
                      break;
              }
              this.audioEnabled = audioTracks[i].enabled;
          }
          return !this.audioEnabled;
      };
      VertoRTC.prototype.getVideoMute = function () {
          return this.videoEnabled;
      };
      VertoRTC.prototype.setVideoMute = function (what) {
          var videoTracks = this.localStream.getVideoTracks();
          for (var i = 0, len = videoTracks.length; i < len; i++) {
              switch (what) {
                  case 'on':
                      videoTracks[i].enabled = true;
                      break;
                  case 'off':
                      videoTracks[i].enabled = false;
                      break;
                  case 'toggle':
                      videoTracks[i].enabled = !videoTracks[i].enabled;
                  default:
                      break;
              }
              this.videoEnabled = videoTracks[i].enabled;
          }
          return !this.videoEnabled;
      };
      VertoRTC.prototype.getMediaParams = function () {
          var _this = this;
          var audio;
          if (this.options.useMic && this.options.useMic === 'none') {
              // console.log('Microphone Disabled');
              audio = false;
          }
          else if (this.options.videoParams && this.options.screenShare) {
              //this.options.videoParams.chromeMediaSource == 'desktop') {
              console.error('SCREEN SHARE', this.options.videoParams);
              audio = false;
          }
          else {
              audio = {};
              if (this.options.audioParams) {
                  audio = this.options.audioParams;
              }
              if (this.options.useMic !== 'any') {
                  //audio.optional = [{sourceId: this.options.useMic}]
                  audio.deviceId = { exact: this.options.useMic };
              }
          }
          if (this.options.useVideo && this.options.localVideo) {
              getUserMedia({
                  constraints: {
                      audio: false,
                      video: this.options.videoParams,
                  },
                  localVideo: this.options.localVideo,
                  onsuccess: function (e) {
                      _this.options.localVideoStream = e;
                      // console.log('local video ready');
                  },
                  onerror: function (e) {
                      console.error('local video error!');
                  },
              });
          }
          var video = {};
          var useVideo = false;
          var bestFrameRate = this.options.videoParams.vertoBestFrameRate;
          var minFrameRate = this.options.videoParams.minFrameRate || 15;
          delete this.options.videoParams.vertoBestFrameRate;
          if (this.options.screenShare) {
              console.log('screenShare');
              // fix for chrome to work for now, will need to change once we figure out how to do this in a non-mandatory style constraint.
              if (!!navigator.mozGetUserMedia) {
                  var dowin = confirm('Do you want to share an application window? If not you will share a screen.');
                  video = {
                      width: {
                          min: this.options.videoParams.minWidth,
                          max: this.options.videoParams.maxWidth,
                      },
                      height: {
                          min: this.options.videoParams.minHeight,
                          max: this.options.videoParams.maxHeight,
                      },
                      mediaSource: dowin ? 'window' : 'screen',
                  };
              }
              else {
                  var opt = [];
                  opt.push({ sourceId: this.options.useCamera });
                  if (bestFrameRate) {
                      opt.push({ minFrameRate: bestFrameRate });
                      opt.push({ maxFrameRate: bestFrameRate });
                  }
                  video = {
                      mandatory: this.options.videoParams,
                      optional: opt,
                  };
              }
          }
          else {
              video = {
                  //mandatory: this.options.videoParams,
                  width: {
                      min: this.options.videoParams.minWidth,
                      max: this.options.videoParams.maxWidth,
                  },
                  height: {
                      min: this.options.videoParams.minHeight,
                      max: this.options.videoParams.maxHeight,
                  },
              };
              useVideo = this.options.useVideo;
              if (useVideo &&
                  this.options.useCamera &&
                  this.options.useCamera !== 'none') {
                  //if (!video.optional) {
                  //video.optional = [];
                  //}
                  if (this.options.useCamera !== 'any') {
                      //video.optional.push({sourceId: obj.options.useCamera});
                      video.deviceId = this.options.useCamera;
                  }
                  if (bestFrameRate) {
                      //video.optional.push({minFrameRate: bestFrameRate});
                      //video.optional.push({maxFrameRate: bestFrameRate});
                      video.frameRate = {
                          ideal: bestFrameRate,
                          min: minFrameRate,
                          max: 30,
                      };
                  }
              }
              else {
                  console.log('Camera Disabled');
                  video = false;
                  useVideo = false;
              }
          }
          return { audio: audio, video: video, useVideo: useVideo };
      };
      VertoRTC.prototype.createAnswer = function (params) {
          var _this = this;
          this.type = 'answer';
          this.remoteSDP = params.sdp;
          // console.debug('inbound sdp: ', params.sdp);
          var onSuccess = function (stream) {
              _this.localStream = stream;
              _this.peer = FSRTCPeerConnection({
                  type: _this.type,
                  attachStream: _this.localStream,
                  onICE: function (candidate) { return onICE(_this, candidate); },
                  onICEComplete: function () { return onICEComplete(_this); },
                  onRemoteStream: function (stream) { return onRemoteStream(_this, stream); },
                  onICESDP: function (sdp) {
                      return onICESDP(_this, sdp);
                  },
                  onChannelError: function (e) {
                      return onChannelError(_this, e);
                  },
                  constraints: _this.constraints,
                  iceServers: _this.options.iceServers,
                  offerSDP: {
                      type: 'offer',
                      sdp: _this.remoteSDP,
                  },
              });
              onStreamSuccess(_this, stream);
          };
          var onError = function (e) {
              onStreamError(_this, e);
          };
          this.options.useCamera = params.useCamera;
          var mediaParams = this.getMediaParams();
          // console.log('Audio constraints', mediaParams.audio);
          // console.log('Video constraints', mediaParams.video);
          if (this.options.useVideo && this.options.localVideo) {
              getUserMedia({
                  constraints: {
                      audio: false,
                      video: {
                          mandatory: this.options.videoParams,
                          optional: [],
                      },
                  },
                  localVideo: this.options.localVideo,
                  onsuccess: function (e) {
                      this.options.localVideoStream = e;
                      // console.log('local video ready');
                  },
                  onerror: function (e) {
                      console.error('local video error!');
                  },
              });
          }
          getUserMedia({
              constraints: {
                  audio: mediaParams.audio,
                  video: mediaParams.video,
              },
              onsuccess: onSuccess,
              onerror: onError,
          });
      };
      VertoRTC.prototype.stopRinger = function (ringer) {
          if (!ringer)
              return;
          if (typeof ringer.stop == 'function') {
              ringer.stop();
          }
          else {
              if (ringer.active) {
                  var tracks = ringer.getTracks();
                  tracks.forEach(function (track, index) {
                      track.stop();
                  });
              }
          }
      };
      VertoRTC.prototype.call = function (profile) {
          var _this = this;
          var screen = false;
          this.type = 'offer';
          if (this.options.videoParams && this.options.screenShare) {
              //this.options.videoParams.chromeMediaSource == 'desktop') {
              screen = true;
          }
          var onSuccess = function (stream) {
              _this.localStream = stream;
              if (screen) {
                  _this.constraints.offerToReceiveVideo = false;
              }
              _this.peer = FSRTCPeerConnection({
                  type: _this.type,
                  attachStream: _this.localStream,
                  onICE: function (candidate) { return onICE(_this, candidate); },
                  onICEComplete: function () { return onICEComplete(_this); },
                  onRemoteStream: screen
                      ? function (stream) { }
                      : function (stream) { return onRemoteStream(_this, stream); },
                  onOfferSDP: function (sdp) { return onOfferSDP(_this, sdp); },
                  onICESDP: function (sdp) { return onICESDP(_this, sdp); },
                  onChannelError: function (e) { return onChannelError(_this, e); },
                  constraints: _this.constraints,
                  iceServers: _this.options.iceServers,
              });
              onStreamSuccess(_this, stream);
          };
          var onError = function (e) {
              onStreamError(_this, e);
          };
          var mediaParams = this.getMediaParams();
          // console.log('Audio constraints', mediaParams.audio);
          // console.log('Video constraints', mediaParams.video);
          if (mediaParams.audio || mediaParams.video) {
              getUserMedia({
                  constraints: {
                      audio: mediaParams.audio,
                      video: mediaParams.video,
                  },
                  video: mediaParams.useVideo,
                  onsuccess: onSuccess,
                  onerror: onError,
              });
          }
          else {
              onSuccess(null);
          }
      };
      VertoRTC.resSupported = function (w, h) {
          return this.validRes.find(function (res) { return res[0] === w && res[1] === h; });
      };
      VertoRTC.bestResSupported = function () {
          var w = 0;
          var h = 0;
          this.validRes.forEach(function (res) {
              if (res[0] >= w && res[1] >= h) {
                  w = res[0];
                  h = res[1];
              }
          });
          return [w, h];
      };
      VertoRTC.getValidRes = function (cam, func) {
          // const cached = localStorage.getItem('res_' + cam);
          // if (cached) {
          //   const cache = JSON.parse(cached);
          //   if (cache) {
          //     this.validRes = cache.validRes;
          //     // console.log('CACHED RES FOR CAM ' + cam, cache);
          //   } else {
          //     console.error('INVALID CACHE');
          //   }
          //   return func ? func(cache) : null;
          // }
          this.validRes = [];
          resI = 0;
          this.checkRes(cam, func);
      };
      VertoRTC.checkPerms = function (runtime, check_audio, check_video) {
          var _this = this;
          getUserMedia({
              constraints: {
                  audio: check_audio,
                  video: check_video,
              },
              onsuccess: function (e) {
                  e.getTracks().forEach(function (track) {
                      track.stop();
                  });
                  // console.info('media perm init complete');
                  if (runtime) {
                      setTimeout(function () { return runtime(true); }, 100);
                  }
              },
              onerror: function (e) {
                  if (check_video && check_audio) {
                      // console.error('error, retesting with audio params only');
                      return _this.checkPerms(runtime, check_audio, false);
                  }
                  console.error(e);
                  console.error('media perm init error');
                  if (runtime) {
                      runtime(false);
                  }
              },
          });
      };
      VertoRTC.checkRes = function (cam, func) {
          var _this = this;
          if (resI >= resList.length) {
              var res = {
                  validRes: this.validRes,
                  bestResSupported: this.bestResSupported(),
              };
              // localStorage.setItem('res_' + cam, JSON.stringify(res));
              if (func)
                  return func(res);
              return;
          }
          var video = {
              mandatory: {},
              optional: [],
          };
          if (cam) {
              video.optional = [{ sourceId: cam }];
              // video.deviceId = { exact: cam };
          }
          var w = resList[resI][0];
          var h = resList[resI][1];
          resI++;
          video = {
              // width: { exact: w },
              // height: { exact: h },
              minWidth: w,
              minHeight: h,
              maxWidth: w,
              maxHeight: h,
          };
          getUserMedia({
              constraints: {
                  audio: ttl++ == 0,
                  video: cam !== 'none' ? video : false,
              },
              onsuccess: function (e) {
                  e.getTracks().forEach(function (track) {
                      track.stop();
                  });
                  // console.info(w + 'x' + h + ' supported.');
                  _this.validRes.push([w, h]);
                  _this.checkRes(cam, func);
              },
              onerror: function (e) {
                  console.warn(w + 'x' + h + ' not supported.');
                  _this.checkRes(cam, func);
              },
          });
      };
      VertoRTC.validRes = [];
      return VertoRTC;
  }());

  var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
  /**
   * Enum for TelnyxRTC's call states. These should be ordered and auto-incrementing.
   * @hidden
   */
  var TelnyxRTCCallState;
  (function (TelnyxRTCCallState) {
      TelnyxRTCCallState[TelnyxRTCCallState["new"] = 1] = "new";
      TelnyxRTCCallState[TelnyxRTCCallState["requesting"] = 2] = "requesting";
      TelnyxRTCCallState[TelnyxRTCCallState["trying"] = 3] = "trying";
      TelnyxRTCCallState[TelnyxRTCCallState["recovering"] = 4] = "recovering";
      TelnyxRTCCallState[TelnyxRTCCallState["ringing"] = 5] = "ringing";
      TelnyxRTCCallState[TelnyxRTCCallState["answering"] = 6] = "answering";
      TelnyxRTCCallState[TelnyxRTCCallState["early"] = 7] = "early";
      TelnyxRTCCallState[TelnyxRTCCallState["active"] = 8] = "active";
      TelnyxRTCCallState[TelnyxRTCCallState["held"] = 9] = "held";
      TelnyxRTCCallState[TelnyxRTCCallState["hangup"] = 10] = "hangup";
      TelnyxRTCCallState[TelnyxRTCCallState["destroy"] = 11] = "destroy";
      TelnyxRTCCallState[TelnyxRTCCallState["purge"] = 12] = "purge";
  })(TelnyxRTCCallState || (TelnyxRTCCallState = {}));
  /**
   * @hidden
   */
  var Direction;
  (function (Direction) {
      Direction["inbound"] = "inbound";
      Direction["outbound"] = "outbound";
  })(Direction || (Direction = {}));
  /**
   * @hidden
   */
  var Message;
  (function (Message) {
      Message["display"] = "display";
      Message["info"] = "info";
      Message["pvtEvent"] = "pvtEvent";
      Message["clientReady"] = "clientReady";
  })(Message || (Message = {}));
  var Enum = {
      states: Object.freeze((_a = {},
          _a[TelnyxRTCCallState.new] = (_b = {},
              _b[TelnyxRTCCallState.requesting] = 1,
              _b[TelnyxRTCCallState.recovering] = 1,
              _b[TelnyxRTCCallState.ringing] = 1,
              _b[TelnyxRTCCallState.destroy] = 1,
              _b[TelnyxRTCCallState.answering] = 1,
              _b[TelnyxRTCCallState.hangup] = 1,
              _b),
          _a[TelnyxRTCCallState.requesting] = (_c = {},
              _c[TelnyxRTCCallState.trying] = 1,
              _c[TelnyxRTCCallState.hangup] = 1,
              _c),
          _a[TelnyxRTCCallState.recovering] = (_d = {},
              _d[TelnyxRTCCallState.answering] = 1,
              _d[TelnyxRTCCallState.hangup] = 1,
              _d),
          _a[TelnyxRTCCallState.trying] = (_e = {},
              _e[TelnyxRTCCallState.active] = 1,
              _e[TelnyxRTCCallState.early] = 1,
              _e[TelnyxRTCCallState.hangup] = 1,
              _e),
          _a[TelnyxRTCCallState.ringing] = (_f = {},
              _f[TelnyxRTCCallState.answering] = 1,
              _f[TelnyxRTCCallState.hangup] = 1,
              _f),
          _a[TelnyxRTCCallState.answering] = (_g = {},
              _g[TelnyxRTCCallState.active] = 1,
              _g[TelnyxRTCCallState.hangup] = 1,
              _g),
          _a[TelnyxRTCCallState.active] = (_h = {},
              _h[TelnyxRTCCallState.answering] = 1,
              _h[TelnyxRTCCallState.requesting] = 1,
              _h[TelnyxRTCCallState.hangup] = 1,
              _h[TelnyxRTCCallState.held] = 1,
              _h),
          _a[TelnyxRTCCallState.held] = (_j = {},
              _j[TelnyxRTCCallState.hangup] = 1,
              _j[TelnyxRTCCallState.active] = 1,
              _j),
          _a[TelnyxRTCCallState.early] = (_k = {},
              _k[TelnyxRTCCallState.hangup] = 1,
              _k[TelnyxRTCCallState.active] = 1,
              _k),
          _a[TelnyxRTCCallState.hangup] = (_l = {},
              _l[TelnyxRTCCallState.destroy] = 1,
              _l),
          _a[TelnyxRTCCallState.destroy] = {},
          _a[TelnyxRTCCallState.purge] = (_m = {},
              _m[TelnyxRTCCallState.destroy] = 1,
              _m),
          _a)),
      state: TelnyxRTCCallState,
      direction: Direction,
      message: Message,
  };

  /*
   * Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   * Copyright (C) 2005-2017, Anthony Minessale II <anthm@freeswitch.org>
   *
   * Version: MPL 1.1
   *
   * The contents of this file are subject to the Mozilla Public License Version
   * 1.1 (the "License"); you may not use this file except in compliance with
   * the License. You may obtain a copy of the License at
   * http://www.mozilla.org/MPL/
   *
   * Software distributed under the License is distributed on an "AS IS" basis,
   * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
   * for the specific language governing rights and limitations under the
   * License.
   *
   * The Original Code is Verto HTML5/Javascript Telephony Signaling and Control Protocol Stack for FreeSWITCH
   *
   * The Initial Developer of the Original Code is
   * Anthony Minessale II <anthm@freeswitch.org>
   * Portions created by the Initial Developer are Copyright (C)
   * the Initial Developer. All Rights Reserved.
   *
   * Contributor(s):
   *
   * Seven Du <dujinfang@x-y-t.cn>
   * Xueyun Jiang <jiangxueyun@x-y-t.cn>
   *
   * verto-dialog.js - Verto Dialog
   *
   */
  /**
   * @hidden
   */
  var VertoDialog = /** @class */ (function () {
      function VertoDialog(direction, verto, params) {
          var _this = this;
          this.params = Object.assign({
              useVideo: verto.options.useVideo,
              useStereo: verto.options.useStereo,
              screenShare: false,
              useCamera: params.screenShare
                  ? false
                  : verto.options.deviceParams.useCamera,
              useMic: verto.options.deviceParams.useMic,
              useSpeak: verto.options.deviceParams.useSpeak,
              tag: verto.options.tag,
              localTag: verto.options.localTag,
              login: verto.options.login,
              videoParams: verto.options.videoParams,
          }, params);
          this.verto = verto;
          this.direction = direction;
          this.lastState = null;
          this.state = this.lastState = Enum.state.new;
          this.callbacks = verto.callbacks;
          this.answered = false;
          this.attach = params.attach || false;
          this.screenShare = params.screenShare || false;
          this.useCamera = this.params.useCamera;
          this.useMic = this.params.useMic;
          this.useSpeak = this.params.useSpeak;
          this.onStateChange = params.onStateChange;
          this.rtc = null;
          if (this.params.callID) {
              this.callID = this.params.callID;
          }
          else {
              this.callID = this.params.callID = v1_1();
          }
          if (typeof this.params.tag === 'function') {
              this.audioStream = this.params.tag();
          }
          else if (this.params.tag) {
              this.audioStream = document.querySelector(this.params.tag);
          }
          if (this.params.useVideo) {
              this.videoStream = this.audioStream;
          }
          if (this.params.localTag) {
              this.localVideo = document.querySelector(this.params.localTag);
          }
          this.verto.dialogs[this.callID] = this;
          var RTCcallbacks = {};
          if (this.direction == Enum.direction.inbound) {
              if (this.params.display_direction === 'outbound') {
                  this.params.remote_caller_id_name = this.params.caller_id_name;
                  this.params.remote_caller_id_number = this.params.caller_id_number;
              }
              else {
                  this.params.remote_caller_id_name = this.params.callee_id_name;
                  this.params.remote_caller_id_number = this.params.callee_id_number;
              }
              if (!this.params.remote_caller_id_name) {
                  this.params.remote_caller_id_name = 'Nobody';
              }
              if (!this.params.remote_caller_id_number) {
                  this.params.remote_caller_id_number = 'UNKNOWN';
              }
              RTCcallbacks.onMessage = function (rtc, msg) {
                  console.debug(msg);
              };
              RTCcallbacks.onAnswerSDP = function (rtc, sdp) {
                  console.error('answer sdp', sdp);
              };
          }
          else {
              this.params.remote_caller_id_name = 'Outbound Call';
              this.params.remote_caller_id_number = this.params.destination_number;
          }
          RTCcallbacks.onICESDP = function (rtc) {
              // console.log('RECV ' + rtc.type + ' SDP', rtc.mediaData.SDP);
              if (_this.state == Enum.state.requesting ||
                  _this.state == Enum.state.answering ||
                  _this.state == Enum.state.active) {
                  location.reload();
                  return;
              }
              if (rtc.type == 'offer') {
                  if (_this.state == Enum.state.active) {
                      _this.setState(Enum.state.requesting);
                      _this.sendMethod(_this.verto.module + ".attach", {
                          sdp: rtc.mediaData.SDP,
                      });
                  }
                  else {
                      _this.setState(Enum.state.requesting);
                      _this.sendMethod(_this.verto.module + ".invite", {
                          sdp: rtc.mediaData.SDP,
                      });
                  }
              }
              else {
                  //answer
                  _this.setState(Enum.state.answering);
                  _this.sendMethod(_this.attach ? _this.verto.module + ".attach" : _this.verto.module + ".answer", {
                      sdp: _this.rtc.mediaData.SDP,
                  });
              }
          };
          RTCcallbacks.onICE = function (rtc) {
              //console.log("cand", rtc.mediaData.candidate);
              if (rtc.type == 'offer') {
                  // console.log('offer', rtc.mediaData.candidate);
                  return;
              }
          };
          RTCcallbacks.onStream = function (rtc, stream) {
              if (_this.verto.options.permissionCallback &&
                  typeof _this.verto.options.permissionCallback.onGranted === 'function') {
                  _this.verto.options.permissionCallback.onGranted(stream);
              }
              // console.log('stream started');
          };
          RTCcallbacks.onError = function (e) {
              if (_this.verto.options.permissionCallback &&
                  typeof _this.verto.options.permissionCallback.onDenied === 'function') {
                  _this.verto.options.permissionCallback.onDenied();
              }
              // console.error('ERROR:', e);
              _this.hangup({ cause: 'Device or Permission Error' });
          };
          this.rtc = new VertoRTC({
              callbacks: RTCcallbacks,
              localVideo: this.screenShare ? null : this.localVideo,
              useVideo: this.params.useVideo ? this.videoStream : null,
              useAudio: this.audioStream,
              useStereo: this.params.useStereo || false,
              videoParams: this.params.videoParams || {},
              audioParams: verto.options.audioParams || {},
              iceServers: verto.options.iceServers,
              screenShare: this.screenShare,
              useCamera: this.useCamera,
              useMic: this.useMic,
              useSpeak: this.useSpeak,
          });
          if (this.direction == Enum.direction.inbound) {
              if (this.attach) {
                  this.answer();
              }
              else {
                  this.ring();
              }
          }
      }
      VertoDialog.prototype.invite = function () {
          this.rtc.call();
      };
      VertoDialog.prototype.sendMethod = function (method, obj) {
          var _this = this;
          obj.dialogParams = {};
          for (var i in this.params) {
              if (i == 'sdp' && method != this.verto.module + ".invite" && method != this.verto.module + ".attach") {
                  continue;
              }
              if (obj.noDialogParams && i != 'callID') {
                  continue;
              }
              obj.dialogParams[i] = this.params[i];
          }
          delete obj.noDialogParams;
          this.verto.call(method, obj, function (e) {
              /* Success */
              _this.processReply(method, true, e);
          }, function (e) {
              /* Error */
              _this.processReply(method, false, e);
          });
      };
      VertoDialog.prototype.setAudioOutDevice = function (sinkId, callback, arg) {
          var _this = this;
          var element = this.audioStream;
          if (typeof element.sinkId !== 'undefined') {
              var devname_1 = this.find_name(sinkId);
              // console.info(
              //   'Dialog: ' + this.callID + ' Setting speaker:',
              //   element,
              //   devname
              // );
              return element
                  .setSinkId(sinkId)
                  .then(function () {
                  // console.log(
                  //   'Dialog: ' +
                  //     this.callID +
                  //     ' Success, audio output device attached: ' +
                  //     sinkId
                  // );
                  if (callback) {
                      callback(true, devname_1, arg);
                  }
              })
                  .catch(function (error) {
                  var errorMessage = error;
                  if (error.name === 'SecurityError') {
                      errorMessage =
                          'Dialog: ' +
                              _this.callID +
                              ' You need to use HTTPS for selecting audio output ' +
                              'device: ' +
                              error;
                  }
                  if (callback) {
                      callback(false, null, arg);
                  }
                  console.error(errorMessage);
              });
          }
          else {
              console.warn('Dialog: ' +
                  this.callID +
                  ' Browser does not support output device selection.');
              if (callback) {
                  callback(false, null, arg);
              }
              return Promise.reject();
          }
      };
      VertoDialog.prototype.setState = function (state) {
          var _this = this;
          if (this.state == Enum.state.ringing) {
              this.stopRinging();
          }
          if (this.state == state || !this.checkStateChange(this.state, state)) {
              console.error('Dialog ' +
                  this.callID +
                  ': INVALID state change from ' +
                  this.state +
                  ' to ' +
                  state);
              this.hangup();
              return false;
          }
          // console.log(
          //   'Dialog ' +
          //     this.callID +
          //     ': state change from ' +
          //     this.state.name +
          //     ' to ' +
          //     state.name
          // );
          this.lastState = this.state;
          this.state = state;
          if (this.callbacks.onDialogState) {
              this.callbacks.onDialogState(this);
          }
          if (this.onStateChange) {
              this.onStateChange(this.state, this.lastState);
          }
          switch (this.state) {
              case Enum.state.early:
              case Enum.state.active:
                  var speaker_1 = this.useSpeak;
                  // console.info('Using Speaker: ', speaker);
                  if (speaker_1 && speaker_1 !== 'any' && speaker_1 !== 'none') {
                      setTimeout(function () {
                          _this.setAudioPlaybackDevice(speaker_1);
                      }, 500);
                  }
                  break;
              case Enum.state.trying:
                  setTimeout(function () {
                      if (_this.state == Enum.state.trying) {
                          _this.setState(Enum.state.hangup);
                      }
                  }, 30000);
                  break;
              case Enum.state.purge:
                  this.setState(Enum.state.destroy);
                  break;
              case Enum.state.hangup:
                  if (this.lastState > Enum.state.requesting &&
                      this.lastState < Enum.state.hangup) {
                      this.sendMethod(this.verto.module + ".bye", {});
                  }
                  this.setState(Enum.state.destroy);
                  break;
              case Enum.state.destroy:
                  // TODO: This should probably only happen when a tag wasn't specified.
                  // if (typeof this.verto.options.tag === 'function') {
                  //   this.verto.options.tag().remove();
                  // }
                  delete this.verto.dialogs[this.callID];
                  if (this.params.screenShare) {
                      this.rtc.stopPeer();
                  }
                  else {
                      this.rtc.stop();
                  }
                  break;
          }
          return true;
      };
      VertoDialog.prototype.processReply = function (method, success, e) {
          // console.log(
          //   'Response: ' + method + ' State:' + this.state.name,
          //   success,
          //   e
          // );
          switch (method) {
              case this.verto.module + ".answer":
              case this.verto.module + ".attach":
                  if (success) {
                      this.setState(Enum.state.active);
                  }
                  else {
                      this.hangup();
                  }
                  break;
              case this.verto.module + ".invite":
                  if (success) {
                      this.setState(Enum.state.trying);
                  }
                  else {
                      this.setState(Enum.state.destroy);
                  }
                  break;
              case this.verto.module + ".bye":
                  this.hangup();
                  break;
              case this.verto.module + ".modify":
                  if (e.holdState) {
                      if (e.holdState == 'held') {
                          if (this.state != Enum.state.held) {
                              this.setState(Enum.state.held);
                          }
                      }
                      else if (e.holdState == 'active') {
                          if (this.state != Enum.state.active) {
                              this.setState(Enum.state.active);
                          }
                      }
                  }
                  break;
              default:
                  break;
          }
      };
      VertoDialog.prototype.hangup = function (params) {
          if (params) {
              if (params.causeCode) {
                  this.causeCode = params.causeCode;
              }
              if (params.cause) {
                  this.cause = params.cause;
              }
          }
          if (!this.cause && !this.causeCode) {
              this.cause = 'NORMAL_CLEARING';
          }
          if (this.state >= Enum.state.new && this.state < Enum.state.hangup) {
              this.setState(Enum.state.hangup);
          }
          else if (this.state < Enum.state.destroy) {
              this.setState(Enum.state.destroy);
          }
      };
      VertoDialog.prototype.stopRinging = function () {
          // console.log('stop ringing');
          if (this.verto.ringer) {
              this.rtc.stopRinger(this.verto.ringer);
          }
      };
      VertoDialog.prototype.indicateRing = function () {
          var _this = this;
          if (this.verto.ringer) {
              this.verto.ringer.src = this.verto.options.ringFile;
              this.verto.ringer.play();
              // console.log('playing ringer');
              setTimeout(function () {
                  _this.stopRinging();
                  if (_this.state == Enum.state.ringing) {
                      _this.indicateRing();
                  }
              }, this.verto.options.ringSleep);
          }
      };
      VertoDialog.prototype.ring = function () {
          this.setState(Enum.state.ringing);
          this.indicateRing();
      };
      VertoDialog.prototype.useVideo = function (on) {
          this.params.useVideo = on;
          if (on) {
              this.videoStream = this.audioStream;
          }
          else {
              this.videoStream = null;
          }
          this.rtc.useVideo(this.videoStream, this.localVideo);
      };
      VertoDialog.prototype.setMute = function (what) {
          return this.rtc.setMute(what);
      };
      VertoDialog.prototype.mute = function () {
          return this.rtc.setMute('on');
      };
      VertoDialog.prototype.unmute = function () {
          return this.rtc.setMute('off');
      };
      VertoDialog.prototype.toggleMute = function () {
          return this.rtc.setMute('toggle');
      };
      VertoDialog.prototype.getMute = function () {
          return this.rtc.getMute();
      };
      VertoDialog.prototype.setVideoMute = function (what) {
          return this.rtc.setVideoMute(what);
      };
      VertoDialog.prototype.$getVideoMute = function () {
          return this.rtc.getVideoMute();
      };
      VertoDialog.prototype.useStereo = function (on) {
          this.params.useStereo = on;
          this.rtc.useStereo(on);
      };
      VertoDialog.prototype.dtmf = function (digits) {
          if (digits) {
              this.sendMethod(this.verto.module + ".info", {
                  dtmf: digits,
              });
          }
      };
      VertoDialog.prototype.rtt = function (obj) {
          var pobj = {};
          if (!obj) {
              return false;
          }
          pobj.code = obj.code;
          pobj.chars = obj.chars;
          if (pobj.chars || pobj.code) {
              this.sendMethod(this.verto.module + ".info", {
                  txt: obj,
                  noDialogParams: true,
              });
          }
      };
      VertoDialog.prototype.transfer = function (dest, params) {
          if (dest) {
              this.sendMethod(this.verto.module + ".modify", {
                  action: 'transfer',
                  destination: dest,
                  params: params,
              });
          }
      };
      VertoDialog.prototype.hold = function (params) {
          this.sendMethod(this.verto.module + ".modify", {
              action: 'hold',
              params: params,
          });
      };
      VertoDialog.prototype.unhold = function (params) {
          this.sendMethod(this.verto.module + ".modify", {
              action: 'unhold',
              params: params,
          });
      };
      VertoDialog.prototype.toggleHold = function (params) {
          this.sendMethod(this.verto.module + ".modify", {
              action: 'toggleHold',
              params: params,
          });
      };
      VertoDialog.prototype.message = function (msg) {
          var err = 0;
          msg.from = this.params.login;
          if (!msg.to) {
              console.error('Missing To');
              err++;
          }
          if (!msg.body) {
              console.error('Missing Body');
              err++;
          }
          if (err) {
              return false;
          }
          this.sendMethod(this.verto.module + ".info", {
              msg: msg,
          });
          return true;
      };
      VertoDialog.prototype.answer = function (params) {
          if (this.answered)
              return;
          if (!params)
              params = {};
          params.sdp = this.params.sdp;
          if (params) {
              if (params.useVideo) {
                  this.useVideo(true);
              }
              this.params.callee_id_name = params.callee_id_name;
              this.params.callee_id_number = params.callee_id_number;
              if (params.useCamera) {
                  this.useCamera = params.useCamera;
              }
              if (params.useMic) {
                  this.useMic = params.useMic;
              }
              if (params.useSpeak) {
                  this.useSpeak = params.useSpeak;
              }
          }
          this.rtc.createAnswer(params);
          this.answered = true;
      };
      VertoDialog.prototype.handleAnswer = function (params) {
          var _this = this;
          this.gotAnswer = true;
          if (this.state >= Enum.state.active) {
              return;
          }
          if (this.state >= Enum.state.early) {
              this.setState(Enum.state.active);
          }
          else {
              if (this.gotEarly) ;
              else {
                  // console.log('Dialog ' + this.callID + ' Answering Channel');
                  this.rtc.answer(params.sdp, function () {
                      _this.setState(Enum.state.active);
                  }, function (e) {
                      console.error(e);
                      _this.hangup();
                  });
                  // console.log('Dialog ' + this.callID + 'ANSWER SDP', params.sdp);
              }
          }
      };
      VertoDialog.prototype.cidString = function (enc) {
          var party = this.params.remote_caller_id_name +
              (enc ? ' &lt;' : ' <') +
              this.params.remote_caller_id_number +
              (enc ? '&gt;' : '>');
          return party;
      };
      VertoDialog.prototype.sendMessage = function (msg, params) {
          if (this.callbacks.onMessage) {
              this.callbacks.onMessage(this.verto, this, msg, params);
          }
      };
      VertoDialog.prototype.handleInfo = function (params) {
          this.sendMessage(Enum.message.info, params);
      };
      VertoDialog.prototype.handleDisplay = function (params) {
          if (params.display_name) {
              this.params.remote_caller_id_name = params.display_name;
          }
          if (params.display_number) {
              this.params.remote_caller_id_number = params.display_number;
          }
          this.sendMessage(Enum.message.display, {});
      };
      VertoDialog.prototype.handleMedia = function (params) {
          var _this = this;
          if (this.state >= Enum.state.early) {
              return;
          }
          this.gotEarly = true;
          this.rtc.answer(params.sdp, function () {
              // console.log('Dialog ' + this.callID + 'Establishing early media');
              _this.setState(Enum.state.early);
              if (_this.gotAnswer) {
                  // console.log('Dialog ' + this.callID + 'Answering Channel');
                  _this.setState(Enum.state.active);
              }
          }, function (e) {
              console.error(e);
              _this.hangup();
          });
          // console.log('Dialog ' + this.callID + 'EARLY SDP', params.sdp);
      };
      VertoDialog.prototype.checkStateChange = function (oldS, newS) {
          if (newS == Enum.state.purge || Enum.states[oldS][newS]) {
              return true;
          }
          return false;
      };
      VertoDialog.prototype.find_name = function (id) {
          var sourceMatch = this.verto.audioOutDevices.find(function (source) { return source.id === id; });
          return sourceMatch ? sourceMatch.label : id;
      };
      return VertoDialog;
  }());

  /**
   * @TODO Use a WS package and remove the retry logic in this module.
   * @hidden
   */
  var Verto = /** @class */ (function () {
      function Verto(params, callbacks) {
          this._ws_socket = null;
          this.q = [];
          this._ws_callbacks = {};
          this._current_id = 0;
          this.options = {};
          this.SERNO = 1;
          this.dialog = null;
          this.dialogs = {};
          this.params = params;
          this.module = params.module;
          this.callbacks = callbacks;
          this.rpcClient = this; // backward compatible
          this.generateGUID = v1_1();
          this.connect();
      }
      Verto.prototype.connect = function (params, callbacks) {
          var _this = this;
          if (params === void 0) { params = this.params; }
          if (callbacks === void 0) { callbacks = this.callbacks; }
          if (!params || !params.socketUrl) {
              return;
          }
          this.options = Object.assign({
              login: null,
              password: null,
              socketUrl: null,
              tag: null,
              localTag: null,
              videoParams: {},
              audioParams: {},
              loginParams: {},
              deviceParams: {
                  onResCheck: null,
              },
              userVariables: {},
              iceServers: false,
              ringSleep: 6000,
              sessid: null,
              onmessage: function (e) {
                  return _this.handleMessage(e.eventData);
              },
          }, params, callbacks);
          if (this.options.deviceParams.useCamera !== 'none') {
              VertoRTC.getValidRes(this.options.deviceParams.useCamera, this.options.deviceParams.onResCheck);
          }
          if (!this.options.deviceParams.useMic) {
              this.options.deviceParams.useMic = 'any';
          }
          if (!this.options.deviceParams.useSpeak) {
              this.options.deviceParams.useSpeak = 'any';
          }
          if (this.options.sessid) {
              this.sessid = this.options.sessid;
          }
          else {
              this.sessid =
                  localStorage.getItem('verto_session_uuid') || v1_1();
              localStorage.setItem('verto_session_uuid', this.sessid);
          }
          this.dialogs = {};
          this.callbacks = callbacks || {};
          this.eventSUBS = {};
          this.connectSocket();
          var tag = this.options.tag;
          var element = typeof tag === 'function' ? tag() : document.querySelector(tag);
          if (this.options.ringFile && element) {
              this.ringer = element;
          }
          this.login();
      };
      Verto.prototype.connectSocket = function () {
          var _this = this;
          if (this.to) {
              clearTimeout(this.to);
          }
          if (!this.socketReady()) {
              this.authing = false;
              if (this._ws_socket) {
                  delete this._ws_socket;
              }
              // No socket, or dying socket, let's get a new one.
              this._ws_socket = new WebSocket(this.options.socketUrl);
              if (this._ws_socket) {
                  // Set up onmessage handler.
                  this._ws_socket.onmessage = function (event) {
                      _this._onMessage(event);
                  };
                  this._ws_socket.onclose = function (w) {
                      if (!_this.ws_sleep) {
                          _this.ws_sleep = 1000;
                          _this.ws_cnt = 0;
                      }
                      if (_this.options.onWSClose) {
                          _this.options.onWSClose(_this);
                      }
                      console.error('Websocket Lost ' +
                          _this.ws_cnt +
                          ' sleep: ' +
                          _this.ws_sleep +
                          'msec');
                      _this.to = setTimeout(function () {
                          console.log('Attempting Reconnection....');
                          _this.connectSocket();
                      }, _this.ws_sleep);
                      _this.ws_cnt++;
                      if (_this.ws_sleep < 3000 && _this.ws_cnt % 10 === 0) {
                          _this.ws_sleep += 1000;
                      }
                  };
                  // Set up sending of message for when the socket is open.
                  this._ws_socket.onopen = function () {
                      if (_this.to) {
                          clearTimeout(_this.to);
                      }
                      _this.ws_sleep = 1000;
                      _this.ws_cnt = 0;
                      if (_this.options.onWSConnect) {
                          _this.options.onWSConnect(_this);
                      }
                      var req;
                      while ((req = _this.q.pop())) {
                          _this._ws_socket.send(req);
                      }
                  };
              }
          }
          return Boolean(this._ws_socket);
      };
      Verto.prototype.socketReady = function () {
          if (this._ws_socket === null || this._ws_socket.readyState > 1) {
              return false;
          }
          return true;
      };
      Verto.prototype.purge = function () {
          var _this = this;
          // purging dialogs
          Object.keys(this.dialogs).forEach(function (dialog) {
              _this.dialogs[dialog].setState(Enum.state.purge);
          });
          this.eventSUBS = {};
      };
      Verto.prototype.call = function (method, params, success_cb, error_cb) {
          // Construct the JSON-RPC 2.0 request.
          if (!params) {
              params = {};
          }
          if (this.sessid) {
              params.sessid = this.sessid;
          }
          var request = {
              jsonrpc: '2.0',
              method: method,
              params: params,
              id: this._current_id++,
          };
          if (!success_cb) {
              success_cb = function (e) {
                  console.log('Success: ', e);
              };
          }
          if (!error_cb) {
              error_cb = function (e) {
                  console.log('Error: ', e);
              };
          }
          var requestJson = JSON.stringify(request);
          if (this._ws_socket.readyState < 1) {
              // The websocket is not open yet; we have to set sending of the message in onopen.
              this.q.push(requestJson);
          }
          else {
              // We have a socket and it should be ready to send on.
              this._ws_socket.send(requestJson);
          }
          // Setup callbacks.  If there is an id, this is a call and not a notify.
          if ('id' in request && typeof success_cb !== 'undefined') {
              this._ws_callbacks[request.id] = {
                  request: requestJson,
                  request_obj: request,
                  success_cb: success_cb,
                  error_cb: error_cb,
              };
          }
      };
      Verto.prototype._onMessage = function (event) {
          var _this = this;
          // Check if this could be a JSON RPC message.
          var response;
          // Special sub proto
          if (event.data[0] == '#' && event.data[1] == 'S' && event.data[2] == 'P') {
              if (event.data[3] == 'U') {
                  this.up_dur = parseInt(event.data.substring(4));
              }
              else if (this.speedCB && event.data[3] == 'D') {
                  this.down_dur = parseInt(event.data.substring(4));
                  var up_kps = ((this.speedBytes * 8) /
                      (this.up_dur / 1000) /
                      1024).toFixed(0);
                  var down_kps = ((this.speedBytes * 8) /
                      (this.down_dur / 1000) /
                      1024).toFixed(0);
                  console.info('Speed Test: Up: ' + up_kps + ' Down: ' + down_kps);
                  this.speedCB(event, {
                      upDur: this.up_dur,
                      downDur: this.down_dur,
                      upKPS: up_kps,
                      downKPS: down_kps,
                  });
                  this.speedCB = null;
              }
              return;
          }
          response = JSON.parse(event.data);
          if (typeof response === 'object' &&
              'jsonrpc' in response &&
              response.jsonrpc === '2.0') {
              /// @todo Handle bad response (without id).
              // If this is an object with result, it is a response.
              if ('result' in response && this._ws_callbacks[response.id]) {
                  // Get the success lcallback.
                  var success_cb = this._ws_callbacks[response.id].success_cb;
                  // set the sessid if present
                  // if ('sessid' in response.result && !this.options.sessid || (this.options.sessid != response.result.sessid)) {
                  //     this.options.sessid = response.result.sessid;
                  //     if (this.options.sessid) {
                  //         console.log("setting session UUID to: " + this.options.sessid);
                  //     }
                  // }
                  // Delete the callback from the storage.
                  delete this._ws_callbacks[response.id];
                  // Run callback with result as parameter.
                  success_cb(response.result, this);
                  return;
              }
              else if ('error' in response && this._ws_callbacks[response.id]) {
                  // If this is an object with error, it is an error response.
                  // Get the error callback.
                  var error_cb_1 = this._ws_callbacks[response.id].error_cb;
                  var orig_req_1 = this._ws_callbacks[response.id].request;
                  // if this is an auth request, send the credentials and resend the failed request
                  if (!this.authing &&
                      response.error.code == -32000 &&
                      this.options.login &&
                      this.options.password) {
                      this.last_response = response;
                      this.authing = true;
                      this.call('login', {
                          login: this.options.login,
                          passwd: this.options.password,
                          loginParams: this.options.loginParams,
                          userVariables: this.options.userVariables,
                      }, this._ws_callbacks[response.id].request_obj.method == 'login'
                          ? function (e) {
                              _this.authing = false;
                              // console.log('logged in');
                              delete _this._ws_callbacks[response.id];
                              if (_this.options.onWSLogin) {
                                  _this.options.onWSLogin(_this, true);
                              }
                          }
                          : function (e) {
                              _this.authing = false;
                              if (_this._ws_socket) {
                                  _this._ws_socket.send(orig_req_1);
                              }
                              if (_this.options.onWSLogin) {
                                  _this.options.onWSLogin(_this, true);
                              }
                          }, function (e) {
                          delete _this._ws_callbacks[response.id];
                          error_cb_1(response.error, _this);
                          if (_this.options.onWSLogin) {
                              _this.options.onWSLogin(_this, false);
                          }
                      });
                      return;
                  }
                  // Delete the callback from the storage.
                  delete this._ws_callbacks[response.id];
                  // Run callback with the error object as parameter.
                  error_cb_1(response.error, this);
                  return;
              }
          }
          // This is not a JSON-RPC response.  Call the fallback message handler, if given.
          if (typeof this.options.onmessage === 'function') {
              event.eventData = response;
              if (!event.eventData) {
                  event.eventData = {};
              }
              var reply = this.options.onmessage(event);
              if (reply && typeof reply === 'object' && event.eventData.id) {
                  var msg = {
                      jsonrpc: '2.0',
                      id: event.eventData.id,
                      result: reply,
                  };
                  if (this._ws_socket !== null) {
                      this._ws_socket.send(JSON.stringify(msg));
                  }
              }
          }
      };
      Verto.prototype.handleMessage = function (data) {
          var _this = this;
          if (!(data && data.method)) {
              console.error('Invalid Data', data);
              return;
          }
          if (data.params.callID) {
              var dialog = this.dialogs[data.params.callID];
              if (data.method === this.module + ".attach" && dialog) {
                  delete dialog.verto.dialogs[dialog.callID];
                  dialog.rtc.stop();
                  dialog = null;
              }
              if (dialog) {
                  switch (data.method) {
                      case this.module + ".bye":
                          console.log(data.params);
                          dialog.hangup(data.params);
                          break;
                      case this.module + ".answer":
                          dialog.handleAnswer(data.params);
                          break;
                      case this.module + ".media":
                          dialog.handleMedia(data.params);
                          break;
                      case this.module + ".display":
                          dialog.handleDisplay(data.params);
                          break;
                      case this.module + ".info":
                          dialog.handleInfo(data.params);
                          break;
                      default:
                          console.debug('INVALID METHOD OR NON-EXISTANT CALL REFERENCE IGNORED', dialog, data.method);
                          break;
                  }
              }
              else {
                  switch (data.method) {
                      case this.module + ".attach":
                          data.params.attach = true;
                          if (data.params.sdp && data.params.sdp.indexOf('m=video') > 0) {
                              data.params.useVideo = true;
                          }
                          if (data.params.sdp && data.params.sdp.indexOf('stereo=1') > 0) {
                              data.params.useStereo = true;
                          }
                          dialog = new VertoDialog(Enum.direction.inbound, this, data.params);
                          dialog.setState(Enum.state.recovering);
                          break;
                      case this.module + ".invite":
                          if (data.params.sdp && data.params.sdp.indexOf('m=video') > 0) {
                              data.params.wantVideo = true;
                          }
                          if (data.params.sdp && data.params.sdp.indexOf('stereo=1') > 0) {
                              data.params.useStereo = true;
                          }
                          dialog = new VertoDialog(Enum.direction.inbound, this, data.params);
                          break;
                      default:
                          console.debug('INVALID METHOD OR NON-EXISTANT CALL REFERENCE IGNORED');
                          break;
                  }
              }
              return {
                  method: data.method,
              };
          }
          else {
              switch (data.method) {
                  case this.module + ".punt":
                      this.purge();
                      this.logout();
                      break;
                  case this.module + ".event":
                      var list = null;
                      var key_1 = null;
                      if (data.params) {
                          key_1 = data.params.eventChannel;
                      }
                      if (key_1) {
                          list = this.eventSUBS[key_1];
                          if (!list) {
                              list = this.eventSUBS[key_1.split('.')[0]];
                          }
                      }
                      if (!list && key_1 && key_1 === this.sessid) {
                          if (this.callbacks.onMessage) {
                              this.callbacks.onMessage(this, null, Enum.message.pvtEvent, data.params);
                          }
                      }
                      else if (!list && key_1 && this.dialogs[key_1]) {
                          this.dialogs[key_1].sendMessage(Enum.message.pvtEvent, data.params);
                      }
                      else if (!list) {
                          if (!key_1) {
                              key_1 = 'UNDEFINED';
                          }
                          console.error('UNSUBBED or invalid EVENT ' + key_1 + ' IGNORED');
                      }
                      else {
                          list.forEach(function (sub) {
                              if (!sub || !sub.ready) {
                                  console.error('invalid EVENT for ' + key_1 + ' IGNORED');
                              }
                              else if (sub.handler) {
                                  sub.handler(_this, data.params, sub.userData);
                              }
                              else if (_this.callbacks.onEvent) {
                                  _this.callbacks.onEvent(_this, data.params, sub.userData);
                              }
                              else {
                                  console.log('EVENT:', data.params);
                              }
                          });
                      }
                      break;
                  case this.module + ".info":
                      if (this.callbacks.onMessage) {
                          this.callbacks.onMessage(this, null, Enum.message.info, data.params.msg);
                      }
                      break;
                  case this.module + ".clientReady":
                      if (this.callbacks.onMessage) {
                          this.callbacks.onMessage(this, null, Enum.message.clientReady, data.params);
                          console.debug('CLIENT READY', data.params);
                      }
                      break;
                  default:
                      console.error('INVALID METHOD OR NON-EXISTANT CALL REFERENCE IGNORED', data.method);
                      break;
              }
          }
      };
      Verto.prototype.processReply = function (method, success, e) {
          var _this = this;
          console.log('Response: ' + method, success, e);
          switch (method) {
              case this.module + ".subscribe":
                  Object.keys(e.unauthorizedChannels || {}).forEach(function (channel) {
                      console.error('drop unauthorized channel: ' + channel);
                      delete _this.eventSUBS[channel];
                  });
                  Object.keys(e.subscribedChannels || {}).forEach(function (channel) {
                      _this.eventSUBS[channel].forEach(function (sub) {
                          sub.ready = true;
                          if (sub.readyHandler) {
                              sub.readyHandler(_this, channel);
                          }
                      });
                  });
                  break;
              case this.module + ".unsubscribe":
                  console.error(e);
                  break;
          }
      };
      Verto.prototype.sendMethod = function (method, params, success_cb, error_cb) {
          var _this = this;
          this.call(method, params, function (e) {
              /* Success */
              _this.processReply(method, true, e);
              if (success_cb)
                  success_cb(e);
          }, function (e) {
              /* Error */
              console.log('sendMethod ERR', e);
              if (error_cb)
                  error_cb(e);
              _this.processReply(method, false, e);
          });
      };
      Verto.prototype.broadcast = function (channel, params) {
          var msg = {
              eventChannel: channel,
              data: __assign({}, params),
          };
          this.sendMethod(this.module + ".broadcast", msg);
      };
      Verto.prototype.fsAPI = function (cmd, arg, success_cb, failed_cb) {
          this.sendMethod('jsapi', {
              command: 'fsapi',
              data: {
                  cmd: cmd,
                  arg: arg,
              },
          }, success_cb, failed_cb);
      };
      Verto.prototype.fsStatus = function (success_cb, failed_cb) {
          this.sendMethod('jsapi', {
              command: 'fsapi',
              data: {
                  cmd: 'status',
              },
          }, success_cb, failed_cb);
      };
      Verto.prototype.showFSAPI = function (what, success_cb, failed_cb) {
          this.sendMethod('jsapi', {
              command: 'fsapi',
              data: {
                  cmd: 'show',
                  arg: what + ' as json',
              },
          }, success_cb, failed_cb);
      };
      Verto.prototype.do_subscribe = function (verto, channel, subChannels, sparams) {
          var params = sparams || {};
          var local = params.local;
          var obj = {
              eventChannel: channel,
              userData: params.userData,
              handler: params.handler,
              ready: false,
              local: false,
              readyHandler: params.readyHandler,
              serno: this.SERNO++,
          };
          var isnew = false;
          if (!verto.eventSUBS[channel]) {
              verto.eventSUBS[channel] = [];
              subChannels.push(channel);
              isnew = true;
          }
          verto.eventSUBS[channel].push(obj);
          if (local) {
              obj.ready = true;
              obj.local = true;
          }
          if (!isnew && verto.eventSUBS[channel][0].ready) {
              obj.ready = true;
              if (obj.readyHandler) {
                  obj.readyHandler(verto, channel);
              }
          }
          return {
              serno: obj.serno,
              eventChannel: channel,
          };
      };
      Verto.prototype.subscribe = function (channel, sparams) {
          var _this = this;
          var r = [];
          var subChannels = [];
          var params = sparams || {};
          if (typeof channel === 'string') {
              r.push(this.do_subscribe(this, channel, subChannels, params));
          }
          else {
              Object.keys(channel || {}).forEach(function (c) {
                  r.push(_this.do_subscribe(_this, channel, subChannels, params));
              });
          }
          if (subChannels.length) {
              this.sendMethod(this.module + ".subscribe", {
                  eventChannel: subChannels.length == 1 ? subChannels[0] : subChannels,
                  subParams: params.subParams,
              });
          }
          return r;
      };
      Verto.prototype.unsubscribe = function (handle) {
          var _this = this;
          if (!handle) {
              Object.keys(this.eventSUBS).forEach(function (event) {
                  _this.unsubscribe(_this.eventSUBS[event]);
              });
          }
          else {
              var unsubChannels_1 = {};
              var sendChannels = [];
              if (typeof handle == 'string') {
                  delete this.eventSUBS[handle];
                  unsubChannels_1[handle]++;
              }
              else {
                  Object.keys(handle).forEach(function (channel) {
                      if (typeof channel == 'string') {
                          delete _this.eventSUBS[channel];
                          unsubChannels_1[channel]++;
                      }
                      else {
                          var eventChannel = handle[channel].eventChannel;
                          if (_this.eventSUBS[eventChannel]) {
                              _this.eventSUBS[eventChannel] = _this.eventSUBS[eventChannel].reduce(function (acc, ec) {
                                  if (ec.serno != handle[channel].serno) {
                                      acc.push(ec);
                                  }
                                  return acc;
                              }, []);
                              if (_this.eventSUBS[eventChannel].length === 0) {
                                  delete _this.eventSUBS[eventChannel];
                                  unsubChannels_1[eventChannel]++;
                              }
                          }
                      }
                  });
              }
              sendChannels = Object.keys(unsubChannels_1).map(function (i) {
                  // Sending Unsubscribe
                  return i;
              });
              if (sendChannels.length) {
                  this.sendMethod(this.module + ".unsubscribe", {
                      eventChannel: sendChannels.length == 1 ? sendChannels[0] : sendChannels,
                  });
              }
          }
      };
      Verto.prototype.newCall = function (args, callbacks) {
          if (!this.socketReady()) {
              console.error('Not Connected...');
              return;
          }
          var dialog = new VertoDialog(Enum.direction.outbound, this, args);
          dialog.invite();
          if (callbacks) {
              dialog.callbacks = callbacks;
          }
          return dialog;
      };
      Verto.prototype.logout = function (msg) {
          // verto logout
          this.closeSocket();
          if (this.callbacks.onWSClose) {
              this.callbacks.onWSClose(this, false);
          }
          this.purge();
      };
      Verto.prototype.closeSocket = function () {
          if (this.socketReady()) {
              this._ws_socket.onclose = function (w) {
                  // console.log('Closing Socket');
              };
              this._ws_socket.close();
          }
      };
      Verto.prototype.speedTest = function (bytes, cb) {
          var socket = this._ws_socket;
          if (socket) {
              this.speedCB = cb;
              this.speedBytes = bytes;
              socket.send('#SPU ' + bytes);
              var loops = bytes / 1024;
              var rem = bytes % 1024;
              var data = new Array(1024).join('.');
              for (var i = 0; i < loops; i++) {
                  socket.send('#SPB ' + data);
              }
              if (rem) {
                  socket.send('#SPB ' + data);
              }
              socket.send('#SPE');
          }
      };
      Verto.prototype.deviceParams = function (obj) {
          if (obj === void 0) { obj = {}; }
          this.options.deviceParams = __assign(__assign({}, this.options.deviceParams), obj);
          if (obj.useCamera) {
              VertoRTC.getValidRes(this.options.deviceParams.useCamera, obj ? obj.onResCheck : undefined);
          }
      };
      Verto.prototype.videoParams = function (obj) {
          if (obj === void 0) { obj = {}; }
          this.options.videoParams = __assign(__assign({}, this.options.videoParams), obj);
      };
      Verto.prototype.iceServers = function (iceServers) {
          this.options.iceServers = iceServers;
      };
      Verto.prototype.loginData = function (params) {
          this.options.login = params.login;
          this.options.password = params.password;
          this.options.loginParams = params.loginParams;
          this.options.userVariables = params.userVariables;
      };
      Verto.prototype.login = function (msg) {
          this.call('login', {});
      };
      Verto.prototype.hangup = function (callID) {
          var _this = this;
          if (callID) {
              var dialog = this.dialogs[callID];
              if (dialog) {
                  dialog.hangup();
              }
          }
          else {
              Object.keys(this.dialogs).forEach(function (dialogKey) {
                  _this.dialogs[dialogKey].hangup();
              });
          }
      };
      return Verto;
  }());
  Verto.videoDevices = [];
  Verto.audioInDevices = [];
  Verto.audioOutDevices = [];
  Verto.unloadJobs = [];
  Verto.checkDevices = function (runtime) {
      var aud_in = [];
      var aud_out = [];
      var vid = [];
      var gotDevices = function (deviceInfos) {
          // Handles being called several times to update labels. Preserve values.
          deviceInfos.forEach(function (deviceInfo) {
              var text = '';
              if (deviceInfo.kind === 'audioinput') {
                  text = deviceInfo.label || 'microphone ' + (aud_in.length + 1);
                  aud_in.push({
                      id: deviceInfo.deviceId,
                      kind: 'audio_in',
                      label: text,
                  });
              }
              else if (deviceInfo.kind === 'audiooutput') {
                  text = deviceInfo.label || 'speaker ' + (aud_out.length + 1);
                  aud_out.push({
                      id: deviceInfo.deviceId,
                      kind: 'audio_out',
                      label: text,
                  });
              }
              else if (deviceInfo.kind === 'videoinput') {
                  text = deviceInfo.label || 'camera ' + (vid.length + 1);
                  vid.push({
                      id: deviceInfo.deviceId,
                      kind: 'video',
                      label: text,
                  });
              }
          });
          Verto.videoDevices = vid;
          Verto.audioInDevices = aud_in;
          Verto.audioOutDevices = aud_out;
          if (runtime) {
              runtime(true);
          }
      };
      var handleError = function (error) {
          console.log('device enumeration error: ', error);
          if (runtime)
              runtime(false);
      };
      navigator.mediaDevices
          .enumerateDevices()
          .then(gotDevices)
          .catch(handleError);
  };
  Verto.refreshDevices = Verto.checkDevices;
  Verto.checkPerms = VertoRTC.checkPerms;
  Verto.init = function (options, runtime) {
      if (!options) {
          options = {};
      }
      if (options.skipPermCheck && !options.skipDeviceCheck) {
          Verto.checkDevices(runtime);
      }
      else if (!options.skipPermCheck && options.skipDeviceCheck) {
          VertoRTC.checkPerms(function (status) { return runtime(status); }, true, options.useCamera);
      }
      else {
          runtime(null);
      }
  };
  Verto.LiveArray = VertoHashArray;
  Verto.Conf = VertoConfMan;
  Verto.Dialog = VertoDialog;

  // Copyright Joyent, Inc. and other Node contributors.

  var R = typeof Reflect === 'object' ? Reflect : null;
  var ReflectApply = R && typeof R.apply === 'function'
    ? R.apply
    : function ReflectApply(target, receiver, args) {
      return Function.prototype.apply.call(target, receiver, args);
    };

  var ReflectOwnKeys;
  if (R && typeof R.ownKeys === 'function') {
    ReflectOwnKeys = R.ownKeys;
  } else if (Object.getOwnPropertySymbols) {
    ReflectOwnKeys = function ReflectOwnKeys(target) {
      return Object.getOwnPropertyNames(target)
        .concat(Object.getOwnPropertySymbols(target));
    };
  } else {
    ReflectOwnKeys = function ReflectOwnKeys(target) {
      return Object.getOwnPropertyNames(target);
    };
  }

  function ProcessEmitWarning(warning) {
    if (console && console.warn) console.warn(warning);
  }

  var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
    return value !== value;
  };

  function EventEmitter() {
    EventEmitter.init.call(this);
  }
  var events = EventEmitter;

  // Backwards-compat with node 0.10.x
  EventEmitter.EventEmitter = EventEmitter;

  EventEmitter.prototype._events = undefined;
  EventEmitter.prototype._eventsCount = 0;
  EventEmitter.prototype._maxListeners = undefined;

  // By default EventEmitters will print a warning if more than 10 listeners are
  // added to it. This is a useful default which helps finding memory leaks.
  var defaultMaxListeners = 10;

  Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
    enumerable: true,
    get: function() {
      return defaultMaxListeners;
    },
    set: function(arg) {
      if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
        throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
      }
      defaultMaxListeners = arg;
    }
  });

  EventEmitter.init = function() {

    if (this._events === undefined ||
        this._events === Object.getPrototypeOf(this)._events) {
      this._events = Object.create(null);
      this._eventsCount = 0;
    }

    this._maxListeners = this._maxListeners || undefined;
  };

  // Obviously not all Emitters should be limited to 10. This function allows
  // that to be increased. Set to zero for unlimited.
  EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
    if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
      throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
    }
    this._maxListeners = n;
    return this;
  };

  function $getMaxListeners(that) {
    if (that._maxListeners === undefined)
      return EventEmitter.defaultMaxListeners;
    return that._maxListeners;
  }

  EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
    return $getMaxListeners(this);
  };

  EventEmitter.prototype.emit = function emit(type) {
    var args = [];
    for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
    var doError = (type === 'error');

    var events = this._events;
    if (events !== undefined)
      doError = (doError && events.error === undefined);
    else if (!doError)
      return false;

    // If there is no 'error' event listener then throw.
    if (doError) {
      var er;
      if (args.length > 0)
        er = args[0];
      if (er instanceof Error) {
        // Note: The comments on the `throw` lines are intentional, they show
        // up in Node's output if this results in an unhandled exception.
        throw er; // Unhandled 'error' event
      }
      // At least give some kind of context to the user
      var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
      err.context = er;
      throw err; // Unhandled 'error' event
    }

    var handler = events[type];

    if (handler === undefined)
      return false;

    if (typeof handler === 'function') {
      ReflectApply(handler, this, args);
    } else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i)
        ReflectApply(listeners[i], this, args);
    }

    return true;
  };

  function _addListener(target, type, listener, prepend) {
    var m;
    var events;
    var existing;

    if (typeof listener !== 'function') {
      throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
    }

    events = target._events;
    if (events === undefined) {
      events = target._events = Object.create(null);
      target._eventsCount = 0;
    } else {
      // To avoid recursion in the case that type === "newListener"! Before
      // adding it to the listeners, first emit "newListener".
      if (events.newListener !== undefined) {
        target.emit('newListener', type,
                    listener.listener ? listener.listener : listener);

        // Re-assign `events` because a newListener handler could have caused the
        // this._events to be assigned to a new object
        events = target._events;
      }
      existing = events[type];
    }

    if (existing === undefined) {
      // Optimize the case of one listener. Don't need the extra array object.
      existing = events[type] = listener;
      ++target._eventsCount;
    } else {
      if (typeof existing === 'function') {
        // Adding the second element, need to change to array.
        existing = events[type] =
          prepend ? [listener, existing] : [existing, listener];
        // If we've already got an array, just append.
      } else if (prepend) {
        existing.unshift(listener);
      } else {
        existing.push(listener);
      }

      // Check for listener leak
      m = $getMaxListeners(target);
      if (m > 0 && existing.length > m && !existing.warned) {
        existing.warned = true;
        // No error code for this since it is a Warning
        // eslint-disable-next-line no-restricted-syntax
        var w = new Error('Possible EventEmitter memory leak detected. ' +
                            existing.length + ' ' + String(type) + ' listeners ' +
                            'added. Use emitter.setMaxListeners() to ' +
                            'increase limit');
        w.name = 'MaxListenersExceededWarning';
        w.emitter = target;
        w.type = type;
        w.count = existing.length;
        ProcessEmitWarning(w);
      }
    }

    return target;
  }

  EventEmitter.prototype.addListener = function addListener(type, listener) {
    return _addListener(this, type, listener, false);
  };

  EventEmitter.prototype.on = EventEmitter.prototype.addListener;

  EventEmitter.prototype.prependListener =
      function prependListener(type, listener) {
        return _addListener(this, type, listener, true);
      };

  function onceWrapper() {
    var args = [];
    for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);
    if (!this.fired) {
      this.target.removeListener(this.type, this.wrapFn);
      this.fired = true;
      ReflectApply(this.listener, this.target, args);
    }
  }

  function _onceWrap(target, type, listener) {
    var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
    var wrapped = onceWrapper.bind(state);
    wrapped.listener = listener;
    state.wrapFn = wrapped;
    return wrapped;
  }

  EventEmitter.prototype.once = function once(type, listener) {
    if (typeof listener !== 'function') {
      throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
    }
    this.on(type, _onceWrap(this, type, listener));
    return this;
  };

  EventEmitter.prototype.prependOnceListener =
      function prependOnceListener(type, listener) {
        if (typeof listener !== 'function') {
          throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
        }
        this.prependListener(type, _onceWrap(this, type, listener));
        return this;
      };

  // Emits a 'removeListener' event if and only if the listener was removed.
  EventEmitter.prototype.removeListener =
      function removeListener(type, listener) {
        var list, events, position, i, originalListener;

        if (typeof listener !== 'function') {
          throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
        }

        events = this._events;
        if (events === undefined)
          return this;

        list = events[type];
        if (list === undefined)
          return this;

        if (list === listener || list.listener === listener) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else {
            delete events[type];
            if (events.removeListener)
              this.emit('removeListener', type, list.listener || listener);
          }
        } else if (typeof list !== 'function') {
          position = -1;

          for (i = list.length - 1; i >= 0; i--) {
            if (list[i] === listener || list[i].listener === listener) {
              originalListener = list[i].listener;
              position = i;
              break;
            }
          }

          if (position < 0)
            return this;

          if (position === 0)
            list.shift();
          else {
            spliceOne(list, position);
          }

          if (list.length === 1)
            events[type] = list[0];

          if (events.removeListener !== undefined)
            this.emit('removeListener', type, originalListener || listener);
        }

        return this;
      };

  EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

  EventEmitter.prototype.removeAllListeners =
      function removeAllListeners(type) {
        var listeners, events, i;

        events = this._events;
        if (events === undefined)
          return this;

        // not listening for removeListener, no need to emit
        if (events.removeListener === undefined) {
          if (arguments.length === 0) {
            this._events = Object.create(null);
            this._eventsCount = 0;
          } else if (events[type] !== undefined) {
            if (--this._eventsCount === 0)
              this._events = Object.create(null);
            else
              delete events[type];
          }
          return this;
        }

        // emit removeListener for all listeners on all events
        if (arguments.length === 0) {
          var keys = Object.keys(events);
          var key;
          for (i = 0; i < keys.length; ++i) {
            key = keys[i];
            if (key === 'removeListener') continue;
            this.removeAllListeners(key);
          }
          this.removeAllListeners('removeListener');
          this._events = Object.create(null);
          this._eventsCount = 0;
          return this;
        }

        listeners = events[type];

        if (typeof listeners === 'function') {
          this.removeListener(type, listeners);
        } else if (listeners !== undefined) {
          // LIFO order
          for (i = listeners.length - 1; i >= 0; i--) {
            this.removeListener(type, listeners[i]);
          }
        }

        return this;
      };

  function _listeners(target, type, unwrap) {
    var events = target._events;

    if (events === undefined)
      return [];

    var evlistener = events[type];
    if (evlistener === undefined)
      return [];

    if (typeof evlistener === 'function')
      return unwrap ? [evlistener.listener || evlistener] : [evlistener];

    return unwrap ?
      unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
  }

  EventEmitter.prototype.listeners = function listeners(type) {
    return _listeners(this, type, true);
  };

  EventEmitter.prototype.rawListeners = function rawListeners(type) {
    return _listeners(this, type, false);
  };

  EventEmitter.listenerCount = function(emitter, type) {
    if (typeof emitter.listenerCount === 'function') {
      return emitter.listenerCount(type);
    } else {
      return listenerCount.call(emitter, type);
    }
  };

  EventEmitter.prototype.listenerCount = listenerCount;
  function listenerCount(type) {
    var events = this._events;

    if (events !== undefined) {
      var evlistener = events[type];

      if (typeof evlistener === 'function') {
        return 1;
      } else if (evlistener !== undefined) {
        return evlistener.length;
      }
    }

    return 0;
  }

  EventEmitter.prototype.eventNames = function eventNames() {
    return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
  };

  function arrayClone(arr, n) {
    var copy = new Array(n);
    for (var i = 0; i < n; ++i)
      copy[i] = arr[i];
    return copy;
  }

  function spliceOne(list, index) {
    for (; index + 1 < list.length; index++)
      list[index] = list[index + 1];
    list.pop();
  }

  function unwrapListeners(arr) {
    var ret = new Array(arr.length);
    for (var i = 0; i < ret.length; ++i) {
      ret[i] = arr[i].listener || arr[i];
    }
    return ret;
  }

  var STUN_SERVER = { urls: 'stun:stun.telnyx.com:3843' };
  var TURN_SERVER = {
      urls: 'turn:turn.telnyx.com:3478?transport=tcp',
      username: 'turnuser',
      credential: 'turnpassword',
  };
  var REGISTRAR_SERVER = 'sip:sip.telnyx.com:7443';
  var getElement = function (element) {
      if (element instanceof HTMLMediaElement) {
          return element;
      }
      else if (typeof element === 'string') {
          return document.querySelector(element);
      }
      else if (element instanceof Function) {
          return element();
      }
  };
  var BaseClient = /** @class */ (function () {
      function BaseClient(o) {
          this.turnServer = TURN_SERVER;
          this.stunServer = STUN_SERVER;
          this.registrarServer = REGISTRAR_SERVER;
          this.eventBus = new events();
          Object.assign(this, __assign({ env: 'production', useCamera: false, useSpeaker: true, useMic: true }, o));
      }
      Object.defineProperty(BaseClient.prototype, "localElement", {
          get: function () {
              return this._localElement;
          },
          set: function (el) {
              this._localElement = el && getElement(el);
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(BaseClient.prototype, "remoteElement", {
          get: function () {
              return this._remoteElement;
          },
          set: function (el) {
              this._remoteElement = el && getElement(el);
          },
          enumerable: true,
          configurable: true
      });
      BaseClient.prototype.on = function (message, cb) {
          this.eventBus.on(message, cb);
          return this;
      };
      return BaseClient;
  }());

  /**
   * @hidden
   */
  var TelnyxRTCCall = /** @class */ (function () {
      function TelnyxRTCCall(call) {
          this.call = call;
      }
      Object.defineProperty(TelnyxRTCCall.prototype, "state", {
          get: function () {
              switch (this.call.state) {
                  case TelnyxRTCCallState.requesting:
                  case TelnyxRTCCallState.recovering:
                  case TelnyxRTCCallState.trying:
                  case TelnyxRTCCallState.early:
                      return 'connecting';
                  case TelnyxRTCCallState.active:
                      return 'active';
                  case TelnyxRTCCallState.held:
                      return 'held';
                  case TelnyxRTCCallState.hangup:
                  case TelnyxRTCCallState.destroy:
                      return 'done';
                  case TelnyxRTCCallState.answering:
                      return 'ringing';
                  default:
                      return 'new';
              }
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(TelnyxRTCCall.prototype, "isMuted", {
          get: function () {
              return this.call.getMute();
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(TelnyxRTCCall.prototype, "isHeld", {
          get: function () {
              return this.state === 'held';
          },
          enumerable: true,
          configurable: true
      });
      TelnyxRTCCall.prototype.hangup = function () {
          this.call.hangup();
      };
      TelnyxRTCCall.prototype.answer = function () {
          this.call.answer();
      };
      TelnyxRTCCall.prototype.reject = function () {
          this.call.hangup();
      };
      TelnyxRTCCall.prototype.hold = function () {
          this.call.hold();
      };
      TelnyxRTCCall.prototype.unhold = function () {
          this.call.unhold();
      };
      TelnyxRTCCall.prototype.mute = function () {
          this.call.mute();
      };
      TelnyxRTCCall.prototype.unmute = function () {
          this.call.unmute();
      };
      TelnyxRTCCall.prototype.dtmf = function (input) {
          this.call.dtmf(input);
      };
      TelnyxRTCCall.prototype.transfer = function (input, params) {
          this.call.transfer(input);
      };
      TelnyxRTCCall.prototype.setAudioOutDevice = function (sinkId, callback) {
          return this.call.setAudioOutDevice(sinkId, callback);
      };
      return TelnyxRTCCall;
  }());

  var MODULE = 'telnyx_rtc';
  var HOST = 'rtc.telnyx.com';
  var HOST_DEV = 'rtcdev.telnyx.com';
  var TelnyxRTC_PORT = 14938;
  var TelnyxRTCClient = /** @class */ (function (_super) {
      __extends(TelnyxRTCClient, _super);
      function TelnyxRTCClient(o) {
          var _this = _super.call(this, o) || this;
          _this.module = _this.module || MODULE;
          _this.port = _this.port || TelnyxRTC_PORT;
          _this.host = _this.host || (_this.env === 'development' ? HOST_DEV : HOST);
          if (!checkAllowedModules(_this.module)) {
              throw new Error("Module " + _this.module + " is not supported");
          }
          return _this;
      }
      TelnyxRTCClient.prototype.connect = function () {
          return __awaiter(this, void 0, void 0, function () {
              var callbacks;
              var _this = this;
              return __generator(this, function (_a) {
                  switch (_a.label) {
                      case 0: return [4 /*yield*/, this.checkPermissions()];
                      case 1:
                          _a.sent();
                          return [4 /*yield*/, this.checkDevices()];
                      case 2:
                          _a.sent();
                          callbacks = {
                              onWSConnect: function () {
                                  _this.eventBus.emit('socket.connect');
                              },
                              onWSLogin: function () {
                                  _this.eventBus.emit('ready');
                                  _this.eventBus.emit('registered');
                              },
                              onWSClose: function () {
                                  _this.eventBus.emit('unregistered');
                                  _this.eventBus.emit('socket.close');
                              },
                              onDialogState: function (d) {
                                  _this.eventBus.emit('callUpdate', new TelnyxRTCCall(d));
                              },
                          };
                          this.telnyxRTC = new Verto({
                              login: this.credentials.username + "@" + this.host,
                              password: this.credentials.token || this.credentials.password,
                              socketUrl: "wss://" + this.host + ":" + this.port,
                              module: this.module,
                              // ringFile: 'bell_ring2.wav',
                              iceServers: [this.stunServer, this.turnServer],
                              deviceParams: {
                                  useMic: getDeviceString(this.useMic),
                                  useSpeaker: getDeviceString(this.useSpeaker),
                                  useCamera: getDeviceString(this.useCamera),
                              },
                              tag: function () { return _this.remoteElement; },
                          }, callbacks);
                          return [2 /*return*/];
                  }
              });
          });
      };
      TelnyxRTCClient.prototype.newCall = function (options) {
          if (!options.destination) {
              throw new TypeError('destination is required');
          }
          var call = this.telnyxRTC.newCall({
              destination_number: options.destination,
              caller_id_name: options.callerName || 'Telnyx',
              caller_id_number: options.callerNumber || this.credentials.username,
              // callee_id_number: options.remoteCallerNumber || options.destination,
              // callee_id_name: options.remoteCallerName || 'Outbound Call',
              outgoingBandwidth: 'default',
              incomingBandwidth: 'default',
              useStereo: true,
              useVideo: this.useCamera,
          });
          return new TelnyxRTCCall(call);
      };
      TelnyxRTCClient.prototype.disconnect = function () {
          if (this.telnyxRTC) {
              this.telnyxRTC.logout();
              this.telnyxRTC = null;
          }
          this.eventBus.removeAllListeners();
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.checkDevices = function () {
          return __awaiter(this, void 0, void 0, function () {
              return __generator(this, function (_a) {
                  return [2 /*return*/, Verto.checkDevices()];
              });
          });
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.checkPermissions = function () {
          return __awaiter(this, void 0, void 0, function () {
              var _this = this;
              return __generator(this, function (_a) {
                  return [2 /*return*/, new Promise(function (resolve, reject) {
                          var useAudio = true;
                          var useVideo = _this.useCamera;
                          Verto.checkPerms(function (result) { return (result ? resolve() : reject()); }, useAudio, useVideo);
                      })];
              });
          });
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.getVideoDevices = function () {
          return Verto.videoDevices;
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.getAudioInDevices = function () {
          return Verto.audioInDevices;
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.getAudioOutDevices = function () {
          return Verto.audioOutDevices;
      };
      /**
       * @hidden
       */
      TelnyxRTCClient.prototype.getDevices = function () {
          return __spreadArrays(this.getAudioInDevices(), this.getAudioOutDevices(), this.getVideoDevices());
      };
      return TelnyxRTCClient;
  }(BaseClient));

  var TelnyxRTC = /** @class */ (function (_super) {
      __extends(TelnyxRTC, _super);
      function TelnyxRTC() {
          return _super !== null && _super.apply(this, arguments) || this;
      }
      return TelnyxRTC;
  }(TelnyxRTCClient));

  exports.TelnyxRTC = TelnyxRTC;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
