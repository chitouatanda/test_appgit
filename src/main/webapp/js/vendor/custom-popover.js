var customPopover = angular.module("custom.popover", ["ui.bootstrap"]).run(["$templateCache", function($templateCache) {
	  $templateCache.put("template/popover/popover-template.html",
	    "<div class=\"popover {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
	    "  <div class=\"arrow\"></div>\n" +
	    "\n" +
	    "  <div class=\"popover-inner\">\n" +
	    "      <h3 class=\"popover-title\" ng-bind=\"title\" ng-show=\"title\"></h3>\n" +
	    "      <div class=\"popover-content\"></div>\n" +
	    "  </div>\n" +
	    "</div>\n" +
	    "");
	}]);

customPopover.directive( 'popoverTemplatePopup', [ '$http', '$templateCache', '$compile', '$timeout', function ( $http, $templateCache, $compile, $timeout ) {
	  return {
	    restrict: 'EA',
	    replace: true,
	    scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&', manualHide: '&', compileScope: '&' },
	    templateUrl: 'template/popover/popover-template.html',
	    link: function(scope, iElement) {
	      scope.$watch('content', function( templateUrl) {
	        if (!templateUrl) {
	        	return; 
	        }
	        $http.get(templateUrl, {cache: $templateCache})
	        	.then(function(response) {
	          var contentEl = angular.element(iElement[0].querySelector('.popover-content'));
	          contentEl.children().remove();
	          contentEl.append($compile(response.data.trim())(scope.$parent.$parent.$parent));
	          $timeout(function(){ scope.$digest(); });
	        });
	      });
	    }
	  };
	}])

customPopover.directive( 'popoverTemplate', [ '$tooltip', function ( $tooltip ) {
	  return $tooltip( 'popoverTemplate', 'popover', 'click' );
	}]);

