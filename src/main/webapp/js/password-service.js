define(['angular', 'angular-resource'], function(angular) {
	angular.module('password-service', ['ngResource'])
	.factory("PasswordService", function ($resource) {
			return $resource("", {}, {
				lockUserAfterFiveAttempts: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/lockuser/:username"
				},
				resetUserAfterFiveAttempts: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/resetuser/:username"
				},
				getUser: {
					method : 'GET',
					isArray : false,
					url: "/spr/rest/passwordattempt/getuserfromdb/:username"
				}
			});
		});
});




