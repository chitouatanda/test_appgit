define(
		[ 'angular', 'angular-ui-select','secondaryqa/secondaryqa-service', 'teamnotice-service', 'angular-localStorage', 'user-service','xtaas-audio-player', 'angular-timeZone', 'angular-x-editable'],
		function(angular) {
			angular
					.module(
							'SecondaryQaDetailController',
							[ 'ui.select','secondaryqa-service', 'teamnotice-service', 'ngStorage', 'user-service','xtaasaudioplayer', 'xeditable'])
					.controller(
							'SecondaryQaDetailController',
							[
									'$scope',
									'$sce',
									'$stateParams',
									'$modal',
									'$state',
									'$timeout',
									'$filter',
									'qADetailService',
									'qadetail',
									'qADetailFeedbackService',
									'qADetailSaveFeedbackService',
									'qADetailSubmitFeedbackService',
									'returnProspectToPrimaryService',
									'$localStorage',
									'UserServiceById',
									'$rootScope',
									'qaRejectReasons',
									'attributeMappings',
									'user',
									'getDisplayLabelService',
									'qaProspectListService',
									'qaUpdateProspectService',
									'NotificationService',
									'GetOrganizationService',
									'changeLeadStatusService',
									'ClientMappingService',
									function($scope, $sce, $stateParams, $modal, $state, $timeout, $filter, qADetailService, qadetail, qADetailFeedbackService, qADetailSaveFeedbackService, qADetailSubmitFeedbackService, returnProspectToPrimaryService, $localStorage, UserServiceById, $rootScope, qaRejectReasons, attributeMappings, user, getDisplayLabelService, qaProspectListService, qaUpdateProspectService,NotificationService,GetOrganizationService, changeLeadStatusService, ClientMappingService) {
										// $scope.cfAnswers = [];
										// $scope.cfAnswers[0] = {"question": "CUSTOMFIELD_01sec", "answer": "1.0"};
										// $scope.cfAnswers[1] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[2] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[3] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[4] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[5] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[6] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[7] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[8] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[9] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[10] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[11] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[12] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										// $scope.cfAnswers[13] = {"question": "CUSTOMFIELD_02", "answer": "2.0"};
										$scope.showComment = {};
										$scope.main = {'activeRow' : undefined};
										$scope.loading = false;
										$scope.qaDetail = qadetail;
										$scope.isNotesEdited = false;
										if ($scope.qaDetail.prospectCallLog.prospectCall.notes.length > 0) {
											$scope.agentNotes = $scope.qaDetail.prospectCallLog.prospectCall.notes[0].text;
										}
										$scope.prospectDomain = $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.domain;
										if ($scope.prospectDomain == null || $scope.prospectDomain == undefined) {
											$scope.prospectDomain = "";	
										}

										// Remove HTTP, HTTPS, WWW from url.
										if ($scope.qaDetail != null && $scope.qaDetail.prospectCallLog != null && $scope.qaDetail.prospectCallLog.prospectCall != null && $scope.qaDetail.prospectCallLog.prospectCall.prospect != null && $scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomCompanyUrl != null) {
											var cleanUrl = $scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomCompanyUrl.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "");
											$scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomCompanyUrl = cleanUrl;
										}
										if ($scope.qaDetail != null && $scope.qaDetail.prospectCallLog != null && $scope.qaDetail.prospectCallLog.prospectCall != null && $scope.qaDetail.prospectCallLog.prospectCall.prospect != null && $scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomPersonUrl != null) {
											var cleanUrl = $scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomPersonUrl.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "");
											$scope.qaDetail.prospectCallLog.prospectCall.prospect.zoomPersonUrl = cleanUrl;
										}

										// commented below line & added new line to show phone number to QA
										// $scope.secEditablePhoneText = 'XXX-XXX-XXXX';
										$scope.secEditablePhoneText = $scope.qaDetail.prospectCallLog.prospectCall.prospect.phone;
										$scope.secEditableEmailText = $scope.qaDetail.prospectCallLog.prospectCall.prospect.email;
										if($scope.qaDetail.campaign  != null && $scope.qaDetail.campaign != undefined && $scope.qaDetail.campaign.hideNonSuccessPII) {
											if($scope.qaDetail != null && $scope.qaDetail.prospectCallLog != null && $scope.qaDetail.prospectCallLog.prospectCall != null && $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus != null && !($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == 'SUCCESS')) {
												$scope.secEditablePhoneText = 'XXX-XXX-XXXX';
												$scope.secEditableEmailText = 'XXXXXXXXXX@XXX'
											}
										}
										/*  ############### Date :- 13/11/18 Get the organization of loggedInUser  Start ############### */
										$scope.loggedInUserOrganization = {};
										GetOrganizationService.query({id : user.organization}).$promise.then(function(organization) {
											$scope.loggedInUserOrganization = organization;
										});
										/*  ############### Date :- 13/11/18 Get the organization of loggedInUser  End ############### */

										/** DATE :- 03/10/2019
                 						 *  If Logged in user role is "SECONDARYQA" (Secondary) then only user will be able to access the page
                 						 *  otherwise redirected to login page. 
                 						 */
										$scope.isSecQaLoggedIn = function() {
											var role = $filter('filter')(user.roles, "SECONDARYQA")[0];
											if (role != 'SECONDARYQA') {
												$state.go('login');
												return;
											}
										}
										$scope.isSecQaLoggedIn();
										if ($scope.qaDetail.campaign!= null && $scope.qaDetail.campaign != undefined && $scope.qaDetail.campaign.callGuide != null && $scope.qaDetail.campaign.callGuide != undefined) {
											$scope.confirmSectionScript =  $scope.qaDetail.campaign.callGuide.confirmSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.confirmSection.script) : null;
											$scope.questionSectionScript = $scope.qaDetail.campaign.callGuide.questionSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.questionSection.script) : null;
											$scope.consentSectionScript = $scope.qaDetail.campaign.callGuide.consentSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.consentSection.script) : null;
											$scope.noteSectionScript = $scope.qaDetail.campaign.callGuide.noteSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.noteSection.script) : null;
										}
										// END
										/*  ############### Date :- 16/07/19 Client mappings of campaign  Start ############### */
										$scope.clientMappings = [];
										ClientMappingService.query({campaignId : $scope.qaDetail.campaign.id}).$promise.then(function(mappings) {
												$scope.clientMappings = mappings;
												if ($scope.clientMappings != null && $scope.clientMappings != undefined && $scope.clientMappings.length > 0) {
													$scope.prepareCustomQuestions();
												} else {
													$scope.generateQuestionsWithoutMappings();
												}
										});
										/*  ############### Date :- 16/07/19 Client mappings of campaign  End ############### */

										// /* ############### Date:- 20-11-18 Added selectbox while editing lead details for Department, Industry, Min employee, Max employee, Min revenue, Max revenue, Management level  START ############### */
										$scope.revenue = {};
										$scope.employeeCount = {};
										$scope.attributeMappings = angular.copy(attributeMappings);
										$scope.leadDetailsMapping = {};
										$scope.employeeCount = $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minEmployeeCount + "-" + $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxEmployeeCount;  
										
										if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minEmployeeCount == 10000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxEmployeeCount == 0) {
											$scope.employeeCount = "Over 10000";
										}

										$scope.mapRevenueAsPerUI = function() {
											if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 0 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 10000000) {
												$scope.revenue = "0-$10M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 5000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 10000000) {
												$scope.revenue = "$5M-$10M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 10000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 25000000) {
												$scope.revenue = "$10M-$25M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 25000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 50000000) {
												$scope.revenue = "$25M-$50M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 50000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 100000000) {
												$scope.revenue = "$50M-$100M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 100000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 250000000) {
												$scope.revenue = "$100M-$250M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 250000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 500000000) {
												$scope.revenue = "$250M-$500M";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 500000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 1000000000) {
												$scope.revenue = "$500M-$1B";
											}
											else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue == 1000000000 && $scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue == 5000000000) {
												$scope.revenue = "$1B-$5B";
											}
											else {
												$scope.revenue = "More than $5Billion"
											}
										 }

										$scope.mapRevenueAsPerUI();

										$scope.leadDetailsMapping.industries = $scope.attributeMappings.INDUSTRY.pickList;	   
										if ($scope.leadDetailsMapping != undefined && $scope.leadDetailsMapping.industries != null && $scope.leadDetailsMapping.industries != undefined) {
											$scope.leadDetailsMapping.industries.sort(function(obj1, obj2) {
												var x = obj1.label.toLowerCase();
												var y = obj2.label.toLowerCase();
												if (x < y) {return -1;}
												if (x > y) {return 1;}
												return 0;
										   });
										}
										$scope.showSelectedIndustry = function() {
											var selectedIndustry = $filter('filter')($scope.leadDetailsMapping.industries, {label: $scope.qaDetail.prospectCallLog.prospectCall.prospect.industry});
											if (selectedIndustry != undefined || selectedIndustry != null) {
												if (selectedIndustry.length > 0 ) {
													return selectedIndustry[0].label;
												}
											}
										};

										$scope.leadDetailsMapping.departments = $scope.attributeMappings.DEPARTMENT.pickList;	   
										if ($scope.leadDetailsMapping != undefined && $scope.leadDetailsMapping.departments != null && $scope.leadDetailsMapping.departments != undefined) {
											$scope.leadDetailsMapping.departments.sort(function(obj1, obj2) {
												var x = obj1.label.toLowerCase();
												var y = obj2.label.toLowerCase();
												if (x < y) {return -1;}
												if (x > y) {return 1;}
												return 0;
										   });
										}
										$scope.showSelectedDepartment = function() {
											var selectedDepartment = $filter('filter')($scope.leadDetailsMapping.departments, {label: $scope.qaDetail.prospectCallLog.prospectCall.prospect.department});
											if (selectedDepartment != undefined || selectedDepartment != null) {
												if (selectedDepartment.length > 0) {
													return selectedDepartment[0].label;
												}
											}
										};

										$scope.leadDetailsMapping.revenueRange = $scope.attributeMappings.REVENUE.pickList;	   
										$scope.showSelectedRevenueRange = function() {
											var selectedRevenueRange = $filter('filter')($scope.leadDetailsMapping.revenueRange, {label: $scope.revenue});
											if (selectedRevenueRange != undefined || selectedRevenueRange != null) {
												if (selectedRevenueRange.length > 0) {
													return selectedRevenueRange[0].label;
												}
											}
										};

										$scope.leadDetailsMapping.employeeRange = $scope.attributeMappings.EMPLOYEE.pickList;	   
										$scope.showSelectedEmployeeRange = function() {
											var selectedEmployeeRange = $filter('filter')($scope.leadDetailsMapping.employeeRange, {label: $scope.employeeCount});
											if (selectedEmployeeRange != undefined || selectedEmployeeRange != null) {
												if (selectedEmployeeRange.length > 0) {
													return selectedEmployeeRange[0].label;
												}
											}
										};

										$scope.leadDetailsMapping.managementLevel = []
										angular.forEach($scope.attributeMappings.MANAGEMENT_LEVEL.pickList, function(object, key) {
											var newObject = {};
											if(object.label.toLowerCase() != 'all') {
												if(object.label == 'MANAGER') {
													newObject = {
														'label':'Manager',
														'value':'Manager'
													}
												} else if(object.label == 'Non Management') {
													newObject = {
														'label':'Non-Manager',
														'value':'Non-Manager'
													}
												} else if(object.label == 'DIRECTOR') {
													newObject = {
														'label':'Director',
														'value':'Director'
													}
												} else if(object.label == 'VP_EXECUTIVES') {
													newObject = {
														'label':'VP-Level',
														'value':'VP-Level'
													}
												} else if(object.label == 'C_EXECUTIVES') {
													newObject = {
														'label':'C-Level',
														'value':'C-Level'
													}
												} else {
													newObject = {
														'label': object.label,
														'value': object.value
													}
												}
												$scope.leadDetailsMapping.managementLevel.push(newObject);
											}
										});	   
										$scope.showSelectedManagementLevel = function() {
											var selectedManagementLevel = $filter('filter')($scope.leadDetailsMapping.managementLevel, {label: $scope.qaDetail.prospectCallLog.prospectCall.prospect.managementLevel});
											if (selectedManagementLevel != undefined || selectedManagementLevel != null) {
												if (selectedManagementLevel.length > 0 ) {
													return selectedManagementLevel[0].label;
												}
											}
										};

										$scope.assignMinAndMaxEmployee = function() {
										var minEmployeeCount = "";
										var maxEmployeeCount = "";
										var isHyphenFound = false;
											for (var i = 0; i <  $scope.employeeCount.length; i++) { 
												if ($scope.employeeCount[i] == "-") {
													isHyphenFound = true;
												}
													if (!isHyphenFound) {
														minEmployeeCount += $scope.employeeCount[i];
													}
													else {
														if ($scope.employeeCount[i] != "-") {
															maxEmployeeCount += $scope.employeeCount[i];
														}
													}
											}
											if (minEmployeeCount == "Over 10000") {
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minEmployeeCount = 10000;
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxEmployeeCount = 0;
											}
											else {
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minEmployeeCount = parseInt(minEmployeeCount);
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxEmployeeCount = parseInt(maxEmployeeCount);
											}
										}

										$scope.assignMinAndMaxRevenue = function() {
											var minRevenue = "";
											var maxRevenue = "";
											var isHyphenFound = false;
												for (var i = 0; i <  $scope.revenue.length; i++) { 
													if ($scope.revenue[i] == "-") {
														isHyphenFound = true;
													}
														if (!isHyphenFound) {
															minRevenue += $scope.revenue[i];
														}
														else {
															if ($scope.revenue[i] != "-") {
																maxRevenue += $scope.revenue[i];
															}
														}
												}
												if (minRevenue == "0") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 0	
												} else if (minRevenue == "$5M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 5000000	
												} else if (minRevenue == "$10M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 10000000	
												} else if (minRevenue == "$25M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 25000000	
												} else if (minRevenue == "$50M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 50000000	
												} else if (minRevenue == "$100M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 100000000	
												} else if (minRevenue == "$250M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 250000000	
												} else if (minRevenue == "$500M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 500000000	
												} else if (minRevenue == "$1B") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.minRevenue = 1000000000	
												} 

												if (maxRevenue == "$10M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 10000000
												} else if (maxRevenue == "$25M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 25000000
												} else if (maxRevenue == "$50M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 50000000
												} else if (maxRevenue == "$100M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 100000000
												} else if (maxRevenue == "$250M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 250000000
												} else if (maxRevenue == "$500M") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 500000000
												} else if (maxRevenue == "$1B") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 1000000000
												} else if (maxRevenue == "$5B") {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 5000000000
												} else {
													$scope.qaDetail.prospectCallLog.prospectCall.prospect.customAttributes.maxRevenue = 0
												}
											}
										// /* ############### Date:- 20-11-18 Added selectbox while editing lead details for Department, Industry, Min employee, Max employee, Min revenue, Max revenue, Management level  END ############### */
										
										/* START QUESTIONNAIRE VALIDATION
										 * DATE : 16/05/2017
										 * REASON :  Below code is added to handle "Is answer valid" check-box & "answerCertification" field on QADetails page */
										//console.log($scope.qaDetail);
										if (qadetail.prospectCallLog.qaFeedback == null) {
											qadetail.prospectCallLog.qaFeedback = {};
											/*qadetail.prospectCallLog.qaFeedback.isAnswerValid = {};
											qadetail.prospectCallLog.qaFeedback.answerCertification = {};*/
										}

										$scope.generateQuestionsWithoutMappings = function() {
											if ($scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers == null || $scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers == undefined) {
												$scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers = [];
											}
											if(!$scope.qaDetail.prospectCallLog.prospectCall.answers != null && $scope.qaDetail.prospectCallLog.prospectCall.answers != undefined){
												angular.forEach($scope.qaDetail.questionAttributeMap, function(questionAtributeMapValue, questionAttributeMapKey) {
													var answer = "";
													var question = questionAtributeMapValue.questionSkin;
													angular.forEach($scope.qaDetail.prospectCallLog.prospectCall.answers, function(answersValue, answersKey) {
														if (question == answersValue.question) {
															answer = answersValue.answer;															
														}
																});
													var obj = {};
													obj.isAnswerValid = "";
													obj.answerCertification = "";
													obj.sequence = questionAtributeMapValue.sequence;
													obj.question = question;
													obj.answer = answer;
													$scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers.push(obj);
												});
											}
										}
										/* END QUESTIONNAIRE VALIDATION */
										
										$scope.totalWeight = 0;
										$scope.isReadOnlyClass = 'btn-main done pull-right';
										
										$scope.isFormComplete = false;
										
										$scope.isReadOnly = false;// variable for read-only form when prospect is in QA_COMPLETE status
										$scope.isScorePresent = false;//variable for checking form filling is initiated
										$scope.isScoreAttributeComplete = undefined;//variable for checking form is completely filled or not
										$scope.isleadValidationChecked = false; //variable for checking lead validation 

										
										$scope.qaRejectReasons = qaRejectReasons;
										if($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus) {
											var status = "";
											if ($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == "AUTO") {
												status = "DIALERCODE";
											} else {
												status = $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus;
											}
											getDisplayLabelService.query({labelentity: status + 'DISPOSITIONSTATUS',key:$scope.qaDetail.prospectCallLog.prospectCall.subStatus}).$promise.then( function(data) {
												angular.forEach(data,  function(value, key){
													if(key == $scope.qaDetail.prospectCallLog.prospectCall.subStatus) {
														$scope.subStatus = value;	
													}												
												});												 	
											});	
										}
										
										if (qadetail.callLogMap[0] != null && qadetail.callLogMap[0] != undefined ) {
											var url = "";   // If AWS recording url found in the prospect link that otherwise link twilio recording url on QA screen.  
											if (qadetail.prospectCallLog.prospectCall.recordingUrlAws != null) {
												url = qadetail.prospectCallLog.prospectCall.recordingUrlAws; 
											} else {
												url = qadetail.prospectCallLog.prospectCall.recordingUrl;
											}
											$scope.recordingURL = $sce.trustAsResourceUrl(url);
										}else{
											var url = "";   // If AWS recording url found in the prospect link that otherwise link twilio recording url on QA screen.  
											if (qadetail.prospectCallLog.prospectCall.recordingUrlAws != null) {
												url = qadetail.prospectCallLog.prospectCall.recordingUrlAws; 
											} else {
												url = qadetail.prospectCallLog.prospectCall.recordingUrl;
											}
											$scope.recordingURL = $sce.trustAsResourceUrl(url);
										}
										if(qadetail.qaFeedbackForm == null) {
											$scope.qaFeedbackForm = [];
										} else {
											$scope.qaFeedbackForm = qadetail.qaFeedbackForm.formAtrributes;
											
											if(qadetail.prospectCallLog.qaFeedback == null) {
												$scope.overallScore = 0;
											} else {												
											    $scope.overallScore = qadetail.prospectCallLog.qaFeedback.overallScore;
												
												var index = 1;
												$scope.isScorePresent = false;
												$scope.isScoreAttributeComplete = true;
												angular.forEach($scope.qaFeedbackForm,  function(formValue, key){
													angular.forEach(qadetail.prospectCallLog.qaFeedback.feedbackResponseList,  function(feedbackResponseValue, index){
														if(key === feedbackResponseValue.sectionName) {
															angular.forEach(feedbackResponseValue.responseAttributes,  function(responseAttributesValue, subIndex){
																if (responseAttributesValue.score != undefined) {
																	$scope.isScorePresent = true;
																} else {
																	$scope.isScoreAttributeComplete = false;
														    	}
																formValue[subIndex].score = responseAttributesValue.score;
																formValue[subIndex].feedback = responseAttributesValue.feedback;
																formValue[subIndex].attributeComment = responseAttributesValue.attributeComment;
																formValue[subIndex].rejectionReason = responseAttributesValue.rejectionReason;
																if(responseAttributesValue.feedback != undefined) {
																	formValue[subIndex].checked = formValue[subIndex].feedback;
																}
																
															});
														}
													});
												});
												if (!$scope.isScorePresent) {
													$scope.isScoreAttributeComplete = false;
												}
//												DATE:29/12/2017 Reason:secondaryqa detail page disable handled (If feedbackGivenByUser!=current user isReadOnly true previsoly)
												/*UserServiceById.query({username: qadetail.prospectCallLog.qaFeedback.qaId}).$promise.then(function(userData){
													$scope.feedbackGivenByUser  = userData;
													if ($scope.feedbackGivenByUser.id != user.id) {
														$scope.isReadOnly = true;
														$scope.isReadOnlyClass = 'btn-back pull-right';
													}
												});*/
											}
										}
										
										$scope.emailStatus = '--';
										if ($scope.qaDetail.prospectCallLog.prospectCall.emailBounce) {
											qADetailService.emailStatus({
												prospectCallId: $scope.qaDetail.prospectCallLog.prospectCall.prospectCallId
											}).$promise.then(function(emailBounce) {
												if (emailBounce) {
													if (emailBounce.action && emailBounce.action.toLowerCase() == 'bounce') {
														if (emailBounce.status && emailBounce.status.toLowerCase().indexOf('spam') != -1) {
															$scope.emailStatus = 'Bounce - Spam';
														} else {
															$scope.emailStatus = 'Bounce - Others';
														}
													} else {
														$scope.emailStatus = 'Not Sent';
													}
												}
											});
										} else if ($scope.qaDetail.prospectCallLog.prospectCall.deliveredAssetId == null 
												|| $scope.qaDetail.prospectCallLog.prospectCall.deliveredAssetId == undefined) {
											$scope.emailStatus = 'Not Sent';
										} else {
											$scope.emailStatus = 'Delivered';
										}
										
										$scope.mapNumberToLetter = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N"];
										
										$scope.convertSecToMin = function(seconds) {                   
						                	return new Date(1970, 0, 1).setSeconds(seconds);
						                }
							            
										$scope.getDescription = function(key, index) {
											angular.forEach($scope.qaFeedbackForm,  function(value, skey){
												if(key == skey) {
													$scope.description = value[index].description;
												}
											});
										}

										var isNumber =  function(n) {
											  return (Object.prototype.toString.call(n) === '[object Number]' || Object.prototype.toString.call(n) === '[object String]') &&!isNaN(parseFloat(n)) && isFinite(n.toString().replace(/^-/, ''));
										}
										
										$scope.showCommentBox = function(index) {
											$scope.showComment[index]  = true;
										}
										
										$scope.doCalculatScore = function(){
											$scope.isScorePresent = false;
											$scope.isScoreAttributeComplete = true;
											$scope.isleadValidationChecked = false;
											
											var totScore = 0;
											var pointEarned = 0;
											var divisorPoint = 0;
											angular.forEach($scope.qaFeedbackForm,  function(value, key){
											    angular.forEach(value,  function(subvalue, subkey){
											    	if(subvalue.checked){
											    		if(subvalue.feedback == "YES") {
											    			pointEarned++;
											    			divisorPoint++;
												    	}
											    		if(subvalue.feedback == "NO") {
											    			divisorPoint++;
												    	}
											    		if(subvalue.attribute == "LEAD_VALIDATION_VALID"){
											    			$scope.isleadValidationChecked = true;
											    		}
											    		$scope.isScorePresent = true;
											    	} else {
											    		$scope.isScoreAttributeComplete = false;
											    		
											    		value[subkey].percentage = undefined;
											    		value[subkey].score = undefined;
											    		value[subkey].feedback = undefined;
											    	}
											    });
											});
											if (!$scope.isScorePresent) {
												$scope.isScoreAttributeComplete = false;
											}
											if (pointEarned == 0 && divisorPoint == 0) {
												if ($scope.calculateScore) {
													$scope.overallScore = -1;
												} else {
													$scope.overallScore = undefined;
												}
												
											} else {
												if (divisorPoint != 0) {
													$scope.overallScore = parseFloat((pointEarned/divisorPoint)*100).toFixed(2);
												}
											}
										}
										
										/*START
										 *DATE : 22/11/2017
										 *REASON :if attribute = "LEAD_VALIDATION_VALID" && radio checked = 'NO' then drop-down list will appear else not */ 
										$scope.showRejectReasons = function (feedback, attribute) {
											if (feedback == 'NO' && attribute == 'LEAD_VALIDATION_VALID') {
												$scope.isRejectReasonsVisible = true;
											} else if (feedback == 'YES' && attribute == 'LEAD_VALIDATION_VALID') {
												$scope.isRejectReasonsVisible = false;
												if (angular.isDefined($scope.qaFeedbackForm)) {
													angular.forEach($scope.qaFeedbackForm, function(answersValue, answersKey) {
														if (answersKey == "Lead Validation") {
															angular.forEach(answersValue, function(value, key) {
																if (value.attribute == "LEAD_VALIDATION_VALID" && value.checked == 'YES') {
																	$scope.qaFeedbackForm[answersKey][key].rejectionReason = null;
																}
															});
														}
													});
												}
											}
								        }
										
										if (angular.isDefined($scope.qaFeedbackForm)) {
											angular.forEach($scope.qaFeedbackForm, function(answersValue, answersKey) {
												if (answersKey == "Lead Validation") {
													angular.forEach(answersValue, function(value, key) {
														if (value.attribute == "LEAD_VALIDATION_VALID" && value.checked == 'NO') {
															$scope.isRejectReasonsVisible = true;
														}
													});
												}
											});
										}
										/*END*/
										
										$scope.goBack = function() {
											if (($scope.isScorePresent && !$scope.isReadOnly && $scope.isFormModified) || $scope.isNotesEdited) {												
												var modalInstance = $modal.open({
													scope: $scope,
													templateUrl: 'saveConfirmModal.html',
													controller: NavigationModalInstanceCtrl,
													resolve: {
														prospId: function () {
												          return '';
												        },
														navId: function () {
													          return '';
													    }
												      }
												});
												$scope.isQaScoreCompleted = true;												
											} else {
												//delete $localStorage.prospectCallList;
												//delete $localStorage.agentList;
							                    //delete $localStorage.campaignList;
												$scope.loading = true;
												$state.go('secondaryqa');												
											} 
										}
										
										$scope.calculateScore = true;
										$scope.isFormModified = false;
										$scope.addScore = function(parentIndex, key, index, score, feedback, questionSkin, item, event, customer_satisfaction) {
											if ($scope.isReadOnly) return;
											$scope.isFormModified = true; //If form is modified
											var testData = $scope.qaFeedbackForm;
											
											angular.forEach(testData,  function(value, skey){
												if(key == skey) {
													value[index].score = score;
													if (value[index].feedback === feedback)  {
														value[index].checked = false;
														value[index].feedback = undefined;
														$scope.calculateScore = false;
														$scope['showCommentMsg'+parentIndex+index] = undefined;
													} else {
														$scope.calculateScore = true;
														value[index].feedback = feedback;
														$scope['showCommentMsg'+parentIndex+index] = key;
														value[index].checked = feedback;
													}
												}
											});
											$scope.qaFeedbackForm = testData;
											$scope.doCalculatScore();
										}
										
										// removed condition - $scope.qaDetail.prospectCallLog.status == "QA_COMPLETE" || 
										if ($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == "DIALERCODE" || $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == "CALLBACK") {
											$scope.isReadOnly = true;
											$scope.isReadOnlyClass = 'btn-back pull-right';											
										} 
										
										$scope.isQaScoreCompleted = true;
									
										$scope.getDeliveredAssetTitle = function() {
											var assetName = "NA";
											if ( $scope.qaDetail.campaign.assets != undefined && $scope.qaDetail.campaign.assets != null) {
												for (var index = 0; index < $scope.qaDetail.campaign.assets.length; index++) {
													var asset = $scope.qaDetail.campaign.assets[index];
													if (asset.assetId === $scope.qaDetail.prospectCallLog.prospectCall.deliveredAssetId) {
														assetName = asset.name;
													}
												}
											}
											return assetName;
										}
										
										$scope.saveFeedback = function(act){
											// $scope.hideSaveSubmitButton = true;
											$scope.feedbackResponseList = [];
											var inputform = $scope.qaFeedbackForm;
											angular.forEach(inputform,  function(values, key){
												var feedbackSectionResponse = {
														"sectionName":key,
														"comments":"",
														"responseAttributes":[]
												}
											    angular.forEach(values,  function(subvalues, subkey){
											    	if (subvalues.score == undefined) {
											    		$scope.isQaScoreCompleted = false;
											    	}
											    	var feedbackResponseAttribute = {
															"attribute":subvalues.attribute,
											    			"attributeComment":subvalues.attributeComment,
											    			"feedback":subvalues.feedback,
											    			"rejectionReason":subvalues.rejectionReason,
											    			"score":subvalues.score
													}
											    	if(feedbackResponseAttribute.feedback == undefined) {
														feedbackResponseAttribute.feedback = "";
														feedbackResponseAttribute.score = 0;
													}
											    	feedbackSectionResponse.responseAttributes.push(feedbackResponseAttribute);
												 });
												$scope.feedbackResponseList.push(feedbackSectionResponse);
											});

											var value; // Variable is added to avoid service call to backend if mandatory fields are not selected. 
											if ($scope.clientMappings.length > 0) {
												 value = $scope.assignClientMappingValues();
											}
											// Below value will be null/undefined when no client mappings are available for the campaign.
											if (value == undefined || value == null || value == false) {
												$scope.save(act);											 
											}									 
										} 
										
										var NavigationModalInstanceCtrl = function ($scope, $modalInstance, prospId, navId) {
										    $scope.prospId = prospId;
										    $scope.navId = navId;
											$scope.goToSave = function () {
												if(prospId && navId == 'next') {													
													$modalInstance.dismiss('cancel');
													$scope.$parent.loading = true;
													var index = getIndex(prospId);											
													if(index == arrProspectCallList[1].length-1 || index == undefined) {
														if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {
															var lastpageNo = Math.ceil($scope.prospectCallList.prospectCallCount/$scope.filterCriteriaObj.pageSize);
															if ($scope.filterCriteriaObj.currentPage == lastpageNo) {
																$scope.disabldNextArrow = true;
															} else {
																$scope.$parent.loading = true;
																$scope.filterCriteriaObj.currentPage += 1;
																qaProspectListService.query({searchstring: angular.toJson($scope.getFilterObject())}, null).$promise.then(function(data) {
																	$scope.prospectCallList = data;
																	if ($scope.prospectCallList.prospectCallSearchList.length == 0) {
																		$scope.noNextRecordFound = true;
																		$scope.disabldNextArrow = true;
																		$scope.$parent.loading = false;
																	} else {
																		$localStorage.prospectCallList = JSON.stringify($scope.prospectCallList); //save prospect call search result
																		$localStorage.filterCriteriaObj = JSON.stringify($scope.filterCriteriaObj); //save prospect call search result
																		window.location.href ='/#!/secondaryqadetail/' + $scope.prospectCallList.prospectCallSearchList[0].prospectCall.prospectCallId;
																	}
																});
															}
														} else {
															$scope.disabldNextArrow = true;
														}
														
													} else {
														$scope.$parent.loading = true;
														$scope.disabldNextArrow = false;
														$scope.nextId = arrProspectCallList[1][index + 1].prospectCall.prospectCallId;
														window.location.href ='/#!/secondaryqadetail/' + $scope.nextId;
													}
												} else if(prospId && navId == 'prev') {													
													$modalInstance.dismiss('cancel');
													$scope.$parent.loading = true;
													var index = getIndex(prospId);
													if(index == 0 || index == undefined) {
														if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {
															if ($scope.filterCriteriaObj.currentPage == 0) {
																$scope.disabldPrevArrow = true;
															} else {
																$scope.filterCriteriaObj.currentPage -= 1;
																$scope.$parent.loading = true;
																qaProspectListService.query({searchstring: angular.toJson($scope.getFilterObject())}, null).$promise.then(function(data) {
																	$scope.prospectCallList = data;
																	if ($scope.prospectCallList.prospectCallSearchList.length == 0) {
																		$scope.noPreviousRecordFound = true;
																		$scope.disabldPrevArrow = true;
																		$scope.$parent.loading = false;
																	} else {
																		$localStorage.prospectCallList = JSON.stringify($scope.prospectCallList); //save prospect call search result
																		$localStorage.filterCriteriaObj = JSON.stringify($scope.filterCriteriaObj); //save prospect call search result
																		window.location.href ='/#!/secondaryqadetail/' + $scope.prospectCallList.prospectCallSearchList[0].prospectCall.prospectCallId;
																	}
																});
															}
														} else {
															$scope.disabldPrevArrow = true;
														}
														
													} else {
														$scope.$parent.loading = true;
														$scope.disabldPrevArrow = false;
														$scope.prevId = arrProspectCallList[1][index - 1].prospectCall.prospectCallId;														
														window.location.href ='/#!/secondaryqadetail/' + $scope.prevId;	
													}
												} else {
													$scope.saveFeedback('SAVE')
													$modalInstance.dismiss('cancel');
													$scope.$parent.loading = true;
													$state.go('secondaryqa');	
												}																								
											};
                                  		  
											$scope.cancel = function () {
												$modalInstance.dismiss('cancel');
											};

										};
										
										var saveFeedbackInLocalStorage = function (act) {
											//save feedback in local storage
											if ($localStorage.prospectCallList != undefined && $localStorage.prospectCallList != "") {
												$scope.prospectCallList = JSON.parse($localStorage.prospectCallList);
												angular.forEach($scope.prospectCallList.prospectCallSearchList,  function(prospectCallDetail, index) {
													if (prospectCallDetail.prospectCall.prospectCallId == $stateParams.prospectCallId) {
														prospectCallDetail.overallScore = parseFloat($scope.overallScore);
														if ($scope.isQaScoreCompleted && act == 'SUBMIT') {
															prospectCallDetail.status = "QA_COMPLETE";
														} else if ($scope.isQaScoreCompleted && act == 'RETURN') {
															prospectCallDetail.status = "QA_INITIATED";
														} else {
															prospectCallDetail.status = "SECONDARY_QA_INITIATED";
														}
													}
												});
												$localStorage.prospectCallList = JSON.stringify($scope.prospectCallList);
											}
										}
										
										var performOperationOnResponse = function(act) {
//											DATE:02/01/2018	On submit redirect to next lead
											$scope.loading = false;
											if (act == 'SUBMIT') {
												$scope.isFormModified = false;
												saveFeedbackInLocalStorage(act);
												// NotificationService.log("SUCCESS",'Record is submitted');
												$scope.isReadOnly = true;
												var index = getIndex($stateParams.prospectCallId);
												if(index == arrProspectCallList[1].length-1 || index == undefined) {
													if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {
														var lastpageNo = Math.ceil($scope.prospectCallList.prospectCallCount/$scope.filterCriteriaObj.pageSize);
														if ($scope.filterCriteriaObj.currentPage == lastpageNo) {
															delete $localStorage.prospectCallList;
															$state.go('secondaryqa');
														}
													}
												}
												$scope.nextPage($stateParams.prospectCallId, "SUBMIT");
											} else if (act == 'RETURN') {
												$scope.isFormModified = false;
												saveFeedbackInLocalStorage(act);
												NotificationService.log("SUCCESS",'Record is sent to primary qa.');
												$scope.isReadOnly = true;
												/*$scope.nextPage($stateParams.prospectCallId);*/
											} else {
												/*$scope.loading = true;*/
												$scope.isFormModified = false;
												$rootScope.saveData = true;
												$rootScope.submitData = false;
												saveFeedbackInLocalStorage(act);
												NotificationService.log("SUCCESS",'Record is saved');
												/*$state.go('secondaryqa');*/
											}		
										}
										
										//Date - 15-01-2018
										//REASON - added function to validate lead details on submit
										$scope.validateLeadDetails = function() {
											if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.firstName == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.firstName == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.firstName == "") {
												NotificationService
														.log('error',
																'Please enter First Name');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.lastName == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.lastName == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.lastName == "") {
												NotificationService
														.log('error',
																'Please enter Last Name');
												return false;
											} else if (($scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine1 == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine1 == null
													  ||$scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine1 == "") 
													&& ($scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine2 == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine2 == null
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine2 == "")) {
												NotificationService
														.log('error',
																'Please enter Address');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.city == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.city == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.city == "") {
												NotificationService
														.log('error',
																'Please enter City');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.stateCode == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.stateCode == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.stateCode == "") {
												NotificationService
														.log('error',
																'Please enter State');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.country == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.country == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.country == "") {
												NotificationService
														.log('error',
																'Please enter Country');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.zipCode == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.zipCode == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.zipCode == "") {
												NotificationService
														.log('error',
																'Please enter Zip Code');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.phone == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.phone == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.phone == "") {
												NotificationService
														.log('error',
																'Please enter Phone');
												return false;
											} else if ($scope.qaDetail.prospectCallLog.prospectCall.prospect.email == undefined
													|| $scope.qaDetail.prospectCallLog.prospectCall.prospect.email == null
													  || $scope.qaDetail.prospectCallLog.prospectCall.prospect.email == "") {
												NotificationService
														.log('error',
																'Please enter Email');
												return false;
											}
											return true;
										}
                                  	  
										$scope.save = function (act) {
											$scope.loading = true;  
											var agentNotes = "";
											if ($scope.qaDetail.prospectCallLog.prospectCall.notes != null && $scope.qaDetail.prospectCallLog.prospectCall.notes != undefined && $scope.qaDetail.prospectCallLog.prospectCall.notes.length > 0) {
												agentNotes = $scope.qaDetail.prospectCallLog.prospectCall.notes[0].text
											} 
											var sendData = {
													"qaId": $scope.qaDetail.prospectCallLog.qaFeedback.qaId,
													"secQaId": user.id,
													"feedbackResponseList" :  $scope.feedbackResponseList,
													"overallScore" : parseFloat($scope.overallScore),
													"feedbackTime": new Date(),
													/*"isAnswerValid": $scope.isAnswerValid,
													"answerCertification": $scope.answerCertification,*/
													"qaAnswers": $scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers,
													"agentNotes" : agentNotes
												};
											
											/* START PROSPECT STATUS AFTER SUBMIT
											 * DATE : 15/12/2017
											 * REASON : showing error popup if reject reason is not selected */
											var noRejectReason = false;
											if (angular.isDefined($scope.qaFeedbackForm)) {
												angular.forEach($scope.qaFeedbackForm, function(answersValue, answersKey) {
													if (answersKey == "Lead Validation") {
														angular.forEach(answersValue, function(value, key) {
															if (value.attribute == "LEAD_VALIDATION_VALID" && value.checked == 'NO') {
																if (value.rejectionReason == undefined || value.rejectionReason == null || value.rejectionReason == '') {
																	noRejectReason = true;
																}
															}
														});
													}
												});
											}
											
											if (noRejectReason) {
												NotificationService.log('error','Please select reject reason');
												$scope.hideSaveSubmitButton = false;
												$scope.loading = false;
												return;
											}
											if ($scope.verifyValidLead()) {
													if(act == 'SAVE') {
														if ($scope.qaDetail.prospectCallLog.status == 'SECONDARY_QA_INITIATED') {
															qADetailSaveFeedbackService.save({"prospectCallId": $stateParams.prospectCallId, "secondaryQaReview" : 'YES'}, sendData).$promise.then(function(data) { 
																$scope.hideSaveSubmitButton = false;
																$scope.isNotesEdited = false;
																performOperationOnResponse(act);
															},function(error) {
																$scope.loading = false;
																$scope.hideSaveSubmitButton = false;
															});
														} else {
															qADetailSaveFeedbackService.save({"prospectCallId": $stateParams.prospectCallId, "secondaryQaReview" : 'YES'}, sendData).$promise.then(function(data) { 
																$scope.hideSaveSubmitButton = false;
																$scope.isNotesEdited = false;
																performOperationOnResponse(act);
															},function(error) {
																$scope.loading = false;
																$scope.hideSaveSubmitButton = false;
															});
														}
													}
													if(act == 'SUBMIT') {
														if (!$scope.validateLeadDetails()) {
															$scope.hideSaveSubmitButton = false;
															$scope.loading = false;
															return;
														}
														if (!noRejectReason) {
															if (($scope.qaDetail.prospectCallLog.status == 'QA_INITIATED' || $scope.qaDetail.prospectCallLog.status == 'WRAPUP_COMPLETE') && $scope.isRejectReasonsVisible && $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == 'SUCCESS') {
																qADetailSubmitFeedbackService.save({"prospectCallId": $stateParams.prospectCallId, "secondaryQaReview" : 'YES'}, sendData).$promise.then(function(data) { 
																	$scope.hideSaveSubmitButton = false;
																	$scope.isNotesEdited = false;
																	performOperationOnResponse(act);
																},function(error) {
																	$scope.loading = false;
																	$scope.hideSaveSubmitButton = false;
																});
															} else {
																qADetailSubmitFeedbackService.save({"prospectCallId": $stateParams.prospectCallId, "secondaryQaReview" : 'NO'}, sendData).$promise.then(function(data) { 
																	$scope.hideSaveSubmitButton = false;
																	$scope.isNotesEdited = false;
																	performOperationOnResponse(act);
																},function(error) {
																	$scope.loading = false;
																	$scope.hideSaveSubmitButton = false;
																});
															}
														} else {
															NotificationService.log('error','Please select reject reason');
															$scope.hideSaveSubmitButton = false;
														}
													}
													if(act == 'RETURN') {
														if (!$scope.validateLeadDetails()) {
															$scope.hideSaveSubmitButton = false;
															$scope.loading = false;
															return;
														}
														if (!noRejectReason) {
															returnProspectToPrimaryService.save({"prospectCallId": $stateParams.prospectCallId}, sendData).$promise.then(function(data) {
																$scope.hideSaveSubmitButton = false;
																$scope.isNotesEdited = false;
																performOperationOnResponse(act);
															},function(error) {
																$scope.loading = false;
																$scope.hideSaveSubmitButton = false;
															});
														} else {
															NotificationService.log('error','Please select reject reason');
															$scope.hideSaveSubmitButton = false;
														}
													}
										      }
										}

										$scope.verifyValidLead = function() {
											var inValidLead = true;
											if (angular.isDefined($scope.qaFeedbackForm)) {
												angular.forEach($scope.qaFeedbackForm, function(answersValue, answersKey) {
													if (answersKey == "Lead Validation") {
														angular.forEach(answersValue, function(value, key) {
															if (value.attribute == "LEAD_VALIDATION_VALID" && value.checked == 'YES' && $scope.invalidSpecs) {
																$scope.loading = false;
																inValidLead = false;
																NotificationService.log('error','Invalid specification for ' + $scope.invalidColumnHeader + ' Please reject the lead.');
															}
														});
													}
												});
											}
											return inValidLead;
								        }
										
										$scope.toggleComment = function (key, skin, pid, id, comment, commentAction) {											
											
											if(skin == $scope.main.activeRow) {
												$scope.main.activeRow = null;
												$scope['showHidePop_' + pid + '_' + id] = false;
											} else {
												$scope.main.activeRow = skin;
												$scope['showHidePop_' + pid + '_' + id] = true;	
											}
											if (commentAction == undefined) {
												angular.forEach($scope.qaFeedbackForm,  function(values, mainLabelkey){
													if (mainLabelkey == key) {
														if (values[id].attributeComment) {
															$scope['tempComment' + pid + '_' + id]  = values[id].attributeComment;
														}
													}
												});
											}
											if (commentAction == 'Cancel') {
												angular.forEach($scope.qaFeedbackForm,  function(values, mainLabelkey){
													if (mainLabelkey == key) {
														values[id].attributeComment = $scope['tempComment' + pid + '_' + id];
														$scope['tempComment' + pid + '_' + id] = undefined;
													}
												});
											}
										}
										
										$scope.cancel = function(){
											$state.go('secondaryqa');
										}
										
										$scope.notSorted = function(obj){
											if (!obj) {
												return [];
											}
											return Object.keys(obj);
										}	
										var arrProspectCallList = [];
										if ($localStorage.prospectCallList != null && $localStorage.prospectCallList != '') {
											var parsed = JSON.parse($localStorage.prospectCallList);
											for(var x in parsed){
												arrProspectCallList.push(parsed[x]);
											}											
										}
										
										 //Get client time-zone
						                var tz = jstz.determine();
						                var timeZone = tz.name(); // returns "America/Los Angeles"
										
										$scope.getFilterObject = function() {
											var dispostionStatuses = [];
											dispostionStatuses.push('SECONDARY_QA_INITIATED');
											var filterObject = {
							                        "clause": "ByQa2",
							                        "campaignId": $scope.filterCriteriaObj.selectedCampaignId,
							                        "agentIds": $scope.filterCriteriaObj.agentIds,
							                        "page": $scope.filterCriteriaObj.currentPage - 1,
							                        "size": $scope.filterCriteriaObj.pageSize,
							                        "toDate": $scope.filterCriteriaObj.endDate,
							                        "fromDate": $scope.filterCriteriaObj.startDate,
							                        "startHour": $scope.filterCriteriaObj.startHour,
							                        "endHour":  $scope.filterCriteriaObj.endHour,
							                        "startMinute":  $scope.filterCriteriaObj.startMinute,
							                        "endMinute": $scope.filterCriteriaObj.endMinute,
//							                        "disposition": dispostionStatuses,
							                        "prospectCallIds": $scope.allprospectCallId,
							                        "clientId": $scope.filterCriteriaObj.selectedClientId,
							                        "prospectCallStatus":  $scope.filterCriteriaObj.qastatus,
							                        "timeZone": timeZone
											};
											return filterObject;
										}

										$scope.noNextRecordFound = false;
										$scope.noPreviousRecordFound = false;
										$scope.disabldNextArrow = false;
										$scope.disabldPrevArrow = false;

										$scope.nextPage = function(prospId, action) {
											if (($scope.isScorePresent && !$scope.isReadOnly && $scope.isFormModified) || $scope.isNotesEdited) {																							
												var modalInstance = $modal.open({
													scope: $scope,
													templateUrl: 'saveConfirmModal.html',
													controller: NavigationModalInstanceCtrl,
													resolve: {
														prospId: function () {
												          return prospId;
												        },
														navId: function () {
													          return 'next';
													    }
												      }
												});
												$scope.isQaScoreCompleted = true;
										  } else {
												var index = getIndex(prospId);											
												if(index == arrProspectCallList[1].length-1 || index == undefined) {
													if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {
														var lastpageNo = Math.ceil($scope.prospectCallList.prospectCallCount/$scope.filterCriteriaObj.pageSize);
														if ($scope.filterCriteriaObj.currentPage == lastpageNo) {
															$scope.disabldNextArrow = true;
														} else {
															$scope.loading = true;
															$scope.filterCriteriaObj.currentPage += 1;
															if (action == "SUBMIT") {
																$scope.filterCriteriaObj.currentPage = 1;	
															}
															qaProspectListService.query({searchstring: angular.toJson($scope.getFilterObject())}, null).$promise.then(function(data) {
																$scope.prospectCallList = data;
																if ($scope.prospectCallList.prospectCallSearchList.length == 0) {
																	$scope.noNextRecordFound = true;
																	$scope.disabldNextArrow = true;
																	$scope.loading = false;
																} else {
																	$localStorage.prospectCallList = JSON.stringify($scope.prospectCallList); //save prospect call search result
																	$localStorage.filterCriteriaObj = JSON.stringify($scope.filterCriteriaObj); //save prospect call search result
																	window.location.href ='/#!/secondaryqadetail/' + $scope.prospectCallList.prospectCallSearchList[0].prospectCall.prospectCallId;
																	$scope.loading = false;
																}
															});
														}
													} else {
														$scope.disabldNextArrow = true;
													}
												} else {
													$scope.loading = true;
													$scope.disabldNextArrow = false;
													$scope.nextId = arrProspectCallList[1][index + 1].prospectCall.prospectCallId;
													$scope.nextStat = arrProspectCallList[1][index + 1].prospectCall.dispositionStatus;	
													/*if($scope.nextStat == 'CALLBACK' || $scope.nextStat == 'DIALERCODE') {
														$scope.nextPage($scope.nextId);
													} else {*/
														window.location.href ='/#!/secondaryqadetail/' + $scope.nextId;	
													//}													
												}
										   }
										}

										$scope.prevPage = function(prospId){
											if ($scope.disabldPrevArrow) return;
											if (($scope.isScorePresent && !$scope.isReadOnly && $scope.isFormModified) || $scope.isNotesEdited) {																							
												var modalInstance = $modal.open({
													scope: $scope,
													templateUrl: 'saveConfirmModal.html',
													controller: NavigationModalInstanceCtrl,
													resolve: {
														prospId: function () {
												          return prospId;
												        },
														navId: function () {
													          return 'prev';
													    }
												      }
												});
												$scope.isQaScoreCompleted = true;
											} else {
												var index = getIndex(prospId);
												if(index == 0 || index == undefined) {
													if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {
														if ($scope.filterCriteriaObj.currentPage == 0) {
															$scope.disabldPrevArrow = true;
														} else {
															$scope.filterCriteriaObj.currentPage -= 1;
															if ($scope.filterCriteriaObj.currentPage < 1) {
																$scope.filterCriteriaObj.currentPage = 1;
															}
															$scope.loading = true;
															qaProspectListService.query({searchstring: angular.toJson($scope.getFilterObject())}, null).$promise.then(function(data) {
																$scope.prospectCallList = data;
																if ($scope.prospectCallList.prospectCallSearchList.length == 0) {
																	$scope.noPreviousRecordFound = true;
																	$scope.disabldPrevArrow = true;
																	$scope.loading = false;
																} else {
																	$localStorage.prospectCallList = JSON.stringify($scope.prospectCallList); //save prospect call search result
																	$localStorage.filterCriteriaObj = JSON.stringify($scope.filterCriteriaObj); //save prospect call search result
																	var prevRecordIndex = $scope.prospectCallList.prospectCallSearchList.length - 1;
																	window.location.href ='/#!/secondaryqadetail/' + $scope.prospectCallList.prospectCallSearchList[prevRecordIndex].prospectCall.prospectCallId;
																	$scope.loading = false;
																}
															});
														}
													} else {
														$scope.disabldPrevArrow = true;
													}
													
												} else {
													$scope.loading = true;
													$scope.disabldPrevArrow = false;
													$scope.prevId = arrProspectCallList[1][index - 1].prospectCall.prospectCallId;
													$scope.prevStat = arrProspectCallList[1][index - 1].prospectCall.dispositionStatus;
													/*if($scope.prevStat == 'CALLBACK' || $scope.prevStat == 'DIALERCODE') {
														$scope.prevPage($scope.prevId);
													} else {*/
														window.location.href ='/#!/secondaryqadetail/' + $scope.prevId;	
													//}	
												}
										    }
										}
										
										$timeout(function () { 
											$scope.noNextRecordFound = false;
											$scope.noPreviousRecordFound = false;
						                }, 5000);
										
										
										
										
										$scope.alldisposition = [];
										$scope.allprospectCallId = [];
										
										if ($localStorage.filterCriteriaObj != null && $localStorage.filterCriteriaObj != "" && $localStorage.prospectCallList != null && $localStorage.prospectCallList != '') {
											$scope.filterCriteriaObj = JSON.parse($localStorage.filterCriteriaObj);
											$scope.prospectCallList = JSON.parse($localStorage.prospectCallList);
											if ($scope.filterCriteriaObj.dispositionStatus != undefined && $scope.filterCriteriaObj.dispositionStatus != "") {
												$scope.alldisposition.push($scope.filterCriteriaObj.dispositionStatus);
											}
											if ($scope.filterCriteriaObj.callId != undefined && $scope.filterCriteriaObj.callId != "") {
												$scope.allprospectCallId.push($scope.filterCriteriaObj.callId);
											}
										}
										
										var getIndex = function(prospId) {
											if (arrProspectCallList.length != 0) {
												for(var i = 0; i < arrProspectCallList[1].length; i++) {
													   if(arrProspectCallList[1][i].prospectCall.prospectCallId === prospId) {
														   return i;
												        }
												}
											}
										}
										
										var chkIndex = function(prospId) {
											var index = getIndex(prospId);
											if ($scope.filterCriteriaObj != undefined && $localStorage.prospectCallList != null) {//get value from localstorage
												var lastpageNo = Math.ceil($scope.prospectCallList.prospectCallCount/$scope.filterCriteriaObj.pageSize);
												//if (lastpageNo == $scope.filterCriteriaObj.currentPage && index == $scope.filterCriteriaObj.currentPageLength-1) {
												if (lastpageNo == $scope.filterCriteriaObj.currentPage || $scope.filterCriteriaObj.currentPageLength == 1) {
													$scope.nextProspect = arrProspectCallList[1][index + 1]
													if ($scope.nextProspect == null || $scope.nextProspect == undefined) {
														$scope.disabldNextArrow = true;
													}
												}
												if ((index == 0 || index == undefined) && $scope.filterCriteriaObj.currentPage == 0) {
													$scope.disabldPrevArrow = true;
												}
											} else {
												if(arrProspectCallList.length != 0 && index == arrProspectCallList[1].length-1 || index == undefined) {
													$scope.disabldNextArrow = true;
												} 
												if(index == 0 || index == undefined) {
													$scope.disabldPrevArrow = true;
												}
											}
										}
										
										if($scope.qaDetail.prospectCallLog.prospectCall.prospectCallId){
											chkIndex($scope.qaDetail.prospectCallLog.prospectCall.prospectCallId);											
										}
										
//										DATE:18/12/2017	Added method to show phone no in xxx-xxx-xxxx format and verify edited phone no
										$scope.saveLeadDetails = function(){
											$scope.loading = true;
											if ($scope.secEditablePhoneText != 'XXX-XXX-XXXX') {
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.phone = $scope.secEditablePhoneText;
											}
											if ($scope.secEditableEmailText != 'XXXXXXXXXX@XXX') {
												$scope.qaDetail.prospectCallLog.prospectCall.prospect.email = $scope.secEditableEmailText;
											}
											$scope.qaDetail.prospectCallLog.prospectCall.prospect.prospectCallId = $stateParams.prospectCallId;
											$scope.assignMinAndMaxEmployee();
											$scope.assignMinAndMaxRevenue();
											qaUpdateProspectService.update({"prospectCallId": $stateParams.prospectCallId}, $scope.qaDetail.prospectCallLog.prospectCall.prospect).$promise.then(function(data) { 
												NotificationService.log('success','Lead detail save successfully');
												$scope.loading = false;
												if($scope.qaDetail.campaign  != null && $scope.qaDetail.campaign != undefined && $scope.qaDetail.campaign.hideNonSuccessPII){
													if($scope.qaDetail != null && $scope.qaDetail.prospectCallLog != null && $scope.qaDetail.prospectCallLog.prospectCall != null && $scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus != null && !($scope.qaDetail.prospectCallLog.prospectCall.dispositionStatus == 'SUCCESS')) {
														$scope.secEditablePhoneText = 'XXX-XXX-XXXX';
														$scope.secEditableEmailText = 'XXXXXXXXXX@XXX'
													}
												}
											},function(error) {
												console.log("Error " + JSON.stringify(error));
												$scope.loading = false;
											});
										}

										/* START
										 * DATE : 10/05/2017 
										 * REASON : Added to handle QA validation / questionnaire block */
										$scope.isCurrentQuestion = 1;
										if ($scope.clientMappings.length == 0) {	
											$scope.questionAttributeMap = [];
											$scope.totalQuestion = 0;
											if(angular.isDefined($scope.qaDetail.questionAttributeMap)) {
												angular.forEach($scope.qaDetail.questionAttributeMap, function(v, k) {
													$scope.totalQuestion = $scope.totalQuestion + 1;
											});
										}
								     }
							            
							            $scope.leftSlide = function () {
							                var i = $scope.isCurrentQuestion;
							                if (i == 1) {
							                } 
							                else {
							                    i--;
							                }
							                $scope.slidequestion(i);
							            }
							            
							            $scope.rightSlide = function () {
							                //console.log( $scope.qaDetail.questionAttributeMap.length);
							                var i = $scope.isCurrentQuestion;
							                if (i == $scope.totalQuestion) {
							                } 
							                else {
							                    i++;
							                }
							                $scope.slidequestion(i);
							            }
							            
							            $scope.slidequestion = function (currentQuestion) {
							                $scope.isCurrentQuestion = currentQuestion;
							            }
							            
							            $scope.questionSkin;
							            $scope.getAnswer = function(questionSkin) {
							            	angular.forEach($scope.qaDetail.prospectCallLog.prospectCall.answers,  function(value, key) {
							            		if (key == questionSkin) {
							            			return value;
												} else {
													return null;
												}
							            	});
							            }
							            
							            /* START QUESTIONNAIRE BLOCK VALIDATION
							             * DATE : 13/05/2017
							             * REASON : below code is used to validate questionnaire */
										if ($scope.clientMappings.length == 0) {	
											if(angular.isDefined($scope.qaDetail.questionAttributeMap)) {
													$scope.certifyQuestionnaire = [];
													$scope.isAgentValidationRequired = [];
													angular.forEach($scope.qaDetail.questionAttributeMap, function(questionAttributeMapValue, questionAttributeMapKey) {
														if(questionAttributeMapKey  != "CUSTOMFIELD_01"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_02"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_03"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_04"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_05"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_06"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_07"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_08"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_09"
																									&& questionAttributeMapKey  != "CUSTOMFIELD_10") {
																										$scope.questionSkin = questionAttributeMapValue.questionSkin;
																										var questionPickList = questionAttributeMapValue.pickList;
																										$scope.crmName = questionAttributeMapValue.crmName;
																										var answerFlag = true;
																										var validAnsers = [];
																										angular.forEach($scope.qaDetail.prospectCallLog.prospectCall.answers, function(answersValue, answersKey) {
																												if (answerFlag) {
																													if(answersValue.question == $scope.questionSkin) {
																														angular.forEach(questionPickList, function(pickListValue, pickListKey) {
																															if (pickListValue.value == answersValue.answer) {
																																$scope.answer = pickListValue.label;
																																answerFlag = false;
																															}
																														});
																													} else {
																														$scope.answer = "";
																													}
																												}
																										});
																										angular.forEach($scope.qaDetail.campaign.qualificationCriteria.expressions, function(expressionsValue, expressionsKey) {
																												if (expressionsValue.attribute == questionAttributeMapValue.crmName) {
																															$scope.isAgentValidationRequired.push(expressionsValue.agentValidationRequired);
																															$scope.operator = expressionsValue.operator;
																															angular.forEach(expressionsValue.value, function(expressionsObjValue, expressionsObjKey) {
																																angular.forEach(questionPickList, function(pickListValue, pickListKey) {
																																	if (pickListValue.value == expressionsObjValue) {
																																		validAnsers.push(pickListValue.label);
																																	}
																																});
																															});
																												}
																										});
																										$scope.certifyQuestionnaire.push({'questionSkin' : $scope.questionSkin, 'answer' : $scope.answer, 'validAnswers' : validAnsers, 'operator' : $scope.operator, 'crmName': $scope.crmName});
																									}
																									$scope.totalQuestion = $scope.certifyQuestionnaire.length;
													});							            		
											}
										}
										$scope.prepareCustomQuestions = function() {
											$scope.certifyQuestionnaire = [];
											var clientMappingValues = [];
											if ($scope.clientMappings != null && $scope.clientMappings != undefined) {
													angular.forEach($scope.clientMappings,  function(clientValue, clientKey) {
														if (clientValue.clientValues != null && clientValue.clientValues != undefined) {
															if (clientValue.clientValues.length > 1) {
																clientMappingValues = clientValue.clientValues;
																if (clientValue.stdAttribute != null && clientValue.stdAttribute != undefined) {
																	$scope.sortClientValuesAlphabetically(clientMappingValues);
																	$scope.certifyQuestionnaire.push({'colHeader': clientValue.colHeader, 'stdAttribute' : clientValue.stdAttribute, 'clientValues' : clientMappingValues, 'answerCertification':"", 'clientValue': "" });
																}
															}
														}
													});
													$scope.prepareQaAnswers();
													$scope.totalQuestion = $scope.certifyQuestionnaire.length;
											}
										}

									$scope.sortClientValuesAlphabetically = function (clientValues) {
											if (clientValues != null && clientValues != undefined) {
												clientValues.sort(function(a, b) {
													if (a.toLowerCase() < b.toLowerCase()) return -1;
													if (a.toLowerCase() > b.toLowerCase()) return 1;
													return 0;
												});
											}		
									}	

									$scope.prepareQaAnswers = function () {
										if ($scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers != undefined && $scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers.length > 0) {
													angular.forEach($scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers,  function(qaClientValue, qaClientKey) {
																	angular.forEach($scope.certifyQuestionnaire, function(certifyValue, certifyKey) {
																				if (certifyValue.stdAttribute == qaClientValue.attribute) {
																							certifyValue.answerCertification = qaClientValue.answerCertification;
																							certifyValue.clientValue = qaClientValue.clientValue
																				}
																	})
													});
										 } else {
											$scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers = [];
											angular.forEach($scope.clientMappings,  function(clientValue, clientKey) {
												if (clientValue.clientValues != null && clientValue.clientValues != undefined) {
													if (clientValue.clientValues.length > 1) {
														if (clientValue.stdAttribute != null && clientValue.stdAttribute != undefined) {
																	var obj = {};
																	obj.isAnswerValid = "";
																	obj.answerCertification = "";
																	obj.sequence = clientValue.sequence;
																	obj.attribute = clientValue.stdAttribute;
																	obj.clientValue =  clientValue.clientValue;	
																	$scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers.push(obj);
														}
													}
												}
											});		
										 }
								}

									$scope.assignClientMappingValues = function() {
										// Below flags are added to keep answercertification and client value mandatory.
										var isAnswerCertificationEntered = true; 
										var isclientValueSelected = true;	
										$scope.invalidSpecs = false;		
										angular.forEach($scope.certifyQuestionnaire, function(value, key) {
											if (value.answerCertification == null || value.answerCertification == undefined || value.answerCertification == "") {
												isAnswerCertificationEntered = false;
											}
											if (value.clientValue == null || value.clientValue == undefined || value.clientValue == "") {
												isclientValueSelected = false;
											}
											if (value.clientValue == 'INVALID SPECS' || value.clientValue == 'INVALID SPECIFICATIONS' || value.clientValue == 'Invalid Specifications' || value.clientValue == 'INVALID Specifications' ) {
												$scope.invalidColumnHeader = value.colHeader;
												$scope.invalidSpecs = true;
											}
											var val = $scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers.filter(val => val.attribute == value.stdAttribute);
											if (val.length == 0) {
												var obj = {};
												obj.isAnswerValid = "";
												obj.answerCertification = value.answerCertification;
												obj.sequence = value.sequence;
												obj.attribute = value.stdAttribute;
												obj.clientValue =  value.clientValue;	
												$scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers.push(obj);
											}
												angular.forEach($scope.qaDetail.prospectCallLog.qaFeedback.qaAnswers, function(qaValue, qaKey) {
															if (value.stdAttribute == qaValue.attribute) {
																	qaValue.answerCertification = value.answerCertification;
																	qaValue.clientValue = value.clientValue;
															}
												})
										})
										if (!isAnswerCertificationEntered) {
											NotificationService.log('error','Required fields are missing.(Certify, client value)');
											return true;
										}
										if (!isclientValueSelected) {
											NotificationService.log('error','Required fields are missing.(Certify, client value)');
											return true;
										}
										return false;
									}
									/* END QUESTIONNAIRE BLOCK VALIDATION */	
							            
							            
							            /* START ADDRESS FIELD SHOW/HIDE
							             * DATE : 14/06/2017
							             * REASON : Added to show and hide address field in UI.  */
							            $scope.getAddress1Flag = function() {
							            	$scope.addressLine1Flag = true;
							            	$scope.addressLine2Flag = true;
							            	$scope.addressLine1 = $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine1;
							            	$scope.addressLine2 = $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine2;
							            	
											if ($scope.addressLine1 == '' || $scope.addressLine1 == null || $scope.addressLine1 == undefined) {
												$scope.addressLine1Flag = false;
											}
											if ($scope.addressLine2 == '' || $scope.addressLine2 == null || $scope.addressLine2 == undefined) {
												$scope.addressLine2Flag = false;
											}
											if (($scope.addressLine1Flag && !$scope.addressLine2Flag) || (!$scope.addressLine1Flag && !$scope.addressLine2Flag) || ($scope.addressLine1Flag && $scope.addressLine2Flag)) {
												return true;
											}
										}
							            $scope.getAddress2Flag = function() {
							            	$scope.addressLine1Flag = true;
							            	$scope.addressLine2Flag = true;
							            	$scope.addressLine1 = $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine1;
							            	$scope.addressLine2 = $scope.qaDetail.prospectCallLog.prospectCall.prospect.addressLine2;
							            	
											if ($scope.addressLine1 == '' || $scope.addressLine1 == null || $scope.addressLine1 == undefined) {
												$scope.addressLine1Flag = false;
											}
											if ($scope.addressLine2 == '' || $scope.addressLine2 == null || $scope.addressLine2 == undefined) {
												$scope.addressLine2Flag = false;
											}
											if ((!$scope.addressLine1Flag && $scope.addressLine2Flag) || ($scope.addressLine1Flag && $scope.addressLine2Flag)) {
												return true;
											}
										}
										/* END ADDRESS FIELD SHOW/HIDE */
										
										$scope.showChangeLeadStatusModal = function () {
											$scope.leadStatus = "";
											$scope.changeLeadStatusModal = $modal.open({
												scope: $scope,
												templateUrl: 'changeLeadStatusModal.html',
												controller: 'changeLeadStatusModalController',
												windowClass: 'changeLeadStatusModal',
												backdrop: 'static'
											});
										}

										$scope.showCampaignScriptModal = function () {
											$scope.scriptSelectedTab="questions";
											if ($scope.qaDetail.campaign!= null && $scope.qaDetail.campaign != undefined && $scope.qaDetail.campaign.callGuide != null && $scope.qaDetail.campaign.callGuide != undefined) {
												$scope.confirmSectionScript =  $scope.qaDetail.campaign.callGuide.confirmSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.confirmSection.script) : null;
												$scope.questionSectionScript = $scope.qaDetail.campaign.callGuide.questionSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.questionSection.script) : null;
												$scope.consentSectionScript = $scope.qaDetail.campaign.callGuide.consentSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.consentSection.script) : null;
												$scope.noteSectionScript = $scope.qaDetail.campaign.callGuide.noteSection != null ? $sce.trustAsHtml($scope.qaDetail.campaign.callGuide.noteSection.script) : null;
											}
											$scope.campaignScriptModal = $modal.open({
												scope: $scope,
												templateUrl: 'campaignScriptModal.html',
												controller: 'campaignScriptModalController',
												windowClass: '',
												backdrop: 'static'
											});
										}

										$scope.showCallbackHistoryModal = function () {
											$scope.callbackHistoryModal = $modal.open({
												scope: $scope,
												templateUrl: 'callBackHistoryModal.html',
												controller: 'callbackHistoryModalController',
												windowClass: '',
												backdrop: 'static'
											});
										}

										$scope.showAgentNotesModal = function () {
											$scope.agentNotesModal = $modal.open({
												scope: $scope,
												templateUrl: 'agentNotesModal.html',
												controller: 'agentNotesModalController',
												windowClass: '',
												backdrop: 'static'
											});
										}

										$scope.setLeadStatus = function(status) {
											$scope.leadStatus = status;
										}
										
										$scope.saveLeadStatus = function() {
											if($scope.leadStatus == "") {
												return;
											}
											changeLeadStatusService.query({"prospectCallId": $stateParams.prospectCallId ,"leadStatus":$scope.leadStatus}, null).$promise.then(function(data) {
												$scope.changeLeadStatusModal.close();
												NotificationService.log('success','Lead status updated successfully');

											});
										}
										
									} 								
									
									]).controller('changeLeadStatusModalController', function ($scope, $modalInstance) {
										$scope.cancel = function () {
											$modalInstance.dismiss('cancel');
										};
									}).controller('campaignScriptModalController', function ($scope, $modalInstance) {
										$scope.cancel = function () {
											$modalInstance.dismiss('cancel');
										};
									}).controller('callbackHistoryModalController', function ($scope, $modalInstance) {
										$scope.cancel = function () {
											$modalInstance.dismiss('cancel');
										};
									}).controller('agentNotesModalController', function ($scope, $modalInstance) {
										$scope.cancel = function () {
											$modalInstance.dismiss('cancel');
										};
									}).filter('capitalize', function() {
							            return function(input, all) {
							                return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
							                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
							                }) : '';
							            }
									}).filter('revFilter', function() {
										return function(number) {
											if (number) {
												abs = Math.abs(number);
												if (abs >= Math.pow(10, 12)) {
													// trillion
													number = (number / Math.pow(10, 12))
															.toFixed(1)
															+ "Trillion";
												} else if (abs < Math.pow(10, 12)
														&& abs >= Math.pow(10, 9)) {
													// billion
													number = (number / Math.pow(10, 9))
															.toFixed(1)
															+ "Billion";
												} else if (abs < Math.pow(10, 9)
														&& abs >= Math.pow(10, 6)) {
													// million
													number = (number / Math.pow(10, 6))
															.toFixed(1)
															+ "Million";
												} else if (abs < Math.pow(10, 6)
														&& abs >= Math.pow(10, 3)) {
													// thousand
													number = (number / Math.pow(10, 3))
															.toFixed(1)
															+ "Thousand";
												}
												return number;
											}
										};
									});
			
			
		});
