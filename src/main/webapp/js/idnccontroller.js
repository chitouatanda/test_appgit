define(['angular', 'angular-ui-select', 'user-service','idnc-service','partner-service','notification-service','addnewidnc'], function(angular) {
	angular.module('IdncController', ['user-service', 'ui.select', 'idnc-service', 'partner-service', 'notification-service'])
		.controller('IdncController', [
		                                      '$scope', 
		                                      '$rootScope',
		                                      '$location',
		                                      '$modal',
		                                      '$state',
		                                      'idncService',
		                                      'idncSaveService',
		                                      'PartnerService',
		                                      'NotificationService',
		                                      'user',
		                                      '$filter',
		                                      function ($scope, $rootScope, $location, $modal, $state, idncService, idncSaveService,  PartnerService, NotificationService, user, $filter) {
		                                    	  
		                                    	  //$scope.idnclist = [];
		                                    	  $scope.main = {
											                page: 0,
											                take: 10,
											                direction: "ASC",
											                sort:"",
											                start: 1,
											                end: 10,
		                                    	  };
		                                    	  
												  $scope.userRoleSupervisor = $filter('filter')(user.roles, "SUPERVISOR")[0];
												  
												  if (user != null && user != undefined && user.roles != null && user.roles != undefined && !user.roles.includes("SUPERVISOR")) {
														$state.go('login');
														return;
												  }	
		                                    	  
		                                    	  $scope.modal = {pnum : ''};
		                                    	  $scope.loading = true;
		                                    	  $scope.idncTotalRecord = 0;
		                                    	  idncService.get({searchstring: angular.toJson({"phoneNumber" : "", "partnerId":"", "page" : 0, "size" : 10})}).$promise.then(function(data){
		                                        	  $scope.idnclist = data.dncNumbers;
		                                        	  $scope.idncTotalRecord = data.dncNumberCount;
		                                        	  $scope.main.pages = parseInt(data.dncNumberCount/$scope.main.take);
		                                        	  if($scope.idncTotalRecord < $scope.main.end) 
	  								                    {
	  								                    	$scope.main.end = $scope.idncTotalRecord;
	  								                    }
		                                        	  $scope.loading = false;
		                                    	  },function(error){
		                                    		  $scope.loading = false;
		                                    	  });
		                                    	  $scope.filterIdncList = function(){
			  	                                  		$scope.loading = true;
			  	                                  		
			  	                                  		  ///"teamIds":$scope.allTeamIds,"campaignId":"54dc9e86920d1f8d26a39d58","direction":"DESC","sort":"prospectCall.callStartTime","toDate":"2015-03-02","fromDate":"",
			  	                                  if(($scope.phoneNo == "" || $scope.phoneNo == undefined) && ($scope.partnerId == "" || $scope.partnerId == undefined)) {
			  	                                	var filterObject = {"phoneNumber" : "", "partnerId" : "", "page" : $scope.main.page, "size" : $scope.main.take};
			  	                                  } else if (($scope.phoneNo != "" || $scope.phoneNo != undefined) && ($scope.partnerId == "" || $scope.partnerId == undefined)) {
			  	                                	var filterObject = {"phoneNumber" : $scope.phoneNo, "partnerId" : "", "page" : $scope.main.page, "size" : $scope.main.take};
			  	                                  } else if (($scope.phoneNo == "" || $scope.phoneNo == undefined) && ($scope.partnerId != "" || $scope.partnerId != undefined)) {
			  	                                	var filterObject = {"phoneNumber" : "", "partnerId" : $scope.partnerId, "page" : $scope.main.page, "size" : $scope.main.take};
			  	                                  } else {
			  	                                	  var filterObject = {"phoneNumber" : $scope.phoneNo, "partnerId" : $scope.partnerId, "page" : $scope.main.page, "size" : $scope.main.take};
			  	                                  }
			  	                                  		
			  	                                  		
			  	                                  idncService.query({searchstring: angular.toJson(filterObject)}, null).$promise
			  												.then(
			  														function(data) { 

			  															$scope.idnclist = data.dncNumbers;
			  			                                        	    $scope.idncTotalRecord = data.dncNumberCount;
			  															$scope.main.pages = parseInt(data.dncNumberCount/$scope.main.take);
			  															$scope.loading = false;

			  														},
			  														function(error) {
			  															///alert('Input is not validated');
			  															$scope.loading = false;
			  														});
			  	                                  		  
			  	                                  	  }  
		                                        	  
		                                        	  
		                                        	 $scope.nextPage = function() {
		  								            	console.log($scope.main.pages);
		  								                if ($scope.main.page < $scope.main.pages) {
		  								                    $scope.main.page++;
		  								                    $scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
		  								                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
		  								                    if($scope.idncTotalRecord < $scope.main.end) 
		  								                    {
		  								                    	$scope.main.end = $scope.idncTotalRecord;
		  								                    }
		  								                    
		  								                    $scope.filterIdncList();
		  								                }
		  								            };
		  								            
		  								            $scope.previousPage = function() {
		  								                if ($scope.main.page > 0) {
		  								                    $scope.main.page--;
		  								                    $scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
		  								                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
		  								                    if($scope.idncTotalRecord < $scope.main.end) 
		  								                    {
		  								                    	$scope.main.end = $scope.idncTotalRecord;
		  								                    }
		  								                    $scope.filterIdncList();
		  								                }
		  								            };
		  	                                  	  
		  	                                  	  $scope.showrows = function(){
		  	                                  		$scope.main.page = 0;
		  	                                  		$scope.main.pages = parseInt($scope.idncTotalRecord/$scope.main.take);
		  	                                  		$scope.main.start = parseInt(($scope.main.page*$scope.main.take) + 1);
		  						                    $scope.main.end = parseInt(($scope.main.page*$scope.main.take) + $scope.main.take);
		  						                    if($scope.idncTotalRecord < $scope.main.end) {
		  						                    	$scope.main.end = $scope.idncTotalRecord;
		  						                    }
		  	                                  		$scope.filterIdncList();
		  	                                  	  }
		  	                                  	  
		  	                                  	 $scope.search = function(){
			  	                                  	$scope.main.page = 0;
			  	                                    $scope.filterIdncList();
		  	                                  	 }	
			  	                                 $scope.addNewIdnc  =  function(){
			  	                                	$scope.modal.pnum = '';
			  	                                	$scope.modal.note = '';
			                                  		var modalInstance = $modal.open({
														scope : $scope,
														templateUrl : '/partials/addnewidnc.html',
														controller : AddNewIdncModalInstanceCtrl,
														windowClass : 'addnewidnc',
															resolve: {
																user : function () {
																	return user;
																}
															}
													});
			  	                                 } 
			                                  	
			                                 
	}]).directive('myEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.myEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	});
});
