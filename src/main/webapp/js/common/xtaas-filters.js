define(['angular'], function(angular) {
    angular.module('xtaas-filters', [])
    .filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    })
    .filter('pascal', function () {
        return function(s) {
        	return s.replace(/(\w)(\w*)/g, function(g0,g1,g2) { 
        		return g1.toUpperCase() + g2.toLowerCase();
        	});
		}
    })
    .filter('excerpt', function() {
    	return function(s, length) {
    		if (angular.isDefined(s)) {
    			return s.substring(0, length);
    		}
    	}
    });
});