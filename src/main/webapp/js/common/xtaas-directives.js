define(['angular'], function(angular) {
    angular.module('xtaas-directives', [])
    .directive('rightscroll', function() {
    	return {
    		restrict: 'A',
    		link: function (scope, element, attrs) { 
    			var update = function () {
                    element[0].scrollLeft = element[0].scrollWidth
                };
                scope.$watch(attrs.ngModel, function () {
                    update();
                });
    		}
    	}
    })
})
