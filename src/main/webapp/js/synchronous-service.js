define(['angular', 'angular-resource'], function(angular) {
    angular.module('synchronous-service', ['ngResource'])
    .service('synchronousService', [function () {
   	    var serviceMethod = function (url, requestType, data) {
   	        var request;
   	        if (window.XMLHttpRequest) {
   	            request = new XMLHttpRequest();
   	        } else if (window.ActiveXObject) {
   	            request = new ActiveXObject("Microsoft.XMLHTTP");
   	        } else {
   	            throw new Error("Your browser don't support XMLHttpRequest");
   	        }

   	        request.open(requestType, url, false);//the false is for making the call synchronous
   	        request.setRequestHeader("Content-type", "application/json");
   	        request.send(data);

   	        if (request.status === 200) {
   	            return request.responseText;
   	        }
   	    };
   	    return serviceMethod;
   	}]);
});

