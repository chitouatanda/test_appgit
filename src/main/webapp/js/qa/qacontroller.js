define(['angular', 'angular-ui-select', 'qa/qa-service', 'angucomplete-alt', 'pagination', 'teamnotice-service', 'notification-service', 'angular-localStorage', 'angular-timeZone','d3graph','xtaas-audio-player','d3','nv-d3','underscore', 'login-service'], function(angular) {
    angular.module('QaController', ['ui.select', 'qa-service', 'angucomplete-alt', 'angularUtils.directives.dirPagination', 'teamnotice-service', 'notification-service', 'ngStorage', 'nvd3ChartDirectives','xtaasaudioplayer', 'login-service'])
        .controller('QaController', [
            '$scope',
            '$rootScope',
            '$sce',
            '$stateParams',
            '$window',
            '$modal',
            '$state',
            '$timeout',
            '$filter',
            'searchTeamService',
            'searchQaCampaignAgentService',
            'searchQaCampaignService',
            'qaProspectListService',
            'NoticeService',
            'NotificationService',            
            '$localStorage',
            'user',
            'dispositionStatusMetricsService',
            'prospectCallStatusMetricsService',
            'scoredRateMetricsService',
            'qaMetricsService',
            'qaDownloadReportService',
            'GetOrganizationService',
            'ClientLeadReportService',
            'DemandshoreLeadReportService',
            'qaLeadSearchService',
            'qaFirstTimeLoginService',
            'CampaignMetaDataService',
            '$q',
            '$http',
            'LoginService',
            '$location',
            function($scope, $rootScope, $sce, $stateParams, $window,
                $modal, $state, $timeout, $filter, searchTeamService, searchQaCampaignAgentService, searchQaCampaignService, qaProspectListService, NoticeService, 
                NotificationService, $localStorage, user, dispositionStatusMetricsService, prospectCallStatusMetricsService, scoredRateMetricsService,qaMetricsService, qaDownloadReportService, GetOrganizationService, ClientLeadReportService, DemandshoreLeadReportService, qaLeadSearchService,qaFirstTimeLoginService, CampaignMetaDataService, $q, $http, LoginService,$location) {

                $scope.loading = true;
                $scope.currentPage = 1;
                $scope.showDisposition = false;
                $scope.showScoreAlert = false;
                $scope.currentRecordingUrl = "";
                $scope.searchLead = "";
                $scope.qaCampaign = {};
                $scope.disableReportDownloadButton = false;
                if($localStorage.validUser == null || $localStorage.validUser == undefined || $localStorage.validUser == '') {
                    var temp = LoginService.logout();
                    temp.then(function() {
                        $rootScope.isUserLoggedIn = false;				
                        $localStorage.$reset();	
                        $location.path('/login');
                    });
                }
                //Get client time-zone
                var tz = jstz.determine();
                var timeZone = "US/Pacific"; // tz.name(); // returns "America/Los Angeles"   
                
                $scope.notice = NoticeService.get({
                    userId: user.id
                });


               /*  ############### Date :- 10/02/2021 Get the organization of loggedInUser and campaign metadata  START ############### */
               $scope.loggedInUserOrganization = {};
               $scope.showCallBackOption = false;
               $q.all([$http.get(`/spr/organization/${user.organization}`, null)]).then(function(values) {
                   if (values[0].data != undefined && values[0].data != null) {
                       $scope.loggedInUserOrganization = values[0].data;
                   }
                   if ($scope.loggedInUserOrganization != null && $scope.loggedInUserOrganization != undefined && 
                        $scope.loggedInUserOrganization.downloadCallBackReport != null && $scope.loggedInUserOrganization.downloadCallBackReport != undefined) {
                           $scope.showCallBackOption = $scope.loggedInUserOrganization.downloadCallBackReport;
                   }
               })
               // /*  ############### Date :- 10/02/2021 Get the organization of loggedInUser and campaign metadata  End ############### */


                /** DATE :- 03/10/2019
                 *  If Logged in user role is "QA" (Primary) then only user will be able to access the page
                 *  otherwise redirected to login page. 
                 */
                $scope.isPrimaryQaLoggedIn = function() {
                    var role = $filter('filter')(user.roles, "QA")[0];
                    if (role != 'QA') {
                       $state.go('login');
                       return;
                    }
                }
                $scope.isPrimaryQaLoggedIn();
                // END

                $scope.qa = {
                    teamList: [],
                    campaignList: [],
                    agentList: [],
                    dispositionStatusList: [],
					prospectCallStatusList: [],
                    scoredRateList: [],
                    leadStatusList: []
                };

                 /* 
                 * START  
                 * DATE :- 20-05-2020 
                 * By default show selected todays date on UI and dont give any service call. */
                var today = new Date();
                $scope.qa.startDate = $filter('date')(today, 'yyyy-MM-dd');
                $scope.qa.endDate = $filter('date')(today, 'yyyy-MM-dd');
                $scope.sDateSelectedText = $scope.qa.startDate;
                $scope.eDateSelectedText = $scope.qa.endDate;                        
                $scope.sDateSelected = true;
                // END


                $scope.successLeadStatusList = [
                    "NOT_SCORED",
                    "QA_ACCEPTED",
                    "QA_REJECTED",
                    "QA_RETURNED",
                    "ACCEPTED",
                    "REJECTED"
                ]

                /**
                 *  DATE :- 02/01/20 
                 *  Added below options in the leadstatus filter to 
                 *  fetch answering machine records.
                 */
                $scope.ansMachineLeadStatusList = [
                    "ANSWERMACHINE",
                    "GATEKEEPER_ANSWERMACHINE",
                    "CALLBACK"
                ]

                $scope.failureLeadStatusList = [
                    "AUTO",
                    "DNCL",
                    "PROSPECT_UNREACHABLE",
                    "FAILED_QUALIFICATION",
                    "NO_CONSENT","DNRM",
                    "NOTINTERESTED",
                    "OTHER",
                    "OUT_OF_COUNTRY"    
                ]

               
            if ($scope.showCallBackOption) {
                $scope.qa.leadStatusList = [
                {
                    "key":"SUCCESS",
                    "value":"SUCCESS"
                },
                {
                    "key": "NOT_SCORED",
                    "value": "NOT_SCORED"
                },
                {
                    "key": "QA_ACCEPTED",
                    "value": "QA_ACCEPTED"
                },
                {
                    "key": "QA_REJECTED",
                    "value": "QA_REJECTED"
                },
                {
                    "key": "QA_RETURNED",
                    "value": "QA_RETURNED"
                },
                {
                    "key": "ACCEPTED",
                    "value": "ACCEPTED"
                },
                {
                    "key": "REJECTED",
                    "value": "REJECTED"
                },
                {
                    "key":"ANS M/C",
                    "value":"ANS M/C"
                },
                {
                    "key": "GK_ANSWERMACHINE",
                    "value": "GK_ANSWERMACHINE"
                },
                {
                    "key": "ANSWERMACHINE",
                    "value": "ANSWERMACHINE"
                },
                {
                    "key": "NOT_DISPOSED",
                    "value": "NOT_DISPOSED"
                },
                {
                    "key": "CALLBACK",
                    "value": "CALLBACK"
                },
                {
                    "key":"FAILURE",
                    "value":"FAILURE"
                },
                {
                    "key": "AUTO",
                    "value": "AUTO"
                },
                {
                    "key": "DNCL",
                    "value": "DNCL",
                    "toolTip":"Do Not Call List"
                },
                {
                    "key": "PROSPECT_UNREACHABLE",
                    "value": "PROSPECT_UNREACHABLE"
                },
                {
                    "key": "FAILED_QUALIFICATION",
                    "value": "FAILED_QUALIFICATION"
                },
                {
                    "key": "NO_CONSENT",
                    "value": "NO_CONSENT"
                },
                {
                    "key": "DNRM",
                    "value": "DNRM",
                    "toolTip":"Do Not Record Me"
                },
                {
                    "key": "NOTINTERESTED",
                    "value": "NOTINTERESTED"
                },
                {
                    "key": "OTHER",
                    "value": "OTHER"
                },
                {
                    "key": "OUT_OF_COUNTRY",
                    "value": "OUT_OF_COUNTRY"
                }
                ]
            } else {
                $scope.qa.leadStatusList = [
                    {
                        "key":"SUCCESS",
                        "value":"SUCCESS"
                    },
                    {
                        "key": "NOT_SCORED",
                        "value": "NOT_SCORED"
                    },
                    {
                        "key": "QA_ACCEPTED",
                        "value": "QA_ACCEPTED"
                    },
                    {
                        "key": "QA_REJECTED",
                        "value": "QA_REJECTED"
                    },
                    {
                        "key": "QA_RETURNED",
                        "value": "QA_RETURNED"
                    },
                    {
                        "key": "ACCEPTED",
                        "value": "ACCEPTED"
                    },
                    {
                        "key": "REJECTED",
                        "value": "REJECTED"
                    },
                    {
                        "key":"ANS M/C",
                        "value":"ANS M/C"
                    },
                    {
                        "key": "GK_ANSWERMACHINE",
                        "value": "GK_ANSWERMACHINE"
                    },
                    {
                        "key": "ANSWERMACHINE",
                        "value": "ANSWERMACHINE"
                    },
                    {
                        "key": "NOT_DISPOSED",
                        "value": "NOT_DISPOSED"
                    },
                    {
                        "key":"FAILURE",
                        "value":"FAILURE"
                    },
                    {
                        "key": "AUTO",
                        "value": "AUTO"
                    },
                    {
                        "key": "DNCL",
                        "value": "DNCL",
                        "toolTip":"Do Not Call List"
                    },
                    {
                        "key": "PROSPECT_UNREACHABLE",
                        "value": "PROSPECT_UNREACHABLE"
                    },
                    {
                        "key": "FAILED_QUALIFICATION",
                        "value": "FAILED_QUALIFICATION"
                    },
                    {
                        "key": "NO_CONSENT",
                        "value": "NO_CONSENT"
                    },
                    {
                        "key": "DNRM",
                        "value": "DNRM",
                        "toolTip":"Do Not Record Me"
                    },
                    {
                        "key": "NOTINTERESTED",
                        "value": "NOTINTERESTED"
                    },
                    {
                        "key": "OTHER",
                        "value": "OTHER"
                    },
                    {
                        "key": "OUT_OF_COUNTRY",
                        "value": "OUT_OF_COUNTRY"
                    }
                    ]
            }

                $scope.agent = {};
                $scope.graphData = [];
                $scope.agentSearchBy = {};
                $scope.leadStatusSearchBy = {};
                $scope.campaignSearchBy = {};
                $scope.allPartnerIds = [];

                /* Duration dropdown values */
                $scope.durationList = [
                    "Under 30 secs",
                    "Up to 1 min",
                    "1 min to 3 mins",
                    "Over 10 mins",
                    "Over 30 mins"
                ];


                //Show Rows drop down
                $scope.totalPageSize = [{
                    id: 10,
                    name: '10 rows'
                }, {
                    id: 20,
                    name: '20 rows'
                }, {
                    id: 30,
                    name: '30 rows'
                }, {
                    id: 40,
                    name: '40 rows'
                }];
                // $scope.pageSize = 10;
                $scope.pageSize = $scope.totalPageSize[3].id;

                

                /* Quick date dropdown values */
                $scope.quickDateList = [
                    "Today",
                    "Yesterday",
                    "Current Week",
                    "Last Week",
                    "Last Two Weeks",
                    "Month To Date",
                    "Last Month"
                ];

                /* Quick time dropdown values */
                $scope.quickTimeList = [
                    "In Past hour",
                    "In Past 4 hours",
                    "In Past 8 hours"
                ];
                	
                

                /* Status dropdown values */
                $scope.statusList = [
                    "Not Scored",
                    "In Progress",
                    "Scored"
                ];

                /* Start hour dropdown values */
                $scope.startHourList = [{
                    "key": "0",
                    "value": "12 AM"
                }, {
                    "key": "1",
                    "value": "01 AM"
                }, {
                    "key": "2",
                    "value": "02 AM"
                }, {
                    "key": "3",
                    "value": "03 AM"
                }, {
                    "key": "4",
                    "value": "04 AM"
                }, {
                    "key": "5",
                    "value": "05 AM"
                }, {
                    "key": "6",
                    "value": "06 AM"
                }, {
                    "key": "7",
                    "value": "07 AM"
                }, {
                    "key": "8",
                    "value": "08 AM"
                }, {
                    "key": "9",
                    "value": "09 AM"
                }, {
                    "key": "10",
                    "value": "10 AM"
                }, {
                    "key": "11",
                    "value": "11 AM"
                }, {
                    "key": "12",
                    "value": "12 PM"
                }, {
                    "key": "13",
                    "value": "01 PM"
                }, {
                    "key": "14",
                    "value": "02 PM"
                }, {
                    "key": "15",
                    "value": "03 PM"
                }, {
                    "key": "16",
                    "value": "04 PM"
                }, {
                    "key": "17",
                    "value": "05 PM"
                }, {
                    "key": "18",
                    "value": "06 PM"
                }, {
                    "key": "19",
                    "value": "07 PM"
                }, {
                    "key": "20",
                    "value": "08 PM"
                }, {
                    "key": "21",
                    "value": "09  PM"
                }, {
                    "key": "22",
                    "value": "10 PM"
                }, {
                    "key": "23",
                    "value": "11 PM"
                }];

                /* End hour dropdown values */
                $scope.endHourList = $scope.startHourList;
                
                
                /* Hide message box */
                $timeout(function () { 
                	$rootScope.saveData = false;
                	$rootScope.submitData = false;
                }, 5000);
                
                
                /* End time dropdown values */
                $scope.setEndTimeList = function() {
                    $scope.endHourList = [];
                    console.log($scope.agentSearchBy.startTime);
                    var flag = false;
                    angular.forEach($scope.startHourList, function(item, key) {
                        if ($scope.agentSearchBy.startTime < item.key) {
                            $scope.endHourList.push({
                                "key": item.key,
                                "value": item.value
                            });
                        }

                    });
                }

                // $scope.getQaMetrics = function(qaMetricsCriteria){
	            // 	qaMetricsService.query({searchString : angular.toJson(qaMetricsCriteria)}, null).$promise.then(function(qaMetricsData) {
	            // 		$scope.qa.qaMetricsList = qaMetricsData;
	            // 	});
                // }

              $scope.calGraphApi = function(searchByDispositionStatusObject, searchByProspectCallStatusObject) {
            	  
            	  searchByDispositionStatusObject.clause = "ByDispositionStatus";
            	  searchByProspectCallStatusObject.clause = "ByProspectCallStatus";
            	  
					if(searchByDispositionStatusObject != undefined && searchByProspectCallStatusObject != undefined) {
						//Service for ByDispositionStatus					
						dispositionStatusMetricsService.query({searchString : angular.toJson(searchByDispositionStatusObject)}, null).$promise.then(function(disStatusListData) {						
								$scope.qa.dispositionStatusList = disStatusListData;							
								$scope.graphData = [];
								
								if (disStatusListData.COUNT_FAILURE_PROSPECTCALL != undefined && disStatusListData.COUNT_FAILURE_PROSPECTCALL != null && disStatusListData.COUNT_FAILURE_PROSPECTCALL != '' ){
									$scope.graphData.push({"key":'Failure',"y":disStatusListData.COUNT_FAILURE_PROSPECTCALL})	
								} else {
									$scope.graphData.push({"key":'Failure',"y":0})
								}
								
								if (disStatusListData.COUNT_SUCCESS_PROSPECTCALL != undefined && disStatusListData.COUNT_SUCCESS_PROSPECTCALL != null && disStatusListData.COUNT_SUCCESS_PROSPECTCALL != '' ){
									$scope.graphData.push({"key":'Success',"y":disStatusListData.COUNT_SUCCESS_PROSPECTCALL})	
								} else {
									$scope.graphData.push({"key":'Success',"y":0})
								}
								
								if (disStatusListData.COUNT_CALLBACK_PROSPECTCALL != undefined && disStatusListData.COUNT_CALLBACK_PROSPECTCALL != null && disStatusListData.COUNT_CALLBACK_PROSPECTCALL != '' ){
									$scope.graphData.push({"key":'Callback',"y":disStatusListData.COUNT_CALLBACK_PROSPECTCALL})	
								} else {
									$scope.graphData.push({"key":'Callback',"y":0})
								}
								
								if (disStatusListData.COUNT_DIALERCODE_PROSPECTCALL != undefined && disStatusListData.COUNT_DIALERCODE_PROSPECTCALL != null && disStatusListData.COUNT_DIALERCODE_PROSPECTCALL != '' ){
									$scope.graphData.push({"key":'Dialercode',"y":disStatusListData.COUNT_DIALERCODE_PROSPECTCALL})	
								} else {
									$scope.graphData.push({"key":'Dialercode',"y":0})
								}
						       
								$scope.newvar = [];
								$timeout(function () {								
									$scope.newvar = $scope.graphData;							
							    }, 100);							
						});
														
						//Service for ByProspectCallStatus					
						prospectCallStatusMetricsService.query({searchString : angular.toJson(searchByProspectCallStatusObject)}, null).$promise.then(function(prosCallStatusListData) {						
								$scope.qa.prospectCallStatusList = prosCallStatusListData;
						});
					}					
				}
                
                
                $scope.agentSearchBy = {
                    "selectedTeam": null
                };

                $scope.userRoleAgent = $filter('filter')(user.roles, "AGENT")[0];
                
                $scope.getLastEightHours = function () {
                	var today = Date.now();
					var startday = today - (8 * 3600000);
                    $scope.qa.startMinute = $filter('date')(startday, 'mm');
                    $scope.qa.endMinute = $filter('date')(today, 'mm');
                    $scope.qa.startHour = $filter('date')(startday, 'HH');
                    $scope.qa.endHour = $filter('date')(today, 'HH');
					
                    if($scope.qa.startHour < 10) {
                    	$scope.qa.startHour = $scope.qa.startHour.replace(/^0+/, '');    	                    		
                    }
                    if($scope.qa.endHour < 10) {
                    	$scope.qa.endHour = $scope.qa.endHour.replace(/^0+/, '');    	                    		
                    }
                }
                
                $scope.getDefaultDate = function (arr, pkey) {
                	var retVal;
                	angular.forEach(arr,  function(value, key){    							
                    	if(key == pkey) {
                    		retVal = value.value;
                    	}
					});
                	return retVal;
                }

                $scope.getDetailsWithoutProspects = function() {
                    $scope.loading = true;
                    $scope.readFilterCriteriaFromLocalstorage();
	                    if ($scope.main != null && $scope.main != undefined) {
							$scope.main = {
			                    page: 0,
			                    take: 5,
			                    direction: "DESC",
			                    sort: "prospectCall.callStartTime",
			                    start: 1,
			                    end: 10,
			                };
						}
						var filterObject = {
	                        "clause": "ByQa",
	                        "teamIds": $scope.allTeamIds,
	                        "campaignIds": $scope.qaCampaigns,
	                        "agentIds": $scope.agentIds,
	                        "sort": $scope.sortColumn,
	                        "direction": $scope.direction,
							"page": $scope.currentPage - 1,
	                        "size": $scope.pageSize,
	                        "toDate": $scope.qa.endDate,
	                        "fromDate": $scope.qa.startDate,
	                        "callDurationRangeStart": $scope.qa.startCallDuration,
	                        "callDurationRangeEnd": $scope.qa.endCallDuration,
	                        "startHour": $scope.qa.startHour,
	                        "endHour": $scope.qa.endHour,
	                        "startMinute": $scope.qa.startMinute,
                            "endMinute": $scope.qa.endMinute,
                            "leadStatus": $scope.leadStatuses,
	                        "disposition":["SUCCESS"],
	                        "clientId": $scope.agentSearchBy.selectedClientId,
	                        "prospectCallStatus": $scope.agentSearchBy.qastatus,
                            "timeZone": timeZone,
                            "qaSearchString": $scope.searchLead
	                    };
						qaFirstTimeLoginService.get({
							searchstring: angular.toJson(filterObject)
		                }).$promise.then(function(data) {
                            $scope.prospectCallList = data;
		                    $scope.totalRecordCount = data.prospectCallCount;
		                    
		                    /* DATE:16/01/2017	REASON:Added to fetch campaignList and agentList from prospectList call */
                            $scope.qaCampaign = data.campaignSearchResult;
                            $scope.qa.campaignList = [];
                            $scope.qa.agentList = [];
                            if (data.campaignSearchResult != null && data.campaignSearchResult != undefined) {
								angular.forEach(data.campaignSearchResult.campaignsList,  function(campaignListValue, campaignListkey){
									$scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});
									if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
										$scope.allPartnerIds.push(campaignListValue.organizationId);
									}
								});
                            }
                            $scope.qa.campaignList.sort(function(a, b) {
                                if (a.value.toLowerCase() < b.value.toLowerCase()) return -1;
                                if (a.value.toLowerCase() > b.value.toLowerCase()) return 1;
                                return 0;
                            });
                            if (data.agentSearchResult != null && data.agentSearchResult != undefined) {
	    						angular.forEach(data.agentSearchResult,  function(agent, index) {
	    							$scope.qa.agentList.push({"key":agent.id,"value":agent.id+' - '+agent.firstName+' '+agent.lastName})
	    						});
                            }
    						$localStorage.campaignList = JSON.stringify($scope.qa.campaignList);
    						$localStorage.agentList = JSON.stringify($scope.qa.agentList);
    						
		                    $scope.main.pages = parseInt($scope.prospectCallList.prospectCallCount / $scope.main.take);
		                    $scope.loading = false;
		                    saveDataInLocalStorage();
		                }, function(error) {
		                    $scope.loading = false;
		                });
                }
                
                $scope.readFilterCriteriaFromLocalstorage = function() {
                	if ($localStorage.filterCriteriaObj != undefined && $localStorage.filterCriteriaObj != null && $localStorage.filterCriteriaObj != '') {
                		$scope.filterCriteriaObj = JSON.parse($localStorage.filterCriteriaObj);
    					$scope.agentSearchBy.selectedCampaign = $scope.filterCriteriaObj.selectedCampaignId;
    					$scope.agentSearchBy.quickTime = $scope.filterCriteriaObj.quickTime;
    					$scope.agentSearchBy.selectedAgentMulti = $scope.filterCriteriaObj.agentIds;
    					$scope.agentSearchBy.callId = $scope.filterCriteriaObj.callId;
    					$scope.agentSearchBy.selectedTeam = $scope.filterCriteriaObj.selectedTeamId;						
    					$scope.agentSearchBy.selectedClientId = $scope.filterCriteriaObj.selectedClientId;
    					$scope.qa.startDate = $scope.filterCriteriaObj.startDate;
    					$scope.qa.endDate = $scope.filterCriteriaObj.endDate;
    					$scope.agentSearchBy.startTime = $scope.filterCriteriaObj.startTime;
    					$scope.agentSearchBy.endTime = $scope.filterCriteriaObj.endTime;
                        $scope.agentSelectedText = $scope.filterCriteriaObj.agentSelectedText;
                        $scope.leadStatusSelectedText = $scope.filterCriteriaObj.leadStatusSelectedText;
                        $scope.campaignSelectedText = $scope.filterCriteriaObj.campaignSelectedText;
    					$scope.agentSearchBy.selectedDispositionStatus = $scope.filterCriteriaObj.dispositionStatus;
    					//$scope.agentSearchBy.qastatus=null;
    					$scope.agentSearchBy.qastatus = $scope.filterCriteriaObj.qastatus;
    					
    					/* START
    					 * DATE : 19/06/2017
    					 * REASON : fetching data from localStorage & storing in $scope for pagination to work properly */
    					$scope.allTeamIds = $scope.filterCriteriaObj.allTeamIds;
                        $scope.agentIds = $scope.filterCriteriaObj.agentIds;
                        $scope.leadStatuses = $scope.filterCriteriaObj.leadStatuses
                        $scope.qaCampaigns = $scope.filterCriteriaObj.qaCampaigns
    					
    					$scope.qa.startCallDuration = $scope.filterCriteriaObj.startCallDuration;
    					$scope.qa.endCallDuration = $scope.filterCriteriaObj.endCallDuration;
    					
    					$scope.alldisposition = $scope.filterCriteriaObj.alldisposition;
    					$scope.allsubstatus = $scope.filterCriteriaObj.allsubstatus;
    					
    					$scope.qa.startHour = $scope.filterCriteriaObj.startHour;
    					$scope.qa.endHour = $scope.filterCriteriaObj.endHour;
    					$scope.qa.startMinute = $scope.filterCriteriaObj.startMinute;
    					$scope.qa.endMinute = $scope.filterCriteriaObj.endMinute;
    					
    					$scope.allprospectCallId = $scope.filterCriteriaObj.allprospectCallId;
    					/* END */
    					
    					if ($scope.filterCriteriaObj.subStatus != null && $scope.filterCriteriaObj.subStatus != '') {
    						$scope.agentSearchBy.selectedDispositionStatus = $scope.filterCriteriaObj.dispositionStatus+'-'+$scope.filterCriteriaObj.subStatus;
                        }

                        if ($scope.filterCriteriaObj.leadStatusSearchBy == undefined) {
                            $scope.filterCriteriaObj.leadStatusSearchBy = {};
                        }
                        if ($scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti2 != undefined || $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti != null) {
                            $scope.leadStatusSearchBy.selectedStatusMulti2 = $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti2;
                            $scope.leadStatusSearchBy.selectedStatusMulti = $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti;
                            if ($scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti2.length > 0) {
                                $scope.leadStatusSelected = true;
                            }
                        } 

                        if ($scope.filterCriteriaObj.campaignSearchBy == undefined) {
                            $scope.filterCriteriaObj.campaignSearchBy = {};
                        }
                        if ($scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti2 != undefined || $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti != null) {
                            $scope.campaignSearchBy.selectedCampaignMulti2 = $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti2;
                            $scope.campaignSearchBy.selectedCampaignMulti = $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti;
                        } 

                        $scope.agentSearchBy.duration = $scope.filterCriteriaObj.selectedDuration;  					

    					//Set filter criteria
                        $scope.callIdSelectedText = $scope.filterCriteriaObj.callId;   
                        if($scope.callIdSelectedText != undefined && $scope.callIdSelectedText != "") {
                        	$scope.callIdSelected = true;	
                        }                        

                        if($scope.agentSelectedText != undefined && $scope.agentSelectedText != "") {
                        	$scope.agentSelected = [];
                        	angular.forEach($scope.agentSelectedText[0], function(item, key) {
                        		$scope.agentSelected[key] = true;
                            }); 
                            $scope.teamSelected = true;                		
                        }

                        if($scope.leadStatusSelectedText != undefined && $scope.leadStatusSelectedText != "") {
                        	$scope.statusSelected = [];
                        	angular.forEach($scope.leadStatusSelectedText[0], function(item, key) {
                        		$scope.statusSelected[key] = true;
                            });                 		
                        }

                        if($scope.campaignSelectedText != undefined && $scope.campaignSelectedText != "") {
                        	$scope.qaCampaignSelected = [];
                        	angular.forEach($scope.campaignSelectedText[0], function(item, key) {
                        		$scope.qaCampaignSelected[key] = true;
                            });                 		
                            $scope.campaignSelected = true;
                        }
                        
                        $scope.sDateSelectedText = $scope.filterCriteriaObj.startDate;
                        if($scope.sDateSelectedText != undefined && $scope.sDateSelectedText != "") {
                         $scope.sDateSelected = true;
                        }
                        
                        $scope.eDateSelectedText = $scope.filterCriteriaObj.endDate;
                      
                        if($scope.eDateSelectedText != undefined && $scope.eDateSelectedText != "") {
                         $scope.sDateSelected = true;
                        }
                        
                        
                        if($scope.filterCriteriaObj.quickTime == "In Past 8 hours") {							
                        	$scope.getLastEightHours();    	                    
                        	$scope.sTimeSelectedText = $scope.getDefaultDate($scope.startHourList, $scope.qa.startHour);
                        	$scope.eTimeSelectedText = $scope.getDefaultDate($scope.endHourList, $scope.qa.endHour);    	                    
    	                    
                            $scope.agentSearchBy.startTime = $scope.qa.startHour;
                            $scope.agentSearchBy.endTime = $scope.qa.endHour;
                            $scope.sDateSelected = true;
                            
    					} else {
    						if($scope.filterCriteriaObj.startTime != null) {
    						  $scope.sTimeSelectedText = $filter('filter')($scope.startHourList, $scope.filterCriteriaObj.startTime)[0].value;
    						  $scope.agentSearchBy.startTime = $scope.qa.startHour;
    						  $scope.sDateSelected = true;
    						}
    						if($scope.filterCriteriaObj.startTime != null) {
    							$scope.eTimeSelectedText = $filter('filter')($scope.startHourList, $scope.filterCriteriaObj.endTime)[0].value;
    							$scope.agentSearchBy.endTime = $scope.qa.endHour;
    							$scope.sDateSelected = true;
    						}   
    					}
                        
                        $scope.disposeSelectedText = $scope.filterCriteriaObj.dispositionStatus;
                        if($scope.disposeSelectedText != undefined && $scope.disposeSelectedText != "") {
                         $scope.disposeSelected = true;
                        }
                        
                        $scope.durSelectedText = $scope.filterCriteriaObj.selectedDuration;
                        if($scope.durSelectedText != undefined && $scope.durSelectedText != "") {
                            $scope.durSelected = true;
                        }

                       
                        $scope.setProspCalltxt =$scope.filterCriteriaObj.prospCalltxt;
                        if($scope.setProspCalltxt != undefined && $scope.setProspCalltxt != "") {
                        	//$scope.sDateSelected = true;
                        }
                        
                        $scope.setDispStatustxt =$scope.filterCriteriaObj.dispStatustxt;
                        if($scope.setDispStatustxt != undefined && $scope.setDispStatustxt != "") {
                        	$scope.sDateSelected = true;
                        }
                        
    					
    					//Pagination Values
    					$scope.currentPage = $scope.filterCriteriaObj.currentPage;
    					$scope.pageSize = $scope.filterCriteriaObj.pageSize;
    					$scope.currentPageLength = $scope.filterCriteriaObj.currentPageLength;
    					$scope.totalRecordCount = $scope.filterCriteriaObj.totalRecordCount;
    					$scope.sortColumn = $scope.filterCriteriaObj.sortColumn;
						$scope.direction = $scope.filterCriteriaObj.direction;
                	}
				}
                
              //TODO:- Create service for getting the data from local storage
				//Get data from local storage if it is present else call API
				if ($localStorage.prospectCallList != null && $localStorage.prospectCallList != '') {//check if data is present in local storage or not
					$scope.prospectCallList = JSON.parse($localStorage.prospectCallList);
					
					if ($localStorage.campaignList != null && $localStorage.campaignList != '') {
                        $scope.qaCampaign.campaignsList = JSON.parse($localStorage.campaignList);
                        $scope.qa.campaignList = JSON.parse($localStorage.campaignList);   
                        $scope.qa.campaignList.sort(function(a, b) {
                            if (a.value.toLowerCase() < b.value.toLowerCase()) return -1;
                            if (a.value.toLowerCase() > b.value.toLowerCase()) return 1;
                            return 0;
                        });
					}
					if ($localStorage.agentList != null && $localStorage.agentList != '') {
						$scope.qa.agentList = JSON.parse($localStorage.agentList);
					}
					
					if ($localStorage.filterCriteriaObj != null && $localStorage.filterCriteriaObj != '') {
						$scope.readFilterCriteriaFromLocalstorage();
					}
					$scope.loading = false;
				} else {
					
					//$scope.agentSearchBy.quickTime = "In Past 8 hours";
					if($scope.agentSearchBy.quickTime == "In Past 8 hours") {						
						$scope.getLastEightHours();    	                    
                    	$scope.sTimeSelectedText = $scope.getDefaultDate($scope.startHourList, $scope.qa.startHour);
                    	$scope.eTimeSelectedText = $scope.getDefaultDate($scope.endHourList, $scope.qa.endHour);
                        $scope.agentSearchBy.startTime = $scope.qa.startHour;
                        $scope.agentSearchBy.endTime = $scope.qa.endHour;
                        $scope.sDateSelected = true;   
					}
					
					//API call for getting prospect call search
					if ($scope.userRoleAgent == "AGENT") {
						
						qaProspectListService.get({
		                    searchstring: angular.toJson({
		                        "clause": "ByAgent",
		                        "disposition":["SUCCESS"],
		                        "sort": $scope.sortColumn,
		                        "direction": $scope.direction,
		                        "page": $scope.currentPage - 1,
		                        "size": $scope.pageSize,
		                        "timeZone": timeZone
		                    })
		                }).$promise.then(function(data) {
                            $scope.prospectCallList = data;
                            if($scope.prospectCallList.prospectCallSearchList.length < 1){
                                NotificationService.log('success', 'There is no data.');
                            }
		                    $scope.currentPageLength = data.prospectCallSearchList.length;
		                    $scope.totalRecordCount = data.prospectCallCount;
		                    
		                    /* DATE:16/01/2017	REASON:Added to fetch campaignList and agentList from prospectList call */
                            $scope.qaCampaign = data.campaignSearchResult;
                            $scope.qa.campaignList = [];
                            $scope.qa.agentList = [];
                            if (data.campaignSearchResult != null && data.campaignSearchResult != undefined) {
	    						angular.forEach(data.campaignSearchResult.campaignsList,  function(campaignListValue, campaignListkey){
	    							$scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});
	    							if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
	    								$scope.allPartnerIds.push(campaignListValue.organizationId);
	    							}
	    						});
                            }
                            if (data.agentSearchResult != null && data.agentSearchResult != undefined) {
	    						angular.forEach(data.agentSearchResult,  function(agent, index) {
	    							$scope.qa.agentList.push({"key":agent.id,"value":agent.id+' - '+agent.firstName+' '+agent.lastName})
	    						});
                            }
                            $localStorage.campaignList = JSON.stringify($scope.qa.campaignList);
                            $scope.qa.campaignList.sort(function(a, b) {
                                if (a.value.toLowerCase() < b.value.toLowerCase()) return -1;
                                if (a.value.toLowerCase() > b.value.toLowerCase()) return 1;
                                return 0;
                            });
    						$localStorage.agentList = JSON.stringify($scope.qa.agentList);
    						
		                    $scope.main.pages = parseInt($scope.prospectCallList.prospectCallCount / $scope.main.take);
		                    $scope.loading = false;
		                    saveDataInLocalStorage();
		                }, function(error) {
		                    $scope.loading = false;
		                });						
						
					} else {
                        $scope.getDetailsWithoutProspects();
					}
				}
				if ($scope.userRoleAgent != "AGENT") {
					//API call for getting drop-down values for teams, campaigns, agents, clients
					
					var searchObject = {"clause":"ByQa", "timeZone":timeZone}; 
					/*var searchObject = {"clause":"ByQa"};*/ 
					/*searchQaCampaignService.query({searchString : angular.toJson(searchObject)}, null).$promise.then(function(campaignListData) {
						$scope.qaCampaign = campaignListData;
						angular.forEach(campaignListData.campaignsList,  function(campaignListValue, campaignListkey){
							
							$scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});
							if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
								$scope.allPartnerIds.push(campaignListValue.organizationId);
							}
						});
					});*/
					
					searchTeamService.query({searchString : angular.toJson(searchObject)}, null).$promise.then(function(teamListData) {
						angular.forEach(teamListData,  function(team, index) {
							$scope.qa.teamList.push({"key":team.teamId,"value":team.name})
						});
					});
					
					/*searchQaCampaignAgentService.query({searchString : angular.toJson(searchObject)}, null).$promise.then(function(agentListData) {
						angular.forEach(agentListData,  function(agent, index) {
							$scope.qa.agentList.push({"key":agent.id,"value":agent.firstName+' '+agent.lastName})
						});
					});*/
						
					//Service for scoredRateStatus
					$scope.pastPerformance = false;
					/*scoredRateMetricsService.query().$promise.then(function(scoredRateListData) {						
							$scope.qa.scoredRateList = scoredRateListData;
							if($scope.qa.scoredRateList.QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS != undefined && $scope.qa.scoredRateList.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS != undefined && $scope.qa.scoredRateList.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS != undefined){
								var yesterdayRate = ($scope.qa.scoredRateList.QA_COMPLETE_PROSPECTCALL_COUNT_LAST48_HOURS - $scope.qa.scoredRateList.QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS)
					            var todayRate = $scope.qa.scoredRateList.QA_COMPLETE_PROSPECTCALL_COUNT_LAST24_HOURS					            
					            var percRate = (((todayRate - yesterdayRate) / yesterdayRate ) * 100 );
								$scope.performanceHidden = false;
								if(isNaN(percRate) || !isFinite(percRate) || percRate == 0.00) {
									$scope.performanceHidden = true;
								} else {
									$scope.finalPercRate = percRate;
									if ($scope.finalPercRate > 0) {
										$scope.pastPerformance = false;
									} else {
										$scope.finalPercRate = Math.abs(percRate);
										$scope.pastPerformance = true;										
									}
								}
							}					
							
							if($scope.qa.scoredRateList.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS != undefined && $scope.qa.scoredRateList.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS != null){
								if($scope.qa.scoredRateList.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS < 1) {
									$scope.reviewRateCount = 0;	
								} else {
									$scope.reviewRateCount = parseFloat($scope.qa.scoredRateList.QA_REVIEWED_PROSPECTCALL_COUNT_LAST24_HOURS / $scope.qa.scoredRateList.TOTAL_PROSPECTCALL_FOR_QA_LAST24_HOURS).toFixed(2);
								}
							}
					});*/
					
					
					if($scope.agentSearchBy.quickTime == "In Past 8 hours"){
						var today = Date.now();
						var startday = today - (8 * 3600000);
	                    $scope.qa.startMinute = $filter('date')(startday, 'mm');
	                    $scope.qa.endMinute = $filter('date')(today, 'mm');
	                    $scope.qa.startHour = $filter('date')(startday, 'HH');
	                    $scope.qa.endHour = $filter('date')(today, 'HH');	
					}
					
					
					// var searchByDispositionStatusObject = {"clause":"ByDispositionStatus","startHour":$scope.qa.startHour, "endHour":$scope.qa.endHour,"endMinute":$scope.qa.endMinute, "campaignId": $scope.agentSearchBy.selectedCampaign, "agentIds": $scope.agentIds,"timeZone":timeZone};
					// var searchByProspectCallStatusObject = {"clause":"ByProspectCallStatus","startHour":$scope.qa.startHour, "endHour":$scope.qa.endHour,"endMinute":$scope.qa.endMinute, "campaignId": $scope.agentSearchBy.selectedCampaign, "agentIds": $scope.agentIds,"timeZone":timeZone};
					var secondaryQaMetricsCriteria = {
                        "clause":"ByQaStat",
                        "startHour":$scope.qa.startHour, 
                        "endHour":$scope.qa.endHour,
                        "endMinute":$scope.qa.endMinute, 
                        "campaignIds": $scope.agentSearchBy.selectedCampaign, 
                        "agentIds": $scope.agentIds,"timeZone":timeZone
                    };
                    //$scope.getQaMetrics(secondaryQaMetricsCriteria);

					//Call header block API's
					// DATE:12/01/2018		REASON:Commented QA stats calls
					// $scope.calGraphApi(searchByDispositionStatusObject, searchByProspectCallStatusObject);								
					
					//end of page load API call
				}
                

                /* Agent date values */
                $scope.setAgentDateValues = function() {
                    var curr = new Date; // get current date
                    var currDate = curr.getDate();
                    var first = currDate - 30 // First day is the day of the month - the day of the week
                    var firstday = new Date(curr.setDate(first));
                    var lastday = Date.now();

                    $scope.agent.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
                    $scope.agent.endDate = $filter('date')(lastday, 'yyyy-MM-dd');
                }

                /* GOTO splash method */
                $scope.goToSplash = function() {
                    delete $localStorage.prospectCallList;
                    delete $localStorage.filterCriteriaObj;
                    delete $localStorage.agentList;
                    delete $localStorage.campaignList;
                    $state.go('agentsplash');
                }

                /* Set Duration dropdown values */
                $scope.setDuration = function() {
                    var value = $scope.agentSearchBy.duration;
                    console.log(value);
                    if (value == 'Under 30 secs') {
                        $scope.qa.startCallDuration = 0;
                        $scope.qa.endCallDuration = 30;
                    } else if (value == 'Up to 1 min') {
                        $scope.qa.startCallDuration = 0;
                        $scope.qa.endCallDuration = 60;
                    } else if (value == '1 min to 3 mins') {
                        $scope.qa.startCallDuration = 60;
                        $scope.qa.endCallDuration = 180;
                    } else if (value == 'Over 10 mins') {
                        $scope.qa.startCallDuration = 600;
                        $scope.qa.endCallDuration = 10800;
                    } else if (value == 'Over 30 mins') {
                        $scope.qa.startCallDuration = 1800;
                        $scope.qa.endCallDuration = 10800;
                    } else {
                    	 $scope.qa.startCallDuration = null;
                         $scope.qa.endCallDuration = null;
                    }
                }

                /* Calculate days in month */
                function daysInMonth(month, year) {
                    var dd = new Date(year, month, 0);
                    return dd.getDate();
                }

                /* Set quick date */
                $scope.setQuickDate = function() {
                    var value = $scope.agentSearchBy.quickDate;
                    console.log(value);
                    var today = Date.now();
                    if (value == 'Today') {
                        var lastday = today - 86400000;
                        $scope.qa.startDate = $filter('date')(today, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(today, 'yyyy-MM-dd');

                    } else if (value == 'Yesterday') {
                        var endday = today - 86400000;
                        var startday = today - (2 * 86400000);
                        $scope.qa.startDate = $filter('date')(endday, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(endday, 'yyyy-MM-dd');

                    } else if (value == 'Last Week') {
                        var curr = new Date;
                        var currDate = curr.getDate() - 7;
                        var first = currDate - curr.getDay();
                        var last = first + 6;

                        var firstday = new Date(curr.setDate(first));
                        var lastday = new Date(curr.setDate(last));

                        $scope.qa.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(lastday, 'yyyy-MM-dd');

                    } else if (value == 'Last Two Weeks') {
                        var curr = new Date;
                        var currDate = curr.getDate() - 7;
                        var first = currDate - curr.getDay();
                        var last = first + 6;

                        var firstday = new Date(curr.getFullYear(), curr.getMonth(), ((curr.getDate() - curr.getDay()) - 14));

                        //var firstday = new Date(curr.setDate(first));
                        var lastday = new Date(curr.setDate(last));

                        $scope.qa.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(lastday, 'yyyy-MM-dd');

                    } else if (value == 'Month To Date') {
                        var startday = today - (30 * 86400000);
                        var date = new Date();
                        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

                        $scope.qa.startDate = $filter('date')(firstDay, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(today, 'yyyy-MM-dd');

                    } else if (value == 'Last Month') {
                        var startday = new Date();
                        startday.setDate(1);
                        startday.setMonth(startday.getMonth() - 1);
                        var endday = new Date();

                        endday = new Date(endday.getFullYear(), endday.getMonth(), 0);

                        $scope.qa.startDate = $filter('date')(startday, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(endday, 'yyyy-MM-dd');

                    } else if (value == 'Current Week') {
                        var curr = new Date; // get current date
                        var currDate = curr.getDate();
                        var first = currDate - curr.getDay(); // First day is the day of the month - the day of the week
                        var last = first + 6; // last day is the first day + 13

                        var firstday = new Date(curr.setDate(first));
                        var lastday = Date.now();

                        $scope.qa.startDate = $filter('date')(firstday, 'yyyy-MM-dd');
                        $scope.qa.endDate = $filter('date')(lastday, 'yyyy-MM-dd');
                    } else {
                    	$scope.qa.startDate = null;
                        $scope.qa.endDate = null;
                    }
                }

                /* Set start hour */
                $scope.setStartHour = function() {
                    $scope.qa.startHour = $scope.agentSearchBy.startTime;
                }

                /* Set end hour */
                $scope.setEndHour = function() {
                    $scope.qa.endHour = $scope.agentSearchBy.endTime;
                }

                /* Set quick time */
                $scope.setQuickTime = function() {
                	$scope.preventScoredBlock = false;
                    var value = $scope.agentSearchBy.quickTime;
                    var today = Date.now();
                    //3600000
                    console.log($filter('date')(today, 'h:mm'));
                    
                    if (value == 'In Past hour') {
                        var lastday = today - 3600000;
                        $scope.qa.startMinute = $filter('date')(lastday, 'mm');
                        $scope.qa.endMinute = $filter('date')(today, 'mm');
                        $scope.qa.startHour = $filter('date')(lastday, 'HH');
                        $scope.qa.endHour = $filter('date')(today, 'HH');

                    } else if (value == 'In Past 4 hours') {

                        var startday = today - (4 * 3600000);
                        $scope.qa.startMinute = $filter('date')(startday, 'mm');
                        $scope.qa.endMinute = $filter('date')(today, 'mm');
                        $scope.qa.startHour = $filter('date')(startday, 'HH');
                        $scope.qa.endHour = $filter('date')(today, 'HH');

                    } else if (value == 'In Past 8 hours') {

                        var startday = today - (8 * 3600000);
                        $scope.qa.startMinute = $filter('date')(startday, 'mm');
                        $scope.qa.endMinute = $filter('date')(today, 'mm');
                        $scope.qa.startHour = $filter('date')(startday, 'HH');
                        $scope.qa.endHour = $filter('date')(today, 'HH');
                    } else {
                    	 $scope.qa.startMinute = "";
                         $scope.qa.endMinute = "";
                         $scope.qa.startHour = "";
                         $scope.qa.endHour = "";
                         $scope.sTimeSelectedText = "";
                 		 $scope.eTimeSelectedText = "";
                 		 $scope.agentSearchBy.startTime = null;
                 		 $scope.agentSearchBy.endTime = null;
                 		 $scope.sDateSelected = false;
                    }
                }

                /* Close disposition */
                $scope.closeDisposition = function() {
                    $scope.showDisposition = false
                }

                /* Reset quick date */
                $scope.resetQuickDate = function() { //reset quick date if start date and date changed when quick date already selected
                    if ($scope.agentSearchBy.quickDate != undefined) {
                        $scope.agentSearchBy.quickDate = null;
                    }
                }

                /* Calculate max date */
                $scope.maxDate = new Date().toISOString().substring(0, 10);

                /* Calculate open start date */
                $scope.openStartDate = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.startDateOpened = true;
                };

                /* Calculate open end date */
                $scope.openEndDate = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.endDateOpened = true;
                };

                /* Set disposition values */
                $scope.setDisposition = function(status) {
                	$scope.preventScoredBlock = true;
                	$scope.agentSearchBy.dispositionStatus = null;
                    $scope.agentSearchBy.subStatus = null;
                	/* DATE:12/12/2017		REASON:Added to show QA_COMPLETE leads READY_FOR_DELIVERY AND REJECTED_LEADS */
                    if (status == 'READY_FOR_DELIVERY') {
                		$scope.agentSearchBy.qastatus = 'READY_FOR_DELIVERY';
                	}
                    if (status == 'REJECTED_LEADS') {
                		$scope.agentSearchBy.qastatus = 'REJECTED_LEADS';
                	}
                    if (status == "SUCCESS") {
                    	$scope.agentSearchBy.dispositionStatus = status;
                    	$scope.agentSearchBy.qastatus = "";
                        //$scope.agentSearchBy.subStatus = "SUBMITLEAD";
                    } else if (status == "CALLBACK") {
                    	$scope.agentSearchBy.dispositionStatus = status;
                        //$scope.agentSearchBy.subStatus = "CALLBACK_USER_GENERATED";
                    } else if (status == "FAILURE") {
                    	$scope.agentSearchBy.dispositionStatus = status;
                        $scope.agentSearchBy.subStatus = null;
                        if ($scope.agentSearchBy.subStatus == "CALLBACK_USER_GENERATED" || $scope.agentSearchBy.subStatus == "SUBMITLEAD") {
                            $scope.agentSearchBy.subStatus = null;
                        }
                    } else if (status == "DIALERCODE") {
                    	$scope.agentSearchBy.dispositionStatus = status;
                        $scope.agentSearchBy.subStatus = null;
                        if ($scope.agentSearchBy.subStatus == "CALLBACK_USER_GENERATED" || $scope.agentSearchBy.subStatus == "SUBMITLEAD") {
                            $scope.agentSearchBy.subStatus = null;
                        }
                    } else {
                        var splitType = status.split('-');
                        if (splitType[0] == "FAILURE") {
                            $scope.agentSearchBy.dispositionStatus = splitType[0];
                            $scope.agentSearchBy.subStatus = splitType[1];
                        } else if (splitType[0] == "DIALERCODE") {
                            $scope.agentSearchBy.dispositionStatus = splitType[0];
                            $scope.agentSearchBy.subStatus = splitType[1];
                        }

                    }
                }

                $scope.filterCriteriaObj = {};

                var makeFilterCriteriaObj = function() {   
                       
                        if ($scope.agentSearchBy.selectedAgent != undefined && $scope.qa.agentList.length != 0) {
                            $scope.selectedAgent = $filter('filter')($scope.qa.agentList, {
                                key: $scope.agentSearchBy.selectedAgent
                            })[0];
                            $scope.filterCriteriaObj.selectedAgentName = $scope.selectedAgent.value;
                            $scope.filterCriteriaObj.selectedAgentId = $scope.selectedAgent.key;
                        }


                        if ($scope.qa.leadStatusList.length != 0) {
                            $scope.selectedStatus = $filter('filter')($scope.qa.leadStatusList, {
                                key: $scope.agentSearchBy.selectedStatus
                            })[0];
                            $scope.filterCriteriaObj.selectedStatusName = $scope.selectedStatus.value;
                            $scope.filterCriteriaObj.selectedStatusId = $scope.selectedStatus.key;
                        }
						$scope.filterCriteriaObj.quickTime = $scope.agentSearchBy.quickTime;						
						if($scope.agentSearchBy.selectedAgentMulti.length > 0) {
							$scope.agentSearchBy.selectedAgentMulti2 = [];
							$scope.filterCriteriaObj.agentIds = $scope.agentSearchBy.selectedAgentMulti;	
						} 
						if($scope.agentSearchBy.selectedAgentMulti2.length > 0) {
							$scope.agentSearchBy.selectedAgentMulti = [];
							$scope.filterCriteriaObj.agentIds = $scope.agentSearchBy.selectedAgentMulti2;	
                        }	
                        
                        if ($scope.leadStatusSearchBy.selectedStatusMulti != undefined) {
                            if ($scope.leadStatusSearchBy.selectedStatusMulti.length > 0) {
                                $scope.leadStatusSearchBy.selectedStatusMulti2 = [];
                                $scope.filterCriteriaObj.leadStatuses = $scope.leadStatusSearchBy.selectedStatusMulti;
                                $scope.filterCriteriaObj.leadStatusSearchBy = {};
                                $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti = $scope.leadStatusSearchBy.selectedStatusMulti;
                            } else {
                                $scope.filterCriteriaObj.leadStatusSearchBy = {};
                                $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti = $scope.leadStatusSearchBy.selectedStatusMulti;
                            }
                        } 
    
    
                        if ($scope.leadStatusSearchBy.selectedStatusMulti2 != undefined) {
                            if ($scope.leadStatusSearchBy.selectedStatusMulti2.length > 0) {
                                $scope.leadStatusSearchBy.selectedStatusMulti = [];
                                $scope.filterCriteriaObj.leadStatuses = $scope.leadStatusSearchBy.selectedStatusMulti2;
                                $scope.filterCriteriaObj.leadStatusSearchBy = {};
                                $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti2 = $scope.leadStatusSearchBy.selectedStatusMulti2;
                            }  else {
                                $scope.filterCriteriaObj.leadStatusSearchBy = {};
                                $scope.filterCriteriaObj.leadStatusSearchBy.selectedStatusMulti2 = $scope.leadStatusSearchBy.selectedStatusMulti2;
                            }
                        }

                        if ($scope.campaignSearchBy.selectedCampaignMulti != undefined) {
                            if ($scope.campaignSearchBy.selectedCampaignMulti.length > 0) {
                                $scope.campaignSearchBy.selectedCampaignMulti2 = [];
                                $scope.filterCriteriaObj.qaCampaigns = $scope.campaignSearchBy.selectedCampaignMulti;
                                $scope.filterCriteriaObj.campaignSearchBy = {};
                                $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti = $scope.campaignSearchBy.selectedCampaignMulti;
                            } else {
                                $scope.filterCriteriaObj.campaignSearchBy = {};
                                $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti = $scope.campaignSearchBy.selectedCampaignMulti;
                            }
                        } 
    
    
                        if ($scope.campaignSearchBy.selectedCampaignMulti2 != undefined) {
                            if ($scope.campaignSearchBy.selectedCampaignMulti2.length > 0) {
                                $scope.campaignSearchBy.selectedCampaignMulti = [];
                                $scope.filterCriteriaObj.qaCampaigns = $scope.campaignSearchBy.selectedCampaignMulti2;
                                $scope.filterCriteriaObj.campaignSearchBy = {};
                                $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti2 = $scope.campaignSearchBy.selectedCampaignMulti2;
                            }  else {
                                $scope.filterCriteriaObj.campaignSearchBy = {};
                                $scope.filterCriteriaObj.campaignSearchBy.selectedCampaignMulti2 = $scope.campaignSearchBy.selectedCampaignMulti2;
                            }
                        }

						$scope.filterCriteriaObj.callId = $scope.agentSearchBy.callId;
						$scope.filterCriteriaObj.selectedTeamId = $scope.agentSearchBy.selectedTeam;						
						$scope.filterCriteriaObj.selectedClientId = $scope.agentSearchBy.selectedClientId;
                        $scope.filterCriteriaObj.selectedDuration = $scope.agentSearchBy.duration;
                        $scope.filterCriteriaObj.leadStatuses = $scope.leadStatuses;
                        $scope.filterCriteriaObj.qaCampaigns = $scope.qaCampaigns;
                        $scope.filterCriteriaObj.quickDate = $scope.agentSearchBy.quickDate;
                        $scope.filterCriteriaObj.startDate = $scope.qa.startDate;
                        $scope.filterCriteriaObj.endDate = $scope.qa.endDate;
                        $scope.filterCriteriaObj.qastatus = $scope.agentSearchBy.qastatus;
                        $scope.filterCriteriaObj.dispStatustxt = $scope.setDispStatustxt;
                        $scope.filterCriteriaObj.prospCalltxt = $scope.setProspCalltxt;
                        if($scope.agentSearchBy.startTime < 10 && $scope.agentSearchBy.startTime != null){
                        	$scope.agentSearchBy.startTime = $scope.agentSearchBy.startTime.replace(/^0+/, '');
                        	$scope.filterCriteriaObj.startTime = $scope.agentSearchBy.startTime.replace(/^0+/, '');
                        } else {
                        	$scope.filterCriteriaObj.startTime = $scope.agentSearchBy.startTime;
                        }
                        
                        if($scope.agentSearchBy.endTime < 10 && $scope.agentSearchBy.endTime != null){
                        	$scope.agentSearchBy.endTime = $scope.agentSearchBy.endTime.replace(/^0+/, '');	
                        	$scope.filterCriteriaObj.endTime = $scope.agentSearchBy.endTime.replace(/^0+/, '');
                        } else {
                        	$scope.filterCriteriaObj.endTime = $scope.agentSearchBy.endTime;
                        }
                        
                        $scope.filterCriteriaObj.startMinute = $scope.qa.startMinute;
                        $scope.filterCriteriaObj.endMinute = $scope.qa.endMinute;
                        $scope.filterCriteriaObj.startHour = $scope.qa.startHour;
                        $scope.filterCriteriaObj.endHour = $scope.qa.endHour;
                        
                        
                        $scope.filterCriteriaObj.selectedDispositionStatus = $scope.agentSearchBy.dispositionStatus;
                        $scope.filterCriteriaObj.dispositionStatus = $scope.agentSearchBy.dispositionStatus;  
                        $scope.filterCriteriaObj.agentSelectedText = $scope.agentSelectedText;
                        $scope.filterCriteriaObj.leadStatusSelectedText = $scope.leadStatusSelectedText;
                        $scope.filterCriteriaObj.campaignSelectedText = $scope.campaignSelectedText;                        
                        
                        if ($scope.agentSearchBy.subStatus != null && $scope.agentSearchBy.subStatus != '') {
							$scope.filterCriteriaObj.dispositionStatus = $scope.agentSearchBy.dispositionStatus+'-'+$scope.agentSearchBy.subStatus;
						}
                        
                        /* START
                         * DATE : 19/06/2017
                         * REASON : storing data of $scope into $localStorage for pagination to work properly */
                        $scope.filterCriteriaObj.allTeamIds = $scope.allTeamIds;
                        $scope.filterCriteriaObj.agentIds = $scope.agentIds;
                        
                        $scope.filterCriteriaObj.startCallDuration = $scope.qa.startCallDuration;
                        $scope.filterCriteriaObj.endCallDuration = $scope.qa.endCallDuration;
                        
                        $scope.filterCriteriaObj.alldisposition = $scope.alldisposition;
                        $scope.filterCriteriaObj.allsubstatus = $scope.allsubstatus;
                        
                        $scope.filterCriteriaObj.allprospectCallId = $scope.allprospectCallId;
                        /* END */
                        
						//Pagination Values
						$scope.filterCriteriaObj.currentPage = $scope.currentPage;
						$scope.filterCriteriaObj.pageSize = $scope.pageSize;
						$scope.filterCriteriaObj.currentPageLength = $scope.currentPageLength;
						$scope.filterCriteriaObj.totalRecordCount = $scope.totalRecordCount;
						$scope.filterCriteriaObj.sortColumn = $scope.sortColumn;
						$scope.filterCriteriaObj.direction = $scope.direction;
						
                    }
                    //TODO:- Create service for Storing the data from local storage
                var saveDataInLocalStorage = function() {
                    makeFilterCriteriaObj();
                    $localStorage.prospectCallList = JSON.stringify($scope.prospectCallList); //save prospect call search result
                    $localStorage.filterCriteriaObj = JSON.stringify($scope.filterCriteriaObj); //save search criteria 
                }

                
                $scope.hideAllFilters = function () {
                	$scope.callIdSelected = false;
                    $scope.teamSelected = false;
                    $scope.agentSelected = false;
                    $scope.campaignSelected = false;                
                    $scope.sDateSelected = false;                
                    $scope.disposeSelected = false;
                    $scope.durSelected = false;
                    $scope.leadStatusSelected = false;
                }
                
                
                $scope.filterAgent = function() {
                    $scope.isResetButtonClicked = false;
                    $scope.loading = true;
                    var disposition = [];
                    $scope.allTeamIds = [];
                    $scope.agentIds = [];
                    $scope.qaCampaigns = [];
                    $scope.alldisposition = [];
                    var substatus = [];
                    $scope.allsubstatus = [];
                    var prospectCallId = [];
                    $scope.allprospectCallId = [];
                    $scope.disableReportDownloadButton = false;
                    //Close all filter popups
                    $scope.resetPop();
                    
                    //Check for multiple Agent 
                    $scope.setAgentArr();

                    var successFlag = false;
                    var failureFlag = false; 
                    var answerMachineFlag = false;
                    if ($scope.leadStatusSearchBy.selectedStatusMulti2 != undefined && $scope.leadStatusSearchBy.selectedStatusMulti2 != null) {
                            angular.forEach($scope.leadStatusSearchBy.selectedStatusMulti2,  function(item){
                            if($scope.successLeadStatusList.includes(item)){
                                successFlag = true;
                            } else if($scope.failureLeadStatusList.includes(item)){
                                failureFlag = true;
                            } else if ($scope.ansMachineLeadStatusList.includes(item)) {
                                answerMachineFlag = true;
                            }
                        });
                    }
                    if (successFlag && failureFlag) {
                        NotificationService.log('error', 'Please select either SUCCESS or FAILURE lead statuses.');
                        $scope.loading = false;
                        return;
                    } else if (successFlag && answerMachineFlag) {
                        NotificationService.log('error', 'Please select either SUCCESS or ANSWER MACHINE lead statuses.');
                        $scope.loading = false;
                        return;
                    } else if (failureFlag && answerMachineFlag) {
                        NotificationService.log('error', 'Please select either FAILURE or ANSWER MACHINE lead statuses.');
                        $scope.loading = false;
                        return;
                    } else {
                        $scope.setLeadStatus();
                    }

                    $scope.setQaCampaign();
                    
                    if ($scope.qa.endDate != undefined && $scope.qa.startDate != undefined && $scope.qa.startDate > $scope.qa.endDate) {
                        NotificationService.log('error', 'Start Date should be lesser than the End Date');
                        $scope.loading = false;
                        return;
                    }
                    
                    // if ($scope.qa.endDate == undefined && $scope.qa.startDate != undefined) {
                    //     NotificationService.log('error', 'Please Select End Date');
                    //     $scope.loading = false;
                    //     return;
                    // }

                    if ($scope.qa.endDate != undefined && ($scope.qa.startDate == undefined || $scope.qa.startDate == null)) {
                        NotificationService.log('error', 'Please Select Start Date');
                        $scope.loading = false;
                        return;
                    }

                    if ($scope.agentSearchBy.endTime == undefined && $scope.agentSearchBy.startTime != undefined) {
                        NotificationService.log('error', 'Please Select End Time');
                        $scope.loading = false;
                        return;
                    }

                    if ($scope.agentSearchBy.endTime != undefined && ($scope.agentSearchBy.startTime == undefined || $scope.agentSearchBy.startTime == null)) {
                        NotificationService.log('error', 'Please Select Start Time');
                        $scope.loading = false;
                        return;
                    }
                    
                    if ($scope.qa.endDate != undefined && $scope.qa.endDate != "" && $scope.qa.startDate != undefined && $scope.qa.startDate != "") {
                    	$scope.sDateSelectedText = $scope.qa.startDate;
                        $scope.eDateSelectedText = $scope.qa.endDate;                        
                        $scope.sDateSelected = true;   
                    } else {                    	
                        $scope.sDateSelectedText = "";
                        $scope.eDateSelectedText = "";         
                        
                        if($scope.qa.endDate != null && $scope.qa.startDate != null && $scope.agentSearchBy.startTime != null && $scope.agentSearchBy.endTime != null) {
                        	$scope.sDateSelected = true;	
                        } else {
                        	$scope.sDateSelected = false;
                        }                        
                        
                    }
                    
                    if($scope.qa.startHour != undefined && $scope.qa.startHour != "" && $scope.qa.endHour != undefined && $scope.qa.endHour != "") {
                    	$scope.sTimeSelectedText = $scope.getDefaultDate($scope.startHourList, $scope.qa.startHour);
                    	$scope.eTimeSelectedText = $scope.getDefaultDate($scope.endHourList, $scope.qa.endHour);                    	
                        $scope.agentSearchBy.startTime = $scope.qa.startHour;
                        $scope.agentSearchBy.endTime = $scope.qa.endHour;
                        $scope.sDateSelected = true;   
                    } else {                    	
                        $scope.sTimeSelectedText = "";
                        $scope.eTimeSelectedText = "";  
                        if($scope.qa.endDate != null && $scope.qa.endDate != "" && $scope.qa.startDate != null && $scope.qa.startDate != "" && $scope.agentSearchBy.startTime == null && $scope.agentSearchBy.endTime == null) {
                        	$scope.sDateSelected = true;	
                        } else {
                        	$scope.sDateSelected = false;
                        } 
                    }
                    
                    
                    if ($scope.agentSearchBy.selectedTeam != undefined) {
                        if ($scope.agentSearchBy.selectedTeam.key != undefined) {
                            $scope.allTeamIds.push($scope.agentSearchBy.selectedTeam.key);
                            $scope.agentSearchBy.selectedTeam = $scope.agentSearchBy.selectedTeam.key;
                            $scope.teamSelectedText = $scope.agentSearchBy.selectedTeam.key;
                            $scope.teamSelected = true;
                        } else {
                            $scope.allTeamIds.push($scope.agentSearchBy.selectedTeam);
                            $scope.teamSelectedText = $filter('filter')($scope.qa.teamList, $scope.agentSearchBy.selectedTeam)[0].value;                            
                            $scope.teamSelected = true; 
                        }
                    } else {
                    	$scope.teamSelectedText = "";
                    	if($scope.agentSearchBy.selectedAgentMulti2.length > 0 && $scope.agentSearchBy.selectedTeam != undefined) {
                    	  $scope.teamSelected = true; 
                    	} else {
                    		$scope.teamSelected = false;	
                    	}
                    }
                    
                    if ($scope.agentSearchBy.dispositionStatus != null && $scope.agentSearchBy.dispositionStatus != "" ) {
                    	if($scope.agentSearchBy.dispositionStatus.indexOf("-") > -1){
                        	var splitType = $scope.agentSearchBy.dispositionStatus.split('-');
                            if (splitType[0] == "FAILURE") {
                                $scope.agentSearchBy.dispositionStatus = splitType[0];
                                $scope.agentSearchBy.subStatus = splitType[1];
                                
                                disposition.push($scope.agentSearchBy.dispositionStatus);
                                $scope.alldisposition.push($scope.agentSearchBy.dispositionStatus);
                                
                                substatus.push($scope.agentSearchBy.subStatus);
                                $scope.allsubstatus.push($scope.agentSearchBy.subStatus);
                                
                                if($scope.agentSearchBy.subStatus != null) {
                                	$scope.disposeSelectedText = $scope.agentSearchBy.subStatus;
                                	$scope.disposeSelected = true;
                                } else {
                                	$scope.disposeSelectedText = $scope.agentSearchBy.dispositionStatus;
                                	$scope.disposeSelected = true;
                                }
                                
                            } else if (splitType[0] == "DIALERCODE") {
                                $scope.agentSearchBy.dispositionStatus = splitType[0];
                                $scope.agentSearchBy.subStatus = splitType[1];
                                
                                disposition.push($scope.agentSearchBy.dispositionStatus);
                                $scope.alldisposition.push($scope.agentSearchBy.dispositionStatus);
                                
                                substatus.push($scope.agentSearchBy.subStatus);
                                $scope.allsubstatus.push($scope.agentSearchBy.subStatus);
                                
                                if($scope.agentSearchBy.subStatus != null) {
                                	$scope.disposeSelectedText = $scope.agentSearchBy.subStatus;
                                	$scope.disposeSelected = true;
                                } else {
                                	$scope.disposeSelectedText = $scope.agentSearchBy.dispositionStatus;
                                	$scope.disposeSelected = true;
                                }
                            }   
                        }else{
                        	if ($scope.agentSearchBy.dispositionStatus != null && $scope.agentSearchBy.dispositionStatus != "" ) {
                                disposition.push($scope.agentSearchBy.dispositionStatus);
                                $scope.alldisposition.push($scope.agentSearchBy.dispositionStatus);
                            }
                            
                            if ($scope.agentSearchBy.subStatus != null) {
                                substatus.push($scope.agentSearchBy.subStatus);
                                $scope.allsubstatus.push($scope.agentSearchBy.subStatus);
                            }
                            
                            if($scope.agentSearchBy.subStatus != null) {
                            	
                            	$scope.disposeSelectedText = $scope.agentSearchBy.subStatus;
                            	
                            	if($scope.agentSearchBy.subStatus == 'CALLBACK_USER_GENERATED' || $scope.agentSearchBy.subStatus == 'CALLBACK_GATEKEEPER') {
                            		$scope.disposeSelectedText = 'CALLBACK';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'DNCL') {
                            		$scope.disposeSelectedText = 'FAILURE-DNCL';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'DNRM') {
                            		$scope.disposeSelectedText = 'FAILURE-DNRM';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'BADNUMBER') {
                            		$scope.disposeSelectedText = 'FAILURE-BADNUMBER';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'LANGUAGEBARRIER') {
                            		$scope.disposeSelectedText = 'FAILURE-LANGUAGEBARRIER';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'NO_LONGER_EMPLOYED') {
                            		$scope.disposeSelectedText = 'FAILURE-NO_LONGER_EMPLOYED';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'NOTINTERESTED') {
                            		$scope.disposeSelectedText = 'FAILURE-NOTINTERESTED';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'OTHER') {
                            		$scope.disposeSelectedText = 'FAILURE-OTHER';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'UNSERVICEABLE') {
                            		$scope.disposeSelectedText = 'FAILURE-UNSERVICEABLE';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'ANSWERMACHINE') {
                            		$scope.disposeSelectedText = 'DIALERCODE-ANSWERMACHINE';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'DEADAIR') {
                            		$scope.disposeSelectedText = 'DIALERCODE-DEADAIR';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'BUSY') {
                            		$scope.disposeSelectedText = 'DIALERCODE-BUSY';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'NOANSWER') {
                            		$scope.disposeSelectedText = 'DIALERCODE-NOANSWER';
                            	}
                            	if($scope.agentSearchBy.subStatus == 'GATEKEEPER_ANSWERMACHINE') {
                            		$scope.disposeSelectedText = 'DIALERCODE-GATEKEEPER_ANSWERMACHINE';
                            	}
                            	$scope.disposeSelected = true;
                            } else {
                            	$scope.disposeSelectedText = $scope.agentSearchBy.dispositionStatus;
                            	$scope.disposeSelected = true;
                            }
                            	
                        }
                    } else {
                    	$scope.disposeSelected = false;
                    	$scope.disposeSelectedText = "";
                    }
                        
                    if($scope.agentSearchBy.selectedClientId != null && $scope.agentSearchBy.selectedClientId != '' && $scope.agentSearchBy.selectedClientId != undefined){
                    		$scope.campaignSelected = true;
                    		$scope.clientSelectedText = $scope.agentSearchBy.selectedClientId;
                    } else {                    	
                        $scope.clientSelectedText = "";
                        if($scope.agentSearchBy.selectedCampaign != null && $scope.agentSearchBy.selectedClientId != null) {
                        	$scope.campaignSelected = true;	
                        } else {
                        	$scope.campaignSelected = false;
                        }
                        
                    }
                    
                    if($scope.agentSearchBy.quickTime != null && $scope.agentSearchBy.quickTime != '' && $scope.agentSearchBy.quickTime != undefined){
                    	$scope.preventScoredBlock = false;              		
                    }                    

                    if($scope.agentSearchBy.duration != null && $scope.agentSearchBy.duration != '' && $scope.agentSearchBy.duration != undefined){
                		$scope.durSelected = true;
                		$scope.durSelectedText = $scope.agentSearchBy.duration;
                    } else {                    	
                        $scope.durSelectedText = "";
                        $scope.durSelected = false;
                    }

                    if ($scope.agentSearchBy.callId != null && $scope.agentSearchBy.callId != '' && $scope.agentSearchBy.callId != undefined) {
                        prospectCallId.push($scope.agentSearchBy.callId);
                        $scope.allprospectCallId.push($scope.agentSearchBy.callId);
                        $scope.callIdSelected = true;
                        $scope.callIdSelectedText = $scope.agentSearchBy.callId;
                    } else {                    	
                        $scope.callIdSelectedText = "";  
                        $scope.callIdSelected = false;
                    }
                    
                    var objAgent = {};
                    $scope.agentSelected = [];
                    if ($scope.agentSearchBy.selectedAgentMulti2.length > 0 || $scope.agentSearchBy.selectedAgentMulti.length  > 0) {                    	
                          if($scope.agentSearchBy.selectedAgentMulti2.length > 0) {                        	  
                        	  $scope.setAllowClientAgent = false;                        	  
                        	  if($scope.qa.agentList.length > 0){
                        		  $scope.agentSelectedText = [];                        		  
	                        	  for(i=0; i<$scope.agentSearchBy.selectedAgentMulti2.length; i++){
	                        		  $scope.agentName = $filter('filter')($scope.qa.agentList, $scope.agentSearchBy.selectedAgentMulti2[i])[0].value;
	                        		  $scope.agentKey = $scope.agentSearchBy.selectedAgentMulti2[i];
	                        		  objAgent[$scope.agentKey] = $scope.agentName; 
	                        		  $scope.agentSelected[$scope.agentKey] = true;
	                        	  }
	                        	  $scope.agentSelectedText.push(objAgent);
                        	  }
                          }else{
                        	  if($scope.qa.agentList.length > 0){
                        		  $scope.agentSelectedText = [];
                        		  $scope.preventScoredBlock = false;
                            	  for(i=0; i<$scope.agentSearchBy.selectedAgentMulti.length; i++){
                            		  $scope.agentName = $filter('filter')($scope.qa.agentList, $scope.agentSearchBy.selectedAgentMulti[i])[0].value;
                            		  $scope.agentKey = $scope.agentSearchBy.selectedAgentMulti[i];
                            		  objAgent[$scope.agentKey] = $scope.agentName; 
                            		  $scope.agentSelected[$scope.agentKey] = true;
                            	  }
                            	  $scope.agentSelectedText.push(objAgent);    
                        	  }
                          }
                          
                          $scope.teamSelected = true;                    	  
                    }


                    var objStatus = {};
                    $scope.statusSelected = [];
                    if ($scope.leadStatusSearchBy.selectedStatusMulti2 != undefined && $scope.leadStatusSearchBy.selectedStatusMulti != undefined) {
                            if ($scope.leadStatusSearchBy.selectedStatusMulti2.length > 0 || $scope.leadStatusSearchBy.selectedStatusMulti.length  > 0) {                    	
                                if($scope.leadStatusSearchBy.selectedStatusMulti2.length > 0) {                        	  
                                    //   $scope.setAllowClientAgent = false;                        	  
                                    if($scope.qa.leadStatusList.length > 0){
                                        $scope.leadStatusSelectedText = [];                        		  
                                        for(i=0; i<$scope.leadStatusSearchBy.selectedStatusMulti2.length; i++) {
                                            $scope.leadStatusKey = $scope.leadStatusSearchBy.selectedStatusMulti2[i];
                                            objStatus[$scope.leadStatusKey] = $scope.leadStatusKey; 
                                            $scope.statusSelected[$scope.leadStatusKey] = true;
                                        }
                                        $scope.leadStatusSelectedText.push(objStatus);
                                    }
                                } else {
                                    if($scope.qa.leadStatusList.length > 0) {
                                        $scope.leadStatusSelectedText = [];
                                        $scope.preventScoredBlock = false;
                                        for(i=0; i<$scope.leadStatusSearchBy.selectedStatusMulti.length; i++){
                                            $scope.leadStatusKey = $scope.leadStatusSearchBy.selectedStatusMulti[i];
                                            objStatus[$scope.leadStatusKey] = $scope.leadStatusKey; 
                                            $scope.statusSelected[$scope.leadStatusKey] = true;
                                        }
                                        $scope.leadStatusSelectedText.push(objStatus);    
                                    }
                                }
                                
                                $scope.leadStatusSelected = true;                    	  
                            }
                    }
                    
                    var objCampaign = {};
                    $scope.qaCampaignSelected = [];
                    if ($scope.campaignSearchBy.selectedCampaignMulti2 != undefined && $scope.campaignSearchBy.selectedCampaignMulti != undefined) {    
                    if ($scope.campaignSearchBy.selectedCampaignMulti2.length > 0 || $scope.campaignSearchBy.selectedCampaignMulti.length  > 0) {                    	
                          if($scope.campaignSearchBy.selectedCampaignMulti2.length > 0) {                        	  
                        	//   $scope.setAllowClientAgent = false;                        	  
                        	  if($scope.qa.campaignList.length > 0){
                        		  $scope.campaignSelectedText = [];                        		  
	                        	  for(i=0; i<$scope.campaignSearchBy.selectedCampaignMulti2.length; i++) {
	                        		  $scope.campaignKey = $scope.campaignSearchBy.selectedCampaignMulti2[i];
                                       var campaignName = $filter('filter')($scope.qa.campaignList, $scope.campaignKey)[0].value; 
                                      objCampaign[$scope.campaignKey] = campaignName.toString(); 
	                        		  $scope.qaCampaignSelected[$scope.campaignKey] = true;
	                        	  }
	                        	  $scope.campaignSelectedText.push(objCampaign);
                        	  }
                          } else {
                        	  if($scope.qa.campaignList.length > 0) {
                        		  $scope.campaignSelectedText = [];
                        		  $scope.preventScoredBlock = false;
                            	  for(i=0; i<$scope.campaignSearchBy.selectedCampaignMulti.length; i++){
                                    $scope.campaignKey = $scope.campaignSearchBy.selectedCampaignMulti[i];
                                    objCampaign[$scope.campaignKey] = $scope.campaignKey; 
                                    $scope.qaCampaignSelected[$scope.campaignKey] = true;
                            	  }
                            	  $scope.campaignSelectedText.push(objCampaign);    
                        	  }
                          }
                          $scope.campaignSelected = true;                    	  
                    }
                }

                if ($scope.allPartnerIds == null || $scope.allPartnerIds == undefined ||($scope.allPartnerIds != null && $scope.allPartnerIds != undefined && $scope.allPartnerIds.length == 0)) {
                    $scope.allPartnerIds.push(user.organization);
                }
                    
                    
                    var filterObject = {
                        "clause": "ByQa",
                        "teamIds": $scope.allTeamIds,
                        "partnerIds": $scope.allPartnerIds,
                        "campaignIds": $scope.qaCampaigns,
                        "agentIds": $scope.agentIds,
                        "sort": $scope.sortKey,
                        "direction": $scope.main.direction,
                        "page": 0,
                        "size": $scope.pageSize,
                        "toDate": $scope.qa.endDate,
                        "fromDate": $scope.qa.startDate,
                        "callDurationRangeStart": $scope.qa.startCallDuration,
                        "callDurationRangeEnd": $scope.qa.endCallDuration,
                        "startHour": $scope.qa.startHour,
                        "endHour": $scope.qa.endHour,
                        "startMinute": $scope.qa.startMinute,
                        "endMinute": $scope.qa.endMinute,
                        "leadStatus": $scope.leadStatuses,
//                        "disposition": disposition,
                        "disposition":["SUCCESS"],
                        "subStatus": substatus,
                        "prospectCallIds": prospectCallId,
                        "clientId": $scope.agentSearchBy.selectedClientId,
                        "prospectCallStatus": $scope.agentSearchBy.qastatus,
                        "timeZone": timeZone,
                        "qaSearchString": $scope.searchLead
                    };
                    
                    var filterObject2 = {
                            "clause": "ByQa",                            
                            "campaignIds": $scope.agentSearchBy.selectedCampaign,
                            "agentIds": $scope.agentIds,                            
                            "startHour": $scope.qa.startHour,
                            "endHour": $scope.qa.endHour,
                            "startMinute": $scope.qa.startMinute,
                            "endMinute": $scope.qa.endMinute,
                            "timeZone": timeZone
                        };
                    
                    var filterObject3 = {
                    		"clause": "ByQa",                            
                            "campaignIds": $scope.agentSearchBy.selectedCampaign,
                            "agentIds": $scope.agentIds,                            
                            "startHour": $scope.qa.startHour,
                            "endHour": $scope.qa.endHour,
                            "startMinute": $scope.qa.startMinute,
                            "endMinute": $scope.qa.endMinute,
                            "timeZone": timeZone
                        };

                    qaProspectListService.query({
                        searchstring: angular.toJson(filterObject)
                    }, null).$promise.then(function(data) {
                            console.log(data);
                            $scope.prospectCallList = data;
                            // if($scope.prospectCallList.prospectCallSearchList.length < 1){
                            //     NotificationService.log('success', 'There is no data.');
                            // }
                            $scope.currentPageLength = data.prospectCallSearchList.length;
                            $scope.totalRecordCount = data.prospectCallCount;
                            
                            /* DATE:16/01/2017	REASON:Added to fetch campaignList and agentList from prospectList call */
//                            if ($localStorage.campaignList == null || $localStorage.campaignList == '' || $localStorage.agentList == null || $localStorage.agentList == '') {
                            	$scope.qaCampaign = data.campaignSearchResult;
                                $scope.qa.campaignList = [];
                                // $scope.qa.agentList = [];
                                if (data.campaignSearchResult != null && data.campaignSearchResult != undefined) {
    								angular.forEach(data.campaignSearchResult.campaignsList,  function(campaignListValue, campaignListkey){
    									$scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});
    									if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
    										$scope.allPartnerIds.push(campaignListValue.organizationId);
    									}
    								});
                                }
                                $scope.qa.campaignList.sort(function(a, b) {
                                    if (a.value.toLowerCase() < b.value.toLowerCase()) return -1;
                                    if (a.value.toLowerCase() > b.value.toLowerCase()) return 1;
                                    return 0;
                                });
                                // if (data.agentSearchResult != null && data.agentSearchResult != undefined) {
    	    					// 	angular.forEach(data.agentSearchResult,  function(agent, index) {
    	    					// 		$scope.qa.agentList.push({"key":agent.id,"value":agent.id+' - '+agent.firstName+' '+agent.lastName})
    	    					// 	});
                                // }
        						// $localStorage.campaignList = JSON.stringify($scope.qa.campaignList);
        						// $localStorage.agentList = JSON.stringify($scope.qa.agentList);
//                            }
                            $localStorage.campaignList = JSON.stringify($scope.qa.campaignList);
                            $localStorage.qaCampaign = JSON.stringify($scope.qaCampaign);
                            $scope.main.pages = parseInt(data.prospectCallCount / $scope.main.take);
                            $scope.loading = false;
                            saveDataInLocalStorage(); //save data in local storage for cashing purpose
                            $scope.readFilterCriteriaFromLocalstorage();
                        },
                        function(error) {
                            $scope.loading = false;
                        });
                    
                    
                    if($scope.preventScoredBlock == false){
                    	//Head Block API calling
                        // var searchByDispositionStatusObject = filterObject2;
                        // var searchByProspectCallStatusObject = filterObject3;
                        var qaMetricsCriteria = angular.copy(filterObject);
    				    qaMetricsCriteria.clause = "ByQaStat";
                        //$scope.getQaMetrics(qaMetricsCriteria);
        				// DATE:12/01/2018		REASON:Commented QA stats calls
                        // $scope.calGraphApi(searchByDispositionStatusObject, searchByProspectCallStatusObject);	
                    }                    
                }

                /* Pagination Code */

                $scope.noSize = false;
                $scope.noStatus = false;
                //If no QA present
                $scope.noQA = false;
                if ($scope.currentPageLength == 0) {
                    $scope.noQA = true;
                    $scope.noStatus = false;
                }
                
                $scope.prospectCallIdSort = false;
            	$scope.agentNameSort = false;
            	$scope.campaignNameSort = false;
            	$scope.callStartTimeSort = false;
            	$scope.dispositionStatusSort = false;
            	$scope.callDurationSort = false;   
            	$scope.statusSort = false; 
                
            	$scope.sorts = {
                        active: '',
                        descending: undefined
                }  
            	
            	$scope.callIdSortFlag = false;
            	$scope.agentSortFlag = false;
            	$scope.campaignSortFlag = false;
            	$scope.dateSortFlag = false;
            	$scope.disposeSortFlag = false;
                $scope.durationSortFlag = false;
                $scope.leadStatusSortFlag = false;
            	$scope.statusScoreFlag = false;
            	
            	
            	// $scope.changeSorting = function(column) {
                //     if ($scope.isResetButtonClicked) {
                //         $scope.reset();
                //     }
            	// 	$scope.callIdSortFlag = false;
                // 	$scope.agentSortFlag = false;
                // 	$scope.campaignSortFlag = false;
                // 	$scope.dateSortFlag = false;
                // 	$scope.disposeSortFlag = false;
                //     $scope.durationSortFlag = false;
                //     $scope.leadStatusSortFlag = false;
                // 	$scope.statusScoreFlag = false;
                	
            	// 	if(column == 'prospectCall.prospectCallId') {
            	// 		$scope.callIdSortFlag = true;
            	// 	}
            	// 	if(column == 'prospectCall.agentId') {
            	// 		$scope.agentSortFlag = true;
            	// 	}
            	// 	if(column == 'prospectCall.campaignId') {
            	// 		$scope.campaignSortFlag = true;
            	// 	}
            	// 	if(column == 'prospectCall.callStartTime') {
            	// 		$scope.dateSortFlag = true;
            	// 	}
            	// 	if(column == 'prospectCall.dispositionStatus') {
            	// 		$scope.disposeSortFlag = true;
            	// 	}
            	// 	if(column == 'prospectCall.callDuration') {
            	// 		$scope.durationSortFlag = true;
                //     }
                //     if(column == 'prospectCall.leadStatus') {
            	// 		$scope.leadStatusSortFlag = true;
            	// 	}
            	// 	if(column == 'qaFeedback.overallScore') {
            	// 		$scope.statusScoreFlag = true;
            	// 	}
            		
            		
                //     var sorts = $scope.sorts;
                //     $scope.main.direction = "ASC";
                //     $scope.sortKey = column;
                    
                //     if (sorts.active == column) {
                //     	sorts.descending = !sorts.descending;
                //     	if(sorts.descending == false) {
                //     		$scope.main.direction = "ASC";                    		
                //     	} else {
                //     		$scope.main.direction = "DESC";                    		
                //     	}
                //     } else {
                //     	sorts.active = column;
                //     	sorts.descending = false;
                //     	$scope.main.direction = "ASC";
                //     }
                //     $scope.sortColumn = column;
                //     $scope.direction = $scope.main.direction;
                //     $scope.filterAgent();
                // };
            	
                

                $scope.main = {
                    page: 0,
                    take: 5,
                    direction: "DESC",
                    sort: "prospectCall.callStartTime",
                    start: 1,
                    end: 10,
                };

                $scope.notSorted = function(obj) {
                    if (!obj) {
                        return [];
                    }
                    return Object.keys(obj);
                }

                $scope.convertSecToMin = function(seconds) {                   
                	return new Date(1970, 0, 1).setSeconds(seconds);
                }

                $scope.pageChanged = function(selectedStatus, selectedName, currentPage, pageSize, disposition) {
                        $scope.loading = true;
                        var filterObject = {
                            "clause": "ByQa",
                            "direction": $scope.direction,
                            "sort": $scope.sortColumn,
                            "name": selectedName,
                            "page": currentPage - 1,
                            "size": pageSize,
                            "teamIds":$scope.allTeamIds,
                            "campaignIds":$scope.qaCampaigns,
            				"agentIds":$scope.agentIds,
            				"leadStatus": $scope.leadStatuses,
            				"toDate":$scope.qa.endDate,
            				"fromDate":$scope.qa.startDate,
            				"callDurationRangeStart":$scope.qa.startCallDuration,
            				"callDurationRangeEnd":$scope.qa.endCallDuration,
            				"startHour":$scope.qa.startHour,
            				"endHour":$scope.qa.endHour,
            				"startMinute":$scope.qa.startMinute,
            				"endMinute":$scope.qa.endMinute,
            				// "disposition":$scope.alldisposition,
            				"disposition":["SUCCESS"],
            				"subStatus":$scope.allsubstatus,
            				"prospectCallIds":$scope.allprospectCallId,
            				"clientId":$scope.agentSearchBy.selectedClientId,
            				"prospectCallStatus":$scope.agentSearchBy.qastatus,
                            "timeZone" : timeZone,
                            "qaSearchString": $scope.searchLead
                        };
                        qaProspectListService.query({
                            searchstring: angular.toJson(filterObject)
                        }).$promise.then(function(data) {
                            $scope.prospectCallList = data;
                            if($scope.prospectCallList.prospectCallSearchList.length < 1){
                                NotificationService.log('success', 'There is no data.');
                            }
                            $scope.totalRecordCount = data.prospectCallCount;
                            $scope.currentPageLength = data.prospectCallSearchList.length;
                            $scope.noSize = false;
                            $scope.noStatus = false;
                            saveDataInLocalStorage();
                            if ($scope.totalRecordCount == 0) {
                                $scope.noStatus = true;
                                $scope.noQA = false;
                            }
                            if ($scope.currentPageLength < 10) {
                                $scope.noSize = true;
                            }
                            $scope.loading = false;
                        });
                    } //End of pageChanged function

                /* End of Pagination Code */


            /* Below download report code is commented because download is enabled only for secondary qa for INTERNAL and CLIENT organization level. */
            //     $scope.downloadReport = function() {
            //         $scope.loading = true;
            //         if ($scope.loggedInUserOrganization.name == "DEMANDSHORE") {
            //             if ($scope.qaCampaigns == null || $scope.qaCampaigns == undefined || $scope.qaCampaigns.length == 0) {
            //                 $scope.setAllCampaignIds();
            //             }
            //         } else {
            //             if ($scope.qaCampaigns == null || $scope.qaCampaigns == undefined || $scope.qaCampaigns.length == 0) {
            //                 NotificationService.log('error', 'Please select one campaign.');
            //                 $scope.loading = false;
            //                 return;
            //             }
            //         }
            //         var downloadCriteriaObject = {
            //             "clause": "ByQa",
            //             "direction": $scope.direction,
            //             "sort": $scope.sortColumn,
            //             "teamIds":$scope.allTeamIds,
            //             "campaignIds":$scope.qaCampaigns,
            //             "agentIds":$scope.agentIds,                				
            //             "toDate":$scope.qa.endDate,
            //             "fromDate":$scope.qa.startDate,
            //             "callDurationRangeStart":$scope.qa.startCallDuration,
            //             "callDurationRangeEnd":$scope.qa.endCallDuration,
            //             "startHour":$scope.qa.startHour,
            //             "endHour":$scope.qa.endHour,
            //             "startMinute":$scope.qa.startMinute,
            //             "endMinute":$scope.qa.endMinute,
            //             "leadStatus": $scope.leadStatuses,
            //             "disposition":$scope.alldisposition,
            //             "subStatus":$scope.allsubstatus,
            //             "prospectCallIds":$scope.allprospectCallId,
            //             "clientId":$scope.agentSearchBy.selectedClientId,
            //             "prospectCallStatus":$scope.agentSearchBy.qastatus,
            //             "timeZone" : timeZone
            //         }
            //         if ($scope.loggedInUserOrganization.name == "DEMANDSHORE") {
            //             $scope.disableReportDownloadButton = true;
            //             DemandshoreLeadReportService.query({downloadCriteriaObject : angular.toJson(downloadCriteriaObject)}, null).$promise.then(function(leadreportData) {
            //                 if (leadreportData) {
            //                     $scope.loading = false;
            //                     NotificationService.log('success','Lead report sent successfully.');
            //                 }
            //             });
            //         } else {
            //         if ($scope.loggedInUserOrganization.organizationLevel == "INTERNAL") {
            //              if ($scope.qaCampaigns.length > 1) {
            //                 NotificationService.log('error', 'Please select only one campaign.');
            //                 $scope.loading = false;
            //                 return;
            //             }
            //         	// Normal report download - start
            //             // window.location.href = "/spr/leadreport/filter/client?searchstring=" + angular.toJson(downloadCriteriaObject);    
            //         	// Normal report download - end

            //         	// This is for thread logic - start
            //             $scope.disableReportDownloadButton = true;
            //             ClientLeadReportService.query({downloadCriteriaObject : angular.toJson(downloadCriteriaObject)}, null).$promise.then(function(leadreportData) {
            //                 if (leadreportData) {
            //                     $scope.loading = false;
            //                     NotificationService.log('success','LeadReport will be sent to reports@xtaascorp.com');
            //                 }
            //             });
            //             // thread logic - end.
            //         } else {
            //                 if ($scope.qaCampaigns.length > 1) {
            //                     NotificationService.log('error', 'Please select only one campaign.');
            //                     $scope.loading = false;
            //                     return;
            //                 }
            //                 window.location.href = "/spr/leadreport/filter/connect?searchstring=" + angular.toJson(downloadCriteriaObject);        
            //         }
            //         $scope.loading = false;
            //     }
            // }

            // $scope.setAllCampaignIds = function() {
            //     if ($scope.qa.campaignList != null && $scope.qa.campaignList != undefined) {
            //         $scope.qaCampaigns = [];
            //         angular.forEach($scope.qa.campaignList, function(value, key) { 
            //             if (value != null && value != undefined && value != '') {
            //                 $scope.qaCampaigns.push(value.key);
            //             }
            //         });
            //     }
            // }

                /* Audio popup */
                $scope.audioClick = false;
                $scope.showaudiopopup = function(prospectDetail, index) {
                    var url = "";   // If AWS recording url found in the prospect link that otherwise link twilio recording url on QA screen.  
                    if (prospectDetail.prospectCall.recordingUrlAws != null) {
                        url = prospectDetail.prospectCall.recordingUrlAws; 
                    } else {
                        url = prospectDetail.prospectCall.recordingUrl;
                    }
                    $scope.currentRecordingUrl = $sce.trustAsResourceUrl(url);
                    $scope['audioClick' + index] = true;
                    var modalInstance = $modal.open({
                        scope: $scope,
                        templateUrl: 'showaudiopopup.html',                        
                        controller: ModalInstanceCtrl,
                        backdrop: 'static',
                        windowClass: 'showaudiopopup'
                    }); 
                    
                    modalInstance.result.then(function () {
        			    
           		 	}, function () {
           		 		for(var i=0; i< $scope.totalRecordCount; i++){
           		 			$scope['audioClick' + i] = false;
           		 		}           		    
           		 	});
                }

                var ModalInstanceCtrl = function($scope, $modalInstance, NotificationService) {
                    $scope.cancel = function() {                    	
                    	$scope.audioClick = false;
                    	for(var i=0; i< $scope.totalRecordCount; i++){
                    		$scope.$parent['audioClick' + i] = false;
                    	}
                        $modalInstance.dismiss('cancel');                        
                        document.getElementsByClassName('modal-body')[0].style.display = 'none';
                        $scope.currentRecordingUrl = "";
                    };
                }

                $scope.goto = function(id, dispositionStatus) {
                    $scope.loading = true;
                    // if (dispositionStatus == 'CALLBACK') {
                    //     $scope.loading = false;
                    //     NotificationService.log('error', 'Scoring of calls disposed as Callback is not supported');
                    // } else 
                    if (dispositionStatus == 'DIALERCODE') {
                    	$scope.loading = false;
                        NotificationService.log('error', 'Scoring of calls disposed as DialerCode is not supported');
                    } else {
                        $state.go('qadetail', {
                            'prospectCallId': id
                        });
                    }
                }

                $scope.searchProspect = function() {
                    if ($scope.qaCampaigns == null || $scope.qaCampaigns == undefined || $scope.qaCampaigns.length == 0) {
                        $scope.searchLead = "";
                        NotificationService.log('error', 'Please select one campaign.');
                        $scope.loading = false;
                        return;
                    }
                    $scope.loading = true;
                    var filterObject = {
                        "clause": "ByQa",
                        "teamIds": $scope.allTeamIds,
                        "campaignIds": $scope.qaCampaigns,
                        "agentIds": $scope.agentIds,
                        "sort": $scope.sortColumn,
                        "direction": $scope.direction,
                        "page": $scope.currentPage - 1,
                        "size": $scope.pageSize,
                        "toDate": $scope.qa.endDate,
                        "fromDate": $scope.qa.startDate,
                        "callDurationRangeStart": $scope.qa.startCallDuration,
                        "callDurationRangeEnd": $scope.qa.endCallDuration,
                        "startHour": $scope.qa.startHour,
                        "endHour": $scope.qa.endHour,
                        "startMinute": $scope.qa.startMinute,
                        "endMinute": $scope.qa.endMinute,
                        "leadStatus": $scope.leadStatuses,
                        "disposition":["SUCCESS"],
                        "clientId": $scope.agentSearchBy.selectedClientId,
                        "prospectCallStatus": $scope.agentSearchBy.qastatus,
                        "timeZone": timeZone,
                        "qaSearchString": $scope.searchLead
                    };
                    qaLeadSearchService.query({
                        searchstring: angular.toJson(filterObject)
                    }).$promise.then(function(data) {
                        $scope.prospectCallList = data;
                        if($scope.prospectCallList.prospectCallSearchList.length < 1){
                            NotificationService.log('success', 'There is no data.');
                        }
                        $scope.totalRecordCount = data.prospectCallCount;
                        $scope.loading = false;
                    }, function(error) {
                        $scope.loading = false;
                    });
                } 


                /* Reset search dropdown */
                $scope.reset = function() {
                    $scope.isResetButtonClicked = true;
                	$scope.searchLead = ""; // reset search input box
                    // delete campaignList and agentList to bind new list fertched.
                    $scope.disableReportDownloadButton = false;
                	delete $localStorage.agentList;
                    delete $localStorage.campaignList;
                    delete $localStorage.qaCampaign;
                    delete $localStorage.filterCriteriaObj;
                    $scope.agentSearchBy.dispositionStatus = "";
                    $scope.agentSearchBy.subStatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.allTeamIds = [];
                    $scope.agentIds = [];
                    $scope.leadStatuses = [];
                    $scope.qaCampaigns = [];
                    $scope.agentSearchBy.selectedAgentMulti2 = [];
                    $scope.agentSearchBy.selectedAgentMulti = [];
                    $scope.leadStatusSearchBy.selectedStatusMulti2 = [];
                    $scope.leadStatusSearchBy.selectedStatusMulti = [];
                    $scope.campaignSearchBy.selectedCampaignMulti = [];
                    $scope.campaignSearchBy.selectedCampaignMulti2= [];
                    $scope.agentSearchBy.selectedTeam = null;
                    $scope.agentSearchBy.selectedCampaign = null;
                    $scope.agentSearchBy.selectedAgent = null;
                    $scope.agentSearchBy.duration = null;
                    $scope.agentSearchBy.quickDate = null;
                    $scope.qa.startDate = null;
                    $scope.qa.endDate = null;
                    $scope.agentSearchBy.selectedClientId = null;
                    $scope.agentSearchBy.qastatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.agentSearchBy.startTime = null;
                    $scope.agentSearchBy.endTime = null;
                    $scope.agentSearchBy.quickTime = null;
                    //$scope.agentSearchBy.quickTime = null;
                    $scope.searchKeyword = null;
                    $scope.qa.startCallDuration = null;
                    $scope.qa.endCallDuration = null;
                    $scope.qa.startHour = null;
                    $scope.qa.endHour = null;
                    $scope.qa.startMinute = null;
                    $scope.qa.endMinute = null;
                    $scope.agentSearchBy.selectedDispositionStatus = "";
                    
                    //Hide multiselect dropdown list                    
                    $scope.hideMultiSelectPop(true);
                    
                    //Reset Filter top right background color
                    $scope.callIdSelected = false;
                    $scope.teamSelected = false;
                    $scope.agentSelected = false;
                    $scope.campaignSelected = false;                
                    $scope.sDateSelected = false;                
                    $scope.disposeSelected = false;
                    $scope.durSelected = false;
                    $scope.leadStatusSelected = false;

                    // reset sorting to fetch PRIMARY_REVIEW records first.
                    $scope.sortKey = null;
                    $scope.main.direction = null;
                    $scope.currentPage = 1;

                    // Reset All Search criterias
                    $scope.resetSearchCriteria();
                    
                    $scope.setQuickTime();
                    $scope.getDetailsWithoutProspects();

                     // reset sort flags
                     $scope.qaIdSortFlag = false;
                     $scope.callIdSortFlag = false;
                     $scope.agentSortFlag = false;
                     $scope.campaignSortFlag = false;
                     $scope.dateSortFlag = false;
                     $scope.disposeSortFlag = false;
                     $scope.durationSortFlag = false;
                     $scope.leadStatusSortFlag = false;
                     $scope.statusScoreFlag = false;
                }

                //Hide search criteria on click on close button
                $scope.hideAgentCriteria = function (key, index, val) {                	
                		$scope.agentSelected[key] = false;
                		if($scope.agentSearchBy.selectedAgentMulti != undefined && $scope.agentSearchBy.selectedAgentMulti.length > 0) {  
                            var indx = $scope.agentSearchBy.selectedAgentMulti.indexOf(key);
                            $scope.agentSearchBy.selectedAgentMulti.splice(indx, 1);                			
                            var indx = $scope.agentSearchBy.selectedAgentMulti2.indexOf(key);
                			$scope.agentSearchBy.selectedAgentMulti2.splice(indx, 1);        
                        } else {                			
                            var indx = $scope.agentSearchBy.selectedAgentMulti2.indexOf(key);
                			$scope.agentSearchBy.selectedAgentMulti2.splice(indx, 1);
                		}
                		delete $scope.agentSelectedText[0][key];
                		var tempObj = {};
                		for(var i=0; i< Object.keys($scope.agentSelectedText[0]).length; i++) {
                    		tempObj[Object.keys($scope.agentSelectedText[0])[i]] = $scope.agentSelectedText[0][Object.keys($scope.agentSelectedText[0])[i]];
                		}                		
                		$scope.agentSelectedText[0] = tempObj;
                		$scope.teamSelected = false;  
                		$scope.filterAgent();
                }

                //Hide qa criteria on click on close button
                $scope.hideLeadStatusCriteria = function (key, index, val) {                	
                    $scope.statusSelected[key] = false;
                    if ($scope.leadStatusSearchBy.selectedStatusMulti != undefined) { 
                            $scope.leadStatusSearchBy.selectedStatusMulti.splice(index, 1);                			
                       } else if ($scope.leadStatusSearchBy.selectedStatusMulti2 != undefined) {                			
                            $scope.leadStatusSearchBy.selectedStatusMulti2.splice(index, 1);
                    }
                    delete $scope.leadStatusSelectedText[0][key];
                    var tempObj = {};
                    for(var i=0; i< Object.keys($scope.leadStatusSelectedText[0]).length; i++) {
                        tempObj[Object.keys($scope.leadStatusSelectedText[0])[i]] = $scope.leadStatusSelectedText[0][Object.keys($scope.leadStatusSelectedText[0])[i]];
                    }                		
                    $scope.leadStatusSelectedText[0] = tempObj;
                    $scope.leadStatusSelected = false;     
                    $scope.filterAgent();
            }


            $scope.hideCampaignCriteria = function (key, index, val) {                	
                $scope.qaCampaignSelected[key] = false;
                if($scope.campaignSearchBy.selectedCampaignMulti != undefined && $scope.campaignSearchBy.selectedCampaignMulti.length > 0) {  
                    var indx = 	$scope.campaignSearchBy.selectedCampaignMulti.indexOf(key);
                    $scope.campaignSearchBy.selectedCampaignMulti.splice(indx, 1);                			
                } else {                			
                    var indx = 	$scope.campaignSearchBy.selectedCampaignMulti2.indexOf(key);
                    $scope.campaignSearchBy.selectedCampaignMulti2.splice(indx, 1);
                }
                delete $scope.campaignSelectedText[0][key];
                var tempObj = {};
                for(var i=0; i< Object.keys($scope.campaignSelectedText[0]).length; i++) {
                    tempObj[Object.keys($scope.campaignSelectedText[0])[i]] = $scope.campaignSelectedText[0][Object.keys($scope.campaignSelectedText[0])[i]];
                }                		
                $scope.campaignSelectedText[0] = tempObj;
                $scope.campaignSelected = false;     
                $scope.filterAgent();
        }
                
                $scope.hideCriteria = function (skey) {
                	$scope[skey] = false;
                	                	
                	if(skey == 'callIdSelectedText') {                		
                		$scope.agentSearchBy.callId = null;
                		$scope.callIdSelected = false;
                	}
                	if(skey == 'teamSelectedText') {
                		$scope.allTeamIds = [];
                		$scope.agentSearchBy.selectedTeam = null;
                		$scope.teamSelected = false;
                	}
                	if(skey == 'agentSelectedText') {
                		$scope.agentIds = [];
                		$scope.agentSearchBy.selectedAgent = null;
                		$scope.teamSelected = false;
                	}
                	if(skey == 'campaignSelectedText') {
                		$scope.qaCampaigns = [];
                		$scope.campaignSelected = false;
                	}
                	if(skey == 'clientSelectedText') {
                		$scope.agentSearchBy.selectedClientId = null;
                		$scope.campaignSelected = false;
                	}
                	if(skey == 'sDateSelectedText') {
                		$scope.sDateSelectedText = "";
                		$scope.eDateSelectedText = "";
                		$scope.qa.startDate = null;
                		$scope.qa.endDate = null;
                		$scope.sDateSelected = false;                		
                	}
                	if(skey == 'eDateSelectedText') {
                		$scope.sDateSelectedText = "";
                		$scope.eDateSelectedText = "";
                		$scope.qa.startDate = null;
                		$scope.qa.endDate = null;
                		$scope.sDateSelected = false;
                	}
                	if(skey == 'sTimeSelectedText') {
                		$scope.sTimeSelectedText = "";
                		$scope.eTimeSelectedText = "";
                		$scope.agentSearchBy.startTime = null;
                		$scope.agentSearchBy.endTime = null;
                		$scope.qa.startHour = null;
                		$scope.qa.endHour = null;
                		$scope.sDateSelected = false;
                		$scope.agentSearchBy.quickTime = null;
                	}
                	if(skey == 'eTimeSelectedText') {
                		$scope.sTimeSelectedText = "";
                		$scope.eTimeSelectedText = "";
                		$scope.agentSearchBy.startTime = null;
                		$scope.agentSearchBy.endTime = null;
                		$scope.qa.startHour = null;
                		$scope.qa.endHour = null;
                		$scope.sDateSelected = false;
                		$scope.agentSearchBy.quickTime = null;
                	}
                	if(skey == 'disposeSelectedText') {
                		$scope.agentSearchBy.selectedDispositionStatus = "";
                		$scope.agentSearchBy.dispositionStatus = "";
                		$scope.disposeSelected = false;                		
                	}
                	if(skey == 'durSelectedText') {
                        $scope.agentSearchBy.duration = null;
                        $scope.qa.startCallDuration = null;
                        $scope.qa.endCallDuration = null;
                		$scope.durSelected = false;                		
                    }
                    if(skey == 'leadStatusSelectedText') {
                        $scope.leadStatuses = [];
                		$scope.leadStatusSelected = false;                		
                	}
                	if(skey == 'setProspCalltxt') {
                		$scope.agentSearchBy.qastatus = null;                		          		
                	}
                	if(skey == 'setDispStatustxt') {
                		$scope.qa.startDate = ""
                        $scope.qa.endDate = ""                		                		          		
                	}
                	
                	$scope.filterAgent();
                }
                
              //Reset all pagination filter popups
                $scope.resetPop = function() {
                	$scope.showHidecall = false;
                    $scope.showHideagent = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;
                    $scope.showHidedispos = false;
                    $scope.showHidedur = false;
                    $scope.showHideLeadStatus = false;
                }
                
              //Reset and hide call id filter popup
                $scope.resetCall = function() {
                	if($scope.callIdSelectedText != null && $scope.callIdSelectedText != "") {
                		$scope.agentSearchBy.callId = $scope.callIdSelectedText;
                	} else {
                		$scope.agentSearchBy.callId = '';
                	}
                	$scope.showHidecall = false;                	
                }
                
              //Reset and hide agent filter popup
                $scope.resetAgent = function() {
                	$scope.showHideagent = false;
                }

                //Reset and hide agent filter popup
                $scope.resetLeadStatus = function() {
                	$scope.showHideLeadStatus = false;
                }
                
              //Reset and hide client filter popup
                $scope.resetClient = function() {
                	$scope.showHideclient = false;
                }
                
              //Reset and hide date / time filter popup
                $scope.resetDate = function() {
                	if($scope.sDateSelectedText != null && $scope.sDateSelectedText != "") {
                		$scope.qa.startDate = $scope.sDateSelectedText;
                	} else {
                		$scope.qa.startDate = "";
                	}
                	if($scope.eDateSelectedText != null && $scope.eDateSelectedText != "") {
                		$scope.qa.endDate = $scope.eDateSelectedText;
                	} else {
                		$scope.qa.endDate = "";
                	}
                	if($scope.sTimeSelectedText != null && $scope.sTimeSelectedText != "") {
                		$scope.agentSearchBy.startTime = $filter('filter')($scope.startHourList, $scope.sTimeSelectedText)[0].key;
                	} else {
                		$scope.agentSearchBy.startTime = null;
                	}
                	if($scope.eTimeSelectedText != null && $scope.eTimeSelectedText != "") {
                		$scope.agentSearchBy.endTime = $filter('filter')($scope.endHourList, $scope.eTimeSelectedText)[0].key;
                	} else {
                		$scope.agentSearchBy.endTime = null;
                	}
                	$scope.showHidedate = false;
                }
                //Reset and hide disposition filter popup                
                $scope.resetDispose = function() {
                	if($scope.disposeSelectedText != null && $scope.disposeSelectedText != "") {
                		$scope.agentSearchBy.selectedDispositionStatus = $scope.disposeSelectedText;
                	} else {
                		$scope.agentSearchBy.selectedDispositionStatus = "";
                	}
                	$scope.showHidedispos = false;
                }
                
                //Reset and hide duration filter popup
                $scope.resetDur = function() {
                	if($scope.durSelectedText != null && $scope.durSelectedText != "") {
                		$scope.agentSearchBy.duration = $scope.durSelectedText;
                	} else {
                		$scope.agentSearchBy.duration = null;
                	}
                	$scope.showHidedur = false;
                }

                // Reset and hide leadStatus filter popup
                $scope.setLeadStatus = function() {
                    $scope.leadStatuses = [];
                    if ($scope.leadStatusSearchBy.selectedStatusMulti != undefined) {
                        if($scope.leadStatusSearchBy.selectedStatusMulti.length > 0) {
                            $scope.leadStatusSearchBy.selectedStatusMulti2 = [];
                            for(i=0;i < $scope.leadStatusSearchBy.selectedStatusMulti.length;i++) {
                                $scope.leadStatuses.push($scope.leadStatusSearchBy.selectedStatusMulti[i]);	
                            }                    	
                        }
                    }
                    
                    if ($scope.leadStatusSearchBy.selectedStatusMulti2 != undefined) {
                        if($scope.leadStatusSearchBy.selectedStatusMulti2.length > 0) {
                            $scope.leadStatusSearchBy.selectedStatusMulti = [];
                            for(i=0;i < $scope.leadStatusSearchBy.selectedStatusMulti2.length;i++) {
                                $scope.leadStatuses.push($scope.leadStatusSearchBy.selectedStatusMulti2[i]);	
                            }                    	
                        }
                    }
                    $scope.showHideLeadStatus = false;
                }

                // Reset and hide campaign filter popup
                $scope.setQaCampaign = function() {
                if ($scope.campaignSearchBy.selectedCampaignMulti != undefined) {
                    if($scope.campaignSearchBy.selectedCampaignMulti.length > 0) {
                        $scope.campaignSearchBy.selectedCampaignMulti2 = [];
                        for(i=0;i < $scope.campaignSearchBy.selectedCampaignMulti.length;i++) {
                            $scope.qaCampaigns.push($scope.campaignSearchBy.selectedCampaignMulti[i]);	
                        }                    	
                    }
                }
                    
                if ($scope.campaignSearchBy.selectedCampaignMulti2 != undefined) {
                    if($scope.campaignSearchBy.selectedCampaignMulti2.length > 0) {
                        $scope.campaignSearchBy.selectedCampaignMulti = [];
                        for(i=0;i < $scope.campaignSearchBy.selectedCampaignMulti2.length;i++) {
                            $scope.qaCampaigns.push($scope.campaignSearchBy.selectedCampaignMulti2[i]);	
                        }                    	
                    }
                }
                	$scope.showHideclient = false;
                }

                $scope.showHidecall = false;
                $scope.showHideagent = false;
                $scope.showHideclient = false;
                $scope.showHidedate = false;
                $scope.showHidedispos = false;
                $scope.showHidedur = false;
                $scope.showHideLeadStatus = false;
                
                //When Call Id filter selected
                $scope.showHideCallSearch = function(){                	
                    $scope.showHideagent = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;
                    $scope.showHidedispos = false;
                    $scope.showHidedur = false;
                	$scope.showHideLeadStatus = false;
                	if($scope.showHidecall == false){
                		$scope.showHidecall = true;	
                	} else {
                		$scope.showHidecall = false;
                	}
                }
                
                //When agent filter selected
                $scope.showHideAgentSearch = function(){                	
                	$scope.showHidecall = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;
                    $scope.showHidedispos = false;
                    $scope.showHidedur = false;
                    $scope.showHideLeadStatus = false;
                	if($scope.showHideagent == false){
                		$scope.showHideagent = true;	
                	} else {
                		$scope.showHideagent = false;
                	}
                }
                
                //When client filter selected
                $scope.showHideClientSearch = function(){                	
                	$scope.showHidecall = false;
                	$scope.showHideagent = false;
                    $scope.showHidedate = false;                    
                    $scope.showHidedispos = false;                    
                    $scope.showHidedur = false;
                    $scope.showHideLeadStatus = false;
                	if($scope.showHideclient == false){
                		$scope.showHideclient = true;	
                	} else {
                		$scope.showHideclient = false;
                	}
                }
                
                //When Date / time filter selected
                $scope.showHideDateSearch = function(){                	
                	$scope.showHidecall = false;
                	$scope.showHideagent = false;
                    $scope.showHideclient = false;                    
                    $scope.showHidedispos = false;
                    $scope.showHidedur = false;
                    $scope.showHideLeadStatus = false;
                	if($scope.showHidedate == false){
                		$scope.showHidedate = true;	
                	} else {
                		$scope.showHidedate = false;
                	}
                }
                
                //When disposition filter selected
                $scope.showHideDisposSearch = function(){                	
                	$scope.showHidecall = false;
                	$scope.showHideagent = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;
                    $scope.showHidedur = false;
                    $scope.showHideLeadStatus = false;
                	if($scope.showHidedispos == false){
                		$scope.showHidedispos = true;	
                	} else {
                		$scope.showHidedispos = false;
                	}
                }
                
                //When duration filter selected
                $scope.showHideDurSearch = function(){                	
                	$scope.showHidecall = false;
                	$scope.showHideagent = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;                                        
                    $scope.showHidedispos = false;
                    $scope.showHideLeadStatus = false;
                	if($scope.showHidedur == false){
                		$scope.showHidedur = true;	
                	} else {
                		$scope.showHidedur = false;
                	}
                }

                //When status filter selected
                $scope.showHideLeadStatusSearch = function(){                	
                	$scope.showHidecall = false;
                	$scope.showHideagent = false;
                    $scope.showHideclient = false;
                    $scope.showHidedate = false;                                        
                    $scope.showHidedispos = false;
                    $scope.showHidedur = false;
                	if($scope.showHideLeadStatus == false){
                		$scope.showHideLeadStatus = true;	
                	} else {
                		$scope.showHideLeadStatus = false;
                	}
                }

                /* nvD3 graph initialization */
                $scope.colorFunction2 = function() {
    				return function(d, i) {
    					if(d.key == "Failure"){
    				     	return '#E6705F'
    				    }
    				    if(d.key == "Success"){
    				    	return '#36D7B6'
    				    }
    				    if(d.key == "Callback"){
    				    	return '#F4BA70'
    				    }
    				    if(d.key == "Dialercode"){
    				    	return '#8E9699'
    				    }
    				};
    			}
                
                $scope.toolTipContentFunction = function(){
                	return function(key, x, y, e, graph) {
                    	return  '<h6>' + key + '</h6>'                            
                	}
                }

            
                $scope.colorFunction = function() {
    			return function(d, i) {			
    			    if(d.data.key == "Failure"){
    			     	return '#E6705F'
    			    }
    			    if(d.data.key == "Success"){
    			    	return '#36D7B6'
    			    }
    			    if(d.data.key == "Callback"){
    			    	return '#F4BA70'
    			    }
    			    if(d.data.key == "Dialercode"){
    			    	return '#8E9699'
    			    }
    		    };
    		}
            
                $scope.xFunction = function(){
                    return function(d) {                
                        return d.key +' : '+ d.y;
                    };
                }
                $scope.yFunction = function(){
                    return function(d) {
                        return d.y;
                    };
                }

                $scope.descriptionFunction = function(){
                    return function(d){
                        return d.key;
                    }
                }
                /* end of nvD3 graph initialization */
                
                $scope.setAgentArr = function(){                	
                	if($scope.agentSearchBy.selectedAgentMulti.length > 0) {
                		$scope.agentSearchBy.selectedAgentMulti2 = [];
                    	for(i=0;i < $scope.agentSearchBy.selectedAgentMulti.length;i++){
                    		$scope.agentIds.push($scope.agentSearchBy.selectedAgentMulti[i]);	
                    	}                    	
                    }
                	
                	if($scope.agentSearchBy.selectedAgentMulti2.length > 0) {
                		$scope.agentSearchBy.selectedAgentMulti = [];
                    	for(i=0;i < $scope.agentSearchBy.selectedAgentMulti2.length;i++){
                    		$scope.agentIds.push($scope.agentSearchBy.selectedAgentMulti2[i]);	
                    	}                    	
                    }
                }
                
                $scope.convert = function (str) {
                    var date = new Date(str),
                        mnth = ("0" + (date.getMonth()+1)).slice(-2),
                        day  = ("0" + date.getDate()).slice(-2);
                    return [ date.getFullYear(), mnth, day ].join("-");
                }
                
                $scope.preventScoredBlock = false;
                $scope.setCallStatus = function(key, key1, key2){
                	$scope.preventScoredBlock = true;
                	
                	$scope.agentSearchBy.dispositionStatus = "";
                    $scope.agentSearchBy.subStatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.allTeamIds = [];
                    $scope.agentIds = [];
                   /// $scope.agentSearchBy.selectedAgentMulti2 = [];
                   /// $scope.agentSearchBy.selectedAgentMulti = [];
                    $scope.agentSearchBy.selectedTeam = null;
                   /// $scope.agentSearchBy.selectedCampaign = null;
                    $scope.agentSearchBy.selectedAgent = null;
                    $scope.agentSearchBy.duration = null;
                    $scope.leadStatuses = [];
                    $scope.qaCampaigns = [];
                    $scope.agentSearchBy.quickDate = null;
                    $scope.qa.startDate = null;
                    $scope.qa.endDate = null;
                    $scope.agentSearchBy.selectedClientId = null;
                    $scope.agentSearchBy.qastatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.agentSearchBy.startTime = null;
                    $scope.agentSearchBy.endTime = null;                    
                   /// $scope.agentSearchBy.quickTime = null;
                    $scope.searchKeyword = null;
                    $scope.qa.startCallDuration = null;
                    $scope.qa.endCallDuration = null;
                    ///$scope.qa.startHour = null;
                    ///$scope.qa.endHour = null;
                    ///$scope.qa.startMinute = null;
                    ///$scope.qa.endMinute = null;
                    $scope.agentSearchBy.selectedDispositionStatus = "";
                    
                    //Hide multiselect dropdown list                    
                    $scope.hideMultiSelectPop(true);
                    
                    //Reset Filter top right background color
                    $scope.callIdSelected = false;
                    $scope.teamSelected = false;
                    $scope.agentSelected = false;
                    $scope.campaignSelected = false;                
                    $scope.sDateSelected = false;                
                    $scope.disposeSelected = false;
                    $scope.durSelected = false;
                    $scope.leadStatusSelected = false; 
                	
                	$scope.callIdSelectedText = "";
                    $scope.teamSelectedText = "";
                    $scope.agentSelectedText = [];
                    $scope.campaignSelectedText = "";
                    $scope.clientSelectedText = "";
                    $scope.sDateSelectedText = "";
                    $scope.eDateSelectedText = "";
                    $scope.sTimeSelectedText = "";
                    $scope.eTimeSelectedText = "";
                    $scope.disposeSelectedText = "";
                    $scope.durSelectedText = "";
                    $scope.leadStatusSelectedText = "";  
                    $scope.setProspCalltxt = "";
                    $scope.setDispStatustxt = "";
                	
                	if (key!=''){
                		$scope.agentSearchBy.dispositionStatus = key;  
                    	$scope.agentSearchBy.selectedDispositionStatus = key;
                	}
                	if (key1!='' && key2==''){                		
                		$scope.agentSearchBy.qastatus = key1;
                		$scope.setProspCalltxt = key1;
                	}
                	if (key2!=''){                		  
                		var today = Date.now();
                		if(key2 == 'COUNT_LAST_WEEK') {                			
                			var today=new Date();
                			var first = new Date().setDate(today.getDate()-today.getDay()-7);
                			var last = new Date().setDate(today.getDate()-today.getDay()-1);
                			
                			var firstd = new Date(first);
                			var lastd = new Date(last);
                			
                			var firstday = firstd.toString("yyyy-MM-dd");
                			var lastday = lastd.toString("yyyy-MM-dd");
                			

                            $scope.qa.startDate = $scope.convert(firstday);
                            $scope.qa.endDate = $scope.convert(lastday);
                            $scope.setDispStatustxt = "Pending past week";
                            $scope.agentSearchBy.qastatus = key1;
                		}
                		if(key2 == 'COUNT_LAST24_HOURS') {
                			var endday = today - 86400000;
                            var startday = today - (2 * 86400000);
                            $scope.qa.startDate = $filter('date')(endday, 'yyyy-MM-dd');
                            $scope.qa.endDate = $filter('date')(today, 'yyyy-MM-dd');
                            $scope.setDispStatustxt = "Scored In Past 24 Hours";
                            $scope.agentSearchBy.qastatus = key1;
                		}
                	}
                	saveDataInLocalStorage();                	
                }
                
                $scope.resetSearchCriteria = function (){
                	//Reset All Search criterias
                    $scope.callIdSelectedText = "";
                    $scope.teamSelectedText = "";
                    $scope.agentSelectedText = [];
                    $scope.campaignSelectedText = "";
                    $scope.clientSelectedText = "";
                    $scope.sDateSelectedText = "";
                    $scope.eDateSelectedText = "";
                    $scope.sTimeSelectedText = "";
                    $scope.eTimeSelectedText = "";
                    $scope.disposeSelectedText = "";
                    $scope.durSelectedText = "";  
                    $scope.leadStatusSelectedText = "";
                    $scope.setProspCalltxt = "";
                    $scope.setDispStatustxt = "";
                }
                
                $scope.redirect = function(){
                	window.location = "#!/secondaryqa"               	
                }
                
                $scope.hideMultiSelectPop = function (flag){
                  if(flag == true) {
                	  //document.getElementsByClassName('btn-group').className = "abcd";
                	  $(".btn-group").removeClass("open");
                  }
                }
                
                $scope.setClientAgent = function() {                	
                    $scope.preventScoredBlock = true;
                    $scope.disableReportDownloadButton = false;
                }
                
                $scope.setAllowClientAgent = function() {
                	$scope.preventScoredBlock = false;
                }
                
                $scope.setClickable = function (val,subval) {
                	
                  if(val != undefined && val != 0) {   
                	$scope.agentSearchBy.dispositionStatus = "";
                    $scope.agentSearchBy.subStatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.allTeamIds = [];
                    $scope.agentIds = [];
                    $scope.leadStatuses = [];
                    $scope.qaCampaigns = [];
                    ///$scope.agentSearchBy.selectedAgentMulti2 = [];
                   /// $scope.agentSearchBy.selectedAgentMulti = [];
                    $scope.agentSearchBy.selectedTeam = null;
                    ///$scope.agentSearchBy.selectedCampaign = null;
                    $scope.agentSearchBy.selectedAgent = null;
                    $scope.agentSearchBy.duration = null;
                    $scope.agentSearchBy.quickDate = null;
                    $scope.qa.startDate = null;
                    $scope.qa.endDate = null;
                    $scope.agentSearchBy.selectedClientId = null;
                    $scope.agentSearchBy.qastatus = null;
                    $scope.agentSearchBy.callId = null;
                    $scope.agentSearchBy.startTime = null;
                    $scope.agentSearchBy.endTime = null;                    
                    ///$scope.agentSearchBy.quickTime = null;
                    $scope.searchKeyword = null;
                    $scope.qa.startCallDuration = null;
                    $scope.qa.endCallDuration = null;
                   /// $scope.qa.startHour = null;
                   /// $scope.qa.endHour = null;
                   /// $scope.qa.startMinute = null;
                   /// $scope.qa.endMinute = null;
                    $scope.agentSearchBy.selectedDispositionStatus = "";
                    
                    //Hide multiselect dropdown list                    
                    $scope.hideMultiSelectPop(true);
                    
                    //Reset Filter top right background color
                    $scope.callIdSelected = false;
                    $scope.teamSelected = false;
                    $scope.agentSelected = false;
                    $scope.campaignSelected = false;                
                    $scope.sDateSelected = false;                
                    $scope.disposeSelected = false;
                    $scope.durSelected = false;
                    $scope.leadStatusSelected = false;
                	$scope.callIdSelectedText = "";
                    $scope.teamSelectedText = "";
                    $scope.agentSelectedText = [];
                    $scope.campaignSelectedText = "";
                    $scope.clientSelectedText = "";
                    $scope.sDateSelectedText = "";
                    $scope.eDateSelectedText = "";
                    $scope.sTimeSelectedText = "";
                    $scope.eTimeSelectedText = "";
                    $scope.disposeSelectedText = "";
                    $scope.durSelectedText = ""; 
                    $scope.leadStatusSelectedText = ""; 
                    $scope.setProspCalltxt = "";
                    $scope.setDispStatustxt = "";            		
                	$scope.setCallStatus(subval,'','');
                	$scope.filterAgent();                		
                	}
                }
                
                
                
                //Initialize agent multiselect dropdown
                $scope.member = {roles: []};
                $scope.agentSearchBy.selectedAgentMulti = [];
                $scope.agentSearchBy.selectedAgentMulti2 = [];

                //Initialize Lead Status Multiselect dropdown
                if ($scope.leadStatusSearchBy.selectedStatusMulti == undefined) {
                    $scope.leadStatusSearchBy.selectedStatusMulti = [];
                }
                else {
                    if ($scope.leadStatusSearchBy.selectedStatusMulti.length == 0) {
                        $scope.leadStatusSearchBy.selectedStatusMulti = [];
                    }
                }

                if ($scope.leadStatusSearchBy.selectedStatusMulti2 == undefined) {
                    $scope.leadStatusSearchBy.selectedStatusMulti2 = [];
                }
                else {
                    if ($scope.leadStatusSearchBy.selectedStatusMulti2.length == 0) {
                        $scope.leadStatusSearchBy.selectedStatusMulti2 = [];
                    }
                }

                //Initialize Campaign Multiselect dropdown
                if ($scope.campaignSearchBy.selectedCampaignMulti == undefined) {
                    $scope.campaignSearchBy.selectedCampaignMulti = [];
                }
                else {
                    if ($scope.campaignSearchBy.selectedCampaignMulti.length == 0) {
                        $scope.campaignSearchBy.selectedCampaignMulti = [];
                    }
                }

                if ($scope.campaignSearchBy.selectedCampaignMulti2 == undefined) {
                    $scope.campaignSearchBy.selectedCampaignMulti2 = [];
                }
                else {
                    if ($scope.campaignSearchBy.selectedCampaignMulti2.length == 0) {
                        $scope.campaignSearchBy.selectedCampaignMulti2 = [];
                    }
                }
                
                /*	TO FETCH PRIMARY_QA_AGENTS_LIST*/
                $scope.getAgentsByQA1 = function() {
                	var searchObject = {"clause":"ByQa" };
	            	searchQaCampaignAgentService.query({searchString : angular.toJson(searchObject)}, null).$promise.then(function(agentListData) {
	            		$scope.qa.agentList = [];
						angular.forEach(agentListData,  function(agent, index) {
							$scope.qa.agentList.push({"key":agent.id,"value":agent.id+' - '+agent.firstName+' '+agent.lastName})
						});
					});      
                } 
                
                /*	TO FETCH PRIMARY_QA_CAMPAIGNS_LIST*/
                $scope.getCampaingsByQA1 = function(){
                		var searchObjectClient = {"clause":"ByQa"};
    					searchQaCampaignService.query({searchString : angular.toJson(searchObjectClient)}, null).$promise.then(function(campaignListData) {
    						$scope.qaCampaign = campaignListData;
    						$scope.qa.campaignList = [];
    						angular.forEach(campaignListData.campaignsList,  function(campaignListValue, campaignListkey){
    							$scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});   					
    							if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
    								$scope.allPartnerIds.push(campaignListValue.organizationId);
    							}
    						});
    					});	
            	 } 
    
              //on Change of Campaign show related Agents
                $scope.setAgentList = function(){ 
                	$scope.member = {roles: []};
                	$scope.agentSearchBy.selectedAgentMulti = [];
                	$scope.agentSearchBy.selectedAgentMulti2 = [];

                	if($scope.agentSearchBy.selectedCampaign != undefined && $scope.agentSearchBy.selectedCampaign != "" && $scope.agentSearchBy.selectedCampaign != null) {
                		var searchObject = {"clause":"ByCampaign", "campaignIds":$scope.agentSearchBy.selectedCampaign};
                	} else {
                		var searchObject = {"clause":"ByQa"};
                	}
                	$scope.qa.agentList = [];

                	searchQaCampaignAgentService.query({searchString : angular.toJson(searchObject)}, null).$promise.then(function(agentListData) {
                		angular.forEach(agentListData,  function(agent, index) {
                			$scope.qa.agentList.push({"key":agent.id,"value":agent.id+' - '+agent.firstName+' '+agent.lastName})
                		});
                	});                 

                }

                	// on Change Campaign show related Clients
                    $scope.setClientList = function(){
                    	$scope.preventScoredBlock = false;
                		if($scope.agentSearchBy.selectedCampaign != undefined && $scope.agentSearchBy.selectedCampaign != "" && $scope.agentSearchBy.selectedCampaign != null) {
                			$scope.campaignListArr = [];
                    		$scope.campaignListArr.push($scope.agentSearchBy.selectedCampaign);
                    		/*var searchObjectClient = {"clause":"ByQa", "ids":$scope.campaignListArr, "timeZone":timeZone,"prospectCallStatus":$scope.agentSearchBy.qastatus};
        					searchQaCampaignService.query({searchString : angular.toJson(searchObjectClient)}, null).$promise.then(function(campaignListData) {
        						$scope.qaCampaign = campaignListData;
        						angular.forEach(campaignListData.campaignsList,  function(campaignListValue, campaignListkey){
        							if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
        								$scope.allPartnerIds.push(campaignListValue.organizationId);
        							}
        						});
        					});*/
                    		$scope.qa.campaignList = [];
    						angular.forEach($scope.qaCampaign.campaignsList,  function(campaignListValue, campaignListkey) {
                                if (campaignListValue.id != undefined && campaignListValue.name != undefined) {
                                    $scope.qa.campaignList.push({"key":campaignListValue.id,"value":campaignListValue.name});
                                }
                                if (campaignListValue.key != undefined && campaignListValue.value != undefined) {
                                    $scope.qa.campaignList.push({"key":campaignListValue.key,"value":campaignListValue.value});
                                }
    							if ($scope.allPartnerIds.indexOf(campaignListValue.organizationId) == -1) {
    								$scope.allPartnerIds.push(campaignListValue.organizationId);
    							}
                            });
                            $scope.qa.campaignList.sort(function(a, b) {
                                if (a.value.toLowerCase() < b.value.toLowerCase()) return -1;
                                if (a.value.toLowerCase() > b.value.toLowerCase()) return 1;
                                return 0;
                            });
                	 } 
                }   
                   
            }
        ]).filter('capitalize', function() {
            return function(input, all) {
                return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                }) : '';
            }
        }).filter('toPercentVal', function() {
            return function(value) {
                return parseFloat(value).toFixed(0);
            };
        }).directive('dropdownMultiselect', function(){
        	   return {
        	       restrict: 'E',
        	       scope:{           
        	            model: '=',
        	            options: '=',
        	            pre_selected: '=preSelected',
        	            multiarr: '=multiarr'
        	       },
        	       template: "<div class='btn-group' data-ng-class='{open: open}'>"+
        	        "<button class='btn btn-small' data-ng-click='open=!open;openDropdown()'><span ng-show='model.length == 1' style='width: 90%;float:left;'>{{models[0] | limitTo:20}}</span><span ng-show='model.length > 1' style='width: 90%;float:left;'>{{model.length}} Agents Selected</span><span ng-show='model.length == 0' style='width: 90%;float:left;'>Agents</span> <span class='caret'></span><span class='searchicon-call-white'></span></button>"+        	                
        	                "<ul class='dropdown-menu showhidemenu' aria-labelledby='dropdownMenu'>" +                    
        	                    "<li><input type='text' placeholder='Search Agent' class='txtcls' ng-model='multifilter'></li><li class='divider'></li>" +
        	                    "<li data-ng-repeat='option in options | filter:multifilter'> <a data-ng-click='setSelectedItem()'><span class='pull-left check-option'>{{option.value}}</span><span class='pull-right' data-ng-class='isChecked(option.key)'></span></a></li>" +                                        
        	                "</ul>" +
        	            "</div>" ,
        	       controller: function($scope, $rootScope){
        	    	   
        	    	   $scope.models=[];
        	    	         	    	   
        	    	   $rootScope.opentrue = false;
        	           $scope.openDropdown = function(){   
        	        	   $rootScope.openDropdown = false;   
        	        	   $rootScope.opentrue = true;
        	        	   console.log($rootScope.opentrue);
        	               $scope.selected_items = [];
        	               for(var i=0; i<$scope.pre_selected.length; i++){ 
        	                   	$scope.selected_items.push($scope.pre_selected[i].key);
        	               }                                        
        	            };
        	           
        	            $scope.selectAll = function () {
        	                $scope.model = _.pluck($scope.options, 'key');
        	                console.log($scope.model);
        	            };            
        	            $scope.deselectAll = function() {
        	                $scope.model=[];
        	                console.log($scope.model);
        	            };
        	            $scope.setSelectedItem = function(){
        	            	$scope.multiarr = [];
        	            	$rootScope.opentrue = false;
        	                var key = this.option.key;
        	                var value = this.option.value;        	                
        	                console.log(name);
        	                if (_.contains($scope.model, key)) {        	                	
        	                    $scope.model = _.without($scope.model, key);
        	                    $scope.models = _.without($scope.models, value);        	                    
        	                } else {        	                	
        	                    $scope.model.push(key);        	                    
        	                    $scope.models = [];
        	                    $scope.models.push(value);        	                            	                    
        	                }
        	                console.log($scope.model);
        	                return false;
        	            };
        	            $scope.isChecked = function (key) {                 
        	                if (_.contains($scope.model, key)) {
        	                	$rootScope.opentrue = false;
        	                    return 'icon-ok';
                            }
        	                return false;
        	            };                                 
        	       }
        	   } 
        	}).directive('dropdownMultiselect2', function(){
        	   return {
        	       restrict: 'E',
        	       scope:{           
        	            model: '=',
        	            options: '=',
        	            pre_selected: '=preSelected',
        	            multiarr: '=multiarr'
        	       },
        	       template: "<div class='btn-group' data-ng-class='{open: open}'>"+
        	        "<button class='btn btn-small2' data-ng-click='open=!open;openDropdown()'><span ng-show='model.length == 1' style='width: 90%;float:left;color:#4e4e4e;'>{{models1[0] | limitTo:20}}</span><span ng-show='model.length > 1' style='color:#4e4e4e;width: 90%;float:left;'>{{model.length}} Agents Selected</span><span ng-show='model.length == 0' style='width: 90%;float:left;color:#4e4e4e;'>Agents</span> <span class='caret2'></span><span class='searchicon-call-black'></span></button>"+        	                
        	                "<ul class='dropdown-menu showhidemenu' aria-labelledby='dropdownMenu'>" +                    
        	                    "<li><input type='text' placeholder='Search Agent' class='txtcls' ng-model='multifilter'></li><li class='divider'></li>" +
        	                    "<li data-ng-repeat='option in options | filter:multifilter'> <a data-ng-click='setSelectedItem()'><span class='pull-left check-option set-color'>{{option.value}}</span><span class='pull-right' data-ng-class='isChecked(option.key)'></span></a></li>" +                                        
        	                "</ul>" +
        	            "</div>" ,
        	       controller: function($scope, $rootScope){
        	    	   $scope.models1=[];        	    	   
        	    	   $rootScope.opentrue1 = false;
        	           $scope.openDropdown = function(){
        	        	   
        	        	   $rootScope.opentrue1 = true;
        	               $scope.selected_items = [];
        	               for(var i=0; i<$scope.pre_selected.length; i++){ 
        	                   $scope.selected_items.push($scope.pre_selected[i].key);
        	               }                                        
        	            };
        	           
        	            $scope.selectAll = function () {
        	                $scope.model = _.pluck($scope.options, 'key');
        	                console.log($scope.model);
        	            };            
        	            $scope.deselectAll = function() {
        	                $scope.model=[];
        	                console.log($scope.model);
        	            };
        	            $scope.setSelectedItem = function(){
        	            	$scope.multiarr = [];
        	            	$rootScope.opentrue1 = false;
        	                var key = this.option.key;
        	                var value = this.option.value;        	                
        	                console.log(name);
        	                if (_.contains($scope.model, key)) {        	                	
        	                    $scope.model = _.without($scope.model, key);
        	                    $scope.models1 = _.without($scope.models1, value);        	                    
        	                } else {        	                	
        	                    $scope.model.push(key);
        	                    $scope.models1=[];
        	                    $scope.models1.push(value);
        	                }
        	                console.log($scope.model);
        	                return false;
        	            };
        	            $scope.isChecked = function (key) {                 
        	                if (_.contains($scope.model, key)) {
        	                	$rootScope.opentrue1 = false;
        	                    return 'icon-ok';
        	                }
        	                return false;
        	            };                                 
        	       }
        	   } 
        	}) .directive('leadStatusMultiSelect', function() {
                return {
                    restrict: 'E',
                    scope:{           
                         model: '=',
                         options: '=',
                         pre_selected: '=preSelected',
                         multiarr: '=multiarr'
                    },
                    template: "<div class='btn-group' data-ng-class='{open: open}'>"+
                     "<button class='btn btn-small2' data-ng-click='open=!open;openDropdown()'><span ng-show='model.length == 1' style='width: 90%;float:left;color:#4e4e4e;'>{{model[0] | limitTo:20}}</span><span ng-show='model.length > 1' style='color:#4e4e4e;width: 90%;float:left;'>{{model.length}} Status Selected</span><span ng-show='model.length == 0' style='width: 90%;float:left;color:#4e4e4e;'>Status</span> <span class='caret2'></span><span class='searchicon-call-black'></span></button>"+        	                
                             "<ul class='dropdown-menu showhidemenu' aria-labelledby='dropdownMenu' style='max-height: 350px !important;overflow: scroll !important;width: 240px;'>" +                    
                                 "<li><input type='text' placeholder='Search By Lead Status' class='txtcls' ng-model='multifilter'></li><li class='divider'></li>" +
                                 "<li data-ng-repeat='option in options | filter:multifilter'  data-ng-class='isParentNodeChecked(option.key)'> <a data-ng-click='setSelectedItem()'><span class='pull-left check-option set-color' title='{{option.toolTip}}'>{{option.value}}</span><span class='pull-right' data-ng-class='isChecked(option.key)'></span></a></li>" +                                        
                             "</ul>" +
                         "</div>" ,
                    controller: function($scope, $rootScope){
                        $scope.models1=[];        	    	   
                        $rootScope.opentrue1 = false;
                        $scope.openDropdown = function() {
                            $rootScope.opentrue1 = true;
                            $scope.selected_items = [];
                            for(var i=0; i<$scope.pre_selected.length; i++) { 
                                $scope.selected_items.push($scope.pre_selected[i].key);
                            }                                        
                         };
                        
                         $scope.selectAll = function () {
                             $scope.model = _.pluck($scope.options, 'key');
                             console.log($scope.model);
                         };            
                         $scope.deselectAll = function() {
                             $scope.model=[];
                             console.log($scope.model);
                         };
                         $scope.setSelectedItem = function(){
                             $scope.multiarr = [];
                             $rootScope.opentrue1 = false;
                             var key = this.option.key;
                             var value = this.option.value;        	                
                             console.log(name);
                             if (_.contains($scope.model, key)) {        	                	
                                 $scope.model = _.without($scope.model, key);
                                 $scope.models1 = _.without($scope.models1, value);        	                    
                             } else {        	                	
                                 $scope.model.push(key);
                                 $scope.models1=[];
                                 $scope.models1.push(value);
                             }
                             console.log($scope.model);
                             return false;
                         };
                         $scope.isChecked = function (key) {                 
                             if (_.contains($scope.model, key)) {
                                 $rootScope.opentrue1 = false;
                                 return 'icon-ok';
                             }
                             return false;
                         };     
                         $scope.isParentNodeChecked = function (key) {                 
                            if (key == 'SUCCESS' || key == 'FAILURE' || key == 'ANS M/C') {
                               return 'leadStatusDisable';
                           } 
                            return false;
                        };                               
                    }
                } 
             }).directive('campaignMultiSelect', function() {
                return {
                    restrict: 'E',
                    scope:{           
                         model: '=',
                         options: '=',
                         pre_selected: '=preSelected',
                         multiarr: '=multiarr'
                    },
                    template: "<div class='btn-group' data-ng-class='{open: open}'>"+
                     "<button class='btn btn-small2' data-ng-click='open=!open;openDropdown()'><span ng-show='model.length == 1' style='width: 90%;float:left;color:#4e4e4e;'>{{models1[0] | limitTo:20}}</span><span ng-show='model.length > 1' style='color:#4e4e4e;width: 90%;float:left;'>{{model.length}} Campaigns Selected</span><span ng-show='model.length == 0' style='width: 90%;float:left;color:#4e4e4e;'>Campaign</span> <span class='caret2'></span><span class='searchicon-call-black'></span></button>"+        	                
                             "<ul class='dropdown-menu showhidemenu' aria-labelledby='dropdownMenu'>" +                    
                                 "<li><input type='text' placeholder='Search By Campaign' class='txtcls' ng-model='multifilter'></li><li class='divider'></li>" +
                                 "<li data-ng-repeat='option in options | filter:multifilter'> <a data-ng-click='setSelectedItem()'><span class='pull-left check-option set-color'>{{option.value}}</span><span class='pull-right' data-ng-class='isChecked(option.key)'></span></a></li>" +                                        
                             "</ul>" +
                         "</div>" ,
                    controller: function($scope, $rootScope){
                        $scope.models1=[];        	    	   
                        $rootScope.opentrue1 = false;
                        $scope.openDropdown = function() {
                            $rootScope.opentrue1 = true;
                            $scope.selected_items = [];
                            for(var i=0; i<$scope.pre_selected.length; i++) { 
                                $scope.selected_items.push($scope.pre_selected[i].key);
                            }                                        
                         };
                        
                         $scope.selectAll = function () {
                             $scope.model = _.pluck($scope.options, 'key');
                             console.log($scope.model);
                         };            
                         $scope.deselectAll = function() {
                             $scope.model=[];
                             console.log($scope.model);
                         };
                         $scope.setSelectedItem = function(){
                             $scope.multiarr = [];
                             $rootScope.opentrue1 = false;
                             var key = this.option.key;
                             var value = this.option.value;        	                
                             console.log(name);
                             if (_.contains($scope.model, key)) {        	                	
                                 $scope.model = _.without($scope.model, key);
                                 $scope.models1 = _.without($scope.models1, value);        	                    
                             } else {        	                	
                                 $scope.model.push(key);
                                 $scope.models1=[];
                                 $scope.models1.push(value);
                             }
                             console.log($scope.model);
                             return false;
                         };
                         $scope.isChecked = function (key) {                 
                             if (_.contains($scope.model, key)) {
                                 $rootScope.opentrue1 = false;
                                 return 'icon-ok';
                             }
                             return false;
                         };                                 
                    }
                } 
             }).filter('toPercent', function() {
                return function(value) {
                    return parseFloat(value).toFixed(2);
                };
            });
});
