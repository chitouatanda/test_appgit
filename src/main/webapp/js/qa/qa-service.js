define(['angular', 'angular-resource'], function(angular) {
    angular.module('qa-service', ['ngResource'])
    .factory("searchTeamService", function ($resource) {
        return $resource("spr/team?searchstring=:searchString", {}, {
            query: {
                method : 'GET',
                isArray: true
            }
        });
    }).factory("searchQaCampaignService", function ($resource) {
        return $resource("spr/rest/campaign?searchstring=:searchString", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("searchQaCampaignAgentService", function ($resource) {
        return $resource("spr/agent?searchstring=:searchString", {}, {
            query: {
                method : 'GET',
                isArray: true
            }
        });
    }).factory("dispositionStatusMetricsService", function ($resource) {
        return $resource("spr/qa/metrics?searchstring=:searchString", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("getDisplayLabelService", function($resource) {
        return $resource("/spr/label/:labelentity/:key",null, {
            query: {
            method : 'GET',
            isArray: false
        }
    });
    }).factory("prospectCallStatusMetricsService", function ($resource) {
        return $resource("spr/qa/metrics?searchstring=:searchString", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("scoredRateMetricsService", function ($resource) {
        return $resource("spr/qa/metrics/scoredrate", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("qaProspectListService", function ($resource) {
        return $resource("spr/qa/prospectcall/search?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("qaFirstTimeLoginService", function ($resource) {
        return $resource("spr/qa/login/firstTime?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("qADetailService", function ($resource) {
        return $resource("spr/qa/:prospectCallId", null, {
            query: {
                method : 'GET',
                isArray: false
            },
            emailStatus: {
            	method : 'GET',
            	url : 'spr/email/status/:prospectCallId',
            	isArray : false
            }
        });
    }).factory("qADetailFeedbackService", function ($resource) {
        return $resource("spr/qa/:prospectCallId/feedback", null, {
        	save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
        });
    }).factory("qADetailSaveFeedbackService", function ($resource) {
    	// DATE : 05/06/2017	REASON : Added secondaryQaReview to handle status
        return $resource("spr/qa/:prospectCallId/:secondaryQaReview/savefeedback", null, {
        	save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
        });
    }).factory("qADetailSubmitFeedbackService", function ($resource) {
        // DATE : 05/06/2017	REASON : Added secondaryQaReview to handle status
        return $resource("spr/qa/:prospectCallId/:secondaryQaReview/submitfeedback", null, {
        	save: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
        });
    }).factory("qaRejectReasonService", function ($resource) {
        return $resource("spr/rest/qarejectreason", null, {
        	query: {
                method : 'GET',
                isArray: true
            }
        });
    }).factory("qaUpdateProspectService", function ($resource) {
	    return $resource("spr/prospect/:prospectCallId", null, {
	    	update: {
	            method: 'PUT',
	            headers : {
	                'Content-Type': 'application/json'
	            }
	        },
	    });
    }).factory("qaMetricsService", function ($resource) {
        return $resource("spr/qa/metrics?searchstring=:searchString", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("qaDownloadReportService", function ($resource) {
        return $resource("/spr/leadreport/filter", null, {
        	download: {
                method: 'POST',
                headers : {
                    'Content-Type': 'application/json'
                }
            },
        });
    }).factory("GetOrganizationService", function ($resource) {
        return $resource("/spr/organization/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("CampaignMetaDataService", function ($resource) {
        return $resource("/spr/campaignmetadata", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("EmailStatusService", function ($resource) {
        return $resource("/spr/organization/:id", null, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("ClientLeadReportService", function ($resource) {
        return $resource("/spr/leadreport/filter/client/:version?searchstring= :downloadCriteriaObject", {}, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("DemandshoreLeadReportService", function ($resource) {
        return $resource("/spr/leadreport/demandshore?searchstring= :downloadCriteriaObject", {}, {
            query: {
                method : 'GET',
                isArray : false
            }
        });
    }).factory("changeLeadStatusService", function ($resource) {
	    return $resource("spr/prospect/update/:prospectCallId/:leadStatus", null, {
	    	query: {
	            method: 'POST',
	            headers : {
	                'Content-Type': 'application/json'
	            }
	        },
	    });
    }).factory("qaLeadSearchService", function ($resource) {
        return $resource("spr/qa/search?searchstring=:searchstring", null, {
            query: {
                method : 'GET',
                isArray: false
            }
        });
    }).factory("ClientMappingService", function ($resource) {
        return $resource("/spr/clientMapping/:campaignId", null, {
            query: {
                method : 'GET',
                isArray : true
            }
        })
    });
});
