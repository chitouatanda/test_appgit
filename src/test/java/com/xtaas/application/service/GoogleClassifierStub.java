package com.xtaas.application.service;

import java.util.Optional;

import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifier;

import org.springframework.stereotype.Service;

@Service
public class GoogleClassifierStub extends GoogleClassifier {
    public GoogleClassifierStub() {
        super("project_id", "model_id", "location_id", 65);
    }

    // protected PredictionServiceClient getPredictionServiceClient() throws IOException {
    //     PredictionServiceSettings settings = 
    //         PredictionServiceSettings.newBuilder().setEndpoint("localhost:" + port).build() ;
    //     return PredictionServiceClient.create(settings);
    // }

    public Optional<PhoneNumberType> classify(ClassifyContentInput contentInput) {
        return Optional.ofNullable(fakeClassify(contentInput.getTextContent()));
    }

    public static PhoneNumberType fakeClassify(String text) {
        if ("___NON_DETERMINISTIC___".equals(text)) {
            return null;
        }
        int ordinal = text.hashCode() % PhoneNumberType.values().length;
        return PhoneNumberType.values()[ordinal];
    }
}