package com.xtaas.application.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.github.javafaker.Faker;
import com.xtaas.db.config.DummyMongoConfig;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.*;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.ml.classifier.service.CallClassificationService;
import com.xtaas.ml.classifier.service.GoogleClassifier;
import com.xtaas.ml.transcribe.service.AwsS3PersistenceService;
import com.xtaas.ml.transcribe.service.AwsTranscriptionService;
import com.xtaas.ml.transcribe.service.InMemoryPersistenceService;
import com.xtaas.ml.transcribe.service.TranscriberFactory;
import com.xtaas.ml.transcribe.service.TranscriberThreadPoolConfig;
import com.xtaas.service.*;
import com.xtaas.BeanLocator;

import org.junit.jupiter.api.Test;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.util.ReflectionTestUtils;


// TODO use MongoDB test container
/**
 * 
 * This test requires: * mongodb instance running on the environment
 *
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { CallClassificationService.class, TranscriberFactory.class,
        AwsTranscriptionService.class, AwsS3PersistenceService.class, InMemoryPersistenceService.class,
        TranscriberThreadPoolConfig.class, GoogleClassifier.class, DummyMongoConfig.class, MongoConfig.class,
        CallClassificationJobQueue.class, GlobalContact.class, ProspectCallLog.class,
        CallClassificationJobQueueRepository.class, GlobalContactRepository.class, ProspectCallLogRepository.class, CampaignRepository.class,
        ProspectCallLogRepositoryCustomDummyImpl.class,
        ApplicationEnvironmentPropertyServiceImpl.class,
}, loader = AnnotationConfigContextLoader.class)
public class AgentServiceImplIntegrationTest {
    @Autowired
    private MongoOperations mongoOps;

    @Autowired
    private GlobalContactRepository globalContactRepo;

    @Autowired
    private CallClassificationJobQueueRepository queueRepo;

    @Autowired
    private ProspectCallLogRepository prospectCallLogRepo;

    @Autowired
    private CampaignRepository campaignRepo;

    @Autowired
    ApplicationContext context;

    @Value("${server.base.url}")
    String serverUrl;

    private AgentServiceImpl agentService;
    private final Faker faker = new Faker(new Random(17049));

    // com.xtaas.db.entity and com.xtaas.domain.entity packages. Both has parent
    // class AbstractEntity which are identical.
    // private List<Class<? extends AbstractEntity>> entities =
    private final List<Class<? extends Object>> entities = Arrays.asList(ProspectCallLog.class, GlobalContact.class,
            CallClassificationJobQueue.class, Campaign.class);

    @BeforeEach
    public void setup() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        entities.stream().filter(e -> !mongoOps.collectionExists(e)).forEach(e -> mongoOps.createCollection(e));

        agentService = new AgentServiceImpl();
        ReflectionTestUtils.setField(agentService, "callClassificationJobQueueRepository", queueRepo);
        ReflectionTestUtils.setField(agentService, "globalContactRepository", globalContactRepo);
        ReflectionTestUtils.setField(agentService, "campaignRepository", campaignRepo);
        ReflectionTestUtils.setField(BeanLocator.class, "context", context);

    }

    @Test
    public void test_change_direct_phone_job_enqueue_when_flag_not_set_success() {
        final Date start = new Date();

        final Campaign campaign = createCampaign(true);
        final ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        final GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), false, false);

        agentService.updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final List<CallClassificationJobQueue> entries = queueRepo.findByPhone(contact.getPhone());
        assertThat(entries).hasSize(1);
        final CallClassificationJobQueue entry = entries.get(0);
        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(entry.getPhone()).isEqualTo(contact.getPhone());
        softly.assertThat(entry.getId()).isNotBlank();
        softly.assertThat(entry.getMessageLifecycleId()).isNotBlank();
        softly.assertThat(entry.getTtl()).isEqualTo(3);
        softly.assertThat(entry.getProducer()).isEqualTo("AgentServiceImpl");
        softly.assertThat(entry.getProducerServerId()).isEqualTo(serverUrl);
        softly.assertThat(entry.getNamespace()).isEqualTo(serverUrl);
        softly.assertThat(entry.getJobCreationTime()).isAfter(start);
        softly.assertThat(entry.getRecordingUrl()).isEqualTo(prospectCallLog.getProspectCall().getRecordingUrlAws());
        softly.assertThat(entry.getCallDuration()).isEqualTo(prospectCallLog.getProspectCall().getCallDuration());
        softly.assertThat(entry.getStatus()).isEqualTo(CallClassificationJobQueue.Status.QUEUED);
        softly.assertAll();
    }

    @Test
    public void test_change_direct_phone_when_use_direct_flag_false() {
        final Campaign campaign = createCampaign(false);
        final ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        final GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), false, false);

        final Optional<CallClassificationJobQueue> enqueued = agentService
                .updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(enqueued).isEmpty();
        final List<CallClassificationJobQueue> entries = queueRepo.findByPhone(contact.getPhone());
        softly.assertThat(entries).hasSize(0);
        softly.assertAll();
    }

    @Test
    public void test_change_direct_phone_when_flag_already_set_by_ml() {
        final Campaign campaign = createCampaign(true);
        final ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        final GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), true, true);

        final Optional<CallClassificationJobQueue> enqueued = agentService
                .updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(prospectCallLog.getProspectCall().getProspect().isDirectPhone()).isTrue();
        softly.assertThat(enqueued).isEmpty();
        final List<CallClassificationJobQueue> entries = queueRepo.findByPhone(contact.getPhone());
        softly.assertThat(entries).hasSize(0);
        softly.assertAll();
    }

    @Test
    public void test_change_direct_phone_when_flag_already_set_by_ml_on_another_contact() {
        final Campaign campaign = createCampaign(true);
        final ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        final GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), false, false);
        final String phone = contact.getPhone();
        createContactInDB(phone, true, true);
        createContactInDB(phone, false, false);

        final Optional<CallClassificationJobQueue> enqueued = agentService
                .updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(prospectCallLog.getProspectCall().getProspect().isDirectPhone()).isTrue();
        softly.assertThat(enqueued).isEmpty();
        final List<CallClassificationJobQueue> entries = queueRepo.findByPhone(phone);
        softly.assertThat(entries).hasSize(0);
        final List<GlobalContact> contacts = globalContactRepo.findByPhone(phone);
        softly.assertThat(contacts).allMatch(c -> c.isDirectClassificationML()).allMatch(c -> c.isDirectPhone());
        softly.assertAll();
    }

    // need to rethink on how to handle queue duplicates as some recordings are very
    // short
    @Test
    @Disabled("rethink strategy or move it downstream")
    public void test_change_direct_phone_when_duplicate_job_in_queue_for_phone() {
        final Campaign campaign = createCampaign(true);
        final ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        final GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), false, false);
        agentService.updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final Optional<CallClassificationJobQueue> enqueued = agentService
                .updateDirectPhoneFlagOrEnqueueClassificationJob(prospectCallLog);

        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(enqueued).isEmpty();
        final List<CallClassificationJobQueue> entries = queueRepo.findByPhone(contact.getPhone());
        softly.assertThat(entries).hasSize(1);
        softly.assertAll();
    }

    private ProspectCallLog createProspectLogInDB(final Campaign campaign) {
        final ProspectCallLog prospectCallLog = new ProspectCallLog();
        // prospectCallLog.setId(getFakeId());
        final Prospect prospect = new Prospect();
        prospect.setFirstName(faker.name().firstName());
        prospect.setLastName(faker.name().lastName());
        prospect.setPhone(faker.phoneNumber().phoneNumber());
        final ProspectCall prospectCall = new ProspectCall(getFakeId(), getFakeId(), prospect);
        prospectCall.setCampaignId(campaign.getId());
        prospectCall.setRecordingUrlAws(faker.internet().url());
        prospectCall.setCallDuration(faker.number().numberBetween(0, 300));
        prospectCallLog.setProspectCall(prospectCall);
        return prospectCallLogRepo.save(prospectCallLog);
    }

    private GlobalContact createContactInDB(final ProspectCall prospectCall, final boolean dFlag,
            final boolean mlFlag) {
        // FIXME can lead to unsafe publish. instead use a copy constructor
        final GlobalContact contact = new GlobalContact().toGlobalContact(prospectCall);
        contact.setDirectPhone(dFlag);
        contact.setDirectClassificationML(mlFlag);
        return globalContactRepo.save(contact);
    }

    private String createContactInDB(final String phone, final boolean dFlag, final boolean mlFlag) {
        return globalContactRepo.save(new GlobalContact().withId(faker.bothify("??###???###########?????"))
                .withFirstName(faker.name().firstName()).withLastName(faker.name().lastName()).withPhone(phone)
                .withDirectPhone(dFlag).withDirectClassificationML(mlFlag)).getId();
    }

    private String getFakeId() {
        return faker.bothify("??###???###########?????");
    }

    private Campaign createCampaign(final boolean useDirectClassificationML) {
        final Campaign campaign = new Campaign();
        campaign.setUseDirectClassificationML(useDirectClassificationML);
        return campaignRepo.save(campaign);
    }

    @AfterEach
    public void tearDown() {
        entities.stream().forEach(e -> mongoOps.dropCollection(e));
    } 
}