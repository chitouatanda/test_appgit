package com.xtaas.application.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import com.google.cloud.automl.v1.AnnotationPayload;
import com.google.cloud.automl.v1.ClassificationAnnotation;
import com.google.cloud.automl.v1.PredictRequest;
import com.google.cloud.automl.v1.PredictResponse;
import com.google.cloud.automl.v1.PredictionServiceClient;
import com.google.common.math.StatsAccumulator;
import com.xtaas.BeanLocator;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.ClassifierSampleLog;
import com.xtaas.db.entity.ClassifierStatisticsLog;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.ClassifierSampleLogRepository;
import com.xtaas.db.repository.ClassifierStatisticsLogRepository;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifierModelTestBase;
import com.xtaas.ml.classifier.service.GoogleClassifierStatistics;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = { GoogleClassifierStatistics.class, MongoConfig.class,
		ClassifierSampleLogRepository.class, ClassifierStatisticsLogRepository.class

}, loader = AnnotationConfigContextLoader.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest({ PredictionServiceClient.class, PredictResponse.class, AnnotationPayload.class,
		ClassificationAnnotation.class, StatsAccumulator.class })
public class GoogleClassifierStatsIntegrationTest extends GoogleClassifierModelTestBase {

	@Autowired
	private MongoOperations mongoOps;

	@Autowired
	GoogleClassifierStatistics classifier;

	@Autowired
	ClassifierStatisticsLogRepository classifierStatisticsLogRepository;

	@Autowired
	ClassifierSampleLogRepository classifierSampleLogRepository;

	@Autowired
	ApplicationContext context;

	private final List<Class<? extends Object>> entities = Arrays.asList(ProspectCallLog.class, GlobalContact.class,
			CallClassificationJobQueue.class, Campaign.class);

	public GoogleClassifierStatsIntegrationTest() {

	}

	@Before
	public void setup() {
		entities.stream().filter(e -> !mongoOps.collectionExists(e)).forEach(e -> mongoOps.createCollection(e));
		ReflectionTestUtils.setField(classifier, "scores", new ConcurrentHashMap<>());
		ReflectionTestUtils.setField(classifier, "failedSamples", new ConcurrentHashMap<>());
		ReflectionTestUtils.setField(classifier, "statsRepo",
				classifierStatisticsLogRepository);
		ReflectionTestUtils.setField(classifier, "sampleRepo",
				classifierSampleLogRepository);
		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
	}

	@Test
	public void test_stats_saving_in_db() throws IOException {
		classifier.classify(mockAndReturnContent(0.50f, 0.59f, "Direct", "Indirect"));
		List<ClassifierStatisticsLog> statsBeforeCall = classifierStatisticsLogRepository.findAll();
		classifier.statsSapshot();
		List<ClassifierStatisticsLog> statsAfterCall = classifierStatisticsLogRepository.findAll();
		assertTrue(statsAfterCall.size() > statsBeforeCall.size());
	}

	@Test
	public void test_failedsamples_saving_in_db() throws IOException {
		classifier.classify(mockAndReturnContent(0.50f, 0.59f, "Direct", "Indirect"));
		List<ClassifierSampleLog> sizeBeforeCall = classifierSampleLogRepository.findAll();
		classifier.sampleSnapshot();
		List<ClassifierSampleLog> sizeAfterCall = classifierSampleLogRepository.findAll();
		assertTrue(sizeAfterCall.size() > sizeBeforeCall.size());
	}

	private ClassifyContentInput mockAndReturnContent(float directValue, float indirectValue, String directDisplayName,
			String indirectDisplayName) throws IOException {
		PredictionServiceClient mockPClient = mock(PredictionServiceClient.class);
		PredictResponse mockResp = mock(PredictResponse.class);
		mockStatic(PredictionServiceClient.class);
		when(PredictionServiceClient.create()).thenReturn(mockPClient);
		when(mockPClient.predict(any(PredictRequest.class))).thenReturn(mockResp);
		AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
		ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
		when(directClassPayload.getDisplayName()).thenReturn(directDisplayName);
		when(directClassPayload.getClassification()).thenReturn(directClass);
		when(directClass.getScore()).thenReturn(directValue);
		AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
		ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
		when(indirectClassPayload.getDisplayName()).thenReturn(indirectDisplayName);
		when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
		when(indirectClass.getScore()).thenReturn(indirectValue);
		List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);
		when(mockResp.getPayloadList()).thenReturn(mockPayloadList);
		ClassifyContentInput contentInput = new ClassifyContentInput(UUID.randomUUID(), "random text", "text/plain");
		return contentInput;
	}

	@AfterEach
	public void tearDown() {
		entities.stream().forEach(e -> mongoOps.dropCollection(e));
	}
}