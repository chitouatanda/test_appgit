package com.xtaas.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static com.xtaas.db.entity.CallClassificationJobQueue.Status.*;
import static com.xtaas.application.service.GoogleClassifierStub.fakeClassify;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.transcribe.AmazonTranscribe;
import com.amazonaws.services.transcribe.AmazonTranscribeClient;
import com.github.javafaker.Faker;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.xtaas.db.config.DummyMongoConfig;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.CallClassificationJobQueue;
import com.xtaas.db.entity.GlobalContact;
import com.xtaas.db.entity.Prospect;
import com.xtaas.db.entity.ProspectCall;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.repository.*;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.ml.classifier.service.CallClassificationService;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.service.AwsS3PersistenceService;
import com.xtaas.ml.transcribe.service.AwsTranscriptionService;
import com.xtaas.ml.transcribe.service.InMemoryPersistenceService;
import com.xtaas.ml.transcribe.service.TranscriberFactory;
import com.xtaas.ml.transcribe.service.TranscriberThreadPoolConfig;
import com.xtaas.service.*;
import com.xtaas.BeanLocator;

import org.junit.jupiter.api.Test;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.SocketUtils;

// TODO use MongoDB test container
/**
 * 
 * This test requires: * mongodb instance running on the environment
 *
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { CallClassificationService.class, TranscriberFactory.class,
        AwsTranscriptionService.class, AwsS3PersistenceService.class, InMemoryPersistenceService.class,
        TranscriberThreadPoolConfig.class, GoogleClassifierStub.class, DummyMongoConfig.class, MongoConfig.class,
        CallClassificationJobQueue.class, GlobalContact.class, ProspectCallLog.class,
        CallClassificationJobQueueRepository.class, GlobalContactRepository.class, ProspectCallLogRepository.class,
        CampaignRepository.class, ProspectCallLogRepositoryCustomDummyImpl.class,
        ApplicationEnvironmentPropertyServiceImpl.class, }, loader = AnnotationConfigContextLoader.class)
// @TestPropertySource(properties = { "SERVER_BASE_URL = TEST_INSTANCE", "CLASSIFIER_JOB_INSTANCE = TEST_INSTANCE" })
public class CallClassificationServiceIntegrationTest {
    // ApplicationListener list;
    @Autowired
    private MongoOperations mongoOps;

    @Autowired
    private GlobalContactRepository globalContactRepo;

    @Autowired
    private CallClassificationJobQueueRepository queueRepo;

    @Autowired
    private ProspectCallLogRepository prospectCallLogRepo;

    @Autowired
    private CampaignRepository campaignRepo;

    @Autowired
    private CallClassificationService subject;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private AwsTranscriptionService awsTranscriptionService;

    @Autowired
    private GoogleClassifierStub googleClassifier;

    @Autowired
    private TranscriberFactory transcriberFactory;

    @Value("${server.base.url}")
    private String serverUrl;

    private AgentServiceImpl agentService;
    private Faker faker = new Faker(new Random(17049));
    private int transcribeServerPort;
    private WireMockServer transcribeServer;
    // private int classifyServerPort;
    // private WireMockServer classifyServer;

    // com.xtaas.db.entity and com.xtaas.domain.entity packages. Both has parent
    // class AbstractEntity which are identical.
    // private List<Class<? extends AbstractEntity>> entities =
    private List<Class<? extends Object>> entities = Arrays.asList(ProspectCallLog.class, GlobalContact.class,
            CallClassificationJobQueue.class, Campaign.class);

    @BeforeEach
    public void setup() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        entities.stream().filter(e -> !mongoOps.collectionExists(e)).forEach(e -> mongoOps.createCollection(e));

        agentService = new AgentServiceImpl();
        ReflectionTestUtils.setField(agentService, "callClassificationJobQueueRepository", queueRepo);
        ReflectionTestUtils.setField(agentService, "globalContactRepository", globalContactRepo);
        ReflectionTestUtils.setField(agentService, "campaignRepository", campaignRepo);
        ReflectionTestUtils.setField(BeanLocator.class, "context", context);

        ReflectionTestUtils.setField(transcriberFactory, "pollWaitTime", 1);
       
        transcribeServerPort = SocketUtils.findAvailableTcpPort(5000, 10000);
        transcribeServer = new WireMockServer(transcribeServerPort);
        transcribeServer.start();
        // awsTranscriptionService.setPort(transcribeServerPort);
        // awsTranscriptionService.clearClientCache();
        @SuppressWarnings("unchecked")
        Map<String,AmazonTranscribe> clientCache = 
            (Map<String,AmazonTranscribe>) ReflectionTestUtils.getField(awsTranscriptionService, "clientMap");
        clientCache.put(Regions.US_WEST_2.getName().toLowerCase(), getTranscribeClient());

        // googleClassifier.setPort(transcribeServerPort);
         

        // classifyServerPort = SocketUtils.findAvailableTcpPort(5000, 10000);
        // classifyServer = new WireMockServer(
        // com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig()
        // .httpsPort(classifyServerPort)
        // .keystorePath("client/server/server_keystore.jks")
        // .keystorePassword("secret")
        // );
        // classifyServer.start();
    }

    /**
     * enqueue dequeue and add to job (QUEUED) job (INPROGRESS) job (TRANSCRIBED) --
     * transcription and Transcribe persistence job (CLASSIFIED) -- classification
     * and GlobalContact persistence job (SUCCESS) -- prospect log updated
     * 
     * @throws IOException
     */
    @Test
    public void test_classifiction_job_completed_deterministically() throws IOException {
        CallClassificationJobQueue entry = enqueueJobAndValidatePreconditions();

        stubTranscribeSubmit(entry);
        stubTranscribeCheckStatus(entry);
        String textContent = faker.lorem().paragraph();
        stubFetchTranscription(entry, textContent);
        
        // due to SSL configuration problems, I have used custom GoogleClassifierStub instead

        // mock classify
        // configureFor("localhost", classifyServerPort);
        // double directScore = 0.8989502, indirectScore = 0.10098731;
        // String classificationResp = "{\"payload\":[{\"displayName\":\"Direct\",\"classification\":{\"score\":"+directScore+"}},{\"displayName\":\"Indirect\",\"classification\":{\"score\":"+indirectScore+"}}]}";
        // stubFor(post(urlEqualTo("/v1/projects/project_id/locations/location_id/models/model_id:predict"))
        //     .withRequestBody(containing(transcription))
        //     .willReturn(aResponse()
        //         .withStatus(200)
        //         .withBody(classificationResp)));
        
        subject.classifyCallRecordings();

        // get CallClassificationJob object and validate success
        List<CallClassificationJobQueue> entries = queueRepo.findByPhone(entry.getPhone());
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(entries).filteredOn(e -> e.getStatus() == QUEUED).hasSize(0);
        softly.assertThat(entries).filteredOn(e -> e.getStatus() == INPROCESS).hasSize(0);
        softly.assertThat(entries).filteredOn(e -> e.getStatus() == COMPLETED).hasSize(1);
        entries.stream().findFirst().ifPresent(
            e -> {
                softly.assertThat(e.getErrorMsg()).isNull();
                softly.assertThat(e.getConsumer()).isEqualTo("CallClassificationService");
                softly.assertThat(e.getConsumerServerId()).isEqualTo(serverUrl);
            }
        );

        // validate GlobalContact has been updated
        boolean isDirectPhone = PhoneNumberType.Direct == fakeClassify(textContent);
        List<GlobalContact> contacts = globalContactRepo.findByPhone(entry.getPhone());
        softly.assertThat(contacts).filteredOn(c -> c.isDirectClassificationML()).hasSize(1);
        softly.assertThat(contacts).filteredOn(c -> !c.isDirectClassificationML()).hasSize(0);
        softly.assertThat(contacts).allMatch(c -> {
            return c.isDirectPhone() == isDirectPhone;
        });

        // validate prospectCallLog has been updated
        Optional<ProspectCallLog> optional = prospectCallLogRepo.findById(entry.getProspectCallLogId());
        softly.assertThat(optional).isNotEmpty();
        softly.assertThat(optional.get()).matches(p -> (p.getProspectCall().getProspect().isDirectPhone() == isDirectPhone));
        softly.assertAll();
    }

    @Test
    public void test_classifiction_job_completed_non_deterministically() throws IOException {
        CallClassificationJobQueue entry = enqueueJobAndValidatePreconditions();

        stubTranscribeSubmit(entry);
        stubTranscribeCheckStatus(entry);
        String textContent = "___NON_DETERMINISTIC___";
        stubFetchTranscription(entry, textContent);
        
        subject.classifyCallRecordings();

        // get CallClassificationJob object and validate success
        List<CallClassificationJobQueue> entries = queueRepo.findByPhone(entry.getPhone());

        assertThat(entries).hasSize(1);
        SoftAssertions softly = new SoftAssertions();
        entries.stream().findFirst().ifPresent(
            e -> {
                softly.assertThat(e.getStatus()).isEqualTo(COMPLETED);
                softly.assertThat(e.getErrorMsg()).isEqualTo("NON_DETERMINISTIC");
            }
        );
      
        // validate GlobalContact has not been updated
        List<GlobalContact> contacts = globalContactRepo.findByPhone(entry.getPhone());
        softly.assertThat(contacts).filteredOn(c -> c.isDirectClassificationML()).hasSize(0);
        softly.assertThat(contacts).filteredOn(c -> !c.isDirectClassificationML()).hasSize(1);
        softly.assertAll();
    }

    @Test
    public void test_classifiction_job_failed_with_error() throws IOException, InterruptedException {
        CallClassificationJobQueue entry = enqueueJobAndValidatePreconditions();

        IntStream.rangeClosed(0, 3).forEach(cnt -> {
            stubTranscribeSubmit(entry);
            stubTranscribeCheckStatusInProgrss(entry);
            
            subject.classifyCallRecordings();

            // Thread.sleep(10000);
            // get CallClassificationJob object and validate success
            List<CallClassificationJobQueue> entries = queueRepo.findByPhone(entry.getPhone());
            assertThat(entries).hasSize(1);
            SoftAssertions softly = new SoftAssertions();
            entries.stream().findFirst().ifPresent(
                e -> {
                    if (cnt < 2) {
                        softly.assertThat(e.getStatus()).isEqualTo(QUEUED);
                        softly.assertThat(e.getErrorMsg()).isNotNull().contains("Max number of times to poll exceeded for job");
                    } else {
                        softly.assertThat(e.getStatus()).isEqualTo(ERROR);
                        softly.assertThat(e.getErrorMsg()).isNotNull().contains("Max number of times to poll exceeded for job");
                    }
                }
            );

            // validate GlobalContact has not been updated
            List<GlobalContact> contacts = globalContactRepo.findByPhone(entry.getPhone());
            softly.assertThat(contacts).filteredOn(c -> c.isDirectClassificationML()).hasSize(0);
            softly.assertThat(contacts).filteredOn(c -> !c.isDirectClassificationML()).hasSize(1);
            softly.assertAll();
        });


    }

    private void stubFetchTranscription(CallClassificationJobQueue entry, String textContent) {
        String mediaObjectName = new S3AudioMediaLink(URI.create(entry.getRecordingUrl())).getMediaObjectName();
        String transcription = "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"" + textContent + "\"}]}}";
        configureFor("localhost", transcribeServerPort);
        stubFor(get(urlEqualTo("/s3/" + mediaObjectName))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody(transcription)));
    }

    private void stubTranscribeCheckStatus(CallClassificationJobQueue entry) {
        configureFor("localhost", transcribeServerPort);
        String mediaObjectName = new S3AudioMediaLink(URI.create(entry.getRecordingUrl())).getMediaObjectName();
        stubFor(post(urlEqualTo("/transcribe/"))
            .withHeader("X-Amz-Target", equalTo("Transcribe.GetTranscriptionJob"))
            .withRequestBody(containing(mediaObjectName))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("{\"TranscriptionJob\": {\"TranscriptionJobStatus\":\"COMPLETED\"," + 
                        "\"Transcript\":{\"TranscriptFileUri\":\"http://localhost:" + 
                            transcribeServerPort + "/s3/" + mediaObjectName + "\"}" +
                "}}")));
    }

    private void stubTranscribeCheckStatusInProgrss(CallClassificationJobQueue entry) {
        configureFor("localhost", transcribeServerPort);
        String mediaObjectName = new S3AudioMediaLink(URI.create(entry.getRecordingUrl())).getMediaObjectName();
        stubFor(post(urlEqualTo("/transcribe/"))
            .withHeader("X-Amz-Target", equalTo("Transcribe.GetTranscriptionJob"))
            .withRequestBody(containing(mediaObjectName))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("{\"TranscriptionJob\": {\"TranscriptionJobStatus\":\"IN_PROGRESS\"}}")));
    }

    private void stubTranscribeSubmit(CallClassificationJobQueue entry) {
        // mock transcribe 
        configureFor("localhost", transcribeServerPort);
        stubFor(post(urlEqualTo("/transcribe/"))
            .withHeader("X-Amz-Target", equalTo("Transcribe.StartTranscriptionJob"))
            .withRequestBody(containing(entry.getRecordingUrl()))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("{\"TranscriptionJob\": {\"TranscriptionJobStatus\":\"IN_PROGRESS\"}}")));
    }

    @Test
    @Disabled("Not implemented")
    public void test_classifiction_job_success_2020() {
        List<CallClassificationJobQueue> all = Collections.nCopies(2020, new CallClassificationJobQueue())
                .stream()
                .map(i -> enqueueJobAndValidatePreconditions())
                .collect(Collectors.toList());
        
        // mock transcribe 
        // mock classify

        subject.classifyCallRecordings();

        all.stream()
            .forEach(entry -> {
                List<CallClassificationJobQueue> entries = queueRepo.findByPhone(entry.getPhone());

                SoftAssertions softly = new SoftAssertions();
                softly.assertThat(entries).filteredOn(e -> e.getStatus() == QUEUED).hasSize(0);
                softly.assertThat(entries).filteredOn(e -> e.getStatus() == INPROCESS).hasSize(1);
                softly.assertAll();
            });

        // get CallClassificationJob object and validate success
        // validate GlobalContact has been updated
        // validate prospectCallLog has been updated
    }

    private CallClassificationJobQueue enqueueJobAndValidatePreconditions() {
        Campaign campaign = createCampaign(true);
        ProspectCallLog prospectCallLog = createProspectLogInDB(campaign);
        GlobalContact contact = createContactInDB(prospectCallLog.getProspectCall(), false, false);
        ReflectionTestUtils.invokeMethod(agentService, "updateDirectPhoneFlagOrEnqueueClassificationJob", prospectCallLog);
        List<CallClassificationJobQueue> entries = queueRepo.findByPhone(contact.getPhone());
        assumeTrue(() -> entries.size() == 1);
        CallClassificationJobQueue entry = entries.get(0);
        assumeTrue(() -> entry.getTtl() == 3);
        assumeTrue(() -> !entry.getRecordingUrl().trim().isEmpty());
        assumeTrue(() -> entry.getStatus() == CallClassificationJobQueue.Status.QUEUED);
        return entry;
    }

    private ProspectCallLog createProspectLogInDB(Campaign campaign) {
        ProspectCallLog prospectCallLog = new ProspectCallLog();
        // prospectCallLog.setId(getFakeId());
        Prospect prospect = new Prospect();
        prospect.setFirstName(faker.name().firstName());
        prospect.setLastName(faker.name().lastName());
        prospect.setPhone(faker.phoneNumber().phoneNumber());
        ProspectCall prospectCall = new ProspectCall(getFakeId(), getFakeId(), prospect);
        prospectCall.setCampaignId(campaign.getId());
        String fakeRecUrl = "https://xtaasrecordings.s3-us-west-2.amazonaws.com/recordings/" + UUID.randomUUID() + ".wav";
        prospectCall.setRecordingUrlAws(fakeRecUrl);
        prospectCall.setCallDuration(faker.number().numberBetween(0, 300));
        prospectCallLog.setProspectCall(prospectCall);
        return prospectCallLogRepo.save(prospectCallLog);
    }

    private GlobalContact createContactInDB(ProspectCall prospectCall, boolean dFlag, boolean mlFlag) {
        // FIXME can lead to unsafe publish. instead use a copy constructor
        GlobalContact contact = new GlobalContact()
            .toGlobalContact(prospectCall);
        contact.setDirectPhone(dFlag);
        contact.setDirectClassificationML(mlFlag);
        return globalContactRepo.save(contact);
    }

    private String createContactInDB(String phone, boolean dFlag, boolean mlFlag) {
        return globalContactRepo.save(new GlobalContact()
            .withId(faker.bothify("??###???###########?????"))
            .withFirstName(faker.name().firstName())
            .withLastName(faker.name().lastName())
            .withPhone(phone)
            .withDirectPhone(dFlag)
            .withDirectClassificationML(mlFlag)).getId();
    }

    private String getFakeId() {
        return faker.bothify("??###???###########?????");
    }

    private Campaign createCampaign(boolean useDirectClassificationML) {
        Campaign campaign = new Campaign();
        campaign.setUseDirectClassificationML(useDirectClassificationML);
        return campaignRepo.save(campaign);
    }

    protected AmazonTranscribe getTranscribeClient() {
        return AmazonTranscribeClient.builder()
                .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(
                        "http://localhost:" + transcribeServerPort + "/transcribe", Regions.US_WEST_2.getName().toLowerCase()))
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials("YOUR-ACCESSKEYID", "YOUR-SECRETACCESSKEY")))
                .build();
    }
    @AfterEach
    public void tearDown() {
        entities.stream().forEach(e -> mongoOps.dropCollection(e));
        transcribeServer.stop();
        // classifyServer.stop();
    } 
}