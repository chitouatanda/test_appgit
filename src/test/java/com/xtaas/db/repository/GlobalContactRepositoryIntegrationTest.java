package com.xtaas.db.repository;

import static org.assertj.core.api.Assertions.assertThat;
// import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.github.javafaker.Faker;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.GlobalContact;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * 
 * This test requires:
 * * mongodb instance running on the environment
 *
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MongoConfig.class, GlobalContact.class,GlobalContactRepository.class},
loader = AnnotationConfigContextLoader.class)
// @Import(MongoConfig.class)
public class GlobalContactRepositoryIntegrationTest {

    @Autowired
    private MongoOperations mongoOps;

    @Autowired
    private GlobalContactRepository globalContactRepository;

    private Faker faker = new Faker(new Random(17049));

    @BeforeEach
    public void setup() {
        if (!mongoOps.collectionExists(GlobalContact.class)) {
            mongoOps.createCollection(GlobalContact.class);
        }
    }

    @Test
    public void test_fetch_by_id() {
        String phone = faker.phoneNumber().phoneNumber();
        String id1 = createContactInDB(phone, true, true);

        Optional<GlobalContact> savedContact = globalContactRepository.findById(id1);
        assertThat(savedContact).isPresent().hasValueSatisfying(c -> phone.equals(c.getPhone())); 
    }

    @Test
    public void test_fetch_by_phone() {
        createContactInDB(faker.phoneNumber().phoneNumber(), true, true);
        String phone = faker.phoneNumber().phoneNumber();
        createContactInDB(phone, true, true);
        createContactInDB(phone, true, true);
        createContactInDB(faker.phoneNumber().phoneNumber(), true, true);

        List<GlobalContact> contacts = globalContactRepository.findByPhone(phone);
        assertThat(contacts).hasSize(2).allMatch(c -> phone.equals(c.getPhone()));
    }

    private String createContactInDB(String phone, boolean dFlag, boolean mlFlag) {
        return globalContactRepository.save(new GlobalContact()
            .withId(faker.bothify("??###???###########?????"))
            .withFirstName(faker.name().firstName())
            .withLastName(faker.name().lastName())
            .withPhone(phone)
            .withDirectPhone(dFlag)
            .withDirectClassificationML(mlFlag)).getId();
    }

    @AfterEach
    public void tearDown() {
        mongoOps.dropCollection(GlobalContact.class);
    }
}