package com.xtaas.db.repository;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.xtaas.application.service.QaMetricsServiceImpl.QaMetrics;
import com.xtaas.db.entity.ProspectCallLog;
import com.xtaas.db.entity.ProspectCallLog.ProspectCallStatus;
import com.xtaas.domain.entity.Campaign;
import com.xtaas.domain.valueobject.AgentSearchResult;
import com.xtaas.domain.valueobject.CampaignCriteria;
import com.xtaas.domain.valueobject.DistinctCampaignIdsDTO;
import com.xtaas.domain.valueobject.DomainCompanyCountPair;
import com.xtaas.domain.valueobject.Pivot;
import com.xtaas.web.dto.CallableGetbackCriteriaDTO;
import com.xtaas.web.dto.GlobalContactCriteriaDTO;
import com.xtaas.web.dto.ProspectCallSearchDTO;
import com.xtaas.web.dto.RecycleStatusDTO;
import com.xtaas.web.dto.TranscribeCriteriaDTO;
import com.xtaas.web.dto.ZoomBuySearchDTO;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

// Need this as the real ProspectCallLogRepository brings in a number of dependencies
//      some of them difficult to construct
// @Component
public class ProspectCallLogRepositoryCustomDummyImpl implements ProspectCallLogRepositoryCustom {

    @Override
    public List<AgentSearchResult> getAgentCallStats(List<String> agentIds, Date fromDate, Date toDate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getAvgFeedbackScore(String campaignId, Date fromDate, Date toDate) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO,
            Pageable pageRequest) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long countProspectCallsByQa(List<String> agentIds, List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int countProspectCallsByAgent(String agentId, ProspectCallSearchDTO prospectCallSearchDTO)
            throws ParseException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean updateCallStatus(String prospectCallId, ProspectCallStatus status, String callSid, Date callbackDate,
            Integer callRetryCount, String allocatedAgentId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean updateProspectInteractionSessionId(String prospectCallId, String prospectInteractionSessionId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int countProspectsContacted(String campaignId, Date fromDate, Date toDate) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int countProspectsDelivered(String campaignId, Date fromDate, Date toDate) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int countProspectsDialed(String campaignId, Date fromDate, Date toDate) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HashMap<QaMetrics, Integer> getQaMetricsByDispositionStatus(List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashMap<QaMetrics, Integer> getQaMetricsByProspectCallStatus(List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashMap<QaMetrics, Integer> getQaMetricsByScoreRate(List<String> campaignIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeNonAsciiCharacters(String prospectCallId, String nonAsciiCompanyName) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HashSet<String> findCallableOrganizations(String campaignId, List<String> organizationNames) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashMap<String, HashMap<String, Integer>> countRecycleRecords(Map<String, List<String>> stringClauses,
            String campaignId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveRecycleRecords(Map<String, List<String>> stringClauses, String campaignId,
            List<RecycleStatusDTO> listDTOs) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsByQaWithoutPagination(List<String> agentIds,
            List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int countProspectCallsBySecQa(ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsBySecQaWithoutPagination(List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> searchProspectCallsBySecQa(List<String> campaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO, Pageable pageRequest) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long countProspectCallsBySecQa(List<String> campaignIds, ProspectCallSearchDTO prospectCallSearchDTO)
            throws ParseException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ProspectCallLog> getRingingProspects(List<String> campaignIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Criteria getCallableProspectsCriteria(Campaign campaign) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int findAllCallableRecordCount(Campaign campaign) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ProspectCallLog> findAllCallableRecords(Campaign campaign) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> findAllCallableRecordsWithLimit(Campaign campaign) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Pivot> generatePivotAggregationQuery(Campaign campaign, String fieldName, boolean isLimit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuery(List<String> campaignIds,
            int duplicateCompanyDuration) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> getDistinctCampaignIds(List<String> agentIds,
            ProspectCallSearchDTO prospectCallSearchDTO) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getDistinctPartnerIds(List<String> campaignIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int nukeCallableProspectByPivots(Campaign campaign, List<String> list, String checkPoint, String action) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ProspectCallLog> searchFailureProspectCallsByQa(List<String> agentIds, List<String> activeCampaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> searchFailutreProspectCallsBySecQa(List<String> activeCampaignIds,
            ProspectCallSearchDTO prospectCallSearchDTO, PageRequest pageRequest) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getDistinctTeamIds(List<String> campaignIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProspectCallLog> findByCampaignIdAndStatus(GlobalContactCriteriaDTO criteriaDTO) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getbackCallableProspects(CallableGetbackCriteriaDTO criteriaDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getDepartmentHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getTitleHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getIndustryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getCountryHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getSicCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getEmployeeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getRevenueHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getNewlyBuyProspectsOfCampaign(String campaignId) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean updateInteractionData(List<String> campaignIds, List<String> emails, List<String> phoneNumbers) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQueryQA(List<String> campaignIds,
            int duplicateCompanyDuration) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DomainCompanyCountPair> generateDuplicateProspectAggregationQuerySecondaryQA(List<String> campaignIds,
            int duplicateCompanyDuration) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DistinctCampaignIdsDTO> generateDistinctCampaignIdsAggregationQuery(String startDate, String endDate,
            List<String> agentIds) throws ParseException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getDistinctCampaignIdsForFilter(List<String> agentIds,
            ProspectCallSearchDTO prospectCallSearchDTO) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public long getNaicsCodeHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getUSStateHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getZipHealthChecksCount(CampaignCriteria campaignCriteria, ZoomBuySearchDTO zoomBuySearchDTO) {
		// TODO Auto-generated method stub
		return 0;
	}

    @Override
    public void bulkinsert(List<ProspectCallLog> prospectCallLogList) {
        // TODO Auto-generated method stub
    }

    @Override
    public void bulkUpdate(List<ProspectCallLog> prospectCallLogList) {

    }

	@Override
	public List<ProspectCallLog> getProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria)
			throws ParseException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCountOfProspectsForTranscription(TranscribeCriteriaDTO transcribeCriteria) throws ParseException {
		// TODO Auto-generated method stub
		return 0;
	}

}