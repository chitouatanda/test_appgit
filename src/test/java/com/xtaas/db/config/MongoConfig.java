package com.xtaas.db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.xtaas.db.repository.*;

@Configuration
@EnableMongoRepositories(basePackages = "com.xtaas.db.repository",
    includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { 
        CallClassificationJobQueueRepository.class,
        GlobalContactRepository.class,
        CampaignRepository.class,
        ClassifierSampleLogRepository.class,
        ClassifierStatisticsLogRepository.class
    })
)
public class MongoConfig extends AbstractMongoClientConfiguration {
    @Override
    protected String getDatabaseName() {
        return "test";
    }

    @Override
    public MongoClient mongoClient() {
        return MongoClients.create("mongodb://localhost:27017");
    }

    @Override
    public String getMappingBasePackage() {
        return "com.xtaas.db";
    }

    @Bean
    MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
            return new MongoTransactionManager(dbFactory);
    }
}
