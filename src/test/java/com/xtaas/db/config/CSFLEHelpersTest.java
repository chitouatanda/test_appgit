package com.xtaas.db.config;

import org.bson.Document;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.*;

public class CSFLEHelpersTest {

    @Test
    public void test_createJSONSchema_one_collection_multiple_attributes() {
        Document doc2 = CSFLEHelpers.createJSONSchema("secret", 
                         Arrays.asList("prospectCall.prospect.firstName",
                                       "prospectCall.prospect.lastName",
                                       "prospectCall.prospect.email",
                                       "prospectCall.prospect.phone"));
        assertThat(doc2).isEqualTo(createJSONSchema1CMA("secret"));
    }

    @Test
    public void test_createJSONSchema_multiple_collection_multiple_attributes() {
        Document doc2 = CSFLEHelpers.createJSONSchema("secret", 
                            Arrays.asList("prospectCall.prospect.firstName",
                                        "prospectCall.prospect.lastName",
                                        "prospectCall.prospect.email",
                                        "prospectCall.prospect.phone",
                                        "globalContact.firstName",
                                        "globalContact.lastName",
                                        "globalContact.email",
                                        "globalContact.phone"));
        Document createJSONSchemaMCMA = createJSONSchemaMCMA("secret");
        assertThat(doc2).isEqualTo(createJSONSchemaMCMA);
    }

    public static Document createJSONSchema1CMA(String keyId) throws IllegalArgumentException {
        return new Document()
                .append("bsonType", "object").append("encryptMetadata",
                        CSFLEHelpers.createEncryptMetadataSchema(keyId))
                .append("properties", new Document()
                    .append("prospectCall", new Document().append("bsonType", "object")
                        .append("properties", new Document()
                            .append("prospect", new Document().append("bsonType", "object")
                                .append("properties", new Document()
                                    .append("firstName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("lastName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("email", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("phone", CSFLEHelpers.buildEncryptedField("string", true)))))));
    }

    public static Document createJSONSchemaMCMA(String keyId) throws IllegalArgumentException {
        return new Document()
                .append("bsonType", "object").append("encryptMetadata",
                        CSFLEHelpers.createEncryptMetadataSchema(keyId))
                .append("properties", new Document()
                    .append("prospectCall", new Document().append("bsonType", "object")
                        .append("properties", new Document()
                            .append("prospect", new Document().append("bsonType", "object")
                                .append("properties", new Document()
                                    .append("firstName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("lastName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("email", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("phone", CSFLEHelpers.buildEncryptedField("string", true))))))
                    .append("globalContact", new Document().append("bsonType", "object")
                                .append("properties", new Document()
                                    .append("firstName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("lastName", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("email", CSFLEHelpers.buildEncryptedField("string", true))
                                    .append("phone", CSFLEHelpers.buildEncryptedField("string", true)))));
    }
}
