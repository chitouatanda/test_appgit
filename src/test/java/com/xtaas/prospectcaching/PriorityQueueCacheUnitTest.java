//package com.xtaas.prospectcaching;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertArrayEquals;
//import static org.junit.Assert.assertEquals;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.stream.Collectors;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.xtaas.application.service.AgentConferenceServiceImpl;
//import com.xtaas.application.service.AgentServiceImpl;
//import com.xtaas.application.service.AuthTokenGeneratorServiceImpl;
//import com.xtaas.application.service.CallServiceImpl;
//import com.xtaas.application.service.CampaignService;
//import com.xtaas.application.service.CampaignServiceImpl;
//import com.xtaas.application.service.DownloadRecordingsToAws;
//import com.xtaas.application.service.InternalDNCListServiceImpl;
//import com.xtaas.application.service.OutboundNumberServiceImpl;
//import com.xtaas.application.service.PlivoOutboundNumberServiceImpl;
//import com.xtaas.application.service.ProspectCallServiceImpl;
//import com.xtaas.application.service.QaServiceImpl;
//import com.xtaas.application.service.TeamServiceImpl;
//import com.xtaas.application.service.TelnyxOutboundNumberServiceImpl;
//import com.xtaas.db.config.MongoConfig;
//import com.xtaas.db.entity.Prospect;
//import com.xtaas.db.repository.AbmListDetailRepository;
//import com.xtaas.db.repository.AbmListNewRepository;
//import com.xtaas.db.repository.ActivePreviewCallQueueRepository;
//import com.xtaas.db.repository.AgentActivityLogDNormRepository;
//import com.xtaas.db.repository.AgentActivityLogRepository;
//import com.xtaas.db.repository.AgentConferenceRepository;
//import com.xtaas.db.repository.AgentRepository;
//import com.xtaas.db.repository.AgentStatusLogRepository;
//import com.xtaas.db.repository.ApplicationPropertyRepository;
//import com.xtaas.db.repository.CRMMappingRepository;
//import com.xtaas.db.repository.CallAbandonOptOutRequestRepository;
//import com.xtaas.db.repository.CallClassificationJobQueueRepository;
//import com.xtaas.db.repository.CallLogRepository;
//import com.xtaas.db.repository.CampaignContactRepository;
//import com.xtaas.db.repository.CampaignDNormRepository;
//import com.xtaas.db.repository.CampaignMetaDataRepository;
//import com.xtaas.db.repository.CampaignRepository;
//import com.xtaas.db.repository.ClientMappingRepository;
//import com.xtaas.db.repository.CountryRepository;
//import com.xtaas.db.repository.DNCListRepository;
//import com.xtaas.db.repository.DataProviderRepository;
//import com.xtaas.db.repository.EntityMappingRepository;
//import com.xtaas.db.repository.FailedRecordingToAWSRepository;
//import com.xtaas.db.repository.GeographyRepository;
//import com.xtaas.db.repository.GlobalContactRepository;
//import com.xtaas.db.repository.LoginRepository;
//import com.xtaas.db.repository.NewClientMappingRepository;
//import com.xtaas.db.repository.OrganizationRepository;
//import com.xtaas.db.repository.OrganizationTypesRepository;
//import com.xtaas.db.repository.OutboundNumberRepository;
//import com.xtaas.db.repository.PCIAnalysisRepository;
//import com.xtaas.db.repository.PartnerRepository;
//import com.xtaas.db.repository.PlivoOutboundNumberRepository;
//import com.xtaas.db.repository.ProspectCallInteractionRepository;
//import com.xtaas.db.repository.ProspectCallLogLeadIQRepository;
//import com.xtaas.db.repository.ProspectCallLogRepository;
//import com.xtaas.db.repository.QaFeedbackFormRepository;
//import com.xtaas.db.repository.QaRejectReasonRepository;
//import com.xtaas.db.repository.QaRepository;
//import com.xtaas.db.repository.RealTimeDeliveryRepository;
//import com.xtaas.db.repository.StateCallConfigRepository;
//import com.xtaas.db.repository.SuccessCallLogRepository;
//import com.xtaas.db.repository.SupervisorStatsRepository;
//import com.xtaas.db.repository.SuppressionListNewRepository;
//import com.xtaas.db.repository.TeamNoticeRepository;
//import com.xtaas.db.repository.TeamRepository;
//import com.xtaas.db.repository.TelnyxOutboundNumberRepository;
//import com.xtaas.db.repository.VerticalRepository;
//import com.xtaas.db.repository.VoiceMailRepository;
//import com.xtaas.db.repository.ZoomTokenRepository;
//import com.xtaas.domain.service.AgentCalendarServiceImpl;
//import com.xtaas.domain.service.TeamNoticeServiceImpl;
//import com.xtaas.filter.StateCodeFilter;
//import com.xtaas.filter.TimeZoneFilter;
//import com.xtaas.service.AgentRegistrationServiceImpl;
//import com.xtaas.service.CRMMappingServiceImpl;
//import com.xtaas.service.CallLogServiceImpl;
//import com.xtaas.service.ConfigServiceImpl;
//import com.xtaas.service.DataProviderServiceImpl;
//import com.xtaas.service.DialerService;
//import com.xtaas.service.DomoUtilsServiceImpl;
//import com.xtaas.service.DownloadClientMappingReportServiceImpl;
//import com.xtaas.service.GeographyServiceImpl;
//import com.xtaas.service.HttpService;
//import com.xtaas.service.LeadIQSearchServiceImpl;
//import com.xtaas.service.LeadReportServiceImpl;
//import com.xtaas.service.PlivoServiceImpl;
//import com.xtaas.service.PriorityQueueCacheOperationService;
//import com.xtaas.service.PropertyServiceImpl;
//import com.xtaas.service.ProspectCacheServiceImpl;
//import com.xtaas.service.ProspectCallLogServiceImpl;
//import com.xtaas.service.ProspectInMemoryCacheServiceImpl;
//import com.xtaas.service.RestClientService;
//import com.xtaas.service.TelnyxServiceImpl;
//import com.xtaas.service.UserServiceImpl;
//import com.xtaas.service.VoiceMailServiceImpl;
//import com.xtaas.service.XtaasUserDetailsServiceImpl;
//import com.xtaas.service.ZoomInfoSearchServiceImpl;
//import com.xtaas.utils.MCrypt;
//import com.xtaas.utils.XtaasDateUtils;
//import com.xtaas.web.dto.ProspectCallDTO;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.context.support.AnnotationConfigContextLoader;
//
//@ExtendWith(SpringExtension.class)
//@ContextConfiguration(classes = { PriorityQueueCacheOperationService.class, MongoConfig.class, CampaignRepository.class,
//		TimeZoneFilter.class, StateCodeFilter.class, ProspectCallLogServiceImpl.class,
//		TelnyxOutboundNumberRepository.class, ProspectInMemoryCacheServiceImpl.class, TeamRepository.class,
//		QaFeedbackFormRepository.class,LeadIQSearchServiceImpl.class,
//		CampaignServiceImpl.class, AgentServiceImpl.class, AgentRepository.class, RealTimeDeliveryRepository.class,
//		VerticalRepository.class, CallLogRepository.class, AgentActivityLogRepository.class,
//		AgentRegistrationServiceImpl.class, AgentActivityLogDNormRepository.class, DownloadRecordingsToAws.class,
//		FailedRecordingToAWSRepository.class, ZoomTokenRepository.class, ProspectCallServiceImpl.class,
//		GlobalContactRepository.class, CallClassificationJobQueueRepository.class, TelnyxServiceImpl.class,
//		DomoUtilsServiceImpl.class, PCIAnalysisRepository.class, AgentStatusLogRepository.class, MCrypt.class,
//		ProspectCacheServiceImpl.class, SuccessCallLogRepository.class, LeadReportServiceImpl.class,
//		ActivePreviewCallQueueRepository.class, PropertyServiceImpl.class, CallServiceImpl.class,ProspectCallLogLeadIQRepository.class,
//		ConfigServiceImpl.class, QaServiceImpl.class, LoginRepository.class, CallLogServiceImpl.class,
//		InternalDNCListServiceImpl.class, AgentCalendarServiceImpl.class, UserServiceImpl.class,
//		GeographyServiceImpl.class, OrganizationRepository.class, ProspectCallInteractionRepository.class,
//		AgentConferenceServiceImpl.class, AgentConferenceRepository.class, ProspectCallLogRepository.class,
//		ApplicationPropertyRepository.class, GeographyRepository.class, XtaasUserDetailsServiceImpl.class,
//		CampaignContactRepository.class, SupervisorStatsRepository.class, OrganizationTypesRepository.class,
//		DataProviderServiceImpl.class, DataProviderRepository.class, DNCListRepository.class, QaRepository.class,
//		ClientMappingRepository.class, NewClientMappingRepository.class, AbmListNewRepository.class,ZoomInfoSearchServiceImpl.class,
//		QaRejectReasonRepository.class, DownloadClientMappingReportServiceImpl.class, AbmListDetailRepository.class,
//		TeamNoticeServiceImpl.class, EntityMappingRepository.class,TeamNoticeRepository.class, HttpService.class,
//		TelnyxOutboundNumberServiceImpl.class, SuppressionListNewRepository.class, PlivoServiceImpl.class,
//		PlivoOutboundNumberServiceImpl.class, OutboundNumberRepository.class, PlivoOutboundNumberRepository.class, CampaignMetaDataRepository.class,
//		CallAbandonOptOutRequestRepository.class, CountryRepository.class,StateCallConfigRepository.class, VoiceMailServiceImpl.class, CRMMappingRepository.class,
//		VoiceMailRepository.class, DialerService.class, RestClientService.class,PartnerRepository.class,OutboundNumberServiceImpl.class,
//		AuthTokenGeneratorServiceImpl.class, CampaignDNormRepository.class, TeamServiceImpl.class, CRMMappingServiceImpl.class }, loader = AnnotationConfigContextLoader.class)
//public class PriorityQueueCacheUnitTest {
//
//	final private PriorityQueueCacheOperationService priorityQueueCacheOperationService;
//
//	private Comparator<ProspectCallDTO> qualityLifoOrder;
//
//	public PriorityQueueCacheUnitTest(
//			@Autowired PriorityQueueCacheOperationService priorityQueueCacheOperationService) {
//		this.priorityQueueCacheOperationService = priorityQueueCacheOperationService;
//	}
//
//	@BeforeEach
//	public void setup() {
//		ReflectionTestUtils.setField(priorityQueueCacheOperationService, "listOfProspectsInCache",
//				new ConcurrentHashMap<>());
//		qualityLifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort);
//	}
//
//	@Test // It should insert the given prospects in then cache.
//	public void should_insert_prospects_in_cache() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(1);
//	}
//
//	@Test // It should append the given prospect to the cache.
//	public void should_append_prospects_in_cache() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> dtoList1 = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto.setProspectCallId("1234");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList1.add(dto1);
//		priorityQueueCacheOperationService.insert("1", dtoList1, qualityLifoOrder);
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(2);
//	}
//
//	@Test // It should not append the given prospect to the cache if prospect is already
//			// present in cache.
//	public void should_not_append_prospects_in_cache() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> dtoList1 = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("123");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList1.add(dto1);
//		priorityQueueCacheOperationService.insert("1", dtoList1, qualityLifoOrder);
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(1);
//	}
//
//	@Test // The cache should be empty for campaign if empty list of prospects passed.
//	public void empty_cache_if_empty_list_of_prosect_campaign() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(0);
//	}
//
//	@Test // It should clear the cache of given campaign.
//	public void should_clear_cache_given_campaign() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		priorityQueueCacheOperationService.clearCampaignCache("1");
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(0);
//	}
//
//	@Test // It should give the count of prospects from cache and remove from cache of given campaign.
//	public void should_remove_prospects_from_cache() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("1111");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setProspect(new Prospect());
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setProspect(new Prospect());
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospectsFromCache = priorityQueueCacheOperationService.remove("1", 2, false);
//		assertThat(prospectsFromCache).isNotNull().hasSize(2);
//		assertArrayEquals(dtoList.toArray(), prospectsFromCache.toArray());
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(0);
//	}
//	
//	@Test // It should give the count of prospects from cache when timezone is open
//	public void should_remove_prospects_from_cache_when_timezone_open() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("1111");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		Prospect prospect = new Prospect();
//		prospect.setStateCode("ZZ");
//		dto1.setProspect(prospect);
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		Prospect prospect1 = new Prospect();
//		prospect1.setStateCode("ZZ");
//		dto.setProspect(prospect1);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospectsFromCache = priorityQueueCacheOperationService.remove("1", 2, true);
//		assertThat(prospectsFromCache).isNotNull().hasSize(2);
//		assertArrayEquals(dtoList.toArray(), prospectsFromCache.toArray());
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(0);
//	}
//
//	@Test // It should give the empty list from cache when campaignId is not present in cache.
//	public void should_give_empty_list_when_campaign_not_present_in_cache() {
//		List<ProspectCallDTO> prospectsFromCache = priorityQueueCacheOperationService.remove("1", 2, false);
//		assertThat(prospectsFromCache).isNotNull().hasSize(0);
//	}
//
//	@Test // It should give the count of prospects from cache and remove from cache of given campaign.
//	public void should_give_noOfProspects_present_in_cache() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("1111");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setProspect(new Prospect());
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setProspect(new Prospect());
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospectsFromCache = priorityQueueCacheOperationService.remove("1", 5, false);
//		assertThat(prospectsFromCache).isNotNull().hasSize(2);
//	}
//
//	@Test // It should give the count of prospects from cache and remove from cache of given campaign by retryspeed.
//	public void should_give_noOfProspects_retryspeed() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("1111");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setProspect(new Prospect());
//		dto1.setCallRetryCount(1);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setProspect(new Prospect());
//		dto.setCallRetryCount(2);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		List<Float> ratios = new ArrayList<>();
//		ratios.add(1.7f);
//		ratios.add(2.0f);
//		ratios.add(2.0f);
//		ratios.add(2.0f);
//		ratios.add(2.2f);
//		ratios.add(2.2f);
//		ratios.add(2.5f);
//		ratios.add(2.5f);
//		ratios.add(2.599f);
//		ratios.add(2.599f);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospectsFromCache = priorityQueueCacheOperationService.removeProspectsByRatio("1", 1.5f,
//				ratios, false);
//		assertThat(prospectsFromCache).isNotNull().hasSize(1);
//	}
//
//	@Test // It should sort the list ascending.(FIFO)
//	public void should_sort_list_ascending() {
//		Comparator<ProspectCallDTO> fifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getCreatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setCallRetryCount(13);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -2));
//		dto1.setProspectCallId("1");
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 2));
//		dto.setProspectCallId("2");
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, fifoOrder);
//		List<ProspectCallDTO> data = priorityQueueCacheOperationService.getCurrentState("1");
//		data.stream().forEach(v -> System.out.println(v.getCallRetryCount()));
//		assertEquals(10, data.get(0).getCallRetryCount());
//		assertEquals(13, data.get(1).getCallRetryCount());
////		assertArrayEquals(getProspectList().toArray(), data.toArray());
//	}
//
//	@Test // It should sort the list ascending.(LIFO)
//	public void should_sort_list_asc_callretry_count() {
//		Comparator<ProspectCallDTO> fifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getCreatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setCallRetryCount(10);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setCallRetryCount(13);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, fifoOrder);
//		List<ProspectCallDTO> data = priorityQueueCacheOperationService.getCurrentState("1");
//		data.stream().forEach(v -> System.out.println(v.getCallRetryCount()));
//		assertEquals(10, data.get(0).getCallRetryCount());
//		assertEquals(13, data.get(1).getCallRetryCount());
//	}
//
//	@Test // It should sort the list descending.(LIFO)
//	public void should_sort_list_desc_callretry_count() {
//		Comparator<ProspectCallDTO> fifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getCreatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setCallRetryCount(10);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, fifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // It should sort the list(QUALITY_FIFO)
//	public void should_sort_list_asc_quality_fifo() {
//		Comparator<ProspectCallDTO> qualityFifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount).thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket should sort on callretrycount.(QUALITY_FIFO)
//	public void should_sort_list_quality_fifo_on_callretry_count() {
//		Comparator<ProspectCallDTO> qualityFifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount).thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket same callretry count should sort on updatedDate.(QUALITY_FIFO)
//	public void should_sort_list_quality_fifo_on_updated_date() {
//		Comparator<ProspectCallDTO> qualityFifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount).thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(10);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket same updated date should sort on updatedDate.(QUALITY_FIFO)
//	public void should_sort_list_quality_fifo_on() {
//		Comparator<ProspectCallDTO> qualityFifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount).thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(20);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // It should sort the list(QUALITY_LIFO)
//	public void should_sort_list_asc_quality_lifo() {
//		Comparator<ProspectCallDTO> qualityLifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket should sort on callretrycount.(QUALITY_LIFO)
//	public void should_sort_list_quality_lifo_on_callretry_count() {
//		Comparator<ProspectCallDTO> qualityLifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket same callretry count should sort on updatedDate.(QUALITY_LIFO)
//	public void should_sort_list_quality_lifo_on_updated_date() {
//		Comparator<ProspectCallDTO> qualityLifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(10);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same quality bucket same updated date should sort on updatedDate.(QUALITY_LIFO)
//	public void should_sort_list_quality_filo_on() {
//		Comparator<ProspectCallDTO> qualityLifoOrder = Comparator.comparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(20);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // It should sort the list(RETRY_FIFO)
//	public void should_sort_list_asc_retry_fifo() {
//		Comparator<ProspectCallDTO> retryFifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same callretry count should sort on quality bucket.(RETRY_FIFO)
//	public void should_sort_list_retry_fifo_on_callretry_count() {
//		Comparator<ProspectCallDTO> retryFifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(12);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // same callretry count Same quality bucket should sort on updatedDate.(RETRY_FIFO)
//	public void should_sort_list_retry_fifo_on_updated_date() {
//		Comparator<ProspectCallDTO> retryFifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(10);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same callretrycount same updated date should sort on quality bucket.(RETRY_FIFO)
//	public void should_sort_list_retry_fifo_on() {
//		Comparator<ProspectCallDTO> retryFifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate);
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(20);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(20);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryFifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // It should sort the list(RETRY_LIFO)
//	public void should_sort_list_asc_retry_lifo() {
//		Comparator<ProspectCallDTO> retryLifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(10);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same callretrycount should sort on quality bucket.(RETRY_LIFO)
//	public void should_sort_list_retry_lifo_on_callretry_count() {
//		Comparator<ProspectCallDTO> retryLifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(12);
//		dto1.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(12);
//		dto.setCreatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // same callretry count Same quality bucket  should sort on updatedDate DESC.(RETRY_LIFO)
//	public void should_sort_list_retry_lifo_on_updated_date() {
//		Comparator<ProspectCallDTO> retryLifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(10);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), -15));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(1111);
//		dto.setCallRetryCount(10);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("2222", "1111");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // Same callretry count and  same updated date should sort on Quality bucket.(RETRY_LIFO)
//	public void should_sort_list_retry_lifo_on() {
//		Comparator<ProspectCallDTO> retryLifoOrder = Comparator.comparing(ProspectCallDTO::getCallRetryCount)
//				.thenComparing(ProspectCallDTO::getQualityBucketSort)
//				.thenComparing(ProspectCallDTO::getUpdatedDate, Comparator.reverseOrder());
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setProspectCallId("1111");
//		dto1.setQualityBucketSort(1111);
//		dto1.setCallRetryCount(20);
//		dto1.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto1.setCampaignId("1");
//		dtoList.add(dto1);
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setProspectCallId("2222");
//		dto.setQualityBucketSort(2222);
//		dto.setCallRetryCount(20);
//		dto.setUpdatedDate(XtaasDateUtils.getNextDate(new Date(), 20));
//		dto.setCampaignId("1");
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, retryLifoOrder);
//		List<ProspectCallDTO> prospects = priorityQueueCacheOperationService.getCurrentState("1");
//		List<String> actualList = prospects.stream().map(v -> v.getProspectCallId()).collect(Collectors.toList());
//		List<String> expectedList = Arrays.asList("1111", "2222");
//		assertArrayEquals(expectedList.toArray(), actualList.toArray());
//	}
//	
//	@Test // It should not append the given prospect to the cache if prospect is already present in cache.
//	public void should_not_append_prospects_in_cache_for_more_than_one() {
//		List<ProspectCallDTO> dtoList = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto = new ProspectCallDTO();
//		dto.setAgentId("ixagent3");
//		dto.setProspectCallId("123");
//		dto.setVoiceProvider("PLIVO");
//		dto.setAutoMachineDetected(false);
//		dto.setDailyCallRetryCount(2020120902L);
//		dto.setCallRetryCount(1234);
//		dto.setCampaignId("1");
//		dto.setQualityBucketSort(2551);
//		dtoList.add(dto);
//		priorityQueueCacheOperationService.insert("1", dtoList, qualityLifoOrder);
//		List<ProspectCallDTO> dtoList1 = new ArrayList<ProspectCallDTO>();
//		ProspectCallDTO dto1 = new ProspectCallDTO();
//		dto1.setAgentId("ixagent3");
//		dto1.setProspectCallId("123");
//		dto1.setVoiceProvider("PLIVO");
//		dto1.setAutoMachineDetected(false);
//		dto1.setDailyCallRetryCount(2020120902L);
//		dto1.setCallRetryCount(1234);
//		dto1.setCampaignId("1");
//		dto1.setQualityBucketSort(2551);
//		dtoList1.add(dto1);
//		ProspectCallDTO dto2 = new ProspectCallDTO();
//		dto2.setAgentId("ixagent3");
//		dto2.setProspectCallId("11111");
//		dto2.setVoiceProvider("PLIVO");
//		dto2.setAutoMachineDetected(false);
//		dto2.setDailyCallRetryCount(2020120902L);
//		dto2.setCallRetryCount(1234);
//		dto2.setCampaignId("1");
//		dto2.setQualityBucketSort(2551);
//		dtoList1.add(dto2);
//		ProspectCallDTO dto3 = new ProspectCallDTO();
//		dto3.setAgentId("ixagent3");
//		dto3.setProspectCallId("22222");
//		dto3.setVoiceProvider("PLIVO");
//		dto3.setAutoMachineDetected(false);
//		dto3.setDailyCallRetryCount(2020120902L);
//		dto3.setCallRetryCount(1234);
//		dto3.setCampaignId("1");
//		dto3.setQualityBucketSort(2551);
//		dtoList1.add(dto3);
//		priorityQueueCacheOperationService.insert("1", dtoList1, qualityLifoOrder);
//		assertThat(priorityQueueCacheOperationService.getCurrentState("1")).isNotNull().hasSize(3);
//	}
//		
//}