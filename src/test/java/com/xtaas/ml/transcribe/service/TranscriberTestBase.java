package com.xtaas.ml.transcribe.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import com.xtaas.ml.transcribe.dto.AwsS3PersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

import org.apache.commons.lang3.ArrayUtils;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
public class TranscriberTestBase {

    /**
     * Default constructor
     */
    public TranscriberTestBase() {

    }

    protected List<S3AudioMediaLink> getTestAudioMediaLinks() {
        return getTestAudioMediaLinks("us-west-2");
    }
    
    protected List<S3AudioMediaLink> getTestAudioMediaLinks(String region) {
        return Arrays.asList(
            // indirect, machine
            new S3AudioMediaLink(URI.create("https://s3-" + region +  ".amazonaws.com/xtaasrecordings/recordings/fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav")),
            // indirect, machine
            new S3AudioMediaLink(URI.create("https://s3-" + region + ".amazonaws.com/xtaasrecordings/recordings/480f2514-49ad-11ea-82de-02a4a4317a2f.wav"))
        );
    }

    static File getOutFile(JobResult jobResult, String[] outfileNameHolder) {
        String jobId = jobResult.getJobTracker().getJobTrailId().toString().substring(30);
        try {
            Path path = Files.createTempFile("transcribe-text-urls-" + jobId, ".txt");
            outfileNameHolder[0] = path.toString();
            File file = path.toFile();
            file.deleteOnExit();
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static void postProcessTranscript(JobResult jobResult, TranscriptPersistenceResult persistenceResult,
            Function<URI, String> urlShortener, File outFile) {
        String shortTranscriptUri = urlShortener.apply(((AwsS3PersistenceResult)persistenceResult).getTranscriptUri());
        String shortMediaFileUri = urlShortener.apply(jobResult.getJobTracker().getMediaLink().getMediaUri());
        // System.out.println(shortTranscriptUri);
    
        // File outFile = new File(jobResult.getOutFile());
        try {
            if (!outFile.exists()) {
                outFile.getParentFile().mkdirs();
                outFile.createNewFile();
            }
            try (
                    FileWriter writer = new FileWriter(outFile, true);
                    ICSVWriter csvwriter = new CSVWriterBuilder(writer)
                                                .withQuoteChar(CSVWriter.NO_QUOTE_CHARACTER)
                                                .build()) {
                csvwriter.writeNext(ArrayUtils.addAll(new String[]{shortMediaFileUri, shortTranscriptUri}));
                    // jobResult.getJobTracker().getLabels().toArray(new String[0])));
            }
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
