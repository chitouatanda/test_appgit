package com.xtaas.ml.transcribe.service;
import static com.xtaas.ml.util.TestUtil.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.xtaas.ml.transcribe.dto.InMemoryPersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AwsTranscriptionService.class, AppTekTranscriptionService.class,
        TranscriberFactory.class, TranscriberThreadPoolConfig.class}, loader = AnnotationConfigContextLoader.class)
public class AdhocTranscribeJob {
    final private TranscriberFactory factory;
    private String transcriptionServiceProvider;
    public AdhocTranscribeJob(@Autowired TranscriberFactory factory, @Autowired ShortenUrlService shortenUrlService,
            @Autowired AwsS3PersistenceService persistenceService) {
        this.factory = factory;
        this.transcriptionServiceProvider = "AWS";
    }
    // @Test
    public void transcribe_job() throws IOException {
        // Path infile = getFilePathFromSamePackageAsClass(getClass(), "transcribe-test-input-10.csv"),
        //         outfile = getFileCreateFileAndAncestorsIfRequired("data/transcription/",
        //                 "transcribe-test-input-10.csv");
        Path infile = getFileCreateFileAndAncestorsIfRequired("/Users/sushant/Desktop/xtaas_transcription/",
                "Incoming_Voicemail_XTaaS_April_to_date.csv"),
             outfile = getFileCreateFileAndAncestorsIfRequired("data/transcription/",
                "Incoming_Voicemail_XTaaS_April_to_date.csv");
        transcribe(infile, outfile);
        validate(infile, outfile);
    }
    private void transcribe(Path infile, Path outfile) throws IOException {
        BlockingQueue<AdhocTranscribeJobIO> queue = new ArrayBlockingQueue<>(1000);
        AtomicBoolean transcriptionComplete = new AtomicBoolean(false);
        try (Writer writer = new FileWriter(outfile.toFile())) {
            Thread csvWriterThread = new Thread(initOutputWriterRunnable(writer, queue, transcriptionComplete));
            csvWriterThread.start();
            List<S3AudioMediaLink> mediaLinks = getAudioMediaLinksFromCsv(infile);
            CountDownLatch latch = new CountDownLatch(mediaLinks.size());
            final Transcriber service = factory.getTrancriber(
                    () -> TranscriberConfig.builder().withTranscriptionServiceProvider(transcriptionServiceProvider)
                            .withPersistenceService((j) -> new InMemoryPersistenceResult(
                                    getTranscriptText(j.getTrancriptDataInputStream())))
                            .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, queue))
                            .withTranscriptPostProcessConsumer((j, r) -> latch.countDown()));
            CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
            statusFuture.join();
            transcriptionComplete.set(true);
            csvWriterThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
    private Runnable initOutputWriterRunnable(Writer writer, BlockingQueue<AdhocTranscribeJobIO> queue, 
            AtomicBoolean transcriptionComplete) {
        StatefulBeanToCsv<AdhocTranscribeJobIO> beanToCsv = 
            new StatefulBeanToCsvBuilder<AdhocTranscribeJobIO>(writer)
                .withSeparator(',')
                .withQuotechar(CSVWriter.DEFAULT_QUOTE_CHARACTER)
                .build();
        return () -> {
            try {
                while (!transcriptionComplete.get()) {
                    AdhocTranscribeJobIO input = queue.poll(100, TimeUnit.MILLISECONDS);
                    if (input == null) {
                        continue;
                    }
                    beanToCsv.write(input);
                    writer.flush();
                }
            } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException | IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        };
    }
    // AWS response, will be different for Apptek
    private static String getTranscriptText(InputStream in) {
        try {
            String transcriptPath = "$['results']['transcripts'][0]['transcript']";
            String trasncriptJson = IOUtils.toString(in, "UTF-8");
            DocumentContext jsonContext = JsonPath.parse(trasncriptJson);
            String transcriptText = jsonContext.read(transcriptPath);
            System.out.println(transcriptText);
            return transcriptText;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private static void postProcessTranscript(JobResult jobResult, TranscriptPersistenceResult persistenceResult,
            BlockingQueue<AdhocTranscribeJobIO> queue) {
        try {
            S3AudioMediaLinkAdhocTranscribeJobInputAdapter mediaLinkAdapter = 
                (S3AudioMediaLinkAdhocTranscribeJobInputAdapter) jobResult
                    .getJobTracker().getMediaLink();
            AdhocTranscribeJobIO input = mediaLinkAdapter.getAdhocTranscribeJobInput();
            input.setTranscript(persistenceResult.getTranscript());
            queue.put(input);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
    private List<S3AudioMediaLink> getAudioMediaLinksFromCsv(Path infile) {
        
        try (
            Reader reader = Files.newBufferedReader(infile);
        ) {
            List<S3AudioMediaLink> links = new ArrayList<S3AudioMediaLink>();
            CsvToBean<AdhocTranscribeJobIO> csvToBean = 
                new CsvToBeanBuilder<AdhocTranscribeJobIO>(reader)
                    .withType(AdhocTranscribeJobIO.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            Iterator<AdhocTranscribeJobIO> jobInputIterator = csvToBean.iterator();
            while (jobInputIterator.hasNext()) {
                AdhocTranscribeJobIO jobInput = jobInputIterator.next();
                links.add(new S3AudioMediaLinkAdhocTranscribeJobInputAdapter(jobInput));
            }
            links.stream().forEach(l -> System.out.println(l.getMediaUri()));
            return links;
        } catch (SecurityException | IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void validate(Path infile, Path outfile) {
    }
    static private class S3AudioMediaLinkAdhocTranscribeJobInputAdapter extends S3AudioMediaLink {
        private AdhocTranscribeJobIO jobInput;

        S3AudioMediaLinkAdhocTranscribeJobInputAdapter(AdhocTranscribeJobIO in) {
            super(URI.create(in.getAwsAudioUrl()));
            this.jobInput = in;
        }

        // @Override
        // public URI getMediaUri() {
        //     return URI.create(jobInput.getSpeech());
        // }
        public AdhocTranscribeJobIO getAdhocTranscribeJobInput() {
            return jobInput;
        }
    }
}