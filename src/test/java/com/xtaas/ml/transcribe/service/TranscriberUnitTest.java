package com.xtaas.ml.transcribe.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.IOException;
import java.net.URL;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.transcribe.AmazonTranscribe;
import com.amazonaws.services.transcribe.AmazonTranscribeClient;
import com.amazonaws.services.transcribe.AmazonTranscribeClientBuilder;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobResult;
import com.amazonaws.services.transcribe.model.InternalFailureException;
import com.amazonaws.services.transcribe.model.LanguageCode;
import com.amazonaws.services.transcribe.model.LimitExceededException;
import com.amazonaws.services.transcribe.model.Media;
import com.amazonaws.services.transcribe.model.Settings;
import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.StartTranscriptionJobResult;
import com.amazonaws.services.transcribe.model.Transcript;
import com.amazonaws.services.transcribe.model.TranscriptionJob;
import com.google.cloud.automl.v1.AnnotationPayload;
import com.google.cloud.automl.v1.ClassificationAnnotation;
import com.google.cloud.automl.v1.PredictResponse;
import com.google.cloud.automl.v1.PredictionServiceClient;
import com.xtaas.ml.classifier.service.GoogleClassifier;
import com.xtaas.ml.transcribe.dto.InMemoryPersistenceResult;
import com.xtaas.ml.transcribe.dto.JobResult;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.TranscriptPersistenceResult;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = { GoogleClassifier.class, TranscriberUnitTest.class, TranscriberFactory.class,
        AwsS3PersistenceService.class, InMemoryPersistenceService.class, AwsTranscriptionService.class,
        TranscriberThreadPoolConfig.class, }, loader = AnnotationConfigContextLoader.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*" })
@TestPropertySource(properties = {"transcribe.status_check.wait_time_in_ms = 1"})
@PrepareForTest({ PredictionServiceClient.class, 
        PredictResponse.class, AnnotationPayload.class,
        ClassificationAnnotation.class, AmazonS3ClientBuilder.class, AmazonTranscribeClient.class,
        AmazonTranscribeClientBuilder.class, AwsTranscriptionService.class, Instant.class })
public class TranscriberUnitTest extends TranscriberTestBase {
    final static private Logger logger = LoggerFactory.getLogger(Transcriber.class);
    @Autowired private TranscriberFactory factory;
    @Autowired private InMemoryPersistenceService inMemoryPersistenceService;

    // The value of AmazonTranscribe client is initiated once and cached in AWSTranscription class
    // Since by default JUnit lifecycle is per method we have to define field as static
    // in JUnit5 we can use @TestInstance(LifeCycle.PER_CLASS)
    private static AmazonTranscribe mockTrClient;

    /**
     * Default constructor
     */
    public TranscriberUnitTest() {
    }

    @Test
    public void transcribe_aws_success() throws Exception {
        String METHOD_NAME = "transcribe_aws_success";
        AmazonTranscribe mockTrClient = getMockTranscriptionClient();

        // inject utc time 2014-12-22T10:15:30Z, timestamp 1419243330000 to construct job id
        injectFixedTimeInstant("2014-12-22T10:15:30Z");

        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks("test-region");
        logger.debug("{}:, input: {}" , METHOD_NAME, mediaLinks);
        
        mockTrancriptionJob(mockTrClient, //jobId1, trtxt1, numTimesCheck, 
        "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav.1419243330000",
        "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript1\"}]}}",
        3, mediaLinks.get(0).getMediaUri().toString(), "test-transcript-uri-1");

        mockTrancriptionJob(mockTrClient,
            "480f2514-49ad-11ea-82de-02a4a4317a2f.wav.1419243330000",
            "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript2\"}]}}", 
            5, mediaLinks.get(1).getMediaUri().toString(), "test-transcript-uri-2"); 

        String transcriptionServiceProvider = "AWS";
        List<AudioMediaTranscript> transcripts = new CopyOnWriteArrayList<>();

        TrafficManager mgr = mock(TrafficManager.class);

        Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true)
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withTrafficManager(mgr)
                .withPersistenceService((j) -> inMemoryPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, 
                        transcripts))
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        List<String> status = statusFuture.get();
        logger.debug("{}: finished processing. status: {}, transcripts: {}", METHOD_NAME, status, transcripts);
        // verify(mockTrClient, times(2)).startTranscriptionJob((StartTranscriptionJobRequest) any());
        assertEquals(2, transcripts.size());
        transcripts.stream().forEach(t -> {
            switch (t.getObjectId()) {
                case "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript1", t.getTranscript());
                    break;
                case "480f2514-49ad-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript2", t.getTranscript());
                    break;
            
                default:
                    break;
            }
        });
        // important to verify that tokens are acquired and released correctly
        // and there is no leak
        verify(mgr, times(2)).acquire();
        verify(mgr, times(2)).release();
    }


    // @Test
    public void transcribe_aws_check_status_polling_retries_exceeded_failure() throws Exception {
        String METHOD_NAME = "transcribe_aws_check_status_polling_retries_exceeded_failure";
        AmazonTranscribe mockTrClient = getMockTranscriptionClient();

        // inject utc time 2014-12-22T10:15:30Z, timestamp 1419243330000 to construct job id
        injectFixedTimeInstant("2014-12-22T10:15:30Z");

        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks("test-region");
        logger.debug("{}:, input: {}", METHOD_NAME, mediaLinks);
        
        mockTrancriptionJob(mockTrClient, //jobId1, trtxt1, numTimesCheck, 
        "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav.1419243330000",
        "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript1\"}]}}",
        3, mediaLinks.get(0).getMediaUri().toString(), "test-transcript-uri-1");

        mockTrancriptionJob(mockTrClient,
            "480f2514-49ad-11ea-82de-02a4a4317a2f.wav.1419243330000",
            "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript2\"}]}}", 
            25, mediaLinks.get(1).getMediaUri().toString(), "test-transcript-uri-2"); 

        String transcriptionServiceProvider = "AWS";
        List<AudioMediaTranscript> transcripts = new CopyOnWriteArrayList<>();
        TrafficManager mgr = mock(TrafficManager.class);

        Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true)
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withTrafficManager(mgr)
                .withPersistenceService((j) -> inMemoryPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, 
                        transcripts))
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        List<String> status = statusFuture.get();
        logger.debug("{}: finished processing. status: {}, transcripts: {}", METHOD_NAME, status, transcripts);
        // verify(mockTrClient, times(2)).startTranscriptionJob((StartTranscriptionJobRequest) any());
        assertEquals(2, transcripts.size());
        transcripts.stream().forEach(t -> {
            switch (t.getObjectId()) {
                case "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript1", t.getTranscript());
                    break;
            
                default:
                    break;
            }
        });
        verify(mgr, times(2)).acquire();
        verify(mgr, times(2)).release();
    }


    @Test
    public void transcribe_aws_check_status_poll_throws_exception() throws Exception {
        String METHOD_NAME = "transcribe_aws_check_status_poll_throws_exception";
        AmazonTranscribe mockTrClient = getMockTranscriptionClient();

        // inject utc time 2014-12-22T10:15:30Z, timestamp 1419243330000 to construct job id
        injectFixedTimeInstant("2014-12-22T10:15:30Z");
        
        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks("test-region");
        logger.debug("{}:, input: {}", METHOD_NAME, mediaLinks);
        mockTrancriptionJob(mockTrClient, //jobId1, trtxt1, numTimesCheck, 
        "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav.1419243330000",
        "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript1\"}]}}",
        3, mediaLinks.get(0).getMediaUri().toString(), "test-transcript-uri-1");

        mockTranscriptionJobCheckStatusExceptionally(mockTrClient,
            "480f2514-49ad-11ea-82de-02a4a4317a2f.wav.1419243330000",
            "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript2\"}]}}", 
            25, mediaLinks.get(1).getMediaUri().toString(), "test-transcript-uri-2"); 

        String transcriptionServiceProvider = "AWS";
        List<AudioMediaTranscript> transcripts = new CopyOnWriteArrayList<>();
        TrafficManager mgr = mock(TrafficManager.class);

        Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true)
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withTrafficManager(mgr)
                .withPersistenceService((j) -> inMemoryPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, 
                        transcripts))
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        List<String> status = statusFuture.get();
        logger.debug("{}: finished processing. status: {}, transcripts: {}", METHOD_NAME, status, transcripts);
        // verify(mockTrClient, times(2)).startTranscriptionJob((StartTranscriptionJobRequest) any());
        assertEquals(1, transcripts.size());
        transcripts.stream().forEach(t -> {
            switch (t.getObjectId()) {
                case "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript1", t.getTranscript());
                    break;
            
                default:
                    break;
            }
        });
        verify(mgr, times(2)).acquire();
        verify(mgr, times(2)).release();
    }


    @Test
    public void transcribe_aws_check_status_poll_failed_status() throws Exception {
        String METHOD_NAME = "transcribe_aws_check_status_poll_throws_exception";
        AmazonTranscribe mockTrClient = getMockTranscriptionClient();

        // inject utc time 2014-12-22T10:15:30Z, timestamp 1419243330000 to construct job id
        injectFixedTimeInstant("2014-12-22T10:15:30Z");
        
        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks("test-region");
        logger.debug("{}:, input: {}", METHOD_NAME, mediaLinks);
        mockTrancriptionJob(mockTrClient, //jobId1, trtxt1, numTimesCheck, 
        "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav.1419243330000",
        "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript1\"}]}}",
        3, mediaLinks.get(0).getMediaUri().toString(), "test-transcript-uri-1");

        mockTranscriptionJobCheckStatusReturnsFailed(mockTrClient,
            "480f2514-49ad-11ea-82de-02a4a4317a2f.wav.1419243330000",
            "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript2\"}]}}", 
            25, mediaLinks.get(1).getMediaUri().toString(), "test-transcript-uri-2"); 

        String transcriptionServiceProvider = "AWS";
        List<AudioMediaTranscript> transcripts = new CopyOnWriteArrayList<>();
        TrafficManager mgr = mock(TrafficManager.class);

        Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true)
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withTrafficManager(mgr)
                .withPersistenceService((j) -> inMemoryPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, 
                        transcripts))
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        List<String> status = statusFuture.get();
        logger.debug("{}: finished processing. status: {}, transcripts: {}", METHOD_NAME, status, transcripts);
        // verify(mockTrClient, times(2)).startTranscriptionJob((StartTranscriptionJobRequest) any());
        assertEquals(1, transcripts.size());
        transcripts.stream().forEach(t -> {
            switch (t.getObjectId()) {
                case "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript1", t.getTranscript());
                    break;
            
                default:
                    break;
            }
        });
        verify(mgr, times(2)).acquire();
        verify(mgr, times(2)).release();
    }

    @Test
    public void transcribe_aws_submit_req_throws_exception() throws Exception {
        String METHOD_NAME = "transcribe_aws_submit_req_throws_exception";
        AmazonTranscribe mockTrClient = getMockTranscriptionClient();

        // inject utc time 2014-12-22T10:15:30Z, timestamp 1419243330000 to construct job id
        injectFixedTimeInstant("2014-12-22T10:15:30Z");
        
        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks("test-region");
        logger.debug("{}:, input: {}", METHOD_NAME, mediaLinks);
        mockTrancriptionJob(mockTrClient, //jobId1, trtxt1, numTimesCheck, 
        "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f.wav.1419243330000",
        "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript1\"}]}}",
        3, mediaLinks.get(0).getMediaUri().toString(), "test-transcript-uri-1");

        mockTranscriptionJobSubmitReqExceptionally(mockTrClient,
            "480f2514-49ad-11ea-82de-02a4a4317a2f.wav.1419243330000",
            "{\"results\" : { \"transcripts\" : [{\"transcript\" : \"Hello, this is transcript2\"}]}}", 
            25, mediaLinks.get(1).getMediaUri().toString(), "test-transcript-uri-2"); 

        String transcriptionServiceProvider = "AWS";
        List<AudioMediaTranscript> transcripts = new CopyOnWriteArrayList<>();
        TrafficManager mgr = mock(TrafficManager.class);

        Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                .withMediaLinkValidator((l) -> true)
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withTrafficManager(mgr)
                .withPersistenceService((j) -> inMemoryPersistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> postProcessTranscript(j, r, 
                        transcripts))
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        List<String> status = statusFuture.get();
        logger.debug("{}: finished processing. status: {}, transcripts: {}", METHOD_NAME, status, transcripts);
        // verify(mockTrClient, times(2)).startTranscriptionJob((StartTranscriptionJobRequest) any());
        assertEquals(1, transcripts.size());
        transcripts.stream().forEach(t -> {
            switch (t.getObjectId()) {
                case "fbf9d7b4-49ac-11ea-82de-02a4a4317a2f":
                    assertEquals("Hello, this is transcript1", t.getTranscript());
                    break;
                // case "480f2514-49ad-11ea-82de-02a4a4317a2f":
                //     assertEquals("Hello, this is transcript2", t.getTranscript());
                //     break;
            
                default:
                    break;
            }
        });
        verify(mgr, times(2)).acquire();
        verify(mgr, times(2)).release();
    }


    private void injectFixedTimeInstant(String instantExpected) {
        Clock clock = Clock.fixed(Instant.parse(instantExpected), ZoneId.of("UTC"));
        Instant instant = Instant.now(clock);
        mockStatic(Instant.class);
        when(Instant.now()).thenReturn(instant);
    }

    private void mockTrancriptionJob(AmazonTranscribe mockTrClient, String jobId, String trtxt, int numTimesCheck, String mediaUri, String transcriptUri)
            throws Exception, IOException {
        StartTranscriptionJobResult jobStartResult = mock(StartTranscriptionJobResult.class);
        TranscriptionJob job = mock(TranscriptionJob.class);
        StartTranscriptionJobRequest startReq = new StartTranscriptionJobRequest()
                    .withMedia(new Media().withMediaFileUri(mediaUri))
                    .withMediaFormat("wav").withLanguageCode(LanguageCode.EnUS).withTranscriptionJobName(jobId)
                    .withSettings(new Settings().withChannelIdentification(true));
        // when(mockTrClient.startTranscriptionJob(isA(StartTranscriptionJobRequest.class))).thenReturn(jobStartResult);
        // have to use doReturn because mockTrClient.startTranscriptionJob throws a LimitExceededException
        // because of previosly defined throw exception behavior
        doReturn(jobStartResult).when(mockTrClient).startTranscriptionJob(startReq);
        // when(mockTrClient.startTranscriptionJob(startReq)).thenReturn(jobStartResult);
        when(jobStartResult.getTranscriptionJob()).thenReturn(job);
        GetTranscriptionJobResult jobResult = mock(GetTranscriptionJobResult.class);
        GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest().withTranscriptionJobName(jobId);
        when(mockTrClient.getTranscriptionJob(eq(jobRequest))).thenReturn(jobResult);
        when(jobResult.getTranscriptionJob()).thenReturn(job);
        List<String> returnedStatus = new ArrayList<>();
        if (numTimesCheck > 1) {
            returnedStatus.addAll(Collections.nCopies(numTimesCheck - 1, "IN_PROGRESS")); 
        }
        returnedStatus.add("COMPLETED");
        when(job.getTranscriptionJobStatus()).thenReturn("IN_PROGRESS", returnedStatus.toArray(new String[0]));
        URL url = mock(URL.class);
        whenNew(URL.class).withParameterTypes(String.class)
            .withArguments(transcriptUri).thenReturn(url);
        when(url.openStream()).thenReturn(IOUtils.toInputStream(trtxt, "UTF-8"));   
        when(job.getTranscript()).thenReturn(new Transcript().withTranscriptFileUri(transcriptUri));
    }

    private void mockTranscriptionJobCheckStatusExceptionally(AmazonTranscribe mockTrClient, String jobId, String trtxt, int numTimesCheck, String mediaUri, String transcriptUri)
            throws Exception, IOException {
        StartTranscriptionJobResult jobStartResult = mock(StartTranscriptionJobResult.class);
        TranscriptionJob job = mock(TranscriptionJob.class);
        StartTranscriptionJobRequest startReq = new StartTranscriptionJobRequest()
                    .withMedia(new Media().withMediaFileUri(mediaUri))
                    .withMediaFormat("wav").withLanguageCode(LanguageCode.EnUS).withTranscriptionJobName(jobId)
                    .withSettings(new Settings().withChannelIdentification(true));
        when(mockTrClient.startTranscriptionJob(startReq)).thenReturn(jobStartResult);
        when(jobStartResult.getTranscriptionJob()).thenReturn(job);
        when(job.getTranscriptionJobStatus()).thenReturn("IN_PROGRESS");
        GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest().withTranscriptionJobName(jobId);
        when(mockTrClient.getTranscriptionJob(eq(jobRequest))).thenThrow(new InternalFailureException("Die, die, die my darling; Die, die, die, die, die, die"));
    }

    private void mockTranscriptionJobCheckStatusReturnsFailed(AmazonTranscribe mockTrClient, String jobId, String trtxt, int numTimesCheck, String mediaUri, String transcriptUri)
            throws Exception, IOException {
                StartTranscriptionJobResult jobStartResult = mock(StartTranscriptionJobResult.class);
                TranscriptionJob job = mock(TranscriptionJob.class);
                StartTranscriptionJobRequest startReq = new StartTranscriptionJobRequest()
                            .withMedia(new Media().withMediaFileUri(mediaUri))
                            .withMediaFormat("wav").withLanguageCode(LanguageCode.EnUS).withTranscriptionJobName(jobId)
                            .withSettings(new Settings().withChannelIdentification(true));
                // when(mockTrClient.startTranscriptionJob(isA(StartTranscriptionJobRequest.class))).thenReturn(jobStartResult);
                // have to use doReturn because mockTrClient.startTranscriptionJob throws a LimitExceededException
                // because of previosly defined throw exception behavior
                doReturn(jobStartResult).when(mockTrClient).startTranscriptionJob(startReq);
                // when(mockTrClient.startTranscriptionJob(startReq)).thenReturn(jobStartResult);
                when(jobStartResult.getTranscriptionJob()).thenReturn(job);
                GetTranscriptionJobResult jobResult = mock(GetTranscriptionJobResult.class);
                GetTranscriptionJobRequest jobRequest = new GetTranscriptionJobRequest().withTranscriptionJobName(jobId);
                when(mockTrClient.getTranscriptionJob(eq(jobRequest))).thenReturn(jobResult);
                when(jobResult.getTranscriptionJob()).thenReturn(job);
                when(job.getTranscriptionJobStatus()).thenReturn("IN_PROGRESS", "FAILED");
                
    }


    private void mockTranscriptionJobSubmitReqExceptionally(AmazonTranscribe mockTrClient, String jobId, String trtxt, int numTimesCheck, String mediaUri, String transcriptUri)
            throws Exception, IOException {
        StartTranscriptionJobRequest startReq = new StartTranscriptionJobRequest()
                    .withMedia(new Media().withMediaFileUri(mediaUri))
                    .withMediaFormat("wav").withLanguageCode(LanguageCode.EnUS).withTranscriptionJobName(jobId)
                    .withSettings(new Settings().withChannelIdentification(true));
        when(mockTrClient.startTranscriptionJob(startReq)).thenThrow(new LimitExceededException("test exception"));
    }

    private AmazonTranscribe getMockTranscriptionClient() {
        if (mockTrClient != null) {
            return mockTrClient;
        } else {
            mockTrClient = mockTranscriptionClient();
            return mockTrClient;
        }
    }
        

    private AmazonTranscribe mockTranscriptionClient() {
        AmazonS3ClientBuilder mockBuilder = mock(AmazonS3ClientBuilder.class);
        AmazonS3 mockS3Client = mock(AmazonS3.class);
        mockStatic(AmazonS3ClientBuilder.class);

        when(AmazonS3ClientBuilder.standard()).thenReturn(mockBuilder);
        when(mockBuilder.withCredentials((AWSCredentialsProvider)any())).thenReturn(mockBuilder);
        when(mockBuilder.withRegion((String)any())).thenReturn(mockBuilder);
        when(mockBuilder.build()).thenReturn(mockS3Client);

        mockStatic(AmazonTranscribeClient.class);
        AmazonTranscribeClientBuilder mockTrBuilder = mock(AmazonTranscribeClientBuilder.class);
        AmazonTranscribe mockTrClient = mock(AmazonTranscribe.class);
        when(AmazonTranscribeClient.builder()).thenReturn(mockTrBuilder);
        when(mockTrBuilder.withCredentials((AWSCredentialsProvider)any())).thenReturn(mockTrBuilder);
        when(mockTrBuilder.withRegion("test-region")).thenReturn(mockTrBuilder);
        when(mockTrBuilder.build()).thenReturn(mockTrClient);
        return mockTrClient;
    }

    static void postProcessTranscript(JobResult jobResult, TranscriptPersistenceResult persistenceResult, List<AudioMediaTranscript> resultHolder) {
        resultHolder.add(new AudioMediaTranscript(jobResult.getJobTracker().getMediaLink(),
            (InMemoryPersistenceResult) persistenceResult));
    }

    static private class AudioMediaTranscript {
        final private S3AudioMediaLink mediaLink;
        final private InMemoryPersistenceResult result;

        public AudioMediaTranscript(S3AudioMediaLink mediaLink, InMemoryPersistenceResult result) {
            this.mediaLink = mediaLink;
            this.result = result;
        }

        public String getObjectId() {
            return mediaLink.getMediaObjectName();
        }

        public String getTranscript() {
            return result.getTranscript();
        }

        @Override
        public String toString() {
            return "AudioMediaTranscript [mediaLink=" + mediaLink + ", result=" + result + "]";
        }
    }
}