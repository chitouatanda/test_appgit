package com.xtaas.ml.transcribe.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

import com.github.javafaker.Faker;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.CallLog;
import com.xtaas.db.repository.CallLogRepository;
import com.xtaas.ml.transcribe.dto.JobQueueS3AudioMediaLink;
import com.xtaas.ml.transcribe.dto.S3AudioMediaLink;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
// @ContextConfiguration(classes = {SpringComponentScanAppTest.class}, loader = AnnotationConfigContextLoader.class)
@ContextConfiguration(classes = { MongoConfig.class, AwsTranscriptionService.class, AppTekTranscriptionService.class,
		TranscriberFactory.class, DBPersistenceService.class, TranscriberThreadPoolConfig.class,AwsS3PersistenceService.class,
		ShortenUrlService.class }, loader = AnnotationConfigContextLoader.class)
public class TranscriberIntegrationTest extends TranscriberTestBase {
	final private TranscriberFactory factory;
	final private ShortenUrlService shortenUrlService;
	final private DBPersistenceService dbPersistenceService;
	final private AwsS3PersistenceService persistenceService;
	final private CallLogRepository callLogRepository;

	public TranscriberIntegrationTest(@Autowired TranscriberFactory factory,
			@Autowired ShortenUrlService shortenUrlService, @Autowired DBPersistenceService dbPersistenceService,
			@Autowired AwsS3PersistenceService awsS3PersistenceService, 
			@Autowired CallLogRepository callLogRepository) {
		this.factory = factory;
		this.shortenUrlService = shortenUrlService;
		this.dbPersistenceService = dbPersistenceService;
		this.persistenceService = awsS3PersistenceService;
		this.callLogRepository = callLogRepository;
	}

	@Test
    @Disabled("real test")
    public void transcribe_audio_file_using_AWSTranscription() throws IOException, InterruptedException {
        String transcriptionServiceProvider = "AWS";
        invokeAndValidate(transcriptionServiceProvider, getTestAudioMediaLinks());
    }
    @Test
    @Disabled("real test")
    public void transcribe_audio_file_using_AppTekTranscription() throws IOException, InterruptedException {
        String transcriptionServiceProvider = "APPTEK";
        invokeAndValidate(transcriptionServiceProvider, getTestAudioMediaLinks());
    }
    @Test
    @Disabled("real test")
    void transcribe_audio_file_exceptionally() throws InterruptedException {
        List<S3AudioMediaLink> mediaLinks = getTestAudioMediaLinks();
        Transcriber service = factory.getTranscriber((j, r) -> {throw new RuntimeException("Exception Test");});
        CompletableFuture<List<String>> statusFutures = service.transcribe(() -> mediaLinks);
        List<String> status = statusFutures.join();
        assertEquals("FAILURE", status.get(0));
    }

	@Test
	public void check_transcription_getting_saved_in_DB() throws IOException, InterruptedException {
		// String transcriptionServiceProvider = "AWS";
		// List<S3AudioMediaLink> links = Arrays.asList(JobQueueS3AudioMediaLink.builder().withMediaUri(
		// 		"https://s3-us-west-2.amazonaws.com/xtaasrecordings/recordings/3e1978f0-c858-11ea-9de7-0270b7165627.wav")
		// 		.withPhone("+919096537738").withProspectCallLogId("12345")
		// 		.withProspectCallId("caba78d7-4137-41a8-8991-e6044ba4ad00").withCallDuration(123)
		// 		.withMessageId("Random msg").build());
		// invokeTranscribeAndPersistInDB(transcriptionServiceProvider, links);
		// CallLog log = callLogRepository.findCallLogByPcid("caba78d7-4137-41a8-8991-e6044ba4ad00");
		// assertEquals(
		// 		"Hey. Uh, sorry I missed your call. Can you please call me back? This is site manager SSG Support. Thank you.",
		// 		log.getCallLogMap().get("transcript"));
	}

	private CompletableFuture<List<String>> invokeTranscribeAndPersistInDB(String transcriptionServiceProvider,
			List<S3AudioMediaLink> mediaLinks) throws IOException, InterruptedException {
		final Transcriber service = factory.getTrancriber(
				() -> TranscriberConfig.builder().withTranscriptionServiceProvider(transcriptionServiceProvider)
						.withPersistenceService((j) -> dbPersistenceService.saveTranscript(j)));
		CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
		statusFuture.join();
		return statusFuture;
	}

    private void invokeAndValidate(String transcriptionServiceProvider, List<S3AudioMediaLink> mediaLinks)
            throws IOException, InterruptedException, MalformedURLException, ProtocolException {
        String[] outfileNameHolder = new String[1];
        CompletableFuture<List<String>> statusFuture = invokeTranscribe(transcriptionServiceProvider,
        mediaLinks, outfileNameHolder);
        // wait for the asynchronous operations to complete
        statusFuture.join();
        validate(transcriptionServiceProvider, outfileNameHolder);
    }
    private CompletableFuture<List<String>> invokeTranscribe(String transcriptionServiceProvider,
            List<S3AudioMediaLink> mediaLinks,
            String[] outfileNameHolder) throws IOException, InterruptedException {
        
        CountDownLatch latch = new CountDownLatch(mediaLinks.size());
        final Transcriber service = factory.getTrancriber(() -> 
            TranscriberConfig.builder()
                // .withMediaLinkValidator((l) -> testValidLabels(l))
                .withTranscriptionServiceProvider(transcriptionServiceProvider)
                .withPersistenceService((j) -> persistenceService.saveTranscript(j))
                .withTranscriptPostProcessConsumer((j, r) -> TranscriberTestBase.postProcessTranscript(j, r, 
                        (o) -> shortenUrlService.shortenUrl(o), TranscriberTestBase.getOutFile(j, outfileNameHolder)))
                        // (o) -> o.toString(), getOutFile(j)))
                .withTranscriptPostProcessConsumer((j, r) -> latch.countDown())
            );
        CompletableFuture<List<String>> statusFuture = service.transcribe(() -> mediaLinks);
        return statusFuture;
    }
    private void validate(String transcriptionServiceProvider, String[] outfileNameHolder)
            throws InterruptedException, IOException, MalformedURLException, ProtocolException {
        assertNotNull(outfileNameHolder[0]);
        assertTrue(() -> new File(outfileNameHolder[0]).exists());
        String out = Files.readAllLines(Paths.get(outfileNameHolder[0])).get(0);
        String trUrlShort = out.split(",")[1];
        HttpURLConnection.setFollowRedirects(false);
        HttpURLConnection conn = ((HttpURLConnection) new URL(trUrlShort).openConnection());
        conn.setRequestMethod("GET");
        conn.connect();
        int code = conn.getResponseCode();
        assertEquals(301, code);
        String trUrl = conn.getHeaderField("location");
        assertTrue(() -> trUrl.contains(transcriptionServiceProvider));
        conn.disconnect();
        
        conn = ((HttpURLConnection) new URL(trUrl).openConnection());
        conn.setRequestMethod("GET");
        conn.connect();
        code = conn.getResponseCode();
        assertEquals(200, code);
        String size = conn.getHeaderField("Content-Length");
        assertTrue(Integer.parseInt(size) > 0);
        HttpURLConnection.setFollowRedirects(true);
    }
    // private static boolean testValidLabels(S3AudioMediaLink mediaLink) {
    //     List<String> validLabels = new ArrayList<>();
    //     for (String label : mediaLink.getLabels()) {
    //         switch (label.toLowerCase()) {
    //             case "direct": validLabels.add("direct"); break;
    //             case "indirect": validLabels.add("indirect"); break;
    //             case "human": validLabels.add("human"); break;
    //             case "machine": validLabels.add("machine"); break;
            
    //             default:
    //                 break;
    //     }}
    //     // System.out.println(v.getMediaUri() + "," + v.getLabels());
    //     return !validLabels.isEmpty();
    // }
}