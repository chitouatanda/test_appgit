package com.xtaas.ml.classifier.service;

import static com.xtaas.ml.classifier.service.GoogleClassifierModelTestBase.ModelStatus.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Optional;

import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.entity.ClassifierSampleLog;
import com.xtaas.db.entity.ClassifierStatisticsLog;
import com.xtaas.db.repository.ClassifierSampleLogRepository;
import com.xtaas.db.repository.ClassifierStatisticsLogRepository;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *<p>
 *
 * @author sushant
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {GoogleClassifierStatistics.class,MongoConfig.class,
    ClassifierSampleLogRepository.class, ClassifierStatisticsLogRepository.class
}, loader = AnnotationConfigContextLoader.class)
public class GoogleClassifierModelIntegrationTest extends GoogleClassifierModelTestBase {
    final static private Logger log = LoggerFactory.getLogger(GoogleClassifierModelTestBase.class);
    final private GoogleClassifierStatistics classifier;
    final private ClassifierSampleLogRepository sampleRepo;
    final private ClassifierStatisticsLogRepository statsRepo;
    final private MongoOperations mongoOps;
    final private List<Class<? extends Object>> entities;
    
    /**
     * Default constructor
     */
    public GoogleClassifierModelIntegrationTest(
        @Autowired GoogleClassifierStatistics classifier,
        @Autowired ClassifierSampleLogRepository sampleRepo,
        @Autowired ClassifierStatisticsLogRepository statsRepo,
        @Autowired MongoOperations mongoOps
        ) {
        this.classifier = classifier;
        this.sampleRepo = sampleRepo;
        this.statsRepo = statsRepo;
        this.mongoOps = mongoOps;
            this.entities = Arrays.asList(ClassifierSampleLog.class, ClassifierStatisticsLog.class);
    }

    @BeforeEach
    void setup() {
        entities.stream().filter(e -> !mongoOps.collectionExists(e)).forEach(e -> mongoOps.createCollection(e));
    }

    @Test
    @Disabled("real test")
    void testModel() {
        final List<GoogleClassifierModelTestBase.ClassifyModelTestIO> incorrectPredictions = new ArrayList<>();
        final List<GoogleClassifierModelTestBase.ClassifyModelTestIO> notQualified = new ArrayList<>();
        final EnumMap<GoogleClassifierModelTestBase.ModelStatus, Integer> counters = new EnumMap<>(GoogleClassifierModelTestBase.ModelStatus.class);
        // initialize counters to 0
        Arrays.stream(values()).forEach(m -> counters.put(m, 0));

        final String infileName = "classifier-test-input-10.csv";
        // process it as a stream
        GoogleClassifierModelTestBase.getClassfifyModelTestIOFromCSVFile(infileName).stream()
            .forEach(i -> {
                Optional<PhoneNumberType> classify = classifier.classify(i);
                classify.ifPresent( prediction -> {
                    i.predictedClass(classify.get());
                    GoogleClassifierModelTestBase.ModelStatus modelStatus = status(i.getPredictedClass(), i.getTaggedClass());
                    counters.put(modelStatus, counters.get(modelStatus) + 1);
                    if (modelStatus == FalsePositive || modelStatus == FalseNegative) {
                        incorrectPredictions.add(i);
                    }
                });

                if (!classify.isPresent()) {
                    GoogleClassifierModelTestBase.ModelStatus modelStatus = FalseNegative;
                    counters.put(modelStatus, counters.get(modelStatus) + 1);
                    notQualified.add(i);
                }
            });
        
        List<ClassifierStatisticsLog> statsSnapshot = classifier.statsSapshot();
        List<ClassifierStatisticsLog> statsRepoAll = statsRepo.findAll();
        
        log.debug("*****************************************************************");
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(statsRepoAll).hasSize(statsSnapshot.size());
        log.debug(statsRepoAll.toString());
        List<ClassifierSampleLog> sampleSnapshot = classifier.sampleSnapshot();
        List<ClassifierSampleLog> sampleRepoAll = sampleRepo.findAll();
        softly.assertThat(sampleRepoAll).hasSize(sampleSnapshot.size());
        log.debug(sampleSnapshot.toString()); 

        log.debug("\n");
        log.debug("Not Qualified Predictions");
        log.debug("id, actual");
        notQualified.stream().forEach(nq -> {
            log.debug(String.format( "%s, %s", nq.getId(), nq.getTaggedClass()));
        });

        log.debug("\n\n");
        log.debug("Incorrect Predictions");
        log.debug("id, text, mimeType, actual, prediction");
        incorrectPredictions.stream().forEach(incorrect -> 
                    log.debug(String.format( "%s, %s, %s, %s, %s", 
                        incorrect.getId(), 
                        incorrect.getTextContent(), 
                        incorrect.getMimeType(),
                        incorrect.getTaggedClass(),
                        incorrect.getPredictedClass())));
        log.debug("\n\n");

        log.debug(String.format("Precision %f", precision(counters)));
        log.debug(String.format("Recall    %f", recall(counters)));
        log.debug(String.format("Accuracy %f", accuracy(counters)));
        softly.assertAll();
    }
    
    @AfterEach
    void teardown() {
        entities.stream().forEach(e -> mongoOps.dropCollection(e));
    }
}
