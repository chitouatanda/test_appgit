// package com.xtaas.ml.classifier.service;
// import static org.assertj.core.api.Assertions.assertThat;
// import static org.junit.Assert.assertEquals;
// import static org.mockito.ArgumentMatchers.any;
// import static org.mockito.Mockito.times;
// import static org.mockito.Mockito.verify;
// import static org.powermock.api.mockito.PowerMockito.mock;
// import static org.powermock.api.mockito.PowerMockito.mockStatic;
// import static org.powermock.api.mockito.PowerMockito.when;
// import java.io.IOException;
// import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collections;
// import java.util.List;
// import java.util.Random;
// import java.util.UUID;
// import java.util.concurrent.ConcurrentHashMap;
// import java.util.concurrent.CopyOnWriteArrayList;
// import java.util.stream.Collectors;

// import com.google.cloud.automl.v1.AnnotationPayload;
// import com.google.cloud.automl.v1.ClassificationAnnotation;
// import com.google.cloud.automl.v1.PredictRequest;
// import com.google.cloud.automl.v1.PredictResponse;
// import com.google.cloud.automl.v1.PredictionServiceClient;
// import com.google.common.math.StatsAccumulator;
// import com.xtaas.BeanLocator;
// import com.xtaas.db.config.MongoConfig;
// import com.xtaas.db.entity.ClassifierSampleLog;
// import com.xtaas.db.entity.ClassifierStatisticsLog;
// import com.xtaas.db.repository.ClassifierSampleLogRepository;
// import com.xtaas.db.repository.ClassifierStatisticsLogRepository;
// import com.xtaas.ml.classifier.dto.ClassifyContentInput;

// import org.junit.Before;
// import org.junit.Ignore;
// import org.junit.Test;
// import org.junit.jupiter.api.Disabled;
// import org.junit.runner.RunWith;
// import org.mockito.Mockito;
// import org.powermock.core.classloader.annotations.PrepareForTest;
// import org.powermock.modules.junit4.PowerMockRunner;
// import org.powermock.modules.junit4.PowerMockRunnerDelegate;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.ApplicationContext;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.support.AnnotationConfigContextLoader;
// import org.springframework.test.util.ReflectionTestUtils;
// @RunWith(PowerMockRunner.class)
// @ContextConfiguration(classes = { GoogleClassifierStatistics.class, MongoConfig.class,
// 		ClassifierSampleLogRepository.class, ClassifierStatisticsLogRepository.class
// }, loader = AnnotationConfigContextLoader.class)
// @PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
// @PrepareForTest({ PredictionServiceClient.class, PredictResponse.class, AnnotationPayload.class,
// 		ClassificationAnnotation.class, StatsAccumulator.class })
// @Disabled
// public class GoogleClassifierStatisticsUnitTest extends GoogleClassifierModelTestBase {

// 	@Autowired
// 	GoogleClassifierStatistics classifier;

// 	@Autowired
// 	ClassifierSampleLogRepository sampleRepo;

// 	@Autowired
// 	ClassifierStatisticsLogRepository statsRepo;

// 	@Autowired
// 	ApplicationContext context;
// 	public GoogleClassifierStatisticsUnitTest() {
// 	}
// 	@Before
// 	public void setup() {
// 		ReflectionTestUtils.setField(classifier, "scores", new ConcurrentHashMap<>());
// 		ReflectionTestUtils.setField(classifier, "failedSamples", new ConcurrentHashMap<>());
// 		ReflectionTestUtils.setField(classifier, "randomSamples", new CopyOnWriteArrayList<>());
// 		ReflectionTestUtils.setField(classifier, "sampleRepo",sampleRepo);
// 		ReflectionTestUtils.setField(classifier, "statsRepo",statsRepo);
// 		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
// 	}

// 	@Test // It should call saveAll method of ClassifierStatisticsLogRepository.
// 	public void should_call_saveall_method_of_statsrepo() {
// 		List<ClassifierStatisticsLog> statLogs = new ArrayList<>();
// 		ClassifierStatisticsLogRepository localMockRepository = Mockito.mock(ClassifierStatisticsLogRepository.class);
// 		ReflectionTestUtils.setField(classifier, "statsRepo", localMockRepository);
// 		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
// 		Mockito.when(localMockRepository.saveAll(statLogs)).thenReturn(statLogs);
// 		classifier.statsSapshot();
// 		verify(localMockRepository, times(1)).saveAll(statLogs);
// 	}
// 	@Test // It should call saveAll method of ClassifierSampleLogRepository.
// 	public void should_call_saveall_method_of_samplelog_repo() {
// 		List<ClassifierSampleLog> classifierLogs = new ArrayList<>();
// 		ClassifierSampleLogRepository localMockRepository = Mockito.mock(ClassifierSampleLogRepository.class);
// 		ReflectionTestUtils.setField(classifier, "sampleRepo", localMockRepository);
// 		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
// 		Mockito.when(localMockRepository.saveAll(classifierLogs)).thenReturn(classifierLogs);
// 		classifier.sampleSnapshot();
// 		verify(localMockRepository, times(1)).saveAll(classifierLogs);
// 	}
// 	@Test // When scores are empty then it should return empty list.
// 	public void should_return_empty_list() {
// 		ReflectionTestUtils.setField(classifier, "scores", new ConcurrentHashMap<>());
// 		List<ClassifierStatisticsLog> scoresLogs = classifier.statsSapshot();
// 		assertThat(scoresLogs).isNotNull().hasSize(0);
// 	}
// 	@Test // When failedsamples are empty then it should return empty list.
// 	public void should_return_list() {
// 		ReflectionTestUtils.setField(classifier, "failedSamples", new ConcurrentHashMap<>());
// 		List<ClassifierSampleLog> failedSamples = classifier.sampleSnapshot();
// 		assertThat(failedSamples).isNotNull().hasSize(0);
// 	}

// 	@Test // Scores map should get filled.
// 	public void check_whether_scores_map_get_filled() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.65f, 0.35f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.64f, 0.36f, "Direct", "Indirect"));
// 		List<ClassifierStatisticsLog> logs = classifier.statsSapshot();
// 		assertThat(logs).isNotNull().hasSize(2);
// 	}
	
// 	@Test // Failed samples map should get filled.
// 	public void check_whether_failedsamples_map_get_filled() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.45f, 0.55f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.44f, 0.56f, "Direct", "Indirect"));
// 		List<ClassifierSampleLog> logs = classifier.sampleSnapshot();
// 		assertThat(logs).isNotNull().hasSize(2);
// 	}
// 	@Test // When sample size is 5 and if we are passing the value less than in map then
// 			// it should replace with max value in map.
// 	@Ignore
// 	public void max_value_shouldbe_replaced_failedsamples() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.45f, 0.55f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.44f, 0.56f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.43f, 0.57f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.42f, 0.58f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.41f, 0.59f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.50f, 0.50f, "Direct", "Indirect"));
// 		List<ClassifierSampleLog> logs = classifier.sampleSnapshot();
// 		List<Float> actualValues = logs.stream().map(t -> t.getScore()).collect(Collectors.toList());
// 		Collections.sort(actualValues);
// 		List<Float> expectedValues = Arrays.asList(0.55f, 0.56f, 0.57f, 0.58f, 0.59f);
// 		assertEquals(expectedValues, actualValues);
// 	}
// 	@Test // When sample size is 5 and if we are passing the value greater than in map
// 			// then it should not affect existing map.
// 	public void shouldnt_modify_failedsamples_map() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.45f, 0.54f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.44f, 0.55f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.43f, 0.56f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.42f, 0.57f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.41f, 0.58f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.50f, 0.59f, "Direct", "Indirect"));
// 		List<ClassifierSampleLog> logs = classifier.sampleSnapshot();
// 		List<Float> actualValues = logs.stream().map(t -> t.getScore()).collect(Collectors.toList());
// 		Collections.sort(actualValues);
// 		List<Float> expectedValues = Arrays.asList(0.54f, 0.55f, 0.56f, 0.57f, 0.58f);
// 		assertEquals(expectedValues, actualValues);
// 	}

// 	@Test // Scores map should get empty.
// 	public void check_whether_scores_map_get_empty() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.65f, 0.35f, "Direct", "Indirect"));
// 		classifier.statsSapshot();
// 		List<ClassifierStatisticsLog> emptyStats = classifier.statsSapshot();
// 		assertThat(emptyStats).isNotNull().hasSize(0);
// 	}

// 	@Test // When failedsamples are empty then it should return empty list.
// 	public void should_empty_list() {
// 		ReflectionTestUtils.setField(classifier, "failedSamples", new ConcurrentHashMap<>());
// 		List<ClassifierSampleLog> failedSamples = classifier.sampleSnapshot();
// 		assertThat(failedSamples).isNotNull().hasSize(0);
// 	}

// 	@Test // Failed samples map should get empty.
// 	public void check_whether_failed_map_get_empty() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.45f, 0.55f, "Direct", "Indirect"));
// 		classifier.sampleSnapshot();
// 		List<ClassifierSampleLog> emptyLogs = classifier.sampleSnapshot();
// 		assertThat(emptyLogs).isNotNull().hasSize(0);
// 	}

// 	@Test // Random samples map should get filled.
// 	public void check_whether_random_map_get_filled() throws IOException {
// 		Random randomMock = Mockito.mock(Random.class);
// 		ReflectionTestUtils.setField(classifier, "random", randomMock);
// 		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
// 		Mockito.when(randomMock.nextInt(10)).thenReturn(4);
// 		classifier.classify(mockAndReturnContent(0.65f, 0.35f, "Direct", "Indirect"));
// 		List<ClassifierSampleLog> emptyLogs = classifier.randomSnapshot();
// 		assertThat(emptyLogs).isNotNull().hasSize(2);  // One for direct entry and other for indirect entry.
// 	}

// 	@Test // Random samples map should get empty.
// 	public void check_whether_random_samples_get_empty() throws IOException {
// 		classifier.classify(mockAndReturnContent(0.45f, 0.55f, "Direct", "Indirect"));
// 		classifier.randomSnapshot();
// 		List<ClassifierSampleLog> emptyLogs = classifier.randomSnapshot();
// 		assertThat(emptyLogs).isNotNull().hasSize(0);
// 	}

// 	@Test // Random samples map should get filled.
// 	@Ignore
// 	public void check_whether_random_ma1p_get_filled() throws IOException {
// 		List<ClassifierSampleLog> sampleLogs = new ArrayList<>();
// 		Random randomMock = Mockito.mock(Random.class);
// 		ReflectionTestUtils.setField(classifier, "random", randomMock);
// 		Mockito.when(randomMock.nextInt(10)).thenReturn(1);
// 		GoogleClassifierStatistics statsMock = Mockito.mock(GoogleClassifierStatistics.class);
// 		ReflectionTestUtils.setField(classifier, "bufferSize", 2);
// 		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
// 		Mockito.when(statsMock.randomSnapshot()).thenReturn(sampleLogs);
// 		classifier.classify(mockAndReturnContent(0.65f, 0.35f, "Direct", "Indirect"));
// 		classifier.classify(mockAndReturnContent(0.65f, 0.35f, "Direct", "Indirect"));
// 		Mockito.verify(statsMock, Mockito.times(1)).randomSnapshot();
// 	}

// 		// Below method is to fill the scores and failedsamples map.
// 	private ClassifyContentInput mockAndReturnContent(float directValue, float indirectValue, String directDisplayName,
// 			String indirectDisplayName) throws IOException {
// 		PredictionServiceClient mockPClient = mock(PredictionServiceClient.class);
// 		PredictResponse mockResp = mock(PredictResponse.class);
// 		mockStatic(PredictionServiceClient.class);
// 		when(PredictionServiceClient.create()).thenReturn(mockPClient);
// 		when(mockPClient.predict(any(PredictRequest.class))).thenReturn(mockResp);
// 		AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
// 		ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
// 		when(directClassPayload.getDisplayName()).thenReturn(directDisplayName);
// 		when(directClassPayload.getClassification()).thenReturn(directClass);
// 		when(directClass.getScore()).thenReturn(directValue);
// 		AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
// 		ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
// 		when(indirectClassPayload.getDisplayName()).thenReturn(indirectDisplayName);
// 		when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
// 		when(indirectClass.getScore()).thenReturn(indirectValue);
// 		List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);
// 		when(mockResp.getPayloadList()).thenReturn(mockPayloadList);
// 		ClassifyContentInput contentInput = new ClassifyContentInput(UUID.randomUUID(), "random text", "text/plain");
// 		return contentInput;
// 	}
// }