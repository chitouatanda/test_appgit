package com.xtaas.ml.classifier.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.google.cloud.automl.v1.AnnotationPayload;
import com.google.cloud.automl.v1.ClassificationAnnotation;
import com.google.cloud.automl.v1.PredictRequest;
import com.google.cloud.automl.v1.PredictResponse;
import com.google.cloud.automl.v1.PredictionServiceClient;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.xtaas.db.config.MongoConfig;
import com.xtaas.db.repository.ClassifierSampleLogRepository;
import com.xtaas.db.repository.ClassifierStatisticsLogRepository;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = {GoogleClassifierStatistics.class, 
    MongoConfig.class,
    ClassifierSampleLogRepository.class, ClassifierStatisticsLogRepository.class

}, loader = AnnotationConfigContextLoader.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
// @PowerMockIgnore({"javax.management.*"})
@PrepareForTest({PredictionServiceClient.class, PredictResponse.class, AnnotationPayload.class,
        ClassificationAnnotation.class})
public class GoogleClassifierUnitTest extends GoogleClassifierModelTestBase {
    private static Logger log = LoggerFactory.getLogger(GoogleClassifierUnitTest.class);
    @Autowired GoogleClassifierStatistics classifier;

    /**
     * Default constructor
     */
    public GoogleClassifierUnitTest() {
    }

    @Test
    public void test_direct_phone_number_type() throws IOException {
        PredictionServiceClient mockClient = mock(PredictionServiceClient.class);
        PredictResponse mockResp = mock(PredictResponse.class);
        mockStatic(PredictionServiceClient.class);

        when(PredictionServiceClient.create()).thenReturn(mockClient);
        when(mockClient.predict(any(PredictRequest.class))).thenReturn(mockResp);

        AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
        when(directClassPayload.getDisplayName()).thenReturn("Direct");
        when(directClassPayload.getClassification()).thenReturn(directClass);
        when(directClass.getScore()).thenReturn(0.93f);

        AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
        when(indirectClassPayload.getDisplayName()).thenReturn("Indirect");
        when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
        when(indirectClass.getScore()).thenReturn(0.07f);

        List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);

        when(mockResp.getPayloadList()).thenReturn(mockPayloadList);

        ClassifyContentInput contentInput = new ClassifyContentInput(UUID.randomUUID(), "random text", "text/plain");

        Optional<PhoneNumberType> phoneNumberType = classifier.classify(contentInput);
        assertTrue(phoneNumberType.isPresent());

        assertEquals(PhoneNumberType.Direct, phoneNumberType.get());
    }

    @Test
    public void test_indirect_phone_number_type() throws IOException {
        PredictionServiceClient mockClient = mock(PredictionServiceClient.class);
        PredictResponse mockResp = mock(PredictResponse.class);
        mockStatic(PredictionServiceClient.class);

        when(PredictionServiceClient.create()).thenReturn(mockClient);
        when(mockClient.predict(any(PredictRequest.class))).thenReturn(mockResp);

        AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
        when(directClassPayload.getDisplayName()).thenReturn("Direct");
        when(directClassPayload.getClassification()).thenReturn(directClass);
        when(directClass.getScore()).thenReturn(0.09f);

        AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
        when(indirectClassPayload.getDisplayName()).thenReturn("Indirect");
        when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
        when(indirectClass.getScore()).thenReturn(0.91f);

        List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);

        when(mockResp.getPayloadList()).thenReturn(mockPayloadList);

        // ClassifyContentInput contentInput = new ClassifyContentInput(UUID.fromString("2ca90877-2454-4f79-8363-9eb7bb848740"), "been forwarded to an automated voice messaging system. 2516228000 is not available Yeah. Yeah. at the tone. Please record your message. Okay. Thank you.", "text/plain");
        ClassifyContentInput contentInput = new ClassifyContentInput(UUID.randomUUID(), "random text", "text/plain");

        Optional<PhoneNumberType> phoneNumberType = classifier.classify(contentInput);
        assertTrue(phoneNumberType.isPresent());
        assertEquals(PhoneNumberType.Indirect, phoneNumberType.get());
    }

    // TODO set threshold env param here
    @Test
    public void test_non_deterministic_phone_number_type() throws IOException {
        PredictionServiceClient mockClient = mock(PredictionServiceClient.class);
        PredictResponse mockResp = mock(PredictResponse.class);
        mockStatic(PredictionServiceClient.class);

        when(PredictionServiceClient.create()).thenReturn(mockClient);
        when(mockClient.predict(any(PredictRequest.class))).thenReturn(mockResp);

        AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
        when(directClassPayload.getDisplayName()).thenReturn("Direct");
        when(directClassPayload.getClassification()).thenReturn(directClass);
        when(directClass.getScore()).thenReturn(0.45f);

        AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
        ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
        when(indirectClassPayload.getDisplayName()).thenReturn("Indirect");
        when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
        when(indirectClass.getScore()).thenReturn(0.55f);

        List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);

        when(mockResp.getPayloadList()).thenReturn(mockPayloadList);

        ClassifyContentInput contentInput = new ClassifyContentInput(UUID.randomUUID(), "random text", "text/plain");

        Optional<PhoneNumberType> phoneNumberType = classifier.classify(contentInput);
        assertFalse(phoneNumberType.isPresent());
    }

    // the next two methods is used to determine correct threadhold value for optimum precision and recall
    @Test
    @Ignore
    public void test_with_snapshot_google_classifier_result() throws IOException {
        try (
            BufferedWriter writer = new BufferedWriter(new FileWriter("data/classifier-run-test-1000-stats.csv"));
        ) {
            writer.write("Threshold,Precision,Recall,Accuracy");
            writer.newLine();
            for(int threshold = 50; threshold < 100; threshold = threshold + 10) {
                classifier.setTestThreshold(Float.valueOf(threshold));
                final List<ClassifyModelTestIO> incorrectPredictions = new ArrayList<>();
                final List<ClassifyModelTestIO> notQualified = new ArrayList<>();
                final EnumMap<ModelStatus, Integer> counters = new EnumMap<>(ModelStatus.class);
                // initialize counters to 0
                Arrays.stream(ModelStatus.values()).forEach(m -> counters.put(m, 0));
                String googleScoresFile = GoogleClassifierUnitTest.class.getResource("classifier-run-test-1000-scores.csv").getFile();
                Map<String, EnumMap<PhoneNumberType, Float>> googleScoresSnapshot =
                        googleScoresSnapshot(googleScoresFile);

                final String infileName = GoogleClassifierUnitTest.class.getResource("classifier-test-input-1000.csv").getFile()   ;
                getClassfifyModelTestIOFromCSVFile(infileName).stream()
                    .forEach(i -> {
                        PredictionServiceClient mockClient = mock(PredictionServiceClient.class);
                        PredictResponse mockResp = mock(PredictResponse.class);
                        mockStatic(PredictionServiceClient.class);

                        try {
                            when(PredictionServiceClient.create()).thenReturn(mockClient);
                        } catch (IOException e) { throw new RuntimeException(e); }
                        when(mockClient.predict(any(PredictRequest.class))).thenReturn(mockResp);

                        AnnotationPayload directClassPayload = mock(AnnotationPayload.class);
                        ClassificationAnnotation directClass = mock(ClassificationAnnotation.class);
                        when(directClassPayload.getDisplayName()).thenReturn("Direct");
                        when(directClassPayload.getClassification()).thenReturn(directClass);
                        EnumMap<PhoneNumberType, Float> enumMap = googleScoresSnapshot.get(hash(i.getTextContent()));
                        // if(enumMap == null) {
                        //     System.out.println(i.getId() + "," + i.getTextContent());
                        // }
                        when(directClass.getScore()).thenReturn(enumMap.get(PhoneNumberType.Direct));

                        AnnotationPayload indirectClassPayload = mock(AnnotationPayload.class);
                        ClassificationAnnotation indirectClass = mock(ClassificationAnnotation.class);
                        when(indirectClassPayload.getDisplayName()).thenReturn("Indirect");
                        when(indirectClassPayload.getClassification()).thenReturn(indirectClass);
                        when(indirectClass.getScore()).thenReturn(enumMap.get(PhoneNumberType.Indirect));
                        List<AnnotationPayload> mockPayloadList = Arrays.asList(directClassPayload, indirectClassPayload);

                        when(mockResp.getPayloadList()).thenReturn(mockPayloadList);

                        Optional<PhoneNumberType> classify = classifier.classify(i);
                        classify.ifPresent( prediction -> {
                            i.predictedClass(classify.get());
                            GoogleClassifierModelTestBase.ModelStatus modelStatus = GoogleClassifierModelTestBase.ModelStatus.status(i.getPredictedClass(), i.getTaggedClass());
                            counters.put(modelStatus, counters.get(modelStatus) + 1);
                            if (modelStatus == GoogleClassifierModelTestBase.ModelStatus.FalsePositive || modelStatus == GoogleClassifierModelTestBase.ModelStatus.FalseNegative) {
                                incorrectPredictions.add(i);
                            }
                        });

                        if (!classify.isPresent()) {
                            GoogleClassifierModelTestBase.ModelStatus modelStatus = GoogleClassifierModelTestBase.ModelStatus.FalseNegative;
                            counters.put(modelStatus, counters.get(modelStatus) + 1);
                            notQualified.add(i);
                        }
                    });

                log.debug(classifier.statsSapshot().toString());
                log.debug(classifier.sampleSnapshot().toString()); 
                log.debug("\n");
                log.debug("Not Qualified Predictions");
                log.debug("id, actual");
                notQualified.stream().forEach(nq -> {
                    log.debug(String.format( "%s, %s", nq.getId(), nq.getTaggedClass()));
                });

                log.debug("\n\n");
                log.debug("Incorrect Predictions");
                log.debug("id, text, mimeType, actual, prediction");
                incorrectPredictions.stream().forEach(incorrect -> {
                    log.debug(String.format( "%s, %s, %s, %s, %s", 
                        incorrect.getId(), 
                        incorrect.getTextContent(), 
                        incorrect.getMimeType(),
                        incorrect.getTaggedClass(),
                        incorrect.getPredictedClass()));
                    });
                log.debug("\n\n");

                log.debug(String.format("threshold: %s", threshold));
                Float precision = GoogleClassifierModelTestBase.ModelStatus.precision(counters);
                log.debug(String.format("Precision %f", precision));
                Float recall = GoogleClassifierModelTestBase.ModelStatus.recall(counters);
                log.debug(String.format("Recall    %f", recall));
                Float accuracy = GoogleClassifierModelTestBase.ModelStatus.accuracy(counters);
                log.debug(String.format("Accuracy %f", accuracy));

                writer.write(String.format("%d,%f,%f,%f", threshold, precision, recall, accuracy));
                writer.newLine();
                writer.flush();
            }
        }
        classifier.setTestThreshold(null);
    }


    @Test
    @Ignore
    public void cleanUpRunScores() throws IOException {
        String googleScoresFile = GoogleClassifierUnitTest.class.getResource("classifier-run-test-1000-scores.csv").getFile();
        try (
            Reader reader = Files.newBufferedReader(Paths.get(googleScoresFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter("data/classifier-run-test-1000-scores.csv"));
        ) {
            Map<String, EnumMap<PhoneNumberType, Float>> snapshot = new HashMap<>();
            CsvToBean<GoogleContentScore> csvToBean = 
                new CsvToBeanBuilder<GoogleContentScore>(reader)
                    .withType(GoogleContentScore.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<GoogleContentScore> csvScoreIterator = csvToBean.iterator();

            writer.write("id,text,mimetype,classname,score");
            writer.newLine();
            while (csvScoreIterator.hasNext()) {
                GoogleContentScore csvScore = csvScoreIterator.next();
                // System.out.println(csvScore.getId() + "," + csvScore.getClassName());
                String textContent = csvScore.getText();
                String quotedText = textContent.contains(",") ? '\"' + textContent + '\"' : textContent;
                writer.write(String.format("%s,%s,%s,%s,%f", csvScore.getId(), 
                                                                    quotedText, 
                                                                    "text/plain",
                                                                    csvScore.getClassName().trim(),
                                                                    csvScore.getScore()));
                writer.newLine();
            }
        }
    }


    static Map<String, EnumMap<PhoneNumberType, Float>> googleScoresSnapshot(String infileName) {
        try (
            Reader reader = Files.newBufferedReader(Paths.get(infileName));
        ) {
            Map<String, EnumMap<PhoneNumberType, Float>> snapshot = new HashMap<>();
            CsvToBean<GoogleContentScore> csvToBean = 
                new CsvToBeanBuilder<GoogleContentScore>(reader)
                    .withType(GoogleContentScore.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<GoogleContentScore> csvScoreIterator = csvToBean.iterator();

            while (csvScoreIterator.hasNext()) {
                GoogleContentScore csvScore = csvScoreIterator.next();
                // System.out.println(csvScore.getId() + "," + csvScore.getClassName());
                String hash = hash(csvScore.getText());
                snapshot.computeIfAbsent(hash, key -> new EnumMap<PhoneNumberType, Float>(PhoneNumberType.class)).put(PhoneNumberType.valueOfCaseInsensitive(csvScore.getClassName()).get(), new Float(csvScore.getScore()));
            }
            return snapshot;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String hash(String input) 
    { 
        try { 
            MessageDigest md = MessageDigest.getInstance("SHA-1"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        } 
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    } 


    public static class GoogleContentScore {
        @CsvBindByName(column="id") private String id;
        @CsvBindByName private String text;
        @CsvBindByName private String className;
        @CsvBindByName private float score;

        public String getId() { return id; }
        public void setId(String id) {this.id = id; }
        public String getText() { return text; } 
        public void setText(String text) { this.text = text; }
        public String getClassName() { return className; }
        public void setClassName(String className) { this.className = className; } 
        public float getScore() { return score; } 
        public void setScore(float score) { this.score = score; } 
    }
}
