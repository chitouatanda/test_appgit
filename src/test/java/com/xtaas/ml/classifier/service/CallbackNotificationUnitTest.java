//package com.xtaas.ml.classifier.service;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//import com.xtaas.application.service.CampaignService;
//import com.xtaas.db.config.MongoConfig;
//import com.xtaas.db.repository.ApplicationPropertyRepository;
//import com.xtaas.db.repository.ProspectCallLogRepositoryImpl;
//import com.xtaas.mvc.model.CallbackProspectCacheDTO;
//import com.xtaas.service.PropertyServiceImpl;
//import com.xtaas.service.ProspectCacheServiceImpl;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.powermock.modules.junit4.PowerMockRunnerDelegate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.AnnotationConfigContextLoader;
//
//@RunWith(PowerMockRunner.class)
//@ContextConfiguration(classes = { ProspectCacheServiceImpl.class, PropertyServiceImpl.class,
//		ApplicationPropertyRepository.class, MongoConfig.class,CampaignService.class,
//		ProspectCallLogRepositoryImpl.class }, loader = AnnotationConfigContextLoader.class)
//@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
//public class CallbackNotificationUnitTest {
//
//	@Autowired
//	ProspectCacheServiceImpl prospectCacheServiceImpl;
//
//	@Test
//	public void delay_queue_should_return_object() throws InterruptedException, ParseException {
//		List<CallbackProspectCacheDTO> dtoList = new ArrayList<>();
//		CallbackProspectCacheDTO dto = new CallbackProspectCacheDTO();
//		dto.setAgentId("ixagent2");
//		dto.setCallRetryCount(1);
//		dto.setCampaignId("1234");
//		dto.setDataSlice("slice 0");
//		dto.setOutboundNumber("127832974680");
//		dto.setProspectCallId("yuqwtfxrplkmiujhbgfytqfrw");
//		CallbackProspectCacheDTO object = new CallbackProspectCacheDTO(dto, 5000);
//		dtoList.add(object);
//		prospectCacheServiceImpl.insertCallbackRecordsInCache(dtoList);
//		Thread.sleep(8000);
//		assertThat(prospectCacheServiceImpl.getCallbackRecordsFromCache()).isNotNull();
//	}
//	
//	@Test
//	public void delay_queue_should_not__return_object() throws InterruptedException, ParseException {
//		List<CallbackProspectCacheDTO> dtoList = new ArrayList<>();
//		CallbackProspectCacheDTO dto = new CallbackProspectCacheDTO();
//		dto.setAgentId("ixagent2");
//		dto.setCallRetryCount(1);
//		dto.setCampaignId("1234");
//		dto.setDataSlice("slice 0");
//		dto.setOutboundNumber("127832974680");
//		dto.setProspectCallId("yuqwtfxrplkmiujhbgfytqfrw");
//		CallbackProspectCacheDTO object = new CallbackProspectCacheDTO(dto, 9000);
//		dtoList.add(object);
//		prospectCacheServiceImpl.insertCallbackRecordsInCache(dtoList);
//		Thread.sleep(2000);
//		assertThat(prospectCacheServiceImpl.getCallbackRecordsFromCache()).isNull();
//	}
//}