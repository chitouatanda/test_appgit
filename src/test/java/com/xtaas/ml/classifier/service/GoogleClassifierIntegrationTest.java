package com.xtaas.ml.classifier.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;
import java.util.UUID;

import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *<p>
 *
 * @author sushant
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {GoogleClassifierStatistics.class}, loader = AnnotationConfigContextLoader.class)
public class GoogleClassifierIntegrationTest {
    final private GoogleClassifier classifier;
    
    /**
     * Default constructor
     */
    public GoogleClassifierIntegrationTest(@Autowired GoogleClassifierStatistics classifier) {
        this.classifier = classifier;
    }

    @Test
    @Disabled("require real connection params")
    void testClassify() {
        String content = "If you know your party's extension, you may dial it at any time. so, uh If you know your party's extension, you may dial it at any time.";

        Optional<PhoneNumberType> classOut = classifier.classify(
            new ClassifyContentInput(UUID.randomUUID(), content, "text/plain"));
        classOut.ifPresent(c -> assertEquals(PhoneNumberType.Indirect, c));
    }
}
