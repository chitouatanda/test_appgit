package com.xtaas.ml.classifier.service;

import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.UUID;

import com.opencsv.CSVReader;
import com.xtaas.ml.classifier.dto.ClassifyContentInput;
import com.xtaas.ml.classifier.service.GoogleClassifier.PhoneNumberType;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
public class GoogleClassifierModelTestBase {

    static class ClassifyModelTestIO extends ClassifyContentInput {
        private PhoneNumberType taggedClass;
        private PhoneNumberType predictedClass;

        public ClassifyModelTestIO(final UUID idIn, final String textContentIn, final String mimeTypeIn,
                final PhoneNumberType taggedClassIn) {
            super(idIn, textContentIn, mimeTypeIn);
            this.taggedClass = taggedClassIn;
        }

        public ClassifyModelTestIO predictedClass(final PhoneNumberType predictedClassIn) {
            this.predictedClass = predictedClassIn;
            return this;
        }

        public PhoneNumberType getTaggedClass() {
            return taggedClass;
        }

        public PhoneNumberType getPredictedClass() {
            return predictedClass;
        }
    }

    enum ModelStatus {
        TruePositive, FalsePositive, FalseNegative, TrueNegative;

        static public ModelStatus status(final PhoneNumberType predicted, final PhoneNumberType actual) {
            if (predicted == actual && PhoneNumberType.Direct == predicted) {
                return TruePositive;
            } else if (predicted != actual && PhoneNumberType.Direct == predicted) {
                return FalsePositive;
            } else if (predicted != actual && PhoneNumberType.Indirect == predicted) {
                return FalseNegative;
            } else {
                return TrueNegative;
            }
        }

        public static Float accuracy(EnumMap<ModelStatus, Integer> counters) {
            int totalCnt = counters.get(ModelStatus.TruePositive) + counters.get(ModelStatus.FalsePositive)
                    + counters.get(ModelStatus.FalseNegative) + counters.get(ModelStatus.TrueNegative);

            if (totalCnt == 0) {
                return Float.NaN;
            }
            return (counters.get(ModelStatus.TruePositive) * 1.0f + counters.get(ModelStatus.TrueNegative)) / totalCnt;
        }

        public static Float recall(EnumMap<ModelStatus, Integer> counters) {
            int totalTruePositive = counters.get(ModelStatus.TruePositive) + counters.get(ModelStatus.FalseNegative);
            if (totalTruePositive == 0) {
                return Float.NaN;
            }
            return counters.get(ModelStatus.TruePositive) * 1.0f / totalTruePositive;
        }

        public static Float precision(EnumMap<ModelStatus, Integer> counters) {
            int totalPredictedPositive = counters.get(ModelStatus.TruePositive)
                    + counters.get(ModelStatus.FalsePositive);
            if (totalPredictedPositive == 0) {
                return Float.NaN;
            }
            return counters.get(ModelStatus.TruePositive) * 1.0f / totalPredictedPositive;
        }
    }

    /**
     * Default constructor
     */
    public GoogleClassifierModelTestBase() {

    }

    static List<ClassifyModelTestIO> getClassfifyModelTestIOFromCSVFile(String infileName) {
        Path path = null;
        try {
            URL resource = GoogleClassifierModelTestBase.class.getResource(infileName);
            path = Paths.get(resource.toURI());
        } catch (URISyntaxException e1) {
            throw new RuntimeException(e1);
        }
        try (CSVReader reader = new CSVReader(new FileReader(path.toFile()))) {
            final List<ClassifyModelTestIO> inputList = new ArrayList<>();
            final List<String[]> records = reader.readAll();
    
            for (final String[] record : records) {
                if (record.length == 0) {
                    continue;
                }
                
                PhoneNumberType.valueOfCaseInsensitive(record[3])
                    .ifPresent(type -> inputList.add(new ClassifyModelTestIO(
                                                            UUID.fromString(record[0]),
                                                            record[1],
                                                            record[2],
                                                            type)));
                
            }
            return inputList;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}
