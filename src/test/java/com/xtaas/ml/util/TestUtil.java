package com.xtaas.ml.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * <p>
 *
 * @author sushant
 * @version 1.0
 */
public class TestUtil {

    static public <T> Path getFilePathFromSamePackageAsClass(Class<T> clazz, String fileName) {
        try {
            URL url = clazz.getResource(fileName);
            return Paths.get(url.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    static public Path getFileCreateFileAndAncestorsIfRequired(String dir, String fileName) {
        Path fpath = null;
        try {
            fpath = Paths.get(Files.createDirectories(Paths.get(dir)).toString(), fileName);
            if (Files.notExists(fpath)) {
                Files.createFile(fpath);
            }
        } catch (FileAlreadyExistsException e) {
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fpath;
    }
}
