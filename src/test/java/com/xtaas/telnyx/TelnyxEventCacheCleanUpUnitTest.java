package com.xtaas.telnyx;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.ConcurrentHashMap;
import com.xtaas.BeanLocator;
import com.xtaas.service.ProspectCacheServiceImpl;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = { ProspectCacheServiceImpl.class

}, loader = AnnotationConfigContextLoader.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
public class TelnyxEventCacheCleanUpUnitTest {

	@Autowired
	ProspectCacheServiceImpl prospectCacheService;

	@Autowired
	ApplicationContext context;

	public TelnyxEventCacheCleanUpUnitTest() {

	}

	@Before
	public void setup() {
		ReflectionTestUtils.setField(BeanLocator.class, "context", context);
	}

	@Test
	public void events_should_add_in_telnyx_map() {
		prospectCacheService.addInTelnyxEventIds("call.recording.saved1");
		prospectCacheService.addInTelnyxEventIds("call.recording.saved2");
		prospectCacheService.addInTelnyxEventIds("call.recording.saved3");
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		ConcurrentHashMap<String, DateTime> result = prospectCacheService.getTelnyxEventIds();
		assertThat(result.size()).isEqualTo(3);
	}

	@Test
//	@ThreadCount(5)
	public void map_should_be_empty_after_given_time() throws InterruptedException {
		prospectCacheService.addInTelnyxEventIdsForTest("call.recording.saved1", DateTime.now().minusMinutes(5));
		prospectCacheService.addInTelnyxEventIdsForTest("call.recording.saved2", DateTime.now().minusMinutes(4));
		prospectCacheService.addInTelnyxEventIdsForTest("call.recording.saved3", DateTime.now().minusMinutes(10));
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		prospectCacheService.cleanObsoleteTelnyxEventEntries();
		ConcurrentHashMap<String, DateTime> result = prospectCacheService.getTelnyxEventIds();
		assertThat(result.size()).isEqualTo(0);
	}

}