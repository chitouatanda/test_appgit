// package com.xtaas.service;

// import java.util.ArrayList;
// import java.util.List;

// import org.codehaus.jackson.annotate.JsonMethod;
// import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
// import org.codehaus.jackson.map.DeserializationConfig;
// import org.codehaus.jackson.map.ObjectMapper;
// import org.junit.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.junit.runner.RunWith;
// import org.powermock.modules.junit4.PowerMockRunner;
// import org.powermock.modules.junit4.PowerMockRunnerDelegate;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.junit.jupiter.SpringExtension;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.support.AnnotationConfigContextLoader;

// import com.xtaas.application.service.CampaignServiceImpl;
// import com.xtaas.db.config.MongoConfig;
// import com.xtaas.db.repository.DataBuyQueue;
// import com.xtaas.db.repository.DataBuyQueueRepository;
// import com.xtaas.domain.entity.Campaign;
// import com.xtaas.insideview.Companies;
// import com.xtaas.insideview.CompanyResponse;
// import com.xtaas.insideview.PersonResponse;

// @RunWith(SpringJUnit4ClassRunner.class)
// //@RunWith(PowerMockRunner.class)
// @ContextConfiguration(classes = { CampaignServiceImpl.class , DataBuyQueueRepository.class, InsideViewServiceImpl.class

// }, loader = AnnotationConfigContextLoader.class)
// //@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
// public class InsideViewBuyTest {

// 	@Autowired
// 	private InsideViewServiceImpl insideViewServiceImpl;
	
// 	@Autowired
// 	private DataBuyQueueRepository dataBuyQueueRepository;
	
// 	@Autowired
// 	private CampaignServiceImpl campaignService;
	
// 	@Test
// 	public void searchCompany() {
// 		getDbqObject();
		
// 		StringBuffer sb  = new StringBuffer();
// 		try {
// 			//either name/website/ticker is mandatory
			
// 			/*
// 			 * 	HTTP 400
// 					Invalid query parameter(s).

// 				HTTP 401
// 					Unauthorized Error - Cannot use API's without setting a valid accessToken in header.

// 				HTTP 405
// 					Method Not Allowed - Your are not allowed to access this API.

// 				HTTP 429
// 					Request is throttled.
// 			 */
// 		String personresp = "{\"contacts\":[{\"id\":\"OD7VdWUCGGSre-puvpKoJlscbecNBSzgDhTS63P6oHoyVbQg5Ib0Dn9fHD3-gprk\",\"companyId\":\"2073410\",\"companyName\":\"#1 Network, Inc.\",\"titles\":[\"Webmaster\"],\"city\":\"Effingham\",\"state\":\"IL\",\"country\":\"United States\",\"active\":true,\"hasPhone\":true,\"hasEmail\":false,\"firstName\":\"Barb\",\"lastName\":\"Gould\",\"fullName\":\"Barb Gould\",\"peopleId\":\"72rIihOXZC-Z8Mz3BIQo3SpqDr6m5GrDvPW5IuhXerE0DsqIgt9Sn_8DhzoFuOxK\",\"confidenceScore\":25.0,\"phoneType\":\"CORP\"},{\"id\":\"mURVmNFEvZJ4OiLUE8I5u72jCRZuMDC79OS5CnzOSh2FjbTqSSNNxfV6zJTOuhm-\",\"companyId\":\"2073410\",\"companyName\":\"#1 Network, Inc.\",\"titles\":[\"Controller\"],\"city\":\"Effingham\",\"state\":\"IL\",\"country\":\"United States\",\"active\":true,\"hasPhone\":true,\"hasEmail\":false,\"firstName\":\"Greg\",\"lastName\":\"Koester\",\"fullName\":\"Greg Koester\",\"peopleId\":\"Mww_J9VN2LspQxtKfMIMbOlYkNdwU3FkM_epG1YLdi1ivAQwqkoCwqLZjiSFWBR5\",\"confidenceScore\":38.0,\"phoneType\":\"CORP\"}],\"totalResults\":\"1851207\",\"page\":\"1\",\"resultsPerPage\":\"2\"}";

// 		String response = "{\"companies\":[{\"name\":\"Alphabet Inc\",\"city\":\"Mountain View\",\"state\":\"CA\",\"country\":\"United States\",\"companyId\":736233},{\"name\":\"Google LLC\",\"city\":\"Mountain View\",\"state\":\"CA\",\"country\":\"United States\",\"companyId\":7573216},{\"name\":\"Google UK Limited\",\"city\":\"London\",\"country\":\"United Kingdom\",\"companyId\":1416385},{\"name\":\"Google Ireland Limited\",\"city\":\"Dublin\",\"state\":\"Co. Dublin\",\"country\":\"Ireland\",\"companyId\":1475170},{\"name\":\"Google India Pvt Ltd\",\"city\":\"Bengaluru\",\"state\":\"Karnataka\",\"country\":\"India\",\"companyId\":1750717},{\"name\":\"Google Australia Pty. Ltd.\",\"city\":\"Sydney\",\"state\":\"NSW\",\"country\":\"Australia\",\"companyId\":1456802},{\"name\":\"Google Canada Corporation\",\"city\":\"Toronto\",\"state\":\"ON\",\"country\":\"Canada\",\"companyId\":2725793},{\"name\":\"Google Germany GmbH\",\"city\":\"Hamburg\",\"state\":\"HH\",\"country\":\"Germany\",\"companyId\":3221568},{\"name\":\"Google Ventures\",\"city\":\"Mountain View\",\"state\":\"CA\",\"country\":\"United States\",\"companyId\":15183010},{\"name\":\"Google Affiliate Network Inc.\",\"country\":\"United States\",\"companyId\":1739593}],\"page\":1,\"resultsPerPage\":10,\"totalResults\":108}";
// 		CompanyResponse companies = null;
// 	        if (response != null) {
// 	            ObjectMapper mapper= new ObjectMapper().setVisibility(JsonMethod.FIELD,Visibility.ANY);
// 	            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
// 	            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
// 	            companies=mapper.readValue(response.toString(), CompanyResponse.class);
// 	            System.out.println(companies);
// 	        }
	        
// 	        PersonResponse personResponse = null;
// 	        if (personresp != null) {
// 	            ObjectMapper mapper= new ObjectMapper().setVisibility(JsonMethod.FIELD,Visibility.ANY);
// 	            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
// 	            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
// 	            personResponse=mapper.readValue(personresp.toString(), PersonResponse.class);
// 	            System.out.println(personResponse);
// 	        }
			
// 			/*
// 			 * TODO dont have access to get company search
// 			 */
// 			sb.append("name=Google");
// 			//sb.append("website=google.com");
// 			String params =  sb.toString(); 
// 			String companyurl = "https://api.insideview.com/api/v1/companies";
// 			insideViewServiceImpl.getIVResponse(companyurl,params);
			
			
			
			
// 			//sb.append("agents");//***??????
// 			//sb.append("areaCodes=234,650");//*** why did you get this ? {"totalResults":"0","page":"1","resultsPerPage":"1"}
// 			//sb.append("businessTypes=public,private");/***
// 			//sb.append("cities=Arizona,San Fransisco");/*** {"totalResults":"0","page":"1","resultsPerPage":"1"}
// 			//sb.append("&peopleCountries=249");// if countries are given,then this should not be there in search other wise error.
// 			//sb.append("&peopleStates=California,new york");/*** {"totalResults":"0","page":"1","resultsPerPage":"1"}
// 			//sb.append("&state=California,new york");//*** {"totalResults":"0","page":"1","resultsPerPage":"1"}
// 			//sb.append("&companyName=Google, Inc.");//*** {"totalResults":"0","page":"1","resultsPerPage":"1"}
// 			//sb.append("regions=NorthAmerica");
// 			sb.append("countries=249,247");//https://kb.insideview.com/hc/en-us/articles/203433458-API-Reference-Data
// 			//sb.append("&industries=8,9")
// 			sb.append("&subIndustries=8_1,8_10,8_11,8_12,8_13,8_14,8_15,8_16,8_17,8_18,8_19,8_2,8_20");//https://kb.insideview.com/hc/en-us/articles/203433458-API-Reference-Data
// 			sb.append("&active=true");//If present returns only active / inactive contacts
// 		    sb.append("&isEmailRequired=false");//If set to true, only contacts with an email address will be counted.
// 			sb.append("&isPhoneRequired=true");//If set to true, only contacts with a phone number will be counted.
// 			//sb.append("&phoneType=DIRECT");//***Accepts DIRECT, CORP, and ANY to find the corresponding contact's phone numbers. 
// 			sb.append("&jobFunctions=7,8");////https://kb.insideview.com/hc/en-us/articles/203433458-API-Reference-Data
// 			sb.append("&jobLevels=5,4");
// 			/*sb.append("&minEmployees=10");
// 			sb.append("&maxEmployees=100");
// 			sb.append("&minRevenue=500");
// 			sb.append("&maxRevenue=1000000");*/
// 			//sb.append("&naics=511");
// 			//sb.append("&sic=511")
// 			//sb.append("&categoryIds=1000000");//??????
// 			//sb.append("&subCategoryIds=1000000");//??????
// 			//sb.append("&productIds")//????
// 			sb.append("&primaryIndustryOnly=true");
// 			//sb.append("&primaryNAICSOnly=true");
// 			sb.append("&primarySICOnly=true");

			
// 			//sb.append("titles=manager");//***
// 			sb.append("&resultsPerPage=1");//Default: 10, maximum value: 500
// 			sb.append("&page=1");//Default: 1
// 			sb.append("&sortBy=companyName");//Attributes used to sort the list of results. Valid values are: active, title, popularity, companyName
// 			sb.append("&sortOrder=asc");//Valid values are: asc, desc
		


// 			String params1 =  sb.toString();
			
// 			String personUrl = "https://api.insideview.com/api/v1/target/contacts";
// 			//insideViewServiceImpl.getPersonResponseCriteria(personUrl,params);
// 			String  contactId = "OD7VdWUCGGSre-puvpKoJkHuxZ36PYMu_z97EuOYDRrxtCoSeIjFNqR0jHmxcmnZ";
// 			String personDetailUrl = "https://api.insideview.com/api/v1/target/contact/";
// 			//insideViewServiceImpl.getResponseDetails(personDetailUrl,contactId);

// 		}catch(Exception e) {
// 			e.printStackTrace();
// 		}
		
// 	}
	
// 	private DataBuyQueue getDbqObject() {
// 		DataBuyQueue dbq = dataBuyQueueRepository.findByCampaignIdAndStatus("5f501202fa930e671e78337a", "COMPLETED");
// 		List<String> campaignIds = new ArrayList<String>();
// 		campaignIds.add("5f501202fa930e671e78337a");
// 		List<Campaign> campaigns = campaignService.getCampaignByIds(campaignIds);
// 		Campaign	campaign =campaigns.get(0);
// 		insideViewServiceImpl.getData(campaign, dbq, 100L);
// 		return dbq;
// 	}
// }
