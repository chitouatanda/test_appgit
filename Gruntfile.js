module.exports = function(grunt) {
	grunt.initConfig({
		config: {
			local: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://gory-web-2586.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			qa: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			dev: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://gory-web-2586.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			staging: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://mighty-wave-2270.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			prod: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			corp2: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			demand: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			devpsh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			qapsh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			stagingpsh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			prodpsh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			corp2psh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			},
			demandpsh: {
			    options: {
			    	variables: {
			    		'pusherToken': '1d602eed081f1f7dd639',
			    		'pusherCluster': 'us2',
			    		'bssServerBaseUrl': 'http://xtaascorp-bss.herokuapp.com',
			    		'reportingUrl': 'http://52.0.132.241/jasperserver-pro'
			    	}
			    }
			}
		},
		replace: {
			dist: {
			    options: {
			    	variables: {
			    		'pusherToken': '<%= grunt.config.get("pusherToken") %>',
			    		'pusherCluster': '<%= grunt.config.get("pusherCluster") %>',
			    		'bssServerBaseUrl': '<%= grunt.config.get("bssServerBaseUrl") %>',
			    		'reportingUrl': '<%= grunt.config.get("reportingUrl") %>'
			    		
			    	},
			    	force: true
			    },
			    files: [{
			    	expand: true, flatten: true, src: ['src/main/client/js/**'], dest: 'src/main/webapp/js/'
			    }, {
			    	expand: true, flatten: true, src: ['src/main/client/partials/**'], dest: 'src/main/webapp/partials/'
			    }]
			}
		}
	});
	grunt.loadNpmTasks('grunt-config');
	grunt.loadNpmTasks('grunt-replace');
	grunt.registerTask('default', ['config:' + process.env.SPRING_PROFILES_ACTIVE, 'replace']);
}