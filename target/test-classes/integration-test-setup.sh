#!/bin/bash

docker image build -t xtaas-mongodb:integration-test .

docker run -p 27017:27017 --name xtaas-mongodb-integration-test xtaas-mongodb:integration-test
